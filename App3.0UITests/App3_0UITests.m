//
//  App3_0UITests.m
//  App3.0UITests
//
//  Created by mac on 17/2/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface App3_0UITests : XCTestCase

@end

@implementation App3_0UITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
//    [[app.collectionViews.cells.otherElements containingType:XCUIElementTypeStaticText identifier:@"\U4e0d\U8bb8\U5220\U6211\U5546\U54c1"].element swipeUp];
    [app.buttons[Localized(@"enter_login")] tap];
    
    XCUIElement *textField = [app.textFields elementBoundByIndex:0];
    XCTAssert(textField.exists);
    [textField tap];
    [textField typeText:@"18866839200"];
    
    XCUIElement *textField2 = [app.secureTextFields element];
    XCTAssert(textField.exists);
    [textField2 tap];
    [textField2 typeText:@"123456"];
    [app.buttons[Localized(@"enter_login")] tap];
    
    sleep(3);
    [app.tabBars.buttons[@"购物"] tap];
    
    XCUIElement *collectionView = [[[[[[[[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeCollectionView].element;
    
    int ccc =  (int)collectionView.cells.count;
    
    for (int x = 0; x < ccc; x+=2) {
    
        [collectionView swipeUp];
        
    }
    
    
}

@end
