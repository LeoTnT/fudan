

//
//  EnterViewController.m
//  App3.0
//
//  Created by mac on 17/3/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "EnterViewController.h"
#import "FDLoginController.h"
#import "XSRegisterViewController.h"
#import "VersionUpdateModel.h"
#import "RemindVersionUpdateView.h"
#import "RegisterModel.h"
#import "LaunchIntroductionView.h"

#import "LCTabbarController.h"
#import "MFSideMenuContainerViewController.h"
#import "LeftController.h"

@interface EnterViewController ()<RemindVersionUpdateViewDelegate>
/**注册vc*/
@property(nonatomic,strong)RemindVersionUpdateView *remindView;
//计时器
@property (nonatomic, strong) dispatch_source_t timer;

@property (nonatomic, assign) BOOL isFinishGif;
@property (nonatomic, assign) BOOL isFinishNet;

@end

@implementation EnterViewController {
    UIImageView *backImage;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    backImage = [UIImageView new];
    backImage.image = IS_iPhoneX? [UIImage imageNamed:@"background_image_X"]:[UIImage imageNamed:@"background_image"];
    backImage.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    [self.view addSubview:backImage];

    //版本更新
    [self getVersionInfo];
    // 获取商城配置
    [self getAppConfig];

    //播放启动gif
    [self startGifPlay];
}

- (void)getVersionInfo {
    [HTTPManager getVersionInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            VersionUpdateDataModel *model=[VersionUpdateDataModel mj_objectWithKeyValues:dic[@"data"]];
            //获取当前版本
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            CFShow((__bridge CFTypeRef)(infoDictionary));
            NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            //比较版本
            if ([app_Version compare:model.client_version options:NSNumericSearch] ==NSOrderedAscending) {
                self.remindView=[[RemindVersionUpdateView alloc] initWithInfo:model];
                self.remindView.delegate=self;
            }else{
                [self logAction];
            }
        }else{
            [self logAction];
        }
    } fail:^(NSError *error) {
        [self logAction];
    }];
}

- (void)getAppConfig {
    [HTTPManager getShopConfigWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            AppConfigModel *config = [AppConfigModel mj_objectWithKeyValues:dic[@"data"]];
            [AppConfigManager ShardInstnce].appConfig = config;
            if (!config.is_enable_app) {
                // 系统维护
                [[NSNotificationCenter defaultCenter] postNotificationName:@"systemMaintenance" object:nil];
            }
        }
    } fail:^(NSError *error) {

    }];
}

- (void)closeUpdateView{
    [self.remindView removeFromSuperview];
    [self logAction];
}

- (void)logAction {

    // 如果本地存在guid，自动登录
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];

    NSLog(@"guid = %@",guid);
    NSData *data = [ud objectForKey:USERINFO_LOGIN];
    LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
    NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,userInfo.uid]];

    if (!isEmptyString(guid)) {
        [HTTPManager autoLoginWithSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
            BOOL data = [dic[@"data"] boolValue];
            if (state.status && data) {
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [[UserInstance ShardInstnce] setupUserInfo];
                });

                //登录环信
                [[EMClient sharedClient] loginWithUsername:userInfo.uid password:pwd completion:^(NSString *aUsername, EMError *aError) {
                    if (!aError) {
                        self.isFinishNet = YES;
                        if (self.isFinishGif && self.isFinishNet) {
                            //倒计时结束切换根控制器
                            [self prepareLogin:YES];
                        }

                    } else {
                        NSLog(@"登录失败");
                        [self removeUserConfig];
                    }
                }];

            } else {
                [self removeUserConfig];
            }
        } fail:^(NSError * _Nonnull error) {
            [self removeUserConfig];
        }];
    }else{
        [self removeUserConfig];
    }
}

- (void)removeUserConfig {
    self.isFinishNet = YES;
    // 清除可能存在的数据
    [[UserInstance ShardInstnce] logout];
    if (self.isFinishGif && self.isFinishNet) {
        //倒计时结束切换根控制器
        [self prepareLogin:YES];
    }
}

- (void)prepareLogin:(BOOL)canEnter {

    UIWindow *window= [UIApplication sharedApplication].keyWindow;
    LCTabbarController *TabVC = [[LCTabbarController alloc] init];
    LeftController *leftVC = [[LeftController alloc] init];
    MFSideMenuContainerViewController *containerVC = [MFSideMenuContainerViewController containerWithCenterViewController:TabVC leftMenuViewController:leftVC rightMenuViewController:nil];
    containerVC.menuSlideAnimationEnabled = NO;//
    containerVC.shadow.enabled = NO;//去除阴影
    [containerVC setLeftMenuWidth:(FontNum(213)+20)];//设置左滑视图宽度
    [containerVC setPanMode:MFSideMenuPanModeSideMenu];
    window.rootViewController = containerVC;

    if (canEnter) {
        [backImage removeFromSuperview];
    } else {
        [window addSubview:backImage];
        FDLoginController *login = [[FDLoginController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
        [window.rootViewController presentViewController:navi animated:YES completion:^{
            [backImage removeFromSuperview];
        }];
    }
    
    // 引导页
    if (![[XSTool getStrUseKey:@"IS_FIRST_Load"] isEqualToString:@"YES"]) {
        [XSTool saveStr:@"YES" forKey:@"IS_FIRST_Load"];
        LaunchIntroductionView *launch = [LaunchIntroductionView sharedWithImages:@[@"launch0.png",@"launch1.png",@"launch2.png"]];
        launch.currentColor = [UIColor whiteColor];
    }
}

- (void)loginAction {
    FDLoginController *loginVC = [[FDLoginController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

- (void)registerAction {
    XSRegisterViewController *registerVC = [[XSRegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}


#pragma mark ==================gif广告UI==================
- (void)startGifPlay {

    /********* 动图数组 *********/
    UIImageView *imageV = [[UIImageView alloc] init];
    [self.view addSubview:imageV];

    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view.mas_width);
    }];

    //gif
    imageV.image = [UIImage animatedImageWithImages:[self getImagesWithGif:@"fd_ad"] duration:3];
    [self startCountdown];
}

#pragma mark -- 事件处理
- (void)startCountdown {

    int totalTime = 4;

    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:self.timer walltime:totalTime stop:^{

        dispatch_source_cancel(self.timer);

        self.isFinishGif = YES;

        if (self.isFinishGif && self.isFinishNet) {
            //倒计时结束切换根控制器
            [self prepareLogin:YES];
        }

    } otherAction:^(int time) {

    }];
}

#pragma mark == 根据gif获取图片数组
- (NSArray *)getImagesWithGif:(NSString *)gifName {

    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:gifName withExtension:@"gif"];

    CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef)fileUrl, NULL);
    size_t gifCount = CGImageSourceGetCount(gifSource);
    NSMutableArray *frames = [[NSMutableArray alloc]init];
    for (size_t i = 0; i< gifCount; i++) {
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
        UIImage *image = [UIImage imageWithCGImage:imageRef];
        [frames addObject:image];
        CGImageRelease(imageRef);
    }
    return frames;
}

@end


