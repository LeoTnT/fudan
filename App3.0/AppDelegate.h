//
//  AppDelegate.h
//  App3.0
//
//  Created by mac on 17/2/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSExceptionHandler.h"
#import "XSFileLogger.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) XSFileLogger *logger;

@property (nonatomic, assign) BOOL isEable;

@end

