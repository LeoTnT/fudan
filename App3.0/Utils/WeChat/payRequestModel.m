//
//  payRequestModel.m
//  App3.0
//
//  Created by xinshang on 2017/9/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "payRequestModel.h"

@implementation payRequestModel


+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"openID":@"appid",
             @"partnerId":@"partnerid",
             @"prepayId":@"prepayid",
             @"nonceStr":@"noncestr",
             @"timeStamp":@"timestamp",
             @"package":@"package",
             @"sign":@"sign"
             };
}

@end
