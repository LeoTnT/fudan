//
//  ViewController.m
//  BMKSearch
//
//  Created by apple on 2017/7/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "S_BaiduMap.h"
#import "SearchTableView.h"
#import "AddStatusViewController.h"
#import "AddVideoStatusViewController.h"
#import "LocateViewController.h"
@interface S_BaiduMap ()<UITableViewDataSource,UITableViewDelegate,UISearchControllerDelegate,UISearchResultsUpdating,BMKLocationServiceDelegate,BMKMapViewDelegate,BMKGeoCodeSearchDelegate,BMKPoiSearchDelegate,UIActionSheetDelegate,MKMapViewDelegate,CLLocationManagerDelegate>

@property (nonatomic ,strong)UITableView *tableView;

@property (nonatomic ,strong)BMKMapView *mapView;

@property (nonatomic ,strong)BMKLocationService *locService;
@property (nonatomic ,strong)BMKGeoCodeSearch *geoCodeSearch;//geo搜索服务
@property (nonatomic ,strong)BMKPointAnnotation *pointAnnotation;
@property (nonatomic ,strong)BMKPoiSearch *poisearch;
@property (nonatomic, strong) UISearchController *searchController;
@property(nonatomic,assign)CLLocationCoordinate2D currentCoordinate;
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,assign)NSInteger currentSelectLocationIndex;
@property (nonatomic ,strong)UIButton *mapPoint;
@property (nonatomic ,strong)UIButton *mapCenter;
@property (nonatomic ,strong)SearchTableView *tab;

@property (nonatomic ,strong)UIButton *rightButtonItem ;
@property (nonatomic ,assign)BOOL isSelected;


@property (strong, nonatomic) NSArray *maps;

@end

@implementation S_BaiduMap
{
    CLLocationCoordinate2D locationMapCenter;
    // 搜索
    CLLocationCoordinate2D leftBottomPoint;
    CLLocationCoordinate2D rightBottomPoint;
    BOOL isShowSearch;
    BMKPoiInfo *currentLocation;
    BOOL isShowPath;
    BOOL isSlide;// 是否滑动
}
-(BMKPointAnnotation *)pointAnnotation {
    if (!_pointAnnotation) {
        
        _pointAnnotation = [[BMKPointAnnotation alloc]init];
        [self.mapView addAnnotation:_pointAnnotation];
    }
    return _pointAnnotation;
}



-(UIButton *)mapPoint {
    if (!_mapPoint) {
        
        _mapPoint = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.mapView addSubview:_mapPoint];
        [_mapPoint mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mapView.mas_centerX);
            make.centerY.mas_equalTo(self.mapView.mas_centerY);
        }];
    }
    return _mapPoint;
}


-(UIButton *)mapCenter {
    if (!_mapCenter) {
        
        _mapCenter = [UIButton buttonWithType:UIButtonTypeCustom];
        [_mapCenter setBackgroundImage:[UIImage imageNamed:@"earth01_index_icon_19"] forState:UIControlStateNormal];
        [_mapCenter addTarget:self action:@selector(setMapCenter) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:_mapCenter];
    }
    return _mapCenter;
}


- (UIButton *)rightButtonItem {
    if (!_rightButtonItem) {
        
        _rightButtonItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [_rightButtonItem setTitleColor:[UIColor hexFloatColor:@"00CD00"] forState:UIControlStateNormal];
        [_rightButtonItem addTarget:self action:@selector(sendLocation:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_rightButtonItem]];
    }
    return _rightButtonItem;
}

- (void) setMapCenter {
    
    [self setMapCenterCoordinate:locationMapCenter];
    self.currentCoordinate = locationMapCenter;
    
}

- (BMKPoiSearch *)poisearch {
    if (!_poisearch) {
        
        _poisearch = [[BMKPoiSearch alloc] init];
        _poisearch.delegate = self;
    }
    return _poisearch;
}

-(BMKLocationService *)locService {
    if (!_locService) {
        _locService = [[BMKLocationService alloc]init];
        
        
    }
    return _locService;
}

-(BMKGeoCodeSearch *)geoCodeSearch {
    if (!_geoCodeSearch) {
        _geoCodeSearch = [[BMKGeoCodeSearch alloc]init];
        _geoCodeSearch.delegate = self;
    }
    return _geoCodeSearch;
}

- (BMKMapView *)mapView {
    if (!_mapView) {
        
        _mapView = [[BMKMapView alloc] init];
//        _mapView.zoomEnabled = NO;
//        _mapView.zoomEnabledWithTap  = NO;
        _mapView.zoomLevel = 17;
        _mapView.scrollEnabled = YES;
        _mapView.userTrackingMode = BMKUserTrackingModeHeading;//设置定位的状态
        _mapView.mapType = BMKMapTypeStandard;//设置地图为空白类型
        _mapView.showsUserLocation = NO;//先关闭显示的定位图层
        _mapView.userTrackingMode = BMKUserTrackingModeFollow;//设置定位的状态
        _mapView.showsUserLocation = YES;//显示定位图层
        
        
    }
    return _mapView;
}



- (UIView *) searchvC {
    self.tab =   [SearchTableView new];
    @weakify(self);
    [self.tab setTouch:^(BMKPoiInfo *model){
        @strongify(self);
        self.searchController.active = NO;
        [self setMapCenterCoordinate:model.pt];
        self.searchController.searchBar.text = nil;
        
    }];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.tab];
    _searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = YES;
    _searchController.searchBar.frame = CGRectMake(0, 0, mainWidth, 44.0);
    _searchController.searchBar.placeholder =@"搜索地点";
    return _searchController.searchBar;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets  = NO;
    self.title = Localized(@"位置");
    UIButton *cancel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    [cancel setTitle:Localized(@"cancel_btn") forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancel]];
    
 
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.mapView.delegate = self;
}

- (void) cancelAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)sendLocation:(UIButton *)sender
{
    
    if ([sender.titleLabel.text  isEqualToString:@"发送"]) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(sendLocationLatitude:andAddress:)]) {
            if (self.dataSource.count>1) {
                if (self.isSelected) {
                    BMKPoiInfo *model = self.dataSource[1];
//                    if (self.fatherVC &&([self.fatherVC isKindOfClass:[AddStatusViewController class]] || [self.fatherVC isKindOfClass:[AddVideoStatusViewController class]]))  {
//                        [self.delegate sendLocationLatitude:model.pt andAddress:model.name];
//                    }else{
                       [self.delegate sendLocationLatitude:model.pt andAddress:model.address];
//                    }
                    
                }else{
                    BMKPoiInfo *model = self.dataSource[0];
//                    if (self.fatherVC &&([self.fatherVC isKindOfClass:[AddStatusViewController class]] || [self.fatherVC isKindOfClass:[AddVideoStatusViewController class]])) {
//                        [self.delegate sendLocationLatitude:model.pt andAddress:model.name];
//                    }else{
                        [self.delegate sendLocationLatitude:model.pt andAddress:model.address];
//                    }
                }
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([sender.titleLabel.text  isEqualToString:@"导航"]){
        
        [self navgationSender];
        NSLog(@" - - - -  - -导航");
    }
    
}



- (void)setShowType:(BMKShowType)showType {
    _showType = showType;
    
    switch (showType) {
        case BMKShowSearchList:
            [self showLocationInfor];
            break;
        case BMKShowMap:
            [self showMap];
            break;
            
    }
}

- (void) showMap {
    
    [self.view addSubview:self.mapView];
    self.mapView.frame = CGRectMake(0, 64, mainWidth, mainHeight);
    [self.rightButtonItem setTitle:@"导航" forState:UIControlStateNormal];
}



- (void) showLocationInfor {
    
    self.currentSelectLocationIndex = 0;
    self.mapView.frame = CGRectMake(0, 50, mainWidth, 200);
    _dataSource = [NSMutableArray array];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+250,  mainWidth , mainHeight - 64 - 250) style:UITableViewStylePlain];
    
    UIView *backView = [UIView new];
    backView.frame = CGRectMake(0, 64, mainWidth, 250);
    [backView addSubview:self.mapView];
    UIView *view = [self searchvC];
    
    [backView addSubview:view];
    
    [self.view addSubview:backView];
    
    _tableView.tableFooterView = [UIView new];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:self.tableView];
    [self.mapView bringSubviewToFront:self.mapPoint];
    [self.locService startUserLocationService];
    [self.mapCenter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mapView.mas_right).offset(-10);
        make.bottom.mas_equalTo(self.mapView.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    self.locService.delegate = self;
    [self.rightButtonItem setTitle:@"发送" forState:UIControlStateNormal];
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
}


//FIXME:
- (void)setLocationCoordinate:(CLLocationCoordinate2D)locationCoordinate {
    _locationCoordinate = locationCoordinate;
//    locationMapCenter = locationCoordinate;
    isShowPath = YES;
    _pointAnnotation = [[BMKPointAnnotation alloc]init];
    [self.mapView addAnnotation:_pointAnnotation];
    self.pointAnnotation.coordinate = locationCoordinate;
    self.mapView.centerCoordinate = locationCoordinate;


//    [self setMapCenterCoordinate:locationCoordinate];
//    self.currentCoordinate = locationCoordinate;
    
}



/**
 搜索完成 设置中心。
 
 @param cccc cccc description
 */
- (void) setMapCenterCoordinate:(CLLocationCoordinate2D )cccc {
    if (!isShowPath) {
        
        BMKCoordinateRegion region ;//表示范围的结构体
        region.center = cccc;//中心点
        region.span.latitudeDelta = 0.006;//经度范围（设置为0.1表示显示范围为0.2的纬度范围）
        region.span.longitudeDelta = 0.006;//纬度范围
        [self.mapView setRegion:region animated:YES];
//
//        currentLocation = nil;
        
    }
}


#pragma mark BMKLocationServiceDelegate
- (void) didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    if (!isShowPath) {
        
        [_mapView updateLocationData:userLocation];
        locationMapCenter = userLocation.location.coordinate;
        
        [self setMapCenterCoordinate:locationMapCenter];
    }
    
}

#pragma mark BMKMapViewDelegate
/**
 *地图区域改变完成后会调用此接口
 *@param mapview 地图View
 *@param animated 是否动画
 */
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (!isShowPath) {
        
        CLLocationCoordinate2D MapCoordinate=[_mapView convertPoint:_mapPoint.center toCoordinateFromView:_mapView];
        self.pointAnnotation.coordinate = MapCoordinate;
        if(isSlide) {
            isSlide = NO;
            self.currentSelectLocationIndex = 1;
            return;
        }
        if (self.currentCoordinate.latitude == MapCoordinate.latitude && self.currentCoordinate.longitude == MapCoordinate.longitude) return;
        self.currentCoordinate = MapCoordinate;
        
    }
}



/**
 显示传入经纬度对应地点
 */
- (void)setCurrentCoordinate:(CLLocationCoordinate2D)currentCoordinate {
    
    _currentCoordinate = currentCoordinate;
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = currentCoordinate ;
    BOOL flag = [self.geoCodeSearch reverseGeoCode:reverseGeocodeSearchOption];
    if(flag)    [self.locService stopUserLocationService];
    
}



#pragma mark BMKGeoCodeSearchDelegate

/**
 *返回反地理编码搜索结果
 *@param searcher 搜索对象
 *@param result 搜索结果
 *@param error 错误号，@see BMKSearchErrorCode
 */

- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    

        if (error == BMK_SEARCH_NO_ERROR) {
            [self.dataSource removeAllObjects];
            [self.dataSource addObjectsFromArray:result.poiList];
            
            if (!currentLocation) {
                
                currentLocation =[[BMKPoiInfo alloc]init];
                currentLocation.address=result.address;
                currentLocation.name=@"[当前位置]";
                currentLocation.pt=result.location;
                currentLocation.city=result.addressDetail.city;
            }
            [self.dataSource insertObject:currentLocation atIndex:0];
            
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
            [self.locService stopUserLocationService];
            
        }else{
            
            NSLog(@"errr   =%u",error);
        }
    

}




/**
 UISearchController 搜索字符串
 */
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    searchController.searchBar.showsCancelButton = YES;
    
    isShowSearch = YES;
    if (searchController.searchBar.text.length >0 && ![searchController.searchBar.text isEqualToString:@""]) {
        
        [self startSearchMapPath:searchController.searchBar.text];
    }
}



/**
 区域搜索
 */
- (void) startSearchMapPath:(NSString *)searchText {
    leftBottomPoint = [_mapView convertPoint:CGPointMake(0,_mapView.frame.size.height) toCoordinateFromView:self.mapView];
    rightBottomPoint = [_mapView convertPoint:CGPointMake(_mapView.frame.size.width,0) toCoordinateFromView:self.mapView];
    
    BMKBoundSearchOption*boundSearchOption = [[BMKBoundSearchOption alloc]init];
    boundSearchOption.pageIndex = 0;
    boundSearchOption.pageCapacity = 20;
    boundSearchOption.keyword = searchText;
    boundSearchOption.leftBottom =leftBottomPoint;
    boundSearchOption.rightTop =rightBottomPoint;
    
    BOOL flag = [self.poisearch poiSearchInbounds:boundSearchOption];
    if(flag) NSLog(@"范围内检索发送成功");
    else NSLog(@"范围内检索发送失败");
    
}

#pragma mark implement BMKSearchDelegate
- (void) onGetPoiResult:(BMKPoiSearch *)searcher result:(BMKPoiResult*)result errorCode:(BMKSearchErrorCode)error {
    if (error == BMK_SEARCH_NO_ERROR) {
        NSMutableArray *arr = [NSMutableArray array];
        for (int i = 0; i < result.poiInfoList.count; i++) {
            BMKPoiInfo *first =result.poiInfoList[i];
            [arr addObject:first];
        }
        self.tab.dataSource = arr;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MapCellIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MapCellIdentifier"];
    }
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BMKPoiInfo *model=[self.dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text=model.name;
    cell.detailTextLabel.text=model.address;
    cell.detailTextLabel.textColor=[UIColor grayColor];
    if (self.isSelected) {
        cell.accessoryType = indexPath.row == self.currentSelectLocationIndex ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
    }else{
        cell.accessoryType = indexPath.row == 0 ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.isSelected=YES;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType =  UITableViewCellAccessoryCheckmark;
    BMKPoiInfo *model=[self.dataSource objectAtIndex:indexPath.row];
    BMKMapStatus *mapStatus =[self.mapView getMapStatus];
    mapStatus.targetGeoPt=model.pt;
    isSlide = YES;
    [self.mapView setMapStatus:mapStatus withAnimation:YES];
    self.currentSelectLocationIndex=indexPath.row;
    [self.tableView reloadData];
}


- (void)didDismissSearchController:(UISearchController *)searchController {
    
    isShowSearch = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    self.mapView.delegate = nil;
    self.locService.delegate = nil;
    self.geoCodeSearch.delegate = nil;
}






#pragma mark ----导航方法-----------

- (NSArray *)getInstalledMapAppWithEndLocation:(CLLocationCoordinate2D)endLocation
{
//    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    NSString *appName = [infoDictionary objectForKey:(NSString *)kCFBundleExecutableKey]; //获取项目名称
    NSMutableArray *maps = [NSMutableArray array];
    
    //苹果地图
    NSMutableDictionary *iosMapDic = [NSMutableDictionary dictionary];
    iosMapDic[@"title"] = @"苹果地图";
    [maps addObject:iosMapDic];
    
    //百度地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        NSMutableDictionary *baiduMapDic = [NSMutableDictionary dictionary];
        baiduMapDic[@"title"] = @"百度地图";
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=目的地&mode=driving&coord_type=gcj02",endLocation.latitude,endLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        baiduMapDic[@"url"] = urlString;
        [maps addObject:baiduMapDic];
    }
//    //高德地图
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
//        NSMutableDictionary *gaodeMapDic = [NSMutableDictionary dictionary];
//        gaodeMapDic[@"title"] = @"高德地图";
//        NSString *urlString = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&lat=%f&lon=%f&dev=0&style=2",appName,APP_SCHEMES,endLocation.latitude,endLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        gaodeMapDic[@"url"] = urlString;
//        [maps addObject:gaodeMapDic];
//    }
//
//    //谷歌地图
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSMutableDictionary *googleMapDic = [NSMutableDictionary dictionary];
//        googleMapDic[@"title"] = @"谷歌地图";
//        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%f,%f&directionsmode=walking",appName,@"nav123456",endLocation.latitude, endLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        googleMapDic[@"url"] = urlString;
//        [maps addObject:googleMapDic];
//    }
//
//
//
//    //腾讯地图
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"qqmap://"]]) {
//        NSMutableDictionary *qqMapDic = [NSMutableDictionary dictionary];
//        qqMapDic[@"title"] = @"腾讯地图";
//        NSString *urlString = [[NSString stringWithFormat:@"qqmap://map/routeplan?from=我的位置&type=walk&tocoord=%f,%f&to=终点&coord_type=1&policy=0",endLocation.latitude, endLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        qqMapDic[@"url"] = urlString;
//        [maps addObject:qqMapDic];
//    }
//
    return maps;
}



//苹果地图
- (void)navAppleMap
{
    
    CLLocationCoordinate2D sss = [WGS84TOGCJ02 transformFromBaiduToGCJ:self.locationCoordinate];
    NSDictionary *address = @{
                              @"country":self.pathShowAddress
                              };
    CLLocationCoordinate2D gps = sss;
    MKMapItem *currentLoc = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:gps addressDictionary:address]];
    NSArray *items = @[currentLoc,toLocation];
    NSDictionary *dic = @{
                          MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving,
                          MKLaunchOptionsMapTypeKey : @(MKMapTypeStandard),
                          MKLaunchOptionsShowsTrafficKey : @(YES)
                          };
    
    [MKMapItem openMapsWithItems:items launchOptions:dic];
}

#pragma mark UIActionSheet
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != -1 && buttonIndex < self.maps.count) {
        if (buttonIndex == 0) {
            [self navAppleMap];
            return;
        }
        NSDictionary *dic = self.maps[buttonIndex];
        NSString *urlString = dic[@"url"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
}


- (void)navgationSender
{
    self.maps = [self getInstalledMapAppWithEndLocation:self.locationCoordinate];
    UIActionSheet *action = [[UIActionSheet alloc] init];
    for (NSDictionary *dic in self.maps) {
        [action addButtonWithTitle:[NSString stringWithFormat:@"%@", dic[@"title"]]];
    }
    [action addButtonWithTitle:Localized(@"cancel_btn")];
    action.cancelButtonIndex = self.maps.count;
    action.delegate = self;
    [action showInView:self.view];
    
}





- (void)dealloc {
    
    NSLog(@"%s",__FUNCTION__);
}

@end
