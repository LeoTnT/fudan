//
//  ViewController.h
//  BMKSearch
//
//  Created by apple on 2017/7/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    BMKShowSearchList,
    BMKShowMap,
} BMKShowType;

@protocol MapLocationViewDelegate <NSObject>

/*!
 @method
 @brief 发送位置信息的回调
 @param latitude    纬度
 @param longitude   经度
 @param address     地址信息
 */
-(void)sendLocationLatitude:(CLLocationCoordinate2D )coordinate
                 andAddress:(NSString *)address;
@end

@interface S_BaiduMap : UIViewController

@property (nonatomic, weak) id<MapLocationViewDelegate> delegate;

@property (nonatomic ,assign)BMKShowType showType;

@property (nonatomic ,assign)CLLocationCoordinate2D locationCoordinate;
@property (nonatomic ,copy)NSString *pathShowAddress;

@property(nonatomic,strong)UIViewController *fatherVC;

@end

