//
//  TableViewController.h
//  
//
//  Created by apple on 2017/7/17.
//
//

#import <UIKit/UIKit.h>

@interface SearchTableView : UIViewController

@property (nonatomic ,copy)void (^touch)(BMKPoiInfo *model);

@property (nonatomic ,strong)NSMutableArray *dataSource;
@end

