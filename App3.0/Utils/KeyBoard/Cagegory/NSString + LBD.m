//
//  NSString + LBD.m
//  LBDVshow
//
//  Created by xuyi on 16/4/8.
//  Copyright © 2016年 汪宗奎. All rights reserved.
//

#import "NSString + LBD.h"
#import "FaceSourceManager.h"
#import "EmojiTextAttachment.h"
#import <CoreText/CoreText.h>

#define UIColorHex(hex) [UIColor colorWithRed:((hex>>16)&0x0000FF)/255.0 green:((hex>>8)&0x0000FF)/255.0 blue:((hex>>0)&0x0000FF)/255.0 alpha:1.0]

#define BEGIN_FLAG @"["
#define END_FLAG @"]"

@implementation NSNumber (LBD)

- (BOOL)lbd_isVailable; {
    return YES;
}

@end

@implementation NSNull (LBD)

- (BOOL)lbd_isVailable; {
    return NO;
}
@end

@implementation NSString (LBD)

/// 判断字符串是否是空白
- (BOOL)lbd_isVailable; {
    return !(self.length <= 0
             || [self.lowercaseString isEqualToString:@"null"]
             || [self.lowercaseString isEqualToString:@"(null)"]
             || [self.lowercaseString isEqualToString:@"<null>"]
             || [self.lowercaseString isEqualToString:@"(null)"]
             ||([self isKindOfClass:[NSNull class]])
             || [self stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0 );
}


/// 字符串是否包含表情
- (BOOL)lbd_containsEmoji; {
    BOOL containsEmoji;
    [self lbd_removeEmojiWithStopWhenFind:YES containsEmoji:&containsEmoji];
    return containsEmoji;
}

- (NSString *)lbd_removeEmojiWithContainsEmoji:(BOOL *)containsEmoji; {
    return [self lbd_removeEmojiWithStopWhenFind:NO containsEmoji:containsEmoji];
}

/// 删除字符串内的表情符号，如果有则返回删除后的表情符号，如果没有，则返回nil
- (NSString *)lbd_removeEmojiWithStopWhenFind:(BOOL)stopWhenFind containsEmoji:(BOOL *)containsEmoji {
    if (containsEmoji != nil) { *containsEmoji = NO; }
    if (self.length == 0) { return self; }
    
    NSMutableString *result = [NSMutableString string];
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         BOOL haveEmoji = NO;
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     haveEmoji = YES;
                 }
             }
         }
         else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 haveEmoji = YES;
             }
         }
         else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 haveEmoji = YES;
             }
             else if (0x2B05 <= hs && hs <= 0x2b07) {
                 haveEmoji = YES;
             }
             else if (0x2934 <= hs && hs <= 0x2935) {
                 haveEmoji = YES;
             }
             else if (0x3297 <= hs && hs <= 0x3299) {
                 haveEmoji = YES;
             }
             else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 haveEmoji = YES;
             }
         }
         
         if (haveEmoji) {
             if (stopWhenFind) {
                 *stop = YES;
             }
         }
         else {
             [result appendString:substring];
         }
     }];
    
    // 判断是否包含Emoji
    if (containsEmoji != nil) {
        *containsEmoji = (result.length != self.length);
    }
    
    return result;
}


-(NSString *)formatThousandString {
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: [self integerValue]]];
    return numberString;
}

#define KFacialSizeWidth  25
#define KFacialSizeHeight 25
-(NSAttributedString *)changeToEmojiStringWithHaveReply:(BOOL)haveReply WithFontSize:(NSUInteger)size {
    static UIImage * tempImage = nil;
    
    NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc]initWithString:self];
    [strAtt addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} range:NSMakeRange(0, self.length)];
    if (haveReply) {
        if ([self rangeOfString:@"Reply @"].location != NSNotFound) {
            NSRange startRange = [self rangeOfString:@"Reply @"];
            NSRange endRange = [self rangeOfString:@":"];
            NSRange range = NSMakeRange(startRange.location + startRange.length -1, endRange.location - startRange.location - startRange.length +1);
            [strAtt addAttributes:@{NSForegroundColorAttributeName:UIColorHex(0x1a4e84)} range:range];
        }
    }
    
    // 正则表达式匹配表情
    NSString * pattern = @"\\[[A-Za-z0-9\u4e00-\u9fa5]+\\]";
    NSError *error = nil;
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray *results = [re matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    for (NSInteger i = results.count - 1; i >= 0; i--) {
        NSTextCheckingResult * result = results[i];
        NSString * imgStr = [strAtt.string substringWithRange:result.range];
        EmojiTextAttachment *textAttachment = [[EmojiTextAttachment alloc]init];
        tempImage = [UIImage imageNamed:[[FaceSourceManager ShardInstnce].faceData objectForKey:imgStr]];
        textAttachment.image = tempImage;
        textAttachment.emojiTag = imgStr;
        textAttachment.bounds = CGRectMake(0, -3, KFacialSizeHeight*(tempImage.size.width/tempImage.size.height), KFacialSizeHeight);
        NSAttributedString * strImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [strAtt replaceCharactersInRange:result.range withAttributedString:strImage];
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    [strAtt addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, strAtt.length)];
    
    //可以识别url的正则表达式
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
    for (NSTextCheckingResult *match in arrayOfAllMatches)
    {
        NSString* substringForMatch;
        substringForMatch = [self substringWithRange:match.range];
        NSMutableAttributedString * strAttUrl = [[NSMutableAttributedString alloc]initWithString:substringForMatch];
        [strAttUrl addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:mainColor,NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, substringForMatch.length)];
        [strAtt replaceCharactersInRange:match.range withAttributedString:strAttUrl];
    }
    
    return  strAtt;
}

-(NSAttributedString *)changeToEmojiStringWithHaveReply:(BOOL)haveReply {
    static UIImage * tempImage = nil;
   
    NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc]initWithString:self];
    [strAtt addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]} range:NSMakeRange(0, self.length)];
    if (haveReply) {
        if ([self rangeOfString:@"Reply @"].location != NSNotFound) {
            NSRange startRange = [self rangeOfString:@"Reply @"];
            NSRange endRange = [self rangeOfString:@":"];
            NSRange range = NSMakeRange(startRange.location + startRange.length -1, endRange.location - startRange.location - startRange.length +1);
            [strAtt addAttributes:@{NSForegroundColorAttributeName:UIColorHex(0x1a4e84)} range:range];
        }
    }
    
    // 正则表达式匹配表情
    NSString * pattern = @"\\[[A-Za-z0-9\u4e00-\u9fa5]+\\]";
    NSError *error = nil;
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray *results = [re matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    for (NSInteger i = results.count - 1; i >= 0; i--) {
        NSTextCheckingResult * result = results[i];
        NSString * imgStr = [strAtt.string substringWithRange:result.range];
        EmojiTextAttachment *textAttachment = [[EmojiTextAttachment alloc]init];
        tempImage = [UIImage imageNamed:[[FaceSourceManager ShardInstnce].faceData objectForKey:imgStr]];
        textAttachment.image = tempImage;
        textAttachment.emojiTag = imgStr;
        textAttachment.bounds = CGRectMake(0, -3, KFacialSizeHeight*(tempImage.size.width/tempImage.size.height), KFacialSizeHeight);
        NSAttributedString * strImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [strAtt replaceCharactersInRange:result.range withAttributedString:strImage];
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    [strAtt addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, strAtt.length)];
    
    //可以识别url的正则表达式
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
    for (NSTextCheckingResult *match in arrayOfAllMatches)
    {
        NSString* substringForMatch;
        substringForMatch = [self substringWithRange:match.range];
        NSMutableAttributedString * strAttUrl = [[NSMutableAttributedString alloc]initWithString:substringForMatch];
        [strAttUrl addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:mainColor,NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, substringForMatch.length)];
        [strAtt replaceCharactersInRange:match.range withAttributedString:strAttUrl];
    }
    
    return  strAtt;
}


-(NSAttributedString *)MoreChangeStringToAttribuedString:(NSArray*)selectArry
{
    NSMutableDictionary *textChangeColor=[[NSMutableDictionary alloc]init];
    NSString *tempStr = self;
    NSInteger rangeLocation = 0;
    for (NSString *selectStr in selectArry) {
        NSRange sRang=[tempStr rangeOfString:selectStr];
        if (sRang.location!=NSNotFound )
        {
            [textChangeColor setValue:selectStr forKey:NSStringFromRange(NSMakeRange(rangeLocation + sRang.location  , sRang.length))];
            rangeLocation = rangeLocation + sRang.length + sRang.location ;
            tempStr = [tempStr substringFromIndex:sRang.length + sRang.location  ];
        }
    }
    NSMutableAttributedString *normalAttStr = [[NSMutableAttributedString alloc]initWithString:self ];
    if (textChangeColor.count>0)
    {
        NSArray *arryhh=[textChangeColor allKeys];
        for (int i=0; i<arryhh.count; i++)
        {
            NSRange range = NSRangeFromString(arryhh[i]);
            [normalAttStr addAttribute:NSForegroundColorAttributeName value:UIColorHex(0x1a4e84) range:NSMakeRange(range.location - 1, range.length+1)];
        }
    }
    NSString * pattern = @"\\[[_A-Za-z0-9]+\\]";
    NSError *error = nil;
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray *results = [re matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    for (NSInteger i = results.count - 1; i >= 0; i--) {
        NSTextCheckingResult * result = results[i];
        NSString * imgStr = [normalAttStr.string substringWithRange:result.range];
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc]init];
        textAttachment.image = [UIImage imageNamed:imgStr];
        textAttachment.bounds = CGRectMake(0, -3, KFacialSizeWidth, KFacialSizeHeight);
        NSAttributedString * strImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [normalAttStr replaceCharactersInRange:result.range withAttributedString:strImage];
    }
    return normalAttStr;
}



-(void)getImageRangeWithArray : (NSMutableArray*)array {
    NSRange range=[self rangeOfString: BEGIN_FLAG];
    NSRange range1=[self rangeOfString: END_FLAG];
    
    if (range.length > 0 && range1.length>0) {
        if (range.location > 0 && (range1.location > range.location)) {
            [array addObject:[self substringToIndex:range.location]];//加文字
            [array addObject:[self substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)]];//加表情
            NSString *str=[self substringFromIndex:range1.location+1];
            [str getImageRangeWithArray: array];
        }
        else {
            NSString *nextstr=[self substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)];
            //NSLog(@"+++ nextStr %@",nextstr);
            
            //排除文字是“”的
            if (nextstr.length > 0) {
                [array addObject:nextstr];
                NSString *str=[self substringFromIndex:range1.location+1];
                [str getImageRangeWithArray: array];
            }else {
                return;
            }
        }
        
    } else if  (self != nil) {
        [array addObject:self];
    }
}


-  (int)convertToInt:(NSString*)strtemp {
    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return (strlength+1)/2;
}


+(NSAttributedString *)returnAttributedStringWithImageName:(NSString *)imageName andImageRect:(CGRect)imageRect {
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc]init];
    textAttachment.image = [UIImage imageNamed:imageName];
    textAttachment.bounds = imageRect;
    NSAttributedString * tempAttrat = [NSAttributedString attributedStringWithAttachment:textAttachment];
    return tempAttrat;
}

-(NSAttributedString *)returnAttributedStringWithImageName:(NSString *)imageName andImageRect:(CGRect)imageRect andInsertLocation:(NSInteger)location {
    NSMutableAttributedString * finish = [[NSMutableAttributedString alloc]initWithString:self];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc]init];
    textAttachment.image = [UIImage imageNamed:imageName];
    textAttachment.bounds = imageRect;
    NSAttributedString * tempAttrat = [NSAttributedString attributedStringWithAttachment:textAttachment];
    if (location>self.length) {
        [finish insertAttributedString:tempAttrat atIndex:self.length];
    }
    else {
        [finish insertAttributedString:tempAttrat atIndex:location];
    }
    return finish;
}

-(NSArray *)rangeArrayOfSearchString:(NSString *)string {
    NSString * pattern = string;
    NSError *error = nil;
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    return [re matchesInString:self options:0 range:NSMakeRange(0, self.length)];;
}

@end

