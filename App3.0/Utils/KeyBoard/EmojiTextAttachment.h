//
//  EmojiTextAttachment.h
//  App3.0
//
//  Created by mac on 2017/5/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiTextAttachment : NSTextAttachment
@property(strong, nonatomic) NSString *emojiTag;
@end
