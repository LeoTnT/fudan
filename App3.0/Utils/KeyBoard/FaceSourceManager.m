//
//  FaceSourceManager.m
//  FaceKeyboard

//  Company：     SunEee
//  Blog:        devcai.com
//  Communicate: 2581502433@qq.com

//  Created by ruofei on 16/3/30.
//  Copyright © 2016年 ruofei. All rights reserved.
//

#import "FaceSourceManager.h"

static FaceSourceManager* face = nil;

@implementation FaceSourceManager

+(FaceSourceManager*)ShardInstnce
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        face = [[FaceSourceManager alloc] init];
    });
    return face;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"face" ofType:@"plist"];
        _faceData = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }
    return self;
}

//从持久化存储里面加载表情源
+ (NSArray *)loadFaceSource
{
    NSMutableArray *subjectArray = [NSMutableArray array];
    
    //NSArray *sources = @[@"face", @"systemEmoji",@"emotion",@"systemEmoji",@"face",@"systemEmoji",@"emotion",@"emotion",@"face",@"face",@"emotion",@"face", @"emotion",@"face", @"emotion"];
    NSArray *sources = @[@"face"];
    
    for (int i = 0; i < sources.count; ++i)
    {
        NSString *plistName = sources[i];
        
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
        NSDictionary *faceDic = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        NSArray *allkeys = faceDic.allKeys;
        
        FaceThemeModel *themeM = [[FaceThemeModel alloc] init];
        
        if ([plistName isEqualToString:@"face"]) {
            themeM.themeStyle = FaceThemeStyleCustomEmoji;
            themeM.themeDecribe = [NSString stringWithFormat:@"f%d", i];
            themeM.themeIcon = @"face";
        }else if ([plistName isEqualToString:@"systemEmoji"]){
            themeM.themeStyle = FaceThemeStyleSystemEmoji;
            themeM.themeDecribe = @"sEmoji";
            themeM.themeIcon = @"face";
        }
        else {
            themeM.themeStyle = FaceThemeStyleGif;
            themeM.themeDecribe = [NSString stringWithFormat:@"e%d", i];
            themeM.themeIcon = @"face";
        }
        
        
        NSMutableArray *modelsArr = [NSMutableArray array];
        
        for (int i = 0; i < allkeys.count; ++i) {
            NSString *name = allkeys[i];
            FaceModel *fm = [[FaceModel alloc] init];
            fm.faceTitle = name;
            fm.faceIcon = [faceDic objectForKey:name];
            [modelsArr addObject:fm];
        }
        
        // 表情排序
        NSArray* sorted = [modelsArr sortedArrayUsingComparator:
                           ^(FaceModel *obj1, FaceModel *obj2){
                               NSString *str1 = [obj1.faceIcon substringFromIndex:9];
                               NSString *str2 = [obj2.faceIcon substringFromIndex:9];
                               if([str1 integerValue] < [str2 integerValue]) {
                                   return(NSComparisonResult)NSOrderedAscending;
                               }else {
                                   return(NSComparisonResult)NSOrderedDescending;
                               }
                           }];
        
        themeM.faceModels = sorted;
        
        [subjectArray addObject:themeM];
    }
    
    
    return subjectArray;
}


@end
