//
//  DBHandler.h
//  App3.0
//
//  Created by mac on 17/3/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>
#import "ContactModel.h"

@interface DBHandler : NSObject

/**
 *  拿取DB操作单例
 *
 *  @return DB操作实例-单例对象
 */
+ (DBHandler *)sharedInstance;

/**
 *  新插入或者更新联系人
 */
- (void)addOrUpdateContact:(ContactDataParser *)contact;


/**
 *  更新联系人信息
 */
- (void)updateContactWithKey:(NSString *)key value:(NSString *)value byUid:(NSString *)uid;

/**
 *  通过uid删除联系人
 */
- (void)deleteContactByUid:(NSString *)uid;

/**
 *  通过uid获取联系人
 */
- (NSMutableArray *)getContactByUid:(NSString *)uid;

/**
 *  通过关键字模糊查询联系人
 */
- (NSMutableArray *)getContactByKeyword:(NSString *)keyword;

/**
 *  获取全部联系人
 */
- (NSMutableArray *)getAllContact;

/**
 *  新插入或者更新群组
 */
- (void)addOrUpdateGroup:(GroupDataModel *)group;

/**
 *  通过groupId获取群组
 */
- (NSMutableArray *)getGroupByGroupId:(NSString *)groupId;

/**
 *  更新群信息
 */
- (void)updateGroupWithKey:(NSString *)key value:(NSString *)value byGid:(NSString *)gid;

/**
 *  通过groupId删除群组
 */
- (void)deleteGroupByGroupId:(NSString *)groupId;

/**
 *  获取全部群组
 */
- (NSMutableArray *)getAllGroup;

/**
 * 新增或者更新群成员信息
 */
- (void)addOrUpdateGroupMember:(NickNameDataParser *)model;

/**
 * 通过编号获取一条群成员信息
 * @param memberId = uid
 */
- (NSMutableArray *)getMemberByMemberId:(NSString *)memberId;
@end
