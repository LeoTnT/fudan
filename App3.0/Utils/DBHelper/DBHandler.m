//
//  DBHandler.m
//  App3.0
//
//  Created by mac on 17/3/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DBHandler.h"
#import "UserInstance.h"
#define db_name @"db.sqlite"

@interface DBHandler () {
    FMDatabaseQueue *_xbDbQueue; // 需要使用 FMDatabaseQueue 来保证线程安全
    //    FMDatabase *_db;
}
@end

static DBHandler *dbHandler = nil;
@implementation DBHandler

#pragma mark -dbFilePath
+ (NSString *)dbFilePath {
    NSArray *documentArr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dbPath = [[documentArr objectAtIndex:0] stringByAppendingPathComponent:db_name];
    return dbPath;
}

#pragma mark -sharedInstance
+ (DBHandler *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dbHandler = [[DBHandler alloc] init];
    });
    return dbHandler;
}

- (instancetype)init {
    if (self = [super init]) {
        _xbDbQueue = [FMDatabaseQueue databaseQueueWithPath:[DBHandler dbFilePath]];
        //        _db = [FMDatabase databaseWithPath:[DBHandler dbFilePath]];
        [self createContactTable];
        [self createGroupTable];
        [self createGroupMembersTable];
        //        [self creatExceptionReportList];
    }
    return self;
}

- (void)createContactTable {
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            // owner 用来标识联系人属于哪个用户
            NSString *sql = @"CREATE TABLE 'contact' ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL , 'uid' VARCHAR(255), 'username' VARCHAR(255), 'nickname' VARCHAR(255), 'avatar' VARCHAR(255), 'relation' VARCHAR(255), 'owner' VARCHAR(255), 'remark' VARCHAR(255), 'mobile' VARCHAR(255), 'shield' VARCHAR(255), 'disturb' VARCHAR(255))";
            BOOL res = [db executeUpdate:sql];
            if (!res) {
                NSLog(@"error when createing db table");
            }
            else {
                NSLog(@"success to create db table");
            }
            [db close];
        }
        else {
            NSLog(@"error when open db");
        }
    }];
    
}

- (void)createGroupTable {
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            // owner 用来标识群组属于哪个用户， groupowner 群主
            NSString *sql = @"CREATE TABLE 'emgroup' ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL ,'groupid' VARCHAR(255), 'groupname' VARCHAR(255), 'groupdesc' VARCHAR(255), 'groupavatar' VARCHAR(255), 'groupowner' VARCHAR(255), 'membercount' VARCHAR(255), 'owner' VARCHAR(255), 'shield' VARCHAR(255), 'disturb' VARCHAR(255), 'type' VARCHAR(255))";
            BOOL res = [db executeUpdate:sql];
            if (!res) {
                NSLog(@"error when createing db table");
            }
            else {
                NSLog(@"success to create db table");
            }
            [db close];
        }
        else {
            NSLog(@"error when open db");
        }
    }];
    
}

- (void)createGroupMembersTable {
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            // owner 用来标识群组属于哪个用户， groupowner 群主
            NSString *sql = @"CREATE TABLE 'groupmembers' ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL , 'memberid' VARCHAR(255), 'membernickname' VARCHAR(255), 'memberavatar' VARCHAR(255))";
            BOOL res = [db executeUpdate:sql];
            if (!res) {
                NSLog(@"error when createing db table");
            }
            else {
                NSLog(@"success to create db table");
            }
            [db close];
        }
        else {
            NSLog(@"error when open db");
        }
    }];
    
}

//
///**
// 异常表
// */
//- (void) creatExceptionReportList {
//
//    if ([_db open]) {
//        // beforTime 插入时间 interface_url 接口地址 machine_type 设备型号
//        NSString *sql = @"CREATE TABLE 'ExceptionReport' ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL ,'beforTime' VARCHAR(255), 'interface_url' VARCHAR(255), 'machine_type' VARCHAR(255), 'identify_num' VARCHAR(255))";
//        BOOL res = [_db executeUpdate:sql];
//        if (!res) {
//            NSLog(@" ExceptionReport error when createing db table");
//        }
//        else {
//            NSLog(@"ExceptionReport success to create db table");
//        }
//        [_db close];
//    }
//    else {
//        NSLog(@"error when open db");
//    }
//
//}

#pragma mark - 联系人
- (void)addOrUpdateContact:(ContactDataParser *)contact
{
    NSArray *arr = [self getContactByUid:contact.uid];
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        if (arr == nil || arr.count == 0) {
            [db executeUpdate:@"INSERT INTO contact(uid,username,nickname,avatar,relation,owner,remark,mobile,shield,disturb)VALUES(?,?,?,?,?,?,?,?,?,?)",contact.uid,contact.username,contact.nickname,contact.avatar,contact.relation,[UserInstance ShardInstnce].uid,contact.remark,contact.mobile,contact.is_shield,contact.is_not_disturb];
        } else {
            [db executeUpdate:@"UPDATE 'contact' SET username = ?  WHERE uid = ? and owner = ?",contact.username,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET nickname = ?  WHERE uid = ? and owner = ?",contact.nickname,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET avatar = ?  WHERE uid = ? and owner = ?",contact.avatar,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET relation = ?  WHERE uid = ? and owner = ?",contact.relation,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET remark = ?  WHERE uid = ? and owner = ?",contact.remark,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET mobile = ?  WHERE uid = ? and owner = ?",contact.mobile,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET shield = ?  WHERE uid = ? and owner = ?",contact.is_shield,contact.uid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'contact' SET disturb = ?  WHERE uid = ? and owner = ?",contact.is_not_disturb,contact.uid,[UserInstance ShardInstnce].uid];
        }
        
        
        [db close];
    }];
    
}

- (void)updateContactWithKey:(NSString *)key value:(NSString *)value byUid:(NSString *)uid {
    NSArray *arr = [self getContactByUid:uid];
    if (!arr || arr.count == 0) {
        return;
    }
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        NSString *query = [NSString stringWithFormat:@"UPDATE 'contact' SET %@ = '%@'  WHERE uid = '%@' and owner = '%@'",key,value,uid,[UserInstance ShardInstnce].uid];
        [db executeUpdate:query];
        
        [db close];
    }];
    
}

- (NSMutableArray *)getContactByUid:(NSString *)uid
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from contact where uid = '%@' and owner = '%@'", uid,[UserInstance ShardInstnce].uid];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            ContactDataParser *contact = [[ContactDataParser alloc] init];
            contact.uid = [res stringForColumn:@"uid"];
            contact.username = [res stringForColumn:@"username"];
            contact.nickname = [res stringForColumn:@"nickname"];
            contact.avatar = [res stringForColumn:@"avatar"];
            contact.relation = [NSNumber numberWithInteger:[[res stringForColumn:@"relation"] integerValue]];
            contact.remark = [res stringForColumn:@"remark"];
            contact.mobile = [res stringForColumn:@"mobile"];
            contact.is_shield = [NSNumber numberWithInteger:[[res stringForColumn:@"shield"] integerValue]];
            contact.is_not_disturb = [NSNumber numberWithInteger:[[res stringForColumn:@"disturb"] integerValue]];
            
            [dataArray addObject:contact];
            
        }
        [db close];
    }];
    
    
    return dataArray;
}

- (NSMutableArray *)getContactByKeyword:(NSString *)keyword
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from contact where ( username like '%%%@%%' or nickname like '%%%@%%' or mobile like '%%%@%%' or remark like '%%%@%%') and owner = '%@'", keyword,keyword,keyword,keyword,[UserInstance ShardInstnce].uid];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            ContactDataParser *contact = [[ContactDataParser alloc] init];
            contact.uid = [res stringForColumn:@"uid"];
            contact.username = [res stringForColumn:@"username"];
            contact.nickname = [res stringForColumn:@"nickname"];
            contact.avatar = [res stringForColumn:@"avatar"];
            contact.relation = [NSNumber numberWithInteger:[[res stringForColumn:@"relation"] integerValue]];
            contact.remark = [res stringForColumn:@"remark"];
            contact.mobile = [res stringForColumn:@"mobile"];
            contact.is_shield = [NSNumber numberWithInteger:[[res stringForColumn:@"shield"] integerValue]];
            contact.is_not_disturb = [NSNumber numberWithInteger:[[res stringForColumn:@"disturb"] integerValue]];
            
            if (![contact.uid isEqualToString:@"robot"]) {
                [dataArray addObject:contact];
            }
            
        }
        [db close];
    }];
    
    
    return dataArray;
}

- (void)deleteContactByUid:(NSString *)uid
{
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db executeUpdate:@"DELETE FROM 'contact' WHERE uid = ? and owner = ?",uid,[UserInstance ShardInstnce].uid];
        [db close];
    }];
    
}

- (NSMutableArray *)getAllContact
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from contact where owner = '%@'", [UserInstance ShardInstnce].uid];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            ContactDataParser *contact = [[ContactDataParser alloc] init];
            contact.uid = [res stringForColumn:@"uid"];
            contact.username = [res stringForColumn:@"username"];
            contact.nickname = [res stringForColumn:@"nickname"];
            contact.avatar = [res stringForColumn:@"avatar"];
            contact.relation = [NSNumber numberWithInteger:[[res stringForColumn:@"relation"] integerValue]];
            contact.remark = [res stringForColumn:@"remark"];
            contact.mobile = [res stringForColumn:@"mobile"];
            contact.is_shield = [NSNumber numberWithInteger:[[res stringForColumn:@"shield"] integerValue]];
            contact.is_not_disturb = [NSNumber numberWithInteger:[[res stringForColumn:@"disturb"] integerValue]];
            
            if (![contact.uid isEqualToString:@"robot"]) {
                [dataArray addObject:contact];
            }
            
            
        }
        [db close];
    }];
    
    
    return dataArray;
}

#pragma mark - 群组

- (void)addOrUpdateGroup:(GroupDataModel *)group
{
    NSArray *arr = [self getGroupByGroupId:group.gid];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        if (arr == nil || arr.count == 0) {
            
            [db executeUpdate:@"INSERT INTO emgroup(groupid,groupname,groupavatar,groupowner,groupdesc,membercount,owner,shield,disturb,type)VALUES(?,?,?,?,?,?,?,?,?,?)",group.gid,group.name,group.avatar,group.owner,group.desc,group.member_count,[UserInstance ShardInstnce].uid,group.is_shield,group.is_not_disturb,group.type];
        } else {
            [db executeUpdate:@"UPDATE 'emgroup' SET groupname = ?  WHERE groupid = ? and owner = ?",group.name,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET groupavatar = ?  WHERE groupid = ? and owner = ?",group.avatar,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET groupowner = ?  WHERE groupid = ? and owner = ?",group.owner,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET groupdesc = ?  WHERE groupid = ? and owner = ?",group.desc,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET memberCount = ?  WHERE groupid = ? and owner = ?",group.member_count,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET shield = ?  WHERE groupid = ? and owner = ?",group.is_shield,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET disturb = ?  WHERE groupid = ? and owner = ?",group.is_not_disturb,group.gid,[UserInstance ShardInstnce].uid];
            [db executeUpdate:@"UPDATE 'emgroup' SET type = ?  WHERE groupid = ? and owner = ?",group.type,group.gid,[UserInstance ShardInstnce].uid];
        }
        
        
        [db close];
    }];
    
}

- (NSMutableArray *)getGroupByGroupId:(NSString *)groupId
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from emgroup where groupid = '%@' and owner = '%@'", groupId,[UserInstance ShardInstnce].uid];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            GroupDataModel *group = [[GroupDataModel alloc] init];
            group.gid = [res stringForColumn:@"groupid"];
            group.type = [res stringForColumn:@"type"];
            group.name = [res stringForColumn:@"groupname"];
            group.avatar = [res stringForColumn:@"groupavatar"];
            group.owner = [res stringForColumn:@"groupowner"];
            group.desc = [res stringForColumn:@"groupdesc"];
            group.member_count = [res stringForColumn:@"memberCount"];
            group.is_shield = [NSNumber numberWithInteger:[[res stringForColumn:@"shield"] integerValue]];
            group.is_not_disturb = [NSNumber numberWithInteger:[[res stringForColumn:@"disturb"] integerValue]];
            
            [dataArray addObject:group];
            
        }
        [db close];
    }];
    
    
    return dataArray;
}

- (void)updateGroupWithKey:(NSString *)key value:(NSString *)value byGid:(NSString *)gid {
    NSArray *arr = [self getGroupByGroupId:gid];
    if (!arr || arr.count == 0) {
        return;
    }
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        NSString *query = [NSString stringWithFormat:@"UPDATE 'emgroup' SET %@ = '%@'  WHERE groupid = '%@' and owner = '%@'",key,value,gid,[UserInstance ShardInstnce].uid];
        [db executeUpdate:query];
        
        [db close];
    }];
    
}

- (void)deleteGroupByGroupId:(NSString *)groupId
{
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db executeUpdate:@"DELETE FROM 'emgroup' WHERE groupid = ? and owner = ?",groupId,[UserInstance ShardInstnce].uid];
        [db close];
    }];
    
}

- (NSMutableArray *)getAllGroup
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from emgroup where owner = '%@'",[UserInstance ShardInstnce].uid];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            GroupDataModel *group = [[GroupDataModel alloc] init];
            group.gid = [res stringForColumn:@"groupid"];
            group.type = [res stringForColumn:@"type"];
            group.name = [res stringForColumn:@"groupname"];
            group.avatar = [res stringForColumn:@"groupavatar"];
            group.owner = [res stringForColumn:@"groupowner"];
            group.desc = [res stringForColumn:@"groupdesc"];
            group.member_count = [res stringForColumn:@"memberCount"];
            group.is_shield = [NSNumber numberWithInteger:[[res stringForColumn:@"shield"] integerValue]];
            group.is_not_disturb = [NSNumber numberWithInteger:[[res stringForColumn:@"disturb"] integerValue]];
            
            [dataArray addObject:group];
            
        }
        [db close];
    }];
    
    return dataArray;
}

#pragma mark - 群联系人
- (void)addOrUpdateGroupMember:(NickNameDataParser *)model
{
    NSArray *arr = [self getMemberByMemberId:model.uid];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        if (arr == nil || arr.count == 0) {
            
            [db executeUpdate:@"INSERT INTO groupmembers(memberid,membernickname,memberavatar)VALUES(?,?,?)",model.uid,model.nickname,model.logo];
        } else {
            [db executeUpdate:@"UPDATE 'groupmembers' SET memberNickName = ?  WHERE memberid = ? ",model.nickname,model.uid];
            [db executeUpdate:@"UPDATE 'groupmembers' SET memberAvatar = ?  WHERE memberid = ? ",model.logo,model.uid];
        }
        
        
        [db close];
    }];
    
}

- (NSMutableArray *)getMemberByMemberId:(NSString *)memberId;
{
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    [_xbDbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        //编写SQL查询语句
        NSString *query = [NSString stringWithFormat:@"select * from groupmembers where memberid = '%@'", memberId];
        FMResultSet *res = [db executeQuery:query];
        
        while ([res next]) {
            NickNameDataParser *model = [[NickNameDataParser alloc] init];
            model.uid = [res stringForColumn:@"memberid"];
            model.nickname = [res stringForColumn:@"membernickname"];
            model.logo = [res stringForColumn:@"memberavatar"];
            
            [dataArray addObject:model];
            
        }
        [db close];
    }];
    
    return dataArray;
}
@end
