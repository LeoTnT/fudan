//
//  Y_VolumeMAView.h
//  BTC-Kline
//
//  Created by yate1996 on 16/5/3.
//  Copyright © 2016年 yate1996. All rights reserved.
//
//成交量等数值
#import <UIKit/UIKit.h>
@class Y_KLineModel;
@interface Y_VolumeMAView : UIView

-(instancetype)initWithScreenStatus:(BOOL)screenStatusVertical;

//+(instancetype)view;

-(void)maProfileWithModel:(Y_KLineModel *)model;
@end
