//
//  Constants.h
//  App3.0
//
//  Created by mac on 17/2/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


#define XSPath(file) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]stringByAppendingPathComponent:file]

#define StatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define navBarHeight   self.navigationController.navigationBar.frame.size.height
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define mainHeight     [[UIScreen mainScreen] bounds].size.height
#define mainWidth      [[UIScreen mainScreen] bounds].size.width
#define SCREEN_SCALE  (SCREEN_WIDTH/375.0f)
#define FontNum(x)   x*SCREEN_SCALE
#define MAIN_VC_HEIGHT   mainHeight-StatusBarHeight-navBarHeight

#define MyLanguage                @"MyLanguage"
#define SelectLang   [[NSUserDefaults standardUserDefaults] objectForKey:MyLanguage]

//重写NSLog,Debug模式下打印日志和当前行数
#if DEBUG
#define XSLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define XSLog(FORMAT, ...) nil
#endif

#define BG_COLOR [UIColor hexFloatColor:@"f4f4f4"] //eeeeee
#define LINE_COLOR [UIColor hexFloatColor:@"cccccc"]

#define TAB_SELECTED_COLOR [UIColor hexFloatColor:@"0090ff"]

#define JYSMainSelelctColor       [UIColor hexFloatColor:@"5888ED"] //0090ff
#define JYSMainTextColor       [UIColor hexFloatColor:@"111111"] //0090ff

#define JYSRedColor            [UIColor hexFloatColor:@"F95453"]//大盘红色
#define JYSRedString           @"F95453"
#define JYSGreenColor          [UIColor hexFloatColor:@"00BF84"]//大盘绿色
#define JYSGreenString         @"00BF84"
#define JYSGrayButtonColor          [UIColor hexFloatColor:@"D1D5DD"]//涨跌幅为0时按钮颜色


//#define JYSChangeColor(tempColor,key)
//
//#define kMyColor(tempColor)    [UIColor colorWithHexString:[[kUserDefault objectForKey:@"kAllColorDict"]objectForKey:tempColor] alpha:1.0f]


#define avatarDefaultImg [UIImage imageNamed:@"tab_user"]

#define mainColor           [UIColor hexFloatColor:@"0070CC"]
#define mainGrayColor       [UIColor hexFloatColor:@"828282"]
#define HighLightColor_Main [UIColor hexFloatColor:@"FFAF02"]
#define HighLightColor_Gray [UIColor hexFloatColor:@"9d9d9d"]
#define NAVBAR_COLOR        [UIColor hexFloatColor:@"f7f7f7"]
#define COLOR_BUTTON_DISABLED [UIColor hexFloatColor:@"cfcfcf"]
#define SKILL_COLOR [UIColor hexFloatColor:@"369ff2"]
#define GreenColor           [UIColor hexFloatColor:@"1CD846"]
#define RedColor           [UIColor hexFloatColor:@"FF504B"]
#define SYSTEM_FONT(float)  [UIFont systemFontOfSize:float]

// 最新切图色值
#define COLOR_111111 [UIColor hexFloatColor:@"111111"]
#define COLOR_666666 [UIColor hexFloatColor:@"666666"]
#define COLOR_999999 [UIColor hexFloatColor:@"999999"]
#define LINE_COLOR_NORMAL [UIColor hexFloatColor:@"e6e6e6"]
#define BG_COLOR_NORMAL [UIColor hexFloatColor:@"f4f4f4"]

#define SCREEN_H_Height(float) (float)*SCREEN_HEIGHT/667.0f// 粉丝圈
#define SCREEN_W_Width(float) (float)*mainWidth/375.0f
#define FANS_BG_BUTTON      [UIColor hexFloatColor:@"353b3d"]
#define FAN_CIRCLE_COLOR    [UIColor hexFloatColor:@"5a6e97"]

#define ORDER_BG_COLOR   [UIColor hexFloatColor:@"F3F4F7"]


#define APP_NAME                @"去燥星球"
#define ENTER_LABEL_TITLE       @"主人，生意来了！"
#define ENTER_LABEL_DES         @"私享机器人自动营销，业务遍地开花"
#define ENTER_LABEL_TITLECOLOR  [UIColor hexFloatColor:@"209ee7"]
#define ENTER_LABEL_DESCOLOR    [UIColor hexFloatColor:@"5fb3f3"]
//syn
#define STATUS_HEIGHT 20
#define SKILL_BTN_SPACE 10
#define NORMOL_SPACE 10

//
#define NAV_BACK_EMPTY_TITLE    @"        "
#define Localized(Str) NSLocalizedString(Str, @"")

// 购物车
#define CART_TOTALVIEW_HEIGHT 60.0f
#define NOTIFICATION_REFRESH_CART @"refreshCart"
#define NOTIFICATION_REFRESHLIST @"refreshlist"
#define NOTIFICATION_EDIT_SPEC @"editspec"
#define NOTIFICATION_LOOK_DETAIL @"lookGoodsDetail"
// 系统相关
//#define KNOTIFICATION_LOGINCHANGE @"notification_loginchange"
/** @brief 登录状态变更的通知 */
#define KNOTIFICATION_LOGINCHANGE @"loginStateChange"
/** @brief 实时音视频呼叫 */
#define KNOTIFICATION_CALL @"callOutWithChatter"
/** @brief 关闭实时音视频 */
#define KNOTIFICATION_CALL_CLOSE @"callControllerClose"
/** @brief 从后台进入通知,倒计时刷新状态变更的 */
#define KNOTIFICATION_TIMECHANGE @"timeStateChange"

//本地存储guid使用的key
#define APPAUTH_KEY          @"appauth"

/**本地存储登录时用户信息使用的key*/
#define USERINFO_LOGIN    @"loginUserInfo"

/**本地存储登录过的账号*/
#define ACCOUNT_LIST        @"ACCOUNT_LIST"

/**本地存储搜索历史记录*/
#define SEARCH_HISTORY_LIST        @"SEARCH_HISTORY_LIST"

/**本地存储用户聊天设置*/
#define USER_CHATSET    @"USER_CHATSET"

// 群组配置
#define GROUP_MAXUSERSCOUNT  200

#define TOAST_REQUEST_FAIL @"请求失败,请稍后再试"
#define TOAST_DO_FAIL @"操作失败,请稍后再试"
#define NetFailure @"网络异常，请稍后重试"

#define wScale mainWidth/375.0
#define HScale mainHeight/667.0
#define Color(colorStr)           [UIColor hexFloatColor:colorStr]
#define Alert(message) [XSTool showToastWithView:self.view Text:message]
#define DefaultImage        [UIImage imageNamed:@"no_pic"]
#define DefaultAvatarImage        [UIImage imageNamed:@"user_fans_avatar"]
// 聊天
/*
 消息的回撤消息两个字段：与安卓统一
 1.消息的动作，是透传消息必须有的字段
 public static final String REVOKE_ACTION = "REVOKE_ACTION";
 2.需要撤回消息的id
 public static final String CUSTOM_MSG_ID = "CUSTOM_MSG_ID";
 */
// 自定义聊天消息
//1.
#define MSG_TYPE @"MSG_TYPE"
//2.
#define CUSTOM_MSG_ID @"CUSTOM_MSG_ID"
//3.1 撤回消息
#define REVOKE_ACTION @"REVOKE_ACTION"
//3.2.1 创建群组
#define CREATE_GROUP @"CREATE_GROUP"
//3.2.2 邀请某一个人加入群组
#define GROUP_INVITE_ACTION @"GROUP_INVITE_ACTION"
//3.2.3 删除群组里面的某一个人
#define DELE_GROUP_MEMBER @"DELE_GROUP_MEMBER_ACTION"
//3.2.4 离开了某一个群组，在微信和QQ上似乎没有这个功能
#define LEVAVE_GROUP @"LEVAVE_GROUP_MEMBER_ACTION"
//3.2.5 群组名称发生改变 xx 把群组名称修改为
#define GROUP_NAME_CHANGE @"GROUP_NAME_CHANGE"
//3.2.6 群组邀请需要群主审核
#define GROUP_INVITE_NEED_CONFIRM @"GROUP_INVITE_NEED_CONFIRM"
//3.2.7 群组邀请需要群主审核 记录ID
#define GROUP_INVITE_ID @"GROUP_INVITE_ID"
//3.2.8 群组邀请需要群主审核 审核通过
#define GROUP_INVITE_CONFIRM @"GROUP_INVITE_CONFIRM"
//3.2.9 群组邀请需要群主审核 记录群主
#define GROUP_INVITE_OWNER @"GROUP_INVITE_OWNER"
//3.3.1 红包选项  发送红包
#define READ_PACKET @"READ_PACKET_ACTION"
//3.3.2 接收某一条红包 带CUSTOM_MSG_ID作为回执    XX接收了XX的红包   这个表示红包已经被领取了
//带CUSTOM_MSG_ID作为回执(todo: 这个地方似乎并没有带回去回执消息)
#define READ_PACKET_BACK @"READ_PACKET_BACK"
//3.3.3 红包已经被退还
#define READ_PACKET_BACK_REJCET @"READ_PACKET_BACK_REJCET"
//3.3.4 红包已经被服务器处理
#define READ_PACKET_HANDLE @"READ_PACKET_HANDLE"
//3.3.5 红包是谁发的
#define MESSAGE_PACK_FROM @"MESSAGE_PACK_FROM"

//3.4.1 转账
#define TRANSFER_BANLANCE @"TRANSFER_BANLANCE"
//3.4.2 转账金额标记
#define TRANSFER_BANLANCE_TAG @"TRANSFER_BANLANCE_TAG"
//3.4.3 接收某一条转账  带CUSTOM_MSG_ID作为回执 这个是转账接收了
#define TRANSFER_MONEY_COLLECTION @"TRANSFER_MONEY_COLLECTION"
//3.4.4 接拒绝接收这一条转账
#define TRANSFER_MONEY_COLLECTION_REJECT @"TRANSFER_MONEY_COLLECTION_REJECT"
//3.5.1 本地消息通知
#define MESSAGE_SAFE_NOTCIE @"MESSAGE_SAFE_NOTCIE"
//3.6.1 好友关系发生改变
#define MESSAGE_RELATION_CHANGE @"MESSAGE_RELATION_CHANGE"
//3.7.1 消息分享
#define MESSAGE_SHARE_TYPE @"MESSAGE_SHARE_TYPE"
#define MESSAGE_SHARE_DATA @"MESSAGE_SHARE_DATA"
//3.8.1 关注消息
#define MESSAGE_FOCUS_TYPE @"MESSAGE_FOCUS_TYPE"
//3.9.1 系统活动消息推送
#define MESSAGE_SYSTEM_PUSH @"MESSAGE_SYSTEM_PUSH"
//3.10.1 语音视频
#define MESSAGE_ATTR_IS_VOICE_CALL @"MESSAGE_ATTR_IS_VOICE_CALL"
#define MESSAGE_ATTR_IS_VIDEO_CALL @"MESSAGE_ATTR_IS_VIDEO_CALL"
//3.11.1 用户名片分享
#define MESSAGE_SHARE_USER      @"MESSAGE_SHARE_USER"
#define MESSAGE_SHARE_USER_ICO  @"MESSAGE_SHARE_USER_ICO"
#define MESSAGE_SHARE_USER_ID   @"MESSAGE_SHARE_USER_ID"
//3.12.1 商品信息展示
#define SHOW_PRODUCT_CONTENT @"SHOW_PRODUCT_CONTENT"    // 仅自己可见的商品消息类型
#define MESSAGE_PRODUCT_TYPE @"MESSAGE_PRODUCT_TYPE"

// 录音
#define RECORD_SLIDUP_CANCEL    @"手指上滑 取消发送"
#define RECORD_TOUCHOUT_CANCEL    @"松开手指 取消发送"
#define RECORD_TIME_SHORT    @"时间过短"

//请求成功回调block
typedef void (^requestSuccessBlock)(NSDictionary *_Nullable dic, BOOL state);

//请求失败回调block
typedef void (^requestFailureBlock)(NSError *_Nonnull error);

//请求进度回调block
typedef void (^requestProgressBlock)(NSProgress * _Nonnull uploadProgress);

#define MAIN_FONT [UIFont systemFontOfSize:16]

#define isNull(x)             (!x || [x isKindOfClass:[NSNull class]])
#define isEmptyString(x)      (isNull(x) || [x isEqual:@""] || [x isEqual:@"(null)"])

#define REQUEST_URL(args) [[NSString stringWithFormat:@"%@/api/v1/%@",ImageBaseUrl,args] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

#ifdef DEBUG //开发阶段
#define NSLog(format,...) printf("%s",[[NSString stringWithFormat:(format), ##__VA_ARGS__] UTF8String])
#else //发布阶段
#define NSLog(...)
#endif

#endif /* Constants_h */
