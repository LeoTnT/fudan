//
//  AppConfigManager.h
//  App3.0
//
//  Created by admin on 2018/3/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMConfigModel : NSObject
@property (copy, nonatomic) NSString *appkey;           // 用户名前缀
@property (copy, nonatomic) NSString *msg_Xsy_addr_ip;      // ip
@property (copy, nonatomic) NSString *msg_Xsy_addr_upload;  // 上传路径
@end

@interface AppConfigModel : NSObject
@property (assign, nonatomic) unsigned int is_enable_sign;  // 是否启用签到
@property (assign, nonatomic) unsigned int is_enable_app;  // 是否启用App
@property (copy, nonatomic) NSString *app_disable_msg;  // App关闭时提示
@property (strong, nonatomic) IMConfigModel *im;  // im配置

@property (assign, nonatomic) unsigned int supply_enter;  // 是否启用委托商入驻
@property (assign, nonatomic) unsigned int supply_enter_pay;  // 是否启用委托商入驻缴费
@property (copy, nonatomic) NSString *enter_pay_number;//委托商入驻费
@property (copy, nonatomic) NSString *enter_pay_coin_type;//"ETH",委托商入驻费币种

@property (assign, nonatomic) unsigned int user_two_verify;  // 是否启用二次验证

@property (assign, nonatomic) unsigned int c2c_order_cancel_num;  // c2c订单当日取消次数
@property (assign, nonatomic) unsigned int c2c_order_cancel_time;  // c2c禁用后多久可以下单

@property (copy, nonatomic) NSString *logo_mobile;  // 手机端Logo
@property (copy, nonatomic) NSString *logo_mobile_abort_logo;//手机端关于页Logo

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *copyright;
@property (copy, nonatomic) NSString *symbol;
@end

@interface JYSColorModel : NSObject
@property (copy, nonatomic) NSString *roseColor;            // 涨的颜色
@property (copy, nonatomic) NSString *fellColor;            // 跌的颜色
@property (copy, nonatomic) NSString *grayButtonColor;      // 涨跌幅为0时按钮颜色

@end

@interface RateTypesModel : NSObject
@property (copy, nonatomic) NSString *currency;           // 转换币种
@property (copy, nonatomic) NSString *name;      // 转换币种名称
@property (copy, nonatomic) NSString *sign;      // sign
@property (copy, nonatomic) NSString *symbol;      // 转换币种显示的符号
@property (copy, nonatomic) NSString *rate;      // 转换币种比率

@end


@interface SymbolRateModel : NSObject
@property (copy, nonatomic) NSString *base_coin;           // 基础币种
@property (copy, nonatomic) NSString *default_coin;      // 对应的默认转换的币种
@property (copy, nonatomic) NSArray *types;  //
@end

@interface AppConfigManager : NSObject

@property (strong, nonatomic) AppConfigModel *appConfig;

@property (strong, nonatomic) JYSColorModel *colorConfig;

@property (strong, nonatomic) SymbolRateModel *symbolRateModel;
@property (strong, nonatomic) RateTypesModel *rateConfig;

@property (copy, nonatomic) NSString *app_Version;

+ (AppConfigManager *)ShardInstnce;

@end
