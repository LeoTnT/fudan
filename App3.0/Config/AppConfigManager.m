//
//  AppConfigManager.m
//  App3.0
//
//  Created by admin on 2018/3/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AppConfigManager.h"

static AppConfigManager* manager = nil;

@implementation AppConfigManager
+ (AppConfigManager *)ShardInstnce {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AppConfigManager alloc] init];
    });
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(NSString *)app_Version{
    if (isEmptyString(_app_Version)) {
        //获取当前版本
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        CFShow((__bridge CFTypeRef)(infoDictionary));
        NSString *versionStr = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
        if (isEmptyString(versionStr)) {
            versionStr = @"0";
        }
        _app_Version = versionStr;
    }
    return _app_Version;
}
@end

@implementation AppConfigModel

@end

@implementation IMConfigModel

@end



@implementation JYSColorModel

@end



@implementation RateTypesModel

@end

@implementation SymbolRateModel
+(NSDictionary *)mj_objectClassInArray {
    return @{@"types":@"RateTypesModel"};
}
@end

