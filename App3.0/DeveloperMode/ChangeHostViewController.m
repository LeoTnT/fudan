//
//  ChangeHostViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/6/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "ChangeHostViewController.h"

@interface ChangeHostViewController ()
@property (nonatomic, strong) UITextField *host;
@property (nonatomic, strong) UITextField *ws;
@property (nonatomic, strong) UITextField *mall;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation ChangeHostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = [NSMutableArray array];
    [self settupData];
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settupData {
    
#ifdef APP_SHOW_JYS
    NSString *host = [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] : @"http://tradefeature.abc123rt.com";
    [self.dataArray addObject:host];
    
    NSString *ws = [[NSUserDefaults standardUserDefaults] stringForKey:@"WSBaseUrl"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"WSBaseUrl"] : @"ws://192.168.0.200:8091/ws";
    [self.dataArray addObject:ws];
#elif defined APP_SHOW_MALLANDJYS
    NSString *host = [[NSUserDefaults standardUserDefaults] stringForKey:@"JYSBaseURL"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"JYSBaseURL"] : @"http://tradefeature.abc123rt.com";
    [self.dataArray addObject:host];
    
    NSString *ws = [[NSUserDefaults standardUserDefaults] stringForKey:@"WSBaseUrl"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"WSBaseUrl"] : @"ws://192.168.0.200:8091/ws";
    [self.dataArray addObject:ws];
    
    NSString *mall = [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] : @"http://masterv3.xsy.dsceshi.cn";
    [self.dataArray addObject:mall];
#else
    NSString *mall = [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"ImageBaseUrl"] : @"http://masterv3.xsy.dsceshi.cn";
    [self.dataArray addObject:mall];
#endif
    
}

- (void)setSubviews {
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 70)];
    self.tableView.tableFooterView = footerView;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = mainColor;
    [btn setTitle:Localized(@"save") forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = SYSTEM_FONT(16);
    [btn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(20, 25, 0, 25));
    }];
}

- (void)save {
//    if (isEmptyString(self.host.text) || isEmptyString(self.ws.text)) {
//        [XSTool showToastWithView:self.view Text:@"请输入内容"];
//        return;
//    }
#ifdef APP_SHOW_JYS
    [[NSUserDefaults standardUserDefaults] setObject:self.host.text forKey:@"JYSBaseURL"];
    [[NSUserDefaults standardUserDefaults] setObject:self.ws.text forKey:@"WSBaseUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:self.host.text forKey:@"ImageBaseUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@/api/v1",self.host.text] forKey:@"BaseUrl"];
#elif defined APP_SHOW_MALLANDJYS
    [[NSUserDefaults standardUserDefaults] setObject:self.host.text forKey:@"JYSBaseURL"];
    [[NSUserDefaults standardUserDefaults] setObject:self.ws.text forKey:@"WSBaseUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:self.mall.text forKey:@"ImageBaseUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@/api/v1",self.mall.text] forKey:@"BaseUrl"];
#else
    [[NSUserDefaults standardUserDefaults] setObject:self.host.text forKey:@"ImageBaseUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@/api/v1",self.host.text] forKey:@"BaseUrl"];
#endif

}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = SYSTEM_FONT(14);
    UITextField *tf = [UITextField new];
    tf.placeholder = self.dataArray[indexPath.row];
    [cell.contentView addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell);
        make.left.mas_equalTo(80);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(30);
    }];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"HOST：";
        self.host = tf;
    } else {
        cell.textLabel.text = @"WS：";
        self.ws = tf;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
