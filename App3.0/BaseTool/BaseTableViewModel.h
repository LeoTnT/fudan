//
//  BaseTableViewModel.h
//  App3.0
//
//  Created by apple on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseTableViewModel : NSObject

@property (nonatomic ,strong)RACSignal *signal;

@property (nonatomic ,strong)NSMutableArray *tableDataSource;

@end
