//
//  XSBaseTablewCell.m
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTablewCell.h"

@implementation XSBaseTablewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.space = 10;
        [self setContentView];
    }
    return self;
}

- (UILabel *)lineLabel {
    if (!_lineLabel) {
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.mj_h, mainWidth, .5)];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview:line];
        _lineLabel = line;
    }
    return _lineLabel;
}

- (void) setContentView {
    
}
- (UIImageView *)headerImageView {
    if (!_headerImageView) {
        _headerImageView = [UIImageView new];
        _headerImageView.userInteractionEnabled = YES;
        [_headerImageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"no_pic"]];
        [self.contentView addSubview:_headerImageView];
    }
    
    return _headerImageView;
}

- (UILabel *)titleName {
    if (!_titleName) {
        
        _titleName = [BaseTool labelWithTitle:@"  " textAlignment:0 font:[UIFont systemFontOfSize:16] titleColor:nil];
        [self.contentView addSubview:_titleName];
    }
    return _titleName;
}

@end


@implementation XSBaseCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.space = 10;
        [self setContentView];
    }
    return self;
}

- (void) setContentView {
    
}

- (UIImageView *)headerImageView {
    if (!_headerImageView) {
        _headerImageView = [UIImageView new];
        _headerImageView.userInteractionEnabled = YES;
        [_headerImageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"no_pic"]];
        [self.contentView addSubview:_headerImageView];
    }
    return _headerImageView;
}


@end
