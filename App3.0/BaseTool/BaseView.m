//
//  BaseView.m
//  App3.0
//
//  Created by apple on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView
- (instancetype)init {
    if (self = [super init]) {
        
        [self setContentView];
    }
    return self;
}

- (void)setSubViews {
    
}

- (void) setContentView {
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setContentView];
    }
    return self;
}

@end


@implementation QuickCollectionView
- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout= [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.itemSize = self.cellSize;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return _flowLayout;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        _collectionView= [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        [self addSubview:_collectionView];
        
#ifdef __IPHONE_11_0
        if (@available(iOS 11.0, *)) {
            _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            _collectionView.scrollIndicatorInsets = _collectionView.contentInset;
        }
#endif
   
    }
    return _collectionView;
}

- (void)setContentView {
    [super setContentView];

    
    
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return nil;
}

@end
