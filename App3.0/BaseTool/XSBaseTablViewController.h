//
//  XSBaseTablViewController.h
//  App3.0
//
//  Created by apple on 2017/9/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewModel.h"
@interface XSBaseTablViewController : XSBaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic ,strong)BaseTableViewModel *tableModel;
@property (nonatomic ,assign)NSInteger page;
@property (nonatomic ,copy)NSString *loadingImage;
@property (nonatomic ,copy)NSString *popViewControllerName;
@property (nonatomic ,assign)BOOL isRegist;

@property (nonatomic ,strong)NSMutableArray *dataSource;
@property (nonatomic,strong) UIView *tableViewHeaderView;
@property (nonatomic,strong) UIView *tableViewFooterView;

- (void) setupRefesh;
- (void) endRefresh;
- (void) loadRefreshData;
- (void) setHeaderRefresh;
@end
