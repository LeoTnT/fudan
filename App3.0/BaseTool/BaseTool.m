//
//  BaseTool.m
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseTool.h"

@implementation BaseTool

static NSString *key = @"asdf87290oiapso";

static UIView *alert_BagView;
static UILabel * internetLabel;

+ (void) hiddenInternet {
    [UIView animateWithDuration:1 delay:1 options:UIViewAnimationOptionCurveLinear animations:^{
        alert_BagView.transform = CGAffineTransformIdentity;
         alert_BagView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [alert_BagView removeFromSuperview];
        alert_BagView = nil;
    }];
}


+ (void) getCountryList {

}

+ (void)removeObjectForKey:(NSString *)str {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:str];
    [userDefaults synchronize];
}

+ (void)saveStr:(NSString *)str forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:str forKey:key];
    [defaults synchronize];
}

+ (NSString *)getStrUseKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:key];
    return str;
}

+ (BOOL) creatPlistWithExceptionReport:(id )value key:(NSString *)key {
    
    NSError *error = nil;
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSMutableDictionary *temp;
    if (plistXML) {
        temp = (NSMutableDictionary *) [NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
        [temp setValue:@{@"time":currentTime()} forKey:key];
    }else{
        temp = [NSMutableDictionary dictionary];
        [temp setValue:@{@"time":currentTime()} forKey:key];
        
    }
    NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:temp format:NSPropertyListXMLFormat_v1_0 options:kCFPropertyListMutableContainersAndLeaves error:&error];
    if(plistData) {
        return  [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(@" * * * *   = %@", error);
        return NO;
        
    }
    
}


+ (NSMutableDictionary *) getContentsOfFile {
    //    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    //    NSString *plistPath = [path stringByAppendingPathComponent:@"exceptionReport.plist"];
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    if (!plistPath) {
        return NULL;
    }
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    return userInfo;
}

+ (BOOL) deleteExceptionReportForKey:(NSString *)key {
    //    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    //    NSString *plistPath = [path stringByAppendingPathComponent:@"exceptionReport.plist"];
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    if (!plistPath) {
        return NO;
    }
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
     [userInfo removeObjectForKey:key];
    return  [userInfo writeToFile:plistPath atomically:YES];
}

+ (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}


+(NSDictionary *)returnDictionaryWithDataPath:(NSString *)path
{
    
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *temp = (NSDictionary *) [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
    
    return temp;
}





+ (UIView *) viewWithColor:(UIColor *)color {
    UIView *view = [UIView new];
    
    if (color) view.backgroundColor = color;
    return view;
}

+ (UILabel *) labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font titleColor:(UIColor *)color{
    
    UILabel *label = [UILabel new];
    label.userInteractionEnabled = YES;
    if (textAlignment)  label.textAlignment = textAlignment;
    label.textColor = color ? color :[UIColor blackColor];
    if (font) label.font = font;
    if (!isEmptyString(title)) {
        label.text = title;
    }
    return label;
}


+ (UIImageView *) imageWithName:(NSString *)imageName superView:(UIView *)view{
    
    UIImageView *imageView = [UIImageView new];
    if (!isEmptyString(imageName)) imageView.image = [UIImage imageNamed:imageName];
    imageView.userInteractionEnabled = YES;
    [view addSubview:imageView];
    return imageView;
}


+ (UIButton *) buttonWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font superView:(UIView *)view {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    btn.titleLabel.font = font;
    if (view) [view addSubview:btn];
    return btn;
}

+ (UIButton *) buttonTitle:(NSString *)title image:(NSString *)imageName superView:(UIView *)view {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    if (view) [view addSubview:btn];
    return btn;
}


+ (UITextField *) tetxFieldWithPlaceholder:(NSString *)placeholder font:(id)font textColor:(UIColor *)color showDefaultKeyBoard:(UIKeyboardType)keyBoardeType {
    UITextField *textField = [UITextField new];
    if (keyBoardeType) textField.keyboardType = keyBoardeType;
    textField.placeholder = placeholder;
    if ([font isKindOfClass:[UIFont class]]) {
        textField.font = font;
    }else if ([font isKindOfClass:[NSString class]]){
        NSString * font1 = font;
        textField.font = [UIFont systemFontOfSize:font1.floatValue];
    }
    textField.textColor = color;
    
    return textField;
}

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                        leftImageView:(NSString *)imageName
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UIImageView *leftImage = [UIImageView new];
    UIImage *iii = [UIImage imageNamed:imageName];
    leftImage.image = iii;
    
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftImage];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(leftView);
        
    }];
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:15];
    
    return textField;
}

+ (UITextField *)createTextFieldWith:(CGRect)frame
                       leftViewFrame:(CGRect)leftViewFrame
                         placeHolder:(NSString *)placeHolder
                           leftTitle:(NSString *)title
                       leftImageView:(NSString *)imageName
                       textAlignment:(NSTextAlignment)textAlignment
                        keyboardType:(UIKeyboardType)keyboardType
                       tapLeftAction:(void (^)())tapAction;
{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UIView *leftView = [[UIView alloc] init];
    leftView.userInteractionEnabled = YES;
    leftView.frame = leftViewFrame;
    //    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    
    [leftView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (tapAction) {
            tapAction();
        }
    }]];
    
    
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
    //    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:12];
    
    if (!isEmptyString(title)) {
        UILabel *leftTitle = [UILabel new];
        leftTitle.text = title;
        leftTitle.font = [UIFont systemFontOfSize:12];
        [leftView addSubview:leftTitle];
        [leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftView);
            isEmptyString(imageName) ?  make.centerX.mas_equalTo(leftView):make.right.mas_equalTo(leftView.mas_centerX);
        }];
        
    }
    if (!isEmptyString(imageName)) {
        UIImageView *leftImage = [UIImageView new];
        UIImage *iii = [UIImage imageNamed:imageName];
        leftImage.userInteractionEnabled = YES;
        leftImage.image = iii;
        [leftView addSubview:leftImage];
        [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftView);
            isEmptyString(title) ? make.centerX.mas_equalTo(leftView):make.left.mas_equalTo(leftView.mas_centerX);
        }];
    }
    
    
    
    return textField;
}


+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                            leftTitle:(NSString *)title
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UILabel *leftTitle = [UILabel new];
    leftTitle.text = title;
    
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftTitle];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    [leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(leftView);
        
    }];
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:15];
    
    return textField;
}


+ (UIButton *) buttonWithImage:(NSString *)imageName selected:(NSString *)selectedImageName superView:(UIView *)superView {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    if (superView) [superView addSubview:btn];
    return btn;
}
 

+(UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else if ([appRootVC isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)appRootVC;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        return  result=nav.childViewControllers.lastObject;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        result=nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    
    
    return result;
}


+ (NSArray *) getTopViewControllers {
    @try {
        
        UIViewController *currentViewController = [BaseTool getCurrentVC];
        NSArray *viewControllers = currentViewController.navigationController.viewControllers;
        
        return viewControllers.count > 0  ? viewControllers :@[];
    }
    @catch (NSException *exception){
        
    }
}

+ (void) navigationControllersIsContainViewController:(id )vc viewController:(void(^)(BOOL isContainVC,UIViewController * getViewController,NSString *containName))viewC {
    
    Class vcClass;
    NSArray *arr;
    if ([vc isKindOfClass:[NSString class]]) {
        NSString *sss = vc;
         if ([sss containsString:@","]) {
            arr = [sss componentsSeparatedByString:@","];
        }else{
             vcClass = NSClassFromString(vc);
        }
     }else if ( [vc isKindOfClass:[UIViewController class]]){
        vcClass = [vc class];
    }else{
        viewC(NO,[UIViewController new],@"");
    }
    NSArray *vcDataSource = [BaseTool getTopViewControllers];
    
    if (!vcDataSource) {
        
        viewC(NO,nil,nil);
        
     }else{
        
        __block BOOL isContain = false;
        __block UIViewController *getCurrentVC;
        if (arr.count > 0) {
            NSString *vcName;
            for (NSString *matching in arr) {
                NSString *newName = matching;
                for (UIViewController *viewController in vcDataSource) {
                    Class vcC = [viewController class];
                    NSString *sssss = NSStringFromClass(vcC);
                    if ([sssss isEqualToString:newName]) {
                        isContain = YES;
                        getCurrentVC = viewController;
                        vcName = sssss;
                    }
                }
            }
             viewC(isContain,getCurrentVC,vcName);
        }else{
             [vcDataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                UIViewController  *viewController = vcDataSource[idx];
                Class vcC = [viewController class];
                NSString *sssss = NSStringFromClass(vcC);
                if ([sssss isEqualToString:vc]) {
                    isContain = YES;
                    getCurrentVC = viewController;
                }
                
            }];
            viewC(isContain,getCurrentVC,NSStringFromClass(vcClass));
        }
    }
 
}


+ (UIButton *) logButtonWithTitle:(NSString *)title superView:(UIView *)superView {
    UIFont *btnFont = [UIFont systemFontOfSize:16];
    UIButton * loginBtn = [BaseTool buttonWithTitle:title titleColor:[UIColor whiteColor] font:btnFont superView:superView];
    loginBtn.backgroundColor = mainColor;
    loginBtn.layer.cornerRadius = 4;
    loginBtn.layer.borderWidth = .5;
    loginBtn.layer.masksToBounds = YES;
    loginBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    return loginBtn;
 }


+ (UIButton *) creatButtonWithtitle:(NSString *)title superView:(UIView *)superView {
    UIFont *btnFont = [UIFont systemFontOfSize:16];
    UIButton *creatAccount = [BaseTool buttonWithTitle:title titleColor:[UIColor whiteColor] font:btnFont superView:superView];
//    creatAccount.layer.borderColor = mainColor.CGColor;
    creatAccount.layer.borderWidth = .5;
    creatAccount.layer.cornerRadius = 4;
    creatAccount.layer.masksToBounds = YES;
    creatAccount.layer.borderColor = [UIColor whiteColor].CGColor;
     return creatAccount;
}


+ (UIImageView *) aotoLogConfigBackView:(UIView *)superView {
    
    //    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
    UIImageView *backImage = [UIImageView new];
    backImage.frame = superView.bounds;
    CGSize viewSize =superView.bounds.size;
    NSString*viewOrientation =@"Portrait";//横屏请设置成 @"Landscape"
    NSString*launchImage =nil;
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for(NSDictionary* dict in imagesDict) {
        CGSize imageSize =CGSizeFromString(dict[@"UILaunchImageSize"]);
        if(CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]]) {
            launchImage = dict[@"UILaunchImageName"];
        }
    }
    UIImage *iii =  [UIImage imageNamed:launchImage];
    
    backImage.image = iii;
    
    [superView addSubview:backImage];
    return backImage;
}


+ (UIImage *) getAPPIconImage {
    NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
    NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    UIImage* image = [UIImage imageNamed:icon];
    return image;
}


+ (NSString *)getCacheSize
{
    long long sumSize = 0;
    
    NSString *cacheFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    NSFileManager *filemanager = [NSFileManager defaultManager];
    
    //获取当前缓存路径下的所有子路径
    NSArray *subPaths = [filemanager subpathsOfDirectoryAtPath:cacheFilePath error:nil];
    
    //遍历所有子文件
    for (NSString *subPath in subPaths) {
        //1）.拼接完整路径
        NSString *filePath = [cacheFilePath stringByAppendingFormat:@"/%@",subPath];
        //2）.计算文件的大小
        long long fileSize = [[filemanager attributesOfItemAtPath:filePath error:nil]fileSize];
        //3）.加载到文件的大小
        sumSize += fileSize;
    }
    float size_m = sumSize/(1000*1000);
    return [NSString stringWithFormat:@"%.2fM",size_m];
    
}


+ (NSString *)touchQRImageGetStringWithImage:(UIImage *)image {
    // CIDetector(CIDetector可用于人脸识别)进行图片解析，从而使我们可以便捷的从相册中获取到二维码
    // 声明一个CIDetector，并设定识别类型 CIDetectorTypeQRCode
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];
    
    // 取得识别结果
    NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    NSString *scannedResult;
    for (int index = 0; index < [features count]; index ++) {
        CIQRCodeFeature *feature = [features objectAtIndex:index];
        scannedResult = feature.messageString;
        NSLog(@"scannedResult - - %@", scannedResult);
        // 在此发通知，告诉子类二维码数据
        //        [SGQRCodeNotificationCenter postNotificationName:SGQRCodeInformationFromeAibum object:scannedResult];
    }
    return scannedResult;
}


@end
