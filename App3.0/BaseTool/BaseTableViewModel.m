//
//  BaseTableViewModel.m
//  App3.0
//
//  Created by apple on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BaseTableViewModel.h"

@implementation BaseTableViewModel

- (instancetype)init {
    if (self = [super init]) {
        @weakify(self);
        self.signal = [RACObserve(self, self.tableDataSource) map:^id _Nullable(id  _Nullable value) {
            @strongify(self);
            return self.tableDataSource.count > 0 ? @0 : @1;
        }];
    }
    return self;
}

- (void)dealloc {
    
    NSLog(@"BaseTableViewModel   dealloc ");
}

@end
