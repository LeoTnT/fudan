//
//  XSBaseTablViewController.m
//  App3.0
//
//  Created by apple on 2017/9/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTablViewController.h"

@interface XSBaseTablViewController ()

@property (nonatomic ,strong)UIImageView *loading;

@end

@implementation XSBaseTablViewController

- (UIView *)tableViewHeaderView {
    if (!_tableViewHeaderView) {
        _tableViewHeaderView = [BaseTool viewWithColor:[UIColor blackColor]];
        self.tableView.tableHeaderView = _tableViewHeaderView;
    }
    return _tableViewHeaderView;
}

- (UIView *)tableViewFooterView {
    if (!_tableViewFooterView) {
        _tableViewFooterView = [BaseTool viewWithColor:[UIColor blackColor]];
        self.tableView.tableFooterView = _tableViewFooterView;
    }
    return _tableViewFooterView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.page = 1;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];

    
    self.tableModel = [BaseTableViewModel new];
    
    [self.tableModel.signal subscribeNext:^(NSNumber *isShow) {
        @strongify(self);
        if (![isShow boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
//                [self.loading removeFromSuperview];
//                self.loading = nil;
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
//                [self loading];
            });
        }
        
    }];



}

- (UIImageView *)loading {
    if (!_loading) {
        _loading = [BaseUITool imageWithName:isEmptyString(self.loadingImage) ? @"empty_logo" :self.loadingImage superView:self.tableView];
        [_loading mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.tableView);
        }];
    }
    return _loading;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)setIsRegist:(BOOL)isRegist {
    
    _isRegist = isRegist;
    if (!isRegist) {
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
}
-(NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
#ifdef __IPHONE_11_0
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
        }
#endif
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
        
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.00001)];
    }
    return _tableView;
}

- (void)setupRefesh {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [header setTitle:Localized(@"下拉加载最新数据") forState:MJRefreshStatePulling];
    self.tableView.mj_header = header;
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadOtherData)];
    
    
}

- (void) endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) setHeaderRefresh {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [header setTitle:Localized(@"下拉加载最新数据") forState:MJRefreshStatePulling];
    self.tableView.mj_header = header;
}

- (void) loadRefreshData {
    
}
- (void) loadNewData {
    self.page =1;
    [self loadRefreshData];
}

- (void) loadOtherData {
    self.page ++;
    [self loadRefreshData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)dealloc {
    
    NSLog(@"控制器被dealloc: %@", [[self class]description]);
}
@end
