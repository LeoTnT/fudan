//
//  XSBaseCollectionView.m
//  App3.0
//
//  Created by apple on 2017/10/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseCollectionView.h"

@interface XSBaseCollectionView ()

@property (nonatomic ,strong)UIButton *loading;

@property (nonatomic ,strong)UIView *centerView;
@end

@implementation XSBaseCollectionView
{
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.page = 1;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
#ifdef __IPHONE_11_0
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.collectionView.scrollIndicatorInsets = self.collectionView.contentInset;
    }
#endif
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.collectionModel = [BaseCollectionModel new];
    self.collectionModel.isObserve = self.isShowBackView;
    [self.collectionModel.signal subscribeNext:^(NSNumber *isShow) {
        @strongify(self);
        NSLog(@"isShow  = = %@",isShow);
        if ([isShow  isEqual: @0]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.centerView.hidden = YES;
                    self.loading.hidden = YES;
                });
        }else if ([isShow  isEqual: @1]){
                dispatch_async(dispatch_get_main_queue(), ^{

                    self.centerView.hidden = NO;
                    self.loading.hidden = NO;
                });
            }

    }];

    
}

- (UIView *)centerView {
    if (!_centerView) {
        _centerView = [UIView new];
        _centerView.userInteractionEnabled = YES;
        
    }
    return _centerView;
}

- (UIButton *)loading {
    if (!_loading) {
        

         UIButton *btn = [BaseUITool buttonTitle:@"非常抱歉，暂无该类店铺入驻" image:isEmptyString(self.loadingImage) ? @"empty_logo" :self.loadingImage superView:self.centerView];
        
        _nextStop = [BaseUITool buttonWithTitle:@"看看别的" titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14] superView:self.centerView];
        _nextStop.backgroundColor = mainColor;
        _nextStop.layer.cornerRadius = 4;
        _nextStop.layer.masksToBounds = YES;
        [self.collectionView addSubview:self.centerView];
        [self.centerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.collectionView);
            make.bottom.mas_equalTo(btn.mas_bottom).offset(18+36+10);
        }];
        
       
        [btn setTitleColor:[UIColor hexFloatColor:@"999999"] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setImagePosition:(LXMImagePositionTop) spacing:19];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self.centerView);
        }];

        
        [_nextStop mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.centerView);
            make.bottom.mas_equalTo(self.centerView.mas_bottom).offset(-18);
            make.height.mas_equalTo(36);
            make.width.mas_equalTo(120);
        }];
        
        @weakify(self);
        [_nextStop setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            @strongify(self);
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        _loading = btn;

        
    }
    return _loading;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout= [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.minimumInteritemSpacing = 3;
        _flowLayout.minimumLineSpacing = 3;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _flowLayout;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        _collectionView= [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:self.flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];

    }
    return _collectionView;
}


- (void) setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadNewData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.collectionView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadOtherData];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    self.collectionView.mj_footer=footer;
}
- (void) loadRefreshData {
    
}

- (void) loadNewData {
    self.page =1;
    [self loadRefreshData];
}

- (void) loadOtherData {
    self.page ++;
    [self loadRefreshData];
}

- (void) endRefresh {
    [self.collectionView.mj_footer endRefreshing];
    [self.collectionView.mj_header endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 0;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (void)dealloc {
    
    NSLog(@"控制器被dealloc: %@", [[self class]description]);
}
@end
