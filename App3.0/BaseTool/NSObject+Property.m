//
//  NSObject+Property.m
//  
//
//  Created by apple on 2017/11/27.
//

#import "NSObject+Property.h"

@implementation NSObject (Property)



+ (void )creatPropertyCodeWith:(NSDictionary *)dict {
    
    NSMutableString *strM = [NSMutableString string];
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id _Nonnull propertyName, id  _Nonnull value, BOOL * _Nonnull stop) {
        
        NSString *code;
        
        if ([value isKindOfClass:NSClassFromString(@"__NSCFString")]) {
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSString *%@;",propertyName];
            
        }else if ([value isKindOfClass:NSClassFromString(@"__NSCFNumber")]){
            
            code=[NSString stringWithFormat:@"@property (nonatomic,assign) int %@;",propertyName];
            
        }else if ([value isKindOfClass:NSClassFromString(@"__NSArrayI")]){
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSArray *%@;",propertyName];
            
        }else if ([value isKindOfClass:NSClassFromString(@"__NSDictionary")]){
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSDictionary %@;",propertyName];
            
        } else if ([value isKindOfClass:NSClassFromString(@"NSTaggedPointerString")]) {
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSString *%@;",propertyName];
            
        }else if ([value isKindOfClass:NSClassFromString(@"__NSCFConstantString")]) {
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSString *%@;",propertyName];
            
        }else if ([value isKindOfClass:NSClassFromString(@"NSTaggedPointerString")]) {
            
            code=[NSString stringWithFormat:@"@property (nonatomic,strong) NSString *%@;",propertyName];
            
        }
        
        [strM appendFormat:@"\n%@\n",code];
        
        NSLog(@"%@",strM);
        
    }];
}

@end
