//
//  BaseUITool.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"




@interface BaseUITool : NSObject

+ (BOOL) isTestAccount ;

+ (void)showInternetInfor;
+ (void) hiddenInternet;

+ (UIFont *) noramlFont;
+ (UIColor *) normalColor;
+ (UIView *) viewWithColor:(UIColor *)color;

+ (UIImageView *) imageWithName:(NSString *)imageName superView:(UIView *)view;

+ (UILabel *) labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font titleColor:(UIColor *)color;

+ (UIButton *) buttonWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font superView:(UIView *)view;

+ (UITextField *) tetxFieldWithPlaceholder:(NSString *)placeholder
                                      font:(id)font textColor:(UIColor *)color
                       showDefaultKeyBoard:(UIKeyboardType)keyBoardeType;

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                        leftImageView:(NSString *)imageName
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType;

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                            leftTitle:(NSString *)title
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType;

+ (UITextField *)createTextFieldWith:(CGRect)frame
                       leftViewFrame:(CGRect)leftViewFrame
                         placeHolder:(NSString *)placeHolder
                           leftTitle:(NSString *)title
                       leftImageView:(NSString *)imageName
                       textAlignment:(NSTextAlignment)textAlignment
                        keyboardType:(UIKeyboardType)keyboardType
                       tapLeftAction:(void (^)())tapAction;


/**
 左边图片

 @param frame frame description
 @param leftViewFrame leftViewFrame description
 @param placeHolder placeHolder description
 @param imageName imageName description
 @param textAlignment textAlignment description
 @param keyboardType keyboardType description
 @return return value description
 */
+ (UITextField *) textFiledFrame:(CGRect)frame leftViewFrame:(CGRect)leftViewFrame
                     placeHolder:(NSString *)placeHolder leftImageView:(NSString *)imageName
                   textAlignment:(NSTextAlignment)textAlignment
                    keyboardType:(UIKeyboardType)keyboardType ;

+ (UIButton *) buttonTitle:(NSString *)title image:(NSString *)imageName superView:(UIView *)view;

+ (UIButton *) buttonWithImage:(NSString *)imageName selected:(NSString *)selectedImageName superView:(UIView *)superView ;

+ (void) configShopInfor:(void (^)(NSMutableArray *arr))completed error:(void (^)())errorAction;

+ (UIViewController *)getCurrentVC;

+ (NSArray *) getTopViewControllers;

+ (void) navigationControllersIsContainViewController:(id )vc viewController:(void(^)(BOOL isContainVC,UIViewController * getViewController,NSString *containName))viewC ;

+ (UIImageView *) aotoLogConfigBackView:(UIView *)superView;
@end
