//
//  UIAlertController+Category.h
//  App3.0
//
//  Created by xinshang on 2018/6/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CallBackBlock)(NSInteger btnIndex);

@interface UIAlertController (Category)

/**
 自定义封装的UIAlertController方法
 @param viewController 显示的vc
 @param alertControllerStyle UIAlertControllerStyle 样式
 @param title 标题
 @param message 提示信息
 @param block 回调block
 @param cancelBtnTitle 取消button标题
 @param destructiveBtnTitle 红色的按钮
 @param otherBtnTitles 其他button标题
 */

+ (void)showAlertCntrollerWithViewController:(UIViewController*)viewController alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle title:(NSString*)title message:(NSString*)message CallBackBlock:(CallBackBlock)block cancelButtonTitle:(NSString *)cancelBtnTitle
                      destructiveButtonTitle:(NSString *)destructiveBtnTitle
                           otherButtonTitles:(NSString *)otherBtnTitles,...NS_REQUIRES_NIL_TERMINATION;

@end
