//
//  ItemCell.h
//  ActionSheetExtension
//
//  Created by yixiang on 15/7/6.
//  Copyright (c) 2015年 yixiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvancedPublishGoodsViewController.h"

#define RGBCOLOR(r, g, b)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r, g, b, a)   [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

@interface Item : NSObject

@property (nonatomic , strong) NSString *icon;//图片地址

@property (nonatomic , strong) NSString *title;//标题

@end

/*
 "gxz_rifan" = "0.1";
 "gxz_type" = 1;
 "gxz_value" = 16;
 */
@interface Ad_GXZModel :NSObject

@property (nonatomic ,copy)NSString *gxz_rifan;
@property (nonatomic ,copy)NSString *gxz_type;
@property (nonatomic ,copy)NSString *gxz_value;

@end

@interface ActionItemCell : UITableViewCell

-(void)setData : (NSDictionary *)item;

@end


