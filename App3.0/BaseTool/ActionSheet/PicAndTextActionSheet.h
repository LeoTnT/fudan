//
//  PicAndTextActionSheet.h
//  ActionSheetExtension
//
//  Created by yixiang on 15/7/6.
//  Copyright (c) 2015年 yixiang. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PicAndTextActionSheet : UIView



- (instancetype) initWithList : (NSArray *)list title : (NSString *) title selected:(void (^)(NSInteger index))DidSelectIndex;

- (void) showInView : (UIViewController *)controller;

//@property (nonatomic ,copy)void (^didSelectIndex)(NSInteger index);

@end
