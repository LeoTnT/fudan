//
//  ItemCell.m
//  ActionSheetExtension
//
//  Created by yixiang on 15/7/6.
//  Copyright (c) 2015年 yixiang. All rights reserved.
//

#import "ActionItemCell.h"

@interface ActionItemCell()

//@property (nonatomic , strong) UIImageView *leftView;
//@property (nonatomic , strong) UILabel *titleLabel;
@property (nonatomic , strong) id  item;

@end

@implementation ActionItemCell

//在initWithStyle中，生成控件，将控件添加到self.contentView中
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textLabel.font = [UIFont systemFontOfSize:17];
        UIView *line = [BaseUITool viewWithColor:[UIColor hexFloatColor:@"ECECEC"]];
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20.5);
            make.right.mas_equalTo(self).offset(-20.5);
            make.height.mas_equalTo(1);
            make.bottom.mas_equalTo(self);
        }];
    }
    return self;
}


//设置数据
-(void)setData:(NSDictionary *)item{
    
    if ([item isKindOfClass:[Item class]]) {
       Item *_item = (Item *)item;
        self.imageView.image = [UIImage imageNamed:_item.icon];
        self.textLabel.text = _item.title;
    }else if ([item isKindOfClass:[Ad_GXZModel class]]){

        Ad_GXZModel *model = (Ad_GXZModel *)item;
        self.textLabel.text = [NSString stringWithFormat:@"商品贡献值%@%%,日返%@%%",model.gxz_value,model.gxz_rifan];
    }
    

    
}

//设置点击事件
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.backgroundColor = RGBCOLOR(12, 102, 188);
    }else{
        self.backgroundColor = [UIColor whiteColor];
    }
    
}

@end

@implementation Item



@end


@implementation Ad_GXZModel


@end
