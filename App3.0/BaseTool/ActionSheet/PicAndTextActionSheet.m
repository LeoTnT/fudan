//
//  PicAndTextActionSheet.m
//  ActionSheetExtension
//
//  Created by yixiang on 15/7/6.
//  Copyright (c) 2015年 yixiang. All rights reserved.
//

#import "PicAndTextActionSheet.h"
#import "ActionItemCell.h"

@interface PicAndTextActionSheet ()<UITableViewDataSource , UITableViewDelegate , UIGestureRecognizerDelegate>

@property (nonatomic , strong) UITableView *tableview;
@property (nonatomic , strong) NSArray *listData;
@property (nonatomic , strong) NSString * title;
@property (nonatomic , strong) UIView *customerView;
@property (nonatomic ,copy)void (^didSelectIndex)(NSInteger index);

@end

@implementation PicAndTextActionSheet

- (instancetype) initWithList : (NSArray *)list title : (NSString *) title selected:(void (^)(NSInteger index))DidSelectIndex{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, mainWidth, mainHeight);
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:.3];
        
        CGFloat space = 12;
        
        _tableview = [[UITableView alloc] initWithFrame:CGRectMake(space, 0, mainWidth-space*2, 50*[list count]) style:UITableViewStylePlain];
        _tableview.dataSource = self;
        _tableview.delegate = self;
        _tableview.scrollEnabled = NO;
        _tableview.layer.cornerRadius = 5;
        _tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *cancelLabel = [[UILabel alloc] initWithFrame:CGRectMake(space,CGRectGetHeight(_tableview.frame)+10, mainWidth-space*2, 50)];
        cancelLabel.layer.cornerRadius =5;
        cancelLabel.layer.backgroundColor = [UIColor whiteColor].CGColor;
        cancelLabel.font = [UIFont systemFontOfSize:17];
        cancelLabel.text = Localized(@"cancel_btn");
        cancelLabel.textAlignment = NSTextAlignmentCenter;
        cancelLabel.textColor = mainColor;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
        cancelLabel.userInteractionEnabled = YES;
        [cancelLabel addGestureRecognizer:tapRecognizer];
        
        _customerView = [[UIView alloc] initWithFrame:CGRectMake(0, mainHeight, mainWidth, CGRectGetHeight(_tableview.frame)+80)];
        _customerView.backgroundColor = [UIColor clearColor];
        [_customerView addSubview:_tableview];
        [_customerView addSubview:cancelLabel];
        
        [self addSubview:_customerView];
        self.didSelectIndex = DidSelectIndex;
        
        _listData = list;
        _title = title;
        
    }
    return self;
}

//如果tableview处于uiview上面，uiview整个背景有点击事件，但是我们需要如果我们点击tableview的时候，处理tableview的点击事件，而不是uiview的事件。在这里，我们需要判断我们点击事件是否在uiview上还是在uitableview上。
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isKindOfClass:[self class]]){
        return YES;
    }
    return NO;
}

-(void)animeData{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    self.userInteractionEnabled = YES;
    
    [UIView animateWithDuration:0.25f animations:^{
        self.backgroundColor = RGBACOLOR(160, 160, 160, 0.4);
        CGRect originRect = _customerView.frame;
        originRect.origin.y = mainHeight - CGRectGetHeight(_customerView.frame);
        _customerView.frame = originRect;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) tappedCancel{
    [UIView animateWithDuration:.25 animations:^{
        self.alpha = 0;
        CGRect originRect = _customerView.frame;
        originRect.origin.y = mainHeight;
        _customerView.frame = originRect;
    } completion:^(BOOL finished) {
        if (finished) {
            for (UIView *v in _customerView.subviews) {
                [v removeFromSuperview];
            }
            [_customerView removeFromSuperview];
        }
    }];
}

- (void)  showInView:(UIViewController *)controller{
    if (controller) {
        [controller.view addSubview:self];
    }else{
        [[UIApplication sharedApplication].delegate.window.rootViewController.view addSubview:self];
    }
    [self animeData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_listData count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 50;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellIdentifier = @"DownSheetCell";
    ActionItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[ActionItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    [cell setData:[_listData objectAtIndex:indexPath.row]];
    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self tappedCancel];
    
    if (self.didSelectIndex) {
        self.didSelectIndex(indexPath.row);
    }

}

@end
