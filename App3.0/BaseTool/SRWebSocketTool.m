//
//  SRWebSocketTool.m
//  App3.0
//
//  Created by Sunny on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "SRWebSocketTool.h"

@interface SRWebSocketTool ()
@property (nonatomic,strong) SRWebSocket *webSocket;
@end

@implementation SRWebSocketTool

singletonImplementation(SRWebSocketTool)

- (SRWebSocket *)webSocket {
    if (_webSocket == nil) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:WSBaseUrl] ];
        _webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
        _webSocket.delegate = self;
    }
    return _webSocket;
}

- (void) configWebSocketService {
    
    @try{
        [self closeWebSocket];
        
        self.webSocket = nil;
        [self.webSocket open];
    }@catch (NSException *exception){
        [self closeWebSocket];
        self.webSocket = nil;
        [self.webSocket open];
        
    }


    [RACObserve(self, webSocket.readyState) subscribeNext:^(id  _Nullable x) {
        [self reconnect];
    }];
    
    [self netWorkStatusCheck];
//    self.webSocketMessage = [RACSubject subject];
}

#pragma mark 实时监测网络变化
- (void)netWorkStatusCheck {
//    @weakify(self);
    [XSHTTPManager checkNetStatus:^(NSUInteger netStatus) {
        XSLog(@"%ld",netStatus);
        if (netStatus == XSNetworkStatusNotReachable || netStatus == XSNetworkStatusUnknown) {
            XSLog(@"无网络");
        } else {
            [self reconnect];
        }
    }];
}

- (RACSubject *)webSocketMessage {
    if (_webSocketMessage == nil) {
        _webSocketMessage = [RACSubject subject];
    }
    return _webSocketMessage;
}


- (void) closeWebSocket {

    if (self.webSocket && self.webSocket.readyState == SR_OPEN) {
        [self.webSocket closeWithCode:SRStatusCodeGoingAway reason:nil];
        self.webSocket = nil;
    }

    if (self.webSocketMessage) {
         [self.webSocketMessage sendCompleted];
        self.webSocketMessage = nil;
    }
    self.webSocket = nil;

}

#pragma mark websorcet
// message will either be an NSString if the server is using text
//{"ping":1526010810}
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    
    NSData *data = [self uncompressZippedData:message];
    NSError *error;
    NSDictionary *dicJson=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([dicJson.allKeys containsObject:@"ping"]) {
        NSDictionary *ddd= @{@"pong":dicJson[@"ping"]};
        NSData *aaaaa= [NSJSONSerialization dataWithJSONObject:ddd options:NSJSONWritingPrettyPrinted error:nil];
        if (self.webSocket.readyState == SR_OPEN) {
            [self.webSocket send:aaaaa];
        } else {
            [self reconnect];
        }
    }else {
        [self printJson:data path:@""];
        [self messageWithType:dicJson];
        
    }
//    XSLog(@"didReceiveMessage   dicJson =%@ ",dicJson);
    
}

//打印JSON数据
-(void)printJson:(id)responseData path:(NSString *)path

{
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //    NSLog(@"------%@-----\n%@\n------------",path,responseStr);
    NSLog(@"\n -------↓↓↓↓-Json-↓↓↓↓---------%@----------URL---》 \n %@ \n 《----------↑↑↑↑-Json-↑↑↑↑---------------------------- \n",path,responseStr);
    
}

//rep = "market.ethcny.trade.detail";

- (void) messageWithType:(NSDictionary *)dic {
    
    NSString *key = nil ;
    NSString *message = nil;
    if ([dic.allKeys containsObject:@"ch"]) {
        NSString *value = dic[@"ch"];
        key = @"ch";
        message = value;
        XSLog(@"%@",value);
//        if ([value isEqualToString:@"market.overview"]){
//            key = @"ch";
//            message = value;
//        } else {
//            key = @"ch";
//            message = value;
//        }
    }else if ([dic.allKeys containsObject:@"rep"]){
        NSString *value = dic[@"rep"];
        key = @"rep";
        message =  value;
    }else if ([dic.allKeys containsObject:@"sub"]){
        NSString *value = dic[@"sub"];
        key = @"sub";
        message =  value;
    }
    if (isEmptyString(key) ||isEmptyString(message) || !dic) {
        return;
    }
    
    [self.webSocketMessage sendNext:RACTuplePack(key,message,dic)];
//    [self.webSocketMessage sendCompleted];
//    self.webSocketMessage = nil;
    
//        NSLog(@" key = %@ message = %@  dic=  %@",key,message,dic);

}

- (void) dingy {
    
//    NSArray *symbols = @[@"gpacny"];
//
//    for (NSInteger x = 0; x <symbols.count; x ++) {
//
//        NSString *ss = symbols[x];
//        //请求最新成交记录
//        [self sendMessage:[ss stringByAppendingString:@".trade.detail"] key:@"req" deep:nil];
//        // 请求市场最新价格之类的
//        [self sendMessage:[ss stringByAppendingString:@".detail"] key:@"req" deep:nil];
//        // 请求市场深度
//        [self sendMessage:[ss stringByAppendingString:@".depth.step"] key:@"req" deep:@"0"];
//        //订阅新数据
//        [self sendNewMessage:[ss stringByAppendingString:@".depth.step"] key:@"sub" deep:@"0"];
//        // 请求市场最新成交记录
//        [self sendMessage:[ss stringByAppendingString:@".trade.detail"] key:@"sub" deep:nil];
//        //订阅市场行情
//        [self sendMessage:[ss stringByAppendingString:@".detail"] key:@"sub" deep:nil];
//    }
    [self sendNewMessage:@"overview" key:@"sub" deep:nil];
}

- (void) reconnect {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.webSocket.readyState == SR_OPEN || self.webSocket.readyState == SR_CONNECTING) {
            return ;
        }
        [self.webSocket closeWithCode:1000 reason:@"xxx_mm"];
        self.webSocket = nil;
        [self.webSocket open];
    });
}




- (void) sendMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep{
    NSString *ss;
    if (deep.length) {
        ss = [NSString stringWithFormat:@"market.%@%@",message,deep];
    }else{
        ss = [NSString stringWithFormat:@"market.%@",message];
    }
    
    NSDictionary *d = @{key:ss};
     NSString *sssss = d.mj_JSONString;
    
    XSLog(@"%@",sssss);
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket send:sssss];
    } else {
        [self reconnect];
    }
}


- (void) sendNewMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep{
    [self sendNewMessage:message key:key deep:deep isTradeView:NO];
}
- (void) sendNewMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep isTradeView:(BOOL)isTradeView{
    NSString *ss;
    NSArray *arr;
    if (deep.length) {
        ss = [NSString stringWithFormat:@"market.%@%@",message,deep];
        if (isTradeView) {
//            arr = @[@"bids.9",@"asks.9"];
            arr = @[@"bids.5",@"asks.5"];
        }else{
            arr = @[@"bids.50",@"asks.50"];

        }
    }else{
        ss = [NSString stringWithFormat:@"market.%@",message];
        arr = @[];
    }
    
    //    NSDictionary * arr = @{@"values":@[@"bids.11",@"asks.11"]};
    
    NSDictionary *d;
    
    if (arr.count) {
        d = @{key:ss,
              @"pick":arr
              };
    } else {
        d = @{key:ss
              };
    }
    
    NSString *ssss = d.mj_JSONString;
    XSLog(@"%@",ssss);
    
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket send:ssss];
    } else {
        [self reconnect];
    }
}

- (void)sendKlineRequestMessage:(NSString *)message key:(NSString *)key period:(NSString *)period fromTime:(NSNumber *)from toTime:(NSNumber *)to ID:(NSString *)idStr{
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    NSString * requestString = [NSString stringWithFormat:@"market.%@.kline.%@",message,period];
    
    params[key] = requestString;
    params[@"id"] = idStr;
    
    if (from) {
        params[@"from"] = from;
    }
    if (to) {
        params[@"to"] = to;
    }
    
    NSString *requestJson = params.mj_JSONString;
    
    XSLog(@"%@",requestJson);
    
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket send:requestJson];
    } else {
        [self reconnect];
    }
}


- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
     [self dingy];
    
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
//     [self reconnect];
}
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    if (code != 1000) {
//        [self reconnect];
    }
    
}
- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    
    XSLog(@"pongPayload  =%@",pongPayload);
}

-(NSData *)uncompressZippedData:(NSData *)compressedData
{
    if ([compressedData length] == 0) return compressedData;
    
    unsigned long full_length = [compressedData length];
    
    unsigned long half_length = [compressedData length] / 2;
    NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
    BOOL done = NO;
    int status;
    z_stream strm;
    strm.next_in = (Bytef *)[compressedData bytes];
    strm.avail_in = (uint)[compressedData length];
    strm.total_out = 0;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    if (inflateInit2(&strm, (15+32)) != Z_OK) return nil;
    while (!done) {
        // Make sure we have enough room and reset the lengths.
        if (strm.total_out >= [decompressed length]) {
            [decompressed increaseLengthBy: half_length];
        }
        strm.next_out = [decompressed mutableBytes] + strm.total_out;
        strm.avail_out = (uint)([decompressed length] - strm.total_out);
        // Inflate another chunk.
        status = inflate (&strm, Z_SYNC_FLUSH);
        if (status == Z_STREAM_END) {
            done = YES;
        } else if (status != Z_OK) {
            break;
        }
    }
    if (inflateEnd (&strm) != Z_OK) return nil;
    // Set real length.
    if (done) {
        [decompressed setLength: strm.total_out];
        return [NSData dataWithData: decompressed];
    } else {
        return nil;
    }
}

// Return YES to convert messages sent as Text to an NSString. Return NO to skip NSData -> NSString conversion for Text messages. Defaults to YES.
- (BOOL)webSocketShouldConvertTextFrameToString:(SRWebSocket *)webSocket {
    return NO;
}


-(void)dealloc{
    XSLog(@"SRWebSocketTool");
}

@end
