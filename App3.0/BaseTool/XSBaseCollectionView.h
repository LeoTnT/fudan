//
//  XSBaseCollectionView.h
//  App3.0
//
//  Created by apple on 2017/10/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionModel.h"
@interface XSBaseCollectionView : XSBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong)UICollectionView *collectionView;

@property (nonatomic ,strong)UICollectionViewFlowLayout *flowLayout;

@property (nonatomic ,copy)NSString *loadingImage;

@property (nonatomic ,strong)UIButton *nextStop;

@property (nonatomic ,assign)NSInteger page;

@property (nonatomic ,assign)BOOL isShowBackView;

@property (nonatomic ,strong)BaseCollectionModel *collectionModel;



- (void) setRefreshStyle;

- (void) loadRefreshData;

- (void) endRefresh;
@end
