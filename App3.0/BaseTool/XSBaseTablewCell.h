//
//  XSBaseTablewCell.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSBaseTablewCell : UITableViewCell
@property (nonatomic ,strong) UIImageView *headerImageView;

@property (nonatomic ,strong) UILabel *titleName;
@property (nonatomic,strong) UILabel *lineLabel;

@property (nonatomic ,assign)CGFloat space;
- (void) setContentView;
@end


@interface XSBaseCollectionCell : UICollectionViewCell

@property (nonatomic ,strong) UIImageView *headerImageView;
@property (nonatomic ,assign)CGFloat space;
- (void) setContentView;
@end
