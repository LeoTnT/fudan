//
//  BaseTool.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
//static inline NSString *currentTime (){
//    NSDate *datenow = [NSDate date];
//    NSString *timeSp = [NSString stringWithFormat:@"%f", [datenow timeIntervalSince1970]*1000];
//    return timeSp;
//}
//
// 
//static inline NSString *GET_UUID() {
//    
//    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//}
//
//
//static inline NSString *GET_SystemVersion() {
//    
//    return [[UIDevice currentDevice] systemVersion];
//}

static inline float GET_HEIGT(CGFloat space) {
   CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height >= 667) {
        return space * height/667;
    }else{
        return space-10;
    }
//
}

@interface BaseTool : NSObject


+ (void) hiddenInternet;

+ (void) getCountryList ;
+ (void) getCurrentLanguages;

/**
 国际化
  @param name n选择语言
 */
+ (void) changeLangrageWithName:(NSString *)name ;

+ (BOOL)saveUser:(LoginDataParser *)user;
+ (void) removeUserInfor;
+ (LoginDataParser *)user;

+ (void) cacheBitList;
+ (NSMutableArray *) getBIT_LIST;
+ (void)logout;



+ (NSString *)convertToJsonData:(NSDictionary *)dict;
+ (NSDictionary *)convertToParamFromParameters:(NSDictionary *)dict;
+ (BOOL) creatPlistWithExceptionReport:(id )value key:(NSString *)key;
+ (BOOL) deleteExceptionReportForKey:(NSString *)key;
+ (NSMutableDictionary *) getContentsOfFile;
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
/**
 根据路径获取字典
 */
+(NSDictionary *)returnDictionaryWithDataPath:(NSString *)path;



+ (NSString *)getStrUseKey:(NSString *)key;
+ (void)saveStr:(NSString *)str forKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)str;

// log button 黑底 白字
+ (UIButton *) logButtonWithTitle:(NSString *)title superView:(UIView *)superView;
// 白底 黑字
+ (UIButton *) creatButtonWithtitle:(NSString *)title superView:(UIView *)superView ;

+ (UIView *) viewWithColor:(UIColor *)color;
+ (UIImageView *) imageWithName:(NSString *)imageName superView:(UIView *)view;

+ (UILabel *) labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font titleColor:(UIColor *)color;

+ (UIButton *) buttonWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font superView:(UIView *)view;

+ (UITextField *) tetxFieldWithPlaceholder:(NSString *)placeholder
                                      font:(id)font textColor:(UIColor *)color
                       showDefaultKeyBoard:(UIKeyboardType)keyBoardeType;

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                        leftImageView:(NSString *)imageName
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType;

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                            leftTitle:(NSString *)title
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType;

+ (UITextField *)createTextFieldWith:(CGRect)frame
                       leftViewFrame:(CGRect)leftViewFrame
                         placeHolder:(NSString *)placeHolder
                           leftTitle:(NSString *)title
                       leftImageView:(NSString *)imageName
                       textAlignment:(NSTextAlignment)textAlignment
                        keyboardType:(UIKeyboardType)keyboardType
                       tapLeftAction:(void (^)())tapAction;

+ (UIButton *) buttonTitle:(NSString *)title image:(NSString *)imageName superView:(UIView *)view;

+ (UIButton *) buttonWithImage:(NSString *)imageName selected:(NSString *)selectedImageName superView:(UIView *)superView ;

 
+ (UIViewController *)getCurrentVC;

+ (NSArray *) getTopViewControllers;

/**
 获取app 图标
 */
+ (UIImage *) getAPPIconImage;

+ (void) navigationControllersIsContainViewController:(id )vc viewController:(void(^)(BOOL isContainVC,UIViewController * getViewController,NSString *containName))viewC ;

+ (UIImageView *) aotoLogConfigBackView:(UIView *)superView;


+ (NSString *)getCacheSize;


+ (NSString *)touchQRImageGetStringWithImage:(UIImage *)image;


+ (NSMutableArray *) setLabelTitleWithNumber:(NSArray *)arr;
@end
