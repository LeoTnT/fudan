//
//  BaseUITool.m
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BaseUITool.h"

@implementation BaseUITool


+ (BOOL) isTestAccount {
    NSString *uid = [UserInstance ShardInstnce].uid ;
    if ([uid isEqualToString:@"1"] || isEmptyString(uid)) {
         return YES;
    }else{
        return NO;
    }
}

/**
 默认显示 14
 */
+ (UIFont *) noramlFont {
    return [UIFont systemFontOfSize:14];
}

/**
 搜索文字颜色
 */

+ (UIColor *) normalColor {
    return [UIColor hexFloatColor:@"f4f4f4"];
}


+ (UIView *) viewWithColor:(UIColor *)color {
    UIView *view = [UIView new];
    if (color) view.backgroundColor = color;
    return view;
}

+ (UILabel *) labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font titleColor:(UIColor *)color{
    
    UILabel *label = [UILabel new];
    if (textAlignment)  label.textAlignment = textAlignment;
    label.textColor = color ? color :[UIColor blackColor];
    if (font) label.font = font;
    if (!isEmptyString(title)) {
        label.text = title;
    }
    return label;
}


+ (UIImageView *) imageWithName:(NSString *)imageName superView:(UIView *)view{
    
    UIImageView *imageView = [UIImageView new];
    if (!isEmptyString(imageName)) imageView.image = [UIImage imageNamed:imageName];    
    imageView.userInteractionEnabled = YES;
    [view addSubview:imageView];
    return imageView;
}


+ (UIButton *) buttonWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font superView:(UIView *)view {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    btn.titleLabel.font = font;
    if (view) [view addSubview:btn];
    return btn;
}

+ (UIButton *) buttonTitle:(NSString *)title image:(NSString *)imageName superView:(UIView *)view {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    if (view) [view addSubview:btn];
    return btn;
}


+ (UITextField *) tetxFieldWithPlaceholder:(NSString *)placeholder font:(id)font textColor:(UIColor *)color showDefaultKeyBoard:(UIKeyboardType)keyBoardeType {
    UITextField *textField = [UITextField new];
    if (keyBoardeType) textField.keyboardType = keyBoardeType;
    textField.placeholder = placeholder;
    if ([font isKindOfClass:[UIFont class]]) {
        textField.font = font;
    }else if ([font isKindOfClass:[NSString class]]){
        NSString * font1 = font;
        textField.font = [UIFont systemFontOfSize:font1.floatValue];
    }
    textField.textColor = color;

    return textField;
}

+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                         leftImageView:(NSString *)imageName
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UIImageView *leftImage = [UIImageView new];
    UIImage *iii = [UIImage imageNamed:imageName];
    leftImage.image = iii;
    
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftImage];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(leftView);
        
    }];
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:15];
    
    return textField;
}

+ (UITextField *)createTextFieldWith:(CGRect)frame
                       leftViewFrame:(CGRect)leftViewFrame
                         placeHolder:(NSString *)placeHolder
                           leftTitle:(NSString *)title
                       leftImageView:(NSString *)imageName
                       textAlignment:(NSTextAlignment)textAlignment
                        keyboardType:(UIKeyboardType)keyboardType
                       tapLeftAction:(void (^)())tapAction;
{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UIView *leftView = [[UIView alloc] init];
    leftView.userInteractionEnabled = YES;
    leftView.frame = leftViewFrame;
//    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    
    [leftView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (tapAction) {
            tapAction();
        }
    }]];

    
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
//    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:12];
    
    if (!isEmptyString(title)) {
        UILabel *leftTitle = [UILabel new];
        leftTitle.text = title;
        leftTitle.font = [UIFont systemFontOfSize:12];
        [leftView addSubview:leftTitle];
        [leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftView);
            isEmptyString(imageName) ?  make.centerX.mas_equalTo(leftView):make.right.mas_equalTo(leftView.mas_centerX);
        }];
        
    }
    if (!isEmptyString(imageName)) {
        UIImageView *leftImage = [UIImageView new];
        UIImage *iii = [UIImage imageNamed:imageName];
        leftImage.userInteractionEnabled = YES;
        leftImage.image = iii;
        [leftView addSubview:leftImage];
        [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftView);
            isEmptyString(title) ? make.centerX.mas_equalTo(leftView):make.left.mas_equalTo(leftView.mas_centerX);
        }];
    }
    

    
    return textField;
}


+ (UITextField *) textFiledFrame:(CGRect)frame leftViewFrame:(CGRect)leftViewFrame
                     placeHolder:(NSString *)placeHolder leftImageView:(NSString *)imageName
                   textAlignment:(NSTextAlignment)textAlignment
                    keyboardType:(UIKeyboardType)keyboardType {
    
   UITextField *textField = [BaseUITool createTextFieldWith:frame leftViewFrame:leftViewFrame placeHolder:placeHolder leftTitle:nil leftImageView:imageName textAlignment:textAlignment keyboardType:keyboardType tapLeftAction:nil];
    textField.font = [UIFont systemFontOfSize:14];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    return textField;
}


+ (UITextField *) createTextFieldWith:(CGRect)frame
                          placeHolder:(NSString *)placeHolder
                        leftTitle:(NSString *)title
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    UILabel *leftTitle = [UILabel new];
    leftTitle.text = title;
    
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftTitle];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftView.frame = CGRectMake(0, 0, 56-25, frame.size.height);
    [leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(leftView);
        
    }];
    [textField setLeftView:leftView];
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = [UIFont systemFontOfSize:15];
    
    return textField;
}


+ (UIButton *) buttonWithImage:(NSString *)imageName selected:(NSString *)selectedImageName superView:(UIView *)superView {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    if (superView) [superView addSubview:btn];
    return btn;
}



+ (void) configShopInfor:(void (^)(NSMutableArray *arr))completed error:(void (^)())errorAction {
    
    if ([UserInstance ShardInstnce].homeMenuArray.count == 0) {
        RACSignal *signal = [XSHTTPManager  rac_POSTURL:Url_GetMallMenuList params:@{@"type":@"app_index",@"limit":@(16)}];
        [signal subscribeNext:^(resultObject *state) {
            if (state.status) {
                NSMutableArray *arr = [NSMutableArray array];
                NSArray *data = state.data[@"data"];
                [data.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                    TabMallMenuItem *item=[TabMallMenuItem mj_objectWithKeyValues:x];
                    [arr addObject:item];
                }completed:^{
                    [UserInstance ShardInstnce].homeMenuArray = arr;
                    if (completed) {
                        completed(arr);
                    }

                }];
            }
            
        }];
        
        [signal subscribeError:^(NSError * _Nullable error) {

            if (errorAction) {
                errorAction();
            }
        }];
    }
}

+ (UIViewController *)getCurrentVC {
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else if ([appRootVC isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)appRootVC;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        return  result=nav.childViewControllers.lastObject;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        result=nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    
    
    return result;
}


+ (NSArray *) getTopViewControllers {
    @try {
        
        UIViewController *currentViewController = [BaseUITool getCurrentVC];
          NSArray *viewControllers = currentViewController.navigationController.viewControllers;
        
        return viewControllers.count > 0  ? viewControllers :@[];
    }
    @catch (NSException *exception){
        
    }
}

+ (void) navigationControllersIsContainViewController:(id )vc viewController:(void(^)(BOOL isContainVC,UIViewController * getViewController,NSString *containName))viewC {
    
    Class vcClass;
    NSArray *arr;
    if ([vc isKindOfClass:[NSString class]]) {
        NSString *sss = vc;
        
        if ([sss containsString:@","]) {
            arr = [sss componentsSeparatedByString:@","];
        }else{
            
            vcClass = NSClassFromString(vc);
        }
        
    }else if ( [vc isKindOfClass:[UIViewController class]]){
        vcClass = [vc class];
    }else{
        viewC(NO,[UIViewController new],@"");
    }
    NSArray *vcDataSource = [BaseUITool getTopViewControllers];
    
    if (!vcDataSource) {
        
        viewC(NO,nil,nil);
        
        
    }else{
        
        __block BOOL isContain = false;
        __block UIViewController *getCurrentVC;
        if (arr.count > 0) {
            NSString *vcName;
            for (NSString *matching in arr) {
                NSString *newName = matching;
                for (UIViewController *viewController in vcDataSource) {
                    Class vcC = [viewController class];
                    NSString *sssss = NSStringFromClass(vcC);
                    if ([sssss isEqualToString:newName]) {
                        isContain = YES;
                        getCurrentVC = viewController;
                        vcName = sssss;
                    }
                }
            }
            
            viewC(isContain,getCurrentVC,vcName);
        }else{
            
            [vcDataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                UIViewController  *viewController = vcDataSource[idx];
                Class vcC = [viewController class];
                NSString *sssss = NSStringFromClass(vcC);
                if ([sssss isEqualToString:vc]) {
                    isContain = YES;
                    getCurrentVC = viewController;
                }
                
            }];
            viewC(isContain,getCurrentVC,NSStringFromClass(vcClass));
        }
    }
    
    
    
}




+ (UIImageView *) aotoLogConfigBackView:(UIView *)superView {
    
    //    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
    UIImageView *backImage = [UIImageView new];
    backImage.frame = superView.bounds;
    CGSize viewSize =superView.bounds.size;
    NSString*viewOrientation =@"Portrait";//横屏请设置成 @"Landscape"
    NSString*launchImage =nil;
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for(NSDictionary* dict in imagesDict) {
        CGSize imageSize =CGSizeFromString(dict[@"UILaunchImageSize"]);
        if(CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]]) {
            launchImage = dict[@"UILaunchImageName"];
        }
    }
    
    backImage.image = [UIImage imageNamed:launchImage];
    
    
    return backImage;
}


static UILabel *internetLabel;

/**
 *  提示用户
 */
+ (void)showInternetInfor
{
    UIViewController *vc = [BaseUITool getCurrentVC];
    if (!vc) return;
    
    internetLabel = [[UILabel alloc] init];
    internetLabel.backgroundColor = [UIColor hexFloatColor:@"e3a869"];
    
    internetLabel.mj_x = 0;
    internetLabel.mj_w = vc.navigationController.view.mj_w;
    internetLabel.mj_h = 35;
    internetLabel.mj_y = 64 - internetLabel.mj_h;
    
    [vc.navigationController.view insertSubview:internetLabel belowSubview:vc.navigationController.navigationBar];
    
    internetLabel.textColor = [UIColor whiteColor];
    internetLabel.textAlignment = NSTextAlignmentCenter;
    
    internetLabel.text = @"您的网路已断开链接";
    internetLabel.alpha = 0.0;
    [UIView animateWithDuration:1 animations:^{
        internetLabel.transform = CGAffineTransformMakeTranslation(0, internetLabel.mj_h);
        internetLabel.alpha = 1.0;
    } completion:^(BOOL finished) {

    }];
}


+ (void) hiddenInternet {
    [UIView animateWithDuration:1 delay:1 options:UIViewAnimationOptionCurveLinear animations:^{
        internetLabel.transform = CGAffineTransformIdentity;
        
        internetLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        [internetLabel removeFromSuperview];
    }];
}
@end
