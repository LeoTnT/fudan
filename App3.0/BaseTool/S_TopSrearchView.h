//
//  S_TopSrearchView.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_TopSrearchView : UIView

@property (nonatomic ,strong)UIView *searchField;


@property (nonatomic ,strong)UILabel *searchLabel;


@end
