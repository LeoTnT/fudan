//
//  BaseCollectionModel.m
//  App3.0
//
//  Created by apple on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BaseCollectionModel.h"

@implementation BaseCollectionModel


- (instancetype)init {
    if (self = [super init]) {
        

    }
    return self;
}

- (void)setIsObserve:(BOOL)isObserve {
    _isObserve = isObserve;
    if (isObserve) {
        @weakify(self);
        self.signal = [RACObserve(self, self.collectionDataSource) map:^id _Nullable(id  _Nullable value) {
            @strongify(self);
            NSLog(@"  11111-- - - - - - - -  %ld",self.collectionDataSource.count);
            return self.collectionDataSource.count == 0 ? @1 : @0;
        }];
    }
}

- (void)dealloc {
    
    NSLog(@"BaseCollectionModel   dealloc ");
}

@end
