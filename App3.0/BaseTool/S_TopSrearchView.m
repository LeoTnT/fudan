//
//  S_TopSrearchView.m
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_TopSrearchView.h"

@implementation S_TopSrearchView
{
    UIImageView *searchImg;
}


- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        UIView *searchField = [UIView new];

        self.searchField = searchField;
        searchField.frame = self.bounds;
        //搜索图片
        searchImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, (30-15)/2.0, 15, 15)];
        searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
        searchImg.userInteractionEnabled=YES;
        [searchField addSubview:searchImg];
        
        _searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(30-20)/2.0 , mainWidth-110-CGRectGetMaxX(searchImg.frame), 20)];
        _searchLabel.userInteractionEnabled=YES;
        _searchLabel.font=[UIFont systemFontOfSize:12];
        _searchLabel.textColor=mainGrayColor;
        [searchField addSubview:_searchLabel];
        [self addSubview:searchField];
        self.backgroundColor = [BaseUITool normalColor];
        self.layer.cornerRadius = frame.size.height/2;
        self.layer.masksToBounds = YES;
        
    }
    return self;
}
@end
