//
//  SRWebSocketTool.h
//  App3.0
//
//  Created by Sunny on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>
#import <zlib.h>

@interface SRWebSocketTool : NSObject<SRWebSocketDelegate>
singletonInterface(SRWebSocketTool)

@property (nonatomic,strong) RACSubject *webSocketMessage;

- (void) configWebSocketService;
- (void) closeWebSocket;
- (void) sendMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep;

- (void) sendNewMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep;
- (void) sendNewMessage:(NSString *)message key:(NSString *)key deep:(NSString *)deep isTradeView:(BOOL)isTradeView;

- (void)sendKlineRequestMessage:(nonnull NSString *)message key:(nonnull NSString *)key period:(nonnull NSString *)period fromTime:(nullable NSNumber *)from toTime:(nullable NSNumber *)to ID:(nonnull NSString *)idStr;

@end
