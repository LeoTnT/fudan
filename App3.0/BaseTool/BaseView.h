//
//  BaseView.h
//  App3.0
//
//  Created by apple on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView

- (void) setContentView;

- (void) setSubViews;

@end


@interface QuickCollectionView :BaseView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>


@property (nonatomic ,strong)UICollectionView *collectionView;
@property (nonatomic ,strong)UICollectionViewFlowLayout *flowLayout;
@property (nonatomic ,assign)CGSize cellSize;
@property (nonatomic ,strong)NSMutableArray *dataSource;
- (void)setContentView;
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath ;

@end
