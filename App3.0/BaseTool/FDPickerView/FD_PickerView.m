//
//  HYZ_AddressPickerView.m
//  HYZ_AddressPickerView
//
//  Created by 周顺 on 2017/12/18.
//  Copyright © 2017年 AIRWALK. All rights reserved.
//

#import "FD_PickerView.h"

@interface FD_PickerView ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIPickerView *pickerView;

@end

@implementation FD_PickerView

+ (instancetype)pickerView {
    FD_PickerView *pickerView = [[FD_PickerView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    pickerView.alpha = 0;
    pickerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.35];
    return pickerView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat height = 236.0;
    
    CGFloat titleViewHeight = 40;
    CGFloat buttonWidth = 70;
    CGFloat buttonHeigth = 40;

    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight, screenWidth, height)];
    contentView.backgroundColor = [UIColor colorWithRed:236 / 255.0 green:236 / 255.0 blue:236 / 255.0 alpha:1.0];
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, titleViewHeight)];
//    [UIColor colorWithRed:245 / 255.0 green:245 / 255.0 blue:245 / 255.0 alpha:1.0];
    titleView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:titleView];
    
//    UIFont *font = [UIFont systemFontOfSize:17.0f];
    CGFloat buttonOffset = 10.0f;
    CGFloat fontSize = 17.0f;
    
    //取消按钮
    CGRect leftBtnFrame = CGRectMake(buttonOffset, 0, buttonWidth, buttonHeigth);
    UIButton *leftButton = [self fd_buttonWithFrame:leftBtnFrame title:@"取消" titleColor:[UIColor hexFloatColor:@"999999"] fontSize:fontSize target:self selector:@selector(cancelButtonClick)];
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [titleView addSubview:leftButton];
    //确定按钮
    CGRect rightBtnFrame = CGRectMake(self.frame.size.width - buttonWidth - buttonOffset, 0, buttonWidth, buttonHeigth);
    UIButton *rightButton = [self fd_buttonWithFrame:rightBtnFrame title:@"确定" titleColor:mainColor fontSize:fontSize target:self selector:@selector(doneButtonClick)];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [titleView addSubview:rightButton];

    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, titleView.frame.size.height+1, screenWidth, height-40)];
//    [UIColor colorWithRed:236 / 255.0 green:236 / 255.0 blue:236 / 255.0 alpha:1.0];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [self.contentView addSubview:pickerView];
    self.pickerView = pickerView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [self addGestureRecognizer:tap];
    
    UITapGestureRecognizer *emptyTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emptyTap)];
    [self.contentView addGestureRecognizer:emptyTap];
}

- (UIButton *)fd_buttonWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor fontSize:(CGFloat)fontSize target:(id)target selector:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.adjustsImageWhenHighlighted = NO;
    button.frame = frame;
    if (title) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    if (titleColor) {
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    }
    if (fontSize > 0) {
        button.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    }
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)cancelButtonClick {
    [self dismiss];
}

- (void)doneButtonClick {
    [self dismiss];
    
    // 给外界回调
    if (self.completionHandler) {
        self.completionHandler(self.selectRow);
    }
}

- (void)tap {
    [self dismiss];
}

- (void)emptyTap {
    // 不做消失处理
}

- (void)show {
    
    self.alpha = 1.0;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    [UIView animateWithDuration:0.25 animations:^{
        
        CGRect frame = self.contentView.frame;
        frame.origin.y -= frame.size.height;
        self.contentView.frame = frame;
        
    } completion:^(BOOL finished) {
        [self refresh];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
       
        CGRect frame = self.contentView.frame;
        frame.origin.y += frame.size.height;
        self.contentView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0;
        [self removeFromSuperview];
        
    }];
}

#pragma mark - <UIPickerViewDelegate, UIPickerViewDataSource>

// 列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (self.pickerRow) {
        return self.pickerRow;
    }
    return 3;
}

// 行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    return self.dataArr.count;
}

// 显示内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    NSString *titleName = self.dataArr[row];
    return titleName;
}

// 显示内容控件
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    //设置分割线的颜色
    for (UIView *singleLine in pickerView.subviews) {
        if (singleLine.frame.size.height < 1) {
//            singleLine.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
            singleLine.backgroundColor = [UIColor colorWithRed:214 / 255.0 green:214 / 255.0 blue:214 / 255.0 alpha:1.0];
        }
    }
    
    UILabel* pickerLabel = (UILabel *)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // pickerLabel.minimumScaleFactor = 8.0;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        // [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14.0f];
        pickerLabel.textColor = [UIColor hexFloatColor:@"#333333"];
//        pickerLabel.numberOfLines = 0;
    }

    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

// 每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}

// 选中行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectRow = row;
    [pickerView selectRow:row inComponent:0 animated:NO];
}

- (void)refresh {
    if (self.pickerView) {
        if (self.selectRow) {
            [self.pickerView selectRow:self.selectRow inComponent:0 animated:YES];
        }else {
            [self.pickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

//- (void)dealloc {
//    NSLog(@"%s", __func__);
//}

@end
