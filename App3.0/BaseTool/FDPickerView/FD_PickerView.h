//
//  HYZ_AddressPickerView.h
//  HYZ_AddressPickerView
//
//  Created by 周顺 on 2017/12/18.
//  Copyright © 2017年 AIRWALK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FD_PickerView : UIView

//数据源
@property (nonatomic, strong) NSArray *dataArr;
//选中行
@property (nonatomic, assign) NSInteger selectRow;

/*选择器的列数, 默认为3
 *如需修改, 择传入列数
 */
@property(nonatomic, assign) NSInteger pickerRow;

@property (nonatomic, copy) void(^completionHandler)(NSInteger selectRow);

+ (instancetype)pickerView;

- (void)show;

@end
