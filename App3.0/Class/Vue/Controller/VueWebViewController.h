//
//  VueWebViewController.h
//  App3.0
//
//  Created by mac on 2017/11/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef NS_ENUM(NSInteger, VueType) {
    VueTypeGoodsDetailPicture = 1,//商品图文详情
    VueTypeNotice,// 公告详情
};
@interface VueWebViewController : XSBaseViewController
@property(nonatomic, copy) NSString *goodsId;
@property(nonatomic, copy) NSString *noticeId;
@property(nonatomic, assign) NSInteger vueType;
@end
