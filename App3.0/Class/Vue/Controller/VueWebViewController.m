//
//  VueWebViewController.m
//  App3.0
//
//  Created by mac on 2017/11/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VueWebViewController.h"
#import <WebKit/WebKit.h>

@interface VueWebViewController ()<WKNavigationDelegate, WKUIDelegate>
@property (nonatomic, strong) WKWebView *webView;
@end

@implementation VueWebViewController

-(WKWebView *)webView {
    if (!_webView) {
        WKUserContentController *wkUController=[[WKUserContentController alloc] init];
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        [wkUController addUserScript:wkUserScript];
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-(STATUS_HEIGHT+navBarHeight))configuration:wkWebConfig];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBarHidden = NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self getInfo];
}

- (void)getInfo {
    if (self.vueType==VueTypeNotice) {
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager getNoticeDetailInfoWithDic:@{@"id":self.noticeId} Success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                [XSTool hideProgressHUDWithView:self.view];
//                NSString *htmls = [NSString stringWithFormat:@"<style type=\"text/css\"> \n"
//                                   "img,iframe,video {height:auto; max-width:100%%; word-break:break-all;}\n"
//                                   "</style> \n"
//                                   "%@",dic[@"data"][@"content"]];
//                [self.webView loadHTMLString:htmls baseURL:[NSURL URLWithString:ImageBaseUrl]];
                
                NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
                [self.webView loadHTMLString:[headerString stringByAppendingString:[NSString stringWithFormat:@"%@",dic[@"data"][@"content"]]] baseURL:[NSURL URLWithString:ImageBaseUrl]];
                [self.view addSubview:self.webView];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }else if (self.vueType==VueTypeGoodsDetailPicture){
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager getGoodsDetailPictureInfoWithDic:@{@"product_id":self.goodsId} Success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                [XSTool hideProgressHUDWithView:self.view];
                NSString *htmls = [NSString stringWithFormat:@"<style type=\"text/css\"> \n"
                                   "img,iframe,video {height:auto; max-width:100%%; word-break:break-all;}\n"
                                   "</style> \n"
                                   "%@",dic[@"data"][@"detail"]];
                [self.webView loadHTMLString:htmls baseURL:[NSURL URLWithString:ImageBaseUrl]];
                [self.view addSubview:self.webView];
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    [XSTool showProgressHUDWithView:self.view];
}

// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    //       [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
    [XSTool hideProgressHUDWithView:self.view];
    [XSTool showToastWithView:self.view Text:NetFailure];
}

// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation
{
    
}

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSLog(@"%@",navigationResponse.response.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //    decisionHandler(WKNavigationResponsePolicyCancel);
}

// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSLog(@"%@",navigationAction.request.URL.absoluteString);
    
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
    //不允许跳转
    //    decisionHandler(WKNavigationActionPolicyCancel);
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
}


@end
