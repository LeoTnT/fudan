//
//  SkillPraiseAndCommentView.h
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkillPraiseAndCommentView : UIView
/**点赞数组*/
@property(nonatomic,strong)NSArray *praiseArray;
/**评论回复数组*/
@property(nonatomic,strong)NSArray *commentArray;
/**点赞高度*/
@property(nonatomic,assign)CGFloat praiseHeight;
/**总高度*/
@property(nonatomic,assign)CGFloat height;

@property(nonatomic,strong)NSArray *replyOrCommentViewArray;
@end
