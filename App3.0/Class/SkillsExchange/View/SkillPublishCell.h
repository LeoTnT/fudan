//
//  SkillPublishCell.h
//  App3.0
//
//  Created by mac on 2017/7/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SkillPublishCellDelegate <NSObject>
- (void)skillPublishClick;
@end

@interface SkillPublishCell : UICollectionViewCell
@property (nonatomic, weak) id<SkillPublishCellDelegate>delegate;
@end
