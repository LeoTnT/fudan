//
//  SkillPraiseAndCommentView.m
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillPraiseAndCommentView.h"
#import "SkillPraiseView.h"
#import "SkillReplyView.h"
#define  LEFTSPACE 10
#define HEADIMGHEIGHT 50
@implementation SkillPraiseAndCommentView
#pragma mark-刷新界面
-(void)setPraiseArray:(NSArray *)praiseArray{
    _praiseArray=praiseArray;
    self.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[SkillPraiseView class]]||[view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
    if (praiseArray.count==0) {
        self.praiseHeight=0;
    }else{
        SkillPraiseView *praise=[[SkillPraiseView alloc] init];
        praise.praiseArray=praiseArray;
        self.praiseHeight=praise.praiseHeight;
        praise.frame=CGRectMake(0, LEFTSPACE, mainWidth-3*LEFTSPACE-HEADIMGHEIGHT, praise.praiseHeight);
        [self addSubview:praise];
    }
    self.height=self.praiseHeight+LEFTSPACE;
}
#pragma mark-刷新界面
-(void)setCommentArray:(NSArray *)commentArray{
    _commentArray=commentArray;
    if (self.praiseArray.count&&commentArray.count) {
        //添加分割线
        UILabel *cutLine=[[UILabel alloc] initWithFrame:CGRectMake(0, self.praiseHeight+LEFTSPACE+5, mainWidth-3*LEFTSPACE-HEADIMGHEIGHT, 0.5)];
        [cutLine setBackgroundColor:LINE_COLOR];
        cutLine.alpha=0.5;
        [self addSubview:cutLine];
    }
    //     self.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
    self.backgroundColor=[UIColor hexFloatColor:@"f3f3f5"];
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[SkillReplyView class]]) {
            [view removeFromSuperview];
        }
    }
    NSMutableArray *tempArray=[NSMutableArray array];
    CGFloat tempHeight = self.praiseArray.count? self.praiseHeight+20 : self.praiseHeight;
    for (int i=0; i<commentArray.count; i++) {
        //评论
        SkillReplyView *reply=[[SkillReplyView alloc] init];
        reply.reply=[commentArray objectAtIndex:i];
        reply.frame=CGRectMake(0, tempHeight, mainWidth-3*LEFTSPACE-HEADIMGHEIGHT, reply.replyViewHeight);
        [self addSubview:reply];
        [tempArray addObject:reply];
        tempHeight+=reply.replyViewHeight;
    }
    self.height=tempHeight;
    //    self.replyOrDeleteCommentBtnArray=tempArray;
    self.replyOrCommentViewArray=tempArray;
}
@end
