//
//  SkillAddCell.h
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SkillAddCellDelegate <NSObject>
@optional
- (void)addSkill:(NSString *)skill;
- (void)addSkill:(NSString *)skill index:(NSInteger)index;
@end

@interface SkillAddCell : UICollectionViewCell
@property (nonatomic, weak) id<SkillAddCellDelegate>delegate;

// 用于一个页面多个cell的情况，判断属于哪一个section
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSString *placeholderText;
@end
