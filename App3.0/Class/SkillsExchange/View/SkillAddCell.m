//
//  SkillAddCell.m
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillAddCell.h"

@interface SkillAddCell()
@property (nonatomic, strong) XSCustomButton *add;
@property (nonatomic, strong) UITextField *skillAddTF;
@end

@implementation SkillAddCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        self.add = [[XSCustomButton alloc] initWithFrame:CGRectMake(0, 5, 55, 34) title:Localized(@"添加") titleColor:[UIColor whiteColor] fontSize:14 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.add setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
        [self.add setBorderWith:0 borderColor:[BG_COLOR CGColor] cornerRadius:3];
        [self.add addTarget:self action:@selector(addAction:) forControlEvents:UIControlEventTouchUpInside];
        self.add.enabled = NO;
        [rightView addSubview:self.add];
        // 验证码输入框
        self.skillAddTF = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.skillAddTF.placeholder = @"输入你擅长的技能";
        self.skillAddTF.font = [UIFont systemFontOfSize:14];
        self.skillAddTF.rightView = rightView;
        self.skillAddTF.rightViewMode = UITextFieldViewModeAlways;
        self.skillAddTF.borderStyle = UITextBorderStyleRoundedRect;
        [self.skillAddTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:self.skillAddTF];
    }
    return self;
}

- (void)addAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(addSkill:)]) {
        [self.delegate addSkill:self.skillAddTF.text];
    }
    if ([self.delegate respondsToSelector:@selector(addSkill:index:)]) {
        [self.delegate addSkill:self.skillAddTF.text index:self.index];
    }
    self.skillAddTF.text = nil;
}

-(void)textFieldDidChange:(UITextField *)textField
{
    CGFloat maxLength = 6;
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position || !selectedRange)
    {
        if (toBeString.length > maxLength)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1)
            {
                textField.text = [toBeString substringToIndex:maxLength];
            }
            else
            {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    if (textField.text.length) {
        self.add.enabled = YES;
    } else {
        self.add.enabled = NO;
    }
}

- (void)setPlaceholderText:(NSString *)placeholderText {
    _placeholderText = placeholderText;
    self.skillAddTF.placeholder = placeholderText;
}
@end
