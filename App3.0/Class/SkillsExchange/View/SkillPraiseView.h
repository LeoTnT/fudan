//
//  SkillPraiseView.h
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkillPraiseView : UIView
/**测试数组*/
@property(nonatomic,strong)NSArray *praiseArray;
/**展示标签*/
@property(nonatomic,strong)UILabel *praiseLabel;
/**点赞高度*/
@property(nonatomic,assign)CGFloat praiseHeight;
@end
