//
//  SkillStatusCell.h
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillPraiseAndCommentView.h"
#import "FansCircleModel.h"
#import "LoginModel.h"
#import "ReuseImageView.h"
#import "UIView+XLExtension.h"
#import "XLPhotoBrowser.h"
#import "FanCircleShareView.h"
#import "SkillModel.h"

@interface SkillStatusCell : UITableViewCell<XLPhotoBrowserDelegate, XLPhotoBrowserDatasource>
/**高度*/
@property(nonatomic,assign)CGFloat height;
/**用户头像*/
@property(nonatomic,strong)UIButton *headImgBtn;
/**昵称*/
@property(nonatomic,strong)UILabel *nickNameLabel;
/**我擅长*/
@property(nonatomic,strong)UIView *mySkillView;
@property(nonatomic,strong)UILabel *mySkillLabel;
/**我想学*/
@property(nonatomic,strong)UIView *wantSkillView;
@property(nonatomic,strong)UILabel *wantSkillLabel;
/**发表内容*/
@property(nonatomic,strong)UILabel *contentLabel;
/**评论内容*/
@property(nonatomic,strong)SkillPraiseAndCommentView *praiseAndCommentView;
/**时间*/
@property(nonatomic,strong)UILabel *timeLabel;
/**点赞评论按钮*/
@property(nonatomic,strong)UIButton *praiseAndCommentBtn;
/**点赞*/
@property(nonatomic,strong)UIButton *praiseBtn;
/**点赞评论按钮区域*/
@property(nonatomic,strong)UIView *praiseAndCommentBtnView;
/**评论*/
@property(nonatomic,strong)UIButton *commentBtn;
/**删除按钮*/
@property(nonatomic,strong)UIButton *deleteBtn;
/**播放按钮*/
@property(nonatomic,strong)UIButton *playVideoBtn;
/**动态*/
@property(nonatomic,strong)SkillExchangeModel *status;
/**当前用户*/
@property(nonatomic,strong)LoginDataParser *parser;
/**点赞评论背景图*/
@property(nonatomic,strong)UIImageView *commentAndPraiseTopView;
/**地址*/
@property(nonatomic,strong)UILabel *positionLabel;
/**视频缩略图*/
@property(nonatomic,strong)ReuseImageView *videoImageView;
/**删除或者回复评论按钮数组*/
@property(nonatomic,strong)NSArray *replyOrDeleteCommentViewArray;
/**图片网址数组*/
@property(nonatomic,strong)NSMutableArray *imageUrlArray;
/**评论点赞是否展示*/
@property(nonatomic,assign)BOOL praiseAndConmmentViewHidden;
/**点击添加评论或点赞的透明按钮*/
@property(nonatomic,strong)UIButton *clearBtn;
/**当前控制器*/
@property(nonatomic,strong) UIViewController *currentVC;
@property(nonatomic,strong)FanCircleShareView *shareView;

@end
