//
//  SkillDescTextCell.h
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SkillDescTextCellDelegate <NSObject>
@optional
- (void)SkillDescTextViewDidBeginEditing:(UITextView *)textView;
- (void)SkillDescTextViewDidChange:(UITextView *)textView;
@end

@interface SkillDescTextCell : UICollectionViewCell
@property (nonatomic, weak) id<SkillDescTextCellDelegate>delegate;
@end
