//
//  SkillCollectionCell.h
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SkillModel;
@class SkillCollectionCell;
@protocol SkillCollectionCellDelegate <NSObject>
@optional
- (void)skillDidClick:(SkillCollectionCell *)cell;
@end

@interface SkillCollectionCell : UICollectionViewCell
@property (nonatomic, weak) id<SkillCollectionCellDelegate>delegate;
@property (nonatomic, strong) SkillModel *model;

// 用于一个页面多个cell的情况，判断属于哪一个section
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSString *cellTitle;
@property (nonatomic, strong) UIButton *skillButton;
@end
