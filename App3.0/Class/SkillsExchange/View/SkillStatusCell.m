//
//  SkillStatusCell.m
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillStatusCell.h"
#import "PraiseAndCommentView.h"
#import "UUImageAvatarBrowser.h"
#import "ReuseImageView.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import <AVFoundation/AVTime.h>
#import "XSFormatterDate.h"
#import "CLPlayerView.h"

#define LEFTSPACE  10

@interface SkillStatusCell ()
@property(nonatomic,strong)UIImageView *headImageView;
@end

@implementation SkillStatusCell
#pragma mark-重写构造方法
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //头像
        self.headImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 45, 45)];
        self.headImageView.userInteractionEnabled=YES;
        self.headImageView.image=[UIImage imageNamed:@"user_fans_avatar"];
        [self.contentView addSubview:self.headImageView];
        self.headImgBtn=[[UIButton alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        [self.headImgBtn setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.headImgBtn];
        //昵称
        self.nickNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(45+2*10, 10,  (mainWidth-2*10-50), 25)];
        self.nickNameLabel.textColor=FAN_CIRCLE_COLOR;
        self.nickNameLabel.font=[UIFont boldSystemFontOfSize:14];
        self.nickNameLabel.numberOfLines=0;
        [self.contentView addSubview:self.nickNameLabel];
        
        // 我擅长
        self.mySkillView = [[UIView alloc] init];
        [self.contentView addSubview:self.mySkillView];
        self.mySkillLabel = [[UILabel alloc] init];
        self.mySkillLabel.text = @"我擅长:";
        self.mySkillLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.mySkillLabel];

        // 我想学
        self.wantSkillView = [[UIView alloc] init];
        [self.contentView addSubview:self.wantSkillView];
        self.wantSkillLabel = [[UILabel alloc] init];
        self.wantSkillLabel.text = @"我想学:";
        self.wantSkillLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.wantSkillLabel];

        
        self.deleteBtn=[[UIButton alloc] init];
        [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        self.deleteBtn.titleLabel.font=[UIFont systemFontOfSize:13];
        [self.deleteBtn setTitleColor:FAN_CIRCLE_COLOR forState:UIControlStateNormal];
        [self.contentView addSubview:self.deleteBtn];
        //内容
        self.contentLabel=[[UILabel alloc] init];
        self.contentLabel.textAlignment=NSTextAlignmentLeft;
        self.contentLabel.font=[UIFont systemFontOfSize:14];
        self.contentLabel.textColor=[UIColor blackColor];
        self.contentLabel.numberOfLines=0;
        self.contentLabel.userInteractionEnabled=YES;
        [self.contentView addSubview:self.contentLabel];
        //地点
        self.positionLabel=[[UILabel alloc] init];
        self.positionLabel.textColor=FAN_CIRCLE_COLOR;
        self.positionLabel.font=[UIFont systemFontOfSize:13];
        self.positionLabel.textAlignment=NSTextAlignmentLeft;
        [self.contentView addSubview:self.positionLabel];
        //时间
        self.timeLabel=[[UILabel alloc] init];
        self.timeLabel.textColor=mainGrayColor;
        self.timeLabel.font=[UIFont systemFontOfSize:13];
        self.timeLabel.textAlignment=NSTextAlignmentLeft;
        [self.contentView addSubview:self.timeLabel];
        //点赞评论按钮
        self.praiseAndCommentBtn=[[UIButton alloc] init];
        [self.praiseAndCommentBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_handle"]  forState:UIControlStateNormal];
        [self.praiseAndCommentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.contentView addSubview:self.praiseAndCommentBtn];
        self.clearBtn=[[UIButton alloc] init];
        [self.clearBtn setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.clearBtn];
        //点击评论按钮，出现评论或者点赞
        self.praiseAndCommentBtnView=[[UIView alloc] init];
        //        self.praiseAndCommentBtnView.backgroundColor=[UIColor colorWithRed:53/255.0 green:59/255.0 blue:61/255.0 alpha:1];
        self.praiseAndCommentBtnView.backgroundColor=[UIColor hexFloatColor:@"4c5154"];
        self.praiseAndCommentBtnView.layer.cornerRadius=5;
        [self.contentView addSubview:self.praiseAndCommentBtnView];
        //评论按钮
        self.praiseBtn=[[UIButton alloc] init];
        [self.praiseBtn setImage:[UIImage imageNamed:@"user_fans_favour"] forState:UIControlStateNormal];
        [self.praiseAndCommentBtnView addSubview:self.praiseBtn];
        self.praiseBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.praiseBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        self.commentBtn=[[UIButton alloc] init];
        [self.commentBtn setImage:[UIImage imageNamed:@"user_fans_comment"] forState:UIControlStateNormal];
        [self.commentBtn setTitle:@"  评论" forState:UIControlStateNormal];
        [self.praiseAndCommentBtnView addSubview:self.commentBtn];
        self.commentBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.commentBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        //评论点赞区域
        self.praiseAndCommentView=[[SkillPraiseAndCommentView alloc] init];
        [self.contentView addSubview:self.praiseAndCommentView];
        self.commentAndPraiseTopView=[[UIImageView alloc] init];
        self.commentAndPraiseTopView.image=[UIImage imageNamed:@"user_fans_triangle"];
        [self.contentView addSubview:self.commentAndPraiseTopView];
    }
    return self;
}
#pragma mark-刷新界面
-(void)setStatus:(SkillExchangeModel *)status{
    _status=status;
    //清界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[ReuseImageView class]]||[view isKindOfClass:[CLPlayerView class]]) {
            [view removeFromSuperview];
        }
    }
    if (self.status.is_thumbs_up == 1) {
        [self.praiseBtn setTitle:@"  取消" forState:UIControlStateNormal];
    }else{
        [self.praiseBtn setTitle:@"  赞" forState:UIControlStateNormal];
    }
    [self.headImageView getImageWithUrlStr:status.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.nickNameLabel.text=status.nickname;
    CGFloat space=10,tempHeight = 0.0;
    
    CGFloat viewHeight = 30;
    if (status.skill_goodat.count > 3) {
        viewHeight = 60;
    }
    self.mySkillLabel.frame = CGRectMake(CGRectGetMaxX(self.headImageView.frame)+space,CGRectGetMaxY(self.nickNameLabel.frame) , 60, 30);
    self.mySkillView.frame = CGRectMake(CGRectGetMaxX(self.mySkillLabel.frame),CGRectGetMaxY(self.nickNameLabel.frame) , CGRectGetWidth(self.nickNameLabel.frame)-60, viewHeight);
    for (UIView *view in self.mySkillView.subviews) {
        [view removeFromSuperview];
    }
    NSMutableArray *colorArr = [@[@"85c9ff",@"6ad7cb",@"86cf89",@"40e5ef"] mutableCopy];
    UIView *lastView = nil;
    NSInteger i = 0,j = 0;
    for (NSString *str in status.skill_goodat) {
        UILabel *label = [[UILabel alloc] init];
        label.text = [NSString stringWithFormat:@"  %@  ",str];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor hexFloatColor:colorArr[i]];
//        [colorArr removeObjectAtIndex:0];
        i == 3?i = 0:i++;
        j++;
        label.layer.masksToBounds = YES;
        label.layer.cornerRadius = 2;
        [self.mySkillView addSubview:label];
        if (j == 4) {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.mySkillView.mas_left);
                make.centerY.mas_equalTo(self.mySkillLabel.mas_centerY).offset(30);
                make.height.mas_equalTo(20);
            }];
        } else {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                lastView?make.left.mas_equalTo(lastView.mas_right).offset(10):make.left.mas_equalTo(self.mySkillView.mas_left);
                make.centerY.mas_equalTo(self.mySkillLabel.mas_centerY).offset(j > 4?30:0);
                make.height.mas_equalTo(20);
            }];
        }
        
        lastView = label;
    }
    
    viewHeight = 30;
    if (status.skill_want.count > 3) {
        viewHeight = 60;
    }
    self.wantSkillLabel.frame = CGRectMake(CGRectGetMaxX(self.headImageView.frame)+space,CGRectGetMaxY(self.mySkillView.frame) , 60, 30);
    self.wantSkillView.frame = CGRectMake(CGRectGetMaxX(self.wantSkillLabel.frame),CGRectGetMaxY(self.mySkillView.frame) , CGRectGetWidth(self.nickNameLabel.frame)-60, viewHeight);
    for (UIView *view in self.wantSkillView.subviews) {
        [view removeFromSuperview];
    }
    colorArr = [@[@"ffd38d",@"ff8dad",@"fca091",@"fcc191"] mutableCopy];
    lastView = nil;
    i = 0;
    j = 0;
    for (NSString *str in status.skill_want) {
        UILabel *label = [[UILabel alloc] init];
        label.text = [NSString stringWithFormat:@"  %@  ",str];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor whiteColor];
        label.layer.masksToBounds = YES;
        label.layer.cornerRadius = 2;
        label.backgroundColor = [UIColor hexFloatColor:colorArr[i]];
//        [colorArr removeObjectAtIndex:0];
        i == 3?i = 0:i++;
        j++;
        [self.wantSkillView addSubview:label];
        
        if (j == 4) {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.mySkillView.mas_left);
                make.centerY.mas_equalTo(self.wantSkillLabel.mas_centerY).offset(30);
                make.height.mas_equalTo(20);
            }];
        } else {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                lastView?make.left.mas_equalTo(lastView.mas_right).offset(10):make.left.mas_equalTo(self.mySkillView.mas_left);
                make.centerY.mas_equalTo(self.wantSkillLabel.mas_centerY).offset(j > 4?30:0);
                make.height.mas_equalTo(20);
            }];
        }
        lastView = label;
    }
    
    self.contentLabel.attributedText=[status.remark changeToEmojiStringWithHaveReply:NO WithFontSize:14];
    if (status.remark.length<=0) {
        self.contentLabel.frame=CGRectMake(CGRectGetMaxX(self.headImageView.frame)+space,CGRectGetMaxY(self.wantSkillView.frame)+space, CGRectGetWidth(self.nickNameLabel.frame), 0);
    }else{
        CGRect frame= [self.contentLabel.attributedText boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.contentLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        self.contentLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), CGRectGetMaxY(self.wantSkillView.frame)+space, CGRectGetWidth(self.nickNameLabel.frame),frame.size.height);
    }
    tempHeight=CGRectGetMaxY(self.contentLabel.frame);
    if (status.image.count) {
        self.imageUrlArray=[NSMutableArray array];
        CGFloat width=(CGRectGetWidth(self.contentLabel.frame)-2*5)/3.0;
        CGFloat height=width;
        UIView *tempView;
        for (int i=0; i<status.image.count; i++) {
            int line=i/3;
            int column=i%3;
            ReuseImageView *image=[[ReuseImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame)+column*(5+width), CGRectGetMaxY(self.contentLabel.frame)+space+line*width+(line+1)*5,width,height)];
            image.userInteractionEnabled=YES;
            [image getImageWithUrlStr:status.image[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            image.contentMode = UIViewContentModeScaleAspectFill;
            image.clipsToBounds=YES;//超出裁剪
            [self.contentView addSubview:image];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
            [image addGestureRecognizer:tap];
            image.tag=i;
            tempView=image;
            [self.imageUrlArray addObject:status.image[0]];
        }
        tempHeight=CGRectGetMaxY(tempView.frame);
    }
    self.timeLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",status.w_time]];
    self.positionLabel.frame=CGRectZero;
    self.timeLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), tempHeight+space,  CGRectGetWidth(self.nickNameLabel.frame), 20);
    self.deleteBtn.frame=CGRectMake(CGRectGetMinX(self.timeLabel.frame)+120, CGRectGetMinY(self.timeLabel.frame), 50, 20);
    if ([status.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        self.deleteBtn.hidden=NO;
    }else{
        self.deleteBtn.hidden=YES;
    }
    if (status.is_thumbs_up == 1) {
        [self.praiseBtn setTitle:@"  取消" forState:UIControlStateNormal];
    }else{
        [self.praiseBtn setTitle:@"  赞" forState:UIControlStateNormal];
    }
    self.praiseAndCommentBtn.frame=CGRectMake(mainWidth-10-25, CGRectGetMinY(self.timeLabel.frame)-1, 25, 25);
    self.clearBtn.frame=CGRectMake(mainWidth-50, CGRectGetMinY(self.timeLabel.frame)-12.5, 50, 50);
    self.praiseAndCommentBtnView.frame=CGRectMake(mainWidth-2*10-22,CGRectGetMinY(self.timeLabel.frame)-7.5, 0, 40);
    tempHeight=CGRectGetMaxY(self.praiseAndCommentBtnView.frame);
    //评论或者点赞按钮
    self.praiseBtn.frame=CGRectMake(0, 0, 0, 25);
    self.commentBtn.frame=CGRectMake(60, 0, 0, 25);
    
    //评论点赞区域
    self.praiseAndCommentView.praiseArray=status.thumbsup;
    self.praiseAndCommentView.commentArray=status.comment;
    self.praiseAndCommentView.frame=CGRectMake(CGRectGetMinX(self.contentLabel.frame),tempHeight+10 ,CGRectGetWidth(self.contentLabel.frame), self.praiseAndCommentView.height);
    if (self.status.thumbsup.count||self.status.comment.count) {
        self.commentAndPraiseTopView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentView.frame)+10, CGRectGetMinY(self.praiseAndCommentView.frame)-5, 10, 5);
        self.height=CGRectGetMaxY(self.praiseAndCommentView.frame)+10;
    }else{
        self.commentAndPraiseTopView.frame=CGRectZero;
        self.praiseAndCommentView.frame=CGRectZero;
        self.height=tempHeight+10;
    }
    self.replyOrDeleteCommentViewArray=self.praiseAndCommentView.replyOrCommentViewArray;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark-放大图片
-(void)enLarge:(UITapGestureRecognizer *)tap{
    NSInteger imageTag = tap.view.tag;
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:self.status.image currentImageIndex:imageTag pageControlHidden:YES];
    [browser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    browser.isCusctomAdd = YES;
    browser.delegate = self;
    [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
    [self.currentVC.tabBarController.view addSubview:browser];
}
-(void)photoBrowserDismiss{
    [self.currentVC.navigationController setNavigationBarHidden:NO animated:NO];
//    [self.currentVC.tabBarController.tabBar setHidden:NO];
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}
-(void)setPraiseAndConmmentViewHidden:(BOOL)praiseAndConmmentViewHidden{
    _praiseAndConmmentViewHidden=praiseAndConmmentViewHidden;
    if (praiseAndConmmentViewHidden) {
        self.praiseAndCommentBtnView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentBtn.frame)-10, CGRectGetMinY(self.praiseAndCommentBtn.frame)-5, 0, 35);
        self.praiseBtn.frame=CGRectMake(0, 0, 0, 35);
        self.commentBtn.frame=CGRectMake(75, 0, 0, 35);
        self.praiseAndCommentBtn.selected=NO;
    }else{
        self.praiseAndCommentBtnView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentBtn.frame)-10-150, CGRectGetMinY(self.praiseAndCommentBtn.frame)-5, 150, 35);
        self.praiseBtn.frame=CGRectMake(0, 0, 150/2.0, 35);
        self.commentBtn.frame=CGRectMake(75, 0, 150/2.0, 35);
        self.praiseAndCommentBtn.selected=YES;
    }
}
@end
