//
//  SkillReplyView.m
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillReplyView.h"
#import "XSFormatterDate.h"

@implementation SkillReplyView
#pragma mark-重写构造方法
-(instancetype)init{
    if (self=[super init]) {
        //评论内容
        self.nickNameAndContentLabel=[[UILabel alloc] init];
        self.nickNameAndContentLabel.textAlignment=NSTextAlignmentLeft;
        self.nickNameAndContentLabel.textColor=[UIColor blackColor];
        self.nickNameAndContentLabel.numberOfLines=0;
        self.nickNameAndContentLabel.font=[UIFont systemFontOfSize:13];
        [self addSubview:self.nickNameAndContentLabel];
    }
    return self;
}
#pragma mark-测试数组
-(void)setReply:(SkillReplayModel *)reply{
    _reply=reply;
    //判断是评论还是回复
    if (reply.to_username.length==0) {//评论
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@：%@",reply.from_nickname,reply.content]];
        [attri addAttribute:NSForegroundColorAttributeName value:FAN_CIRCLE_COLOR range:NSMakeRange(0, reply.from_nickname.length)];
        self.nickNameAndContentLabel.attributedText=attri;
        
    }else{
        NSString *str1=reply.from_nickname;
        long length1=str1.length;
        NSString *str2=[str1 stringByAppendingString:@"回复"];
        long length2=str2.length;
        NSString *str3=[str2 stringByAppendingString:reply.to_nickname];
        long length3=reply.to_nickname.length;
        NSString *str4=[str3 stringByAppendingString:@":"];
        NSString *str5=[str4 stringByAppendingString:reply.content];
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:str5];
        [attri addAttribute:NSForegroundColorAttributeName value:FAN_CIRCLE_COLOR range:NSMakeRange(0, length1)];
        [attri addAttribute:NSForegroundColorAttributeName value:FAN_CIRCLE_COLOR range:NSMakeRange(length2, length3)];
        self.nickNameAndContentLabel.attributedText=attri;
    }
    //根据昵称内容获取行高
    NSDictionary * dic1=@{NSFontAttributeName:[UIFont systemFontOfSize:13]};
    CGRect frame1= [self.nickNameAndContentLabel.text boundingRectWithSize:CGSizeMake(self.frame.size.width, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic1 context:nil];
    self.nickNameAndContentLabel.frame=CGRectMake(5,5, frame1.size.width, frame1.size.height);
    self.replyViewHeight=CGRectGetMaxY(self.nickNameAndContentLabel.frame)+5;
}
@end
