//
//  SkillPublishCell.m
//  App3.0
//
//  Created by mac on 2017/7/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillPublishCell.h"

@interface SkillPublishCell()
@property (nonatomic, strong) UIButton *skillPublish;
@end
@implementation SkillPublishCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.skillPublish = [UIButton buttonWithType:UIButtonTypeCustom];
        self.skillPublish.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.skillPublish setTitle:Localized(@"exchange_skill_pulish") forState:UIControlStateNormal];
        [self.skillPublish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.skillPublish setBackgroundColor:SKILL_COLOR];
        [self.skillPublish addTarget:self action:@selector(skillPublishAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.skillPublish];
    }
    return self;
}

- (void)skillPublishAction {
    if ([self.delegate respondsToSelector:@selector(skillPublishClick)]) {
        [self.delegate skillPublishClick];
    }
}
@end
