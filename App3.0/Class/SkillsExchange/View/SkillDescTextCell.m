//
//  SkillDescTextCell.m
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillDescTextCell.h"

@interface SkillDescTextCell() <UITextViewDelegate>
@property (nonatomic, strong) UITextView *textView;
@end

@implementation SkillDescTextCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.textView.text = @"请输入技能描述，清晰准确的描述有助于用户了解你的服务（不少于10字）";
        self.textView.textColor = [UIColor hexFloatColor:@"c8c8c8"];
        self.textView.layer.masksToBounds = YES;
        self.textView.layer.borderColor = [LINE_COLOR CGColor];
        self.textView.layer.borderWidth = 0.5f;
        self.textView.layer.cornerRadius = 2;
        self.textView.delegate = self;
        [self addSubview:self.textView];
    }
    return self;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(SkillDescTextViewDidBeginEditing:)]) {
        [self.delegate SkillDescTextViewDidBeginEditing:textView];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(SkillDescTextViewDidChange:)]) {
        [self.delegate SkillDescTextViewDidChange:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length == 0) {
        textView.text = @"请输入技能描述，清晰准确的描述有助于用户了解你的服务（不少于10字）";
        self.textView.textColor = [UIColor hexFloatColor:@"c8c8c8"];
    }
}
@end
