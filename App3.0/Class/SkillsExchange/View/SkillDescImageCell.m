//
//  SkillDescImageCell.m
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillDescImageCell.h"

@interface SkillDescImageCell()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *deleteBtn;
@end

@implementation SkillDescImageCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.imageView.image = [UIImage imageNamed:@"no_pic"];
        [self addSubview:self.imageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClick:)];
        [self addGestureRecognizer:tap];
        
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.deleteBtn setBackgroundImage:[UIImage imageNamed:@"skill_image_delete"] forState:UIControlStateNormal];
        [self.deleteBtn addTarget:self action:@selector(deleteImageAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.deleteBtn];
        
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.imageView.mas_right);
            make.centerY.mas_equalTo(self.imageView.mas_top);
        }];
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.imageView.image = image;
}

- (void)imageViewClick:(UITapGestureRecognizer *)sender {
    if ([self.delegate respondsToSelector:@selector(skillImageDidClick:)]) {
        [self.delegate skillImageDidClick:self.index];
    }
}

- (void)deleteImageAction {
    if ([self.delegate respondsToSelector:@selector(skillImageDeleteDidClick:)]) {
        [self.delegate skillImageDeleteDidClick:self.index];
    }
}

- (void)setShowDeleteBtn:(BOOL)showDeleteBtn {
    _showDeleteBtn = showDeleteBtn;
    self.deleteBtn.hidden = !showDeleteBtn;
}
@end
