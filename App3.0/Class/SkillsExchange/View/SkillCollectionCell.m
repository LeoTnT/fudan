//
//  SkillCollectionCell.m
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillCollectionCell.h"
#import "SkillModel.h"

@interface SkillCollectionCell()

@end

@implementation SkillCollectionCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.skillButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.skillButton.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.skillButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [self.skillButton addTarget:self action:@selector(skillClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.skillButton];
    }
    return self;
}

- (void)setModel:(SkillModel *)model {
    _model = model;
    self.skillButton.selected = model.selected;
    if ([model.skill_name isEqualToString:@"add_skill"]) {
        [self.skillButton setTitle:nil forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_add"] forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_add"] forState:UIControlStateSelected];
    } else {
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_normal"] forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_select"] forState:UIControlStateSelected];
        [self.skillButton setTitle:model.skill_name forState:UIControlStateNormal];
        [self.skillButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.skillButton setTitleColor:SKILL_COLOR forState:UIControlStateSelected];
    }
}

- (void)setCellTitle:(NSString *)cellTitle {
    _cellTitle = cellTitle;
    if ([cellTitle isEqualToString:@"add_skill"]) {
        [self.skillButton setTitle:nil forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_add"] forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_add"] forState:UIControlStateSelected];
    } else {
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_normal"] forState:UIControlStateNormal];
        [self.skillButton setBackgroundImage:[UIImage imageNamed:@"skill_btn_select"] forState:UIControlStateSelected];
        [self.skillButton setTitle:cellTitle forState:UIControlStateNormal];
        [self.skillButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.skillButton setTitleColor:SKILL_COLOR forState:UIControlStateSelected];
    }
}

- (void)skillClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.model.selected = sender.selected;
    if (isEmptyString([sender titleForState:UIControlStateNormal])) {
        if ([self.delegate respondsToSelector:@selector(skillDidClick:)]) {
            [self.delegate skillDidClick:self];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(skillDidClick:)]) {
            [self.delegate skillDidClick:self];
        }
    }
}

@end
