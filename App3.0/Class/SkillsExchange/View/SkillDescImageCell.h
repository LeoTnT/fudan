//
//  SkillDescImageCell.h
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SkillDescImageCellDelegate <NSObject>
@optional
- (void)skillImageDidClick:(NSInteger)index;
- (void)skillImageDeleteDidClick:(NSInteger)index;
@end

@interface SkillDescImageCell : UICollectionViewCell
@property (nonatomic, weak) id<SkillDescImageCellDelegate>delegate;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL showDeleteBtn;
@end
