//
//  SkillReplyView.h
//  App3.0
//
//  Created by mac on 2017/7/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillModel.h"
@interface SkillReplyView : UIView
/**昵称*/
@property(nonatomic,strong)UILabel *nickName;
/**分割线*/
@property(nonatomic,strong)UIView *lineView;
/**昵称  评论内容*/
@property(nonatomic,strong)UILabel *nickNameAndContentLabel;
/**一条回复的高度*/
@property(nonatomic,assign)CGFloat replyViewHeight;
/**测试数组*/
@property(nonatomic,strong) SkillReplayModel *reply;
@end
