//
//  MySkillViewController.m
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MySkillViewController.h"
#import "SkillCollectionCell.h"
#import "SkillAddCell.h"
#import "SkillModel.h"

@interface MySkillViewController () <UICollectionViewDelegate, UICollectionViewDataSource, SkillCollectionCellDelegate, SkillAddCellDelegate>
@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) NSMutableArray *titles;
@property (nonatomic, strong) NSMutableArray *selectedSkills;
@property (nonatomic, assign) BOOL showAddCell;
@end

@implementation MySkillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"我的技能");
    
    self.showAddCell = NO;
//    NSArray *names = @[@"摄影",@"做饭",@"跆拳道",@"弹钢琴",@"PS设计",@"篮球",@"开车",@"游泳",@"英语",@"吉他",@"炒股",@"add_skill"];
//    self.titles = [NSMutableArray array];
//    for (NSString *name in names) {
//        SkillModel *model = [SkillModel new];
//        model.skill_name = name;
//        model.selected = NO;
//        [self.titles addObject:model];
//    }
    [self setSubviews];
    [self getSkillList];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"save") action:^{
        [XSTool showProgressHUDWithView:self.view];
        NSMutableString *skill_str = [NSMutableString string];
        for (SkillModel *model in self.selectedSkills) {
            if (isEmptyString(skill_str)) {
                [skill_str appendString:model.skill_name];
            } else {
                [skill_str appendString:[NSString stringWithFormat:@",%@",model.skill_name]];
            }
            
        }
        [HTTPManager setMySkills:skill_str success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
            } else {
                NSLog(@"%@",state.info);
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }];
}

- (void)setSubviews {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //    layout.itemSize = CGSizeMake((mainWidth-50)/4, 44);
    //    //同一行相邻两个cell的最小间距
    layout.minimumInteritemSpacing = 5;
    //最小两行之间的间距
    layout.minimumLineSpacing = 10;
    self.collectView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    [self.collectView registerClass:[SkillCollectionCell class] forCellWithReuseIdentifier:@"skillCell"];
    [self.collectView registerClass:[SkillAddCell class] forCellWithReuseIdentifier:@"skillAddCell"];
    self.collectView.backgroundColor = [UIColor whiteColor];
    self.collectView.delegate = self;
    self.collectView.dataSource = self;
    self.collectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.collectView];
}

- (void)getSkillList {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getSkillListWithSuccess:^(NSDictionary *dic, resultObject *state) {
//        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            SkillListModel *model = [SkillListModel mj_objectWithKeyValues:dic];
            [self.titles removeAllObjects];
            [self.titles addObjectsFromArray:model.data];
            
            SkillModel *add = [SkillModel new];
            add.skill_name = @"add_skill";
            add.selected = NO;
            [self.titles addObject:add];
            
            [HTTPManager getMySkillListWithSuccess:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    SkillListModel *model = [SkillListModel mj_objectWithKeyValues:dic];
                    for (SkillModel *sm in model.data) {
                        if (![self.titles containsObject:sm]) {
                            [self.titles insertObject:sm atIndex:self.titles.count-1];
                        }
                    }
                    for (SkillModel *sm in self.titles) {
                        if ([model.data containsObject:sm]) {
                            sm.selected = YES;
                            [self.selectedSkills addObject:sm];
                        }
                    }
                    [self.collectView reloadData];
                }
            } fail:^(NSError *error) {
                
            }];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)selectedSkills {
    if (!_selectedSkills) {
        _selectedSkills = [NSMutableArray array];
    }
    return _selectedSkills;
}

- (NSMutableArray *)titles {
    if (!_titles) {
        _titles = [NSMutableArray array];
    }
    return _titles;
}

- (void)skillDidClick:(SkillCollectionCell *)cell {
    SkillModel *model = cell.model;
    if ([model.skill_name isEqualToString:@"add_skill"]) {
        self.showAddCell = model.selected;
        [self.collectView reloadData];
    } else {
        if (model.selected) {
            [self.selectedSkills addObject:model];
        } else {
            [self.selectedSkills removeObject:model];
        }
        NSLog(@"%@",self.selectedSkills);
    }
}

- (void)addSkill:(NSString *)skill {
    if (isEmptyString(skill)) {
        [XSTool showToastWithView:self.view Text:@"请输入技能名称"];
        return;
    }
    SkillModel *model = [SkillModel new];
    model.skill_name = skill;
    model.selected = YES;
    [self.titles insertObject:model atIndex:self.titles.count-1];
    [self.selectedSkills addObject:model];
    [self.collectView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.showAddCell?2:1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return self.titles.count;
    }
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SkillCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillCell" forIndexPath:indexPath];
        cell.model = self.titles[indexPath.row];
        cell.delegate = self;
        return cell;
    }
    
    SkillAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillAddCell" forIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}

//每一个分组的上左下右间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 5, 10, 5);
}
//
//定义每一个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(80, 44);
    }
    return CGSizeMake(mainWidth-20, 44);
}

//cell的点击事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //cell被点击后移动的动画
    [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
}
@end
