//
//  SkillPublishViewController.m
//  App3.0
//
//  Created by mac on 2017/7/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillPublishViewController.h"
#import "SkillCollectionCell.h"
#import "SkillAddCell.h"
#import "SkillModel.h"
#import "SkillDescTextCell.h"
#import "SkillDescImageCell.h"
#import "SkillPublishCell.h"
#import "UIAlertView+XSAlertView.h"
#import "ZYQAssetPickerController.h"


@interface SkillPublishViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, SkillCollectionCellDelegate, SkillAddCellDelegate, SkillDescTextCellDelegate, SkillDescImageCellDelegate, SkillPublishCellDelegate, ZYQAssetPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) NSMutableArray *mySkills;             // 我擅长的技能列表
@property (nonatomic, strong) NSMutableArray *selectedMySkills;     // 我选中的擅长技能
@property (nonatomic, strong) NSMutableArray *wantSkills;           // 我想学的技能列表
@property (nonatomic, strong) NSMutableArray *selectedWantSkills;   // 我选中的想学技能
@property (nonatomic, strong) NSMutableArray *skillImages;          // 描述图片集合
@property (nonatomic, assign) BOOL showMyAddCell;
@property (nonatomic, assign) BOOL showWantAddCell;
@property (nonatomic, copy) NSString *skillDescString;


/**拍照控制器*/
@property(nonatomic,strong)UIImagePickerController *imagePickVC;
@end

@implementation SkillPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我要交换";
    self.autoHideKeyboard = YES;
    
    self.showMyAddCell = NO;
    self.showWantAddCell = NO;
    
    [self setSubviews];
    [self getSkillList];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"cancel_btn") action:^{
        
    }];
}

- (void)setSubviews {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //    layout.itemSize = CGSizeMake((mainWidth-50)/4, 44);
    //    //同一行相邻两个cell的最小间距
    layout.minimumInteritemSpacing = 5;
    //最小两行之间的间距
    layout.minimumLineSpacing = 10;
    self.collectView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    [self.collectView registerClass:[SkillCollectionCell class] forCellWithReuseIdentifier:@"skillCell"];
    [self.collectView registerClass:[SkillAddCell class] forCellWithReuseIdentifier:@"skillAddCell"];
    [self.collectView registerClass:[SkillDescTextCell class] forCellWithReuseIdentifier:@"skillDescTextCell"];
    [self.collectView registerClass:[SkillDescImageCell class] forCellWithReuseIdentifier:@"skillDescImageCell"];
    [self.collectView registerClass:[SkillPublishCell class] forCellWithReuseIdentifier:@"skillPublishCell"];
    [self.collectView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
    [self.collectView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"foot"];
    self.collectView.backgroundColor = [UIColor whiteColor];
    self.collectView.delegate = self;
    self.collectView.dataSource = self;
    self.collectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.collectView];
}

- (void)getSkillList {
    [HTTPManager getMySkillListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            SkillListModel *model = [SkillListModel mj_objectWithKeyValues:dic];
            [self.mySkills removeAllObjects];
            [self.mySkills addObjectsFromArray:model.data];
            
            SkillModel *add = [SkillModel new];
            add.skill_name = @"add_skill";
            add.selected = NO;
            [self.mySkills addObject:add];
            
            [self.collectView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
    
    [HTTPManager getSkillListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            SkillListModel *model = [SkillListModel mj_objectWithKeyValues:dic];
            [self.wantSkills removeAllObjects];
            [self.wantSkills addObjectsFromArray:model.data];
            
            SkillModel *add = [SkillModel new];
            add.skill_name = @"add_skill";
            add.selected = NO;
            [self.wantSkills addObject:add];
            
            [self.collectView reloadData];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)mySkills {
    if (!_mySkills) {
        _mySkills = [NSMutableArray array];
    }
    return _mySkills;
}

- (NSMutableArray *)wantSkills {
    if (!_wantSkills) {
        _wantSkills = [NSMutableArray array];
    }
    return _wantSkills;
}

- (NSMutableArray *)selectedMySkills {
    if (!_selectedMySkills) {
        _selectedMySkills = [NSMutableArray array];
    }
    return _selectedMySkills;
}

- (NSMutableArray *)selectedWantSkills {
    if (!_selectedWantSkills) {
        _selectedWantSkills = [NSMutableArray array];
    }
    return _selectedWantSkills;
}

- (NSMutableArray *)skillImages {
    if (!_skillImages) {
        _skillImages = [NSMutableArray array];
    }
    return _skillImages;
}

- (void)popView {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSkillCircle" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SkillPublishCellDelegate
- (void)skillPublishClick {
    if (self.selectedMySkills.count == 0) {
        [XSTool showToastWithView:self.view Text:@"请选择擅长的技能"];
        return;
    }
    if (self.selectedWantSkills.count == 0) {
        [XSTool showToastWithView:self.view Text:@"请选择想学的技能"];
        return;
    }
    [XSTool showProgressHUDTOView:self.view withText:@"发布中..."];
    NSMutableString *gtStr=[NSMutableString string];
    for (SkillModel *model in self.selectedMySkills) {
        [gtStr appendString:[NSString stringWithFormat:@"%@,",model.skill_name]];
    }
    NSMutableString *wtStr=[NSMutableString string];
    for (SkillModel *model in self.selectedWantSkills) {
        [wtStr appendString:[NSString stringWithFormat:@"%@,",model.skill_name]];
    }
    if (self.skillImages.count == 0) {
        NSDictionary *param = @{@"skill_goodat_str":[gtStr substringToIndex:gtStr.length-1],@"skill_want_str":[wtStr substringToIndex:wtStr.length-1],@"remark":self.skillDescString?self.skillDescString:@""};
        [HTTPManager addSkillExchangeWithData:param success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:Localized(@"add_skill_publish_successs")];
                [self performSelector:@selector(popView) withObject:nil afterDelay:0.5f];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        NSDictionary *uploadParam=@{@"type":@"article",@"formname":@"file"};        
        @weakify(self);
        [HTTPManager upLoadPhotosWithDic:uploadParam andDataArray:self.skillImages WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
            @strongify(self);
            if (state) {
                NSString *imgStr = [dic[@"data"] componentsJoinedByString:@","];
                NSDictionary *param = @{@"skill_goodat_str":[gtStr substringToIndex:gtStr.length-1],@"skill_want_str":[wtStr substringToIndex:wtStr.length-1],@"img_str":imgStr,@"remark":self.skillDescString?self.skillDescString:@""};
                [HTTPManager addSkillExchangeWithData:param success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [XSTool showToastWithView:self.view Text:Localized(@"add_skill_publish_successs")];
                        [self performSelector:@selector(popView) withObject:nil afterDelay:0.5f];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            }
        } fail:^(NSError * _Nonnull error) {
            
        }];
    }
    
    
    
}

#pragma mark - SkillCollectionCellDelegate
- (void)skillDidClick:(SkillCollectionCell *)cell {
    SkillModel *model = cell.model;
    NSInteger index = cell.index;
    if (index == 0) {
        // 我擅长的
        if ([model.skill_name isEqualToString:@"add_skill"]) {
            self.showMyAddCell = model.selected;
            [self.collectView reloadData];
        } else {
            if (model.selected) {
                if (self.selectedMySkills.count >= 4) {
                    [XSTool showToastWithView:self.view Text:@"最多选择4个"];
                    cell.skillButton.selected = NO;
                    model.selected = NO;
                } else {
                    [self.selectedMySkills addObject:model];
                }
                
            } else {
                [self.selectedMySkills removeObject:model];
            }
        }
    } else {
        // 我想学的
        if ([model.skill_name isEqualToString:@"add_skill"]) {
            self.showWantAddCell = model.selected;
            [self.collectView reloadData];
        } else {
            if (model.selected) {
                if (self.selectedWantSkills.count >= 4) {
                    [XSTool showToastWithView:self.view Text:@"最多选择4个"];
                    cell.skillButton.selected = NO;
                    model.selected = NO;
                } else {
                    [self.selectedWantSkills addObject:model];
                }
                
            } else {
                [self.selectedWantSkills removeObject:model];
            }
        }
    }
    
}

#pragma mark - SkillAddCellDelegate
- (void)addSkill:(NSString *)skill index:(NSInteger)index{
    if (isEmptyString(skill)) {
        [XSTool showToastWithView:self.view Text:@"请输入技能名称"];
        return;
    }
    if (index == 1) {
        // 我擅长的
        if (self.selectedMySkills.count >= 4) {
            [XSTool showToastWithView:self.view Text:@"最多选择4个"];
        } else {
            SkillModel *model = [SkillModel new];
            model.skill_name = skill;
            model.selected = YES;
            [self.mySkills insertObject:model atIndex:self.mySkills.count-1];
            [self.selectedMySkills addObject:model];
            [self.collectView reloadData];
        }
        
    } else {
        // 我想学的
        if (self.selectedWantSkills.count >= 4) {
            [XSTool showToastWithView:self.view Text:@"最多选择4个"];
        } else {
            SkillModel *model = [SkillModel new];
            model.skill_name = skill;
            model.selected = YES;
            [self.wantSkills insertObject:model atIndex:self.wantSkills.count-1];
            [self.selectedWantSkills addObject:model];
            [self.collectView reloadData];
        }
        
    }
    
}

#pragma mark - SkillDescTextCellDelegate
- (void)SkillDescTextViewDidBeginEditing:(UITextView *)textView {
    
    textView.text = self.skillDescString;
    textView.textColor = [UIColor blackColor];
}

- (void)SkillDescTextViewDidChange:(UITextView *)textView {
    self.skillDescString = textView.text;
}

#pragma mark - SkillDescImageCellDelegate
- (void)skillImageDidClick:(NSInteger)index {
    if (index >= self.skillImages.count) {
        [self choosePhotos];
    } else {
        [self.skillImages removeObjectAtIndex:index];
        [self.collectView reloadData];
    }
}

- (void)skillImageDeleteDidClick:(NSInteger)index {
    
}

#pragma mark-ScrollView Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return self.mySkills.count;
    } else if (section == (self.showMyAddCell?2:1)) {
        return self.wantSkills.count;
    } else if (section == 3 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        if (self.skillImages.count > 0 && self.skillImages.count < 3) {
            return self.skillImages.count+1;
        } else if (self.skillImages.count == 3) {
            return self.skillImages.count;
        }
        return 1;
    }
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SkillCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillCell" forIndexPath:indexPath];
        cell.model = self.mySkills[indexPath.row];
        cell.delegate = self;
        cell.index = indexPath.section;
        return cell;
    } else if (indexPath.section == (self.showMyAddCell?2:1)) {
        SkillCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillCell" forIndexPath:indexPath];
        cell.model = self.wantSkills[indexPath.row];
        cell.delegate = self;
        cell.index = indexPath.section;
        return cell;
    } else if (indexPath.section == 2 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        SkillDescTextCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillDescTextCell" forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    } else if (indexPath.section == 3 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        SkillDescImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillDescImageCell" forIndexPath:indexPath];
        if (self.skillImages.count < 3) {
            if (indexPath.row < self.skillImages.count) {
                cell.image = self.skillImages[indexPath.row];
                cell.showDeleteBtn = YES;
            } else {
                cell.image = [UIImage imageNamed:@"skill_image_add"];
                cell.showDeleteBtn = NO;
            }
        } else {
            cell.image = self.skillImages[indexPath.row];
            cell.showDeleteBtn = YES;
        }
        
        cell.index = indexPath.row;
        cell.delegate = self;
        return cell;
    } else if (indexPath.section == 4 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        SkillPublishCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillPublishCell" forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }
    
    SkillAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skillAddCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.index = indexPath.section;
    if (indexPath.section == (self.showMyAddCell?3:2)) {
        cell.placeholderText = @"输入你想学的技能";
    }
    return cell;
}

//每一个分组的上左下右间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
//
//定义每一个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 || indexPath.section == (self.showMyAddCell?2:1)) {
        return CGSizeMake(80, 44);
    } else if (indexPath.section == 2 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        return CGSizeMake(mainWidth-20, 100);
    }
    else if (indexPath.section == 3 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        return CGSizeMake((mainWidth-40)/3, (mainWidth-40)/3);
    }
    return CGSizeMake(mainWidth-20, 44);
}

//cell的点击事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //cell被点击后移动的动画
    [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
}

// 和UITableView类似，UICollectionView也可设置段头段尾
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 || indexPath.section == (self.showMyAddCell?2:1) || indexPath.section == 2 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        if([kind isEqualToString:UICollectionElementKindSectionHeader])
        {
            UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"head" forIndexPath:indexPath];
            if(headerView == nil)
            {
                headerView = [[UICollectionReusableView alloc] init];
            }
            if ([headerView subviews].count == 0) {
                UILabel *title = [[UILabel alloc] init];
                if (indexPath.section == 0) {
                    title.text = @"我擅长";
                } else if (indexPath.section == (self.showMyAddCell?2:1)) {
                    title.text = @"我想学";
                } else {
                    title.text = @"描述";
                }
                
                title.textColor = [UIColor blackColor];
                title.font = [UIFont systemFontOfSize:16];
                [headerView addSubview:title];
                
                UIView *line = [[UIView alloc] init];
                line.backgroundColor = LINE_COLOR;
                [headerView addSubview:line];
                
                [title mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(headerView.mas_left).offset(10);
                    make.right.mas_equalTo(headerView.mas_right).offset(-10);
                    make.centerY.mas_equalTo(headerView.mas_centerY);
                }];
                
                [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(title);
                    make.top.mas_equalTo(headerView.mas_bottom).offset(-0.5);
                    make.bottom.mas_equalTo(headerView.mas_bottom);
                }];
            } else {
                for (UIView *view in headerView.subviews) {
                    if ([view isKindOfClass:[UILabel class]]) {
                        UILabel *title = (UILabel *)view;
                        if (indexPath.section == 0) {
                            title.text = @"我擅长";
                        } else if (indexPath.section == (self.showMyAddCell?2:1)) {
                            title.text = @"我想学";
                        } else {
                            title.text = @"描述";
                        }
                    }
                }
                
            }
            return headerView;
        }
        else if([kind isEqualToString:UICollectionElementKindSectionFooter])
        {
            UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"foot" forIndexPath:indexPath];
            if(footerView == nil)
            {
                footerView = [[UICollectionReusableView alloc] init];
            }
            footerView.backgroundColor = [UIColor lightGrayColor];
            
            return footerView;
        }
    }
    
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == (self.showMyAddCell?2:1) || section == 2 + (self.showMyAddCell?1:0) + (self.showWantAddCell?1:0)) {
        return CGSizeMake(mainWidth, 44);
    }
    return CGSizeMake(mainWidth, 0);
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(mainWidth, 0);
}

#pragma mark-选择照片
-(void)choosePhotos{
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cameraAct=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 3-self.skillImages.count;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *photoAct=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cameraAct];
    [alertVC addAction:photoAct];
    [alertVC addAction:cancelAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark-拍照
-(void)takeAPhoto{
    //拍照模式是否可用
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.imagePickVC=[[UIImagePickerController alloc] init];
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate = self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    //保存图片到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [self.skillImages addObject:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    //刷新表格
    [self.collectView reloadData];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        [asset setGetFullScreenImage:^(UIImage * result) {
            [self.skillImages addObject:result];
            [self.collectView reloadData];
        }];
    }
}
-(void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker{
    [XSTool showToastWithView:picker.view Text:@"最多选择三张图片"];
}
@end
