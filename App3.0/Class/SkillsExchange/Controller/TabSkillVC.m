//
//  TabSkillVC.m
//  App3.0
//
//  Created by mac on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabSkillVC.h"
#import "SkillModel.h"
#import "ChatKeyBoard.h"
#import "SkillStatusCell.h"
#import "PersonalViewController.h"
#import "SkillReplyView.h"
#import "SkillPraiseView.h"
#import "SkillPublishViewController.h"
#import "SearchVC.h"

@interface TabSkillVC () <ChatKeyBoardDelegate>
/**记录点击过评论点赞的cell*/
@property(nonatomic,strong)NSMutableArray *praiseAndCommentBtnArray;

@property (strong, nonatomic) ChatKeyBoard *keyBoard;
/**动态数组*/
@property(nonatomic,strong)NSMutableArray  *statusArray;
/**保存cell高度的字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
/**记录被点击的评论点赞的cell*/
@property(nonatomic,strong)SkillStatusCell *tempCell;
/**评论或者回复的某条动态*/
@property(nonatomic,strong)SkillStatusCell *replyOrCommentCell;
/**评论还是回复*/
@property(nonatomic,assign)BOOL commentOrNot;
/**动态编号*/
@property(nonatomic,strong)NSString *topicId;
/**评论编号*/
@property(nonatomic,strong)NSString *replyId;
/**用户信息*/
@property(nonatomic,strong)LoginDataParser *loginDataParser;
/**某条回复*/
@property(nonatomic,strong)SkillReplayModel *reply;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面

@property (nonatomic, strong) UIView *searchField;
@property (nonatomic, strong) GetSkillModel *model;
@property (nonatomic, strong) UILabel *tableFooterView;
@end

@implementation TabSkillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationItem.title = Localized(@"技能交换");
    self.autoHideKeyboard = YES;
    self.model = [GetSkillModel new];
    self.model.page = 1;
    [self setNavBarView];
    [self setSubviews];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"我要交换" action:^{
        SkillPublishViewController *sp = [[SkillPublishViewController alloc] init];
        [self.navigationController pushViewController:sp animated:YES];
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"refreshSkillCircle" object:nil];
    //加载数据
    [self.tableView.mj_header beginRefreshing];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    self.searchField.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    self.searchField.hidden = NO;
}

- (void)dealloc {
    
    
}

- (void)setNavBarView {
    //输入搜索
    self.searchField = [[UIView alloc] initWithFrame:CGRectMake(40, 7, mainWidth-130, 30)];
    self.searchField.backgroundColor = [UIColor whiteColor];
    self.searchField.layer.cornerRadius = 15;
    self.searchField.layer.masksToBounds = YES;
    
    //添加手势搜索
    [self.searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchSkills)]];
    //    self.navigationItem.titleView = self.searchField;
    [self.navigationController.navigationBar addSubview:self.searchField];
    
    //搜索图片
    UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(10, (30-15)/2.0, 15, 15)];
    searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
    searchImg.userInteractionEnabled=YES;
    [self.searchField addSubview:searchImg];
    
    //搜索文字
    UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(30-20)/2.0 , mainWidth-110-CGRectGetMaxX(searchImg.frame), 20)];
    searchLabel.userInteractionEnabled=YES;
    searchLabel.text=@"输入搜索内容";
    searchLabel.font=[UIFont systemFontOfSize:12];
    searchLabel.textColor=mainGrayColor;
    [self.searchField addSubview:searchLabel];
}

- (void)setSubviews {
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    @weakify(self);
    [self setRefreshHeader:^{
        @strongify(self);
        self.model.page = 1;
        [self requestData];
    }];
    [self setRefreshFooter:^{
        @strongify(self);
        self.model.page++;
        [self requestData];
    }];
    
    self.tableFooterView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    self.tableFooterView.text = @"没有更多了";
    self.tableFooterView.textColor = COLOR_666666;
    self.tableFooterView.font = [UIFont systemFontOfSize:14];
    self.tableFooterView.textAlignment = NSTextAlignmentCenter;
    
    self.praiseAndCommentBtnArray=[NSMutableArray array];
    self.keyBoard = [ChatKeyBoard keyBoard];
    self.keyBoard.keyBoardStyle = KeyBoardStyleComment;
    self.keyBoard.associateTableView = self.tableView;
    self.keyBoard.delegate = self;
    self.keyBoard.allowVoice = NO;
    self.keyBoard.allowMore = NO;
    self.keyBoard.allowFace = NO;
    self.keyBoard.allowSend = YES;
    [self.view addSubview:self.keyBoard];
    

    [self.view addSubview:self.defaultView];
    [self.view bringSubviewToFront:self.defaultView];
}

- (void)searchSkills {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleRoundedRect];
    [self.navigationController pushViewController:search animated:YES];
    [search setSearchAction:^(NSString *text) {
        NSLog(@"%@",text);
        [self.navigationController popViewControllerAnimated:YES];
        self.model.keywords = text;
        [self.tableView.mj_header beginRefreshing];
    }];
}
#pragma mark-Lazy Loading
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}
-(NSMutableArray *)statusArray{
    if (!_statusArray) {
        _statusArray=[NSMutableArray array];
    }
    return _statusArray;
}

- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:self.view.bounds];
        _defaultView.button.hidden = YES;
        _defaultView.baseButton.hidden = NO;
        _defaultView.titleLabel.text = @"暂无技能发布";
        _defaultView.hidden = YES;
        [_defaultView.baseButton setTitle:@"去发布" forState:UIControlStateNormal];
        [_defaultView.baseButton addTarget:self action:@selector(gotoPublish) forControlEvents:UIControlEventTouchUpInside];
    }
    return _defaultView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData {
    [HTTPManager getSkillExchangeListWithParams:self.model.mj_keyValues WithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            SkillExchangeListModel *model=[SkillExchangeListModel mj_objectWithKeyValues:dic[@"data"]];
            if (self.model.page == 1) {
                // 下拉刷新
                [self.statusArray removeAllObjects];
                [self.statusArray addObjectsFromArray:model.data];
                if (self.statusArray.count) {
                    self.tableView.tableFooterView = nil;
                    [self.tableView reloadData];
                } else {
                    self.tableView.tableFooterView = self.tableFooterView;
                }
            } else {
                // 上拉加载更多
                if (model.data.count) {
                    self.tableView.tableFooterView = nil;
                    [self.statusArray addObjectsFromArray:model.data];
                    [self.tableView reloadData];
                } else {
                    self.tableView.tableFooterView = self.tableFooterView;
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)chatKeyBoardSendText:(NSString *)text{
    [self.view endEditing:YES];
    if (self.commentOrNot==YES) {//评论
        @weakify(self);
        NSDictionary *param = @{@"flag":@(1),@"skill_exchange_id":self.topicId,@"content":text};
        [HTTPManager commentForSkillExchangeWithCommentData:param success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                @strongify(self);
                SkillReplayModel *reply = [SkillReplayModel new];
                reply.type = 1;
                reply.user_id = [UserInstance ShardInstnce].uid;
                reply.from_nickname = [UserInstance ShardInstnce].nickName;
                reply.content = text;
                reply.comment_id = dic[@"data"];
                self.reply = reply;
                [self refreshAfterCommitOrReply];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }else{//回复
        @weakify(self);
        NSDictionary *param = @{@"flag":@(1),@"comment_id":self.replyId,@"content":text};
        [HTTPManager replyForSkillExchangeWithReplyData:param success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                @strongify(self);
                SkillReplayModel *reply = [SkillReplayModel new];
                reply.type = 1;
                reply.user_id = [UserInstance ShardInstnce].uid;
                reply.from_nickname = [UserInstance ShardInstnce].nickName;
                reply.to_nickname = self.replyOrCommentCell.status.nickname;
                reply.content = text;
                reply.comment_id = dic[@"data"];
                self.reply = reply;
                [self refreshAfterCommitOrReply];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            
        }];
    }
}

- (void)refreshAfterCommitOrReply {
    SkillExchangeModel *status = self.replyOrCommentCell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:status.comment];
    [tempArray addObject:self.reply];
    status.comment=tempArray;
    self.replyOrCommentCell.status=status;
    [self.tableView reloadData];
    self.replyOrCommentCell=nil;
    self.reply=nil;
}

- (void)gotoPublish {
    SkillPublishViewController *sp = [[SkillPublishViewController alloc] init];
    [self.navigationController pushViewController:sp animated:YES];
}

#pragma mark - button action
-(void)deleteStatus:(UIButton *)deleteBtn{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        SkillStatusCell *cell=(SkillStatusCell *)deleteBtn.superview.superview;
        [XSTool showProgressHUDTOView:self.view withText:nil];
        @weakify(self);
        [HTTPManager deleteSkillExchangeWithId:cell.status.se_id success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if(state.status){
                [self.statusArray removeObject:cell.status];
                [self.tableView reloadData];
                if (self.statusArray.count == 0) {
                    self.defaultView.hidden = NO;
                }
            }else{
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showPraiseOrCommentBtn:(UIButton *)button{
    button.selected=!button.selected;
    SkillStatusCell *cell=(SkillStatusCell *)button.superview.superview;
    if (button.selected) {
        self.tempCell=cell;
        //其他的评论点赞按钮隐藏
        for (int i=0; i<self.praiseAndCommentBtnArray.count; i++) {
            SkillStatusCell *cell1=[self.praiseAndCommentBtnArray objectAtIndex:i];
            if (![cell1 isEqual:self.tempCell]) {
                cell1.praiseAndConmmentViewHidden=YES;
                [self.praiseAndCommentBtnArray removeObject:cell1];
            }
        }
        [self.praiseAndCommentBtnArray addObject:cell];
        cell.praiseAndConmmentViewHidden=NO;
    }else{
        cell.praiseAndConmmentViewHidden=YES;
        self.tempCell=nil;
        [self.praiseAndCommentBtnArray removeObject:self.tempCell];
    }
}

-(void)addComment:(UIButton *)button{
    SkillStatusCell *cell=(SkillStatusCell *)button.superview.superview.superview;
    cell.praiseAndConmmentViewHidden=YES;
    [self.praiseAndCommentBtnArray removeObject:cell];
    self.tempCell=nil;
    self.replyOrCommentCell=cell;
    self.topicId=cell.status.se_id;
    self.commentOrNot = YES;
    self.keyBoard.placeHolder = @"";
    [self.keyBoard keyboardUpforComment];
}

-(void)addPraise:(UIButton *)button{
    SkillStatusCell *cell=(SkillStatusCell *)button.superview.superview.superview;
    SkillExchangeModel *status=cell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.thumbsup];
    cell.praiseBtn.imageView.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.75 delay:0 options:0 animations: ^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
        }];
        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    } completion:^(BOOL finished) {
        cell.praiseAndConmmentViewHidden=YES;
        [self.praiseAndCommentBtnArray removeObject:cell];
        self.tempCell=nil;
        if (cell.status.is_thumbs_up == 1) {//取消点赞
            @weakify(self);
            [HTTPManager thumbupForSkillExchange:cell.status.se_id flag:0 success:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state) {
                    for (int i=0; i<cell.status.thumbsup.count; i++) {
                        NSString *user=[cell.status.thumbsup objectAtIndex:i];
                        if ([user isEqualToString:[UserInstance ShardInstnce].nickName]) {
                            [tempArray removeObject:user];
                        }
                    }
                    status.thumbsup=tempArray;
                    status.is_thumbs_up = 0;
                    [self.tableView reloadData];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }else{//点赞
            @weakify(self);
            [HTTPManager thumbupForSkillExchange:cell.status.se_id flag:1 success:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state) {
                    [tempArray addObject:[UserInstance ShardInstnce].nickName];
                    status.thumbsup=tempArray;
                    status.is_thumbs_up = 1;
                    [self.tableView reloadData];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }
    }];
}

-(void)goToUserMainController:(UIButton *)btn{
    SkillStatusCell *cell= (SkillStatusCell *)btn.superview.superview;
    PersonalViewController *personVC=[[PersonalViewController alloc] initWithUid:cell.status.user_id];
    personVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:personVC animated:YES];
}

-(void)replyOrDeleteComment:(UITapGestureRecognizer *)tap{
    SkillReplyView *view=(SkillReplyView *)tap.view;
    SkillStatusCell *cell=(SkillStatusCell *)tap.view.superview.superview.superview;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.comment];
    if ([cell.status.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        if ([view.reply.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [XSTool showProgressHUDTOView:self.view withText:nil];
                if (view.reply.type == 1) {
                    // 这是一条正常的评论
                    @weakify(self);
                    [HTTPManager deleteCommentForSkillExchangeWithCommentId:view.reply.comment_id success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state) {
                            [tempArray removeObject:view.reply];
                            cell.status.comment=tempArray;
                            [self.tableView reloadData];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                } else if (view.reply.type == 2) {
                    // 这是一条回复
                    @weakify(self);
                    [HTTPManager deleteReplyForSkillExchangeWithReplyId:view.reply.comment_id success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state) {
                            [tempArray removeObject:view.reply];
                            cell.status.comment=tempArray;
                            [self.tableView reloadData];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                }
            }];
            UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:confirm];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            self.replyOrCommentCell=cell;
            self.commentOrNot = NO;
            self.replyId=view.reply.comment_id;
            self.keyBoard.placeHolder = [NSString stringWithFormat:@"回复%@:",view.reply.from_nickname];
            [self.keyBoard keyboardUpforComment];
        }
    } else {
        if ([view.reply.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [XSTool showProgressHUDTOView:self.view withText:nil];
                if (view.reply.type == 1) {
                    // 这是一条正常的评论
                    @weakify(self);
                    [HTTPManager deleteCommentForSkillExchangeWithCommentId:view.reply.comment_id success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state) {
                            [tempArray removeObject:view.reply];
                            cell.status.comment=tempArray;
                            [self.tableView reloadData];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                } else if (view.reply.type == 2) {
                    // 这是一条回复
                    @weakify(self);
                    [HTTPManager deleteReplyForSkillExchangeWithReplyId:view.reply.comment_id success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state) {
                            [tempArray removeObject:view.reply];
                            cell.status.comment=tempArray;
                            [self.tableView reloadData];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                }
            }];
            UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:confirm];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            if ([view.reply.user_id isEqualToString:cell.status.user_id]) {
                // 只能回复动态发布者
                self.replyOrCommentCell=cell;
                self.commentOrNot = NO;
                self.replyId=view.reply.comment_id;
                self.keyBoard.placeHolder = [NSString stringWithFormat:@"回复%@:",view.reply.from_nickname];
                [self.keyBoard keyboardUpforComment];
            }
        }
    }
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.statusArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.heightDic[@(indexPath.row)] floatValue]==0) {
        return 0.1;
    }
    return [self.heightDic[@(indexPath.row)] floatValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId=@"statusCell";
    SkillStatusCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[SkillStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.currentVC=self;
    cell.status=[self.statusArray objectAtIndex:indexPath.row];
    [cell.deleteBtn addTarget:self action:@selector(deleteStatus:) forControlEvents:UIControlEventTouchUpInside];
    [cell.clearBtn addTarget:self action:@selector(showPraiseOrCommentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [cell.commentBtn addTarget:self action:@selector(addComment:) forControlEvents:UIControlEventTouchUpInside];
    [cell.praiseBtn addTarget:self action:@selector(addPraise:) forControlEvents:UIControlEventTouchUpInside];
    [cell.headImgBtn addTarget:self action:@selector(goToUserMainController:) forControlEvents:UIControlEventTouchUpInside];
    for (SkillReplyView *view in cell.replyOrDeleteCommentViewArray) {
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(replyOrDeleteComment:)]];
    }

    //记录高度
    self.heightDic[@(indexPath.row)]=@(cell.height);
    return cell;
}

#pragma mark-ScrollView Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}
@end
