//
//  SkillModel.h
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FansCircleModel.h"

@interface GetSkillModel : NSObject
@property (nonatomic, copy) NSString *keywords;
@property (nonatomic, assign) unsigned int page;
@end

@interface SkillModel : NSObject
@property (nonatomic, copy) NSString *skill_id;
@property (nonatomic, copy) NSString *skill_name;
@property (nonatomic, assign) BOOL selected;
@end

@interface SkillListModel : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface SkillReplayModel : Reply
@property (nonatomic, assign) unsigned int type; //(1评论|2回复)
@property (nonatomic, copy) NSString *comment_id; //评论id
@property (nonatomic, copy) NSString *user_id;
@end

@interface SkillExchangeModel : NSObject
@property (nonatomic, copy) NSString *se_id;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, strong) NSArray *skill_goodat;
@property (nonatomic, strong) NSArray *skill_want;
@property (nonatomic, strong) NSArray *image;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, strong) NSArray *thumbsup;
@property (nonatomic, strong) NSArray *comment;
@property (nonatomic, assign)unsigned int is_thumbs_up;//当前用户有没有点赞
@end

@interface SkillExchangeListModel : NSObject
@property(nonatomic, strong)NSArray *data;
@property(nonatomic, assign)NSInteger current_page;
@property(nonatomic, assign)NSInteger per_page;
@property(nonatomic, assign)NSInteger total;
@end
