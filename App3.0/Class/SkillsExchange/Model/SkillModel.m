//
//  SkillModel.m
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillModel.h"

@implementation GetSkillModel
@end

@implementation SkillModel
- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    if (![object isKindOfClass:[SkillModel class]]) {
        return NO;
    }
    SkillModel *model = (SkillModel *)object;
    if ([model.skill_name isEqualToString:self.skill_name]) {
        return YES;
    } else {
        return NO;
    }
}
@end

@implementation SkillListModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"SkillModel",
             };
}
@end

@implementation SkillReplayModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{
             @"content" : @"content",
             @"from_nickname" : @"nickname",
             };
}
@end

@implementation SkillExchangeModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"skill_goodat" : @"NSString",
             @"skill_want":@"NSString",
             @"image" : @"NSString",
             @"thumbsup":@"NSString",
             @"comment":@"SkillReplayModel",
             };
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{
             @"se_id" : @"id",
             };
}
@end

@implementation SkillExchangeListModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"SkillExchangeModel",
             };
}
@end

