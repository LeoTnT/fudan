//
//  ClipViewViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClipViewViewController;
typedef NS_ENUM(NSUInteger,ClipType) {
    CIRCULARCLIP = 0, //圆形裁剪
    SQUARECLIP = 1, //方形裁剪
    RECTANGLELIP //矩形
    
};

@protocol ClipViewControllerDelegate <NSObject>

-(void)clipViewController:(ClipViewViewController *)clipViewController FinishClipImage:(UIImage *)editImage;

@end

@interface ClipViewViewController : UIViewController<UIGestureRecognizerDelegate>
{
    UIImageView *_imageView;
    UIImage *_image;
    UIView * _overView;
}
@property (nonatomic, assign)CGFloat scaleRation;//图片缩放的最大倍数
@property (nonatomic, assign)CGFloat radius; //圆形裁剪框的半径
@property (nonatomic, assign)CGRect circularFrame;//裁剪框的frame
@property (nonatomic, assign)CGRect OriginalFrame;
@property (nonatomic, assign)CGRect currentFrame;
@property (nonatomic, assign)ClipType clipType;  //裁剪的形状
@property (nonatomic, strong)id<ClipViewControllerDelegate>delegate;

-(instancetype)initWithImage:(UIImage *)image;
@end

