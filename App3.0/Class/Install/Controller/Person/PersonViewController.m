//
//  PersonViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PersonViewController.h"
#import "LoginModel.h"
#import "UserInstance.h"
#import "TOCropViewController.h"
#import "UserQRViewController.h"
#import "XSClipImage.h"
#import "UIImage+XSWebImage.h"
#import "WithdrawViewController.h"
#import "ReceiverAddressViewController.h"
#import "UUImageAvatarBrowser.h"
#import "PopUpView.h"

@interface PersonViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,TOCropViewControllerDelegate,PopViewDelegate>

@property(nonatomic,strong) UIImagePickerController *imagePickViewController;//拍照控制器
@property(nonatomic,strong) UIButton *manButton;//男按钮
@property(nonatomic,strong) UIButton *womenButton;//女按钮
@property(nonatomic,strong) UIImageView *iconImage;//头像
@property(nonatomic,strong) NSString *nickName;//昵称
@property(nonatomic,strong) LoginDataParser *userInformation;//用户信息
@property(nonatomic,assign) BOOL takePhotoFlag;//是否是现拍的照片
@property(nonatomic,copy) NSMutableString *imageString;//上传的图片返回结果字符串
@property (nonatomic, strong) PopUpView *sheetView;//弹出的view
@property (nonatomic, strong) NSArray *contentArray;
@end

@implementation PersonViewController

#pragma mark - lazy loadding

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"person_info");
//    self.navigationController.navigationBarHidden = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
#ifdef APP_SHOW_JYS
    self.contentArray = @[@[Localized(@"person_info_head"), Localized(@"person_info_nick"), Localized(@"person_info_mobile")],@[Localized(@"person_info_no"), Localized(@"person_info_qr")]];
#elif defined APP_SHOW_MALLANDJYS
    self.contentArray = @[@[Localized(@"person_info_head"), Localized(@"person_info_nick"), Localized(@"person_info_mobile"), Localized(@"收货地址")],@[Localized(@"person_info_no"), Localized(@"person_info_qr")]];
#else
    self.contentArray = @[@[Localized(@"person_info_head"), Localized(@"person_info_nick"), Localized(@"person_info_mobile"), Localized(@"收货地址")],@[Localized(@"person_info_no"), Localized(@"person_info_qr")]];
#endif
    
    
    self.iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE*7, NORMOL_SPACE, NORMOL_SPACE*4, NORMOL_SPACE*4)];
    self.iconImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        [UUImageAvatarBrowser showImage:self.iconImage title:@""];
    }];
    [self.iconImage addGestureRecognizer:tap];
    
    [self getUserInformation];
    [self setSubviews];
}

-(NSMutableString *)imageString {
    if (!_imageString) {
        _imageString = [NSMutableString string];
    }
    return _imageString;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getUserInformation {
    
    if ([[UserInstance ShardInstnce].nickName isEqualToString:@""]||[UserInstance ShardInstnce].nickName.length==0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [userDefaults objectForKey:USERINFO_LOGIN];
        self.userInformation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        self.userInformation.nickname = APP_NAME;
        NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:self.userInformation];
        [userDefaults setObject:userData forKey:USERINFO_LOGIN];
        [userDefaults synchronize];
        // 实例化user数据
        [[UserInstance ShardInstnce] setupUserInfo];
    }
    [self.iconImage getImageWithUrlStr:[UserInstance ShardInstnce].avatarImgUrl andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    
}

- (void)setSubviews {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.backgroundColor = BG_COLOR;
}

- (void)selectManAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    NSString *sex=@"0";
    [HTTPManager setUserInformationWithSex:sex success:^(NSDictionary * _Nullable dic, resultObject *state) {
        //成功修改
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"资料修改成功"];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [userDefaults  objectForKey:USERINFO_LOGIN];
            self.userInformation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            self.userInformation.sex = @"0";
            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:self.userInformation];
            [userDefaults setObject:userData forKey:USERINFO_LOGIN];
            [userDefaults synchronize];
            [[UserInstance ShardInstnce] setupUserInfo];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    if (self.womenButton.selected) {
        self.womenButton.selected = !self.womenButton.selected;
    }
    [self sexAction:self.manButton];
    [self sexAction:self.womenButton];
}

- (void)selectWomenAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    NSString *sex = @"1";
    [HTTPManager setUserInformationWithSex:sex success:^(NSDictionary * _Nullable dic, resultObject *state) {
        
        //成功修改
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"资料修改成功"];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [userDefaults  objectForKey:USERINFO_LOGIN];
            self.userInformation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            self.userInformation.sex = @"1";
            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:self.userInformation];
            [userDefaults setObject:userData forKey:USERINFO_LOGIN];
            [userDefaults synchronize];
            [[UserInstance ShardInstnce] setupUserInfo];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
    if (self.manButton.selected) {
        self.manButton.selected = !self.manButton.selected;
    }
    [self sexAction:self.manButton];
    [self sexAction:self.womenButton];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (self.takePhotoFlag) {
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
        cropController.delegate = self;
        [picker presentViewController:cropController animated:NO completion:nil];
    } else {
        
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"]) {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
            cropController.delegate = self;
            [picker pushViewController:cropController animated:YES];
        }
    }
}
#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.takePhotoFlag) {
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
            [weakSelf.imagePickViewController dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIImage *tempImage;
        NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
        if (imageData.length>=1024*1024*2) {//2M以及以上
            if (imageData.length<1024*1024*4) {
                imageData = UIImageJPEGRepresentation(image, 0.4);
            } else if (imageData.length<1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.2);
                
            } if (imageData.length>1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.1);
                
            }
            
            //            if (imageData.length>=0.2) {
            //
            //                [XSTool showToastWithView:self.view Text:@"图片过大，请重新选择！"];
            //                return ;
            //            } else {
            
            tempImage = [UIImage imageWithData:imageData];
            //            }
        } else {
            tempImage = [UIImage imageWithData:imageData];
            
        }
        if (tempImage) {
            
            //压缩
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager upLoadUserInformationWithIconImage:tempImage progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSDictionary * _Nullable dic, BOOL state) {
                if ([dic[@"status"] integerValue]==1) {
                    [self.imageString setString:[NSString stringWithFormat:@"%@",dic[@"data"]]];
                    //修改头像
                    [HTTPManager setUserInformationWithLogo:self.imageString success:^(NSDictionary * _Nullable dic, resultObject *state) {
                        if (state.status) {
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:@"资料修改成功"];
                            LoginDataParser *parser = [[UserInstance ShardInstnce] getUserInfo];
                            parser.logo = self.imageString;
                            
                            //1.存储信息
                            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:parser];
                            [user setObject:userData forKey:USERINFO_LOGIN];
                            [user synchronize];
                            self.iconImage.image = image;
                            // 实例化user数据
                            [[UserInstance ShardInstnce] setupUserInfo];
                            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
                            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
                        } else {
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                        
                    } failure:^(NSError * _Nonnull error) {
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:NetFailure];
                    }];
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
    }];
    
}

- (void)choosePhotoes {
    [self.view endEditing:YES];
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypePhoto;
    [self.sheetView.oneBtn setTitle:Localized(@"拍照") forState:UIControlStateNormal];
    [self.sheetView.oneBtn addTarget:self action:@selector(takeAPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.sheetView.otherBtn setTitle:Localized(@"从相册中选择") forState:UIControlStateNormal];
    [self.sheetView.otherBtn addTarget:self action:@selector(openPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView ] ;
}

- (void)takeAPhoto {
    [self clickToCancelLink];
    self.imagePickViewController = [[UIImagePickerController alloc] init];
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action1];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if (!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickViewController.showsCameraControls = YES;
    
    //摄像头捕获模式
    self.imagePickViewController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickViewController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    self.imagePickViewController.delegate = self;
    [self presentViewController:self.imagePickViewController animated:YES completion:^{
        self.takePhotoFlag = YES;
    }];
}


- (void)openPhotoLibrary {
    [self clickToCancelLink];
    //打开相册
    self.imagePickViewController = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.imagePickViewController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        //允许修改
        self.imagePickViewController.allowsEditing = YES;
    }
    self.imagePickViewController.delegate = self;
    self.imagePickViewController.allowsEditing = NO;
    [self presentViewController:self.imagePickViewController animated:YES completion:nil];
    
}

#pragma mark -  PopViewDelegate
- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}



#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0&&indexPath.section == 0) {
        return 60;
    } else {
        return 40;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = self.contentArray[indexPath.section][indexPath.row];
    if ([title isEqualToString:Localized(@"person_info_head")]) {
        //修改头像
        [self choosePhotoes];
    } else if ([title isEqualToString:Localized(@"person_info_nick")]) {
        //修改昵称
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"register_name") preferredStyle:UIAlertControllerStyleAlert];
        
        //添加按钮
        __weak typeof(alertController) weakAlert = alertController;
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *name = [weakAlert.textFields.lastObject text];
            if (name.length==0) {
                [XSTool showToastWithView:self.view Text: @"昵称不能为空"];
            } else {
                if ([self stringContainsEmoji:name]) {
                    [XSTool showToastWithView:self.view Text:@"你好，昵称不支持表情"];
                } else {
                    NSCharacterSet *whiteString = [NSCharacterSet whitespaceCharacterSet];
                    NSString *nameString = [[NSString alloc]initWithString:[name stringByTrimmingCharactersInSet:whiteString]];
                    [XSTool showProgressHUDWithView:self.view];
                    //提交昵称
                    [HTTPManager setUserInformationWithNickName:nameString success:^(NSDictionary * _Nullable dic, resultObject *state) {
                        
                        [XSTool hideProgressHUDWithView:self.view];
                        //成功修改
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"资料修改成功"];
                            //刷新表格
                            self.userInformation.nickname = [weakAlert.textFields.lastObject text];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                            });
                            
                            //1.修改沙盒存储信息
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            NSData *data = [userDefaults objectForKey:USERINFO_LOGIN];
                            self.userInformation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                            self.userInformation.nickname = name;
                            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:self.userInformation];
                            [userDefaults setObject:userData forKey:USERINFO_LOGIN];
                            [userDefaults synchronize];
                            // 实例化user数据
                            [[UserInstance ShardInstnce] setupUserInfo];
                        } else {
                            
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                        
                    } failure:^(NSError * _Nonnull error) {
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:NetFailure];
                    }];
                }
            }
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        
        // 添加文本框
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.textColor = [UIColor blackColor];
            textField.delegate = self;
            textField.tag = 1500;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        // 弹出对话框
        [self presentViewController:alertController animated:YES completion:nil];
    } else if ([title isEqualToString:Localized(@"person_info_mobile")]) {
        
    } else if ([title isEqualToString:Localized(@"收货地址")]) {
        //收货地址
        ReceiverAddressViewController *ReceiverVC = [[ReceiverAddressViewController alloc] init];
        [self.navigationController pushViewController:ReceiverVC animated:YES];
    } else if ([title isEqualToString:Localized(@"person_info_no")]) {
        
    } else if ([title isEqualToString:Localized(@"person_info_qr")]) {
        //我的二维码
        UserQRViewController *userQRVC = [[UserQRViewController alloc] init];
        [self.navigationController pushViewController:userQRVC animated:YES];
    }
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"personCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    }
    NSArray *arr = self.contentArray[indexPath.section];
    NSString *title = arr[indexPath.row];
    cell.textLabel.text = title;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell.contentView addSubview:self.iconImage];
            }
                break;
            case 1: {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.detailTextLabel.text = [UserInstance ShardInstnce].nickName;
            }
                break;
//            case 2: {
//                cell.textLabel.text = @"性别";
//                CGFloat height = 40;
//
//                //默认是男
//                UIView *dView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, height*3, height)];
//                self.manButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, height*3/2, height)];
//                [self.manButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                self.manButton.titleLabel.font = [UIFont systemFontOfSize:15];
//                [self.manButton setImage:[UIImage imageNamed:@"user_info_man"] forState:UIControlStateNormal];
//                [self.manButton setImage:[UIImage imageNamed:@"user_info_man_s"] forState:UIControlStateSelected];
//                [self.manButton setTitle:@"男" forState:UIControlStateNormal];
//
//                self.manButton.titleEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(self.manButton.imageView.frame)-NORMOL_SPACE, 0, 0);
//                [self.manButton addTarget:self action:@selector(selectManAction:) forControlEvents:UIControlEventTouchUpInside];
//                [dView addSubview:self.manButton];
//                //            self.manButton.backgroundColor = [UIColor yellowColor];
//                self.womenButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.manButton.frame), 0, height*3/2, height)];
//                self.womenButton.titleLabel.font = [UIFont systemFontOfSize:15];
//                [self.womenButton setImage:[UIImage imageNamed:@"user_info_woman"] forState:UIControlStateNormal];
//                [self.womenButton setImage:[UIImage imageNamed:@"user_info_woman_s"] forState:UIControlStateSelected];
//                [self.womenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                [self.womenButton setTitle:@"女" forState:UIControlStateNormal];
//                self.womenButton.titleEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(self.womenButton.imageView.frame)-NORMOL_SPACE, 0, 0);
//                [self.womenButton addTarget:self action:@selector(selectWomenAction:) forControlEvents:UIControlEventTouchUpInside];
//                [dView addSubview:self.womenButton];
//                //            self.womenButton.backgroundColor = [UIColor greenColor];
//                cell.accessoryView = dView;
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                if ([[UserInstance ShardInstnce].sex integerValue]==0) {
//                    self.manButton.selected = YES;
//                    self.womenButton.selected = NO;
//                } else {
//                    self.womenButton.selected = YES;
//                    self.manButton.selected = NO;
//                }
//                [self sexAction:self.manButton];
//                [self sexAction:self.womenButton];
//            }
//                break;
            case 2: {
                cell.detailTextLabel.text = [UserInstance ShardInstnce].mobile;
            }
                break;
            case 3: {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0: {
                cell.accessoryType = UITableViewCellAccessoryNone;
                LoginDataParser* _parser = [[UserInstance ShardInstnce] getUserInfo];
                cell.detailTextLabel.text = _parser.username;
            }
                break;
            case 1: {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE*5, NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
                img.image = [UIImage imageNamed:@"user_setting_qr"];
                [cell.contentView addSubview:img];
            }
                
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = self.contentArray[section];
    return arr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.contentArray.count;
}

- (void)sexAction:(UIButton *) sender {
    if (sender.selected) {
        sender.userInteractionEnabled = NO;
    } else {
        sender.userInteractionEnabled = YES;
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.markedTextRange == nil) {
        if (textField.text.length >10&&textField.tag==1500) {
            [XSTool showToastWithView:self.view Text:@"昵称长度只能为1~10"];
            textField.text = [textField.text substringToIndex:10];
        }
    }
}
//判断是否是系统表情
-(BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}
@end
