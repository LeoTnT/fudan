//
//  UserQRViewController.m
//  App3.0
//
//  Created by mac on 17/4/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserQRViewController.h"
#import "UserInstance.h"
#import "XSShareView.h"
#import "XSQR.h"

@interface UserQRViewController ()
@property (strong, nonatomic) UIImage *qrImage;
@end

@implementation UserQRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"person_info_qr");
    self.view.backgroundColor = BG_COLOR;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self addSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSubviews {
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.right.mas_equalTo(self.view).offset(-25);
        make.centerY.mas_equalTo(self.view).offset(-30);
        make.height.mas_equalTo(mainWidth+25);
    }];
    
    UIImageView *avatar = [UIImageView new];
    avatar.layer.masksToBounds = YES;
    avatar.layer.cornerRadius = 3;
    [avatar getImageWithUrlStr:[UserInstance ShardInstnce].avatarImgUrl andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    [bgView addSubview:avatar];
    [avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(bgView).offset(-20);
        make.left.mas_equalTo(bgView).offset(40);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.text = [UserInstance ShardInstnce].nickName;
    nameLabel.font = [UIFont systemFontOfSize:16];
    [bgView addSubview:nameLabel];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(avatar.mas_right).offset(12);
        make.top.mas_equalTo(avatar).offset(7);
    }];
    
    UILabel *desLabel = [UILabel new];
    desLabel.text = Localized(@"person_qr_suggest");
    desLabel.textColor = COLOR_999999;
    desLabel.font = [UIFont systemFontOfSize:13];
    [bgView addSubview:desLabel];
    [desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel);
        make.bottom.mas_equalTo(avatar).offset(-5);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
    [bgView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.right.mas_equalTo(bgView).offset(-12);
        make.bottom.mas_equalTo(avatar.mas_top).offset(-20);
        make.height.mas_equalTo(0.5);
    }];
    
    UIImageView *qrImgView = [UIImageView new];
    self.qrImage = [XSQR createQrImageWithContentString:[UserInstance ShardInstnce].userName type:XSQRTypeUser];
    [qrImgView setImage:self.qrImage];
    [bgView addSubview:qrImgView];
    [qrImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(37);
        make.right.mas_equalTo(bgView).offset(-37);
        make.top.mas_equalTo(bgView).offset(30);
        make.height.mas_equalTo(qrImgView.mas_width);
    }];
    
    UIButton *save = [UIButton buttonWithType:UIButtonTypeCustom];
    save.backgroundColor = [UIColor whiteColor];
    [save setTitle:Localized(@"save_mobile") forState:UIControlStateNormal];
    [save setTitleColor:COLOR_666666 forState:UIControlStateNormal];
    save.titleLabel.font = [UIFont systemFontOfSize:16];
    save.layer.masksToBounds = YES;
    save.layer.cornerRadius = 3;
    [save addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:save];
    [save mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_bottom).offset(45);
        make.left.mas_equalTo(bgView);
        make.right.mas_equalTo(self.view.mas_centerX).offset(-4);
        make.height.mas_equalTo(44);
    }];
    
    UIButton *share = [UIButton buttonWithType:UIButtonTypeCustom];
    share.backgroundColor = mainColor;
    [share setTitle:Localized(@"share") forState:UIControlStateNormal];
    [share setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    share.titleLabel.font = [UIFont systemFontOfSize:16];
    share.layer.masksToBounds = YES;
    share.layer.cornerRadius = 3;
    [share addTarget:self action:@selector(shareClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:share];
    [share mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(save);
        make.left.mas_equalTo(self.view.mas_centerX).offset(4);
        make.right.mas_equalTo(bgView);
        make.height.mas_equalTo(44);
    }];
    
}

- (void)saveClick {
    //因为没有协议，执行的回调的时候只能按照固定的格式写回调方法
    UIImageWriteToSavedPhotosAlbum(self.qrImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image: (UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    [XSTool showToastWithView:self.view Text:@"二维码已保存到手机"];
}


- (void)shareClick {
    NSData *data = UIImagePNGRepresentation(self.qrImage);
#ifdef ALIYM_AVALABLE
    YWMessageBodyImage *body = [[YWMessageBodyImage alloc] initWithMessageImageData:data];
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:body];
    trVC.isShare = YES;
    [self.navigationController pushViewController:trVC animated:YES];
#elif defined EMIM_AVALABLE
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:data displayName:@"image.jpeg"];
    NSString *from = [[EMClient sharedClient] currentUsername];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:nil from:from to:nil body:body ext:nil];
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:message];
    [self.navigationController pushViewController:trVC animated:YES];
#else
    TranspondListViewController *trVC = [[TranspondListViewController alloc] init];
    XM_TransModel *transModel = [[XM_TransModel alloc] initShareImage:data];
    trVC.transModel = transModel;

    [self.navigationController pushViewController:trVC animated:YES];
#endif
    
}
@end
