//
//  SetupViewController.m
//  App3.0
//
//  Created by mac on 2017/7/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetupViewController.h"
#import "AccountSafeViewController.h"
#import "SetNewMessageViewController.h"
#import "SetNoDisturbViewController.h"
#import "SetChatViewController.h"
#import "AboutViewController.h"
#import "XSCacheManager.h"
#import "BlackListViewController.h"
#import "EnterViewController.h"
#import "PersonViewController.h"
#import "AppDelegate.h"
#import "LanguageSelectView.h"

@interface SetupViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contentArray;
@property (nonatomic, strong) LanguageSelectView *languageView;

@end

@implementation SetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"me_item_setting");
#ifdef APP_SHOW_JYS
    [self.contentArray addObjectsFromArray:@[@[Localized(@"person_info")],@[Localized(@"account_safe")],@[Localized(@"语言设置")], @[Localized(@"clear_cash")], @[Localized(@"about")]]];
#elif defined APP_SHOW_MALLANDJYS
    [self.contentArray addObjectsFromArray:@[@[Localized(@"person_info")],@[Localized(@"account_safe")],@[Localized(@"新消息通知"), Localized(@"勿扰模式"), Localized(@"聊天设置"), Localized(@"黑名单")], @[Localized(@"clear_cash")], @[Localized(@"about")]]];
#else
    [self.contentArray addObjectsFromArray:@[@[Localized(@"person_info")],@[Localized(@"account_safe")],@[Localized(@"新消息通知"), Localized(@"勿扰模式"), Localized(@"聊天设置"), Localized(@"黑名单")], @[Localized(@"clear_cash")], @[Localized(@"about")]]];
#endif
    
    [self addSubviews];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)contentArray {
    if (!_contentArray) {
        _contentArray  = [NSMutableArray array];
    }
    return _contentArray;
}

- (void)addSubviews {
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
//    _tableView.bounces = NO;
    _tableView.delegate = self;
    _tableView.sectionHeaderHeight = NORMOL_SPACE;
    _tableView.sectionFooterHeight = 0;
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 100)];
    self.tableView.tableFooterView = footerView;
    XSCustomButton *exitBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 30, mainWidth-2*NORMOL_SPACE, NORMOL_SPACE*5) title:Localized(@"exit_app") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [exitBtn setBorderWith:1 borderColor:[mainColor CGColor] cornerRadius:5];
    [exitBtn addTarget:self action:@selector(exitAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:exitBtn];
}

- (void)exitAction:(UIButton *) sender{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [user objectForKey:USERINFO_LOGIN];
    LoginDataParser *loginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager LoginOutWithName:loginInfo.username success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            //清除信息
            [[UserInstance ShardInstnce] logout];
            //跳转到enter界面
            EnterViewController *enter = [[EnterViewController alloc] init];
            UINavigationController* enterNav = [[UINavigationController alloc] initWithRootViewController:enter];
            AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.window.rootViewController = nil;
            appDelegate.window.rootViewController = enterNav;
            [[XMPPManager sharedManager] xmppUserLogout];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"%@",error.description);
    }];
    
}

#pragma mark - UITableViewDelegate
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.contentArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return tableView.sectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = self.contentArray[section];
    return arr.count;
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idString = @"installCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    }
    cell.textLabel.text = self.contentArray[indexPath.section][indexPath.row];
    NSString *title = self.contentArray[indexPath.section][indexPath.row];
    if ([title isEqualToString:@"清除缓存"]) {
        cell.detailTextLabel.text = [[XSCacheManager sharedInstance] getAllCacheSize];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = self.contentArray[indexPath.section][indexPath.row];

    if ([title isEqualToString:Localized(@"语言设置")]) {
        [self languageBtnAction];
    }else{
    UIViewController *VC;
    if ([title isEqualToString:Localized(@"person_info")]) {
        VC = [[PersonViewController alloc] init];
    } else if ([title isEqualToString:Localized(@"account_safe")]) {
#ifdef APP_SHOW_JYS
        VC = [[JYSAccountSafeViewController alloc] init];
#else
        VC = [[AccountSafeViewController alloc] init];
#endif
    } else if ([title isEqualToString:Localized(@"新消息通知")]) {
        VC = [[SetNewMessageViewController alloc] init];
    } else if ([title isEqualToString:Localized(@"勿扰模式")]) {
        VC = [[SetNoDisturbViewController alloc] init];
    } else if ([title isEqualToString:Localized(@"聊天设置")]) {
        VC = [[SetChatViewController alloc] init];
    } else if ([title isEqualToString:Localized(@"黑名单")]) {
        VC = [[BlackListViewController alloc] init];
    } else if ([title isEqualToString:Localized(@"clear_cash")]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"确定清除所有缓存数据吗？") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [XSTool showProgressHUDWithView:self.view];
            [[XSCacheManager sharedInstance] getAllCacheSize];
            [[XSCacheManager sharedInstance] clearLocalCache];
            //                    [[XSCacheManager sharedInstance] clearConversationCache];
            [XSTool hideProgressHUDWithView:self.view];
            [self.tableView reloadData];
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else if ([title isEqualToString:Localized(@"about")]) {
        VC = [[AboutViewController alloc] init];
    }
    [self.navigationController pushViewController:VC animated:YES];
    }
}

#pragma mark --- 语言切换
- (void)languageBtnAction {
    NSArray *languageArr = @[@{@"zh-Hans":Localized(@"language_zh_cn")},@{@"zh-Hant":Localized(@"language_zh_tw")},@{@"en":Localized(@"language_english")}];
    
    _languageView = [[LanguageSelectView alloc] init];
    [self.view addSubview:_languageView];
    self.languageView.dataArray = languageArr;
    
    self.languageView.hidden = NO;
    @weakify(self);
    [self.languageView setSeletAction:^(NSString *name,NSString *value){
        @strongify(self);
        if (![SelectLang isEqualToString:value]) {
            [self changeLanguageTo:value name:name];
        }
        // 切换语言后
        NSLog(@"模拟器语言切换之后：%@",SelectLang );
        self.languageView.hidden = YES;
        [self.languageView removeFromSuperview];
    }];
}

- (void)changeLanguageTo:(NSString *)language name:(NSString *)name {
//    // 设置语言
//    [NSBundle setLanguage:language];
//
//    // 然后将设置好的语言存储好，下次进来直接加载
//    [[NSUserDefaults standardUserDefaults] setObject:language forKey:MyLanguage];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    
//    NSString *alertText = [NSString stringWithFormat:@"%@%@",Localized(@"正切换至"),Localized(name)];
//    [XSTool showProgressHUDTOView:self.view withText:alertText];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [XSTool hideProgressHUDWithView:self.view];
//        // 我们要把系统windown的rootViewController替换掉
//        UIWindow *window= [UIApplication sharedApplication].keyWindow;
//        window.rootViewController = [XSTool enterMainViewController];
//        
//        
//    });
}


@end
