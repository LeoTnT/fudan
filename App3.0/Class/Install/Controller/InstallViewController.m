//
//  InstallViewController.m
//  App3.0
//tableView
//  Created by nilin on 2017/3/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "InstallViewController.h"
#import "ModifyPassWordViewController.h"
#import "BindBankCardViewController.h"
#import "ReceiverAddressViewController.h"
#import "BindAccountViewController.h"
#import "RechargeViewController.h"
#import "ChatSettingViewController.h"
#import "AboutViewController.h"
#import "XSCustomButton.h"
#import "PersonViewController.h"
#import "LoginModel.h"
#import "EnterViewController.h"
#import "AppDelegate.h"
#import "UserInstance.h"
#import "XSCacheManager.h"

@interface InstallViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contentArray;
@property (nonatomic, strong) NSString *nickName;
@end

@implementation InstallViewController
#pragma mark - lazy loadding
- (NSMutableArray *)contentArray {
    if (!_contentArray) {
        _contentArray  = [NSMutableArray array];
    }
    return _contentArray;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT-NORMOL_SPACE*6) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.delegate = self;
        _tableView.sectionHeaderHeight = NORMOL_SPACE;
        _tableView.sectionFooterHeight = 0;
        _tableView.showsVerticalScrollIndicator = NO; 
    }
    return _tableView;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBarHidden = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = BG_COLOR;
    //    [self.contentArray addObjectsFromArray:@[Localized(@"person_info"),@"账号绑定",@"绑定银行卡",@"修改密码",Localized(@"收货地址"),Localized(@"修改机器人昵称"),Localized(@"clear_cash"),@"关于鱼宝机器人"]];
    [self.contentArray addObjectsFromArray:@[Localized(@"person_info"),Localized(@"setting_tv_baccount"),Localized(@"bind_bank_title"),Localized(@"setting_tv_pwd"),Localized(@"收货地址"),Localized(@"聊天设置"),Localized(@"修改机器人昵称"),Localized(@"clear_cash"),Localized(@"about")]];
    [self getInfo];
    [self addSubViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma private
- (void)getInfo {
    self.nickName = Localized(@"你好");
}

- (void)addSubViews {
    self.navigationItem.title = Localized(@"me_item_setting");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self.view addSubview:self.tableView];

    XSCustomButton *exitBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, mainHeight-NORMOL_SPACE*6, mainWidth-2*NORMOL_SPACE, NORMOL_SPACE*5) title:Localized(@"exit_app") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [exitBtn setBorderWith:1 borderColor:[mainColor CGColor] cornerRadius:5];
    [exitBtn addTarget:self action:@selector(exitAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:exitBtn];
    
}

- (void)exitAction:(UIButton *) sender{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [user objectForKey:USERINFO_LOGIN];
    LoginDataParser *loginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager LoginOutWithName:loginInfo.username success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            //清除信息
            [[UserInstance ShardInstnce] logout];
            //跳转到enter界面
            EnterViewController *enter = [[EnterViewController alloc] init];
            UINavigationController* enterNav = [[UINavigationController alloc] initWithRootViewController:enter];
            AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.window.rootViewController = nil;
            appDelegate.window.rootViewController = enterNav;
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"%@",error.description);
    }];
    
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idString = @"installCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section==0) {
        cell.textLabel.text = self.contentArray[indexPath.row];
    }
    else{
        cell.textLabel.text = self.contentArray[indexPath.row+6];
        if (indexPath.row==0) {
            cell.detailTextLabel.text = self.nickName;
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0: {
                
                //个人资料
                PersonViewController *personVC = [[PersonViewController alloc] init];
                [self.navigationController pushViewController:personVC animated:YES];
                
            }
                break;
            case 1: {
                
                //账号绑定
                BindAccountViewController *accountVC = [[BindAccountViewController alloc] init];
                [self.navigationController pushViewController:accountVC animated:YES];
                
            }
                break;
            case 2: {
                
                //绑定银行卡
                BindBankCardViewController *bankVC = [[BindBankCardViewController alloc] init];
                [self.navigationController pushViewController:bankVC animated:YES];
            }
                break;
            case 3: {
                
                //修改密码
                ModifyPassWordViewController *modifyVC = [[ModifyPassWordViewController alloc] init];
                [self.navigationController pushViewController:modifyVC animated:YES];
            }
                break;
            case 4: {
                
                //收货地址
                ReceiverAddressViewController *receiverVC = [[ReceiverAddressViewController alloc] init];
                [self.navigationController pushViewController:receiverVC animated:YES];
            }
                break;
            case 5: {
                
                //聊天设置
                ChatSettingViewController *receiverVC = [[ChatSettingViewController alloc] init];
                [self.navigationController pushViewController:receiverVC animated:YES];
            }
                break;
        }
    } else {
        switch (indexPath.row) {
            case 0: {
                
                //修改昵称
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"register_name") preferredStyle:UIAlertControllerStyleAlert];
                __weak typeof(alert) weakAlert = alert;
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.nickName = [NSString stringWithFormat:@"%@",[weakAlert.textFields.lastObject text]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    });
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    NSLog(@"点击了取消按钮");
                }]];
                
                // 添加文本框
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.textColor = [UIColor blackColor];
                    [textField addTarget:self action:@selector(usernameDidChange:) forControlEvents:UIControlEventEditingChanged];
                }];
                
                // 弹出对话框
                [self presentViewController:alert animated:true completion:nil];
            }
                break;
            case 1: {
                
                //清除缓存
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"确定清除所有缓存数据吗？") message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [XSTool showProgressHUDWithView:self.view];
                    [[XSCacheManager sharedInstance] getAllCacheSize];
                    [[XSCacheManager sharedInstance] clearLocalCache];
                    //                    [[XSCacheManager sharedInstance] clearConversationCache];
                    [XSTool hideProgressHUDWithView:self.view];
                }];
                UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:action1];
                [alert addAction:action2];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
                break;
            case 2: {
                
                //关于
                AboutViewController *receiverVC = [[AboutViewController alloc] init];
                [self.navigationController pushViewController:receiverVC animated:YES];
                
            }
                break;
        }
    }
    
}

- (void)usernameDidChange:(UITextField *)username {
    NSLog(@"%@", username.text);
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return tableView.sectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 6;
    }else{
        return 3;
    }
}


@end
