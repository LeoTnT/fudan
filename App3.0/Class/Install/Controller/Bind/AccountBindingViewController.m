//
//  AccountBindingViewController.m
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AccountBindingViewController.h"
#import "AccountBindingCell.h"
#import "AccountBindingModel.h"
#import "BindEmailViewController.h"
#import "InputPassWordViewController.h"
#import <TencentOpenAPI/TencentOAuth.h>

@interface AccountBindingViewController ()<UITableViewDelegate,UITableViewDataSource,BindEmailDelegate,WXApiDelegate,TencentSessionDelegate>
{
    TencentOAuth *tencentOAuth;
}
/** 账号绑定tableView */
@property (nonatomic, strong) UITableView * accountBindTableView;
/** 账号绑定模式 */
@property (nonatomic, strong) NSMutableArray * bindMethodArray;

@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *logo;

@end

@implementation AccountBindingViewController

-(NSMutableArray *)bindMethodArray
{
    if (!_bindMethodArray) {
        _bindMethodArray = [[NSMutableArray alloc] init];
    }
    return _bindMethodArray;
}

-(UITableView *)accountBindTableView
{
    if (!_accountBindTableView) {
        _accountBindTableView = [[UITableView alloc] init];
        _accountBindTableView.showsVerticalScrollIndicator = NO;
        _accountBindTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _accountBindTableView.bounces = NO;
        _accountBindTableView.backgroundColor = BG_COLOR;
    }
    return _accountBindTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weiXinToLoginIn:) name:@"WeixinCodeNotification" object:nil];
    self.navigationItem.title = Localized(@"setting_tv_baccount");
    self.view.backgroundColor = BG_COLOR;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self addSubviews];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getInfo];
}

- (void)addSubviews {
    [self.view addSubview:self.accountBindTableView];
    self.accountBindTableView.dataSource = self;;
    self.accountBindTableView.delegate = self;
    [self.accountBindTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 52)];
    headerView.backgroundColor = BG_COLOR;
    self.accountBindTableView.tableHeaderView = headerView;
    
    //您已绑定的账号
    UILabel * tipsLab1 = [[UILabel alloc] init];
    tipsLab1.text = Localized(@"your_bind_account");
    tipsLab1.font = [UIFont systemFontOfSize:14];
    tipsLab1.textColor = [UIColor hexFloatColor:@"666666"];
    [headerView addSubview:tipsLab1];
    [tipsLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headerView).offset(12.5);
        make.baseline.mas_equalTo(headerView.mas_centerY).offset(-3);
    }];
    
    //绑定后，您便可使用绑定账号登录网站
    UILabel * tipsLab2 = [[UILabel alloc] init];
    tipsLab2.text = Localized(@"bind_account_des");
    tipsLab2.font = [UIFont systemFontOfSize:11];
    tipsLab2.textColor = [UIColor hexFloatColor:@"666666"];
    [headerView addSubview:tipsLab2];
    [tipsLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headerView).offset(12.5f);
        make.top.mas_equalTo(tipsLab1.mas_baseline).offset(6.5f);
    }];
}

#pragma mark - 账号绑定状态数据请求
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager requestAccountBindingData:nil success:^(NSDictionary *dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        @strongify(self);
        if (state.status) {
            [self.bindMethodArray removeAllObjects];
            //            BindParser *parser = [[BindParser alloc] initWithDictionary:dic];
            [self.bindMethodArray setArray:[AccountBindingModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            
            [self.accountBindTableView reloadData];
        } else {
            [MBProgressHUD showMessage:dic[@"info"] view:self.view];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}

#pragma mark tableView delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bindMethodArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellID = @"AccountBindingCellID";
    AccountBindingCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AccountBindingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.bindMethodArray.count > 0) {
        AccountBindingModel * model = self.bindMethodArray[indexPath.row];
        [cell setDataWithModel:model];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.bindMethodArray.count > 0) {
        AccountBindingModel * model = self.bindMethodArray[indexPath.row];
        if ([model.type isEqualToString:@"email"]) {
            //邮箱绑定
            if ([model.status integerValue] == 1) {
                BindEmailViewController * bindEmailVC = [[BindEmailViewController alloc] init];
                bindEmailVC.delegate = self;
                [self.navigationController pushViewController:bindEmailVC animated:YES];
            } else {
                BindEmailViewController * bindEmailVC = [[BindEmailViewController alloc] init];
                bindEmailVC.delegate = self;
                [self.navigationController pushViewController:bindEmailVC animated:YES];
            }
        } else if ([model.type isEqualToString:@"mobile"]) {
            //换绑手机号
            if ([model.status integerValue] == 1) {
                InputPassWordViewController *controller = [[InputPassWordViewController alloc] init];
                [self.navigationController pushViewController:controller animated:YES];
            } 
        } else if ([model.type isEqualToString:@"qq"]) {
            //qq绑定与解绑
            if ([model.status integerValue] == 1) {
                [self unbindAccountWithType:model.type];
            } else {
                if ([TencentOAuth iphoneQQInstalled]) {
                    tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];   //注册
                    NSArray* permissions = [NSArray arrayWithObjects:
                                            kOPEN_PERMISSION_GET_USER_INFO,
                                            kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                                            nil];
                    [tencentOAuth authorize:permissions]; //授权
                    [tencentOAuth incrAuthWithPermissions:permissions];
                } else {
                    [XSTool showToastWithView:self.view Text:@"尚未安装QQ客户端"];
                }
            }
        } else if ([model.type isEqualToString:@"weixin"]) {
            //微信绑定与解绑
            if ([model.status integerValue] == 1) {
                [self unbindAccountWithType:model.type];
            } else {
                if ([WXApi isWXAppInstalled]) {
                    SendAuthReq* req = [[SendAuthReq alloc] init];
                    req.scope = WXAuthScope; // @"post_timeline,sns"
                    req.state = WXAuthReqState;
                    req.openID = WXAPPID;
                    [WXApi sendAuthReq:req viewController:self delegate:self];
                }else{
                    [XSTool showToastWithView:self.view Text:@"尚未安装微信客户端"];
                }
            }
        }
    }
}
#pragma mark BindEmailDelegate
- (void)bindEmailSuccess {
    [self getInfo];
}

- (void)weiXinToLoginIn:(NSNotification *)notification {
    NSString *code = notification.userInfo[@"weiXinReturnCode"];
    [self getBindUserOrSocialInformationWithType:@"weixin" code:code openid:nil];
}

#pragma mark 获取绑定信息----
- (void)getBindUserOrSocialInformationWithType:(NSString *) type code:(NSString *) code openid:(NSString *) openid {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getAccountBindingInfoWithType:type code:code openid:openid success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            ThirdPartyParser *partyParser = [ThirdPartyParser mj_objectWithKeyValues:dic[@"data"]];
            if ([partyParser.status integerValue]==1) {
                [XSTool showToastWithView:self.view Text:@"此账号已被其他账号绑定，请先解绑！"];
                [self getInfo];
            } else {
                [self toBindTheAccountWithModel:partyParser Type:type code:code];
            }
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
#pragma mark 绑定账号
- (void)toBindTheAccountWithModel:(ThirdPartyParser *)model Type:(NSString *) type code:(NSString *) code {
    NSString * nickName = [type isEqualToString:@"qq"]?self.nickName:model.nickname;
    NSString * logo = [type isEqualToString:@"qq"]?self.logo:model.logo;
    NSString * sex = [type isEqualToString:@"qq"]?self.sex:model.sex;
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager toBindAccountWithIscreate:@"2" type:type openid:model.openid unionid:model.unionid token:nil name:nil pass:nil verify:nil nickname:nickName logo:logo sex:sex intro:nil success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        
        if (state.status) {
//            [self performSelector:@selector(refreshData) withObject:nil afterDelay:1.0f];
            //刷新界面
            [self getInfo];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
#pragma mark 解除绑定（QQ&&微信）
- (void)unbindAccountWithType:(NSString *)type {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager unbindTheAccountWithType:type success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:dic[@"info"]];
        if (state.status) {
            [self performSelector:@selector(refreshData) withObject:nil afterDelay:1.25f];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
-(void)refreshData {
    [self getInfo];
}

- (void)qqLogin:(UIButton *)sender {
    if ([TencentOAuth iphoneQQInstalled]) {
        tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];   //注册
        NSArray* permissions = [NSArray arrayWithObjects:kOPEN_PERMISSION_GET_USER_INFO,kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,nil];
        [tencentOAuth authorize:permissions];
        [tencentOAuth incrAuthWithPermissions:permissions];
    } else {
        [XSTool showToastWithView:self.view Text:@"尚未安装QQ客户端"];
    }
}

- (void)tencentDidLogin {
    if (tencentOAuth.accessToken && 0 != [tencentOAuth.accessToken length]){
        //  记录登录用户的OpenID、Token以及过期时间
        //token =46983A7B5D5B8F8A01257B69AE258B6D    data =Wed Aug 23 11:42:34 2017
        XSLog(@"token =%@    data =%@",tencentOAuth.accessToken,tencentOAuth.expirationDate);
        self.token = tencentOAuth.accessToken;
        self.openId = tencentOAuth.openId;
        
        [tencentOAuth getUserInfo];
//        [XSTool showToastWithView:self.view Text:@"授权成功"];
    } else {
        [XSTool showToastWithView:self.view Text:@"登录不成功"];
//        [self getInfo];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled {
    if (cancelled){
        [XSTool showToastWithView:self.view Text:@"用户取消登录"];
//        [self getInfo];
    }else {
        [XSTool showToastWithView:self.view Text:@"登录失败"];
//        [self getInfo];
    }
}

- (void)tencentDidNotNetWork {
    [XSTool showToastWithView:self.view Text:@"无网络连接，请设置网络"];
}

- (void)getUserInfoResponse:(APIResponse *)response {
    [self analysisResponse:response];
}

- (void)analysisResponse:(APIResponse *)notify {
    if (notify) {
        APIResponse *response = notify;
        if (URLREQUEST_SUCCEED == response.retCode && kOpenSDKErrorSuccess == response.detailRetCode) {
            self.nickName = response.jsonResponse[@"nickname"];
            self.logo = response.jsonResponse[@"figureurl_qq_1"];
            self.sex = [NSString stringWithFormat:@"%@",[response.jsonResponse[@"gender"] isEqualToString:Localized(@"person_info_gender_man")]?@"0":@"1"];
            [self getBindUserOrSocialInformationWithType:@"qq" code:nil openid:self.openId];
        } else {
            NSString *errMsg = [NSString stringWithFormat:@"errorMsg:%@\n%@", response.errorMsg, [response.jsonResponse objectForKey:@"msg"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Localized(@"操作失败") message:errMsg delegate:self cancelButtonTitle:Localized(@"我知道啦") otherButtonTitles: nil];
            [alert show];
        }
    }
}

#pragma mark dealloc
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WeixinCodeNotification" object:nil];
}

@end
