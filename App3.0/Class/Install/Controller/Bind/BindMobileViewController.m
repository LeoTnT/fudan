//
//  BindMobileViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BindMobileViewController.h"
#import "RSAEncryptor.h"
#import "WXApi.h"

@interface BindMobileViewController ()
@property (nonatomic, copy)NSString *mobile;

@property (strong, nonatomic) UITextField *tfPhone;
@property (strong, nonatomic) UITextField *tfCode;
@property (strong, nonatomic) XSCustomButton *codeBtn;
@property (strong, nonatomic) XSCustomButton *nextBtn;
@property (assign, nonatomic) BOOL isTimer;
@end

@implementation BindMobileViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.navigationItem.title = Localized(@"bind_mobile_change");
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    [self addSubviews];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        UIViewController *controller =  self.navigationController.viewControllers[self.navigationController.viewControllers.count-3];
        [self.navigationController popToViewController:controller animated:YES];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSubviews {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, mainWidth, 100)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgView];
    
    // textField-leftView
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    // 用户名输入框
    _tfPhone = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, mainWidth, 30)];
    
    
    _tfPhone.font = [UIFont systemFontOfSize:14];
    _tfPhone.leftView = leftView;
    _tfPhone.leftViewMode = UITextFieldViewModeAlways;
    _tfPhone.keyboardType = UIKeyboardTypeNumberPad;
    [_tfPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_tfPhone];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(60, CGRectGetMaxY(_tfPhone.frame)+10, mainWidth-60, 1)];
    lineView.backgroundColor = BG_COLOR;
    [bgView addSubview:lineView];
    
    leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    _codeBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(0, 0, 100, 40) title:Localized(@"register_sms") titleColor:[UIColor whiteColor] fontSize:14 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [_codeBtn setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    [_codeBtn setBorderWith:0 borderColor:[BG_COLOR CGColor] cornerRadius:3];
    [_codeBtn addTarget:self action:@selector(getCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    _codeBtn.enabled = NO;
    [rightView addSubview:_codeBtn];
    // 验证码输入框
    _tfCode = [[UITextField alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lineView.frame)+10, mainWidth, 30)];
    _tfCode.placeholder = Localized(@"sms_send_dialog_sms_hint");
    _tfCode.font = [UIFont systemFontOfSize:14];
    _tfCode.leftView = leftView;
    _tfCode.leftViewMode = UITextFieldViewModeAlways;
    _tfCode.rightView = rightView;
    _tfCode.rightViewMode = UITextFieldViewModeAlways;
    _tfCode.keyboardType = UIKeyboardTypeNumberPad;
    [_tfCode addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_tfCode];
    
    //下一步
    _nextBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(bgView.frame)+10, mainWidth-20, 50) title:Localized(@"change_mobile_btn_text") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [_nextBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [_nextBtn setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    _nextBtn.enabled = NO;
    [_nextBtn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_nextBtn];
    
    if (self.mobile.length > 0) {
        _tfPhone.text = self.mobile;
        _tfPhone.enabled = NO;
        _codeBtn.enabled = YES;
    } else {
        _tfPhone.placeholder = Localized(@"input_mobile");
    }
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    BOOL phoneTrue = NO;
    BOOL codeTrue = NO;
    // 手机号码格式判断
    if (self.tfPhone.text.length == 11) {
        phoneTrue = YES;
        if (_isTimer == NO) {
            self.codeBtn.enabled = YES;
        } else {
            self.codeBtn.enabled = NO;
        }
    } else {
        phoneTrue = NO;
        self.codeBtn.enabled = NO;
    }
    
    // 验证码格式判断
    if (self.tfCode.text.length == 6) {
        codeTrue = YES;
    } else {
        codeTrue = NO;
    }
    
    // 都符合可进行下一步
    if (phoneTrue && codeTrue) {
        self.nextBtn.enabled = YES;
    } else {
        self.nextBtn.enabled = NO;
    }
}

#pragma mark - 获取验证码
-(void)getCodeAction:(UIButton *) sender{
    
    sender.enabled = NO; // 设置按钮为不可点击
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"code_tip") message:_tfPhone.text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = NO; // 设置按钮为不可点击
        
        // 获取验证码
        [HTTPManager getSmsVerifyWithDic:@{@"mobile":_tfPhone.text,@"changeMobile":@"1"} success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool showToastWithView:self.view Text:state.info];
            if (state.status) {
                self.isTimer = YES;
                __block NSInteger time = 59; //倒计时时间
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
                dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
                dispatch_source_set_event_handler(_timer, ^{
                    
                    if(time <= 0){ //倒计时结束，关闭
                        
                        dispatch_source_cancel(_timer);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //设置按钮的样式
                            [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                            sender.enabled = YES; // 设置按钮可点击
                            self.isTimer = NO;
                        });
                        
                    }else{
                        
                        int seconds = time % 60;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //设置按钮显示读秒效果
                            [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), seconds] forState:UIControlStateNormal];
                            
                        });
                        time--;
                    }
                });
                dispatch_resume(_timer);
            } else {
                sender.enabled = YES; // 设置按钮为可点击
            }
        } fail:^(NSError * _Nonnull error){
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            sender.enabled = YES; // 设置按钮为可点击
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = YES;
    }];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - 更换绑定
-(void)nextAction:(UIButton *) sender{
    @weakify(self);
    [HTTPManager setupMobile:self.tfPhone.text verify:self.tfCode.text token:isEmptyString(self.token)?@"":self.token success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"更换绑定成功！"];
            //修改沙盒存储信息
            NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
            NSData *data = [userDefaults1  objectForKey:USERINFO_LOGIN];
            LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            userInfo.mobile = self.tfPhone.text;
            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
            [userDefaults1 setObject:userData forKey:USERINFO_LOGIN];
            [userDefaults1 synchronize];
            // 实例化user数据
            [[UserInstance ShardInstnce] setupUserInfo];
            UIViewController *controller =  self.navigationController.viewControllers[self.navigationController.viewControllers.count-3];
            [self.navigationController popToViewController:controller animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }

    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        [XSTool hideProgressHUDWithView:self.view];
    }];
}
@end

