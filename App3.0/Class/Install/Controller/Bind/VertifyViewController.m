//
//  VertifyViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VertifyViewController.h"
#import "XSCustomButton.h"
#import "ProvinceViewController.h"
#import "TownViewController.h"
#import "IdentityCardModel.h"
#import "TOCropViewController.h"
#import "XSClipImage.h"
#import "UserModel.h"
#import "FansCircleModel.h"
#import "UIImage+XSWebImage.h"
#import "UIImageView+WebCache.h"

typedef NS_ENUM(NSInteger, SeletedPhotoType) {
    SeletedPhotoTypeNone,
    SeletedPhotoTypeFontIdentity,
    SeletedPhotoTypeBehindIdentity
};

@interface VertifyViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewDelegate>
@property (nonatomic, assign) SeletedPhotoType seletedPhotoType;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (nonatomic, strong) UITableView *tableView;//表格
@property (nonatomic, strong) UITextField *realName;//真实姓名
@property (nonatomic, strong) UITextField *number;//证件号
@property (nonatomic, strong) NSMutableArray *tempArray;//上传图片后返回的地址数组
@property (nonatomic, assign) BOOL keyBoardIsShow;
@property (nonatomic, assign) BOOL flag;
@property (nonatomic, assign) NSInteger apptove;
@property (nonatomic, strong) UIButton *fontButton;//正面照
@property (nonatomic, strong) UIButton *behindButton;//反面照
@property (nonatomic, strong) XSCustomButton *vertifyBtn;
@property (nonatomic, strong) UIImage *fontImage;//正面照
@property (nonatomic, strong) UIImage *behindImage;//反面照
@property (nonatomic, assign) CGFloat smallHeight;
@property (nonatomic, assign) CGFloat bigHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, copy) NSString *reason;
@end

@implementation VertifyViewController
#pragma mark - lazy loadding
-(XSCustomButton *)vertifyBtn {
    if (!_vertifyBtn) {
        _vertifyBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, NORMOL_SPACE*4) title:Localized(@"user_approve") titleColor:[UIColor whiteColor] fontSize:15 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [_vertifyBtn addTarget:self action:@selector(vertifyIdentify:) forControlEvents:UIControlEventTouchUpInside];
        [_vertifyBtn setBorderWith:0 borderColor:nil cornerRadius:5];;
    }
    return _vertifyBtn;
}

- (UIButton *)fontButton {
    if (!_fontButton) {
        _fontButton = [[UIButton alloc] initWithFrame:CGRectMake(0,NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, self.buttonHeight)];
        [_fontButton setTitle:Localized(@"user_approve_card_front") forState:UIControlStateNormal];
        _fontButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_fontButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_fontButton setImage:img forState:UIControlStateNormal];
        _fontButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(self.fontButton.imageView.frame), CGRectGetWidth(_fontButton.titleLabel.frame), 0, 0);
        _fontButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE*2, -CGRectGetWidth(_fontButton.imageView.frame), 0, 0);
        _fontButton.backgroundColor = BG_COLOR;
        [self.fontButton addTarget:self action:@selector(uploadFrontAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fontButton;
}

- (UIButton *)behindButton {
    if (!_behindButton) {
        _behindButton = [[UIButton alloc] initWithFrame:CGRectMake(0,NORMOL_SPACE, mainWidth-4*normal, self.buttonHeight)];
        [_behindButton setTitle:Localized(@"user_approve_card_back") forState:UIControlStateNormal];
        _behindButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_behindButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_behindButton setImage:img forState:UIControlStateNormal];
        _behindButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(self.behindButton.imageView.frame), CGRectGetWidth(_behindButton.titleLabel.frame), 0, 0);
        _behindButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE*2, -CGRectGetWidth(_behindButton.imageView.frame), 0, 0);
        _behindButton.backgroundColor = BG_COLOR;
        [self.behindButton addTarget:self action:@selector(uploadReverseAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _behindButton;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.sectionHeaderHeight = 20;
        _tableView.sectionFooterHeight = 0;
        _tableView.bounces = NO;
    }
    return _tableView;
}

- (NSMutableArray *)tempArray {
    if (!_tempArray) {
        _tempArray = [NSMutableArray array];
    }
    return _tempArray;
}

- (UITextField *)realName {
    if (!_realName) {
        _realName = [UITextField new];
        _realName.placeholder = Localized(@"user_approve_name_hint");
        _realName.delegate = self;
        _realName.textAlignment = NSTextAlignmentRight;
        _realName.tintColor = LINE_COLOR;
        _realName.returnKeyType = UIReturnKeyNext;
        //设置光标的位置
        
        _realName.font = [UIFont systemFontOfSize:13];
    }
    return _realName;
}

//- (UITextField *)detail {
//    if (!_detail) {
//
//        _detail = [UITextField new];
//        _detail.placeholder = Localized(@"register_addr");
//        _detail.delegate = self;
//        _detail.textAlignment = NSTextAlignmentRight;
//        _detail.tintColor = LINE_COLOR;
//        _detail.returnKeyType = UIReturnKeyDone;
//        //设置光标的位置
//        _detail.font = [UIFont systemFontOfSize:13];
//    }
//    return _detail;
//}

- (UITextField *)number {
    if (!_number) {
        
        _number = [UITextField new];
        _number.placeholder = Localized(@"user_approve_cardno_hint");
        _number.delegate = self;
        _number.textAlignment = NSTextAlignmentRight;
        _number.tintColor = LINE_COLOR;
        _number.returnKeyType = UIReturnKeyDone;
        _number.font = [UIFont systemFontOfSize:13];
    }
    return _number;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = BG_COLOR;
    self.smallHeight = 45;
    self.bigHeight = 220;
    self.buttonHeight = 160;
//    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = Localized(@"user_approve_suggest_no_btn");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.reason = @"";
    [self.view addSubview:self.tableView];
    [self getInfo];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    //    if (_isSelectedProvince) {
    //
    //        NSIndexPath *index1 = [NSIndexPath indexPathForRow:2 inSection:0];
    //        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index1, nil] withRowAnimation:UITableViewRowAnimationNone];
    //    }
    //    if (_isSelectedStreet) {
    //
    //        NSIndexPath *index2 = [NSIndexPath indexPathForRow:3 inSection:0];
    //        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index2, nil] withRowAnimation:UITableViewRowAnimationNone];
    //    }
    //
}
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    //这样就拿到了键盘的位置大小信息frame，然后根据frame进行高度处理之类的信息
    //    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    if([self.detail isFirstResponder]){
    //        [self.tableView setContentOffset:CGPointMake(0, 200)];
    //    }
    
    
}
- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    //    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    if ([self.detail isFirstResponder]) {
    //        [self.tableView setContentOffset:CGPointZero];
    //    }
    //
}

- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
        self.flag = YES;
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (self.flag) {
        
        //获取原始照片
        UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
        toVC.delegate=self;
        [picker presentViewController:toVC animated:NO completion:nil];
    } else {
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"]) {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
            toVC.delegate=self;
            [picker pushViewController:toVC animated:YES];
        }
    }
}
#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.flag) {
        //        [self.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
            //            XSLog(@"retain  count = %ld\n",CFGetRetainCount((__bridge  CFTypeRef)(weakSelf.imagePickerVC)));
            [weakSelf.imagePickVC dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [self dismissViewControllerAnimated:YES completion:^{
        NSIndexPath *index;
        if (self.seletedPhotoType==SeletedPhotoTypeFontIdentity) {
            self.fontImage = [XSClipImage scaleImage:image toScale:0.8];
            index  = [NSIndexPath indexPathForRow:0 inSection:1];
        } else {
            self.behindImage = [XSClipImage scaleImage:image toScale:0.8];
            index = [NSIndexPath indexPathForRow:1 inSection:1];
        }
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)uploadFrontAction:(UIButton *) sender {
    self.seletedPhotoType = SeletedPhotoTypeFontIdentity;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    
    //打开相机，选择图片，显示图片,上传图片
    [self choosePhotoes];
}

- (void)uploadReverseAction:(UIButton *) sender {
    self.seletedPhotoType = SeletedPhotoTypeBehindIdentity;
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    
    //打开相机，选择图片，显示图片,上传图片
    [self choosePhotoes];
}

- (void)choosePhotoes{
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //打开相册
        self.imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        self.imagePickVC.delegate = self;
        self.imagePickVC.allowsEditing = NO;
        
        [self presentViewController:self.imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)getInfo {
    
//    [HTTPManager getIdentifyInfoWithParams:@{@"type":@"1"} success:^(NSDictionary *dic, resultObject *state) {
//        if (state.status) {
//            
//        } else {
//        
//        }
//    } failure:^(NSError *error) {
//        
//    }];
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            UserParser *parser = [UserParser mj_objectWithKeyValues:dic];
            self.apptove = [parser.data.approve_user intValue];
            if (self.apptove==-1) {
                self.reason = Localized(@"user_approve_process_detail");
            } else if (self.apptove==0){
                self.reason = Localized(@"shenhezhong");
            }else if (self.apptove==1){
                self.reason = Localized(@"user_approve_process_over");
                
            } else {
                self.reason = [NSString stringWithFormat:@"%@，%@",Localized(@"认证失败"),parser.data.comment];
            }
            [self.tableView reloadData];
            if (self.apptove!=-1) {
                
                self.realName.text = parser.data.truename;
                self.number.text = parser.data.card_no;
                NSArray *array = [parser.data.img_card componentsSeparatedByString:@","];
                if (array.count) {
                    for (int i=0; i<array.count; i++) {
                        NSData *logoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:array[i]]];
                        if (i==0) {
                            self.fontImage = [UIImage imageWithData:logoData];
                            if (self.fontImage) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    self.seletedPhotoType = SeletedPhotoTypeFontIdentity;
                                    NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:1];
                                    [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                                });
                            }
                            
                        } else if (i==1) {
                            self.behindImage = [UIImage imageWithData:logoData];
                            if (self.behindImage) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    self.seletedPhotoType = SeletedPhotoTypeBehindIdentity;
                                    NSIndexPath *index2 = [NSIndexPath indexPathForRow:1 inSection:1];
                                    [self.tableView reloadRowsAtIndexPaths:@[index2] withRowAnimation:UITableViewRowAnimationNone];
                                    
                                });
                            }
                            
                        }
                    }
                    
                }

                //一个section刷新
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                if (self.apptove==0||self.apptove==1) {
                    //禁止用户交互
                    self.realName.enabled = NO;
                    self.number.enabled = NO;
                    self.fontButton.userInteractionEnabled = NO;
                    self.behindButton.userInteractionEnabled = NO;
                    self.vertifyBtn.enabled = NO;
                }
                
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showProgressHUDTOView:self.view withText:NetFailure];
    }];
}

- (void)vertifyIdentify:(UIButton *) sender {
    if (self.apptove==-1||self.apptove==2) {
        //实名认证
        [self submitInfo];
    } else {
        [XSTool showProgressHUDTOView:self.view withText:@"已提交实名认证，审核中"];
    }
}

- (void)submitInfo {
    if ([self.tempArray count]>0    ) {
        [self.tempArray removeAllObjects];
    }
    if (self.fontImage) {
        [self.tempArray addObject:self.fontImage];
    }
    if (self.behindImage) {
        [self.tempArray addObject:self.behindImage];
    }
    //图片上传
    //    [self.tempArray addObjectsFromArray:@[self.fontImage,self.behindImage]];
    
    //信息完整
    if (self.tempArray.count==2&&self.realName.text.length>0&&(![self.realName.text isEqualToString:@" "])&&self.number.text.length>0&&(![self.number.text isEqualToString:@" "])) {
        NSDictionary *param=@{@"type":@"idcard",@"formname":@"file"};
        NSMutableString *tempImageString=[NSMutableString string];
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager upLoadPhotosWithDic:param andDataArray:self.tempArray WithSuccess:^(NSDictionary *dic, BOOL state) {
            if([dic[@"status"] integerValue]==1){//发表
                for (NSString *str in dic[@"data"]) {
                    [tempImageString appendString:[NSString stringWithFormat:@"%@,",str]];
                }
                NSString  *submitImageString = [tempImageString substringToIndex:tempImageString.length-1];
                
                //提交认证
                NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                NSString *nameString = [[NSString alloc]initWithString:[self.realName.text stringByTrimmingCharactersInSet:whiteSpace]];
                NSString *numberString = [[NSString alloc]initWithString:[self.number.text stringByTrimmingCharactersInSet:whiteSpace]];
                [HTTPManager submitIdentityCarInfoWithImageString:submitImageString trueName:nameString card:numberString  Success:^(NSDictionary * _Nullable dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];;
                    if (state.status) {
                        [XSTool showToastWithView:self.view Text:state.info];
                        [self performSelector:@selector(popViewController) withObject:self afterDelay:0.5];
                        
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                    
                } failure:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                    
                }];
                
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善认证信息"];
    }
    
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *topIdString = @"topId", *bigIdString = @"bigId";
    if (indexPath.section==0) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:topIdString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //内容
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2*NORMOL_SPACE, NORMOL_SPACE, NORMOL_SPACE*8, self.smallHeight-2*NORMOL_SPACE)];
        titleLabel.font = [UIFont systemFontOfSize:15];
        [cell.contentView addSubview:titleLabel];
        switch (indexPath.row) {
            case 0: {
                titleLabel.text = Localized(@"user_approve_name");
                self.realName.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-NORMOL_SPACE*4-CGRectGetWidth(titleLabel.frame), CGRectGetHeight(titleLabel.frame));
                [cell.contentView addSubview:self.realName];
                
            }
                break;
            case 1: {
                titleLabel.text = Localized(@"user_approve_cardno");
                self.number.frame =  CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-NORMOL_SPACE*4-CGRectGetWidth(titleLabel.frame), CGRectGetHeight(titleLabel.frame));
                [cell.contentView addSubview:self.number];
                
            }
                break;
        }
        
        return cell;
        
    } else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:bigIdString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //内容
        UIView *bView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.bigHeight)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, NORMOL_SPACE*12, self.smallHeight-NORMOL_SPACE*2)];
        titleLabel.font = [UIFont systemFontOfSize:15];
        [bView addSubview:titleLabel];
        UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-CGRectGetMaxX(titleLabel.frame)-NORMOL_SPACE,self.smallHeight-NORMOL_SPACE*2)];
        hintLabel.font = [UIFont systemFontOfSize:12];
        hintLabel.textAlignment = NSTextAlignmentRight;
        hintLabel.text = Localized(@"上传图片大小控制在3M以内！");
        hintLabel.textColor = [UIColor redColor];
        [bView addSubview:hintLabel];
        if (indexPath.row==0) {
            titleLabel.text  =Localized(@"user_approve_card_front");
            self.fontButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, self.buttonHeight);
            [bView addSubview:self.fontButton];
            [self.fontButton setBackgroundImage:self.fontImage forState:UIControlStateNormal];
            if (self.seletedPhotoType == SeletedPhotoTypeFontIdentity) {
                [self.fontButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
                [self.fontButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                self.fontButton.backgroundColor = FANS_BG_BUTTON;
            }
            
            [cell.contentView addSubview:bView];
        } else {
            titleLabel.text  =Localized(@"user_approve_card_back");
            self.behindButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, self.buttonHeight);
            [bView addSubview:self.behindButton];
            [self.behindButton setBackgroundImage:self.behindImage forState:UIControlStateNormal];
            
            if (self.seletedPhotoType == SeletedPhotoTypeBehindIdentity) {
                [self.behindButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
                [self.behindButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                self.behindButton.backgroundColor = FANS_BG_BUTTON;
            }
            [cell.contentView addSubview:bView];
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if (indexPath.section==0) {
    //        if (indexPath.row==2) {
    //            [self.view endEditing:YES];
    //            ProvinceViewController *provinceVC = [[ProvinceViewController alloc] init];
    //            [self.navigationController pushViewController:provinceVC animated:YES];
    //            _isSelectedProvince = YES;
    //            [self.view endEditing:YES];
    //
    //        }
    //        if (indexPath.row==3) {
    //            if (_isSelectedProvince) {
    //                [self.view endEditing:YES];
    //                TownViewController *townVC = [[TownViewController alloc] init];
    //                townVC.areaId = [[self.areaIdArray lastObject] integerValue];
    //                [self.navigationController pushViewController:townVC animated:YES];
    //                _isSelectedStreet = YES;
    //            }
    //            [self.view endEditing:YES];
    //
    //        }
    //    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 2;
    } else {
        return 2;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return self.smallHeight;
    } else {
        return self.bigHeight;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        if (self.apptove==2) {
            return NORMOL_SPACE*14;
        }
        return NORMOL_SPACE*12;
    } else {
        return tableView.sectionHeaderHeight;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==0) {
        return 0.1;
    } else {
        return NORMOL_SPACE*6;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth , NORMOL_SPACE*12)];
        UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, NORMOL_SPACE*4)];
        descLabel.text = Localized(@"说明：实名认证后可通过认证信息进行账号找回，保证资金安全！");
        descLabel.numberOfLines = 0;
        descLabel.font = [UIFont qsh_systemFontOfSize:12];
        descLabel.textColor = mainGrayColor;
        [headerView addSubview:descLabel];
        UILabel *progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE+CGRectGetMaxY(descLabel.frame), mainWidth-4*NORMOL_SPACE, NORMOL_SPACE*4)];
        UIColor *color =  [UIColor hexFloatColor:@"FFDEAD"];
        progressLabel.backgroundColor = color;
        progressLabel.font = [UIFont qsh_systemFontOfSize:14];
        
        NSLog(@"%@",self.reason);
        NSArray *array ;
        if (self.apptove==2) {
            array = [self.reason componentsSeparatedByString:@"，"];
            progressLabel.text = [NSString stringWithFormat:@"   审核进度：%@！",[array firstObject]];
        } else {
            progressLabel.text = [NSString stringWithFormat:@"   审核进度：%@！",self.reason];
        }
        
        
        NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc]initWithString:progressLabel.text];
        NSUInteger index = [[noteStr string] rangeOfString:@"："].location+1;
        NSUInteger index2 = [[noteStr string] rangeOfString:@"！"].location+1;
        NSRange range = NSMakeRange(index, index2-index);
        [noteStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range];
        [progressLabel setAttributedText:noteStr];
        progressLabel.layer.cornerRadius = 5;
        progressLabel.layer.masksToBounds = YES;
        [headerView addSubview:progressLabel];
        if (self.apptove==2) {
            UILabel *reasonLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(progressLabel.frame), CGRectGetMaxY(progressLabel.frame)+NORMOL_SPACE, CGRectGetWidth(progressLabel.frame), 20)];
            reasonLabel.text = [NSString stringWithFormat:@"   审核理由：%@",[array lastObject]];
            reasonLabel.font = [UIFont qsh_systemFontOfSize:14];
            reasonLabel.textColor = [UIColor lightGrayColor];
            [headerView addSubview:reasonLabel];
        }
        
        return headerView;
        
    } else {
        return nil;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section==1) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE*6)];
        self.vertifyBtn.frame = CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, mainWidth-4*NORMOL_SPACE, NORMOL_SPACE*4);
        [footerView addSubview:self.vertifyBtn];
        return footerView;
    } else {
        return nil;
    }
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField==_realName) {
        return [_number becomeFirstResponder];
    } else {
        return [_number resignFirstResponder];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [_realName resignFirstResponder];
    [_number resignFirstResponder];
    //    [_detail resignFirstResponder];
    
}

@end
