//
//  BindBankCardViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindBankCardViewController : XSBaseTableViewController

@property (nonatomic, assign) NSInteger apptove;//审核状态，实名认证
@property (nonatomic, assign) NSUInteger number;

- (void)getInfo;
@end
