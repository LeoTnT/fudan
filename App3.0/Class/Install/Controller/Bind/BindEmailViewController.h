//
//  BindEmailViewController.h
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
@protocol BindEmailDelegate <NSObject>

@optional
- (void)bindEmailSuccess;

@end
@interface BindEmailViewController : XSBaseViewController

@property (nonatomic,assign) BOOL isApprove;

/** delegate */
@property (nonatomic, weak) id<BindEmailDelegate>delegate;

@end
