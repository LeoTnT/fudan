//
//  RealNameVerificationViewController.m
//  App3.0
//
//  Created by admin on 2017/12/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RealNameVerificationViewController.h"
#import "RealNameVerificationModel.h"
#import "RealNameVerifyHeaderView.h"
#import "TOCropViewController.h"
#import "XSClipImage.h"

@interface RealNameVerificationViewController ()<UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate,RealNameVerificationDelegate>

/** 表格 */
@property (nonatomic, strong) UITableView * verificationTableView;

@property (nonatomic, strong) UIImagePickerController *imagePickerVC;
@property (nonatomic, assign) BOOL flag;

/** 图片数组 */
@property (nonatomic, strong) NSMutableArray * idCardImages;
/** headerView */
@property (nonatomic, strong) RealNameVerifyHeaderView * headerView;
/** 是否是正面照 */
@property (nonatomic, assign) BOOL isFront;
/** 正面照 */
@property (nonatomic, strong) UIImage * frontImage;
/** 反面照 */
@property (nonatomic, strong) UIImage * behindImage;

@end

@implementation RealNameVerificationViewController

-(NSMutableArray *)idCardImages
{
    if (!_idCardImages) {
        _idCardImages = [[NSMutableArray alloc] init];
    }
    return _idCardImages;
}

-(UITableView *)verificationTableView
{
    if (!_verificationTableView) {
        _verificationTableView = [[UITableView alloc] init];
        _verificationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _verificationTableView.backgroundColor = BG_COLOR;
        _verificationTableView.delegate = self;
        _verificationTableView.showsVerticalScrollIndicator = NO;
    }
    return _verificationTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = Localized(@"user_approve_suggest_no_btn");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setUpUI];
    [self getVerificationInfoData];
}
#pragma mark UI
- (void)setUpUI {
    [self.view addSubview:self.verificationTableView];
    [self.verificationTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.headerView = [[RealNameVerifyHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 809)];
    self.headerView.delegate = self;
    self.verificationTableView.tableHeaderView = self.headerView;
}

#pragma mark RealNameVerificationDelegate
-(void)frontImageBtnClicked {
    self.isFront = YES;
    [self choosePhotoes];
}
-(void)behindImageBtnClicked {
    self.isFront = NO;
    [self choosePhotoes];
}
-(void)upLoadDataWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum {
    [self submitInfoWithRealName:realName IDCardNum:idCardNum];
}

#pragma mark 获取认证进度
- (void)getVerificationInfoData {
    [XSTool showProgressHUDWithView:self.view];
    
    @weakify(self);
    [HTTPManager requestVerificationData:nil success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            XSLog(@"%@",dic);
            RealNameVerificationModel * model = [RealNameVerificationModel mj_objectWithKeyValues:dataDict];
            
            self.frontImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.img_card[0]]]];
            self.behindImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.img_card[1]]]];
            
            [self setData:model];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}
#pragma mark 实名认证进度状态
- (void)setData:(RealNameVerificationModel *)model {

//    model.status = @"2";
    
    if ([model.status integerValue] == 0 || [model.status integerValue] == 1) {
        self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 715);
        [self.headerView setFooterViewHide:YES];
    } else {
        self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 809);
        [self.headerView setFooterViewHide:NO];
    }
    [self.verificationTableView reloadData];
    [self.headerView setStatus:model];
}

#pragma mark scroll tableView滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.headerView.isKeyboardShow) {
        [self.view endEditing:YES];
    }
}

#pragma mark 身份证图片选择与上传
- (void)choosePhotoes{
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //打开相册
        UIImagePickerController * imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        imagePickVC.delegate = self;
        imagePickVC.allowsEditing = NO;
        self.imagePickerVC = imagePickVC;
        
        self.flag = NO;
        
        [self presentViewController:imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)takeAPhoto {
    UIImagePickerController * imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    imagePickVC.delegate=self;
    self.imagePickerVC = imagePickVC;
    [self presentViewController:imagePickVC animated:YES completion:^{
        self.flag = YES;
    }];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (self.flag) {
        //获取原始照片
        UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
        toVC.delegate=self;
       
//        [picker pushViewController:toVC animated:YES];
        
        [picker presentViewController:toVC animated:NO completion:nil];
        
    } else {
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"]) {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
            toVC.delegate=self;
            [picker pushViewController:toVC animated:YES];
        }
    }
}

#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.flag) {
//        [self.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
//            XSLog(@"retain  count = %ld\n",CFGetRetainCount((__bridge  CFTypeRef)(weakSelf.imagePickerVC)));
            [weakSelf.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        }];
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.isFront) {
            self.frontImage = [XSClipImage scaleImage:image toScale:0.8];
            [self.headerView setFrontImage:self.frontImage];
        } else {
            self.behindImage = [XSClipImage scaleImage:image toScale:0.8];
            [self.headerView setBehindImage:self.behindImage];
        }
    }];
}

#pragma mark 提交实名信息数据
- (void)submitInfoWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum {
    if (self.idCardImages.count) {
        [self.idCardImages removeAllObjects];
    }
    if (self.frontImage) {
        [self.idCardImages addObject:self.frontImage];
    }
    if (self.behindImage) {
        [self.idCardImages addObject:self.behindImage];
    }
    
    if (self.idCardImages.count == 2 && realName.length > 0 && idCardNum.length > 0) {
        NSDictionary *param=@{@"type":@"idcard",@"formname":@"file"};
        NSMutableString *tempImageString=[NSMutableString string];
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager upLoadPhotosWithDic:param andDataArray:self.idCardImages WithSuccess:^(NSDictionary *dic, BOOL state) {
            @strongify(self);
            if([dic[@"status"] integerValue]==1){//发表
                for (NSString *str in dic[@"data"]) {
                    [tempImageString appendString:[NSString stringWithFormat:@"%@,",str]];
                }
                NSString  *submitImageString = [tempImageString substringToIndex:tempImageString.length-1];
                //提交认证
                NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                NSString *nameString = [[NSString alloc]initWithString:[realName stringByTrimmingCharactersInSet:whiteSpace]];
                NSString *numberString = [[NSString alloc]initWithString:[idCardNum stringByTrimmingCharactersInSet:whiteSpace]];
                [HTTPManager submitIdentityCarInfoWithImageString:submitImageString trueName:nameString card:numberString  Success:^(NSDictionary * _Nullable dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [XSTool showToastWithView:self.view Text:state.info];
                        [self performSelector:@selector(popViewController) withObject:self afterDelay:0.5];
                        
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } failure:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善认证信息"];
    }
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
