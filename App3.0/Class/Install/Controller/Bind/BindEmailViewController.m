//
//  BindEmailViewController.m
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BindEmailViewController.h"
#import "HTTPManager+Mine.h"
#import "RSAEncryptor.h"

@interface BindEmailViewController ()<UITextFieldDelegate>

/** 密码输入框 */
@property (nonatomic, strong) UITextField * pwdTF;
/** 邮箱输入框 */
@property (nonatomic, strong) UITextField * emailTF;
@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation BindEmailViewController
{
    BOOL _isTimer;// 是否在倒计时
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = Localized(@"绑定邮箱");
    self.view.backgroundColor = BG_COLOR;
    
    [self setUpUI];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setIsApprove:(BOOL)isApprove {
    _isApprove = isApprove;
    self.navigationItem.title = isApprove ? Localized(@"解绑邮箱"):Localized(@"绑定邮箱");
}

#pragma mark 设置UI
- (void)setUpUI {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIView * bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).offset(12);
        make.height.mas_equalTo(88);
    }];
    
    //认证进度中间线
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor hexFloatColor:@"e6e6e6"];
    [self.view addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bgView);
        make.left.mas_equalTo(self.view).offset(40);
        make.right.mas_equalTo(self.view);
        make.height.mas_equalTo(0.5);
    }];
    
    UIImageView * imageV1 = [[UIImageView alloc] init];
    imageV1.image = [UIImage imageNamed:@"user_bind_email_pwd"];
    [self.view addSubview:imageV1];
    
    [imageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.top.mas_equalTo(bgView).offset(11);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    UIImageView * imageV2 = [[UIImageView alloc] init];
    imageV2.image = [UIImage imageNamed:@"user_bind_email_email"];
    [self.view addSubview:imageV2];
    
    [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.bottom.mas_equalTo(bgView).offset(-9);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    //真实姓名输入框
    self.pwdTF = [[UITextField alloc] init];
    self.pwdTF.placeholder = Localized(@"retrieve_pwd");
    self.pwdTF.secureTextEntry = YES;
    self.pwdTF.textColor = [UIColor hexFloatColor:@"171717"];
    self.pwdTF.font = [UIFont systemFontOfSize:14];
    self.pwdTF.returnKeyType = UIReturnKeyNext;
    self.pwdTF.delegate = self;
    [self.view addSubview:self.pwdTF];
    
    [self.pwdTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageV1.mas_right).offset(11.5);
        make.right.mas_equalTo(bgView).offset(-13);
        make.centerY.mas_equalTo(imageV1);
        make.height.mas_equalTo(30);
    }];
    
    //身份证号输入框
    self.emailTF = [[UITextField alloc] init];
    self.emailTF.placeholder = Localized(@"请输入邮箱帐号");
    self.emailTF.textColor = [UIColor hexFloatColor:@"171717"];
    self.emailTF.font = [UIFont systemFontOfSize:14];
    self.emailTF.returnKeyType = UIReturnKeyDone;
    self.emailTF.delegate = self;
    [self.view addSubview:self.emailTF];
    
    [self.emailTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageV2.mas_right).offset(13);
        make.right.mas_equalTo(self.view).offset(-13);
        make.centerY.mas_equalTo(imageV2);
        make.height.mas_equalTo(30);
    }];
    
    UIButton * bindBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bindBtn setTitle:Localized(@"bind_btn_text") forState:UIControlStateNormal];
    bindBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [bindBtn setBackgroundColor:mainColor];
    bindBtn.layer.cornerRadius = 4;
    bindBtn.layer.masksToBounds = YES;
    [bindBtn addTarget:self action:@selector(bindBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bindBtn];
    [bindBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_bottom).offset(43.5f);
        make.centerX.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(FontNum(325), FontNum(50)));
    }];
}

- (void)bindBtnClick {
    if (self.pwdTF.text.length > 0 && self.emailTF.text.length > 0) {
        [self bindEmail];
    } else {
        if (self.pwdTF.text.length == 0) {
            [XSTool showToastWithView:self.view Text:Localized(@"请输入密码")];
        } else {
            [XSTool showToastWithView:self.view Text:Localized(@"请输入邮箱帐号")];
        }
    }
}
#pragma mark 绑定邮箱请求
- (void)bindEmail {
    [XSTool showProgressHUDWithView:self.view];
    
    NSString * rsaPwdString = [RSAEncryptor encryptString:self.pwdTF.text];
    @weakify(self)
    [HTTPManager bindEmailRequestWithPassword:rsaPwdString Email:self.emailTF.text success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:dic[@"info"]];
        if ([dic[@"status"] integerValue] == 1) {
            if ([self.delegate respondsToSelector:@selector(bindEmailSuccess)]) {
                [self.delegate bindEmailSuccess];
                
                [self performSelector:@selector(popViewControll) withObject:nil afterDelay:1.2f];
            }
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

-(void)popViewControll
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
