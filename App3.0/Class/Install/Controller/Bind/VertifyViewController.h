//
//  VertifyViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"
@interface VertifyViewController : XSBaseViewController
@property (nonatomic, copy) NSString *area;
@property (nonatomic, strong) NSArray *areaIdArray;//省市区
@property (nonatomic, retain) AreaParser *parser;//街道信息
@end
