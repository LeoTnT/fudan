//
//  InputPassWordViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "InputPassWordViewController.h"
#import "XSCustomButton.h"
#import "BindMobileViewController.h"
#import "RSAEncryptor.h"

@interface InputPassWordViewController ()<UITextFieldDelegate>
{
    BOOL _isTimer;// 是否在倒计时
}
@property (nonatomic, strong) UIButton *getCodeBtn;
@property (nonatomic, strong) UITextField *tfVerificationCode;
@property (nonatomic, strong) XSCustomButton *nextButton;
@property (nonatomic ,strong) dispatch_source_t timer;
@end

@implementation InputPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.navigationItem.title = Localized(@"bind_mobile_change");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, mainWidth, 50)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];

//    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_code"]];
//    [leftView2 addSubview:leftImg2];
    _getCodeBtn =[[UIButton alloc] init];
    [bgView addSubview:_getCodeBtn];
    [_getCodeBtn setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
    [_getCodeBtn setTitleColor:[UIColor hexFloatColor:@"c0c0c0"] forState:UIControlStateDisabled];
    [_getCodeBtn setTitleColor:mainColor forState:UIControlStateNormal];
    _getCodeBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_getCodeBtn addTarget:self action:@selector(obtainCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    _getCodeBtn.enabled = YES;
    // 验证码输入框
    _tfVerificationCode = [[UITextField alloc] init];
    [bgView addSubview:_tfVerificationCode];
    _tfVerificationCode.placeholder = Localized(@"sms_send_dialog_sms_hint");
    _tfVerificationCode.font = [UIFont systemFontOfSize:15];
    _tfVerificationCode.leftView = leftImg2;
    _tfVerificationCode.leftViewMode = UITextFieldViewModeAlways;
    _tfVerificationCode.keyboardType = UIKeyboardTypeNumberPad;
    _tfVerificationCode.delegate=self;
    
    [_getCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-25);
        make.centerY.mas_equalTo(_tfVerificationCode);
    }];
    [_tfVerificationCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(25);
        make.centerY.mas_equalTo(bgView);
        //防止遮挡获取验证码
        make.width.mas_lessThanOrEqualTo(mainWidth-150);
    }];
    
    self.nextButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(bgView.frame)+2*NORMOL_SPACE, mainWidth-20, 50) title:Localized(@"bind_mobile_next") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [self.nextButton setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [self.nextButton setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    [self.nextButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nextButton];

}

#pragma mark - 获取验证码
-(void)obtainCodeAction:(UIButton *) sender{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"我们将发送短信验证码至已绑定的手机号") message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = NO; // 设置按钮为不可点击
        [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
        // 获取旧号码验证码
        [HTTPManager mobileVerifyWithSuccess:^(NSDictionary *dic, resultObject *state) {
            // 验证码发送成功
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
                _isTimer = YES; // 设置倒计时状态为YES
                sender.enabled = NO; // 设置按钮为不可点击
                
                _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                    //设置按钮的样式
                    dispatch_source_cancel(_timer);
                    [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                    sender.enabled = YES; // 设置按钮可点击
                    
                    _isTimer = NO; // 倒计时状态为NO
                } otherAction:^(int time) {
                    [sender setTitle:[NSString stringWithFormat:@"重新发送( %.2d )", time] forState:UIControlStateNormal];
                }];
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                sender.enabled = YES; // 设置按钮为可点击
            }
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
            sender.enabled = YES; // 设置按钮为可点击
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)nextAction:(UIButton *) sender {
    if (isEmptyString(self.tfVerificationCode.text)) {
        [XSTool showToastWithView:self.view Text:Localized(@"sms_send_dialog_sms_hint")];
    } else {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager mobileValidateWithVerify:self.tfVerificationCode.text success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BindMobileViewController *controller = [[BindMobileViewController alloc] init];
                controller.token = dic[@"data"];
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
       
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
