//
//  BindBankCardViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BindBankCardViewController.h"
#import "RealNameVerificationViewController.h"
#import "UserModel.h"
#import "BankCardModel.h"
#import "AddBankCardViewController.h"
#import "BankTableViewCell.h"
#import "MJRefresh.h"
#import "BankCardDefaultView.h"

@interface BindBankCardViewController ()<BankDelegate,BankCardDefaultViewDelegate>
@property (nonatomic, strong) BankCardDefaultView *defaultView;
@property (nonatomic, strong) NSMutableArray *bankCardArray;//银行卡数组
@property (nonatomic, assign) BOOL isShow;//是否显示cell的隐藏部分
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descString;
@property (nonatomic, assign) CGFloat bankHeight;
@end

@implementation BindBankCardViewController
#pragma mark - lazy loadding
-(BankCardDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[BankCardDefaultView alloc] init];
        _defaultView.delegate = self;
    }
    return _defaultView;
}
- (NSMutableArray *)bankCardArray {
    if (!_bankCardArray) {
        _bankCardArray = [NSMutableArray array];
    }
    return _bankCardArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.bankHeight = 60;
    self.view.backgroundColor = BG_COLOR;

    self.navigationItem.title = Localized(@"bind_bank_title");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.isShow = NO;
    self.number=0;
     [self getInfo];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
-(void)dealloc {
    NSLog(@"%s",__FUNCTION__);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark - private
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.apptove = [parser.approve_user intValue];
            
            if (self.apptove==-1) {
                self.titleString = Localized(@"user_approve_suggest_no");
                self.descString = Localized(@"user_approve_suggest_no_d");
                
                //未提交实名认证
                [self setSubviews];
            } else if (self.apptove==0) {
                
                //已提交审核中
                self.titleString = Localized(@"user_approve_process_ing");
                [self setSubviews];
                self.defaultView.descLabel.hidden = YES;
                self.defaultView.vertifyButton.backgroundColor = LINE_COLOR;
                self.defaultView.vertifyButton.enabled = NO;
                
            } else if (self.apptove==1) {
                
                //审核通过，绑定银行卡
                self.tableViewStyle = UITableViewStylePlain;
                self.tableView.backgroundColor = BG_COLOR;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
                }];
                [self.view addSubview:self.tableView];
                //获取银行卡信息
//                [XSTool showProgressHUDWithView:self.view];
                @weakify(self);
                [HTTPManager getBindBankCardInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
                    @strongify(self);
//                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        self.bankCardArray = [NSMutableArray arrayWithArray:[BankCardDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"]]];
                        if (self.bankCardArray.count>0) {
                            @weakify(self);
                            [self  actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"edit") action:^{
                                @strongify(self);
                                //显示cell隐藏区域
                                self.number++;
                                self.isShow = self.number%2;
                                self.navRightBtn.hidden = NO;
                                if (self.isShow) {
                                    [self.navRightBtn setTitle:Localized(@"完成") forState:UIControlStateNormal];
                                } else {
                                    [self.navRightBtn setTitle:Localized(@"edit") forState:UIControlStateNormal];
                                }
                                self.navRightBtn.selected = !self.navRightBtn.selected;
                                [self.tableView reloadData];
                            }];
                        }
                        [self.tableView reloadData];
                    } else {
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
                } failure:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            } else {
                
                //审核失败
                self.titleString = Localized(@"审核失败，请重新审核！");
                self.descString = Localized(@"user_approve_suggest_no_d");
                [self setSubviews];
            }
            
        } else {
            
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
        
    }];
}

- (void)setSubviews {
    
    self.defaultView.frame = CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT);
    self.defaultView.titleLabel.text = self.titleString;
    self.defaultView.descLabel.text = self.descString;
    [self.view addSubview:self.defaultView];
}

- (void)addBankCard{
    AddBankCardViewController *addVC = [[AddBankCardViewController alloc] init];
    addVC.isAdd = YES;
    [self.navigationController pushViewController:addVC animated:YES];
    self.isShow = NO;
    [self.navRightBtn setTitle:Localized(@"edit") forState:UIControlStateNormal];
    [self.tableView reloadData];
}

- (void)bankCardToDefault:(BankCardDataParser *)bankParser {
    [XSTool showToastWithView:self.view Text:@"正在修改"];
    @weakify(self);
    [HTTPManager defaultBankCardWithBankId:bankParser.ID success:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        
        if (state.status) {
            [self.bankCardArray removeAllObjects];
            [HTTPManager getBindBankCardInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];

                if (state.status) {
                    self.bankCardArray = [NSMutableArray arrayWithArray:[BankCardDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"]]];
                    [self.tableView reloadData];
                }else {
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)bankCardToDelete:(BankCardDataParser *)bankParser {
    
    //弹出警告框
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"确认删除？") message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @weakify(self);
        [HTTPManager deleteBankCardWithBankId:bankParser.ID success:^(NSDictionary * _Nullable dic, resultObject *state) {
             @strongify(self);
            if (state.status) {
                if (self.bankCardArray.count>1) {
                    self.navRightBtn.hidden = NO;
                } else {
                    self.navRightBtn.hidden = YES;
                }
                [XSTool showToastWithView:self.view Text:Localized(@"del_success")];
                [self.bankCardArray removeObject:bankParser];
                [self.tableView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
            
        }];
        
    }];
    [alertControl addAction:cancelAction];
    [alertControl addAction:okAction];
    [self presentViewController:alertControl animated:YES completion:nil];
    
}

- (void)toVertify {
    RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
    
}

#pragma mark - UITableViewDelegate
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//
//    return self.bankCardArray.count;
//}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *bankVC = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.bankHeight+NORMOL_SPACE)];
    bankVC.backgroundColor = BG_COLOR;
    UIView *conVC = [[UIView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, self.bankHeight)];
    conVC.backgroundColor = [UIColor whiteColor];
    [bankVC addSubview:conVC];
    UIButton *addBankBtn = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, self.bankHeight-2*NORMOL_SPACE, self.bankHeight-2*NORMOL_SPACE)];
    [addBankBtn setImage:[UIImage imageNamed:@"user_bank_add"] forState:UIControlStateNormal];
//    [addBankBtn setImage:[UIImage imageNamed:@"bank_add_h"] forState:UIControlStateSelected];
    [addBankBtn addTarget:self action:@selector(addBankCard) forControlEvents:UIControlEventTouchUpInside];
    [conVC addSubview:addBankBtn];
    UILabel *bankLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(addBankBtn.frame), CGRectGetMinY(addBankBtn.frame), mainWidth-NORMOL_SPACE*2-CGRectGetWidth(addBankBtn.frame), CGRectGetHeight(addBankBtn.frame))];
    bankLabel.text = Localized(@"add_bank");
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addBankCard)];
    bankVC.userInteractionEnabled = YES;
    conVC.userInteractionEnabled = YES;
    [conVC addGestureRecognizer:tap];
    //    bankLabel.textColor = [UIColor redColor];
    //    bankLabel.font = [UIFont systemFontOfSize:20];
    [conVC addSubview:bankLabel];
    return bankVC;
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"bankCell";
    BankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[BankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.bankDelegate = self;
    if (indexPath.row%3==0) {
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"45B157"];
    } else if (indexPath.row%3==1) {
        
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"4C9DC1"];
    } else {
        
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"eb8d4f"];
    }
    cell.parser = self.bankCardArray[indexPath.row];
   
    
    cell.isShow = self.isShow;
    return cell;
}

- (CGFloat ) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110.5+12;
}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bankCardArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

       return self.bankHeight+10;
    

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //跳转到修改银行卡界面
    AddBankCardViewController *addVC = [[AddBankCardViewController alloc] init];
    addVC.isAdd = NO;
    addVC.dataParser = self.bankCardArray[indexPath.row];
    [self.navigationController pushViewController:addVC animated:YES];
    self.isShow = NO;
    [self.navRightBtn setTitle:Localized(@"edit") forState:UIControlStateNormal];
    [self.tableView reloadData];
    
}

@end
