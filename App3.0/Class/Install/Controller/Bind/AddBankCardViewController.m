//
//  AddBankCardViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddBankCardViewController.h"
#import "XSCustomButton.h"
#import "BankCardModel.h"
#import "BankCardTypeView.h"
#import "BindBankCardViewController.h"
#import "RSAEncryptor.h"

@interface AddBankCardViewController ()<UITextFieldDelegate,CardTypeDelegate>
@property (nonatomic, strong) UITextField *accountBank;//开户支行
@property (nonatomic, strong) UITextField *bankAddress;//支行地址
@property (nonatomic, strong) UITextField *bankAccount;//银行账号
@property (nonatomic, strong) UITextField *passWordTF;//交易密码
@property (nonatomic, strong) NSMutableArray *typeArray;//银行卡类型数组

@property (nonatomic, strong) NSString *typeId;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, strong) BankCardTypeView *typeView;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) UILabel *bankTypeLabel;
@property (nonatomic, strong) UILabel *accountBankLabel;
@property (nonatomic, strong) UILabel *bankAddressLabel;
@property (nonatomic, strong) UILabel *bankAccountLabel;
@end

@implementation AddBankCardViewController
#pragma mark - lazy loadding
-(UILabel *)bankTypeLabel {
    if (!_bankTypeLabel) {
        _bankTypeLabel = [[UILabel alloc] init];
    }
    return _bankTypeLabel;
    
}

-(UILabel *)accountBankLabel {
    if (!_accountBankLabel) {
        _accountBankLabel = [[UILabel alloc] init];
    }
    return _accountBankLabel;
}

-(UILabel *)bankAddressLabel {
    if (!_bankAddressLabel) {
        _bankAddressLabel = [[UILabel alloc] init];
    }
    return _bankAddressLabel;
    
}

-(UILabel *)bankAccountLabel {
    if (!_bankAccountLabel) {
        _bankAccountLabel = [[UILabel alloc] init];
    }
    return _bankAccountLabel;
}

- (UITextField *)accountBank {
    if (!_accountBank) {
        _accountBank = [[UITextField alloc] init];
        _accountBank.textAlignment = NSTextAlignmentRight;
        _accountBank.returnKeyType = UIReturnKeyNext;
        _accountBank.delegate = self;
    }
    return _accountBank;
}

- (UITextField *)bankAddress {
    if (!_bankAddress) {
        _bankAddress = [[UITextField alloc] init];
        _bankAddress.textAlignment = NSTextAlignmentRight;
        _bankAddress.returnKeyType = UIReturnKeyNext;
        _bankAddress.delegate = self;
    }
    return _bankAddress;
}

- (UITextField *)bankAccount {
    if (!_bankAccount) {
        _bankAccount = [[UITextField alloc] init];
        _bankAccount.textAlignment = NSTextAlignmentRight;
        _bankAccount.returnKeyType = UIReturnKeyDone;
        _bankAccount.delegate = self;
    }
    return _bankAccount;
}

- (UITextField *)passWordTF {
    if (!_passWordTF) {
        _passWordTF = [[UITextField alloc] init];
        _passWordTF.textAlignment = NSTextAlignmentRight;
        _passWordTF.returnKeyType = UIReturnKeyDone;
        _passWordTF.secureTextEntry = YES;
        _passWordTF.delegate = self;
    }
    return _passWordTF;
}

- (NSMutableArray *)typeArray {
    if (!_typeArray) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}

- (BankCardTypeView *)typeView {
    if (!_typeView) {
        _typeView = [[BankCardTypeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.viewType = AlertTypeBankCard;
        _typeView.typeDelegate = self;
    }
    return _typeView;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 44;
//    self.navigationController.navigationBarHidden = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = BG_COLOR;
    if (self.isAdd) {
        self.navigationItem.title = Localized(@"add_bank");
    } else {
        self.navigationItem.title = Localized(@"编辑银行卡");
    }
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    if (self.isAdd) {
        _typeName = Localized(@"selceted_bank_type");
    }else{
        
        _typeName = self.dataParser.name;
        self.typeId = self.dataParser.bank_id;
    }
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)setSubviews {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.cellHeight*2)];
    XSCustomButton *saveCardBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(25, 44, mainWidth-50, 50) title:self.isAdd?Localized(@"添加"):Localized(@"save") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [saveCardBtn addTarget:self action:@selector(submitCardAction:) forControlEvents:UIControlEventTouchUpInside];
    [saveCardBtn setBorderWith:0 borderColor:0 cornerRadius:4];
    [footerView addSubview:saveCardBtn];
    footerView.backgroundColor = BG_COLOR;
//    return footerView;
    self.tableView.tableFooterView = footerView;
}

- (void)hiddenCardView:(CardTypeDataParser *)dataParser {
    self.typeId = dataParser.ID?dataParser.ID:@"";
    _typeName = dataParser.name?dataParser.name:Localized(@"selceted_bank_type");
    NSLog(@"++++%@,----%@",self.typeId,_typeName);
    //消失
    [self.typeView removeFromSuperview];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)popViewController{
    NSUInteger backPage = 1;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[BindBankCardViewController class]]) {
            backPage = 2;
            break;
        }
    }
    if (backPage==2) {
        BindBankCardViewController *controller = (BindBankCardViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
        [controller getInfo];
        [self.navigationController popToViewController:controller animated:YES];
    } else if (backPage==1){
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (void)submitCardAction:(UIButton *) sender {
    [self.view endEditing:YES];
    if ([self.accountBank.text isEqualToString:@""]||self.accountBank.text.length == 0||[_typeName isEqualToString:Localized(@"selceted_bank_type")]||self.typeId==nil||[self.bankAddress.text isEqualToString:@""]||self.bankAddress.text.length==0||[self.bankAccount.text isEqualToString:@""]||self.bankAccount.text.length==0) {
        [XSTool showToastWithView:self.view Text:@"请完善信息！"];
    } else {
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *accountString = [[NSString alloc]initWithString:[self.accountBank.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *bankString = [[NSString alloc]initWithString:[self.bankAccount.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *addressString = [[NSString alloc]initWithString:[self.bankAddress.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString * passwordStr = [RSAEncryptor encryptString:self.passWordTF.text];
        
        if (self.isAdd) {
            [XSTool showProgressHUDWithView:self.view];

            [HTTPManager addBankCardWithBankName:accountString bankId:self.typeId bankCard:bankString bankAddress:addressString passWord:passwordStr success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"add_success")];
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
            
        } else {
            [XSTool showProgressHUDWithView:self.view];
            
            [HTTPManager updateBankCardWithBankName:accountString bankId:self.typeId bankCard:bankString bankAddress:addressString userId:self.dataParser.ID passWord:passwordStr success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                    //返回上一个界面
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
    }
    
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"cCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = COLOR_666666;
        cell.textLabel.font = SYSTEM_FONT(14);
        cell.detailTextLabel.font = SYSTEM_FONT(14);
    }
    switch (indexPath.row) {
        case 0: {
            cell.textLabel.text = Localized(@"银行类型");
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.textColor = COLOR_999999;
            cell.detailTextLabel.text = _typeName;
        }
            break;
        case 1: {
            cell.textLabel.text = Localized(@"kaihu_name");
            cell.detailTextLabel.textColor = Color(@"111111");
            cell.detailTextLabel.text = [UserInstance ShardInstnce].trueName;
            
        }
            break;
        case 2: {
            cell.textLabel.text = Localized(@"bank_account");
            self.bankAccount.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth-10*NORMOL_SPACE, self.cellHeight);
            self.bankAccount.font = SYSTEM_FONT(14);
            if (self.isAdd) {
                self.bankAccount.placeholder = Localized(@"your_bank_card");
            } else {
                self.bankAccount.text = self.dataParser.bankcard;
            }
            [cell.contentView addSubview:self.bankAccount];
            
        }
            break;
        case 3: {
            cell.textLabel.text = Localized(@"开户支行");
            self.accountBank.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth-NORMOL_SPACE*10, self.cellHeight);
            self.accountBank.font = SYSTEM_FONT(14);
            if (self.isAdd) {
                self.accountBank.placeholder = Localized(@"如：高新支行");
            }else{
                self.accountBank.text = self.dataParser.bankname;
            }
            [cell.contentView addSubview:self.accountBank];
            
        }
            break;
        case 4: {
            cell.textLabel.text = Localized(@"kaihu_addr");
            self.bankAddress.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth- NORMOL_SPACE*10, self.cellHeight);
            self.bankAddress.font = SYSTEM_FONT(14);
            if (self.isAdd) {
                self.bankAddress.placeholder = Localized(@"input_kaihu_addr");
                
            } else {
                self.bankAddress.text = self.dataParser.bankaddress;
            }
            
            [cell.contentView addSubview:self.bankAddress];
        }
            break;
        case 5: {
            cell.textLabel.text = Localized(@"trade_pd");
            self.passWordTF.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth- NORMOL_SPACE*10, self.cellHeight);
            self.passWordTF.font = SYSTEM_FONT(14);
            self.passWordTF.placeholder = Localized(@"input_trade_pd");
            
            [cell.contentView addSubview:self.passWordTF];
        }
            break;
    }
    
    return cell;
}

- (NSInteger )numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger ) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (CGFloat ) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.cellHeight;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(12, 0, mainWidth-24, self.cellHeight)];
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.textColor = COLOR_666666;
        titleLabel.font = SYSTEM_FONT(14);
        titleLabel.frame = headerView.frame;
        titleLabel.text = Localized(@"bind_bank_card");
        [headerView addSubview:titleLabel];
        titleLabel.backgroundColor = BG_COLOR;
        return headerView;
    } else {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainHeight, NORMOL_SPACE*2)];
        headerView.backgroundColor = BG_COLOR;
        return headerView;
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    if (section == 0) {
//
//
//    } else {
//    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0&&indexPath.row==0) {
        
        //弹出选择银行类别的警告框
        [HTTPManager getBankTypeSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                self.typeArray = [NSMutableArray arrayWithArray:[CardTypeDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
                self.typeView.typeArray = self.typeArray;
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        [UIView animateWithDuration:0.3 animations:^{
            [self.accountBank resignFirstResponder];
            [self.bankAccount resignFirstResponder];
            [self.bankAddress resignFirstResponder];
            [self.passWordTF resignFirstResponder];
        } completion:^(BOOL finished) {
            if (self.typeId) {
                self.typeView.selectedId = self.typeId;
            }
            [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView] ;
        }];
    }
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ( textField==self.accountBank) {
        return  [self.bankAddress becomeFirstResponder];
    } else if (textField==self.bankAddress) {
        return [self.bankAccount becomeFirstResponder];
    } else if (textField==self.bankAccount)  {
        return [self.bankAccount resignFirstResponder];
    } else {
        return [self.passWordTF resignFirstResponder];
    }
}

@end
