//
//  AddBankCardViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardModel.h"
@interface AddBankCardViewController : XSBaseTableViewController

@property (nonatomic, assign) BOOL isAdd;
@property (nonatomic, strong) BankCardDataParser *dataParser;

@end
