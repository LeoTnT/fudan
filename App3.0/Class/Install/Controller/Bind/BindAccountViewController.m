//
//  BindAccountViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BindAccountViewController.h"
#import "XSCustomButton.h"
#import "UserInstance.h"
#import "LoginModel.h"
#import "BindCountTableViewCell.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "InputPassWordViewController.h"

static CGFloat CellHeight = 60;
@interface BindAccountViewController ()<UITableViewDelegate,UITableViewDataSource,BindCountDelegate,WXApiDelegate,TencentSessionDelegate>
{
    //    LoginDataParser *_userInfo;//用户信息
    TencentOAuth *tencentOAuth;
    
}
@property (nonatomic, strong) NSMutableArray *bindMethodArray;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *logo;
@end

@implementation BindAccountViewController

#pragma mark - lazy lodding
- (NSMutableArray *)bindMethodArray {
    if (!_bindMethodArray) {
        _bindMethodArray = [NSMutableArray array];
    }
    return _bindMethodArray;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.bounces = NO;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionHeaderHeight = NORMOL_SPACE*2; 
        _tableView.sectionFooterHeight = 0.01;
    }
    return _tableView;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = Localized(@"setting_tv_baccount");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self addSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weiXinToLoginIn:) name:@"WeixinCodeNotification" object:nil];
    [self getInfo];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WeixinCodeNotification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    //    UserInstance *userInstance = [UserInstance ShardInstnce];
    //    _userInfo = userInstance.getUserInfo;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getSocialLoginListSuccess:^(NSDictionary *dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
             [self.bindMethodArray removeAllObjects];
//            BindParser *parser = [[BindParser alloc] initWithDictionary:dic];
            [self.bindMethodArray setArray:[BindCountParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            [self.tableView reloadData];
        } else {
            [MBProgressHUD showMessage:dic[@"info"] view:self.view];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
    
}

- (void)addSubviews {
    [self.view addSubview:self.tableView];
}

- (void)weiXinToLoginIn:(NSNotification *)notification {
    NSLog(@"%@",notification.userInfo);
    NSString *code = notification.userInfo[@"weiXinReturnCode"];
    [self getBindUserOrSocialInformationWithType:@"weixin" code:code openid:nil];
}

- (void)getBindUserOrSocialInformationWithType:(NSString *) type code:(NSString *) code openid:(NSString *) openid {
    [HTTPManager getBindUserOrSocialInformationWithType:type code:code openid:openid success:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            ThirdPartyParser *partyParser = [ThirdPartyParser mj_objectWithKeyValues:dic[@"data"]];

            if ([partyParser.status integerValue]==1) {
                [MBProgressHUD showMessage:@"此账号已被其他账号绑定，请先解绑！" view:self.view];
                 [self getInfo];
            } else {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                [dictionary setObject:@"2" forKey:@"iscreate"];
                [dictionary setObject:type forKey:@"type"];
                [dictionary setObject:partyParser.unionid forKey:@"unionid"];
//                if (![iscreate isEqualToString:@"2"]) {
//                    [dictionary setObject:name forKey:@"name"];
//                    [dictionary setObject:pass forKey:@"pass"];
//                }
                
                
//                if (token) {
//                    [dictionary setObject:token forKey:@"token"];
//                }
                if (partyParser.openid) {
                    [dictionary setObject:partyParser.openid forKey:@"openid"];
                    [dictionary setObject:[type isEqualToString:@"qq"]?self.nickName:partyParser.nickname forKey:@"nickname"];
                    [dictionary setObject:[type isEqualToString:@"qq"]?self.logo:partyParser.logo forKey:@"logo"];
                    [dictionary setObject:[type isEqualToString:@"qq"]?self.sex:partyParser.sex forKey:@"sex"];
                }
//                if ([iscreate integerValue]==1) {
//                    [dictionary setObject:verify forKey:@"verify"];
//                }
                //绑定账号
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [HTTPManager bindUserBySocialWithParam:dictionary success:^(NSDictionary *dic, resultObject *state) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    if (state.status) {
                        [self getInfo];
                    } else {
                         [MBProgressHUD showMessage:dic[@"info"] view:self.view];
                    }
                } failure:^(NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [MBProgressHUD showMessage:NetFailure view:self.view];
                }];
            } 
        } else {
              [MBProgressHUD showMessage:dic[@"info"] view:self.view];
        }
        
    }failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"bindCountCell";
    BindCountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[BindCountTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
    }
    cell.delegate = self;
    BindCountParser *parser = self.bindMethodArray[indexPath.row];
    cell.bindCountParser = parser;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bindMethodArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 2*NORMOL_SPACE;
}

#pragma mark - BindCountDelegate
- (void)cancelBindWithType:(NSString *)type {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager cancelSocialBindWithType:type success:^(NSDictionary *dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
            
            //刷新界面
            [self getInfo];
        } else {
            [MBProgressHUD showMessage:state.info view:self.view];
        }
    } failure:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}
-(void)changeSocialWithType:(NSString *)type {
    InputPassWordViewController *controller = [[InputPassWordViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)bindSocialWithType:(NSString *)type {
    if ([type isEqualToString:@"qq"]) {
        if ([TencentOAuth iphoneQQInstalled]) {
            
            tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];   //注册
            
            NSArray* permissions = [NSArray arrayWithObjects:
                                    kOPEN_PERMISSION_GET_USER_INFO,
                                    kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                                    nil];
            [tencentOAuth authorize:permissions]; //授权
            [tencentOAuth incrAuthWithPermissions:permissions];
            
        } else {
             [MBProgressHUD showMessage:@"尚未安装QQ客户端" view:self.view];
            [self getInfo];
        }
        
    } else {
        if ([WXApi isWXAppInstalled]) {
            SendAuthReq* req = [[SendAuthReq alloc] init];
            req.scope = WXAuthScope; // @"post_timeline,sns"
            req.state = WXAuthReqState;
            req.openID = WXAPPID;
            [WXApi sendAuthReq:req viewController:self delegate:self];
        }else{
            [MBProgressHUD showMessage:@"尚未安装微信客户端" view:self.view];
            [self getInfo];
        }
    }
}

- (void)qqLogin:(UIButton *)sender {
    if ([TencentOAuth iphoneQQInstalled]) {
        tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];   //注册
        NSArray* permissions = [NSArray arrayWithObjects:kOPEN_PERMISSION_GET_USER_INFO,kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,nil];
        [tencentOAuth authorize:permissions];
        [tencentOAuth incrAuthWithPermissions:permissions];
    } else {
        [MBProgressHUD showMessage:@"尚未安装QQ客户端" view:self.view];
    }
}

- (void)tencentDidLogin {
    if (tencentOAuth.accessToken && 0 != [tencentOAuth.accessToken length]){
        //  记录登录用户的OpenID、Token以及过期时间
        //token =46983A7B5D5B8F8A01257B69AE258B6D    data =Wed Aug 23 11:42:34 2017
        NSLog(@"token =%@    data =%@",tencentOAuth.accessToken,tencentOAuth.expirationDate);
        self.token = tencentOAuth.accessToken;
        self.openId = tencentOAuth.openId;
        
        [tencentOAuth getUserInfo];
        [MBProgressHUD showMessage:@"授权成功" view:self.view];
    } else {
        [MBProgressHUD showMessage:@"登录不成功" view:self.view];
        [self getInfo];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled {
    if (cancelled){
        [MBProgressHUD showMessage:@"用户取消登录" view:self.view];
        [self getInfo];
    }else {
        [MBProgressHUD showMessage:@"登录失败" view:self.view];
        [self getInfo];
    }
}

- (void)tencentDidNotNetWork {
    [MBProgressHUD showMessage:@"无网络连接，请设置网络" view:self.view];
}

- (void)getUserInfoResponse:(APIResponse *)response {
    [self analysisResponse:response];
}

- (void)analysisResponse:(APIResponse *)notify {
    if (notify) {
        APIResponse *response = notify;
        if (URLREQUEST_SUCCEED == response.retCode && kOpenSDKErrorSuccess == response.detailRetCode) {
            self.nickName = response.jsonResponse[@"nickname"];
            self.logo = response.jsonResponse[@"figureurl_qq_1"];
            self.sex = [NSString stringWithFormat:@"%@",[response.jsonResponse[@"gender"] isEqualToString:@"男"]?@"0":@"1"];
            [self getBindUserOrSocialInformationWithType:@"qq" code:nil openid:self.openId];
        } else {
            NSString *errMsg = [NSString stringWithFormat:@"errorMsg:%@\n%@", response.errorMsg, [response.jsonResponse objectForKey:@"msg"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Localized(@"操作失败") message:errMsg delegate:self cancelButtonTitle:Localized(@"我知道啦") otherButtonTitles: nil];
            [alert show];
        }
    }
}

@end
