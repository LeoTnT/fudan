//
//  BindMobileViewController.h
//  App3.0
//
//  Created by nilin on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginModel.h"

@interface BindMobileViewController : XSBaseViewController

@property (nonatomic, copy) NSString *token;
//- (instancetype)initWithMobile:(NSString *)mobile mobileType:(MobileSetupType)type;
@end
