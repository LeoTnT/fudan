//
//  AddAddressViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"
#import "ReceiverAddressModel.h"
@interface AddAddressViewController : XSBaseTableViewController
@property (nonatomic, copy) NSString *area;
@property (nonatomic, strong) NSArray *areaIdArray;//省市区
@property (nonatomic, strong) NSArray *areaNameArray;
@property (nonatomic, strong) AreaParser *parser;//街道信息
@property (nonatomic, copy) NSString *townStr;
@property (nonatomic, assign) BOOL isAdd;
@property (nonatomic, strong) AddressParser *address;

@end
