//
//  ReceiverAddressViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReceiverAddressViewController.h"
#import "AddressTableViewCell.h"
#import "XSCustomButton.h"
#import "ReceiverAddressModel.h"
#import "AddAddressViewController.h"
#import "MJRefresh.h"
#import "VerifyOrderViewController.h"
#import "DefaultView.h"

@interface ReceiverAddressViewController ()<AddressProtocol>
//@property (nonatomic, strong) UITableView *tableView;//地址展示表格
@property (nonatomic, strong) NSMutableArray *addressArray;//收货地址
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) DefaultView *defaultView;
@property (nonatomic, strong) UIButton *addAddressButton;

@end

@implementation ReceiverAddressViewController
#pragma mark - lazy lodding
-(DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT-50)];
        _defaultView.defaultViewType = DefaultViewTypeForAddressList;
    }
    return _defaultView;
}

- (NSMutableArray *)addressArray {
    if (!_addressArray) {
        _addressArray = [NSMutableArray array];
    }
    return _addressArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.buttonHeight = 49;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"管理收货地址");
//    @weakify(self);
//    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
//        @strongify(self);
//
//    }];
    
    
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[VerifyOrderViewController class]]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self setSubviews];
    [self getInfo];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
  
    [XSTool showProgressHUDTOView:self.view withText:@"正在加载中"];
   @weakify(self);
    [HTTPManager getAddressListsSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
         @strongify(self);
        [self.tableView.mj_header endRefreshing];
       
        if (state.status) {
            [self.addressArray removeAllObjects];
            [self.addressArray  addObjectsFromArray:[AddressParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            
            //判断是否有收藏地址
            if (self.addressArray.count==0) {
                self.defaultView.hidden = NO;
                self.tableView.hidden = YES;
                
            } else {
                self.defaultView.hidden = YES;
                self.tableView.hidden = NO;
                [self.tableView reloadData];
            }
            
            [self.navLeftBtn addTarget:self action:@selector(deliveryAddressInfo) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError * _Nonnull error) {
         [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)deliveryAddressInfo {
    BOOL returnOrderPage = NO;
    VerifyOrderViewController *verifyController;
        NSArray *controllerArray = self.navigationController.viewControllers;
        if (controllerArray.count>0) {
            NSUInteger length = controllerArray.count;
            UIViewController *tempController = controllerArray[length-2];
            if ([tempController isKindOfClass:[VerifyOrderViewController class]]) {
                verifyController = (VerifyOrderViewController *)tempController;
                returnOrderPage = YES;
            }
        }
    if (returnOrderPage) {
            if (self.addressArray.count) {
                for (AddressParser *address in self.addressArray) {
                    if ([address.is_default integerValue]==1) {
                        verifyController.addressId = address.ID;
                        verifyController.nameString = [NSString stringWithFormat:@"%@:%@",Localized(@"收货人"),address.name];
                        verifyController.phoneString = address.mobile;
                        verifyController.addressString = [NSString stringWithFormat:@"%@:%@-%@-%@-%@-%@",Localized(@"收货地址"),[address.province name],[address.city name],[address.county name],[address.town name],[address detail]];
                        [self.navigationController popToViewController:verifyController animated:YES];
                        break;
                    }
                }
                
            } else {
                verifyController.addressId = @" ";
                verifyController.isBackFromReceiver = NO;
                [self.navigationController popToViewController:verifyController animated:YES];
            }
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)setSubviews {
    self.tableViewStyle = UITableViewStylePlain;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, self.buttonHeight+kTabbarSafeBottomMargin, 0));
    }];
    [self.view layoutIfNeeded];
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    
    self.addAddressButton = [[UIButton alloc] init];
    self.addAddressButton.backgroundColor = mainColor;
    [self.addAddressButton setTitle:Localized(@"新增收货地址") forState:UIControlStateNormal];
    [self.addAddressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addAddressButton setImage:[UIImage imageNamed:@"address_add"] forState:UIControlStateNormal];
    [self.addAddressButton setTitle:Localized(@"新增收货地址") forState:UIControlStateHighlighted];
    [self.addAddressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.addAddressButton setImage:[UIImage imageNamed:@"address_add"] forState:UIControlStateHighlighted];
    [self.addAddressButton setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [self.addAddressButton addTarget:self action:@selector(addAddressAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.addAddressButton];
    [self.addAddressButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-kTabbarSafeBottomMargin);
        make.height.mas_equalTo(self.buttonHeight);
    }];
}

- (void)addAddressAction:(UIButton *) sender {
    AddAddressViewController *addressVC = [[AddAddressViewController alloc] init];
    addressVC.isAdd = YES;
    [self.navigationController pushViewController:addressVC animated:YES];
}

- (void)defaultAddressWithId:(NSString *)addressId {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager setDefaultAddressWithId:addressId success:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (!state.status) {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }else{
            //刷新表格
            [self getInfo];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)editAddressWithAddress:(AddressParser *)address{
    AddAddressViewController *addressVC = [[AddAddressViewController alloc] init];
    addressVC.isAdd = NO;
    addressVC.address = address;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
}

- (void)deleteAddressWithId:(NSString *)addressId andSender:(UIButton *)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"您确定要删除此收货地址吗？") message:nil preferredStyle:UIAlertControllerStyleAlert] ;
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [XSTool showToastWithView:self.view Text:@"正在删除"];
        @weakify(self);
        [HTTPManager deleteAddressWithId:addressId success:^(NSDictionary * _Nullable dic, resultObject *state) {
             @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (!state.status) {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
                
            }else{
                for (AddressParser *parser in self.addressArray) {
                    if ([[NSString stringWithFormat:@"%@",parser.ID] isEqualToString:addressId]) {
                        if ([parser.is_default integerValue]) {
                            [self.addressArray removeObject:parser];
                            if (self.addressArray.count) {
                                AddressParser *firstParser = [self.addressArray firstObject];
                                [self defaultAddressWithId:firstParser.ID];
                                
                            }
                        }
                    }
                }
                //刷新界面
                [self getInfo];
                
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"addressCell";
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[AddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //设置内容

    cell.address = self.addressArray[indexPath.row];
    cell.addressDelegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressTableViewCell *cell = (AddressTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addressArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

@end
