//
//  CountyViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"
@interface CountyViewController : XSBaseTableViewController

@property (nonatomic, strong) AreaParser *parser;
@end
