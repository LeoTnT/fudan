//
//  ProvinceViewController.m
//  App3.0
//
//  Created by mac on 2017/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ProvinceViewController.h"
#import "CityViewController.h"
#import "AreaModel.h"
#import "MJRefresh.h"

@interface ProvinceViewController ()
@property (nonatomic, strong) NSMutableArray *provinceArray;
@property (nonatomic, assign) CGFloat cellHeight;
@end

@implementation ProvinceViewController
#pragma mark - lazy loadding

- (NSMutableArray *)provinceArray {
    if (!_provinceArray) {
        _provinceArray = [NSMutableArray array];
    }
    return _provinceArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 45;
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:Localized(@"地区选择") action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.provinceArray = [NSMutableArray array];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getProvinceInfoWithId:@"0" level:@"1" success:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];

        if (state.status) {
             self.provinceArray = [NSMutableArray arrayWithArray:[AreaParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
         [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }] ;
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = @"provinceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
    }
    AreaParser *parser = self.provinceArray[indexPath.row];
    cell.textLabel.text = parser.name;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cellHeight-1, mainWidth, 1)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [cell.contentView addSubview:line];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AreaParser *parser = self.provinceArray[indexPath.row];
    CityViewController *cityVC = [[CityViewController alloc] init];
    cityVC.parser = parser;
    [self.navigationController pushViewController:cityVC animated:YES];
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.provinceArray.count;
    
}


@end
