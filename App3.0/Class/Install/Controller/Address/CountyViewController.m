//
//  CountyViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CountyViewController.h"
#import "TownViewController.h"
#import "CityViewController.h"
#import "AddAddressViewController.h"
#import "VertifyViewController.h"
#import "ProvinceViewController.h"
#import "MJRefresh.h"
#import "ApplyBusinessViewController.h"
#import "TownViewController.h"
#import "XSRegisterViewController.h"
#import "OfflineBusinessViewController.h"

@interface CountyViewController ()
@property (nonatomic, strong) NSMutableArray *countyArray;
@property (nonatomic, assign) CGFloat cellHeight;
@end

@implementation CountyViewController
#pragma mark - lazy loadding

- (NSMutableArray *)countyArray {
    if (!_countyArray) {
        _countyArray = [NSMutableArray array];
    }
    return _countyArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 45;
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:self.parser.name action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getProvinceInfoWithId:self.parser.code level:@"3" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            self.countyArray = [NSMutableArray arrayWithArray:[AreaParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
         [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }] ;
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"provinceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
    }
    AreaParser *parser = self.countyArray[indexPath.row];
    cell.textLabel.text = parser.name;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cellHeight-1, mainWidth, 1)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [cell.contentView addSubview:line];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AreaParser *parser = self.countyArray[indexPath.row];
    NSMutableArray *idAry = [NSMutableArray array];
    NSMutableArray *nameAry = [NSMutableArray array];
    NSArray *ary = [self.navigationController viewControllers];
    NSUInteger nextPage = 0;
    for (UIViewController *controller in ary) {
        if ([controller isKindOfClass:[ApplyBusinessViewController class]] || [controller isKindOfClass:[XSRegisterViewController class]]|| [controller isKindOfClass:[OfflineBusinessViewController class]]) {
            nextPage = 1;
            break;
        }
    }
    NSInteger index = [self.navigationController.viewControllers indexOfObject:self];
    UIViewController *controller = [self.navigationController.viewControllers objectAtIndex:index-1];
    if ([controller isKindOfClass:[CityViewController class]]) {
        CityViewController *cityVC = (CityViewController *)controller;
        
        //获取完整地域
        [nameAry addObject:[NSString stringWithFormat:@"%@",cityVC.parser.name]];
        [nameAry addObject:self.parser.name];
        [nameAry addObject:parser.name];
        [idAry addObject:[NSString stringWithFormat:@"%@",cityVC.parser.code]];
        [idAry addObject:[NSString stringWithFormat:@"%@",self.parser.code]];
        [idAry addObject:[NSString stringWithFormat:@"%@",parser.code]];
    }

    if (nextPage==1) {
        TownViewController *townController = [[TownViewController alloc] init];
        townController.areaArrayForSupply = @[@[[nameAry firstObject],[idAry firstObject]],@[nameAry[1],idAry[1]],@[nameAry[2],idAry[2]]];
        townController.areaId = parser.code;
        [self.navigationController pushViewController:townController animated:YES];
    } else {
        for (UIViewController *vc in ary) {
            if ([vc isMemberOfClass:[AddAddressViewController class]]) {
                //获取完整地域
                AddAddressViewController *addressVC = (AddAddressViewController *) vc;
                addressVC.area= [NSString stringWithFormat:@"%@-%@-%@",[nameAry firstObject],nameAry[1],nameAry[2]];
                addressVC.areaIdArray = idAry;
                addressVC.areaNameArray = nameAry;
                addressVC.parser = parser;
                addressVC.townStr = Localized(@"街道或乡镇");
                [self.navigationController popToViewController:addressVC animated:NO];
            }
            if ([vc isMemberOfClass:[VertifyViewController class]]) {
                
                //获取完整地域
                VertifyViewController *vertifyVC = (VertifyViewController *) vc;
                vertifyVC.area= [NSString stringWithFormat:@"%@-%@-%@",[nameAry firstObject],nameAry[1],nameAry[2]];
                vertifyVC.areaIdArray = idAry;
                [self.navigationController popToViewController:vertifyVC animated:NO];
            }
            if ([vc isMemberOfClass:[XSRegisterViewController class]]) {
                
                //获取完整地域
                XSRegisterViewController *regVC = (XSRegisterViewController *) vc;
                regVC.areaNameArray = nameAry;
                regVC.areaIdArray = idAry;
                [self.navigationController popToViewController:regVC animated:NO];
                break;
            }
        }

    }
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.countyArray.count;
    
}



@end
