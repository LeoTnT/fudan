//
//  CityViewController.m
//  App3.0
//
//  Created by mac on 2017/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CityViewController.h"
#import "CountyViewController.h"
#import "MJRefresh.h"

@interface CityViewController ()

@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic, assign) CGFloat cellHeight;
@end

@implementation CityViewController

#pragma mark - lazy lodding

- (NSMutableArray *)cityArray {
    if (!_cityArray) {
        _cityArray = [NSMutableArray array];
    }
    return _cityArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 45;
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:self.parser.name action:^{
         @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getProvinceInfoWithId:self.parser.code level:@"2" success:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
             self.cityArray = [NSMutableArray arrayWithArray:[AreaParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            //设置子视图
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
        
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"provinceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
    }
    AreaParser *parser = self.cityArray[indexPath.row];
    cell.textLabel.text = parser.name;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cellHeight-1, mainWidth, 1)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [cell.contentView addSubview:line];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AreaParser *parser = self.cityArray[indexPath.row];
    CountyViewController *countyVC = [[CountyViewController alloc] init];
    countyVC.parser = parser;
    [self.navigationController pushViewController:countyVC animated:YES];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cityArray.count;
}

@end
