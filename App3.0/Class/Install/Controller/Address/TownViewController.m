//
//  TownViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TownViewController.h"
#import "AddAddressViewController.h"
#import "CountyViewController.h"
#import "CityViewController.h"
#import "VertifyViewController.h"
#import "AreaModel.h"
#import "MJRefresh.h"
#import "ApplyBusinessViewController.h"
#import "XSRegisterViewController.h"
#import "OfflineBusinessViewController.h"

@interface TownViewController ()
@property (nonatomic, strong) NSMutableArray *townArray;
@property (nonatomic, assign) CGFloat cellHeight;
@end

@implementation TownViewController
#pragma mark - lazy loadding

- (NSMutableArray *)townArray {
    if (!_townArray) {
        _townArray = [NSMutableArray array];
    }
    return _townArray;
}

#pragma mark - private
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 45;
    self.view.backgroundColor = [UIColor whiteColor];
    self.townArray = [NSMutableArray array];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:Localized(@"选择街道") action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getProvinceInfoWithId:self.areaId level:@"4" success:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
             self.townArray = [NSMutableArray arrayWithArray:[AreaParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
        
    }] ;
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"provinceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
    }
    AreaParser *parser = self.townArray[indexPath.row];
    cell.textLabel.text = parser.name;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cellHeight-1, mainWidth, 1)];
    line.backgroundColor = LINE_COLOR;
    [cell.contentView addSubview:line];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AreaParser *parser = self.townArray[indexPath.row];
    NSArray *ary = [self.navigationController viewControllers];
    for (UIViewController *vc in ary) {
        if ([vc isMemberOfClass:[AddAddressViewController class]]) {
            
            //获取完整地域
            AddAddressViewController *addressVC = (AddAddressViewController *) vc;
                addressVC.parser = parser;
                addressVC.townStr = parser.name;
                [self.navigationController popToViewController:addressVC animated:NO];
            break;
        }
        if ([vc isMemberOfClass:[VertifyViewController class]]) {
            
            //获取完整地域
            VertifyViewController *vertifyVC = (VertifyViewController *) vc;
                vertifyVC.parser = parser;
                [self.navigationController popToViewController:vertifyVC animated:NO];
            break;
            
        }
        if ([vc isKindOfClass:[ApplyBusinessViewController class]]) {
            NSMutableArray *areaArray = [NSMutableArray arrayWithArray:self.areaArrayForSupply];
            [areaArray addObject:@[parser.name,parser.code]];
            ((ApplyBusinessViewController *) vc).areaArray = areaArray;
            [self.navigationController popToViewController:vc animated:YES];
            
            break;
        }
        if ([vc isKindOfClass:[OfflineBusinessViewController class]]) {
            NSMutableArray *areaArray = [NSMutableArray arrayWithArray:self.areaArrayForSupply];
            [areaArray addObject:@[parser.name,parser.code]];
            ((OfflineBusinessViewController *) vc).areaArray = areaArray;
            [self.navigationController popToViewController:vc animated:YES];
            
            break;
        }
        if ([vc isMemberOfClass:[XSRegisterViewController class]]) {
            
            //获取完整地域
            XSRegisterViewController *regVC = (XSRegisterViewController *) vc;
            regVC.areaNameArray = @[self.areaArrayForSupply[0][0],self.areaArrayForSupply[1][0],self.areaArrayForSupply[2][0],parser.name];
            regVC.areaIdArray = @[self.areaArrayForSupply[0][1],self.areaArrayForSupply[1][1],self.areaArrayForSupply[2][1],parser.code];
            [self.navigationController popToViewController:regVC animated:NO];
            break;
        }
    }
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.townArray.count;
    
}

@end
