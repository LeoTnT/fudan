//
//  AddAddressViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddAddressViewController.h"
#import "XSCustomButton.h"
#import "Address.h"
#import "XSToastManager.h"
#import "ProvinceViewController.h"
#import "TownViewController.h"
#import "ReceiverAddressModel.h"
#import "ReceiverAddressViewController.h"
@interface AddAddressViewController ()<UITextFieldDelegate>
@property (nonatomic, assign) BOOL keyBoardIsShow;
@property (nonatomic, strong) UITextField *receiver;
@property (nonatomic, strong) UITextField *phone;
@property (nonatomic, strong) UITextField *detailAddress;
@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UILabel *areaLabel;
@property (nonatomic, strong) UILabel *streetLabel;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) UIImageView *receiverImageView;
@property (nonatomic, strong) UIImageView *phoneImageView;
@property (nonatomic, strong) UIImageView *areaImageView;
@property (nonatomic, strong) UIImageView *streetImageView;
@property (nonatomic, strong) UIImageView *detailImageView;
@property (nonatomic, strong) XSCustomButton *saveButton;
@property (nonatomic, assign) BOOL isSelectProvince;
@property (nonatomic, assign) BOOL isSelectStreet;

@end

@implementation AddAddressViewController

#pragma mark - lazy loadding
-(XSCustomButton *)saveButton {
    if (!_saveButton) {
        CGFloat buttonHeight = 40;
        _saveButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(0,mainHeight-buttonHeight-navBarHeight-StatusBarHeight, mainWidth, buttonHeight) title:Localized(@"保存地址") titleColor:[UIColor whiteColor] fontSize:17 backgroundColor:mainColor higTitleColor:[UIColor whiteColor]  highBackgroundColor:mainColor];
    }
    return _saveButton;
}

- (UIImageView *)leftImage {
    if (!_leftImage) {
        _leftImage = [[UIImageView alloc] init];
    }
    return _leftImage;
}

- (UITextField *)receiver {
    if (!_receiver) {
        _receiver = [[UITextField alloc] init];
        _receiver.delegate = self;
        _receiver.tintColor = LINE_COLOR;
        _receiver.font = [UIFont systemFontOfSize:15];
        _receiver.returnKeyType = UIReturnKeyNext;
    }
    return _receiver;
}

- (UITextField *)phone {
    if (!_phone) {
        _phone = [[UITextField alloc] init];
        _phone.delegate = self;
        _phone.tintColor = LINE_COLOR;
        _phone.font = [UIFont systemFontOfSize:15];
        _phone.returnKeyType = UIReturnKeyDone;
    }
    return _phone;
}

- (UITextField *)detailAddress {
    if (!_detailAddress) {
        _detailAddress = [[UITextField alloc] init];
        _detailAddress.delegate = self;
        _detailAddress.tintColor = LINE_COLOR;
        _detailAddress.font = [UIFont systemFontOfSize:15];
        _detailAddress.returnKeyType = UIReturnKeyDone;
    }
    return _detailAddress;
}

- (UILabel *)areaLabel {
    if (!_areaLabel) {
        _areaLabel = [[UILabel alloc] init];
        _areaLabel.font = [UIFont systemFontOfSize:15];
        
    }
    return _areaLabel;
}

- (UILabel *)streetLabel {
    if (!_streetLabel) {
        _streetLabel = [[UILabel alloc] init];
        _streetLabel.font = [UIFont systemFontOfSize:15];
    }
    return _streetLabel;
}

- (UIImageView *)receiverImageView {
    if (!_receiverImageView) {
        _receiverImageView = [[UIImageView alloc] init];
    }
    return _receiverImageView;
}

- (UIImageView *)phoneImageView {
    if (!_phoneImageView) {
        _phoneImageView = [[UIImageView alloc] init];
    }
    return _phoneImageView;
}

- (UIImageView *)areaImageView {
    if (!_areaImageView) {
        _areaImageView = [[UIImageView alloc] init];
    }
    return _areaImageView;
}

- (UIImageView *)streetImageView {
    if (!_streetImageView) {
        _streetImageView = [[UIImageView alloc] init];
    }
    return _streetImageView;
}

- (UIImageView *)detailImageView {
    if (!_detailImageView) {
        _detailImageView = [[UIImageView alloc] init];
    }
    return _detailImageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 50;
    self.autoHideKeyboard = YES;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if (self.isAdd) {
        self.area = Localized(@"请选择省市区");
        self.townStr = Localized(@"街道或乡镇");
    } else {
        self.area = [NSString stringWithFormat:@"%@-%@-%@",self.address.province.name,self.address.city.name,self.address.county.name];
        self.townStr = self.address.town.name;
        self.areaIdArray = [NSArray arrayWithObjects:self.address.province.code,self.address.city.code,self.address.county.code, nil];
        self.areaNameArray = [NSArray arrayWithObjects:self.address.province.name,self.address.city.name,self.address.county.name, nil];
    }
    self.isSelectProvince = NO;
    self.isSelectStreet = NO;
    self.view.backgroundColor = BG_COLOR;
    if (self.isAdd) {
        self.navigationItem.title = Localized(@"新增收货地址");
    }else{
        
        self.navigationItem.title = Localized(@"修改收货地址");
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self setSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.areaIdArray) {
         self.isSelectProvince = YES;
        NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        NSIndexPath *index2 = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index2] withRowAnimation:UITableViewRowAnimationNone];
    }
    if(self.isSelectStreet){
        NSIndexPath *index = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    }
}
-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)setSubviews {
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = BG_COLOR;
    
    [self.saveButton addTarget:self action:@selector(saveAddressAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveButton];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)saveAddressAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    //判断是否有空值的
    if ([self.receiver.text isEqualToString:@" "]||[self.phone.text isEqualToString:@" "]||[ self.townStr isEqualToString:Localized(@"街道或乡镇")]||[self.detailAddress.text isEqualToString:@" "]||[self.area isEqualToString:Localized(@"请选择省市区")]||self.receiver.text.length==0||self.phone.text .length==0|| self.townStr.length==0||self.detailAddress.text.length==0||self.area.length==0) {
        [XSTool showToastWithView:self.view Text:@"请完善收货地址"];
        return;
    } else {
        
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *receiverString = [[NSString alloc]initWithString:[self.receiver.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *detailString = [[NSString alloc]initWithString:[self.detailAddress.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *phoneString = [[NSString alloc]initWithString:[self.phone.text stringByTrimmingCharactersInSet:whiteSpace]];
        if (self.isAdd) {
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager addAddressWithProvince:[self.areaIdArray firstObject] city:self.areaIdArray[1] county:self.areaIdArray[2] town:[NSString stringWithFormat:@"%@",self.parser.code ] provinceName:[self.areaNameArray firstObject] cityName:self.areaNameArray[1] countyName:self.areaNameArray[2] townName:self.parser.name address:detailString contact:receiverString tel:phoneString isDefault:@"0" success:^(NSDictionary * _Nullable dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
            
        } else {
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager updateAddressWithId:self.address.ID province:self.areaIdArray[0] city:self.areaIdArray[1] county:self.areaIdArray[2] town:self.parser.code?self.parser.code:self.address.town.code provinceName:[self.areaNameArray firstObject] cityName:self.areaNameArray[1] countyName:self.areaNameArray[2] townName:self.parser.name?self.parser.name:self.address.town.name address:detailString contact:receiverString tel:phoneString isDefault:self.address.is_default success:^(NSDictionary * _Nullable dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
    }
}

- (void)popViewController {
    ReceiverAddressViewController *controller = (ReceiverAddressViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    [controller getInfo];
    [self.navigationController popToViewController:controller animated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==2) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.view endEditing:YES];
        } completion:^(BOOL finished) {
            ProvinceViewController *province=[[ProvinceViewController alloc] init];
            [self.navigationController pushViewController:province animated:YES];
           
        }];
        
    }
    if (indexPath.row==3) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.view endEditing:YES];
        } completion:^(BOOL finished) {
            if (self.isAdd) {
                if (self.isSelectProvince) {
                    TownViewController *townVC = [[TownViewController alloc] init];
                    townVC.areaId = [self.areaIdArray lastObject];
                    [self.navigationController pushViewController:townVC animated:YES];
                    self.isSelectStreet = YES;
                }
                
            } else {
                TownViewController *townVC = [[TownViewController alloc] init];

                if (self.isSelectProvince) {
                    townVC.areaId = [self.areaIdArray lastObject];
                } else {
                    townVC.areaId = self.address.county.code;
                }
                 [self.navigationController pushViewController:townVC animated:YES];
            }
            
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.cellHeight;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    //    if (cell==nil) {
    NSString *idString = @"adCell";
    UITableViewCell *cell  = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    }
    CGFloat leftSpace = 15,imageHeight = self.cellHeight-2*NORMOL_SPACE;
    switch (indexPath.row) {
        case 0: {
            self.receiverImageView.frame = CGRectMake(leftSpace, NORMOL_SPACE, imageHeight, imageHeight);
            self.receiverImageView.image = [UIImage imageNamed:@"user_site_receiver"];
            self.receiverImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview: self.receiverImageView];
            self.receiver.frame = CGRectMake(CGRectGetMaxX(self.receiverImageView.frame)+NORMOL_SPACE*2, CGRectGetMinY(self.receiverImageView.frame), mainWidth-CGRectGetMaxX(self.receiverImageView.frame)-4*NORMOL_SPACE, imageHeight);
            if (self.isAdd) {
                self.receiver.placeholder = Localized(@"收货人");
            } else {
                self.receiver.text = self.address.name;
            }
            
            [cell.contentView addSubview:self.receiver];
            NSLog(@"cell2:%f",CGRectGetMinX(self.receiver.frame));
        }
            break;
        case 1: {
            self.phoneImageView.frame = CGRectMake(leftSpace, NORMOL_SPACE, imageHeight, imageHeight);
            self.phoneImageView.image = [UIImage imageNamed:@"user_site_mobile"];
            self.phoneImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview: self.phoneImageView];
            self.phone.frame = CGRectMake(CGRectGetMaxX(self.phoneImageView.frame)+NORMOL_SPACE*2, CGRectGetMinY(self.phoneImageView.frame), mainWidth-CGRectGetMaxX(self.phoneImageView.frame)-4*NORMOL_SPACE, imageHeight);
            if (self.isAdd) {
                self.phone.placeholder = Localized(@"person_info_mobile");
            } else {
                self.phone.text = self.address.mobile;
            }
            [cell.contentView addSubview:self.phone];
        }
            break;
        case 2: {
            self.areaImageView.frame = CGRectMake(leftSpace, NORMOL_SPACE, imageHeight, imageHeight);
            self.areaImageView.image = [UIImage imageNamed:@"user_site_location"];
            self.areaImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview: self.areaImageView];
            self.areaLabel.frame = CGRectMake(CGRectGetMaxX(self.areaImageView.frame)+NORMOL_SPACE*2, CGRectGetMinY(self.areaImageView.frame), mainWidth-CGRectGetMaxX(self.areaImageView.frame)-4*NORMOL_SPACE, imageHeight);
            self.areaLabel.text = self.area;
            [cell.contentView addSubview:self.areaLabel];
        }
            break;
        case 3: {
            self.streetImageView.frame = CGRectMake(leftSpace, NORMOL_SPACE, imageHeight, imageHeight);
            self.streetImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.streetImageView.image = [UIImage imageNamed:@"user_site_street"];
            [cell.contentView addSubview: self.streetImageView];
            self.streetLabel.frame = CGRectMake(CGRectGetMaxX(self.streetImageView.frame)+NORMOL_SPACE*2, CGRectGetMinY(self.streetImageView.frame), mainWidth-CGRectGetMaxX(self.streetImageView.frame)-4*NORMOL_SPACE, imageHeight);
            self.streetLabel.text = self.townStr;
            [cell.contentView addSubview:self.streetLabel];
        }
            break;
        case 4: {
            self.detailImageView.frame = CGRectMake(leftSpace, NORMOL_SPACE, imageHeight, imageHeight);
            self.detailImageView.image = [UIImage imageNamed:@"user_site_detail"];
            [cell.contentView addSubview: self.detailImageView];
            self.detailImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.detailAddress.frame = CGRectMake(CGRectGetMaxX(self.detailImageView.frame)+NORMOL_SPACE*2, CGRectGetMinY(self.detailImageView.frame), mainWidth-CGRectGetMaxX(self.detailImageView.frame)-4*NORMOL_SPACE, imageHeight);
            if (self.isAdd) {
                self.detailAddress.placeholder = Localized(@"detail_addr");
            } else {
                self.detailAddress.text = self.address.detail;
            }
            [cell.contentView addSubview:self.detailAddress];
        }
            break;
    }
    
    return cell;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.receiver == textField) {
        return  [self.phone becomeFirstResponder];
    } else if (self.phone==textField){
        return  [self.phone resignFirstResponder];
    } else {
        return [self.view endEditing:YES];
    }
    
}


@end
