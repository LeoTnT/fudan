//
//  TownViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"
@interface TownViewController : XSBaseTableViewController

@property (nonatomic, copy) NSString *areaId;

/**商家入驻所需的地域信息*/
@property (nonatomic, strong) NSArray *areaArrayForSupply;
@end
