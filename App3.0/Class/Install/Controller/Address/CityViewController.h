//
//  CityViewController.h
//  App3.0
//
//  Created by mac on 2017/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"
@interface CityViewController : XSBaseTableViewController

@property (nonatomic, strong) AreaParser *parser;
@end
