//
//  SetNoDisturbViewController.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetNoDisturbViewController.h"
#import "SetNewMessageSecondTableViewCell.h"
#import "XSDatePickerView.h"
#import "UserInstance.h"
#define topSpace 10

@interface SetNoDisturbViewController ()<UITableViewDelegate,UITableViewDataSource,XSDatePickerDelegate>
{
    UITableView *_tableView;
    int disturb_end;
    int disturb_begin;
}
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, strong) XSDatePickerView *startPicker;
@property (nonatomic, strong) XSDatePickerView *endPicker;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) NSDateFormatter* formatter;


@end

@implementation SetNoDisturbViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    self.isOpen = model.is_not_disturb;
    [self setSubviews];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setSubviews{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.sectionHeaderHeight = topSpace;
    _tableView.sectionFooterHeight = 0;
    [self.view addSubview:_tableView];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if (indexPath.row == 0) {
        static NSString *cellID=@"SecondCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell=[[SetNewMessageSecondTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            [((SetNewMessageSecondTableViewCell *)cell).messageSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
        }
        ((SetNewMessageSecondTableViewCell *)cell).title_Label.text= Localized(@"勿扰模式");
        ((SetNewMessageSecondTableViewCell *)cell).detailTitle_Label.text= Localized(@"开启后,在设定时间段内收到新消息或者响铃或者震动");
        ((SetNewMessageSecondTableViewCell *)cell).messageSwitch.on = self.isOpen;
        
    }else{
        static NSString *cellID=@"NormalCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
            cell.textLabel.font=[UIFont systemFontOfSize:15];
            cell.detailTextLabel.font=[UIFont systemFontOfSize:13];
            //                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
        NSString *showString = @"";
        if (indexPath.row == 1) {
            cell.textLabel.text=Localized(@"开始时间");
            
            ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
            NSString *startTime;
            if (!_startTime&&!model.not_disturb_begin) {
                startTime = @"23:00";
            }else{
                if (self.startTime) {
                    startTime = [self.formatter stringFromDate:self.startTime];
                }else{
                    startTime = model.not_disturb_begin;
                }
            }
            if (startTime.length <=2) {
                startTime = @"08:00";
            }
            model.not_disturb_begin = startTime;
            [[UserInstance ShardInstnce] setChatSet:model];
            
            NSString *hourTime = [startTime substringWithRange:NSMakeRange(0, 2)];
            NSString *mintTime = [startTime substringWithRange:NSMakeRange(startTime.length-2, 2)];
            disturb_begin = hourTime.intValue*60+mintTime.intValue;
            
            
            showString = [self getShowStringWithHourTime:hourTime];
            hourTime = [self getHourStringWithHourTime:hourTime];
            startTime = [startTime stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:hourTime];
            
            
            cell.detailTextLabel.text= [NSString stringWithFormat:@"%@%@",showString,startTime];
            
            
            
            
        }else if (indexPath.row == 2){
            cell.textLabel.text=Localized(@"结束时间");
            ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
            NSString *endTime;
            if (!_endTime&&!model.not_disturb_end) {
                endTime = @"08:00";
            }else{
                if (self.endTime) {
                    endTime = [self.formatter stringFromDate:self.endTime];
                }else{
                    endTime = model.not_disturb_end;
                }
            }
            model.not_disturb_end = endTime;
            [[UserInstance ShardInstnce] setChatSet:model];
            if (endTime.length <=2) {
                endTime = @"08:00";
            }
            NSString *hourTime = [endTime substringWithRange:NSMakeRange(0, 2)];
            NSString *mintTime = [endTime substringWithRange:NSMakeRange(endTime.length-2, 2)];
            disturb_end = hourTime.intValue*60+mintTime.intValue;
            
            
            showString = [self getShowStringWithHourTime:hourTime];
            hourTime = [self getHourStringWithHourTime:hourTime];
            endTime = [endTime stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:hourTime];

            
            cell.detailTextLabel.text= [NSString stringWithFormat:@"%@%@",showString,endTime];
            
            if (disturb_end <disturb_begin) {
                cell.detailTextLabel.text= [NSString stringWithFormat:@"%@%@%@",Localized(@"次日"),showString,endTime];
            }
            
        }
        
    }
    
    return cell;
    
}
- (NSString *)getShowStringWithHourTime:(NSString *)hourTime{
    NSString *showString = Localized(@"上午");

    if (hourTime.integerValue >=12) {
        if (hourTime.integerValue<18) {
            showString = @"下午";
            if (hourTime.integerValue==12) {
                showString = Localized(@"凌晨");
            }
        }else{
            showString = Localized(@"晚上");
        }
    }else{
        if (hourTime.integerValue<6) {
            showString = Localized(@"凌晨");
        }else{
            showString = Localized(@"上午");
        }
        
    }
    return showString;
}

- (NSString *)getHourStringWithHourTime:(NSString *)hourTime{
    if (hourTime.integerValue >=12) {
    hourTime = [NSString stringWithFormat:@"%02ld",hourTime.integerValue-12];
    }
    return hourTime;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    return 1;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isOpen == NO) {
        return 1;
    }else{
        return 3;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isOpen == NO) {
        return  85;
    }else{
        if (indexPath.row == 0) {
            return  85;
            
        }else{
            return  45;
        }
    }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return tableView.sectionHeaderHeight;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isOpen) {
        if (indexPath.row == 1) {
            [self startBtnSeltecd];
        }else if (indexPath.row == 2) {
            [self endBtnSeltecd];
        }
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"勿扰模式");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}




//
-(void)switchAction:(id)sender
{
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        self.isOpen = YES;
    }else {
        self.isOpen = NO;
    }
    model.is_not_disturb = isButtonOn?1:0;
    [[UserInstance ShardInstnce] setChatSet:model];
    [_tableView reloadData];
}


-(void)startBtnSeltecd{
    if (self.endPicker) {
        [self.endPicker dismiss];
    }
    
    CGFloat pickerHeight = 216+46;
    if (!self.startPicker) {
        self.startPicker = [[XSDatePickerView alloc] init];
    }
    
    [self.startPicker showInView:self.view withFrame:CGRectMake(0, mainHeight-pickerHeight, mainWidth, pickerHeight) andDatePickerMode:UIDatePickerModeTime];
    self.startPicker.delegate = self;
    
    
}
-(void)endBtnSeltecd{
    if (self.startPicker) {
        [self.startPicker dismiss];
    }
    
    CGFloat pickerHeight = 216+46;
    if (!self.endPicker) {
        self.endPicker = [[XSDatePickerView alloc] init];
    }
    
    [self.endPicker showInView:self.view withFrame:CGRectMake(0, mainHeight-pickerHeight, mainWidth, pickerHeight) andDatePickerMode:UIDatePickerModeTime];
    self.endPicker.delegate = self;
}
-(void)picker:(XSDatePickerView *)picker ValueChanged:(NSDate *)date
{
    NSLog(@"date:%@",date);
    if (picker == self.startPicker) {
        self.startTime =  date;
    }else if (picker == self.endPicker) {
        self.endTime = date;
        
    }
    NSString *startTime = [self.formatter stringFromDate:self.startTime];
    NSString *endTime = [self.formatter stringFromDate:self.endTime];
    NSLog(@"startTime:%@,endTime:%@",startTime,endTime);
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];

    [_tableView reloadRowsAtIndexPaths:@[indexPath1,indexPath2] withRowAnimation:UITableViewRowAnimationFade];
}






-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.startPicker) {
        [self.startPicker dismiss];
    }
    if (self.endPicker) {
        [self.endPicker dismiss];
    }
}
-(NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter =[[NSDateFormatter alloc] init];
        [_formatter setDateFormat:@"HH:mm"];
        
    }
    return _formatter;
}

@end
