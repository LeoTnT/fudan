//
//  SetNewMessageFirstTableViewCell.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetNewMessageFirstTableViewCell.h"

@implementation SetNewMessageFirstTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

        //switch
        self.messageSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(mainWidth -65.f, 8.0f, 60.0f, 28.0f)];
        self.messageSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);

        self.messageSwitch.on = YES;//设置初始为ON的一边
        [self.contentView addSubview:self.messageSwitch];
        self.cellHeight = 45;

    }
    return self;
}




@end
