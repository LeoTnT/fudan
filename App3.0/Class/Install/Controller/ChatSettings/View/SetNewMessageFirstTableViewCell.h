//
//  SetNewMessageFirstTableViewCell.h
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetNewMessageFirstTableViewCell : UITableViewCell

@property (nonatomic, strong) UISwitch *messageSwitch;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic,copy) NSString *title_text;

@end
