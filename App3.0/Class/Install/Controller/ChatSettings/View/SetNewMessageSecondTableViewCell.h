//
//  SetNewMessageSecondTableViewCell.h
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetNewMessageSecondTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *title_Label;
@property (nonatomic, strong) UILabel *detailTitle_Label;
@property (nonatomic, strong) UISwitch *messageSwitch;
@property (nonatomic, assign) CGFloat cellHeight;

@end
