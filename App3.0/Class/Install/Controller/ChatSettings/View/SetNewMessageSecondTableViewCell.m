//
//  SetNewMessageSecondTableViewCell.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetNewMessageSecondTableViewCell.h"

@implementation SetNewMessageSecondTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        CGFloat spaceWidth = 15;

        //title_Label
        self.title_Label = [[UILabel alloc] initWithFrame:CGRectMake(spaceWidth,0,mainWidth - 120,44)];
        self.title_Label.font = [UIFont systemFontOfSize:15];
        self.title_Label.textAlignment = NSTextAlignmentLeft;
        self.title_Label.textColor = [UIColor blackColor];
        self.title_Label.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:self.title_Label];
        

        //switch
        self.messageSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(mainWidth -65.f, 8.0f, 60.0f, 28.0f)];
        self.messageSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
        self.messageSwitch.on = NO;//设置初始为ON的一边
        [self.contentView addSubview:self.messageSwitch];
        
        //DetailTitle_Label
        self.detailTitle_Label = [[UILabel alloc] initWithFrame:CGRectMake(spaceWidth,44,mainWidth - spaceWidth *2,40)];
        self.detailTitle_Label.numberOfLines = 0;
        self.detailTitle_Label.font = [UIFont systemFontOfSize:13];
        self.detailTitle_Label.textAlignment = NSTextAlignmentLeft;
        self.detailTitle_Label.textColor = [UIColor darkGrayColor];
        self.detailTitle_Label.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:self.detailTitle_Label];
        
        self.cellHeight = 85;
        
    }
    return self;
}



@end
