//
//  SetNewMessageViewController.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetNewMessageViewController.h"
#import "SetNewMessageFirstTableViewCell.h"
#import "SetNewMessageSecondTableViewCell.h"

#define topSpace 10

@interface SetNewMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}


@end

@implementation SetNewMessageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubviews];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setSubviews{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.sectionHeaderHeight = topSpace;
    _tableView.sectionFooterHeight = 0; 
    [self.view addSubview:_tableView];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            static NSString *cellID=@"FirstCell";
            cell=[tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell=[[SetNewMessageFirstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.textLabel.font=[UIFont systemFontOfSize:15];
                [ ((SetNewMessageFirstTableViewCell *)cell).messageSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
            }
            ((SetNewMessageFirstTableViewCell *)cell).textLabel.text= Localized(@"接收新消息通知");
            ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.tag = indexPath.section+indexPath.row;
            ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.on = model.is_rec_msg;
        }
        
    }else if(indexPath.section==1){
        static NSString *cellID=@"SecondCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell=[[SetNewMessageSecondTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            [ ((SetNewMessageFirstTableViewCell *)cell).messageSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
        }
        ((SetNewMessageSecondTableViewCell *)cell).title_Label.text= Localized(@"通知显示消息详情");
        ((SetNewMessageSecondTableViewCell *)cell).detailTitle_Label.text= @"关闭后,当收到消息时,通知提示将不显示发信人和内容摘要";
        ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.tag = indexPath.section+indexPath.row;
        ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.on = model.is_show_msg;
        
    }else if (indexPath.section == 2){
        if (indexPath.row == 1) {
            static NSString *cellID=@"NormalCell";
            cell=[tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
                cell.textLabel.text=Localized(@"新消息提示音");
                cell.detailTextLabel.text=Localized(@"跟随系统");
                cell.textLabel.font=[UIFont systemFontOfSize:15];
                cell.detailTextLabel.font=[UIFont systemFontOfSize:13];
//                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }
            [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseVoice)]];
            cell.layer.masksToBounds = YES;
        }else{
            static NSString *cellID=@"ThirdCell";
            cell=[tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell=[[SetNewMessageFirstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.textLabel.font=[UIFont systemFontOfSize:15];
                [ ((SetNewMessageFirstTableViewCell *)cell).messageSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
            }
            if (indexPath.row == 0) {
                ((SetNewMessageFirstTableViewCell *)cell).textLabel.text= Localized(@"声音");
                ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.on = model.is_voice;
            }else{
                ((SetNewMessageFirstTableViewCell *)cell).textLabel.text= Localized(@"震动");
                ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.on = model.is_vibrate;
                
            }
            ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.tag = indexPath.section+indexPath.row;
        }
    }
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    return 1;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    if (!model.is_rec_msg) {
        return 1;
    }
    return 3;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return  1;
    }if (section == 1) {
        return  0;
    }else{
        return 3;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return  45;
    }if (indexPath.section == 1) {
        return  0;
    }else{
        ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
        if (indexPath.row == 1 &&model.is_voice == 0) {
            return 0;
        }
        return 45;
    }}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return  0;
    }
    return tableView.sectionHeaderHeight;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"新消息提醒");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)chooseVoice{
    
}

-(void)switchAction:(id)sender
{
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        NSLog(@"is open");
        
    }else {
        NSLog(@"is close");

    }
    switch (switchButton.tag) {
        case 0:
        {
            // 接受新消息通知
            model.is_rec_msg = isButtonOn?1:0;
        }
            break;
        case 1:
        {
            // 通知显示消息详情
            model.is_show_msg = isButtonOn?1:0;
        }
            break;
        case 2:
        {
            // 声音
            model.is_voice = isButtonOn?1:0;
                   }
            break;
        case 4:
        {
            // 振动
            model.is_vibrate = isButtonOn?1:0;
        
        }
            break;
        default:
            break;
    }
    [[UserInstance ShardInstnce] setChatSet:model];
    [_tableView reloadData];
}

@end
