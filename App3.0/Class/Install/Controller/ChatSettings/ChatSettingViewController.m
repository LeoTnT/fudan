//
//  ChatSettingViewController.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatSettingViewController.h"
#import "SetNewMessageViewController.h"
#import "SetNoDisturbViewController.h"
#import "SetChatViewController.h"
#import "SetPrivacyViewController.h"
#import "SetCommonViewController.h"

#define cellHeight 45
#define topSpace 10

@interface ChatSettingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}
@property (nonatomic,copy) NSArray *dataArray;
@end

@implementation ChatSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubviews];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setSubviews{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.dataArray = @[Localized(@"新消息通知"),Localized(@"勿扰模式"),Localized(@"me_item_chat"),Localized(@"隐私"),Localized(@"通用")];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, cellHeight*5 + topSpace) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.sectionHeaderHeight = topSpace;
    _tableView.sectionFooterHeight = 0;
    [self.view addSubview:_tableView];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"ChatSettingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    return 1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return tableView.sectionHeaderHeight;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0) {
        UIViewController *VC;
        switch (indexPath.row) {
            case 0://新消息提醒
            {
                VC = [[SetNewMessageViewController alloc] init];
            }
                break;
            case 1://勿扰模式
            {
                VC = [[SetNoDisturbViewController alloc] init];
            }
                break;
            case 2://聊天
            {
                VC = [[SetChatViewController alloc] init];
            }
                break;
            case 3://隐私
            {
                VC = [[SetPrivacyViewController alloc] init];
            }
                break;
            case 4://通用
            {
                VC = [[SetCommonViewController alloc] init];
            }
                break;
        }
        [self.navigationController pushViewController:VC animated:YES];
        
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"聊天设置");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}


@end

