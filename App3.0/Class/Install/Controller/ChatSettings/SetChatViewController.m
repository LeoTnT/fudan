//
//  SetChatViewController.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetChatViewController.h"
#import "SetNewMessageFirstTableViewCell.h"
#import "XSCacheManager.h"
#define topSpace 10
#define cellHeight 45

@interface SetChatViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}


@end

@implementation SetChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubviews];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setSubviews{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.sectionHeaderHeight = topSpace;
    _tableView.sectionFooterHeight = 0; 
    [self.view addSubview:_tableView];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {//聊天背景
            static NSString *cellID=@"NormalCell";
            cell=[tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
                cell.textLabel.font=[UIFont systemFontOfSize:15];
            }
            cell.textLabel.text=Localized(@"聊天背景");
            
        } else {
            static NSString *cellID=@"SecondCell";
            ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
            cell=[tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell=[[SetNewMessageFirstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.textLabel.font=[UIFont systemFontOfSize:15];
                [ ((SetNewMessageFirstTableViewCell *)cell).messageSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
            }
            ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.tag = indexPath.row;
            if (indexPath.row == 0) {
                ((SetNewMessageFirstTableViewCell *)cell).textLabel.text= Localized(@"使用听筒播放语音");
                ((SetNewMessageFirstTableViewCell *)cell).messageSwitch.on = model.is_receiver_voice;
            }
//            else if (indexPath.row == 1) {
//                ((SetNewMessageFirstTableViewCell *)cell).textLabel.text= @"回车按键发送消息";
//            }
        }
    }else{
        static NSString *cellID=@"NormalCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        }
        if (indexPath.row == 0) {
//            cell.textLabel.text=@"聊天记录迁移";
            cell.textLabel.text=Localized(@"清空聊天记录");

        }else if (indexPath.row == 1){
            cell.textLabel.text=Localized(@"清空聊天记录");
        }
    }
    
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    return 1;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        return 1;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  cellHeight;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 1)
    {
        return topSpace*3;
    }else{
        return tableView.sectionHeaderHeight;
        
    }
    
    
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, mainWidth, topSpace *3)];
        label.font = [UIFont systemFontOfSize:13];
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor darkGrayColor];
        label.backgroundColor=[UIColor clearColor];
        label.minimumScaleFactor = 11;
        label.text = Localized(@"聊天记录");
        label.adjustsFontSizeToFitWidth = YES;        
        return label;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        return view;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
//        NSArray *conArray = [[EMClient sharedClient].chatManager getAllConversations];
//        [[EMClient sharedClient].chatManager deleteConversations:conArray isDeleteMessages:YES completion:^(EMError *aError) {
//            
//        }];
        
        //清空聊天记录
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"确定清除所有聊天记录吗？") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [XSTool showProgressHUDWithView:self.view];
            [[XSCacheManager sharedInstance] clearConversationCache];
            [XSTool hideProgressHUDWithView:self.view];
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"me_item_chat");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}




//
-(void)switchAction:(id)sender
{
    ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        NSLog(@"is open");
    }else {
        NSLog(@"is close");
    }
    if (switchButton.tag == 0) {
        model.is_receiver_voice = isButtonOn?1:0;
    }
    [[UserInstance ShardInstnce] setChatSet:model];
    [_tableView reloadData];
}

@end
