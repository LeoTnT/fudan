//
//  FeedbackViewController.h
//  App3.0
//
//  Created by nilin on 2018/6/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UpDataAction)();
@interface FeedbackViewController : XSBaseViewController
@property (nonatomic ,copy)UpDataAction upAction;
@property (assign,nonatomic) BOOL isAd;

@end


@interface FeedbackTakePhotoView : UIView

@property (nonatomic ,strong)NSMutableArray *imageData;
@property (nonatomic ,copy)void (^isTakePhoto)();
@property (nonatomic ,copy)void (^getImageNumber)(NSInteger number);
@end

@interface FeedbackTakePhotoCancel : UIButton
@property (nonatomic ,strong)UIImageView *imageView1;
@property (nonatomic ,copy)void (^ButtonCancel)(NSInteger);
@property (assign,nonatomic) NSInteger index;
@property (strong,nonatomic) UIButton *btn;
@end
