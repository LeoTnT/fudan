//
//  AboutViewController.m
//  App3.0
//
//  Created by 瑞 on 17/6/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AboutViewController.h"
#import "AgreementViewController.h"
#import "RemindVersionUpdateView.h"

#define cellHeight 45

@interface AboutViewController ()<UITableViewDelegate,UITableViewDataSource,RemindVersionUpdateViewDelegate>
{
    UITableView *_tableView;
}
@property(nonatomic,strong)RemindVersionUpdateView *remindView;

@property (nonatomic,copy) NSArray *dataArray;

@property(nonatomic,strong) UIImageView *imgView;
@property(nonatomic,strong) UILabel *imglabel;
@property(nonatomic,strong) UILabel *protocolLB;
@property(nonatomic,strong) UILabel *bootmLB;
@end

@implementation AboutViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"about");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
}


- (void)setUpUI
{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    //新商业icon
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth/3 + 10,NORMOL_SPACE * 4, mainWidth/3 -10, mainWidth/3 -10)];
        self.imgView.image = [UIImage imageNamed:@"user_set_about"];
    //从配置接口中获取图片    
    if ([AppConfigManager ShardInstnce].appConfig.logo_mobile_abort_logo) {

        [self.imgView getImageWithUrlStr:[AppConfigManager ShardInstnce].appConfig.logo_mobile_abort_logo andDefaultImage:[UIImage imageNamed:@"user_set_about"]];
    }
    
    self.imgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.imgView];
    
    //imglabel
    self.imglabel = [[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.imgView.frame)+10,mainWidth,30)];
    self.imglabel.font = [UIFont systemFontOfSize:16];
    self.imglabel.textAlignment = NSTextAlignmentCenter;
    self.imglabel.backgroundColor=[UIColor clearColor];
    self.imglabel.textColor = [UIColor blackColor];
    self.imglabel.text = APP_NAME;
    [self.view addSubview:self.imglabel];
    
    if ([AppConfigManager ShardInstnce].appConfig.title) {
        self.imglabel.text = [AppConfigManager ShardInstnce].appConfig.title;
    }
    
    //protocolLabel
    [self setAddTableView];
    
    //bootmLB
    self.bootmLB = [[UILabel alloc] init];
    self.bootmLB.font = [UIFont systemFontOfSize:14];
    self.bootmLB.textAlignment = NSTextAlignmentCenter;
    self.bootmLB.textColor = [UIColor darkGrayColor];
    self.bootmLB.backgroundColor=[UIColor clearColor];
    self.bootmLB.numberOfLines = 0;
    self.bootmLB.text = [NSString stringWithFormat:@"Copyright＠2016 %@",APP_NAME];
    [self.view addSubview:self.bootmLB];
    [self.bootmLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-40);
        make.left.right.mas_equalTo(self.view);
    }];
    
    if ([AppConfigManager ShardInstnce].appConfig.copyright) {
       self.bootmLB.text = [AppConfigManager ShardInstnce].appConfig.copyright;
    }
}



-(void)setAddTableView{
    self.dataArray = @[Localized(@"new_ver_check"),Localized(@"软件使用许可协议"),Localized(@"bug_submit")];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.imglabel.frame) +30, mainWidth, cellHeight*self.dataArray.count) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.sectionFooterHeight = 0; 
    [self.view addSubview:_tableView];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"aboutCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];

    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    
    if (indexPath.row == 0 ) {
        cell.detailTextLabel.text = [AppConfigManager ShardInstnce].app_Version;
    }else{
        cell.detailTextLabel.text = @"";
    }
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

   
    if (indexPath.row == 0 ) {
        [self getVersionInfo];
    }else if (indexPath.row == 1 ) {
        
        AgreementViewController *agreeVC=[[AgreementViewController alloc] initWithAgreementType:AgreementTypeUsingPermissions];
        [self.navigationController pushViewController:agreeVC animated:YES];
    }else if (indexPath.row == 2){
        //反馈平台
//        [self.navigationController pushViewController:[NSClassFromString(@"ErrorReportingController") new] animated:YES];
        
        //走反馈接口
         [self.navigationController pushViewController:[NSClassFromString(@"FeedbackViewController") new] animated:YES];
    }
    
}

-(void)getVersionInfo{
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getVersionInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            VersionUpdateDataModel *model=[VersionUpdateDataModel mj_objectWithKeyValues:dic[@"data"]];
            //比较版本
            NSString *appVersion = [AppConfigManager ShardInstnce].app_Version;
            if ([appVersion compare:model.client_version options:NSNumericSearch] ==NSOrderedAscending) {
                self.remindView=[[RemindVersionUpdateView alloc] initWithInfo:model];
                self.remindView.delegate=self;
            }else{
                Alert(@"当前已是最新版");
            }
        }else{
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        Alert(NetFailure);
    }];
}

-(void)closeUpdateView{
    [self.remindView removeFromSuperview];
}

@end
