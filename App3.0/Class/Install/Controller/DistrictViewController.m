//
//  DistrictViewController.m
//  App3.0
//
//  Created by mac on 2017/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DistrictViewController.h"

@interface DistrictViewController ()

@end

@implementation DistrictViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置子视图
    [self setSubViews];
}
#pragma mark-设置子视图
-(void)setSubViews{
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:Localized(@"地区选择") action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
