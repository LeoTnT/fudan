//
//  ModifyPayPswViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ModifyPayPswViewController.h"
#import "XSCustomButton.h"
#import "XSCustomButton.h"
#import "RSAEncryptor.h"
#define leftSpace 20
#define inputHeight 100
#define baseHeight 45
#define labelWidth 100
@interface ModifyPayPswViewController ()<UITextFieldDelegate>
{
    UITextField *_payPassWord;//支付密码
    UITextField *_verifyPassWord;//确认密码

}
@end

@implementation ModifyPayPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setSubviews];
    
   
}
-(void)setSubviews{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, mainWidth-leftSpace, leftSpace)];
    titleLabel.text = Localized(@"pwd_pay_suggest");
    titleLabel.textColor = mainGrayColor;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:titleLabel];
    
    //密码输入框view
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame)+leftSpace/2, mainWidth, inputHeight)];
    inputView.backgroundColor = [UIColor whiteColor];
    UILabel *oldLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, labelWidth, leftSpace*3/2)];
    oldLabel.textColor = [UIColor blackColor];
    oldLabel.text = Localized(@"pwd_pay_pay");
    oldLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:oldLabel];
    _payPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(oldLabel.frame), CGRectGetMinY(oldLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(oldLabel.frame), CGRectGetHeight(oldLabel.frame))];
    _payPassWord.delegate = self;
    _payPassWord.placeholder = Localized(@"pwd_pay_pay");
    _payPassWord.font = [UIFont systemFontOfSize:15];
    _payPassWord.secureTextEntry = YES;
    _payPassWord.returnKeyType = UIReturnKeyNext;
    [inputView addSubview:_payPassWord];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(oldLabel.frame), CGRectGetMaxY(oldLabel.frame)+leftSpace/2, mainWidth-CGRectGetMinX(oldLabel.frame), 1)];
    lineView.backgroundColor = LINE_COLOR;
    [inputView addSubview:lineView];
    
    UILabel *verifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(lineView.frame)+leftSpace/2, labelWidth, leftSpace*3/2)];
    verifyLabel.textColor = [UIColor blackColor];
    verifyLabel.text = Localized(@"pwd_pay_repay");
    verifyLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:verifyLabel];
    _verifyPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verifyLabel.frame), CGRectGetMinY(verifyLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(verifyLabel.frame), CGRectGetHeight(verifyLabel.frame))];
    _verifyPassWord.delegate = self;
    _verifyPassWord.placeholder = Localized(@"pwd_pay_repay");
    _verifyPassWord.font = [UIFont systemFontOfSize:15];
    _verifyPassWord.secureTextEntry = YES;
    _verifyPassWord.returnKeyType = UIReturnKeyDone;
    [inputView addSubview:_verifyPassWord];
       //保存按钮

    XSCustomButton *saveBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(0, leftSpace/2+CGRectGetMaxY(inputView.frame), mainWidth, leftSpace*2) title:Localized(@"save") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [saveBtn setBorderWith:0 borderColor:nil cornerRadius:5];
    [saveBtn addTarget:self action:@selector(savAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    
    [self.view addSubview:inputView];
    
}
#pragma mark - 输入框协议
#pragma mark - 返回按钮，切换响应者
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _payPassWord) {
        return [_verifyPassWord becomeFirstResponder];
    }
    else{
        
        return [_verifyPassWord resignFirstResponder];
    }
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
#pragma mark - 保存
-(void)savAction:(UIButton *) sender{
    NSString *encryptNewPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_payPassWord.text]];
    NSString *encryptVerifyPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_verifyPassWord.text]];
    NSLog(@"%@%@",encryptVerifyPassWord,encryptNewPassWord);
    @weakify(self);
    [HTTPManager resetPayPwdWithParamDic:@{@"type":@"pay",@"newPwd":encryptNewPassWord,@"rePwd":encryptVerifyPassWord} WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if(state.status){//成功
            [XSTool showToastWithView:self.view Text:@"设置支付密码成功"];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
          [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
//    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = Localized(@"支付密码设置");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
