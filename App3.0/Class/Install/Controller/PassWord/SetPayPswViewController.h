//
//  SetPayPswViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SetPayPswViewController : XSBaseViewController<UITextFieldDelegate>
/**手机*/
@property(nonatomic,copy)NSString *phone;
/**验证码*/
@property(nonatomic,copy)NSString *verify;
@end
