//
//  ModifyOldPayViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ModifyOldPayViewController.h"
#import "XSCustomButton.h"
#import "XSCustomButton.h"
#import "MissPayPswViewController.h"
#import "RSAEncryptor.h"
#import "OrderPayViewController.h"
#import "FormsVC.h"
#import "WithdrawViewController.h"
#import "TransferAccountViewController.h"
#import "UserModel.h"


@interface ModifyOldPayViewController ()<UITextFieldDelegate>
{
    UITextField *_oldPassWord;
    UITextField *_newPassWord;
    UITextField *_verifyPassWord;
}

@end

@implementation ModifyOldPayViewController
{
    NSString  *payPassWord;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
}

- (void)getUserInformation {
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            UserParser *parser = [UserParser mj_objectWithKeyValues:dic];
            payPassWord = parser.data.pay_password;
            if (isEmptyString(parser.data.pay_password)) {
                self.navigationItem.title = Localized(@"设置支付密码");
            } else {
                self.navigationItem.title = Localized(@"pwd_pay_mod");
            }
            [self setSubviews];
            
        } else {
            self.navigationItem.title = Localized(@"pwd_pay_mod");
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
        self.navigationItem.title = Localized(@"pwd_pay_mod");
    }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)setSubviews{
    
    CGFloat leftSpace  = 20;
    CGFloat inputHeight = isEmptyString(payPassWord)? 100 :150;
    //    CGFloat baseHeight  = 45;
    CGFloat labelWidth = 100;
    
//    self.navigationController.navigationBarHidden=NO;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, mainWidth-leftSpace, leftSpace)];
    titleLabel.text = Localized(@"密码由6-20位英文字母、数字或者符号组成");
    
    titleLabel.textColor = mainGrayColor;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:titleLabel];
    
    //密码输入框view
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame)+leftSpace/2, mainWidth, inputHeight)];
    inputView.backgroundColor = [UIColor whiteColor];
    
    UIView *lineView;
    if (!isEmptyString(payPassWord)) {
        UILabel *oldLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, labelWidth, leftSpace*3/2)];
        oldLabel.textColor = [UIColor blackColor];
        oldLabel.text = Localized(@"pwd_login_old");
        oldLabel.font = [UIFont systemFontOfSize:15];
        [inputView addSubview:oldLabel];
        _oldPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(oldLabel.frame), CGRectGetMinY(oldLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(oldLabel.frame), CGRectGetHeight(oldLabel.frame))];
        _oldPassWord.delegate = self;
        _oldPassWord.placeholder = Localized(@"pwd_login_old_hint");
        _oldPassWord.font = [UIFont systemFontOfSize:15];
        _oldPassWord.secureTextEntry = YES;
        _oldPassWord.returnKeyType = UIReturnKeyNext;
        [inputView addSubview:_oldPassWord];
        lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(oldLabel.frame), CGRectGetMaxY(oldLabel.frame)+leftSpace/2, mainWidth-CGRectGetMinX(oldLabel.frame), 1)];
        lineView.backgroundColor = LINE_COLOR;
        [inputView addSubview:lineView];
    }
    
    CGRect isShowNewLabel = isEmptyString(payPassWord) ? CGRectMake(leftSpace, leftSpace/2, labelWidth, leftSpace*3/2):CGRectMake(leftSpace, CGRectGetMaxY(lineView.frame)+leftSpace/2, labelWidth, leftSpace*3/2);
    
    
    UILabel *newLabel = [[UILabel alloc] initWithFrame:isShowNewLabel];
    newLabel.textColor = [UIColor blackColor];
    newLabel.text = Localized(@"pwd_login_new");
    newLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:newLabel];
    _newPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(newLabel.frame), CGRectGetMinY(newLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(newLabel.frame), CGRectGetHeight(newLabel.frame))];
    _newPassWord.delegate = self;
    _newPassWord.placeholder = @"请输入6~20位新密码";
    _newPassWord.font = [UIFont systemFontOfSize:15];
    _newPassWord.font = [UIFont systemFontOfSize:15];
    _newPassWord.secureTextEntry = YES;
    _newPassWord.returnKeyType = UIReturnKeyNext;
    [inputView addSubview:_newPassWord];
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(newLabel.frame), CGRectGetMaxY(newLabel.frame)+leftSpace/2, mainWidth-CGRectGetMinX(newLabel.frame), 1)];
    lineView2.backgroundColor = LINE_COLOR;
    [inputView addSubview:lineView2];
    
    UILabel *verifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(lineView2.frame)+leftSpace/2, labelWidth, leftSpace*3/2)];
    verifyLabel.textColor = [UIColor blackColor];
    verifyLabel.text = Localized(@"pwd_login_renew");
    verifyLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:verifyLabel];
    _verifyPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verifyLabel.frame), CGRectGetMinY(verifyLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(verifyLabel.frame), CGRectGetHeight(verifyLabel.frame))];
    _verifyPassWord.delegate = self;
    _verifyPassWord.placeholder = Localized(@"pwd_login_renew");
    _verifyPassWord.font = [UIFont systemFontOfSize:15];
    _verifyPassWord.font = [UIFont systemFontOfSize:15];
    _verifyPassWord.secureTextEntry = YES;
    _verifyPassWord.returnKeyType = UIReturnKeyDone;
    [inputView addSubview:_verifyPassWord];
    //忘记密码
    UIButton *missBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-leftSpace-labelWidth, CGRectGetMaxY(inputView.frame)+leftSpace/2+10, labelWidth, leftSpace+10)];
    [missBtn setTitle:Localized(@"pwd_pay_forget") forState:UIControlStateNormal];
    [missBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    missBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [missBtn addTarget:self action:@selector(forgetAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:missBtn];
    //保存按钮
    
    XSCustomButton *saveBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, leftSpace/2+CGRectGetMaxY(missBtn.frame), mainWidth-2*10, leftSpace*2) title:Localized(@"save") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [saveBtn setBorderWith:0 borderColor:nil cornerRadius:5];
    [saveBtn addTarget:self action:@selector(add1Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    
    [self.view addSubview:inputView];
    
}
#pragma mark - 输入框协议
#pragma mark - 返回按钮，切换响应者
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _oldPassWord) {
        return [_newPassWord becomeFirstResponder];
        
    }else if(textField == _newPassWord) {
        return [_verifyPassWord becomeFirstResponder];
        
    }else{
        
        return [_verifyPassWord resignFirstResponder];
    }
    
}

#pragma mark - 忘记密码
-(void)forgetAction:(UIButton *) sender{
    MissPayPswViewController *missVC = [[MissPayPswViewController alloc] init];
    missVC.navigationItem.title = @"";
    [self.navigationController pushViewController:missVC animated:YES];
    
}

#pragma mark - 保存
-(void)add1Action:(UIButton *) sender{
    
    if (isEmptyString(payPassWord)) {
        
        if ( !isEmptyString(_newPassWord.text) && !isEmptyString(_verifyPassWord.text)) {
            
            [self changePassword];
        }else{
            [XSTool showToastWithView:self.view Text:@"请完善信息"];
        }
    }else{
        
        if (!isEmptyString(_newPassWord.text) && !isEmptyString(_verifyPassWord.text) &&!isEmptyString(_oldPassWord.text)) {
            
            [self changePassword];
        }else{
            [XSTool showToastWithView:self.view Text:@"请完善信息"];
        }
    }
    
    
    
    
}

- (void) changePassword {
    
    NSString *encryptOldPassWord = [RSAEncryptor encryptString:_oldPassWord.text];
    NSString *encryptNewPassWord = [RSAEncryptor encryptString:_newPassWord.text];
    NSString *encryptVerifyPassWord = [RSAEncryptor encryptString:_verifyPassWord.text];
    [XSTool showProgressHUDTOView:self.view withText:nil];
    
    NSDictionary *params ;
    if (isEmptyString(payPassWord)) {
        
        params = @{@"type":@"pay",
                   @"newPwd":encryptNewPassWord,
                   @"rePwd":encryptVerifyPassWord};
    }else{
        params = @{@"type":@"pay",@"oldPwd":encryptOldPassWord,@"newPwd":encryptNewPassWord,@"rePwd":encryptVerifyPassWord};
    }
    
    [HTTPManager resetPayPwdWithParamDic:params WithSuccess:^(NSDictionary *dic, resultObject *state) {
        
        [XSTool hideProgressHUDWithView:self.view];
        if(state.status){//成功
            [XSTool showToastWithView:self.view Text:@"修改支付密码成功"];
            [MBProgressHUD showMessage:@"修改支付密码成功" view:self.view hideTime:1.5 doSomeThing:^{
                
                [self popViewController];
            }];
            
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
    
    
}


- (void)popViewController{
    NSUInteger returnLastPage = 0;
    UIViewController *tempController;
    //直接调回到最初始修改登录、支付密码的地方
    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[OrderPayViewController class]]) {
            tempController = vc;
            returnLastPage = 1;
            break;
        }
        if ([vc isKindOfClass:[WithdrawViewController class]]) {
            tempController = vc;
            returnLastPage = 3;
            break;
        }
        if ([vc isKindOfClass:[TransferAccountViewController class]]) {
            tempController = vc;
            returnLastPage = 3;
            break;
        }
    }
    if (returnLastPage==0) {
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        [self.navigationController popToViewController:tempController animated:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self getUserInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
