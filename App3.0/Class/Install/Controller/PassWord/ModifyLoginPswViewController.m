//
//  ModifyLoginPswViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ModifyLoginPswViewController.h"
#import "XSCustomButton.h"
#import "UserLoginedMissLoginPswViewController.h"
#import "RSAEncryptor.h"
#define leftSpace 20
#define inputHeight 150
#define baseHeight 45
#define labelWidth 100
@interface ModifyLoginPswViewController ()<UITextFieldDelegate>
{
    
    UITextField *_oldPassWord;
    UITextField *_newPassWord;
    UITextField *_verifyPassWord;
}

@end

@implementation ModifyLoginPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubviews];
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setSubviews{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, mainWidth-leftSpace, leftSpace)];
    titleLabel.text = Localized(@"密码由6-20位英文字母、数字或者符号组成");
    titleLabel.textColor = mainGrayColor;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:titleLabel];
    
    //密码输入框view
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame)+leftSpace/2, mainWidth, inputHeight)];
    inputView.backgroundColor = [UIColor whiteColor];
    UILabel *oldLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, leftSpace/2, labelWidth, leftSpace*3/2)];
    oldLabel.textColor = [UIColor blackColor];
    oldLabel.text = Localized(@"pwd_login_old");
    oldLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:oldLabel];
    _oldPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(oldLabel.frame), CGRectGetMinY(oldLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(oldLabel.frame), CGRectGetHeight(oldLabel.frame))];
    _oldPassWord.delegate = self;
    _oldPassWord.placeholder = Localized(@"pwd_login_old_hint");
    _oldPassWord.font = [UIFont systemFontOfSize:15];
    _oldPassWord.font = [UIFont systemFontOfSize:15];
    _oldPassWord.secureTextEntry = YES;
    _oldPassWord.returnKeyType = UIReturnKeyNext;
    [inputView addSubview:_oldPassWord];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(oldLabel.frame), CGRectGetMaxY(oldLabel.frame)+leftSpace/2, mainWidth-CGRectGetMinX(oldLabel.frame), 1)];
    lineView.backgroundColor = LINE_COLOR;
    [inputView addSubview:lineView];
    
    UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(lineView.frame)+leftSpace/2, labelWidth, leftSpace*3/2)];
    newLabel.textColor = [UIColor blackColor];
    newLabel.text = Localized(@"pwd_login_new");
    newLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:newLabel];
    _newPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(newLabel.frame), CGRectGetMinY(newLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(newLabel.frame), CGRectGetHeight(newLabel.frame))];
    _newPassWord.delegate = self;
    _newPassWord.placeholder = Localized(@"pwd_login_new");
    _newPassWord.font = [UIFont systemFontOfSize:15];
    _newPassWord.font = [UIFont systemFontOfSize:15];
    _newPassWord.secureTextEntry = YES;
    _newPassWord.returnKeyType = UIReturnKeyNext;
    [inputView addSubview:_newPassWord];
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(oldLabel.frame), CGRectGetMaxY(newLabel.frame)+leftSpace/2, mainWidth-CGRectGetMinX(newLabel.frame), 1)];
    lineView2.backgroundColor = LINE_COLOR;
    [inputView addSubview:lineView2];
    
    UILabel *verifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(lineView2.frame)+leftSpace/2, labelWidth, leftSpace*3/2)];
    verifyLabel.textColor = [UIColor blackColor];
    verifyLabel.text = Localized(@"pwd_login_renew");
    verifyLabel.font = [UIFont systemFontOfSize:15];
    [inputView addSubview:verifyLabel];
    _verifyPassWord = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verifyLabel.frame), CGRectGetMinY(verifyLabel.frame), mainWidth-2*leftSpace-CGRectGetWidth(verifyLabel.frame), CGRectGetHeight(verifyLabel.frame))];
    _verifyPassWord.delegate = self;
    _verifyPassWord.placeholder = Localized(@"pwd_login_renew");
    _verifyPassWord.font = [UIFont systemFontOfSize:15];
    _verifyPassWord.font = [UIFont systemFontOfSize:15];
    _verifyPassWord.secureTextEntry = YES;
    _verifyPassWord.returnKeyType = UIReturnKeyDone;
    [inputView addSubview:_verifyPassWord];
    //忘记密码
    UIButton *missBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-leftSpace-labelWidth, CGRectGetMaxY(inputView.frame)+leftSpace/2+10, labelWidth, leftSpace+10)];
    [missBtn setTitle:Localized(@"pwd_login_forget") forState:UIControlStateNormal];
    [missBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    missBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [missBtn addTarget:self action:@selector(misAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:missBtn];
    //保存按钮
    
    XSCustomButton *saveBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, leftSpace/2+CGRectGetMaxY(missBtn.frame), mainWidth-2*10, leftSpace*2) title:Localized(@"save") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [saveBtn setBorderWith:0 borderColor:nil cornerRadius:5];
    [saveBtn addTarget:self action:@selector(sveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    
    [self.view addSubview:inputView];
    
}
#pragma mark - 输入框协议
#pragma mark - 返回按钮，切换响应者
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _oldPassWord) {
        return [_newPassWord becomeFirstResponder];
        
    }else if(textField == _newPassWord) {
        return [_verifyPassWord becomeFirstResponder];
        
    }else{
        
        return [_verifyPassWord resignFirstResponder];
    }
    
}

#pragma mark - 忘记密码
-(void)misAction:(UIButton *) sender{
    UserLoginedMissLoginPswViewController *vc=[[UserLoginedMissLoginPswViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 保存
-(void)sveAction:(UIButton *) sender{
    NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_oldPassWord.text]];
    NSString *encryptNewPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_newPassWord.text]];
    NSString *encryptVerifyPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_verifyPassWord.text]];
    NSLog(@"%@,%@,%@",encryptOldPassWord,encryptNewPassWord,encryptVerifyPassWord);
    @weakify(self);
    [HTTPManager resetLoginPwdWithParamDic:@{@"type":@"pwd",@"oldPwd":encryptOldPassWord,@"newPwd":encryptNewPassWord,@"rePwd":encryptVerifyPassWord} WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if(state.status){//成功
            [XSTool showToastWithView:self.view Text:@"修改登录密码成功"];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:dic[@"data"][@"guid"] forKey:APPAUTH_KEY];
            [ud synchronize];
            [self performSelector:@selector(popViewController) withObject:dic afterDelay:1];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
          [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"pwd_login_mod");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
         @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)popViewController{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
