//
//  MissPayPswViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MissPayPswViewController.h"
#import "SetPayPswViewController.h"
#import "LoginModel.h"
#import "AppDelegate.h"
@interface MissPayPswViewController ()
{
    UITextField *_tfPhone;
    UITextField *_tfCode;
    XSCustomButton *_nextBtn;
    XSCustomButton *_getCodeBtn;//获取验证码按钮
    BOOL _isTimer;// 是否在倒计时
    BOOL _phoneFormatIsTrue;//手机格式是否正确
}
@end
@implementation MissPayPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:Localized(@"找回支付密码") action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self setSubViews];
    
}
#pragma mark - 设置子视图
-(void)setSubViews{
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @" ";
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, mainWidth, 100)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgView];
    
    // textField-leftView
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    // 用户名输入框
    _tfPhone = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, mainWidth, 30)];
    _tfPhone.placeholder = Localized(@"请输入11位手机号码");
    _tfPhone.font = [UIFont systemFontOfSize:14];
    _tfPhone.leftView = leftView;
    _tfPhone.leftViewMode = UITextFieldViewModeAlways;
    _tfPhone.keyboardType = UIKeyboardTypePhonePad;
    [_tfPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_tfPhone];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(60, 50, mainWidth-60, 1)];
    lineView.backgroundColor = BG_COLOR;
    [bgView addSubview:lineView];
    
    leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    // 验证码输入框
    _tfCode = [[UITextField alloc] initWithFrame:CGRectMake(0, 60, mainWidth-110, 30)];
    _tfCode.placeholder = Localized(@"retrieve_code");
    _tfCode.font = [UIFont systemFontOfSize:14];
    _tfCode.leftView = leftView;
    _tfCode.leftViewMode = UITextFieldViewModeAlways;
    _tfCode.keyboardType = UIKeyboardTypeNumberPad;
    [_tfCode addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_tfCode];
    
    //右侧按钮
    _getCodeBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_tfCode.frame), CGRectGetMinY(_tfCode.frame)-5, 100, 40) title:Localized(@"register_sms") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [_getCodeBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [_getCodeBtn setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    [_getCodeBtn addTarget:self action:@selector(getCodAction:) forControlEvents:UIControlEventTouchUpInside];
//    _getCodeBtn.enabled = NO;
    [bgView addSubview:_getCodeBtn];
    
    //下一步
    _nextBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, 80+120, mainWidth-20, 50) title:Localized(@"bind_mobile_next") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [_nextBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [_nextBtn setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    _nextBtn.enabled = NO;
    [_nextBtn addTarget:self action:@selector(nexAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_nextBtn];
    //获取用户手机号
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData=[user objectForKey:USERINFO_LOGIN];
    LoginDataParser *parser=[NSKeyedUnarchiver unarchiveObjectWithData:userData];
    _tfPhone.text=parser.mobile;
    _tfPhone.userInteractionEnabled=NO;
}
#pragma mark - 获取验证码
-(void)getCodAction:(UIButton *) sender{
    //获取验证码
    
    [HTTPManager getSmsVerifyWithDic:@{@"mobile":_tfPhone.text} success:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (!state.status) {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            return ;
        }else{
            _isTimer = YES; // 设置倒计时状态为YES
            sender.enabled = NO; // 设置按钮为不可点击
            __block NSInteger time = 59; //倒计时时间
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            
            dispatch_source_set_event_handler(_timer, ^{
                
                if(time <= 0){ //倒计时结束，关闭
                    
                    dispatch_source_cancel(_timer);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //设置按钮的样式
                        [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                        sender.enabled = YES; // 设置按钮可点击
                        
                        _isTimer = NO; // 倒计时状态为NO
                    });
                    
                }else{
                    
                    int seconds = time % 60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //设置按钮显示读秒效果
                        [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), seconds] forState:UIControlStateNormal];
                        
                    });
                    time--;
                }
            });
            dispatch_resume(_timer);
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}

#pragma mark - 下一步
-(void)nexAction:(UIButton *) sender{
    //检测验证码
    @weakify(self);
    [HTTPManager checkSmsVerifyValidWithMobile:_tfPhone.text smsVerify:_tfCode.text success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            SetPayPswViewController *payVC = [[SetPayPswViewController alloc] init];
            payVC.verify=_tfCode.text;
            payVC.phone=_tfPhone.text;
            [self.navigationController pushViewController:payVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:state.info];
        }

    } fail:^(NSError * _Nonnull error) {
         [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
//    BOOL phoneTrue = NO;
    BOOL codeTrue = NO;
//    // 手机号码格式判断
//    if (_tfPhone.text.length == 11) {
//        phoneTrue = YES;
//        if (_isTimer == NO) {
//            _getCodeBtn.enabled = YES;
//        } else {
//            _getCodeBtn.enabled = NO;
//        }
//    } else {
//        phoneTrue = NO;
//        _getCodeBtn.enabled = NO;
//    }
    
    // 验证码格式判断
    if (_tfCode.text.length == 6) {
        codeTrue = YES;
    } else {
        codeTrue = NO;
    }
    
    // 都符合可进行下一步
    if (codeTrue) {
        _nextBtn.enabled = YES;
    } else {
        _nextBtn.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
