//
//  SetPayPswViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserLoginedSetLoginPswViewController.h"
#import "XSApi.h"
#import "ModifyPassWordViewController.h"
#import "RSAEncryptor.h"
#import "LoginViewController.h"
#import "EnterViewController.h"
#import "AppDelegate.h"

@interface UserLoginedSetLoginPswViewController ()
{
    NSString *_mobile;
    NSString *_smsVerify;
    
    UITextField *_tfPwd11;
    UITextField *_tfPwdAgain11;
    XSCustomButton *_finishBtn11;
}
@end

@implementation UserLoginedSetLoginPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = BG_COLOR;
    self.title = @"";
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:Localized(@"找回登录密码") action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, mainWidth, 100)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgView];
    
    // textField-leftView
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    // 密码输入框
    _tfPwd11 = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, mainWidth, 30)];
    _tfPwd11.placeholder = @"设置6~20位登录密码";
    _tfPwd11.font = [UIFont systemFontOfSize:14];
    _tfPwd11.leftView = leftView;
    _tfPwd11.leftViewMode = UITextFieldViewModeAlways;
    _tfPwd11.delegate=self;
    _tfPwd11.secureTextEntry=YES;
    [_tfPwd11 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_tfPwd11];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(60, 50, mainWidth-60, 1)];
    lineView.backgroundColor = BG_COLOR;
    [bgView addSubview:lineView];
    
    leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    leftImg.frame = CGRectMake(20, 0, 30, 30);
    [leftView addSubview:leftImg];
    // 再次输入密码输入框
    _tfPwdAgain11 = [[UITextField alloc] initWithFrame:CGRectMake(0, 60, mainWidth, 30)];
    _tfPwdAgain11.placeholder = Localized(@"reset_pwd2");
    _tfPwdAgain11.font = [UIFont systemFontOfSize:14];
    _tfPwdAgain11.leftView = leftView;
    _tfPwdAgain11.delegate=self;
    _tfPwdAgain11.secureTextEntry=YES;
    [_tfPwdAgain11 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _tfPwdAgain11.leftViewMode = UITextFieldViewModeAlways;
    [bgView addSubview:_tfPwdAgain11];
    
    // 完成
    _finishBtn11 = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, 20+120, mainWidth-20, 50) title:Localized(@"完成") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [_finishBtn11 setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [_finishBtn11 setDisabledBackgroundColor:COLOR_BUTTON_DISABLED titleColor:[UIColor whiteColor]];
    [_finishBtn11 addTarget:self action:@selector(finAction) forControlEvents:UIControlEventTouchUpInside];
    _finishBtn11.enabled = NO;
    [self.view addSubview:_finishBtn11];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (id)initWithMobile:(NSString *)mobile smsVerify:(NSString *)smsVerify
{
    self = [super init];
    if (self) {
        _mobile = mobile;
        _smsVerify = smsVerify;
    }
    return self;
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if ([_tfPwdAgain11.text isEqualToString:_tfPwd11.text]&&_tfPwd11.text.length) {
        _finishBtn11.enabled = YES;
    }else {
        _finishBtn11.enabled = NO;
    }
}

- (void)finAction
{
     NSString *encryptPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_tfPwd11.text]];
    NSLog(@"%@",encryptPassWord);
    //参数
    NSDictionary *dic=@{@"type":@1,@"mode":@1,@"mobile":self.phone,@"password":encryptPassWord,@"verify":self.verify};
    [HTTPManager findLoginPwdWithParamDic:dic WithSuccess:^(NSDictionary *dic, resultObject *state) {
        if(state.status){//成功
            [XSTool showToastWithView:self.view Text:@"找回登录密码成功,请重新登录"];
            [self performSelector:@selector(popViewController) withObject:nil afterDelay:1];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)popViewController{
    //删除登陆的账号填充
//    [[UserInstance ShardInstnce] deleteAccount:@{@"account":self.phone}];
    
    //清除信息
    [[UserInstance ShardInstnce] logout];
    
    //跳转到enter界面
    EnterViewController *enter = [[EnterViewController alloc] init];
    UINavigationController* enterNav = [[UINavigationController alloc] initWithRootViewController:enter];
    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = nil;
    appDelegate.window.rootViewController = enterNav;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
