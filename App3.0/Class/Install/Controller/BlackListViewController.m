//
//  BlackListViewController.m
//  App3.0
//
//  Created by mac on 2017/7/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BlackListViewController.h"
#import "AttentionCell.h"
#import "SearchVC.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "EmptyView.h"
#import "PersonalViewController.h"

@interface BlackListViewController () <UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property(nonatomic, strong)UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *indexs;
@property (nonatomic, strong) NSMutableArray *blackListArr;
@property (nonatomic, strong) EmptyView *emptyView;
@end

@implementation BlackListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"黑名单");
    self.autoHideKeyboard = YES;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.searchBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    __weak typeof (self) wSelf = self;
    // 下拉刷新
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [wSelf.tableView reloadData];
//        [wSelf.tableView.mj_header endRefreshing];
//    }];
    
    // 添加空页面
    _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleContact frame:self.view.bounds];
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    [self loadData];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (NSMutableArray *)blackListArr {
    if (!_blackListArr) {
        _blackListArr = [NSMutableArray array];
    }
    return _blackListArr;
}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (NSArray *)sortedArray:(NSArray *)array {
    // 首字母排序
    NSArray *resultArr = [array contactArrayWithPinYinFirstLetterFormat];
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in resultArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
    return resultArr;
}

- (void)reloadData {
    if (self.blackListArr.count > 0) {
        [self.blackListArr removeAllObjects];
    }
    if (self.indexs.count > 0) {
        [self.indexs removeAllObjects];
    }
    // 从本地数据库拿数据
    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myBlackList = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
        if ([parser.is_shield integerValue] == 1) {
            [myBlackList addObject:parser];
        }
    }
    if (myBlackList.count > 0) {
        [self.blackListArr addObjectsFromArray:[self sortedArray:myBlackList]];
    }
    [self.tableView reloadData];
    if (self.blackListArr.count == 0) {
        _emptyView.hidden = NO;
    } else {
        _emptyView.hidden = YES;
    }
}

- (void)loadData {
    if (self.blackListArr.count > 0) {
        [self.blackListArr removeAllObjects];
    }
    
    // 从本地数据库拿数据
    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myBlackList = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
        if ([parser.is_shield integerValue] == 1) {
            [myBlackList addObject:parser];
        }
    }
    if (myBlackList.count > 0) {
        [self.blackListArr addObjectsFromArray:[self sortedArray:myBlackList]];
    }
    [self.tableView reloadData];
    if (self.blackListArr.count == 0) {
        _emptyView.hidden = NO;
    } else {
        _emptyView.hidden = YES;
    }

    @weakify(self);
    [HTTPManager getMyBlackListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                for (ContactDataParser *pa in myBlackList) {
                    [[DBHandler sharedInstance] updateContactWithKey:@"shield" value:@"0" byUid:pa.uid];
                }
                [myBlackList removeAllObjects];
                BlackListModel *model = [BlackListModel mj_objectWithKeyValues:dic];
                [myBlackList addObjectsFromArray:model.data];
                for (BlackListDataModel *pa in myBlackList) {
                    [[DBHandler sharedInstance] updateContactWithKey:@"shield" value:@"1" byUid:pa.account];
                }
                
                [self.blackListArr removeAllObjects];
                [self.blackListArr addObjectsFromArray:[self sortedArray:myBlackList]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.blackListArr.count == 0) {
                        _emptyView.hidden = NO;
                    } else {
                        _emptyView.hidden = YES;
                    }
                    [self.tableView reloadData];
                });
            });
            
        } else {
            
        }
    } fail:^(NSError *error) {
        
    }];
}

#pragma mark - searchbar delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myBlackList = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
        if ([parser.is_shield integerValue] == 1) {
            [myBlackList addObject:parser];
        }
    }
    [self.blackListArr removeAllObjects];
    if (searchText.length > 0) {
        NSMutableArray *tempArr = [NSMutableArray array];
        for (ContactDataParser *model in myBlackList) {
            if ([[model getName] containsString:searchText]) {
                [tempArr addObject:model];
            }
        }
        [self.blackListArr addObjectsFromArray:[tempArr contactArrayWithPinYinFirstLetterFormat]];
    } else {
        [self reloadData];
    }
    [self.tableView reloadData];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.blackListArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.blackListArr[section][@"content"];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = COLOR_666666;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = self.blackListArr[section][@"firstLetter"];
    titleLabel.text=title;
    [myView addSubview:titleLabel];
    
    return myView;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count;
        }
        count++;
    }
    return  0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.indexs objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    BlackListDataModel *contact = self.blackListArr[indexPath.section][@"content"][indexPath.row];
    PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:contact.account];
    [self.navigationController pushViewController:perVC animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"AttentionCell%ld ",(long)indexPath.row];
    AttentionCell *cell = (AttentionCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[AttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (self.blackListArr.count > 0) {
        [cell setCellData:self.blackListArr[indexPath.section][@"content"][indexPath.row]];
    }
    
    return cell;
}


- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, mainWidth, MAIN_VC_HEIGHT-44) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.sectionIndexColor = COLOR_666666;
    }
    return _tableView;
}

- (UISearchBar *)searchBar
{
    if (_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 2, mainWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.placeholder = Localized(@"search");
        _searchBar.translucent = YES; // 设置是否透明
        // 去掉灰色边框
        for (UIView *view in self.searchBar.subviews) {
            if ([view isKindOfClass:NSClassFromString(@"UIView")]&&view.subviews.count>0) {
                view.backgroundColor = BG_COLOR;
                [[view.subviews objectAtIndex:0] removeFromSuperview];
                break;
            }
        }
    }
    return _searchBar;
}
@end
