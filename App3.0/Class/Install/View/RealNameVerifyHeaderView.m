//
//  RealNameVerifyHeaderView.m
//  App3.0
//
//  Created by admin on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RealNameVerifyHeaderView.h"
#import "RealNameVerificationModel.h"
#import "UIButton+XSWebImage.h"

@interface RealNameVerifyHeaderView ()<UITextFieldDelegate>

/** line1View */
@property (nonatomic, strong) UIView * line1View;

/** 认证进度图片 */
@property (nonatomic, strong) UIImageView * verifyImageV1;
/** 认证进度图片 */
@property (nonatomic, strong) UIImageView * verifyImageV2;
/** 认证进度图片 */
@property (nonatomic, strong) UIImageView * verifyImageV3;
/** 认证进度图片 */
@property (nonatomic, strong) UIImageView * verifyImageV4;
/** 重新填写label */
@property (nonatomic, strong) UILabel * verifyLabel3;
/** 真实姓名 */
@property (nonatomic, strong) UITextField * realNameTF;
/** 身份证号 */
@property (nonatomic, strong) UITextField * IDCardNoTF;
/** 身份证正面 */
@property (nonatomic, strong) UIButton * IDCardFrontBtn;
/** 身份证背面 */
@property (nonatomic, strong) UIButton * IDCardBackBtn;
/** footerView */
@property (nonatomic, strong) UIView * tableFooterV;
/** 确认按钮 */
@property (nonatomic, strong) UIButton * determineBtn;
/** 说明、失败原因 */
@property (nonatomic, strong) UILabel * tipsLab;


@end

@implementation RealNameVerifyHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
        self.isKeyboardShow = NO;
    }
    return self;
}

-(void)setUpUI {
    self.backgroundColor = BG_COLOR;
    
    UIView * bgView1 = [[UIView alloc] init];
    bgView1.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView1];
    
    [bgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(10);
        make.height.mas_equalTo(92);
    }];
    
    //认证进度
    UILabel * verifyProgressLab = [[UILabel alloc] init];
    verifyProgressLab.text = Localized(@"rzjd");
    verifyProgressLab.font = [UIFont systemFontOfSize:14];
    verifyProgressLab.textColor = [UIColor hexFloatColor:@"171717"];
    [self addSubview:verifyProgressLab];
    [verifyProgressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(11.5);
        make.top.mas_equalTo(bgView1).offset(15);
    }];
    
    //认证进度中间线
    UIView * line1View = [[UIView alloc] init];
    line1View.backgroundColor = [UIColor hexFloatColor:@"dedede"];
    [self addSubview:line1View];
    self.line1View = line1View;
    
    [line1View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bgView1);
        make.width.mas_equalTo(155);
        make.height.mas_equalTo(2);
    }];
    
    //信息填写ImageView
    self.verifyImageV1 = [[UIImageView alloc] init];
    [self addSubview:self.verifyImageV1];
    
    [self.verifyImageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(line1View.mas_left).offset(5);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    //信息填写ImageView
    self.verifyImageV2 = [[UIImageView alloc] init];
    [self addSubview:self.verifyImageV2];
    
    [self.verifyImageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.verifyImageV1.mas_right).offset(64);
        make.centerY.mas_equalTo(line1View);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    //信息审核||重新填写ImageView
    self.verifyImageV3 = [[UIImageView alloc] init];
    [self addSubview:self.verifyImageV3];
    
    [self.verifyImageV3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(line1View);
        make.size.centerY.mas_equalTo(self.verifyImageV1);
    }];
    
    //认证通过ImageView
    self.verifyImageV4 = [[UIImageView alloc] init];
    [self addSubview:self.verifyImageV4];
    
    [self.verifyImageV4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line1View.mas_right).offset(-5);
        make.left.mas_equalTo(self.verifyImageV3.mas_right).offset(64);
        make.size.centerY.mas_equalTo(self.verifyImageV1);
    }];
    
    self.verifyImageV1.image = [UIImage imageNamed:@"verify_tatus2"];
    self.verifyImageV2.image = self.verifyImageV4.image = [UIImage imageNamed:@"verify_tatus1"];
    
    //信息填写
    UILabel * verifyLab1 = [[UILabel alloc] init];
    verifyLab1.text = Localized(@"xxtx");
    verifyLab1.textColor = [UIColor hexFloatColor:@"666666"];
    verifyLab1.font = [UIFont systemFontOfSize:12];
    [self addSubview:verifyLab1];
    
    [verifyLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.verifyImageV1);
        make.top.mas_equalTo(self.verifyImageV1.mas_bottom).offset(7.5f);
    }];
    
    //信息审核
    UILabel * verifyLab2 = [[UILabel alloc] init];
    verifyLab2.text = Localized(@"xxsh");
    verifyLab2.textColor = [UIColor hexFloatColor:@"666666"];
    verifyLab2.font = [UIFont systemFontOfSize:12];
    [self addSubview:verifyLab2];
    
    [verifyLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.verifyImageV2);
        make.top.mas_equalTo(self.verifyImageV2.mas_bottom).offset(7.5f);
    }];
    
    //重新填写
    UILabel * verifyLab3 = [[UILabel alloc] init];
    verifyLab3.text = Localized(@"cxtx");
    verifyLab3.textColor = [UIColor hexFloatColor:@"666666"];
    verifyLab3.font = [UIFont systemFontOfSize:12];
    verifyLab3.hidden = YES;
    [self addSubview:verifyLab3];
    self.verifyLabel3 = verifyLab3;
    
    [verifyLab3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.verifyImageV3);
        make.top.mas_equalTo(self.verifyImageV3.mas_bottom).offset(7.5f);
    }];
    
    //信息审核
    UILabel * verifyLab4 = [[UILabel alloc] init];
    verifyLab4.text = Localized(@"rztg");
    verifyLab4.textColor = [UIColor hexFloatColor:@"666666"];
    verifyLab4.font = [UIFont systemFontOfSize:12];
    [self addSubview:verifyLab4];
    
    [verifyLab4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.verifyImageV4);
        make.top.mas_equalTo(self.verifyImageV4.mas_bottom).offset(7.5f);
    }];
    
    UIView * bgViewMid = [[UIView alloc] init];
    bgViewMid.backgroundColor = [UIColor clearColor];
    [self addSubview:bgViewMid];
    
    [bgViewMid mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(bgView1.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    
    //tips
    self.tipsLab = [[UILabel alloc] init];
    self.tipsLab.text = Localized(@"说明：实名认证后可通过认证信息进行账号找回，保证资金安全！");
    self.tipsLab.textColor = [UIColor hexFloatColor:@"666666"];
    self.tipsLab.numberOfLines = 0;
    self.tipsLab.font = [UIFont systemFontOfSize:11];
    [self addSubview:self.tipsLab];
    
    [self.tipsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgViewMid).offset(12);
        make.right.mas_equalTo(bgViewMid).offset(-12);
        make.centerY.mas_equalTo(bgViewMid);
    }];
    
    UIView * bgView2 = [[UIView alloc] init];
    bgView2.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView2];
    
    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(bgViewMid.mas_bottom);
        make.height.mas_equalTo(88.5f);
    }];
    
    //实名身份证中间线
    UIView * line2View = [[UIView alloc] init];
    line2View.backgroundColor = [UIColor hexFloatColor:@"dedede"];
    [self addSubview:line2View];
    
    [line2View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.right.mas_equalTo(bgView2);
        make.left.mas_equalTo(bgView2).offset(12);
        make.height.mas_equalTo(0.5f);
    }];
    
    //真实姓名
    UILabel * realNameLab = [[UILabel alloc] init];
    realNameLab.text = Localized(@"user_approve_name");
    realNameLab.font = [UIFont systemFontOfSize:14];
    realNameLab.textColor = [UIColor hexFloatColor:@"171717"];
    [self addSubview:realNameLab];
    
    [realNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(11.5);
        make.top.mas_equalTo(bgView2.mas_top).offset(15);
        make.width.mas_equalTo(60);
    }];
    
    //真实姓名输入框
    self.realNameTF = [[UITextField alloc] init];
    self.realNameTF.placeholder = Localized(@"user_approve_name_hint");
    self.realNameTF.textColor = [UIColor hexFloatColor:@"171717"];
    self.realNameTF.font = [UIFont systemFontOfSize:14];
    self.realNameTF.returnKeyType = UIReturnKeyNext;
    self.realNameTF.delegate = self;
    [self addSubview:self.realNameTF];
    
    [self.realNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(realNameLab.mas_right).offset(13);
        make.right.mas_equalTo(self).offset(-13);
        make.centerY.mas_equalTo(realNameLab);
        make.height.mas_equalTo(30);
    }];
    
    //身份证号
    UILabel * idNumberLab = [[UILabel alloc] init];
    idNumberLab.text = Localized(@"user_approve_cardno");
    idNumberLab.font = [UIFont systemFontOfSize:14];
    idNumberLab.textColor = [UIColor hexFloatColor:@"171717"];
    [self addSubview:idNumberLab];
    
    [idNumberLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(11.5f);
        make.bottom.mas_equalTo(bgView2.mas_bottom).offset(-15.5f);
        make.width.mas_equalTo(60);
    }];
    
    //身份证号输入框
    self.IDCardNoTF = [[UITextField alloc] init];
    self.IDCardNoTF.placeholder = Localized(@"user_approve_cardno_hint");
    self.IDCardNoTF.textColor = [UIColor hexFloatColor:@"171717"];
    self.IDCardNoTF.font = [UIFont systemFontOfSize:14];
    self.IDCardNoTF.returnKeyType = UIReturnKeyDone;
    self.IDCardNoTF.delegate = self;
    [self addSubview:self.IDCardNoTF];
    
    [self.IDCardNoTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(idNumberLab.mas_right).offset(13);
        make.right.mas_equalTo(self).offset(-13);
        make.centerY.mas_equalTo(idNumberLab);
        make.height.mas_equalTo(30);
    }];
    
    UIView * bgView3 = [[UIView alloc] init];
    bgView3.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView3];
    
    [bgView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(bgView2.mas_bottom).offset(6);
        make.height.mas_equalTo(235);
    }];
    
    self.IDCardFrontBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.IDCardFrontBtn setBackgroundImage:[UIImage imageNamed:@"idCard_front"] forState:UIControlStateNormal];
    [self.IDCardFrontBtn addTarget:self action:@selector(IDCardFrontBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.IDCardFrontBtn];
    [self.IDCardFrontBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView3).offset(27.5f);
        make.centerX.mas_equalTo(bgView3);
        make.size.mas_equalTo(CGSizeMake(205, 130));
    }];
    
    //上传身份证正面
    UILabel * descLab1 = [[UILabel alloc] init];
    descLab1.text = Localized(@"user_approve_card_front");
    descLab1.font = [UIFont systemFontOfSize:14];
    descLab1.textColor = [UIColor hexFloatColor:@"171717"];
    [self addSubview:descLab1];
    
    [descLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.IDCardFrontBtn.mas_bottom).offset(17.5f);
        make.centerX.mas_equalTo(self.IDCardFrontBtn);
    }];
    
    //上传图片大小控制在3M以内！
    UILabel * descLab2 = [[UILabel alloc] init];
    descLab2.text = Localized(@"上传图片大小控制在3M以内！");
    descLab2.font = [UIFont systemFontOfSize:11];
    descLab2.textColor = [UIColor hexFloatColor:@"999999"];
    [self addSubview:descLab2];
    
    [descLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(descLab1.mas_bottom).offset(12.5f);
        make.centerX.mas_equalTo(self.IDCardFrontBtn);
    }];
    
    //身份证照片背面
    UIView * bgView4 = [[UIView alloc] init];
    bgView4.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView4];
    
    [bgView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(bgView3.mas_bottom).offset(6);
        make.height.mas_equalTo(235);
    }];
    
    self.IDCardBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.IDCardBackBtn setBackgroundImage:[UIImage imageNamed:@"idCard_back"] forState:UIControlStateNormal];
    [self.IDCardBackBtn addTarget:self action:@selector(IDCardBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.IDCardBackBtn];
    [self.IDCardBackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView4).offset(27.5f);
        make.centerX.mas_equalTo(bgView4);
        make.size.mas_equalTo(CGSizeMake(205, 130));
    }];
    
    //上传身份证背面
    UILabel * descLab3 = [[UILabel alloc] init];
    descLab3.text = Localized(@"user_approve_card_back");
    descLab3.font = [UIFont systemFontOfSize:14];
    descLab3.textColor = [UIColor hexFloatColor:@"171717"];
    [self addSubview:descLab3];
    
    [descLab3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.IDCardBackBtn.mas_bottom).offset(17.5f);
        make.centerX.mas_equalTo(self.IDCardBackBtn);
    }];
    
    //上传图片大小控制在3M以内！
    UILabel * descLab4 = [[UILabel alloc] init];
    descLab4.text = Localized(@"上传图片大小控制在3M以内！");
    descLab4.font = [UIFont systemFontOfSize:11];
    descLab4.textColor = [UIColor hexFloatColor:@"999999"];
    [self addSubview:descLab4];
    
    [descLab4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(descLab3.mas_bottom).offset(12.5f);
        make.centerX.mas_equalTo(self.IDCardBackBtn);
    }];
    
    //tableFooterView
    UIView * footerView = [[UIView alloc] init];
    footerView.backgroundColor = BG_COLOR;
    [self addSubview:footerView];
    self.tableFooterV = footerView;
    
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView4.mas_bottom);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(94);
    }];
    
    self.determineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.determineBtn setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
    self.determineBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    self.determineBtn.backgroundColor = mainColor;
    self.determineBtn.layer.cornerRadius = 4;
    self.determineBtn.layer.masksToBounds = YES;
    
    [self.determineBtn addTarget:self action:@selector(determineBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:self.determineBtn];
    [self.determineBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(footerView);
        make.size.mas_equalTo(CGSizeMake(FontNum(325), 50));
    }];
}

-(void)setStatus:(RealNameVerificationModel *)model {
    if ([model.status integerValue] == -1 || [model.status integerValue] == 0 || [model.status integerValue] == 1) {
        [self.line1View mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(155);
        }];
        self.verifyImageV3.hidden = self.verifyLabel3.hidden = YES;
        
        if ([model.status integerValue] == -1) {
            self.verifyImageV1.image = [UIImage imageNamed:@"verify_tatus2"];
            self.verifyImageV2.image = self.verifyImageV4.image = [UIImage imageNamed:@"verify_tatus1"];
        } else {
            if ([model.status integerValue] == 0) {
                self.verifyImageV1.image = [UIImage imageNamed:@"verify_tatus4"];
                self.verifyImageV2.image = [UIImage imageNamed:@"verify_tatus2"];
                self.verifyImageV4.image = [UIImage imageNamed:@"verify_tatus1"];
            } else {
                self.verifyImageV1.image = self.verifyImageV2.image = self.verifyImageV4.image = [UIImage imageNamed:@"verify_tatus4"];
            }
            
            self.realNameTF.text = model.truename;
            self.IDCardNoTF.text = model.card_no;
            self.tipsLab.text = Localized(@"说明：实名认证后可通过认证信息进行账号找回，保证资金安全！");
            
            if (model.img_card) {
                [self.IDCardFrontBtn getBackImageWithUrlStr:model.img_card[0] andDefaultImage:[UIImage imageNamed:@"idCard_front"] forState:UIControlStateNormal];
                [self.IDCardBackBtn getBackImageWithUrlStr:model.img_card[1] andDefaultImage:[UIImage imageNamed:@"idCard_back"] forState:UIControlStateNormal];
            }
            
            self.realNameTF.userInteractionEnabled = self.IDCardNoTF.userInteractionEnabled = self.IDCardFrontBtn.userInteractionEnabled = self.IDCardBackBtn.userInteractionEnabled = NO;
        }
    } else {
        [self.line1View mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(236);
        }];
        self.verifyImageV3.hidden = self.verifyLabel3.hidden = NO;
        self.verifyImageV1.image = [UIImage imageNamed:@"verify_tatus4"];
        self.verifyImageV2.image = [UIImage imageNamed:@"verify_tatus3"];
        self.verifyImageV3.image = [UIImage imageNamed:@"verify_tatus2"];
        self.verifyImageV4.image = [UIImage imageNamed:@"verify_tatus1"];
        
        
        self.realNameTF.text = model.truename;
        self.IDCardNoTF.text = model.card_no;
        self.tipsLab.text = [NSString stringWithFormat:@"%@：%@",Localized(@"fail_reson"),model.comment];
        if (model.img_card) {
            [self.IDCardFrontBtn getBackImageWithUrlStr:model.img_card[0] andDefaultImage:[UIImage imageNamed:@"idCard_front"] forState:UIControlStateNormal];
            [self.IDCardBackBtn getBackImageWithUrlStr:model.img_card[1] andDefaultImage:[UIImage imageNamed:@"idCard_back"] forState:UIControlStateNormal];
        }
        
        self.realNameTF.userInteractionEnabled = self.IDCardNoTF.userInteractionEnabled = self.IDCardFrontBtn.userInteractionEnabled = self.IDCardBackBtn.userInteractionEnabled = YES;
        
    }
}

#pragma mark textField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.realNameTF) {
        return [self.IDCardNoTF becomeFirstResponder];
    } else {
        return [self.IDCardNoTF resignFirstResponder];
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.isKeyboardShow = YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.isKeyboardShow = NO;
}

#pragma mark 身份证照片按钮点击
-(void)IDCardFrontBtnClick {
    if ([self.delegate respondsToSelector:@selector(frontImageBtnClicked)]) {
        [self.delegate frontImageBtnClicked];
    }
}
-(void)IDCardBackBtnClick
{
    if ([self.delegate respondsToSelector:@selector(behindImageBtnClicked)]) {
        [self.delegate behindImageBtnClicked];
    }
}
#pragma mark 确定按钮点击
-(void)determineBtnClick
{
    if ([self.delegate respondsToSelector:@selector(upLoadDataWithRealName:IDCardNum:)]) {
        [self.delegate upLoadDataWithRealName:self.realNameTF.text IDCardNum:self.IDCardNoTF.text];
    }
}

#pragma mark 设置数据
-(void)setFrontImage:(UIImage *)frontImage {
    [self.IDCardFrontBtn setBackgroundImage:frontImage forState:UIControlStateNormal];
}
-(void)setBehindImage:(UIImage *)behindImage {
    [self.IDCardBackBtn setBackgroundImage:behindImage forState:UIControlStateNormal];
}

-(void)setFooterViewHide:(BOOL)footerHide
{
    self.tableFooterV.hidden = footerHide;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

@end
