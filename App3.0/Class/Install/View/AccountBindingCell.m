//
//  AccountBindingCell.m
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AccountBindingCell.h"
#import "AccountBindingModel.h"

@interface AccountBindingCell ()

/** logo */
@property (nonatomic, strong) UIImageView * logoImageView;
/** 账号类型 */
@property (nonatomic, strong) UILabel * titleLabel;
/** 账号类型描述 */
@property (nonatomic, strong) UILabel * descriptionLabel;
/** 账号绑定*/
@property (nonatomic, strong) UILabel * bindingLabel;

@end
@implementation AccountBindingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _logoImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_logoImageView];
        [_logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.size.mas_equalTo(CGSizeMake(36, 36));
            make.centerY.mas_equalTo(self);
        }];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.text = Localized(@"security_email_open");
        _titleLabel.textColor = [UIColor hexFloatColor:@"666666"];
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_logoImageView.mas_right).offset(10);
            make.baseline.mas_equalTo(self.mas_centerY).offset(6.75);
            make.width.mas_lessThanOrEqualTo(160);
        }];
        
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.font = [UIFont systemFontOfSize:11];
        _descriptionLabel.textColor = [UIColor hexFloatColor:@"666666"];
        [self.contentView addSubview:_descriptionLabel];
        _descriptionLabel.hidden = YES;
        
        [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_titleLabel);
            make.top.mas_equalTo(_titleLabel.mas_baseline).offset(7);
            make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH - 140);
        }];
        
        UIImageView * moreImageView = [[UIImageView alloc] init];
        moreImageView.image = [UIImage imageNamed:@"cart_gostore_arrow"];
        [self.contentView addSubview:moreImageView];
        
        [moreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-12.5f);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(7.5, 13));
        }];
        
        _bindingLabel = [[UILabel alloc] init];
        _bindingLabel.font = [UIFont systemFontOfSize:14];
//        _bindingLabel.text = @"去认证";
        _bindingLabel.textColor = [UIColor hexFloatColor:@"666666"];
        [self.contentView addSubview:_bindingLabel];
        
        [_bindingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(moreImageView.mas_left).offset(-6);
            make.centerY.mas_equalTo(moreImageView);
        }];
        
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor hexFloatColor:@"e6e6e6"];
        [self.contentView addSubview:lineView];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.mas_equalTo(self);
            make.left.mas_equalTo(self).offset(12);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

-(void)setDataWithModel:(AccountBindingModel *)model {
    self.titleLabel.text = model.name;
    if ([model.status integerValue] == 1) {
        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.baseline.mas_equalTo(self.mas_centerY).offset(-3.5);
        }];
        self.descriptionLabel.hidden = NO;
        self.descriptionLabel.text = [NSString stringWithFormat:@"%@：%@",Localized(@"当前已绑定"),model.target];
        
        if ([model.type isEqualToString:@"mobile"]) {
            self.bindingLabel.text = Localized(@"换绑手机");
        } else if ([model.type isEqualToString:@"email"]) {
            self.bindingLabel.text = Localized(@"更换绑定");
        } else if ([model.type isEqualToString:@"qq"] || [model.type isEqualToString:@"weixin"]) {
            self.bindingLabel.text = Localized(@"un_bind_btn_text");
        }
        self.bindingLabel.textColor = [UIColor hexFloatColor:@"171717"];
        
    } else {
        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.baseline.mas_equalTo(self.mas_centerY).offset(6.75);
        }];
        self.descriptionLabel.hidden = YES;
        
        self.bindingLabel.text = Localized(@"去绑定");
        self.bindingLabel.textColor = [UIColor hexFloatColor:@"666666"];
    }
    
    if ([model.type isEqualToString:@"mobile"]) {
        self.logoImageView.image = [UIImage imageNamed:@"user_binding_phone"];
    } else if ([model.type isEqualToString:@"email"]) {
        self.logoImageView.image = [UIImage imageNamed:@"user_bind_email"];
    } else if ([model.type isEqualToString:@"qq"]) {
        self.logoImageView.image = [UIImage imageNamed:@"user_bind_qq"];
    } else {
        self.logoImageView.image = [UIImage imageNamed:@"user_binding_wechat"];
    }
}

@end
