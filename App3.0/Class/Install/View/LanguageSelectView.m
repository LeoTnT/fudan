//
//  LanguageSelectView.m
//  App3.0
//
//  Created by xinshang on 2017/10/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LanguageSelectView.h"
//#import "NSBundle+Language.h"

#define CellHeight 45
@interface LanguageSelectView()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,copy) NSString *currentLg;

@end


@implementation LanguageSelectView
-(instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, mainWidth, mainHeight);
        if (!_dataArray) {
            _dataArray = [NSMutableArray array];
        }
        if (!self.currentLg) {
           self.currentLg = SelectLang;
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        self.userInteractionEnabled = YES;
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        [self loadTableView];
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
        
    }
    return self;
}
- (void)tapAction
{
    [self removeFromSuperview];
}

//然后根据具体的业务场景去写逻辑就可以了,比如
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    //Tip:我们可以通过打印touch.view来看看具体点击的view是具体是什么名称,像点击UITableViewCell时响应的View则是UITableViewCellContentView.
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        //返回为NO则屏蔽手势事件
        return NO;
    }
    return YES;
}
- (void)loadTableView
{
    self.userInteractionEnabled = YES;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(35, (mainHeight-64-80 - 320)/2, mainWidth - 35*2,320) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView=[[UIView alloc]init];//隐藏多余的分割线
    [self addSubview:self.tableView];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.dataArray.count+1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor=[UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryNone;

    }
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
 
        if (indexPath.row == 0) {
            cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.text = Localized(@"请选择语言");
        }else{
            if (self.dataArray.count > 0) {
                NSDictionary *dic = self.dataArray[indexPath.row-1];
                cell.textLabel.text = [NSString stringWithFormat:@"%@",dic.allValues.firstObject];
                
                if ([self.currentLg isEqualToString:dic.allKeys.firstObject]) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
        }
    
 
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.row == 0) {
            return;
        }else{
    
    if (self.dataArray.count > 0) {
        NSDictionary *dic = self.dataArray[indexPath.row-1];
        NSString *str = [NSString stringWithFormat:@"%@",dic.allKeys.firstObject];
        self.currentLg = str;
        [self.tableView reloadData];
        if (_SeletAction) {
            _SeletAction(dic.allValues.firstObject,str);
        }
    }
        }
}


- (void)setTableViewHeightWithArrayCount:(NSInteger)counts
{
    if (counts == 0) {
        return;
    }
    if (counts*CellHeight < 320) {
        self.tableView.frame = CGRectMake(35, (mainHeight-64-80- self.dataArray.count*CellHeight)/2, mainWidth - 35*2,counts*CellHeight);
        
    }else if(self.dataArray.count*CellHeight > mainHeight -64)
    {
        self.tableView.frame = CGRectMake(35,5, mainWidth - 35*2,mainHeight -64-90);
        
    }
    
    
}

-(void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self setTableViewHeightWithArrayCount:(self.dataArray.count+1)];
    [self.tableView reloadData];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    self.hidden = YES;
}

@end

