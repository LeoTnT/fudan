//
//  LanguageSelectView.h
//  App3.0
//
//  Created by xinshang on 2017/10/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageSelectView : UIView

@property (nonatomic,strong)NSArray *dataArray;
@property (nonatomic ,strong)void(^SeletAction)(NSString *name,NSString *value);

@end
