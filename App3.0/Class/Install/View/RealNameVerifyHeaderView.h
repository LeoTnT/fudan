//
//  RealNameVerifyHeaderView.h
//  App3.0
//
//  Created by admin on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RealNameVerificationModel;

@protocol RealNameVerificationDelegate <NSObject>

-(void)frontImageBtnClicked;

-(void)behindImageBtnClicked;

-(void)upLoadDataWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum;

@end

@interface RealNameVerifyHeaderView : UIView

/** 键盘是否显示 */
@property (nonatomic, assign) BOOL isKeyboardShow;
/** delegate */
@property (nonatomic, weak) id<RealNameVerificationDelegate>delegate;
/**设置当前审核状态界面显示*/
-(void)setStatus:(RealNameVerificationModel *)model;
/**设置身份证照*/
-(void)setFrontImage:(UIImage *)frontImage;
-(void)setBehindImage:(UIImage *)behindImage;
/**是否隐藏底部按钮*/
-(void)setFooterViewHide:(BOOL)footerHide;

@end
