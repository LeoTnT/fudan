//
//  AccountBindingCell.h
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AccountBindingModel;

@interface AccountBindingCell : UITableViewCell

-(void)setDataWithModel:(AccountBindingModel *)model;

@end
