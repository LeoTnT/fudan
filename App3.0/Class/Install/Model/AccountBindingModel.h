//
//  AccountBindingModel.h
//  App3.0
//
//  Created by admin on 2017/12/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPManager+Mine.h"
@interface AccountBindingModel : NSObject

/** 简介 */
@property (nonatomic, copy) NSString *memo;
/** 绑定标识 */
@property (nonatomic, copy) NSString *type;
/** 名称 */
@property (nonatomic, copy) NSString *name;
/** 绑定目标 */
@property (nonatomic, copy) NSString *target;
/** 绑定状态(0未绑定|1已绑定) */
@property (nonatomic, copy) NSString *status;

@end
