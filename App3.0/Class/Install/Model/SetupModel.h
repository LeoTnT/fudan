//
//  SetupModel.h
//  App3.0
//
//  Created by mac on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatSetModel : NSObject
@property (nonatomic, assign) unsigned int is_rec_msg;          //接收消息(1是|0否)
@property (nonatomic, assign) unsigned int is_show_msg;         //显示消息(1是|0否)
@property (nonatomic, assign) unsigned int is_voice;            //提示声音(1是|0否)
@property (nonatomic, assign) unsigned int is_vibrate;          //是否振动(1是|0否)
@property (nonatomic, assign) unsigned int is_not_disturb;      //勿扰时间(0否|1是)
@property (nonatomic, copy) NSString* not_disturb_begin;   //勿扰启用时间
@property (nonatomic, copy) NSString* not_disturb_end;     //勿扰结束时间
@property (nonatomic, assign) unsigned int is_enter_msg;        //回车发送(0否|1是)
@property (nonatomic, assign) unsigned int is_receiver_voice;   //听筒播放(0否|1是)
@property (nonatomic, assign) unsigned int is_can_talk;         //非好友是否可聊天(0否|1是)
@end

@interface SetupModel : NSObject

@end
