//
//  StoreInforSectionHeaderView.m
//  App3.0
//
//  Created by apple on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "StoreInforSectionHeaderView.h"

@implementation StoreInforSectionHeaderView
{
    UIButton *centerButton;
    UIImageView *imageView;
    UIButton *rightButton;
    UILabel *label;
}


- (void)setContentView {
    [super setContentView];
    self.backgroundColor = [UIColor whiteColor];
    
//    UIView *centerView = [BaseUITool viewWithColor:[UIColor whiteColor]];
//    [self addSubview:centerView];
//    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(mainWidth/2);
//        make.centerY.mas_equalTo(self);
//        make.height.mas_equalTo(self);
//    }];
//
   imageView = [BaseUITool imageWithName:@"ni_pic" superView:self];
    [self addSubview:imageView];

    label = [BaseUITool labelWithTitle:@" " textAlignment:0 font:[UIFont systemFontOfSize:16] titleColor:[UIColor hexFloatColor:@"000000"]];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];

    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(label.mas_left).offset(-5);
        make.size.mas_equalTo(CGSizeMake(19, 19));
        make.centerY.mas_equalTo(self);
    }];
    

    
//    centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [centerButton setTitleColor:[UIColor hexFloatColor:@"000000"] forState:UIControlStateNormal];
//    centerButton.titleLabel.font = [UIFont systemFontOfSize:16];
//    [centerButton setImagePosition:(LXMImagePositionLeft) spacing:5];
//    [self addSubview:centerButton];
//
//    [centerButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.mas_equalTo(self);
//    }];

    
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImagePosition:(LXMImagePositionRight) spacing:-5];
    [rightButton setTitleColor:[UIColor hexFloatColor:@"929292"] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:rightButton];
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-13.5);
    }];
    self.layer.masksToBounds = YES;
}

- (void)setType:(StoreInforHeaderType)type {
    _type = type;
    NSString *title;
    NSString *imageName;
    
    switch (type) {
        case StoreInforHeaderTypeOne:
            title = @"进店必看";
            imageName = @"s_inforOne";
            break;
        case StoreInforHeaderTypeTwo:
           
            title = @" 新品上市";
            imageName = @"s_inforTwo";
            break;
        case StoreInforHeaderTypeThree:
            title = @"精品特惠";
            imageName = @"s_inforThree";
            break;

    }
    label.text = title;
    imageView.image = [UIImage imageNamed:imageName];
    [centerButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [centerButton setTitle:title forState:UIControlStateNormal];
    if (type != StoreInforHeaderTypeOne) {
        [rightButton setTitle:@"更多" forState:UIControlStateNormal];
        [rightButton setImage:[UIImage imageNamed:@"cart_gostore_arrow"] forState:UIControlStateNormal];
    }
    
}



- (void)setModel:(S_SysProductModel *)model {
    _model = model;
    [rightButton setTitle:@"更多" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"cart_gostore_arrow"] forState:UIControlStateNormal];
    [centerButton setTitle:model.name forState:UIControlStateNormal];
    [centerButton setImage:[UIImage imageNamed:@"no_pic"] forState:UIControlStateNormal];
    label.text = model.name;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];

}

@end
