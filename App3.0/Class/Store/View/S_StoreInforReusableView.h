//
//  S_StoreInforReusableView.h
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "S_StoreInformation.h"


@interface S_StoreInforReusableView : UICollectionReusableView

@property (nonatomic ,strong)UIButton *collecButton; 
@property (nonatomic ,copy)void (^ButtonSelected)(NSInteger);
@property (nonatomic ,assign)BOOL is_fav;

@property (nonatomic,copy)void (^cashier)();
-(void)collectionHeadView:(S_SupplyInfoModel *)model;



@end

