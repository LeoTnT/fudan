//
//  S_StoreInForCell.h
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "S_StoreInformation.h"
@interface S_StoreInForCell : XSBaseCollectionCell

@property (nonatomic ,strong)Product_arrModel *model;


@end


@interface  S_SectionOneCell:XSBaseCollectionCell


@property (nonatomic ,copy)void (^tapAction)(SupplyAdvertModle *model);

@property (nonatomic ,strong)NSMutableArray *arr;

@property (nonatomic ,strong,readonly)NSArray *adverArr;

@end


@interface S_SectionImageModel :UIImageView


@property (nonatomic ,strong)SupplyAdvertModle *model;

@end
