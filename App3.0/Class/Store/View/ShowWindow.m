//
//  ShowWindow.m
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ShowWindow.h"
#import "ChatViewController.h"
@implementation ShowWindow
{
    UIImageView *headImage;
    UILabel *nameString;
}

+ (instancetype)creatViewWithWindow:(S_SupplyInfoModel *)model {
    
    return [[self alloc]initWithModel:model];
}


-(instancetype)initWithModel:(S_SupplyInfoModel *)model {
    
    if (self = [super init]) {
     
        [self creatContentView:model];
    }
    return self;
}

- (void)setModel:(S_SupplyInfoModel *)model {
 
    dispatch_async(dispatch_get_main_queue(), ^{
        [headImage getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        nameString.text = model.name;
    });
}

- (void) creatContentView:(S_SupplyInfoModel *)Model {
    
    
    self.backgroundColor = [UIColor colorWithRed:0/255.0f green:188/255.0f blue:40/255.0f alpha:1];
    
    self.layer.cornerRadius = 25;
    self.layer.masksToBounds = YES;
    headImage = [UIImageView new];

    headImage.layer.cornerRadius = 25;
    headImage.layer.masksToBounds = YES;
    [self addSubview:headImage];
    
    
    nameString = [UILabel new];
    
    nameString.textColor = [UIColor whiteColor];
    nameString.textAlignment = NSTextAlignmentCenter;
    if (IS_IPHONE_5) {
        nameString.font = [UIFont systemFontOfSize:13];
    }else if (IS_IPHONE_6){
        nameString.font = [UIFont systemFontOfSize:14];
    }else if (IS_IPHONE_6P){
        nameString.font = [UIFont systemFontOfSize:15];
    }
    
    nameString.adjustsFontSizeToFitWidth = YES;
    [self addSubview:nameString];
    
    UIImageView *rightImage = [UIImageView new];
    rightImage.image = [UIImage imageNamed:@"store_xh"];
    [self addSubview:rightImage];

    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.left.mas_equalTo(self.mas_left).offset(5);
    }];
    
    
    [nameString mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headImage.mas_right).offset(5);
        make.centerY.mas_equalTo(headImage.mas_centerY);
        make.height.mas_equalTo(40);
    }];
    
    [rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameString.mas_right).offset(5);
        make.right.mas_equalTo(self.mas_right).offset(-5);
        make.centerY.mas_equalTo(nameString.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
}


@end
