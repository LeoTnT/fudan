//
//  S_StoreInForCell.m
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreInForCell.h"
@interface S_StoreInForCell ()

//@property (nonatomic ,strong)UIImageView *storeImage;
//@property (nonatomic ,strong)UILabel *storeInfor;
//@property (nonatomic ,strong)UILabel *storePrice;// 原价
//@property (nonatomic ,strong)UILabel *storeDiscount;// 打折
@end

@implementation S_StoreInForCell
{
    UIImageView *storeImage;
    UILabel *storeInfor;
    UILabel *storePrice;
    UILabel *marketLabel;
}


- (void)setContentView {
    storeImage = [UIImageView new];
    self.backgroundColor = [UIColor whiteColor];
    
    [storeImage getImageWithUrlStr:nil andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    [self.contentView addSubview:storeImage];
    
    storeInfor = [UILabel new];
    storeInfor.numberOfLines = 2;
    storeInfor.textColor = [UIColor hexFloatColor:@"111111"];
    storeInfor.font = [UIFont systemFontOfSize:14];
    
    
    storePrice = [UILabel new];
    storePrice.font = [UIFont systemFontOfSize:18];
    storePrice.textColor = [UIColor hexFloatColor:@"3F8EF7"];
    
    
    [self.contentView addSubview:storeInfor];
    [self.contentView addSubview:storePrice];
    
    marketLabel = [UILabel new];
    marketLabel.textAlignment = NSTextAlignmentRight;
    marketLabel.textColor = [UIColor lightGrayColor];
    marketLabel.font = [UIFont systemFontOfSize:13];

    [self.contentView addSubview:marketLabel];
    
    
    
    CGFloat leftMargin = 5;
    CGFloat itemWidth = (mainWidth - 2)/2;
    [storeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(leftMargin);
        make.height.mas_equalTo(itemWidth);

    }];
    [storeInfor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(storeImage.mas_bottom).offset(2*leftMargin);
        make.left.mas_equalTo(self.mas_left).offset(2*leftMargin);
        make.right.mas_equalTo(-2*leftMargin);
    }];
    
    [storePrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(storeInfor.mas_bottom);
        make.left.mas_equalTo(storeInfor);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-5);
        make.right.mas_equalTo(self.mas_centerX);
    }];
    
    [marketLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-2*leftMargin);
        make.centerY.mas_equalTo(storePrice.mas_centerY);
        make.left.mas_equalTo(self.mas_centerX);
        
    }];
    
}


- (void)setModel:(Product_arrModel *)model {
    _model = model;
    Product_arrModel *proModel = [Product_arrModel mj_objectWithKeyValues:model];
    [storeImage getImageWithUrlStr:proModel.image_thumb andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    storeInfor.text = proModel.product_name;
    storePrice.text = [NSString stringWithFormat:@"¥%@",proModel.sell_price];
    marketLabel.text = [NSString stringWithFormat:@"%@人感兴趣",proModel.fav_num];
}

@end

@implementation S_SectionOneCell
{
    S_SectionImageModel *supply_left,*supply_right_top,*supply_right_bottom,*supply_bottom;
    
    NSMutableArray *positionData;
}

+ (S_SectionImageModel *) creatImageView:(UIView *)superView {
    S_SectionImageModel *imageView = [S_SectionImageModel new];
    [imageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"no_pic"]];
    imageView.userInteractionEnabled = YES;
    [superView addSubview:imageView];
    return imageView;
}

- (void)setContentView {
    
    supply_left = [S_SectionOneCell creatImageView:self.contentView];
    supply_right_top = [S_SectionOneCell creatImageView:self.contentView];
    supply_right_bottom = [S_SectionOneCell creatImageView:self.contentView];
    supply_bottom = [S_SectionOneCell creatImageView:self.contentView];

    
    [supply_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.contentView);
        make.width.mas_equalTo(mainWidth/3+45);
    }];
    
    [supply_right_top mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(self.contentView);
        make.left.mas_equalTo(supply_left.mas_right);
        make.bottom.mas_equalTo(supply_left.mas_centerY);
    }];
    
    [supply_right_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(supply_left.mas_right);
        make.bottom.mas_equalTo(supply_left);
        make.top.mas_equalTo(supply_right_top.mas_bottom);
        make.right.mas_equalTo(self.contentView);
        
    }];
 
    [supply_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.top.mas_equalTo(supply_left.mas_bottom);
    }];
    
    _adverArr = @[
                 @{
                     @"imageView":supply_left,
                     @"position":@"supply_left"
                     
                     },
                 @{
                     @"imageView":supply_right_top,
                     @"position":@"supply_right_top"
                     },
                 @{
                     @"imageView":supply_right_bottom,
                     @"position":@"supply_right_bottom"
                     },
                 @{
                     @"imageView":supply_bottom,
                     @"position":@"supply_bottom"
                     }
                 ];
    SEL s1 = @selector(supply_leftAction:);
//    SEL s2 = @selector(supply_leftAction:);
//    SEL s3 = @selector(supply_leftAction:);
//    SEL s4 = @selector(supply_leftAction:);
    
    
    [supply_left addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:s1]];
    [supply_right_top addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:s1]];
    [supply_right_bottom addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:s1]];
    [supply_bottom addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:s1]];
    

}

- (void) supply_leftAction:(UIGestureRecognizer *)sender {

    if (self.tapAction) {
        S_SectionImageModel *view = sender.view;
        self.tapAction(view.model);
    }
}



- (void)setArr:(NSMutableArray *)arr {
    _arr = arr;
    positionData = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        SupplyAdvertModle *model = [SupplyAdvertModle mj_objectWithKeyValues:arr[idx]];
        NSString *position_name = model.position;
        [positionData addObject:@{position_name:model}];
    }];
    
    for (NSInteger index = 0; index < _adverArr.count; index ++) {
        NSString *ss = [NSString stringWithFormat:@"%@",_adverArr[index][@"position"]];
        S_SectionImageModel *imageView = _adverArr[index][@"imageView"];
        for (NSDictionary *dic in positionData) {
            if ([dic.allKeys.firstObject isEqualToString:ss]) {
                SupplyAdvertModle *model = dic[ss];
                imageView.model = model;
                
            }
        }
        
    }

}
@end


@implementation S_SectionImageModel

- (void)setModel:(SupplyAdvertModle *)model {
    _model = model;
    [self sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
}

@end
