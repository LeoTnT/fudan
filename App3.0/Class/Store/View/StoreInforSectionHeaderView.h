//
//  StoreInforSectionHeaderView.h
//  App3.0
//
//  Created by apple on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "S_StoreInformation.h"
typedef enum : NSUInteger {
    StoreInforHeaderTypeOne,// 进店
    StoreInforHeaderTypeTwo,//新品
    StoreInforHeaderTypeThree,//精品
} StoreInforHeaderType;
@interface StoreInforSectionHeaderView : BaseView

@property (nonatomic ,assign)StoreInforHeaderType type;

@property (nonatomic ,strong)S_SysProductModel *model;

@end
