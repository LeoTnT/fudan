//
//  S_StoreCell.h
//  App3.0
//
//  Created by apple on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class S_StoreModel;
#import "Store_SupplyModel.h"
@interface S_StoreCell : UITableViewCell

@property (nonatomic ,strong)S_StoreModel *model;

@property (nonatomic ,strong)UIButton *collectB,*talkB;

+(instancetype)cellWithTableView:(UITableView *)tableView;
@end



@interface S_collectionCell :XSBaseCollectionCell

@property (nonatomic ,strong)Store_products *model;

@end


@interface S_StoreModel :NSObject

@property (nonatomic ,copy)NSString *user_ID;
@property (nonatomic ,copy)NSString *username;
@property (nonatomic ,copy)NSString *approve_supply;
@property (nonatomic ,copy)NSString *domain;
@property (nonatomic ,copy)NSString *logo;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *province;
@property (nonatomic ,copy)NSString *city;
@property (nonatomic ,copy)NSString *county;
@property (nonatomic ,copy)NSString *town;
@property (nonatomic ,copy)NSString *address;
@property (nonatomic ,assign)double longitude;
@property (nonatomic ,assign)double latitude;
@property (nonatomic ,copy)NSString *eva;

@property (nonatomic ,assign)BOOL isFav;

/**
 到定位地点
 */
@property (nonatomic ,copy)NSString *distance;

@end
/**
 "id": 1,
 "username": "888888",
 "approve_supply": 1,
 "domain": "888888",
 "logo": "/upload/product/777777/20170103/test.png",
 "name": "默认商家",
 "province": "福建",
 "city": "临沂市",
 "county": "涵江",
 "town": "三江口镇",
 "address": "型那不就恐怖",
 "longitude": "117.206633000000",
 "latitude": "39.089340000000",
 "eva": 10
 */
