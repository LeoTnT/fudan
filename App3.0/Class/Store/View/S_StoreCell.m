//
//  S_StoreCell.m
//  App3.0
//
//  Created by apple on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreCell.h"

@implementation S_StoreCell{
    
    UIImageView *showImage;
    UILabel *titleL,*addressL;
    
}


+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *ID = @"S_StoreCell";
    S_StoreCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        
        cell = [[S_StoreCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    
        [self creatContentView];
    }
    return self;
}





-(void)setModel:(S_StoreModel *)model {
    if (_model == model) return;
    

    [showImage getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    
    titleL.text = model.name;
    addressL.text = [NSString stringWithFormat:@"%@km",model.distance];
    model.isFav ? [self.collectB setTitle:@"取消收藏" forState:UIControlStateNormal]:[self.collectB setTitle:@"收藏店铺" forState:UIControlStateNormal];
    
}

- (void)creatContentView {
    
    CGFloat topMargin = 10;
    
    showImage = [UIImageView new];
    
    [self addSubview:showImage];
    
    titleL = [self creatL];
    addressL = [self creatL];
    titleL.font = [UIFont systemFontOfSize:14];
    addressL.font = [UIFont systemFontOfSize:14];
    
    self.collectB = [self creatBtn:UIButtonTypeSystem];
    [self.collectB setTitle:@"[收藏]" forState:UIControlStateNormal];
    
    self.talkB = [self creatBtn:UIButtonTypeCustom];
    self.collectB.titleLabel.font = [UIFont systemFontOfSize:14];
    self.talkB.backgroundColor = [UIColor colorWithRed:126/255.0f green:187/255.0f blue:245/255.0f alpha:1];
    self.talkB.titleLabel.textColor = [UIColor whiteColor];
    [self.talkB setTitle:@"会话" forState:UIControlStateNormal];
    self.talkB.titleLabel.font = [UIFont systemFontOfSize:14];
    self.talkB.layer.cornerRadius = 5;
    self.talkB.layer.masksToBounds = YES;
    CGFloat labelHight = 2 *topMargin;
    [showImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(topMargin);
        make.top.mas_equalTo(self).offset(topMargin);
        make.width.mas_equalTo(7*topMargin);
        make.height.mas_equalTo(6*topMargin);
    }];

    
    
    [addressL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(showImage.mas_right).offset(topMargin);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-topMargin);
        make.height.mas_equalTo(labelHight);
    }];
    
    
    [self.talkB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-labelHight/2);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-topMargin);
        make.size.mas_equalTo(CGSizeMake(80, 3*topMargin));
        
    }];
    
    [self.collectB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.talkB.mas_centerX);
        make.top.mas_equalTo(self.mas_top).offset(topMargin);
        make.height.mas_equalTo(labelHight);
    }];
    
    
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(showImage.mas_top);
        make.left.mas_equalTo(showImage.mas_right).offset(topMargin);
        make.height.mas_equalTo(labelHight);
        make.right.mas_equalTo(self.collectB.mas_left);
    }];
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (UILabel *)creatL {
    
    UILabel *title = [UILabel new];
    [title sizeToFit];
    [self addSubview:title];
    return title;
}

-(UIButton *)creatBtn:(UIButtonType)buttonTYpe {
    
    UIButton *btn = [UIButton buttonWithType:buttonTYpe];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:btn];
    
    return btn;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation S_collectionCell
{
    UILabel *sell_price;
}

- (void)setContentView {
    [super setContentView];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.headerImageView.frame = self.contentView.bounds;
    UIView *bottomView = [BaseUITool viewWithColor:[UIColor hexFloatColor:@"ffffff"]];
    [self.contentView addSubview:bottomView];
    bottomView.alpha = .5;
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(20);
    }];
    
    UIFont *font11 = [UIFont systemFontOfSize:11];
    sell_price = [BaseUITool labelWithTitle:@" " textAlignment:(NSTextAlignmentRight) font:font11 titleColor:[UIColor blackColor]];
    [bottomView addSubview:sell_price];
    [sell_price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bottomView.mas_right).offset(-2);
        make.centerY.mas_equalTo(bottomView);
    }];
}

- (void)setModel:(Store_products *)model {
    _model = model;
    self.hidden = !model ? YES :NO;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.image_thumb] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    sell_price.text = [NSString stringWithFormat:@"¥%@",model.sell_price];
}

@end




@implementation S_StoreModel


+ (NSDictionary *)replacedKeyFromPropertyName

{
    
    return @{@"user_ID" : @"id"};
    
}

@end
