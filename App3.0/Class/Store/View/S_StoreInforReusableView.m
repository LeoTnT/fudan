//
//  S_StoreInforReusableView.m
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreInforReusableView.h"
#import <CoreLocation/CoreLocation.h>
#import "S_CashierController.h"

#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
@implementation S_StoreInforReusableView
{
    NSString *telephone;
    SDCycleScrollView *cycleScrollView;
    UIImageView *topImageView;
    UIImageView *stroeHeaderView;
    UILabel *storeName;
    UILabel *collectInfor;
}


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setContentView];
    }
    
    return self;
}


- (void) setContentView {
    
    topImageView = [BaseUITool imageWithName:nil superView:self];
    
    [topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(125);
    }];
    
    UIView *shadeView = [BaseUITool viewWithColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    
    [self addSubview:shadeView];
    [shadeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(125);
    }];
    stroeHeaderView = [BaseUITool imageWithName:nil superView:shadeView];
    stroeHeaderView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    stroeHeaderView.layer.cornerRadius =4;
    stroeHeaderView.layer.masksToBounds = YES;
    [stroeHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.bottom.mas_equalTo(-11.5);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    
    storeName = [BaseUITool labelWithTitle:@" " textAlignment:0 font:[UIFont systemFontOfSize:16] titleColor:[UIColor whiteColor]];
    [shadeView addSubview:storeName];
    [storeName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(stroeHeaderView.mas_right).offset(7.5);
        make.top.mas_equalTo(stroeHeaderView);
        make.right.mas_equalTo(shadeView).offset(-80);
    }];
    
    
    collectInfor = [BaseUITool labelWithTitle:@" " textAlignment:0 font:[UIFont systemFontOfSize:11] titleColor:[UIColor whiteColor]];
    [self addSubview:collectInfor];
    [collectInfor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(storeName);
        make.bottom.mas_equalTo(stroeHeaderView.mas_bottom);
        
    }];
    
    
    _collecButton = [BaseUITool buttonWithImage:@"store_Nocollect" selected:@"store_collect" superView:self];
    [_collecButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(stroeHeaderView);
        make.right.mas_equalTo(-12.5);
    }];
    
    UIView *vvvv1 = [BaseUITool viewWithColor:[UIColor whiteColor]];
    [self addSubview:vvvv1];
    
    [vvvv1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(topImageView.mas_bottom);
        make.height.mas_equalTo(64);
    }];
    
    NSArray *im = @[@"store_fl",@"store_fx",@"store_newS",@"store_jx"];
    NSArray *ti = @[@"所有分类",@"全部宝贝",@"新品上市",@"精选商品",];
    NSMutableArray *iiii = [NSMutableArray array];
    [ti enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:im[idx]] forState:UIControlStateNormal];
        [btn setTitle:ti[idx] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor hexFloatColor:@"666666"] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn setImagePosition:(LXMImagePositionTop) spacing:7.5];
        [vvvv1 addSubview:btn];
        btn.tag = idx+800;
        @weakify(self);
        [btn setBlockForControlEvents:UIControlEventTouchUpInside block:^(UIButton *sender) {
            @strongify(self);
            if (self.ButtonSelected) {
                self.ButtonSelected(sender.tag);
            }
            
        }];
        [iiii addObject:btn];
    }];
    
    [iiii mas_distributeViewsAlongAxis:MASAxisTypeHorizontal
                      withFixedSpacing:20
                           leadSpacing:6
                           tailSpacing:7];
    [iiii mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(64);
    }];
    
    [self layoutIfNeeded];
    cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, CGRectGetMaxY(vvvv1.frame), mainWidth, 136) delegate:nil placeholderImage:[UIImage imageNamed:@"no_pic"]];
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    cycleScrollView.hidesForSinglePage = YES;
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;

    cycleScrollView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
    cycleScrollView.pageDotColor = [UIColor whiteColor];
    cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [self addSubview:cycleScrollView];


    
    
    
}


- (void)setIs_fav:(BOOL)is_fav {
    _is_fav = is_fav;
    _collecButton.selected = is_fav;
}


-(void)collectionHeadView:(S_SupplyInfoModel *)model {
    
    telephone = model.tel;

    storeName.text = model.name;
    
    collectInfor.text = [NSString stringWithFormat:@"收藏人数: %@ 人",model.fav_num?model.fav_num:@"0"];
    [topImageView sd_setImageWithURL:[NSURL URLWithString:model.background] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    [stroeHeaderView sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    NSArray *imaArr = [model.banner_wap componentsSeparatedByString:@","];
    NSMutableArray *allImage = [NSMutableArray array];
    for (int x = 0; x < imaArr.count; x ++) {
        
        NSString *str = imaArr[x];
        [allImage addObject:str];
    }
    if (allImage.count ==0) {
        
        return;
    }
    
    cycleScrollView.imageURLStringsGroup = allImage;


}



- (void)payForAction{
    
    if (_cashier) {
        _cashier();
        
    }
}



- (void)dealloc{
    
    NSLog(@"%s",__FUNCTION__);
}

- (void)touchAction:(UIButton *)sender {
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",telephone]]];
    
}

@end


