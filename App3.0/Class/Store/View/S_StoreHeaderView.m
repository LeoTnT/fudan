//
//  S_StoreHeaderView.m
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreHeaderView.h"

@implementation S_StoreHeaderView
{
    UILabel *title;
    UILabel *sell_num;
    UILabel *product_num;
    UIImageView *imageView;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setSubViews];
        
    }
    return self;
}

- (void) setSubViews {
    UIFont *titleFont = [UIFont systemFontOfSize:16];
    UIFont *titleFont1 = [UIFont systemFontOfSize:11];
    UIColor *btnColor = mainColor;
    imageView = [BaseUITool imageWithName:nil superView:self];
    [imageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"no_pic"]];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(11.5);
        make.top.mas_equalTo(10.5);
        make.size.mas_equalTo(CGSizeMake(39.5, 39));
    }];
    
    
    title = [BaseUITool labelWithTitle:@" " textAlignment:0 font:titleFont titleColor:[UIColor blackColor]];
    [self addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(7.5);
        make.top.mas_equalTo(16.5);
        make.right.mas_equalTo(-70);
        make.height.mas_equalTo(15.5);
    }];
    
    sell_num = [BaseUITool labelWithTitle:@" " textAlignment:0 font:titleFont1 titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:sell_num];
    [sell_num mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(title.mas_bottom).offset(7);
        make.bottom.mas_equalTo(imageView);
        make.left.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_centerX);
    }];

    product_num = [BaseUITool labelWithTitle:@" " textAlignment:0 font:titleFont1 titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:product_num];
    [product_num mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(sell_num);
        make.left.mas_equalTo(sell_num.mas_right);
        make.right.mas_equalTo(title.mas_right);
    }];
    
    _goShop = [BaseUITool buttonWithTitle:@"进店" titleColor:btnColor font:[UIFont systemFontOfSize:14] superView:self];
    [_goShop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(imageView);
        make.size.mas_equalTo(CGSizeMake(55, 31));
    }];
    

    _goShop.layer.cornerRadius = 5;
    _goShop.layer.borderWidth = .5;
    _goShop.layer.borderColor = btnColor.CGColor;
    _goShop.layer.masksToBounds = YES;
    
}

- (void)setModel:(Store_SupplyModel *)model {
    _model = model;
    title.text = model.name;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    sell_num.text = [NSString stringWithFormat:@"销量 %d",model.sell_num];
    product_num.text = [NSString stringWithFormat:@"共%d件宝贝",model.product_num];
   
}

@end
