//
//  S_StoreHeaderView.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store_SupplyModel.h"
@interface S_StoreHeaderView : UICollectionReusableView

@property (nonatomic ,strong)UIButton *goShop;

@property (nonatomic ,strong)Store_SupplyModel *model;

@end

