//
//  S_ChoueseCity.h
//  App3.0
//
//  Created by apple on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_ChoueseCity : XSBaseViewController

@property (nonatomic ,copy)NSString *showAddress;
@property (nonatomic ,copy)void (^SelectedCountry)(NSDictionary *,NSString *);
@property (nonatomic ,copy)void (^SelectedCity)(NSDictionary *,NSString *);
@end

@interface City : NSObject
@property (nonatomic, copy) NSString *cityName; // 城市名
@property (nonatomic, copy) NSString *cityLetter; // 城市名拼音
@property (nonatomic ,copy) NSString *cityID;
@end
