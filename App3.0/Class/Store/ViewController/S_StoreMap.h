//
//  S_StoreMap.h
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface S_StoreMap : XSBaseViewController

@property (nonatomic ,copy)void (^SendeAddress)(CLLocationCoordinate2D ,NSString *);

@end
@interface MyAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic,   copy) NSString *title;
@property (nonatomic,   copy) NSString *subtitle;

@end

