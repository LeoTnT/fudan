//
//  S_StoreInformation.m
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreInformation.h"
#import "S_StoreInForCell.h"
#import "S_StoreInforReusableView.h"
#import "XSShareView.h"
#import "ShowWindow.h"
#import "ChatViewController.h"
#import "ProductListViewController.h"
#import "S_storeSortController.h"

#import "S_CashierController.h"
#import "GoodsDetailViewController.h"
#import "StoreInforSectionHeaderView.h"
#import "SearchVC.h"
@interface S_StoreInformation ()





@end

@implementation S_StoreInformation
{
    S_SupplyInfoModel *suppyModel;
    NSMutableArray *adverDataSource;
    BOOL isShowAdver;
    NSInteger startIndex;
    BOOL is_fav;
}




- (void)setStoreInfor:(NSString *)storeInfor {

    _storeInfor = storeInfor;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self requestData:@{@"uid":[NSString stringWithFormat:@"%@",storeInfor]}];

    
}

- (void) requestData:(NSDictionary *)dic {
    
    
    
    RACSignal *signal = [XSHTTPManager rac_POSTURL:Url_supply_GetSupplyIndex params:dic];
    
    [[signal doNext:^(id  _Nullable x) {
        
    }]subscribeNext:^(resultObject *state) {
       [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            S_InforModel *model = [S_InforModel mj_objectWithKeyValues:state.data];
            suppyModel = model.supplyInfo;
            is_fav = model.is_fav;
            isShowAdver = model.supplyAdvert.count == 0 ? NO :YES;
            startIndex = isShowAdver ? 2 : 1;
            adverDataSource =  model.supplyAdvert;
            if (model.supplyProduct.count == 0) {
                [XSTool showToastWithView:self.view Text:@"商家没有更多信息"];
            }
            [model.supplyProduct.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                S_SysProductModel *sysModel = [S_SysProductModel mj_objectWithKeyValues:x];
                [arr addObject:sysModel];
            }completed:^{
                
                 self.collectionModel.collectionDataSource =  arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.collectionView reloadData];
                    
                    [self endRefresh];
                    
                });
            }];


        }else{
            startIndex = 1;
            NSLog(@" - - - - - -%@",[NSThread currentThread]);
            [MBProgressHUD showMessage:state.info view:self.view];
            [self.collectionView reloadData];
            [self endRefresh];
        }
        
    }];

    [signal subscribeError:^(NSError * _Nullable error) {
    
        [self endRefresh];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    startIndex = 1;
    S_TopSrearchView *topView = [[S_TopSrearchView alloc] initWithFrame:CGRectMake(0, 0, mainWidth - 100, 30)];

    self.navigationItem.titleView = topView;
    @weakify(self);
    [topView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
        [self.navigationController pushViewController:search animated:YES];
        
    }]];
    topView.searchLabel.text = isEmptyString(self.searchText) ? @"搜索宝贝":self.searchText;
    //购物车按钮
    UIButton *shopCartBtn=[[UIButton alloc] initWithFrame:CGRectMake(0 ,0, 30, 30)];
    [shopCartBtn setBackgroundImage:[UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeMake(30, 30)] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shopCartBtn];
    
    
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.collectionView removeFromSuperview];
        self.collectionView = nil;
        [self.navigationController popViewControllerAnimated:YES];
        
    }];

    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-49-kTabbarSafeBottomMargin);
        
    }];
    [self.collectionView registerClass:[S_StoreInforReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"S_StoreInforReusableView"];
    
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"two"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    [self.collectionView registerClass:[S_StoreInForCell class] forCellWithReuseIdentifier:@"Store_InforMation"];
    [self.collectionView registerClass:[S_SectionOneCell class] forCellWithReuseIdentifier:@"S_SectionOneCell"];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    self.collectionView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
    self.flowLayout.minimumInteritemSpacing = 2;
//    self.flowLayout.minimumLineSpacing = 2;
    self.isShowBackView = NO;
    [self.nextStop setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self);
        self.tabBarController.selectedIndex = 2;
    }];
    UIView *bottomView = [BaseUITool viewWithColor:[UIColor whiteColor]];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(49);
        make.bottom.mas_equalTo(-kTabbarSafeBottomMargin);
    }];
    
    
    UIButton *allFenlei = [BaseUITool buttonTitle:@"全部分类" image:nil superView:self.view];
    allFenlei.titleLabel.font = [UIFont systemFontOfSize:14];
    [allFenlei setTitleColor:COLOR_666666 forState:UIControlStateNormal];
    [allFenlei mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bottomView);
        make.left.mas_equalTo(bottomView);
        make.size.mas_equalTo(CGSizeMake(mainWidth/2-0.5, 49));
    }];
    
    [allFenlei setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self);
        S_storeSortController *sort = [S_storeSortController new];
        sort.storeID = self.storeInfor;
        [self.navigationController pushViewController:sort animated:YES];
    }];
    
    
    
    UIView *linView = [BaseUITool viewWithColor:[UIColor hexFloatColor:@"dddddd"]];
    [bottomView addSubview:linView];
    [linView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bottomView);
        make.size.mas_equalTo(CGSizeMake(.5, 23));
    }];
    
    UIButton *callSeller = [BaseUITool buttonTitle:@"联系卖家" image:nil superView:self.view];
    callSeller.titleLabel.font = [UIFont systemFontOfSize:14];
    [callSeller setTitleColor:COLOR_666666 forState:UIControlStateNormal];
    [callSeller mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bottomView);
        make.right.mas_equalTo(bottomView);
        make.size.mas_equalTo(allFenlei);
    }];
    
    [callSeller setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self);

        if ([suppyModel.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
            [XSTool showToastWithView:self.view Text:@"无法与自己聊天"];
            return;
        }
#ifdef ALIYM_AVALABLE
        YWPerson *person = [[YWPerson alloc] initWithPersonId:self.detailInfo.supplyInfo.user_id];
        YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
        YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
        chatVC.title = self.detailInfo.supplyInfo.showname;
        chatVC.avatarUrl = self.detailInfo.supplyInfo.logo;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:suppyModel.user_id type:EMConversationTypeChat createIfNotExist:YES];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
        chatVC.title = suppyModel.name;
        chatVC.avatarUrl = suppyModel.logo;
        [self.navigationController pushViewController:chatVC animated:YES];
#else
        XMChatController *chatVC = [XMChatController new];
        SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:suppyModel.user_id title:suppyModel.name avatarURLPath:suppyModel.logo];
        chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
        [self.navigationController pushViewController:chatVC animated:YES];
#endif
    }];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (section == 0) {
        return 1;
    }else{
        if (isShowAdver && section == 1) {
            return 1;
        }else{
            S_SysProductModel *model =  self.collectionModel.collectionDataSource[section-startIndex];
            return model.product_arr.count;
        }
    }

}




- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UICollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        return cell;
    }else{
        
        if (indexPath.section == 1 && isShowAdver) {
            S_SectionOneCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"S_SectionOneCell" forIndexPath:indexPath];
            cell.arr = adverDataSource;
            
            [cell setTapAction:^(SupplyAdvertModle *model) {
                NSLog(@" - - - - -  -%@",model.mj_keyValues);
            }];
            
            return cell;
        }else{
         
            S_StoreInForCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Store_InforMation" forIndexPath:indexPath];
            S_SysProductModel *model = self.collectionModel.collectionDataSource[indexPath.section-startIndex];
            cell.model = model.product_arr[indexPath.row];
            cell.layer.masksToBounds = YES;
            return cell;
        }

    }
    
    
    return nil;
}


- (UICollectionReusableView *) collectionView:(UICollectionView *)collectionView  sectionOne:(NSIndexPath *)indexPath {
    
    S_StoreInforReusableView * reuseView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"S_StoreInforReusableView" forIndexPath:indexPath];
    
    [reuseView collectionHeadView:suppyModel];
    reuseView.is_fav = is_fav;
    
    @weakify(self);
    [reuseView setCashier:^{
        if (suppyModel.is_enable_quickpay) {
            
            @strongify(self);
            S_CashierController *cashier =[S_CashierController new];
            cashier.showTitle = suppyModel.name;
            cashier.quick_percent_pay = suppyModel.quick_pay;
            cashier.supply_name = suppyModel.username;
            [self xs_pushViewController:cashier];
            
        }else{
            
            [XSTool showToastWithView:self.view Text:@"商家暂未开通此功能"];
            
        }
    }];
    
    [reuseView.collecButton setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        @strongify(self);
        
        [self collectionButtonAction:suppyModel];
    }];
    
    [reuseView setButtonSelected:^(NSInteger tag) {
        @strongify(self);
        switch (tag) {
            case 800:{
                S_storeSortController *sort = [S_storeSortController new];
                sort.storeID = self.storeInfor;
                [self.navigationController pushViewController:sort animated:YES];
            }
                
                break;
            case 801:{
                ProductListViewController * prod = [ProductListViewController new];
                ProductModel *model = [ProductModel new];
                model.uid = suppyModel.user_id;
                prod.model = model;
                [self.navigationController pushViewController:prod animated:YES];
            }
                
                break;
            case 802:{
                ProductListViewController *prod = [ProductListViewController new];
                ProductModel *model = [ProductModel new];
                model.uid = suppyModel.user_id;
                model.recommend=@"1";
                prod.model = model;
                [self.navigationController pushViewController:prod animated:YES];
            }
                
                break;
            case 803:{
                ProductListViewController *prod = [ProductListViewController new];
                ProductModel *model = [ProductModel new];
                model.uid = suppyModel.user_id;
                model.recommend=@"4";
                prod.model = model;
                [self.navigationController pushViewController:prod animated:YES];
            }
                break;
                
        }
    }];
    
    return reuseView;
}


- (void)collectionButtonAction:(S_SupplyInfoModel *)model {
    
    if (isEmptyString([UserInstance ShardInstnce].guid)) {
        [MBProgressHUD showMessage:@"用户未登录" view:self.view];
        return;
    }

    if (isEmptyString(model.user_id)) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (is_fav == YES) {
        [HTTPManager deleteShopCarInfoWithFavId:model.user_id success:^(NSDictionary *dic, resultObject *state) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:state.info view:self.view];
            if (state.status) {
                is_fav = NO;
                
            }
            [self.collectionView reloadData];
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        }];
    }else{
        [HTTPManager setFavorite:model.user_id Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:state.info view:self.view];
            
            if (state.status) {
                is_fav = YES;
                //                [self setStoreInfor:suppyModel.user_id];
            }
            [self.collectionView reloadData];
        } failure:^(NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
        
    }
    
    
    
}


// 头视图
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
       return  [self collectionView:collectionView sectionOne:indexPath];
    }else{
        
        if (kind == UICollectionElementKindSectionFooter) {
            
            UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
            footerView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
            return  footerView;
        } else if (kind == UICollectionElementKindSectionHeader){
            
            UICollectionReusableView *headerViwe = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"two" forIndexPath:indexPath];
            for (StoreInforSectionHeaderView *view  in headerViwe.subviews) {
                [view removeFromSuperview];
            }
            StoreInforSectionHeaderView *sectionOneHeader = [StoreInforSectionHeaderView new];
            if (indexPath.section == 1 && isShowAdver) {
                sectionOneHeader.type = StoreInforHeaderTypeOne;
            }else{
                S_SysProductModel *model = self.collectionModel.collectionDataSource[indexPath.section-startIndex];
                sectionOneHeader.model = model;
            }
            
            sectionOneHeader.frame = headerViwe.bounds;
            [headerViwe addSubview:sectionOneHeader];
            
            return headerViwe;
        }else{
            return nil;
        }
        
        
    }

    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {

    return  section != 0 ? CGSizeMake(mainWidth, 10):CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return  CGSizeMake(mainWidth, 187+136);
        
    }else {
        
        return CGSizeMake(mainWidth, 44);
    }
}
#pragma mark -- UICollectionViewDelegate
//设置每个 UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0 ) {
        
        return CGSizeMake(mainWidth, 10.5);
    }else{
       CGFloat itemWidth = (mainWidth - 2)/2;

        if (indexPath.section == 1 && isShowAdver) {
            return CGSizeMake(mainWidth, 224.5+112);
        }else{
         
            return CGSizeMake(itemWidth, itemWidth+75);
        }
        
    }

}
//定义每个UICollectionView 的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    return UIEdgeInsetsZero;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if ((indexPath.section == 0 && !isShowAdver) || (isShowAdver && indexPath.section > 1)) {
     
        [self selectedRow:indexPath];
    }
    

    
}

- (void) selectedRow:(NSIndexPath *)indexPath {
    S_SysProductModel *model = self.collectionModel.collectionDataSource[indexPath.section-startIndex];
    Product_arrModel *supModel = [Product_arrModel mj_objectWithKeyValues:model.product_arr[indexPath.row]];
    GoodsDetailViewController *deatil = [GoodsDetailViewController new];
    deatil.goodsID =supModel.product_id;
    [self.navigationController pushViewController:deatil animated:YES];
}


-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    
    return startIndex +self.collectionModel.collectionDataSource.count;
    
}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

}


-(void)dealloc {
    
    NSLog(@"销毁了");
}


@end


@implementation S_SupplyInfoModel
@end

@implementation S_SysProductModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"ID":@"id"};
}
+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{
             @"Product_arrModel" : @"product_arr"
             };
}
@end

@implementation S_InforModel
+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{
             @"S_SysProductModel" : @"supplyProduct",
             @"SupplyAdvertModle" : @"supplyAdvert"
             };
}
@end

@implementation Product_arrModel


@end

@implementation SupplyAdvertModle

@end
