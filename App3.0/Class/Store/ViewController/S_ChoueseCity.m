//
//  S_ChoueseCity.m
//  App3.0
//
//  Created by apple on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_ChoueseCity.h"



@interface S_ChoueseCity ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout
>
@property (nonatomic ,strong)UICollectionView  *myCollectionView;

@property (nonatomic ,strong)NSMutableArray *cityNames ;

@property (nonatomic ,strong)NSArray *theLastArr;

@property (nonatomic ,strong)NSMutableArray *theSecendCell ;

@property (nonatomic ,strong)NSMutableArray *AreaList;

@property (nonatomic ,strong)UILabel *locationAddress;

@property (nonatomic ,strong)NSMutableArray *cityArr;

@end

@implementation S_ChoueseCity{
    
    NSString *lastHeadTitle;
    NSDictionary *cityList;
    
}

static NSString *chouseIdentifier = @"chouseCellIdentifier";
static NSString *chouseHeadIdentifier = @"HeadIdentifier";

- (NSMutableArray *)theSecendCell {
    if (!_theSecendCell) {
        _theSecendCell = [NSMutableArray array];

        
    }
    return _theSecendCell;
}

- (NSMutableArray *)cityArr {
    if (!_cityArr) {
        _cityArr = [NSMutableArray array];
 
    }
    return _cityArr;
}


- (UILabel *)locationAddress {
    if (!_locationAddress) {
        UILabel *labelTitle = [UILabel new];
        labelTitle.text = @"   定位城市:";
        labelTitle.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:labelTitle];
        
        _locationAddress = [UILabel new];
        _locationAddress.backgroundColor = [UIColor whiteColor];
        _locationAddress.textColor = [UIColor redColor];
        _locationAddress.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:_locationAddress];
        [labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left);
            make.top.mas_equalTo(self.view);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(100);
        }];
        [_locationAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.top.mas_equalTo(labelTitle);
            make.left.mas_equalTo(labelTitle.mas_right);
            make.right.mas_equalTo(self.view.mas_right);
        }];
    }
    return _locationAddress;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    
    self.locationAddress.text = self.showAddress;

    [self hotArea];
}

- (void)setShowAddress:(NSString *)showAddress {
    if (_showAddress == showAddress)         return;
    _showAddress = showAddress;
    self.locationAddress.text = self.showAddress;

}

- (void)hotArea {
    
    NSArray *arr1= @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"J",@"K",@"L",@"M",@"N",@"P",@"Q",@"R",@"S",@"T",@"W",@"X",@"Y",@"Z"];
    
    for (int x = 0; x < arr1.count; x ++) {
        NSDictionary *dic = @{@"title":arr1[x]};
        [self.theSecendCell addObject:dic];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getHotAreaSuccrss:^(NSDictionary * _Nullable dic, resultObject *state) {
    
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSArray *arr = dic[@"data"];
        
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
           
            [self.cityArr addObject:arr[idx]];
        }];
        

        [self.myCollectionView reloadData];
        
    } failure:^(NSError * _Nonnull error) {
       
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cityNames = [NSMutableArray array];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"选择城市";
    self.myCollectionView.backgroundColor = [UIColor whiteColor];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];    
  
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [self getPredicateArr];
        });
    
}


- (UICollectionView *)myCollectionView {
    if (!_myCollectionView) {
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _myCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 40, mainWidth, mainHeight-40-60) collectionViewLayout:flowLayout];
        
        [_myCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:chouseHeadIdentifier];
        [_myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:chouseIdentifier];
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout *)_myCollectionView.collectionViewLayout;
        collectionViewLayout.headerReferenceSize = CGSizeMake(mainWidth, 40);
        _myCollectionView.showsVerticalScrollIndicator = NO;
        _myCollectionView.delegate = self;
        _myCollectionView.dataSource = self;
        
        [self.view addSubview:_myCollectionView];
    }
    return _myCollectionView;
}





- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger number = 0;
    switch (section) {
            
        case 0:return number = self.cityArr.count;break;
        case 1:return number = self.theSecendCell.count;break;
        case 2:if (self.theLastArr) {return number = self.theLastArr.count;};break;
            
        default:break;
    }
    return number;
}



- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:chouseIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];

    if (indexPath.section == 0) {
        [self getTheFirstCellLabel:self.cityArr[indexPath.row][@"name"] superView:cell.contentView];
    }else if (indexPath.section == 1){
        [self getTheFirstCellLabel:self.theSecendCell[indexPath.row][@"title"] superView:cell.contentView];
    }else if(indexPath.section == 2 && [self.theLastArr count]>0){
        
       
        NSString *citySreing = [[self.theLastArr objectAtIndex:indexPath.row] cityName];
    
       UILabel *label =  [self getTheFirstCellLabel:citySreing superView:cell.contentView];
    
        [label sizeToFit];
    }
    
    
    return cell;
}



- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
   
    UICollectionReusableView *reuseView=[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:chouseHeadIdentifier forIndexPath:indexPath];
  
    switch (indexPath.section) {
        case 0:[self creatHeadLabel:@"热门城市" superView:reuseView];break;
        case 1:[self creatHeadLabel:@"全部城市" superView:reuseView];break;
        case 2:[self creatHeadLabel:lastHeadTitle superView:reuseView];break;
            default:break;
    }
    
    reuseView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    return reuseView;

}





#pragma mark -- UICollectionViewDelegate
//设置每个 UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    
    if (indexPath.section == 0) {
        return CGSizeMake((mainWidth - 15)/3, 30);
    }else if (indexPath.section == 1){
        return CGSizeMake((mainWidth - 15)/6, 30);
    }else if (indexPath.section == 2){
        return CGSizeMake((mainWidth - 15)/3, 30);
    }

    return CGSizeMake((mainWidth - 15)/3, 30);
}
//定义每个UICollectionView 的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5,0);
}

//定义每个UICollectionView 的纵向间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}


-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    if (self.theLastArr) {
        return 3;
    }
    return 2;
}

- (UILabel *)getTheFirstCellLabel:(NSString *)cityOfItem superView:(UIView *)superView{
    for (UILabel *label in superView.subviews) {
        [label removeFromSuperview];
    }
    UILabel *labelCity = [UILabel new];
    labelCity.textAlignment = NSTextAlignmentCenter;
    labelCity.layer.borderWidth = .5;
    labelCity.alpha = .6;
    labelCity.layer.cornerRadius = 2;
    labelCity.layer.masksToBounds = YES;
    labelCity.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [superView addSubview:labelCity];

    
    [labelCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 0, 5));}];
    labelCity.text = cityOfItem;
    return labelCity;
}



- (void)getPredicateArr {

    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"cityList" ofType:@"plist"];
    cityList = [XSTool returnDictionaryWithDataPath:plistPath];
    NSString *pinyinplistPath = [[NSBundle mainBundle] pathForResource:@"cityPredicate" ofType:@"plist"];
    NSDictionary *pinyinDic = [XSTool returnDictionaryWithDataPath:pinyinplistPath];
    
    
    NSArray *arr = pinyinDic.allKeys;
    for (int x = 0; x <arr.count; x ++) {
        City *city = [City new];
        city.cityLetter = pinyinDic[arr[x]][@"pinyin"];
        city.cityName = pinyinDic[arr[x]][@"name"];
        [self.cityNames addObject:city];
//        city = nil;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        XSBenchmark(^{
            NSString *characterString = self.theSecendCell[indexPath.row][@"title"];
            lastHeadTitle = characterString;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityLetter BEGINSWITH [cd] %@",characterString];
            self.theLastArr =  [self.cityNames filteredArrayUsingPredicate:predicate];
        }, ^(double ms) {
            
                [self.myCollectionView reloadData];
            
        });
        

      
    }else if (indexPath.section == 2){
        City *cyti = (City *) self.theLastArr[indexPath.row];
        NSString *cityName = [NSString stringWithFormat:@"name_%@",cyti.cityName];

        if (_SelectedCountry){
            _SelectedCountry(cityList[cityName],cyti.cityName);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }else if (indexPath.section == 0){
        
        
        if (_SelectedCity) {
            _SelectedCity(self.cityArr[indexPath.row],self.cityArr[indexPath.row][@"name"]);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self.view removeFromSuperview];
    
}

-(UILabel *)creatHeadLabel:(NSString *)title superView:(UIView *)superView{
    
    for (UILabel *label in superView.subviews) {
        [label removeFromSuperview];
    }
    UILabel *headLable = [UILabel new];
    [superView addSubview:headLable];
    headLable.text = title;
    [headLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 15, 0, 5));}];
    return headLable;
}
@end

@implementation City

@end
