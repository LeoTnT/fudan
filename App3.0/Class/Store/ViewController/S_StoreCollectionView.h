//
//  S_StoreCollectionView.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseCollectionView.h"
#import "S_StoreViewController.h"
@interface S_StoreCollectionView : XSBaseCollectionView

@property (nonatomic ,strong)SupplyModel *requestModel;
@property (nonatomic ,copy)NSString *searchText;
@property (nonatomic ,assign)BOOL isLRequestData;
@end
