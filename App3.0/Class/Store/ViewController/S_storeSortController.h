//
//  S_storeSortController.h
//  App3.0
//
//  Created by apple on 2017/4/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_storeSortController : XSBaseViewController

@property (nonatomic ,copy)NSString *storeID;

@end

@interface S_StoreSortModel : NSObject

@property (nonatomic ,copy)NSString *category_supply_id;
@property (nonatomic ,copy)NSString *category_supply_name;


@end
//"category_supply_id" = 132;
//"category_supply_name" = batch;
//disabled = 0;
//"u_time" = 0;
//"w_time" = 1489484863;
