//
//  S_storeSortController.m
//  App3.0
//
//  Created by apple on 2017/4/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_storeSortController.h"
#import "ProductListViewController.h"
#import "DefaultView.h"
#import "YKMultiLevelTableView.h"
#import "YKNodeModel.h"
@interface S_storeSortController ()

@property (nonatomic ,strong)YKMultiLevelTableView *tableView;
@property (nonatomic ,assign)NSInteger page;
@property (nonatomic ,strong)NSMutableArray *sortArr;
@property (nonatomic ,strong)DefaultView *emptyView;
@end

@implementation S_storeSortController

static NSString *storeSortCell = @"storeSortCell";

- (NSMutableArray *)sortArr {
    if (!_sortArr) {
        _sortArr = [NSMutableArray array];
    }
    return _sortArr;
}


- (YKMultiLevelTableView *)tableView {
    if (!_tableView) {
        @weakify(self);
        _tableView = [[YKMultiLevelTableView alloc] initWithFrame:self.view.bounds needPreservation:NO selectBlock:^(YKNodeModel *model) {
            @strongify(self);
                ProductListViewController * prod = [ProductListViewController new];
                ProductModel *proModel = [ProductModel new];
                proModel.scateId = model.category_supply_id;
                prod.model = proModel;
                [self.navigationController pushViewController:prod animated:YES];
            
        }];
        [self.view addSubview:_tableView];
        _tableView.tableFooterView=[UIView new];
    
        MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            @strongify(self);
            [self getNewData];
        }];
        //设置不同刷新状态的文字
        [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
        [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
        [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
        //设置字体大小和文字颜色
        header.stateLabel.font=[UIFont systemFontOfSize:14];
        header.stateLabel.textColor=[UIColor darkGrayColor];
        header.lastUpdatedTimeLabel.hidden=YES;
        _tableView.mj_header=header;
        MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            
            
        }];
        [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
        [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
        [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
        //设置字体大小和文字颜色
        footer.stateLabel.font=[UIFont systemFontOfSize:14];
        footer.stateLabel.textColor=[UIColor darkGrayColor];
        _tableView.mj_footer=footer;
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"店铺分类";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.view.backgroundColor = [UIColor whiteColor];
    // 默认页
    self.emptyView = [[DefaultView alloc] initWithFrame:self.view.bounds];
    self.emptyView.button.hidden = YES;
    self.emptyView.titleLabel.text = @"暂无更多分类";
    self.emptyView.hidden=YES;
    [self.view addSubview:self.emptyView];
    [self getNewData];
}
-(void)getNewData{
    if (!self.storeID) return;
    self.page=1;
    [XSTool showProgressHUDWithView:self.view];

    RACSignal *signal = [XSHTTPManager rac_POSTURL:Url_supplys_category params:@{@"id":self.storeID}];
    
    /*
     "category_id": 10,
     "category_name": "消费电子",
     "parent_id": 0,
     "logo": "",
     "image": "",
     "_child": [
     {
     "category_id": 20,
     "category_name": "电脑",
     "parent_id": 10,
     "logo": "",
     "image": "",
     "_child": [
     {
     "category_id": 22,
     "category_name": "笔记本电脑",
     "parent_id": 20,
     "logo": "",
     "image": ""
     }
     ]
     },
     */
    
    
    [signal subscribeNext:^(resultObject *state) {
        NSLog(@"state   =%@",state);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.sortArr removeAllObjects];
            NSArray *arr = state.data[@"data"];
            [arr.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                YKNodeModel *model = [YKNodeModel mj_objectWithKeyValues:x];
                model.root = YES;
                model.leaf = 0;
                model.isOpen = NO;
                [self.sortArr addObject:model];
            }completed:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.sortArr.count) {
                        self.tableView.nodes = self.sortArr;
                        self.emptyView.hidden=YES;
                    }else{
                        self.emptyView.hidden=NO;
                    }
                });
            }];

        }else{
            self.emptyView.hidden=NO;
            [XSTool showToastWithView:self.view Text:state.info];
        }

        
    }];
    
    [signal subscribeError:^(NSError * _Nullable error) {
        self.emptyView.hidden=NO;
        [XSTool hideProgressHUDWithView:self.view];
        [_tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常,请稍后重试"];
    }];
    

    
}



@end


@implementation S_StoreSortModel



@end
