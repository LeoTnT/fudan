//
//  S_StoreViewController.h
//  App3.0
//
//  Created by apple on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSBaseViewController.h"

@interface S_StoreViewController : XSBaseViewController

- (instancetype)initWithSearchText:(NSString *)search;


@end



//{"longitude":"118","latitude":"35"}
@interface SupplyModel : NSObject

@property (nonatomic ,copy)NSString *supplyName;
@property (nonatomic ,copy)NSString *province;
@property (nonatomic ,copy)NSString *city;
@property (nonatomic ,copy)NSString *county;
@property (nonatomic ,copy)NSString *town;
@property (nonatomic ,assign)NSInteger page;
@property (nonatomic ,assign)NSInteger limit;
@property (nonatomic ,copy)NSString *industry;

@property (nonatomic ,copy)NSString *_order;

@property (nonatomic ,copy)NSString *name;
//@property (nonatomic ,assign)double longitude;
//@property (nonatomic ,assign)double latitude;

@property (nonatomic ,copy)NSString *user_position;

- (instancetype)initWithPage;
@end

/**
 supplyName	可选	string	店铺名
 province	可选	int	省id
 city	可选	int	市id
 county	可选	int	区ID
 town	可选	int	街道ID
 page	可选	string	页码	1
 limit	可选	string	每页数量	10
 industry 行业
 */


