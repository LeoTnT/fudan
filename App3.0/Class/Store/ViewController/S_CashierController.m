//
//  S_CashierController.m
//  App3.0
//
//  Created by apple on 2017/4/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_CashierController.h"
#import "OrderPayViewController.h"
@interface S_CashierController ()
@property (nonatomic ,strong)UITextField *noTextField;
@end

@implementation S_CashierController
{
    UITextField *consumeText;
    UILabel *label1;
    UILabel *label2;
    UILabel *label3;
    UITextField *label4;
    CGFloat labelHight;
    CGFloat space;
    UIButton *payBtn;
    NSString *payForMoney;
    CGFloat noDiscount ;// 支付数额
    CGFloat _discount;// 折扣
    NSString *mianzhikou;
}



- (void)setShowTitle:(NSString *)showTitle {
    
    self.navigationItem.title = showTitle;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    [self conetentView];
    
}



- (void)conetentView{
    space = 20;
    labelHight = 60;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
   
    consumeText = [self createTextFieldWith:CGRectMake(space, 64+space, mainWidth-40, 50) viewFrame:CGRectMake(0, 0, 80, 50) labelFrame: CGRectMake(5, 0, 80, 50) placeHolder:@"询问服务员后输入" leftViewText:@"消费金额:" textAlignment:NSTextAlignmentRight keyboardType:UIKeyboardTypeDecimalPad];
    
    [consumeText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:consumeText];
    
    
    UIButton *btn =  [self creatButtnWithTitle:nil image:[UIImage imageNamed:@"reg_unselect"] frame:CGRectMake(space, CGRectGetMaxY(consumeText.frame)+25, 15, 15) selected:@selector(btnAction:)];
    


    label1 = [UILabel new];
    label1.text = @"输入不参与优惠金额(如酒水、套餐)";
    label1.textColor = [UIColor grayColor];
    label1.backgroundColor = [UIColor clearColor];
    
    label1.frame = CGRectMake(CGRectGetMaxX(btn.frame)+space/2,CGRectGetMaxY(consumeText.frame), mainWidth-CGRectGetMaxX(btn.frame)-space, labelHight);
    [self.view addSubview:label1];


    
    label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(label1.frame), mainWidth, labelHight)];
    label2.backgroundColor = [UIColor whiteColor];
    
   label2.attributedText = [self labelAttributedStr:@"0.00"];
    

    [self.view addSubview:label2];

    
    label3 = [UILabel new];
    label3.text = @"实付金额:";
    label3.backgroundColor = [UIColor whiteColor];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.frame = CGRectMake(0, CGRectGetMaxY(label2.frame)+1, 100, labelHight);
    [self.view addSubview:label3];
    
    label4 = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label3.frame), CGRectGetMinY(label3.frame), mainWidth - CGRectGetMaxX(label3.frame), labelHight)];
    label4.text = @"¥0.00  ";
    label4.userInteractionEnabled = NO;
//    [label4 addTarget:self action:@selector(label4TextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    label4.backgroundColor = [UIColor whiteColor];
    label4.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:label4];

  
    payBtn = [self creatButtnWithTitle:@"确认支付" image:nil frame:CGRectMake(space, CGRectGetMaxY(label4.frame)+space, mainWidth-2*space, 40) selected:@selector(payAction)];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [payBtn setBackgroundColor:[UIColor grayColor]];
    

    
    
}

- (void)payAction {
 
    if (isEmptyString(payForMoney)) {
        [XSTool showToastWithView:self.view Text:@"请输入金额"];
        
        return;
    }
    if (isEmptyString(self.supply_name))return;
    
    [XSTool showProgressHUDWithView:self.view];
//    supply_name	必须	string	商家编号
//    pay_total
    
//    {
//        status : 1,
//        info : ,
//        data : {
//            order_id : 86,
//            table_name : quick_order
//        }
//    }
    NSDictionary *dic;
    if (!isEmptyString(mianzhikou)) {
        
        dic = @{@"supply_name":self.supply_name,
                @"pay_total":payForMoney,
                @"price_fixed":mianzhikou};
    }else{
        dic = @{@"supply_name":self.supply_name,
                @"pay_total":payForMoney};
    }
    
    
    [HTTPManager quickorderCreateQuickOrder:dic Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
                                        [XSTool hideProgressHUDWithView:self.view];
                                        
                                        if (state.status) {
                                            OrderPayViewController *order= [OrderPayViewController new];
                                            NSString *ord = dic[@"data"][@"order_id"];
                                            
                                            
                                            if (ord.length ==0) {
                                                return ;
                                            }
                                            order.orderId = ord;
                                            order.tableName = dic[@"data"][@"table_name"];
                                            [self.navigationController pushViewController:order animated:YES];
                                        } else {
                                            [XSTool showToastWithView:self.view Text:state.info];
                                        }
                                        
//                                        NSLog(@"didc  =%@",dic);
                                        
                                    } failure:^(NSError * _Nonnull error) {
                                        [XSTool hideProgressHUDWithView:self.view];
                                    }];
    
}



-(void)textFieldDidChange :(UITextField *)theTextField{
    payForMoney = theTextField.text;
    CGFloat money;
    money = self.quick_percent_pay !=0 ? self.quick_percent_pay /100.0 * [payForMoney floatValue] : [payForMoney floatValue];
    CGFloat tininzhifu = payForMoney.floatValue - money;
    noDiscount = money;
    _discount = tininzhifu;
    [self resetTextInfor:tininzhifu payMoney:noDiscount];
}



/**
 输入不打折扣
 */
- (void)noTextFieldTextFieldDidChange:(UITextField *)textField {
    
    CGFloat ttt = textField.text.floatValue;
    mianzhikou = textField.text;
    if (ttt > payForMoney.floatValue) {
        self.noTextField.text = @"";
    };
    
    // 需要支付数额
    CGFloat discount = payForMoney.floatValue - ttt;
    CGFloat money; // 折扣
    money = self.quick_percent_pay !=0 ? self.quick_percent_pay /100.0 * discount : discount;
    
    CGFloat allMoney =  ttt + money;
    CGFloat tininzhifu = discount - money;
    [self resetTextInfor:tininzhifu payMoney:allMoney];
    
}





/**

 @param money 折扣
 @param payM 需要支付的
 */
- (void) resetTextInfor:(CGFloat )money payMoney:(CGFloat )payM{
    NSString *btnTitle,*contentTexT;

    if ( payM >0) {
        btnTitle = [NSString stringWithFormat:@"¥%.2f",payM];
        btnTitle =  btnTitle.length == 1 ? @"":btnTitle;
      
        contentTexT = btnTitle;
        NSString *title = [NSString stringWithFormat:@"确认支付%@",btnTitle];
        
        [payBtn setTitle:title forState:UIControlStateNormal];
        
        [payBtn setBackgroundColor:RGB(109, 216, 242)];
    }else{
        [payBtn setBackgroundColor:[UIColor grayColor]];
        [payBtn setTitle:@"确认支付" forState:UIControlStateNormal];
        
        contentTexT = @"¥0";
    }
    // 替您支付
    label2.attributedText = [self labelAttributedStr:[NSString stringWithFormat:@"%.2f",money]];
    
    label4.text = contentTexT;
    
    
}



- (void)btnAction:(UIButton *)sender {
    
    [consumeText resignFirstResponder];
    if (!sender.selected) {
        sender.selected = YES;
        [self.view addSubview:self.noTextField];
        [sender setBackgroundImage:[UIImage imageNamed:@"reg_select"] forState:UIControlStateNormal];
        [UIView animateWithDuration:.25 animations:^{
            self.noTextField.frame = CGRectMake(20, CGRectGetMaxY(label1.frame), mainWidth-40, 50);
            label2.frame = CGRectMake(0, CGRectGetMaxY(self.noTextField.frame)+space, mainWidth, labelHight);
        }];
        
    }else{
        
        [self.noTextField removeFromSuperview];
        sender.selected = NO;
        
        [self resetTextInfor:_discount payMoney:noDiscount];
        _noTextField.text = nil;
        [sender setBackgroundImage:[UIImage imageNamed:@"reg_unselect"] forState:UIControlStateNormal];
        [UIView animateWithDuration:.25 animations:^{
            label2.frame =CGRectMake(0, CGRectGetMaxY(label1.frame), mainWidth, labelHight);
        }];
        
        
    }
    [UIView animateWithDuration:.25 animations:^{
        
        label3.frame = CGRectMake(0, CGRectGetMaxY(label2.frame)+space/2, 100, labelHight);
        label4.frame =  CGRectMake(CGRectGetMaxX(label3.frame), CGRectGetMinY(label3.frame), mainWidth - CGRectGetMaxX(label3.frame), labelHight);
       payBtn.frame = CGRectMake(space, CGRectGetMaxY(label4.frame)+space, mainWidth-2*space, 40);
    }];
    
}

//CGRectMake(5, 0, 120, 50) CGRectMake(0, 0, www, 50)
- (UITextField *)noTextField {
    if (!_noTextField) {
        _noTextField = [self createTextFieldWith:CGRectMake(20, CGRectGetMaxY(label1.frame), 0, 0) viewFrame:CGRectMake(0, 0, 120, 50) labelFrame:CGRectMake(5, 0, 120, 50) placeHolder:@"询问服务员后输入" leftViewText:@"不参与优惠金额:" textAlignment:NSTextAlignmentRight keyboardType:UIKeyboardTypeDecimalPad];
        
        [_noTextField addTarget:self action:@selector(noTextFieldTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
    }
    return _noTextField;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self.noTextField resignFirstResponder];
    [consumeText resignFirstResponder];
}


- (UIButton *)creatButtnWithTitle:(NSString *)titile
                image:(UIImage *) image frame:(CGRect)frame
                         selected:(SEL)selected{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (image) {
        
        [btn setBackgroundImage:image forState:UIControlStateNormal];
    }
    if (titile) {
        [btn setTitle:titile forState:UIControlStateNormal];
    }
    [btn addTarget:self action:selected forControlEvents:UIControlEventTouchUpInside];
    
    btn.frame = frame;
    [self.view addSubview:btn];

    return btn;
}

- (UITextField *) createTextFieldWith:(CGRect)frame viewFrame:(CGRect)viewframe
                           labelFrame:(CGRect)labelFrame
                          placeHolder:(NSString *)placeHolder
                         leftViewText:(NSString *)text
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.text = text;
    [leftLabel sizeToFit];
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftLabel];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftLabel.frame = labelFrame;
//    CGFloat www = leftLabel.frame.size.width;
//    CGRectMake(0, 0, www, 50)
    leftView.frame = viewframe;

    leftLabel.textColor = [UIColor lightGrayColor];
    leftLabel.font = [UIFont systemFontOfSize:15];
    [textField setLeftView:leftView];
    
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = leftLabel.font;
    return textField;
}

- (NSAttributedString *)labelAttributedStr:(NSString *)str {
    NSString *str1 = @"    您的土豪朋友";
    NSString *str2 =APP_NAME;
    NSString *str3 = @"将替您支付";
    NSString *endString = [NSString stringWithFormat:@"%@%@%@%@元",str1,str2,str3,str];
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:endString];
    
    
    [AttributedStr addAttribute:NSForegroundColorAttributeName
     
                          value:[UIColor redColor]
     
                          range:NSMakeRange(str1.length, str2.length)];
    
    
    
    [AttributedStr addAttribute:NSForegroundColorAttributeName
     
                          value:[UIColor redColor]
     
                          range:NSMakeRange(str1.length+str2.length+str3.length, str.length)];
    
    
    return AttributedStr;
}

@end
