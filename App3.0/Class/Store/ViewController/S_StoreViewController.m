//
//  S_StoreViewController.m
//  App3.0
//
//  Created by apple on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreViewController.h"
#import "JSDropDownMenu.h"
#import "S_StoreCell.h"
#import "SearchBarView.h"
#import "JSDropDownMenu.h"
#import "S_ChoueseCity.h"
#import "S_StoreInformation.h"
#import "ChatViewController.h"
#import "SearchVC.h"
#import "LocateViewController.h"
#import "GoodsDetailViewController.h"
#import "ProductListViewController.h"
#import "ADDetailWebViewController.h"

#import "TabMallModel.h"

@interface S_StoreViewController ()
<UITableViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate,
JSDropDownMenuDataSource,
JSDropDownMenuDelegate,
SearchBarViewDelegate,
BMKLocationServiceDelegate,
BMKGeoCodeSearchDelegate
>

@property (nonatomic ,strong)UITableView *mainTableView;
@property (nonatomic ,strong)UIImageView  *advImageView;

@property (nonatomic ,strong)NSMutableArray *listArr;
// 行业列表
@property (nonatomic ,strong)NSMutableArray *data1;

@property (nonatomic ,strong)SearchBarView *searchView;

@property (nonatomic, strong) BMKLocationService *manager;

@property (nonatomic, strong)BMKGeoCodeSearch *geoCoder;

@property (nonatomic ,strong)UIButton *locationButton;// 选择城市

@property (nonatomic ,strong)NSMutableArray *searchBarCityList;

@end

@implementation S_StoreViewController
{

    NSArray *_data3;
    NSInteger _currentData1Index;
    NSInteger _currentData2Index;
    NSInteger _currentData3Index;
    NSInteger _currentData1SelectedIndex;
    NSInteger pageNumber;
    UILabel *addressLabel; // 地址
    NSString *searchContent;
    // 定位经纬度
    CLLocationCoordinate2D coordinate;
    BOOL selectedOne,selectedTwo,selectedThree;
    BOOL isShowSearchText; // 定位是否请求数据
    NSString *currentCity;
    NSString *cityName;
    NSString *chouseCityName;
    BOOL isSelectedOtherCity;
    NSDictionary *cityList;
    NSDictionary *advertisementModel;
    
    NSString *cityID;
    BOOL isRefresh;
    UIView *bgView;
    __block BOOL haveSelectedCity;//有没有去选择城市
    
    BOOL isLoadData; // 定位一次
    JSDropDownMenu *_headMenu;
    
}



- (BMKGeoCodeSearch *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[BMKGeoCodeSearch alloc] init];
    }
    return _geoCoder;
}

#pragma mark - 懒加载
- (BMKLocationService *)manager
{
    if (!_manager) {
        _manager = [[BMKLocationService alloc] init];
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        
    }
    return _manager;
}




-(instancetype)initWithSearchText:(NSString *)search {
    
    if (self = [super init]) {
        
        [self isExistSearchText:search];
    }
    return self;
}


- (void) isExistSearchText:(NSString *)searchText {
    
//    self.navigationController.navigationBarHidden=NO;
    
    self.searchView.alpha = 1;
    self.locationButton.alpha = 1;
    
    if (searchText) {
        isShowSearchText = YES;
        self.searchView.textField.text = searchText;
        searchContent = searchText;
        SupplyModel *model = [[SupplyModel alloc] initWithPage];
        model.name = searchText;
        [self getSupplyList:model];
    }else{

        
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [self.navigationController.navigationBar addSubview:self.searchView];
    [self.navigationController.navigationBar addSubview:self.locationButton];
    if (haveSelectedCity) {
        return;
    }

    if (!isShowSearchText) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    self.manager.delegate = self;
    self.geoCoder.delegate = self;
    [self.manager startUserLocationService];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //    requestLoccationAddress = dispatch_queue_create("requestLoccationAddress", DISPATCH_QUEUE_SERIAL_INACTIVE);
    
    selectedOne = YES;
    self.data1 = [NSMutableArray array];
    [self.searchBarCityList addObject:@{@"name":@"全部城市"}];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        
        @strongify(self);
        [self.locationButton removeFromSuperview];
        [self.searchView removeFromSuperview];
        self.searchView = nil;
        for (UIView *view in self.view.subviews) {
            [view removeFromSuperview];
        }
        
//        UIViewController *vc = self.navigationController.viewControllers[0];
//        [self.navigationController popToViewController:vc animated:YES];
//        self.tabBarController.tabBar.hidden = NO;
        [self.navigationController popViewControllerAnimated:YES];
        
    }];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    
    [self creatContentView];
    [self setupRefesh];
    [self getAdvertisement];
    [self getIndustryList:@"1"];
    self.mainTableView.tableFooterView = [UIView new];
    
#ifdef __IPHONE_11_0
    if (@available(iOS 11.0, *)) {
        self.mainTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.mainTableView.scrollIndicatorInsets = self.mainTableView.contentInset;
    }
#endif
}



- (SearchBarView *)searchView {
    if (!_searchView) {
        
        
        _searchView = [[SearchBarView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.locationButton.frame)+10, 7, mainWidth-CGRectGetWidth(self.locationButton.frame)-10, 30)];
        _searchView.searchBarStyle = SearchBarStyleRoundedRectNoButton;
        _searchView.delegate = self;
        _searchView.userInteractionEnabled = YES;
        
        [_searchView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            
            searchContent = nil;
            SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
            search.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:search animated:YES];
            
        }]];
        
    }
    return _searchView;
}



- (NSMutableArray *)listArr {
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}



- (UITableView *)mainTableView {
    if (!_mainTableView) {
        
        _mainTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        [self.view addSubview:_mainTableView];
        
        [_mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bgView.mas_bottom);
            make.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(self.view);
//            make.edges.mas_equalTo(UIEdgeInsetsMake(SCREEN_H_Height(100)+95+64, 0, 0, 0));
        }];
        
    }
    return _mainTableView;
}



-(UIButton *)locationButton {
    if (!_locationButton) {
        
        _locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _locationButton.backgroundColor = [UIColor clearColor];
        
        _locationButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_locationButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _locationButton.frame = CGRectMake(50, 7, 70, 30);
        [_locationButton addTarget:self action:@selector(selectedLocation) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _locationButton;
}


#pragma mark ---
#pragma mark --- 跳转选择城市界面

- (void)selectedLocation {
    S_ChoueseCity *chouseCity =  [[S_ChoueseCity alloc] init];
    isSelectedOtherCity = YES;
    _currentData1Index = 0;
    _currentData1SelectedIndex = 1;
    
    @weakify(self);
    [chouseCity setSelectedCountry:^(NSDictionary *dic,NSString *showTitle) {
        @strongify(self);
        haveSelectedCity=YES;
        [self pushInformation:dic showTitle:showTitle];
    }];
    
    
    [chouseCity setSelectedCity:^(NSDictionary *dic, NSString *title) {
        @strongify(self);
        haveSelectedCity=YES;
        [self selectedCity:dic title:title];
        
    }];
    chouseCity.showAddress = self.locationButton.titleLabel.text;
    [self.navigationController pushViewController:chouseCity animated:YES];
    
}

- (void)pushInformation:(NSDictionary *)dic showTitle:(NSString *)showTitle {
    [self.locationButton setTitle:showTitle forState:UIControlStateNormal];
    NSString *dicKey = dic[@"name"];
    NSString *requestID = dic[dicKey][@"name"];
    [self getAreaList:requestID];
    SupplyModel *model = [[SupplyModel alloc]initWithPage];
    //    model.page = 1;
    //    model.limit = 15;
    model.city = requestID;
    chouseCityName = requestID;
    [self getSupplyList:model];
    
}

- (void)selectedCity:(NSDictionary *)dic title:(NSString *)title {
    [self.locationButton setTitle:title forState:UIControlStateNormal];
    
    NSString *ID = dic[@"name"];
    [self getAreaList:ID];
    SupplyModel *model = [[SupplyModel alloc]initWithPage];
    model.city = ID;
    chouseCityName = ID;
    [self getSupplyList:model];
    
}


#pragma  mark - -
#pragma  mark - - MJRefresh
- (void)setupRefesh {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [header setTitle:Localized(@"下拉加载最新数据") forState:MJRefreshStatePulling];
    self.mainTableView.mj_header = header;
    
    self.mainTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadOtherData)];
    
    
}


- (void) loadNewData {
    
    if (!isShowSearchText) {
        pageNumber = 1;
        SupplyModel *model = [[SupplyModel alloc]initWithPage];
        
        if (_currentData1Index) model.industry = self.data1[_currentData1Index][@"id"];
        if (_currentData2Index) model.county = self.searchBarCityList[_currentData2Index][@"name"];
        model._order = _data3[_currentData3Index][@"list"];
        if (searchContent) model.supplyName = searchContent;
        //    if (selectedOne || selectedThree) {
        NSDictionary *dic = @{@"longitude":[NSString stringWithFormat:@"%.15f",coordinate.longitude],@"latitude":[NSString stringWithFormat:@"%.15f",coordinate.latitude]};
        model.user_position = dic.mj_JSONString;
        //    }
        
        if (!isEmptyString(currentCity)) [self.locationButton setTitle:currentCity forState:UIControlStateNormal];
        model.city = isSelectedOtherCity ? chouseCityName:cityName;
        [self getSupplyList:model];
    }else{
        
        [self.mainTableView.mj_header endRefreshing];
    }
}

- (void)loadOtherData {
    pageNumber ++;
    SupplyModel *model = [SupplyModel new];
    if (_currentData1Index) model.industry = self.data1[_currentData1Index][@"id"];
    if (_currentData2Index) model.county = self.searchBarCityList[_currentData2Index][@"name"];
    model._order = _data3[_currentData3Index][@"list"];
    //    if (selectedOne || selectedThree) {
    NSDictionary *dic = @{@"longitude":[NSString stringWithFormat:@"%.15f",coordinate.longitude],@"latitude":[NSString stringWithFormat:@"%.15f",coordinate.latitude]};
    model.user_position = dic.mj_JSONString;
    //    }
    model.page = pageNumber;
    model.limit = 15;
    if (searchContent) model.supplyName = searchContent;
    
    model.city = isSelectedOtherCity ? chouseCityName:cityName;
    [self getSupplyList:model];
    
}


- (void)getIndustryList:(NSString *)page  {
    
    if (self.data1.count >0) {
        return;
    }
    [HTTPManager getIndustryListsParams:@{@"page":page,
                                          @"limit":@"100"}
                                Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
                                    if (state.status) {
                                        if ([_data1 count]>0) [_data1 removeAllObjects];
                                        NSArray *arr = dic[@"data"][@"data"];
                                        for (int x = 0; x < arr.count; x ++) {
                                            [_data1 addObject:arr[x]];
                                        }
                                        [self.data1 insertObject:@{@"name":@"全部行业",
                                                                   @"id":@""} atIndex:0];
                                    }
                                } failure:^(NSError * _Nonnull error) {
                                    
                                    NSLog(@"errr =%@",error.localizedDescription);
                                }];
}


-(NSMutableArray *)searchBarCityList {
    if (!_searchBarCityList) {
        
        _searchBarCityList = [NSMutableArray array];
        
    }
    return _searchBarCityList;
}

- (void)search:(NSString *)text {
    
}
- (void)searchTextChanged:(NSString *)text {
    
}
- (void)searchTypeChanged:(UIButton *)searchButton {
    
}


- (void) advertisementShowViewController {
    
    if (advertisementModel == NULL) return;
    
    NSString *link_in = advertisementModel[@"link_in"];
    NSString *link_objid = advertisementModel[@"link_objid"];
    NSString *name = advertisementModel[@"name"];
    if ([link_in isEqualToString:@"product"]) {
        [self goGoodsDetailWithGoodsId:link_objid];
        
    }else if([link_in isEqualToString:@"store"]){
        //进入店铺
        if (!isEmptyString(link_objid)) {
            S_StoreInformation *infoVC=[[S_StoreInformation alloc] init];
            infoVC.storeInfor=link_objid;
            infoVC.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:infoVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"店铺不存在"];
        }
    }else if([link_in isEqualToString:@"productlists"]){//商品列表
        NSMutableDictionary *paraDic=[NSMutableDictionary dictionary];
        NSArray *listArray=[link_objid componentsSeparatedByString:@";"];
        for (NSString *listStr in listArray) {
            NSArray *tempArray=[listStr componentsSeparatedByString:@"="];
            [paraDic setValue:[tempArray lastObject] forKey:[tempArray firstObject]];
        }
        ProductListViewController *listVC = [[ProductListViewController alloc] init];
        listVC.model = [ProductModel mj_objectWithKeyValues:paraDic];
        listVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:listVC animated:YES];
    }else{
        [self goDetailWebViewWithUrlStr:link_in title:name placeTitle:@"广告"];
    }
}

-(void)goDetailWebViewWithUrlStr:(NSString *)urlStr title:(NSString *)title placeTitle:(NSString *)placeTitle{
    ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
    adWeb.urlStr=urlStr;
    adWeb.navigationItem.title=title.length?title:placeTitle;
    adWeb.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:adWeb animated:YES];
}
/*
 data : 1 (
	{
	backcolor : ,
	link_in : store,
	link_objid : ,
	name : 8,
	url : http://nwmvg.zj.dsceshi.cn/index.php?s=/Mobile/Stores/index,
	image : /upload/gugert/20170515/b24848a376c1f20005fa3705b1e97b85.png
 },
 */
-(void)goGoodsDetailWithGoodsId:(NSString *)goodsId{
    
    if (goodsId.length) {
        
        GoodsDetailViewController *goodsDetailVC = [GoodsDetailViewController new];
        goodsDetailVC.goodsID=goodsId;
        goodsDetailVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:goodsDetailVC animated:YES];
    }else{
        [XSTool showToastWithView:self.view Text:@"商品不存在"];
    }
}



- (void) creatContentView {
    isLoadData = YES;
    CGFloat topMargin = 10;
    self.advImageView = [UIImageView new];
    self.advImageView.frame = CGRectMake(0, 0, mainWidth, SCREEN_H_Height(100));
    self.advImageView.backgroundColor = [UIColor clearColor];
    self.advImageView.image=[UIImage imageNamed:@"no_pic"];
    self.advImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.advImageView];
    
    _advImageView.userInteractionEnabled = YES;
    @weakify(self);
    [self.advImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        [self advertisementShowViewController];
    }]];
    
    
    // 指定默认选中
    _currentData1Index = 0;
    _currentData1SelectedIndex = 1;
    _currentData2Index=0;
    _currentData3Index=0;
    _data3 = @[@{@"name":@"默认排序",@"list":@""},@{@"name":@"离我最近",
                 @"list":@"distance"}, @{@"name":@"好评优先",
                                         @"list":@"eva"}];
    
    _headMenu = [[JSDropDownMenu alloc] initWithOrigin:CGPointMake(0, CGRectGetMaxY(self.advImageView.frame)+topMargin/2) andHeight:45];
    _headMenu.indicatorColor = [UIColor colorWithRed:175.0f/255.0f green:175.0f/255.0f blue:175.0f/255.0f alpha:1.0];
    _headMenu.separatorColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0];
    _headMenu.textColor = [UIColor colorWithRed:83.f/255.0f green:83.f/255.0f blue:83.f/255.0f alpha:1.0f];
    
    _headMenu.dataSource = self;
    _headMenu.delegate = self;
    
    [self.view addSubview:_headMenu];
    
    bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    
    bgView.userInteractionEnabled = YES;
    bgView.layer.borderWidth = .5;
    bgView.layer.borderColor = [UIColor hexFloatColor:@"f4f4f4"].CGColor;
    [self.view addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(_headMenu.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    
    UIImageView *locaImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_verify_address"]];
    [bgView addSubview:locaImage];
    [locaImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView.mas_left).offset(12);
        make.centerY.mas_equalTo(bgView);
    }];
    
    UILabel *bgImage = [UILabel new];
    bgImage.text = @"当前位置:";
    bgImage.font = [UIFont systemFontOfSize:15];
    [bgView addSubview:bgImage];
    [bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locaImage.mas_right).offset(5);
        make.centerY.mas_equalTo(bgView.mas_centerY);
    }];
    
    
    addressLabel = [UILabel new];
    addressLabel.text = @"正在定位中...";
    
    addressLabel.font = [UIFont systemFontOfSize:15];
    addressLabel.backgroundColor = [UIColor whiteColor];
    addressLabel.textAlignment = NSTextAlignmentLeft;
    [bgView addSubview:addressLabel];

    
    
    
    UIImageView *rightImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"store_loca"]];
    [bgView addSubview:rightImage];
    
    [rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView.mas_right).offset(-12.5);
        make.centerY.mas_equalTo(bgView.mas_centerY);
    }];
    
    /*  重新定位*/
//
//    relocation = [UILabel new];
//    relocation.text = @"";
//    relocation.textAlignment = NSTextAlignmentLeft;
//    relocation.font = [UIFont qsh_systemFontOfSize:14];
////    [relocation setTitle:@"(重新定位)" forState:UIControlStateNormal];
////    relocation.titleLabel.font = [UIFont systemFontOfSize:14];
////    [relocation addTarget:self action:@selector(relocationAction) forControlEvents:UIControlEventTouchUpInside];
//    [bgView addSubview:relocation];
    
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgImage.mas_right).offset(5);
        make.centerY.mas_equalTo(bgImage.mas_centerY);
//        make.right.mas_equalTo(rightImage.mas_left).offset(-15);
    }];
    

    
    [bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        [self relocationAction];
    }]];
    
}


#pragma mark - - - -
#pragma mark - - - - 定位 请求数据
/**
 定位 请求数据
 */
- (void)relocationAction {
    isSelectedOtherCity = NO;
    isLoadData = NO;
    haveSelectedCity=NO;
    self.manager.delegate = self;
    self.geoCoder.delegate = self;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.manager startUserLocationService];
    
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    S_StoreCell *cell = [S_StoreCell cellWithTableView:tableView];
    cell.model = self.listArr[indexPath.row];
    cell.collectB.tag = indexPath.row +1000;
    cell.talkB.tag = indexPath.row +10000;
    [cell.collectB addTarget:self action:@selector(collectionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.talkB addTarget:self action:@selector(talkButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (void)collectionButtonAction:(UIButton *)sender {
    S_StoreModel *model  = self.listArr[sender.tag-1000];
    if (isEmptyString(model.user_ID)) {
        [MBProgressHUD showMessage:@"店铺信息错误" view:self.view];
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (model.isFav) {
        [HTTPManager deleteShopCarInfoWithFavId:model.user_ID success:^(NSDictionary *dic, resultObject *state) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (state.status) {
                [sender setTitle:@"收藏店铺" forState:UIControlStateNormal];
                model.isFav=NO;
            }
            
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        }];
    }else{
        [HTTPManager setFavorite:model.user_ID Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (state.status) {
                [sender setTitle:@"取消收藏" forState:UIControlStateNormal];
                model.isFav=YES;
            }
        } failure:^(NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
        
    }
    
}



- (void)talkButtonAction:(UIButton *)sender {
    S_StoreModel *model  = self.listArr[sender.tag-10000];
#ifdef ALIYM_AVALABLE
    YWPerson *person = [[YWPerson alloc] initWithPersonId:model.user_ID];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = model.name;
    chatVC.avatarUrl = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,model.logo];
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:model.user_ID type:EMConversationTypeChat createIfNotExist:YES];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.avatarUrl = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,model.logo];
    chatVC.title = model.name;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    XMChatController * chatVC = [XMChatController new];
    
    SiganlChatModel *siganlModel = [SiganlChatModel new];
    siganlModel.conversationID = getJID(model.user_ID);
    siganlModel.avatarURLPath = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,model.logo];
    siganlModel.title = model.name;
    siganlModel.userName = model.username;
    ConversationModel *contactModel = [[ConversationModel alloc] initWithSiganlChatModel:siganlModel];
    chatVC.conversationModel = contactModel;
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    S_StoreInformation *infor = [S_StoreInformation new];
    S_StoreModel *model = self.listArr[indexPath.row];
    infor.storeInfor = model.user_ID;
    [self.navigationController pushViewController:infor animated:YES];
}




#pragma mark   ------
#pragma mark   ---- JSDropDownMenu筛选菜单

- (NSInteger)numberOfColumnsInMenu:(JSDropDownMenu *)menu { return 3;}

-(BOOL)displayByCollectionViewInColumn:(NSInteger)column{return NO;}

-(BOOL)haveRightTableViewInColumn:(NSInteger)column{return NO;}

-(CGFloat)widthRatioOfLeftColumn:(NSInteger)column{return 1;}

-(NSInteger)currentLeftSelectedRow:(NSInteger)column{
    
    if (column==0) {return _currentData1Index;
    }else if (column==1) {return _currentData2Index;
    }else if (column==2) {return _currentData3Index;
    }
    return 0;
}



- (NSInteger)menu:(JSDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column leftOrRight:(NSInteger)leftOrRight leftRow:(NSInteger)leftRow{
    
    if (column==0) {return _data1.count;
    } else if (column==1){return self.searchBarCityList.count;
    } else if (column==2){return _data3.count;
    }
    return 0;
}



- (NSString *)menu:(JSDropDownMenu *)menu titleForColumn:(NSInteger)column{
    
    switch (column) {
        case 0:return [self.data1 count]==0 ?@"全部行业" : _data1[_currentData2Index][@"name"];break;
        case 1: return self.searchBarCityList[_currentData2Index][@"name"];break;
        case 2: return _data3[_currentData3Index][@"name"];break;
        default:return nil;break;
    }
}



- (NSString *)menu:(JSDropDownMenu *)menu titleForRowAtIndexPath:(JSIndexPath *)indexPath {
    
    if (indexPath.column==0) {return _data1[indexPath.row][@"name"];
    } else if (indexPath.column==1) {return self.searchBarCityList[indexPath.row][@"name"];
    } else {return _data3[indexPath.row][@"name"];}
}

- (void)menu:(JSDropDownMenu *)menu didSelectRowAtIndexPath:(JSIndexPath *)indexPath {
    SupplyModel *model = [[SupplyModel alloc]initWithPage];
    NSString * industry,*county,*_order;
    switch (indexPath.column) {
        case 0:{
            selectedOne = YES;
            selectedTwo = NO;
            selectedThree = NO;
            if (indexPath.row != 0) industry = self.data1[indexPath.row][@"id"];
            
            _currentData1Index = indexPath.row;
            if (_currentData2Index) county = self.searchBarCityList[_currentData2Index][@"name"];
            _order = _data3[_currentData3Index][@"list"];
        }
            break;
        case 1:{
            selectedOne = NO;
            selectedTwo = YES;
            selectedThree = NO;
            if (indexPath.row != 0) county = self.searchBarCityList[indexPath.row][@"name"];
            _currentData2Index = indexPath.row;
            if (_currentData1Index) industry = self.data1[_currentData1Index][@"id"];
            _order = _data3[_currentData3Index][@"list"];
            
        }
            break;
        case 2:{
            selectedOne = NO;
            selectedTwo = NO;
            selectedThree = YES;
            
            _order = _data3[indexPath.row][@"list"];
            _currentData3Index = indexPath.row;
            if (_currentData1Index) industry = self.data1[_currentData1Index][@"id"];
            if (_currentData2Index) county = self.searchBarCityList[_currentData2Index][@"name"];
            
            
        }
            break;
            
        default:
            break;
    }
    if (!isEmptyString(searchContent)) model.supplyName = searchContent;
    
    NSDictionary *dic = @{@"longitude":[NSString stringWithFormat:@"%.15f",coordinate.longitude],@"latitude":[NSString stringWithFormat:@"%.15f",coordinate.latitude]};
    model.user_position = dic.mj_JSONString;
    
    model.industry = industry;
    model.county = county;
    model._order = _order;
    model.city = isSelectedOtherCity ? chouseCityName:cityName;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self getSupplyList:model];
    
    
}




// 解决手势与点击事件冲突
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        
        return NO;
    }else if ([NSStringFromClass([touch.view class])isEqualToString:@"UIView"]){
        
        return NO;
    }
    
    return YES;
}

#pragma mark - - - getSupplyList 请求数据

- (void)getSupplyList:(SupplyModel *)model{
    if (self.searchView.textField.text.length) {
        if (!model.county.length) {
            model.user_position=@"";
            model.city=@"";
        }
        model.supplyName=self.searchView.textField.text;
    }

    [HTTPManager getSupply_ListsParams:model Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
            
            NSArray *arr = dic[@"data"][@"list"][@"data"];
            if (model.page == 1) {[self.listArr removeAllObjects];}
            if (arr.count == 0) [MBProgressHUD showMessage:@"暂无更多数据" view:self.view];
            
            
            for (NSInteger x =0; x <arr.count; x ++) {
                @autoreleasepool {
                    
                    S_StoreModel *storeModel = [S_StoreModel mj_objectWithKeyValues:arr[x]];
                    if (!storeModel.distance) {
                        CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(storeModel.latitude, storeModel.longitude);
                        NSString *str = [self getDistanceFromLocation:coordinate needAddress:coor];
                        storeModel.distance = str;
                    }
                    [self.listArr addObject:storeModel];
                }
                
            }
            
            [self.mainTableView reloadData];
            
            
        }else{
            
            [MBProgressHUD showMessage:dic[@"infor"] view:self.view];
        }
        
        [self tableViewEndRefreshing];
    } failure:^(NSError * _Nonnull error) {
        [self tableViewEndRefreshing];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}


- (void)tableViewEndRefreshing {
    
    [self.mainTableView.mj_header endRefreshing];
    [self.mainTableView.mj_footer endRefreshing];
    
}


/**
 计算经纬度距离
 
 @param myLocation 当前定位
 @param tmpNewsModel 后台传来位置
 */
- (NSString *)getDistanceFromLocation:(CLLocationCoordinate2D )myLocation needAddress:(CLLocationCoordinate2D)tmpNewsModel {
    
    CLLocation *orig=[[CLLocation alloc] initWithLatitude:myLocation.latitude  longitude:myLocation.longitude];
    CLLocation* dist=[[CLLocation alloc] initWithLatitude:tmpNewsModel.latitude longitude:tmpNewsModel.longitude];
    CLLocationDistance kilometers=[orig distanceFromLocation:dist]/1000.0f;
    NSString *distance = [NSString stringWithFormat:@"%.3f",kilometers];
    
    return distance;
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchView removeFromSuperview];
    [self.locationButton removeFromSuperview];
    self.searchView = nil;
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.geoCoder = nil;
    self.manager.delegate = nil;
    self.manager = nil;
}

- (void) getAreaList:(NSString *)locationCity {
    
    if (!cityList) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"cityList" ofType:@"plist"];
        cityList = [XSTool returnDictionaryWithDataPath:plistPath];
    }
    NSMutableString *lC = locationCity.mutableCopy;
    NSString *areaKey;
    NSString *areaID;
    if ([lC containsString:@"市"]) {
        NSRange range = [lC rangeOfString:@"市"];
        [lC deleteCharactersInRange:range];
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
        
    }else{
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
    }
    
    
    if (!areaID) return;
    
    
    [self getAddressArea:areaID];
    
    
    
}





- (void)getAddressArea:(NSString *)areaID {
    
    if ([self.searchBarCityList count]>0) [self.searchBarCityList removeAllObjects];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getAreaChildList:areaID Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
            NSArray *arr = dic[@"data"];
            [self.searchBarCityList addObject:@{@"name":@"全部城市"}];
            for (int x = 0; x <arr.count ; x ++) {
                [self.searchBarCityList addObject:arr[x]];
            }
        }
        
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:@"网络异常,请稍后重试" view:self.view];
    }];
    if (!isRefresh) {
        [self.mainTableView.mj_header beginRefreshing];
        isRefresh = YES;
    }
}


- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    [self.manager stopUserLocationService];
    coordinate = userLocation.location.coordinate;
    [self startGeocodesearchWithCoordinate:userLocation.location.coordinate];
    
}


-(void)startGeocodesearchWithCoordinate:(CLLocationCoordinate2D)coor
{
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = coor;
    BOOL flag = [self.geoCoder reverseGeoCode:reverseGeocodeSearchOption];
    if(flag)
    {
        isLoadData = NO;
        NSLog(@"反geo检索发送成功");
    }
    else
    {
        NSLog(@"反geo检索发送失败");
    }
}

- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    if (error == BMK_SEARCH_NO_ERROR)
    {
        
       
       addressLabel.text = [NSString stringWithFormat:@"%@%@",result.addressDetail.district,result.addressDetail.streetName];
        
        [self.locationButton setTitle:result.addressDetail.city forState:UIControlStateNormal];
        [self getAreaList:result.addressDetail.city];
        if (isShowSearchText) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        }
        if ( !isLoadData) {
            
            cityName = result.addressDetail.city;
            
            if ([cityName containsString:@"市"]) cityName = [cityName stringByReplacingOccurrencesOfString:@"市" withString:@""];
            SupplyModel *model = [[SupplyModel alloc]initWithPage];
            //            coordinate = [WGS84TOGCJ02 transformFromGCJToBaidu:coordinate];
            NSDictionary *dic = @{@"longitude":[NSString stringWithFormat:@"%.15f",coordinate.longitude],@"latitude":[NSString stringWithFormat:@"%.15f",coordinate.latitude]};
            model._order = @"distance";
            model.user_position = dic.mj_JSONString;
            model.city=cityName;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self getSupplyList:model];
            
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
    }else{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}




// 广告
- (void)getAdvertisement {
    
    [HTTPManager basicadvertListsSuccrss:^(NSDictionary * _Nullable dic, resultObject *state) {
        
        if (state.status) {
            NSArray *array = dic[@"data"];
            if (array.count == 0) return;
            //            NSString *str = dic[@"data"][@"mobile_supply_list_top][@"image"];
            
            advertisementModel = dic[@"data"][@"mobile_supply_list_top"][0];
            [self.advImageView getImageWithUrlStr:advertisementModel[@"image"] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            
        }
        
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
    
    
}


@end

@implementation SupplyModel

- (instancetype)initWithPage {
    if (self = [super init]) {
        self.county = @"";
        self.industry = @"";
        self._order = @"";
        self.supplyName = @"";
        self.industry = @"";
        self.page = 1;
        self.limit = 15;
    }
    return self;
}


@end




