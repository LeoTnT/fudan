//
//  S_StoreInformation.h
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_StoreInformation : XSBaseCollectionView


@property (nonatomic ,copy)NSString *storeInfor;

@property (nonatomic ,copy)NSString *searchText;
@end


/*
 user_id    int    商家id
 username    string    商家编号
 uprovince    string    商家所在省
 ucity    string    商家所在省
 ucounty    string    商家所在县区
 utown    string    商家所在街镇
 uaddress    string    商家详细地址
 logo    string    店铺logo
 banner_wap    string    店铺手机端幻灯片
 banner_pc    string    店铺PC端幻灯片
 name    string    店铺名
 mobile    string    店铺用户手机号
 tel    string    店铺电话
 province    string    店铺所在省
 city    string    店铺所在省
 county    string    店铺所在县区
 town    string    店铺所在街镇
 address    string    店铺详细地址
 fav_num    int    店铺被收藏数
 longitude    string    店铺位置经度
 latitude    string    店铺位置纬度
 desc    string    店铺简介
 iname    string    店铺行业
 is_enable_quickpay    int    是否启用商家快速买单
 quick_pay
 */

@interface S_SupplyInfoModel : NSObject


@property (nonatomic,assign)BOOL is_enable_quickpay;

@property (nonatomic,assign)NSInteger quick_pay;


@property (nonatomic ,copy)NSString *background;
@property (nonatomic ,copy)NSString *logo;
@property (nonatomic ,copy)NSString *banner_wap;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *province;
@property (nonatomic ,copy)NSString *city;
@property (nonatomic ,copy)NSString *town;
@property (nonatomic ,copy)NSString *address;

@property (nonatomic ,assign)CGFloat longitude;
@property (nonatomic ,assign)CGFloat latitude;
@property (nonatomic ,copy)NSString *mobile;
@property (nonatomic ,copy)NSString *uprovince;

@property (nonatomic ,copy)NSString *username;

@property (nonatomic ,copy)NSString *ucity;
@property (nonatomic ,copy)NSString *utown;
@property (nonatomic ,copy)NSString *uaddress;
@property (nonatomic ,copy)NSString *tel;

@property (nonatomic ,copy)NSString *fav_num;
@end

//@interface S_HotProductModel : NSObject
//
//@end
//@interface S_NewProductModel : NSObject
//
//@end
/*
 "product_id": 30,
 "product_name": "商品名-测试2次",
 "sell_price": "0.00",
 "market_price": "0.00",
 "image": "/upload/product/777777/20170103/test.png",
 "user_id": 1,
 "sell_num": 0,
 "look_num": 0
 |--id    int    分类
 |--name    string    分类名称
 |--product_arr    array    下属商品数据
 */
@interface S_SysProductModel : NSObject
@property (nonatomic ,copy)NSString *ID;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *image;
@property (nonatomic ,strong)NSArray *product_arr;



@end

@interface SupplyAdvertModle :NSObject

@property (nonatomic ,copy)NSString *image;
@property (nonatomic ,copy)NSString *link_in;
@property (nonatomic ,copy)NSString *link_objid;
@property (nonatomic ,copy)NSString *position;
@property (nonatomic ,copy)NSString *position_name;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *url;

/*
 image = "http://paigong-lws.dsceshi.cn/uploads/upsrv/e533ff08988020070161a91e9f278842/20171215/8a183f70910e6c384ac08e8965fea11b.jpg";
 "link_in" = productlists;
 "link_objid" = "";
 position = "supply_left";
 "position_name" = "\U624b\U673a\U5e97\U94fa\U9996\U9875\U4e0a\U65b9\U504f\U5de6";
 title = "\U6211\U5c31\U53d11\U4e2a\U5e7f\U544a";
 url = "";
 */
@end

@interface Product_arrModel:NSObject

/*
 --|--product_id    string    商品
 |--|--product_name    string    商品名
 |--|--image_thumb    string    商品图
 |--|--sell_price    float    售价
 |--|--sell_num    int    已售数
 |--|--fav_num    int    收藏数
 */
@property (nonatomic ,copy)NSString *product_id;
@property (nonatomic ,copy)NSString *product_name;
@property (nonatomic ,copy)NSString *sell_price;
@property (nonatomic ,copy)NSString *image_thumb;
@property (nonatomic ,copy)NSString *sell_num;
@property (nonatomic ,copy)NSString *fav_num;

@end



@interface S_InforModel : NSObject

@property (nonatomic ,strong)S_SupplyInfoModel *supplyInfo;
//@property (nonatomic ,strong)NSMutableArray *hotProduct;
@property (nonatomic ,strong)NSMutableArray *supplyAdvert;
@property (nonatomic ,strong)NSMutableArray *supplyProduct;

@property (nonatomic ,assign)BOOL is_fav;

@end



/*
 {
 "status": 1,
 "info": "",
 "data": {

 "hotProduct": [],
 "newProduct": [],
 "sysProduct": [
 {
 "product_id": 11,
 "product_name": "商品11 普通商品",
 "sell_price": "0.00",
 "market_price": "0.00",
 "image": "/upload/product/777777/20170103/pro.png",
 "user_id": 10,
 "sell_num": 9,
 "look_num": 9
 },
 {
 "product_id": 30,
 "product_name": "商品名-测试2次",
 "sell_price": "0.00",
 "market_price": "0.00",
 "image": "/upload/product/777777/20170103/test.png",
 "user_id": 1,
 "sell_num": 0,
 "look_num": 0
 },
 {
 "product_id": 33,
 "product_name": "123",
 "sell_price": "0.00",
 "market_price": "0.00",
 "image": "/Upload/AdDefault/14298620997476662.jpg",
 "user_id": 1,
 "sell_num": 0,
 "look_num": 0
 },
 {
 "product_id": 36,
 "product_name": "qwewqe",
 "sell_price": "0.00",
 "market_price": "0.00",
 "image": "1.jpg",
 "user_id": 1,
 "sell_num": 0,
 "look_num": 0
 }
 ]
 }
 }
 */
