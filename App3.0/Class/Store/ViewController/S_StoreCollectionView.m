//
//  S_StoreCollectionView.m
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreCollectionView.h"
#import "S_StoreHeaderView.h"
#import "S_StoreCell.h"
#import "S_StoreInformation.h"
#import "GoodsDetailViewController.h"
#import "SearchVC.h"
@interface S_StoreCollectionView ()

//@property (nonatomic ,strong)NSMutableArray *collectionModel.collectionDataSource;
@property (nonatomic ,strong)S_TopSrearchView *topView;

@property (strong, nonatomic) DefaultView *emptyView;
@end

@implementation S_StoreCollectionView
{
    
//    UILabel *searchLabel;
    BOOL isSearchText; // 默认NO
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.topView.searchLabel.text = isEmptyString(_searchText) ? @"搜索店铺":_searchText;
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.collectionView registerClass:[S_collectionCell class] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerClass:[S_StoreHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerView"];

    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.isShowBackView = YES;
    self.flowLayout.minimumInteritemSpacing = 2;
    self.flowLayout.minimumLineSpacing = 2;
    self.flowLayout.headerReferenceSize = CGSizeMake(mainWidth, 65);
    self.flowLayout.footerReferenceSize = CGSizeMake(mainWidth, 12);
    self.loadingImage = @"store_backImage";
    [self setRefreshStyle];

    self.emptyView = [[DefaultView alloc] initWithFrame:self.view.bounds];
    self.emptyView.button.hidden = YES;
    self.emptyView.titleLabel.text = @"无搜索结果";
    self.emptyView.hidden = YES;
    [self.view addSubview:self.emptyView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isSearchText) {
        [self loadRefreshData];
    }

}

- (void)loadRefreshData {

    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    SupplyModel *model = [SupplyModel new];
    model.page = self.page;
    model.limit = 15;

    self.requestModel = model;

}


- (void)setRequestModel:(SupplyModel *)requestModel {
    _requestModel = requestModel;
    RACSignal *signal =  [XSHTTPManager rac_POSTURL:Url_Supply_Lists params:requestModel.mj_keyValues];
    [signal subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSArray *list = state.data[@"list"][@"data"];
            self.emptyView.hidden = YES;
            if (list.count == 0) {
                [self endRefresh];
                if (self.page == 1) {
                    self.emptyView.hidden = NO;
                } else {
                    [MBProgressHUD showMessage:Localized(@"no_more_data") view:self.view hideTime:1 doSomeThing:nil];
                }
                
                return ;
            }
            [list.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                Store_SupplyModel *model = [Store_SupplyModel mj_objectWithKeyValues:x];
                [arr addObject:model];
            } completed:^{
                if (self.page !=1) {
                    [self.collectionModel.collectionDataSource addObjectsFromArray:arr];
                }else{
                    self.collectionModel.collectionDataSource = arr;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                    [self endRefresh];
                });
                
            }];
        }
        
    }] ;

    [signal subscribeError:^(NSError * _Nullable error) {
        [self endRefresh];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}

- (void)setSearchText:(NSString *)searchText {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _searchText = searchText;
    isSearchText = YES;
    self.topView.searchLabel.text = searchText;
    SupplyModel *model = [SupplyModel new];
    model.name = searchText;
    model.page = 1;
    model.limit =10;
    self.requestModel = model;
    
}

- (S_TopSrearchView *)topView {
    if (!_topView) {
        _topView = [[S_TopSrearchView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 30)];
        
        self.navigationItem.titleView = _topView;
        //购物车按钮
        UIButton *shopCartBtn=[[UIButton alloc] initWithFrame:CGRectMake(0 ,0, 30, 30)];
        [shopCartBtn setBackgroundImage:[UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeMake(30, 30)] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shopCartBtn];
        
        
        @weakify(self);
        [_topView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            @strongify(self);
            SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
//            search.searchtype = SearchStore;
            [self.navigationController pushViewController:search animated:YES];
            
        }]];
    }
    return _topView;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if (kind == UICollectionElementKindSectionHeader) {
        S_StoreHeaderView *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        Store_SupplyModel *model;
        @try {
            model = self.collectionModel.collectionDataSource[indexPath.section];
            
        }@catch (NSException *exception){
            model = [Store_SupplyModel new];
            
        }
        advertisement.model = model;
        @weakify(self);
        [advertisement.goShop setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            @strongify(self);
            if (!model) {
                return ;
            }
            S_StoreInformation *infor = [S_StoreInformation new];
            infor.storeInfor = [NSString stringWithFormat:@"%d",model.user_id];
            [self.navigationController pushViewController:infor animated:YES];
            
        }];
        advertisement.userInteractionEnabled = YES;
        return advertisement;
    }else if (kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerView" forIndexPath:indexPath];
        view.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        return view;
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *aaa = self.collectionModel.collectionDataSource;
    Store_SupplyModel *model =  aaa[indexPath.section];
    if (model.products.count !=0  ) {
        
        S_collectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        if (model.products) {
            cell.model = model.products[indexPath.row];
        }else{
            cell.model = [Store_products new];
        }
        return cell;
    }else{
        return nil;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return self.collectionModel.collectionDataSource.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    Store_SupplyModel *model =  self.collectionModel.collectionDataSource[section];
    return model.products.count;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    Store_SupplyModel *model =  self.collectionModel.collectionDataSource[indexPath.section];
    CGFloat cellWidth = (mainWidth-24-4)/3;
    return model.products ?  CGSizeMake(cellWidth,cellWidth):CGSizeMake(mainWidth, 0.00001);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Store_SupplyModel *model =  self.collectionModel.collectionDataSource[indexPath.section];
    Store_products *cellModel = model.products[indexPath.row];
    GoodsDetailViewController *detail = [GoodsDetailViewController new];
    detail.goodsID = cellModel.product_id;
    if (isEmptyString(cellModel.product_id)) {
        return;
    }
    [self.navigationController pushViewController:detail animated:YES];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    Store_SupplyModel *model =  self.collectionModel.collectionDataSource[section];
    if (model.products.count !=0) {
        
        return UIEdgeInsetsMake(0, 12, 15, 12);
    }else{
        return UIEdgeInsetsZero;
    }
}


@end




