//
//  S_StoreMap.m
//  App3.0
//
//  Created by apple on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_StoreMap.h"

@interface S_StoreMap ()<CLLocationManagerDelegate,MKMapViewDelegate>

@property (nonatomic, strong)CLGeocoder *geoCoder;
/** 位置管理者 */
@property (nonatomic, strong) CLLocationManager *manager;

@property (nonatomic ,strong) MKMapView *mapView;

@property (nonatomic ,strong)UIButton *sendButton;
@end

@implementation S_StoreMap
{
    CLLocationCoordinate2D locationCenter;
   __block NSString *FormattedAddressLine;
}

- (CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}
#pragma mark - 懒加载
- (CLLocationManager *)manager
{
    
    if (!_manager) {
        
        _manager = [[CLLocationManager alloc] init];

        _manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            [_manager requestWhenInUseAuthorization];
        }
        
        if([[UIDevice currentDevice].systemVersion floatValue] >= 9.0){
            _manager.allowsBackgroundLocationUpdates = YES;
        }
        
        
    }
    return _manager;
}


- (UIButton *)sendButton {
    if (!_sendButton) {
    
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        [_sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_sendButton addTarget:self action:@selector(sendAddress) forControlEvents:UIControlEventTouchUpInside];
        _sendButton.frame = CGRectMake(0, 0, 60, 30);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_sendButton];
    }
    return _sendButton;
}


- (MKMapView *)mapView {
    if (!_mapView) {
        
        _mapView = [[MKMapView alloc]initWithFrame:self.view.bounds];
        
        [self.view addSubview:_mapView];
    }
    return _mapView;
}



#pragma mark - CLLocationManagerDelegate
/**
 *  更新到位置之后调用
 *
 *  @param manager   位置管理者
 *  @param locations 位置数组
 */
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"定位到了");
    
    CLLocation *location = [locations lastObject];
    CLLocationCoordinate2D coordinate = location.coordinate;
    locationCenter = coordinate;
    [manager stopUpdatingLocation];
//    [self.mapView setCenterCoordinate:coordinate animated:YES];
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000) animated:YES];
    MyAnnotation *pinAnnotation = [MyAnnotation new];
    pinAnnotation.coordinate = coordinate;
    [self.mapView addAnnotation:pinAnnotation];
}

/**
 *  授权状态发生改变时调用
 *
 *  @param manager 位置管理者
 *  @param status  状态
 */
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
            // 用户还未决定
        case kCLAuthorizationStatusNotDetermined:
        {
            NSLog(@"用户还未决定");
            break;
        }
            // 问受限
        case kCLAuthorizationStatusRestricted:
        {
            NSLog(@"访问受限");
            break;
        }
            // 定位关闭时和对此APP授权为never时调用
        case kCLAuthorizationStatusDenied:
        {
            // 定位是否可用（是否支持定位或者定位是否开启）
            if([CLLocationManager locationServicesEnabled])
            {
                NSLog(@"定位开启，但被拒");
            }else
            {
                NSLog(@"定位关闭，不可用");
            }
            //            NSLog(@"被拒");
            break;
        }
            // 获取前后台定位授权
        case kCLAuthorizationStatusAuthorizedAlways:
            //        case kCLAuthorizationStatusAuthorized: // 失效，不建议使用
        {
            NSLog(@"获取前后台定位授权");
            break;
        }
            // 获得前台定位授权
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            NSLog(@"获得前台定位授权");
            break;
        }
        default:
            break;
    }
    
    
    
}

// 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"定位失败");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.manager.delegate = self;
    self.mapView.delegate = self;
    self.sendButton.backgroundColor = [UIColor clearColor];
    
}




- (void)sendAddress{
    
    
    if (!locationCenter.latitude || !locationCenter.longitude)  return;
        
    [XSTool hideProgressHUDWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        CLLocation *loction = [[CLLocation alloc] initWithLatitude:locationCenter.latitude longitude:locationCenter.longitude];
        [self.geoCoder reverseGeocodeLocation:loction completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if (error) {NSLog(@" 反编码 - - ");return ;}
            
            for (CLPlacemark *placeMark in placemarks)
            {
                NSDictionary *addressDic=placeMark.addressDictionary;
                NSArray *state=[addressDic objectForKey:@"FormattedAddressLines"];
                FormattedAddressLine = state[0];
                NSLog(@"FormattedAddressLine  %@    - -%@",FormattedAddressLine,placeMark.name);
                
            }
            
            if (_SendeAddress) {
                _SendeAddress(locationCenter,FormattedAddressLine);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [XSTool hideProgressHUDWithView:self.view];
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }];
    });
    


}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.title = @"按住选择位置";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    
    [self.manager startUpdatingLocation];//定位位置
    self.mapView.userTrackingMode = MKUserTrackingModeNone;
    UILongPressGestureRecognizer *tap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    
    [self.mapView addGestureRecognizer:tap];
    
}

- (void)tapAction:(UILongPressGestureRecognizer  *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        
        CGPoint point = [tap locationInView:_mapView];
        locationCenter = [_mapView convertPoint:point toCoordinateFromView:_mapView];
        MyAnnotation *pinAnnotation = [MyAnnotation new];
        pinAnnotation.coordinate = locationCenter;
        [self.mapView addAnnotation:pinAnnotation];
    }
    
}

- (void)applyMapViewMemoryHotFix{
    
    switch (self.mapView.mapType) {
        case MKMapTypeHybrid:
        {
            self.mapView.mapType = MKMapTypeStandard;
        }
            
            break;
        case MKMapTypeStandard:
        {
            self.mapView.mapType = MKMapTypeHybrid;
        }
            
            break;
        default:
            break;
    }
    
    
    self.mapView.mapType = MKMapTypeStandard;
    
    
    
}



- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    locationCenter = userLocation.coordinate;
}



/**
 显示地图区域更改
 */
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    [self applyMapViewMemoryHotFix];
    MyAnnotation *pinAnnotation = [MyAnnotation new];
    pinAnnotation.coordinate = locationCenter;
    [self.mapView addAnnotation:pinAnnotation];
}
// 每次添加大头针都会调用此方法  可以设置大头针的样式
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // 判断大头针位置是否在原点,如果是则不加大头针
    if([annotation isKindOfClass:[mapView.userLocation class]])return nil;
    if ([annotation isKindOfClass:[MyAnnotation class]]) {
        
        [self.mapView removeAnnotation:annotation];
    }

    //
    return nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.mapView removeFromSuperview];
    self.mapView.delegate = nil;
    self.manager.delegate = nil;
    self.mapView = nil;
    self.manager = nil;
}


@end

@implementation MyAnnotation

@end
