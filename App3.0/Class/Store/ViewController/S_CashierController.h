//
//  S_CashierController.h
//  App3.0
//
//  Created by apple on 2017/4/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_CashierController : XSBaseViewController

@property (nonatomic,assign)NSInteger quick_percent_pay;

@property (nonatomic ,copy)NSString *showTitle;

@property (nonatomic ,copy)NSString *supply_name;

@end
