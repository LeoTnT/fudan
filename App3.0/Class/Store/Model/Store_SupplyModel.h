//
//  Store_SupplyModel.h
//  App3.0
//
//  Created by apple on 2017/12/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Store_SupplyModel:NSObject

@property (nonatomic,assign) int sell_num;

@property (nonatomic ,strong) NSArray *products;

@property (nonatomic,assign) int product_num;

@property (nonatomic,assign) int user_id;

@property (nonatomic,strong) NSString *name;

@property (nonatomic,strong) NSString *logo;

@end

@interface Store_products :NSObject

@property (nonatomic ,copy)NSString *product_id;
@property (nonatomic ,copy)NSString *image_thumb;
@property (nonatomic ,copy)NSString *sell_price;
@end



