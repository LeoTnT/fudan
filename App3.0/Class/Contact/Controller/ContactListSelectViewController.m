//
//  ContactListSelectViewController.m
//  App3.0
//
//  Created by mac on 17/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ContactListSelectViewController.h"
#import "NSString+PinYin.h"
#import "ContactModel.h"
#import "ContactSeletCell.h"
#import "ChatViewController.h"
#import "UserInstance.h"
#import "DBHandler.h"
#import "GroupSetTitleViewController.h"

@interface ContactListSelectViewController () <UISearchBarDelegate>
{
    NSMutableArray *_contactDataArr;
    NSMutableArray *_selectedArr;
    NSMutableArray *_allContactArr;
}
@property (nonatomic, strong)UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *indexs;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *totalView;
@property (nonatomic, strong) UIButton *totalButton;
@end

@implementation ContactListSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationItem.title = Localized(@"发起群聊");
    self.autoHideKeyboard = YES;
    
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 55+kTabbarSafeBottomMargin, 0));
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.sectionIndexColor = COLOR_666666;
    
    [self.view addSubview:self.searchBar];
    [self setupDataArray];
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    self.totalView.hidden = YES;
//    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"创建" action:^{
//        if (_selectedArr.count > 0) {
//            // 创建群聊
//            // 群名称
//            NSMutableArray *invitees = [NSMutableArray array];
//            NSMutableString *subject = [NSMutableString string];
//            // 先把自己的名字加进去
//            [subject appendString:[UserInstance ShardInstnce].nickName];
//            NSInteger index = 0;
//            for (ContactDataParser *parser in _selectedArr) {
//                [invitees addObject:parser.uid];
//                if (index < 2) {
//                    [subject appendString:[NSString stringWithFormat:@"、%@",parser.nickname]];
//                }
//                index++;
//            }
//            
//            // 邀请的成员拼接字符串
//            NSString *id_str = [invitees componentsJoinedByString:@","];
//            NSDictionary *groupInfo = @{@"name":subject,@"desc":@"简介",@"type":@(0),@"public":@(1),@"allowinvites":@(1),@"invite_need_confirm":@(0),@"maxusers":@(200),@"id_str":id_str};
//            GroupSetTitleViewController *stVC = [[GroupSetTitleViewController alloc] init];
//            stVC.groupOptions = groupInfo;
//            [self.navigationController pushViewController:stVC animated:YES];
//
//        } else {
//            [XSTool showToastWithView:self.view Text:@"请选择群成员"];
//        }
//        
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (UIView *)totalView {
    if (!_totalView) {
        _totalView = [[UIView alloc] init];
        _totalView.backgroundColor = [UIColor hexFloatColor:@"e3e6e8"];
        [self.view addSubview:_totalView];
        
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.backgroundColor = [UIColor hexFloatColor:@"e3e6e8"];
        [_totalView addSubview:_scrollView];
        
        _totalButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_totalButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        [_totalButton setTitleColor:mainColor forState:UIControlStateNormal];
        [_totalButton addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
        [_totalView addSubview:_totalButton];
        
        [_totalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-kTabbarSafeBottomMargin);
            make.height.mas_equalTo(55);
        }];
        [_totalButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_totalView).offset(-31);
            make.centerY.mas_equalTo(_totalView);
        }];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(_totalView);
            make.right.mas_equalTo(_totalButton.mas_left).offset(-12);
        }];
    }
    return _totalView;
}

- (void)refreshTotalView {
    for (UIView *view in self.scrollView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    UIView *lastView = nil;
    [self.scrollView setContentSize:CGSizeMake(_selectedArr.count*54.5+12, self.scrollView.frame.size.height)];
    for (ContactDataParser *parser in _selectedArr) {
        UIImageView *imgView = [[UIImageView alloc] init];
        [imgView getImageWithUrlStr:parser.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
        imgView.layer.masksToBounds = YES;
        imgView.layer.cornerRadius = 5;
        [self.scrollView addSubview:imgView];
        
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(lastView?lastView.mas_right:self.scrollView).offset(lastView?18.5:12);
            make.centerY.mas_equalTo(self.scrollView);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        lastView = imgView;
    }
    
    [self.totalButton setTitle:[NSString stringWithFormat:@"确定(%lu)",(unsigned long)_selectedArr.count] forState:UIControlStateNormal];
}

- (void)confirmAction {
    if (_selectedArr.count > 0) {
        // 创建群聊
        // 群名称
        NSMutableArray *invitees = [NSMutableArray array];
        NSMutableString *subject = [NSMutableString string];
        // 先把自己的名字加进去
        [subject appendString:[UserInstance ShardInstnce].nickName];
        NSInteger index = 0;
        for (ContactDataParser *parser in _selectedArr) {
            [invitees addObject:parser.uid];
            if (index < 2) {
                [subject appendString:[NSString stringWithFormat:@"、%@",parser.nickname]];
            }
            index++;
        }
        
        // 邀请的成员拼接字符串
        NSString *id_str = [invitees componentsJoinedByString:@","];
//        NSDictionary *groupInfo = @{@"name":subject,@"desc":@"简介",@"type":@(0),@"public":@(1),@"allowinvites":@(1),@"invite_need_confirm":@(0),@"maxusers":@(200),@"id_str":id_str};
        GroupSetTitleViewController *stVC = [[GroupSetTitleViewController alloc] init];
        RoomInfor *model = [RoomInfor new];
        model.name = subject;
        model.desc = @"";
        model.type = @"0";
        model.publicID = @"1";
        model.allowinvites = @"1";
        model.invite_need_confirm = @"0";
        model.maxusers = @"200";
        model.id_str = id_str;
        stVC.groupOptions = model;
        [self.navigationController pushViewController:stVC animated:YES];
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请选择群成员"];
    }
}

- (void)setupDataArray
{
    _selectedArr = [NSMutableArray array];
    _allContactArr = [NSMutableArray array];
    
    NSMutableArray *allArr = [[DBHandler sharedInstance] getAllContact];
    for (ContactDataParser *contact in allArr) {
        if ([contact.relation integerValue] > 0) {
            [_allContactArr addObject:contact];
        }
    }
    _contactDataArr = [[_allContactArr contactArrayWithPinYinFirstLetterFormat] mutableCopy];
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in _contactDataArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
}

#pragma mark - searchbar delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"%s",__FUNCTION__);
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    for (NSIndexPath *indexPath in [self.tableView indexPathsForVisibleRows]) {
        ContactSeletCell *cell = (ContactSeletCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.isSelect = NO;
    }
    [_contactDataArr removeAllObjects];
    if (searchText.length > 0) {
        NSMutableArray *tempArr = [NSMutableArray array];
        for (ContactDataParser *model in _allContactArr) {
            if ([[model getName] containsString:searchText]) {
                [tempArr addObject:model];
            }
        }
        [_contactDataArr addObjectsFromArray:[tempArr contactArrayWithPinYinFirstLetterFormat]];
    } else {
        [_contactDataArr addObjectsFromArray:[_allContactArr contactArrayWithPinYinFirstLetterFormat]];
    }
    [self.tableView reloadData];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _contactDataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *contentArr = [_contactDataArr[section] objectForKey:@"content"];
    return contentArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 90, 22)];
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.textColor = mainGrayColor;
    
    NSString *title = _contactDataArr[section][@"firstLetter"];
    titleLabel.text=title;
    [myView  addSubview:titleLabel];
    
    return myView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count;
        }
        count++;
    }
    return  0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.indexs objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    NSArray *contentArr = [_contactDataArr[indexPath.section] objectForKey:@"content"];
    ContactDataParser *parser = contentArr[indexPath.row];
    ContactSeletCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.isSelect = !cell.isSelect;
    if (cell.isSelect) {
        [_selectedArr addObject:parser];
    } else {
        [_selectedArr removeObject:parser];
    }
    NSLog(@"%lu",(unsigned long)_selectedArr.count);
    if (_selectedArr.count == 0) {
        self.totalView.hidden = YES;
    } else {
        self.totalView.hidden = NO;
        [self refreshTotalView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"ContactSelect%ld ",(long)indexPath.row];
    ContactSeletCell *cell = (ContactSeletCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[ContactSeletCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSArray *contentArr = [_contactDataArr[indexPath.section] objectForKey:@"content"];
    ContactDataParser *parser = contentArr[indexPath.row];
    [cell setCellData:parser];
    cell.isSelect = NO;
    for (ContactDataParser *parser1 in _selectedArr) {
        if ([parser.uid isEqualToString:parser1.uid]) {
            cell.isSelect = YES;
        }
    }
    return cell;
}

- (UISearchBar *)searchBar
{
    if (_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.placeholder = Localized(@"search");
        _searchBar.translucent = YES; // 设置是否透明
        // 去掉灰色边框
        for (UIView *view in self.searchBar.subviews) {
            if ([view isKindOfClass:NSClassFromString(@"UIView")]&&view.subviews.count>0) {
                view.backgroundColor = BG_COLOR;
                [[view.subviews objectAtIndex:0] removeFromSuperview];
                break;
            }
        }
    }
    return _searchBar;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
@end
