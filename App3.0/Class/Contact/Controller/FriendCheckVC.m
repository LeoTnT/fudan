//
//  FriendCheckVC.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FriendCheckVC.h"
#import <Contacts/Contacts.h>
#import <MessageUI/MessageUI.h>
#import "ContactModel.h"
#import "NSString+PinYin.h"
#import "EmptyView.h"
#import "FriendCheckCell.h"
#import "XSShareView.h"
#import "XSFormatterDate.h"

@interface FriendCheckVC () <MFMessageComposeViewControllerDelegate,UINavigationControllerDelegate, FriendCheckCellDelegate>
{
    BOOL _checkFinished;
}
@property (strong, nonatomic) NSMutableArray *sortArray;
@property (strong, nonatomic) NSMutableArray *contactArr;   // 存放所有联系人
@property (strong, nonatomic) NSMutableArray *fansArr;      // 存放已注册未关注的联系人
@property (strong, nonatomic) NSMutableArray *indexs;
@property (strong, nonatomic) EmptyView *emptyView; // 空页面
@end

@implementation FriendCheckVC

- (NSMutableArray *)sortArray {
    if (!_sortArray) {
        _sortArray = [NSMutableArray array];
    }
    return _sortArray;
}

- (NSMutableArray *)contactArr {
    if (!_contactArr) {
        _contactArr = [NSMutableArray array];
    }
    return _contactArr;
}

- (NSMutableArray *)fansArr {
    if (!_fansArr) {
        _fansArr = [NSMutableArray array];
    }
    return _fansArr;
}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (EmptyView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleContact frame:self.view.bounds];
    }
    return _emptyView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationItem.title = Localized(@"通讯录好友");
    self.view.backgroundColor = [UIColor whiteColor];
    _checkFinished = NO;
    
    [self checkContact];
    self.tableViewStyle = UITableViewStyleGrouped;
    
    [self.view addSubview:self.emptyView];
    self.emptyView.hidden = YES;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)checkContact
{
    [self.contactArr removeAllObjects];
    [self.fansArr removeAllObjects];
    [self.sortArray removeAllObjects];
    [XSTool showProgressHUDTOView:self.view withText:@"获取中"];
    @weakify(self);
    // 判断授权状态
    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusNotDetermined) {
        
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            @strongify(self);
            if (granted) {
                NSLog(@"授权成功");
                
                //获取联系人仓库
//                CNContactStore *store = [[CNContactStore alloc] init];
                
                //创建联系人信息的请求对象
                NSArray *keys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactOrganizationNameKey];
                
                //根据请求key，创建请求对象
                CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
                
                //发送请求
                [store enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
                    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                    // 获取姓名
                    NSString *givenName = contact.givenName;
                    NSString *familyName = contact.familyName;
                    NSString *orgName = contact.organizationName;
                    NSLog(@"%@-%@",givenName,familyName);
                    NSString *name = [NSString stringWithFormat:@"%@%@%@",familyName,givenName,orgName];
                    [dic setValue:name forKey:@"name"];
                    //获取电话
                    NSMutableArray *phoneNumArr = [NSMutableArray array];
                    NSArray *phoneArray = contact.phoneNumbers;
                    for (CNLabeledValue *labelValue in phoneArray) {
                        CNPhoneNumber *number = labelValue.value;
                        NSLog(@"%@--%@",number.stringValue,labelValue.label);
                        [phoneNumArr addObject:number.stringValue];
                        
                    }
                    [dic setValue:phoneNumArr forKey:@"phone"];
                    [self.contactArr addObject:dic];
                    
                }];
                _checkFinished = YES;
                [HTTPManager getDeviceFansWithContents:self.contactArr success:^(NSDictionary * dic, resultObject *state) {
                    @strongify(self);
                    DeviceFansParser *parser = [DeviceFansParser mj_objectWithKeyValues:dic];
                    NSLog(@"返回数据：%@",parser);
                    if (state.status) {
                        [self.fansArr addObjectsFromArray:parser.data.notFans];
                    }
                    NSMutableArray *tempArr = [NSMutableArray array];
                    for (int i = 0; i < self.contactArr.count; i++) {
                        NSDictionary *mDic = self.contactArr[i];
                        for (int j = 0; j < parser.data.notFans.count; j++) {
                            DeviceFansDetailParser *dfdParser = parser.data.notFans[j];
                            if ([mDic[@"name"] isEqualToString:dfdParser.name]) {
                                [tempArr addObject:mDic];
                                break;
                            }
                        }
                    }
                    for (int i = 0; i < self.contactArr.count; i++) {
                        NSDictionary *mDic = self.contactArr[i];
                        for (int j = 0; j < parser.data.isFans.count; j++) {
                            DeviceFansDetailParser *dfdParser = parser.data.isFans[j];
                            if ([mDic[@"name"] isEqualToString:dfdParser.name]) {
                                [tempArr addObject:mDic];
                                break;
                            }
                        }
                    }
                    for (NSDictionary *mDic in tempArr) {
                        [self.contactArr removeObject:mDic];
                    }
                    [self sortContactList:self.contactArr];
                    if (self.contactArr.count == 0) {
                        self.emptyView.hidden = NO;
                    } else {
                        self.emptyView.hidden = YES;
                    }
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.tableView reloadData];
                } fail:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                }];
            }
            else {
                NSLog(@"授权失败");
                self.emptyView.hidden = NO;
            }
        }];
    }
    else if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized) {
        //获取联系人仓库
        CNContactStore *store = [[CNContactStore alloc] init];
        
        //创建联系人信息的请求对象
        NSArray *keys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactOrganizationNameKey];
        
        //根据请求key，创建请求对象
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        
        //发送请求
        [store enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            @strongify(self);
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            // 获取姓名
            NSString *givenName = contact.givenName;
            NSString *familyName = contact.familyName;
            NSString *orgName = contact.organizationName;
            NSString *name = [NSString stringWithFormat:@"%@%@%@",familyName,givenName,orgName];
            [dic setValue:name forKey:@"name"];
            //获取电话
            NSMutableArray *phoneNumArr = [NSMutableArray array];
            NSArray *phoneArray = contact.phoneNumbers;
            for (CNLabeledValue *labelValue in phoneArray) {
                CNPhoneNumber *number = labelValue.value;
                [phoneNumArr addObject:number.stringValue];
            }
            [dic setValue:phoneNumArr forKey:@"phone"];
            
            [self.contactArr addObject:dic];
            
        }];
        _checkFinished = YES;

        [HTTPManager getDeviceFansWithContents:self.contactArr success:^(NSDictionary * dic, resultObject *state) {
            @strongify(self);
            DeviceFansParser *parser = [DeviceFansParser mj_objectWithKeyValues:dic];
            if (state.status) {
                [self.fansArr addObjectsFromArray:parser.data.notFans];
            }
            NSMutableArray *tempArr = [NSMutableArray array];
            for (int i = 0; i < self.contactArr.count; i++) {
                NSDictionary *mDic = self.contactArr[i];
                for (int j = 0; j < parser.data.notFans.count; j++) {
                    DeviceFansDetailParser *dfdParser = parser.data.notFans[j];
                    if ([mDic[@"name"] isEqualToString:dfdParser.name]) {
                        [tempArr addObject:mDic];
                    }
                }
            }
            for (int i = 0; i < self.contactArr.count; i++) {
                NSDictionary *mDic = self.contactArr[i];
                for (int j = 0; j < parser.data.isFans.count; j++) {
                    DeviceFansDetailParser *dfdParser = parser.data.isFans[j];
                    if ([mDic[@"name"] isEqualToString:dfdParser.name]) {
                        NSLog(@"%@====%@",mDic[@"name"],dfdParser.name);
                        [tempArr addObject:mDic];
                    }
                }
            }
            for (NSDictionary *mDic in tempArr) {
                [self.contactArr removeObject:mDic];
            }
            [self sortContactList:self.contactArr];
            if (self.contactArr.count == 0) {
                self.emptyView.hidden = NO;
            } else {
                self.emptyView.hidden = YES;
            }
            [XSTool hideProgressHUDWithView:self.view];
            [self.tableView reloadData];
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
        }];
    }
    
}

- (void)sortContactList:(NSArray *)contacts {
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSDictionary *dic in contacts) {
        [tempArr addObject:dic[@"name"]];
        
    }
    NSArray *resultArr = [tempArr arrayWithPinYinFirstLetterFormat];
    
    // 对比字符串找出对应contactmodel来替代字符串
    for (int i = 0; i < resultArr.count; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:resultArr[i][@"firstLetter"] forKey:@"firstLetter"];
        NSMutableArray *arr = [NSMutableArray array];
        for (NSString *string in resultArr[i][@"content"]) {
            for (NSDictionary *dic in contacts) {
                if ([string isEqualToString:dic[@"name"]]) {
                    [arr addObject:dic];
                    break;
                }
            }
            [dic setObject:arr forKey:@"content"];
        }
        [self.sortArray addObject:dic];
    }
    
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in resultArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
}

#pragma mark - FriendCheckCellDelegate
- (void)attationClick:(DeviceFansDetailParser *)contact {
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager attentionFansWithUid:contact.uid success:^(NSDictionary * dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self checkContact];
            
            // 刷新联系人列表
            [[ChatHelper shareHelper].friendVC getRelationsData];
            
            // 发送透传消息
            NSDictionary *cmdExtDic = @{MSG_TYPE:MESSAGE_RELATION_CHANGE};
#ifdef ALIYM_AVALABLE
            YWMessageBodyCustomize *cmdBody = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:cmdExtDic.mj_JSONString summary:MESSAGE_RELATION_CHANGE isTransparent:YES];
            YWPerson *person = [[YWPerson alloc] initWithPersonId:contact.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            [conversation asyncSendMessageBody:cmdBody progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                
            }];
#elif defined EMIM_AVALABLE
            EMCmdMessageBody *cmdBody = [[EMCmdMessageBody alloc] initWithAction:MESSAGE_RELATION_CHANGE];
            NSString *from = [[EMClient sharedClient] currentUsername];
            
            // 生成message
            EMMessage *cmdMessage = [[EMMessage alloc] initWithConversationID:contact.uid from:from to:contact.uid body:cmdBody ext:cmdExtDic];
            cmdMessage.chatType = EMChatTypeChat;// 设置消息类型
            //发送消息
            [[EMClient sharedClient].chatManager sendMessage:cmdMessage progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                
            }];
#else
            [XMPPAddFriendsManager addBuddyWithJid:contact.uid];
#endif
            
            // 插入时间消息
            NSString *text = [XSFormatterDate currentTime];
            NSDictionary *extDic = @{MSG_TYPE:MESSAGE_FOCUS_TYPE};
#ifdef ALIYM_AVALABLE
            YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
            NSDictionary *controlParameters = @{kYWMsgCtrlKeyClientLocal:@(1)};
            [conversation asyncSendMessageBody:body controlParameters:controlParameters progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                
            }];
            
            text = [NSString stringWithFormat:@"你已关注了%@，快来聊天吧。",contact.nickname];
            extDic = @{MSG_TYPE:MESSAGE_FOCUS_TYPE};
            body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
            [conversation asyncSendMessageBody:body controlParameters:controlParameters progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                
            }];
#elif defined EMIM_AVALABLE
            EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:contact.uid type:EMConversationTypeChat createIfNotExist:YES];
            // 生成message
            EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
            EMMessage *message = [[EMMessage alloc] initWithConversationID:contact.uid from:[UserInstance ShardInstnce].uid to:contact.uid body:body ext:extDic];
            message.chatType = EMChatTypeChat;// 设置消息类型
            [conver insertMessage:message error:nil];
            
            // 插入关注消息
            text = [NSString stringWithFormat:@"你已关注了%@，快来聊天吧。",contact.nickname];
            body = [[EMTextMessageBody alloc] initWithText:text];
            extDic = @{MSG_TYPE:MESSAGE_FOCUS_TYPE};
            // 生成message
            message = [[EMMessage alloc] initWithConversationID:contact.uid from:[UserInstance ShardInstnce].uid to:contact.uid body:body ext:extDic];
            message.chatType = EMChatTypeChat;// 设置消息类型
            [conver insertMessage:message error:nil];
#else
            
#endif
            
        }
        [XSTool hideProgressHUDWithView:self.view];
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
        [XSTool hideProgressHUDWithView:self.view];
    }];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sortArray.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.fansArr.count;
    }
    NSArray *contentArr = [self.sortArray[section-1] objectForKey:@"content"];
    return contentArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 10;
    }
    return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = COLOR_666666;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = self.sortArray[section-1][@"firstLetter"];
    titleLabel.text=title;
    [myView  addSubview:titleLabel];
    
    return myView;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count+1;
        }
        count++;
    }
    return 0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }
    return [self.indexs objectAtIndex:section-1];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return;
    }
    NSArray *contentArr = [self.sortArray[indexPath.section-1] objectForKey:@"content"];
    NSDictionary *phoneDic = contentArr[indexPath.row];
    if (phoneDic && [phoneDic valueForKey:@"phone"]) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getShareInfo:@{@"type":@"app",@"target":@"ios"} Succrss:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                XSShareModel *shareModel = [XSShareModel mj_objectWithKeyValues:dic[@"data"]];
                [self sendMessage:shareModel.link withPhoneNum:[phoneDic valueForKey:@"phone"]];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
        
    } else {
        [XSTool showToastWithView:self.view Text:@"暂不支持此功能"];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chatCell1";
    FriendCheckCell *cell = (FriendCheckCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[FriendCheckCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.delegate = self;
    }
    if (indexPath.section == 0) {
        [cell setAttationData:self.fansArr[indexPath.row]];
    } else {
        NSArray *contentArr = [self.sortArray[indexPath.section-1] objectForKey:@"content"];
        NSDictionary *dic = contentArr[indexPath.row];
        [cell setCellData:dic];
    }
    return cell;
}

-(void)sendMessage:(NSString *)message withPhoneNum:(NSArray *)phoneNum
{
    if (phoneNum.count == 0) {
        [XSTool showToastWithView:self.view Text:@"请选择正确的联系人"];
        return;
    }
    if ([MFMessageComposeViewController canSendText]) {
        //实例化MFMessageComposeViewController,并设置委托
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        
        messageController.delegate = self;
        messageController.messageComposeDelegate = self;
        
        //设置短信内容
        messageController.body = message;
        
        //设置发送给谁
        NSString *phoneString = phoneNum[0];
        messageController.recipients = @[phoneString];
        NSLog(@"%@----%@",messageController.body,messageController.recipients);
        //推到发送试图控制器
        [self presentViewController:messageController animated:YES completion:^{
            
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                        message:@"该设备不支持短信功能"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
        [alert show];
    }
    
    
}


//发送短信后回调的方法
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSString *tipContent;
    
    switch (result) {
        case MessageComposeResultCancelled:
            tipContent = @"发送短信已取消";
            break;
            
        case MessageComposeResultFailed:
            tipContent = @"发送短信失败";
            break;
            
        case MessageComposeResultSent:
            tipContent = @"发送成功";
            break;
            
        default:
            break;
    }
    
    UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:Localized(@"prompt") message:tipContent delegate:nil cancelButtonTitle:Localized(@"cancel_btn") otherButtonTitles:nil];
    [alterView show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
