//
//  FriendsVC.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FriendsVC.h"
#import "AttentionCell.h"
#import "SearchVC.h"
#import "FriendCheckVC.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "EmptyView.h"
#import "AddFriendsViewController.h"
#import "FansVC.h"
#import "AttentionVC.h"
#import "GroupChatVC.h"

@interface FriendsVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_friendsListArr;
    EmptyView *_emptyView;
}
@property (strong, nonatomic) UITableView *tableView;
@property(nonatomic, strong)UIButton *searchBtn;
@property (strong, nonatomic) NSMutableArray *indexs;
@property (strong, nonatomic) NSArray *titleArray;
@end

@implementation FriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _friendsListArr = [NSMutableArray array];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.searchBtn];
    
    // 添加空页面
    _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleContact frame:self.view.bounds];
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    self.titleArray = @[@{@"image":@"chat_friend",@"title":@"新的朋友"},
                        @{@"image":@"chat_attention",@"title":@"关注"},
                        @{@"image":@"chat_fans",@"title":@"粉丝"},
                        @{@"image":@"chat_group",@"title":@"群组"}];
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (NSArray *)sortedArray:(NSArray *)array {
    // 首字母排序
    NSArray *resultArr = [array contactArrayWithPinYinFirstLetterFormat];
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in resultArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
    return resultArr;
}

- (void)reloadData {
    if (_friendsListArr.count > 0) {
        [_friendsListArr removeAllObjects];
    }
    if (self.indexs.count > 0) {
        [self.indexs removeAllObjects];
    }
    // 从本地数据库拿数据
    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myFriends = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
        if ([parser.relation integerValue] == ContactRelationFriend) {
            [myFriends addObject:parser];
        }
    }
    if (myFriends.count > 0) {
        [_friendsListArr addObjectsFromArray:[self sortedArray:myFriends]];
    }
    [self.tableView reloadData];
//    if (_friendsListArr.count == 0) {
//        _emptyView.hidden = NO;
//    } else {
//        _emptyView.hidden = YES;
//    }
}

- (void)checkContacts
{
    if ([ChatHelper shareHelper].transVC) {
        return;
    }
    FriendCheckVC *check = [[FriendCheckVC alloc] init];
    check.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:check animated:YES];
}

- (void)popView {
    [ChatHelper shareHelper].isSendingCard = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        return _friendsListArr.count;
    }
    return 1+_friendsListArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        NSArray *arr = _friendsListArr[section][@"content"];
        return arr.count;
    }
    if (section == 0) {
        return self.titleArray.count;
    }
    NSArray *arr = _friendsListArr[section-1][@"content"];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        return 22;
    }
    if (section == 0) {
        return 0.1;
    }
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        //自定义Header标题
        UIView* myView = [[UIView alloc] init];
        myView.backgroundColor = BG_COLOR;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
        titleLabel.textColor = mainGrayColor;
        titleLabel.font = [UIFont systemFontOfSize:12];
        
        NSString *title = _friendsListArr[section][@"firstLetter"];
        titleLabel.text=title;
        [myView addSubview:titleLabel];
        
        return myView;
    }
    if (section == 0) {
        return nil;
    }
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = COLOR_666666;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = _friendsListArr[section-1][@"firstLetter"];
    titleLabel.text=title;
    [myView addSubview:titleLabel];
    
    return myView;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (_friendsListArr.count < 20) {
        return nil;
    }
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count+1;
        }
        count++;
    }
    return  0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        return [self.indexs objectAtIndex:section];
    }
    if (section == 0) {
        return nil;
    }
    return [self.indexs objectAtIndex:section-1];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
#ifdef ALIYM_AVALABLE
    
#elif defined EMIM_AVALABLE
    if ([ChatHelper shareHelper].transVC) {
        ContactDataParser *contact = _friendsListArr[indexPath.section][@"content"][indexPath.row];
        NSLog(@"%@",contact.username);
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:contact.uid type:EMConversationTypeChat createIfNotExist:YES];
        
        //生成Message
        NSString *from = [[EMClient sharedClient] currentUsername];
        EMMessage *emMessgae = [ChatHelper shareHelper].transVC.message;
        EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:from to:conversation.conversationId body:emMessgae.body ext:emMessgae.ext];
        message.chatType = (EMChatType)conversation.type;
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
            });
        }];
        return;
    }
    if ([ChatHelper shareHelper].isSendingCard) {
        // 发送名片
        ContactDataParser *contact = _friendsListArr[indexPath.section][@"content"][indexPath.row];
        EMConversation *conversation = [ChatHelper shareHelper].chatVC.conversation;
        // 拓展
        NSDictionary *extDic = @{MSG_TYPE:MESSAGE_SHARE_USER,MESSAGE_SHARE_USER_ICO:contact.avatar,MESSAGE_SHARE_USER_ID:contact.uid};
        
        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:contact.nickname];
        // 生成message
        EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:[UserInstance ShardInstnce].uid to:conversation.conversationId body:body ext:extDic];
        message.chatType = (EMChatType)conversation.type;// 设置消息类型
        
        @weakify(self);
        //发送消息
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            @strongify(self);
            [self performSelector:@selector(popView) withObject:nil afterDelay:0];
        }];
        return;
    }
#else
    
   
#endif
    
    if (indexPath.section == 0) {
        NSString *title = self.titleArray[indexPath.row][@"title"];
        if ([title isEqualToString:@"新的朋友"]) {
            AddFriendsViewController *check = [[AddFriendsViewController alloc] init];
            [self.navigationController pushViewController:check animated:YES];
        } else if ([title isEqualToString:@"关注"]) {
            AttentionVC *check = [[AttentionVC alloc] init];
            [self.navigationController pushViewController:check animated:YES];
        } else if ([title isEqualToString:@"粉丝"]) {
            FansVC *check = [[FansVC alloc] init];
            [self.navigationController pushViewController:check animated:YES];
        } else if ([title isEqualToString:@"群组"]) {
            GroupChatVC *check = [[GroupChatVC alloc] init];
            [self.navigationController pushViewController:check animated:YES];
        }
    } else if (indexPath.section < 1+_friendsListArr.count) {
        ContactDataParser *contact = _friendsListArr[indexPath.section-1][@"content"][indexPath.row];
        NSLog(@"%@",contact.username);

#ifdef ALIYM_AVALABLE
        
#elif defined EMIM_AVALABLE
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:contact.uid type:EMConversationTypeChat createIfNotExist:YES];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
        chatVC.title = [contact getName];
        chatVC.avatarUrl = contact.avatar;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#else
        
#endif
        
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([ChatHelper shareHelper].transVC || [ChatHelper shareHelper].isSendingCard) {
        static NSString *CellIdentifier = @"Cell";
        AttentionCell *cell = (AttentionCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[AttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if (_friendsListArr.count > 0) {
            ContactDataParser *data = _friendsListArr[indexPath.section][@"content"][indexPath.row];
            [cell setCellData:data];
        }
        
        return cell;
    }
    if (indexPath.section == 0) {
        static NSString *cellIdentifier = @"cell0";
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            UIImageView *avatar = [[UIImageView alloc] initWithFrame:CGRectMake(12, 9.5, 36, 36)];
//            avatar.layer.cornerRadius = 5;
//            avatar.layer.masksToBounds = YES;
            [cell.contentView addSubview:avatar];
            
            UILabel *title = [[UILabel alloc] init];
            title.font = [UIFont systemFontOfSize:16];
            [cell.contentView addSubview:title];
            [title mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell);
                make.left.mas_equalTo(avatar.mas_right).offset(8);
            }];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            NSDictionary *dic = self.titleArray[indexPath.row];
            avatar.image = [UIImage imageNamed:dic[@"image"]];
            title.text = dic[@"title"];
            if (indexPath.row < self.titleArray.count-1) {

                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 54.5, mainWidth-12, 0.5)];
                lineView.backgroundColor = LINE_COLOR_NORMAL;
                [cell.contentView addSubview:lineView];
            }
        }
        return cell;
    }
    static NSString *CellIdentifier = @"Cell";
    AttentionCell *cell = (AttentionCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[AttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (_friendsListArr.count > 0) {
        ContactDataParser *data = _friendsListArr[indexPath.section-1][@"content"][indexPath.row];
        [cell setCellData:data];
    }
    
    return cell;
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}


- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, mainWidth, self.view.frame.size.height-50) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = BG_COLOR;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.sectionIndexColor = COLOR_666666;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 9, mainWidth-24, 32)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"搜索联系人") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 5;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -2.5, 0, 0)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2.5, 0, 0)];
    }
    return _searchBtn;
}
@end
