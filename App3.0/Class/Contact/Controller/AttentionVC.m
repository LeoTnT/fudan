//
//  AttentionVC.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AttentionVC.h"
#import "AttentionCell.h"
#import "SearchVC.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "EmptyView.h"

@interface AttentionVC () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_attentionListArr;
    EmptyView *_emptyView;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *indexs;
@property(nonatomic, strong)UIButton *searchBtn;
@end

@implementation AttentionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"关注";
    
    _attentionListArr = [NSMutableArray array];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.searchBtn];
    
    __weak typeof (self) wSelf = self;
    // 下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [wSelf.tableView reloadData];
        [wSelf.tableView.mj_header endRefreshing];
    }];
    
    // 添加空页面
    _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleContact frame:self.view.bounds];
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (NSArray *)sortedArray:(NSArray *)array {
    // 首字母排序
    NSArray *resultArr = [array contactArrayWithPinYinFirstLetterFormat];
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in resultArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
    return resultArr;
}

- (void)reloadData {
    if (_attentionListArr.count > 0) {
        [_attentionListArr removeAllObjects];
    }
    if (self.indexs.count > 0) {
        [self.indexs removeAllObjects];
    }
    // 从本地数据库拿数据
    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myAttentions = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
        if ([parser.relation integerValue] == ContactRelationAttention) {
            [myAttentions addObject:parser];
        }
    }
    if (myAttentions.count > 0) {
        [_attentionListArr addObjectsFromArray:[self sortedArray:myAttentions]];
    }
    [self.tableView reloadData];
    if (_attentionListArr.count == 0) {
        _emptyView.hidden = NO;
    } else {
        _emptyView.hidden = YES;
    }
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _attentionListArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = _attentionListArr[section][@"content"];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = COLOR_666666;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = _attentionListArr[section][@"firstLetter"];
    titleLabel.text=title;
    [myView addSubview:titleLabel];
    
    return myView;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (_attentionListArr.count < 20) {
        return nil;
    }
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count;
        }
        count++;
    }
    return  0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.indexs objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
#ifdef ALIYM_AVALABLE
    ContactDataParser *contact = _attentionListArr[indexPath.section][@"content"][indexPath.row];
    YWPerson *person = [[YWPerson alloc] initWithPersonId:contact.uid];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    if ([ChatHelper shareHelper].transVC) {
        //生成Message
        if ([ChatHelper shareHelper].transVC.isShare) {
            // 分享的是一个messagebody
            YWMessageBodyCustomize *body = [ChatHelper shareHelper].transVC.message;
            [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
                });
            }];
        } else {
            id<IYWMessage> ywMessgae = [ChatHelper shareHelper].transVC.message;
            [conversation asyncForwardMessage:ywMessgae progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
                });
            }];
        }
        

        return;
    }
    if ([ChatHelper shareHelper].isSendingCard) {
        // 发送名片
        // 拓展
        NSDictionary *extDic = @{MSG_TYPE:MESSAGE_SHARE_USER,MESSAGE_SHARE_USER_ICO:contact.avatar,MESSAGE_SHARE_USER_ID:contact.uid};
        YWConversation *sConversation = [SPKitExample sharedInstance].chatVC.conversation;
        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:contact.nickname];
        
        @weakify(self);
        //发送消息
        [sConversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
            
        } completion:^(NSError *error, NSString *messageID) {
            @strongify(self);
            [ChatHelper shareHelper].isSendingCard = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }];

        return;
    }
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = [contact getName];
    chatVC.avatarUrl = contact.avatar;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    ContactDataParser *contact = _attentionListArr[indexPath.section][@"content"][indexPath.row];
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:contact.uid type:EMConversationTypeChat createIfNotExist:YES];
    if ([ChatHelper shareHelper].transVC) {
        //生成Message
        NSString *from = [[EMClient sharedClient] currentUsername];
        EMMessage *emMessgae = [ChatHelper shareHelper].transVC.message;
        EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:from to:conversation.conversationId body:emMessgae.body ext:emMessgae.ext];
        message.chatType = (EMChatType)conversation.type;
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
            });
        }];
        return;
    }
    if ([ChatHelper shareHelper].isSendingCard) {
        // 发送名片
        // 拓展
        NSDictionary *extDic = @{MSG_TYPE:MESSAGE_SHARE_USER,MESSAGE_SHARE_USER_ICO:contact.avatar,MESSAGE_SHARE_USER_ID:contact.uid};
        EMConversation *conversation = [ChatHelper shareHelper].chatVC.conversation;
        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:contact.nickname];
        // 生成message
        EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:[UserInstance ShardInstnce].uid to:conversation.conversationId body:body ext:extDic];
        message.chatType = (EMChatType)conversation.type;// 设置消息类型
        
        @weakify(self);
        //发送消息
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            @strongify(self);
            [ChatHelper shareHelper].isSendingCard = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        return;
    }
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.title = [contact getName];
    chatVC.avatarUrl = contact.avatar;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    ContactDataParser *contact = _attentionListArr[indexPath.section][@"content"][indexPath.row];
    
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:contact.uid title:[contact getName] avatarURLPath:contact.avatar];
    ConversationModel *model = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    
    if ([ChatHelper shareHelper].transVC) {
        //生成Message
        [XMPPSignal shareXM_SignalModel:[ChatHelper shareHelper].transVC.transModel contact:model finash:^(NSString *title) {
            [MBProgressHUD showMessage:@"分享成功" view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
        return;
    }
    XMChatController *chatVC = [XMChatController new];
    chatVC.conversationModel = model;
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"AttentionCell%ld ",(long)indexPath.row];
    AttentionCell *cell = (AttentionCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[AttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (_attentionListArr.count > 0) {
        [cell setCellData:_attentionListArr[indexPath.section][@"content"][indexPath.row]];
    }
    
    return cell;
}


- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, mainWidth, self.view.frame.size.height-50) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = BG_COLOR;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.sectionIndexColor = COLOR_666666;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 9, mainWidth-24, 32)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"搜索联系人") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 5;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -2.5, 0, 0)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2.5, 0, 0)];
    }
    return _searchBtn;
}
@end
