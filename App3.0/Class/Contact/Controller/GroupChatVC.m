//
//  GroupChatVC.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupChatVC.h"
#import "GroupChatCell.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "EmptyView.h"
#import "XMGroupChatController.h"

@interface GroupChatVC () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_groupListArr;
    EmptyView *_emptyView;
}
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation GroupChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"群组";
    
    _groupListArr = [NSMutableArray array];
    [self.view addSubview:self.tableView];
    
    // 添加空页面
    _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleGroup frame:self.view.bounds];
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    [self getGroupListData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (NSArray *)sortedArray:(NSArray *)array {
    NSArray* sorted = [array sortedArrayUsingComparator:^NSComparisonResult(GroupDataModel *obj1, GroupDataModel *obj2) {
        if (NSOrderedDescending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (NSOrderedAscending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return sorted;
}

- (void)getGroupListData
{
    if (_groupListArr.count > 0) {
        [_groupListArr removeAllObjects];
    }
    
    // 从本地数据库拿数据
    NSArray *dbArr = [[DBHandler sharedInstance] getAllGroup];
    NSArray* sorted = [self sortedArray:dbArr];
    if (sorted.count > 0) {
        [_groupListArr addObjectsFromArray:sorted];
        [self.tableView reloadData];
    }
    if (_groupListArr.count == 0) {
        _emptyView.hidden = NO;
    } else {
        _emptyView.hidden = YES;
    }
    
    __weak typeof(self) wSelf = self;
    // 使用新接口，从环信获取群组列表放在后台操作
    [HTTPManager getMyGroupListWithSuccess:^(NSDictionary * dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            GroupModel *dataModel = [GroupModel mj_objectWithKeyValues:dic];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSMutableArray *tempArr = [NSMutableArray array];
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:_groupListArr];
                for (GroupDataModel *model in dataModel.data) {
                    [tempArr addObject:model];
                    // 加入数据库
                    [[DBHandler sharedInstance] addOrUpdateGroup:model];
                    
                    // 数据库排除现有群组，剩下的就是将要删除的
                    for (GroupDataModel *oldParser in _groupListArr) {
                        if ([oldParser.gid isEqualToString:model.gid]) {
                            [deleteArr removeObject:oldParser];
                        }
                    }
                }
                
                // 数据库删除不存在的群组
                for (GroupDataModel *delParser in deleteArr) {
                    [[DBHandler sharedInstance] deleteGroupByGroupId:delParser.gid];
                }
                
                [_groupListArr removeAllObjects];
                [_groupListArr addObjectsFromArray:[self sortedArray:tempArr]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_groupListArr.count == 0) {
                        _emptyView.hidden = NO;
                    } else {
                        _emptyView.hidden = YES;
                    }
                    [wSelf.tableView reloadData];
                });
            });
        } else {
            
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
    }];
    
}

- (void)newGroupChat
{
    
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _groupListArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupDataModel *group = _groupListArr[indexPath.row];
#ifdef ALIYM_AVALABLE
    YWTribe *tribe = [YWTribe new];
    tribe.tribeId = group.gid;
    YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    if ([ChatHelper shareHelper].transVC) {
        //生成Message
        if ([ChatHelper shareHelper].transVC.isShare) {
            // 分享的是一个messagebody
            YWMessageBodyCustomize *body = [ChatHelper shareHelper].transVC.message;
            [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
                });
            }];
        } else {
            id<IYWMessage> ywMessgae = [ChatHelper shareHelper].transVC.message;
            [conversation asyncForwardMessage:ywMessgae progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
                });
            }];
        }
        
        return;
    }
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = group.name;
    chatVC.avatarUrl = group.avatar;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:group.gid type:EMConversationTypeGroupChat createIfNotExist:YES];
    if ([ChatHelper shareHelper].transVC) {
        //生成Message
        NSString *from = [[EMClient sharedClient] currentUsername];
        EMMessage *emMessgae = [ChatHelper shareHelper].transVC.message;
        EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:from to:conversation.conversationId body:emMessgae.body ext:emMessgae.ext];
        message.chatType = (EMChatType)conversation.type;
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
            });
        }];
        return;
    }
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.title = group.name;
    chatVC.avatarUrl = group.avatar;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    XMPPRoomManager *model = [XMPPSignal roomConfigWithRoomName:group.gid];
    if (!model) return;
    ConversationModel *conversation = [[ConversationModel alloc] initWithConversation:model];
    conversation.title = group.name;
    
    if ([ChatHelper shareHelper].transVC) {
        ConversationModel *conversionModel = [XMPPSignal roomConfigWithConversation:conversation];
        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
        message.type = @"txt";
        message.body = [ChatHelper shareHelper].transVC.transModel.messageText;
        message.ext = [ChatHelper shareHelper].transVC.transModel.extDic.mj_JSONString;
        NSString *messageString = message.mj_JSONString;
        [ChatHelper shareHelper].transVC.transModel.messageText = messageString;
        [XMPPSignal shareXM_GroupModel:[ChatHelper shareHelper].transVC.transModel contact:conversionModel finash:^(NSString *title) {
            [MBProgressHUD showMessage:@"分享成功" view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
        return;
    }
    
    XMGroupChatController *vc = [[XMGroupChatController alloc] initWithRoomJID:conversation];
    [self.navigationController pushViewController:vc animated:YES];
#endif
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"groupCell%ld",(long)indexPath.row];
    GroupChatCell *cell = (GroupChatCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[GroupChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    [cell setCellData:_groupListArr[indexPath.row]];

    return cell;
}


- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.view.frame.size.height) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = BG_COLOR;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}
@end
