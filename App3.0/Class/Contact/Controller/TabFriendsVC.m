//
//  TabFriendsVC.m
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//
#define SEGMENT_HEIGHT 44.0f

#import "TabFriendsVC.h"
#import "FriendsVC.h"
#import "AttentionVC.h"
#import "GroupChatVC.h"
#import "FansVC.h"
#import "SearchVC.h"
#import "AddFriendsOrGroupViewController.h"
#import "AddFriendsViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "PopViewController.h"
#import "ScanViewController.h"
#import "ContactListSelectViewController.h"

@interface TabFriendsVC () <UIScrollViewDelegate, PopViewDelegate, UIPopoverPresentationControllerDelegate>
{
    NSArray *_titleArr;
    CGFloat baseLeft;
}
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) FriendsVC *friendsVC;
@property (nonatomic, strong) AttentionVC *attentionVC;
@property (nonatomic, strong) FansVC *fansVC;
@property (nonatomic, strong) GroupChatVC *gcVC;
@property (nonatomic, strong) PopViewController *popVC;
@property (nonatomic, strong) TabChatVC *tabChatVC;

@end

@implementation TabFriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    baseLeft = 0;
#ifdef APP_SHOW_JYS
    
    self.navigationItem.title = Localized(@"消息");
    _titleArr = @[Localized(@"me_item_chat"),Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans"),Localized(@"me_contact_group")];
    if (![ChatHelper shareHelper].transVC && ![ChatHelper shareHelper].isSendingCard) {
        baseLeft = mainWidth;
        @weakify(self);
        [self actionCustomRightBtnWithNrlImage:@"nav_add" htlImage:nil title:nil action:^{
            @strongify(self);
            [self popView];
        }];
        
        self.tabChatVC = [[TabChatVC alloc] init];
        [self addChildViewController:self.tabChatVC];
        self.tabChatVC.view.frame = CGRectMake(0, 0, mainWidth, self.scrollView.frame.size.height);
        [self.scrollView addSubview:self.tabChatVC.view];
        [ChatHelper shareHelper].tabChatVC = self.tabChatVC;
    }

    
#else
    self.navigationItem.title = Localized(@"me_contact_friends");
    _titleArr = @[Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans"),Localized(@"me_contact_group")];
    if (![ChatHelper shareHelper].transVC && ![ChatHelper shareHelper].isSendingCard) {
        @weakify(self);
        [self actionCustomRightBtnWithNrlImage:@"nav_addfriend" htlImage:nil title:nil action:^{
            @strongify(self);
            [self addFriend];
        }];
    }
#endif
    
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT-0.5, mainWidth, 0.5)];
    lineView.backgroundColor = BG_COLOR;
    [self.view addSubview:lineView];
    
    self.friendsVC = [[FriendsVC alloc] init];
    [self addChildViewController:self.friendsVC];
    self.friendsVC.view.frame = CGRectMake(baseLeft, 0, mainWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.friendsVC.view];
    self.attentionVC = [[AttentionVC alloc] init];
    [self addChildViewController:self.attentionVC];
    self.attentionVC.view.frame = CGRectMake(baseLeft+mainWidth, 0, mainWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.attentionVC.view];
    self.fansVC = [[FansVC alloc] init];
    [self addChildViewController:self.fansVC];
    self.fansVC.view.frame = CGRectMake(baseLeft+mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.fansVC.view];
    if (![ChatHelper shareHelper].isSendingCard) {
        self.gcVC = [[GroupChatVC alloc] init];
        [self addChildViewController:self.gcVC];
        self.gcVC.view.frame = CGRectMake(baseLeft+mainWidth*3, 0, mainWidth, self.scrollView.frame.size.height);
        [self.scrollView addSubview:self.gcVC.view];
    }
    
    [self.friendsVC reloadData];
    [self.fansVC reloadData];
    [self.attentionVC reloadData];
    
    [ChatHelper shareHelper].friendVC = self;
    [self getRelationsData];
}

-(void)popView{
    
    _popVC = [[PopViewController alloc] init];
    _popVC.modalPresentationStyle = UIModalPresentationPopover;
    _popVC.dataSource = @[@{@"title":Localized(@"发起群聊"),
                            @"image":@"chat_add_group"
                            },
                          @{@"title":Localized(@"添加朋友"),
                            @"image":@"chat_add_friend"
                            },
                          @{@"title":Localized(@"扫一扫"),
                            @"image":@"chat_add_qr"
                            }];
    
    //设置依附的按钮
    _popVC.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    //可以指示小箭头颜色
    _popVC.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    
    //content尺寸
    _popVC.preferredContentSize = CGSizeMake(400, 400);
    
    //pop方向
    _popVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    //delegate
    _popVC.popoverPresentationController.delegate = self;
    _popVC.delegate = self;
    [self presentViewController:_popVC animated:YES completion:nil];
}

//代理方法 ,点击即可dismiss掉每次init产生的PopViewController
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}

- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    ScanViewController *scanVC = [[ScanViewController alloc] init];
    scanVC.style = style;
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
}

#pragma mark -popView delegate
- (void)menuClick:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }
            break;
        case 1:
        {
            // 添加朋友
            AddFriendsViewController *addVC = [[AddFriendsViewController alloc] init];
            [self.navigationController pushViewController:addVC animated:YES];
        }
            break;
        case 2:
        {
            // 扫一扫
            [self qqStyle];
        }
            break;
        default:
            break;
    }
    [_popVC dismissViewControllerAnimated:YES completion:nil];
}

- (void)getRelationsData {
    [HTTPManager getRelationListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            ContactParser *dataParser = [ContactParser mj_objectWithKeyValues:dic];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSMutableArray *tempArr = [NSMutableArray array];
                NSMutableArray *allContacts = [[DBHandler sharedInstance] getAllContact];
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:allContacts];
                for (ContactDataParser *parser in dataParser.data) {
                    [tempArr addObject:parser];
                    
                    // 加入数据库
                    [[DBHandler sharedInstance] addOrUpdateContact:parser];
                    
                    NSLog(@"%@",parser.nickname);
                    // 更新群组成员数据库
                    NickNameDataParser *nndP = [NickNameDataParser new];
                    nndP.logo = parser.avatar;
                    nndP.nickname = parser.nickname;
                    nndP.uid = parser.uid;
                    [[DBHandler sharedInstance] addOrUpdateGroupMember:nndP];
                    
                    // 数据库排除现有联系人，剩下的就是将要删除的
                    for (ContactDataParser *oldParser in allContacts) {
                        if ([oldParser.uid isEqualToString:parser.uid]) {
                            [deleteArr removeObject:oldParser];
                        }
                    }
                }
                
                // 数据库删除不存在的联系人
                for (ContactDataParser *delParser in deleteArr) {
                    if ([delParser.relation integerValue] > 0) {
                        [[DBHandler sharedInstance] deleteContactByUid:delParser.uid];
                    }
                    
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.friendsVC reloadData];
                    [self.fansVC reloadData];
                    [self.attentionVC reloadData];
                });
            });
        } else {
            [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)reloadContacts {
    [self.friendsVC reloadData];
    [self.fansVC reloadData];
    [self.attentionVC reloadData];
}

- (void)addFriend
{
    AddFriendsViewController *addVC = [[AddFriendsViewController alloc] init];
    addVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
    NSInteger index = segmentedControl.selectedSegmentIndex;
    self.navigationItem.title = segmentedControl.sectionTitles[index];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    self.navigationItem.title = self.segmentControl.sectionTitles[page];
}

#pragma mark - self cycle

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT, mainWidth, mainHeight-SEGMENT_HEIGHT)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
#ifdef APP_SHOW_JYS
        if ([ChatHelper shareHelper].isSendingCard) {
            _scrollView.contentSize = CGSizeMake(mainWidth*3, mainHeight-200);
        } else if ([ChatHelper shareHelper].transVC) {
            _scrollView.contentSize = CGSizeMake(mainWidth*4, mainHeight-200);
        } else {
            _scrollView.contentSize = CGSizeMake(mainWidth*5, mainHeight-200);
        }
#else
        if ([ChatHelper shareHelper].isSendingCard) {
            _scrollView.contentSize = CGSizeMake(mainWidth*3, mainHeight-200);
        } else {
            _scrollView.contentSize = CGSizeMake(mainWidth*4, mainHeight-200);
        }
#endif
        
        
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, SEGMENT_HEIGHT)];
#ifdef APP_SHOW_JYS
        if ([ChatHelper shareHelper].isSendingCard) {
            _segmentControl.sectionTitles = @[Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans")];
        } else if ([ChatHelper shareHelper].transVC) {
            _segmentControl.sectionTitles = @[Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans"),Localized(@"me_contact_group")];
        } else {
            _segmentControl.sectionTitles = @[Localized(@"me_item_chat"),Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans"),Localized(@"me_contact_group")];
        }
#else
        if ([ChatHelper shareHelper].isSendingCard) {
            _segmentControl.sectionTitles = @[Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans")];
        } else {
            _segmentControl.sectionTitles = @[Localized(@"me_contact_friends"),Localized(@"me_contact_focus"),Localized(@"me_contact_fans"),Localized(@"me_contact_group")];
        }
#endif
        
        _segmentControl.titleTextAttributes = @{NSForegroundColorAttributeName : COLOR_666666, NSFontAttributeName : [UIFont systemFontOfSize:14]};
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR, NSFontAttributeName : [UIFont systemFontOfSize:14]};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 3;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        [_segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}
@end
