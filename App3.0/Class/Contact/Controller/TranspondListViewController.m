//
//  TranspondListViewController.m
//  App3.0
//
//  Created by mac on 17/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TranspondListViewController.h"
#import "NSString+PinYin.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "ConversationModel.h"
#import "ChatHelper.h"
#import "TabFriendsVC.h"
#import "ContactListSelectViewController.h"
#import "SearchVC.h"
#import "ChatListCell.h"
#import "XMPPCoredataChatContact+CoreDataClass.h"

@interface TranspondListViewController ()<UISearchBarDelegate, NSFetchedResultsControllerDelegate>
{
    NSMutableArray *_contactDataArr;
    NSMutableArray *_selectedArr;
    ConversationModel *currentModel;
    UIAlertView *shareAlert;

}

@property(nonatomic, strong)UISearchBar *searchBar;
@property (nonatomic ,strong)NSFetchedResultsController *resultController;
@end

@implementation TranspondListViewController

- (id)initWithMessage:(id)message
{
    self = [super init];
    if (self) {
        _message = message;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationItem.title = @"选择";
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    [self.view addSubview:self.searchBar];
    [self setupDataArray];
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    [ChatHelper shareHelper].transVC = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSFetchedResultsController *)resultController {
    
    if (!_resultController) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==%@", [XMPPManager sharedManager].stream.myJID.bare];
        NSFetchRequest *peopleRequest = [XMPPCoredataChatContact MR_requestAllWithPredicate:predicate];
        peopleRequest.predicate = predicate;
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mostRecentMessageTimestamp" ascending:NO];
        peopleRequest.sortDescriptors = @[sortDescriptor];
        _resultController = [XMPPCoredataChatContact MR_fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"mostRecentMessageTimestamp" ascending:NO delegate:self];
        
    }
    return _resultController;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(XMPPCoredataChatContact *)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            break;
            
        default:
            break;
    }
    
}





- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView beginUpdates];
}

-(void) controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView endUpdates];
    
}


- (ConversationModel *) conversionModel:(XMPPCoredataChatContact *)contact {
    ConversationModel *model = [ConversationModel mj_objectWithKeyValues:contact.contactString];
    if (!contact) {
        if (model.chatType == UUChatTypeChat) {
            [model getUserInfor:model.bareJID.user];
        }else if (model.chatType == UUChatTypeChatRoom){
            [model getGroupInfor:model.bareJID.user];
        }
        
    }
    
    if (isEmptyString(model.title)) {
        if (model.chatType == UUChatTypeChat) {
            [model getUserInfor:model.bareJID.user];
        }else if (model.chatType == UUChatTypeChatRoom){
            [model getGroupInfor:model.bareJID.user];
        }
    }
    
    
    model.lastestDate = contact.mostRecentMessageTimestamp;
    model.chatType = [contact.bareJidStr containsString:XMMPP_BASESTR] ? UUChatTypeChat:UUChatTypeChatRoom;
    model.bareJID = [XMPPJID jidWithString:contact.bareJidStr];
    model.messageCount =[contact.messageCount integerValue];
    return model;
}

#pragma mark-  - - -- -  获取群信息
- (void) reloadGroupMenberWithID:(NSString *)UID withModel:(ConversationModel *)model{
    
    if (isEmptyString(UID)) {
        return;
    }
    [HTTPManager fetchGroupInfoWithGroupId:UID getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
        XMPP_LOG(@"state infor =%@ UID =%@",state.info,UID)
        GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
        if (state.status) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                model.title = parser.data.name;
                model.avatarURLPath = parser.data.avatar;
                model.member_count =parser.data.member_count;
                // 防止同一时间刷新两次列表造成数据重复添加
                NSArray *groupT = [[DBHandler sharedInstance] getGroupByGroupId:parser.data.gid];
                if (groupT.count == 0) {
                    [_contactDataArr  addObject:model];
                }
                // 添加数据库操作放在最后
                [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
            });
            
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}

- (void)setupDataArray
{
    _contactDataArr = [NSMutableArray array];
    __weak typeof(self) wSelf = self;
#ifdef ALIYM_AVALABLE
    [[[SPKitExample sharedInstance].ywIMKit.IMCore getConversationService] asyncFetchAllConversationsWithCompletionBlock:^(NSArray *aConversationsArray) {
        NSArray* sorted = [aConversationsArray sortedArrayUsingComparator:
                           ^(YWConversation *obj1, YWConversation* obj2){
                               id<IYWMessage> message1 = [obj1 conversationLatestMessage];
                               id<IYWMessage> message2 = [obj2 conversationLatestMessage];
                               return [message2.time compare:message1.time];
                           }];
        
        for (YWConversation *conversation in sorted) {
            ConversationModel *model = [[ConversationModel alloc] initWithConversation:conversation];
             if (conversation.conversationType == YWConversationTypeP2P) {
                 YWPerson *person = [(YWP2PConversation *)conversation person];
                 if ([person.personId isEqualToString:@"robot"]) {
                     continue;
                 }
                 NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:person.personId];
                 if (contacts != nil && contacts.count > 0) {
                     // 本地数据库存在
                     ContactDataParser *contact = contacts[0];
                     model.title = [contact getName];
                     model.avatarURLPath = contact.avatar;
                 }
             } else if (conversation.conversationType == YWConversationTypeTribe) {
                 YWTribe *tribe = [(YWTribeConversation *)conversation tribe];
                 NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:tribe.tribeId];
                 if (groupDBArray != nil && groupDBArray.count > 0) {
                     GroupDataModel *parser = groupDBArray[0];
                     model.title = parser.name;
                     model.avatarURLPath = parser.avatar;
                 }
             }
            [_contactDataArr addObject:model];
        }
        [wSelf.tableView reloadData];
    }];
#elif defined EMIM_AVALABLE
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSArray* sorted = [conversations sortedArrayUsingComparator:
                       ^(EMConversation *obj1, EMConversation* obj2){
                           EMMessage *message1 = [obj1 latestMessage];
                           EMMessage *message2 = [obj2 latestMessage];
                           if(message1.timestamp > message2.timestamp) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    for (EMConversation *conversation in sorted) {
        NSLog(@"%@-%d",conversation.conversationId,conversation.unreadMessagesCount);
        if ([conversation.conversationId isEqualToString:@"robot"]) {
            continue;
        }
        
        ConversationModel *model = [[ConversationModel alloc] initWithConversation:conversation];
        if (conversation.type == EMConversationTypeChat) {
            NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:conversation.conversationId];
            if (contacts != nil && contacts.count > 0) {
                ContactDataParser *contact = contacts[0];
                model.title = [contact getName];
                model.avatarURLPath = contact.avatar;
            }
            
        } else if (conversation.type == EMConversationTypeGroupChat) {
            NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:conversation.conversationId];
            if (groupDBArray != nil && groupDBArray.count > 0) {
                GroupDataModel *parser = groupDBArray[0];
                model.title = parser.name;
                model.avatarURLPath = parser.avatar;
            }
        }
        [_contactDataArr addObject:model];
        
    }
    [self.tableView reloadData];
#else
    [self.resultController performFetch:nil];
    
#endif
    
    
}

#pragma mark - searchbar delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"%s",__FUNCTION__);
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
    return NO;
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }
#ifdef XSIM_AVALABLE
    return self.resultController.fetchedObjects.count;
#else
    return _contactDataArr.count;
#endif
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 50;
    }
    return 68;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.1;
    }
    return 22;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section != 1) {
        return nil;
    }
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = mainGrayColor;
    titleLabel.text = @"最近聊天";
    titleLabel.font = [UIFont systemFontOfSize:12];
    [myView addSubview:titleLabel];
    
    return myView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TabFriendsVC *friendsVC = [[TabFriendsVC alloc] init];
            [self.navigationController pushViewController:friendsVC animated:YES];
        } else if (indexPath.row == 1) {
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }
        return;
    }
    
    
    
        //生成Message
#ifdef ALIYM_AVALABLE
    __weak typeof(self) wSelf = self;
    currentModel = _contactDataArr[indexPath.row];
    YWConversation *conversation = currentModel.conversation;
    if (self.isShare) {
        [conversation asyncSendMessageBody:self.message progress:^(CGFloat progress, NSString *messageID) {
            
        } completion:^(NSError *error, NSString *messageID) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [wSelf.navigationController popViewControllerAnimated:YES];
            });
        }];
    } else {
        [conversation asyncForwardMessage:self.message progress:^(CGFloat progress, NSString *messageID) {
            
        } completion:^(NSError *error, NSString *messageID) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [wSelf.navigationController popViewControllerAnimated:YES];
            });
        }];
    }
    
#elif defined EMIM_AVALABLE
    __weak typeof(self) wSelf = self;
    currentModel = _contactDataArr[indexPath.row];
    NSString *from = [[EMClient sharedClient] currentUsername];
    EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)currentModel.conversation).conversationId from:from to:((EMConversation *)currentModel.conversation).conversationId body:((EMMessage *)_message).body ext:((EMMessage *)_message).ext];
    message.chatType = (EMChatType)((EMConversation *)currentModel.conversation).type;
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        if (!error) {
            [XSTool showToastWithView:self.view Text:@"发送成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
        }else{
            [XSTool showToastWithView:self.view Text:error.description];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wSelf.navigationController popViewControllerAnimated:YES];
        });
    }];
#else
    XMPPCoredataChatContact *contact = self.resultController.fetchedObjects[indexPath.row];
    ConversationModel *model = [self conversionModel:contact];
    if (isEmptyString(model.conversation)) {
        [MBProgressHUD showMessage:@"获取会话信息有误" view:self.view];
        return;
    }
    if (model.chatType == UUChatTypeChatRoom) {
        ConversationModel *conversionModel = [XMPPSignal roomConfigWithConversation:model];
        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
        message.type = @"txt";
        message.body = self.transModel.messageText;
        message.ext = self.transModel.extDic.mj_JSONString;
        NSString *messageString = message.mj_JSONString;
        self.transModel.messageText = messageString;
        [XMPPSignal shareXM_GroupModel:self.transModel contact:conversionModel finash:^(NSString *title) {
            [MBProgressHUD showMessage:@"分享成功" view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    }else{
        [XMPPSignal shareXM_SignalModel:self.transModel contact:model finash:^(NSString *title) {
            [MBProgressHUD showMessage:@"分享成功" view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
        
    }
#endif

    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    __weak typeof(self) wSelf = self;
    if (buttonIndex == 1) {
        //生成Message
#ifdef ALIYM_AVALABLE
        YWConversation *conversation = currentModel.conversation;
        if (self.isShare) {
            [conversation asyncSendMessageBody:self.message progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [wSelf.navigationController popViewControllerAnimated:YES];
                });
            }];
        } else {
            [conversation asyncForwardMessage:self.message progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"发送成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }else{
                    [XSTool showToastWithView:self.view Text:error.description];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [wSelf.navigationController popViewControllerAnimated:YES];
                });
            }];
        }
#elif defined EMIM_AVALABLE
        NSString *from = [[EMClient sharedClient] currentUsername];
        EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)currentModel.conversation).conversationId from:from to:((EMConversation *)currentModel.conversation).conversationId body:((EMMessage *)_message).body ext:nil];
        message.chatType = EMChatTypeChat;// 设置为单聊消息
        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
            }else{
                [XSTool showToastWithView:self.view Text:error.description];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [wSelf.navigationController popViewControllerAnimated:YES];
            });
        }];
#else
        [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
        
    }
}
//alert标题靠左
-(void) willPresentAlertView:(UIAlertView *)alertView
{
    NSString *title = [NSString stringWithFormat:@"发送给 %@",currentModel.title];
    for(UIView *subview in shareAlert.subviews)
    {
        if([[subview class] isSubclassOfClass:[UILabel class]])
        {
            UILabel *label = (UILabel*)subview;
            if([label.text isEqualToString:title])
                label.textAlignment = NSTextAlignmentLeft;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"transTopCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = indexPath.row == 0?@"选择联系人":@"创建新聊天";
        }
        return cell;
    }
    NSString *CellIdentifier = [NSString stringWithFormat:@"chatCell%ld",(long)indexPath.row];
    ChatListCell *cell = (ChatListCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
#ifdef XSIM_AVALABLE
    XMPPCoredataChatContact *contact  = self.resultController.fetchedObjects[indexPath.row];
    
    [cell setChatListCellData:[self conversionModel:contact]];
#else
    [cell setChatListCellData:_contactDataArr[indexPath.row]];
#endif
    
    return cell;
    
    return cell;
}

- (UISearchBar *)searchBar
{
    if (_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.placeholder = Localized(@"search");
        _searchBar.translucent = YES; // 设置是否透明
    }
    return _searchBar;
}
@end
