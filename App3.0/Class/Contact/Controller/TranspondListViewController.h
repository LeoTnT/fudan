//
//  TranspondListViewController.h
//  App3.0
//
//  Created by mac on 17/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
@class XM_TransModel;

@interface TranspondListViewController : XSBaseTableViewController
@property (nonatomic,assign) BOOL isShare;
@property (nonatomic,copy) NSString * messageText;

/*
 环信：EMMessageBody
 云旺：YWMessageBody
 新商：XM_TransModel
 */
@property (nonatomic, strong) id message;
- (id)initWithMessage:(id)message;

@property (nonatomic,strong) XM_TransModel *transModel;
@end
