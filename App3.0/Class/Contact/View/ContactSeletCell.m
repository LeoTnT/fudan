//
//  ContactSeletCell.m
//  App3.0
//
//  Created by mac on 17/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ContactSeletCell.h"

@interface ContactSeletCell ()
{
    UIImageView *_avatar;
    UILabel *_title;
    UIImageView *_selectImg;
    UILabel *_muteTime;
}
@end

@implementation ContactSeletCell
@synthesize isSelect = _isSelect;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.isSelect = NO;
        
        _avatar = [[UIImageView alloc] init];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 5;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        _selectImg = [[UIImageView alloc] init];
        _selectImg.image = [UIImage imageNamed:@"checkbox_unselected"];
        [self addSubview:_selectImg];
        
        [_selectImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.centerY.mas_equalTo(self);
        }];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_selectImg.mas_right).offset(14);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8.5);
            make.centerY.mas_equalTo(self);
        }];
        
        _muteTime = [[UILabel alloc] init];
        _muteTime.font = [UIFont systemFontOfSize:12];
        _muteTime.textColor = mainGrayColor;
        _muteTime.textAlignment = NSTextAlignmentRight;
        [self addSubview:_muteTime];
        _muteTime.hidden = YES;
        
        [_muteTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-10);
            make.centerY.mas_equalTo(self);
            make.width.mas_equalTo(100);
        }];
    }
    return self;
}

- (void)setCellData:(ContactDataParser *)model
{
    [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _title.text = [model getName];
    _muteTime.hidden = YES;
    _selectImg.hidden = NO;
    _muteTime.text = @"";
    _muteTimeString = @"";
}

- (void)setIsSelect:(BOOL)isSelect
{
    _isSelect = isSelect;
    if (isSelect) {
        _selectImg.image = [UIImage imageNamed:@"checkbox_selected"];
    } else {
        _selectImg.image = [UIImage imageNamed:@"checkbox_unselected"];
    }
}

- (void)setMuteTimeString:(NSString *)muteTimeString {
    _muteTimeString = muteTimeString;
    _muteTime.hidden = NO;
    _muteTime.text = [NSString stringWithFormat:@"禁言中:%@",muteTimeString];
    _selectImg.hidden = YES;
}
@end
