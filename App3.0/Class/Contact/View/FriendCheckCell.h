//
//  FriendCheckCell.h
//  App3.0
//
//  Created by mac on 2017/7/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FriendCheckCellDelegate <NSObject>

- (void)attationClick:(DeviceFansDetailParser *)contact;

@end

@interface FriendCheckCell : UITableViewCell
@property (nonatomic, weak) id<FriendCheckCellDelegate>delegate;
- (void)setAttationData:(DeviceFansDetailParser *)model;
- (void)setCellData:(NSDictionary *)data;
@end
