//
//  AttentionCell.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AttentionCell.h"

@interface AttentionCell ()
{
    UIImageView *_avatar;
    UILabel *_title;
}
@end

@implementation AttentionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _avatar = [[UIImageView alloc] init];;
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 3;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview:lineView];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8);
            make.right.mas_equalTo(self).offset(-12);
            make.centerY.mas_equalTo(self);
        }];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar);
            make.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

- (void)setCellData:(ContactDataParser *)model
{
    [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _title.text = [model getName];
}
@end
