//
//  AttentionCell.h
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactModel.h"

@interface AttentionCell : UITableViewCell
- (void)setCellData:(ContactDataParser *)model;
@end
