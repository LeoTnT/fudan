//
//  FriendCheckCell.m
//  App3.0
//
//  Created by mac on 2017/7/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FriendCheckCell.h"

@interface FriendCheckCell ()
{
    UIImageView *_avatar;
    UILabel *_title;
    UIButton *_attationBtn;
    UILabel *_detailLabel;
    DeviceFansDetailParser *_parser;
}
@end

@implementation FriendCheckCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _avatar = [[UIImageView alloc] init];;
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 5;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        _attationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_attationBtn setTitle:Localized(@"me_contact_focus") forState:UIControlStateNormal];
        [_attationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _attationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_attationBtn setBackgroundColor:mainColor];
        [_attationBtn addTarget:self action:@selector(attationAction:) forControlEvents:UIControlEventTouchUpInside];
        _attationBtn.layer.masksToBounds = YES;
        _attationBtn.layer.cornerRadius = 5;
        [self addSubview:_attationBtn];
        
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.text = @"短信邀请";
        _detailLabel.font = [UIFont systemFontOfSize:14];
        _detailLabel.textColor = COLOR_999999;
        [self addSubview:_detailLabel];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview:lineView];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8);
            make.right.mas_lessThanOrEqualTo(_attationBtn.mas_left).offset(-12);
            make.centerY.mas_equalTo(self);
        }];
        [_attationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-22.5);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(55, 31));
        }];
        [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-22.5);
            make.centerY.mas_equalTo(self);
        }];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar);
            make.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

- (void)setAttationData:(DeviceFansDetailParser *)model {
    _parser = model;
    _attationBtn.hidden = NO;
    _detailLabel.hidden = YES;
    [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _title.text = model.name;
}

- (void)setCellData:(NSDictionary *)data {
    _attationBtn.hidden = YES;
    _detailLabel.hidden = NO;
    [_avatar setImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _title.text = data[@"name"];
}
- (void)attationAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(attationClick:)]) {
        [self.delegate attationClick:_parser];
    }
}
@end
