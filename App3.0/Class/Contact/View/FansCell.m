//
//  FansCell.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FansCell.h"

@interface FansCell ()
{
    UIImageView *_avatar;
    UILabel *_title;
    UILabel *_number;
}
@end
@implementation FansCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(12, 8, 50, 50)];
        _avatar.image = [UIImage imageNamed:@"avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 3;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] initWithFrame:CGRectMake(74, 18, mainWidth-124, 30)];
        _title.text = @"一级会员";
        _title.font = [UIFont systemFontOfSize:20];
        [self addSubview:_title];
        
        _number = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-100, 23, 78, 20)];
        _number.text = @"0";
        _number.textColor = [UIColor hexFloatColor:@"0090ff"];
        _number.textAlignment = NSTextAlignmentRight;
        _number.font = [UIFont systemFontOfSize:16];
        [self addSubview:_number];
        
        UILabel *ren = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-20, 23, 20, 20)];
        ren.text = @"人";
        ren.textColor = [UIColor hexFloatColor:@"fdc0c0"];
        ren.font = [UIFont systemFontOfSize:16];
        ren.textAlignment = NSTextAlignmentLeft;
        [self addSubview:ren];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(74, 66, mainWidth-74, 0.5)];
        lineView.backgroundColor = [UIColor hexFloatColor:@"e5e5e5"];
        [self addSubview:lineView];
    }
    return self;
}

- (void)setData:(NSDictionary *)data
{
//    _title.text = [data objectForKey:@"title"];
}
@end
