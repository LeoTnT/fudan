//
//  ContactSeletCell.h
//  App3.0
//
//  Created by mac on 17/3/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactModel.h"

@interface ContactSeletCell : UITableViewCell
@property (nonatomic, assign) BOOL isSelect;
- (void)setCellData:(ContactDataParser *)model;

// 禁言时长
@property (copy, nonatomic) NSString *muteTimeString;
@end
