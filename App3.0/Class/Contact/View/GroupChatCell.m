//
//  GroupChatCell.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupChatCell.h"

@interface GroupChatCell ()
{
    UIImageView *_avatar;
    UILabel *_title;
    UILabel *_descript;
    UILabel *_peopleNum;
}
@end

@implementation GroupChatCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _avatar = [[UIImageView alloc] init];
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 3;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        _peopleNum = [[UILabel alloc] init];
        _peopleNum.font = [UIFont systemFontOfSize:16];
        _peopleNum.textColor = COLOR_999999;
        [self addSubview:_peopleNum];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview:lineView];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.top.mas_equalTo(self.mas_top).offset(9.5);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        [_peopleNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_title.mas_right);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_left);
            make.right.mas_equalTo(self.mas_right);
            make.bottom.mas_equalTo(self.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

- (void)setCellData:(GroupDataModel *)model
{
    [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _title.text = model.name;
    _peopleNum.text = [NSString stringWithFormat:@"(%d)",[model.member_count intValue]];
    
}

@end
