//
//  ContactModel.h
//  App3.0
//
//  Created by mac on 17/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ContactRelationType)
{
    ContactRelationFriend = 1, // 好友关系
    ContactRelationAttention,  // 我的关注
    ContactRelationFans        // 我的粉丝
};

typedef NS_ENUM(NSInteger, ContactShieldType)
{
    ContactShieldNone = 0,      // 正常
    ContactShieldNoDisturbing,  // 免打扰
    ContactShieldShield         // 屏蔽
};

// 好友、关注、粉丝model
@interface ContactDataParser : NSObject
@property (nonatomic,copy)NSString* uid;
@property (nonatomic,copy)NSString* username;
@property (nonatomic,copy)NSString* nickname;
@property (nonatomic,copy)NSString* showname;
@property (nonatomic,copy)NSString* avatar;
@property (nonatomic,copy)NSString* mobile;
@property (nonatomic,strong)NSNumber* relation;
@property (nonatomic,strong)NSNumber* is_shield;
@property (nonatomic,strong)NSNumber* is_shield_me;
@property (nonatomic,strong)NSNumber* is_not_disturb;
@property (nonatomic,strong)NSNumber* is_can_talk;
@property (nonatomic,copy)NSString* remark;
@property (nonatomic,copy)NSString* owner;  // DB用字段 所属者

- (NSString *)getName;
@end

@interface ContactParser : NSObject
@property (nonatomic,retain)NSArray* data;
@end

// 获取用户信息model
@interface UserInfoDataParser : NSObject
@property (nonatomic,copy)NSString* uid;
@property (nonatomic,copy)NSString* showname;
@property (nonatomic,copy)NSString* username;
@property (nonatomic,copy)NSString* nickname;
@property (nonatomic,copy)NSString* mobile;
@property (nonatomic,copy)NSString* logo;
@property (nonatomic,copy)NSString* logo_source;    // 大图
@property (nonatomic,strong)NSNumber* relation;
@property (nonatomic,strong)NSNumber* is_can_talk;
@property (nonatomic,copy)NSString* remark;

- (NSString *)getName;
@end

@interface UserInfoParser : NSObject
@property (nonatomic,strong)UserInfoDataParser* data;
@end

// 群组model
@interface GroupDataParser : NSObject
@property (nonatomic,copy)NSString* group_id;
@property (nonatomic,strong)NSNumber* status;
@property (nonatomic,copy)NSString* group_name;
@property (nonatomic,copy)NSString* group_desc;
@property (nonatomic,copy)NSString* group_avatar;
@property (nonatomic,copy)NSString* group_owner;
@property (nonatomic,strong)NSNumber* w_time;
@property (nonatomic,strong)NSNumber* memberCount;
@end

@interface GroupDataModel : NSObject
@property (nonatomic,copy)NSString* gid;             // 群id
@property (nonatomic,copy)NSString* type;           // 群类型（普通0|门派……）
@property (nonatomic,copy)NSString* owner;          // 群主用户id
@property (nonatomic,copy)NSString* name;           // 群名称
@property (nonatomic,copy)NSString* desc;           // 群描述
@property (nonatomic,copy)NSString* avatar;         // 群头像
@property (nonatomic,copy)NSString* w_time;         // 建群时间
@property (nonatomic,copy)NSString* member_count;    // 群员数量
@property (nonatomic,copy)NSString* maxusers;    // 群成员上限
@property (nonatomic,assign)unsigned int isPublic;      // 是否公开群(1是|0否)
@property (nonatomic,assign)unsigned int invite_need_confirm;      // 入群是否需要审核(0否|1是)
@property (nonatomic,assign)unsigned int allowinvites;      // 是否允许成员邀请(0否|1是)
@property (nonatomic,assign)unsigned int allowapply;      // 是否允许申请(0否|1是)
@property (nonatomic,strong)NSNumber* is_shield;      // 屏蔽
@property (nonatomic,strong)NSNumber* is_not_disturb; // 免打扰
@property (nonatomic,strong)NSArray* members;       // 群成员信息(最多50条)
@property (nonatomic,assign)unsigned int in_group;      // 是否已在群中(1是|0否)

@end

@interface GroupMemberAddResultModel : NSObject
@property (nonatomic,copy)NSString* gid;                    // 群id
@property (nonatomic,copy)NSString* nick_str;               // 被邀请用户昵称
@property (nonatomic,copy)NSString* avatar;
@property (nonatomic,copy)NSString* owner;
@property (nonatomic,copy)NSString* name;
@property (nonatomic,copy)NSString* desc;
@property (nonatomic,copy)NSString* member_count;
@property (nonatomic,copy)NSString* user_nick;          // 邀请人
@property (nonatomic,copy)NSString* invite_num;         // 被邀请人数量
@property (nonatomic,copy)NSString* invite_id;      // 邀请记录(需审核时)
@end

@interface GroupModel : NSObject
@property (nonatomic,strong)NSArray* data;
@end

@interface GroupAddResultModel : NSObject
@property (nonatomic,copy)NSString* gid;                    // 群id
@property (nonatomic,copy)NSString* group_name;             // 群名
@property (nonatomic,copy)NSString* nick_str;               // 实际拉进群的用户昵称拼接字符串
@property (nonatomic,copy)NSString* avatar;
@end

@interface GroupFetchParser : NSObject
@property (nonatomic,strong)GroupDataModel* data;
@end

@interface NickNameDataParser : NSObject
@property (nonatomic,copy)NSString* logo;               // 用户头像
@property (nonatomic,copy)NSString* nickname;           // 用户昵称
@property (nonatomic,copy)NSString* uid;                // 用户id
@property (nonatomic,copy)NSString* username;           // 用户编号
@property (nonatomic,copy)NSString* spell_first;        // 用户昵称首字母
@property (nonatomic,copy)NSString* spell_short;        // 用户昵称缩写
@property (nonatomic,copy)NSString* relation;        // 关系

- (NSString *)getName;
@end

// 获取手机通讯录中的好友情况

@interface DeviceFansDetailParser : NSObject
@property (nonatomic,copy)NSString* name;
@property (nonatomic,copy)NSString* num;
@property (nonatomic,copy)NSString* uid;
@property (nonatomic,copy)NSString* avatar;
@property (nonatomic,copy)NSString* nickname;
@property (nonatomic,copy)NSString* username;
@property (nonatomic,strong)NSNumber* relation;
@end

@interface DeviceFansDataParser : NSObject
@property (nonatomic,strong)NSArray* notReg;
@property (nonatomic,strong)NSArray* notFans;
@property (nonatomic,strong)NSArray* isFans;
@property (nonatomic,strong)NSArray* contact;
@end

@interface DeviceFansParser : NSObject
@property (nonatomic,strong)DeviceFansDataParser* data;
@end

@interface MuteMemberModel : NSObject
@property (nonatomic,copy)NSString* uid;
@property (nonatomic,copy)NSString* nickname;
@property (nonatomic,copy)NSString* expire;
@end

@interface MemberMuteModel : NSObject
@property (nonatomic,strong)NSArray* list;
@property (nonatomic,copy)NSString* duration;
@end

@interface MemberMuteListModel : NSObject
@property (nonatomic,strong)NSArray* data;
@end

// 屏蔽信息
@interface BlackListDataModel : ContactDataParser
@property (nonatomic,copy)NSString* account;
@property (nonatomic,copy)NSString* firstPinYin;        // 用户昵称首字母
@property (nonatomic,copy)NSString* pinyin;        // 用户昵称缩写
@end

@interface BlackListModel : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface ContactModel : NSObject


@end
