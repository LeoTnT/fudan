//
//  ContactModel.m
//  App3.0
//
//  Created by mac on 17/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ContactModel.h"

@implementation ContactDataParser

- (NSString *)getName {
    if (self.remark.length > 0) {
        return self.remark;
    }else if (self.nickname.length > 0) {
        return self.nickname;
    }else if (self.username.length > 0) {
        return self.username;
    }
    return self.uid;
}
@end

@implementation ContactParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"ContactDataParser"
             };
}
@end

@implementation UserInfoDataParser

- (NSString *)getName {
    if (self.remark.length > 0) {
        return self.remark;
    }else if (self.nickname.length > 0) {
        return self.nickname;
    }else if (self.username.length > 0) {
        return self.username;
    }
    return self.uid;
}
@end

@implementation UserInfoParser

@end

@implementation GroupDataParser

@end

@implementation GroupDataModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"members" : @"NickNameDataParser"
             };
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{
             @"isPublic" : @"public"
             };
}
@end

@implementation GroupModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"GroupDataModel"
             };
}
@end
@implementation GroupMemberAddResultModel
@end

@implementation GroupAddResultModel
@end

@implementation GroupFetchParser

@end

@implementation NickNameDataParser
- (NSString *)getName {
    if (self.nickname.length > 0) {
        return self.nickname;
    }else if (self.username.length > 0) {
        return self.username;
    }
    return self.uid;
}
@end

@implementation DeviceFansDetailParser

@end

@implementation DeviceFansDataParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"notReg" : @"DeviceFansDetailParser",
             @"notFans" : @"DeviceFansDetailParser",
             @"isFans" : @"DeviceFansDetailParser",
             @"contact" : @"DeviceFansDetailParser"
             };
}
@end

@implementation DeviceFansParser

@end

@implementation MuteMemberModel

@end

@implementation MemberMuteModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"list" : @"MuteMemberModel"
             };
}
@end

@implementation MemberMuteListModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"MuteMemberModel"
             };
}
@end

@implementation BlackListDataModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{
             @"uid" : @"target_uid",
             @"nickname" : @"nickName",
             };
}
@end

@implementation BlackListModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"BlackListDataModel"
             };
}
@end

@implementation ContactModel


@end
