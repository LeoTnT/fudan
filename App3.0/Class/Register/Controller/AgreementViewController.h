//
//  AgreementViewController.h
//  App3.0
//
//  Created by nilin on 2017/6/30.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AgreementType) {
    AgreementTypeReg = 1,
    AgreementTypeRecharge,
    AgreementTypeStore,
    AgreementTypeUsingPermissions
};
@interface AgreementViewController : XSBaseViewController
- (instancetype)initWithAgreementType:(AgreementType)type;
@end
