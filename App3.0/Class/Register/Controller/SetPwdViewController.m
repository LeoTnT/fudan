//
//  SetPwdViewController.m
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetPwdViewController.h"
#import "RegisterModel.h"
#import "LoginModel.h"
#import "AppDelegate.h"
#import "UserInstance.h"
#import "RSAEncryptor.h"

@interface SetPwdViewController ()
{
    NSString *_mobile;
    NSString *_verify;
    NSString *_intro;
    NSString *_nickname;
    
    UITextField *_tfPwd;
    UITextField *_tfPwdAgain;
    XSCustomButton *_finishBtn;
}
@property (strong, nonatomic) UITextField *tfPayPwd;
@property (strong, nonatomic) UITextField *tfPayPwdAgain;
@end

@implementation SetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = Localized(@"enter_register");
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    // 密码输入框
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView1 addSubview:leftImg1];
    _tfPwd = [[UITextField alloc] init];
    [self.view addSubview:_tfPwd];
    _tfPwd.placeholder = Localized(@"请设置6~20位登录密码");
    _tfPwd.font = [UIFont systemFontOfSize:15];
    _tfPwd.leftView = leftView1;
    _tfPwd.secureTextEntry=YES;
    _tfPwd.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView1= [[UIView alloc] init];
    [self.view addSubview:lineView1];
    lineView1.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    
    // 重复密码输入框
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView2 addSubview:leftImg2];
    _tfPwdAgain = [[UITextField alloc] init];
    [self.view addSubview:_tfPwdAgain];
    _tfPwdAgain.placeholder = Localized(@"请确认登录密码");
    _tfPwdAgain.font = [UIFont systemFontOfSize:15];
    _tfPwdAgain.leftView = leftView2;
    _tfPwdAgain.secureTextEntry=YES;
    _tfPwdAgain.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView2= [[UIView alloc] init];
    [self.view addSubview:lineView2];
    lineView2.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    
#ifdef APP_SHOW_JYS
    // 支付密码输入框
    UIView *leftView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView3 addSubview:leftImg3];
    _tfPayPwd = [[UITextField alloc] init];
    [self.view addSubview:_tfPayPwd];
    _tfPayPwd.placeholder = Localized(@"请设置6位支付密码");
    _tfPayPwd.font = [UIFont systemFontOfSize:15];
    _tfPayPwd.leftView = leftView3;
    _tfPayPwd.secureTextEntry=YES;
    _tfPayPwd.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView3= [[UIView alloc] init];
    [self.view addSubview:lineView3];
    lineView3.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    
    
    // 重复支付密码输入框
    UIView *leftView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView4 addSubview:leftImg4];
    _tfPayPwdAgain = [[UITextField alloc] init];
    [self.view addSubview:_tfPayPwdAgain];
    _tfPayPwdAgain.placeholder = Localized(@"请确认支付密码");
    _tfPayPwdAgain.font = [UIFont systemFontOfSize:15];
    _tfPayPwdAgain.leftView = leftView4;
    _tfPayPwdAgain.secureTextEntry=YES;
    _tfPayPwdAgain.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView4= [[UIView alloc] init];
    [self.view addSubview:lineView4];
    lineView4.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
#endif
    
    // 登录
    _finishBtn=[[XSCustomButton alloc] initWithTitle:Localized(@"enter_register") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] cornerRadius:4 backGroundColor:mainColor hBackGroundColor:HighLightColor_Main];
    [_finishBtn addTarget:self action:@selector(finishAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_finishBtn];
    
    //设置约束
    [_tfPwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(self.view).offset(121-64);
        make.right.mas_equalTo(self.view).offset(-25);
    }];
    [leftImg1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView1);
        make.centerY.mas_equalTo(leftView1);
    }];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.bottom.mas_equalTo(_tfPwd.mas_bottom).offset(13);
        make.width.mas_equalTo(_tfPwd);
        make.height.mas_equalTo(0.5);
    }];
    
    [_tfPwdAgain mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.right.mas_equalTo(_tfPwd);
        make.top.mas_equalTo(lineView1.mas_bottom).offset(32);
    }];
    [leftImg2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView2);
        make.centerY.mas_equalTo(leftView2);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfPwdAgain.mas_bottom).offset(13);
    }];
    
#ifdef APP_SHOW_JYS
    [_tfPayPwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.right.mas_equalTo(_tfPwd);
        make.top.mas_equalTo(lineView2.mas_bottom).offset(32);
    }];
    [leftImg3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView3);
        make.centerY.mas_equalTo(leftView3);
    }];
    [lineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfPayPwd.mas_bottom).offset(13);
    }];
    
    [_tfPayPwdAgain mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.right.mas_equalTo(_tfPwd);
        make.top.mas_equalTo(lineView3.mas_bottom).offset(32);
    }];
    [leftImg4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView4);
        make.centerY.mas_equalTo(leftView4);
    }];
    [lineView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfPayPwdAgain.mas_bottom).offset(13);
    }];
    
    [_finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfPayPwdAgain.mas_bottom).offset(43.5);
        make.left.mas_equalTo(lineView4);
        make.right.mas_equalTo(lineView4);
        make.height.mas_equalTo(50);
    }];

#else
    [_finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfPwdAgain.mas_bottom).offset(43.5);
        make.left.mas_equalTo(lineView2);
        make.right.mas_equalTo(lineView2);
        make.height.mas_equalTo(50);
    }];
#endif
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithMobile:(NSString *)mobile smsVerify:(NSString *)smsVerify intro:(NSString *)intro nickName:(NSString *)nickname
{
    self = [super init];
    if (self) {
        _mobile = mobile;
        _verify = smsVerify;
        _intro = intro;
        _nickname = nickname;
    }
    return self;
}


- (void)finishAction
{
    if(!(_tfPwd.text.length&&_tfPwdAgain.text.length)) {
        [XSTool showToastWithView:self.view Text:@"密码不能为空"];
        return;
    }
    if(![_tfPwd.text isEqualToString:_tfPwdAgain.text]) {
        [XSTool showToastWithView:self.view Text:@"两次输入的密码不一致"];
        return;
    }
    
#ifdef APP_SHOW_JYS
    if(!(self.tfPayPwd.text.length && self.tfPayPwdAgain.text.length)) {
        [XSTool showToastWithView:self.view Text:@"支付密码不能为空"];
        return;
    }
    if(![self.tfPayPwd.text isEqualToString:self.tfPayPwdAgain.text]) {
        [XSTool showToastWithView:self.view Text:@"两次输入的支付密码不一致"];
        return;
    }
    
    if([self.tfPayPwd.text isEqualToString:_tfPwd.text]) {
        [XSTool showToastWithView:self.view Text:@"密码不能一致"];
        return;
    }
#endif
    
    [self.view endEditing:YES];
    [XSTool showProgressHUDWithView:self.view];
    //加密
    NSString *encryptPassWord = [RSAEncryptor encryptString:_tfPwd.text];
    self.regModel.passwd_one = encryptPassWord;
    self.regModel.passwd_two = encryptPassWord;

#ifdef APP_SHOW_JYS
    NSString *encryptPayPassWord = [RSAEncryptor encryptString:self.tfPayPwd.text];
    self.regModel.pay_password = encryptPayPassWord;
    self.regModel.rePwd = encryptPayPassWord;
#endif
    
    [HTTPManager registerWithParams:self.regModel.mj_keyValues success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 注册成功
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showProgressHUDTOView:self.view withText:@"注册成功，登录中..."];
            //加密
            [HTTPManager doLoginin:dic[@"data"][@"username"]
                          password:encryptPassWord
                           success:^(NSDictionary * _Nullable dic, resultObject *state) {
                
                LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];
                if (state.status) {
                    NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
                    NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];
#ifdef ALIYM_AVALABLE
                    // 阿里云旺初始化
                    [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:parser.data.uid passWord:pwd preloginedBlock:^{
                        
                    } successBlock:^{
                        NSLog(@"登录成功");
                        [XSTool hideProgressHUDWithView:self.view];
                        
                        // 将guid存到本地沙盒
                        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                        [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                        //        [ud synchronize];
                        
                        //将用户信息存储到本地沙盒
                        LoginDataParser *dataPaser = parser.data;
                        NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                        NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                        [user setObject:userData forKey:USERINFO_LOGIN];
                        [user synchronize];
                        
                        // 存储登录账号信息
                        NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":encryptPassWord,@"avatar":dataPaser.logo};
                        [[UserInstance ShardInstnce] saveAccount:accountDic];
                        
                        // 实例化user数据
                        [[UserInstance ShardInstnce] setupUserInfo];
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                    } failedBlock:^(NSError *error) {
                        NSLog(@"登录失败 %@",error.description);
                        [XSTool hideProgressHUDWithView:self.view];
                    }];
                    
                    // 登录腾讯云
                    NSString *tlsPwd = [pwd substringWithRange:NSMakeRange(8, 16)];
                    [[ILiveLoginManager getInstance] tlsLogin:[NSString stringWithFormat:@"xsy%@",parser.data.uid] pwd:tlsPwd succ:^{
                        NSLog(@"-----> succ");
                    } failed:^(NSString *moudle, int errId, NSString *errMsg) {
                        NSLog(@"-----> fail %@,%d,%@",moudle,errId,errMsg);
                    }];
#elif defined EMIM_AVALABLE
                    //登录环信
                    [[EMClient sharedClient] loginWithUsername:parser.data.uid
                                                      password:pwd
                                                    completion:^(NSString *aUsername, EMError *aError) {
                                                        if (!aError) {
                                                            NSLog(@"登录成功");
                                                            [XSTool hideProgressHUDWithView:self.view];
                                                            
                                                            // 将guid存到本地沙盒
                                                            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                            [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                                                            //        [ud synchronize];
                                                            
                                                            //将用户信息存储到本地沙盒
                                                            LoginDataParser *dataPaser = parser.data;
                                                            NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                                                            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                                                            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                                                            [user setObject:userData forKey:USERINFO_LOGIN];
                                                            [user synchronize];
                                                            
                                                            // 存储登录账号信息
                                                            NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":encryptPassWord,@"avatar":dataPaser.logo};
                                                            [[UserInstance ShardInstnce] saveAccount:accountDic];
                                                            
                                                            // 实例化user数据
                                                            [[UserInstance ShardInstnce] setupUserInfo];

                                                            [self dismissViewControllerAnimated:YES completion:nil];
//                                                            AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//                                                            appDelegate.window.rootViewController = [XSTool enterMainViewController];
                                                            //                                                            [self presentViewController:[XSTool enterMainViewController] animated:YES completion:nil];
                                                        } else {
                                                            NSLog(@"登录失败 %@",aError.errorDescription);
                                                            [XSTool hideProgressHUDWithView:self.view];
                                                            [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"环信登录失败:%i",aError.code]];
                                                        }
                                                    }];
#else
                
                [self configInfor:parser password:encryptPassWord];
                [self xmppLogAction:parser password:encryptPassWord];
#endif
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                NSLog(@"%@",error.description);
            }];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void) xmppLogAction:(LoginParser *)parser  password:(NSString *)passWord{
    @weakify(self);
    [XMPPManager xmppUserLogIn:parser.data.uid complment:^(XIM_STREAMSTATE type) {
        @strongify(self);
        if (type == XIM_XMPP_CONNECT) {
            [MBProgressHUD showMessage:Localized(@"errcode_success") view:self.view];
            NSLog(@"XMErrorInvalidType  = XMErrorInvalidTypeSuccess ");
        }else{
            [[XMPPManager sharedManager] xmppUserLogout];
            NSString * str = [XMPPManager getStreamConnet:type];
            [MBProgressHUD showMessage:str view:self.view];
        }
    }];
    
}


- (void) configInfor:(LoginParser *)parser password:(NSString *)passWord{
    NSLog(@"登录成功");
    [XSTool hideProgressHUDWithView:self.view];
    // 将guid存到本地沙盒
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
    //        [ud synchronize];
    
    //将用户信息存储到本地沙盒
    LoginDataParser *dataPaser = parser.data;
    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
    [user setObject:userData forKey:USERINFO_LOGIN];
    [user synchronize];
    // 存储登录账号信息
    NSDictionary *accountDic = @{@"account":parser.data.mobile,@"password":passWord,@"avatar":dataPaser.logo};
    [[UserInstance ShardInstnce] saveAccount:accountDic];
    
    // 实例化user数据
    [[UserInstance ShardInstnce] setupUserInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
