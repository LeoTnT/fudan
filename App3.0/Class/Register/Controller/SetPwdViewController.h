//
//  SetPwdViewController.h
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterModel.h"

@interface SetPwdViewController : XSBaseViewController
@property (strong, nonatomic) RegisterModel *regModel;
- (id)initWithMobile:(NSString *)mobile smsVerify:(NSString *)smsVerify intro:(NSString *)intro nickName:(NSString *)nickname;
@end
