//
//  XSRegisterViewController.h
//  App3.0
//
//  Created by admin on 2018/3/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface XSRegisterViewController : XSBaseTableViewController
@property (nonatomic, strong) NSArray *areaNameArray;
@property (nonatomic, strong) NSArray *areaIdArray;//省市区
@end
