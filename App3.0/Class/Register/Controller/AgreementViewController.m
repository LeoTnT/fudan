//
//  AgreementViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/30.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AgreementViewController.h"
#import <WebKit/WebKit.h>

@interface AgreementViewController ()<WKNavigationDelegate, WKUIDelegate>
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) AgreementType agreementType;
@end

@implementation AgreementViewController

-(WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT)];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.scrollView.bounces = NO;
        _webView.scrollView.showsVerticalScrollIndicator = NO;
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBarHidden = NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    NSString *title;
    if (self.agreementType==AgreementTypeReg) {
        title =[NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"APP服务协议")];
    } else if(self.agreementType==AgreementTypeRecharge) {
        title = Localized(@"网银充值服务协议");
    }else if(self.agreementType==AgreementTypeStore){
        title =[NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"服务协议")];
    }else{
        title =[NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"软件使用许可协议")];
    }
    self.navigationItem.title = title;
    [self getAgreementInformation];
    
}

- (void)getAgreementInformation {
    NSString *type;
    if(self.agreementType == AgreementTypeRecharge){
        type = @"recharge";
    } else if(self.agreementType == AgreementTypeStore){
        type = @"supply";
    }else if(self.agreementType == AgreementTypeReg || self.agreementType == AgreementTypeUsingPermissions){
        type = @"register";
    }
//    else if (self.agreementType==AgreementTypeUsingPermissions) {
//        type = @"soft";
//    }
    @weakify(self);
    [HTTPManager getAgreementWithType:type success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            NSString *url = dic[@"data"];
            if (VUE_ON) {
                [self.webView loadHTMLString:url baseURL:[NSURL URLWithString:ImageBaseUrl]];
            } else {
                [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
            }
            
            [self.view addSubview:self.webView];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

-(instancetype)initWithAgreementType:(AgreementType)type {
    self = [super init];
    if (self) {
        self.agreementType = type;
    }
    return self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    [XSTool showProgressHUDWithView:self.view];
}

// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    //       [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
    [XSTool hideProgressHUDWithView:self.view];
    [XSTool showToastWithView:self.view Text:NetFailure];
}

// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation
{
    
}

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSLog(@"%@",navigationResponse.response.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //    decisionHandler(WKNavigationResponsePolicyCancel);
}

// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSLog(@"%@",navigationAction.request.URL.absoluteString);
    
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
    //不允许跳转
    //    decisionHandler(WKNavigationActionPolicyCancel);
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
}


@end
