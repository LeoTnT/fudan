//
//  XSRegisterViewController.m
//  App3.0
//
//  Created by admin on 2018/3/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSRegisterViewController.h"
#import "RegisterModel.h"
#import "XSRegisterCell.h"
#import "SetPwdViewController.h"
#import "AgreementViewController.h"
#import "ProvinceViewController.h"
#import "XSDatePickerView.h"

@interface XSRegisterViewController () <UITextFieldDelegate, XSDatePickerDelegate>
@property (strong, nonatomic) RegisterInfoModel *regInfoModel;
@property (strong, nonatomic) NSMutableArray *showArray;
@property (strong, nonatomic) RegisterModel *regModel;
@property (nonatomic ,strong) dispatch_source_t timer;
@property (strong, nonatomic) UIButton *nextButton;
@property (assign, nonatomic) BOOL isTimer;
@property (strong, nonatomic) XSDatePickerView *introPicker;        // 邀请码选择器
@property (strong, nonatomic)UITextField *boardField;
@end

@implementation XSRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title = Localized(@"enter_register");
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.regModel = [RegisterModel new];
    [self requestData];
    [self setSubviews];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.areaNameArray) {
        NSInteger index = [self.showArray indexOfObject:@"province"];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        XSRegisterCell *cell = (XSRegisterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.textField.text = [self.areaNameArray componentsJoinedByString:@"-"];
        
        self.regModel.province = self.areaNameArray[0];
        self.regModel.city = self.areaNameArray[1];
        self.regModel.county = self.areaNameArray[2];
        self.regModel.town = self.areaNameArray[3];
        self.regModel.province_code = self.areaIdArray[0];
        self.regModel.city_code = self.areaIdArray[1];
        self.regModel.county_code = self.areaIdArray[2];
        self.regModel.town_code = self.areaIdArray[3];
    }
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)showArray {
    if (!_showArray) {
        _showArray = [NSMutableArray array];
    }
    return _showArray;
}

- (void)requestData {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getRegisterInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        @strongify(self);
        if (state.status) {
            RegisterInfoModel *model = [RegisterInfoModel mj_objectWithKeyValues:dic[@"data"]];
            self.regInfoModel = model;
            
            self.showArray = [NSMutableArray arrayWithArray:self.regInfoModel.show];
            [self.showArray removeObject:@"agreement"];
            [self.showArray removeObject:@"verify"];
            [self.showArray removeObject:@"username"];
            [self.showArray removeObject:@"verify_sms"];
            [self.showArray removeObject:@"paypwd"];
            if ([self.showArray containsObject:@"mobile"]) {
                NSInteger index = [self.showArray indexOfObject:@"mobile"];
                [self.showArray insertObject:@"verify_sms" atIndex:index+1];
            }
            
            [self.tableView reloadData];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)setSubviews {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 150)];
    self.tableView.tableFooterView = footerView;
    
    _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_nextButton setTitle:Localized(@"bind_mobile_next") forState:UIControlStateNormal];
    [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _nextButton.titleLabel.font = SYSTEM_FONT(16);
    [_nextButton setBackgroundImage:[UIImage yy_imageWithColor:mainColor] forState:UIControlStateNormal];
    [_nextButton setBackgroundImage:[UIImage yy_imageWithColor:HighLightColor_Main] forState:UIControlStateHighlighted];
    [_nextButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    _nextButton.layer.masksToBounds = YES;
    _nextButton.layer.cornerRadius = 4;
    [footerView addSubview:_nextButton];
    [_nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(44);
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
        make.height.mas_equalTo(50);
    }];
    
    UIButton *selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setBackgroundImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [selectBtn setBackgroundImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    selectBtn.selected = YES;
    [selectBtn addTarget:self action:@selector(changeProtocol:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:selectBtn];
    self.regModel.agreement = 1;
    
    UILabel *firstLabel = [UILabel new];
    firstLabel.text = @"注册即视为同意";
    firstLabel.font = [UIFont systemFontOfSize:14];
    firstLabel.textColor = COLOR_999999;
    [footerView addSubview:firstLabel];
    
    UILabel *proLabel = [UILabel new];
    proLabel.text = [NSString stringWithFormat:@"《%@APP服务协议》",APP_NAME];
    proLabel.font = SYSTEM_FONT(14);
    proLabel.textColor = mainColor;
    proLabel.userInteractionEnabled = YES;
    [proLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookProtocol:)]];
    [footerView addSubview:proLabel];
    
    [selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nextButton);
        make.bottom.mas_equalTo(footerView).offset(-10);
    }];
    [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectBtn.mas_right).offset(9);
        make.centerY.mas_equalTo(selectBtn);
    }];
    [proLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(firstLabel.mas_right).offset(2);
        make.centerY.mas_equalTo(firstLabel);
    }];
}

- (void)nextAction:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.regModel.mobile.length == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"input_mobile")];
        return;
    }
    if (self.regModel.verify_sms.length == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"sms_send_dialog_sms_hint")];
        return;
    }
    if ([self.regInfoModel.require containsObject:@"intro"] && self.regModel.intro.length == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"register_invite_code")];
        return;
    }
    if ([self.regInfoModel.require containsObject:@"province"] && self.regModel.province.length == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"register_location")];
        return;
    }
    if ([self.regInfoModel.require containsObject:@"address"] && self.regModel.address.length == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"register_addr")];
        return;
    }
    __weak typeof(self) wSelf = self;
    [HTTPManager checkSmsVerifyValidWithMobile:self.regModel.mobile smsVerify:self.regModel.verify_sms success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 验证通过进入下一步
            SetPwdViewController *setPwd = [[SetPwdViewController alloc] init];
            setPwd.regModel = self.regModel;
            [wSelf.navigationController pushViewController:setPwd animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:Localized(@"手机号/验证码输入错误")];
    }];
}

-(void)obtainCodeAction:(UIButton *)sender{
    if (self.regModel.mobile.length != 11) {
        [XSTool showToastWithView:self.view Text:@"请输入正确的手机号码！"];
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"code_tip") message:self.regModel.mobile preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = NO; // 设置按钮为不可点击
        [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
        // 获取验证码
        [HTTPManager regGetSmsVerify:self.regModel.mobile success:^(NSDictionary *dic, resultObject *state) {
            // 验证码发送成功
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
                _isTimer = YES; // 设置倒计时状态为YES
                sender.enabled = NO; // 设置按钮为不可点击
                
                _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                    //设置按钮的样式
                    dispatch_source_cancel(_timer);
                    [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                    sender.enabled = YES; // 设置按钮可点击
                    
                    _isTimer = NO; // 倒计时状态为NO
                } otherAction:^(int time) {
                    [sender setTitle:[NSString stringWithFormat:@"重新发送( %.2d )", time] forState:UIControlStateNormal];
                }];
                
                // 验证码输入框成为第一响应者
                NSInteger index = [self.showArray indexOfObject:@"verify_sms"];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                XSRegisterCell *cell = (XSRegisterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                [cell.textField becomeFirstResponder];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                sender.enabled = YES; // 设置按钮为可点击
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
            sender.enabled = YES; // 设置按钮为可点击
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark XSDatePickerDelegate
- (void)pickerView:(XSDatePickerView *)picker ValueChanged:(NSString *)value {
    NSInteger index = [self.showArray indexOfObject:@"intro"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    XSRegisterCell *cell = (XSRegisterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.textField.text = value;
}

#pragma mark - 是否选中协议
-(void)changeProtocol:(UIButton *) sender{
    sender.selected = !sender.selected;
    self.regModel.agreement = sender.selected;
    self.nextButton.enabled = sender.selected;
}

#pragma mark - 查看协议
-(void)lookProtocol:(UITapGestureRecognizer *) tap{
    
    AgreementViewController *controller = [[AgreementViewController alloc] initWithAgreementType:AgreementTypeReg];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 返回按钮，切换响应者
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    XSRegisterCell *cell = (XSRegisterCell *)textField.superview.superview;
    if ([cell.showName isEqualToString:@"province"]) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.view endEditing:YES];
        } completion:^(BOOL finished) {
            ProvinceViewController *province = [[ProvinceViewController alloc] init];
            [self.navigationController pushViewController:province animated:YES];
        }];
        return NO;
    } else if ([cell.showName isEqualToString:@"intro"] && !self.regInfoModel.edit_intro) {
        [self.view endEditing:YES];
        NSArray *data = self.regInfoModel.introducer;
        if (!self.introPicker) {
            self.introPicker = [[XSDatePickerView alloc] init];
            self.introPicker.delegate = self;
        }
        
        [self.introPicker showData:data InView:self.view withFrame:CGRectMake(0, mainHeight-200, mainWidth, 200) withTitle:Localized(@"use_system_intros")];
        
        
        return NO;
    }
    return YES;
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.showArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 24;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSRegisterCell *cell = [XSRegisterCell createRegisterCellWithTableView:tableView];
    cell.textField.delegate = self;
    cell.showName = self.showArray[indexPath.row];
    //判断键盘谈起
    if (cell.textField.tag==1000) {
        self.boardField=cell.textField;
    }
    if ([self.regInfoModel.require containsObject:self.showArray[indexPath.row]]) {
        cell.isRequire = YES;
    }
    __weak typeof(self) weakSelf = self;
    [cell setObtainCodeAction:^(UIButton *sender) {
        [weakSelf obtainCodeAction:sender];
    }];
    
    [cell setTextDidChangedAction:^(UITextField *sender, NSString *showname) {
        if ([showname isEqualToString:@"mobile"]) {
            weakSelf.regModel.mobile = sender.text;
        } else if ([showname isEqualToString:@"intro"]) {
            // 邀请码
            weakSelf.regModel.intro = sender.text;
        } else if ([showname isEqualToString:@"nickname"]) {
            // 昵称
            weakSelf.regModel.nickname = sender.text;
        } else if ([showname isEqualToString:@"province"]) {
            // 选择地址
            
        } else if ([showname isEqualToString:@"address"]) {
            // 选择地址
            weakSelf.regModel.address = sender.text;
        } else if ([showname isEqualToString:@"verify_sms"]) {
            // 短信验证码
            weakSelf.regModel.verify_sms = sender.text;
        }
    }];
    
    [cell setHelpAction:^(UIButton *sender) {
        UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:Localized(@"invite_code_title") message:nil preferredStyle:UIAlertControllerStyleAlert];
        NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",Localized(@"可向您的推荐人索要6位数推荐邀请码，如无推荐人，可输入平台默认邀请码888888进行注册")]];
        
        NSString *language = SelectLang;
        if (isEmptyString(language)) {
            language = @"zh-Hans";//简体中文
        if ([language isEqualToString:@"zh-Hans"]) {
            //字体
            [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, 45)];
            [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:COLOR_666666 range:NSMakeRange(0, 45)];
            //修改888888文字
            [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"3f8ef7"] range:NSMakeRange(35, 6)];
            
        }
        }
        
        
        [alertVC setValue:alertControllerMessageStr forKey:@"attributedMessage"];
        UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        if(NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_9_0) {

        [conAct setValue:[UIColor hexFloatColor:@"3f8ef7"] forKey:@"titleTextColor"];
        }
        [alertVC addAction:conAct];
        
        [weakSelf presentViewController:alertVC animated:YES completion:nil];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
#pragma mark-键盘处理
-(void)keyboardWillShow:(NSNotification *)notification
{
    if([self.boardField isFirstResponder]){
        NSDictionary *info = [notification userInfo];
        CGFloat duration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
        CGSize keyboardSize = [value CGRectValue].size;
        
        //输入框位置动画加载
        [UIView animateWithDuration:duration animations:^{
            [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(-keyboardSize.height, 0, keyboardSize.height, 0));
            }];
        }];
    }
}
-(void)keyboardDidShow:(NSNotification *)notification{
    if([self.boardField isFirstResponder]){
        NSDictionary *info = [notification userInfo];
        CGFloat duration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
        CGSize keyboardSize = [value CGRectValue].size;
        
        //输入框位置动画加载
        [UIView animateWithDuration:duration animations:^{
            [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(-keyboardSize.height, 0, keyboardSize.height, 0));
            }];
        }];
    }
}
-(void)keyboardWillHidden:(NSNotification *)notification
{
    [UIView animateWithDuration:0.25 animations:^{
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }];
}

@end

