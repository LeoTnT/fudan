//
//  XSRegisterCell.h
//  App3.0
//
//  Created by admin on 2018/3/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSRegisterCell : UITableViewCell
@property (strong, nonatomic) UITextField *textField;
@property (copy, nonatomic) NSString *showName;
@property (assign, nonatomic) BOOL isRequire;
@property (nonatomic ,copy) void (^helpAction)(UIButton *sender);
@property (nonatomic ,copy) void (^obtainCodeAction)(UIButton *sender);
@property (nonatomic ,copy) void (^textDidChangedAction)(UITextField *sender, NSString *showname);
+ (instancetype)createRegisterCellWithTableView:(UITableView *)tableView;
@end
