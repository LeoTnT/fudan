//
//  AccountCell.h
//  App3.0
//
//  Created by mac on 2017/5/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCell : UITableViewCell
@property (nonatomic,copy)void (^deleteAccountAction)();
@end
