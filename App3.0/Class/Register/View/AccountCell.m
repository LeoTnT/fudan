//
//  AccountCell.m
//  App3.0
//
//  Created by mac on 2017/5/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AccountCell.h"

@implementation AccountCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [delBtn setBackgroundImage:[UIImage imageNamed:@"alertViewClose"] forState:UIControlStateNormal];
        [delBtn addTarget:self action:@selector(deleteAccount) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:delBtn];
        delBtn.frame = CGRectMake(mainWidth-2*25-14, (50-11)/2.0, 11, 11);
        //分割线
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 49.5, mainWidth-2*25, 0.5)];
        [self.contentView addSubview:line];
        line.backgroundColor=[UIColor hexFloatColor:@"dddddd"];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
//    self.imageView.bounds =CGRectMake(10,5,40,40);
    self.imageView.frame =CGRectMake(0,7,36,36);
    self.imageView.contentMode =UIViewContentModeScaleAspectFit;
    self.
//    CGRect tmpFrame = self.textLabel.frame;
//    tmpFrame.origin.x = 60;
//    self.textLabel.frame = tmpFrame;
    self.textLabel.font=[UIFont systemFontOfSize:15];
    self.textLabel.frame=CGRectMake(CGRectGetMaxX(self.imageView.frame)+11, 0, 200,50);
//    tmpFrame = self.detailTextLabel.frame;
//    tmpFrame.origin.x = 60;
//    self.detailTextLabel.frame = tmpFrame;
}

- (void)deleteAccount {
    if (_deleteAccountAction) {
        _deleteAccountAction();
    }
}
@end
