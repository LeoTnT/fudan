//
//  SerialCodeView.m
//  App3.0
//
//  Created by mac on 2017/3/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SerialCodeView.h"

#define SPACE_FIELD 10
#define HEIGHT_FIELD 50
@implementation SerialCodeView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 5;
        self.layer.borderWidth = 1;
        self.layer.borderColor = LINE_COLOR.CGColor;
        //设置子视图
        [self addSubViews];
        
    }
    return self;
    
}


#pragma mark - 设置子视图
-(void)addSubViews{
    
    CGFloat width = self.frame.size.width/6;
    //6个输入框
    for(int i=0;i<6;i++){
        
        UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(i*width, 0, width, HEIGHT_FIELD)];
        
        //分割线
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(i*width+1, 0, 1, HEIGHT_FIELD)];
        lineView.backgroundColor = LINE_COLOR;
        [self addSubview:lineView];
        [self addSubview:textField];
    }
    
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


@end
