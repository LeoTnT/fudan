//
//  XSRegisterCell.m
//  App3.0
//
//  Created by admin on 2018/3/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSRegisterCell.h"
#import "AppDelegate.h"

#define phonePrefix   @"1[3|4|5|7|8|][0-9]{9}"

@interface XSRegisterCell ()
@property (strong, nonatomic) UIImageView *leftImageView;
@property (strong, nonatomic) UIButton *rightButton;
@end

@implementation XSRegisterCell

+ (instancetype)createRegisterCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"XSRegisterCell";
    XSRegisterCell *cell = (XSRegisterCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[XSRegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    UIView *lineView = [UIView new];
    lineView.backgroundColor = Color(@"DDDDDD");
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
        make.bottom.mas_equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    _leftImageView = [[UIImageView alloc] init];
    [leftView addSubview:_leftImageView];
    
//    _textField.rightViewMode = UITextFieldViewModeAlways;
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 32)];
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightView addSubview:_rightButton];
    
    
 
    _textField = [UITextField new];
    _textField.font = SYSTEM_FONT(15);
    _textField.leftViewMode = UITextFieldViewModeAlways;
    _textField.leftView = leftView;
    _textField.rightView = rightView;
    _textField.returnKeyType=UIReturnKeyDone;
    [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(lineView);
        make.bottom.mas_equalTo(lineView.mas_top);
        make.height.mas_equalTo(40);
    }];
    
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView);
        make.centerY.mas_equalTo(leftView);
    }];
    
    [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightView);
        make.centerY.mas_equalTo(rightView);
    }];
}

- (void)setShowName:(NSString *)showName {
    _showName = showName;
    
    if ([showName isEqualToString:@"intro"]) {
        // 邀请码
        self.leftImageView.image = [UIImage imageNamed:@"reg_intro"];
        self.textField.placeholder = Localized(@"register_invite_code");
        
        [self.rightButton setBackgroundImage:[UIImage imageNamed:@"reg_help"] forState:UIControlStateNormal];
        [self.rightButton addTarget:self action:@selector(helpClick:) forControlEvents:UIControlEventTouchUpInside];
        self.textField.rightViewMode = UITextFieldViewModeAlways;
    } else if ([showName isEqualToString:@"nickname"]) {
        // 昵称
        self.leftImageView.image = [UIImage imageNamed:@"login_phone"];
        self.textField.placeholder = Localized(@"register_name");
    } else if ([showName isEqualToString:@"mobile"]) {
        // 手机号
        self.leftImageView.image = [UIImage imageNamed:@"login_phone"];
        self.textField.placeholder = Localized(@"请输入11位手机号码");
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        
        [self.rightButton setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
        [self.rightButton setTitleColor:[UIColor hexFloatColor:@"c0c0c0"] forState:UIControlStateDisabled];
        [self.rightButton setTitleColor:mainColor forState:UIControlStateNormal];
        self.rightButton.titleLabel.font=[UIFont systemFontOfSize:15];
        [self.rightButton addTarget:self action:@selector(obtainCodeClick:) forControlEvents:UIControlEventTouchUpInside];
        self.textField.rightViewMode = UITextFieldViewModeAlways;
        
    } else if ([showName isEqualToString:@"province"]) {
        // 选择地区
        self.leftImageView.image = [UIImage imageNamed:@"reg_location"];
        self.textField.placeholder = Localized(@"register_location");
    } else if ([showName isEqualToString:@"address"]) {
        self.textField.tag=1000;
        // 详细地址
        self.leftImageView.image = [UIImage imageNamed:@"reg_location"];
        self.textField.placeholder = Localized(@"register_addr");
    } else if ([showName isEqualToString:@"verify_sms"]) {
        // 短信验证码
        self.leftImageView.image = [UIImage imageNamed:@"login_code"];
        self.textField.placeholder = Localized(@"sms_send_dialog_sms_hint");
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    if (self.isRequire) {
        self.textField.placeholder = [self.textField.placeholder stringByAppendingString:Localized(@"(选填)")];
    }
    
}

- (void)obtainCodeClick:(UIButton *)sender {
    if (self.obtainCodeAction) {
        self.obtainCodeAction(sender);
    }
}

- (void)helpClick:(UIButton *)sender {
    if (self.helpAction) {
        self.helpAction(sender);
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{
//    if ([self.showName isEqualToString:@"mobile"]) {
//        if (textField.text.length == 11) {
//            if (!self.rightButton.selected) {
//                self.rightButton.enabled = YES;
//            } else {
//                self.rightButton.enabled = NO;
//            }
//        } else {
//            self.rightButton.enabled = NO;
//        }
//
//    }
    if (self.textDidChangedAction) {
        self.textDidChangedAction(textField, self.showName);
    }
}

- (BOOL)validatePhone:(NSString *)phonestr {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phonePrefix];
    BOOL isphoneValidate = [predicate evaluateWithObject:phonestr];
    return isphoneValidate;
}
@end
