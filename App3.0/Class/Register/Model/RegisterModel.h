//
//  RegisterModel.h
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterDataParser : NSObject
@property (nonatomic,copy)NSString* username;
@property (nonatomic,copy)NSString* id;
@end

/* 注册项
 |--intro    string    邀请码
 |--mobile    string    手机号
 |--nickname    string    昵称
 |--province    string    区域
 |--address    string    详细地址
 |--agreement    string    协议
 |--verify    string    图形验证码
 |--verify_sms    string    短信验证码
 */
@interface RegisterInfoModel : NSObject
@property (nonatomic, strong) NSArray *show;        // 需要显示的注册项
@property (nonatomic, strong) NSArray *require;     // 必须的注册项
@property (nonatomic, strong) NSArray *introducer;  // 系统推荐的邀请码
@property (nonatomic, copy) NSString *intro;        // 未知
@property (nonatomic, copy) NSString *username;        // 分配的用户编号
@property (nonatomic, assign) unsigned int edit_intro;        // 邀请码是否可修改
@property (nonatomic, assign) unsigned int edit_username;        // 用户编号是否可修改
@property (nonatomic, assign) unsigned int is_show_agreement;        // 协议是否弹窗
@end

@interface RegisterParser : NSObject
@property (nonatomic,strong)RegisterDataParser *data;
@end

@interface RegisterModel : NSObject
@property (nonatomic, copy) NSString *intro;        // 邀请码  必须
@property (nonatomic, copy) NSString *username;        // 用户编号[根据后台配置，可有可无]
@property (nonatomic, copy) NSString *mobile;        // 手机号[根据后台配置或唯一或必填]
@property (nonatomic, copy) NSString *nickname;        //     昵称[根据后台配置]
@property (nonatomic, copy) NSString *verify_img;        // 图形验证码
@property (nonatomic, copy) NSString *verify_sms;        // 短信验证码
@property (nonatomic, copy) NSString *passwd_one;        // 密码  必须
@property (nonatomic, copy) NSString *passwd_two;        // 重复密码
@property (nonatomic, copy) NSString *pay_password;        // 支付密码
@property (nonatomic, copy) NSString *rePwd;        // 重复支付密码
@property (nonatomic, copy) NSString *province;        // 省份[根据后台配置]
@property (nonatomic, copy) NSString *city;        // 城市[根据后台配置]
@property (nonatomic, copy) NSString *county;        // 县[根据后台配置]
@property (nonatomic, copy) NSString *town;        // 手镇[根据后台配置]
@property (nonatomic, copy) NSString *province_code;        // 省编码[根据后台配置]
@property (nonatomic, copy) NSString *city_code;        // 省编码[根据后台配置]
@property (nonatomic, copy) NSString *county_code;        // 省编码[根据后台配置]
@property (nonatomic, copy) NSString *town_code;        // 街镇编码[根据后台配置]
@property (nonatomic, copy) NSString *address;        // 详细地址
@property (nonatomic, assign) unsigned int agreement;        // 协议  必须
@end
