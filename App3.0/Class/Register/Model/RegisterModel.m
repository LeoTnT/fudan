//
//  RegisterModel.m
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RegisterModel.h"

@implementation RegisterDataParser

@end

@implementation RegisterParser

@end

@implementation RegisterInfoModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"show" : @"NSString",
             @"require" : @"NSString",
             @"introducer" : @"NSString"
             };
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"username":@"new_username"};
}
@end

@implementation RegisterModel

@end
