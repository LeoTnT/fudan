//
//  HistoryViewCell.h
//  App3.0
//
//  Created by mac on 2017/11/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchModel.h"

@interface HistoryViewCell : UICollectionViewCell
@property (strong, nonatomic) SearchHistoryItem *item;

- (CGSize)sizeForCell;
@end
