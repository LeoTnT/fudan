//
//  SearchBarView.h
//  App3.0
//
//  Created by mac on 17/3/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SearchBarStyle) {
    SearchBarStyleRoundedRect, // 普通的搜索栏
    SearchBarStyleLine,        // 带下划线的搜索栏
    SearchBarStyleRoundedRectNoButton,//不带搜索按钮
    SearchBarStyleMall,        // 商城搜索风格，带宝贝和店铺选择
};

typedef NS_ENUM(NSInteger, SearchType) {
    SearchProduct = 1,
    SearchStore
};

@protocol SearchBarViewDelegate <NSObject>

- (void)search:(NSString *)text;
- (void)searchTextChanged:(NSString *)text;
- (void)searchTypeChanged:(UIButton *)searchButton;
- (void)searchCancel;
@end

@interface SearchBarView : UIView
@property (nonatomic, weak) id<SearchBarViewDelegate>delegate;
@property (strong, nonatomic) UIButton *searchTypeButton;

@property (nonatomic ,assign)SearchType searchLeftType;

// 搜索栏风格
@property(nonatomic)        SearchBarStyle       searchBarStyle;
@property (nonatomic, strong) UITextField *textField;


@end


@interface StoreSearchView :BaseView

@property (nonatomic ,assign)SearchType searchLeftType;

@property(nonatomic)        SearchBarStyle       searchBarStyle;

@property (nonatomic ,copy)NSString *leftTitle;

@property (nonatomic ,copy)NSString *placeHolder;
@end

