//
//  SearchBarView.m
//  App3.0
//
//  Created by mac on 17/3/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SearchBarView.h"
#import "XSCustomButton.h"
#import <REMenu/REMenu.h>


@interface SearchBarView() <UITextFieldDelegate>
{
    __weak id<SearchBarViewDelegate>delegate;
}
//@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) XSCustomButton *searchBtn;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UITextField *textFieldLine;
@property (strong, nonatomic) REMenu *reMenu;
@end
@implementation SearchBarView
@synthesize delegate;
//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self setSearchBarStyle:self.searchBarStyle];
//    }
//    return self;
//}

- (void)textFieldDidChange:(UITextField *)textField;
{
//    if ([textField isEqual:self.textFieldLine]) {
        if ([delegate respondsToSelector:@selector(searchTextChanged:)]) {
            [delegate searchTextChanged:textField.text];
        }
//    }
}

- (void)searchAction:(XSCustomButton *)sender
{
    if ([delegate respondsToSelector:@selector(search:)]) {
        [delegate search:self.textField.text];
    }
}

- (void)cancelAction:(UIButton *)sender {
    if ([delegate respondsToSelector:@selector(searchCancel)]) {
        [delegate searchCancel];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.textField]) {
        if ([delegate respondsToSelector:@selector(search:)]) {
            [delegate search:self.textField.text];
        }
    } else if ([textField isEqual:self.textFieldLine]) {
        [self endEditing:YES];
    }
    
    return YES;
}

#pragma mark - set
- (void)setSearchBarStyle:(SearchBarStyle)searchBarStyle
{
    switch (searchBarStyle) {
        case SearchBarStyleRoundedRect:
        {
            [self addSubview:self.textField];
            [self addSubview:self.searchBtn];
            
        }
            break;
        case SearchBarStyleLine:
        {
            [self addSubview:self.textField];
            [self addSubview:self.cancelBtn];
            [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self);
                make.right.mas_equalTo(self.cancelBtn.mas_left).offset(-10);
                make.centerY.mas_equalTo(self);
                make.height.mas_equalTo(self);
            }];
            
            [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self).offset(-12);
                make.centerY.mas_equalTo(self);
            }];
//            [self addSubview:self.textFieldLine];
//            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.textFieldLine.frame.size.height-1, self.textFieldLine.frame.size.width, 1)];
//            bottomView.backgroundColor = mainGrayColor;
//            [self.textFieldLine addSubview:bottomView];
        }
            break;
        case SearchBarStyleRoundedRectNoButton:
        {
            [self addSubview:self.textField];
            [self.textField  resignFirstResponder];
            self.textField.enabled = NO;
            
        }
            break;
        case SearchBarStyleMall:
        {
            self.textField.leftView = self.searchTypeButton;
            self.textField.placeholder = @"";
            [self addSubview:self.textField];
            [self addSubview:self.searchBtn];
//            self.textField.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
//            [self.searchBtn removeFromSuperview];
        }
            break;
        default:
            break;
    }
}

- (void)chooseSearchType:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(searchTypeChanged:)]) {
        [delegate searchTypeChanged:self.searchTypeButton];
    }
}


#pragma mark - life scyle

- (UITextField *)textFieldLine
{
    if (_textFieldLine == nil) {
        _textFieldLine = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-10, self.frame.size.height)];
        _textFieldLine.placeholder = Localized(@"search");
        _textFieldLine.font = [UIFont systemFontOfSize:13];
        //_textFieldLine.backgroundColor = BG_COLOR;
        _textFieldLine.borderStyle = UITextBorderStyleNone;
        _textFieldLine.delegate = self;
        _textFieldLine.returnKeyType = UIReturnKeyDone;
        _textFieldLine.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _textFieldLine.frame.size.height, _textFieldLine.frame.size.height)];
        UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chat_search"]];
//        leftImg.frame = CGRectMake(5, 5, _textFieldLine.frame.size.height-10, _textFieldLine.frame.size.height-10);
        [leftView addSubview:leftImg];
        [leftImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(leftView);
            make.centerY.mas_equalTo(leftView);
        }];
        
        
        
        _textFieldLine.leftView = leftView;
        _textFieldLine.leftViewMode = UITextFieldViewModeAlways;
        _textFieldLine.delegate = self;
        [_textFieldLine addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [_textFieldLine becomeFirstResponder];
    }
    return _textFieldLine;
}

- (UITextField *)textField
{
    if (_textField == nil) {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-70, self.frame.size.height)];
        _textField.placeholder = Localized(@"search");
      
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.backgroundColor = BG_COLOR;
        _textField.layer.masksToBounds = YES;
        _textField.layer.cornerRadius = 3;
        _textField.delegate = self;
        _textField.returnKeyType = UIReturnKeySearch;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _textField.frame.size.height, _textField.frame.size.height)];
        UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_nav_search"]];
//        leftImg.frame = CGRectMake(5, 5, _textField.frame.size.height-10, _textField.frame.size.height-10);
        [leftView addSubview:leftImg];
        [leftImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(leftView);
            make.centerY.mas_equalTo(leftView);
        }];
        
        _textField.leftView = leftView;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [_textField becomeFirstResponder];
    }
    return _textField;
}

- (XSCustomButton *)searchBtn
{
    if (_searchBtn == nil) {
        _searchBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(self.frame.size.width-60, 0, 60, self.frame.size.height) title:Localized(@"search") titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [_searchBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:3];
        [_searchBtn addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBtn;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelBtn setTitle:Localized(@"cancel_btn") forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:mainColor forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_cancelBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _cancelBtn;
}

- (UIButton *)searchTypeButton
{
    if (_searchTypeButton == nil) {
        _searchTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 3, 50, 28)];
        [_searchTypeButton setBackgroundImage:[UIImage imageNamed:@"search-sj"] forState:UIControlStateNormal];
        [_searchTypeButton setTitle:@"宝贝" forState:UIControlStateNormal];
        _searchTypeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _searchTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _searchTypeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        [_searchTypeButton setTitleColor:[UIColor hexFloatColor:@"646464"] forState:UIControlStateNormal];
        [_searchTypeButton addTarget:self action:@selector(chooseSearchType:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchTypeButton;
}

- (REMenu *)reMenu
{
    if (_reMenu == nil) {
        REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:@"宝贝"
                                                        subtitle:nil
                                                           image:[UIImage imageNamed:@"search-sp"]
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item) {
                                                              NSLog(@"Item: %@", item);
                                                              [self.searchTypeButton setTitle:@"宝贝" forState:UIControlStateNormal];
                                                              
                                                          }];
        
        REMenuItem *exploreItem = [[REMenuItem alloc] initWithTitle:@"店铺"
                                                           subtitle:nil
                                                              image:[UIImage imageNamed:@"search-dp"]
                                                   highlightedImage:nil
                                                             action:^(REMenuItem *item) {
                                                                 NSLog(@"Item: %@", item);
                                                                 [self.searchTypeButton setTitle:@"店铺" forState:UIControlStateNormal];
                                                                 
                                                             }];
        
        _reMenu = [[REMenu alloc] initWithItems:@[homeItem, exploreItem]];
//        _reMenu.backgroundAlpha = 0.5;
        _reMenu.backgroundColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:0.8];
        _reMenu.font = [UIFont systemFontOfSize:16];
        _reMenu.textColor = [UIColor whiteColor];
        _reMenu.textOffset = CGSizeMake(10, 0);
    }
    return _reMenu;
}
@end


@implementation StoreSearchView

- (void)setContentView {
    [super setContentView];
    
    UITextField *textField = [BaseUITool createTextFieldWith:self.bounds leftViewFrame:CGRectMake(0, 0, 80, self.mj_h) placeHolder:@"请输入店铺" leftTitle:@"店铺" leftImageView:@"clear_icon" textAlignment:0 keyboardType:0 tapLeftAction:^{
    
        NSLog(@" - - - - - - -");
        
    }];
    
    self.backgroundColor = [UIColor hexFloatColor:@"f4f4f4f"];
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
    [self addSubview:textField];
    
}


@end
