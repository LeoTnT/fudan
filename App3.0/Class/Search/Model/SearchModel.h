//
//  SearchModel.h
//  App3.0
//
//  Created by mac on 17/3/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchUserDataParser : NSObject
@property(nonatomic,copy) NSString *uid;
@property(nonatomic,copy) NSString *username;
@property(nonatomic,copy) NSString *nickname;
@property(nonatomic,copy) NSString *avatar;
@property(nonatomic,copy) NSString *shop_name;
@property(nonatomic,copy) NSString *shop_status;
@property(nonatomic,strong) NSNumber *relation;
@end

@interface SearchUserParser : NSObject
@property(nonatomic,strong) NSArray *data;
@end

@interface SearchHistoryItem : NSObject
@property(nonatomic, copy) NSString *keyword;
@property(nonatomic, assign) unsigned int type;   // 商品还是商家 1：商品 2：商家
@end

@interface SearchModel : NSObject
+ (void)addHistoryItem:(SearchHistoryItem *)item;
+ (NSArray *)getAllHistory;
+ (void)deleteAllHistory;
@end
