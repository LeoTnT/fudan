//
//  SearchModel.m
//  App3.0
//
//  Created by mac on 17/3/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchUserDataParser

@end

@implementation SearchUserParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"SearchUserDataParser"
             };
}
@end

@implementation SearchHistoryItem
@end

@implementation SearchModel
+ (NSArray *)getAllHistory {
    NSMutableArray *mutCopyArr = [NSMutableArray array];
    NSArray *all = [[NSUserDefaults standardUserDefaults] arrayForKey:SEARCH_HISTORY_LIST];
    for (NSDictionary *dic in all) {
        SearchHistoryItem *tItem = [SearchHistoryItem mj_objectWithKeyValues:dic];
        [mutCopyArr addObject:tItem];
    }
    return mutCopyArr;
}

+ (void)deleteAllHistory {
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:SEARCH_HISTORY_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)addHistoryItem:(SearchHistoryItem *)item {
    NSArray *listArr = [[NSUserDefaults standardUserDefaults] arrayForKey:SEARCH_HISTORY_LIST];
    NSMutableArray *mutCopyArr = [NSMutableArray arrayWithArray:listArr];
    for (NSDictionary *dic in listArr) {
        SearchHistoryItem *tItem = [SearchHistoryItem mj_objectWithKeyValues:dic];
        if ([tItem.keyword isEqualToString:item.keyword]) {
            [mutCopyArr removeObject:dic];
            break;
        }
    }
    NSLog(@"%@",mutCopyArr);
    [mutCopyArr insertObject:item.mj_keyValues atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:mutCopyArr forKey:SEARCH_HISTORY_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
