//
//  SearchVC.m
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SearchVC.h"
#import "SearchBarView.h"
#import "SearchModel.h"
#import "AttentionCell.h"
#import "PersonalViewController.h"
#import "ProductListViewController.h"
#import "S_StoreCollectionView.h"
#import "PersonViewController.h"
#import <REMenu/REMenu.h>
#import "HistoryViewCell.h"

@interface SearchVC ()<SearchBarViewDelegate, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
{
    SearchBarView *_searchView;
    SearchBarStyle _style;
    UIView *_hotSearchView;
    UILabel *_hotSearchLabel;
    
    NSMutableArray *_userSearchArr;
    NSString *_searchStr;
}
@property (strong, nonatomic) REMenu *reMenu;
@property (strong, nonatomic) UIButton *searchTypeButton;
@property (assign, nonatomic) SearchType searchtype;

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *historyDataArray;
@property (strong, nonatomic) HistoryViewCell *cell;
@end

@implementation SearchVC
- (instancetype)initWithSearchBarStyle:(SearchBarStyle)style
{
    self = [super init];
    if (self) {
        _style = style;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tableViewStyle = UITableViewStylePlain;

    self.view.backgroundColor = BG_COLOR;
    self.tableView.backgroundColor = BG_COLOR;
    
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    if (_style == SearchBarStyleLine) {
        [self actionCustomLeftBtnWithNrlImage:nil htlImage:nil title:nil action:nil];
    }
    
    
    _userSearchArr = [NSMutableArray array];
    _searchtype = SearchProduct;
    
    // 添加自动显示的搜索按钮
    _hotSearchView = [[UIView alloc] init];
    _hotSearchView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_hotSearchView];
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.image = [UIImage imageNamed:@"search_big"];
    [_hotSearchView addSubview:imgView];
    _hotSearchLabel = [[UILabel alloc] init];
    _hotSearchLabel.font = [UIFont systemFontOfSize:16];
    _hotSearchLabel.textColor = [UIColor blackColor];
    [_hotSearchView addSubview:_hotSearchLabel];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [_hotSearchView addGestureRecognizer:tap];
    _hotSearchView.hidden = YES;
    
    [_hotSearchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.height.mas_equalTo(68);
    }];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_hotSearchView).offset(11.5);
        make.centerY.mas_equalTo(_hotSearchView);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [_hotSearchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgView.mas_right).offset(8);
        make.centerY.mas_equalTo(_hotSearchView);
        make.right.mas_equalTo(_hotSearchView).offset(-12);
    }];
    
    if (_style == SearchBarStyleMall) {
        UILabel *headerView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
        headerView.backgroundColor = [UIColor whiteColor];
        headerView.text = @"搜索历史";
        headerView.textColor = COLOR_666666;
        headerView.font = [UIFont systemFontOfSize:14];
        self.tableView.tableHeaderView = headerView;

        UIButton *clear = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
        clear.backgroundColor = [UIColor whiteColor];
        [clear setTitle:@"清空历史记录" forState:UIControlStateNormal];
        clear.titleLabel.font = [UIFont systemFontOfSize:14];
        [clear setTitleColor:COLOR_666666 forState:UIControlStateNormal];
        [clear addTarget:self action:@selector(clearHistory) forControlEvents:UIControlEventTouchUpInside];
        self.tableView.tableFooterView = clear;

    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//     self.navigationController.navigationBarHidden = NO;
    _searchView = [[SearchBarView alloc] initWithFrame:CGRectMake(40, 7, mainWidth-50, 30)];
    _searchView.searchBarStyle = _style;
    _searchtype = SearchProduct;
    if (_style == SearchBarStyleLine) {
        _searchView.frame = CGRectMake(12, 7, mainWidth-24, 30);
    }
    _searchView.delegate = self;
    [self.navigationController.navigationBar addSubview:_searchView];
    
    // 加载历史记录
    [self.historyDataArray removeAllObjects];
    [self.historyDataArray addObjectsFromArray:[SearchModel getAllHistory]];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_searchView) {
        [_searchView removeFromSuperview];
    }
}

- (void)tapAction
{
    [self.view endEditing:YES];
    [_searchView endEditing:YES];
    // 搜索联系人
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager searchUserWithString:_searchStr success:^(NSDictionary * dic, resultObject *state) {
        _hotSearchView.hidden = YES;
        [XSTool hideProgressHUDWithView:self.view];
        SearchUserParser *parser = [SearchUserParser mj_objectWithKeyValues:dic];
        if (state.status) {
            // 搜索成功
            @strongify(self);
            if ([parser.data count]) {
                SearchUserDataParser *dataP = parser.data[0];
                if ([dataP.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
                    PersonViewController *personVC = [[PersonViewController alloc] init];
                    [self.navigationController pushViewController:personVC animated:YES];
                } else {
                    PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:dataP.uid];
                    [self.navigationController pushViewController:perVC animated:YES];
                }
            }else{
                [XSTool showToastWithView:self.view Text:@"没有该用户"];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        _hotSearchView.hidden = YES;
        [XSTool showToastWithView:self.view Text:@"搜索失败"];
        [XSTool hideProgressHUDWithView:self.view];
    }];
}

- (void)searchAction:(UIButton *)sender
{
    
}

#pragma mark -  SearchBarViewDelegate
- (void)search:(NSString *)text
{
    [self.view endEditing:YES];
    [_searchView endEditing:YES];
    switch (_style) {
        case SearchBarStyleMall:
        {
            if (self.searchtype == SearchProduct) {
                if (!isEmptyString(text)) {
                    SearchHistoryItem *item = [SearchHistoryItem new];
                    item.keyword = text;
                    item.type = 1;
                    [SearchModel addHistoryItem:item];
                }
                
                ProductListViewController *pVC = [[ProductListViewController alloc] initWithKeyword:text];
                [self.navigationController pushViewController:pVC animated:YES];
            } else {
                S_StoreCollectionView *storeVC = [S_StoreCollectionView new];
                storeVC.searchText = text;
                [self.navigationController pushViewController:storeVC animated:YES];
            }
            
        }
            break;
        case SearchBarStyleLine:
        {
            if (!isEmptyString(_searchStr)) {
                [self tapAction];
            }
        }
            break;
        default:
            break;
    }
    
    if (self.searchAction) {
        self.searchAction(text);
    }
}

- (void)searchCancel {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchTextChanged:(NSString *)text
{
    if (_style == SearchBarStyleLine && ![text isEqualToString:@""]) {
        
        [_userSearchArr removeAllObjects];
        // 模糊搜索本地数据库
        NSArray *resultArray = [[DBHandler sharedInstance] getContactByKeyword:text];
        if (resultArray != nil && resultArray.count > 0) {
            [_userSearchArr addObjectsFromArray:resultArray];
            [self.tableView reloadData];
            _hotSearchView.hidden = YES;
        } else{
            [_userSearchArr removeAllObjects];
            [self.tableView reloadData];
            _searchStr = text;
            _hotSearchView.hidden = NO;
            _hotSearchLabel.text = [NSString stringWithFormat:@"搜索：%@",text];
        }
        
    } else {
        _hotSearchView.hidden = YES;
        [_userSearchArr removeAllObjects];
        [self.tableView reloadData];
    }
}

- (void)searchTypeChanged:(UIButton *)searchButton
{
    self.searchTypeButton = searchButton;
    if (self.reMenu.isOpen) {
        return [self.reMenu close];
    }
    CGRect rect = [self.searchTypeButton convertRect:self.searchTypeButton.bounds toView:self.view];
    [self.reMenu showFromRect:CGRectMake(rect.origin.x, rect.origin.y+33, 100, 100) inView:self.view];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_style == SearchBarStyleMall) {
        return self.historyDataArray.count;
    }
    return _userSearchArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_style == SearchBarStyleMall) {
        return 44;
    }
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_style == SearchBarStyleMall) {
        SearchHistoryItem *item = self.historyDataArray[indexPath.row];
        [self search:item.keyword];
        return;
    }
    ContactDataParser *userParser = _userSearchArr[indexPath.row];
    if ([userParser.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
        PersonViewController *personVC = [[PersonViewController alloc] init];
        [self.navigationController pushViewController:personVC animated:YES];
    } else {
        PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:userParser.uid];
        [self.navigationController pushViewController:perVC animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_style == SearchBarStyleMall) {
        static NSString *CellIdentifier = @"searchHistoryCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
        }
        SearchHistoryItem *item = self.historyDataArray[indexPath.row];
        cell.textLabel.text = item.keyword;
        return cell;
    }
    static NSString *CellIdentifier = @"searchCell";
    AttentionCell *cell = (AttentionCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[AttentionCell
                 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    ContactDataParser *parser = _userSearchArr[indexPath.row];
    [cell setCellData:parser];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_searchView endEditing:YES];
}

- (REMenu *)reMenu
{
    if (_reMenu == nil) {
        REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:@"宝贝"
                                                        subtitle:nil
                                                           image:[UIImage imageNamed:@"search-sp"]
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item) {
                                                              NSLog(@"Item: %@", item);
                                                              [self.searchTypeButton setTitle:@"宝贝" forState:UIControlStateNormal];
                                                              _searchtype = SearchProduct;
                                                          }];
        
        REMenuItem *exploreItem = [[REMenuItem alloc] initWithTitle:@"店铺"
                                                           subtitle:nil
                                                              image:[UIImage imageNamed:@"search-dp"]
                                                   highlightedImage:nil
                                                             action:^(REMenuItem *item) {
                                                                 NSLog(@"Item: %@", item);
                                                                 [self.searchTypeButton setTitle:@"店铺" forState:UIControlStateNormal];
                                                                 _searchtype = SearchStore;
                                                             }];
        
        _reMenu = [[REMenu alloc] initWithItems:@[homeItem, exploreItem]];
        //        _reMenu.backgroundAlpha = 0.5;
        _reMenu.backgroundColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:0.8];
        _reMenu.font = [UIFont systemFontOfSize:16];
        _reMenu.textColor = [UIColor whiteColor];
        _reMenu.textOffset = CGSizeMake(10, 0);
    }
    return _reMenu;
}

#pragma  mark - history
- (NSMutableArray *)historyDataArray {
    if (!_historyDataArray) {
        _historyDataArray = [NSMutableArray array];
    }
    return _historyDataArray;
}

- (void)clearHistory {
    [self.historyDataArray removeAllObjects];
    [SearchModel deleteAllHistory];
    [self.tableView reloadData];
}

static NSString *historyViewCellId = @"historyViewCellId";
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        //此处必须要有创见一个UICollectionViewFlowLayout的对象
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
        //同一行相邻两个cell的最小间距
        layout.minimumInteritemSpacing = 0.5;
        //最小两行之间的间距
        layout.minimumLineSpacing = 0.5;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        [_collectionView registerClass:[HistoryViewCell class] forCellWithReuseIdentifier:historyViewCellId];
        
    }
    return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.historyDataArray.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HistoryViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:historyViewCellId forIndexPath:indexPath];
    SearchHistoryItem *item = self.historyDataArray[indexPath.row];
    cell.item = item;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_cell == nil) {
        _cell = [[HistoryViewCell alloc] initWithFrame:CGRectZero];//[[NSBundle mainBundle]loadNibNamed:@"HistoryViewCell" owner:nil options:nil][0];
    }
    SearchHistoryItem *item = self.historyDataArray[indexPath.row];
    _cell.item = item;
    return [_cell sizeForCell];
}
@end
