//
//  SearchVC.h
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchBarView.h"

@interface SearchVC : XSBaseTableViewController
@property (nonatomic ,copy) void (^ searchAction)(NSString *text);

- (instancetype)initWithSearchBarStyle:(SearchBarStyle)style;

@end
