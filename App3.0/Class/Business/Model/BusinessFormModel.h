//
//  BusinessFormModel.h
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessFormListParser : NSObject
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessFormListDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *supply_username;
@property (nonatomic, copy) NSString *store_name;
@property (nonatomic, copy) NSString *delivery_id;
@property (nonatomic, copy) NSString *is_private;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *transmit_time;
@property (nonatomic, copy) NSString *confirm_time;
@property (nonatomic, copy) NSString *app_time;
@property (nonatomic, copy) NSString *return_time;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *pay_info;
@property (nonatomic, copy) NSString *pay_yunfei;
@property (nonatomic, copy) NSString *number_total;
@property (nonatomic, copy) NSString *ck_time;
@property (nonatomic, copy) NSString *suc_time;
@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *reward_coupon_total;
@property (nonatomic, copy) NSString *reward_score_total;
@property (nonatomic, copy) NSString *pay_no;
@property (nonatomic, copy) NSString *delivery_price_total;
@property (nonatomic, copy) NSString *active_id;
@property (nonatomic, copy) NSString *coupons_total;
@property (nonatomic, copy) NSString *is_remind;
@property (nonatomic, copy) NSString *active_table;
@property (nonatomic, copy) NSString *source;
@property (nonatomic, copy) NSString *source_supply_id;
@property (nonatomic, strong) NSArray *pay_wallet;
@property (nonatomic, copy) NSString *status_str;
@property (nonatomic, strong) NSArray *product_list;
@property (nonatomic, strong) NSArray *return_info;
@end

@interface BusinessFormProductListDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *image;

@end

@interface BusinessFormParser : NSObject
@property (nonatomic, strong) BusinessFormListParser *list;

@end

/**订单详情*/
@interface BusinessFormOrderSupplyParser : NSObject
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *addr;
@end

@interface BusinessFormOrderDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *number_total;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *desc;
/**
 "id":697,
 "status":0,
 "order_no":"P17081915570823833179",
 "user_id":21830,
 "username":"JZ103191",
 "supply_id":21831,
 "supply_username":"JZ104011",
 "delivery_id":21831,
 "is_private":0,
 "desc":"",
 "w_time":1503129428,
 "pay_time":0,
 "transmit_time":0,
 "confirm_time":0,
 "app_time":0,
 "return_time":0,
 "close_time":0,
 "logistics_type":1,
 "sell_type":1,
 "yunfei":"0.00",
 "pay_info":"",
 "pay_yunfei":"",
 "number_total":2,
 "ck_time":0,
 "suc_time":0,
 "supply_price_total":"2.00",
 "market_price_total":"3.00",
 "sell_price_total":"3.00",
 "pv_total":"0.00",
 "coupon_total":"3.00",
 "product_weight_total":"2.00",
 "score_total":"0.00",
 "reward_coupon_total":"0.00",
 "reward_score_total":"0.00",
 "pay_no":"",
 "delivery_price_total":"0.00",
 "active_id":0,
 "coupons_total":"0.00",
 "is_eval":0,
 "is_remind":0,
 "active_table":"",
 "source":0,
 "source_supply_id":null,
 "pay_wallet":[
 
 ],
 "yunfei_edit":0
 */

@end

@interface BusinessFormOrderLogisticParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *com_code;
@property (nonatomic, copy) NSString *com_no;
@property (nonatomic, copy) NSString *com_name;
@property (nonatomic, copy) NSString *com_id;
@property (nonatomic, copy) NSString *rec_name;
@property (nonatomic, copy) NSString *rec_tel;
@property (nonatomic, copy) NSString *rec_addr;
@property (nonatomic, copy) NSString *comments;

@end

@interface BusinessFormOrderParser : NSObject
@property (nonatomic, strong) BusinessFormOrderSupplyParser *supply;
@property (nonatomic, strong) BusinessFormOrderDetailParser *order;
@property (nonatomic, strong) NSArray *order_list;
@property (nonatomic, copy) NSString *orderStatus;
@property (nonatomic, strong) BusinessFormOrderLogisticParser *logistics;
@property (nonatomic, copy) NSString *is_refund;
@property (nonatomic, copy) NSString *is_reject;
@property (nonatomic, strong) NSDictionary *show_fileds;
@property (nonatomic, copy) NSString *tname;
@property (nonatomic, strong) NSArray *evaInfo;//评价信息
@property (nonatomic, strong) NSArray *log_companies;

@end

@interface BusinessFormOrderLogisticDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *com_name;

@end

@interface BusinessFormOrderListDatailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *delivery_id;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *product_ext_id;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *transmit_time;
@property (nonatomic, copy) NSString *confirm_time;
@property (nonatomic, copy) NSString *app_time;
@property (nonatomic, copy) NSString *return_time;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *suc_time;
@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *is_cod;
@property (nonatomic, copy) NSString *pay_info;
@property (nonatomic, copy) NSString *pay_yunfei;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *supply_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *pv;
@property (nonatomic, copy) NSString *coupon;
@property (nonatomic, copy) NSString *product_weight;
@property (nonatomic, copy) NSString *product_no;
@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *reward_coupon_total;
@property (nonatomic, copy) NSString *reward_coupon;
@property (nonatomic, copy) NSString *reward_score_total;
@property (nonatomic, copy) NSString *reward_score;
@property (nonatomic, copy) NSString *delivery_price_total;
@property (nonatomic, copy) NSString *delivery_price;
@property (nonatomic, copy) NSString *is_eval;
@property (nonatomic, copy) NSString *coupons;
@end

@interface  BusinessLogisticsDataParser: NSObject

@property(nonatomic,copy) NSString *time;
@property(nonatomic,copy) NSString *context;
@property(nonatomic,copy) NSString *js_time;

@end

@interface BusinessLogisticsParser : NSObject

@property(nonatomic,copy) NSString *company;
@property(nonatomic,copy) NSString *no;
@property(nonatomic,copy) NSString *tel;
@property(nonatomic,copy) NSString *status;


@end


@interface BusinessFormModel : NSObject

@end
