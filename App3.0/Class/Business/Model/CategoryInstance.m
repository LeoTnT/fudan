//
//  CategoryInstance.m
//  App3.0
//
//  Created by nilin on 2017/9/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CategoryInstance.h"
static CategoryInstance* category = nil;
#define CATEGORY_LIST @"CATEGORY_LIST"
@implementation CategoryInstance
+ (CategoryInstance*)ShardInstnce {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        category = [[CategoryInstance alloc] init];
    });
    return category;
}
- (void)setupCategoryInfoWithName:(NSString *)name {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    self.nameArray = [NSArray arrayWithArray:[ud arrayForKey:CATEGORY_LIST]];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:self.nameArray];
    if ([temp containsObject:name]) {
        [temp removeObject:name]; 
    }
     [temp insertObject:name atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:CATEGORY_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (NSArray *)getCategory {
  return [[NSUserDefaults standardUserDefaults] arrayForKey:CATEGORY_LIST];
}

- (void)deleteCategory {
    self.nameArray = [NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:CATEGORY_LIST]];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:self.nameArray];
    [temp removeAllObjects];
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:CATEGORY_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
@end
