//
//  BusinessModel.h
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessRegisterParser : NSObject
@property (nonatomic, copy) NSString *approve_supply;
@property (nonatomic, copy) NSString *disabled;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *sname;
@property (nonatomic, copy) NSString *industry_id;
@property (nonatomic, copy) NSString *is_enter_pay;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *card_no;
@property (nonatomic, strong) NSArray *img_card;
@property (nonatomic, copy) NSString *img_license;
@property (nonatomic, copy) NSString *img_zuzhi;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *province_code;
@property (nonatomic, copy) NSString *city_code;
@property (nonatomic, copy) NSString *county_code;
@property (nonatomic, copy) NSString *town_code;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *approve_desc;
@property (nonatomic, copy) NSString *is_open_pay;
@property (nonatomic, copy) NSString *enter_pay_detail;
@property (nonatomic, copy) NSString *enter_pay_number;
@property (nonatomic, strong) NSArray *industry;
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *comment;

@property (nonatomic,copy) NSString *mobile;

@property (nonatomic,copy) NSString *img_card_sc;
@property (nonatomic,copy) NSString *weixin;
@end

@interface BusinessIndustryParser : NSObject
@property (nonatomic, copy) NSString *iid;
@property (nonatomic, copy) NSString *iname;

@end


@interface BusinessApplyIndustryListParser : NSObject
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessApplyIndustryParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;

@end

@interface BusinessSupplyReportParser : NSObject
@property (nonatomic, copy) NSString *income_today;//今日成交额
@property (nonatomic, copy) NSString *income_yestoday;//昨日
@property (nonatomic, copy) NSString *order_today;//今日订单数
@property (nonatomic, copy) NSString *wait_pay;
@property (nonatomic, copy) NSString *wait_send;
@property (nonatomic, copy) NSString *browse_product_today;
@property (nonatomic, copy) NSString *favorite_today;
@property (nonatomic, copy) NSString *browse_supply_today;
@property (nonatomic, copy) NSString *cart_today;
@property (nonatomic, strong) NSDictionary *income_week;
@end

@interface BusinessCompanyParser : NSObject
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessCompanyDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *com_name;
@property (nonatomic, copy) NSString *com_code;
@property (nonatomic, copy) NSString *com_tel;
@property (nonatomic, copy) NSString *com_addr;
@property (nonatomic, copy) NSString *w_time;
@end

@interface BusinessProductParser : NSObject
@property (nonatomic,strong) NSArray *data;
@property (nonatomic, copy) NSString *total;
@end

@interface BusinessProductDetailParser : NSObject
@property (nonatomic, copy)NSString *product_id;
@property (nonatomic, copy)NSString *product_name;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *is_show;//是否上架
@property (nonatomic, copy)NSString *is_del;
@property (nonatomic, copy)NSString *username;
@property (nonatomic, copy)NSString *user_id;
@property (nonatomic, copy)NSString *brand_id;
@property (nonatomic, copy)NSString *category_id;
@property (nonatomic, copy)NSString *category_supply_id;
@property (nonatomic, copy)NSString *recommend_type;
@property (nonatomic, copy)NSString *sell_type;
@property (nonatomic, copy)NSString *keywords;
@property (nonatomic, copy)NSString *image;
@property (nonatomic, copy)NSString *image_list;
@property (nonatomic, copy)NSString *image_thumb;
@property (nonatomic, copy)NSString *w_time;
@property (nonatomic, copy)NSString *u_time;
@property (nonatomic, copy)NSString *c_time;
@property (nonatomic, copy)NSString *c_desc;
@property (nonatomic, copy)NSString *freight_id;
@property (nonatomic, copy)NSString *logistics_type;
@property (nonatomic, copy)NSString *look_num;
@property (nonatomic, copy)NSString *sell_num;
@property (nonatomic, copy)NSString *share_num;
@property (nonatomic, copy)NSString *eva_num;
@property (nonatomic, copy)NSString *is_cod;
@property (nonatomic, copy)NSString *product_ext_id;
@property (nonatomic, copy)NSString *spec_names;//是否上架
@property (nonatomic, copy)NSString *spec_values;
@property (nonatomic, copy)NSString *product_no;
@property (nonatomic, copy)NSString *supply_price;
@property (nonatomic, copy)NSString *market_price;
@property (nonatomic, copy)NSString *sell_price;
@property (nonatomic, copy)NSString *pv;
@property (nonatomic, copy)NSString *coupon;
@property (nonatomic, copy)NSString *product_weight;
@property (nonatomic, copy)NSString *score;
@property (nonatomic, copy)NSString *reward_coupon;
@property (nonatomic, copy)NSString *reward_score;
@property (nonatomic, copy)NSString *delivery_price;
@property (nonatomic, copy)NSString *fav_num;
@property (nonatomic, copy)NSString *source;
@property (nonatomic, copy) NSString *stock_num;
/**自己添加的分类名称*/
@property (nonatomic, copy) NSString *sell_type_name;

@end

@interface CategoryLowerParser : NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface CategoryLowerDetailParser : NSObject

@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *cname;
@end

@interface BusinessSupplyParser : NSObject
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *banner_pc;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *ucity;
@property (nonatomic, copy) NSString *utown;
@property (nonatomic, copy) NSString *uaddress;
@property (nonatomic, copy) NSString *banner_wap;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *uprovince;
@property (nonatomic, copy) NSString *quick_pay;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *ucounty;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *iname;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *is_enable_quickpay;
@end

@interface BusinessSpecParser : NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessSpecListParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSArray *child;


@end

@interface BusinessSpecDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;

@end
//
/**
 "sell_type":[
 {
 "id":1,
 "name":"普通商品"
 },
 {
 "id":2,
 "name":"积分商品"
 }
 ],
 "logistics_type":[
 {
 "id":1,
 "name":"物流配送"
 },
 {
 "id":4,
 "name":"免发货"
 }
 ],
 "recommend_type":[
 {
 "id":1,
 "name":"推荐"
 },
 {
 "id":2,
 "name":"热卖"
 },
 {
 "id":4,
 "name":"新品"
 }
 ],
 "product":{
 "image_list":[
 "http://shangcheng.xsy.dsceshi.cn/upload/common/JZ104011/20170911/67e9a85a3b4d699313ec918224ae12d7.jpg"
 ],
 "product_name":"234",
 "category_id":1033701,
 "freight_id":114,
 "is_show":1,
 "category_supply_id":0,
 "sell_type":1,
 "logistics_type":1,
 "recommend_type":[
 
 ],
 "desc":"<p style="text-indent:0em;margin:4px auto 0px auto;"><font style="font-size:14.000000;color:#000000">刚回家假</font></p>"
 },
 "wholesale_units":{
 "1":"件",
 "2":"盒"
 },
 "wholesale_price_ways":{
 "1":"商品数量",
 "2":"商品规格"
 }
 }
 */
//
@interface BusinessGoodsConfigurationParser : NSObject
@property (nonatomic, strong) NSDictionary *product;
@property (nonatomic, strong) NSArray *sell_type;
@property (nonatomic, strong) NSArray *recommend_type;
@property (nonatomic, strong) NSArray *logistics_type;
@end

@interface BusinessGoodsLogisticTypeParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;

@end

@interface BusinessGoodsSellTypeParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;

@end

@interface BusinessRecordListDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *remain;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *admin_id;
@property (nonatomic, copy) NSString *table;
@property (nonatomic, copy) NSString *table_id;
@property (nonatomic, copy) NSString *type_name;

@end

@interface BusinessRecordListParser : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface BusinessRecordParer : NSObject
@property (nonatomic, strong) BusinessRecordListParser *list;
@property (nonatomic, copy) NSString *has_more;
@property (nonatomic, strong) NSDictionary *trade_type;
@property (nonatomic, copy) NSString *totalMoney;
@property (nonatomic, copy) NSString *scale;
@property (nonatomic, copy) NSString *takecash_open;
@property (nonatomic, copy) NSString *recharge_open;
@property (nonatomic, copy) NSString *total_remain;
@property (nonatomic, copy) NSString *total_out;
@property (nonatomic, copy) NSString *total_in;

@end

@interface BusinessCardTypeDataParser : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;

@end


@interface BusinessBankCardDataParser : NSObject
@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *bankname;
@property (nonatomic, copy) NSString *bankaddress;
@property (nonatomic, copy) NSString *bankcard;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *bankuser;
@property (nonatomic, copy) NSString *image;

@end

@interface BusinessWithDrawWalletTypeSet : NSObject
@property(nonatomic,copy)NSString *intnum;
@property(nonatomic,copy)NSString *maxnum;
@property(nonatomic,copy)NSString *minnum;
@property(nonatomic,copy)NSString *nocheck;
@property(nonatomic,copy)NSString *noday;
@property(nonatomic,copy)NSString *pass2;
@property(nonatomic,copy)NSString *tax;
@property(nonatomic,copy)NSString *taxlow;
@property(nonatomic,copy)NSString *taxtop;
@property(nonatomic,copy)NSString *taxtype;
@property(nonatomic,copy)NSString *hint_num;
@property(nonatomic,copy)NSString *hint_tax;
@end

@interface BusinessWithDrawRule : NSObject
@property(nonatomic,strong) NSArray *bank;
@property(nonatomic,strong) BusinessWithDrawWalletTypeSet *set;
@property(nonatomic,copy) NSString *wallet;
@property(nonatomic,copy) NSString *wallet_type;
@end

@interface BusinessWithDrawRuleBankParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *bankname;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *bankcard;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *bankuser;
@property (nonatomic, copy) NSString *image;


@end

@interface BusinessTakecashParser : NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessTakecashDataParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *wallet_type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *bankname;
@property (nonatomic, copy) NSString *bankaddress;
@property (nonatomic, copy) NSString *bankuser;
@property (nonatomic, copy) NSString *bankcard;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *poundage;
@property (nonatomic, copy) NSString *receive;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *c_time;
@property (nonatomic, copy) NSString *c_remark;
@property (nonatomic, copy) NSString *taxtype;
@property (nonatomic, copy) NSString *wallet_name;

@end


@interface BusinessModel : NSObject

@end
