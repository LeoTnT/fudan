//
//  BusinessRefundModel.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BusinessRefundListParser:NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface BusinessRefundListDataParser:NSObject
@property (nonatomic, copy) NSString *refund_type;//退款类型
@property (nonatomic, copy) NSString *buyer_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *refund_fee;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *send_status_cn;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *refund_no;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *seller_id;
@property (nonatomic, copy) NSString *seller_name;
@property (nonatomic, copy) NSString *refund_status_cn;
@property (nonatomic, copy) NSString *refund_status_en;//退款状态
@property (nonatomic, copy) NSString *cs_status_en;//客服介入状态
@property (nonatomic, copy) NSString *format_remain_time;//剩余退款待处理时间
@property (nonatomic, copy) NSString *buyer_name;
@end

/**退款退货管理界面 顶部信息*/
@interface BusinessRefundMainLastParser: NSObject
@property (nonatomic, copy) NSString *refund_type;
@property (nonatomic, copy) NSString *refund_no;
@property (nonatomic, copy) NSString *format_time;
@end

@interface BusinessRefundMainParser: NSObject

@property (nonatomic, copy) NSString *wait_deal_num;
@property (nonatomic, strong) BusinessRefundMainLastParser *last_refund;

@end

/**已退货，物流信息*/
@interface BusinessRefundDetailLogisticsParser:NSObject
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *buyer_remark;
@property (nonatomic, copy) NSString *com_tel;
@property (nonatomic, copy) NSString *logistics_com;
@property (nonatomic, copy) NSString *logistics_no;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *seller_remark;

@end

/**退款详情*/
@interface BusinessRefundDetailParser:NSObject
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *refund_reason_desc;
@property (nonatomic, copy) NSString *refund_status_en;//状态
@property (nonatomic, copy) NSString *can_close_refund;//客服介入状态
@property (nonatomic, copy) NSString *can_guestbook;//是否可以给买家留言
@property (nonatomic, copy) NSString *refund_type;
@property (nonatomic, copy) NSString *seller_name;
@property (nonatomic, copy) NSString *seller_tel;
@property (nonatomic, copy) NSString *remain_time;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *refund_no;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *seller_avatar;
@property (nonatomic, copy) NSString *goods_status_cn;
@property (nonatomic, copy) NSString *cb_refund_no;
@property (nonatomic, copy) NSString *buyer_avatar;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *cs_status_en;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *money_return_time;
@property (nonatomic, copy) NSString *buyer_name;
@property (nonatomic, copy) NSString *order_status_en;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *goods_return_time;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *format_remain_time;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *seller_id;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *refund_reason_cn;
@property (nonatomic, copy) NSString *buyer_id;
@property (nonatomic, copy) NSString *has_goods_return;
@property (nonatomic, strong) NSArray *refund_fee;
@property (nonatomic, copy) NSString *buyer_tel;
@property (nonatomic, copy) NSString *close_reason_en;
@property (nonatomic, strong) BusinessRefundDetailLogisticsParser *logistics;
@end

@interface  BusinessRefundDetailRefundFeeParser:NSObject
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *wallet_name;

@end

@interface BusinessRefundNegotiateHistoryListParser:NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface BusinessRefundNegotiateHistoryParser:NSObject
@property (nonatomic, copy) NSString *log_str;
@property (nonatomic, copy) NSString *format_time;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, strong) NSArray *pics;//图片

@end

@interface BusinessRefundRejectReasonParser:NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessRefundRejectReasonDataParser:NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *item;

@end


@interface BusinessRefundModel : NSObject

@end
