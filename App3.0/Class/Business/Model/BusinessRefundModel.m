//
//  BusinessRefundModel.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessRefundModel.h"


@implementation BusinessRefundListParser

+(NSDictionary *)mj_objectClassInArray {
    
    return @{@"data":@"BusinessRefundListDataParser"};
}
@end

@implementation BusinessRefundListDataParser


@end

@implementation BusinessRefundMainLastParser

@end

@implementation BusinessRefundMainParser

@end

@implementation BusinessRefundDetailLogisticsParser

@end


@implementation  BusinessRefundDetailParser


+(NSDictionary *)mj_objectClassInArray {
    
    return @{@"refund_fee":@"BusinessRefundDetailRefundFeeParser"};
}
@end

@implementation  BusinessRefundNegotiateHistoryListParser
+(NSDictionary *)mj_objectClassInArray {
    
    return @{@"data":@"BusinessRefundNegotiateHistoryParser"};
}

@end

@implementation BusinessRefundNegotiateHistoryParser


@end

@implementation  BusinessRefundDetailRefundFeeParser


@end

@implementation BusinessRefundRejectReasonParser
+(NSDictionary *)mj_objectClassInArray {
    
    return @{@"data":@"BusinessRefundRejectReasonDataParser"};
}
@end

@implementation BusinessRefundRejectReasonDataParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"ID":@"id"};
}
@end

@implementation BusinessRefundModel

@end
