
//
//  BusinessClassifyModel.m
//  App3.0
//
//  Created by nilin on 2017/8/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessClassifyModel.h"

@implementation BusinessClassifyParser

@synthesize data;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessClassifyDetailParser"};
}

@end

@implementation BusinessClassifyDetailParser


@end

@implementation BusinessPriceFieldParser

@synthesize field;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"field":@"BusinessPriceFieldDetailParser"};
}

@end

@implementation BusinessPriceFieldDetailParser



@end


@implementation BusinessClassifyModel

@end
