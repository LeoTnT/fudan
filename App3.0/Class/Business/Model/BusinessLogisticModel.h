//
//  BusinessLogisticModel.h
//  App3.0
//
//  Created by nilin on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessLogisticParser : NSObject
@property (nonatomic, strong) NSArray *list;//data里的

@end

@interface BusinessLogisticListParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *freight_name;
@property (nonatomic, copy) NSString *u_time;
@property (nonatomic, strong) NSArray *ext;


@end

@interface BusinessLogisticDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *freight_id;
@property (nonatomic, copy) NSString *area_name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *full_price;
@property (nonatomic, copy) NSString *true_price;
@property (nonatomic, copy) NSString *first_weight;
@property (nonatomic, copy) NSString *last_weight;
@property (nonatomic, copy) NSString *u_time;
@property (nonatomic, copy) NSString *area_data_o;
@property (nonatomic, copy) NSString *first;
@property (nonatomic, copy) NSString *last;

@end

@interface BusinessLogisticModel : NSObject

@end
