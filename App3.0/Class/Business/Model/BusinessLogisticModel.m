
//
//  BusinessLogisticModel.m
//  App3.0
//
//  Created by nilin on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessLogisticModel.h"

@implementation BusinessLogisticParser

@synthesize list;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"list":@"BusinessLogisticListParser"};
}

@end

@implementation BusinessLogisticListParser

@synthesize ID,freight_name,u_time,ext;
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

+(NSDictionary *)mj_objectClassInArray {
    return @{@"ext":@"BusinessLogisticDetailParser"};
}
@end

@implementation BusinessLogisticDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessLogisticModel

@end
