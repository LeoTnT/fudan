//
//  CategoryInstance.h
//  App3.0
//
//  Created by nilin on 2017/9/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryInstance : NSObject

/**类目名称*/
@property (nonatomic, strong) NSArray *nameArray;
+ (CategoryInstance*)ShardInstnce;
- (void)setupCategoryInfoWithName:(NSString *) name;
- (NSArray *)getCategory;
- (void) deleteCategory;
@end
