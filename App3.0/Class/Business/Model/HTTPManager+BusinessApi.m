//
//  HTTPManager+BusinessApi.m
//  App3.0
//
//  Created by nilin on 2017/9/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HTTPManager+BusinessApi.h"

@implementation HTTPManager (BusinessApi)

/***************************************************************************退款退货模块begin************************************************************************************/
/**给买家留言*/
+ (void) refundGuestbookWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/Guestbook";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}
/**退款退货列表*/
+ (void)refundListsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/basic/refund/Lists";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**退款详情*/
+ (void)refundViewWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/basic/refund/View";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**协商历史*/
+ (void)refundLogsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/basic/refund/Logs";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**管理界面部分退款信息*/
+ (void)refundInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/Info";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/**检查是否有未结束的退款*/
+ (void) refundCheckWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail; {
    
    NSString *path = @"/api/v1/basic/refund/Check";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}


/**卖家同意仅退款*/
+ (void) refundAgreeOnlyRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/AgreeOnlyRefund";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}


/**卖家同意退款退货申请*/
+ (void)refundAgreeReturnRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/AgreeReturnRefund";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**退货商品，商家确认收货*/
+ (void)refundConfirmWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/Confirm";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**退货商品，商家拒绝收货*/
+ (void)refundRefuseConfirmWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    
    NSString *path = @"/api/v1/supply/refund/RefuseConfirm";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**已发货商品，商家拒绝退款*/
+ (void)refundRefuseRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/RefuseRefund";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**客服介入后，提交凭证*/
+ (void) refundSellerSayWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/SellerSay";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**卖家拒绝退款-拒绝收货原因*/
+ (void) refundGetRefundReasonWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/refund/GetRefuseReason";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

//----------------------------------------------------------//
/**商家版*/

/**商家入驻详情信息*/
+ (void)getSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/register/EnterInfo";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/**商家入驻详情信息*/
+ (void)getOfflineSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1offline/user/register/EnterInfo";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}
/**商家行业信息*/
+ (void)getIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/basic/industry/Lists";
    [XSHTTPManager post:path parameters:@{@"limit":@100} success:success failure:fail];
}

/**商家入驻*/
+ (void)supplyRegisterWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/register/Enter";
    
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}
+ (void)applyOfflineShopWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1offline/user/register/Enter";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}
/**创建入住费用订单*/
+ (void)supplyCreateEnterOrderSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/register/CreateEnterOrder";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/**运营报表*/
+ (void) getReportSupplyReportSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/report/GetSupplyReport";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}


/***************************************************************************运费模块begin************************************************************************************/
/**物流公司*/
+ (void)getLogisticsCompanyListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/logistics/LogisticsCompanyLists";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/**大区*/
+ (void)getRegionSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/freight/GetRegion";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/**运费模板信息列表*/
+ (void)getFreightListsWithPage:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/freight/Lists";
    NSDictionary *param = @{@"page":page,@"limit":limit};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**运费模板详情*/
+ (void)getFreightInfoWithfreightId:(NSString *) freightId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/freight/GetFreightInfo";
    NSDictionary *param = @{@"freightId":freightId};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**删除运费模板*/
+ (void)freightDeleteWithfreightId:(NSString *)freightId orFreightExtId:(NSString *)freightExtId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    
    NSString *path = @"/api/v1/supply/freight/Delete";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (freightId) {
        [param setObject:freightId forKey:@"freightId"];
    }
    if (freightExtId) {
        [param setObject:freightExtId forKey:@"freightExtId"];
    }
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**添加运费信息*/
+ (void)freightAddwithFreightName:(NSString *)freightName freightId:(NSString *)freightId rules:(NSArray *)rules success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    /**"[{"type":"2","allSelectArea":"{\"上海\":[\"上海\"],\"山东\":[\"济南\",\"青岛\"]}","firstWeight":"552","lastWeight":"555"}]"*/
    NSString *path = @"/api/v1/supply/freight/AddApp";
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rules
                                                       options:kNilOptions
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:freightName forKey:@"freightName"];
    [param setObject:jsonString forKey:@"rules"];
    if (freightId) {
        [param setObject:freightId forKey:@"freightId"];
    }
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/***************************************************************************商家---商品分类begin************************************************************************************/

/**添加商品分类*/
+ (void)supplyCategoryInsertWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/category/Insert";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**编辑商品分类*/
+ (void)supplyCategoryUpdateWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/category/Update";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**商家分类列表*/
+ (void)supplyCategoryListsWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/category/Lists";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**商家分类详情*/
+ (void)supplyCategoryEditWithId:(NSString *) categoryId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/scategory/Edit/";
    NSDictionary *param = @{@"id":categoryId};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**删除商家分类*/
+ (void)supplyCategoryDeleteWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/category/Delete";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**新增规格*/
+ (void)supplyInsertSpecAndSpecValueWithSpecName:(NSString *) spec_name specValueData:(NSString *) spec_value_data cid:(NSString *) cid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/InsertSpecAndSpecValue";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    //    NSError *error;
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:spec_value_data
    //                                                       options:kNilOptions
    //                                                         error:&error];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData
    //                                                 encoding:NSUTF8StringEncoding];
    [param setObject:spec_name forKey:@"spec_name"];
    [param setObject:spec_value_data forKey:@"spec_value_data"];
    [param setObject:cid forKey:@"cid"];
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**编辑规格名称*/
+ (void)supplyUpdateSpecName:(NSString *) spec_name specId:(NSString *) spec_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/UpdateSpecName";
    NSDictionary *param = @{@"spec_name":spec_name,@"spec_id":spec_id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**删除规格*/
+ (void)supplyDeleteSpecAndSpecValueSpecId:(NSString *) spec_id  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/DeleteSpecAndSpecValue";
    NSDictionary *param = @{@"spec_id":spec_id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**新增规格值*/
+ (void)supplyInsertSpecAndSpecValueWithSpecId:(NSString *)spec_id specValueName:(NSString *)spec_value_name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/InsertSpecValue";
    NSDictionary *param = @{@"spec_id":spec_id,@"spec_value_name":spec_value_name};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**删除规格值*/
+ (void)supplyDeleteSpecValue:(NSString *) spec_id specValueId:(NSString *) spec_value_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/DeleteSpecValue";
    NSDictionary *param = @{@"spec_id":spec_id,@"spec_value_id":spec_value_id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}


/**获取商家规格*/
+ (void)supplyGetSpecBySupplySuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/GetSpecBySupply";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
}

/***************************************************************************商品管理begin************************************************************************************/
/**商品列表*/
+ (void)supplyProductListsWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/Lists";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**删除商品*/
+ (void)supplyProductDeleteWithId:(NSString *) productId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/Delete";
    NSDictionary *param = @{@"id":productId};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**发布商品*/
+ (void)supplyProductInsertWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/Insert";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**编辑商品*/
+ (void)supplyProductUpdateWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/Update";
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
    
}

/**商品分类至*/
+ (void)supplyProductUpdateScategoryWithIdString:(NSString *) id_str scategory_id:(NSString *)scategory_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/UpdateScategory";
    NSDictionary *param = @{@"id_str":id_str,@"scategory_id":scategory_id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**商品上下架*/
+ (void)supplyProductOnOffWithIdString:(NSString *) id_str isShow:(NSString *) is_show success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/OnOff";
    NSDictionary *param = @{@"id_str":id_str,@"is_show":is_show};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}


/***************************************************************************商家---货款钱包begin************************************************************************************/
/**财务记录*/
+ (void)userMoneyListsWithWalletType:(NSString *)wallet_type leixing:(NSString *)leixing numtype:(NSString *)numtype page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/money/Lists";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:wallet_type forKey:@"wallet_type"];
    [param setObject:page forKey:@"page"];
    [param setObject:limit forKey:@"limit"];
    if (leixing) {
        [param setObject:leixing forKey:@"leixing"];
    }
    if (numtype) {
        [param setObject:numtype forKey:@"numtype"];
    }
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**提现规则*/
+ (void)userTakecashAddWithWalletType:(NSString *) wallet_type  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/takecash/Add";
    NSDictionary *param = @{@"wallet_type":wallet_type};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**获取绑定的银行卡*/
+ (void)userBankCardInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/bank/Lists";
    [XSHTTPManager post:path parameters:nil success:success failure:failure];
    
}
/**提现记录*/
+ (void)userTakecashListsWithWalletType:(NSString *) wallet_type page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/takecash/Lists";
    NSDictionary *param = @{@"wallet_type":wallet_type,@"page":page,@"limit":limit};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**货款提现*/ //remark
+ (void)userTakecashInsertWithWalletType:(NSString *) wallet_type pass2:(NSString *) pass2 number:(NSString *)number bankcardid:(NSString *)bankcardid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/user/takecash/Insert";
    NSDictionary *param = @{@"wallet_type":wallet_type,@"pass2":pass2,@"number":number,@"bankcardid":bankcardid};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**物流配置*/
+ (void)supplyProductLogisticTypeId:(NSString *) Id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/Add";
    NSDictionary *param = @{@"id":Id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/** 获取平台分类*/
+ (void)supplyProductGetCategoryLowerWithCname:(NSString *) cname success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/GetCategoryLower";
    NSDictionary *param = @{@"cname":cname};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}

/**获取分类规格信息*/
+ (void)supplyProductGetSpecWithCategoryId:(NSString *) category_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/GetSpecByCategory";
    NSDictionary *param = @{@"category_id":category_id};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**价格库存信息*/
+ (void)supplyProductGetPriceFieldWithSellType:(NSString *) sell_type operateType:(NSString *) operate_type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/product/GetPriceField";
    NSDictionary *param = @{@"sell_type":sell_type,@"operate_type":operate_type};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
    
}
/**获取店铺信息*/
+ (void)supplyGetSupplyInfoWithUid:(NSString *) uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/supply/GetSupplyInfo";
    NSDictionary *param = @{@"uid":uid};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**店铺设置*/
+ (void)supplySetSupplyInfoWithShopName:(NSString *) shop_name logoImg:(NSString *) logo_img  mobileshophome_images:(NSString *) mobileshophome_images longitude:(NSString *) longitude latitude:(NSString *) latitude tel:(NSString *) tel desc:(NSString *) desc success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/supply/SetSupplyInfo";
    NSDictionary *param = @{@"shop_name":shop_name,@"logo_img":logo_img,@"mobileshophome_images":mobileshophome_images,@"longitude":longitude,@"latitude":latitude,@"tel":tel,@"desc":desc};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**商家----订单数量*/
+ (void)supplySorderGetStatusCountSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/sorder/GetStatusCount";
    [XSHTTPManager post:path parameters:nil success:success failure:fail];
    
}

/**商家----订单列表*/
+ (void)supplySorderListsWithSearchAllAtatus:(NSString *) _search_aal_status page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/sorder/Lists";
    NSDictionary *param = @{@"_search_aal_status":_search_aal_status,@"page":page,@"limit":limit};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**订单详情*/
+ (void) buyerOrderViewWithOrderId:(NSString *) orderId supply:(NSString *) supply success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/buyer/order/View";
    NSDictionary *param = @{@"id":orderId,@"supply":supply};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**发货*/
+ (void)supplySorderSendWithOrderId:(NSString *) order_id companyId:(NSString *) company_id comNo:(NSString *) com_no success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/sorder/Send";
    NSDictionary *param = @{@"order_id":order_id,@"company_id":company_id,@"com_no":com_no};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**关闭订单*/
+ (void)buyerOrderCloseOrderWithIds:(NSString *) orderIds success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/supply/sorder/CloseOrder";
    NSDictionary *param = @{@"id":orderIds};
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}

/**查看物流*/
+ (void)buyerOrderExpressWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSString *path = @"/api/v1/buyer/order/Express/";
    NSDictionary *param = @{
                            @"id": orderId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:fail];
}


@end

