//
//  BusinessModel.m
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessModel.h"

@implementation BusinessRegisterParser

@synthesize disabled,qrcode,industry_id,approve_supply,nickname,enter_pay_detail,username,approve_desc,logo,img_license,sname,is_open_pay,is_enter_pay,img_card,card_no,img_zuzhi,remark,province,province_code,industry,city,city_code,county,county_code,town,town_code,detail,truename,enter_pay_number;

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"industry":@"BusinessIndustryParser"
             };
}

@end

@implementation BusinessIndustryParser

@synthesize iid,iname;

@end

@implementation BusinessApplyIndustryListParser

@synthesize total,per_page,current_page,data;

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"data":@"BusinessApplyIndustryParser"
             };
}

@end

@implementation BusinessApplyIndustryParser

@synthesize ID,name,image;

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation BusinessSupplyReportParser

@synthesize income_week,income_today,income_yestoday,order_today,cart_today,wait_pay,wait_send,browse_supply_today,browse_product_today,favorite_today;

@end

@implementation BusinessCompanyParser

@synthesize total,per_page,current_page,data;

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"data":@"BusinessCompanyDetailParser"
             };
}

@end

@implementation BusinessCompanyDetailParser

@synthesize ID,com_tel,com_addr,com_code,com_name,w_time;
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation BusinessProductParser

@synthesize data,total;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessProductDetailParser"};
}

@end

@implementation BusinessProductDetailParser

@synthesize brand_id,image_thumb,category_supply_id,c_desc,c_time,coupon,category_id,is_cod,reward_coupon,score,source,product_id,product_no,status,sell_num,sell_type,share_num,sell_price,spec_names,spec_values,supply_price,is_show,reward_score,is_del,user_id,keywords,look_num,market_price,image,u_time,w_time,logistics_type,recommend_type,fav_num,freight_id,eva_num,product_ext_id,username,pv,product_name,product_weight,delivery_price,image_list;


@end

@implementation CategoryLowerParser

@synthesize data;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"CategoryLowerDetailParser"};
}

@end

@implementation CategoryLowerDetailParser

@synthesize cid,cname;

@end

@implementation BusinessSupplyParser


@end

@implementation BusinessSpecParser

@synthesize data;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessSpecListParser"};
}

@end

@implementation BusinessSpecListParser

@synthesize ID,name,child;
+(NSDictionary *)mj_replacedKeyFromPropertyName  {
    return @{@"ID":@"id"};
}

+(NSDictionary *)mj_objectClassInArray {

    return @{@"child":@"BusinessSpecDetailParser"};
}

@end

@implementation BusinessSpecDetailParser

@synthesize ID,name;
+(NSDictionary *)mj_replacedKeyFromPropertyName  {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessGoodsConfigurationParser

+(NSDictionary *)mj_objectClassInArray {
    return @{@"logistics_type":@"BusinessGoodsLogisticTypeParser",
             @"sell_type":@"BusinessGoodsSellTypeParser"};

}

@end

@implementation BusinessGoodsLogisticTypeParser


+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessGoodsSellTypeParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
        return @{@"ID":@"id"};
    }
    
@end

@implementation BusinessRecordListDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessRecordListParser

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessRecordListDetailParser"};
}

@end

@implementation BusinessRecordParer



@end

@implementation BusinessCardTypeDataParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}


@end

@implementation BusinessBankCardDataParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessWithDrawWalletTypeSet


@end

@implementation BusinessWithDrawRule
+(NSDictionary *)mj_objectClassInArray {
    return @{@"bank":@"BusinessWithDrawRuleBankParser"};
}
@end

@implementation BusinessWithDrawRuleBankParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end

@implementation BusinessTakecashParser
+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessTakecashDataParser"};
}


@end

@implementation BusinessTakecashDataParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessModel


@end
