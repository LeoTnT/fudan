//
//  HTTPManager+BusinessApi.h
//  App3.0
//
//  Created by nilin on 2017/9/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"

@interface HTTPManager (BusinessApi)
#pragma mark  - - - - - - - --  - - - 商家版

/***************************************************************************退款退货模块begin************************************************************************************/
/**给买家留言*/
+ (void) refundGuestbookWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**退款退货列表*/
+ (void)refundListsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**退款详情*/
+ (void)refundViewWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**协商历史*/
+ (void)refundLogsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**管理界面部分退款信息*/
+ (void)refundInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**检查是否有未结束的退款*/
+ (void) refundCheckWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**卖家同意仅退款*/
+ (void) refundAgreeOnlyRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**卖家同意退款退货申请*/
+ (void)refundAgreeReturnRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**退货商品，商家确认收货*/
+ (void)refundConfirmWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**退货商品，商家拒绝收货*/
+ (void)refundRefuseConfirmWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**已发货商品，商家拒绝退款*/
+ (void)refundRefuseRefundWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**客服介入后，提交凭证*/
+ (void) refundSellerSayWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**卖家拒绝退款-拒绝收货原因*/
+ (void) refundGetRefundReasonWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************************************入驻模块begin************************************************************************************/

/**商家入驻详情信息*/
+ (void)getSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/**商家入驻详情信息*/
+ (void)getOfflineSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家行业信息*/
+ (void)getIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家入驻*/
+ (void)supplyRegisterWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


+ (void)applyOfflineShopWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**创建入住费用订单*/
+ (void)supplyCreateEnterOrderSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**运营报表*/
+ (void) getReportSupplyReportSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************************************运费模块begin************************************************************************************/
/**物流公司*/
+ (void)getLogisticsCompanyListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**大区*/
+ (void)getRegionSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**运费模板信息列表*/
+ (void)getFreightListsWithPage:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**运费模板详情*/
+ (void)getFreightInfoWithfreightId:(NSString *) freightId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**删除运费模板*/
+ (void)freightDeleteWithfreightId:(NSString *)freightId orFreightExtId:(NSString *)freightExtId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/**添加运费信息*/
+ (void)freightAddwithFreightName:(NSString *)freightName freightId:(NSString *)freightId rules:(NSArray *)rules success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/***************************************************************************商家---商品分类begin************************************************************************************/

/**添加商品分类*/
+ (void)supplyCategoryInsertWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**编辑商品分类*/
+ (void)supplyCategoryUpdateWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家分类列表*/
+ (void)supplyCategoryListsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家分类详情*/
+ (void)supplyCategoryEditWithId:(NSString *) categoryId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**删除商家分类*/
+ (void)supplyCategoryDeleteWithParam:(NSDictionary *) param  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/***************************************************************************商品规格begin************************************************************************************/

/**新增规格*/
+ (void)supplyInsertSpecAndSpecValueWithSpecName:(NSString *) spec_name specValueData:(NSString *) spec_value_data cid:(NSString *) cid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**编辑规格名称*/
+ (void)supplyUpdateSpecName:(NSString *) spec_name specId:(NSString *) spec_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**删除规格*/
+ (void)supplyDeleteSpecAndSpecValueSpecId:(NSString *) spec_id  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**新增规格值*/
+ (void)supplyInsertSpecAndSpecValueWithSpecId:(NSString *) spec_id specValueName:(NSString *) spec_value_name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**删除规格值*/
+ (void)supplyDeleteSpecValue:(NSString *) spec_id specValueId:(NSString *) spec_value_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**获取商家规格*/
+ (void)supplyGetSpecBySupplySuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/***************************************************************************商品管理begin************************************************************************************/
/**商品列表*/
+ (void)supplyProductListsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**删除商品*/
+ (void)supplyProductDeleteWithId:(NSString *) productId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**发布商品*/
+ (void)supplyProductInsertWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**编辑商品*/
+ (void)supplyProductUpdateWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商品分类至*/
+ (void)supplyProductUpdateScategoryWithIdString:(NSString *) id_str scategory_id:(NSString *)scategory_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商品上下架*/
+ (void)supplyProductOnOffWithIdString:(NSString *) id_str isShow:(NSString *) is_show success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/***************************************************************************商家---货款钱包begin************************************************************************************/
/**财务记录--交易类型，支出、收入*/
+ (void)userMoneyListsWithWalletType:(NSString *)wallet_type leixing:(NSString *)leixing numtype:(NSString *)numtype page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**提现规则*/
+ (void)userTakecashAddWithWalletType:(NSString *) wallet_type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**获取绑定的银行卡*/
+ (void)userBankCardInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
/**提现记录*/
+ (void)userTakecashListsWithWalletType:(NSString *) wallet_type page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**货款提现*/ //remark
+ (void)userTakecashInsertWithWalletType:(NSString *) wallet_type pass2:(NSString *) pass2 number:(NSString *)number bankcardid:(NSString *)bankcardid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/***************************************************************************商家---运费管理begin************************************************************************************/
/**物流配置*/
+ (void)supplyProductLogisticTypeId:(NSString *) Id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/** 获取平台分类*/
+ (void)supplyProductGetCategoryLowerWithCname:(NSString *) cname success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**获取分类规格信息*/
+ (void)supplyProductGetSpecWithCategoryId:(NSString *) category_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**价格库存信息*/
+ (void)supplyProductGetPriceFieldWithSellType:(NSString *) sell_type operateType:(NSString *) operate_type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/***************************************************************************店铺模块begin************************************************************************************/

/**获取店铺信息*/
+ (void)supplyGetSupplyInfoWithUid:(NSString *) uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**店铺设置*/
+ (void)supplySetSupplyInfoWithShopName:(NSString *) shop_name logoImg:(NSString *) logo_img  mobileshophome_images:(NSString *) mobileshophome_images longitude:(NSString *) longitude latitude:(NSString *) latitude tel:(NSString *) tel desc:(NSString *) desc success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家----订单数量*/
+ (void)supplySorderGetStatusCountSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**商家----订单列表*/
+ (void)supplySorderListsWithSearchAllAtatus:(NSString *) _search_aal_status page:(NSString *) page limit:(NSString *) limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**订单详情*/
+ (void) buyerOrderViewWithOrderId:(NSString *) orderId supply:(NSString *) supply success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**发货*/
+ (void)supplySorderSendWithOrderId:(NSString *) order_id companyId:(NSString *) company_id comNo:(NSString *) com_no success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**关闭订单*/
+ (void)buyerOrderCloseOrderWithIds:(NSString *) orderIds success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**查看物流*/
+ (void)buyerOrderExpressWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//----------------------------------------------------------//
@end

