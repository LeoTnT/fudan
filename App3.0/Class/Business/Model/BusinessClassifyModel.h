//
//  BusinessClassifyModel.h
//  App3.0
//
//  Created by nilin on 2017/8/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessClassifyParser : NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface BusinessClassifyDetailParser : NSObject
@property (nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *category_name;
@property (nonatomic, copy) NSString *ceng;
@property (nonatomic, copy) NSString *parent_id;

@end

@interface BusinessPriceFieldParser : NSObject
@property (nonatomic, strong) NSArray *field;
@property (nonatomic, strong) NSArray *rank;
@property (nonatomic, strong) NSArray *rankfield;
@end

@interface BusinessPriceFieldDetailParser : NSObject
@property (nonatomic, copy) NSString *field;//标识
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *admin_add_display;
@property (nonatomic, copy) NSString *admin_add_edit;
@property (nonatomic, copy) NSString *admin_audit_display;
@property (nonatomic, copy) NSString *admin_audit_edit;
@property (nonatomic, copy) NSString *admin_edit_display;
@property (nonatomic, copy) NSString *admin_edit_edit;
@property (nonatomic, copy) NSString *display;//是否显示
@property (nonatomic, copy) NSString *edit;//是否可输入
@property (nonatomic, copy) NSString *formula;//按公式
@property (nonatomic, copy) NSString *intro;//别名
@property (nonatomic, copy) NSString *require;//是否必填
@property (nonatomic, copy) NSString *scale;//按倍数
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *start;
@property (nonatomic, copy) NSString *supply_add_display;
@property (nonatomic, copy) NSString *supply_add_edit;
@property (nonatomic, copy) NSString *supply_edit_display;
@property (nonatomic, copy) NSString *supply_edit_edit;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *unique;//是否唯一
@property (nonatomic, strong) NSDictionary  *sell_type;
@end

@interface BusinessClassifyModel : NSObject
/**
 
 {
 "status":1,
 "info":"",
 "data":{
 "field":Array[7],
 "desc":{
 "零售价":"我是描述",
 "库存":"如果添加库存，默认入库到第一个库位"
 },
 "rank":[
 
 ],
 "rankfield":[
 
 ]
 }
 }
 
 field:
 {
 "start":"1",
 "sell_type":{
 "1":"1",
 "2":"1"
 },
 "sort":"0",
 "name":"商品编码",
 "field":"product_no",
 "scale":"",
 "formula":"",
 "admin_add_display":"1",
 "admin_add_edit":"1",
 "supply_add_display":"1",
 "supply_add_edit":"1",
 "admin_edit_display":"1",
 "admin_edit_edit":"1",
 "supply_edit_display":"1",
 "supply_edit_edit":"1",
 "admin_audit_display":"1",
 "admin_audit_edit":"1",
 "intro":"",
 "type":"string",
 "require":"1",
 "unique":"1",
 "edit":1,
 "display":1
 },
 
 {
 "admin_add_display" = 1;
 "admin_add_edit" = 1;
 "admin_audit_display" = 1;
 "admin_audit_edit" = 1;
 "admin_edit_display" = 1;
 "admin_edit_edit" = 1;
 display = 1;
 edit = 1;
 field = "product_no";
 formula = "";
 intro = "";
 name = "\U5546\U54c1\U7f16\U7801";
 require = 1;
 scale = "";
 "sell_type" =                 {
 1 = 1;
 100 = 1;
 2 = 1;
 };
 sort = 0;
 start = 1;
 "supply_add_display" = 1;
 "supply_add_edit" = 1;
 "supply_edit_display" = 1;
 "supply_edit_edit" = 1;
 type = string;
 unique = 1;
 }
 */
@end
