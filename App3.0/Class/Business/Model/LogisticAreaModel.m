//
//  LogisticAreaModel.m
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticAreaModel.h"

@implementation LogisticAreaParser

@synthesize data;

+(NSDictionary *)mj_objectClassInArray {

    return @{@"data":@"LogisticAreaListParser"};
}

@end

@implementation LogisticAreaListParser

@synthesize name,child;

+(NSDictionary *)mj_objectClassInArray {
    return @{@"child":@"LogisticAreaDetailParser"};
}

@end

@implementation LogisticAreaDetailParser

@synthesize name,child;
+(NSDictionary *)mj_objectClassInArray {
    return @{@"child":@""};
}

@end

@implementation LogisticAreaModel

@end
