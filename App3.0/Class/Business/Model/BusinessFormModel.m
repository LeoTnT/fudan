//
//  BusinessFormModel.m
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessFormModel.h"

@implementation BusinessFormListParser

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"BusinessFormListDetailParser"};
}

@end

@implementation BusinessFormListDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

+(NSDictionary *)mj_objectClassInArray {

    return @{@"product_list":@"BusinessFormProductListDetailParser"};
}

@end

@implementation BusinessFormProductListDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessFormParser

@synthesize list;

@end

@implementation BusinessFormOrderSupplyParser



@end

@implementation BusinessFormOrderDetailParser



@end


@implementation BusinessFormOrderLogisticParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end

@implementation BusinessFormOrderParser

+(NSDictionary *)mj_objectClassInArray {
    return @{@"order_list":@"BusinessFormOrderListDatailParser",@"log_companies":@"BusinessFormOrderLogisticDetailParser"};
}

@end

@implementation BusinessFormOrderLogisticDetailParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}


@end

@implementation BusinessFormOrderListDatailParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}


@end

@implementation BusinessLogisticsDataParser

@synthesize time,context,js_time;

@end

@implementation BusinessLogisticsParser

@synthesize company,no,tel,status;

@end

@implementation BusinessFormModel

@end
