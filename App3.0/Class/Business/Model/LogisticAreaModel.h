//
//  LogisticAreaModel.h
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogisticAreaParser : NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface LogisticAreaListParser : NSObject

@property (nonatomic, copy) NSString *name;//大区
@property (nonatomic, strong) NSArray *child;//省

@end

@interface LogisticAreaDetailParser : NSObject
@property (nonatomic, copy) NSString *name;//省
@property (nonatomic, strong) NSArray *child;//市

@end

@interface LogisticAreaModel : NSObject

@end
