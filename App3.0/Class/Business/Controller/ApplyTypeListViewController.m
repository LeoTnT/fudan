//
//  ApplyTypeListViewController.m
//  App3.0
//
//  Created by nilin on 2018/4/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "ApplyTypeListViewController.h"
#import "BusinessMainViewController.h"
#import "OfflineBusinessViewController.h"

@interface ApplyTypeListViewController ()
@property (nonatomic, strong) NSArray *itemArray;
@end

@implementation ApplyTypeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商家入驻";
    self.view.backgroundColor = [UIColor whiteColor];

    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    self.itemArray = @[@{@"title":@"线下店铺",@"desc":@"有实体店铺，消费者可到店铺内消费",@"logo":@"business_offline_logo"},@{@"title":@"线上店铺",@"desc":@"消费者在网上下单，商家通过物流发货",@"logo":@"business_online_logo"}];
    
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.bounces = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        OfflineBusinessViewController *controller = [[OfflineBusinessViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        //线上店铺
        BusinessMainViewController *businessController = [[BusinessMainViewController alloc] init];
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager getSupplyRegisterEnterInfoSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BusinessRegisterParser *registerParser = [BusinessRegisterParser mj_objectWithKeyValues:dic[@"data"]];
                if ([registerParser.disabled integerValue]==1) {
                    [XSTool showToastWithView:self.view Text:@"您的商家权限已被禁用，请联系平台管理员！"];
                } else {
                    businessController.businessParser = registerParser;
                    businessController.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:businessController animated:YES];
                }
                
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError * _Nonnull error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"cells";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *itemDic = self.itemArray[indexPath.section];
    cell.imageView.image = [UIImage imageNamed:[itemDic objectForKey:@"logo"]];
    cell.textLabel.text = [itemDic objectForKey:@"title"];
    cell.detailTextLabel.text = [itemDic objectForKey:@"desc"];
    
//    cell.textLabel.font = [UIFont systemFontOfSize:30];
    cell.textLabel.textColor = [UIColor hexFloatColor:@"111111"];
    cell.detailTextLabel.textColor = COLOR_999999;
//    cell.detailTextLabel.font = [UIFont systemFontOfSize:24];
    //2、调整大小
//    CGSize itemSize = CGSizeMake(44, 44);
//    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
//    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
//    [cell.imageView.image drawInRect:imageRect];
//    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();

    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 60)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, mainWidth, 60)];
        label.text = @"请选择入驻店铺类型";
        label.textColor = [UIColor blackColor];
//        label.font = [UIFont systemFontOfSize:20];
        [view addSubview:label];
        return view;
    } else {
        return nil;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 60;
    } else {
        return 12;
    }
}

@end
