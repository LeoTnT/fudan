//
//  LogisticChooseViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticChooseViewController.h"
#import "LogisticCompanyTableViewCell.h"
#import "BusinessModel.h"
#import "SetLogisticViewController.h"

@interface LogisticChooseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *logisticArray;
@property (nonatomic, strong) UIButton *saveButton;
@end

@implementation LogisticChooseViewController

#pragma mark - lazy loadding
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.bounces = NO;
        _tableView.backgroundColor = BG_COLOR;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.rowHeight = 50;
        _tableView.sectionHeaderHeight = 60;
        _tableView.sectionFooterHeight = 0.01;
    }
    return _tableView;
}

-(NSMutableArray *)logisticArray {
    if (!_logisticArray) {
        _logisticArray = [NSMutableArray array];
    }
    return _logisticArray;
}

-(UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [UIButton new];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_saveButton setTitle:Localized(@"save") forState:UIControlStateNormal];
        _saveButton.backgroundColor = mainColor;
        [_saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}
#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"物流选择";
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    //    [self.logisticArray addObjectsFromArray:@[@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递",@"圆通快递"]];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.saveButton];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.height.mas_equalTo(MAIN_VC_HEIGHT-50);
        make.left.right.mas_equalTo(self.view);
    }];
    
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    NSArray *array = [self headerViewWithHeight:60 addToView:self.view];
    UIView *headerView = [array firstObject];
    [headerView layoutIfNeeded];
    [headerView removeFromSuperview];
    [self headerViewWithHeight:60 addToView:nil];
    [self getCompanyInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.logisticArray removeAllObjects];
}

#pragma mark - private
- (void)getCompanyInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getLogisticsCompanyListsSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessCompanyParser *parser = [BusinessCompanyParser mj_objectWithKeyValues:dic[@"data"]];
            [self.logisticArray addObjectsFromArray:parser.data];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
         [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}


- (void)saveAction {
//    SetLogisticViewController *controller = (SetLogisticViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    
    
}

- (NSArray *)headerViewWithHeight:(CGFloat)height addToView:(UIView *)toView {
    // 注意，绝对不能给tableheaderview直接添加约束，必闪退
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, height)];
    
    if (toView) {
        [toView addSubview:headerView];
    } else {
        self.tableView.tableHeaderView = headerView;
    }
    UILabel *tintLabel = [UILabel new];
    tintLabel.font = [UIFont qsh_systemFontOfSize:20];
    NSString *string = @"物流选择（可多选）";
    NSMutableAttributedString *totalAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [totalAttributedString addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor grayColor]
                                  range:NSMakeRange(4, string.length-4)];
    [totalAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(4, string.length-4)];
    tintLabel.attributedText = totalAttributedString;
    [headerView addSubview:tintLabel];
    
    
    
    [tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(self.view).with.mas_offset(15);
        make.right.mas_equalTo(self.view).with.mas_offset(-15);
        make.height.mas_equalTo(60);
    }];
    
    return @[headerView, tintLabel];
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LogisticCompanyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LogisticCompanyTableViewCell idString]];
    if (cell==nil) {
        cell = [[LogisticCompanyTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[LogisticCompanyTableViewCell idString]];
    }
    if (self.logisticArray.count) {
        BusinessCompanyDetailParser *parser = self.logisticArray[indexPath.row];
        cell.companyName = parser.com_name;
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logisticArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 60;
//}


@end
