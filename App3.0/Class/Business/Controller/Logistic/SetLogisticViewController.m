//
//  SetLogisticViewController.m
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetLogisticViewController.h"
#import "LogisticSetCenterTableViewCell.h"
#import "LogisticChooseViewController.h"
#import "LogisticSelectAreaViewController.h"
#import "NSString+Regular.h"

@interface SetLogisticViewController ()<LogisticSetCenterDelegate,UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *rowCountArray;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, assign) NSInteger weightGroupCount;
@property (nonatomic, assign) NSInteger moneyGroupCount;
@property (nonatomic, assign) NSInteger firstGroupCount;
//@property (nonatomic, strong) UIButton *unFreeButton;
//@property (nonatomic, strong) UIButton *allFreeButton;
@property (nonatomic, strong) UIButton *weightButton;
@property (nonatomic, strong) UIButton *moneyButton;
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, assign) CGFloat currentY;
@property (nonatomic, copy) NSString *selectedType;//金额1or重量2
@property (nonatomic, strong) NSMutableArray *rules;
@property (nonatomic, strong) NSMutableDictionary *saveWeightAreaDictionary;//存放每个规则区域
@property (nonatomic, strong) NSMutableDictionary *saveMoneyAreaDictionary;//存放每个规则区域
@property (nonatomic, strong) NSMutableArray *weightParserArray;
@property (nonatomic, strong) NSMutableArray *moneyParserArray;
@property (nonatomic, strong) NSMutableDictionary *saveWeightTextDictionary;
@property (nonatomic, strong) NSMutableDictionary *saveMoneyTextDictionary;

@end

@implementation SetLogisticViewController

#pragma mark - lazy loadding

-(NSMutableDictionary *)saveMoneyTextDictionary {
    if (!_saveMoneyTextDictionary) {
        _saveMoneyTextDictionary = [NSMutableDictionary dictionary];
    }
    return _saveMoneyTextDictionary;
}

-(NSMutableDictionary *)saveWeightTextDictionary {
    if (!_saveWeightTextDictionary) {
        _saveWeightTextDictionary = [NSMutableDictionary dictionary];
    }
    return _saveWeightTextDictionary;
}

-(NSMutableArray *)weightParserArray {
    if (!_weightParserArray) {
        _weightParserArray = [NSMutableArray array];
    }
    return _weightParserArray;
}

-(NSMutableArray *)moneyParserArray {
    if (!_moneyParserArray) {
        _moneyParserArray = [NSMutableArray array];
    }
    return _moneyParserArray;
}

-(NSMutableDictionary *)saveWeightAreaDictionary {
    if (!_saveWeightAreaDictionary) {
        _saveWeightAreaDictionary = [NSMutableDictionary dictionary];
    }
    return _saveWeightAreaDictionary;
}

-(NSMutableDictionary *)saveMoneyAreaDictionary {
    if (!_saveMoneyAreaDictionary) {
        _saveMoneyAreaDictionary = [NSMutableDictionary dictionary];
    }
    return _saveMoneyAreaDictionary;
}

-(NSMutableArray *)rules {
    if (!_rules) {
        _rules = [NSMutableArray array];
    }
    return _rules;
}

//-(UIButton *)unFreeButton {
//    if (!_unFreeButton) {
//        _unFreeButton = [UIButton new];
//        _unFreeButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
//        [_unFreeButton setTitle:@"不包邮" forState:UIControlStateNormal];
//        [_unFreeButton setTitle:@"不包邮" forState:UIControlStateSelected];
//        [_unFreeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        [_unFreeButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
//        [_unFreeButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
//        [_unFreeButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
//        [_unFreeButton addTarget:self action:@selector(unFreeAction:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _unFreeButton;
//}
//
//-(UIButton *)allFreeButton {
//    if (!_allFreeButton) {
//        _allFreeButton = [UIButton new];
//        _allFreeButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
//        [_allFreeButton setTitle:@"全国包邮" forState:UIControlStateNormal];
//        [_allFreeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        [_allFreeButton setTitle:@"全国包邮" forState:UIControlStateSelected];
//        [_allFreeButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
//        
//        [_allFreeButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
//        [_allFreeButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
//        [_allFreeButton addTarget:self action:@selector(allFreeAction:) forControlEvents:UIControlEventTouchUpInside];
//        
//    }
//    return _allFreeButton;
//}
-(UIButton *)weightButton {
    if (!_weightButton) {
        _weightButton = [UIButton new];
        _weightButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
        [_weightButton setTitle:@"按重量计算" forState:UIControlStateNormal];
        [_weightButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_weightButton setTitle:@"按重量计算" forState:UIControlStateSelected];
        [_weightButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
        [_weightButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [_weightButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [_weightButton addTarget:self action:@selector(weightAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _weightButton;
}
-(UIButton *)moneyButton {
    if (!_moneyButton) {
        _moneyButton = [UIButton new];
        _moneyButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
        [_moneyButton setTitle:@"按金额计算" forState:UIControlStateNormal];
        [_moneyButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_moneyButton setTitle:@"按金额计算" forState:UIControlStateSelected];
        [_moneyButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
        [_moneyButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [_moneyButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [_moneyButton addTarget:self action:@selector(moneyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moneyButton;
}

-(NSMutableArray *)rowCountArray {
    if (!_rowCountArray) {
        _rowCountArray = [NSMutableArray arrayWithObjects:@2,@4, nil];
    }
    return _rowCountArray;
}

-(UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [UIButton new];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_saveButton setTitle:Localized(@"save") forState:UIControlStateNormal];
        _saveButton.backgroundColor = mainColor;
        [_saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}

#pragma mark - private

- (void) deleteRulesWithExtId:(NSString *) freightExtId  index:(NSUInteger) index{
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager freightDeleteWithfreightId:nil orFreightExtId:freightExtId success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"规则删除成功"];
            [self updateCellsWith:index];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void) updateCellsWith:(NSUInteger) index {
    if ( [self.selectedType isEqualToString:@"2"]) {
        self.weightGroupCount--;//0 1 2 3=4  0 1 2
        //删除信息
        [self.saveWeightAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index]];
        [self.saveWeightTextDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index]];
        if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index+500]]) {
            [self.saveWeightAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index+500]];
        }
        
        //重新存储
        for (int i=(int)index+1; i<=self.weightGroupCount; i++) {
            if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i]]) {
                NSDictionary *temp = [self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveWeightAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveWeightAreaDictionary setObject:temp forKey:[NSString stringWithFormat:@"%d",i-1]];
                
            }
            if ([self.saveWeightTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]) {
                NSArray *tempArray = [self.saveWeightTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveWeightTextDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveWeightTextDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%d",i-1]];
            }
            if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i+500]]) {
                NSString *temp = [self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i+500]];
                [self.saveWeightAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i+500]];
                [self.saveWeightAreaDictionary setObject:temp forKey:[NSString stringWithFormat:@"%d",(i-1)+500]];
            }
        }
    } else {
        self.moneyGroupCount--;
        //删除信息
        [self.saveMoneyAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index]];
        [self.saveMoneyTextDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index]];
        if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index+500]]) {
            [self.saveMoneyAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index+500]];
        }
        
        //重新存储
        for (int i=(int)index+1; i<=self.moneyGroupCount; i++) {
            if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i]]) {
                NSDictionary *temp = [self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveMoneyAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveMoneyAreaDictionary setObject:temp forKey:[NSString stringWithFormat:@"%d",i-1]];
                
            }
            if ([self.saveMoneyTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]) {
                NSArray *tempArray = [self.saveMoneyTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveMoneyTextDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.saveMoneyTextDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%d",i-1]];
            }
            if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i+500]]) {
                NSString *temp = [self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i+500]];
                [self.saveMoneyAreaDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",i+500]];
                [self.saveMoneyAreaDictionary setObject:temp forKey:[NSString stringWithFormat:@"%d",(i-1)+500]];
            }
            
        }
        
    }
    [self.tableView reloadData];
}

- (void)deleteRuleAction:(UIButton *) sender {
    
    LogisticSetCenterTableViewCell *cell = (LogisticSetCenterTableViewCell *) sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    if ([self.selectedType isEqualToString:@"2"]) {
        if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",sender.tag]]) {
            [self deleteRulesWithExtId:[self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%lu",index.section+500]] index:index.section];
        } else {
            if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index.section]]) {
                
                [self updateCellsWith:index.section];
                
            }
        }
    } else {
        
        if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",sender.tag]]) {
            
            [self deleteRulesWithExtId:[self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%lu",index.section+500]] index:index.section];
        } else {
            if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index.section]]) {
                
                [self updateCellsWith:index.section];
            }
            
        }
        
    }
    
}

-(void)setLogisticParser:(BusinessLogisticListParser *)logisticParser {
    _logisticParser = logisticParser;
    
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}


-(void)setAreaListDictionary:(NSDictionary *)areaListDictionary {
    _areaListDictionary = areaListDictionary;
    //     self.selectedType = @"2";
    if ( [self.selectedType isEqualToString:@"2"]) {
        if (self.isAllArea) {
            [self.saveWeightAreaDictionary setObject:@{@"全国":@[]} forKey:[NSString stringWithFormat:@"%lu",self.indexRule]];
        } else {
            [self.saveWeightAreaDictionary setObject:_areaListDictionary forKey:[NSString stringWithFormat:@"%lu",self.indexRule]];
            
        }
        
        //    self
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:self.indexRule];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        
    } else {
        if (self.isAllArea) {
            [self.saveMoneyAreaDictionary setObject:@{@"全国":@[]} forKey:[NSString stringWithFormat:@"%lu",self.indexRule]];
        } else {
            [self.saveMoneyAreaDictionary setObject:_areaListDictionary forKey:[NSString stringWithFormat:@"%lu",self.indexRule]];
        }
        
        //    self
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:self.indexRule];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
}

- (void)addNewRule:(UIButton *) sender {
    NSUInteger nowIndex = 0;
    if ( [self.selectedType isEqualToString:@"2"]) {
        nowIndex = self.weightGroupCount-1;
    } else {
        nowIndex = self.moneyGroupCount-1;
    }
    //新添规则
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:nowIndex];
    LogisticSetCenterTableViewCell *cell = (LogisticSetCenterTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
    if (cell.areaSelectedLabel.text.length>0&&cell.firstTextField.text>0&&cell.secondTextField.text>0&&(![cell.areaSelectedLabel.text isEqualToString:@""])&&(![cell.firstTextField.text isEqualToString:@""])&&(![cell.secondTextField.text isEqualToString:@""])) {
        NSIndexSet *indexSet;
        if ( [self.selectedType isEqualToString:@"2"]) {
            indexSet = [[NSIndexSet alloc] initWithIndex:self.weightGroupCount];
            self.weightGroupCount++;
        } else {
            indexSet = [[NSIndexSet alloc] initWithIndex:self.moneyGroupCount];
            self.moneyGroupCount++;
        }
        [self.tableView beginUpdates];
        
        [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationTop];
        [self.tableView endUpdates];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    }
    
    
}

- (void)saveAction {
    [self.view endEditing:YES];
    BOOL isAllInput = YES;
    [self.rules removeAllObjects];
    if ([self.selectedType isEqualToString:@"1"]) {
        if (self.saveMoneyAreaDictionary.allKeys.count>=self.moneyGroupCount-1) {
            for (int i=1; i<self.moneyGroupCount; i++) {
                
                NSMutableDictionary *ruleDictionary = [NSMutableDictionary dictionary];
                [ruleDictionary setObject:@"1" forKey:@"type"];
                [ruleDictionary setObject:[self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i]] forKey:@"allSelectArea"];
                if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i+500]]) {
                    [ruleDictionary setObject:[self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i+500]] forKey:@"freightExtId"];
                }
                NSArray *temp = [NSArray arrayWithArray:[self.saveMoneyTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
                if (temp.count==2) {
                    if (isEmptyString([temp firstObject])||isEmptyString([temp lastObject])) {
                        isAllInput = NO;
                        break;
                    } else {
                        [ruleDictionary setObject:[temp firstObject] forKey:@"fullPrice"];
                        [ruleDictionary setObject:[temp lastObject] forKey:@"truePrice"];
                        [self.rules addObject:ruleDictionary];
                    }
                    
                    
                } else {
                    isAllInput = NO;
                    break;
                }
                
            }
            
        } else {
            isAllInput = NO;
        }
        
    } else {
        if (self.saveWeightAreaDictionary.allKeys.count>=self.weightGroupCount-1) {
            for (int i=1; i<self.weightGroupCount; i++) {
                NSMutableDictionary *ruleDictionary = [NSMutableDictionary dictionary];
                [ruleDictionary setObject:@"2" forKey:@"type"];
                [ruleDictionary setObject:[self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i]] forKey:@"allSelectArea"];
                if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i+500]]) {
                    [ruleDictionary setObject:[self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%d",i+500]] forKey:@"freightExtId"];
                }
                NSArray *temp = [NSArray arrayWithArray:[self.saveWeightTextDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
                if (temp.count==4) {
                    if (isEmptyString([temp firstObject])||isEmptyString([temp lastObject])) {
                        isAllInput = NO;
                        break;
                    } else {
                        [ruleDictionary setObject:[temp firstObject] forKey:@"first"];
                        [ruleDictionary setObject:temp[1] forKey:@"firstWeight"];
                        [ruleDictionary setObject:temp[2] forKey:@"last"];
                        [ruleDictionary setObject:[temp lastObject] forKey:@"lastWeight"];
                        [self.rules addObject:ruleDictionary];
                    }
                } else {
                    isAllInput = NO;
                    break;
                }
            }
        }else {
            isAllInput = NO;
        }
        
    }
    NSLog(@"self.rules===%@",self.rules);
    if (isAllInput) {
        
        if (self.logisticParser) {
            //编辑
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager freightAddwithFreightName:self.logisticParser.freight_name freightId:self.logisticParser.ID rules:self.rules success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                    NSLog(@"%@",state.info);
                }
                
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
            
        } else {
            //判断都输入了值
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"运费名称" message:nil preferredStyle:UIAlertControllerStyleAlert];
            //添加按钮
            __weak typeof(alertController) weakAlert = alertController;
            [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.view endEditing:YES];
                //获取输入的昵称
                NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
                if ([weakAlert.textFields.lastObject text].length==0) {
                    [XSTool showToastWithView:self.view Text: @"名称不能为空"];
                } else {
                    if ([[weakAlert.textFields.lastObject text] stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                        [XSTool showToastWithView:self.view Text:@"你好，名称不支持表情"];
                    } else {
                        NSCharacterSet *whiteString = [NSCharacterSet whitespaceCharacterSet];
                        NSString *nameString = [[NSString alloc]initWithString:[[weakAlert.textFields.lastObject text] stringByTrimmingCharactersInSet:whiteString]];
                        
                        //提交模板
                        [XSTool showProgressHUDWithView:self.view];
                        [HTTPManager freightAddwithFreightName:nameString freightId:nil rules:self.rules success:^(NSDictionary *dic, resultObject *state) {
                            [XSTool hideProgressHUDWithView:self.view];
                            if (state.status) {
                                [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                                [self.navigationController popViewControllerAnimated:YES];
                            } else {
                                [XSTool showToastWithView:self.view Text:state.info];
                            }
                            
                        } fail:^(NSError *error) {
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:NetFailure];
                        }];
                    }
                }
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                NSLog(@"点击了取消按钮");
            }]];
            
            // 添加文本框
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.textColor = [UIColor blackColor];
                textField.delegate = self;
                textField.tag = 3500;
                if (self.logisticParser) {
                    textField.text = self.logisticParser.freight_name;
                }
                textField.autocorrectionType = UITextAutocorrectionTypeNo;
                [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            }];
            
            // 弹出对话框
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    }
    
}

//- (void)unFreeAction:(UIButton *) sender {
//    sender.selected = !sender.selected;
//    if (sender.selected) {
//        sender.userInteractionEnabled = NO;
//        if (self.allFreeButton.selected) {
//            self.allFreeButton.selected = NO;
//            self.allFreeButton.userInteractionEnabled = YES;
//        }
//        //不包邮
//        self.weightGroupCount = 2;
//        self.moneyGroupCount = 2;
//        self.firstGroupCount = 2;
//        self.tableView.tableFooterView.hidden = NO;
//        [self.tableView reloadData];
//    }
//    
//}

//- (void)allFreeAction:(UIButton *) sender {
//    sender.selected = !sender.selected;
//    if (sender.selected) {
//        sender.userInteractionEnabled = NO;
//        if (self.unFreeButton.selected) {
//            self.unFreeButton.selected = NO;
//            self.unFreeButton.userInteractionEnabled = YES;
//        }
//        //全国包邮
//        self.weightGroupCount = 1;
//        self.moneyGroupCount = 1;
//        self.firstGroupCount = 1;
//        self.tableView.tableFooterView.hidden = YES;
//        [self.tableView reloadData];
//    } else {
//        
//        self.weightGroupCount = 2;
//        self.moneyGroupCount = 2;
//        self.firstGroupCount = 2;
//        self.tableView.tableFooterView.hidden = NO;
//    }
//    
//}
- (void)weightAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.userInteractionEnabled = NO;
        //按重量计算
        if (self.moneyButton.selected) {
            self.moneyButton.selected = NO;
            self.moneyButton.userInteractionEnabled = YES;
        }
        
        self.selectedType = @"2";
        [self.tableView reloadData];
        [self.rules removeAllObjects];
    }
    
}

- (void)moneyAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.userInteractionEnabled = NO;
        //按金额计算
        if (self.weightButton.selected) {
            self.weightButton.selected = NO;
            self.weightButton.userInteractionEnabled = YES;
        }
        
        self.selectedType = @"1";
        [self.tableView reloadData];
        [self.rules removeAllObjects];
    }
}
- (NSArray *)headerViewWithHeight:(CGFloat)height addToView:(UIView *)toView {
    // 注意，绝对不能给tableheaderview直接添加约束，必闪退
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, height)];
    
    if (toView) {
        [toView addSubview:headerView];
    } else {
        self.tableView.tableFooterView = headerView;
    }
    
    UIButton *addLogisticButton = [UIButton new];
    addLogisticButton.backgroundColor = [UIColor whiteColor];
    [addLogisticButton setTitle:@"添加指定地区" forState:UIControlStateNormal];
    [addLogisticButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [addLogisticButton setImage:[UIImage imageNamed:@"business_red_add"] forState:UIControlStateNormal];
    addLogisticButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    addLogisticButton.layer.cornerRadius = 5;
    addLogisticButton.layer.borderColor = LINE_COLOR_NORMAL.CGColor;
    addLogisticButton.layer.borderWidth = 1;
    addLogisticButton.layer.masksToBounds = YES;
    [headerView addSubview:addLogisticButton];
    
    [addLogisticButton addTarget:self action:@selector(addNewRule:) forControlEvents:UIControlEventTouchUpInside];
    
    [addLogisticButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(height/2-50/2);
        make.left.mas_equalTo(self.view).with.mas_offset(15);
        make.right.mas_equalTo(self.view).with.mas_offset(-15);
        make.height.mas_equalTo(50);
    }];
    
    return @[headerView, addLogisticButton];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}

- (void)textFieldDidChange:(UITextField *) textField {
    if (textField.markedTextRange == nil) {
        if (textField.text.length >20&&textField.tag==3500) {
            [XSTool showToastWithView:self.view Text:@"昵称长度只能为1~20"];
            textField.text = [textField.text substringToIndex:20];
        }
    }
}

#pragma mark - life circle
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"运费设置";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.autoHideKeyboard = YES;
    self.moneyGroupCount = 2;
    self.weightGroupCount = 2;
    self.firstGroupCount = 2;
    self.selectedType = @"2";
    self.rules = 0;
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 40, 0));
    }];
    self.tableView.backgroundColor = BG_COLOR;
    [self.view addSubview:self.saveButton];
    
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(self.view);
    }];
    NSArray *array = [self headerViewWithHeight:70 addToView:self.view];
    UIView *headerView = [array firstObject];
    [headerView layoutIfNeeded];
    [headerView removeFromSuperview];
    [self headerViewWithHeight:70 addToView:nil];
    //    [self unFreeAction:self.unFreeButton];
    [self weightAction:self.weightButton];
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    if (self.logisticParser) {
        
        //区分规则类型
        [self.weightParserArray removeAllObjects];
        [self.moneyParserArray removeAllObjects];
        self.weightGroupCount = 1;
        self.moneyGroupCount = 1;
        for (int i=0; i<self.logisticParser.ext.count; i++) {
            BusinessLogisticDetailParser *detailParser = self.logisticParser.ext[i];
            if ([detailParser.type integerValue]==2) {
                self.selectedType = @"2";
                self.weightButton.selected = YES;
                self.moneyButton.selected = NO;
                self.weightButton.userInteractionEnabled = NO;
                self.moneyButton.userInteractionEnabled = YES;
                [self.weightParserArray addObject:detailParser];
                
                NSDictionary *tempDictionary = [self dictionaryWithJsonString:detailParser.area_data_o];
                [self.saveWeightAreaDictionary setObject:tempDictionary forKey:[NSString stringWithFormat:@"%lu",self.weightGroupCount]];
                [self.saveWeightAreaDictionary setObject:detailParser.ID forKey:[NSString stringWithFormat:@"%lu",self.weightGroupCount+500]];
                
                [self.saveWeightTextDictionary setObject:@[detailParser.first, detailParser.first_weight,detailParser.last,detailParser.last_weight] forKey:[NSString stringWithFormat:@"%lu",self.weightGroupCount]];
                self.weightGroupCount ++;
            } else {
                self.selectedType = @"1";
                self.moneyButton.selected = YES;
                self.weightButton.selected = NO;
                self.weightButton.userInteractionEnabled = YES;
                self.moneyButton.userInteractionEnabled = NO;
                [self.moneyParserArray addObject:detailParser];
                
                NSDictionary *tempDictionary = [self dictionaryWithJsonString:detailParser.area_data_o];
                [self.saveMoneyAreaDictionary setObject:tempDictionary forKey:[NSString stringWithFormat:@"%lu",self.moneyGroupCount]];
                [self.saveMoneyAreaDictionary setObject:detailParser.ID forKey:[NSString stringWithFormat:@"%lu",self.moneyGroupCount+500]];
                [self.saveMoneyTextDictionary setObject:@[detailParser.full_price,detailParser.true_price] forKey:[NSString stringWithFormat:@"%lu",self.moneyGroupCount]];
                self.moneyGroupCount ++;
            }
        }
        if (self.moneyGroupCount==1) {
            self.moneyGroupCount = 2;
        }
        if (self.weightGroupCount==1) {
            self.weightGroupCount = 2;
        }
        [self.tableView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    //    NSLog(@"%@,%@",rectInSuperview ,rectInTableView);
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.tag!=3500) {
        LogisticSetCenterTableViewCell *cell =(LogisticSetCenterTableViewCell *)textField.superview.superview;
        NSIndexPath *index = [self.tableView indexPathForCell:cell];
        
        NSMutableArray *temp = [NSMutableArray array];
        NSString *keyString = [NSString stringWithFormat:@"%lu",index.section];
        if (cell.ruleType==RuleTypeMoney) {
            if ([self.saveMoneyTextDictionary.allKeys containsObject:keyString]) {
                [temp addObjectsFromArray:[self.saveMoneyTextDictionary objectForKey:keyString]];
                
            } else {
                [temp addObjectsFromArray:@[@"",@""]];
            }
            if ([textField isEqual:cell.firstTextField]) {
                
                [temp replaceObjectAtIndex:0 withObject:textField.text];
            } else {
                [temp replaceObjectAtIndex:1 withObject:textField.text];
            }
            [self.saveMoneyTextDictionary setObject:temp forKey:keyString];
        } else {
            if ([self.saveWeightTextDictionary.allKeys containsObject:keyString]) {
                [temp addObjectsFromArray:[self.saveWeightTextDictionary objectForKey:keyString]];
                
            } else {
                [temp addObjectsFromArray:@[@"",@"",@"",@""]];
            }
            if ([textField isEqual:cell.firstTextField]) {
                [temp replaceObjectAtIndex:0 withObject:textField.text];
            } else if ([textField isEqual:cell.secondTextField]){
                [temp replaceObjectAtIndex:1 withObject:textField.text];
            } else if ([textField isEqual:cell.firstWeightTextField]){
                [temp replaceObjectAtIndex:2 withObject:textField.text];
            } else if ([textField isEqual:cell.secondWeightTextField]){
                [temp replaceObjectAtIndex:3 withObject:textField.text];
            }
            [self.saveWeightTextDictionary setObject:temp forKey:keyString];
        }
        
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    if (textField.tag!=3500) {
        LogisticSetCenterTableViewCell *cell =(LogisticSetCenterTableViewCell *)textField.superview.superview;
        NSIndexPath *index = [self.tableView indexPathForCell:cell];
        
        NSMutableArray *temp = [NSMutableArray array];
        NSString *keyString = [NSString stringWithFormat:@"%lu",index.section];
        if (cell.ruleType==RuleTypeMoney) {
            if ([self.saveMoneyTextDictionary.allKeys containsObject:keyString]) {
                [temp addObjectsFromArray:[self.saveMoneyTextDictionary objectForKey:keyString]];
                
            } else {
                [temp addObjectsFromArray:@[@"",@""]];
            }
            if ([textField isEqual:cell.firstTextField]) {
                
                [temp replaceObjectAtIndex:0 withObject:textField.text];
            } else {
                [temp replaceObjectAtIndex:1 withObject:textField.text];
            }
            [self.saveMoneyTextDictionary setObject:temp forKey:keyString];
        } else {
            if ([self.saveWeightTextDictionary.allKeys containsObject:keyString]) {
                [temp addObjectsFromArray:[self.saveWeightTextDictionary objectForKey:keyString]];
                
            } else {
                [temp addObjectsFromArray:@[@"",@"",@"",@""]];
            }
            if ([textField isEqual:cell.firstTextField]) {
                [temp replaceObjectAtIndex:0 withObject:textField.text];
            } else if ([textField isEqual:cell.secondTextField]){
                [temp replaceObjectAtIndex:1 withObject:textField.text];
            } else if ([textField isEqual:cell.firstWeightTextField]){
                [temp replaceObjectAtIndex:2 withObject:textField.text];
            } else if ([textField isEqual:cell.secondWeightTextField]){
                [temp replaceObjectAtIndex:3 withObject:textField.text];
            }
            [self.saveWeightTextDictionary setObject:temp forKey:keyString];
        }
        
    }
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"logistic1Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section==0) {
        //        if (indexPath.row==0) {
        //            cell.textLabel.text = @"是否包邮";
        //            [cell.contentView addSubview:self.unFreeButton];
        //            [cell.contentView addSubview:self.allFreeButton];
        //            [self.unFreeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        //                make.left.mas_equalTo(cell.contentView).with.mas_offset(mainWidth/2);
        //                make.width.mas_equalTo(mainWidth/4);
        //                make.top.bottom.mas_equalTo(cell.contentView);
        //            }];
        //
        //            [self.allFreeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        //                make.left.mas_equalTo(self.unFreeButton.mas_right);
        //                make.width.mas_equalTo(self.unFreeButton);
        //                make.top.bottom.mas_equalTo(self.unFreeButton);
        //            }];
        //            [cell layoutIfNeeded];
        //        } else {
        cell.textLabel.text = @"计价方式";
        [cell.contentView addSubview:self.weightButton];
        [cell.contentView addSubview:self.moneyButton];
        
        [self.moneyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.contentView);
            make.width.mas_equalTo(mainWidth/4+20);
            make.top.bottom.mas_equalTo(cell.contentView);
        }];
        [self.weightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.moneyButton.mas_left);
            make.width.mas_equalTo(self.moneyButton);
            make.top.bottom.mas_equalTo(cell.contentView);
        }];
        [cell layoutIfNeeded];
        //        }
        return cell;
        
    } else {
        if (indexPath.row==0) {
            if ( [self.selectedType isEqualToString:@"2"]) {
                LogisticSetCenterTableViewCell *cell = [[LogisticSetCenterTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[LogisticSetCenterTableViewCell idString]];
                cell.delegate = self;
                cell.areaSelectedLabel.tag = indexPath.section+1000;
                //                if (self.indexRule==indexPath.section) {
                if ([self.saveWeightAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",indexPath.section]]) {
                    NSDictionary *temp = [self.saveWeightAreaDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
                    if (temp.allKeys.count) {
                        NSString *trueString = @"";
                        if (temp.allKeys.count==1&&[temp.allKeys.firstObject isEqualToString:@"全国"]) {
                            trueString = temp.allKeys.firstObject;
                        } else {
                            NSMutableString *areaString = [NSMutableString string];
                            for (int i=0; i<temp.allValues.count; i++) {
                                [areaString appendString:[temp.allValues[i] componentsJoinedByString:@","]];
                                [areaString appendString:@","];
                            }
                            
                            if (areaString.length>1) {
                                trueString = [areaString substringToIndex:areaString.length-1];
                            }
                            
                        }
                        
                        
                        cell.areaSelectedLabel.text = trueString;
                    } else {
                        cell.areaSelectedLabel.text = @"";
                    }
                } else {
                    cell.areaSelectedLabel.text = @"";
                    
                    
                }
                cell.firstTextField.text = @"";
                cell.secondTextField.text = @"";
                if ([self.saveWeightTextDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",indexPath.section]]) {
                    NSArray *tempArray = [self.saveWeightTextDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
                    cell.firstTextField.text = [tempArray firstObject];
                    
                    cell.secondTextField.text = tempArray[1];
                    cell.firstWeightTextField.text = tempArray[2];
                    cell.secondWeightTextField.text = [tempArray lastObject];
                }
                cell.firstTextField.delegate = self;
                cell.secondTextField.delegate = self;
                cell.firstWeightTextField.delegate = self;
                cell.secondWeightTextField.delegate = self;
                cell.ruleType =  [self.selectedType isEqualToString:@"2"]==YES?RuleTypeWeight:RuleTypeMoney;
                
                
                return cell;
                
            } else {
                //                LogisticSetCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LogisticSetCenterTableViewCell idString]];
                //                if (cell==nil) {
                LogisticSetCenterTableViewCell *cell = [[LogisticSetCenterTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[LogisticSetCenterTableViewCell idString]];
                //                }
                cell.delegate = self;
                cell.areaSelectedLabel.tag = indexPath.section+1000;
                //                if (self.indexRule==indexPath.section) {
                if ([self.saveMoneyAreaDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",indexPath.section] ]) {
                    NSDictionary *temp = [self.saveMoneyAreaDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
                    if (temp.allKeys.count) {
                        NSString *trueString = @"";
                        if (temp.allKeys.count==1&&[temp.allKeys.firstObject isEqualToString:@"全国"]) {
                            trueString = temp.allKeys.firstObject;
                        } else {
                            NSMutableString *areaString = [NSMutableString string];
                            for (int i=0; i<temp.allValues.count; i++) {
                                [areaString appendString:[temp.allValues[i] componentsJoinedByString:@","]];
                                [areaString appendString:@","];
                            }
                            
                            if (areaString.length>1) {
                                trueString = [areaString substringToIndex:areaString.length-1];
                            }
                            
                        }
                        cell.areaSelectedLabel.text = trueString;
                    } else {
                        cell.areaSelectedLabel.text = @"";
                    }
                } else {
                    cell.areaSelectedLabel.text = @"";
                }
                cell.firstTextField.text = @"";
                cell.secondTextField.text = @"";
                if ([self.saveMoneyTextDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",indexPath.section]]) {
                    cell.firstTextField.text = [[self.saveMoneyTextDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]] firstObject];
                    cell.secondTextField.text = [[self.saveMoneyTextDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]] lastObject];
                }
                cell.firstTextField.delegate = self;
                cell.secondTextField.delegate = self;
                cell.ruleType =  [self.selectedType isEqualToString:@"2"]==YES?RuleTypeWeight:RuleTypeMoney;
                
                
                return cell;
                
            }
            
        } else {
            NSString *idString = @"deleteCell1";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            UIButton *deleteButton = [UIButton new];
            [deleteButton setTitle:@"删除此区域" forState:UIControlStateNormal];
            deleteButton.titleLabel.font = [UIFont qsh_systemFontOfSize:18];
            [deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            deleteButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [deleteButton addTarget:self action:@selector(deleteRuleAction:) forControlEvents:UIControlEventTouchUpInside];
            deleteButton.tag = indexPath.row+500+1;
            [cell.contentView addSubview:deleteButton];
            UILabel *line = [UILabel new];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cell.contentView addSubview:line];
            [deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.left.right.mas_equalTo(cell.contentView);
            }];
            
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.right.left.mas_equalTo(cell.contentView);
                make.height.mas_equalTo(0.5);
            }];
            
            return cell;
        }
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( [self.selectedType isEqualToString:@"2"]) {
        return self.weightGroupCount;
    } else {
        return self.moneyGroupCount;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    } else if (section==1) {
        
        return 1;
    } else {
        return 2;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return 50;
    } else if (indexPath.section==1) {
        LogisticSetCenterTableViewCell *cell = (LogisticSetCenterTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    } else {
        if (indexPath.row==0) {
            LogisticSetCenterTableViewCell *cell = (LogisticSetCenterTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell.cellHeight;
        } else {
            return 50;
        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

#pragma mark - LogisticSetCenterDelegate
-(void)selectArea:(NSUInteger)index {
    [self.view endEditing:YES];
    LogisticSelectAreaViewController *areaController = [[LogisticSelectAreaViewController alloc] init];
    areaController.indexRules = index-1000;
    [self.navigationController pushViewController:areaController animated:YES];
}

-(void)selectCompany {
    LogisticChooseViewController *chooseController = [[LogisticChooseViewController alloc] init];
    [self.navigationController pushViewController:chooseController animated:YES];
}



@end
