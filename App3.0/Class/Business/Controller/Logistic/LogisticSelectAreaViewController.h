//
//  LogisticSelectAreaViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogisticSelectAreaViewController : XSBaseTableViewController

@property (nonatomic, assign) NSUInteger indexRules;//规则的第几个区域设置

/**编辑时传值*/
@property (nonatomic, strong) NSMutableDictionary *selectedNamesDictionary;

@end
