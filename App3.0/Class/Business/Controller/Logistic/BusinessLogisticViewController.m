//
//  BusinessLogisticViewController.m
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessLogisticViewController.h"
#import "ManageLogisticTableViewCell.h"
#import "BusinessDefaultView.h"
#import "SetLogisticViewController.h"
#import "BusinessLogisticModel.h"

@interface BusinessLogisticViewController ()<ManageLogisticDelegate>
@property (nonatomic, strong) UIButton *addLogisticButton;
@property (nonatomic, strong) UIButton *batchManagementButton;
@property (nonatomic, strong) BusinessDefaultView *defaultView;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) NSMutableArray *logisticListArray;

@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, assign) BOOL isShow;
@property (nonatomic, assign) NSUInteger page;

@end

@implementation BusinessLogisticViewController
#pragma mark - lazy loadding

-(UIButton *)addLogisticButton {
    if (!_addLogisticButton) {
        _addLogisticButton = [[UIButton alloc] initWithFrame:CGRectMake(0, MAIN_VC_HEIGHT-self.bottomHeight, mainWidth/2-0.5, self.bottomHeight)];
        _addLogisticButton.backgroundColor = [UIColor whiteColor];
        _addLogisticButton.titleLabel.font = [UIFont qsh_systemFontOfSize:17];
        [_addLogisticButton setTitle:@"添加物流费" forState:UIControlStateNormal];
        [_addLogisticButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_addLogisticButton setImage:[UIImage imageNamed:@"business_add"] forState:UIControlStateNormal];
        [_addLogisticButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_addLogisticButton addTarget:self action:@selector(addLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addLogisticButton;
}

- (UIButton *)batchManagementButton {
    if (!_batchManagementButton) {
        _batchManagementButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2+0.5, MAIN_VC_HEIGHT-self.bottomHeight, mainWidth/2-0.5, self.bottomHeight)];
        _batchManagementButton.backgroundColor = [UIColor whiteColor];
        _batchManagementButton.titleLabel.font = [UIFont qsh_systemFontOfSize:17];
        [_batchManagementButton setTitle:@"批量管理" forState:UIControlStateNormal];
        [_batchManagementButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_batchManagementButton setImage:[UIImage imageNamed:@"business_manage"]forState:UIControlStateNormal];
        [_batchManagementButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_batchManagementButton addTarget:self action:@selector(batchManagementAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _batchManagementButton;
}

-(BusinessDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[BusinessDefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT)];
        _defaultView.backgroundColor = BG_COLOR;
        _defaultView.tintLabel.text = @"暂时没有物流费信息";
        _defaultView.applyBtn.hidden = YES;
    }
    return _defaultView;
}

-(UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, mainHeight-self.bottomHeight, mainWidth, self.bottomHeight)];
        _doneButton.backgroundColor = mainColor;
        [_doneButton setTitle:Localized(@"完成") forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _doneButton.titleLabel.font = [UIFont qsh_systemFontOfSize:17];
        [_doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

-(NSMutableArray *)logisticListArray {
    if (!_logisticListArray) {
        _logisticListArray = [NSMutableArray array];
    }
    return _logisticListArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomHeight = 50;
    self.isShow = NO;
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"物流费管理";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    //默认界面
    [self.view addSubview:self.defaultView];
    self.tableViewStyle = UITableViewStylePlain;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, self.bottomHeight, 0));
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getLogisticInformation];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getMoreLogisticInformation];
    }];
    [self.view addSubview:self.addLogisticButton];
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2-0.5, mainHeight-self.bottomHeight, 1, self.bottomHeight)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self.view addSubview:line];
    [self.view addSubview:self.batchManagementButton];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    self.isShow = NO;
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getLogisticInformation {
    self.page = 1;
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getFreightListsWithPage:[NSString stringWithFormat:@"%lu",self.page] limit:@"10" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessLogisticParser *parser = [BusinessLogisticParser mj_objectWithKeyValues:dic[@"data"]];
            if (parser.list.count) {
                [self.logisticListArray removeAllObjects];
                [self.logisticListArray addObjectsFromArray:parser.list];
                [self.tableView reloadData];
                self.page++;
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
          [self.tableView.mj_header endRefreshing];
         [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)getMoreLogisticInformation {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getFreightListsWithPage:[NSString stringWithFormat:@"%lu",self.page] limit:@"10" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessLogisticParser *parser = [BusinessLogisticParser mj_objectWithKeyValues:dic[@"data"]];
            if (parser.list.count) {
                [self.logisticListArray addObjectsFromArray:parser.list];
                [self.tableView reloadData];
                self.page++;
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
         [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}


- (void)addLogisticAction:(UIButton *) sender {
    //运费设置
    SetLogisticViewController *setLogisticController = [[SetLogisticViewController alloc] init];
    [self.navigationController pushViewController:setLogisticController animated:YES];
}

- (void)batchManagementAction:(UIButton *) sender {
    [self.view addSubview:self.doneButton];
    self.isShow = YES;
    [self.tableView reloadData];
}

- (void)doneAction:(UIButton *) sender {
    self.isShow = NO;
    [self.tableView reloadData];
    if ([self.view.subviews containsObject:self.doneButton]) {
        [self.doneButton removeFromSuperview];
    }
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"manageLogisticCell";
    ManageLogisticTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[ManageLogisticTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
    }
    cell.listParser = self.logisticListArray[indexPath.row];
    cell.rightView.hidden = !self.isShow;
    cell.logisticDelegate = self;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logisticListArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ////运费设置
    SetLogisticViewController *setLogisticController = [[SetLogisticViewController alloc] init];
    setLogisticController.logisticParser = self.logisticListArray[indexPath.row];
    [self.navigationController pushViewController:setLogisticController animated:YES];
}

#pragma mark - ManageLogisticDelegate
-(void)editLogisticWithLogisticId:(NSString *)logisticId {
    
    //运费设置
    SetLogisticViewController *setLogisticController = [[SetLogisticViewController alloc] init];
    for (BusinessLogisticListParser *parser in self.logisticListArray) {
        if ([parser.ID isEqualToString:logisticId]) {
            setLogisticController.logisticParser = parser;
            break;
        }
    }
    
    [self.navigationController pushViewController:setLogisticController animated:YES];
}

-(void)deleteLogisticWithLogisticId:(NSString *)logisticId {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager freightDeleteWithfreightId:logisticId orFreightExtId:nil success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            for (BusinessLogisticListParser *parser in self.logisticListArray) {
                if ([parser.ID isEqualToString:logisticId]) {
                    [self.logisticListArray removeObject:parser];
                    [self.tableView reloadData];
                    break;
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
         [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
@end
