//
//  SetLogisticViewController.h
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessLogisticModel.h"

@interface SetLogisticViewController : XSBaseTableViewController

/**地区数组 ,*/
@property (nonatomic, strong) NSDictionary *areaListDictionary;

/**规则位置*/
@property (nonatomic, assign) NSUInteger indexRule;

/**模板->编辑时*/
@property (nonatomic, strong) BusinessLogisticListParser *logisticParser;

/**是否是选择了全国*/
@property (nonatomic, assign) BOOL isAllArea;
@end
