//
//  LogisticSelectAreaViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticSelectAreaViewController.h"
#import "LogisticAreaCenterTableViewCell.h"
#import "LogisticAreaTopTableViewCell.h"
#import "LogisticAreaModel.h"
#import "SetLogisticViewController.h"


@interface LogisticSelectAreaViewController ()<AreaCenterDelegate,LogisticAreaViewDelegate>
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, strong) NSMutableArray *areaListArray;
@property (nonatomic, strong) LogisticAreaListParser *areaListParser;
@property (nonatomic, strong) NSMutableArray *openOrCloseArray;
@property (nonatomic, assign) BOOL isSelectedAll;
@property (nonatomic, strong) NSMutableArray *selectedArray;
@end

@implementation LogisticSelectAreaViewController
#pragma mark - lazy loadding
-(NSMutableArray *)selectedArray {
    if (!_selectedArray) {
        _selectedArray = [NSMutableArray array];
    }
    return _selectedArray;
}

-(UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [UIButton new];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_saveButton setTitle:Localized(@"save") forState:UIControlStateNormal];
        _saveButton.backgroundColor = mainColor;
        [_saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}

-(NSMutableArray *)areaListArray {
    if (!_areaListArray) {
        _areaListArray = [NSMutableArray array];
    }
    return _areaListArray;
}

-(NSMutableDictionary *)selectedNamesDictionary{
    if (!_selectedNamesDictionary) {
        _selectedNamesDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedNamesDictionary;
}

-(NSMutableArray *)openOrCloseArray {
    if (!_openOrCloseArray) {
        _openOrCloseArray = [NSMutableArray array];
    }
    return _openOrCloseArray;
}


#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"地区选择");
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.isOpen = NO;
    self.isSelectedAll = NO;
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    self.tableView.backgroundColor = BG_COLOR;
    [self.view addSubview:self.saveButton];
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    [self getAreaInformation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getAreaInformation {
    [self.areaListArray removeAllObjects];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRegionSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            LogisticAreaParser *parser = [LogisticAreaParser mj_objectWithKeyValues:dic];
            [self.areaListArray addObjectsFromArray:parser.data];
            if (self.areaListArray.count) {
                
                //初始化  默认全部关闭
                for (int i=0; i<self.areaListArray.count; i++) {
                    NSDictionary *dic = @{[NSString stringWithFormat:@"%d",i]:@"0"};
                    [self.openOrCloseArray addObject:dic];
                }
                self.areaListParser = nil;
                self.isSelectedAll = YES;
                [self.tableView reloadData];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)saveAction {
    SetLogisticViewController *controller = (SetLogisticViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    controller.indexRule = self.indexRules;
    if (self.isSelectedAll) {
        [self.selectedNamesDictionary removeAllObjects];
        for (LogisticAreaListParser *listParser in self.areaListArray) {
            for (LogisticAreaDetailParser *detailParser in listParser.child) {
                [self.selectedNamesDictionary setObject:detailParser.child forKey:detailParser.name];
            }
        }
        
    }
    if (self.selectedNamesDictionary.allKeys.count) {
        if (self.isSelectedAll) {
            controller.isAllArea = YES;
        } else {
            controller.isAllArea = NO;
        }
        controller.areaListDictionary = self.selectedNamesDictionary;
        [self.navigationController popToViewController:controller animated:YES];
    } else {
        [XSTool showToastWithView:self.view Text:@"请选择城市"];
    }
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"topCell111";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont qsh_systemFontOfSize:20];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            
            NSString *title = @"区域选择(可多选)";
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title];
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor grayColor]
                                     range:NSMakeRange(4, title.length-4)];
            [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(4, title.length-4)];
            cell.textLabel.attributedText = attributedString;
            cell.backgroundColor = [UIColor hexFloatColor:@"E8E8E8"];
            return cell;
        } else {
            LogisticAreaTopTableViewCell *topCell= [tableView dequeueReusableCellWithIdentifier:[LogisticAreaTopTableViewCell idString]];
            if (topCell==nil) {
                topCell = [[LogisticAreaTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[LogisticAreaTopTableViewCell idString]];
            }
            if (self.isSelectedAll) {
                topCell.selectedArray = @[@"全国"];
            }
            
            if (self.selectedArray.count) {
                if ([self.selectedArray objectAtIndex:0]) {
                    NSDictionary *dic = [self.selectedArray firstObject];
                    topCell.selectedArray = [dic objectForKey:@"areaList"];
                }
            }
            
            topCell.areaView.delegate = self;
            topCell.areaListArray = self.areaListArray;
            
            return topCell;
        }
        
    } else {
        if (indexPath.row==0) {
            NSString *title = @"省市选择(可多选)";
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title];
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor grayColor]
                                     range:NSMakeRange(4, title.length-4)];
            [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(4, title.length-4)];
            cell.textLabel.attributedText = attributedString;
            cell.backgroundColor = [UIColor hexFloatColor:@"E8E8E8"];
            return cell;
        } else {

            LogisticAreaCenterTableViewCell *areaCell = [[LogisticAreaCenterTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[LogisticAreaCenterTableViewCell idString]];
            areaCell.delegate = self;
            areaCell.areaView.delegate = self;
            if (self.areaListParser) {
                
                areaCell.areaParser = self.areaListParser.child[indexPath.row-1];
            }
            if ([self.selectedNamesDictionary.allKeys containsObject:areaCell.areaParser.name]) {
                NSArray *temp = [self.selectedNamesDictionary objectForKey:areaCell.areaParser.name];
                if ([temp isEqual:areaCell.areaParser.child]) {
                    areaCell.areaButton.selected = YES;
                } else {
                    areaCell.areaButton.selected = NO;
                }
                areaCell.selectedArray = temp;
            }
            NSDictionary *dic = self.openOrCloseArray[indexPath.row-1];
            areaCell.areaShowType = [dic.allValues.firstObject integerValue];
            areaCell.upOrDownButton.selected = [dic.allValues.firstObject integerValue];

            return areaCell;
        }
        
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 2;
    } else {
        return self.areaListParser?self.areaListParser.child.count+1:0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 50;
        } else {
            LogisticAreaTopTableViewCell *cell = (LogisticAreaTopTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell.cellHeight;
        }
    } else {
        if (indexPath.row==0) {
            return 50;
        } else {
            LogisticAreaCenterTableViewCell *cell = (LogisticAreaCenterTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
            NSLog(@"cell.cellHeight:::%f",cell.cellHeight);
            return cell.cellHeight;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    //    if (indexPath.section==1) {
    //        if (indexPath.row>0) {
    //            LogisticAreaCenterTableViewCell *cell = (LogisticAreaCenterTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    //            self.tableView relo
    //        }
    //    }
}

#pragma mark - AreaCenterDelegate
- (void)openOrCloseAreaViewWithParentName:(NSString *)parentName open:(BOOL)isOpen{
    
    for (LogisticAreaDetailParser *detailArea in self.areaListParser.child) {
        
        if ([detailArea.name isEqualToString:parentName]) {
            
            NSUInteger index = [self.areaListParser.child indexOfObject:detailArea];
            //            NSDictionary *dic = self.openOrCloseArray[index-1];
            [self.openOrCloseArray removeObjectAtIndex:index];
            NSDictionary *dic = @{[NSString stringWithFormat:@"%lu",index]:isOpen?@"1":@"0"};
            [self.openOrCloseArray insertObject:dic atIndex:index];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index+1 inSection:1];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            [self.view layoutIfNeeded];
            break;
        }
    }
    
}

-(void)selectedAreaWithAreaName:(NSString *)areaName selected:(BOOL)isSelected {
    self.isSelectedAll = NO;
    //省 全选
    for (LogisticAreaDetailParser *detailArea in self.areaListParser.child) {
        
        if ([detailArea.name isEqualToString:areaName]) {
            
            if (isSelected) {
                [self.selectedNamesDictionary setObject:detailArea.child forKey:areaName];
                
            } else {
                if ([self.selectedNamesDictionary.allKeys containsObject:areaName]) {
                    [self.selectedNamesDictionary removeObjectForKey:areaName];
                }
            }
            break;
        }
    }

}

#pragma mark - LogisticAreaViewDelegate
- (void)bigAreaName:(NSString *) areaName  andIndex:(NSUInteger ) index selected:(BOOL)isSelected{
    NSLog(@"index::%lu",index);
    //切换省市
    if (index>=200&&index<500) {
        if (index==200) {
            
            //全国
            self.areaListParser = nil;
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            //全选
            if (isSelected) {
                self.isSelectedAll = YES;
                
            } else {
                self.isSelectedAll = NO;
            }
        } else {
            for (LogisticAreaListParser *parser in self.areaListArray) {
                if ([areaName isEqualToString:parser.name]) {
                    if (![self.areaListParser isEqual:parser]) {
                        self.areaListParser = parser;
                        [self.selectedArray removeAllObjects];
                        //选中的区域
                        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                        [dic setObject:@[self.areaListParser.name] forKey:@"areaList"];
                        [self.selectedArray addObject:dic];
                        
                        //闭合状态
                        [self.openOrCloseArray removeAllObjects];
                        for (int i=0; i<self.areaListParser.child.count; i++) {
                            NSDictionary *dic = @{[NSString stringWithFormat:@"%d",i]:@"0"};
                            [self.openOrCloseArray insertObject:dic atIndex:i];
                            
                        }
                        [self.tableView reloadData];
                    }
                    break;
                }
            }
        }
        
    } else {
        if (self.isSelectedAll) {
            [self.selectedNamesDictionary removeAllObjects];
        }
        self.isSelectedAll = NO;
        
        //单个省，市
        
        //所属省是否已是key
        NSString *fatherName;
        for (LogisticAreaDetailParser *parser in self.areaListParser.child) {
            if ([parser.child containsObject:areaName]) {
                fatherName = parser.name;
                break;
            }
        }
        
        //已是key
        if ([self.selectedNamesDictionary.allKeys containsObject:fatherName]) {
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[self.selectedNamesDictionary objectForKey:fatherName]];
            if (isSelected) {
                if (![tempArray containsObject:areaName]) {
                    [tempArray addObject:areaName];
                }
                [self.selectedNamesDictionary setObject:tempArray forKey:fatherName];
            } else {
                if ([tempArray containsObject:areaName]) {
                    [tempArray removeObject:areaName];
                }
                if (tempArray.count) {
                    [self.selectedNamesDictionary setObject:tempArray forKey:fatherName];
                } else {
                    [self.selectedNamesDictionary removeObjectForKey:fatherName];
                }
                [self.tableView reloadData];
                [self.view layoutIfNeeded];
                
                
            }
            
        } else {
            
            //需要新添key
            if (isSelected) {
                [self.selectedNamesDictionary setObject:@[areaName] forKey:fatherName];
            }
            
        }
      
    }
}

@end
