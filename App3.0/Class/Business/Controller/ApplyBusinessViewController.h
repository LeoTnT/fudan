//
//  ApplyBusinessViewController.h
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyBusinessViewController : XSBaseTableViewController

/**是否需要缴费*/
@property (nonatomic, copy) NSString *isEnterPay;

/**缴费金额*/
@property (nonatomic, copy) NSString *enterPayMoney;

/**营业地址*/
@property(nonatomic, strong) NSArray *areaArray;


@end
