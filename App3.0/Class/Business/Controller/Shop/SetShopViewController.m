//
//  SetShopViewController.m
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetShopViewController.h"
#import "ShopLanternSlideTableViewCell.h"
#import "TOCropViewController.h"
#import "XSCustomButton.h"
#import "ZYQAssetPickerController.h"
#import "BusinessModel.h"
#import "UIImage+XSWebImage.h"
#import "UIImageView+WebCache.h"
#import "S_BaiduMap.h"


typedef NS_ENUM(NSInteger, ChoosePhotoesType) {
    ChoosePhotoesTypeIcon,
    ChoosePhotoesTypeLanternSlide
} ;
@interface SetShopViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIImagePickerControllerDelegate,TOCropViewControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate,UITextFieldDelegate,MapLocationViewDelegate>
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UITextField *titleTextField;
@property (nonatomic, strong) UITextField *phoneField;
@property (nonatomic, strong) UILabel *industryLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *mapLabel;
@property (nonatomic, strong) UITextView *introduceTextView;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;//拍照控制器
@property (nonatomic, strong)  UIAlertController *alert;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, assign) ChoosePhotoesType choosePhotoesType;
@property (nonatomic, assign) BOOL takePhotoflag;
@property (nonatomic, strong) BusinessSupplyParser *supplyParser;
@property (nonatomic, assign) CGFloat currentY;
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, strong) UIImage *shopLogoImage;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *latitude;

@end

@implementation SetShopViewController

#pragma maek - lazy loadding

-(UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
        _icon.image = [UIImage imageNamed:@"user_fans_avatar"];
    }
    return _icon;
}

-(UITextField *)titleTextField {
    if (!_titleTextField) {
        _titleTextField = [UITextField new];
        _titleTextField.text = @"";
        _titleTextField.delegate = self;
        _titleTextField.textAlignment = NSTextAlignmentRight;
        _titleTextField.font = [UIFont qsh_systemFontOfSize:17];
        _titleTextField.returnKeyType = UIReturnKeyDone;
    }
    return _titleTextField;
}

-(UITextField *)phoneField {
    if (!_phoneField) {
        _phoneField = [UITextField new];
        _phoneField.text = @"";
        _phoneField.delegate = self;
        _phoneField.textAlignment = NSTextAlignmentRight;
        _phoneField.font = [UIFont qsh_systemFontOfSize:17];
        _phoneField.returnKeyType = UIReturnKeyDone;
        _phoneField.keyboardType = UIKeyboardTypePhonePad;
    }
    return _phoneField;
}

-(UILabel *)industryLabel {
    if (!_industryLabel) {
        _industryLabel = [UILabel new];
        _industryLabel.text = @" ";
        _industryLabel.textAlignment = NSTextAlignmentRight;
        _industryLabel.font = [UIFont qsh_systemFontOfSize:17];
    }
    return _industryLabel;
}

-(UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [UILabel new];
        _addressLabel.font = [UIFont qsh_systemFontOfSize:17];
        _addressLabel.text = @" ";
        _addressLabel.adjustsFontSizeToFitWidth = YES;
        _addressLabel.textAlignment = NSTextAlignmentRight;
    }
    return _addressLabel;
}

-(UILabel *)mapLabel {
    if (!_mapLabel) {
        _mapLabel = [UILabel new];
        _mapLabel.text = @"已设置";
        _mapLabel.textColor = [UIColor greenColor];
        _mapLabel.font = [UIFont qsh_systemFontOfSize:17];
        _mapLabel.textAlignment = NSTextAlignmentRight;
    }
    return _mapLabel;
}

-(UITextView *)introduceTextView {
    if (!_introduceTextView) {
        _introduceTextView = [[UITextView alloc] init];
        _introduceTextView.font = [UIFont qsh_systemFontOfSize:16];
        _introduceTextView.showsVerticalScrollIndicator = NO;
        _introduceTextView.showsHorizontalScrollIndicator = NO;
        _introduceTextView.autocorrectionType = UITextAutocorrectionTypeNo;
        _introduceTextView.backgroundColor = [UIColor whiteColor];
        _introduceTextView.delegate = self;
        _introduceTextView.returnKeyType = UIReturnKeyDone;
    }
    return _introduceTextView;
}

-(NSMutableArray *)photoArray {
    if (!_photoArray) {
        _photoArray = [NSMutableArray arrayWithObject:[UIImage imageNamed:@"user_fans_addphoto_thin"]];
    }
    return _photoArray;
}

#pragma make - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"店铺设置";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"save") action:^{
        
        //保存接口
        @strongify(self);
         [self.view endEditing:YES];
        NSMutableArray *nowArray = [NSMutableArray arrayWithArray:self.photoArray];
        [nowArray removeLastObject];
        if (nowArray.count&&self.iconImage&&self.titleTextField.text.length&&self.longitude&&self.latitude&&self.phoneField.text.length&&self.introduceTextView.text.length&&(![self.titleTextField.text isEqualToString:@""])&&(![self.titleTextField.text isEqualToString:@""])&&(![self.phoneField.text isEqualToString:@""])&&(![self.longitude isEqualToString:@""])&&(![self.introduceTextView.text isEqualToString:@""])&&(![self.latitude isEqualToString:@""])) {
            NSDictionary *param=@{@"type":@"logo",@"formname":@"file"};
            NSMutableArray *uploadArray = [NSMutableArray array];
            [uploadArray addObject:self.iconImage];
            [uploadArray addObjectsFromArray:nowArray];
            [XSTool showProgressHUDTOView:self.view withText:@"上传中"];
            [HTTPManager upLoadPhotosWithDic:param andDataArray:uploadArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state) {
                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:dic[@"data"]];
//                    if (tempArray.count==self.photoArray.count) {
                        NSString *logo = [tempArray firstObject];
                        [tempArray removeObjectAtIndex:0];
                        [HTTPManager supplySetSupplyInfoWithShopName:self.titleTextField.text logoImg:[NSString stringWithFormat:@"%@",logo] mobileshophome_images:[NSString stringWithFormat:@"%@",[tempArray componentsJoinedByString:@","]] longitude:isEmptyString(self.longitude)?@" ":self.longitude latitude:isEmptyString(self.latitude)?@" ":self.latitude tel:self.phoneField.text desc:self.introduceTextView.text success:^(NSDictionary *dic, resultObject *state) {
                            NSLog(@"===info:%@",dic[@"info"]);
                            if (state.status) {
                                [XSTool showToastWithView:self.view Text:@"成功保存"];
                                [self.navigationController popViewControllerAnimated:YES];
                            } else {
                                [XSTool showToastWithView:self.view Text:state.info];
                            }
                        } fail:^(NSError *error) {
                                  [XSTool hideProgressHUDWithView:self.view];
                               [XSTool showToastWithView:self.view Text:NetFailure];
                            }];
//                        }

                    } else {
                          [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                } fail:^(NSError * _Nonnull error) {
                      [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
        } else {
            [XSTool showToastWithView:self.view Text:@"请完善信息"];
        }
    }];
    self.navRightBtn.backgroundColor = mainColor;
    self.navRightBtn.layer.cornerRadius = 5;
    self.navRightBtn.layer.masksToBounds = YES;
    [self.navRightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navRightBtn.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
    self.navRightBtn.frame = CGRectMake(mainWidth-20, 15, 50, 34);
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self getSupplyShopInformation];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}


- (void)getSupplyShopInformation {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager supplyGetSupplyInfoWithUid:[UserInstance ShardInstnce].uid success:^(NSDictionary *dic, resultObject *state) {
        
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.supplyParser = [BusinessSupplyParser mj_objectWithKeyValues:dic[@"data"]];
            if (self.supplyParser.name) {
                self.titleTextField.text = self.supplyParser.name;
            } else {
                self.titleTextField.placeholder = @"请输入店铺名称";
            }
            if (self.supplyParser.tel) {
                self.phoneField.text = self.supplyParser.tel;
            } else {
                self.phoneField.placeholder = Localized(@"input_mobile");
            }
            self.latitude = self.supplyParser.latitude;
            self.longitude = self.supplyParser.longitude;
            self.mapLabel.text = self.supplyParser.latitude.length>1?@"已设置":@"未设置";
            self.introduceTextView.text = self.supplyParser.desc;
            self.iconImage = [UIImage imageNamed:@"user_fans_avatar"];
            [self.tableView reloadData];
            NSError *error;
            if (self.supplyParser.logo.length>1) {
                NSData *logoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.supplyParser.logo] options:NSDataReadingMappedIfSafe error:&error];
                self.iconImage = [UIImage imageWithData:logoData];
                NSLog(@"error:%@",error.description);
                if (self.iconImage) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:0];
                        [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                    });
                    
                }
            } else {
                self.iconImage = [UIImage imageNamed:@"chat_single"];
            }
            
            if (self.supplyParser.banner_wap) {
                NSArray *array = [self.supplyParser.banner_wap componentsSeparatedByString:@","];
                if (array.count) {
                    for (int i=0; i<array.count; i++) {
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:array[i]]];
                        UIImage *image = [UIImage imageWithData:imageData];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.photoArray insertObject:image atIndex:self.photoArray.count-1];
                                NSIndexPath *index1 = [NSIndexPath indexPathForRow:2 inSection:0];
                                [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                            });
                        }
                        
                    }
                    
                }
                
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)choosePhoto {
    [self.view endEditing:YES];
    self.alert =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.takePhotoflag = YES;
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.takePhotoflag = NO;
        self.choosePhotoesType = ChoosePhotoesTypeIcon;
        //打开相册
        self.imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        }
        self.imagePickVC.delegate = self;
        self.imagePickVC.allowsEditing = NO;
        [self presentViewController:self.imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [self.alert addAction:photograph];
    [self.alert addAction:select];
    [self.alert addAction:cancel];
    [self presentViewController:self.alert animated:YES completion:nil];
}


- (void)choosePhotoes {
    [self.view endEditing:YES];
    self.alert =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.takePhotoflag = YES;
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.takePhotoflag = NO;
        self.choosePhotoesType = ChoosePhotoesTypeLanternSlide;
        
        //打开相册
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 3;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [self.alert addAction:photograph];
    [self.alert addAction:select];
    [self.alert addAction:cancel];
    [self presentViewController:self.alert animated:YES completion:nil];
}

- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        //后置摄像头是否可用
        if (!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
        
    }];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    TOCropViewController *toVC;
    if (self.takePhotoflag) {
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        if (self.choosePhotoesType==ChoosePhotoesTypeIcon) {
            toVC=[[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
            toVC.delegate=self;
            [picker presentViewController:toVC animated:NO completion:nil];
        } else {
            if (self.photoArray.count>0) {
                NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
                if (imageData.length>1024*1024*2) {//2M以及以上
                    if (imageData.length<1024*1024*4) {
                        imageData = UIImageJPEGRepresentation(image, 0.5);
                    } else if (imageData.length<1024*1024*10) {
                        imageData = UIImageJPEGRepresentation(image, 0.2);
                        
                    } else {
                        imageData = UIImageJPEGRepresentation(image, 0.1);
                    }
                }
                [self.photoArray insertObject:[UIImage imageWithData:imageData] atIndex:self.photoArray.count-1];
                [self dismissViewControllerAnimated:YES completion:nil];
                
                //刷新表格
                NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
            }
            
        }
        
    }else{
        
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"])
        {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            if (self.choosePhotoesType==ChoosePhotoesTypeIcon) {
                toVC = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
                toVC.delegate=self;
                [picker pushViewController:toVC animated:YES];
            }
            
        }
    }
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = image;
    [self dismissViewControllerAnimated:YES completion:^{
        NSIndexPath *index;
        if (self.choosePhotoesType==ChoosePhotoesTypeIcon) {
            NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
            if (imageData.length>1024*1024*2) {//2M以及以上
                if (imageData.length<1024*1024*4) {
                    imageData = UIImageJPEGRepresentation(image, 0.5);
                } else if (imageData.length<1024*1024*10) {
                    imageData = UIImageJPEGRepresentation(image, 0.2);
                    
                } else {
                    imageData = UIImageJPEGRepresentation(image, 0.1);
                }
            }
            self.iconImage = [UIImage imageWithData:imageData];
            index = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }];
}
#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.takePhotoflag) {
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{

            [weakSelf.imagePickVC dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)deletePicture:(UIButton *)button {
    ShopLanternSlideTableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    [self.photoArray removeObjectAtIndex:index];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - ZYQAssetPickerController Delegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            if (self.photoArray.count>0) {
                NSData *imageData=UIImageJPEGRepresentation(result, 1.0);
                if (imageData.length>1024*1024*2) {//2M以及以上
                    if (imageData.length<1024*1024*4) {
                        imageData = UIImageJPEGRepresentation(result, 0.5);
                    } else if (imageData.length<1024*1024*10) {
                        imageData = UIImageJPEGRepresentation(result, 0.2);
                        
                    } else {
                        imageData = UIImageJPEGRepresentation(result, 0.1);
                    }
                }
                [self.photoArray insertObject:result atIndex:self.photoArray.count-1];
                NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择3张图片"];
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"topCell";
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            idString = @"cellWithIcon";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"店铺头像";
            [cell.contentView addSubview:self.icon];
            [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.top.mas_equalTo(cell.contentView).with.mas_offset(5);
                make.bottom.mas_equalTo(cell.contentView).with.mas_offset(-5);
                make.width.mas_equalTo(40);
            }];
            self.icon.layer.cornerRadius = 20;
            self.icon.layer.masksToBounds = YES;
            self.icon.image = self.iconImage;
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhoto)];
            self.icon.userInteractionEnabled = YES;
            [self.icon addGestureRecognizer:tap];
            return cell;
        } else if ( indexPath.row==1) {
            idString = @"cellWithTextfield1";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.text = @"店铺名称";
            
            [cell.contentView addSubview:self.titleTextField];
            [self.titleTextField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.width.mas_equalTo(mainWidth-100);
                make.top.bottom.mas_equalTo(cell.contentView);
            }];
            
            return cell;
        } else {
            ShopLanternSlideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sliderCells"];
            if (cell==nil) {
                cell = [[ShopLanternSlideTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"sliderCells"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.photosArray = self.photoArray;
            //给cell的删除按钮绑定方法
            for (int i=0; i<cell.deletBtnArray.count; i++) {
                [[cell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
            }
            [cell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotoes)]];
            return cell;
        }
    } else {
        if (indexPath.row==0) {
            idString = @"cellWithText1";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.text = @"所属行业";
            [cell.contentView addSubview: self.industryLabel];
            [self.industryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.width.mas_equalTo(mainWidth-100);
            }];
            if (self.supplyParser) {
                self.industryLabel.text = self.supplyParser.iname;
            }
            return cell;
        } else if (indexPath.row==1) {
            idString = @"cellWithText2";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.text = @"店铺地址";
            [cell.contentView addSubview: self.addressLabel];
            [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.width.mas_equalTo(mainWidth-100);
            }];
            if (self.supplyParser) {
                self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",self.supplyParser.province,self.supplyParser.city,self.supplyParser.county,self.supplyParser.town];
            }
            return cell;
        } else if (indexPath.row==2) {
            idString = @"cellWithText3";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.text = @"地图标注";
            [cell.contentView addSubview: self.mapLabel];
            [self.mapLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.width.mas_equalTo(mainWidth-100);
            }];
            
            return cell;
        } else if (indexPath.row==3) {
            idString = @"cellWithTextfield2";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.text = @"绑定电话";
            [cell.contentView addSubview:self.phoneField];
            [self.phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(mainWidth).with.mas_offset(-10);
                make.width.mas_equalTo(mainWidth-100);
                make.top.bottom.mas_equalTo(cell.contentView);
            }];
            
            return cell;
        } else {
            idString = @"cellWithTextfield3";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 13, 100, 30)];
            titleLabel.text = @"店铺介绍";
            [cell.contentView addSubview:titleLabel];
            [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(cell.contentView).with.mas_offset(13);
                make.left.mas_equalTo(cell.contentView).with.mas_offset(13);
                make.width.mas_equalTo(100);
                make.height.mas_equalTo(30);
            }];
            
            
            [cell.contentView addSubview:self.introduceTextView];
            [self.introduceTextView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(cell.contentView).with.mas_offset(10);
                make.left.mas_equalTo(titleLabel.mas_right);
                make.bottom.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(cell.contentView).with.mas_offset(-10);
            }];
            return cell;
        }
        
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 3;
    } else {
        return 5;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==2) {
            ShopLanternSlideTableViewCell *cell = (ShopLanternSlideTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            
            return cell.height;
        } else {
            return 50;
        }
    } else {
        if (indexPath.row<4) {
            return 50;
        } else {
            return 150;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==1) {
        return 20;
    } else {
        return 0.01;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==1&&indexPath.row==2) {
        S_BaiduMap *locationController = [S_BaiduMap new];
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        [self.navigationController pushViewController:locationController animated:YES];
        
    }
    [self.view endEditing:YES];
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    UITableViewCell *cell =(UITableViewCell *)textView.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
    
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        //这里"\n"对应的是键盘的 return 回收键盘之用
        
        [textView resignFirstResponder];
        
        return NO;
        
    } else {
        return YES;
    }
    
}


#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - MapLocationViewController Delegate

- (void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address {
    self.latitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
    self.mapLabel.text = @"已修改";
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
}
@end
