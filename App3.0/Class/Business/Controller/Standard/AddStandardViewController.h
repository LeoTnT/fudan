//
//  AddStandardViewController.h
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddStandardViewController : XSBaseTableViewController

/**已选平台分类id*/
@property (nonatomic, copy) NSString *categoryId;

@end
