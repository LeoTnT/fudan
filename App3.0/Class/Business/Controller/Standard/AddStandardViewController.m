//
//  AddStandardViewController.m
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddStandardViewController.h"
#import "AddStandardTableViewCell.h"
#import "GoodsStandardViewController.h"

@interface AddStandardViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AddStandstardCellDelegate>
@property (nonatomic, assign) NSUInteger cellCount;//cell个数
@property (nonatomic, assign) CGFloat currentY;
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, strong) NSMutableDictionary *valueDictionary;

@end

@implementation AddStandardViewController

#pragma mark - lazy loadding
-(NSMutableDictionary *)valueDictionary {
    if (!_valueDictionary) {
        _valueDictionary = [NSMutableDictionary dictionary];
    }
    return _valueDictionary;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加商品规格";
    self.view.backgroundColor = BG_COLOR;
    self.autoHideKeyboard = YES;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"save") action:^{
        
        //保存接口
        @strongify(self);
        [self.view endEditing:YES];
        if (self.valueDictionary.allKeys.count<2&&self.valueDictionary.allKeys.count>0) {
            [XSTool showToastWithView:self.view Text:@"请添加至少一个规格值！"];
        } else {
            if ([self.valueDictionary.allValues containsObject:@""]||self.valueDictionary.allKeys.count<self.cellCount) {
                [XSTool showToastWithView:self.view Text:@"请完善信息"];
            } else {
                [self.valueDictionary objectForKey:@"0"];
                NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:self.valueDictionary];
                [temp removeObjectForKey:@"0"];
                NSMutableArray *specArray = [NSMutableArray arrayWithArray:temp.allValues];
                [XSTool showProgressHUDWithView:self.view];
                [HTTPManager supplyInsertSpecAndSpecValueWithSpecName:[self.valueDictionary objectForKey:@"0"] specValueData:[specArray componentsJoinedByString:@","] cid:self.categoryId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        UIAlertController *alertContoller = [UIAlertController alertControllerWithTitle:nil message:@"添加成功，您要继续添加吗？" preferredStyle:UIAlertControllerStyleAlert] ;
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                             GoodsStandardViewController *controller = (GoodsStandardViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                            [controller getStandardInformation];
                            [self.navigationController popToViewController:controller animated:YES];
                        }];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                        }];
                        [alertContoller addAction:cancelAction];
                        [alertContoller addAction:okAction];
                        [self presentViewController:alertContoller animated:YES completion:nil];
                        
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            }
            
        }
        
    }];
    self.navRightBtn.backgroundColor = mainColor;
    self.navRightBtn.layer.cornerRadius = 5;
    self.navRightBtn.layer.masksToBounds = YES;
    [self.navRightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navRightBtn.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
    self.navRightBtn.frame = CGRectMake(mainWidth-20, 15, 50, 34);
    self.cellCount = 2;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    NSArray *array = [self footerViewWithHeight:60 addToView:self.view];
    UIView *footerView = [array firstObject];
    [footerView layoutIfNeeded];
    [footerView removeFromSuperview];
    [self footerViewWithHeight:60 addToView:nil];
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - private
- (NSArray *)footerViewWithHeight:(CGFloat)height addToView:(UIView *)toView {
    // 注意，绝对不能给tableheaderview直接添加约束，必闪退
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, height)];
    
    if (toView) {
        [toView addSubview:footerView];
    } else {
        self.tableView.tableFooterView = footerView;
    }
    
    UIButton *addButton = [UIButton new];
    addButton.backgroundColor = mainColor;
    [addButton setTitle:@"增加值" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.layer.cornerRadius = 5;
    addButton.layer.borderColor = LINE_COLOR_NORMAL.CGColor;
    addButton.layer.borderWidth = 1;
    addButton.layer.masksToBounds = YES;
    [footerView addSubview:addButton];
    
    [addButton addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
    
    [addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(mainWidth/2-13-50);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(40);
    }];
    
    return @[footerView, addButton];
}

- (void)addAction {
    self.cellCount++;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.cellCount-1 inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}

#pragma mark - AddStandstardCellDelegate
-(void)deleteCell:(UIButton *)sender {
    [self.view endEditing:YES];
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    if ([self.valueDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index.row]]) {
        [self.valueDictionary removeObjectForKey:[NSString stringWithFormat:@"%lu",index.row]];
        //重新存储
        for (int i=(int)index.row+1; i<self.cellCount; i++) {
            if ([self.valueDictionary.allKeys containsObject:[NSString stringWithFormat:@"%d",i]]) {
                NSString *valueString = [self.valueDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
                [self.valueDictionary setObject:valueString forKey:[NSString stringWithFormat:@"%d",i-1]];
                
            }
        }
        NSLog(@"%@",self.valueDictionary);
    }
    self.cellCount--;
    [self.tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
    
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    [self.valueDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%lu",index.row]];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    [self.valueDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%lu",index.row]];
    
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    [self.valueDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%lu",index.row]];
    return YES;
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddStandardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AddStandardTableViewCell idString]];
    if (cell==nil) {
        cell = [[AddStandardTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[AddStandardTableViewCell idString]];
        cell.textField.delegate = self;
        
    }
    if (indexPath.row==0) {
        cell.titleLabel.text = @"*规格名：";
        cell.deleteButton.hidden = YES;
        
    } else {
        
        cell.titleLabel.text = @"*规格值：";
        cell.deleteButton.hidden = YES;
        if (indexPath.row>1) {
            cell.deleteButton.hidden = NO;
            cell.delegate = self;
        }
        
        
    }
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellCount;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddStandardTableViewCell *cell = (AddStandardTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

@end
