//
//  StandardMainViewController.m
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "StandardMainViewController.h"
#import "AddStandardViewController.h"
#import "StandardMainTableViewCell.h"
#import "BusinessModel.h"
#import "BusinessDefaultView.h"
#import "NSString+Regular.h"

@interface StandardMainViewController ()<UITableViewDelegate,UITableViewDataSource,BusinessDefaultViewDelegate,StandardMainTableViewCellDelegate,UITextFieldDelegate,SpecValueViewDelegate>
@property (nonatomic, strong) NSMutableArray *specArray;
@property (nonatomic, strong) BusinessDefaultView *defaultView;
@property (nonatomic, strong) NSMutableDictionary *selectedSpecValueDictionary;
@end

@implementation StandardMainViewController
#pragma mark - lazy loadding
-(NSMutableDictionary *)selectedSpecValueDictionary {
    if (!_selectedSpecValueDictionary) {
        _selectedSpecValueDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedSpecValueDictionary;
}

-(BusinessDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [BusinessDefaultView new];
        _defaultView.tintLabel.text = @"您还没有商家规格";
        _defaultView.applyBtn.hidden = YES;
    }
    return _defaultView;
}

-(NSMutableArray *)specArray {
    if (!_specArray) {
        _specArray = [NSMutableArray array];
    }
    return _specArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商品规格管理";
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.autoHideKeyboard = YES;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
//    [self actionCustomRightBtnWithNrlImage:@"business_add" htlImage:nil title:@"新增规格" action:^{
//        @strongify(self);
//        [self.view endEditing:YES];
//        AddStandardViewController *controller = [[AddStandardViewController alloc] init];
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    
    [self.view addSubview:self.defaultView];
   
    [self.defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    self.defaultView.hidden = YES;
    self.tableView.hidden = YES;
    NSArray *array = [self headerViewWithHeight:60 addToView:self.view];
    UIView *headerView = [array firstObject];
    [headerView layoutIfNeeded];
    [headerView removeFromSuperview];
    [self headerViewWithHeight:60 addToView:nil];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     [self getSpecInformation];
}

#pragma mark - private
- (NSArray *)headerViewWithHeight:(CGFloat)height addToView:(UIView *)toView {
    // 注意，绝对不能给tableheaderview直接添加约束，必闪退
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, height)];
    
    if (toView) {
        [toView addSubview:headerView];
    } else {
        self.tableView.tableHeaderView = headerView;
    }
    
    UITextView *tint = [UITextView new];
    tint.userInteractionEnabled = NO;
    tint.font = [UIFont qsh_systemFontOfSize:16];
    tint.text = @"说明：商品规格用于商品发布，常见规格有颜色，尺寸，材质等。";
    [headerView addSubview:tint];
    [tint mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(60);
    }];
    
    return @[headerView, tint];
}

- (void)getSpecInformation {
   
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplyGetSpecBySupplySuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
             BusinessSpecParser *parser = [BusinessSpecParser mj_objectWithKeyValues:dic];
            if (parser.data.count) {
                self.defaultView.hidden = YES;
                self.tableView.hidden = NO;
                [self.specArray removeAllObjects];
                [self.specArray addObjectsFromArray:parser.data];
                [self.tableView reloadData];
            } else {
                self.defaultView.hidden = NO;
                self.tableView.hidden = YES;
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)editSpecName:(UIButton *) sender {
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    BusinessSpecListParser *listParser = self.specArray[index.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"规格" message:nil preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alertController) weakAlert = alertController;
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.view endEditing:YES];
        //获取输入的昵称
        if ([weakAlert.textFields.lastObject text].length==0) {
            [XSTool showToastWithView:self.view Text: @"规格不能为空"];
        } else {
            if ([[weakAlert.textFields.lastObject text] stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                [XSTool showToastWithView:self.view Text:@"你好，规格不支持表情"];
            } else {
                NSCharacterSet *whiteString = [NSCharacterSet whitespaceCharacterSet];
                NSString *nameString = [[NSString alloc]initWithString:[[weakAlert.textFields.lastObject text] stringByTrimmingCharactersInSet:whiteString]];
                
                //提交
                [XSTool showProgressHUDWithView:self.view];
                
                [HTTPManager supplyUpdateSpecName:nameString specId:listParser.ID success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [self getSpecInformation];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                    
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            }
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    // 添加文本框
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.textColor = [UIColor blackColor];
        textField.delegate = self;
        textField.tag = 3600;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    // 弹出对话框
    [self presentViewController:alertController animated:YES completion:nil];

}

- (void)deleteSpec:(UIButton *) sender {
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    BusinessSpecListParser *listParser = self.specArray[index.row];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplyDeleteSpecAndSpecValueSpecId:listParser.ID success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self getSpecInformation];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
 
}

- (void)addSpecValue:(UIButton *) sender {
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    BusinessSpecListParser *listParser = self.specArray[index.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"规格值" message:nil preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alertController) weakAlert = alertController;
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.view endEditing:YES];
        //获取输入的昵称
       
        if ([weakAlert.textFields.lastObject text].length==0) {
            [XSTool showToastWithView:self.view Text: @"规格值不能为空"];
        } else {
            if ([[weakAlert.textFields.lastObject text] stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                [XSTool showToastWithView:self.view Text:@"你好，规格值不支持表情"];
            } else {
                NSCharacterSet *whiteString = [NSCharacterSet whitespaceCharacterSet];
                NSString *nameString = [[NSString alloc]initWithString:[[weakAlert.textFields.lastObject text] stringByTrimmingCharactersInSet:whiteString]];
                
                //提交
                [XSTool showProgressHUDWithView:self.view];

                [HTTPManager supplyInsertSpecAndSpecValueWithSpecId:listParser.ID specValueName:nameString success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [self getSpecInformation];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                    
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            }
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
      
    }]];
    
    // 添加文本框
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.textColor = [UIColor blackColor];
        textField.delegate = self;
        textField.tag = 3500;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    // 弹出对话框
    [self presentViewController:alertController animated:YES completion:nil];
   
}

- (void)deleteSpecValue:(UIButton *) sender {
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    BusinessSpecListParser *listParser = self.specArray[index.row];
    NSArray *temp;
    if ([self.selectedSpecValueDictionary.allKeys containsObject:[NSString stringWithFormat:@"%lu",index.row]]) {
        temp = [self.selectedSpecValueDictionary objectForKey:[NSString stringWithFormat:@"%lu",index.row]];
        if (temp.count) {
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager supplyDeleteSpecValue:listParser.ID specValueId:[temp componentsJoinedByString:@","] success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [self getSpecInformation];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];

        } else {
            [XSTool showToastWithView:self.view Text:@"请选择规格值"];
        }
    } else {
     [XSTool showToastWithView:self.view Text:@"请选择规格值"];
    }
}

- (void)textFieldDidChange:(UITextField *) textField {
    if (textField.markedTextRange == nil) {
        if (textField.text.length >20) {
            if (textField.tag==3500||textField.tag==3600) {
                [XSTool showToastWithView:self.view Text:@"规格值长度只能为1~20"];
                textField.text = [textField.text substringToIndex:20];
            }
            
        }
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
   
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}


#pragma mark - BusinessDefaultViewDelegate
-(void)clicktoApplyBusinessEdition {
    AddStandardViewController *controller = [[AddStandardViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -  StandardMainTableViewCellDelegate
-(void)selectedSpecValue:(BusinessSpecDetailParser *)specParser selected:(BOOL)isSelected{
    for (int i=0; i<self.specArray.count; i++) {
        BusinessSpecListParser *listParser = self.specArray[i];
        for (int j=0; j<listParser.child.count; j++) {
            BusinessSpecDetailParser *detailParser = listParser.child[j];
            if ([detailParser.name isEqualToString:specParser.name]) {
                NSString *keyString = [NSString stringWithFormat:@"%d",i];
                NSMutableArray *temp = [NSMutableArray array];
                //存储选择删除的规格值  规格值数组：cell位置
                if ([self.selectedSpecValueDictionary.allKeys containsObject:keyString]) {
                    temp = [self.selectedSpecValueDictionary objectForKey:keyString];
                }
                if (isSelected) {
                    
                    //存储
                    [temp addObject:specParser.ID];
                   
                } else {
                
                    //取消存储
                    [temp removeObject:specParser.ID];
                    
                }
                 [self.selectedSpecValueDictionary setObject:temp forKey:keyString];
                break;
            }
        }
        
    }

}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StandardMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[StandardMainTableViewCell idString]];
    if (cell==nil) {
        cell = [[StandardMainTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[StandardMainTableViewCell idString]];
    }
    cell.delegate = self;
    BusinessSpecListParser *specParser = self.specArray[indexPath.row];
    cell.nameLabel.text = specParser.name;
    if (specParser.child.count) {
        cell.specValueArray = specParser.child;
        cell.specView.delegate = self;
    }
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.specArray.count) {
        return self.specArray.count;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    StandardMainTableViewCell *cell = (StandardMainTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}


@end
