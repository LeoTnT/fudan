//
//  TransferRecordViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LoanWithDrawRecordViewController.h"
#import "BusinessModel.h"
#import "BusinessTransferAccountRecordTableViewCell.h"
#import "BusinessDefaultView.h"

@interface LoanWithDrawRecordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSMutableArray *recordArray;
@property(nonatomic,assign)int page;
/**保存cell高度的字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
@property(nonatomic,strong)BusinessDefaultView *defaultView;
@property(nonatomic,strong)UIView *topView;
@end

@implementation LoanWithDrawRecordViewController

#pragma mark -lazy loadding
-(NSMutableDictionary *)heightDic {
    if (!_heightDic) {
        _heightDic = [NSMutableDictionary dictionary];
    }
    return _heightDic;
}

-(NSMutableArray *)recordArray {
    if (!_recordArray) {
        _recordArray = [NSMutableArray array];
    }
    return _recordArray;
}

-(UIView *)topView {
    
    if (!_topView) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 10*3)];
        _topView.backgroundColor = [UIColor clearColor];
        UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 10*4, CGRectGetHeight( _topView.frame))];
        desc.text = @"说明：";
        desc.textColor = mainColor;
        desc.font = [UIFont systemFontOfSize:10];
        [ _topView addSubview:desc];
        for (int i=0; i<3; i++) {
            @autoreleasepool {
                CGFloat width = 10*7;
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(desc.frame)+width*i+10*i, 0, width, CGRectGetHeight( _topView.frame))];
                [btn setTitleColor:mainGrayColor forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:10];
                if (i==0) {
                    [btn setTitle:@"正在审核" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review"] forState:UIControlStateNormal];
                    
                }else if(i==1){
                    [btn setTitle:@"审核已通过" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review_success"] forState:UIControlStateNormal];
                    
                }else{
                    [btn setTitle:@"审核未通过" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review_fail"] forState:UIControlStateNormal];
                }
                btn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(btn.imageView.frame), 0, 0);
                [ _topView addSubview:btn];
            }
        }
    }
    return _topView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubviews];
    [self getNewRecord];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)setSubviews{
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = @"提现记录";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    //    self.tableView.bounces = NO;
    
    //设置刷新样式
    [self setRefreshStyle];
    self.defaultView = [[BusinessDefaultView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topView.frame), mainWidth, mainHeight-CGRectGetMaxY(self.topView.frame))];
    self.defaultView.tintLabel.text = @"还没有提现记录哦";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.defaultView.applyBtn setTitle:@"去提现" forState:UIControlStateNormal];
    [self.defaultView.applyBtn setTitleColor:mainColor forState:UIControlStateNormal];
    self.defaultView.applyBtn.backgroundColor = BG_COLOR;
    self.defaultView.applyBtn.layer.cornerRadius = 5;
    self.defaultView.applyBtn.layer.borderWidth = 1;
    self.defaultView.applyBtn.layer.borderColor = mainColor.CGColor;
    self.defaultView.applyBtn.layer.masksToBounds = YES;
    [self.defaultView.applyBtn addTarget:self action:@selector(toWithDrawMoney) forControlEvents:UIControlEventTouchUpInside];
    self.defaultView.hidden = YES;
    [self.view addSubview: self.defaultView];
}

- (void)toWithDrawMoney {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-设置刷新样式
-(void)setRefreshStyle{
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewRecord];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldRecord];
    }];
    
}
#pragma mark-获取新的网络数据
-(void)getNewRecord{
    self.page=1;
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager userTakecashListsWithWalletType:self.wallet_type page:[NSString stringWithFormat:@"%d",self.page] limit:@"5" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            BusinessTakecashParser *withDraw = [BusinessTakecashParser mj_objectWithKeyValues: dic[@"data"][@"list"]];
            if (withDraw.data.count>0) {
                self.defaultView.hidden=YES;
                self.view.backgroundColor=BG_COLOR;
                [self.recordArray removeAllObjects];
                [self.recordArray addObjectsFromArray: withDraw.data];
                
                
            } else {
                self.defaultView.hidden=NO;
                self.view.backgroundColor=[UIColor whiteColor];
            }
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
#pragma mark-获取旧的数据
-(void)getOldRecord{
    self.page++;
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager userTakecashListsWithWalletType:self.wallet_type page:[NSString stringWithFormat:@"%d",self.page] limit:@"5" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            BusinessTakecashParser *withDraw = [BusinessTakecashParser mj_objectWithKeyValues: dic[@"data"][@"list"]];
            if (withDraw.data.count>0) {
                [self.recordArray addObjectsFromArray:withDraw.data];
                [self.tableView reloadData];
            }else{
                self.page--;
            }
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:@"暂无更多记录"];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        self.page--;
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"cellId";
    if (indexPath.row==0) {
        cellId = @"topcell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        }
        [cell.contentView addSubview:self.topView];
        self.heightDic[@(indexPath.row)]=@(CGRectGetHeight(self.topView.frame));
        return cell;
    } else {
        BusinessTransferAccountRecordTableViewCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[BusinessTransferAccountRecordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.withDrawRecord=[self.recordArray objectAtIndex:indexPath.row-1];
        //记录高度
        self.heightDic[@(indexPath.row)]=@(cell.height);
        return cell;
    }
    
}
#pragma mark-行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.recordArray.count+1;
}
#pragma mark-设置高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.heightDic[@(indexPath.row)] floatValue]==0) {
        return 0.1;
    }
    return [self.heightDic[@(indexPath.row)] floatValue];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
