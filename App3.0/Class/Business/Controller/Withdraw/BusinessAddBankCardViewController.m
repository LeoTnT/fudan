//
//  BusinessAddBankCardViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessAddBankCardViewController.h"
#import "XSCustomButton.h"
#import "BusinessModel.h"
#import "BankCardTypeView.h"
#import "BusinessBankCardViewController.h"
#define footerHeight 120
@interface BusinessAddBankCardViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CardTypeDelegate>
@property (nonatomic, strong) UITextField *accountBank;//开户支行
@property (nonatomic, strong) UITextField *bankAddress;//支行地址
@property (nonatomic, strong) UITextField *bankAccount;//银行账号
@property (nonatomic, strong) NSMutableArray *typeArray;//银行卡类型数组
@property (nonatomic, strong) NSString *typeId;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, strong) BankCardTypeView *typeView;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) UILabel *bankTypeLabel;
@property (nonatomic, strong) UILabel *accountBankLabel;
@property (nonatomic, strong) UILabel *bankAddressLabel;
@property (nonatomic, strong) UILabel *bankAccountLabel;
@end

@implementation BusinessAddBankCardViewController

#pragma mark - lazy loadding
-(UILabel *)bankTypeLabel {
    if (!_bankTypeLabel) {
        _bankTypeLabel = [[UILabel alloc] init];
    }
    return _bankTypeLabel;
    
}

-(UILabel *)accountBankLabel {
    if (!_accountBankLabel) {
        _accountBankLabel = [[UILabel alloc] init];
    }
    return _accountBankLabel;
}

-(UILabel *)bankAddressLabel {
    if (!_bankAddressLabel) {
        _bankAddressLabel = [[UILabel alloc] init];
    }
    return _bankAddressLabel;
    
}

-(UILabel *)bankAccountLabel {
    if (!_bankAccountLabel) {
        _bankAccountLabel = [[UILabel alloc] init];
    }
    return _bankAccountLabel;
}

- (UITextField *)accountBank {
    if (!_accountBank) {
        _accountBank = [[UITextField alloc] init];
        _accountBank.textAlignment = NSTextAlignmentRight;
        _accountBank.returnKeyType = UIReturnKeyNext;
        _accountBank.delegate = self;
    }
    return _accountBank;
}

- (UITextField *)bankAddress {
    if (!_bankAddress) {
        _bankAddress = [[UITextField alloc] init];
        _bankAddress.textAlignment = NSTextAlignmentRight;
        _bankAddress.returnKeyType = UIReturnKeyNext;
        _bankAddress.delegate = self;
    }
    return _bankAddress;
}

- (UITextField *)bankAccount {
    if (!_bankAccount) {
        _bankAccount = [[UITextField alloc] init];
        _bankAccount.textAlignment = NSTextAlignmentRight;
        _bankAccount.returnKeyType = UIReturnKeyDone;
        _bankAccount.delegate = self;
    }
    return _bankAccount;
}

- (NSMutableArray *)typeArray {
    if (!_typeArray) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}

- (BankCardTypeView *)typeView {
    if (!_typeView) {
        _typeView = [[BankCardTypeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.viewType = AlertTypeBankCard;
        _typeView.typeDelegate = self;
    }
    return _typeView;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellHeight = 45;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = Localized(@"add_bank");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    if (self.isAdd) {
        _typeName = Localized(@"choose");
    }
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)setSubviews {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
}

- (void)hiddenCardView:(CardTypeDataParser *)dataParser {
    self.typeId = dataParser.ID?dataParser.ID:@"";
    _typeName = dataParser.name?dataParser.name:Localized(@"choose");
    //消失
    [self.typeView removeFromSuperview];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)popViewController{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)submitCardAction:(UIButton *) sender {
    [self.view endEditing:YES];
    if ([self.accountBank.text isEqualToString:@""]||self.accountBank.text.length == 0||[_typeName isEqualToString:Localized(@"choose")]||self.typeId==nil||[self.bankAddress.text isEqualToString:@""]||self.bankAddress.text.length==0||[self.bankAccount.text isEqualToString:@""]||self.bankAccount.text.length==0) {
        [XSTool showToastWithView:self.view Text:@"请完善信息！"];
    } else {
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *accountString = [[NSString alloc]initWithString:[self.accountBank.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *bankString = [[NSString alloc]initWithString:[self.bankAccount.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *addressString = [[NSString alloc]initWithString:[self.bankAddress.text stringByTrimmingCharactersInSet:whiteSpace]];
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager addBankCardWithBankName:accountString bankId:self.typeId bankCard:bankString bankAddress:addressString success:^(NSDictionary * _Nullable dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
    }
    
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"cCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0: {
                cell.textLabel.text = Localized(@"银行类型");
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.detailTextLabel.text =_typeName;
            }
                break;
            case 1: {
                cell.textLabel.text = Localized(@"开户支行");
                self.accountBank.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth-NORMOL_SPACE*10, self.cellHeight);
                if (self.isAdd) {
                    self.accountBank.placeholder = Localized(@"如：高新支行");
                }
                [cell.contentView addSubview:self.accountBank];
                
            }
                break;
            case 2: {
                cell.textLabel.text = @"支行地址";
                self.bankAddress.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth- NORMOL_SPACE*10, self.cellHeight);
                if (self.isAdd) {
                    self.bankAddress.placeholder = @"如：xx市高新区解放路";
                    
                }
                
                [cell.contentView addSubview:self.bankAddress];
            }
                break;
        }
    } else {
        cell.textLabel.text = Localized(@"bank_account");
        self.bankAccount.frame = CGRectMake(9*NORMOL_SPACE, 0,mainWidth-10*NORMOL_SPACE, self.cellHeight);
        if (self.isAdd) {
            self.bankAccount.placeholder = Localized(@"your_bank_card");
        }
        [cell.contentView addSubview:self.bankAccount];
    }
    
    return cell;
}

- (NSInteger )numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger ) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 3;
    }else{
        return 1;
    }
}

- (CGFloat ) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==0) {
        return 0.1;
    }else{
        return footerHeight;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return self.cellHeight;
    }else{
        return NORMOL_SPACE*2;
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, mainHeight, self.cellHeight)];
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.frame = headerView.frame;
        titleLabel.text = Localized(@"bind_bank_card");
        [headerView addSubview:titleLabel];
        titleLabel.backgroundColor = BG_COLOR;
        return headerView;
    } else {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainHeight, NORMOL_SPACE*2)];
        headerView.backgroundColor = BG_COLOR;
        return headerView;
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section==1) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainHeight, self.cellHeight*2)];
        XSCustomButton *saveCardBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE*2, mainWidth-2*NORMOL_SPACE, NORMOL_SPACE*4) title:Localized(@"save") titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [saveCardBtn addTarget:self action:@selector(submitCardAction:) forControlEvents:UIControlEventTouchUpInside];
        [saveCardBtn setBorderWith:0 borderColor:0 cornerRadius:5];
        [footerView addSubview:saveCardBtn];
        footerView.backgroundColor = BG_COLOR;
        return footerView;
        
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0&&indexPath.row==0) {
        
        //弹出选择银行类别的警告框
        [HTTPManager getBankTypeSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                self.typeArray = [NSMutableArray arrayWithArray:[CardTypeDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
                self.typeView.typeArray = self.typeArray;
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
            
        } failure:^(NSError * _Nonnull error) {
           [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        [UIView animateWithDuration:0.3 animations:^{
            [self.accountBank resignFirstResponder];
            [self.bankAccount resignFirstResponder];
            [self.bankAddress resignFirstResponder];
        } completion:^(BOOL finished) {
            if (self.typeId) {
                self.typeView.selectedId = self.typeId;
            }
            [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView] ;
        }];
    }
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ( textField==self.accountBank) {
        return  [self.bankAddress becomeFirstResponder];
    } else if (textField==self.bankAddress) {
        return [self.bankAccount becomeFirstResponder];
    } else {
        return [self.bankAccount resignFirstResponder];
    }
}


@end
