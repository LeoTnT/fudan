//
//  WithdrawViewController.h
//  App3.0
//
//  Created by mac on 2017/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"
@interface LoanWithdrawViewController : XSBaseTableViewController
@property(nonatomic,strong) BusinessBankCardDataParser *parser;//银行卡信息
-(void)getInfo;
@end
