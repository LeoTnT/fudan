//
//  BusinessBankCardViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessBankCardViewController.h"
#import "BusinessAddBankCardViewController.h"
#import "BusinessBankTableViewCell.h"
#import "LoanWithdrawViewController.h"
#import "BusinessModel.h"
@interface BusinessBankCardViewController ()
@property(nonatomic,strong)NSMutableArray *bankCardArray;//银行卡数组
@property(nonatomic,strong)UITableView *tableView;//银行卡表格
@end

@implementation BusinessBankCardViewController

#pragma mark - lazy loadding
- (NSMutableArray *)bankCardArray {
    if (!_bankCardArray) {
        _bankCardArray = [NSMutableArray array];
    }
    return _bankCardArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    self.navigationController.navigationBarHidden =NO;
    //获取银行卡信息
    [self getBankCardInfo];

}
#pragma mark - private
-(void)getBankCardInfo{
    [XSTool showToastWithView:self.view Text:@"正在获取数据"];
    @weakify(self);
    [HTTPManager getBindBankCardInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.bankCardArray = [NSMutableArray arrayWithArray:[BusinessBankCardDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"]]];
            if (self.bankCardArray.count==0) {
                self.tableView.hidden=YES;
                [XSTool showToastWithView:self.view Text:@"暂时没有绑定银行卡信息"];
            }else{
                self.tableView.hidden=NO;
                
                [self.tableView reloadData];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

-(void)setSubViews{
    self.view.backgroundColor=BG_COLOR;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"添加") action:^{
        @strongify(self);
        BusinessAddBankCardViewController *add=[[BusinessAddBankCardViewController alloc] init];
        add.isAdd=YES;
        [self.navigationController pushViewController:add animated:YES];
    }];
    self.navigationItem.title=Localized(@"选择银行卡");
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    
}

#pragma mark -
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bankCardArray.count;
}
#pragma mark-设置行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BusinessBankTableViewCell *cell;
    static NSString *bankCardCellId=@"bankCardCellId";
    cell=[tableView dequeueReusableCellWithIdentifier:bankCardCellId];
    if (!cell) {
        cell=[[BusinessBankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:bankCardCellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.isShow=NO;
    }
    if (self.bankCardArray.count>indexPath.row) {
        cell.parser=[self.bankCardArray objectAtIndex:indexPath.row];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BusinessBankCardDataParser *parser=[self.bankCardArray objectAtIndex:indexPath.row];
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[LoanWithdrawViewController class]]) {
            ((LoanWithdrawViewController *)vc).parser=parser;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
