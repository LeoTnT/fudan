//
//  WithdrawViewController.m
//  App3.0
//
//  Created by mac on 2017/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LoanWithdrawViewController.h"
#import "UserModel.h"
#import "BindBankCardViewController.h"
#import "VertifyViewController.h"
#import "BusinessBankCardViewController.h"
#import "MJRefresh.h"
#import "BusinessModel.h"
#import "LoanWithDrawRecordViewController.h"
#import "RSAEncryptor.h"
#import "PersonViewController.h"
#import "ModifyOldPayViewController.h"
#import "LoginModel.h"
#import "BankCardDefaultView.h"
#import "ContainFieldTableViewCell.h"
#import "GrayBackView.h"

@interface LoanWithdrawViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,BankCardDefaultViewDelegate>
@property(nonatomic,assign)NSUInteger apptove;//实名认证类型
@property(nonatomic,strong)UILabel *mostMoneyLabel;//提示最多提现金额
//@property(nonatomic,strong)UIButton *allWithDrawBtn;//全部提现按钮
@property(nonatomic,strong)UITextField *moneyField;//提现金额

//@property(nonatomic,copy)NSString *walletTypeStr;//提现钱包
@property(nonatomic,strong)BusinessWithDrawRule *withDrawRule;//提现规则
//@property(nonatomic,strong)WithDrawWalletType *walletType;//当前钱包类型
@property(nonatomic,assign)float mostWithDrawMoney;//最大提现金额
@property(nonatomic,assign)float leastWithDrawMoney;//最小提现金额
//@property(nonatomic,strong)NSArray *bankCardArray;//银行卡数组
@property (nonatomic,strong) XSCustomButton *commitBtn;
@property (nonatomic, copy) NSString *payPassWord;
@property (assign, nonatomic) BOOL isTimer;
@property(nonatomic,strong)BankCardDefaultView *defaultView;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descString;
@property(nonatomic,strong)UITextField *walletTypeField;
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)NSArray *placeHoldArray;
@property(nonatomic,strong)UITextField *realMoneyField;
@property(nonatomic,strong)UITextField *bankField;
@property (nonatomic, strong) UILabel *tintLabel;
@property (nonatomic, strong) UILabel *hintLabel;
@end

@implementation LoanWithdrawViewController

#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title=@"添加提现";
    [self setNavBar];
    self.titleArray=@[@"提现金额",Localized(@"sjdz"),@"转到银行卡"];
    self.placeHoldArray=@[@"请输入提现金额",@"实际到账=提现金额-提现管理费",@"请选择银行卡"];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     [self getInfo];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.autoHideKeyboard = YES;
   
    //注册通知
    if (self.parser) {
        [self setBankCard];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Lazy Loading

//-(UIButton *)allWithDrawBtn {
//    if (!_allWithDrawBtn) {
//        _allWithDrawBtn = [[UIButton alloc] init];
//        [_allWithDrawBtn setTitle:@"全部提现" forState:UIControlStateNormal];
//        [_allWithDrawBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        _allWithDrawBtn.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
//        [_allWithDrawBtn addTarget:self action:@selector(withdrawAllMoney) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _allWithDrawBtn;
//}

-(BankCardDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[BankCardDefaultView alloc] init];
        _defaultView.delegate = self;
    }
    return _defaultView;
}

-(XSCustomButton *)commitBtn{
    if (!_commitBtn) {
        //提交按钮
        _commitBtn= [[XSCustomButton alloc] initWithFrame:CGRectMake(10, 350+navBarHeight+StatusBarHeight, mainWidth-20, 50) title:Localized(@"bug_submit_do") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [_commitBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
        [_commitBtn addTarget:self action:@selector(commitWithDraw) forControlEvents:UIControlEventTouchUpInside];
        
        _commitBtn.hidden=YES;
    }
    return _commitBtn;
}

#pragma mark-Private
-(void)getWithDrawWallet{
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager userTakecashAddWithWalletType:@"s_money" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessWithDrawRule *type = [BusinessWithDrawRule mj_objectWithKeyValues:dic[@"data"]];
             self.withDrawRule=type;
             [self setMostMoneyLabel];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
         [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

-(void)setBankCard{
    UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *cardName=self.parser.bankname;
    NSString *cardNum=self.parser.bankcard;
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@(尾号%@)",cardName,[cardNum substringFromIndex:cardNum.length-4]];
    cell.detailTextLabel.textColor=[UIColor blackColor];
}

-(void)setMostMoneyLabel{
    //如果maxnum为空  则没有限制
    if ([self.withDrawRule.set.maxnum integerValue]<=0) {
        self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue];
    }else{
        //计算最多可提现金额
        if ([self.withDrawRule.set.maxnum floatValue]>=[self.withDrawRule.wallet floatValue]) {
            self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue];
        }else{
            self.mostWithDrawMoney=[self.withDrawRule.set.maxnum floatValue];
        }
    }
    if([self.withDrawRule.set.minnum integerValue]<=0){
        self.leastWithDrawMoney=0;
    }else{
        self.leastWithDrawMoney=[self.withDrawRule.set.minnum floatValue];
    }
    long int length1=[@"  可提现金额：" length];
    long int length2=[NSString stringWithFormat:@"%.2f元",self.mostWithDrawMoney].length;
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  可提现金额：%.2f元",self.mostWithDrawMoney]];
    [attri addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(length1, length2)];
    self.mostMoneyLabel.attributedText=attri;
    
    self.hintLabel.text = [NSString stringWithFormat:@"%@",self.withDrawRule.set.hint_num];
    
    //说明
    
   NSString *tempString = [NSString stringWithFormat:@"*%@%@(最低%.2f元，最高%.2f元)",self.withDrawRule.set.tax,@"%",[self.withDrawRule.set.taxlow floatValue],[self.withDrawRule.set.taxtop floatValue]];
    self.tintLabel.text=[NSString stringWithFormat:@"说明:提现管理费=提现金额%@",tempString];
}
-(void)setDefaultBankCard{
//    for (BankCardDataParser *parser in self.bankCardArray) {
//        //设置默认银行卡
//        if ([self.parser.is_default integerValue]==1) {
//            self.parser=parser;
            NSUInteger section = 1; //指定section
            //刷整个section
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:section];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            
            UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@(尾号%@)",self.parser.bankname,[self.parser.bankcard substringFromIndex:self.parser.bankcard.length-4]];
            cell.detailTextLabel.textColor=[UIColor blackColor];
//            break;
//        }
//    }
}

-(void)toVertify{
    VertifyViewController *vertifyVC = [[VertifyViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)setNavBar{
    self.view.backgroundColor=BG_COLOR;
//    self.navigationController.navigationBarHidden =NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"提现记录" action:^{
        @strongify(self);
        LoanWithDrawRecordViewController *vc=[[LoanWithDrawRecordViewController alloc] init];
        vc.wallet_type=@"s_money";
        [self.navigationController pushViewController:vc animated:YES];
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    [self.view addSubview:self.commitBtn];
}

-(void)calculatePoundage{
    //判断金额
    if ([self.moneyField.text floatValue]>self.mostWithDrawMoney) {
        [XSTool showToastWithView:self.view Text: @"余额不足"];
        self.realMoneyField.placeholder = @"提现金额=提现管理费";
        self.realMoneyField.text = @"";
        return ;
    } else if ([self.moneyField.text floatValue]<self.leastWithDrawMoney) {
        [XSTool showToastWithView:self.view Text:@"低于最低提现金额"];
        self.realMoneyField.placeholder = @"提现金额=提现管理费";
        self.realMoneyField.text = @"";
        return;
    } else {
        //计算手续费
        float poundAge=[self.moneyField.text floatValue]*[self.withDrawRule.set.tax floatValue]/100;//手续费
        if (poundAge<=[self.withDrawRule.set.taxlow floatValue]) {//手续费与最低手续费比较
            poundAge=[self.withDrawRule.set.taxlow floatValue];
        }
        if (poundAge>[self.withDrawRule.set.taxtop floatValue]) {//手续费与最高手续费比较  为0没有限制
            if([self.withDrawRule.set.taxtop integerValue]!=0){
                poundAge=[self.withDrawRule.set.taxtop floatValue];
            }
        }
        self.realMoneyField.text=[NSString stringWithFormat:@"%.2f",[self.moneyField.text floatValue]-poundAge];
    }
    
}
-(void)commitWithDraw{
     [self.view endEditing:YES];
        if (self.moneyField.text.length&&self.parser.bankcard.length&&[self.realMoneyField.text floatValue]>=1) {
            //未设置支付密码
            if ([self.payPassWord isEqualToString:@""]) {
                //弹出警告框
                UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:nil message:@"您还没有设置支付密码，是否要设置支付密码？" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //跳转到设置支付密码界面
                    ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
                    [self.navigationController pushViewController:oldPayViewController animated:YES];
                }];
                [alertControl addAction:cancelAction];
                [alertControl addAction:okAction];
                [self presentViewController:alertControl animated:YES completion:nil];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:Localized(@"input_pay_pwd") preferredStyle:UIAlertControllerStyleAlert];
                //添加按钮
                __weak typeof(alert) weakAlert = alert;
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    NSString *passWord = [NSString stringWithFormat:@"%@",[weakAlert.textFields.lastObject text]];
                    if (passWord.length==0||[passWord isEqualToString:@""]) {
                        [XSTool showToastWithView:self.view Text: @"请填写支付密码！"];
                    } else {
                        NSString *encryptPassWord = [RSAEncryptor encryptString:[weakAlert.textFields.lastObject text]];
                        [XSTool showProgressHUDWithView:self.view];
                      [HTTPManager userTakecashInsertWithWalletType:@"s_money" pass2:encryptPassWord number:self.moneyField.text bankcardid:self.parser.ID success:^(NSDictionary *dic, resultObject *state) {
                          [XSTool hideProgressHUDWithView:self.view];
                          if (state.status) {
                              [XSTool showToastWithView:self.view Text:@"提现成功"];
                              //进入记录页面
                              LoanWithDrawRecordViewController *record=[[LoanWithDrawRecordViewController alloc] init];
                              record.wallet_type=@"s_money";
                              [self.navigationController pushViewController:record animated:YES];

                          } else {
                              [XSTool showToastWithView:self.view Text:state.info];
                          }
                          
                      } fail:^(NSError *error) {
                          [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:NetFailure];
                      }];
                    
                    }
                }]];
    
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    
                }]];
    
                // 添加文本框
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.secureTextEntry = YES;
                    textField.delegate = self;
//                    textField.keyboardType=UIKeyboardTypeNumberPad;
                    textField.textColor = [UIColor blackColor];
                }];
                // 弹出对话框
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            [XSTool showToastWithView:self.view Text: @"请完善提现信息"];
        }
}

-(void)hideKeyBoard{
    [self.view endEditing:YES];
}
-(void)showBankCards{
    [self.view endEditing:YES];
    BusinessBankCardViewController *bank=[[BusinessBankCardViewController alloc] init];
    [self.navigationController pushViewController:bank animated:YES];
}

//-(void)withdrawAllMoney{
//    [XSTool showToastWithView:self.view Text:@"全部，刷新表格"];
//}

#pragma mark-TableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    static NSString *cellId=@"cellId";
    //    static NSString *typeCellId=@"typeCellId";
    if (indexPath.section==0) {
        cell=[[ContainFieldTableViewCell alloc] initWithId:cellId TextLabelText:self.titleArray[indexPath.row] fontSize:17 WithFieldFrame:CGRectMake(130, 0, mainWidth-10-130, 50) Placehold:self.placeHoldArray[indexPath.row] textColor:[UIColor blackColor]  fontSize:13 KeyBoardType:UIKeyboardTypeDecimalPad textAlignMent:NSTextAlignmentRight];
        if (indexPath.row==0) {
            self.moneyField=((ContainFieldTableViewCell *)cell).inputField;
            [self.moneyField addTarget:self action:@selector(calculatePoundage) forControlEvents:UIControlEventEditingChanged];
        }
        if (indexPath.row==1) {
            self.realMoneyField=((ContainFieldTableViewCell *)cell).inputField;
            self.realMoneyField.userInteractionEnabled=NO;
        }
    }else{
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        }
        
        cell.textLabel.text=Localized(@"银行类型");
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showBankCards)]];
        cell.detailTextLabel.font = [UIFont qsh_systemFontOfSize:15];
        
        cell.detailTextLabel.text= self.parser?self.parser.bankname:@"请选择银行卡";
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        if (section==0) {
            return 2;
        }
        return 1;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return 2;


}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        if (section==0) {
            return 80;
        }
        return 50;
    }
    return 0.1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        if (section==0) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 100)];
            //添加可提现金额提醒
            self.mostMoneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 10, mainWidth-20, 30)];
            self.mostMoneyLabel.text=@"  可提现金额：0.00元";
            self.mostMoneyLabel.font=[UIFont systemFontOfSize:15];
            [view addSubview:_mostMoneyLabel];
            //添加可提现金额提醒
            self.hintLabel=[[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.mostMoneyLabel.frame), mainWidth-NORMOL_SPACE*2,40)];
            self.hintLabel.textColor = [UIColor grayColor];
            self.hintLabel.font=[UIFont qsh_systemFontOfSize:15];
            self.hintLabel.adjustsFontSizeToFitWidth = YES;
            self.hintLabel.numberOfLines = 0;
            [view addSubview:self.hintLabel];
            return view;
        }
        if (section==1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 50)];
            //添加可提现金额提醒
            self.tintLabel=[[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, mainWidth-NORMOL_SPACE*2, CGRectGetHeight(view.frame))];
            self.tintLabel.textColor = [UIColor grayColor];
            self.tintLabel.font=[UIFont qsh_systemFontOfSize:15];
            self.tintLabel.adjustsFontSizeToFitWidth = YES;
            self.tintLabel.numberOfLines = 0;
            [view addSubview:self.tintLabel];
            return view;
            
        }
        
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {
        return 50;
    }
    return 40;
}


#pragma  mark - private
-(void)allWithDraw{
    self.moneyField.text=[NSString stringWithFormat:@"%.2f",self.mostWithDrawMoney];
    //计算手续费
    [self calculatePoundage];
}

-(void)setUnIdentificationSubviews{
    self.defaultView.titleLabel.text = self.titleString;
    self.defaultView.descLabel.text = self.descString;
    [self.view addSubview:self.defaultView];
    [self.defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

-(void)getInfo{
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView: self.view];
        if(state.status){
            UserParser *parser = [UserParser mj_objectWithKeyValues:dic];
            self.payPassWord=parser.data.pay_password;
            self.apptove = [parser.data.approve_user intValue];
            if (self.apptove==-1) {
                //未提交实名认证
                self.titleString = Localized(@"user_approve_suggest_no");
                self.descString = @"为了保证您的权益，提现前请进行实名认证！";
                [self setUnIdentificationSubviews];
            }else if (self.apptove==0){
                //已提交审核中
                self.titleString = Localized(@"user_approve_process_ing");
                [self setUnIdentificationSubviews];
                self.defaultView.descLabel.hidden = YES;
                self.defaultView.vertifyButton.backgroundColor = LINE_COLOR;
                self.defaultView.vertifyButton.enabled = NO;
            }else if (self.apptove==1){
                self.tableView.hidden=NO;
                self.commitBtn.hidden = NO;
                //获取提现信息
                [self getWithDrawWallet];
            }else{
                //审核失败
                self.titleString = Localized(@"审核失败，请重新审核！");
                self.descString = Localized(@"user_approve_suggest_no_d");
                [self setUnIdentificationSubviews];
            }
        }else{
            [XSTool showToastWithView:self.view Text: state.info];
        }
    } failure:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView: self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
#pragma mark-TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [self.view endEditing:YES];
}
@end
