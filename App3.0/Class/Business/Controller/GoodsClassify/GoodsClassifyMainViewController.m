//
//  GoodsClassifyMainViewController.m
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsClassifyMainViewController.h"
#import "ManageLogisticTableViewCell.h"
#import "BusinessClassifyModel.h"
#import "BusinessDefaultView.h"
#import "SecondClassifyViewController.h"
#import "GoodsManagementViewController.h"
#import "AdvancedPublishGoodsViewController.h"
#import "BusinessGoodsViewController.h"

@interface GoodsClassifyMainViewController ()<ManageLogisticDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UIButton *addClassifyButton;
@property (nonatomic, strong) UIButton *batchManagementButton;
@property (nonatomic, strong) BusinessDefaultView *defaultView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) NSMutableArray *classifyArray;
@property (nonatomic, strong) NSMutableArray *deleteArray;
@property (nonatomic, assign) CGFloat  bottomHeight;
@property (nonatomic, strong) SecondClassifyViewController  *nextController;
@property (nonatomic, assign) NSUInteger page;

@end

@implementation GoodsClassifyMainViewController
#pragma mark - lazy loadding
-(NSMutableArray *)classifyArray {
    if (!_classifyArray) {
        _classifyArray = [NSMutableArray array];
    }
    return _classifyArray;
}

-(NSMutableArray *)deleteArray  {
    if (!_deleteArray) {
        _deleteArray = [NSMutableArray array];
    }
    return _deleteArray;
}

-(UIButton *)addClassifyButton {
    if (!_addClassifyButton) {
        _addClassifyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, MAIN_VC_HEIGHT-self.bottomHeight, mainWidth/2-0.5, self.bottomHeight)];
        _addClassifyButton.backgroundColor = [UIColor whiteColor];
        _addClassifyButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [_addClassifyButton setTitle:@"新增" forState:UIControlStateNormal];
        [_addClassifyButton setTitleColor:mainColor forState:UIControlStateNormal];
        [_addClassifyButton setImage:[UIImage imageNamed:@"business_add"] forState:UIControlStateNormal];
        [_addClassifyButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_addClassifyButton addTarget:self action:@selector(addGoodsClassifyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addClassifyButton;
}

- (UIButton *)batchManagementButton {
    if (!_batchManagementButton) {
        _batchManagementButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2+0.5, MAIN_VC_HEIGHT-self.bottomHeight, mainWidth/2-0.5, self.bottomHeight)];
        _batchManagementButton.backgroundColor = [UIColor whiteColor];
        _batchManagementButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [_batchManagementButton setTitle:@"删除" forState:UIControlStateNormal];
        [_batchManagementButton setTitleColor:mainColor forState:UIControlStateNormal];
        [_batchManagementButton setImage:[UIImage imageNamed:@"business_classify_delete"]forState:UIControlStateNormal];
        [_batchManagementButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_batchManagementButton addTarget:self action:@selector(batchManagementAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _batchManagementButton;
}

-(BusinessDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[BusinessDefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT)];
        _defaultView.backgroundColor = BG_COLOR;
        _defaultView.tintLabel.text = @"暂时没有分类信息";
        _defaultView.applyBtn.hidden = YES;
    }
    return _defaultView;
}

-(UIButton *)deleteButton {
    if (!_deleteButton) {
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(0, mainHeight-self.bottomHeight, mainWidth, self.bottomHeight)];
        _deleteButton.backgroundColor = mainColor;
        [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _deleteButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [_deleteButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}
#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomHeight = 44;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"自定义分类";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    //默认界面
    //    [self.view addSubview:self.defaultView];
    self.tableViewStyle = UITableViewStylePlain;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, self.bottomHeight, 0));
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    //上下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self uploadInformation];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self pullInformation];
        
    }];
    [self.view addSubview:self.addClassifyButton];
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2-0.5, CGRectGetMinY(self.addClassifyButton.frame)+10, 1, self.bottomHeight-2*10)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self.view addSubview:line];
    [self.view addSubview:self.batchManagementButton];
    
    UILabel *topLine = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.addClassifyButton.frame), mainWidth, 0.5)];
    topLine.backgroundColor = LINE_COLOR_NORMAL;
    [self.view addSubview:topLine];
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)uploadInformation {
    self.page = 1;
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager supplyCategoryListsWithParam:@{@"parent_id":@"0",@"tree":@"no",@"page":[NSString stringWithFormat:@"%lu",self.page],@"limit":@"12"} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            BusinessClassifyParser *parser = [BusinessClassifyParser mj_objectWithKeyValues:dic[@"data"]];
            [self.classifyArray setArray:parser.data];
            if (parser.data.count) {
                self.page++;
            }
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
           @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)pullInformation {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager supplyCategoryListsWithParam:@{@"parent_id":@"0",@"tree":@"no",@"page":[NSString stringWithFormat:@"%lu",self.page],@"limit":@"12"} success:^(NSDictionary *dic, resultObject *state) {
           @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            BusinessClassifyParser *parser = [BusinessClassifyParser mj_objectWithKeyValues:dic[@"data"]];
            if (parser.data.count) {
                self.page++;
            }
            [self.classifyArray addObjectsFromArray:parser.data];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
           @strongify(self);
         [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)addGoodsClassifyAction:(UIButton *) sender {
    //编辑分类
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分类名称" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    //添加按钮
    __weak typeof(alertController) weakAlert = alertController;
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //获取输入的昵称
        NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
        if ([weakAlert.textFields.lastObject text].length==0) {
            [XSTool showToastWithView:self.view Text: @"分类名不能为空"];
        } else {
            if ([self stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                [XSTool showToastWithView:self.view Text:@"你好，分类名不支持表情"];
            } else {
                [HTTPManager supplyCategoryInsertWithParam:@{@"parent_id":@"0",@"category_name":[weakAlert.textFields.lastObject text]} success:^(NSDictionary *dic, resultObject *state) {
                    if (state.status) {
                        [self uploadInformation];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
                
                
            }
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"点击了取消按钮");
    }]];
    
    // 添加文本框
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.textColor = [UIColor blackColor];
        textField.delegate = self;
        //        textField.tag = 1500;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.placeholder = @"请输入备注名称";
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    // 弹出对话框
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)batchManagementAction:(UIButton *) sender {
    if (self.classifyArray.count) {
        self.tableView.editing = YES;
        [self.deleteArray removeAllObjects];
        [self.view addSubview:self.deleteButton];
    } else {
        [XSTool showToastWithView:self.view Text:@"请先添加分类！"];
    }
}

- (void)doneAction:(UIButton *) sender {
    
    if (self.deleteArray.count==0) {
        //        [XSTool showToastWithView:self.view Text:@"请选择分类！"];
        self.tableView.editing = NO;
        if ([self.view.subviews containsObject:self.deleteButton]) {
            [self.deleteButton removeFromSuperview];
        }
        return;
    }
    
    if (self.deleteArray.count) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定删除分类？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        //添加按钮
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self deleteClassifyWithClassifyId:[self.deleteArray componentsJoinedByString:@","]];
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        
        // 弹出对话框
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.markedTextRange == nil) {
        if (textField.text.length >10) {
            [XSTool showToastWithView:self.view Text:@"分类长度只能为1~10"];
            textField.text = [textField.text substringToIndex:10];
        }
    }
}

- (BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}

- (void) toNextPage:(UIButton *)  sender {
    self.nextController = [[SecondClassifyViewController alloc] init];
    BusinessClassifyDetailParser *parser = self.classifyArray[sender.tag-200];
    self.nextController.parentId = parser.category_id;
    self.nextController.navigationItem.title = parser.category_name;
    if (self.firstClassifyType==FirstClassifyTypeManageGoods) {
        self.nextController.secondClassifyType = SecondClassifyTypeManageGoods;
    } else if (self.firstClassifyType==FirstClassifyTypePublishGoods) {
        self.nextController.secondClassifyType = SecondClassifyTypePublishGoods;
    } else {
        self.nextController.secondClassifyType = SecondClassifyTypeNormal;
    }
    [self.navigationController pushViewController:self.nextController animated:YES];
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"normal";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.backgroundColor = [UIColor whiteColor];
        
    }
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 7, 30, 30)];
    [button  setImage:[UIImage imageNamed:@"nav_right"] forState:UIControlStateNormal];
    button.tag = indexPath.section+200;
    cell.accessoryView = button;
    [button addTarget:self action:@selector(toNextPage:) forControlEvents:UIControlEventTouchUpInside];
    if (self.classifyArray.count) {
        BusinessClassifyDetailParser *parser = self.classifyArray[indexPath.section];
        cell.textLabel.text = parser.category_name;
        cell.textLabel.textColor = [UIColor hexFloatColor:@"333333"];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.classifyArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section  {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section  {
    
    return 12;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BusinessClassifyDetailParser *parser = self.classifyArray[indexPath.section];
    if (self.tableView.editing) {
        [self.deleteArray addObject:parser.category_id];
    } else {
        if (self.firstClassifyType==FirstClassifyTypeManageGoods) {
            if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[GoodsManagementViewController class]]) {
                GoodsManagementViewController *controller = (GoodsManagementViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                for (BusinessGoodsViewController *childController in controller.childViewControllers) {
                    [childController selectedClassifyWithCategoryId:parser.category_id];
                   
                }
                [self.navigationController popViewControllerAnimated:YES];
            }
        } else if (self.firstClassifyType==FirstClassifyTypePublishGoods)  {
            if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[AdvancedPublishGoodsViewController class]]) {
                AdvancedPublishGoodsViewController *controller = (AdvancedPublishGoodsViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                [controller selectedCategoryWithCategoryParser:parser];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } else {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self editClassifyWithClassifyId:parser.category_id];
        }
        
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath  {
    BusinessClassifyDetailParser *parser = self.classifyArray[indexPath.section];
    if (self.tableView.editing) {
        
        [self.deleteArray removeObject: parser.category_id];
    }
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

// 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 12;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}
#pragma mark - ManageLogisticDelegate
-(void)editClassifyWithClassifyId:(NSString *)classifyId{
    
    //编辑分类
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置分类名" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    //添加按钮
    __weak typeof(alertController) weakAlert = alertController;
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //获取输入的昵称
        NSString *name = [weakAlert.textFields.lastObject text];
        if (name.length==0) {
            [XSTool showToastWithView:self.view Text: @"分类名不能为空"];
        } else {
            if ([self stringContainsEmoji:name]) {
                [XSTool showToastWithView:self.view Text:@"你好，分类名不支持表情"];
            } else {
                
                [XSTool showProgressHUDWithView:self.view];
                [HTTPManager supplyCategoryUpdateWithParam:@{@"id":classifyId,@"category_name":name} success:^(NSDictionary *dic, resultObject *state) {
                    
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [self uploadInformation];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
                
                
            }
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    // 添加文本框
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.textColor = [UIColor blackColor];
        textField.delegate = self;
        //        textField.tag = 1500;
        for (BusinessClassifyDetailParser *detailParser in self.classifyArray) {
            if ([detailParser.category_id isEqualToString:classifyId]) {
                textField.placeholder = detailParser.category_name;
                break;
            }
        }
        
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    // 弹出对话框
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)deleteClassifyWithClassifyId:(NSString *)classifyId {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager supplyCategoryDeleteWithParam:@{@"id":classifyId} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.tableView.editing = NO;
            if ([self.view.subviews containsObject:self.deleteButton]) {
                [self.deleteButton removeFromSuperview];
            }
            
            for (int i=0; i<self.deleteArray.count; i++) {
                NSString *tempString = self.deleteArray[i];
                for (int j=0; j<self.classifyArray.count; j++) {
                    BusinessClassifyDetailParser *detailParser = self.classifyArray[j];
                    if ([tempString isEqualToString:detailParser.category_id]) {
                        [self.classifyArray removeObject:detailParser];
                    }
                }
            }
            [self.deleteArray removeAllObjects];
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

@end
