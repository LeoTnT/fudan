//
//  SecondClassifyViewController.h
//  App3.0
//
//  Created by nilin on 2018/1/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,SecondClassifyType) {
    SecondClassifyTypeNormal,
    SecondClassifyTypeManageGoods,
    SecondClassifyTypePublishGoods,
};
@interface SecondClassifyViewController : XSBaseTableViewController

/**父类*/
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, assign) SecondClassifyType secondClassifyType;
@end
