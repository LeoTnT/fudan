//
//  ThirdClassifyViewController.h
//  App3.0
//
//  Created by nilin on 2018/1/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,ThirdClassifyType) {
    ThirdClassifyTypeNormal,
    ThirdClassifyTypeManageGoods,
    ThirdClassifyTypePublishGoods,
};
@interface ThirdClassifyViewController : XSBaseTableViewController
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, assign) ThirdClassifyType thirdClassifyType;
@end
