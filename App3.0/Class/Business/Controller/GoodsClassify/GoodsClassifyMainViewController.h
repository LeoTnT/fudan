//
//  GoodsClassifyMainViewController.h
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,FirstClassifyType) {
    FirstClassifyTypeNormal,
    FirstClassifyTypeManageGoods,
    FirstClassifyTypePublishGoods,
};

@interface GoodsClassifyMainViewController : XSBaseTableViewController
@property (nonatomic, assign) FirstClassifyType firstClassifyType;
@end
