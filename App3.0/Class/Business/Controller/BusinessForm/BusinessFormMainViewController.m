//
//  BusinessFormMainViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessFormMainViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "BusinessFormViewController.h"


@interface BusinessFormMainViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC1;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC2;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC3;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC4;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC5;
@property (nonatomic, strong) BusinessFormViewController *orderFormVC6;
@property (nonatomic, assign) CGFloat segmentHeight;
@end

@implementation BusinessFormMainViewController

#pragma mark - lazy loadding
-(BusinessFormViewController *)orderFormVC1 {
    if (!_orderFormVC1) {
        _orderFormVC1= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeAll];
        _orderFormVC1.view.frame = CGRectMake(0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC1;
    
}
-(BusinessFormViewController *)orderFormVC2 {
    if (!_orderFormVC2) {
        _orderFormVC2= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeWaitPay];
        _orderFormVC2.view.frame = CGRectMake(mainWidth, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC2;
    
}
-(BusinessFormViewController *)orderFormVC3 {
    if (!_orderFormVC3) {
        _orderFormVC3= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeWaitSend];
        _orderFormVC3.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC3;
    
}
-(BusinessFormViewController *)orderFormVC4 {
    if (!_orderFormVC4) {
        _orderFormVC4= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeWaitReceive];
        _orderFormVC4.view.frame = CGRectMake(mainWidth*3, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC4;
    
}
-(BusinessFormViewController *)orderFormVC5 {
    if (!_orderFormVC5) {
        _orderFormVC5= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeWaitEvaluate];
        _orderFormVC5.view.frame = CGRectMake(mainWidth*4, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC5;
    
}
-(BusinessFormViewController *)orderFormVC6 {
    if (!_orderFormVC6) {
        _orderFormVC6= [[BusinessFormViewController alloc] initWithOrderType:BusinessOrderTypeRefundOrAfterSale];
        _orderFormVC6.view.frame = CGRectMake(mainWidth*5, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC6;
    
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        self.segmentHeight = 40;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.segmentHeight, mainWidth, mainHeight-self.segmentHeight)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*6, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        self.segmentHeight = 40;
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.segmentHeight)];
        _segmentControl.sectionTitles = self.titleArray?self.titleArray:@[Localized(@"alls"),@"待付款",@"待发货",@"待收货",Localized(@"have_finish"),@"已关闭"];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        //        _segmentControl.borderType = HMSegmentedControlBorderTypeRight;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:16],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.segmentHeight = 40;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"订单管理";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    [self getOrderCountInformation];
    [self setSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
-(instancetype)initWithFormType:(BusinessFormType)type {
    self = [super init];
    if (self) {
        self.formType = type;
    }
    return self;
}

-(void)setFormType:(BusinessFormType)formType {
    _formType = formType;
    [self getOrderCountInformation];
    self.segmentControl.selectedSegmentIndex = _formType;
    [self segmentedChangedValue:self.segmentControl];
    for (UIViewController *controller in self.childViewControllers) {
        if ([controller isKindOfClass:[BusinessFormViewController class]]) {
//            if (_formType==BusinessFormTypeWaitReceive) {
                [((BusinessFormViewController *)controller) uploadFormInformation];
//            }
            
        }
    }

}

- (void)getOrderCountInformation {
//    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager supplySorderGetStatusCountSuccess:^(NSDictionary *dic, resultObject *state) {
//        [XSTool hideProgressHUDWithView:self.view];
//        if (state.status) {
        
//            NSArray *dataArray = dic[@"data"];
//            NSUInteger number = [[[dataArray[5] allValues] lastObject] integerValue]+[[[dataArray[6] allValues] lastObject] integerValue];
//            [self.segmentControl setSectionTitles:@[  [NSString stringWithFormat:@"全部(%@)",[[((NSDictionary *)[dataArray lastObject]) allValues] lastObject]], [NSString stringWithFormat:@"待付款(%@)",[[((NSDictionary *)dataArray[0]) allValues] lastObject]], [NSString stringWithFormat:@"待发货(%@)",[[((NSDictionary *)dataArray[1]) allValues] lastObject]], [NSString stringWithFormat:@"已发货(%@)",[[((NSDictionary *)dataArray[2]) allValues] lastObject]], [NSString stringWithFormat:@"待评价(%@)",[[((NSDictionary *)dataArray[3]) allValues] lastObject]], [NSString stringWithFormat:@"退款/售后(%lu)",number]]];
            
//            [self.segmentControl setSectionTitles:@[@"全部", [NSString stringWithFormat:@"待付款(%@)",[dataArray firstObject]], [NSString stringWithFormat:@"待发货(%@)",dataArray[1]], [NSString stringWithFormat:@"已发货(%@)",dataArray[2]], [NSString stringWithFormat:@"待评价(%@)",dataArray[3]]]];
//             self.segmentControl.selectedSegmentIndex = self.formType;
//            [self segmentedChangedValue:self.segmentControl];
//        }
        
//    } fail:^(NSError *error) {
//        [XSTool hideProgressHUDWithView:self.view];
//    }];
    
}
-(NSString*)stringWithDict:(NSDictionary*)dict{
    
    NSArray*keys = [dict allKeys];
    
    NSArray*sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1,id obj2) {
        return[obj1 compare:obj2 options:NSNumericSearch];//正序
    }];
    
    NSString*str =@"";
    for(NSString*categoryId in sortedArray) {
        
        id value = [dict objectForKey:categoryId];
        
        if([value isKindOfClass:[NSDictionary class]]) {
            
            value = [self stringWithDict:value];
            
        }
        
        if([str length] !=0) {
            
            str = [str stringByAppendingString:@","];
            
        }
        
        str = [str stringByAppendingFormat:@"%@:%@",categoryId,value];
        
    }

    return str;
}

-(void) setSubViews {
    
    //根据选项变化位置和内容
    [self addChildViewController:self.orderFormVC1];
    [self.scrollView addSubview:self.orderFormVC1.view];
    
    [self addChildViewController:self.orderFormVC2];
    [self.scrollView addSubview:self.orderFormVC2.view];
    
    [self addChildViewController:self.orderFormVC3];
    [self.scrollView addSubview:self.orderFormVC3.view];
    
    [self addChildViewController:self.orderFormVC4];
    [self.scrollView addSubview:self.orderFormVC4.view];
    
    [self addChildViewController:self.orderFormVC5];
    [self.scrollView addSubview:self.orderFormVC5.view];
    
    [self addChildViewController:self.orderFormVC6];
    [self.scrollView addSubview:self.orderFormVC6.view];
    
}

- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
}

@end
