//
//  BusinessFormViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BusinessOrderType)
{
    BusinessOrderTypeAll = 0,
    BusinessOrderTypeWaitPay=1,
    BusinessOrderTypeWaitSend=2,
    BusinessOrderTypeWaitReceive=3,
    BusinessOrderTypeWaitEvaluate=4,
    BusinessOrderTypeRefundOrAfterSale=5
};

@interface BusinessFormViewController : XSBaseTableViewController
- (instancetype)initWithOrderType:(BusinessOrderType)type;
- (void)uploadFormInformation;
@end
