//
//  BusinessFormMainViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, BusinessFormType)
{
    BusinessFormTypeAll = 0,
    BusinessFormTypeWaitPay,
    BusinessFormTypeWaitSend,
    BusinessFormTypeWaitReceive,
    BusinessFormTypeWaitEvaluate,
    BusinessFormTypeRefundOrAfterSale
};
@interface BusinessFormMainViewController : XSBaseViewController

- (instancetype)initWithFormType:(BusinessFormType)type;
@property (nonatomic, assign) BusinessFormType formType;
@property (nonatomic, strong) NSArray *titleArray;
@end
