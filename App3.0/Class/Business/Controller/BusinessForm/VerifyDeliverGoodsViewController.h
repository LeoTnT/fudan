//
//  VerifyDeliverGoodsViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyDeliverGoodsViewController : XSBaseTableViewController

@property (nonatomic, strong) NSString *orderId;
//@property (nonatomic, strong) NSArray *logisticListParser;
@end
