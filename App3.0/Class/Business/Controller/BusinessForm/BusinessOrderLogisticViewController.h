//
//  BusinessOrderLogisticViewController.h
//  App3.0
//
//  Created by nilin on 2017/9/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessOrderLogisticViewController : XSBaseTableViewController
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *logoUrl;
@property (nonatomic, copy) NSString *count;

@end
