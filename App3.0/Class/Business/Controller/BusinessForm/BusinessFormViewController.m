//
//  BusinessFormViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessFormViewController.h"
#import "BusinessFormModel.h"
#import "BusinessFormTableViewCell.h"
#import "VerifyDeliverGoodsViewController.h"
#import "BusinessOrderDetailViewController.h"
#import "BusinessFormMainViewController.h"
#import "BusinessOrderLogisticViewController.h"

@interface BusinessFormViewController ()<UITableViewDelegate,UITableViewDataSource,BusinessFormGoodsViewDelegate,BusinessFormTableViewCellDelegate>
@property (nonatomic, assign) BusinessOrderType orderType;
@property (nonatomic, strong) NSMutableArray *typeOrderArray;//订单类型数组
@property (nonatomic, strong) DefaultView *defaultView;//默认界面
@property (nonatomic, assign) NSUInteger page;//刷新的页数
@property (nonatomic, assign) NSUInteger limit;
@property (nonatomic, copy) NSString *statusString;//状态

@end

@implementation BusinessFormViewController
#pragma mark - lazy loadding
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        
    }
    return _defaultView;
}

- (NSMutableArray *)typeOrderArray {
    if (!_typeOrderArray) {
        _typeOrderArray = [NSMutableArray array];
    }
    return _typeOrderArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.limit = 5;
    [self setOrderStatus];
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self uploadFormInformation];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self pullFormInformation];
        
    }];
    self.view.backgroundColor = BG_COLOR;
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    [self uploadFormInformation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (instancetype)initWithOrderType:(BusinessOrderType)type {
    self = [super init];
    if (self) {
        self.orderType = type;
    }
    return self;
}

- (void)setOrderStatus {
    switch (self.orderType) {
        case BusinessOrderTypeAll: {
            self.statusString = @" ";
            self.defaultView.titleLabel.text = @"当前没有订单！";
        }
            break;
        case BusinessOrderTypeWaitPay: {
            self.statusString = @"0";
            self.defaultView.titleLabel.text = @"当前没有待付款的订单！";
            
        }
            break;
        case BusinessOrderTypeWaitSend: {
            self.statusString = @"1";
            self.defaultView.titleLabel.text = @"当前没有待发货的订单！";
            
        }
            break;
        case BusinessOrderTypeWaitReceive: {
            self.statusString = @"2";
            self.defaultView.titleLabel.text = @"当前没有待收货的订单！";
        }
            break;
        case BusinessOrderTypeWaitEvaluate: {
            self.statusString = @"3";
            self.defaultView.titleLabel.text = @"当前没有已完成的订单！";
        }
            break;
        case BusinessOrderTypeRefundOrAfterSale: {
            self.statusString = @"4";
            self.defaultView.titleLabel.text = @"当前没有已关闭的订单！";
        }
            break;
    }
    
}

- (void)uploadFormInformation {
    [self.typeOrderArray removeAllObjects];
    self.page = 1;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplySorderListsWithSearchAllAtatus:self.statusString page:[NSString stringWithFormat:@"%lu",self.page] limit:[NSString stringWithFormat:@"%lu",self.limit] success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            NSLog(@"BusinessFormParser======%@",dic);
            BusinessFormParser *listParser = [BusinessFormParser mj_objectWithKeyValues:dic[@"data"]];
            if (listParser.list.data.count) {
                self.defaultView.hidden = YES;
                if (self.tableView.hidden) {
                    self.tableView.hidden = NO;
                    
                }
                [self.typeOrderArray addObjectsFromArray:listParser.list.data];
                [self.tableView reloadData];
                self.page++;
                
            } else {
                self.defaultView.hidden = NO;
                self.tableView.hidden = YES;
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)pullFormInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplySorderListsWithSearchAllAtatus:self.statusString page:[NSString stringWithFormat:@"%lu",self.page] limit:[NSString stringWithFormat:@"%lu",self.limit] success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            BusinessFormParser *listParser = [BusinessFormParser mj_objectWithKeyValues:dic[@"data"]];
            if (listParser.list.data.count) {
                [self.typeOrderArray addObjectsFromArray:listParser.list.data];
                [self.tableView reloadData];
                self.page++;
            } else {
                [XSTool showToastWithView:self.view Text:@"暂时没有更多数据"];
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *idString = @"BusinessFormTableViewCellWithButtons";
//        BusinessFormTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
//        if (cell==nil) {
            BusinessFormTableViewCell *cell = [[BusinessFormTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            
//        }
        cell.orderDelegate = self;
        cell.delegate = self;
        cell.dataParser = self.typeOrderArray[indexPath.row];
        NSLog(@"idString2====%@",idString);
        return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeOrderArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BusinessFormTableViewCell *cell = (BusinessFormTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    BusinessFormTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    BusinessOrderDetailViewController *detailController = [[BusinessOrderDetailViewController alloc] init];
    detailController.orderId = cell.dataParser.ID;
    detailController.typeNumber = [cell.dataParser.status integerValue];
    [self.navigationController pushViewController:detailController animated:YES];
    
}

#pragma mark - BusinessFormTableViewCellDelegate

- (void)deliverGoodsWithOrderId:(NSString *)orderId {
    VerifyDeliverGoodsViewController *controller = [[VerifyDeliverGoodsViewController alloc] init];
    controller.orderId = orderId;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)closeOrderWithOrderId:(NSString *)orderId {
    UIAlertController *alertContoller = [UIAlertController alertControllerWithTitle:nil message:@"您确定要关闭订单吗？" preferredStyle:UIAlertControllerStyleAlert] ;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager buyerOrderCloseOrderWithIds:orderId success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                
                //跳转到退款售后
                BusinessFormMainViewController *controller = (BusinessFormMainViewController *)self.parentViewController;
                controller.formType = BusinessFormTypeRefundOrAfterSale;
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertContoller addAction:cancelAction];
    [alertContoller addAction:okAction];
    [self presentViewController:alertContoller animated:YES completion:nil];
    
}

-(void)lookLogisticWithOrder:(BusinessFormListDetailParser *)orderDetail {
    if ([orderDetail.logistics_type integerValue]==4) {
        //免发货
        [XSTool  showToastWithView:self.view Text:@"暂无物流信息!"];
    } else {
        BusinessOrderLogisticViewController *logisticController = [[BusinessOrderLogisticViewController alloc] init];
          BusinessFormOrderListDatailParser *data = [orderDetail product_list][0];
        logisticController.orderId = orderDetail.ID;
        logisticController.logoUrl = data.image;
        logisticController.count = orderDetail.number_total;

        [self.navigationController pushViewController:logisticController animated:YES];

    }
   
}

@end
