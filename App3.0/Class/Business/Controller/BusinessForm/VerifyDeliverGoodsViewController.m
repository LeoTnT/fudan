//
//  VerifyDeliverGoodsViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VerifyDeliverGoodsViewController.h"
#import "BusinessVerifyOrderTableViewCell.h"
#import "BusinessFormMainViewController.h"
#import "CompanyTypeView.h"
#import "BusinessRefundDetailViewController.h"

@interface VerifyDeliverGoodsViewController ()<UITextFieldDelegate,CompanyTypeViewDelegate>
@property (nonatomic, strong) UIButton *verifyButton;
@property (nonatomic, strong) UILabel *logisticLabel;
@property (nonatomic, strong) UITextField *logisticCodeTextField;
@property (nonatomic, strong) BusinessFormOrderParser *orderParser;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, strong) CompanyTypeView *typeView;

@end

@implementation VerifyDeliverGoodsViewController

#pragma mark - lazy loadding
- (CompanyTypeView *)typeView {
    if (!_typeView) {
        _typeView = [[CompanyTypeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.typeDelegate = self;
    }
    return _typeView;
}

-(UIButton *)verifyButton {
    if (!_verifyButton) {
        _verifyButton = [UIButton new];
        [_verifyButton setTitle:@"确认发货" forState:UIControlStateNormal];
        [_verifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _verifyButton.backgroundColor = mainColor;
        [_verifyButton addTarget:self action:@selector(deliverAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _verifyButton;
}

-(UILabel *)logisticLabel {
    if (!_logisticLabel) {
        _logisticLabel = [UILabel new];
        _logisticLabel.textAlignment = NSTextAlignmentRight;
        _logisticLabel.textColor = [UIColor lightGrayColor];
        
    }
    return _logisticLabel;
}

-(UITextField *)logisticCodeTextField {
    if (!_logisticCodeTextField) {
        _logisticCodeTextField = [UITextField new];
        _logisticCodeTextField.delegate = self;
        _logisticCodeTextField.returnKeyType = UIReturnKeyDone;
        _logisticCodeTextField.textAlignment=  NSTextAlignmentRight;
        _logisticCodeTextField.placeholder = @"请输入快递单号";
    }
    return _logisticCodeTextField;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"确认发货";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.verifyButton];
    [self.verifyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    [self getOrderInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getOrderInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager buyerOrderViewWithOrderId:self.orderId supply:@"1" success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.orderParser = [BusinessFormOrderParser mj_objectWithKeyValues:dic[@"data"]];
            [self.tableView reloadData];
        } else {
            
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)deliverAction:(UIButton *) sender {
    [self.view endEditing:YES];
    if (self.companyId.length>0&&self.logisticCodeTextField.text.length>0&&(![self.logisticCodeTextField.text isEqualToString:@" "])) {
        [XSTool showProgressHUDWithView:self.view];
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *codeString = [[NSString alloc]initWithString:[self.logisticCodeTextField.text stringByTrimmingCharactersInSet:whiteSpace]];
        [HTTPManager supplySorderSendWithOrderId:self.orderId companyId:self.companyId comNo:codeString success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2]  isKindOfClass:[BusinessFormMainViewController class]]) {
                    BusinessFormMainViewController *controller = (BusinessFormMainViewController *)(self.navigationController.viewControllers[self.navigationController.viewControllers.count-2]);
                    controller.formType = BusinessFormTypeWaitReceive;
                    [self.navigationController popToViewController:controller animated:YES];
                } else if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2]  isKindOfClass:[BusinessRefundDetailViewController class]]) {
                    BusinessRefundDetailViewController *controller = (BusinessRefundDetailViewController *)(self.navigationController.viewControllers[self.navigationController.viewControllers.count-2]);
                    controller.isChange = YES;
                    [controller getDetailRefund];
                    [self.navigationController popToViewController:controller animated:YES];
                    
                }
                
                
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - CompanyTypeViewDelegate
- (void)hiddenCardView:(BusinessFormOrderLogisticDetailParser *)dataParser {
    self.companyId = dataParser.ID?dataParser.ID:@"";
    self.companyName = dataParser.com_name?dataParser.com_name:@"请选择物流";
    //消失
    [self.typeView removeFromSuperview];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString;
    if (indexPath.row==0) {
        
        //物流选择
        idString = @"log1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = @"物流选择";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell.contentView addSubview:self.logisticLabel];
        [self.logisticLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.mas_equalTo(cell.contentView);
            make.width.mas_equalTo(mainWidth-120);
        }];
        self.logisticLabel.text = self.companyName?self.companyName:@"请选择物流";
        return cell;
    } else if (indexPath.row==1) {
        
        //物流条码
        idString = @"logCode";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = @"物流条码";
        [cell.contentView addSubview:self.logisticCodeTextField];
        [self.logisticCodeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(cell.contentView);
            make.width.mas_equalTo(mainWidth-120);
            make.right.mas_equalTo(cell.contentView).with.mas_offset(-13);
        }];
        return cell;
    } else {
        
        //订单详情
        idString = @"BusinessVerifyOrderTableViewCell";
        BusinessVerifyOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[BusinessVerifyOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.orderParser) {
            cell.orderParser = self.orderParser;
        }
        
        return cell;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row<2) {
        return 50;
    } else {
        BusinessVerifyOrderTableViewCell *cell = (BusinessVerifyOrderTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        
        if (self.companyId) {
            self.typeView.selectedId = self.companyId;
        }
        self.typeView.typeArray = self.orderParser.log_companies;
        [[[ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView] ;
    }
    
}

@end

