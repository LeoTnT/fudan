//
//  BusinessOrderDetailViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessOrderDetailViewController : XSBaseTableViewController
/**订单id*/
@property (nonatomic, copy) NSString *orderId;

/**类型*/
@property (nonatomic, assign) NSUInteger typeNumber;
@end
