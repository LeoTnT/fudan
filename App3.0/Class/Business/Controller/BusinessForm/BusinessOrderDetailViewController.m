//
//  BusinessOrderDetailViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessOrderDetailViewController.h"
#import "BusinessFormModel.h"
#import "BusinessOrderDetailTopTableViewCell.h"
#import "BusinessOrderDetailBottomTableViewCell.h"
#import "BusinessFormTableViewCell.h"
#import "ChatViewController.h"
#import "PopUpView.h"
#import "ContactModel.h"

@interface BusinessOrderDetailViewController ()<PopViewDelegate>
@property (nonatomic, strong) UIView *bottomView;
//@property (nonatomic, strong) UIButton *firstButton;
//@property (nonatomic, strong) UIButton *secondButton;
@property (nonatomic, strong) BusinessFormOrderParser *orderParser;
@property (nonatomic, strong) PopUpView *sheetView;
@property (nonatomic, strong) UserInfoDataParser *userParser;

@end

@implementation BusinessOrderDetailViewController

#pragma mark - lazy loadding

-(UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

//-(UIButton *)firstButton {
//    if (!_firstButton) {
//        _firstButton = [UIButton new];
//        [_firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        _firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//
//    }
//    return _firstButton;
//}
//
//-(UIButton *)secondButton {
//    if (!_secondButton) {
//        _secondButton = [UIButton new];
//        [_secondButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        _secondButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//
//    }
//    return _secondButton;
//}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
   
//    [self.view addSubview:self.bottomView];
//    [self.bottomView makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.left.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(50);
//    }];
//    [self.bottomView addSubview:self.firstButton];
//    if (self.typeNumber==0) {
    
        //待支付
//        [self.firstButton setTitle:@"修改价格" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(modifyPriceAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth/2-0.5);
//            make.height.mas_equalTo(50);
//        }];
//        UILabel *line = [UILabel new];
//        [self.bottomView addSubview:line];
//        line.backgroundColor = LINE_COLOR_NORMAL;
//        [line makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.firstButton);
//            make.top.mas_equalTo(self.bottomView);
//            make.height.mas_equalTo(50);
//            make.width.mas_equalTo(1);
//        }];
        
//        self.secondButton = [UIButton new];
//        self.secondButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
//        self.secondButton.backgroundColor = [UIColor whiteColor];
//        [self.bottomView addSubview:self.secondButton];
//        [self.secondButton setTitle:@"关闭订单" forState:UIControlStateNormal];
//        [self.secondButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.secondButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.secondButton addTarget:self action:@selector(closeOrderAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.secondButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(line.mas_right);
//            make.width.mas_equalTo(mainWidth/2-0.5);
//            make.top.mas_equalTo(self.bottomView);
//            make.height.mas_equalTo(50);
//        }];

        
//    } else if (self.typeNumber==1) {
//        //待发货
//        [self.firstButton setTitle:@"立即发货" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(deliverGoodsAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//
//    
//    } else if (self.typeNumber==3) {
//    
//        //已发货
//        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//
//    }  else if (self.typeNumber==4) {
//        
//        //待评价
//        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//
//    } else if (self.typeNumber==5) {
//        
//        //完成
//        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//    } else if (self.typeNumber==6) {
//        
//        //已关闭
//        [self.firstButton setTitle:@"删除订单" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(deleteOrderAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//
//        
//    } else if (self.typeNumber==7) {
//        
//        //退款
//        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.mas_equalTo(self.bottomView);
//            make.width.mas_equalTo(mainWidth);
//            make.height.mas_equalTo(50);
//        }];
//
//    } else {
//    
//    }
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;

    [self getOrderInformation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getOrderInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager buyerOrderViewWithOrderId:self.orderId supply:@"1" success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.orderParser = [BusinessFormOrderParser mj_objectWithKeyValues:dic[@"data"]];
            [HTTPManager getUserInfoWithUid:self.orderParser.order.user_id success:^(NSDictionary * dic, resultObject *state) {
                
                if (state.status) {
                    self.userParser = [UserInfoParser mj_objectWithKeyValues:dic].data;
                    
                     [self.tableView reloadData];
                } else {
//                    [XSTool showToastWithView:self.view Text:state.info];
                     [self.tableView reloadData];
                }
            } fail:^(NSError * _Nonnull error) {
                 [self.tableView reloadData];
            }];
           
        } else {
            
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)modifyPriceAction:(UIButton *) sender {
    
}

- (void)closeOrderAction:(UIButton *) sender {
    
}

- (void)deliverGoodsAction:(UIButton *) sender {
   
}

- (void)lookLogisticAction:(UIButton *) sender {
    
}

- (void)deleteOrderAction:(UIButton *) sender {
    
}

- (void)linkBuyer {
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypeOrder;
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView];
   

}

#pragma mark -  PopViewDelegate
- (void)clickToCallMan {
    [UIView animateWithDuration:0.1 animations:^{
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        if (self.userParser) {
            if ([self.userParser.mobile isEqualToString:@""]||[self.userParser.mobile isEqualToString:@" "]) {
                [XSTool showToastWithView:self.view Text:@"卖家电话错误"];
            } else {
                UIWebView*callWebview =[[UIWebView alloc] init];
                NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",self.userParser.mobile]];// 貌似tel:// 或者 tel: 都行
                [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
                //记得添加到view上
                [self.view addSubview:callWebview];
            }

        } else {
            [HTTPManager getUserInfoWithUid:self.orderParser.order.user_id success:^(NSDictionary * dic, resultObject *state) {
                if (state.status) {
                    self.userParser = [UserInfoParser mj_objectWithKeyValues:dic].data;
                    if ([self.userParser.mobile isEqualToString:@""]||[self.userParser.mobile isEqualToString:@" "]) {
                        [XSTool showToastWithView:self.view Text:@"卖家电话错误"];
                    } else {
                        UIWebView*callWebview =[[UIWebView alloc] init];
                        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",self.userParser.mobile]];// 貌似tel:// 或者 tel: 都行
                        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
                        //记得添加到view上
                        [self.view addSubview:callWebview];
                    }
                
            } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                
            }];

        }
        
    }];
}

- (void)clickToMassageMan {
    [UIView animateWithDuration:0.2 animations:^{
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        
        [XSTool showProgressHUDWithView:self.view];
        if (self.userParser) {
            [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
            YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [self.userParser getName];
            chatVC.avatarUrl = self.userParser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [self.userParser getName];
            chatVC.avatarUrl = self.userParser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#else
            XMChatController *chatVC = [XMChatController new];
            SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
            chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
            [self.navigationController pushViewController:chatVC animated:YES];
#endif
            
        } else {
            [HTTPManager getUserInfoWithUid:self.orderParser.order.user_id success:^(NSDictionary * dic, resultObject *state) {
                if (state.status) {
                     self.userParser = [UserInfoParser mj_objectWithKeyValues:dic].data;
                     [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
                    YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
                    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = [self.userParser getName];
                    chatVC.avatarUrl = self.userParser.logo;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
                    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = [self.userParser getName];
                    chatVC.avatarUrl = self.userParser.logo;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
#else
                    XMChatController *chatVC = [XMChatController new];
                    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
                    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
                    [self.navigationController pushViewController:chatVC animated:YES];
#endif
                } else {
                     [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];

        }
        
    }];
    
}

- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString;
    if (indexPath.section==0) {
        idString = @"BusinessOrderDetailTopTableViewCell";
        BusinessOrderDetailTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[BusinessOrderDetailTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.orderParser) {
            BusinessFormOrderLogisticParser *parser = self.orderParser.logistics;
         cell.addressInformationArray = @[parser.rec_name,parser.rec_tel,parser.rec_addr];
        }
        
        [cell.addressView.linkButton addTarget:self action:@selector(linkBuyer) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else if (indexPath.section==1) {
          idString = @"BusinessFormTableViewCell";
        BusinessFormTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[BusinessFormTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.orderParser) {
            cell.orderParser = self.orderParser;

        }
        return cell;

    } else {
        idString = @"BusinessOrderDetailBottomTableViewCell";
        BusinessOrderDetailBottomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[BusinessOrderDetailBottomTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.orderParser) {
            cell.orderParser = self.orderParser.order;
        }
        
        
        return cell;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        BusinessOrderDetailTopTableViewCell *cell = (BusinessOrderDetailTopTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    } else if (indexPath.section==1) {
        BusinessFormTableViewCell *cell = (BusinessFormTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    } else {
        BusinessOrderDetailBottomTableViewCell *cell = (BusinessOrderDetailBottomTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
}


@end
