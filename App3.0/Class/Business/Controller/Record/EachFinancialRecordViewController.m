//
//  EachFinancialRecordViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "EachFinancialRecordViewController.h"
#import "GrayBackView.h"
#import "BusinessModel.h"
#import "RecordTableViewCell.h"

@interface EachFinancialRecordViewController ()<UITableViewDelegate,UITableViewDataSource,GrayBackViewDelegate>
@property (nonatomic, assign) BusinessRecordType recordType;
/**数据源*/
@property(nonatomic,strong)NSMutableArray *dataArray;

/**收入支出类型字典*/
@property(nonatomic,strong)NSDictionary *tradeTypeDic;
/**数据的页数*/
@property(nonatomic,assign)int page;
/**高度字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
/**灰色背景*/
@property(nonatomic,strong)GrayBackView *grayView;
/**交易类型数组*/
@property(nonatomic,strong)NSMutableArray *tradeTypeArray;

@property (strong, nonatomic) DefaultView *emptyView;
@property (nonatomic, copy) NSString *limit;

@end

@implementation EachFinancialRecordViewController
#pragma mark -lazy loadding
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}

-(NSMutableArray *)tradeTypeArray{
    if (!_tradeTypeArray) {
        _tradeTypeArray=[NSMutableArray array];
    }
    return _tradeTypeArray;
}
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray=[NSMutableArray array];
    }
    return _dataArray;
}


#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.limit = @"6";
    self.page = 1;
    [self setSubViews];
    [self setTableRefreshStyle];
    [self getNewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (instancetype)initWithRecordType:(BusinessRecordType)type {
    self = [super init];
    if (self) {
        self.recordType = type;
    }
    return self;
}

-(void)setSubViews{
    self.view.backgroundColor = BG_COLOR;
//    self.navigationController.navigationBarHidden =NO;
    self.tableViewStyle = UITableViewStylePlain;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    // 默认页
    self.emptyView = [[DefaultView alloc] initWithFrame:self.view.bounds];
    self.emptyView.button.hidden = YES;
    self.emptyView.titleLabel.text = @"暂无更多记录";
    self.emptyView.hidden = YES;
    [self.view addSubview:self.emptyView];
}

-(void)setTableRefreshStyle{
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewData];
    }];
   
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldData];
    }];
   
}

-(void)getOldData{
    NSString *numType;
    if (self.recordType==BusinessRecordTypeAll) {
        numType = nil;
    } else if (self.recordType==BusinessRecordTypeIncome) {
        numType = @"ru";
    } else {
        numType = @"out";
    }
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager userMoneyListsWithWalletType:@"s_money" leixing:nil numtype:numType page:[NSString stringWithFormat:@"%d",self.page] limit:self.limit success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            BusinessRecordParer *parser = [BusinessRecordParer mj_objectWithKeyValues:dic[@"data"]];
            if (parser.list.data.count) {
                [self.dataArray addObjectsFromArray:parser.list.data];
                [self.tableView reloadData];
                self.page++;
            } else {
                [XSTool showToastWithView:self.view Text:@"暂无更多记录"];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}


-(void)getNewData{
    self.page = 1;
    NSString *numType = @"";
    if (self.recordType==BusinessRecordTypeAll) {
        numType = nil;
    } else if (self.recordType==BusinessRecordTypeIncome) {
        numType = @"ru";
    } else {
        numType = @"out";
    }
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager userMoneyListsWithWalletType:@"s_money" leixing:nil numtype:numType page:[NSString stringWithFormat:@"%d",self.page] limit:self.limit success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            BusinessRecordParer *parser = [BusinessRecordParer mj_objectWithKeyValues:dic[@"data"]];
            if (parser.list.data.count) {
                self.emptyView.hidden = YES;
                self.view.backgroundColor=BG_COLOR;
                [self.dataArray removeAllObjects];
                [self.dataArray addObjectsFromArray:parser.list.data];
                [self.tableView reloadData];
                self.page++;
            } else {
                self.view.backgroundColor=[UIColor whiteColor];
                self.emptyView.hidden = NO;
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
-(void)alertChooseTypeView{
    //保证只请求一次
    if(self.tradeTypeArray.count){
        self.grayView.hidden=NO;
    }else{
        [XSTool showProgressHUDTOView:self.view withText:nil];
        
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        return self.dataArray.count;
    }else{
        return self.tradeTypeArray.count+1;
    }
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if ([tableView isEqual:self.tableView]) {
        static NSString *cellId=@"RecordTableViewCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[RecordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        ((RecordTableViewCell *)cell).record=self.dataArray[indexPath.row];
        self.heightDic[@(indexPath.row)]=@(((RecordTableViewCell *)cell).height);
    }else{
        static NSString *cellId=@"tradeCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        if(indexPath.row==0){
            cell.textLabel.text=@"全部记录";
        }else{
            cell.textLabel.text=[self.tradeTypeArray[indexPath.row-1] name];
        }
        [cell.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refreshData:)]];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark-行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]&&self.heightDic[@(indexPath.row)]) {
        return [self.heightDic[@(indexPath.row)] floatValue];
    }else  if([tableView isEqual:self.tableView]){
        return 100;
    }
    return 40;
}
-(void)refreshData:(UITapGestureRecognizer *)tap{
    self.grayView.hidden=YES;
    //重新刷新表格
    [self.tableView.mj_header beginRefreshing];
    [self getNewData];
}

#pragma mark-GrayBackViewDelegate
-(void)grayViewClicked{
    self.grayView.hidden=YES;
}

@end
