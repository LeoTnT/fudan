//
//  EachFinancialRecordViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BusinessRecordType)
{
    BusinessRecordTypeAll,
    BusinessRecordTypeIncome,
    BusinessRecordTypeExpend

};

@interface EachFinancialRecordViewController : XSBaseTableViewController
- (instancetype)initWithRecordType:(BusinessRecordType)type;
@end
