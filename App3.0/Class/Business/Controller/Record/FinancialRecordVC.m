//
//  FinancialRecordVC.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//
#define SEGMENT_HEIGHT 40.0f
#import "FinancialRecordVC.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "EachFinancialRecordViewController.h"

@interface FinancialRecordVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) EachFinancialRecordViewController *recordController1;
@property (nonatomic, strong) EachFinancialRecordViewController *recordController2;
@property (nonatomic, strong) EachFinancialRecordViewController *recordController3;
@end

@implementation FinancialRecordVC

#pragma mark - lazy loadding
-(EachFinancialRecordViewController *)recordController1 {
    if (!_recordController1) {
        _recordController1= [[EachFinancialRecordViewController alloc] initWithRecordType:BusinessRecordTypeAll];
        _recordController1.view.frame = CGRectMake(0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _recordController1;
    
}
-(EachFinancialRecordViewController *)recordController2 {
    if (!_recordController2) {
        _recordController2= [[EachFinancialRecordViewController alloc] initWithRecordType:BusinessRecordTypeIncome];
        _recordController2.view.frame = CGRectMake(mainWidth, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _recordController2;
    
}
-(EachFinancialRecordViewController *)recordController3 {
    if (!_recordController3) {
        _recordController3= [[EachFinancialRecordViewController alloc] initWithRecordType:BusinessRecordTypeExpend];
        _recordController3.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _recordController3;
    
}

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT, mainWidth, MAIN_VC_HEIGHT-SEGMENT_HEIGHT)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*3, MAIN_VC_HEIGHT-SEGMENT_HEIGHT);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
//        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, SEGMENT_HEIGHT)];
        _segmentControl.sectionTitles = @[@"货款记录",@"货款收入",@"货款支出"];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 3;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        [_segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}


#pragma mark -life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"财务记录";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    self.view.backgroundColor = BG_COLOR;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT-0.5, mainWidth, 0.5)];
    lineView.backgroundColor = BG_COLOR;
    [self.view addSubview:lineView];
    [self addChildViewController:self.recordController1];
    [self.scrollView addSubview:self.recordController1.view];
    
    [self addChildViewController:self.recordController2];
    [self.scrollView addSubview:self.recordController2.view];
    
    [self addChildViewController:self.recordController3];
    [self.scrollView addSubview:self.recordController3.view];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
}


@end
