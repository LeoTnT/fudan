//
//  DeliveryVoucherViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DeliveryVoucherViewController.h"

@interface DeliveryVoucherViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *verifyButton;
@end

@implementation DeliveryVoucherViewController

#pragma mark - private
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"取货凭证";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.autoHideKeyboard = YES;
    UIView *topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"business_intro"];
    [topView addSubview:imageView];
    self.textView = [UITextView new];
    self.textView.editable = NO;
//    self.textView.userInteractionEnabled = NO;
    self.textView.text = @"取货凭证用于验证交易真实性；\n每笔钱下取货交易，卖家收到一组12位数字取货凭证；\n请先验证取货凭证后，再接单！";
    self.textView.font = [UIFont qsh_systemFontOfSize:16];;
//    self.textView.backgroundColor = [UIColor blackColor];
    [topView addSubview:self.textView];
    [self.view addSubview:topView];
    self.textField = [UITextField new];
    self.textField.delegate = self;
    self.textField.returnKeyType = UIReturnKeyDone;
    self.textField.backgroundColor = [UIColor whiteColor];
    self.textField.placeholder = @"请输入取货凭证码";
    [self.view addSubview:self.textField];
    self.verifyButton = [UIButton new];
    [self.verifyButton setTitle:@"验证" forState:UIControlStateNormal];
    self.verifyButton.backgroundColor = mainColor;
    self.verifyButton.layer.cornerRadius = 5;
    self.verifyButton.layer.masksToBounds = YES;
    [self.view addSubview:self.verifyButton];
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(NORMOL_SPACE);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(120);
    }];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView).with.offset(2*NORMOL_SPACE);
        make.left.mas_equalTo(topView).with.offset(NORMOL_SPACE);
        make.height.mas_equalTo(topView.mas_height).with.offset(-4*NORMOL_SPACE);
        make.width.mas_equalTo(imageView.mas_height);
        
    }];
  
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_top).with.offset(NORMOL_SPACE);
        make.left.mas_equalTo(imageView.mas_right).with.offset(NORMOL_SPACE);
        make.height.mas_equalTo(topView.mas_height).with.offset(-NORMOL_SPACE*2);
        make.right.mas_equalTo(topView).with.offset(-NORMOL_SPACE);
    }];

    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_bottom).with.offset(NORMOL_SPACE);
        make.left.mas_equalTo(self.view).with.offset(NORMOL_SPACE);
        make.size.mas_equalTo(CGSizeMake(mainWidth-2*NORMOL_SPACE, 40));
    }];

    [self.verifyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textField.mas_bottom).with.offset(NORMOL_SPACE*2);
        make.left.mas_equalTo(self.textField);
        make.size.mas_equalTo(CGSizeMake(mainWidth-2*NORMOL_SPACE, 40));
    }];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.view endEditing:YES];
}

@end
