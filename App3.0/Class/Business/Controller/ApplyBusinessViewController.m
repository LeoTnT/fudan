
//
//  ApplyBusinessViewController.m
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ApplyBusinessViewController.h"
#import "XSCustomButton.h"
#import "XSApi.h"
#import "TOCropViewController.h"
#import "TradeView.h"
#import "BusinessModel.h"
#import "ProvinceViewController.h"
#import "OrderPayViewController.h"
#import "UIImage+XSWebImage.h"
#import "UIImageView+WebCache.h"
#import "BusinessMainViewController.h"
#import "AgreementViewController.h"

typedef NS_ENUM(NSInteger, ChoosePhotoType) {
    ChoosePhotoTypeIcon,
    ChoosePhotoTypeFontIdentity,
    ChoosePhotoTypeBehindIdentity,
    ChoosePhotoTypeLicense,
    ChoosePhotoTypeCode
} ;
@interface ApplyBusinessViewController ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate,TradeViewDelegate>

@property (nonatomic, strong) UIImageView *businessIcon;//商家版头像
@property (nonatomic, strong) UITextField *businessName;//商家名称
@property (nonatomic, strong) UITextField *partnerNumber;//股权合伙人编号
@property (nonatomic, strong) UITextField *trueName;//真实姓名
@property (nonatomic, strong) UITextField *identityNumber;//身份证号
@property (nonatomic, strong) UITextField *detailAddress;//详细地址
@property (nonatomic, strong) UILabel *enterMoney;//入住费用
@property (nonatomic, strong) UILabel *cityName;//城市
@property (nonatomic, strong) UIImagePickerController *imagePickVC;//拍照控制器
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, copy) NSString *cityString;
@property (nonatomic, strong) NSMutableArray *tradeArray;//行业数组
@property (nonatomic, strong)  UIAlertController *alert;
@property (nonatomic, assign) ChoosePhotoType choosePhotoType;
@property (nonatomic, strong) UIImage *iconImage;//头像
@property (nonatomic, strong) UIImage *fontImage;//选中的image
@property (nonatomic, strong) UIImage *behindImage;
@property (nonatomic, strong) UIButton *fontButton;//正面照
@property (nonatomic, strong) UIButton *behindButton;//反面照
@property (nonatomic, strong) UIImage *licenseImage;//营业执照image
@property (nonatomic, strong) UIImage *codeImage;//机构代码证
@property (nonatomic, strong) UIButton *licenseButton;
@property (nonatomic, strong) UIButton *codeButton;
@property (nonatomic, assign) BOOL photoFlag;//是否是正面照
@property (nonatomic, assign) BOOL takePhotoflag;//是否是拍照
@property (nonatomic, strong) TradeView *tradeView;
@property (nonatomic, strong) BusinessApplyIndustryParser *industryParser;
@property (nonatomic, strong) UIButton *agreementButton;
@property (nonatomic, assign) CGFloat currentY;
@property (nonatomic, strong) BusinessRegisterParser *businessParser;
@property (nonatomic, strong) XSCustomButton *nextButton;
@end

@implementation ApplyBusinessViewController

#pragma mark - lazy loadding
-(UIButton *)agreementButton {
    if (!_agreementButton) {
        _agreementButton = [[UIButton alloc] init];
        [_agreementButton setBackgroundImage:[UIImage imageNamed:@"user_wallet_select"] forState:UIControlStateNormal];
        [_agreementButton setBackgroundImage:[UIImage imageNamed:@"reg_unselect"] forState:UIControlStateSelected];
        [_agreementButton addTarget:self action:@selector(changeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _agreementButton;
}

- (UIButton *)fontButton {
    if (!_fontButton) {
        _fontButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth-2*NORMOL_SPACE, 160)];
        _fontButton.backgroundColor = BG_COLOR;
        [_fontButton setTitle:Localized(@"user_approve_card_front") forState:UIControlStateNormal];
        _fontButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_fontButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [_fontButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_fontButton setImage:img forState:UIControlStateNormal];
        [_fontButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateSelected];
        
        //偏移
        _fontButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(_fontButton.imageView.frame), CGRectGetWidth(_fontButton.titleLabel.frame), 0, 0);
        _fontButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE, -CGRectGetWidth(_fontButton.imageView.frame), 0, 0);
        [_fontButton addTarget:self action:@selector(uploadFontAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fontButton;
}

- (UIButton *)behindButton {
    if (!_behindButton) {
        _behindButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth-2*NORMOL_SPACE, 160)];
        _behindButton.backgroundColor = BG_COLOR;
        [_behindButton setTitle:@"上传身份证反面" forState:UIControlStateNormal];
        _behindButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_behindButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [_behindButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_behindButton setImage:img forState:UIControlStateNormal];
        [_behindButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateSelected];
        
        //偏移
        _behindButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(_behindButton.imageView.frame), CGRectGetWidth(_behindButton.titleLabel.frame), 0, 0);
        _behindButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE, -CGRectGetWidth(_behindButton.imageView.frame), 0, 0);
        [_behindButton addTarget:self action:@selector(uploadBehindAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _behindButton;
}

- (UIButton *)licenseButton {
    if (!_licenseButton) {
        _licenseButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth-2*NORMOL_SPACE, 160)];
        _licenseButton.backgroundColor = BG_COLOR;
        [_licenseButton setTitle:@"上传营业执照" forState:UIControlStateNormal];
        _licenseButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_licenseButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [_licenseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_licenseButton setImage:img forState:UIControlStateNormal];
        [_licenseButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateSelected];
        
        //偏移
        _licenseButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(_licenseButton.imageView.frame), CGRectGetWidth(_licenseButton.titleLabel.frame), 0, 0);
        _licenseButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE, -CGRectGetWidth(_licenseButton.imageView.frame), 0, 0);
        [_licenseButton addTarget:self action:@selector(licenseAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _licenseButton;
}

- (UIButton *)codeButton {
    if (!_codeButton) {
        _codeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth-2*NORMOL_SPACE, 160)];
        _codeButton.backgroundColor = BG_COLOR;
        [_codeButton setTitle:@"组织机构代码证" forState:UIControlStateNormal];
        _codeButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_codeButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [_codeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        UIImage *img = [UIImage imageNamed:@"user_auth_behind"] ;
        [_codeButton setImage:img forState:UIControlStateNormal];
        [_codeButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateSelected];
        
        //偏移
        _codeButton.imageEdgeInsets = UIEdgeInsetsMake(-CGRectGetHeight(_codeButton.imageView.frame), CGRectGetWidth(_codeButton.titleLabel.frame), 0, 0);
        _codeButton.titleEdgeInsets = UIEdgeInsetsMake(NORMOL_SPACE, -CGRectGetWidth(_codeButton.imageView.frame), 0, 0);
        [_codeButton addTarget:self action:@selector(codeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeButton;
}

- (UIImageView *)businessIcon {
    if (!_businessIcon) {
        _businessIcon = [[UIImageView alloc] init];
        _businessIcon.layer.masksToBounds = YES;
        _businessIcon.image = self.iconImage;
    }
    return _businessIcon;
}

- (UITextField *)businessName {
    if (!_businessName) {
        _businessName = [[UITextField alloc] init];
        //        _businessName.textColor = LINE_COLOR;
        _businessName.delegate = self;
        _businessName.textAlignment = NSTextAlignmentRight;
        _businessName.font = [UIFont systemFontOfSize:15];
        _businessName.placeholder = @"请输入商家名称";
        _businessName.returnKeyType = UIReturnKeyDone;
    }
    return _businessName;
}

- (UITextField *)partnerNumber {
    if (!_partnerNumber) {
        _partnerNumber = [[UITextField alloc] init];
        //        _partnerNumber.textColor = LINE_COLOR;
        _partnerNumber.textAlignment = NSTextAlignmentRight;
        _partnerNumber.font = [UIFont systemFontOfSize:15];
        _partnerNumber.placeholder = @"请输入股权合伙人编号";
        _partnerNumber.delegate = self;
        _partnerNumber.returnKeyType = UIReturnKeyDone;
    }
    return _partnerNumber;
}

- (UITextField *)trueName {
    if (!_trueName) {
        _trueName = [[UITextField alloc] init];
        //        _trueName.textColor = LINE_COLOR;
        _trueName.textAlignment = NSTextAlignmentRight;
        _trueName.font = [UIFont systemFontOfSize:15];
        _trueName.placeholder = @"和身份证上名字保持一致";
        _trueName.delegate = self;
        _trueName.returnKeyType = UIReturnKeyDone;
    }
    return _trueName;
}

- (UITextField *)identityNumber {
    if (!_identityNumber) {
        _identityNumber = [[UITextField alloc] init];
        //        _identityNumber.textColor = LINE_COLOR;
        _identityNumber.textAlignment = NSTextAlignmentRight;
        _identityNumber.font = [UIFont systemFontOfSize:15];
        _identityNumber.placeholder = @"请输入身份身份证号";
        _identityNumber.delegate = self;
        _identityNumber.returnKeyType = UIReturnKeyDone;
    }
    return _identityNumber;
}

- (UITextField *)detailAddress {
    if (!_detailAddress) {
        _detailAddress = [[UITextField alloc] init];
        //        _detailAddress.textColor = LINE_COLOR;
        _detailAddress.textAlignment = NSTextAlignmentRight;
        _detailAddress.font = [UIFont systemFontOfSize:15];
        _detailAddress.placeholder = Localized(@"register_addr");
        _detailAddress.returnKeyType = UIReturnKeyDone;
        _detailAddress.delegate = self;
    }
    return _detailAddress;
}

- (UILabel *)cityName {
    if (!_cityName) {
        _cityName = [[UILabel alloc] init];
        _cityName.textAlignment = NSTextAlignmentRight;
        _cityName.font = [UIFont systemFontOfSize:15];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCity:)];
        _cityName.userInteractionEnabled = YES;
        [_cityName addGestureRecognizer:tap];
    }
    return _cityName;
    
}

- (UILabel *)enterMoney {
    if (!_enterMoney) {
        _enterMoney = [[UILabel alloc] init];
        _enterMoney.font = [UIFont systemFontOfSize:17];
    }
    return _enterMoney;
}

- (NSMutableArray *)tradeArray {
    if (!_tradeArray) {
        _tradeArray = [NSMutableArray array];
    }
    return _tradeArray;
}

- (TradeView *)tradeView {
    if (!_tradeView) {
        _tradeView = [[TradeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _tradeView.delegate = self;
    }
    return _tradeView;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商家版";
    self.view.backgroundColor = [UIColor whiteColor];

    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.iconImage = [UIImage imageNamed:@"chat_single"];
    self.photoFlag = NO;
    self.cityString = @"请选择城市";
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.bounces = NO;
    [self getBusinessInformation];
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    //    [self getTradeInfo];
    
}
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString ;
    UITableViewCell *cell;
    if (indexPath.section==0) {
        //头像,名称，行业，合伙人
        if (indexPath.row==0) {
            idString = @"applyIconCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"店铺头像";
            CGFloat iconSize = 40;
            self.businessIcon.frame = CGRectMake(mainWidth-NORMOL_SPACE*2-iconSize, NORMOL_SPACE, iconSize, iconSize);
            self.businessIcon.image =self.iconImage;
            self.businessIcon.layer.cornerRadius =  iconSize/2;
            [cell.contentView addSubview:self.businessIcon];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.businessIcon.frame)-0.5+NORMOL_SPACE, mainWidth-NORMOL_SPACE, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cell.contentView addSubview:line];
        } else if (indexPath.row==1) {
            idString = @"applyNameCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"商家名称";
            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
            self.businessName.frame = CGRectMake(100, 0, nameWidth, 40);
            [cell.contentView addSubview:self.businessName];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.businessName.frame)-0.5, mainWidth-NORMOL_SPACE, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cell.contentView addSubview:line];
            
        } else {
            idString = @"applyTradesCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"所属行业";
            cell.detailTextLabel.text = self.industryParser.name;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
         
        }
        //        } else {
        //            idString = @"applyPartnerCell";
        //            cell = [tableView dequeueReusableCellWithIdentifier:idString];
        //            if (!cell) {
        //                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
        //                cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //            }
        //            cell.textLabel.text = @"股权合伙人";
        //            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
        //            self.partnerNumber.frame = CGRectMake(100, 0, nameWidth, 40);
        //            [cell.contentView addSubview:self.partnerNumber];
        //            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.partnerNumber.frame)-0.5, mainWidth-NORMOL_SPACE, 0.5)];
        //            line.backgroundColor = LINE_COLOR;
        //            [cell.contentView addSubview:line];
        //        }
    }else if (indexPath.section==1) {
        if (indexPath.row==0) {
            idString = @"applyNameCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = Localized(@"user_approve_name");
            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
            self.trueName.frame = CGRectMake(100, 0, nameWidth, 40);
            [cell.contentView addSubview:self.trueName];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.trueName.frame)-0.5, mainWidth-NORMOL_SPACE, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cell.contentView addSubview:line];
        } else {
            idString = @"applyCodeCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = Localized(@"user_approve_cardno");
            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
            self.identityNumber.frame = CGRectMake(100, 0, nameWidth, 40);
            [cell.contentView addSubview:self.identityNumber];
           
        }
    }else if (indexPath.section==2){
        idString = @"identifyFontCell";
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //内容
        UIView *bView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 220)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE/2, NORMOL_SPACE*14, 45-NORMOL_SPACE)];
        titleLabel.font = [UIFont systemFontOfSize:15];
        [bView addSubview:titleLabel];
        UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-CGRectGetMaxX(titleLabel.frame)-NORMOL_SPACE,45-NORMOL_SPACE)];
        hintLabel.font = [UIFont systemFontOfSize:11];
        hintLabel.textAlignment = NSTextAlignmentRight;
        //        hintLabel.backgroundColor = mainGrayColor;
        //        titleLabel.backgroundColor = mainColor;
        hintLabel.text = Localized(@"上传图片大小控制在3M以内！");
        hintLabel.textColor = [UIColor redColor];
        [bView addSubview:hintLabel];
        CGFloat height = 160;
        if (indexPath.row==0) {
            titleLabel.text  =Localized(@"user_approve_card_front");
            
            //上传图片按钮
            self.fontButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE/2, mainWidth-2*NORMOL_SPACE, height);
            [self.fontButton setBackgroundImage:self.fontImage forState:UIControlStateNormal];
            [bView addSubview:self.fontButton];
            if (self.choosePhotoType==ChoosePhotoTypeFontIdentity) {
                //选过照片
                [self.fontButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
                [self.fontButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                self.fontButton.backgroundColor = [UIColor grayColor];
            }
            [cell.contentView addSubview:bView];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(bView.frame)-0.5, mainWidth-NORMOL_SPACE, 0.5)];
            line.backgroundColor = LINE_COLOR;
            [cell.contentView addSubview:line];
        } else {
            titleLabel.text  =Localized(@"user_approve_card_back");
            self.behindButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE/2, mainWidth-2*NORMOL_SPACE, height);
            self.behindButton.backgroundColor = BG_COLOR;
            [self.behindButton setBackgroundImage:self.behindImage forState:UIControlStateNormal];
            if (self.choosePhotoType==ChoosePhotoTypeBehindIdentity) {
                //选过照片
                [self.behindButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
                [self.behindButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                self.behindButton.backgroundColor = [UIColor grayColor];
            }
            [bView addSubview:self.behindButton];
            [cell.contentView addSubview:bView];
        }
    } else if (indexPath.section==3){
        if (indexPath.row==0) {
            idString = @"applyAddressCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"营业地址";
            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
            self.cityName.frame = CGRectMake(100, 0, nameWidth, 40);
            [cell.contentView addSubview:self.cityName];
            self.cityName.text = self.cityString;
            [cell.contentView addSubview:self.cityName];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.cityName.frame)-0.5, mainWidth-NORMOL_SPACE, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cell.contentView addSubview:line];
        } else {
            idString = @"applyDetailCell";
            cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = Localized(@"detail_addr");
            CGFloat nameWidth = mainWidth-100-NORMOL_SPACE*2;
            self.detailAddress.frame = CGRectMake(100, 0, nameWidth, 40);
            [cell.contentView addSubview:self.detailAddress];
            
        }
    } else if (indexPath.section==4){
        idString = @"identifyFontCell";
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //内容
        UIView *bView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 220)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE/2, NORMOL_SPACE*14, 45-NORMOL_SPACE)];
        titleLabel.font = [UIFont systemFontOfSize:15];
        [bView addSubview:titleLabel];
        UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-CGRectGetMaxX(titleLabel.frame)-NORMOL_SPACE,45-NORMOL_SPACE)];
        hintLabel.font = [UIFont systemFontOfSize:11];
        hintLabel.textAlignment = NSTextAlignmentRight;
        //        hintLabel.backgroundColor = mainGrayColor;
        //        titleLabel.backgroundColor = mainColor;
        hintLabel.text = Localized(@"上传图片大小控制在3M以内！");
        hintLabel.textColor = [UIColor redColor];
        [bView addSubview:hintLabel];
        CGFloat height = 160;
        titleLabel.text  =@"上传营业执照";
        
        //上传图片按钮
        self.licenseButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE/2, mainWidth-2*NORMOL_SPACE, height);
        [self.licenseButton setBackgroundImage:self.licenseImage forState:UIControlStateNormal];
        if (self.choosePhotoType==ChoosePhotoTypeLicense) {
            //选过照片
            [self.licenseButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
            [self.licenseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.licenseButton.backgroundColor = [UIColor grayColor];
            
        }
        [bView addSubview:self.licenseButton];
        [cell.contentView addSubview:bView];
        
        return cell;
    } else if (indexPath.section==5){
        idString = @"identifyFontCell";
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.backgroundColor = mainColor;
        
        //内容
        UIView *bView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 220)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE/2, NORMOL_SPACE*14, 45-NORMOL_SPACE)];
        titleLabel.font = [UIFont systemFontOfSize:15];
        [bView addSubview:titleLabel];
        UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(titleLabel.frame), mainWidth-CGRectGetMaxX(titleLabel.frame)-NORMOL_SPACE,45-NORMOL_SPACE)];
        hintLabel.font = [UIFont systemFontOfSize:11];
        hintLabel.textAlignment = NSTextAlignmentRight;
        //        hintLabel.backgroundColor = mainGrayColor;
        //        titleLabel.backgroundColor = mainColor;
        hintLabel.text = Localized(@"上传图片大小控制在3M以内！");
        hintLabel.textColor = [UIColor redColor];
        [bView addSubview:hintLabel];
        CGFloat height = 160;
        titleLabel.text  =@"上传组织机构代码证";
        
        //上传图片按钮
        self.codeButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+NORMOL_SPACE/2, mainWidth-2*NORMOL_SPACE, height);
        [self.codeButton setBackgroundImage:self.codeImage forState:UIControlStateNormal];
        [bView addSubview:self.codeButton];
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.codeButton.frame)-NORMOL_SPACE*6, CGRectGetWidth(self.codeButton.frame), NORMOL_SPACE*2)];
        infoLabel.text = @"如果是三证合一，可不上传";
        infoLabel.textAlignment = NSTextAlignmentCenter;
        infoLabel.textColor = [UIColor grayColor];
        infoLabel.font = [UIFont systemFontOfSize:12];
        infoLabel.backgroundColor = [UIColor clearColor];
        [bView addSubview:infoLabel];
        if (self.choosePhotoType==ChoosePhotoTypeCode) {
            //选过照片
            [self.codeButton setImage:[UIImage imageNamed:@"user_auth_front"] forState:UIControlStateNormal];
            [self.codeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.codeButton.backgroundColor = [UIColor grayColor];
            infoLabel.textColor = [UIColor whiteColor];
        }
        
        [cell.contentView addSubview:bView];
        
        return cell;
    } else {
        idString = @"finishedCell";
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //服务协议
        UIView *proView = [[UIView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, mainWidth-30, 30)];
        [self.view addSubview:proView];
        
        //是否选中的按钮
        self.agreementButton.frame = CGRectMake(0, 0, 20, 20);
        [proView addSubview:self.agreementButton];
        
        //文字
        UILabel * firstLabel = [[UILabel alloc] init];
        firstLabel.textColor = mainGrayColor;
        NSString *str = @"注册即视为同意";
        UIFont *tFont = [UIFont systemFontOfSize:15];
        firstLabel.font = tFont;
        firstLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        firstLabel.text = str;
        [firstLabel setBackgroundColor:BG_COLOR];
        CGSize size = CGSizeMake(300, 20);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
        CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
        firstLabel.frame =CGRectMake(CGRectGetMaxX(self.agreementButton.frame)+5, 0, actualsize.width, actualsize.height);
        [proView addSubview:firstLabel];
        UILabel * secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(firstLabel.frame), 0, mainWidth-10-CGRectGetMaxX(firstLabel.frame), 20)];
        secondLabel.adjustsFontSizeToFitWidth = YES;
        secondLabel.text = [NSString stringWithFormat:@"%@服务协议",APP_NAME];
        secondLabel.textColor = [UIColor redColor];
        secondLabel.font = [UIFont systemFontOfSize:15];
        
        //添加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkYubaoProtocol:)];
        tap.numberOfTapsRequired = 1;
        secondLabel.userInteractionEnabled = YES;
        [secondLabel addGestureRecognizer:tap];
        [proView addSubview:secondLabel];
        [cell.contentView addSubview:proView];
        self.enterMoney.frame = CGRectMake(NORMOL_SPACE, CGRectGetMaxY(proView.frame), mainWidth-NORMOL_SPACE*2, 40);
        CGFloat nowMaxY = CGRectGetMaxY(proView.frame);
        NSString *nextString = Localized(@"bug_submit_do");
        //        if ([self.isEnterPay integerValue]==1) {
        //            NSString *money = [NSString stringWithFormat:@"入驻费用：%@元",self.enterPayMoney];
        //            NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:money];
        //            [attStr addAttribute:NSForegroundColorAttributeName
        //                           value:[UIColor redColor]
        //                           range:NSMakeRange(5, money.length-6)];
        //            self.enterMoney.attributedText = attStr;
        //
        //             [cell.contentView addSubview:self.enterMoney];
        //            nowMaxY = CGRectGetMaxY(self.enterMoney.frame);
        //            nextString = @"下一步";
        //        }
        
        self.nextButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, nowMaxY+NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, 40) title:nextString titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor]highBackgroundColor:HighLightColor_Main];
        [self.nextButton setBorderWith:0 borderColor:nil cornerRadius:5];
        [self.nextButton addTarget:self action:@selector(clickToNextAction) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:self.nextButton];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 3;
    } else if (section==1||section==2||section==3){
        return 2;
    } else {
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return NORMOL_SPACE;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 60;
        } else {
            return 40;
        }
    } else if (indexPath.section==1) {
        return 40;
    } else if (indexPath.section==2||indexPath.section==4||indexPath.section==5) {
        return 220;
    } else if (indexPath.section==3) {
        return 40;
    } else {
        return 150;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            self.choosePhotoType = ChoosePhotoTypeIcon;
            [self choosePhotoes];
        }
        if (indexPath.row==2) {
            [self.view endEditing:YES];
            [self getTradeInfo];
            
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

#pragma mark - private

- (void) getBusinessInformation {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getSupplyRegisterEnterInfoSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            
            self.businessParser = [BusinessRegisterParser mj_objectWithKeyValues:dic[@"data"]];
            
            //更新pc端申请信息
            if ((NSNull *)self.businessParser.industry_id == [NSNull null]||(!self.businessParser.industry_id)||[[NSString stringWithFormat:@"%@",self.businessParser.industry_id] isEqualToString:@""]) {
            } else {
                for (BusinessIndustryParser *industryParser in self.businessParser.industry) {
                    if ([industryParser.iid isEqualToString:self.businessParser.industry_id]) {
                        self.industryParser = [BusinessApplyIndustryParser mj_objectWithKeyValues:@{@"id":industryParser.iid,@"name":industryParser.iname,@"image":@""}];
                        break;
                    }
                }
                
            }
            if (self.businessParser.logo.length>1) {
                
                NSData *logoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.businessParser.logo]];
                self.iconImage = [UIImage imageWithData:logoData];
                if (self.iconImage) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:0];
                        [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                    });
                }
                
                
            } else {
                self.iconImage = [UIImage imageNamed:@"chat_single"];
            }
            if (self.businessParser.img_card.count) {
                for (int i=0;i<self.businessParser.img_card.count;i++) {
                    if (!isEmptyString(self.businessParser.img_card[i])) {
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.businessParser.img_card[i]]];
                        if (i==0) {
                            self.fontImage = [UIImage imageWithData:imageData];
                        } else {
                            self.behindImage = [UIImage imageWithData:imageData];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSIndexPath *index1 = [NSIndexPath indexPathForRow:i inSection:2];
                            [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                        });
                        
                    }
                    
                }
                
            }
            if (self.businessParser.img_license) {
                NSData *licenseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.businessParser.img_license]];
                self.licenseImage = [UIImage imageWithData:licenseData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:4];
                    [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                });
                
            }
            if (self.businessParser.img_zuzhi) {
                NSData *zuzhiData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.businessParser.img_zuzhi]];
                self.codeImage = [UIImage imageWithData:zuzhiData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:5];
                    [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
                });
            }
            self.businessName.text = self.businessParser.sname?self.businessParser.sname:nil;
            self.trueName.text = self.businessParser.truename?self.businessParser.truename:nil;
            self.identityNumber.text = self.businessParser.card_no?self.businessParser.card_no:nil;
            if (self.businessParser.province&&self.businessParser.city&&self.businessParser.county&&self.businessParser.town&&self.businessParser.province_code&&self.businessParser.county_code&&self.businessParser.city_code&&self.businessParser.town_code) {
                self.areaArray = @[@[self.businessParser.province,self.businessParser.province_code],@[self.businessParser.city,self.businessParser.city_code],@[self.businessParser.county,self.businessParser.county_code],@[self.businessParser.town,self.businessParser.town_code]];
                
            }
            self.detailAddress.text = self.businessParser.detail?self.businessParser.detail:nil;
            [self.tableView reloadData];
            
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
    
}

-(void)setAreaArray:(NSArray *)areaArray {
    _areaArray = areaArray;
    self.cityString = [NSString stringWithFormat:@"%@ %@ %@ %@",[[_areaArray firstObject] firstObject],[_areaArray[1] firstObject],[_areaArray[2] firstObject],[_areaArray[3] firstObject]];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:3];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)getTradeInfo {
    if (self.tradeArray.count) {
        self.tradeView.tradeArray = self.tradeArray;
        [[[UIApplication sharedApplication] keyWindow] addSubview:self.tradeView];
    } else {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getIndustryListsSuccess:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if ([dic[@"status"] integerValue]==1) {
                BusinessApplyIndustryListParser *listParser = [BusinessApplyIndustryListParser mj_objectWithKeyValues:dic[@"data"]];
                [self.tradeArray addObjectsFromArray:listParser.data];
                NSLog(@"trade:::::::%lu",self.tradeArray.count);
                self.tradeView.tradeArray = self.tradeArray;
                [[[UIApplication sharedApplication] keyWindow] addSubview:self.tradeView];
            }
            
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
        
    }
    
}

- (void)uploadFontAction:(UIButton *)sender {
    //     self.photoFlag = YES;
    self.choosePhotoType = ChoosePhotoTypeFontIdentity;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:2];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self choosePhotoes];
}

- (void)uploadBehindAction:(UIButton *)sender {
    //     self.photoFlag = YES;
    self.choosePhotoType = ChoosePhotoTypeBehindIdentity;
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:2];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self choosePhotoes];
}

- (void)licenseAction:(UIButton *)sender {
    //     self.photoFlag = YES;
    self.choosePhotoType = ChoosePhotoTypeLicense;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:4];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self choosePhotoes];
}

- (void)codeAction:(UIButton *)sender {
    //     self.photoFlag = YES;
    self.choosePhotoType = ChoosePhotoTypeCode;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:5];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self choosePhotoes];
    
}

- (void)changeAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.nextButton.enabled = NO;
    } else {
        self.nextButton.enabled = YES;
    }
    
}

- (void)checkYubaoProtocol:(UITapGestureRecognizer *)tap {
    AgreementViewController *agreeVC=[[AgreementViewController alloc] initWithAgreementType:AgreementTypeStore];
    [self.navigationController pushViewController:agreeVC animated:YES];
}

- (void)clickToNextAction {
    NSLog(@"下一步！去支付");
    if (!self.iconImage) {
        [XSTool showToastWithView:self.view Text:@"请选择店铺头像"];
        return;
    }
    if (isEmptyString(self.businessName.text)) {
        [XSTool showToastWithView:self.view Text:@"请完善商家名称"];
        return;
    }
    if (isEmptyString(self.industryParser.name)) {
        [XSTool showToastWithView:self.view Text:@"请完善所属行业"];
        return;
    }
    if (isEmptyString(self.trueName.text)) {
        [XSTool showToastWithView:self.view Text:@"请完善真实姓名"];
        return;
    }
    if (isEmptyString(self.identityNumber.text)) {
        [XSTool showToastWithView:self.view Text:@"请完善身份证号"];
        return;
    }
    if (!self.fontImage) {
        [XSTool showToastWithView:self.view Text:@"请上传身份证正面照片"];
        return;
    }
    if (!self.behindImage) {
        [XSTool showToastWithView:self.view Text:@"请上传身份证背面照片"];
        return;
    }
    if (!(self.areaArray.count==4)) {
        [XSTool showToastWithView:self.view Text:@"请完善营业地址"];
        return;
    }
    if (isEmptyString(self.detailAddress.text)) {
        [XSTool showToastWithView:self.view Text:@"请完善详细地址"];
        return;
    }
    if (!self.licenseImage) {
        [XSTool showToastWithView:self.view Text:@"请上传营业执照照片"];
        return;
    }
    if (self.iconImage&&self.businessName.text&&self.businessName.text.length>0&&self.industryParser.name&&self.trueName.text&&self.trueName.text.length>0&&self.identityNumber.text&&self.identityNumber.text.length>0&&self.behindImage&&self.fontImage&&self.licenseImage&&self.detailAddress.text&&self.detailAddress.text.length>0&&self.areaArray.count==4) {
        //上传图片数组
        NSMutableArray *imageArray = [NSMutableArray array];
        if (self.iconImage&&self.behindImage&&self.fontImage&&self.licenseImage) {
            [imageArray addObject:self.iconImage];
            [imageArray addObject:self.fontImage];
            [imageArray addObject:self.behindImage];
            [imageArray addObject:self.licenseImage];
            if (self.codeImage) {
                [imageArray addObject:self.codeImage];
            }
            if (imageArray.count>3) {
                NSDictionary *param=@{@"type":@"idcard",@"formname":@"file"};
                
                [XSTool showProgressHUDTOView:self.view withText:@"上传中"];
                [HTTPManager upLoadPhotosWithDic:param andDataArray:imageArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                    if ([dic[@"status"] integerValue]==1) {
                        NSArray *tempArray = dic[@"data"];
                        if (tempArray.count==imageArray.count) {
                            
                            //上传信息
                            NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                                         @"agreement":self.agreementButton.selected?@"0":@"1",
                                                                                                         @"name":self.businessName.text,
                                                                                                         @"truename":self.trueName.text,
                                                                                                         @"industry_id":self.industryParser.ID,
                                                                                                         @"logo":[NSString stringWithFormat:@"%@",[tempArray firstObject]],
                                                                                                         @"card_no":self.identityNumber.text,
                                                                                                         @"img_card":[NSString stringWithFormat:@"%@,%@",tempArray[1],tempArray[2]],
                                                                                                         @"img_license":[NSString stringWithFormat:@"%@",tempArray[3]],
                                                                                                         @"province":[[self.areaArray firstObject] firstObject],
                                                                                                         @"city":[self.areaArray[1] firstObject],
                                                                                                         @"county":[self.areaArray[2] firstObject],
                                                                                                         @"town":[self.areaArray[3] firstObject],
                                                                                                         @"province_code":[self.areaArray[0] lastObject],
                                                                                                         @"city_code":[self.areaArray[1] lastObject],
                                                                                                         @"county_code":[self.areaArray[2] lastObject],
                                                                                                         @"town_code":[self.areaArray[3] lastObject],
                                                                                                         @"detail":self.detailAddress.text
                                                                                                         }];
                            if (tempArray.count==5) {
                                [param setObject:[NSString stringWithFormat:@"%@",tempArray[4]] forKey:@"img_zuzhi"];
                            }
                            [HTTPManager supplyRegisterWithParam:param success:^(NSDictionary *dic, resultObject *state) {
                                [XSTool hideProgressHUDWithView:self.view];
                                if (state.status) {
                                    BusinessMainViewController *controller = (BusinessMainViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                                    [HTTPManager getSupplyRegisterEnterInfoSuccess:^(NSDictionary *dic, resultObject *state) {
                                        if (state.status) {
                                            controller.businessParser = [BusinessRegisterParser mj_objectWithKeyValues:dic[@"data"]];
                                            [self.navigationController popToViewController:controller animated:YES];
                                        } else {
                                            [XSTool showToastWithView:self.view Text:state.info];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }
                                    } fail:^(NSError *error) {
                                        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                                    }];
                                    
                                    
                                } else {
                                    [XSTool showToastWithView:self.view Text:state.info];
                                    
                                }
                            } fail:^(NSError *error) {
                                [XSTool hideProgressHUDWithView:self.view];
                                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                            }];
                        }
                        
                    } else {
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                } fail:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                    
                }];
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:@"请完善信息"];
        }
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    }
    
    
}

- (void)choosePhotoes {
    [self.view endEditing:YES];
    self.alert =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //打开相册
        self.imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            //显示视频
            //            self.imagePickVC.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePickVC.sourceType];
        }
        self.imagePickVC.delegate = self;
        self.imagePickVC.allowsEditing = NO;
        [self presentViewController:self.imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [self.alert addAction:photograph];
    [self.alert addAction:select];
    [self.alert addAction:cancel];
    [self presentViewController:self.alert animated:YES completion:nil];
}

- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        //后置摄像头是否可用
        if (!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
        
    }];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    TOCropViewController *toVC;
    if (self.takePhotoflag) {
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        toVC=[[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
        toVC.delegate=self;
        [picker presentViewController:toVC animated:NO completion:nil];
    }else{
        
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"])
        {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            
            if (self.choosePhotoType==ChoosePhotoTypeIcon) {
                toVC = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:image];
            } else {
                toVC = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:image];
            }
            toVC.delegate=self;
            [picker pushViewController:toVC animated:YES];
        }
    }
}
#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.takePhotoflag) {
        //        [self.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
            //            XSLog(@"retain  count = %ld\n",CFGetRetainCount((__bridge  CFTypeRef)(weakSelf.imagePickerVC)));
            [weakSelf.imagePickVC dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = image;
    
    //压缩0.8
    NSData *imageData=UIImageJPEGRepresentation(image, 0.8);
    if (imageData.length>1024*1024*3) {//3M以及以上
        [self dismissViewControllerAnimated:YES completion:^{
            [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
        }];
        
        
    } else {
        NSData *imageData=UIImageJPEGRepresentation(image, 0.4);
        UIImage *newImage = [UIImage imageWithData:imageData];
        [self dismissViewControllerAnimated:YES completion:^{
            NSIndexPath *index;
            switch (self.choosePhotoType) {
                case ChoosePhotoTypeIcon: {
                    self.iconImage = newImage;
                    index = [NSIndexPath indexPathForRow:0 inSection:0];
                }
                    break;
                case ChoosePhotoTypeFontIdentity: {
                    
                    self.fontImage = newImage;
                    index = [NSIndexPath indexPathForRow:0 inSection:2];
                    
                }
                    break;
                case ChoosePhotoTypeBehindIdentity: {
                    self.behindImage = newImage;
                    index = [NSIndexPath indexPathForRow:1 inSection:2];
                }
                    break;
                case ChoosePhotoTypeLicense: {
                    self.licenseImage = newImage;
                    index = [NSIndexPath indexPathForRow:0 inSection:4];
                }
                    break;
                case ChoosePhotoTypeCode: {
                    self.codeImage = newImage;
                    index = [NSIndexPath indexPathForRow:0 inSection:5];
                }
                    break;
            }
            
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }];
        
    }
    
}

- (void)selectCity:(UITapGestureRecognizer *) tap {
    ProvinceViewController *province = [[ProvinceViewController alloc] init];
    [self.navigationController pushViewController:province animated:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    //    NSLog(@"%@,%@",rectInSuperview ,rectInTableView);
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - TradeViewDelegate
- (void)selectedTradeWithTradeParser:(BusinessApplyIndustryParser *)industryParser {
    [self.tradeView removeFromSuperview];
    self.industryParser = industryParser;
    NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)closeTradeView {
    [self.tradeView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
