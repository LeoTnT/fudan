//
//  BusinessMainViewController.h
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@interface BusinessMainViewController : XSBaseTableViewController
@property (nonatomic, strong) BusinessRegisterParser *businessParser;
@end
