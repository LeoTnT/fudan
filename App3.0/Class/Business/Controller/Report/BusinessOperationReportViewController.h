//
//  BusinessOperationReportViewController.h
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessOperationReportViewController : XSBaseTableViewController
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *name;
@end
