//
//  BusinessOperationReportViewController.m
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessOperationReportViewController.h"
#import "ReportTopTableViewCell.h"
#import "ReportBottomTableViewCell.h"
#import "ReportCenterTableViewCell.h"
#import "BusinessModel.h"

@interface BusinessOperationReportViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) BusinessSupplyReportParser *reportParser;

@end

@implementation BusinessOperationReportViewController

#pragma mark - lazy loadding

#pragma makr - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"运营报表";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [self getReportInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getReportInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getReportSupplyReportSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.reportParser = [BusinessSupplyReportParser mj_objectWithKeyValues:dic[@"data"]];
            [self.tableView reloadData]; 
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"reportTopCell";
    if (indexPath.row==0) {
        ReportTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[ReportTopTableViewCell alloc]  initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        cell.iconUrl = self.iconUrl;
        cell.name = self.name;
        return cell;
    } else if (indexPath.row==1) {
        idString = @"reportCenterCell";
        ReportCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[ReportCenterTableViewCell alloc]  initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.reportParser) {
//             cell.itemArray = @[self.reportParser.income_today,self.reportParser.order_today,self.reportParser.browse_product_today,self.reportParser.favorite_today,self.reportParser.cart_today,@"0",self.reportParser.wait_pay,self.reportParser.wait_send,self.reportParser.income_yestoday,self.reportParser.income_today];
            cell.itemArray = @[self.reportParser.income_today,self.reportParser.order_today,self.reportParser.browse_product_today,self.reportParser.favorite_today,self.reportParser.cart_today,self.reportParser.wait_pay,self.reportParser.wait_send,self.reportParser.income_yestoday,self.reportParser.income_today];
        }
        return cell;
    } else {
        idString = @"reportBottomCell";
        ReportBottomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[ReportBottomTableViewCell alloc]  initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.reportParser) {
             cell.incomeWeekDictionary = self.reportParser.income_week;
        }
       
        return cell;
        
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        return 150;
    } else if (indexPath.row==1) {
        return 452;
    } else {
        return 180;
    }
}

@end
