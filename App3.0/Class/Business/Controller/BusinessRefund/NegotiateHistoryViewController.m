
//
//  NegotiateHistoryViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "NegotiateHistoryViewController.h"
#import "BusinessRefundModel.h"
#import "NegotiateHistoryTableViewCell.h"

@interface NegotiateHistoryViewController ()

@property (nonatomic, strong) NSMutableArray *historyArray;

@end

@implementation NegotiateHistoryViewController
#pragma mark - lazyloadding
-(NSMutableArray *)historyArray {
    if (!_historyArray) {
        _historyArray = [NSMutableArray array];
    }
    return _historyArray;
}

#pragma mark - life
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"协商历史";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    [self getHistoryList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - private
- (void) getHistoryList {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundLogsWithParam:@{@"refund_id": self.refundId} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        
        if (state.status) {
            BusinessRefundNegotiateHistoryListParser *parser = [BusinessRefundNegotiateHistoryListParser mj_objectWithKeyValues:dic];
            [self.historyArray addObjectsFromArray:parser.data];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

#pragma mark - tableview

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"NegotiateHistoryTableViewCell";
    NegotiateHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[NegotiateHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        
    }
    if (self.historyArray.count>indexPath.row) {
        cell.dataParser = self.historyArray[indexPath.section];
    }
    
    return cell;
    

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.historyArray.count;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 12;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NegotiateHistoryTableViewCell *cell = (NegotiateHistoryTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;

}

@end
