//
//  RejectReundViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RejectReundViewController.h"
#import "BusinessRefundModel.h"
#import "RefundAddPhotoTableViewCell.h"
#import "PopUpView.h"
#import "ZYQAssetPickerController.h"
#import "BusinessRefundDetailViewController.h"
#import "RejectRefundReasonView.h"


@interface RejectReundViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,PopViewDelegate,ZYQAssetPickerControllerDelegate,RejectRefundReasonViewDelegate>

@property(nonatomic,strong) UIImagePickerController *imagePickViewController;//拍照控制器
@property(nonatomic,assign) BOOL takePhotoFlag;//是否是现拍的照片
@property (nonatomic, strong) NSMutableArray *reasonArray;
@property (nonatomic, strong) UITextField *reasonText;
@property (nonatomic, strong) UILabel *reasonLabel;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) PopUpView *sheetView;//弹出的view
@property (nonatomic ,strong) BusinessRefundRejectReasonDataParser *resonParser;
@property (nonatomic, strong) RejectRefundReasonView *typeView;
@end

@implementation RejectReundViewController
#pragma mark - lazyloadding
- (RejectRefundReasonView *)typeView {
    if (!_typeView) {
        _typeView = [[RejectRefundReasonView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.typeDelegate = self;
    }
    return _typeView;
}
- (NSMutableArray *)reasonArray {
    if (!_reasonArray) {
        _reasonArray = [NSMutableArray array];
    }
    return _reasonArray;
}

- (NSMutableArray *)photoArray {
    if (!_photoArray) {
        
        _photoArray = [NSMutableArray arrayWithObject:[UIImage imageNamed:@"eva_add"]];
    }
    return _photoArray;
}

-(UITextField *)reasonText {
    if (!_reasonText) {
        _reasonText = [[UITextField alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-13, 44)];
        _reasonText.placeholder = @"请输入拒绝说明";
        _reasonText.font = [UIFont systemFontOfSize:15];
        _reasonText.delegate = self;
        _reasonText.textAlignment = NSTextAlignmentRight;
    }
    return _reasonText;
}

-(UILabel *)reasonLabel {
    if (!_reasonLabel) {
        _reasonLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-30, 44)];
        _reasonLabel.textAlignment = NSTextAlignmentRight;
        _reasonLabel.font = [UIFont systemFontOfSize:14];
        _reasonLabel.textColor = COLOR_666666;
    }
    return _reasonLabel;
}

-(UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(11.5, 44, mainWidth-11.5-12.5, 50)];
        [_submitButton setTitle:Localized(@"bug_submit_do") forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitButton.backgroundColor = mainColor;
        _submitButton.layer.cornerRadius = 4;
        _submitButton.layer.masksToBounds = YES;
        [_submitButton addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - life
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"拒绝退款";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    
    [self getReason];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - rejectrefunddelegate
- (void)hiddenCardView:(BusinessRefundRejectReasonDataParser *)dataParser {
    if (dataParser) {
        if (!isEmptyString(dataParser.ID)) {
            self.resonParser = dataParser;
        }
    }
    //消失
    [self.typeView removeFromSuperview];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark -  PopViewDelegate
- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}

#pragma  mark - ZYQAssetPickerControllerDelegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            //压缩0.8
            UIImage *newImage;
            NSData *imageData=UIImageJPEGRepresentation(result, 0.8);
            if (imageData.length>1024*1024*3) {//3M以及以上
                //                [self dismissViewControllerAnimated:YES completion:^{
                //                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
                //                }];
                NSData *imageData=UIImageJPEGRepresentation(result, 0.2);
                newImage = [UIImage imageWithData:imageData];
                
            } else {
                imageData=UIImageJPEGRepresentation(result, 0.4);
                newImage = [UIImage imageWithData:imageData];
                
            }
            
            if (self.photoArray.count<4) {
                [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
                NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
                [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择三张图片"];
}
#pragma mark - private
- (void) getReason {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundGetRefundReasonWithParam:@{@"step":@"refuse_refund"} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessRefundRejectReasonParser *parser = [BusinessRefundRejectReasonParser mj_objectWithKeyValues:dic];
            [self.reasonArray  setArray:parser.data];
            //            self.resonParser = [self.reasonArray firstObject];
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void) submitAction {
    
    if (self.resonParser.ID&&(!isEmptyString(self.reasonText.text))) {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObject:self.resonParser.ID forKey:@"reason"];
        [param setObject:self.refundId forKey:@"refund_id"];
        [param setObject:self.reasonText.text forKey:@"remark"];
        
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.photoArray];
        [tempArray removeLastObject];
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        if (tempArray.count) {
            NSDictionary *photoParam=@{@"type":@"reject",@"formname":@"file"};
            [HTTPManager upLoadPhotosWithDic:photoParam andDataArray:tempArray
                                 WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                                     if ([dic[@"status"] integerValue]==1) {
                                         NSMutableArray *imgs = [NSMutableArray arrayWithArray:dic[@"data"]];
                                         
                                         NSError *error = nil;
                                         NSData *jsonData = [NSJSONSerialization dataWithJSONObject:imgs
                                                                                            options:kNilOptions
                                                                                              error:&error];
                                         NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                                                      encoding:NSUTF8StringEncoding];
                                         [param setObject: jsonString forKey:@"pics"];
                                         [self submitInfoWithParam:param];
                                     } else {
                                         @strongify(self);
                                         [XSTool hideProgressHUDWithView:self.view];
                                         [XSTool showToastWithView:self.view Text:dic[@"info"]];
                                     }
                                     
                                     
                                 } fail:^(NSError * _Nonnull error) {
                                     @strongify(self);
                                     [XSTool hideProgressHUDWithView:self.view];
                                     [XSTool showToastWithView:self.view Text:NetFailure];
                                 }];
            
        } else {
            [self submitInfoWithParam:param];
        }
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息！"];
    }
    
    
}

- (void) submitInfoWithParam:(NSDictionary *) param{
    if (self.rejectRefundType==RejectReundTypeReturn) {
        [HTTPManager refundRefuseConfirmWithParam:param success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BusinessRefundDetailViewController *controller = (BusinessRefundDetailViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                controller.isChange = YES;
                [controller getDetailRefund];
                
                [self.navigationController popToViewController:controller animated:YES];
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        
        [HTTPManager refundRefuseRefundWithParam:param success:^(NSDictionary *dic, resultObject *state) {
            
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BusinessRefundDetailViewController *controller = (BusinessRefundDetailViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                controller.isChange = YES;
                [controller getDetailRefund];
                
                [self.navigationController popToViewController:controller animated:YES];
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
    
    
}

- (void)openPhotoLibrary {
    [self clickToCancelLink];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
    picker.maximumNumberOfSelection = 3;
    picker.assetsFilter = ZYQAssetsFilterAllAssets;
    picker.showEmptyGroups=NO;
    picker.delegate=self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)choosePhotos:(UITapGestureRecognizer *) tap {
    [self.view endEditing:YES];
    if (self.photoArray.count>4) {
        [XSTool showToastWithView:self.view Text:@"最多上传3张！"];
        return;
    }
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypePhoto;
    [self.sheetView.oneBtn setTitle:Localized(@"拍照") forState:UIControlStateNormal];
    [self.sheetView.oneBtn addTarget:self action:@selector(takeAPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.sheetView.otherBtn setTitle:Localized(@"从相册中选择") forState:UIControlStateNormal];
    [self.sheetView.otherBtn addTarget:self action:@selector(openPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView ] ;
    
}

- (void)endEditing{
    [self.view endEditing:YES];
}

- (void)deletePicture:(UIButton *)button {
    RefundAddPhotoTableViewCell *cell= (RefundAddPhotoTableViewCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    NSMutableArray *tempArray = self.photoArray;
    [tempArray removeObjectAtIndex:index];
    [self.photoArray setArray:tempArray];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)takeAPhoto {
    [self clickToCancelLink];
    self.imagePickViewController = [[UIImagePickerController alloc] init];
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action1];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if (!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickViewController.showsCameraControls = YES;
    
    //摄像头捕获模式
    self.imagePickViewController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickViewController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    self.imagePickViewController.delegate = self;
    [self presentViewController:self.imagePickViewController animated:YES completion:^{
        self.takePhotoFlag = YES;
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    //压缩0.8
    UIImage *newImage;
    NSData *imageData=UIImageJPEGRepresentation(image, 0.8);
    if (imageData.length>1024*1024*3) {//3M以及以上
        //                [self dismissViewControllerAnimated:YES completion:^{
        //                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
        //                }];
        NSData *imageData=UIImageJPEGRepresentation(image, 0.2);
        newImage = [UIImage imageWithData:imageData];
        
    } else {
        imageData=UIImageJPEGRepresentation(image, 0.4);
        newImage = [UIImage imageWithData:imageData];
        
    }
    if (self.photoArray.count<4) {
        [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void) displayRejectReaonListView {
    if (self.resonParser.ID) {
        self.typeView.selectedId = self.resonParser.ID;
    }
    self.typeView.reasonArray = self.reasonArray;
    [[[ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView] ;
}

#pragma mark - tableview

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0&&indexPath.row==0) {
        [self displayRejectReaonListView];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"normal";
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor whiteColor];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            NSString *titleString = @"*拒绝原因";
            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:titleString];
            [attString addAttribute:NSForegroundColorAttributeName
                              value:[UIColor redColor]
                              range:NSMakeRange(0, 1)];
            cell.textLabel.attributedText = attString;
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            NSString *reasonString = @"请选择拒绝原因";
            if (self.resonParser) {
                if (isEmptyString(self.resonParser.ID)) {
                    reasonString = @"请选择拒绝原因";
                } else {
                    reasonString = self.resonParser.item;
                }
            }
            self.reasonLabel.text = reasonString;
            
            [cell.contentView addSubview:self.reasonLabel];
            return cell;
        } else {
            idString = @"reason";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor whiteColor];
            }
            NSString *titleString = @"*拒绝说明";
            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:titleString];
            [attString addAttribute:NSForegroundColorAttributeName
                              value:[UIColor redColor]
                              range:NSMakeRange(0, 1)];
            cell.textLabel.attributedText = attString;
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            [cell.contentView addSubview:self.reasonText];
            return cell;
        }
    } else if (indexPath.section==1) {
        idString = @"RefundAddPhotoTableViewCell";
        RefundAddPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[RefundAddPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        
        if (self.photoArray.count<5) {
            cell.photosArray = self.photoArray;
            if (self.photoArray.count==4) {
                [XSTool showToastWithView:self.view Text:@"最多上传3张!"];
            }
        }
        //给cell的删除按钮绑定方法
        for (int i=0; i<cell.deletBtnArray.count; i++) {
            [[cell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (self.photoArray.count>3) {
            
        } else {
            [cell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos:)]];
        }
        
        //绑定手势  收起键盘
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)]];
        return cell;
        
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = BG_COLOR;
            
        }
        [cell.contentView addSubview:self.submitButton];
        return cell;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 2;
    } else {
        
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 12;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        return 44;
    } else if (indexPath.section==1) {
        RefundAddPhotoTableViewCell *cell = (RefundAddPhotoTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        return cell.height;
    } else {
        return 44+50;
    }
}


@end
