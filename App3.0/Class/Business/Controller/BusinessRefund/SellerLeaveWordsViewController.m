//
//  SellerLeaveWordsViewController.m
//  App3.0
//
//  Created by nilin on 2018/1/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "SellerLeaveWordsViewController.h"
#import "ZYQAssetPickerController.h"
#import "OrderEvaTopTableViewCell.h"
#import "AddPhotosTableViewCell.h"
#import "RefundAddPhotoTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
@interface SellerLeaveWordsViewController ()<UITextViewDelegate,ZYQAssetPickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITextView *remark;
@property (nonatomic, strong) UILabel *hiddenLabel;
@property (nonatomic, strong) UILabel *tintLabel;
@property (nonatomic, strong) XSCustomButton *submitEvaBtn;//提交
@property (nonatomic, assign) CGFloat photoCellHeight;//图片的cell的高度
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, assign) CGFloat remarkHeight;
@property (nonatomic, assign) CGFloat normalHeight;
@end

@implementation SellerLeaveWordsViewController
#pragma mark - lazy loadding
- (NSMutableArray *)imageArray {
    if (_imageArray==nil) {
        _imageArray = [NSMutableArray array];
    }
    return _imageArray;
}
- (UITextView *)remark {
    if (!_remark) {
        _remark = [[UITextView alloc] init];
        _remark.delegate = self;
        _remark.returnKeyType = UIReturnKeyDone;
        _remark.tintColor = mainGrayColor;
        _remark.autocorrectionType = UITextAutocorrectionTypeNo;
        _remark.font = [UIFont systemFontOfSize:16];
        
    }
    return _remark;
}
- (UILabel *)hiddenLabel {
    if (!_hiddenLabel) {
        _hiddenLabel = [UILabel new];
        _hiddenLabel.text = @"请输入留言内容";
        _hiddenLabel.font = [UIFont systemFontOfSize:16];
        _hiddenLabel.textColor = LINE_COLOR;
    }
    return _hiddenLabel;
    
}
- (UILabel *)tintLabel {
    if (!_tintLabel) {
        _tintLabel = [UILabel new];
        _tintLabel.backgroundColor = [UIColor whiteColor];
        _tintLabel.text = @"还可以输入200个字";
        _tintLabel.textColor = LINE_COLOR;
        _tintLabel.textAlignment = NSTextAlignmentRight;
        _tintLabel.font = [UIFont systemFontOfSize:14];
    }
    return _tintLabel;
}
- (XSCustomButton *)submitEvaBtn {
    
    if (!_submitEvaBtn) {
        _submitEvaBtn = [XSCustomButton new];
        [_submitEvaBtn setBorderWith:0 borderColor:0 cornerRadius:5];
        [_submitEvaBtn addTarget:self action:@selector(submitEvaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitEvaBtn;
}
#pragma mark - life circle
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.remarkHeight = 120;
    self.normalHeight = 50;
    self.navigationItem.title = @"给买家留言";
    self.view.backgroundColor = BG_COLOR;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self addSubViews];
    self.photoArray=[NSMutableArray arrayWithObject:[UIImage imageNamed:@"user_fans_addphoto_thin"]];
}
#pragma mark - private
- (void)addSubViews {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}


- (void)submitEvaAction:(UIButton *) sender {
    if (isEmptyString(self.remark.text)) {
        [XSTool showToastWithView:self.view Text:@"请完善留言"];
    } else {
        NSMutableDictionary *paramDictionary = [NSMutableDictionary dictionary];
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *remarkString = [[NSString alloc]initWithString:[self.remark.text stringByTrimmingCharactersInSet:whiteSpace]];
        [paramDictionary setObject:self.refundId forKey:@"refund_no"];
        [paramDictionary setObject:remarkString forKey:@"remark"];
        
        NSMutableArray *tempPhotoArray = [NSMutableArray arrayWithArray:self.photoArray];
        [tempPhotoArray removeLastObject];
        if (tempPhotoArray.count==0) {
            [self vertifySubmitWithParam:paramDictionary];
        } else {
            NSDictionary *param=@{@"type":@"evaluation",@"formname":@"file"};
            [XSTool showProgressHUDWithView:self.view];
            @weakify(self);
            [HTTPManager upLoadPhotosWithDic:param andDataArray:tempPhotoArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                @strongify(self);
                if([dic[@"status"] integerValue]==1){
                    
                    NSMutableArray *imgs = [NSMutableArray arrayWithArray:dic[@"data"]];
                    
                    NSError *error = nil;
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:imgs
                                                                       options:kNilOptions
                                                                         error:&error];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                                 encoding:NSUTF8StringEncoding];
                    [paramDictionary setObject: jsonString forKey:@"pics"];
                    
                    
                    //提交
                    [self vertifySubmitWithParam:paramDictionary];
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * _Nonnull error) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
    }
    
}

- (void)vertifySubmitWithParam:(NSDictionary *) paramDictionary {
    [HTTPManager refundGuestbookWithParam:paramDictionary success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"成功留言"];
            [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
    
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate
- (void) textViewDidChange:(UITextView*)textView {
    if ([textView.text length] == 0) {
        self.hiddenLabel.hidden = NO;
    } else {
        self.hiddenLabel.hidden = YES;
        
    }
    if (textView.markedTextRange == nil) {
        if ([textView.text length] == 0) {
            self.tintLabel.text = @"还可以输入200个字";
            
        } else {
            self.tintLabel.text = @"";//这里给空
            
        }
        NSString *nsTextCotent = textView.text;
        NSUInteger existTextNum = [nsTextCotent length];
        NSUInteger remainTextNum = 200 - existTextNum;
        if ((int )remainTextNum<0) {
            remainTextNum = 0;
            self.remark.text = [self.remark.text substringToIndex:200];
        }
        self.tintLabel.text = [NSString stringWithFormat:@"还可以输入%lu个字",remainTextNum];
    }
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        //这里"\n"对应的是键盘的 return 回收键盘之用
        
        [textView resignFirstResponder];
        
        return YES;
        
    }
    
    if (range.location >= 200) {
        
        return  NO;
    } else {
        
        return YES;
    }
    
}

#pragma mark-选择照片
- (void)choosePhotos {
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //打开相册
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 3;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-拍照完毕
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    //保存图片到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    if (self.photoArray.count<4) {
        [self.photoArray insertObject:image atIndex:self.photoArray.count-1];
        [self dismissViewControllerAnimated:YES completion:nil];
        //刷新表格
        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
    }
}

#pragma mark - ZYQAssetPickerController Delegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            //压缩0.8
            UIImage *newImage;
            NSData *imageData=UIImageJPEGRepresentation(result, 0.8);
            if (imageData.length>1024*1024*3) {//3M以及以上
                //                [self dismissViewControllerAnimated:YES completion:^{
                //                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
                //                }];
                NSData *imageData=UIImageJPEGRepresentation(result, 0.2);
                newImage = [UIImage imageWithData:imageData];
                
            } else {
                imageData=UIImageJPEGRepresentation(result, 0.4);
                newImage = [UIImage imageWithData:imageData];
                
            }
            if (self.photoArray.count<4) {
                [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
                NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            }
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择3张图片"];
}

#pragma mark-删除图片
- (void)deletePicture:(UIButton *)button {
    AddPhotosTableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    [self.photoArray removeObjectAtIndex:index];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark-拍照
- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"nCell"];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        line.backgroundColor = BG_COLOR;
        [cell.contentView addSubview:line];
        self.remark.frame = CGRectMake(0,CGRectGetMaxY(line.frame) , mainWidth, self.remarkHeight-10);
        self.hiddenLabel.frame = CGRectMake(NORMOL_SPACE, CGRectGetMinY(self.remark.frame)+NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, NORMOL_SPACE);
        self.tintLabel.frame = CGRectMake(0, CGRectGetMaxY(self.remark.frame), mainWidth-NORMOL_SPACE, NORMOL_SPACE*2);
        [cell.contentView addSubview:self.remark];
        [cell.contentView addSubview:self.hiddenLabel];
        
        [cell.contentView addSubview:self.tintLabel];
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tintLabel.frame)+NORMOL_SPACE, mainWidth, 1)];
        line2.backgroundColor = LINE_COLOR_NORMAL;
        [cell.contentView addSubview:line2];
        return cell;
    } else if (indexPath.row==1) {
        NSString *str=@"topCell";
        RefundAddPhotoTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:str];
        if (!cell) {
            cell=[[RefundAddPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        if (self.photoArray.count<5) {
            cell.photosArray=self.photoArray;
            if (self.photoArray.count==4) {
                [XSTool showToastWithView:self.view Text:@"最多上传3张!"];
            }
        }
        
        
        //给cell的删除按钮绑定方法
        for (int i=0; i<cell.deletBtnArray.count; i++) {
            [[cell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (self.photoArray.count>3) {
            
        } else {
            [cell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos)]];
            
        }
        
        //绑定手势  收起键盘
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)]];
        self.photoCellHeight=cell.height;
        return cell;
    } else  {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"subCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat submitHeight = 40;
        self.submitEvaBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 10, mainWidth-2*NORMOL_SPACE, submitHeight) title:Localized(@"bug_submit_do") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.submitEvaBtn setBorderWith:0 borderColor:0 cornerRadius:5];
        [self.submitEvaBtn addTarget:self action:@selector(submitEvaAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = BG_COLOR;
        [cell.contentView addSubview:self.submitEvaBtn];
        return cell;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        return self.remarkHeight+5*NORMOL_SPACE;
    } else if (indexPath.row==1) {
        return self.photoCellHeight;
    } else {
        return 50;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
