//
//  AgreeRefundGooodsAndMoneyViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AgreeRefundGooodsAndMoneyViewController.h"
#import "BusinessRefundDetailViewController.h"


@interface AgreeRefundGooodsAndMoneyViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) NSArray *itemArray;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) UITextField *receiverText;
@property (nonatomic, strong) UITextField *phoneText;
@property (nonatomic, strong) UITextField *addressText;
@property (nonatomic, strong) UITextField *descText;

@end

@implementation AgreeRefundGooodsAndMoneyViewController
#pragma mark - lazyloadding
-(UITextField *)receiverText {
    if (!_receiverText) {
        _receiverText = [[UITextField alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-13, 44)];
       
        _receiverText.font = [UIFont systemFontOfSize:15];
        _receiverText.delegate = self;
        _receiverText.textAlignment = NSTextAlignmentRight;
    }
    return _receiverText;
}

-(UITextField *)phoneText {
    if (!_phoneText) {
        _phoneText = [[UITextField alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-13, 44)];
        _phoneText.font = [UIFont systemFontOfSize:15];
        _phoneText.delegate = self;
        _phoneText.textAlignment = NSTextAlignmentRight;
    }
    return _phoneText;
}

-(UITextField *)addressText {
    if (!_addressText) {
        _addressText = [[UITextField alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-13, 44)];
        _addressText.font = [UIFont systemFontOfSize:15];
        _addressText.delegate = self;
        _addressText.textAlignment = NSTextAlignmentRight;
    }
    return _addressText;
}

-(UITextField *)descText {
    if (!_descText) {
       _descText = [[UITextField alloc] initWithFrame:CGRectMake(120, 0, mainWidth-120-13, 44)];
        _descText.font = [UIFont systemFontOfSize:15];
        _descText.delegate = self;
        _descText.textAlignment = NSTextAlignmentRight;
    }
    return _descText;
}

-(UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(11.5, 44, mainWidth-11.5-12.5, 50)];
        [_submitButton setTitle:Localized(@"bug_submit_do") forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitButton.backgroundColor = mainColor;
        _submitButton.layer.cornerRadius = 4;
        _submitButton.layer.masksToBounds = YES;
        [_submitButton addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - life
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"同意退货";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    self.itemArray = @[@[@[@"*收货人",@"请输入收货人姓名"],@[@"*手机号",@"请输入收货人手机号"],@[@"*退货地址",@"请输入收货地址"]],@[@[@"*退货说明",@"请描述退货原因"]]];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)submitAction {
    [self.view endEditing:YES];
    if (isEmptyString(self.receiverText.text)) {
        [XSTool showToastWithView:self.view Text:@"请输入收货人姓名"];
        return;
    }
    if (isEmptyString(self.phoneText.text)) {
        [XSTool showToastWithView:self.view Text:@"请输入收货人手机号"];
        return;
    }
    if (isEmptyString(self.addressText.text)) {
        [XSTool showToastWithView:self.view Text:@"请输入收货地址"];
        return;
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.refundId forKey:@"refund_id"];
    [param setObject:self.receiverText.text forKey:@"name"];
    [param setObject:self.phoneText.text forKey:@"mobile"];
    [param setObject:self.addressText.text forKey:@"address"];
    if (!isEmptyString(self.descText.text)) {
       [param setObject:self.descText.text forKey:@"remark"];
    }
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundAgreeReturnRefundWithParam:param success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self popLastController];
            
        } else {
            
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}


- (void) popLastController {
    
    BusinessRefundDetailViewController *controller = (BusinessRefundDetailViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    controller.isChange = YES;
    [controller getDetailRefund];
    [self.navigationController popToViewController:controller animated:YES];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"normal";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:15];
   
    if (indexPath.section==0) {
        NSString *titleString = [self.itemArray[indexPath.section][indexPath.row] firstObject];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:titleString];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor redColor]
                          range:NSMakeRange(0, 1)];
        cell.textLabel.attributedText = attString;
        if (indexPath.row==0) {
            self.receiverText.placeholder = [self.itemArray[indexPath.section][indexPath.row] lastObject];
            [cell.contentView addSubview:self.receiverText];
        } else if (indexPath.row==1) {
            self.phoneText.placeholder = [self.itemArray[indexPath.section][indexPath.row] lastObject];
            [cell.contentView addSubview:self.phoneText];
            
        } else {
            self.addressText.placeholder = [self.itemArray[indexPath.section][indexPath.row] lastObject];
            [cell.contentView addSubview:self.addressText];
        }
        return cell;
    } else {
        if (indexPath.row==0) {
//            NSString *titleString = [self.itemArray[indexPath.section][indexPath.row] firstObject];
//            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:titleString];
//            [attString addAttribute:NSForegroundColorAttributeName
//                              value:[UIColor redColor]
//                              range:NSMakeRange(0, 1)];
//            cell.textLabel.attributedText = attString;
            cell.textLabel.text = [self.itemArray[indexPath.section][indexPath.row] firstObject];
            self.descText.placeholder = [self.itemArray[indexPath.section][indexPath.row] lastObject];
            [cell.contentView addSubview:self.descText];
            return cell;
        } else {
            idString = @"buttonCell";
            UITableViewCell *buttonCell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (buttonCell==nil) {
                buttonCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                buttonCell.selectionStyle = UITableViewCellSelectionStyleNone;
                buttonCell.backgroundColor = BG_COLOR;
            }
            
            [buttonCell.contentView addSubview:self.submitButton];
            return buttonCell;
        }
       
        
    }
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 3;
    } else {
        
        return 2;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        return 44;
    } else {
        if (indexPath.row==0) {
            return 44;
        } else {
           return 100;
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 12;
}
@end
