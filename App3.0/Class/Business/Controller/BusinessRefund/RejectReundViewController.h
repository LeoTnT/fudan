//
//  RejectReundViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,RejectReundType) {
    RejectReundTypeNormal,
    RejectReundTypeReturn,//买家已发货，卖家拒绝退款
    
};

@interface RejectReundViewController : XSBaseTableViewController

@property (nonatomic, copy) NSString *refundId;
@property (nonatomic, assign) RejectReundType rejectRefundType;
@end
