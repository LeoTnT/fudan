//
//  BusinessRefundDetailViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface BusinessRefundDetailViewController : XSBaseTableViewController


@property (nonatomic, copy) NSString *refundId;

/**是否改变状态*/
@property (nonatomic, assign) BOOL isChange;

- (void)getDetailRefund;
@end
