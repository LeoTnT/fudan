//
//  BusinessRefundFormListViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"


typedef NS_ENUM(NSInteger, BusinessRefundType) {
    BusinessRefundTypeWaitSellerCheck,//待卖家审核
    BusinessRefundTypeAll,//全部
    BusinessRefundTypeFinished,//已完成
    BusinessRefundTypeWaitBuyerDeliver,//待买家发货
    BusinessRefundTypeRejectReceiving,//已拒绝收货
    BusinessRefundTypeRejectRefund,//已拒绝退款
    BusinessRefundTypeWaitDeliver,//待收货
    BusinessRefundTypeWaitSubmitVoucher,//待提交凭证
    BusinessRefundTypeWaitKefu,//客服处理中
    BusinessRefundTypeClose,//退款关闭
    BusinessRefundTypeSuccess,//退款成功
    
};

@interface BusinessRefundFormListViewController : XSBaseTableViewController
@property (nonatomic, assign) BusinessRefundType businessRefundType;

@end
