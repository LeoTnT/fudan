//
//  BusinessRefundFormListViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessRefundFormListViewController.h"
#import "BusinessRefundListTableViewCell.h"
#import "RefundListSearchView.h"
#import "BusinessRefundDetailViewController.h"
#import "NegotiateHistoryViewController.h"
#import "ChatViewController.h"



@interface BusinessRefundFormListViewController ()<RefundListSearchViewDelegate,RefundDelegate>
@property (nonatomic, strong) ChatViewController *chatVC;
@property (nonatomic, strong) NSMutableArray *listArray;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, strong) DefaultView *defaultView;
@property (nonatomic, copy)  NSString *refundStatus;//状态
@property (nonatomic, strong) RefundListSearchView *searchView;
@property (nonatomic, strong) NSMutableDictionary *searchDictionary;
@property (nonatomic, strong) NSMutableDictionary *paramDictionary;
@end

@implementation BusinessRefundFormListViewController

#pragma mark - lazyloadding
-(NSMutableDictionary *)paramDictionary {
    if (!_paramDictionary) {
        _paramDictionary = [NSMutableDictionary dictionary];
    }
    return _paramDictionary;

}

-(NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
    
}
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, navBarHeight+StatusBarHeight, mainWidth, mainHeight-(navBarHeight+StatusBarHeight))];
        _defaultView.titleLabel.text = @"暂无订单！";
        _defaultView.button.hidden = YES;
        
    }
    return _defaultView;
}
#pragma mark - life  circle
//-(void)viewWillAppear:(BOOL)animated {
//    
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.limit = @"8";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    [self actionCustomRightBtnWithNrlImage:@"business_search_ funnel" htlImage:nil title:nil action:^{
         @strongify(self);
        
        //筛选
        self.searchView = [[RefundListSearchView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        self.searchView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:self.searchView];

        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    
    //上下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
       [self getRefundOrderList];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getMoreRefundOrderList];
        
    }];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - RefundListSearchViewDelegate
- (void)hiddenView {
    [self.searchView removeFromSuperview];
    [self.searchDictionary removeAllObjects];
    
}

-(void)searchWithDictionary:(NSDictionary *)searchParam {
    [self.searchView removeFromSuperview];
    self.searchDictionary = [ NSMutableDictionary dictionaryWithDictionary:searchParam];
    [self getRefundOrderList];
}

#pragma mark - RefundDelegate


-(void)chatBuyer:(NSString *)uidAndNameAndLogo {
    
    NSArray *temp = [uidAndNameAndLogo componentsSeparatedByString:@","];
    if (temp.count < 3) return;
    
#ifdef ALIYM_AVALABLE
    YWPerson *person = [[YWPerson alloc] initWithPersonId:[temp firstObject]];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = temp[1];
    chatVC.avatarUrl = [temp lastObject];
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:[temp firstObject]  type:EMConversationTypeChat createIfNotExist:YES];
    [XSTool hideProgressHUDWithView:self.view];
    self.chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    self.chatVC.title = temp[1] ;
    self.chatVC.avatarUrl = [temp lastObject];
    self.chatVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:self.chatVC animated:YES];
#else
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:[temp firstObject] title:temp[1] avatarURLPath:[temp lastObject]];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
}

#pragma mark - private
-(void)setBusinessRefundType:(BusinessRefundType)businessRefundType {
    _businessRefundType = businessRefundType;
    NSString *title = @"";
    switch (businessRefundType) {
        case BusinessRefundTypeWaitSellerCheck:
            self.refundStatus = @"waitDeal";
            title = @"等待卖家审核";
            break;
        case BusinessRefundTypeAll:
            self.refundStatus = @" ";
            title = Localized(@"alls");
            break;
        case BusinessRefundTypeFinished:
            self.refundStatus = @"completed";
            title = Localized(@"have_finish");
            break;
        case BusinessRefundTypeWaitBuyerDeliver:
            self.refundStatus = @"waitSend";
            title = @"待买家发货";
            break;
        case BusinessRefundTypeRejectReceiving:
            self.refundStatus = @"confirmRefused";
            title = @"已拒绝收货";
            break;
        case BusinessRefundTypeRejectRefund:
            self.refundStatus = @"refundRefused";
            title = @"已拒绝退款";
            break;
        case BusinessRefundTypeWaitDeliver:
            self.refundStatus = @"waitConfirm";
            title = @"待收货";
            break;
        case BusinessRefundTypeWaitSubmitVoucher:
            self.refundStatus = @"waitSay";
            title = @"待提交凭证";
            break;
        case BusinessRefundTypeWaitKefu:
            self.refundStatus = @"waitCs";
            title = @"客服处理中";
            break;
        case BusinessRefundTypeClose:
            self.refundStatus = @"closed";
            title = @"退款关闭";
            break;
        case BusinessRefundTypeSuccess:
            self.refundStatus = @"success";
            title = @"退款成功";
            break;
    }
    self.navigationItem.title = title;
}

- (void)getRefundOrderList {
    self.page = 1;
    [self.paramDictionary removeAllObjects];
    [self.paramDictionary setDictionary:@{@"limit":self.limit,@"page":[NSString stringWithFormat:@"%lu",self.page],@"role":@"seller",@"_search_status":self.refundStatus}];
    if (self.searchDictionary.allKeys.count) {
        [self.paramDictionary setValuesForKeysWithDictionary:self.searchDictionary];
    }

    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundListsWithParam:self.paramDictionary success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            BusinessRefundListParser *parser = [BusinessRefundListParser mj_objectWithKeyValues:dic[@"data"][@"list"]];
            [self.listArray removeAllObjects];
            [self.listArray addObjectsFromArray:parser.data];
            if (self.listArray.count) {
               
//                self.defaultView.hidden = YES;
                self.page++;
            } else {
                 [XSTool showToastWithView:self.view Text:@"当前您还没有此类订单信息！"];
//                self.defaultView.hidden = NO;
            }
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
            
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)getMoreRefundOrderList {
    
    [self.paramDictionary removeAllObjects];
    [self.paramDictionary setDictionary:@{@"limit":self.limit,@"page":[NSString stringWithFormat:@"%lu",self.page],@"role":@"seller",@"_search_status":self.refundStatus}];
    if (self.searchDictionary.allKeys.count) {
        [self.paramDictionary setValuesForKeysWithDictionary:self.searchDictionary];
    }

    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundListsWithParam:self.paramDictionary success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            BusinessRefundListParser *parser = [BusinessRefundListParser mj_objectWithKeyValues:dic[@"data"][@"list"]];
            [self.listArray addObjectsFromArray:parser.data];
            if (parser.data.count) {
                self.defaultView.hidden = YES;
                self.page++;
            }
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
            
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

#pragma mark - tableview
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"BusinessRefundListTableViewCell";
    BusinessRefundListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[BusinessRefundListTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
    }
    cell.delegate = self;
    if (self.listArray.count>indexPath.section) {
         cell.refundDataParser = self.listArray[indexPath.section];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
     BusinessRefundDetailViewController *controller = [[BusinessRefundDetailViewController alloc] init];
//    NegotiateHistoryViewController *controller = [[NegotiateHistoryViewController alloc] init];
    BusinessRefundListDataParser *dataParser = self.listArray[indexPath.section];
    controller.refundId = dataParser.refund_no;
    [self.navigationController pushViewController:controller animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listArray.count;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 12;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 144;
}

@end
