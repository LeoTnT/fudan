//
//  NegotiateHistoryViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface NegotiateHistoryViewController : XSBaseTableViewController


@property (nonatomic, copy) NSString *refundId;
@end
