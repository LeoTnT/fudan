//
//  SellerLeaveWordsViewController.h
//  App3.0
//
//  Created by nilin on 2018/1/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerLeaveWordsViewController : XSBaseTableViewController

@property (nonatomic, copy) NSString *refundId;
@end
