//
//  RefundManageViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundManageViewController.h"
#import "RefundManageTopTableViewCell.h"
#import "AreaButton.h"
#import "BusinessRefundFormListViewController.h"
#import "BusinessRefundModel.h"
#import "BusinessRefundDetailViewController.h"

@interface RefundManageViewController ()<AreaButtonDelegate,RefundManageTopDelegate>
@property (nonatomic, strong) NSArray *itemArray;
@property (nonatomic, strong) BusinessRefundMainParser *refundParser;

@end

@implementation RefundManageViewController

#pragma mark - lazyloadding


#pragma mark - life
//-(void)viewWillAppear:(BOOL)animated {
//
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
//
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    
    self.fd_prefersNavigationBarHidden = YES;
    
    self.itemArray = @[@[@{@"image":@"business_waitBuyer_reciver",@"title":@"待买家发货"},@{@"image":@"business_rejectReciver",@"title":@"已拒绝收货"},@{@"image":@"business_refund_reject",@"title":@"已拒绝退款"},@{@"image":@"business_wait_reciver",@"title":@"待收货"},@{@"image":@"business_waitVoucher",@"title":@"待提交凭证"},@{@"image":@"business_refund_kefu",@"title":@"客服处理中"}],@[@{@"image":@"business_refund_close",@"title":@"退款关闭"},@{@"image":@"business_refund_money_success",@"title":@"退款成功"}]];
   
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(4, StatusBarHeight+8, 34, 26.5)];
    [backButton setImage:[UIImage imageNamed:@"business_refund_nav"] forState:UIControlStateNormal];
      [backButton setImage:[UIImage imageNamed:@"business_refund_nav"] forState:UIControlStateSelected];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, StatusBarHeight+13.5, mainWidth, 17)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"退款退货";
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:titleLabel];
    
    [self getRefundInformation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)getRefundInformation {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundInfoSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.refundParser = [BusinessRefundMainParser  mj_objectWithKeyValues:dic[@"data"]];
            [self.tableView reloadData];
        } else {
            
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)allButtonAction:(UIButton *) sender {
     BusinessRefundFormListViewController *controller = [[BusinessRefundFormListViewController alloc] init];
     controller.businessRefundType = BusinessRefundTypeAll;
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (void)finishedButtonAction:(UIButton *) sender {
    BusinessRefundFormListViewController *controller = [[BusinessRefundFormListViewController alloc] init];
    controller.businessRefundType = BusinessRefundTypeFinished;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - RefundManageTopDelegate
-(void)lookWaitCheckOrder {
   BusinessRefundFormListViewController *controller = [[BusinessRefundFormListViewController alloc] init];
    controller.businessRefundType = BusinessRefundTypeWaitSellerCheck;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)lookLastRefundWithRefundId:(NSString *)refundId {
    
    //查看最新一笔退款
    BusinessRefundDetailViewController *controller = [[BusinessRefundDetailViewController alloc] init];
    controller.refundId = refundId;
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

#pragma mark - AreaButtonDelegate

- (void)areaButtonClickWithIndex:(NSInteger)index {
    BusinessRefundFormListViewController *controller = [[BusinessRefundFormListViewController alloc] init];
    switch (index) {
        case 100:
        {
            //待买家发货
             controller.businessRefundType = BusinessRefundTypeWaitBuyerDeliver;
            
        }
            break;
        case 101:
        {
             //已拒绝收货
             controller.businessRefundType = BusinessRefundTypeRejectReceiving;
        }
            break;
        case 102:
        {
             //已拒绝退款
             controller.businessRefundType = BusinessRefundTypeRejectRefund;
        }
            break;
        case 103:
        {
             //待收货
             controller.businessRefundType = BusinessRefundTypeWaitDeliver;
        }
            break;
        case 104:
        {
             //待提交凭证
             controller.businessRefundType = BusinessRefundTypeWaitSubmitVoucher;
        }
            break;
        case 105:
        {
            //客服处理中
            controller.businessRefundType = BusinessRefundTypeWaitKefu;
        }
            break;
            
        case 200:
        {
             //退款关闭
             controller.businessRefundType = BusinessRefundTypeClose;
        }
            break;
        
        case 201:
        {
            //退款成功
            controller.businessRefundType = BusinessRefundTypeSuccess;
        }
            break;
    }
   
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - tableview

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 3;
    } else {
        return 1;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"normal1";
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            RefundManageTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[RefundManageTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                
            }
            cell.delegate = self;
            if (self.refundParser) {
                 cell.refundParser = self.refundParser;
            }
           
            return cell;
            
        } else if (indexPath.row==1) {
            idString = @"normal22";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor hexFloatColor:@"41A5EE"];
            }
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            CGFloat width = mainWidth/2;
            CGFloat height = 52;
            UIButton *allButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            allButton.titleLabel.font = [UIFont systemFontOfSize:18];
            [allButton setTitle:Localized(@"alls") forState:UIControlStateNormal];
            [allButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [allButton setImage:[UIImage imageNamed:@"business_refund_report_all"] forState:UIControlStateNormal];
            [allButton setImageEdgeInsets:UIEdgeInsetsMake(0, -9.5, 0, 0)];
            [allButton addTarget:self action:@selector(allButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:allButton];
            UIButton *finishedButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(allButton.frame), 0, width, height)];
            finishedButton.titleLabel.font = [UIFont systemFontOfSize:18];
            [finishedButton setTitle:Localized(@"have_finish") forState:UIControlStateNormal];
            [finishedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [finishedButton setImage:[UIImage imageNamed:@"business_refund_report_finished"] forState:UIControlStateNormal];
            [finishedButton setImageEdgeInsets:UIEdgeInsetsMake(0, -9.5, 0, 0)];
            [finishedButton addTarget:self action:@selector(finishedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:finishedButton];
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(allButton.frame), 11.5, 0.8, 29)];
            line.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:line];
            return cell;
            
            
        } else {
            idString = @"normal33";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            CGFloat width = mainWidth/3;
            CGFloat height = 90;
            NSArray *temp = [self.itemArray firstObject];
            for (int i=0; i<temp.count; i++) {
               @autoreleasepool {
                AreaButton *areaButton = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%3), (int)(i/3)*height, width, height) Model:temp[i] Scale:1.0/3.0 fontSize:13];
                areaButton.index = i+100;
                areaButton.delegate = self;
                [cell.contentView addSubview:areaButton];
               }
            }
            return cell;
        }

    } else {
        idString = @"normal55";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        CGFloat width = mainWidth/3;
        CGFloat height = 90;
        NSArray *temp = [self.itemArray lastObject];
        for (int i=0; i<temp.count; i++) {
             @autoreleasepool {
            AreaButton *areaButton = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%3), (int)(i/3)*height, width, height) Model:temp[i] Scale:1.0/3.0 fontSize:13];
            areaButton.index = i+200;
            areaButton.delegate = self;
            [cell.contentView addSubview:areaButton];
             }
        }
        return cell;
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 305-52-20;
        } else if (indexPath.row==1) {
            return 52;
            
        } else {
            return 180;
        }
    } else {
        
        return 90;
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section==0) {
        return 0.01;
    } else {
        return 12.5;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

@end
