//
//  BusinessRefundDetailViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessRefundDetailViewController.h"
#import "BusinessRefundModel.h"
#import "RefundDetailTopTableViewCell.h"
#import "NegotiateHistoryViewController.h"
#import "RefundDetailGoodsTableViewCell.h"
#import "RefundDetailBottomTableViewCell.h"
#import "VerifyDeliverGoodsViewController.h"
#import "ChatViewController.h"
#import "RejectReundViewController.h"
#import "AgreeRefundGooodsAndMoneyViewController.h"
#import "BusinessRefundFormListViewController.h"
#import "RefundManageViewController.h"
#import "PopUpView.h"
#import "ZYQAssetPickerController.h"
#import "BusinessOrderLogisticViewController.h"
#import "SellerLeaveWordsViewController.h"


@interface BusinessRefundDetailViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,RefundDelegate,PopViewDelegate,ZYQAssetPickerControllerDelegate>

@property (nonatomic, strong) BusinessRefundDetailParser *detailParser;
@property(nonatomic,strong) UIImagePickerController *imagePickViewController;//拍照控制器
@property (nonatomic, strong) ChatViewController *chatVC;
@property(nonatomic,assign) BOOL takePhotoFlag;//是否是现拍的照片
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) PopUpView *sheetView;//弹出的view
@property (nonatomic, strong) BusinessOrderLogisticViewController *logisticController;
@end

@implementation BusinessRefundDetailViewController
#pragma mark - lazyloadding
- (NSMutableArray *)photoArray {
    if (!_photoArray) {
        
        _photoArray = [NSMutableArray arrayWithObject:[UIImage imageNamed:@"eva_add"]];
    }
    return _photoArray;
}

#pragma mark - life circle
//-(void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor  whiteColor];
    self.navigationItem.title = @"退款详情";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self popLastController];
        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    [self setSubViews];
    [self  getDetailRefund];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)setSubViews {
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame), mainWidth, 0.5)];
    line.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
    [self.view addSubview:line];
    
    UIButton *chatButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(line.frame), mainWidth/2, 48.5)];
    [chatButton setTitle:@"联系买家" forState:UIControlStateNormal];
    [chatButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    chatButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [chatButton setImage: [UIImage imageNamed:@"business_refund_chat_1"] forState:UIControlStateNormal];
    [chatButton setImageEdgeInsets:UIEdgeInsetsMake(0, -8.5, 0, 0)];
    [chatButton addTarget:self action:@selector(chatAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:chatButton];
    
    UIButton *callButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(chatButton.frame), CGRectGetMaxY(line.frame), mainWidth/2, 48.5)];
    [callButton setTitle:@"拨打电话" forState:UIControlStateNormal];
    [callButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    callButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [callButton setImage: [UIImage imageNamed:@"business_refund_call"] forState:UIControlStateNormal];
    [callButton setImageEdgeInsets:UIEdgeInsetsMake(0, -8.5, 0, 0)];
    [callButton addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:callButton];
    
    UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(chatButton.frame)-0.25, CGRectGetMinY(chatButton.frame)+13, 1, CGRectGetHeight(chatButton.frame)-13*2)];
    line2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
    [self.view addSubview:line2];
}

- (void) chatAction:(UIButton *) sender {
    [self chatBuyer:[NSString stringWithFormat:@"%@,%@,%@",self.detailParser.buyer_id,self.detailParser.buyer_name,self.detailParser.buyer_avatar]];
}

- (void)callAction:(UIButton *) sender {
    [self callBuyer:self.detailParser.buyer_tel];
}
- (void) popLastController {
    
    if (self.isChange) {
        if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[BusinessRefundFormListViewController class]]) {
            BusinessRefundFormListViewController *controller = (BusinessRefundFormListViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            [controller.tableView.mj_header beginRefreshing];
            [self.navigationController popToViewController:controller animated:YES];
        } else if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[RefundManageViewController class]]) {
            RefundManageViewController *controller = (RefundManageViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            [controller getRefundInformation];
            [self.navigationController popToViewController:controller animated:YES];
        }
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}
- (void)getDetailRefund {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundViewWithParam:@{@"refund_id": self.refundId} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"detailParser======%@",dic);
        if (state.status) {
            self.detailParser = [BusinessRefundDetailParser mj_objectWithKeyValues:dic[@"data"]];
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
- (void)choosePhotos:(UITapGestureRecognizer *) tap {
    [self.view endEditing:YES];
    if (self.photoArray.count>4) {
        [XSTool showToastWithView:self.view Text:@"最多上传3张！"];
        return;
    }
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypePhoto;
    [self.sheetView.oneBtn setTitle:Localized(@"拍照") forState:UIControlStateNormal];
    [self.sheetView.oneBtn addTarget:self action:@selector(takeAPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.sheetView.otherBtn setTitle:Localized(@"从相册中选择") forState:UIControlStateNormal];
    [self.sheetView.otherBtn addTarget:self action:@selector(openPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView ] ;
    
}

- (void)openPhotoLibrary {
    [self clickToCancelLink];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
    picker.maximumNumberOfSelection = 3;
    picker.assetsFilter = ZYQAssetsFilterAllAssets;
    picker.showEmptyGroups=NO;
    picker.delegate=self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)endEditing{
    [self.view endEditing:YES];
}

- (void)deletePicture:(UIButton *)button {
    RefundDetailTopTableViewCell *cell= (RefundDetailTopTableViewCell *)button.superview.superview.superview;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSInteger index=[cell.photoView.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    NSMutableArray *tempArray = self.photoArray;
    [tempArray removeObjectAtIndex:index];
    [self.photoArray setArray:tempArray];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)takeAPhoto {
    [self clickToCancelLink];
    self.imagePickViewController = [[UIImagePickerController alloc] init];
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action1];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if (!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickViewController.showsCameraControls = YES;
    
    //摄像头捕获模式
    self.imagePickViewController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickViewController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    self.imagePickViewController.delegate = self;
    [self presentViewController:self.imagePickViewController animated:YES completion:^{
        self.takePhotoFlag = YES;
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    //压缩0.8
    UIImage *newImage;
    NSData *imageData=UIImageJPEGRepresentation(image, 0.8);
    if (imageData.length>1024*1024*3) {//3M以及以上
        NSData *imageData=UIImageJPEGRepresentation(image, 0.2);
        newImage = [UIImage imageWithData:imageData];
        
    } else {
        imageData=UIImageJPEGRepresentation(image, 0.4);
        newImage = [UIImage imageWithData:imageData];
        
    }
    if (self.photoArray.count<4) {
        [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    }
}
#pragma mark -  PopViewDelegate
- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}
#pragma  mark - ZYQAssetPickerControllerDelegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            //压缩0.8
            UIImage *newImage;
            NSData *imageData=UIImageJPEGRepresentation(result, 0.8);
            if (imageData.length>1024*1024*3) {//3M以及以上
                //                [self dismissViewControllerAnimated:YES completion:^{
                //                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
                //                }];
                NSData *imageData=UIImageJPEGRepresentation(result, 0.2);
                newImage = [UIImage imageWithData:imageData];
                
            } else {
                imageData=UIImageJPEGRepresentation(result, 0.4);
                newImage = [UIImage imageWithData:imageData];
                
            }
            if (self.photoArray.count<4) {
                [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
                NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            }
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择三张图片"];
}
#pragma mark - RefundDelegate
-(void)chatBuyer:(NSString *)uidAndNameAndLogo {
    NSArray *temp = [uidAndNameAndLogo componentsSeparatedByString:@","];
    if (temp.count < 3) return;
#ifdef ALIYM_AVALABLE
    YWPerson *person = [[YWPerson alloc] initWithPersonId:[temp firstObject]];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = temp[1];
    chatVC.avatarUrl = [temp lastObject];
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:[temp firstObject]  type:EMConversationTypeChat createIfNotExist:YES];
    [XSTool hideProgressHUDWithView:self.view];
    self.chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    self.chatVC.title = temp[1] ;
    self.chatVC.avatarUrl = [temp lastObject];
    self.chatVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:self.chatVC animated:YES];
#else
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:[temp firstObject] title:temp[1] avatarURLPath:[temp lastObject]];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
    
}

-(void)callBuyer:(NSString *)mobile {
    if (isEmptyString(mobile)) {
        [XSTool showToastWithView:self.view Text:@"买家电话错误"];
    } else {
        UIWebView*callWebview =[[UIWebView alloc] init];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",mobile]];// 貌似tel:// 或者 tel: 都行
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        //记得添加到view上
        [self.view addSubview:callWebview];
    }
}

-(void)agreeOnlyRefundApplyWithRefund:(NSString *)refundId {
    //仅退款
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundAgreeOnlyRefundWithParam:@{@"refund_id":refundId} success:^(NSDictionary *dic, resultObject *state) {
        
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            //            [XSTool showToastWithView:self.view Text:@"同意退款"];
            self.isChange = YES;
            [self getDetailRefund];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

-(void)rejectRefundApply:(NSString *)refundId{
    //拒绝申请= 已发货
    RejectReundViewController *controller = [[RejectReundViewController alloc] init];
    controller.refundId = refundId;
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)agreeReturnRefundApplyWithRefund:(NSString *)refundId {
    
    //退款退货
    AgreeRefundGooodsAndMoneyViewController *controller = [[AgreeRefundGooodsAndMoneyViewController alloc] init];
    controller.refundId = refundId;
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(void)deliveryGoodsWithOrderId:(NSString *)orderId {
    //检查是否有未结束的退款
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundCheckWithParam:@{@"order_id":orderId} success:^(NSDictionary *dic, resultObject *state) {
        
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            if ([dic[@"data"] integerValue]==1) {
                //有退款中的
                UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"此订单下有未结束的退款，请确定已经和买家协商，如果发货将关闭买家的退款。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    //跳转到发货界面
                    VerifyDeliverGoodsViewController *controller = [[VerifyDeliverGoodsViewController alloc] init];
                    controller.orderId = orderId;
                    [self.navigationController pushViewController:controller animated:YES];
                    
                }];
                [alertControl addAction:cancelAction];
                [alertControl addAction:okAction];
                [self presentViewController:alertControl animated:YES completion:nil];
            } else {
                
            }
        } else {
            
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)sellerSayWithRefund:(NSDictionary *)param {
    //提交举证
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:param];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.photoArray];
    [tempArray removeLastObject];
    
    if (tempArray.count) {
        NSDictionary *photoParam=@{@"type":@"reject",@"formname":@"file"};
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager upLoadPhotosWithDic:photoParam andDataArray:tempArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
            
            @strongify(self);
            if ([dic[@"status"] integerValue]==1) {
                NSMutableArray *imgs = [NSMutableArray arrayWithArray:dic[@"data"]];
                
                NSError *error = nil;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:imgs
                                                                   options:kNilOptions
                                                                     error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                             encoding:NSUTF8StringEncoding];
                [params setObject: jsonString forKey:@"pics"];
                [self submitSellerSayWithParams:params];
            } else {
                
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
        } fail:^(NSError * _Nonnull error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [self submitSellerSayWithParams:params];
    }
    
}


- (void)submitSellerSayWithParams:(NSDictionary *) params {
    [HTTPManager refundSellerSayWithParam:params success:^(NSDictionary *dic, resultObject *state) {
        
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.isChange = YES;
            [self.photoArray removeAllObjects];
            [self getDetailRefund];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)refundConfirmWithRefund:(NSString *)refundId {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager refundConfirmWithParam:@{@"refund_id":refundId} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.isChange = YES;
            [self.photoArray removeAllObjects];
            [self getDetailRefund];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)rejectReceiverRefundApplyMoneyRefund:(NSString *)refundId {
    
    //买家已发货-买家拒绝退款
    RejectReundViewController *controller = [[RejectReundViewController alloc] init];
    controller.rejectRefundType = RejectReundTypeReturn;
    controller.refundId = refundId;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)copyNumberWithString:(NSString *)copyString {
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = copyString;
    [XSTool showToastWithView:self.view Text:@"编号已复制"];
}

- (void)lookLogistics {
    self.logisticController = [[BusinessOrderLogisticViewController alloc] init];
    //  BusinessRefundDetailLogisticsParser
    self.logisticController.orderId = self.detailParser.order_id;
    self.logisticController.logoUrl = self.detailParser.image;
    self.logisticController.count = self.detailParser.product_num;
    
    [self.navigationController pushViewController:self.logisticController animated:YES];
}


-(void)leaveWordsWithRefund:(NSString *)refundId {
    SellerLeaveWordsViewController *controller = [[SellerLeaveWordsViewController alloc] init];
    controller.refundId = refundId;
    [self.navigationController pushViewController:controller animated:YES];
    
}
#pragma mark - tableview

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"normal";
    
    
    if (indexPath.section==0) {
        idString = @"RefundDetailTopTableViewCell";
        RefundDetailTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[RefundDetailTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        cell.refundDetailTopType = RefundDetailTopTypeWaitCheck;
        cell.delegate = self;
        if (self.photoArray.count) {
            if (self.photoArray.count<5) {
                cell.photoArray = self.photoArray;
                if (self.photoArray.count==4) {
                    [XSTool showToastWithView:self.view Text:@"最多上传3张!"];
                }
            }
        }
        
        cell.detailParser = self.detailParser;
        
        if (self.photoArray.count) {
            //给cell的删除按钮绑定方法
            for (int i=0; i<cell.photoView.deletBtnArray.count; i++) {
                [[cell.photoView.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
            }
            if (self.photoArray.count>3) {
                
            } else {
                cell.photoView.userInteractionEnabled = YES;
                cell.photoView.lastImage.userInteractionEnabled = YES;
                [cell.photoView.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos:)]];
            }
            
            //绑定手势  收起键盘
            [cell.photoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)]];
            
        }
        
        
        return cell;
    } else if (indexPath.section==1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(11.5, 15, 120, 14)];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"协商历史";
        titleLabel.font = [UIFont systemFontOfSize:15];
        [cell.contentView addSubview:titleLabel];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else if (indexPath.section==2) {
        idString = @"RefundDetailGoodsTableViewCell";
        RefundDetailGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[RefundDetailGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        cell.delegate = self;
        cell.detailParser = self.detailParser;
        return cell;
    } else {
        idString = @"RefundDetailBottomTableViewCell";
        RefundDetailBottomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[RefundDetailBottomTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        cell.delegate = self;
        cell.detailParser = self.detailParser;
        return cell;
        
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 0.01;
    } else {
        return 12;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        RefundDetailTopTableViewCell *cell = (RefundDetailTopTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    } else if (indexPath.section==1) {
        return 44;
    } else if (indexPath.section==2) {
        RefundDetailGoodsTableViewCell *cell = (RefundDetailGoodsTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
        
    } else {
        RefundDetailBottomTableViewCell *cell = (RefundDetailBottomTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section==1) {
        NegotiateHistoryViewController *controller = [[NegotiateHistoryViewController alloc] init];
        controller.refundId = self.refundId;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}


@end
