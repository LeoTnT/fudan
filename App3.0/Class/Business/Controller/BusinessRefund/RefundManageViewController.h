//
//  RefundManageViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface RefundManageViewController : XSBaseTableViewController
- (void)getRefundInformation;
@end
