//
//  GoodsStandardViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsStandardViewController.h"
#import "BusinessModel.h"
#import "BusinessSpecTopTableViewCell.h"
#import "BusinessClassifyModel.h"
#import "BusinessPriceFieldTableViewCell.h"
#import "AdvancedPublishGoodsViewController.h"
#import "BusinessScanViewController.h"
#import "BusinessDefaultView.h"
#import "AddStandardViewController.h"


@interface GoodsStandardViewController ()<UITableViewDelegate,UITableViewDataSource,BusinessSpecTopDelegate,LogisticAreaViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *specArray;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) NSMutableArray *openOrCloseArray;
@property (nonatomic, strong) NSMutableDictionary *selectedDictionary;
@property (nonatomic, strong) NSMutableArray *fieldArray;//价格属性
@property (nonatomic, strong) NSMutableArray *uniteArray;//规格排列组合结果数组
@property (nonatomic, strong) NSMutableArray *uniteIdArray;//规格排列组合规格值id数组
@property (nonatomic, strong) NSMutableArray *submitArray;//保存的数组
@property (nonatomic, strong) NSMutableDictionary *submitDictionary;//保存数组里的字典
@property (nonatomic, assign) CGFloat currentY;
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, strong) NSMutableDictionary *fieldCountDictionary;//cell规格输入值字典
@property (nonatomic, strong) NSMutableArray *allCellArray;//规格输入cell
@property (nonatomic, assign) NSUInteger nowIndex;
@property (nonatomic, strong) BusinessDefaultView *defaultView;
@end

@implementation GoodsStandardViewController

#pragma mark - lazy loadding
-(BusinessDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [BusinessDefaultView new];
        _defaultView.applyBtn.hidden = YES;
        NSString *tintString = @"目前没有规格，请添加规格";
        _defaultView.tintLabel.text = tintString;
    }
    return _defaultView;
}

-(NSMutableDictionary *)fieldCountDictionary{
    if (!_fieldCountDictionary) {
        _fieldCountDictionary = [NSMutableDictionary dictionary];
    }
    return _fieldCountDictionary;
}

-(NSMutableArray *)submitArray {
    if (!_submitArray) {
        _submitArray = [NSMutableArray array];
    }
    return _submitArray;
}

-(NSMutableArray *)allCellArray {
    if (!_allCellArray) {
        _allCellArray = [NSMutableArray array];
    }
    return _allCellArray;
}

-(NSMutableDictionary *)submitDictionary {
    if (!_submitDictionary) {
        _submitDictionary = [NSMutableDictionary dictionary];
    }
    return _submitDictionary;
}

-(NSMutableArray *)specArray {
    if (!_specArray) {
        _specArray = [NSMutableArray array];
    }
    return _specArray;
}

-(NSMutableArray *)openOrCloseArray {
    if (!_openOrCloseArray) {
        _openOrCloseArray = [NSMutableArray array];
    }
    return _openOrCloseArray;
}

-(NSMutableDictionary *)selectedDictionary {
    if (!_selectedDictionary) {
        _selectedDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedDictionary;
}

-(UIButton *)saveButton {
    if (!_saveButton) {
        _saveButton = [UIButton new];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_saveButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        _saveButton.backgroundColor = mainColor;
        [_saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}

-(NSMutableArray *)fieldArray {
    if (!_fieldArray) {
        _fieldArray = [NSMutableArray array];
    }
    return _fieldArray;
}

-(NSMutableArray *)uniteArray {
    if (!_uniteArray) {
        _uniteArray = [NSMutableArray array];
    }
    return _uniteArray;
}

-(NSMutableArray *)uniteIdArray {
    if (!_uniteIdArray) {
        _uniteIdArray = [NSMutableArray array];
    }
    return _uniteIdArray;
}
#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商品规格";
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.autoHideKeyboard = YES;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:@"business_add" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        AddStandardViewController *controller = [[AddStandardViewController alloc] init];
        controller.categoryId = self.categoryId;
        [self.navigationController pushViewController:controller animated:YES];
        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = BG_COLOR;
    [self.view addSubview:self.saveButton];
    [self.view addSubview:self.defaultView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).with.mas_offset(-50);
        
    }];
    
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    [self.defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).with.mas_offset(-50);
        
    }];
    self.defaultView.hidden = YES;
    self.saveButton.hidden = YES;
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    [self getStandardInformation];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
#pragma mark -模仿qq界面
- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    BusinessScanViewController *scanVC = [[BusinessScanViewController alloc] init];
    scanVC.style = style;
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
}

- (void)setBarCode:(NSString *)barCode {
    _barCode = barCode;
    NSMutableArray *textArray = [NSMutableArray array];
    if ([self.fieldCountDictionary.allKeys containsObject:@(self.nowIndex)]) {
        [textArray addObjectsFromArray: [self.fieldCountDictionary objectForKey:@(self.nowIndex)]];
    } else {
        for (int i=0; i<self.fieldArray.count; i++) {
            [textArray addObject:@""];
        }
    }
    [textArray removeObjectAtIndex:0];
    [textArray insertObject:barCode atIndex:0];
    [self.fieldCountDictionary setObject:textArray forKey:@(self.nowIndex)];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void)getStandardInformation {
    
    //第一个参数 销售类型
    [HTTPManager supplyProductGetPriceFieldWithSellType:self.sellType operateType:self.operateType success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            BusinessPriceFieldParser *parser = [BusinessPriceFieldParser mj_objectWithKeyValues:dic[@"data"]];
            if (parser.field.count) {
                [self.fieldArray removeAllObjects];
                for (int i=0; i<parser.field.count; i++) {
                    BusinessPriceFieldDetailParser *detailParser = parser.field[i];
                    
                    //是否显示
                    if ([detailParser.display integerValue]==1) {
                        [self.fieldArray addObject:detailParser];
                    }
                }
                //                [self.fieldArray addObjectsFromArray:parser.field];
                
                //字典初始化
                for (int i=0; i<self.fieldArray.count; i++) {
                    BusinessPriceFieldDetailParser *detailParser = self.fieldArray[i];
                    [self.submitDictionary setObject:@"1" forKey:detailParser.field];
                }
                [self.submitDictionary setObject:@"1" forKey:@"spec_group_name"];
                [self.submitDictionary setObject:@"1" forKey:@"spec_group"];
                [self.tableView reloadData];
            } else {
                
                
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplyProductGetSpecWithCategoryId:self.categoryId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            
            //规格信息
            BusinessSpecParser *parser = [BusinessSpecParser mj_objectWithKeyValues:dic];
            
            if (parser.data.count) {
                self.defaultView.hidden = YES;
                self.saveButton.hidden = NO;
                [self.specArray removeAllObjects];
                [self.specArray addObjectsFromArray:parser.data];
                for (int i=0; i<self.specArray.count; i++) {
                    [self.openOrCloseArray addObject:@"0"];
                }
                [self.tableView reloadData];
            } else {
                self.defaultView.hidden = NO;
                self.saveButton.hidden = YES;
            }
            
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
    
}

- (void)saveAction {
    [self.view endEditing:YES];
    
    NSUInteger isComplete = 1;//1:信息完整且正确，2：信息不完整，3：商品编号不唯一
    //存储数据
    [self.submitArray removeAllObjects];
    NSMutableString *tintString = [NSMutableString string];//唯一字段提示
    
    for (int i=2; i<self.uniteArray.count+2; i++) {
        BusinessPriceFieldDetailParser *uniqueParser;//有唯一属性的
        NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithDictionary:self.submitDictionary];
        NSArray *nameArray = [self.uniteArray objectAtIndex:i-2];
        NSArray *uniteIdArray = [self.uniteIdArray objectAtIndex:i-2];
        [tempDictionary setObject:[nameArray componentsJoinedByString:@","] forKey:@"spec_group_name"];
        NSMutableArray *allSpecArray = [NSMutableArray array];
        for (BusinessSpecListParser *listParser in self.specArray) {
            [allSpecArray addObjectsFromArray:listParser.child];
        }
        NSMutableArray *idArray = [NSMutableArray array];
        for (int j=0; j<nameArray.count; j++) {
            
            for (BusinessSpecListParser *listParser in self.specArray) {
                for (BusinessSpecDetailParser *detailParser in listParser.child) {
                    if ([detailParser.ID isEqualToString:uniteIdArray[j]]) {
                        [idArray addObject:[NSString stringWithFormat:@"%@:%@",listParser.ID,detailParser.ID]];
                        break;
                    }
                }
            }
        }
        
        [tempDictionary setObject:[idArray componentsJoinedByString:@";"] forKey:@"spec_group"];
        NSArray *editArray = [self.fieldCountDictionary objectForKey:@(i)];
        for (int k=0; k<self.fieldArray.count; k++) {
            BusinessPriceFieldDetailParser *parser = self.fieldArray[k];
            NSString *text = [NSString stringWithFormat:@"%@",editArray[k]];
            if (isEmptyString(text)&&[parser.require integerValue]==1) {
                
                //信息不完整
                isComplete = 2;
                break;
                
            } else {
                [tempDictionary setObject:isEmptyString(text)?@"0":text forKey:parser.field];
                if ([parser.unique integerValue]==1) {
                    uniqueParser = parser;
                    [tintString  setString:[NSString stringWithFormat:@"%@",uniqueParser.name]];
                   
                }
                
            }
            NSLog(@"tempDictionary+++++%@",tempDictionary);
            
        }
        
        BOOL isAdd = YES;
        for (NSDictionary *savedDictionary in self.submitArray) {
            
            if ([[NSString stringWithFormat:@"%@",[savedDictionary objectForKey:uniqueParser.field]] isEqualToString:[NSString stringWithFormat:@"%@",[tempDictionary objectForKey:uniqueParser.field]]]){
                isAdd = NO;
//                [tintString substringToIndex:tintString.length-1];
                [tintString appendString:@"唯一"];
                
                break;
            }
        }
        if (isAdd) {
            [self.submitArray addObject:tempDictionary];
        } else {
            
            isComplete=3;
            break;
        }
        
        NSLog(@"+++++%@",self.submitArray);
        
        
    }
    
    if (isComplete==2) {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    } else if (isComplete==3){
        
        [XSTool showToastWithView:self.view Text:tintString ];
    } else {
        NSLog(@"======%@",self.submitArray);
        [XSTool showToastWithView:self.view Text:@"正在保存"];
        AdvancedPublishGoodsViewController *controller = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
        controller.specFieldArray = self.submitArray;
        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:2];
        [controller.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        [self.navigationController popToViewController:controller animated:YES];
        
    }
    
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}

- (void)combine:(NSMutableArray *)result type:(NSString *) nameOrId data:(NSArray *)data curr:(int)currIndex count:(int)count {
    
    if (currIndex == count) {
        if ([nameOrId isEqualToString:@"name"]) {
            [self.uniteArray addObject:[result mutableCopy]];
        }
        if ([nameOrId isEqualToString:@"id"]) {
            [self.uniteIdArray addObject:[result mutableCopy]];
        }
        [result removeLastObject];
        
    }else {
        NSArray* array = [data objectAtIndex:currIndex];
        
        for (int i = 0; i < array.count; ++i) {
            [result addObject:[array objectAtIndex:i]];
            
            //进入递归循环
            [self combine:result type:nameOrId data:data curr:currIndex+1 count:count];
            
            if ((i+1 == array.count) && (currIndex-1>=0)) {
                [result removeObjectAtIndex:currIndex-1];
            }
        }
    }
}

- (void)batchWriteAction:(UIButton *) sender {
    [self.view endEditing:YES];
    //批量添加
    
    if ([self.fieldCountDictionary.allKeys containsObject:@1]) {
        NSArray *firstArray = [self.fieldCountDictionary objectForKey:@1];
        for (int i=2; i<self.uniteArray.count+2; i++) {
            [self.fieldCountDictionary setObject:firstArray forKey:@(i)];
        }
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息！"];
    }
    
}

- (void)scanAction:(UIButton *) sender {
    [self.view endEditing:YES];
    UITableViewCell *cell =(UITableViewCell *)sender.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    self.nowIndex = index.row;
    
    [self qqStyle];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    //    NSLog(@"%@,%@",rectInSuperview ,rectInTableView);
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    NSLog(@"90909090909===%f",height);
    self.currentY = height;
    NSLog(@"----%f,%f",height,rectInSuperview.origin.y);
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self saveTextFieldWithTextfield:textField];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self saveTextFieldWithTextfield:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self saveTextFieldWithTextfield:textField];
    [self.view endEditing:YES];
    return YES;
}

- (void) saveTextFieldWithTextfield:(UITextField *) textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    NSMutableArray *textArray = [NSMutableArray array];
    if ([self.fieldCountDictionary.allKeys containsObject:@(index.row)]) {
        [textArray addObjectsFromArray: [self.fieldCountDictionary objectForKey:@(index.row)]];
    } else {
        for (int i=0; i<self.fieldArray.count; i++) {
            [textArray addObject:@""];
        }
    }
    [textArray removeObjectAtIndex:textField.tag-100];
    [textArray insertObject:textField.text atIndex:textField.tag-100];
    NSLog(@"textArray====%@",textArray);
    [self.fieldCountDictionary setObject:textArray forKey:@(index.row)];
}
#pragma mark - UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        //        BusinessSpecTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessSpecTopTableViewCell idString]];
        //        if (cell==nil) {
        BusinessSpecTopTableViewCell *cell = [[BusinessSpecTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[BusinessSpecTopTableViewCell idString]];
        //        }
        BusinessSpecListParser *listParser = self.specArray[indexPath.row];
        if ([self.selectedDictionary.allKeys containsObject:listParser.name]) {
            cell.selectedArray = [self.selectedDictionary objectForKey:listParser.name];
        }
        
        cell.delegate = self;
        cell.areaview.delegate = self;
        if (listParser) {
            cell.specParser = listParser;
            
        }
        
        NSLog(@"self.openOrCloseArray:::%@",self.openOrCloseArray[indexPath.row]);
        if ([self.openOrCloseArray[indexPath.row] integerValue]==1) {
            [cell upOrDownView:cell.upOrDownButton];
        }
        
        
        
        return cell;
    } else {
        if (indexPath.row==0) {
            NSString *idString = @"speccellsq";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"价格/库存";
            return cell;
        } else if (indexPath.row==1) {
            //批量填写
            //            BusinessPriceFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessPriceFieldTableViewCell idString]];
            //            if (cell==nil) {
            BusinessPriceFieldTableViewCell *fieldCell = [[BusinessPriceFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[BusinessPriceFieldTableViewCell idString]];
            //            }
            fieldCell.cellType = PriceFieldTableViewCellTypeBatch;
            if (self.fieldCountDictionary.count>indexPath.row-1) {
                fieldCell.editArray = [self.fieldCountDictionary objectForKey:@(indexPath.row)];
            }
            if (self.fieldArray.count) {
                fieldCell.fieldArray = self.fieldArray;
            }
            [fieldCell.batchWrite addTarget:self action:@selector(batchWriteAction:) forControlEvents:UIControlEventTouchUpInside];
            fieldCell.titleLabel.text = @"    批量填写：";
            [fieldCell.scanButton addTarget:self action:@selector(scanAction:) forControlEvents:UIControlEventTouchUpInside];
            for (UIView *view in fieldCell.contentView.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    ((UITextField *) view).delegate = self;
                }
            }
            
            return fieldCell;
            
            
            
        } else {
            //            BusinessPriceFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessPriceFieldTableViewCell idString]];
            //            if (cell==nil) {
            BusinessPriceFieldTableViewCell *fieldCell = [[BusinessPriceFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[BusinessPriceFieldTableViewCell idString]];
            //            }
            fieldCell.cellType = PriceFieldTableViewCellTypeNormal;
            if (self.fieldCountDictionary.count>indexPath.row-2) {
                fieldCell.editArray = [self.fieldCountDictionary objectForKey:@(indexPath.row)];
            }
            if (self.fieldArray.count) {
                fieldCell.fieldArray = self.fieldArray;
            }
            if (self.uniteArray.count+2>indexPath.row) {
                fieldCell.titleLabel.text = [NSString stringWithFormat:@"    %@",[self.uniteArray[indexPath.row-2] componentsJoinedByString:@":"]];
            }
            [fieldCell.scanButton addTarget:self action:@selector(scanAction:) forControlEvents:UIControlEventTouchUpInside];
            for (UIView *view in fieldCell.contentView.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    ((UITextField *) view).delegate = self;
                }
            }
            
            return fieldCell;
        }
        
        
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    
};
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.specArray.count) {
        return 2;
    } else {
        return 0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.specArray.count) {
        if (section==0) {
            return self.specArray.count;
        } else {
            if (self.uniteArray.count) {
                return self.uniteArray.count+2;
            } else {
                return 0;
            }
            
        }
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        BusinessSpecTopTableViewCell *cell = (BusinessSpecTopTableViewCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
        NSLog(@"BusinessSpecTopTableViewCell=%f",cell.cellHeight);
        return cell.cellHeight;
    } else {
        if (indexPath.row==0) {
            return 50;
        } else {
            BusinessPriceFieldTableViewCell *cell = (BusinessPriceFieldTableViewCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
            return cell.cellHeight;
        }
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 0.01;
    } else {
        return 10;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - BusinessSpecTopDelegate
-(void)changeHeight:(BusinessSpecListParser *)parser isOpen:(BOOL)open{
    NSUInteger index = 0;
    for (BusinessSpecListParser *listParser in self.specArray) {
        if ([listParser isEqual:parser]) {
            index = [self.specArray indexOfObject:listParser];
        }
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.openOrCloseArray removeObjectAtIndex:index];
    [self.openOrCloseArray insertObject:[NSString stringWithFormat:@"%d",open] atIndex:index];
    //    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    //    [self.tableView endUpdates];
}

#pragma mark - LogisticAreaViewDelegate
- (void)specParser:(BusinessSpecDetailParser *)specParser selected:(BOOL)isSelected{
    NSString *parentString;
    for (BusinessSpecListParser *listParser in self.specArray) {
        for (BusinessSpecDetailParser *parser in listParser.child) {
            if ([parser isEqual:specParser]) {
                parentString = listParser.name;
                break;
            }
        }
    }
    
    //已存储
    if ([self.selectedDictionary.allKeys containsObject:parentString]) {
        NSMutableArray *oldArray = [NSMutableArray arrayWithArray:[self.selectedDictionary objectForKey:parentString]];
        BOOL isInclude = NO;
        BusinessSpecDetailParser *tempParser;
        for (BusinessSpecDetailParser *thisParser in oldArray) {
            if ([thisParser.ID isEqualToString:specParser.ID]) {
                tempParser = thisParser;
                isInclude = YES;
                break;
            }
        }
        
        if (isSelected) {
            if (isInclude) {
                [oldArray removeObject:tempParser];
                [oldArray addObject:specParser];
                [self.selectedDictionary setObject:oldArray forKey:parentString];
            } else {
                [oldArray addObject:specParser];
                [self.selectedDictionary setObject:oldArray forKey:parentString];
            }
            
            
        } else {
            if (isInclude) {
                [oldArray removeObject:tempParser];
                if (oldArray.count) {
                    [self.selectedDictionary setObject:oldArray forKey:parentString];
                } else {
                    [self.selectedDictionary removeObjectForKey:parentString];
                }
            }
        }
    } else {
        
        //未存储
        if (isSelected) {
            [self.selectedDictionary setObject:@[specParser] forKey:parentString];
        }
    }
    
    //规格选择完整
    if (self.selectedDictionary.allKeys.count) {
        NSArray *tempArray = [NSArray arrayWithArray:self.uniteArray];
        NSLog(@" tempArray:::%@",tempArray);
        NSMutableArray* result = [NSMutableArray array];
        NSMutableArray *array = [NSMutableArray array];
        NSMutableArray *iDarray = [NSMutableArray array];
        for ( int i=0;i< self.specArray.count;i++) {
            BusinessSpecListParser *listParser = self.specArray[i];
            if ([self.selectedDictionary.allKeys containsObject:listParser.name]) {
                NSArray *temp = [self.selectedDictionary objectForKey:listParser.name];
                NSMutableArray *nowArray = [NSMutableArray array];
                NSMutableArray *nowIdArray = [NSMutableArray array];
                for (int j=0; j<temp.count; j++) {
                    BusinessSpecDetailParser *detailParser = temp[j];
                    [nowArray addObject:detailParser.name];
                    [nowIdArray addObject:detailParser.ID];
                }
                [array addObject:nowArray];
                [iDarray addObject:nowIdArray];
            }
        }
        
        [self.uniteArray removeAllObjects];
        [self.uniteIdArray removeAllObjects];
        if (array.count) {
            [self combine:result type:@"name" data:array curr:0 count:(int)array.count];
        }
        if (iDarray.count) {
            [self combine:result type:@"id" data:iDarray curr:0 count:(int)iDarray.count];
        }
        //    for (int i=0; i<self.uniteArray.count; i++) {
        //        NSArray *temp = self.uniteArray[i];
        //    }
        NSLog(@" self.uniteArray:::%@",self.uniteArray);
        //        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
        
    } else {
        [self.uniteArray removeAllObjects];
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    
}

@end
