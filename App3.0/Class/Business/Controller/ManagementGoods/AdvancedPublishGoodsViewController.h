//
//  AdvancedPublishGoodsViewController.h
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"
#import "BusinessClassifyModel.h"

@interface AdvancedPublishGoodsViewController : XSBaseTableViewController

/**平台分类*/
@property (nonatomic, strong) CategoryLowerDetailParser *categoryParser;

/**规格属性价格等*/
@property (nonatomic, strong) NSArray *specFieldArray;

/**商品信息->编辑时传值*/
@property (nonatomic, strong) BusinessProductDetailParser *productParser;

/**描述html*/
@property (nonatomic, copy) NSString *descHtmlString;

/**自定义分类*/
-(void)selectedCategoryWithCategoryParser:(BusinessClassifyDetailParser *)parser;
@end
