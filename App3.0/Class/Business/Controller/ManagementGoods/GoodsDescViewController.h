//
//  GoodsDescViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMWordView.h"

@interface GoodsDescViewController : XSBaseViewController
@property (nonatomic, strong) LMWordView *textView;

/**编辑时传的html*/
@property (nonatomic, copy) NSAttributedString *htmlString;

- (NSString *)exportHTML;
@end
