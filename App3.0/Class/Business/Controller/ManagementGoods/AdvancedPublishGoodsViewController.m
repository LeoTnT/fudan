//
//  AdvancedPublishGoodsViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AdvancedPublishGoodsViewController.h"
#import "AdvancePublishTopTableViewCell.h"
#import "ZYQAssetPickerController.h"
#import "ChooseCategoryViewController.h"
#import "GoodsStandardViewController.h"
#import "BusinessClassifyModel.h"
#import "BusinessModel.h"
#import "BusinessLogisticModel.h"
#import "CommonChooseView.h"
#import "GoodsManagementViewController.h"
#import "SetLogisticViewController.h"
#import "GoodsDescViewController.h"
#import "BusinessMainViewController.h"
#import "GoodsClassifyMainViewController.h"
#import "VideoHandleModel.h"
#import "BankCardTypeView.h"

@interface AdvancedPublishGoodsViewController ()<ZYQAssetPickerControllerDelegate,UINavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,AdvancePublishTopDelegate,CommonChooseViewDelegate,VideoHandleModelDelegate,UINavigationControllerDelegate,CardTypeDelegate>

@property (nonatomic, strong) UITextField *goodsName;
@property (nonatomic, strong) UILabel *categoryName;
@property (nonatomic, strong) UILabel *logisticLabel;
@property (nonatomic, strong) UILabel *categoryLabel;
//@property (nonatomic, strong) UISwitch *saleSwitch;
@property (nonatomic, strong) UIImageView *photoImage;
@property (nonatomic, strong) UIImageView *addPhotoImage;
@property (nonatomic, strong) NSMutableArray *photoArray;
/**图片控制器*/
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
/**视频控制器*/
@property(nonatomic,strong)UIImagePickerController *videoPickerVC;

@property (nonatomic, strong) UIImage *mainImage;

@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@property (nonatomic, assign) CGFloat currentY;

@property (nonatomic, strong) CommonChooseView *chooseView;

@property (nonatomic, strong) BusinessClassifyDetailParser *classifyParser;
@property (nonatomic, strong) BusinessLogisticListParser *logisticParser;
@property (nonatomic, strong) UILabel *specLabel;
@property (nonatomic, strong) BusinessGoodsConfigurationParser *configurationParser;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) NSMutableArray *logisticArray;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) GoodsDescViewController *descController;
@property (nonatomic, strong) GoodsStandardViewController *standardController;
@property (nonatomic, strong) GoodsClassifyMainViewController *classifyController;
@property (nonatomic, strong) NSDictionary *videoDictionary;
@property(nonatomic,strong) BankCardTypeView  *typeView;
@property (nonatomic, strong) BusinessGoodsSellTypeParser *typeParser;
@property (nonatomic, strong) UILabel *sellTypeLabel;
@end

@implementation AdvancedPublishGoodsViewController

#pragma mark - lazy loadding
- (BankCardTypeView *)typeView {
    if (!_typeView) {
        _typeView = [[BankCardTypeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.viewType = AlertTypeSellType;
        _typeView.typeDelegate = self;
    }
    return _typeView;
}
-(GoodsDescViewController *)descController {
    if (!_descController) {
        _descController = [[GoodsDescViewController alloc] init];
    }
    return _descController;
}

-(GoodsStandardViewController *)standardController {
    if (!_standardController) {
        _standardController = [[GoodsStandardViewController alloc] init];
    }
    return _standardController;
}

-(NSMutableArray *)categoryArray {
    if (!_categoryArray) {
        _categoryArray = [NSMutableArray array];
    }
    return _categoryArray;
}

-(NSMutableArray *)logisticArray {
    if (!_logisticArray) {
        _logisticArray = [NSMutableArray array];
    }
    return _logisticArray;
}

-(UITextField *)goodsName {
    if (!_goodsName) {
        _goodsName = [UITextField new];
        _goodsName.delegate = self;
        _goodsName.returnKeyType = UIReturnKeyDone;
        _goodsName.placeholder = @"商品名称";
    }
    return _goodsName;
}

-(UILabel *)categoryName {
    if (!_categoryName) {
        _categoryName = [UILabel new];
        _categoryName.textColor = [UIColor redColor];
        _categoryName.textAlignment = NSTextAlignmentRight;
        _categoryName.text = @"";
    }
    return _categoryName;
}

-(UILabel *)specLabel {
    if (!_specLabel) {
        _specLabel = [UILabel new];
        _specLabel.textColor = [UIColor redColor];
        _specLabel.textAlignment = NSTextAlignmentRight;
        _specLabel.text = @"";
    }
    return _specLabel;
}

-(UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [UIColor redColor];
        _descLabel.textAlignment = NSTextAlignmentRight;
        _descLabel.text = @"";
    }
    return _descLabel;
}

-(UILabel *)logisticLabel {
    if (!_logisticLabel) {
        _logisticLabel = [UILabel new];
        _logisticLabel.text = Localized(@"choose");
        _logisticLabel.textColor = [UIColor grayColor];
        _logisticLabel.textAlignment = NSTextAlignmentRight;
    }
    return _logisticLabel;
}

-(UILabel *)categoryLabel {
    if (!_categoryLabel) {
        _categoryLabel = [UILabel new];
        _categoryLabel.text = Localized(@"choose");
        _categoryLabel.textColor = [UIColor grayColor];
        _categoryLabel.textAlignment = NSTextAlignmentRight;
    }
    return _categoryLabel;
}

-(CommonChooseView *)chooseView {
    if (!_chooseView) {
        _chooseView = [[CommonChooseView alloc] init];
        _chooseView.chooseDelegate = self;
        
    }
    return _chooseView;
}

-(NSMutableArray *)photoArray {
    if (!_photoArray) {
        _photoArray = [NSMutableArray arrayWithObject:[UIImage imageNamed:@"user_fans_addphoto_thin"]];
    }
    return _photoArray;
}
-(UILabel *)sellTypeLabel {
    if (!_sellTypeLabel) {
        _sellTypeLabel = [UILabel new];
        _sellTypeLabel.text = Localized(@"choose");
        _sellTypeLabel.textColor = [UIColor grayColor];
        _sellTypeLabel.textAlignment = NSTextAlignmentRight;
    }
    return _sellTypeLabel;
}
#pragma mark - life circle
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.view.subviews containsObject:self.chooseView]) {
        //运费
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getFreightListsWithPage:@"1" limit:@"100" success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BusinessLogisticParser *parser = [BusinessLogisticParser mj_objectWithKeyValues:dic[@"data"]];
                if (parser.list.count) {
                    [self.logisticArray removeAllObjects];
                    [self.logisticArray addObjectsFromArray:parser.list];
                    self.chooseView.commonChooseViewType = CommonChooseViewTypeLogistic;
                    [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.view).with.mas_offset(mainHeight/3);
                        make.left.right.mas_equalTo(self.view);
                        make.bottom.mas_equalTo(self.view);
                    }];
                    self.chooseView.listArray = self.logisticArray;
                    
                }
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布商品";
    self.view.backgroundColor = BG_COLOR;
    self.autoHideKeyboard = YES;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;

    UIButton *publishButton = [UIButton new];
    [publishButton setTitle:Localized(@"exchange_skill_pulish") forState:UIControlStateNormal];
    [publishButton setBackgroundColor:mainColor];
    [publishButton addTarget:self action:@selector(publishAction) forControlEvents:UIControlEventTouchUpInside];
    publishButton.layer.cornerRadius = 0;
    publishButton.layer.masksToBounds = YES;
    [self.view addSubview:publishButton];
    
    CGFloat buttonHeight = 50;
    
    [publishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(buttonHeight);
    }];
    [self getPrepareInformation];
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //#efeff4
}
- (void)hiddenSellTypeView:(BusinessGoodsSellTypeParser *)selectedParser{
    //消失
    [self.typeView removeFromSuperview];
    if (selectedParser) {
        self.typeParser = selectedParser;
       self.specFieldArray = nil;
        
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:2];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    //    [self.tableView reloadData];
    
}
#pragma mark - private

-(void)setProductParser:(BusinessProductDetailParser *)productParser {
    _productParser = productParser;
    NSArray *listArray = [_productParser.image_list componentsSeparatedByString:@","];
    for (int i=0; i< listArray.count; i++) {
        NSData *logoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:listArray[i]]];
        UIImage *image = [UIImage imageWithData:logoData];
        if (image) {
            [self.photoArray insertObject:image atIndex:self.photoArray.count-1];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[index1] withRowAnimation:UITableViewRowAnimationNone];
            });
        }
        
        
    }
    self.goodsName.text = _productParser.product_name;
    self.categoryParser = [CategoryLowerDetailParser mj_objectWithKeyValues:@{@"cid":_productParser.category_id,@"cname":@" "}];
//    self.logisticParser = [BusinessLogisticListParser mj_objectWithKeyValues:@{@"id":_productParser.freight_id}];
    self.typeParser = [BusinessGoodsSellTypeParser mj_objectWithKeyValues:@{@"id":productParser.sell_type,@"name":productParser.sell_type_name?productParser.sell_type_name:@""}];
    [self.tableView reloadData];
}

- (void)getPrepareInformation {
    [HTTPManager supplyProductLogisticTypeId:self.productParser?self.productParser.product_id:@"" success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.configurationParser = [BusinessGoodsConfigurationParser mj_objectWithKeyValues:dic[@"data"]];
            if (self.typeParser) {
                for (BusinessGoodsSellTypeParser *parser in self.configurationParser.sell_type) {
                    if ([parser.ID isEqualToString:self.typeParser.ID]) {
                        self.typeParser.name = parser.name;
                        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
                        break;
                    }
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>=mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+35)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)keyboardDidShow {
    self.keyBoardIsShow=YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.keyBoardIsShow=NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}


-(void)setCategoryParser:(CategoryLowerDetailParser *)categoryParser {
    _categoryParser = categoryParser;
    self.specFieldArray = nil;
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)publishAction {
    [self.view endEditing:YES];
    if (self.productParser) {
        if (!self.descHtmlString) {
            self.descHtmlString = [self.configurationParser.product objectForKey:@"desc"];
        }
    }
    if (self.photoArray.count==1) {
        [XSTool showToastWithView:self.view Text:@"请选择商品图片"];
        return;
    }
    if (!self.goodsName.text.length||([self.goodsName.text isEqualToString:@""])) {
          [XSTool showToastWithView:self.view Text:@"请完善商品名称"];
        return;
    }
    
    if (self.categoryParser) {
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善商品类目"];
        return;
    }
    if (self.typeParser) {
        
    }else {
        [XSTool showToastWithView:self.view Text:@"请完善商品区域"];
        return;
    }
    if (self.specFieldArray.count) {
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善商品规格"];
        return;
    }
    
    if (!self.descLabel.text.length||[self.descHtmlString isEqualToString:@""]) {
        [XSTool showToastWithView:self.view Text:@"请完善商品描述"];
        return;
    }
    
    if (self.photoArray.count>1&&self.goodsName.text.length>0&&(![self.goodsName.text isEqualToString:@""])&&self.categoryParser&&self.specFieldArray.count&&self.descHtmlString.length&&(![self.descHtmlString isEqualToString:@""])) {
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
         NSError *error;
        NSData *specData = [NSJSONSerialization dataWithJSONObject:self.specFieldArray
                                                           options:kNilOptions
                                                             error:&error];
        NSString *specString = [[NSString alloc] initWithData:specData
                                                     encoding:NSUTF8StringEncoding];
        
        [param setValue:self.goodsName.text forKey:@"product_name"];
        [param setValue:self.descHtmlString forKey:@"desc"];
       
        [param setValue:self.categoryParser.cid forKey:@"category_id"];
        if (self.classifyParser) {
            [param setValue:self.classifyParser.category_id forKey:@"category_supply_id"];
        }
        if (self.logisticParser) {
            [param setValue:self.logisticParser.ID forKey:@"freight_id"];
            
        }
      
        [param setValue:specString forKey:@"spec_price"];
        NSMutableArray *tempPhoto = [NSMutableArray arrayWithArray:self.photoArray];
        [tempPhoto removeLastObject];
        NSDictionary *uploadParam=@{@"type":@"article",@"formname":@"file"};
        
        [XSTool showProgressHUDTOView:self.view withText:@"上传中"];
        [HTTPManager upLoadPhotosWithDic:uploadParam andDataArray:tempPhoto WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
            if ([dic[@"status"] integerValue]==1) {
                NSArray *tempArray = dic[@"data"];
                if (tempArray.count==tempPhoto.count) {
                    BusinessGoodsLogisticTypeParser *typeParser ;
                     BusinessGoodsSellTypeParser *sellTypeParser;
                    if (self.configurationParser) {
                        typeParser = [self.configurationParser.logistics_type firstObject];
                        sellTypeParser = [self.configurationParser.sell_type firstObject];
                    }
                    NSError *error;
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempArray
                                                                       options:kNilOptions
                                                                         error:&error];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                                 encoding:NSUTF8StringEncoding];
                    [param setValue:jsonString forKey:@"image"];
                    [param setValue:self.typeParser.ID forKey:@"sell_type"];
                    [param setValue:typeParser?typeParser.ID:@"1" forKey:@"logistics_type"];
                    
                    if (self.videoDictionary) {
                        
                        //有视频
                        [HTTPManager upLoadDataIsVideo:YES WithDic:@{@"type":@"v_fans",@"formname":@"file"} andData:self.videoDictionary[@"videoData"] WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                            if ([dic[@"status"] integerValue]==1) {
                                NSLog(@"dic=video =%@",dic);
                                [param setValue:[NSString stringWithFormat:@"%@",dic[@"data"][@"video"] ]forKey:@"video"];
                               
                                    [self submitPublishInformationWithParam:param];
                                
                            } else {
                                [XSTool hideProgressHUDWithView:self.view];
                                [XSTool showToastWithView:self.view Text:dic[@"info"]];
                            }
                        } fail:^(NSError * _Nonnull error) {
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:NetFailure];
                        }];
                    } else {
                            [self submitPublishInformationWithParam:param];
                        
                    }  
                }
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        }fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善信息"];
    }
    
    
}

- (void) submitPublishInformationWithParam:(NSMutableDictionary *) param {
    if (self.productParser) {
        
        //编辑
        [param setValue:self.productParser.product_id forKey:@"id"];
        [HTTPManager supplyProductUpdateWithParam:param success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"成功提交"];
                if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[BusinessMainViewController class]]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[GoodsManagementViewController class]]) {
                    GoodsManagementViewController *controller = (GoodsManagementViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                    controller.isChange = YES;
                    [self.navigationController popToViewController:controller animated:YES];
                }
            } else {
                NSLog(@"%@",state.info);
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [HTTPManager supplyProductInsertWithParam:param success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"成功提交"];
                if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[BusinessMainViewController class]]) {
                    GoodsManagementViewController *controller = [[GoodsManagementViewController alloc] init];
                    controller.isChange = YES;
                    [self.navigationController pushViewController:controller animated:YES];
                }
                if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[GoodsManagementViewController class]]) {
                    GoodsManagementViewController *controller = (GoodsManagementViewController *) self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
                    controller.isChange = YES;
                    [self.navigationController popToViewController:controller animated:YES];
                }
                
                
            } else {
                NSLog(@"%@",state.info);
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
}


- (void)choosePhotos {
    [self.view endEditing:YES];
    if (self.chooseView) {
        [self.chooseView removeFromSuperview];
    }
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takePhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //打开相册
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    if([picker isEqual:self.imagePickVC]){
        
        //获取原始照片
        UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        [self.photoArray insertObject:image atIndex:self.photoArray.count-1];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        //刷新表格
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
    } else {
        //视频路径
        NSURL *url=[info objectForKey:@"UIImagePickerControllerMediaURL"];
        VideoHandleModel *model=[[VideoHandleModel alloc]init];
        model.delegate=self;
        [model getVideoFirstImageAndDataAndUrlWithVideoUrl:url];
        
    }
    
  
}

- (void) takeVideo {
    
//    UIAlertController * alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"视频" preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction * firstAction = [UIAlertAction actionWithTitle:@"从相册获取视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        [self choosevideo];
//    }];
    
//    UIAlertAction * secondAction = [UIAlertAction actionWithTitle:@"录制视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
        [self video];
//    }];
    
//    [alert addAction:firstAction];
//    [alert addAction:secondAction];
//    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)choosevideo
{
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
    NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
    ipc.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
    [self presentViewController:ipc animated:YES completion:nil];
    ipc.delegate = self;//设置委托
    
}

//- (void)openLocalCamera{
//
//    UIImagePickerController * picker = [[UIImagePickerController alloc]init];
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    //录制视频时长，默认10s
//    picker.videoMaximumDuration = 20;
//
//    //相机类型（拍照、录像...）这里表示我们打开相机支持的是相机和录像两个功能。
//    picker.mediaTypes = @[(NSString *)kUTTypeMovie];
//    picker.delegate = self;
//    picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
//
//    //设置摄像头模式（拍照，录制视频）为相机模式
//    //    UIImagePickerControllerCameraCaptureModeVideo  这个是设置为视频模式
//    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
//    [self presentViewController:picker animated:YES completion:nil];
//
//}

- (void)takePhoto {
    
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}

-(void)video{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.videoPickerVC=[[UIImagePickerController alloc] init];
    //数据源
    self.videoPickerVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    self.videoPickerVC.mediaTypes=@[(NSString *)kUTTypeMovie];
    //展示拍照控板
    self.videoPickerVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.videoPickerVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModeVideo;
    //后置摄像头
    self.videoPickerVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.videoPickerVC.delegate=self;
    //视频最大时间
    self.videoPickerVC.videoMaximumDuration=15;
    self.videoPickerVC.videoQuality=UIImagePickerControllerQualityTypeMedium;
    [self presentViewController:self.videoPickerVC animated:YES completion:nil];
}

#pragma mark - VideoHandleModelDelegate
-(void)sendVideoDataWithDic:(NSDictionary *)videoDic {
    self.videoDictionary = videoDic;
  
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.videoPickerVC dismissViewControllerAnimated:NO completion:nil];
         [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
      
    });
}

#pragma mark - ZYQAssetPickerController Delegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            [self.photoArray insertObject:result atIndex:self.photoArray.count-1];
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择九张图片"];
}


- (void)deletePicture:(UIButton *)button {
    AdvancePublishTopTableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    UIImage *image =  self.photoArray[index];
    if ([self.mainImage isEqual:image]) {
        self.mainImage = nil;
    }
    [self.photoArray removeObjectAtIndex:index];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"advanceCell11";
    if (indexPath.section==0) {
        idString = @"advanceCell2";
        AdvancePublishTopTableViewCell *advanceCell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (advanceCell==nil) {
            advanceCell = [[AdvancePublishTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            
        }
        advanceCell.delegate = self;
        if (self.mainImage) {
            advanceCell.mainImageView.image = self.mainImage;
        }
        advanceCell.photosArray = self.photoArray;
        //给cell的删除按钮绑定方法
        for (int i=0; i<advanceCell.deletBtnArray.count; i++) {
            [[advanceCell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
        }
        [advanceCell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos)]];
        [advanceCell.photoButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos)]];
        
        return advanceCell;
        
    } else if (indexPath.section==1) {
        idString = @"titlecell111";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (indexPath.row==0) {
            [cell.contentView addSubview:self.goodsName];
            [self.goodsName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(cell.contentView);
                make.left.mas_equalTo(13);
                make.width.mas_equalTo(mainWidth-13*2);
            }];
        } else {
            cell.textLabel.text = @"类目";
            
            [cell.contentView addSubview:self.categoryName];
            [self.categoryName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(cell.contentView).with.mas_offset(60);
                make.right.mas_equalTo(cell.contentView);
                make.top.bottom.mas_equalTo(cell.contentView);
            }];
            //            self.categoryName.backgroundColor = [UIColor yellowColor];
            self.categoryName.text = self.categoryParser?@"已编辑":@"";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
        
    } else if (indexPath.section==2) {
        idString = @"speccell111";
        if (indexPath.row==0) {
            idString = @"aresss111";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = @"商品区域";
            
            [cell.contentView addSubview:self.sellTypeLabel];
            self.sellTypeLabel.text = self.typeParser?self.typeParser.name: Localized(@"choose");
            [self.sellTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell.contentView).with.mas_offset(-20);
                make.width.mas_equalTo(mainWidth-100);
                make.height.mas_equalTo(cell.contentView);
                make.top.mas_equalTo(cell.contentView);
            }];
            return cell;
        } else if (indexPath.row==1) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"商品规格";
            [cell.contentView addSubview:self.specLabel];
            [self.specLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(cell.contentView).with.mas_offset(100);
                make.right.mas_equalTo(cell.contentView);
                make.top.bottom.mas_equalTo(cell.contentView);
            }];
            //            self.categoryName.backgroundColor = [UIColor yellowColor];
            self.specLabel.text = self.specFieldArray.count?@"已编辑":@"";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        } else if (indexPath.row==2){
            idString = @"spec222";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = @"商品描述";
            [cell.contentView addSubview:self.descLabel];
            [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(cell.contentView).with.mas_offset(100);
                make.right.mas_equalTo(cell.contentView);
                make.top.bottom.mas_equalTo(cell.contentView);
            }];
            self.descLabel.text = self.descHtmlString?@"已编辑":@"";
            if (!self.descHtmlString) {
                if (self.productParser) {
                    self.descLabel.text = @"已编辑";
                }
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        } else {
             idString = @"spec333";
            //添加视频
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = @"添加视频（选填）";
            if (self.videoDictionary.allKeys.count==3) {
                cell.detailTextLabel.text = @"已编辑";
                cell.detailTextLabel.textColor = [UIColor redColor];
            }
            return cell;
            
        }
        
    } else {
        idString = @"logistic111";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row==0) {
            cell.textLabel.text = @"运费(选填)";
            
            [cell.contentView addSubview:self.logisticLabel];
            self.logisticLabel.text = self.logisticParser?self.logisticParser.freight_name: Localized(@"choose");
            [self.logisticLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell.contentView).with.mas_offset(-20);
                make.width.mas_equalTo(mainWidth-100);
                make.height.mas_equalTo(cell.contentView);
                make.top.mas_equalTo(cell.contentView);
            }];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            cell.textLabel.text = @"店内分类(选填)";
            [cell.contentView addSubview:self.categoryLabel];
            self.categoryLabel.text = self.classifyParser?self.classifyParser.category_name: Localized(@"choose");
            [self.categoryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell.contentView).with.mas_offset(-20);
                make.width.mas_equalTo(mainWidth-120);
                make.height.mas_equalTo(cell.contentView);
                make.top.mas_equalTo(cell.contentView);
            }];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    } else if(section==2){
        return 4;
    } else {
        return 2;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section<3) {
        return 0.01;
    } else {
        return 10;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 0.01;
    } else {
        return 10;
    }
};

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        AdvancePublishTopTableViewCell *cell = (AdvancePublishTopTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.height;
    } else {
        return 45;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    
    if (indexPath.section==1&&indexPath.row==1) {
        
        //平台分类界面
        ChooseCategoryViewController *chooseController = [[ChooseCategoryViewController alloc] init];
        [self.navigationController pushViewController:chooseController animated:YES];
    }
    if (indexPath.section==2) {
        if (indexPath.row==0) {
            [self.view endEditing:YES];
            if (self.typeParser.ID) {
                self.typeView.selectedId = self.self.typeParser.ID;
            }
            self.typeView.typeArray = [NSMutableArray arrayWithArray:self.configurationParser.sell_type];
            
            [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView] ;
            
            
        } else if (indexPath.row==1) {
            if (self.categoryParser&&self.typeParser) {
                
                //商品规格界面
                GoodsStandardViewController *standardController = [[GoodsStandardViewController alloc] init];
                standardController.categoryId = self.categoryParser.cid;
                standardController.sellType = self.typeParser.ID;
                standardController.operateType = self.productParser.product_id?@"edit":@"add";
                [self.navigationController pushViewController:standardController animated:YES];
                
            } else {
                [XSTool showToastWithView:self.view Text:self.categoryParser?@"请先选择商品区域":@"请先选择分类"];
            }
        } else if (indexPath.row==2) {
            
            //商品描述
            if (!self.descHtmlString) {
                if (self.productParser) {
                    
                    //后台传值
                    NSString *htmlString = [self.configurationParser.product objectForKey:@"desc"];
                    self.descController.htmlString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                }
                
            }
            
            [self.navigationController pushViewController:self.descController animated:YES];
            
        } else {
            [self takeVideo];
        }
    }
    if (indexPath.section==3) {
        if (indexPath.row==0) {
            //            if (self.logisticArray.count) {
            //                self.chooseView.titleLabel.text = @"运费选择";
            //                [self.chooseView.oneButton setTitle:@"新建运费" forState:UIControlStateNormal];
            //                self.chooseView.commonChooseViewType = CommonChooseViewTypeLogistic;
            //                [self.view addSubview:self.chooseView];
            //                [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
            //                    make.top.mas_equalTo(self.view).with.mas_offset(mainHeight/3);
            //                    make.left.right.mas_equalTo(self.view);
            //                    make.bottom.mas_equalTo(self.view);
            //                }];
            //                self.chooseView.listArray = self.logisticArray;
            //
            //            } else {
            
            //运费
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager getFreightListsWithPage:@"1" limit:@"100" success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    BusinessLogisticParser *parser = [BusinessLogisticParser mj_objectWithKeyValues:dic[@"data"]];
                    [self.logisticArray removeAllObjects];
                    [self.logisticArray addObjectsFromArray:parser.list];
                    self.chooseView.titleLabel.text = @"运费选择";
                    [self.chooseView.oneButton setTitle:@"新建运费" forState:UIControlStateNormal];
                    self.chooseView.commonChooseViewType = CommonChooseViewTypeLogistic;
                    [self.view addSubview:self.chooseView];
                    if (self.logisticArray.count) {
                        [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.view).with.mas_offset(mainHeight/3);
                            make.left.right.mas_equalTo(self.view);
                            make.bottom.mas_equalTo(self.view);
                        }];
                        
                    } else {
                        [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.view).with.mas_offset(mainHeight-100);
                            make.left.right.mas_equalTo(self.view);
                            make.bottom.mas_equalTo(self.view);
                        }];
                    }
                    self.chooseView.listArray = self.logisticArray;
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        } else {
            self.classifyController = [[GoodsClassifyMainViewController alloc] init];
            self.classifyController.firstClassifyType = FirstClassifyTypePublishGoods;
            [self.navigationController pushViewController:self.classifyController animated:YES];
            
        }
    }
    
}

#pragma mark -  AdvancePublishTopDelegate
-(void)mainImageWithImage:(UIImage *)image {
    self.mainImage = image;
}

#pragma mark - CommonChooseViewDelegate
-(void)closeCommonChooseView {
    if (self.chooseView) {
        [self.chooseView removeFromSuperview];
        //        self.chooseView = nil;
    }
}

-(void)selectedCategoryWithCategoryParser:(BusinessClassifyDetailParser *)parser {
    self.classifyParser = parser;
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:3];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    
}

-(void)selectedLogisticWithLogisticParser:(BusinessLogisticListParser *)parser {
    self.logisticParser = parser;
    [self.chooseView removeFromSuperview];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:3];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)toNewViewWithType:(NSUInteger)type {
    if (type==1) {
        SetLogisticViewController *controller = [[SetLogisticViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else {
        //编辑分类
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置分类名" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        //添加按钮
        __weak typeof(alertController) weakAlert = alertController;
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //获取输入的昵称
            NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
            if ([weakAlert.textFields.lastObject text].length==0) {
                [XSTool showToastWithView:self.view Text: @"分类名不能为空"];
            } else {
                if ([self stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                    [XSTool showToastWithView:self.view Text:@"你好，分类名不支持表情"];
                } else {
//                    [XSTool showProgressHUDWithView:self.view];
                    [HTTPManager supplyCategoryInsertWithParam:@{@"parent_id":@"0",@"category_name":[weakAlert.textFields.lastObject text]}  success:^(NSDictionary *dic, resultObject *state) {
                      
                        if (state.status) {
                            [XSTool showProgressHUDTOView:self.view withText:Localized(@"add_success")];
                            [HTTPManager supplyCategoryListsWithParam:@{@"parent_id":@"0",@"tree":@"no"} success:^(NSDictionary *dic, resultObject *state) {
                                [XSTool hideProgressHUDWithView:self.view];
                                if (state.status) {
                                    BusinessClassifyParser *parser = [BusinessClassifyParser mj_objectWithKeyValues:dic];
                                    [self.categoryArray removeAllObjects];
                                    if (parser.data.count) {
                                        
                                        [self.categoryArray addObjectsFromArray:parser.data];
                                        self.chooseView.commonChooseViewType = CommonChooseViewTypeCategory;
                                            [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
                                                make.top.mas_equalTo(self.view).with.mas_offset(mainHeight/3);
                                                make.left.right.mas_equalTo(self.view);
                                                make.bottom.mas_equalTo(self.view);
                                            }];
                                            
                                        self.chooseView.listArray = self.categoryArray;
                                    }
                                } else {
                                    [XSTool showToastWithView:self.view Text:state.info];
                                }
                                
                            } fail:^(NSError *error) {
                                 [XSTool hideProgressHUDWithView:self.view];
                                [XSTool showToastWithView:self.view Text:NetFailure];
                            }];
                            
                        } else {
                             [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError *error) {
                        [XSTool showProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:NetFailure];
                    }];
                    
                    
                }
            }
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            NSLog(@"点击了取消按钮");
        }]];
        
        // 添加文本框
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.textColor = [UIColor blackColor];
            textField.delegate = self;
            //        textField.tag = 1500;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        // 弹出对话框
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.markedTextRange == nil) {
        if (textField.text.length >10) {
            [XSTool showToastWithView:self.view Text:@"分类长度只能为1~10"];
            textField.text = [textField.text substringToIndex:10];
        }
    }
}

- (BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UITableViewCell *cell =(UITableViewCell *)textField.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

@end
