//
//  GoodsManagementViewController.h
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, GoodsManagementType) {
    GoodsManagementTypeNormal,
    GoodsManagementTypeEdit
};

@interface GoodsManagementViewController : XSBaseViewController
@property (nonatomic, assign) GoodsManagementType goodsManagementType;
@property (nonatomic, assign) BOOL isChange;
@property (nonatomic, strong) UIButton *selectedAllButton;
@property (nonatomic, strong) NSArray *segmentTitles;
-(instancetype) initWithGoodsManagementType:(GoodsManagementType ) goodsManagementType;
- (void)clickSelectedAllButtonAction:(UIButton *)sender;
@end
