//
//  GoodsManagementViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsManagementViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "BusinessGoodsViewController.h"
#import "AdvancedPublishGoodsViewController.h"
#import "GoodsClassifyMainViewController.h"


@interface GoodsManagementViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, assign) CGFloat segmentHeight;
@property (nonatomic, strong) BusinessGoodsViewController *soldGoodsController;
@property (nonatomic, strong) BusinessGoodsViewController *checkingGoodsController;
@property (nonatomic, strong) BusinessGoodsViewController *unShelveGoodsController;
@property (nonatomic, assign) CGFloat bottomHeight;

@property (nonatomic, strong)  UIButton *soldOrUnsoldButton;
@property (nonatomic, assign) NSUInteger segmentIndex;

@property (nonatomic, assign) NSUInteger soldNum;
@property (nonatomic, assign) NSUInteger checkingNum;
@property (nonatomic, assign) NSUInteger unSoldNum;
@end

@implementation GoodsManagementViewController
#pragma mark - lazy lodding
- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.segmentHeight, mainWidth, mainHeight-self.segmentHeight-50)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*3, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.segmentHeight)];
        _segmentControl.sectionTitles = @[@"出售中(0)",@"待审核(0)",@"已下架(0)"];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        //        _segmentControl.borderType = HMSegmentedControlBorderTypeRight;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:16],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

-(BusinessGoodsViewController *)soldGoodsController {
    if (!_soldGoodsController) {
        _soldGoodsController = [[BusinessGoodsViewController alloc] initWithBusinessGoodsType:BusinessGoodsTypeSold];
        _soldGoodsController.view.frame = CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
        
    }
    return _soldGoodsController;
}

-(BusinessGoodsViewController *)checkingGoodsController {
    if (!_checkingGoodsController) {
        _checkingGoodsController = [[BusinessGoodsViewController alloc] initWithBusinessGoodsType:BusinessGoodsTypeChecking];
        _checkingGoodsController.view.frame = CGRectMake(mainWidth, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
        
    }
    return _checkingGoodsController;
}

-(BusinessGoodsViewController *)unShelveGoodsController {
    if (!_unShelveGoodsController) {
        _unShelveGoodsController = [[BusinessGoodsViewController alloc] initWithBusinessGoodsType:BusinessGoodsTypeUnShelve];
        _unShelveGoodsController.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
        
    }
    return _unShelveGoodsController;
}


#pragma mark - life circle
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.segmentHeight = 50;
    self.segmentIndex = 0;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"商品管理";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.segmentHeight-0.5, mainWidth, 0.5)];
    for (int i=1; i<3; i++) {
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(mainWidth/3*i, 10, 1, self.segmentHeight-20)];
        line1.backgroundColor = LINE_COLOR;
        [self.view addSubview:line1];
        
    }
    lineView.backgroundColor = BG_COLOR;
    [self.view addSubview:lineView];
    
    self.bottomHeight = 50;
    //底部菜单
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, MAIN_VC_HEIGHT-self.bottomHeight, mainWidth, self.bottomHeight)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    UILabel *topLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.5)];
    topLine.backgroundColor = LINE_COLOR;
    [bottomView addSubview:topLine];
    UIButton *addGoodsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth/2, self.bottomHeight)];
    [addGoodsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addGoodsButton setTitle:@"添加新的产品" forState:UIControlStateNormal];
    [addGoodsButton setImage:[UIImage imageNamed:@"business_add"] forState:UIControlStateNormal];
    addGoodsButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [addGoodsButton addTarget:self action:@selector(addGoodsAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:addGoodsButton];
    UIButton *addClassifyButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2, 0, mainWidth/2, self.bottomHeight)];
    [addClassifyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addClassifyButton setTitle:@"分类管理" forState:UIControlStateNormal];
    [addClassifyButton addTarget:self action:@selector(addClassifyAction) forControlEvents:UIControlEventTouchUpInside];
    [addClassifyButton setImage:[UIImage imageNamed:@"business_manage"] forState:UIControlStateNormal];
    addClassifyButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [bottomView addSubview:addClassifyButton];
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2-0.5, NORMOL_SPACE, 1, self.bottomHeight-2*NORMOL_SPACE)];
    line.backgroundColor = LINE_COLOR;
    [bottomView addSubview:line];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"edit") action:^{
        @strongify(self);
        if (self.unSoldNum==0&&self.soldNum==0&&self.checkingNum==0) {
            [XSTool showToastWithView:self.view Text:@"暂无商品,不可编辑！"];
        } else {
            self.navRightBtn.selected = !self.navRightBtn.selected;
            if (self.navRightBtn.selected) {
                self.goodsManagementType = GoodsManagementTypeEdit;
                
            } else {
                self.goodsManagementType = GoodsManagementTypeNormal;
            }
            for (UIView *tempView in bottomView.subviews) {
                [tempView removeFromSuperview];
            }
            if (self.navRightBtn.selected) {
                for (UIView *view in bottomView.subviews) {
                    [view removeFromSuperview];
                }
                self.selectedAllButton = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, self.bottomHeight/2-NORMOL_SPACE, NORMOL_SPACE*8, NORMOL_SPACE*2)];
                [self.selectedAllButton setTitle:@"全选" forState:UIControlStateNormal];
                [self.selectedAllButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [self.selectedAllButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
                [self.selectedAllButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
                [self.selectedAllButton addTarget:self action:@selector(clickSelectedAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [self.selectedAllButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
                
                [bottomView addSubview:self.selectedAllButton];
                UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.5)];
                line.backgroundColor = LINE_COLOR;
                [bottomView addSubview:line];
                CGFloat buttonWidth = (mainWidth/3*2-4*NORMOL_SPACE)/3;
                for (int i=0; i<3; i++) {
                    UIButton *tempButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/3+ NORMOL_SPACE*(i+1)+buttonWidth*i, NORMOL_SPACE, buttonWidth, self.bottomHeight-2*NORMOL_SPACE)];
                    tempButton.tag = 2000+i;
                    [tempButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    tempButton.layer.cornerRadius = 3;
                    tempButton.layer.borderWidth = 0.5;
                    tempButton.layer.borderColor = [UIColor grayColor].CGColor;
                    tempButton.layer.masksToBounds = YES;
                    if (i==0) {
                        [tempButton setTitle:Localized(@"undercarriage") forState:UIControlStateNormal];
                        [tempButton setTitle:Localized(@"up_self") forState:UIControlStateSelected];
                        [tempButton addTarget:self action:@selector(unSoldProducts:) forControlEvents:UIControlEventTouchUpInside];
                        
                        if (self.segmentControl.selectedSegmentIndex==0) {
                            tempButton.hidden = NO;
                            tempButton.selected = NO;
                            
                        } else if (self.segmentControl.selectedSegmentIndex==1) {
                            tempButton.hidden = YES;
                        } else {
                            [tempButton setTitle:Localized(@"up_self") forState:UIControlStateNormal];
                            tempButton.selected = YES;
                            tempButton.hidden = NO;
                        }
                        self.soldOrUnsoldButton = tempButton;
                        
                    } else if (i==1) {
                        [tempButton setTitle:@"分类至" forState:UIControlStateNormal];
                        [tempButton addTarget:self action:@selector(updateCategoryProducts) forControlEvents:UIControlEventTouchUpInside];
                    } else {
                        [tempButton setTitle:@"删除" forState:UIControlStateNormal];
                        [tempButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                        tempButton.layer.borderWidth = 1;
                        tempButton.layer.borderColor = [UIColor redColor].CGColor;
                        [tempButton addTarget:self action:@selector(deleteProducts) forControlEvents:UIControlEventTouchUpInside];
                    }
                    [bottomView addSubview:tempButton];
                }
            } else {
                UILabel *topLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.5)];
                topLine.backgroundColor = LINE_COLOR;
                [bottomView addSubview:topLine];
                UIButton *addGoodsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth/2, self.bottomHeight)];
                [addGoodsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [addGoodsButton setTitle:@"添加新的产品" forState:UIControlStateNormal];
                [addGoodsButton setImage:[UIImage imageNamed:@"business_add"] forState:UIControlStateNormal];
                addGoodsButton.titleLabel.font = [UIFont systemFontOfSize:15];
                [addGoodsButton addTarget:self action:@selector(addGoodsAction) forControlEvents:UIControlEventTouchUpInside];
                [bottomView addSubview:addGoodsButton];
                UIButton *addClassifyButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2, 0, mainWidth/2, self.bottomHeight)];
                [addClassifyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [addClassifyButton setTitle:@"分类管理" forState:UIControlStateNormal];
                [addClassifyButton addTarget:self action:@selector(addClassifyAction) forControlEvents:UIControlEventTouchUpInside];
                [addClassifyButton setImage:[UIImage imageNamed:@"business_manage"] forState:UIControlStateNormal];
                addClassifyButton.titleLabel.font = [UIFont systemFontOfSize:15];
                [bottomView addSubview:addClassifyButton];
                UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2-0.5, NORMOL_SPACE, 1, self.bottomHeight-2*NORMOL_SPACE)];
                line.backgroundColor = LINE_COLOR;
                [bottomView addSubview:line];
                
            }
        }

        [self setViewControllers];
        
    }];
    [self.navRightBtn setTitle:Localized(@"完成") forState:UIControlStateSelected];
    [self.navRightBtn setTitleColor:mainColor forState:UIControlStateSelected];
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setViewControllers];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)unSoldProducts:(UIButton *) sender{
    if (sender.selected) {
       
        if (self.segmentControl.selectedSegmentIndex==0) {
            [self.soldGoodsController soldProducts];
        } else {
            [self.unShelveGoodsController soldProducts];
        }
    } else {
        if (self.segmentControl.selectedSegmentIndex==0) {
            [self.soldGoodsController unSoldProducts];
        } else {
            [self.unShelveGoodsController unSoldProducts];
        }
    }
    
}

- (void)updateCategoryProducts{
    if (self.segmentControl.selectedSegmentIndex==0) {
        [self.soldGoodsController updateCategoryProducts];
    } else {
        [self.unShelveGoodsController updateCategoryProducts];
    }
}

- (void)deleteProducts{
    if (self.segmentControl.selectedSegmentIndex==0) {
        [self.soldGoodsController deleteProducts];
    } else {
        [self.unShelveGoodsController deleteProducts];
    }
}

- (void)clickSelectedAllButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self.soldGoodsController clickSelectedAllButtonAction:sender];
    [self.unShelveGoodsController clickSelectedAllButtonAction:sender];
}

- (void)addGoodsAction {
    AdvancedPublishGoodsViewController *publishController = [[AdvancedPublishGoodsViewController alloc] init];
    [self.navigationController pushViewController:publishController animated:YES];
}

- (void)addClassifyAction {
    GoodsClassifyMainViewController *classifyController = [[GoodsClassifyMainViewController alloc] init];
    [self.navigationController pushViewController:classifyController animated:YES];
}


-(void)setSegmentTitles:(NSArray *)segmentTitles {
    _segmentTitles = segmentTitles;
    self.segmentControl.sectionTitles = _segmentTitles;
};

-(instancetype)initWithGoodsManagementType:(GoodsManagementType)goodsManagementType {
    if (self=[super init]) {
        self.goodsManagementType = goodsManagementType;
    }
    return self;
}
-(void)setIsChange:(BOOL)isChange {
    _isChange = isChange;
    if (_isChange) {
        for (BusinessGoodsViewController *controller in self.childViewControllers) {
            [controller uploadGoodsInformation];
            [self refreshProductCount];
        }
    }
    
}

- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
    self.segmentIndex = (long)segmentedControl.selectedSegmentIndex;
     [self isSoldGoods];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
     self.segmentIndex = page;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    [self isSoldGoods];
}

- (void)isSoldGoods {
    if (self.segmentIndex==0) {
        [self.soldOrUnsoldButton setTitle:Localized(@"undercarriage") forState:UIControlStateNormal];
        self.soldOrUnsoldButton.selected = NO;
        self.soldOrUnsoldButton.hidden = NO;
    } else if (self.segmentIndex==1) {
        self.soldOrUnsoldButton.hidden = YES;
    } else {
        [self.soldOrUnsoldButton setTitle:Localized(@"up_self") forState:UIControlStateNormal];
        self.soldOrUnsoldButton.selected = YES;
        self.soldOrUnsoldButton.hidden = NO;
    }
}

- (void)refreshProductCount {
    __block  NSString *soldNumber,*unSoldNumber,*checkingNumber;
    if (self.goodsManagementType==GoodsManagementTypeNormal) {
        self.soldGoodsController.editType = BusinessGoodsEditTypeNormal;
        self.checkingGoodsController.editType = BusinessGoodsEditTypeNormal;
        self.unShelveGoodsController.editType = BusinessGoodsEditTypeNormal;
        
    } else {
        self.soldGoodsController.editType = BusinessGoodsEditTypeEdit;
        self.checkingGoodsController.editType = BusinessGoodsEditTypeNormal;
        self.unShelveGoodsController.editType = BusinessGoodsEditTypeEdit;
    }
    [self.soldGoodsController returnNumber:^(NSString *number) {
        soldNumber = number;
        self.segmentControl.sectionTitles = @[[NSString stringWithFormat:@"出售中(%@)",soldNumber?soldNumber:@"0"],[NSString stringWithFormat:@"待审核(%@)",checkingNumber?checkingNumber:@"0"],[NSString stringWithFormat:@"已下架(%@)",unSoldNumber?unSoldNumber:@"0"]];
        self.soldNum = [number integerValue];
        self.segmentControl.selectedSegmentIndex = self.segmentIndex;
        [self segmentedChangedValue:self.segmentControl];
    }];
    
    [self.checkingGoodsController returnNumber:^(NSString *number) {
        checkingNumber = number;
        self.segmentControl.sectionTitles = @[[NSString stringWithFormat:@"出售中(%@)",soldNumber?soldNumber:@"0"],[NSString stringWithFormat:@"待审核(%@)",checkingNumber?checkingNumber:@"0"],[NSString stringWithFormat:@"已下架(%@)",unSoldNumber?unSoldNumber:@"0"]];
        self.checkingNum = [number integerValue];
        self.segmentControl.selectedSegmentIndex = self.segmentIndex;
        [self segmentedChangedValue:self.segmentControl];
    }];
    
    [self.unShelveGoodsController returnNumber:^(NSString *number) {
        unSoldNumber = number;
        self.segmentControl.sectionTitles = @[[NSString stringWithFormat:@"出售中(%@)",soldNumber?soldNumber:@"0"],[NSString stringWithFormat:@"待审核(%@)",checkingNumber?checkingNumber:@"0"],[NSString stringWithFormat:@"已下架(%@)",unSoldNumber?unSoldNumber:@"0"]];
        self.unSoldNum = [number integerValue];
        self.segmentControl.selectedSegmentIndex = self.segmentIndex;
        [self segmentedChangedValue:self.segmentControl];
    }];
}

- (void)setViewControllers{
    [self addChildViewController: self.soldGoodsController];
    [self.scrollView addSubview: self.soldGoodsController.view];
    [self addChildViewController: self.checkingGoodsController];
    [self.scrollView addSubview: self.checkingGoodsController.view];
    [self addChildViewController: self.unShelveGoodsController];
    [self.scrollView addSubview:self.unShelveGoodsController.view];
    [self refreshProductCount];
}

@end
