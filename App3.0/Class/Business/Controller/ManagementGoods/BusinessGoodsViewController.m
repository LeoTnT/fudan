//
//  BusinessGoodsViewController.m
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessGoodsViewController.h"
#import "BusinessDefaultView.h"
#import "BusinessGoodsTableViewCell.h"
#import "CommonBottomView.h"
#import "BusinessModel.h"
#import "GoodsManagementViewController.h"
#import "BusinessEditGoodsTableViewCell.h"
#import "AdvancedPublishGoodsViewController.h"
#import "CommonChooseView.h"
#import "XSShareView.h"
#import "GoodsClassifyMainViewController.h"


@interface BusinessGoodsViewController ()<CommonChooseViewDelegate,BusinessGoodsDelegate,BusinessEditDelegate,UITextFieldDelegate>
@property (nonatomic, assign) BusinessGoodsType businessGoodsType;
@property (nonatomic, strong) NSMutableArray *goodsArray;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) UIButton *selectedAllButton;
@property (nonatomic, strong) NSMutableArray *seletedProductArray;
@property (nonatomic, strong) NSMutableArray *allCellArray;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) CommonChooseView *chooseView;
@property (nonatomic, strong) BusinessClassifyDetailParser *categoryParser;
@property (nonatomic, assign) BOOL isShouldUpdate;//是否重新加载分类信息
@property (nonatomic, strong) GoodsClassifyMainViewController *classifyController;

@end

@implementation BusinessGoodsViewController
#pragma mark - lazy loadding
-(CommonChooseView *)chooseView {
    if (!_chooseView) {
        _chooseView = [[CommonChooseView alloc] init];
        _chooseView.chooseDelegate = self;
        _chooseView.titleLabel.text = @"店内分类";
        [_chooseView.oneButton setTitle:@"新建分类" forState:UIControlStateNormal];
        _chooseView.commonChooseViewType = CommonChooseViewTypeCategory;
        
        
    }
    return _chooseView;
}

-(NSMutableArray *)categoryArray {
    if (!_categoryArray) {
        _categoryArray = [NSMutableArray array];
    }
    return _categoryArray;
}

-(NSMutableArray *)seletedProductArray {
    if (!_seletedProductArray) {
        _seletedProductArray = [NSMutableArray array];
    }
    return _seletedProductArray;
}

-(NSMutableArray *)allCellArray {
    if (!_allCellArray) {
        _allCellArray = [NSMutableArray array];
    }
    return _allCellArray;
}

-(NSMutableArray *)goodsArray {
    if (!_goodsArray) {
        _goodsArray = [NSMutableArray array];
    }
    return _goodsArray;
}

-(UIButton *)selectedAllButton {
    if (!_selectedAllButton) {
        _selectedAllButton = [UIButton new];
        
    }
    return _selectedAllButton;
}
#pragma mark - life circle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.page = 1;
    self.limit = @"5";
    self.isShouldUpdate = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //上下拉刷新
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self uploadGoodsInformation];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self pullGoodsInformation];
        
    }];
    //    [self.tableView beginUpdates];
    [self uploadGoodsInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

-(void)setEditType:(BusinessGoodsEditType)editType {
    _editType = editType;
    [self.tableView reloadData];
    
}

- (void) returnNumber:(ReturnNumberBlock)block {
    self.returnNumberBlock = block;
};



-(instancetype)initWithBusinessGoodsType:(BusinessGoodsType )businessGoodsType {
    if (self=[super init]) {
        self.businessGoodsType = businessGoodsType;
    }
    return self;
}

-(void)uploadGoodsInformation {
    //默认界面
    //    BusinessDefaultView *defaultView = [[BusinessDefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    //    [defaultView.applyBtn removeFromSuperview];
    //    NSString *tintString = @"您还没有出售中的商品";
    //    if (self.businessGoodsType==BusinessGoodsTypeUnShelve) {
    //        tintString = @"您还没有已下架的商品";
    //    }
    //    defaultView.tintLabel.text = tintString;
    //    [self.view addSubview:defaultView];
    self.page = 1;
    NSDictionary *param ;
    if (self.businessGoodsType==BusinessGoodsTypeChecking) {
        param = @{@"status":@"0",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    if (self.businessGoodsType==BusinessGoodsTypeSold) {
        param = @{@"is_show":@"1",@"status":@"1",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    if (self.businessGoodsType==BusinessGoodsTypeUnShelve) {
        param = @{@"is_show":@"0",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplyProductListsWithParam:param success:^(NSDictionary *dic, resultObject *state) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessProductParser *parser = [BusinessProductParser mj_objectWithKeyValues:dic[@"data"]];
            if (self.returnNumberBlock!=nil) {
                self.returnNumberBlock(parser.total);
            }
            [self.goodsArray removeAllObjects];
            if (parser.data.count) {
                [self.goodsArray addObjectsFromArray:parser.data];
                
            } else {
                
                [XSTool showToastWithView:self.view Text:@"暂无商品"];
            }
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void) pullGoodsInformation {
    self.page++;
    NSDictionary *param ;
    if (self.businessGoodsType==BusinessGoodsTypeChecking) {
        param = @{@"status":@"0",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    if (self.businessGoodsType==BusinessGoodsTypeSold) {
        param = @{@"is_show":@"1",@"status":@"1",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    if (self.businessGoodsType==BusinessGoodsTypeUnShelve) {
        param = @{@"is_show":@"0",@"page":[NSString stringWithFormat:@"%lu",(unsigned long)self.page],@"limit":self.limit};
    }
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager supplyProductListsWithParam:param success:^(NSDictionary *dic, resultObject *state) {
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessProductParser *parser = [BusinessProductParser mj_objectWithKeyValues:dic[@"data"]];
            if (self.returnNumberBlock!=nil) {
                self.returnNumberBlock(parser.total);
            }
            if (parser.data.count) {
                if (self.returnNumberBlock!=nil) {
                    self.returnNumberBlock(parser.total);
                }
                
                [self.goodsArray addObjectsFromArray:parser.data];
                [self.tableView reloadData];
            } else {
                self.page--;
            }
        } else {
            self.page--;
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        self.page--;
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)unSoldProducts{
    if (self.seletedProductArray.count) {
        [HTTPManager supplyProductOnOffWithIdString:[NSString stringWithFormat:@"%@",[self.seletedProductArray componentsJoinedByString:@","]] isShow:@"0" success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                
                [self.seletedProductArray removeAllObjects];
                GoodsManagementViewController *controller = (GoodsManagementViewController *)self.parentViewController;
                
                controller.isChange = YES;
                if (controller.selectedAllButton.selected) {
                    [controller clickSelectedAllButtonAction:controller.selectedAllButton];
                }
                
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请先选择商品"];
    }
    
}

- (void)soldProducts{
    if (self.seletedProductArray.count) {
        [HTTPManager supplyProductOnOffWithIdString:[NSString stringWithFormat:@"%@",[self.seletedProductArray componentsJoinedByString:@","]] isShow:@"1" success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                [self.seletedProductArray removeAllObjects];
                GoodsManagementViewController *controller = (GoodsManagementViewController *)self.parentViewController;
                controller.isChange = YES;
                if (controller.selectedAllButton.selected) {
                    [controller clickSelectedAllButtonAction:controller.selectedAllButton];
                }
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请先选择商品"];
    }
}

- (void)updateCategoryProducts{
    if (self.seletedProductArray.count) {
//        if (self.categoryArray.count) {
//            if (self.isShouldUpdate) {
//                [self getChooseViewArray];
//            } else {
//                [self addCooseView];
//            }
//
//        } else {
//            //店内分类
//            [self getChooseViewArray];
//        }
        self.classifyController = [[GoodsClassifyMainViewController alloc] init];
        self.classifyController.firstClassifyType = FirstClassifyTypeManageGoods;
        [self.navigationController pushViewController:self.classifyController animated:YES];
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请先选择商品"];
    }
}

- (void) getChooseViewArray {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager supplyCategoryListsWithParam:@{@"parent_id":@"0",@"tree":@"no"} success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            BusinessClassifyParser *parser = [BusinessClassifyParser mj_objectWithKeyValues:dic];
            [self.categoryArray removeAllObjects];
            if (parser.data.count) {
                [self.categoryArray addObjectsFromArray:parser.data];
                
                [self addCooseView];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void) addCooseView {
    [self.view addSubview:self.chooseView];
    [self.chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.mas_offset(mainHeight/3);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    self.chooseView.listArray = self.categoryArray;
}

- (void)deleteProducts{
    if (self.seletedProductArray.count) {
        [HTTPManager supplyProductDeleteWithId:[NSString stringWithFormat:@"%@",[self.seletedProductArray componentsJoinedByString:@","]] success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                [self.seletedProductArray removeAllObjects];
                GoodsManagementViewController *controller = (GoodsManagementViewController *)self.parentViewController;
                if (controller.selectedAllButton.selected) {
                    [controller clickSelectedAllButtonAction:controller.selectedAllButton];
                }
                [self uploadGoodsInformation];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
            
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请先选择商品"];
    }
    
    
}

- (void)clickSelectedAllButtonAction:(UIButton *)sender {
    [self cellsForTableView:self.tableView selected:sender.selected];
}

- (void )cellsForTableView:(UITableView *)tableView selected:(BOOL )isSelected {
    self.selectedAllButton.selected = isSelected;
    [self.tableView reloadData];
}


#pragma mark - CommonChooseViewDelegate
-(void)closeCommonChooseView {
    if (self.chooseView) {
        [self.chooseView removeFromSuperview];
        //        self.chooseView = nil;
    }
}

-(void)selectedClassifyWithCategoryId:(NSString *)classifyId{
//    [self.chooseView removeFromSuperview];
    if (self.seletedProductArray.count) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager supplyProductUpdateScategoryWithIdString:[NSString stringWithFormat:@"%@",[self.seletedProductArray componentsJoinedByString:@","]] scategory_id:classifyId success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [self.seletedProductArray removeAllObjects];
                [XSTool showToastWithView:self.view Text:@"分类成功"];
                self.isShouldUpdate = YES;
                [self.tableView reloadData];
            } else {
                self.isShouldUpdate = NO;
                [XSTool showToastWithView:self.view Text:state.info];
                
            }
        } fail:^(NSError *error) {
            self.isShouldUpdate = NO;
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
    
    
}

-(void)toNewViewWithType:(NSUInteger)type {
    if (type==2) {
        
        //编辑分类
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置分类名" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        //添加按钮
        __weak typeof(alertController) weakAlert = alertController;
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //获取输入的昵称
            NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
            if ([weakAlert.textFields.lastObject text].length==0) {
                [XSTool showToastWithView:self.view Text: @"分类名不能为空"];
            } else {
                if ([self stringContainsEmoji:[weakAlert.textFields.lastObject text]]) {
                    [XSTool showToastWithView:self.view Text:@"你好，分类名不支持表情"];
                } else {
                    
                    [HTTPManager supplyCategoryInsertWithParam:@{@"parent_id":@"0",@"category_name":[weakAlert.textFields.lastObject text]} success:^(NSDictionary *dic, resultObject *state) {
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:Localized(@"add_success")];
                            [HTTPManager supplyCategoryListsWithParam:@{@"parent_id":@"0",@"tree":@"no"} success:^(NSDictionary *dic, resultObject *state) {
                                [XSTool hideProgressHUDWithView:self.view];
                                if (state.status) {
                                    BusinessClassifyParser *parser = [BusinessClassifyParser mj_objectWithKeyValues:dic];
                                    [self.categoryArray removeAllObjects];
                                    if (parser.data.count) {
                                        
                                        [self.categoryArray addObjectsFromArray:parser.data];
                                        self.chooseView.commonChooseViewType = CommonChooseViewTypeCategory;
                                        self.chooseView.listArray = self.categoryArray;
                                    }
                                } else {
                                    [XSTool showToastWithView:self.view Text:state.info];
                                }
                                
                            } fail:^(NSError *error) {
                                [XSTool showToastWithView:self.view Text:NetFailure];
                            }];
                            
                        } else {
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError *error) {
                        [XSTool showProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:NetFailure];
                    }];
                    
                }
            }
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            NSLog(@"点击了取消按钮");
        }]];
        
        // 添加文本框
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.textColor = [UIColor blackColor];
            textField.delegate = self;
            //        textField.tag = 1500;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        // 弹出对话框
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.markedTextRange == nil) {
        if (textField.text.length >10) {
            [XSTool showToastWithView:self.view Text:@"分类长度只能为1~10"];
            textField.text = [textField.text substringToIndex:10];
        }
    }
}

- (BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}



#pragma mark - UITableViewDataSource
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.editType==BusinessGoodsEditTypeNormal) {
        BusinessGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessGoodsTableViewCell idString]];
        if (cell==nil) {
            cell = [[BusinessGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[BusinessGoodsTableViewCell idString]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (self.businessGoodsType==BusinessGoodsTypeChecking) {
            cell.itemType = BusinessGoodsItemTypeChecking;
        }
        if (self.businessGoodsType==BusinessGoodsTypeUnShelve) {
            cell.itemType = BusinessGoodsItemTypeUnsold;
        }
        if (self.businessGoodsType==BusinessGoodsTypeSold) {
            cell.itemType = BusinessGoodsItemTypeNormal;
        }
        cell.delegate = self;
        cell.detailParser = self.goodsArray[indexPath.row];
        
        return cell;
        
    } else {
        BusinessEditGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessEditGoodsTableViewCell idString]];
        if (cell==nil) {
            cell = [[BusinessEditGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[BusinessEditGoodsTableViewCell idString]];
        }
        cell.delegate = self;
        cell.detailParser = self.goodsArray[indexPath.row];
        cell.selectedButton.selected = !self.selectedAllButton.selected;
        [cell clickSelectedButtonAction:cell.selectedButton];
        [self.allCellArray addObject:cell];
        return cell;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.goodsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BusinessGoodsTableViewCell *cell = (BusinessGoodsTableViewCell *) [self tableView:self.tableView cellForRowAtIndexPath:indexPath];
    return cell.height;
}

#pragma mark - BusinessGoodsDelegate

-(void)editProductWithProductParser:(BusinessProductDetailParser *)productParser {
    AdvancedPublishGoodsViewController *controller = [[AdvancedPublishGoodsViewController alloc] init];
    controller.productParser = productParser;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)deleteProductWithId:(NSString *)productId {
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"确认删除商品？" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager supplyProductDeleteWithId:productId success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [self.seletedProductArray removeAllObjects];
                [self uploadGoodsInformation];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    UIAlertAction *canAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertVC addAction:canAct];
    [alertVC addAction:conAct];
    [self presentViewController:alertVC animated:YES completion:nil];
    
    
}

-(void)shareProductWithId:(NSString *)productId {
    [XSShareView creatShareView:productId viewController:self type:ShareTypeProduct];
}

#pragma mark - BusinessEditDelegate
-(void)deleteProductWithId:(NSString *)productId sure:(BOOL)isSure {
    if (isSure) {
        if (![self.seletedProductArray containsObject:productId]) {
            [self.seletedProductArray addObject:productId];
        }
        
    } else {
        if ([self.seletedProductArray containsObject:productId]) {
            [self.seletedProductArray removeObject:productId];
        }
    }
}

@end

