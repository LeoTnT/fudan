//
//  ChooseCategoryViewController.m
//  App3.0
//
//  Created by nilin on 2017/8/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChooseCategoryViewController.h"
#import "BusinessModel.h"
#import "AdvancedPublishGoodsViewController.h"
#import "CategoryInstance.h"
#import "AutoFitLabelView.h"
#import "MSSAutoresizeLabelFlow.h"
#import "RecommendView.h"


@interface ChooseCategoryViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,RecommendViewDelegate>
@property (nonatomic, strong) UITextField *searchText;
@property (nonatomic, strong) UIButton *searchButton;
@property (nonatomic, strong) NSMutableArray *categoryListArray;
@property (nonatomic, strong) RecommendView *recommendView;//推荐
@property (nonatomic, strong) MSSAutoresizeLabelFlow *recordView;//历史搜索记录
@property (nonatomic, strong) UIView *showHistoryAndDeleteView;

@end

@implementation ChooseCategoryViewController
#pragma mark -lazy loadding

-(UITextField *)searchText {
    if (!_searchText) {
        _searchText = [UITextField new];
        _searchText.backgroundColor = [UIColor whiteColor];
        _searchText.placeholder = @"输入类目关键字";
        _searchText.delegate = self;
        _searchText.font = [UIFont qsh_systemFontOfSize:18];
        _searchText.textColor = [UIColor blackColor];
        _searchText.borderStyle = UITextBorderStyleRoundedRect;
    }
    return _searchText;
}

-(UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton new];
        [_searchButton setTitle:Localized(@"search") forState:UIControlStateNormal];
        _searchButton.backgroundColor = mainColor;
        [_searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _searchButton.titleLabel.font = [UIFont qsh_systemFontOfSize:20];
        [_searchButton addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _searchButton;
}

-(NSMutableArray *)categoryListArray {
    if (!_categoryListArray) {
        _categoryListArray = [NSMutableArray array];
    }
    return _categoryListArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择类目";
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIButton *leftButton = [[UIButton alloc]init];
    leftButton.frame = CGRectMake(0, 0, 20, 20);
    [leftButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(popToLastVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItems = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    //解决按钮不靠左 靠右的问题.
    UIBarButtonItem *nagetiveSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    //    nagetiveSpacer.width = -15;//这个值可以根据自己需要自己调整
    self.navigationItem.leftBarButtonItems = @[nagetiveSpacer, leftBarButtonItems];
    
    self.view.userInteractionEnabled = YES;
    UIView *headerView = [UIView new];
    headerView.backgroundColor = BG_COLOR;
    [self.view addSubview:headerView];
    [headerView addSubview:self.searchText];
    [headerView addSubview:self.searchButton];
    
    //推荐
    NSArray *recommendArray = @[@"男装",@"女装",@"手机",@"电脑",@"家装",@"百货",@"玩具",@"运动"];
    self.recommendView = [[RecommendView alloc] init];
    self.recommendView.viewDelegate = self;
    self.recommendView.titleArray = recommendArray;
    [self.view addSubview:self.recommendView];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(CGRectGetMaxY(self.recommendView.frame), 0, 0, 0));
    }];
    //历史记录
    self.showHistoryAndDeleteView = [UIView new];
    [self.view addSubview:self.showHistoryAndDeleteView];
    UILabel *historyLabel = [UILabel new];
    historyLabel.text = @"历史搜索";
    historyLabel.textColor = [UIColor blackColor];
    historyLabel.font = [UIFont qsh_systemFontOfSize:16];
    [self.showHistoryAndDeleteView addSubview:historyLabel];
    UIButton *deleteButton = [UIButton new];
    [deleteButton addTarget:self action:@selector(deleteAllHistoryLists) forControlEvents:UIControlEventTouchUpInside];
    [deleteButton setImage:[UIImage imageNamed:@"user_site_delete_gray"] forState:UIControlStateNormal];
    [self.showHistoryAndDeleteView addSubview:deleteButton];
    self.recordView = [[MSSAutoresizeLabelFlow alloc]initWithFrame:CGRectMake(0, 64*4, mainWidth, 100) titles:[[CategoryInstance ShardInstnce] getCategory] selectedHandler:^(NSUInteger index, NSString *title) {
        self.recordView.hidden = YES;
        self.showHistoryAndDeleteView.hidden = YES;
        [self searchWithTitle:title];
        
    }];
    [self.view addSubview:self.recordView];
    
    
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(64);
    }];
    [self.searchText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headerView).with.mas_offset(15);
        make.top.mas_equalTo(headerView).with.mas_offset(10);
        make.bottom.mas_equalTo(headerView).with.mas_offset(-10);
        make.width.mas_equalTo(headerView).with.mas_offset(-100);
    }];
    
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(headerView).with.mas_offset(-15);
        make.left.mas_equalTo(self.searchText.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(headerView).with.mas_offset(10);
        make.bottom.mas_equalTo(headerView).with.mas_offset(-10);
    }];
    self.searchButton.layer.cornerRadius = 5;
    self.searchButton.layer.masksToBounds = YES;
    
    [self.recommendView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headerView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(self.recommendView.viewHeight);
    }];
 
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.recommendView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    [self.showHistoryAndDeleteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.recommendView.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    [self.recordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.showHistoryAndDeleteView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    [historyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showHistoryAndDeleteView).with.mas_offset(13);
        make.width.mas_equalTo(150);
        make.top.bottom.mas_equalTo(self.showHistoryAndDeleteView);
    }];
    
    [deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.showHistoryAndDeleteView).with.mas_offset(-13);
        make.width.mas_equalTo(20);
        make.top.bottom.mas_equalTo(self.showHistoryAndDeleteView);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void) popToLastVC:(UIButton *) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)deleteAllHistoryLists {
    [[CategoryInstance ShardInstnce] deleteCategory];
    [self.recordView reloadAllWithTitles:@[]];
}
- (void)searchAction:(UIButton *) sender {
    [self.view endEditing:YES];
    self.recordView.hidden = YES;
    self.showHistoryAndDeleteView.hidden = YES;
    [self searchWithTitle:self.searchText.text];
}

- (void) searchWithTitle:(NSString *) title {
    if (title.length==0||[title isEqualToString:@" "]) {
        [XSTool showToastWithView:self.view Text:@"请输入关键字"];
    } else {
        
        //存储数据
        [[CategoryInstance ShardInstnce] setupCategoryInfoWithName:title];
        
        [self.categoryListArray removeAllObjects];
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager supplyProductGetCategoryLowerWithCname:title success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                CategoryLowerParser *parser = [CategoryLowerParser mj_objectWithKeyValues:dic];
                if (parser.data.count) {
                    [self.categoryListArray addObjectsFromArray:parser.data];
                    [self.tableView reloadData];
                } else {
                    [XSTool showToastWithView:self.view Text:@"未找到分类信息"];
                }
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
    
}

#pragma mark - RecommendViewDelegate
-(void)searchCategoryWithTitle:(NSString *)title {
    self.recordView.hidden = YES;
    self.showHistoryAndDeleteView.hidden = YES;
    [self searchWithTitle:title];
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"categoryLower";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
    }
    CategoryLowerDetailParser *parser = self.categoryListArray[indexPath.row];
    cell.textLabel.text = parser.cname;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categoryListArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 取消选中状态
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    AdvancedPublishGoodsViewController *controller = (AdvancedPublishGoodsViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    controller.categoryParser = self.categoryListArray[indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
