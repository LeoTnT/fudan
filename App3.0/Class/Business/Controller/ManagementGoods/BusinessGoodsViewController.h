//
//  BusinessGoodsViewController.h
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessClassifyModel.h"


typedef NS_ENUM(NSUInteger, BusinessGoodsType) {
    BusinessGoodsTypeUnShelve,
    BusinessGoodsTypeSold,
    BusinessGoodsTypeChecking,
    
};

typedef NS_ENUM(NSUInteger, BusinessGoodsEditType) {
    BusinessGoodsEditTypeNormal,
    BusinessGoodsEditTypeEdit
    
};

// 声明的Block重新定义了一个名字 ReturnTextBlock
typedef void (^ReturnNumberBlock)(NSString *number);

@interface BusinessGoodsViewController : XSBaseTableViewController

// 定义的一个Block属性
@property (nonatomic, copy) ReturnNumberBlock returnNumberBlock;

@property (nonatomic, assign) BusinessGoodsEditType editType;

- (void)returnNumber:(ReturnNumberBlock)block;

- (instancetype)initWithBusinessGoodsType:(BusinessGoodsType ) businessGoodsType;

-(void)uploadGoodsInformation;

- (void)clickSelectedAllButtonAction:(UIButton *)sender;

/**下架*/
//- (void)unSoldProductsWithProductIds:(NSString *) productIds;
- (void)unSoldProducts;
/**上架*/
- (void)soldProducts;

/**店内分类*/
- (void)updateCategoryProducts;

/**删除商品*/
- (void)deleteProducts;

/**分类的ID*/
-(void)selectedClassifyWithCategoryId:(NSString *)classifyId;

@end
