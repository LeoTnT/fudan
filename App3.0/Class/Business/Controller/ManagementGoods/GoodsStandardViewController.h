//
//  GoodsStandardViewController.h
//  App3.0
//
//  Created by nilin on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsStandardViewController : XSBaseTableViewController

/**平台分类id*/
@property (nonatomic, copy) NSString *categoryId;

/**条形码编号*/
@property (nonatomic, copy) NSString *barCode;

/**是否是添加商品*/
@property (nonatomic, copy) NSString *operateType;

/**销售类型*/
@property (nonatomic, copy) NSString *sellType;
    
- (void)getStandardInformation;
@end
