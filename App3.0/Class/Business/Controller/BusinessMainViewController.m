//
//  BusinessMainViewController.m
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessMainViewController.h"
#import "BusinessDefaultView.h"
#import "ApplyBusinessViewController.h"
#import "BusinessTopTableViewCell.h"
#import "BusinessNormalTableViewCell.h"
#import "AreaButton.h"
#import "GoodsManagementViewController.h"
#import "BusinessFormMainViewController.h"
#import "AdvancedPublishGoodsViewController.h"
#import "DeliveryVoucherViewController.h"
#import "UserModel.h"
#import "BusinessLogisticViewController.h"
#import "SetShopViewController.h"
#import "FinancialRecordVC.h"
#import "LoanWithdrawViewController.h"
#import "BusinessOperationReportViewController.h"
#import "BusinessModel.h"
#import "BusinessQRView.h"
#import "UIImage+XSWebImage.h"
#import "StandardMainViewController.h"
#import "S_StoreInformation.h"
#import "RefundManageViewController.h"
#import "XSQR.h"


@interface BusinessMainViewController ()<BusinessDefaultViewDelegate,BusinessNormalDelegate>
@property (nonatomic, strong) BusinessDefaultView *defaultView;//未申请商家版默认view
@property (nonatomic, assign) CGFloat bottomViewHeight;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, copy) NSString *isEnterPay;
@property (nonatomic, copy) NSString *enterPayMoney;

@property (nonatomic, strong) BusinessSupplyReportParser *reportParser;
@property (nonatomic, strong) BusinessQRView *qrView;
@property (nonatomic, strong) UIView *qrBackgroundView;
@end

@implementation BusinessMainViewController

#pragma mark - lazy loadding
-(BusinessQRView *)qrView {
    if (!_qrView) {
        _qrView = [[BusinessQRView alloc] initWithFrame:CGRectMake(mainWidth*0.1, mainHeight/2-mainWidth*0.4, mainWidth*0.8, mainWidth*0.8)];
        _qrView.hidden = YES;
    }
    return _qrView;
}

-(UIView *)qrBackgroundView {
    if (!_qrBackgroundView) {
        _qrBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        _qrBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _qrBackgroundView.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(qrBackgroundViewTap)];
        [_qrBackgroundView addGestureRecognizer:tap];
        [[[UIApplication sharedApplication] keyWindow] addSubview:_qrBackgroundView];
        [[[UIApplication sharedApplication] keyWindow] addSubview:self.qrView];
    }
    return _qrBackgroundView;
}

-(NSMutableArray *)titleArray {
    if (!_titleArray) {
         _titleArray = [NSMutableArray arrayWithArray:@[@{@"image":@"user_business_manage",@"title":@"商品管理"},@{@"image":@"user_business_order",@"title":@"订单管理"},@{@"image":@"user_business_logistic",@"title":@"物流费"},@{@"image":@"user_business_set",@"title":@"店铺设置"},@{@"image":@"user_business_report",@"title":@"运营报表"},@{@"image":@"user_business_evidence",@"title":@"商家规格"},@{@"image":@"user_business_wallert",@"title":@"货款钱包"},@{@"image":@"user_business_withdraw",@"title":@"货款提现"},@{@"image":@"user_business_refund",@"title":@"退款退货"},@{@"image":@"user_business_store",@"title":@"店铺首页"}]];
        
        
    }
    return _titleArray;
}

-(BusinessDefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[BusinessDefaultView alloc] init];
        _defaultView.businessDelegate = self;
    }
    return _defaultView;
}
#pragma mark - life circle
//-(void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomViewHeight = 60;
    self.navigationItem.title = @"商家版";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    if (self.businessParser) {
        self.isEnterPay = self.businessParser.is_enter_pay;
        self.enterPayMoney = self.businessParser.enter_pay_number;
        if ([self.businessParser.approve_supply intValue]==1) {
            //已开通商家版->主界面
            self.tableViewStyle = UITableViewStylePlain;
            [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, self.bottomViewHeight*1.2, 0));
            }];
            self.tableView.bounces = NO;
            self.tableView.backgroundColor = BG_COLOR;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            [self.view layoutIfNeeded];
            
            UIView *bottomView = [[UIView alloc] init];
            bottomView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:bottomView];
            [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(self.bottomViewHeight*1.2);
                make.left.right.bottom.mas_equalTo(self.view);
            }];
            AreaButton *button = [[AreaButton alloc] initWithFrame:CGRectZero Model:@{@"image":@"user_business_add",@"title":@"一键发布宝贝"} Scale:1.0/5.0 fontSize:17];
            [button addTarget:self action:@selector(toPublishGoodsAction) forControlEvents:UIControlEventTouchUpInside];
            
            [bottomView addSubview:button];
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.bottom.mas_equalTo(bottomView);
                make.size.mas_equalTo(CGSizeMake(150, 120));
            }];
            [self.tableView reloadData];
            
            //收款二维码
//            [self.qrView.qrImgView getImageWithUrlStr:self.businessParser.qrcode andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            self.qrView.qrImgView.image = [XSQR createQrImageWithContentString:self.businessParser.qrcode type:XSQRTypePAY];
            
            [HTTPManager getReportSupplyReportSuccess:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    self.reportParser = [BusinessSupplyReportParser mj_objectWithKeyValues:dic[@"data"]];
                    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError *error) {
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
            
            
        } else if ([self.businessParser.approve_supply intValue]==0) {
            
            //审核中
            self.defaultView.frame = CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT);
            self.defaultView.tintLabel.text = [NSString stringWithFormat:@"认证状态：%@",self.businessParser.approve_desc];
            self.defaultView.applyBtn.hidden = YES;
            [self.view addSubview:self.defaultView];
            
        } else if ([self.businessParser.approve_supply intValue]==-1||[self.businessParser.approve_supply intValue]==-2) {
            
            //未申请入驻
            //                [XSTool showToastWithView:self.view Text:@"未提交！"];
            self.defaultView.frame = CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT);
            [self.view addSubview:self.defaultView];
        } else {
            self.defaultView.frame = CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT);
            self.defaultView.reasonLabel.textAlignment = NSTextAlignmentCenter;
            self.defaultView.reasonLabel.text = [NSString stringWithFormat:@"认证状态：%@",self.businessParser.approve_desc];
            self.defaultView.tintLabel.textAlignment = NSTextAlignmentLeft;
            self.defaultView.tintLabel.text = [NSString stringWithFormat:@"认证评价：%@",self.businessParser.comment];
            [self.view addSubview:self.defaultView];
        }

    }
}

#pragma mark - private
-(void)setBusinessParser:(BusinessRegisterParser *)businessParser {
    _businessParser = businessParser;
   
}

- (void)toPublishGoodsAction {
   AdvancedPublishGoodsViewController *publishController = [[AdvancedPublishGoodsViewController alloc] init];
    [self.navigationController pushViewController:publishController animated:YES];
    
}

- (void)qrAction:(UIButton *)sender {
    self.qrBackgroundView.hidden = NO;
    self.qrView.hidden = NO;
    [UIImageView animateWithDuration:0.3 animations:^{
        self.qrView.frame = CGRectMake(mainWidth*0.1, (mainHeight-mainWidth)/2, mainWidth*0.8, mainWidth*0.8);
    }];
}

- (void)qrBackgroundViewTap {
    self.qrBackgroundView.hidden = YES;
    [UIImageView animateWithDuration:0.3 animations:^{
        self.qrView.frame = CGRectMake(mainWidth*0.1, mainHeight+mainWidth+100, mainWidth*0.8, mainWidth*0.8);
    } completion:^(BOOL finished) {
        self.qrView.hidden = YES;
    }];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0) {
        BusinessTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessTopTableViewCell idString]];
        if (cell==nil) {
            cell = [[BusinessTopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[BusinessTopTableViewCell idString]];
        }
        if (self.businessParser) {
            cell.iconUrl = self.businessParser.logo;
            cell.name = self.businessParser.nickname;
        }
        cell.titleArray = @[@{@"今日成交额":@"0.00"},@{@"今日访客":@"0"},@{@"今日订单":@"0"}];
        if (self.reportParser) {
            cell.titleArray = @[@{@"今日成交额":self.reportParser.income_today},@{@"今日访客":self.reportParser.browse_supply_today},@{@"今日订单":self.reportParser.order_today}];
        }
        [cell.qrButton addTarget:self action:@selector(qrAction:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
        
    } else {
        NSString *idString = @"normalCell";
        BusinessNormalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusinessNormalTableViewCell idString]];
        if (!cell) {
            cell = [[BusinessNormalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
            
        }
        cell.delegate = self;
        cell.menuArray = self.titleArray;
        return cell;
    }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        return 260;
    } else {
        BusinessNormalTableViewCell *cell = (BusinessNormalTableViewCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
        return cell.height;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - BusinessDefaultViewDelegate
- (void)clicktoApplyBusinessEdition {
    
    //跳转到申请界面
    ApplyBusinessViewController *applyController = [[ApplyBusinessViewController alloc] init];
    applyController.isEnterPay = self.isEnterPay;
    if ([self.isEnterPay integerValue]==1) {
        applyController.enterPayMoney = self.enterPayMoney;
    }
    [self.navigationController pushViewController:applyController animated:YES];
    
}


#pragma mark - BusinessNormalDelegate
-(void)buttonActionWithIndex:(NSUInteger)index {
    if (index==1000) {
        GoodsManagementViewController *goodsController = [[GoodsManagementViewController alloc] initWithGoodsManagementType:GoodsManagementTypeNormal];
        [self.navigationController pushViewController:goodsController animated:YES];
    } else if (index==1001) {
        BusinessFormMainViewController *formController = [[BusinessFormMainViewController alloc] initWithFormType:BusinessFormTypeAll];

        [self.navigationController pushViewController:formController animated:YES];


    } else if (index==1002) {
        NSLog(@"物流费");
        BusinessLogisticViewController *logisticController = [[BusinessLogisticViewController alloc] init];
        [self.navigationController pushViewController:logisticController animated:YES];
        
    } else if (index==1003) {
        NSLog(@"店铺设置");
        SetShopViewController *setShopController = [[SetShopViewController alloc] init];
        [self.navigationController pushViewController:setShopController animated:YES];
    } else if (index==1004) {
        NSLog(@"运营报表");
        BusinessOperationReportViewController *reportController = [[BusinessOperationReportViewController alloc] init];
        reportController.iconUrl = self.businessParser.logo;
        reportController.name = self.businessParser.nickname;
        [self.navigationController pushViewController:reportController animated:YES];
    } else if (index==1005) {
        StandardMainViewController *standardController = [[StandardMainViewController alloc] init];
        [self.navigationController pushViewController:standardController animated:YES];
    } else if (index==1006) {
        NSLog(@"货款钱包");
        FinancialRecordVC *financialController = [[FinancialRecordVC alloc] init];
        [self.navigationController pushViewController:financialController animated:YES];
    } else if (index==1007) {
        NSLog(@"货款提现");
        LoanWithdrawViewController *loanController = [[LoanWithdrawViewController alloc] init];
        [self.navigationController pushViewController:loanController animated:YES];
    }else if (index==1008) {
        NSLog(@"退款退货");
        RefundManageViewController *controller = [[RefundManageViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (index==1009) {
        NSLog(@"店铺首页");
        S_StoreInformation *controller = [[S_StoreInformation alloc] init];
        controller.storeInfor = [UserInstance ShardInstnce].uid;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
