//
//  ShopLanternSlideTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopLanternSlideTableViewCell : UITableViewCell
/**图片数组*/
@property(nonatomic,strong)NSArray *photosArray;
/**cell的高度*/
@property(nonatomic,assign)CGFloat height;
/**最后一个图片*/
@property(nonatomic,strong)UIImageView *lastImage;
/**所有的删除按钮*/
@property(nonatomic,strong)NSMutableArray *deletBtnArray;
/**所有的除了加号的图片*/
@property(nonatomic,strong)NSMutableArray *imagesArray;

@property (nonatomic, strong) UILabel *titleLabel;
@end
