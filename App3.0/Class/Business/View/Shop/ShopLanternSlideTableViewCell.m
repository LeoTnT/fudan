//
//  ShopLanternSlideTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ShopLanternSlideTableViewCell.h"

@implementation ShopLanternSlideTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, mainWidth-20, 20)];
        self.titleLabel.text = @"首页幻灯片";
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}
#pragma mark-刷新界面
-(void)setPhotosArray:(NSArray *)photosArray{
    _photosArray=photosArray;
    self.deletBtnArray=[NSMutableArray array];
    self.imagesArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]||[view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    UIImageView *img;
    if (self.photosArray.count==0) {
        return;
    }
    for (int i=0; i<self.photosArray.count; i++) {
        int columns=2;
        //列数
        int col=i%columns;
        //行数
        int row=i/columns;
        CGFloat imageWidth = mainWidth/2-40;
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10*(col+1)+col*imageWidth,CGRectGetMaxY(self.titleLabel.frame)+10*(row+1)+row*100,imageWidth ,100)];
        if (i==self.photosArray.count-1) {
            imgView.frame = CGRectMake(10*(col+1)+col*imageWidth,CGRectGetMaxY(self.titleLabel.frame)+10*(row+1)+row*100, 100, 100);
        }
        imgView.image=[self.photosArray objectAtIndex:i];
        img=imgView;
        imgView.userInteractionEnabled=YES;
        [self.contentView addSubview:imgView];
        //添加删除按钮×
        if (![imgView.image isEqual:[UIImage imageNamed:@"user_fans_addphoto_thin"]]) {
            UIButton *deleteBtn=[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)-10, CGRectGetMinY(img.frame)-10, 20, 20)];
            deleteBtn.backgroundColor = [UIColor whiteColor];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
            deleteBtn.layer.cornerRadius = CGRectGetHeight(deleteBtn.frame)/2;
            deleteBtn.layer.masksToBounds = YES;
            [self.contentView addSubview:deleteBtn];
            [self.deletBtnArray addObject:deleteBtn];
        }
    }
    self.lastImage=img;
    self.lastImage.userInteractionEnabled=YES;
    if (photosArray.count==0) {
        self.height=NORMOL_SPACE*17;
    }else{
        self.height=CGRectGetMaxY(img.frame)+NORMOL_SPACE;
    }
}


@end
