//
//  RejectRefundReasonView.h
//  App3.0
//
//  Created by nilin on 2018/1/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessRefundModel.h"

@protocol RejectRefundReasonViewDelegate<NSObject>

@optional
-(void) hiddenCardView:(BusinessRefundRejectReasonDataParser *) dataParser;

@end
@interface RejectRefundReasonView : UIView
@property (nonatomic, strong) NSArray *reasonArray;
@property (nonatomic, copy) NSString *selectedId;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, weak) id<RejectRefundReasonViewDelegate> typeDelegate;
@end
