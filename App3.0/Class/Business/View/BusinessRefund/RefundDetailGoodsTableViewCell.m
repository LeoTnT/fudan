//
//  RefundDetailGoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundDetailGoodsTableViewCell.h"
#import "UIImage+XSWebImage.h"

@interface RefundDetailGoodsTableViewCell ()

@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *tintLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *buyerLabel;
@property (nonatomic, strong) UILabel *orderNumberLabel;
@property (nonatomic, strong) UIButton *recopyButton;
@property (nonatomic, strong) UILabel *timeLabel;
@end

@implementation RefundDetailGoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.cellHeight = 222;
        CGFloat imageSize = 70,titleHeight = 40,titlWidth = (mainWidth-11.5-12.5-10)/2;
        self.tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(11.5, 0,mainWidth-11.5*2 , 44)];
        self.tintLabel.textColor = [UIColor blackColor];
        self.tintLabel.text = @"退款信息";
        self.tintLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.tintLabel];
        
        UIView *goodsView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tintLabel.frame), mainWidth, 94)];
        goodsView.backgroundColor = [UIColor hexFloatColor:@"F3F4F7"];
        [self.contentView addSubview:goodsView];
        
        self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, imageSize, imageSize)];
        [goodsView addSubview:self.logo];
        
        //名称
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.logo.frame)+12, 14.5, imageSize, 32)];
        self.titleLabel.numberOfLines = 2;
        self.titleLabel.textColor = [UIColor blackColor];
        [ goodsView addSubview:self.titleLabel];
        
        //价格
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-100-12, CGRectGetMinY(self.titleLabel.frame), 100, 15)];
        self.priceLabel.textAlignment = NSTextAlignmentRight;
        self.priceLabel.font = [UIFont systemFontOfSize:15];
        self.priceLabel.textColor = [UIColor blackColor];
        [ goodsView addSubview:self.priceLabel];
        
        self.descLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), mainWidth-12*3-10-CGRectGetMaxX(self.logo.frame)-100, 15)];
        self.descLabel.font = [UIFont systemFontOfSize:12];
        self.descLabel.textColor = [UIColor hexFloatColor:@"888888"];
        [ goodsView addSubview:self.descLabel];
        //数量
        self.numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*2-12, CGRectGetMaxY(self.priceLabel.frame)+10, titleHeight*2, NORMOL_SPACE*2)];
        self.numberLabel.font = [UIFont systemFontOfSize:13];
        self.numberLabel.textColor = [UIColor hexFloatColor:@"888888"];
        self.numberLabel.textAlignment = NSTextAlignmentRight;
        [ goodsView addSubview:self.numberLabel];
        
        //底部
        self.buyerLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(goodsView.frame)+14, mainWidth-12*2, 20)];
        self.buyerLabel.textColor = COLOR_999999;
        self.buyerLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.buyerLabel];
        
        self.orderNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.buyerLabel.frame), mainWidth-12-84, 20)];
        self.orderNumberLabel.textColor = COLOR_999999;
        self.orderNumberLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.orderNumberLabel];
        
        self.recopyButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-24-50, CGRectGetMinY(self.orderNumberLabel.frame), 50, CGRectGetHeight(self.orderNumberLabel.frame))];
        [self.recopyButton setTitle:@"复制" forState:UIControlStateNormal];
        [self.recopyButton setTitleColor:mainColor forState:UIControlStateNormal];
        self.recopyButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.recopyButton];
        
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.orderNumberLabel.frame), mainWidth-12*2, 20)];
        self.timeLabel.textColor = COLOR_999999;
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.timeLabel];
        
        
    }
    
    return self;
}

- (void)copyNumber:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(copyNumberWithString:)]) {
        [self.delegate copyNumberWithString:self.detailParser.refund_no];
    }
}

- (void)setDetailParser:(BusinessRefundDetailParser *)detailParser {
    
    _detailParser = detailParser;
    [self.logo getImageWithUrlStr:detailParser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]
     ];
    
    NSString *contentString = detailParser.product_name;
    UIFont *font = [UIFont systemFontOfSize:15];
    self.titleLabel.font = font;
    self.titleLabel.text = contentString;
    // label可设置的最大高度和宽度
    CGSize size3 =CGSizeMake(mainWidth-12*3-10-CGRectGetMaxX(self.logo.frame)-100,300);
    NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
    //ios7方法，获取文本需要的size，限制宽度
    CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.logo.frame)+12, 14.5, mainWidth-12*3-10-CGRectGetMaxX(self.logo.frame)-100, actualsize3.height+5);
    self.numberLabel.text = [NSString stringWithFormat:@"×%@",detailParser.product_num];
    self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",[detailParser.sell_price floatValue] ];
    self.descLabel.text = detailParser.spec_name;
    
    NSString *tempString =[NSString stringWithFormat:@"买       家  %@",detailParser.buyer_name];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(11, tempString.length-11)];
    self.buyerLabel.attributedText = attString;

    tempString = [NSString stringWithFormat:@"订单编号  %@",detailParser.order_no];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.orderNumberLabel.attributedText = attString;
    
    tempString = [NSString stringWithFormat:@"成交时间  %@", detailParser.w_time];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.timeLabel.attributedText = attString;
    [self.recopyButton addTarget:self action:@selector(copyNumber:) forControlEvents:UIControlEventTouchUpInside];
}
@end
