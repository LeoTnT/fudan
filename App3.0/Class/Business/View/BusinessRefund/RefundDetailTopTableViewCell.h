//
//  RefundDetailTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RefundDelegate.h"
#import "BusinessRefundModel.h"
#import "RefundAddPhotoView.h"

typedef NS_ENUM(NSInteger, RefundDetailTopType) {
    RefundDetailTopTypeWaitCheck,
    
};

@interface RefundDetailTopTableViewCell : UITableViewCell

@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic, assign) RefundDetailTopType refundDetailTopType;

@property (nonatomic, weak) id<RefundDelegate> delegate;

@property (nonatomic, strong) BusinessRefundDetailParser *detailParser;

/**上传凭证*/
@property (nonatomic, strong)  RefundAddPhotoView *photoView;
@property (nonatomic, strong)  NSArray *photoArray;
@end
