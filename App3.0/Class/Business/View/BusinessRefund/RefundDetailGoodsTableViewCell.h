//
//  RefundDetailGoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessRefundModel.h"
#import "RefundDelegate.h"

@interface RefundDetailGoodsTableViewCell : UITableViewCell

@property (nonatomic, strong) BusinessRefundDetailParser *detailParser;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id<RefundDelegate> delegate;
@end
