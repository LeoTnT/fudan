//
//  RefundManageTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessRefundModel.h"

@protocol  RefundManageTopDelegate<NSObject>
- (void) lookWaitCheckOrder;
- (void) lookLastRefundWithRefundId:(NSString *) refundId;
@end
@interface RefundManageTopTableViewCell : UITableViewCell


@property (nonatomic, weak) id<RefundManageTopDelegate>  delegate;

@property (nonatomic, strong) BusinessRefundMainParser *refundParser;
@end
