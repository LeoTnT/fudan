
//
//  NegotiateHistoryTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "NegotiateHistoryTableViewCell.h"
#import "UIImage+XSWebImage.h"

@interface NegotiateHistoryTableViewCell ()
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation NegotiateHistoryTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self setSubViews];
    }
    return self;
}

- (void)setSubViews {
    self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12.5, 32, 32)];
    [self.contentView addSubview:self.logo];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.logo.frame)+10, 16, mainWidth-(CGRectGetMaxX(self.logo.frame)-10-12), 13)];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:self.titleLabel];
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame)+6.5, CGRectGetWidth(self.titleLabel.frame), 9.5)];
    self.timeLabel.textColor = COLOR_999999;
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.timeLabel];
    
    self.contentLabel = [[ UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.timeLabel.frame)+18, mainWidth-11.5-20.5, 100)];
    self.contentLabel.textColor = [UIColor blackColor];
    self.contentLabel.font = [UIFont systemFontOfSize:13];
    self.contentLabel.numberOfLines = 0;
    [self.contentView addSubview:self.contentLabel];
    
    self.cellHeight = 80;
}

-(void)setDataParser:(BusinessRefundNegotiateHistoryParser *)dataParser {
    _dataParser = dataParser;
    [self.logo getImageWithUrlStr:dataParser.avatar andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.titleLabel.text = dataParser.name;
    self.timeLabel.text = dataParser.format_time;
    
    UIFont * tfont2 = [UIFont systemFontOfSize:13];
    self.contentLabel.font = tfont2;
    self.contentLabel.lineBreakMode =NSLineBreakByTruncatingTail ;
    NSArray *temp = [dataParser.log_str componentsSeparatedByString:@"<br>"];
    NSString *contentString = [temp componentsJoinedByString:@"\n"];
    self.contentLabel.text = contentString;
    // label可设置的最大高度和宽度
    CGSize size2 =CGSizeMake(mainWidth-11.5-20.5,300);
    NSDictionary * tdic2 = [NSDictionary dictionaryWithObjectsAndKeys:tfont2,NSFontAttributeName,nil];
    //ios7方法，获取文本需要的size，限制宽度
    CGSize  actualsize2 =[contentString boundingRectWithSize:size2 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic2 context:nil].size;
    self.contentLabel.frame = CGRectMake(11.5, CGRectGetMaxY(self.timeLabel.frame)+18, mainWidth-11.5-20.5, actualsize2.height);
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            if (![view isEqual:self.logo]) {
                [view removeFromSuperview];
            }
        }
    }
    if (dataParser.pics.count) {
         NSUInteger count =  mainWidth/(10+80);
        for (int i=0; i<dataParser.pics.count; i++) {
           @autoreleasepool {
            NSInteger col = i%count,row = i/count,imageWidth = 80;
            UIImageView *tempImage = [[UIImageView alloc] initWithFrame:CGRectMake(11.5+col*(imageWidth+10), CGRectGetMaxY(self.contentLabel.frame)+15+row*(imageWidth+10), 80, 80)];
            [tempImage getImageWithUrlStr:dataParser.pics[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            [self.contentView addSubview:tempImage];
           }
        }
        CGFloat tempHeight = dataParser.pics.count%count==0?(dataParser.pics.count/count)*(80+10)+15:((dataParser.pics.count/count)+1)*(80+10)+15;
        self.cellHeight = CGRectGetMaxY(self.contentLabel.frame)+tempHeight;
    } else {
        self.cellHeight = CGRectGetMaxY(self.contentLabel.frame)+15;
        
    }
   
}

@end
