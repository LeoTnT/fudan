//
//  RefundListSearchView.h
//  App3.0
//
//  Created by nilin on 2017/12/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RefundListSearchViewDelegate <NSObject>

- (void) searchWithDictionary:(NSDictionary *) searchParam;

- (void) hiddenView;
@end


@interface RefundListSearchView : UIView

@property (nonatomic, weak) id<RefundListSearchViewDelegate> delegate;

@end
