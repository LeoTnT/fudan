//
//  RefundDetailBottomTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundDetailBottomTableViewCell.h"

@interface RefundDetailBottomTableViewCell ()
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UIButton *recopyButton;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *reasonLabel;
@property (nonatomic, strong) UILabel *descLabel;
@end

@implementation RefundDetailBottomTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    
    UIFont *font = [UIFont systemFontOfSize:12];
    self.numberLabel = [[UILabel alloc]  initWithFrame:CGRectMake(12, 15, mainWidth-12-84, 20)];
    self.numberLabel.font = font;
    self.numberLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.numberLabel];
    
    self.recopyButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-24-50, 15, 50, CGRectGetHeight(self.numberLabel.frame))];
    [self.recopyButton setTitle:@"复制" forState:UIControlStateNormal];
    [self.recopyButton setTitleColor:mainColor forState:UIControlStateNormal];
    self.recopyButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.recopyButton];
    
    self.typeLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.numberLabel.frame), mainWidth-12*2, 20)];
    self.typeLabel.font = font;
    self.typeLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.typeLabel];
    
    self.statusLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.typeLabel.frame), mainWidth-12*2, 20)];
    self.statusLabel.font = font;
    self.statusLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.statusLabel];
    
    self.moneyLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.statusLabel.frame), mainWidth-12*2, 20)];
    self.moneyLabel.font = font;
    self.moneyLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.moneyLabel];
    
    self.timeLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.moneyLabel.frame), mainWidth-12*2, 20)];
    self.timeLabel.font = font;
    self.timeLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.timeLabel];
    
    self.reasonLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.timeLabel.frame), mainWidth-12*2, 20)];
    self.reasonLabel.font = font;
    self.reasonLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.reasonLabel];
    
    self.descLabel = [[UILabel alloc]  initWithFrame:CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.reasonLabel.frame), mainWidth-12*2, 20)];
    self.descLabel.font = font;
    self.descLabel.textColor = COLOR_999999;
    [self.contentView addSubview:self.descLabel];
    
    self.cellHeight = CGRectGetMaxY(self.descLabel.frame)+14.5;
    
}
- (void) chatAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(chatBuyer:)]) {
        [self.delegate chatBuyer:[NSString stringWithFormat:@"%@,%@,%@",self.detailParser.buyer_id,self.detailParser.buyer_name,self.detailParser.buyer_avatar]];
    }
    
}

- (void) callAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(callBuyer:)]) {
        [self.delegate callBuyer:self.detailParser.buyer_tel];
    }
    
}

- (void)copyNumber:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(copyNumberWithString:)]) {
        [self.delegate copyNumberWithString:self.detailParser.refund_no];
    }
}
- (void)setDetailParser:(BusinessRefundDetailParser *)detailParser {
    _detailParser = detailParser;
    NSString *tempString = [NSString stringWithFormat:@"退款编号  %@",detailParser.refund_no];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                    value:COLOR_666666
                    range:NSMakeRange(4, tempString.length-4)];
    self.numberLabel.attributedText = attString;
    tempString = [NSString stringWithFormat:@"退款类型  %@",detailParser.refund_type];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.typeLabel.attributedText = attString;
    tempString = [NSString stringWithFormat:@"货物状态  %@",detailParser.goods_status_cn];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.statusLabel.attributedText = attString;
    tempString = [NSString stringWithFormat:@"退款现金  %.2f",[detailParser.sell_price  floatValue]];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.moneyLabel.attributedText = attString;
    
    tempString = [NSString stringWithFormat:@"申请时间  %@",detailParser.w_time];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.timeLabel.attributedText = attString;
    
    tempString = [NSString stringWithFormat:@"退款原因  %@",detailParser.refund_reason_cn];
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.reasonLabel.attributedText = attString;
    
    self.descLabel.numberOfLines = 0;
    NSString *contentString = [NSString stringWithFormat:@"退款说明  %@",detailParser.refund_reason_desc];
    UIFont *font = [UIFont systemFontOfSize:12];
    self.descLabel.font = font;
    tempString = contentString;
    attString = [[NSMutableAttributedString alloc] initWithString:tempString];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:COLOR_666666
                      range:NSMakeRange(4, tempString.length-4)];
    self.descLabel.attributedText = attString;
    // label可设置的最大高度和宽度
    CGSize size3 =CGSizeMake(mainWidth-12*2,300);
    NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
    //ios7方法，获取文本需要的size，限制宽度
    CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
    self.descLabel.frame = CGRectMake(CGRectGetMinX(self.numberLabel.frame), CGRectGetMaxY(self.reasonLabel.frame), mainWidth-12*2, actualsize3.height);
     self.cellHeight = CGRectGetMaxY(self.descLabel.frame)+14.5;
    [self.recopyButton addTarget:self action:@selector(copyNumber:) forControlEvents:UIControlEventTouchUpInside];
}

@end
