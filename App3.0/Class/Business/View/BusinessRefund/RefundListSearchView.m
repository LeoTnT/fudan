//
//  RefundListSearchView.m
//  App3.0
//
//  Created by nilin on 2017/12/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundListSearchView.h"
#import "DateTimePickerView.h"
#import "XSFormatterDate.h"

@interface RefundListSearchView ()<DateTimePickerViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UITextField *refundNumberText;
@property (nonatomic, strong) UITextField *orderNumberText;
@property (nonatomic, strong) UITextField *buyerNumberText;
@property (nonatomic, strong) UILabel *beginLabel;
@property (nonatomic, strong) UILabel *endLabel;
@property (nonatomic, strong)  DateTimePickerView *pickerView;
@property (nonatomic, assign) BOOL isBeginTime;
@end

@implementation RefundListSearchView

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self=[super initWithFrame: frame]) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    self.isBeginTime = YES;
    UIView *leftView = [UIView new];
    leftView.backgroundColor = [UIColor clearColor];
    [self addSubview:leftView];
    //0.76
    UIView *rightView = [UIView new];
    rightView.backgroundColor = [UIColor whiteColor];
    [self addSubview:rightView];
    
    UILabel *refundNumberLabel = [UILabel new];
    refundNumberLabel.text = @"退款编号";
    refundNumberLabel.textColor = COLOR_666666;
    refundNumberLabel.font = [UIFont systemFontOfSize:13];
    [rightView addSubview:refundNumberLabel];
    
    UIColor *color = [UIColor hexFloatColor:@"F4F4F4"];
    self.refundNumberText = [UITextField new];
    //    self.refundNumberText.placeholder = @"请输入退款编号";
    self.refundNumberText.backgroundColor = color;
    self.refundNumberText.layer.cornerRadius = 3;
    self.refundNumberText.delegate = self;
    self.refundNumberText.returnKeyType = UIReturnKeyDone;
    self.refundNumberText.layer.masksToBounds = YES;
    self.refundNumberText.font = [UIFont systemFontOfSize:14];
    [rightView addSubview:self.refundNumberText];
    
    UILabel *orderNumberLabel = [UILabel new];
    orderNumberLabel.text = Localized(@"order_no");
    orderNumberLabel.textColor = COLOR_666666;
    orderNumberLabel.font = [UIFont systemFontOfSize:13];
    [rightView addSubview:orderNumberLabel];
    
    self.orderNumberText = [UITextField new];
    //    self.orderNumberText.placeholder = Localized(@"input_order_num");
    self.orderNumberText.backgroundColor = color;
    self.orderNumberText.delegate = self;
    self.orderNumberText.returnKeyType = UIReturnKeyDone;
    self.orderNumberText.layer.cornerRadius = 3;
    self.orderNumberText.layer.masksToBounds = YES;
    self.orderNumberText.font = [UIFont systemFontOfSize:14];
    [rightView addSubview:self.orderNumberText];
    
    UILabel *buyerNumberLabel = [UILabel new];
    buyerNumberLabel.text = @"买家编号";
    buyerNumberLabel.textColor = COLOR_666666;
    buyerNumberLabel.font = [UIFont systemFontOfSize:13];
    [rightView addSubview:buyerNumberLabel];
    
    self.buyerNumberText = [UITextField new];
    //    self.buyerNumberText.placeholder = @"请输入买家编号";
    self.buyerNumberText.backgroundColor = color;
    self.buyerNumberText.layer.cornerRadius = 3;
    self.buyerNumberText.delegate = self;
    self.buyerNumberText.returnKeyType = UIReturnKeyDone;
    self.buyerNumberText.layer.masksToBounds = YES;
    self.buyerNumberText.font = [UIFont systemFontOfSize:14];
    [rightView addSubview:self.buyerNumberText];
    
    UILabel *line = [UILabel new];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [rightView addSubview:line];
    
    UILabel *applyLabel = [UILabel new];
    applyLabel.text = @"申请时间";
    applyLabel.textColor = COLOR_666666;
    applyLabel.font = [UIFont systemFontOfSize:13];
    [rightView addSubview:applyLabel];
    
    UILabel *beginTintLabel = [UILabel new];
    beginTintLabel.text = @"起始时间";
    beginTintLabel.textColor = COLOR_666666;
    beginTintLabel.font = [UIFont systemFontOfSize:10];
    [rightView addSubview:beginTintLabel];
    
    UILabel *endTintLabel = [UILabel new];
    endTintLabel.text = @"截止时间";
    endTintLabel.textColor = COLOR_666666;
    endTintLabel.font = [UIFont systemFontOfSize:10];
    [rightView addSubview:endTintLabel];
    
    self.beginLabel = [UILabel new];
    self.beginLabel.backgroundColor = color;
    self.beginLabel.layer.cornerRadius = 3;
    self.beginLabel.layer.masksToBounds = YES;
    self.beginLabel.textColor = [UIColor hexFloatColor:@"333333"];
    self.beginLabel.font = [UIFont systemFontOfSize:14];
    self.beginLabel.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *beginTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beginTap:)];
    self.beginLabel.userInteractionEnabled = YES;
    [self.beginLabel addGestureRecognizer:beginTap];
    [rightView addSubview:self.beginLabel];
    
    UILabel *line2 = [UILabel new];
    line2.backgroundColor = LINE_COLOR_NORMAL;
    [rightView addSubview:line2];
    
    self.endLabel = [UILabel new];
    self.endLabel.backgroundColor = color;
    self.endLabel.layer.cornerRadius = 3;
    self.endLabel.layer.masksToBounds = YES;
    self.endLabel.textColor = [UIColor hexFloatColor:@"333333"];
    self.endLabel.font = [UIFont systemFontOfSize:14];
    self.endLabel.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *endTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endTap:)];
    self.endLabel.userInteractionEnabled = YES;
    [self.endLabel addGestureRecognizer:endTap];
    [rightView addSubview:self.endLabel];
    
    UIButton *submitButton = [UIButton new];
    [submitButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont systemFontOfSize:15];
    submitButton.backgroundColor = mainColor;
    [submitButton addTarget:self action:@selector(clickToSubmit:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:submitButton];
    
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(mainWidth*0.24);
    }];
    
    [rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainWidth*0.24);
        make.right.top.bottom.mas_equalTo(self);
    }];
    CGFloat width = mainWidth*0.76-10.5*2;
    [refundNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(rightView).with.mas_offset(10.5);
        make.top.mas_equalTo(rightView).with.mas_offset(17+20);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(12.5);
    }];
    
    
    [self.refundNumberText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(refundNumberLabel.mas_bottom).with.mas_offset(10);
        make.left.right.mas_equalTo(refundNumberLabel);
        make.height.mas_equalTo(width*0.1);
    }];
    
    [orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.refundNumberText.mas_bottom).with.mas_offset(12);
        make.left.right.mas_equalTo(refundNumberLabel);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(12.5);
    }];
    
    
    [self.orderNumberText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(orderNumberLabel.mas_bottom).with.mas_offset(5);
        make.left.right.mas_equalTo(refundNumberLabel);
        make.height.mas_equalTo(width*0.1);
    }];
    
    [buyerNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orderNumberText.mas_bottom).with.mas_offset(12);
        make.left.right.mas_equalTo(refundNumberLabel);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(12.5);
    }];
    
    
    [self.buyerNumberText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(buyerNumberLabel.mas_bottom).with.mas_offset(5);
        make.left.right.mas_equalTo(refundNumberLabel);
        make.height.mas_equalTo(width*0.1);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.buyerNumberText.mas_bottom).with.mas_offset(15);
        make.left.mas_equalTo(refundNumberLabel);
        make.right.mas_equalTo(rightView);
        make.height.mas_equalTo(0.5);
    }];
    
    [applyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(refundNumberLabel);
        make.top.mas_equalTo(line.mas_bottom).with.mas_offset(14.5);
        make.width.height.mas_equalTo(refundNumberLabel);
    }];
    
    [beginTintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(rightView).with.mas_offset(24.5);
        make.top.mas_equalTo(applyLabel.mas_bottom).with.mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(80, 10));
    }];
    
    [self.beginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(rightView).with.mas_offset(14.5);
        make.width.mas_equalTo((mainWidth*0.76-14.5*2-36)/2);
        make.top.mas_equalTo(beginTintLabel.mas_bottom).with.mas_offset(4.5);
        make.height.mas_equalTo((mainWidth*0.76-14.5*2-36)/2*0.25);
    }];
    
    [endTintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.beginLabel.mas_right).with.mas_offset(10+36);
        make.top.mas_equalTo(beginTintLabel);
        make.size.mas_equalTo(CGSizeMake(80, 10));
    }];
    
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.beginLabel.mas_right).with.mas_offset(8);
        make.size.mas_equalTo(CGSizeMake(36-8*2, 0.5));
        make.top.mas_equalTo(self.beginLabel.mas_top).with.mas_equalTo((mainWidth*0.76-14.5*2-36)/2*0.25/2-0.25);
    }];
    [self.endLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.beginLabel.mas_right).with.mas_offset(36);
        make.width.height.top.mas_equalTo(self.beginLabel);
    }];
    
    [submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(rightView);
        make.left.right.mas_equalTo(rightView);
        make.height.mas_equalTo(35);
    }];
    
    
    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenView:)];
    self.userInteractionEnabled = YES;
    leftView.userInteractionEnabled = YES;
    [leftView addGestureRecognizer:tap];
}

- (void)didClickFinishDateTimePickerView:(NSString *)date{
    
    NSLog(@"XSFormatterDate====%@",date);
    if (self.isBeginTime) {
        
        self.beginLabel.text = date;
    } else {
        
        self.endLabel.text = date;
    }
}

- (void) beginTap:(UITapGestureRecognizer *) tap {
    self.isBeginTime = YES;
    self.pickerView = [[DateTimePickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.pickerViewMode = DatePickerViewDateMode;
    [self addSubview:self.pickerView];
    [self.pickerView showDateTimePickerView];
    
}

- (void) endTap:(UITapGestureRecognizer *) tap {
    self.isBeginTime = NO;
    self.pickerView = [[DateTimePickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.pickerViewMode = DatePickerViewDateMode;
    [self addSubview:self.pickerView];
    [self.pickerView showDateTimePickerView];
    
}

- (void)clickToSubmit:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(searchWithDictionary:)]) {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        if (!isEmptyString(self.refundNumberText.text)) {
            [param setObject:self.refundNumberText.text forKey:@"_search_refund_no"];
        }
        if (!isEmptyString(self.orderNumberText.text)) {
            [param setObject:self.orderNumberText.text forKey:@"_search_order_no"];
        }
        if (!isEmptyString(self.buyerNumberText.text)) {
            [param setObject:self.buyerNumberText.text forKey:@"_search_username"];
        }
        if (!isEmptyString(self.beginLabel.text)) {
            [param setObject:self.beginLabel.text forKey:@"_search_w_time__start_date"];
        }
        if (!isEmptyString(self.endLabel.text)) {
            [param setObject:self.endLabel.text forKey:@"_search_w_time__end_date"];
        }
        [self.delegate searchWithDictionary:param];
    }
    
}

- (void)hiddenView:(UITapGestureRecognizer *) tap {
    if ([self.delegate respondsToSelector:@selector(hiddenView)]) {
        [self.delegate hiddenView];
    }
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEditing:YES];
    return  YES;
}

@end
