//
//  RefundDetailTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundDetailTopTableViewCell.h"
#import "RefundAddPhotoView.h"

@interface RefundDetailTopTableViewCell()
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *lastTimeButton;
@property (nonatomic, strong) UITextField *remarkText;
@end

@implementation RefundDetailTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setSubviews];
    }
    return self;
    
}

- (void) setSubviews {
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 75)];
    self.topView.backgroundColor = mainColor;
    [self.contentView addSubview:self.topView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 16, mainWidth-12*2, 15.5)];
    self.titleLabel.textColor = [UIColor whiteColor];
    //    self.titleLabel.text = @"请处理退款申请";
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.topView addSubview:self.titleLabel];
    
    self.lastTimeButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.titleLabel.frame)+13, CGRectGetWidth(self.titleLabel.frame), 20)];
    [self.lastTimeButton setImage:[UIImage imageNamed:@"business_refund_ time"] forState:UIControlStateNormal];
    //    [self.lastTimeButton setTitle:@"剩3天23时23分" forState:UIControlStateNormal];
    self.lastTimeButton.titleLabel.font = [UIFont systemFontOfSize:14];
    self.lastTimeButton.hidden = YES;
    [self.lastTimeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 9.5, 0, 0)];
    self.lastTimeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.topView addSubview:self.lastTimeButton];
    
}

-(void)setDetailParser:(BusinessRefundDetailParser *)detailParser {
    
    _detailParser = detailParser;
    for (UIView *view in self.contentView.subviews) {
        if ([view isEqual:self.topView]||[view isEqual:self.titleLabel]||[view isEqual:self.lastTimeButton]) {
            
        } else {
            
            [view removeFromSuperview];
        }
    }
    CGFloat buttonWidth = 78,buttonHeight = 30;
    if ([detailParser.refund_status_en isEqualToString:@"waitAgree"]) {
        
        //待处理
        
        self.titleLabel.text = @"请处理退款申请";
        self.lastTimeButton.hidden = NO;
        [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
        //底部内容
        UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
        leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel1.layer.cornerRadius = 2.5;
        leftLabel1.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel1];
        UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
        
        rightLabel1.textColor = COLOR_999999;
        rightLabel1.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel1];
        
        UILabel *leftLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel1.frame)+16, 5, 5)];
        leftLabel2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel2.layer.cornerRadius = 2.5;
        leftLabel2.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel2];
        UILabel *rightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12)];
        
        rightLabel2.textColor = COLOR_999999;
        rightLabel2.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel2];
        
        UILabel *leftLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel2.frame)+15, 5, 5)];
        leftLabel3.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel3.layer.cornerRadius = 2.5;
        leftLabel3.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel3];
        UILabel *rightLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+8, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, 12)];
        
        [self.contentView addSubview:rightLabel3];
        
        UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
        agreeButton.layer.cornerRadius = 2;
        agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
        agreeButton.layer.borderWidth = 1;
        
        [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        agreeButton.layer.masksToBounds = YES;
        agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:agreeButton];
        
        UIButton *deliveryButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
        deliveryButton.layer.cornerRadius = 2;
        
        deliveryButton.layer.borderWidth = 0.5;
        deliveryButton.layer.borderColor = mainColor.CGColor;
        [deliveryButton setTitleColor:mainColor forState:UIControlStateNormal];
        deliveryButton.layer.masksToBounds = YES;
        deliveryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:deliveryButton];
        
        
        UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
        linkBuyerButton.layer.cornerRadius = 2;
        linkBuyerButton.layer.borderColor = mainColor.CGColor;
        linkBuyerButton.layer.borderWidth = 0.5;
        
        [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
        linkBuyerButton.layer.masksToBounds = YES;
        linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        self.cellHeight = CGRectGetMaxY(deliveryButton.frame)+17;
        
        if ([detailParser.order_status_en isEqualToString:@"waitSend"]) {
            
            //待发货
            rightLabel1.text = @"如果未发货，请点击同意退款给买家";
            rightLabel2.text = @"如果实际已发货，请主动与买家联系";
            NSString *contentString = @"如果您逾期未响应申请，视作同意买家申请，系统会自动退款给买家";
            rightLabel3.numberOfLines = 0;
            rightLabel3.textColor = COLOR_999999;
            UIFont *font = [UIFont systemFontOfSize:12];
            rightLabel3.font = font;
            rightLabel3.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11,300);
            NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
            rightLabel3.frame = CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+8, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, actualsize3.height);
            //            leftLabel3.frame = CGRectMake(12, CGRectGetMaxY(leftLabel2.frame)+16+2, 5, 5);
            [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
            [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
            [deliveryButton setTitle:@"发货" forState:UIControlStateNormal];
            [deliveryButton addTarget:self action:@selector(toDelivery) forControlEvents:UIControlEventTouchUpInside];
            agreeButton.frame =  CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
            deliveryButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
            self.cellHeight = CGRectGetMaxY(deliveryButton.frame)+17;
            if ([detailParser.can_guestbook integerValue]==1) {
                [self.contentView addSubview:linkBuyerButton];
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                deliveryButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                deliveryButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [deliveryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            
        }
        if ([detailParser.order_status_en isEqualToString:@"waitConfirm"]||[detailParser.order_status_en isEqualToString:@"success"]) {
            //已发货
            if ([detailParser.has_goods_return integerValue]==1) {
                NSString *contentString;
                //退款退货
                contentString = @"如果您同意，请点击“同意退货”，将正确退货地址给买家";
                rightLabel1.numberOfLines = 0;
                rightLabel1.textColor = COLOR_999999;
                UIFont *font = [UIFont systemFontOfSize:12];
                rightLabel1.font = font;
                rightLabel1.text = contentString;
                // label可设置的最大高度和宽度
                CGSize size1 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11,300);
                NSDictionary * tdic1 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
                //ios7方法，获取文本需要的size，限制宽度
                CGSize  actualsize1 =[contentString boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic1 context:nil].size;
                rightLabel1.text = contentString;
                rightLabel1.frame = CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, actualsize1.height);
                
                rightLabel2.text = @"如果您拒绝，买家可以申请客服介入";
                rightLabel2.frame = CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12);
                leftLabel2.frame = CGRectMake(12, CGRectGetMinY(rightLabel2.frame)+2, 5, 5);
                contentString = @"如果您预期未处理申请，视作同意买家申请，系统会自动将当期交易的退货地址发给买家";
                rightLabel3.numberOfLines = 0;
                rightLabel3.textColor = COLOR_999999;
                rightLabel3.font = font;
                rightLabel3.text = contentString;
                // label可设置的最大高度和宽度
                CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11,300);
                NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
                //ios7方法，获取文本需要的size，限制宽度
                CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
                rightLabel3.frame = CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+8, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, actualsize3.height);
                leftLabel3.frame = CGRectMake(12, CGRectGetMinY(rightLabel3.frame)+2, 5, 5);
                [agreeButton setTitle:@"同意退货" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(toAgreeRefundGoods) forControlEvents:UIControlEventTouchUpInside];
                [deliveryButton setTitle:@"拒绝申请" forState:UIControlStateNormal];
                [deliveryButton addTarget:self action:@selector(toRejectRefund) forControlEvents:UIControlEventTouchUpInside];
                
            } else {
                
                //仅退款
                rightLabel1.text = @"如果您同意，将直接退款给买家";
                rightLabel2.text = @"如果您拒绝，买家可以申请客服介入处理";
                NSString *contentString = @"如果您逾期未响应申请，视作同意买家申请，系统会自动退款给买家";
                rightLabel3.numberOfLines = 0;
                rightLabel3.textColor = COLOR_999999;
                UIFont *font = [UIFont systemFontOfSize:12];
                rightLabel3.font = font;
                rightLabel3.text = contentString;
                // label可设置的最大高度和宽度
                CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11,300);
                NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
                //ios7方法，获取文本需要的size，限制宽度
                CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
                rightLabel3.frame = CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+8, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, actualsize3.height);
                [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
                [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
                [deliveryButton setTitle:@"拒绝申请" forState:UIControlStateNormal];
                [deliveryButton addTarget:self action:@selector(toRejectRefund) forControlEvents:UIControlEventTouchUpInside];
                
            }
            agreeButton.frame =  CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
            deliveryButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
            if ([detailParser.can_guestbook integerValue]==1) {
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                deliveryButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                deliveryButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [deliveryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            self.cellHeight = CGRectGetMaxY(deliveryButton.frame)+17;
        }
        
        
    }
    
    if ([detailParser.refund_status_en isEqualToString:@"success"]) {
        
        //仅退款
        self.titleLabel.text = @"退款成功";
        self.lastTimeButton.hidden = YES;
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.titleLabel.frame)+14, mainWidth-2*12, 20)];
        timeLabel.text = detailParser.money_return_time;
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.font = [UIFont systemFontOfSize:14];
        [self.topView addSubview:timeLabel];
        
        //退款金额
        UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.topView.frame), 100, 44)];
        leftLabel.text = @"退款金额";
        leftLabel.textColor = [UIColor blackColor];
        leftLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:leftLabel];
        
        UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-12.5-150, CGRectGetMaxY(self.topView.frame), 150, 44)];
        moneyLabel.text = [NSString stringWithFormat:@"%.2f",[detailParser.sell_price floatValue]];
        moneyLabel.textColor = [UIColor hexFloatColor:@"FC4343"];
        moneyLabel.font = [UIFont systemFontOfSize:15];
        moneyLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:moneyLabel];
        self.cellHeight = CGRectGetMaxY(moneyLabel.frame);
        
        //        }
    }
    
    if ([detailParser.refund_status_en isEqualToString:@"closed"]) {
        self.titleLabel.text = @"退款关闭";
        self.lastTimeButton.hidden = YES;
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.titleLabel.frame)+14, mainWidth-2*12, 20)];
        timeLabel.text = detailParser.close_time;
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.font = [UIFont systemFontOfSize:14];
        [self.topView addSubview:timeLabel];
        
        if (!isEmptyString(detailParser.cs_status_en)) {
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            
            rightLabel1.textColor = COLOR_999999;
            rightLabel1.font = [UIFont systemFontOfSize:12];
            [self.contentView addSubview:rightLabel1];
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_cancel"]) {
                rightLabel1.text = @"买家撤销了退款申请";
            }
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_confirm"]) {
                rightLabel1.text = @"买家确认收货，退款申请关闭";
            }
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_pay"]) {
                rightLabel1.text = @"买家支付预售商品订单的尾款，退款申请关闭";
            }
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_timeout"]) {
                rightLabel1.text = @"因为买家超时未处理，退款申请关闭";
            }
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_cs"]) {
                rightLabel1.text = @"客服审核关闭了买家的退款申请";
            }
            if ([detailParser.close_reason_en isEqualToString:@"closed_by_send"]) {
                rightLabel1.text = @"因为卖家发货，退款申请关闭";
            }
            self.cellHeight = CGRectGetMaxY(rightLabel1.frame)+17;
        }
        
    }
    
    if ([detailParser.refund_status_en isEqualToString:@"refuseApply"]) {
        //已拒绝
        
        //客服是否介入
        
        if ([detailParser.cs_status_en isEqualToString:@"cs_wait_seller_say"]) {
            
            //待卖家提交凭证
            self.titleLabel.text = @"买家已申请客服介入，请提供凭证";
            self.lastTimeButton.hidden = NO;
            [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
            self.photoView = [[RefundAddPhotoView alloc] initWithFrame:CGRectZero];
            self.photoView.photosArray = self.photoArray;
            self.photoView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), mainWidth, self.photoView.height);
            [self.contentView addSubview:self.photoView];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.photoView.frame), mainWidth-11.5, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [self.contentView addSubview:line];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.photoView.frame), 100, 44)];
            descLabel.text = @"描述";
            descLabel.textColor = [UIColor blackColor];
            descLabel.font = [UIFont systemFontOfSize:15];
            [self.contentView addSubview:descLabel];
            self.remarkText = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(descLabel.frame), CGRectGetMaxY(self.photoView.frame), mainWidth-11.5-CGRectGetMaxX(descLabel.frame), 44)];
            self.remarkText.font = [UIFont systemFontOfSize:15];
            self.remarkText.textColor = [UIColor blackColor];
            self.remarkText.textAlignment = NSTextAlignmentRight;
            self.remarkText.placeholder = @"请输入描述内容";
            [self.contentView addSubview:self.remarkText];
            
            UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(descLabel.frame), mainWidth-11.5, 0.5)];
            line2.backgroundColor = LINE_COLOR_NORMAL;
            [self.contentView addSubview:line2];
            
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(line2.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UIFont *font = [UIFont systemFontOfSize:12];
            NSString *contentString = @"";
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            
            rightLabel1.textColor = COLOR_999999;
            rightLabel1.font = font;
            contentString = @"请上传凭证：【商品影响完好的实物图片聊天或记录截图】";
            rightLabel1.numberOfLines = 0;
            rightLabel1.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size1 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11,300);
            NSDictionary * tdic1 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize1 =[contentString boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic1 context:nil].size;
            rightLabel1.frame = CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+13, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, actualsize1.height);
            [self.contentView addSubview:rightLabel1];
            
            
            UILabel *leftLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(rightLabel1.frame)+16, 5, 5)];
            leftLabel2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel2.layer.cornerRadius = 2.5;
            leftLabel2.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel2];
            UILabel *rightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12)];
            rightLabel2.textColor = COLOR_999999;
            rightLabel2.font = font;
            contentString = @"上传完成后，请点击“举证并提交”，客服将根据您的凭证做出处理";
            rightLabel2.numberOfLines = 0;
            rightLabel2.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size2 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11,300);
            NSDictionary * tdic2 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize2 =[contentString boundingRectWithSize:size2 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic2 context:nil].size;
            rightLabel2.frame = CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, actualsize2.height);
            [self.contentView addSubview:rightLabel2];
            
            UILabel *leftLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(rightLabel2.frame)+16, 5, 5)];
            leftLabel3.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel3.layer.cornerRadius = 2.5;
            leftLabel3.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel3];
            UILabel *rightLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, 12)];
            rightLabel3.textColor = COLOR_999999;
            rightLabel3.font = font;
            contentString = @"逾期未提交，淘宝将做出退货退款处理";
            rightLabel3.numberOfLines = 0;
            rightLabel3.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11,300);
            NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
            rightLabel3.frame = CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, actualsize3.height);
            [self.contentView addSubview:rightLabel3];
            
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            
            UIButton *deliveryButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
            deliveryButton.layer.cornerRadius = 2;
            deliveryButton.layer.borderColor = mainColor.CGColor;
            deliveryButton.layer.borderWidth = 0.5;
            
            [deliveryButton setTitleColor:mainColor forState:UIControlStateNormal];
            deliveryButton.layer.masksToBounds = YES;
            deliveryButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [deliveryButton setTitle: @"举证并提交" forState:UIControlStateNormal];
            [deliveryButton addTarget:self action:@selector(sellerSay) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:deliveryButton];
            
            UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
            linkBuyerButton.layer.cornerRadius = 2;
            linkBuyerButton.layer.borderColor = mainColor.CGColor;
            linkBuyerButton.layer.borderWidth = 0.5;
            
            [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
            linkBuyerButton.layer.masksToBounds = YES;
            linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
            //是否退货
            if ([detailParser.has_goods_return integerValue]==1) {
                
                //已发货，退款退货
                [agreeButton setTitle:@"同意退货" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(toAgreeRefundGoods) forControlEvents:UIControlEventTouchUpInside];
            } else {
                
                //已发货，仅退款；
                [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
                [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
            }
            if ([detailParser.can_guestbook integerValue]==1) {
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                deliveryButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                deliveryButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [deliveryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
            
        } else  if ([detailParser.cs_status_en isEqualToString:@"cs_wait_judge"]) {
            
            //客服介入,已提交凭证
            self.titleLabel.text = @"买家已申请客服介入，请等待客服处理";
            self.lastTimeButton.hidden = YES;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            self.titleLabel.frame = CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.topView.frame));
            self.titleLabel.font = [UIFont systemFontOfSize:16];
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            rightLabel1.numberOfLines = 0;
            rightLabel1.textColor = COLOR_999999;
            UIFont *font = [UIFont systemFontOfSize:12];
            rightLabel1.font = font;
            NSString *contentString = @"客服正在审核凭证，大约在4~6个工作日内给出处理意见，请耐心等待";
            rightLabel1.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11,300);
            NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
            rightLabel1.frame = CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, actualsize3.height);
            [self.contentView addSubview:rightLabel1];
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = mainColor.CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:mainColor forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            linkBuyerButton.layer.cornerRadius = 2;
            linkBuyerButton.layer.borderColor = mainColor.CGColor;
            linkBuyerButton.layer.borderWidth = 0.5;
            
            [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
            linkBuyerButton.layer.masksToBounds = YES;
            linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
            //是否退货
            if ([detailParser.has_goods_return integerValue]==1) {
                
                //已发货，退款退货
                [agreeButton setTitle:@"同意退货" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(toAgreeRefundGoods) forControlEvents:UIControlEventTouchUpInside];
            } else {
                
                //已发货，仅退款；
                [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
                [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
            }
            if ([detailParser.can_guestbook integerValue]==1) {
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight);
                
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
        } else {
            
            self.titleLabel.text = @"已拒绝退款申请，请等待买家响应";
            self.lastTimeButton.hidden = NO;
            [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.w_time] forState:UIControlStateNormal];
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            
            rightLabel1.textColor = COLOR_999999;
            rightLabel1.font = [UIFont systemFontOfSize:12];
            rightLabel1.text = @"如果买家超时未响应，退款申请将自动关闭";
            [self.contentView addSubview:rightLabel1];
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = mainColor.CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:mainColor forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            linkBuyerButton.layer.cornerRadius = 2;
            linkBuyerButton.layer.borderColor = mainColor.CGColor;
            linkBuyerButton.layer.borderWidth = 0.5;
            
            [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
            linkBuyerButton.layer.masksToBounds = YES;
            linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
            //已拒绝
            if ([detailParser.has_goods_return integerValue]==1) {
                
                //已发货，退款退货
                [agreeButton setTitle:@"同意退货" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(toAgreeRefundGoods) forControlEvents:UIControlEventTouchUpInside];
            } else {
                
                //已发货，仅退款；
                [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
                [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
            }
            
            if ([detailParser.can_guestbook integerValue]==1) {
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight);
                
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
        }

    }
    
    if ([detailParser.refund_status_en isEqualToString:@"waitBack"]) {
        //待买家发货
        self.titleLabel.text = @"请等待买家发货";
        self.lastTimeButton.hidden = NO;
        [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
        UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
        leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel1.layer.cornerRadius = 2.5;
        leftLabel1.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel1];
        UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
        
        rightLabel1.textColor = COLOR_999999;
        rightLabel1.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel1];
        
        UILabel *leftLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel1.frame)+16, 5, 5)];
        leftLabel2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel2.layer.cornerRadius = 2.5;
        leftLabel2.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel2];
        UILabel *rightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12)];
        
        rightLabel2.textColor = COLOR_999999;
        rightLabel2.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel2];
        
        rightLabel1.text = @"收到买家退货时，请验货后同意退款";
        rightLabel2.text = @"如果买家在超时结束前未退货，退货申请将自动关闭";
        UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel2.frame)+17, buttonWidth, buttonHeight)];
        agreeButton.layer.cornerRadius = 2;
        agreeButton.layer.borderColor = mainColor.CGColor;
        agreeButton.layer.borderWidth = 0.5;
        
        [agreeButton setTitleColor:mainColor forState:UIControlStateNormal];
        agreeButton.layer.masksToBounds = YES;
        agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:agreeButton];
        [agreeButton setTitle:@"收货并退款" forState:UIControlStateNormal];
        [agreeButton addTarget:self action:@selector(agreeRefundAndReceiverGoods) forControlEvents:UIControlEventTouchUpInside];
        if ([detailParser.can_guestbook integerValue]==1) {
            UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel2.frame)+17, buttonWidth, buttonHeight)];
            linkBuyerButton.layer.cornerRadius = 2;
            linkBuyerButton.layer.borderColor = mainColor.CGColor;
            linkBuyerButton.layer.borderWidth = 0.5;
            
            [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
            linkBuyerButton.layer.masksToBounds = YES;
            linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:linkBuyerButton];
            agreeButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel2.frame)+17, buttonWidth, buttonHeight);
            
            [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
            linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
            [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
            agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
            [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
    }
    
    if ([detailParser.refund_status_en isEqualToString:@"waitConfirm"]) {
        //卖家同意退货后卖家已发货
        self.titleLabel.text = @"请确认收货";
        self.lastTimeButton.hidden = NO;
        [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
        //底部
        UILabel *tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(12.5, CGRectGetMaxY(self.topView.frame)+15, mainWidth-12.5*2, 15.5)];
        tintLabel.textColor = [UIColor  blackColor];
        tintLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:tintLabel];
        tintLabel.text = @"买家已退货";
        
        UILabel *logisticLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(tintLabel.frame), CGRectGetMaxY(tintLabel.frame)+12.5, CGRectGetWidth(tintLabel.frame), 13.5)];
        logisticLabel.textColor = COLOR_666666;
        logisticLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:logisticLabel];
        BusinessRefundDetailLogisticsParser *logistics = detailParser.logistics;
        logisticLabel.text = [NSString stringWithFormat:@"%@（%@）",logistics.logistics_com, logistics.com_tel];
        
        
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(logisticLabel.frame)+17.5, mainWidth-11.5, 0.5)];
        line2.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line2];
        
        
        UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(line2.frame)+17, 5, 5)];
        leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel1.layer.cornerRadius = 2.5;
        leftLabel1.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel1];
        UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
        
        rightLabel1.textColor = COLOR_999999;
        rightLabel1.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel1];
        
        UILabel *leftLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel1.frame)+16, 5, 5)];
        leftLabel2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel2.layer.cornerRadius = 2.5;
        leftLabel2.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel2];
        UILabel *rightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12)];
        
        rightLabel2.textColor = COLOR_999999;
        rightLabel2.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel2];
        
        UILabel *leftLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel2.frame)+16, 5, 5)];
        leftLabel3.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel3.layer.cornerRadius = 2.5;
        leftLabel3.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel3];
        UILabel *rightLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, 12)];
        rightLabel3.textColor = COLOR_999999;
        rightLabel3.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel3];
        
        
        UILabel *leftLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(leftLabel3.frame)+16, 5, 5)];
        leftLabel4.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
        leftLabel4.layer.cornerRadius = 2.5;
        leftLabel4.layer.masksToBounds = YES;
        [self.contentView addSubview:leftLabel4];
        UILabel *rightLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel4.frame)+11, CGRectGetMaxY(rightLabel3.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel4.frame)-11, 12)];
        rightLabel4.textColor = COLOR_999999;
        rightLabel4.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:rightLabel4];
        
        
        rightLabel1.text = @"买家已退货，收到买家退货时，请验收后同意退款";
        rightLabel2.text = @"如果拒绝退款，买家可以申请客服介入处理";
        rightLabel3.text = @"如果您逾期未处理，将自动退款给买家";
        rightLabel4.text = @"同意退款后退款将立即返还买家账户";
        
        
        buttonWidth = 68;
        UIButton *logisticButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight)];
        logisticButton.layer.cornerRadius = 2;
        logisticButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
        logisticButton.layer.borderWidth = 1;
        
        [logisticButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        logisticButton.layer.masksToBounds = YES;
        logisticButton.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [self.contentView addSubview:logisticButton];
        
        UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight)];
        agreeButton.layer.cornerRadius = 2;
        agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
        agreeButton.layer.borderWidth = 1;
        
        [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        agreeButton.layer.masksToBounds = YES;
        agreeButton.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [self.contentView addSubview:agreeButton];
        
        UIButton *refuseButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight)];
        refuseButton.layer.cornerRadius = 2;
        refuseButton.layer.borderColor = mainColor.CGColor;
        refuseButton.layer.borderWidth = 0.5;
        
        [refuseButton setTitleColor:mainColor forState:UIControlStateNormal];
        refuseButton.layer.masksToBounds = YES;
        refuseButton.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [self.contentView addSubview:refuseButton];
        
        [logisticButton setTitle:@"查看物流" forState:UIControlStateNormal];
        [agreeButton setTitle:@"收货并退款" forState:UIControlStateNormal];
        [refuseButton setTitle:@"拒绝退款" forState:UIControlStateNormal];
        [logisticButton addTarget:self action:@selector(lookLogistics) forControlEvents:UIControlEventTouchUpInside];
        [agreeButton addTarget:self action:@selector(agreeRefundAndReceiverGoods) forControlEvents:UIControlEventTouchUpInside];
        [refuseButton addTarget:self action:@selector(toRejectRefundGoods) forControlEvents:UIControlEventTouchUpInside];
        if ([detailParser.can_guestbook integerValue]==1) {
            UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight)];
            linkBuyerButton.layer.cornerRadius = 2;
            linkBuyerButton.layer.borderColor = mainColor.CGColor;
            linkBuyerButton.layer.borderWidth = 0.5;
            
            [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
            linkBuyerButton.layer.masksToBounds = YES;
            linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:13];
            [self.contentView addSubview:linkBuyerButton];
            logisticButton.frame = CGRectMake(mainWidth-12.5-10*3-buttonWidth*4, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight);
            agreeButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight);
            refuseButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel4.frame)+17, buttonWidth, buttonHeight);
            
            [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
            linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
            [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
            refuseButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
            [refuseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
        
    }
    
    if ([detailParser.refund_status_en isEqualToString:@"refuseConfirm"]) {
        //卖家拒绝退款
        
        if ([detailParser.cs_status_en isEqualToString:@"cs_wait_seller_say"]) {
            
            //待卖家提交凭证
            self.titleLabel.text = @"买家已申请客服介入，请提供凭证";
            self.lastTimeButton.hidden = NO;
            [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
            self.photoView = [[RefundAddPhotoView alloc] initWithFrame:CGRectZero];
            self.photoView.photosArray = self.photoArray;
            self.photoView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), mainWidth, self.photoView.height);
            [self.contentView addSubview:self.photoView];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.photoView.frame), mainWidth-11.5, 0.5)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [self.contentView addSubview:line];
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(self.photoView.frame), 100, 44)];
            descLabel.text = @"描述";
            descLabel.textColor = [UIColor blackColor];
            descLabel.font = [UIFont systemFontOfSize:15];
            [self.contentView addSubview:descLabel];
            self.remarkText = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(descLabel.frame), CGRectGetMaxY(self.photoView.frame), mainWidth-11.5-CGRectGetMaxX(descLabel.frame), 44)];
            self.remarkText.font = [UIFont systemFontOfSize:15];
            self.remarkText.textColor = [UIColor blackColor];
            self.remarkText.textAlignment = NSTextAlignmentRight;
            self.remarkText.placeholder = @"请输入描述内容";
            [self.contentView addSubview:self.remarkText];
            
            UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(descLabel.frame), mainWidth-11.5, 0.5)];
            line2.backgroundColor = LINE_COLOR_NORMAL;
            [self.contentView addSubview:line2];
            
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(line2.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UIFont *font = [UIFont systemFontOfSize:12];
            NSString *contentString = @"";
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            
            rightLabel1.textColor = COLOR_999999;
            rightLabel1.font = font;
            contentString = @"请上传凭证：【商品影响完好的实物图片聊天或记录截图】";
            rightLabel1.numberOfLines = 0;
            rightLabel1.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size1 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11,300);
            NSDictionary * tdic1 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize1 =[contentString boundingRectWithSize:size1 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic1 context:nil].size;
            rightLabel1.frame = CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+13, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, actualsize1.height);
            [self.contentView addSubview:rightLabel1];
            
            
            UILabel *leftLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(rightLabel1.frame)+16, 5, 5)];
            leftLabel2.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel2.layer.cornerRadius = 2.5;
            leftLabel2.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel2];
            UILabel *rightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, 12)];
            rightLabel2.textColor = COLOR_999999;
            rightLabel2.font = font;
            contentString = @"上传完成后，请点击“举证并提交”，客服将根据您的凭证做出处理";
            rightLabel2.numberOfLines = 0;
            rightLabel2.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size2 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11,300);
            NSDictionary * tdic2 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize2 =[contentString boundingRectWithSize:size2 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic2 context:nil].size;
            rightLabel2.frame = CGRectMake(CGRectGetMaxX(leftLabel2.frame)+11, CGRectGetMaxY(rightLabel1.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel2.frame)-11, actualsize2.height);
            [self.contentView addSubview:rightLabel2];
            
            UILabel *leftLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(rightLabel2.frame)+16, 5, 5)];
            leftLabel3.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel3.layer.cornerRadius = 2.5;
            leftLabel3.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel3];
            UILabel *rightLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, 12)];
            rightLabel3.textColor = COLOR_999999;
            rightLabel3.font = font;
            contentString = @"逾期未提交，淘宝将做出退货退款处理";
            rightLabel3.numberOfLines = 0;
            rightLabel3.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11,300);
            NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
            rightLabel3.frame = CGRectMake(CGRectGetMaxX(leftLabel3.frame)+11, CGRectGetMaxY(rightLabel2.frame)+9, mainWidth-23.5-CGRectGetMaxX(leftLabel3.frame)-11, actualsize3.height);
            [self.contentView addSubview:rightLabel3];
            
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            
            UIButton *deliveryButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
            deliveryButton.layer.cornerRadius = 2;
            deliveryButton.layer.borderColor = mainColor.CGColor;
            deliveryButton.layer.borderWidth = 0.5;
            
            [deliveryButton setTitleColor:mainColor forState:UIControlStateNormal];
            deliveryButton.layer.masksToBounds = YES;
            deliveryButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [deliveryButton setTitle: @"举证并提交" forState:UIControlStateNormal];
            [deliveryButton addTarget:self action:@selector(sellerSay) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:deliveryButton];
            //是否退货
            if ([detailParser.has_goods_return integerValue]==1) {
                
                //已发货，退款退货
                [agreeButton setTitle:@"收货并退款" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(agreeRefundAndReceiverGoods) forControlEvents:UIControlEventTouchUpInside];
            }
            if ([detailParser.can_guestbook integerValue]==1) {
                UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight)];
                linkBuyerButton.layer.cornerRadius = 2;
                linkBuyerButton.layer.borderColor = mainColor.CGColor;
                linkBuyerButton.layer.borderWidth = 0.5;
                
                [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
                linkBuyerButton.layer.masksToBounds = YES;
                linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                deliveryButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel3.frame)+17, buttonWidth, buttonHeight);
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                deliveryButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [deliveryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
            
        } else  if ([detailParser.cs_status_en isEqualToString:@"cs_wait_judge"]) {
            
            //客服介入,已提交凭证
            self.titleLabel.text = @"买家已申请客服介入，请等待客服处理";
            self.lastTimeButton.hidden = YES;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            self.titleLabel.frame = CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.topView.frame));
            self.titleLabel.font = [UIFont systemFontOfSize:16];
            //底部内容
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.topView.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            rightLabel1.numberOfLines = 0;
            rightLabel1.textColor = COLOR_999999;
            UIFont *font = [UIFont systemFontOfSize:12];
            rightLabel1.font = font;
            NSString *contentString = @"客服正在审核凭证，大约在4~6个工作日内给出处理意见，请耐心等待";
            rightLabel1.text = contentString;
            // label可设置的最大高度和宽度
            CGSize size3 =CGSizeMake(mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11,300);
            NSDictionary * tdic3 = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
            //ios7方法，获取文本需要的size，限制宽度
            CGSize  actualsize3 =[contentString boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic3 context:nil].size;
            rightLabel1.frame = CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(self.topView.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, actualsize3.height);
            [self.contentView addSubview:rightLabel1];
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = mainColor.CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:mainColor forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            
            //是否退货
            if ([detailParser.has_goods_return integerValue]==1) {
                
                //已发货，退款退货
                [agreeButton setTitle:@"收货并退款" forState:UIControlStateNormal];
                [agreeButton addTarget:self action:@selector(agreeRefundAndReceiverGoods) forControlEvents:UIControlEventTouchUpInside];
            }
            if ([detailParser.can_guestbook integerValue]==1) {
                UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
                linkBuyerButton.layer.cornerRadius = 2;
                linkBuyerButton.layer.borderColor = mainColor.CGColor;
                linkBuyerButton.layer.borderWidth = 0.5;
                
                [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
                linkBuyerButton.layer.masksToBounds = YES;
                linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
                [self.contentView addSubview:linkBuyerButton];
                agreeButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight);
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
        } else {
            //买家未申请客服介入
            self.titleLabel.text = @"请等待买家响应";
            self.lastTimeButton.hidden = NO;
            [self.lastTimeButton setTitle:[NSString stringWithFormat:@"剩%@",detailParser.format_remain_time] forState:UIControlStateNormal];
            //底部
            UILabel *tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(12.5, CGRectGetMaxY(self.topView.frame)+15, mainWidth-12.5*2, 15.5)];
            tintLabel.textColor = [UIColor  blackColor];
            tintLabel.font = [UIFont systemFontOfSize:16];
            [self.contentView addSubview:tintLabel];
            tintLabel.text = @"买家已退货";
            
            UILabel *logisticLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(tintLabel.frame), CGRectGetMaxY(tintLabel.frame)+12.5, CGRectGetWidth(tintLabel.frame), 13.5)];
            logisticLabel.textColor = COLOR_666666;
            logisticLabel.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:logisticLabel];
            BusinessRefundDetailLogisticsParser *logistics = detailParser.logistics;
            logisticLabel.text = [NSString stringWithFormat:@"%@（%@）",logistics.logistics_com, logistics.com_tel];
            
            
            UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(11.5, CGRectGetMaxY(logisticLabel.frame)+17.5, mainWidth-11.5, 0.5)];
            line2.backgroundColor = LINE_COLOR_NORMAL;
            [self.contentView addSubview:line2];
            
            
            UILabel *leftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(line2.frame)+17, 5, 5)];
            leftLabel1.backgroundColor = [UIColor hexFloatColor:@"DDDDDD"];
            leftLabel1.layer.cornerRadius = 2.5;
            leftLabel1.layer.masksToBounds = YES;
            [self.contentView addSubview:leftLabel1];
            UILabel *rightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftLabel1.frame)+11, CGRectGetMaxY(line2.frame)+15, mainWidth-23.5-CGRectGetMaxX(leftLabel1.frame)-11, 12)];
            
            rightLabel1.textColor = COLOR_999999;
            rightLabel1.font = [UIFont systemFontOfSize:12];
            [self.contentView addSubview:rightLabel1];
            rightLabel1.text = @"如果买家超时未响应，退款申请将自动关闭";
            UIButton *logisticButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            logisticButton.layer.cornerRadius = 2;
            logisticButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
            logisticButton.layer.borderWidth = 1;
            
            [logisticButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            logisticButton.layer.masksToBounds = YES;
            logisticButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:logisticButton];
            
            UIButton *agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
            agreeButton.layer.cornerRadius = 2;
            agreeButton.layer.borderColor = mainColor.CGColor;
            agreeButton.layer.borderWidth = 1;
            
            [agreeButton setTitleColor:mainColor forState:UIControlStateNormal];
            agreeButton.layer.masksToBounds = YES;
            agreeButton.titleLabel.font = [UIFont systemFontOfSize:14];
            
            [self.contentView addSubview:agreeButton];
            
            
            [logisticButton setTitle:@"查看物流" forState:UIControlStateNormal];
            [agreeButton setTitle:@"同意退款" forState:UIControlStateNormal];
            [logisticButton addTarget:self action:@selector(lookLogistics) forControlEvents:UIControlEventTouchUpInside];
            [agreeButton addTarget:self action:@selector(toAgreeRefund) forControlEvents:UIControlEventTouchUpInside];
            if ([detailParser.can_guestbook integerValue]==1) {
                UIButton *linkBuyerButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight)];
                linkBuyerButton.layer.cornerRadius = 2;
                linkBuyerButton.layer.borderColor = mainColor.CGColor;
                linkBuyerButton.layer.borderWidth = 0.5;
                
                [linkBuyerButton setTitleColor:mainColor forState:UIControlStateNormal];
                linkBuyerButton.layer.masksToBounds = YES;
                linkBuyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
                [self.contentView addSubview:linkBuyerButton];
                
                logisticButton.frame = CGRectMake(mainWidth-12.5-10*2-buttonWidth*3, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight);
                agreeButton.frame = CGRectMake(mainWidth-12.5-10-buttonWidth*2, CGRectGetMaxY(rightLabel1.frame)+17, buttonWidth, buttonHeight);
                [linkBuyerButton addTarget:self action:@selector(leaveWords) forControlEvents:UIControlEventTouchUpInside];
                linkBuyerButton.frame = CGRectMake(mainWidth-12.5-buttonWidth, CGRectGetMinY(agreeButton.frame), buttonWidth, buttonHeight);
                [linkBuyerButton setTitle:@"给买家留言" forState:UIControlStateNormal];
                agreeButton.layer.borderColor = [UIColor hexFloatColor:@"BABABA"].CGColor;
                [agreeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
            }
            self.cellHeight = CGRectGetMaxY(agreeButton.frame)+17;
            
        }
        
        
        
    }
}

- (void) lookLogistics {
    if ([self.delegate respondsToSelector:@selector(lookLogistics)]) {
        [self.delegate lookLogistics];
    }
    
}

- (void)leaveWords {
    if ([self.delegate respondsToSelector:@selector(leaveWordsWithRefund:)]) {
        [self.delegate leaveWordsWithRefund:self.detailParser.refund_no];
    }
    
}

- (void) agreeRefundAndReceiverGoods {
    if ([self.delegate respondsToSelector:@selector(refundConfirmWithRefund:)]) {
        [self.delegate refundConfirmWithRefund:self.detailParser.refund_no];
    }
    
}

- (void) sellerSay {
    //举证提交
    if ([self.delegate respondsToSelector:@selector(sellerSayWithRefund:)]) {
        if (isEmptyString(self.remarkText.text)) {
            [XSTool showToastWithView:self Text:@"请完善描述！"];
            return;
        } else {
            [self.delegate sellerSayWithRefund:@{@"refund_id":self.detailParser.refund_no,@"remark":self.remarkText.text}];
        }
        
    }
    
}

- (void) toAgreeRefundGoods {
    
    if ([self.delegate respondsToSelector:@selector(agreeReturnRefundApplyWithRefund:)]) {
        [self.delegate agreeReturnRefundApplyWithRefund:self.detailParser.refund_no];
    }
}

- (void)toAgreeRefund {
    if ([self.delegate respondsToSelector:@selector(agreeOnlyRefundApplyWithRefund:)]) {
        [self.delegate agreeOnlyRefundApplyWithRefund:self.detailParser.refund_no];
    }
}


- (void)toDelivery {
    if ([self.delegate respondsToSelector:@selector(deliveryGoodsWithOrderId:)]) {
        [self.delegate deliveryGoodsWithOrderId:self.detailParser.order_id];
    }
}

- (void)toRejectRefund {
    if ([self.delegate respondsToSelector:@selector(rejectRefundApply:)]) {
        [self.delegate rejectRefundApply:self.detailParser.refund_no];
    }
}

- (void)toRejectRefundGoods {
    
    if ([self.delegate respondsToSelector:@selector(rejectReceiverRefundApplyMoneyRefund:)]) {
        [self.delegate rejectReceiverRefundApplyMoneyRefund:self.detailParser.refund_no];
    }
}
@end
