//
//  BusinessRefundListTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessRefundModel.h"
#import "RefundDelegate.h"

@interface BusinessRefundListTableViewCell : UITableViewCell

@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic, strong) BusinessRefundListDataParser *refundDataParser;
@property (nonatomic, weak) id<RefundDelegate> delegate;
@end
