//
//  RefundAddPhotoView.m
//  App3.0
//
//  Created by nilin on 2018/1/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "RefundAddPhotoView.h"

@interface RefundAddPhotoView()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *tintLabel;
@end
@implementation RefundAddPhotoView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(11, 15, 80, 14.5)];
        self.titleLabel.text = @"上传凭证";
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        self.titleLabel.textColor = [UIColor blackColor];
        [self addSubview:self.titleLabel];
        
        self.tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+12.5, 17.5, mainWidth-12.5*2-CGRectGetMaxX(self.titleLabel.frame), 11.5)];
        self.tintLabel.text = @"最多上传3张";
        self.tintLabel.font = [UIFont systemFontOfSize:12];
        self.tintLabel.textColor = COLOR_999999;
        [self addSubview:self.tintLabel];
    }
    return self;
}

#pragma mark-刷新界面
-(void)setPhotosArray:(NSArray *)photosArray{
    _photosArray=photosArray;
    self.deletBtnArray=[NSMutableArray array];
    self.imagesArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]||[view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    UIImageView *img;
    if (self.photosArray.count==0) {
        return;
    }
    CGFloat imageSize = 75;
    
    for (int i=0; i<self.photosArray.count; i++) {
        int columns= (mainWidth-12)/(imageSize+12);
        //列数
        int col=i%columns;
        //行数
        int row=i/columns;
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(12+col*(imageSize+12),CGRectGetMaxY(self.titleLabel.frame)+15.5+row*(imageSize+10), imageSize, imageSize)];
        imgView.image=[self.photosArray objectAtIndex:i];
        img=imgView;
        imgView.userInteractionEnabled=YES;
        [self addSubview:imgView];
        //添加删除按钮×
        if (i<self.photosArray.count-1) {
            UIButton *deleteBtn=[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)-10, CGRectGetMinY(img.frame)-10, 20, 20)];
            deleteBtn.backgroundColor = [UIColor whiteColor];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
            deleteBtn.layer.cornerRadius = CGRectGetHeight(deleteBtn.frame)/2;
            deleteBtn.layer.masksToBounds = YES;
            [self addSubview:deleteBtn];
            [self.deletBtnArray addObject:deleteBtn];
        }
    }
    self.lastImage=img;
    self.lastImage.userInteractionEnabled=YES;
    if (photosArray.count==0) {
        self.height=NORMOL_SPACE*14;
    }else{
        self.height=CGRectGetMaxY(img.frame)+15;
    }
}

@end
