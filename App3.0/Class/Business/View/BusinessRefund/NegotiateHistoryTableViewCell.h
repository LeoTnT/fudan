//
//  NegotiateHistoryTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessRefundModel.h"


@interface NegotiateHistoryTableViewCell : UITableViewCell

@property (nonatomic, strong) BusinessRefundNegotiateHistoryParser *dataParser;

@property (nonatomic, assign) CGFloat cellHeight;

@end
