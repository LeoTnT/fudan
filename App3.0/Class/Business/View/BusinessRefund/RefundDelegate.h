//
//  RefundDelegate.h
//  App3.0
//
//  Created by nilin on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RefundDelegate <NSObject>
@optional
/** 已发货，拒绝退款*/
-(void)rejectRefundApply:(NSString *) refundId;

/**  联系买家*/
-(void) chatBuyer:(NSString *) uidAndNameAndLogo;
/**  拨打电话*/
-(void) callBuyer:(NSString *) mobile;

/**  同意退款:仅退款refund_id，*/
-(void) agreeOnlyRefundApplyWithRefund:(NSString *) refundId;

/** 同意退款退货退货退款refund_id+...*/
-(void) agreeReturnRefundApplyWithRefund:(NSString *) refundId;

/**发货*/
- (void) deliveryGoodsWithOrderId:(NSString *) orderId;

/**举证提交*/
- (void) sellerSayWithRefund:(NSDictionary *) param;

/**查看物流*/
- (void) lookLogistics;

/**收货并退款*/
- (void)refundConfirmWithRefund:(NSString *) refundId;

/**已退货，卖家拒绝退款*/
- (void)rejectReceiverRefundApplyMoneyRefund:(NSString *) refundId;

/**复制编号*/
- (void) copyNumberWithString:(NSString *) copyString;

/**给买家留言*/
- (void) leaveWordsWithRefund:(NSString *) refundId;
@end
