//
//  RefundManageTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundManageTopTableViewCell.h"
@interface RefundManageTopTableViewCell()
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UILabel *tintLabel;
@property (nonatomic, strong) UILabel *upToDateLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *bottomView;
@end
@implementation RefundManageTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor hexFloatColor:@"118EEA"];
        self.countLabel = [UILabel new];
        self.countLabel.textColor = [UIColor whiteColor];
        self.countLabel.font = [UIFont systemFontOfSize:42];
        self.countLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.countLabel];
        
        self.tintLabel = [UILabel new];
        self.tintLabel.textColor = [UIColor whiteColor];
        self.tintLabel.alpha = 0.5;
        self.tintLabel.font = [UIFont systemFontOfSize:14];
        self.tintLabel.text = @"待处理(笔)";
        self.tintLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.tintLabel];
        
        self.bottomView = [UIView new];
//        bottomView.backgroundColor = [UIColor redColor];
        self.bottomView.alpha = 0.9;
        [self.contentView addSubview:self.bottomView];
        
        self.upToDateLabel = [UILabel new];
//        self.upToDateLabel.text = @"最新一笔 12月25日 8:03";
        self.upToDateLabel.textColor = [UIColor whiteColor];
        self.upToDateLabel.font = [UIFont qsh_systemFontOfSize:13];
        [self.bottomView addSubview:self.upToDateLabel];
        
        self.statusLabel = [UILabel new];
//        self.statusLabel.text = @"退款退货";
        self.statusLabel.textColor = [UIColor whiteColor];
        self.statusLabel.textAlignment = NSTextAlignmentRight;
        self.statusLabel.font = [UIFont qsh_systemFontOfSize:13];
        [self.bottomView addSubview:self.statusLabel];
        
        self.bottomView.backgroundColor = [UIColor hexFloatColor:@"299AEC"];
        
        [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.contentView);
            make.width.mas_lessThanOrEqualTo(mainWidth-12*2);
            make.top.mas_equalTo(94);
            make.height.mas_equalTo(34);
        }];
        
        
        [self.tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.countLabel.mas_bottom).with.mas_offset(10.5);
            make.centerX.mas_equalTo(self.contentView);
            make.width.mas_lessThanOrEqualTo(mainWidth-12*2);
            make.height.mas_equalTo(15.5);
        }];
        
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.height.mas_equalTo(29);
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo(100);
            make.right.mas_equalTo(self.bottomView).with.mas_offset(-13.5);
            make.height.top.mas_equalTo(self.bottomView);
        }];
        
        [self.upToDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bottomView).with.mas_offset(12);
            make.right.mas_equalTo(self.statusLabel.mas_left).with.mas_offset(-10);
            make.height.top.mas_equalTo(self.bottomView);
        }];
        
       
        
        
    }
    return self;
}

-(void)setRefundParser:(BusinessRefundMainParser *)refundParser {
    
    _refundParser = refundParser;
    self.countLabel.text = refundParser.wait_deal_num;
    if (isEmptyString(refundParser.last_refund)) {
        self.bottomView.hidden = YES;
    } else {
        self.upToDateLabel.text = [NSString stringWithFormat:@"最新一笔 %@",refundParser.last_refund.format_time];
        self.statusLabel.text = refundParser.last_refund.refund_type;
    }

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickToLookOrderList:)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickToLookOrderList:)];
    self.countLabel.userInteractionEnabled = YES;
    self.tintLabel.userInteractionEnabled = YES;
    self.userInteractionEnabled = YES;
    
    [self.countLabel addGestureRecognizer:tap];
    [self.tintLabel addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickToLookRefund:)];
    self.bottomView.userInteractionEnabled = YES;
    self.userInteractionEnabled = YES;
    [self.bottomView addGestureRecognizer:tap3];
}

- (void)clickToLookOrderList:(UITapGestureRecognizer *) tap {
    if ([self.delegate respondsToSelector:@selector(lookWaitCheckOrder)]) {
        [self.delegate lookWaitCheckOrder];
    }
    
}

- (void) clickToLookRefund:(UITapGestureRecognizer *) tap {
    
    if ([self.delegate respondsToSelector:@selector(lookLastRefundWithRefundId:)]) {
        [self.delegate lookLastRefundWithRefundId:self.refundParser.last_refund.refund_no];
    }
}
@end
