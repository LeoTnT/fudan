//
//  BusinessRefundListTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessRefundListTableViewCell.h"
#import "UIImage+XSWebImage.h"

@interface BusinessRefundListTableViewCell()
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *lastTimeButton;
@property (nonatomic, strong) UIButton *buyerButton;
@property (nonatomic, strong) UILabel *refundStatusLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@end

@implementation BusinessRefundListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.cellHeight = 140;
        CGFloat imageSize = 70,titleHeight = 40,titlWidth = (mainWidth-11.5-12.5-10)/2;
        self.buyerButton = [[UIButton alloc] initWithFrame:CGRectMake(11.5, 0,titlWidth , 40)];
        self.buyerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.buyerButton setImage:[UIImage imageNamed:@"business_refund_chat"] forState:UIControlStateNormal];
        self.buyerButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.buyerButton setTitleColor:[UIColor hexFloatColor:@"444444"] forState:UIControlStateNormal];
        [self.buyerButton setImageEdgeInsets:UIEdgeInsetsMake(0, -4.5, 0, 0)];
        [self.contentView addSubview:self.buyerButton];
        
        self.refundStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.buyerButton.frame)+10, 0, titlWidth, 40)];
        self.refundStatusLabel.textColor = mainColor;
        self.refundStatusLabel.font = [UIFont systemFontOfSize:14];
        self.refundStatusLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.refundStatusLabel];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.buyerButton.frame), mainWidth, 0.5)];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line];
        
        self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.buyerButton.frame)+15, imageSize, imageSize)];
        [self.contentView addSubview:self.logo];
        
        //名称
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.logo.frame)+12, CGRectGetMaxY(self.buyerButton.frame)+17, imageSize, 32)];
        self.titleLabel.numberOfLines = 2;
        [ self.contentView addSubview:self.titleLabel];
        
        //价格
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*2-12, CGRectGetMinY(self.titleLabel.frame), titleHeight*2, 15)];
        self.priceLabel.textAlignment = NSTextAlignmentRight;
        self.priceLabel.font = [UIFont systemFontOfSize:15];
        self.priceLabel.textColor = [UIColor hexFloatColor:@"333333"];
        [ self.contentView addSubview:self.priceLabel];
        //数量
        self.numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*2-12, CGRectGetMaxY(self.priceLabel.frame)+10, titleHeight*2, NORMOL_SPACE*2)];
        self.numberLabel.font = [UIFont systemFontOfSize:13];
        self.numberLabel.textColor = [UIColor hexFloatColor:@"888988"];
        self.numberLabel.textAlignment = NSTextAlignmentRight;
        [ self.contentView addSubview:self.numberLabel];
        
        //剩余天数
        self.lastTimeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.logo.frame)-20, mainWidth-CGRectGetMaxX(self.logo.frame)-12-100-12, 20)];
        [self.lastTimeButton setImage:[UIImage imageNamed:@"business_last_ time"] forState:UIControlStateNormal];
        [self.lastTimeButton setTitleColor:COLOR_666666 forState:UIControlStateNormal];
        self.lastTimeButton.titleLabel.font = [UIFont qsh_systemFontOfSize:13];
        [self.lastTimeButton setImageEdgeInsets:UIEdgeInsetsMake(0, -4.5, 0, 0)];
        [self.contentView addSubview:self.lastTimeButton];
        
        //状态
        self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-100-12, CGRectGetMinY(self.lastTimeButton.frame),100, CGRectGetHeight(self.lastTimeButton.frame))];
        self.statusLabel.font = [UIFont qsh_systemFontOfSize:12];
        self.statusLabel.textColor = [UIColor hexFloatColor:@"FF6600"];
        self.statusLabel.textAlignment = NSTextAlignmentRight;
        
        [ self.contentView addSubview:self.statusLabel];
        
    }
    
    return self;
}

-(void)setRefundDataParser:(BusinessRefundListDataParser *)refundDataParser {
    _refundDataParser = refundDataParser;
    
    [self.buyerButton setTitle:refundDataParser.buyer_name forState:UIControlStateNormal];
    [self.buyerButton addTarget:self action:@selector(toLinkBuyer) forControlEvents:UIControlEventTouchUpInside];
    self.refundStatusLabel.text = refundDataParser.refund_status_cn;
    
    [self.logo getImageWithUrlStr:refundDataParser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    
    UIFont *tFont = [UIFont systemFontOfSize:15];
    self.titleLabel.font = tFont;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.text = refundDataParser.product_name;
    CGSize size = CGSizeMake(300, 200);
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
    CGSize  actualsize =[refundDataParser.product_name boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.logo.frame)+12, CGRectGetMaxY(self.buyerButton.frame)+17,mainWidth-CGRectGetMaxX(self.logo.frame)-12*2-10-CGRectGetWidth(self.priceLabel.frame), actualsize.height>45?45:actualsize.height);
    self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",[refundDataParser.sell_price floatValue]];
    if (!isEmptyString(refundDataParser.format_remain_time)) {
        [self.lastTimeButton setTitle:[NSString stringWithFormat:@"还剩%@",refundDataParser.format_remain_time] forState:UIControlStateNormal];
    } else {
        self.lastTimeButton.hidden = YES;
    }
    
    self.numberLabel.text = [NSString stringWithFormat:@"×%@",refundDataParser.product_num];
    self.statusLabel.text = [NSString stringWithFormat:@"%@  %@",refundDataParser.send_status_cn,refundDataParser.refund_type];
    
    
}

- (void)toLinkBuyer {
    
    if ([self.delegate respondsToSelector:@selector(chatBuyer:)]) {
        [self.delegate chatBuyer:[NSString stringWithFormat:@"%@,%@,%@",self.refundDataParser.buyer_id,self.refundDataParser.buyer_name,self.refundDataParser.image]];
    }
}


@end
