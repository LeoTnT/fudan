//
//  StandardMainTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"
#import "SpecValueView.h"

@protocol StandardMainTableViewCellDelegate <NSObject>

- (void) selectedSpecValue:(BusinessSpecDetailParser *) specParser selected:(BOOL) isSelected;
- (void) editSpecName:(UIButton *) sender;
- (void) deleteSpec:(UIButton *) sender;
- (void) addSpecValue:(UIButton *) sender;
- (void) deleteSpecValue:(UIButton *) sender;
@end

@interface StandardMainTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *editNameButton;
@property (nonatomic, strong) UIButton *deleteNameButton;
@property (nonatomic, strong) UIButton *addSpecValueButton;
@property (nonatomic, strong) UIButton *deleteSpecValueButton;
@property (nonatomic, strong) SpecValueView *specView;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id<StandardMainTableViewCellDelegate> delegate;

@property (nonatomic, strong) NSArray *specValueArray;

+(NSString *)idString;
@end
