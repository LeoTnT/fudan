
//
//  SpecValueView.m
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SpecValueView.h"

@implementation SpecValueView

-(instancetype)initWithFrame:(CGRect)frame {
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)setSpecArray:(NSArray *)specArray {
    _specArray = specArray;
    for (UIView *view in self.subviews) {
            [view removeFromSuperview];
    }
    
    CGFloat height = 30,width = (mainWidth-10*5)/4;
    for (int i=0; i<_specArray.count; i++) {
        int row = i/4;
        int col = i%4;
        BusinessSpecDetailParser *parser = _specArray[i];
        @autoreleasepool {
            UIButton *valueButton = [UIButton new];
            valueButton.tag = i+1000;
            [valueButton setImage:[UIImage imageNamed:@"business_spec"] forState:UIControlStateNormal];
            [valueButton setImage:[UIImage imageNamed:@"business_spec_selected"] forState:UIControlStateSelected];
            valueButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
            valueButton.titleLabel.textAlignment = NSTextAlignmentLeft;
            [valueButton setTitle:parser.name forState:UIControlStateNormal];
            [valueButton setTitle:parser.name forState:UIControlStateSelected];
            [valueButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
            [valueButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self addSubview:valueButton];
            [valueButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo((row+1)*10+row*height);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.left.mas_equalTo(10*(col+1)+col*width);
            }];
            [valueButton addTarget: self action:@selector(valueAction:) forControlEvents:UIControlEventTouchUpInside];
            }
    }
    
    NSUInteger row = _specArray.count/4;
    self.viewHeight = _specArray.count%4==0?(row*height+(10*(row+1))):((row+1)*height+10*(row+2));
}

- (void)valueAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    BusinessSpecDetailParser *parser = self.specArray[sender.tag-1000];
    if ([self.delegate respondsToSelector:@selector(selectedSpecValue:selected:)]) {
        [self.delegate selectedSpecValue:parser selected:sender.selected];
    }
}
@end
