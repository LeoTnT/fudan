//
//  AddStandardTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddStandstardCellDelegate <NSObject>

- (void)deleteCell:(UIButton *) sender;

@end

@interface AddStandardTableViewCell : UITableViewCell

/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;

/**输入框*/
@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UIButton *deleteButton;

@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic, weak) id<AddStandstardCellDelegate> delegate;

+(NSString *)idString;
@end
