//
//  AddStandardTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddStandardTableViewCell.h"

@implementation AddStandardTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        UILabel *tintLabel = [UILabel new];
//        tintLabel.text = @"新增";
        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor grayColor];
        self.titleLabel.font = font;
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.titleLabel];
        self.textField = [UITextField new];
        self.textField.textColor = [UIColor blackColor];
        self.textField.font = font;
        self.textField.returnKeyType = UIReturnKeyDone;
        [self.contentView addSubview:self.textField];
        self.deleteButton = [UIButton new];
        [self.deleteButton setImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
//        [self.deleteButton removeTarget:self action:@selector(deleteCell:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.deleteButton];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView).with.mas_offset(13);
            make.width.mas_equalTo(80);
        }];
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLabel.mas_right).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-68);
            make.top.mas_equalTo(self.contentView).with.mas_offset(10);
            make.bottom.mas_equalTo(self.contentView).with.mas_offset(-10);
        }];
        
        [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.textField.mas_right).with.mas_offset(5);
            make.width.mas_equalTo(50);
            make.top.bottom.mas_equalTo(self.contentView);
        }];
        self.textField.borderStyle = UITextBorderStyleRoundedRect;
        self.cellHeight = 50;
    }
    return self;
}

- (void)deleteAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(deleteCell:)]) {
        [self.delegate deleteCell:sender];
    }
}

+(NSString *)idString {
return @"AddStandardTableViewCell";
}
@end
