//
//  SpecValueView.h
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"
@protocol SpecValueViewDelegate <NSObject>

@optional
- (void) selectedSpecValue:(BusinessSpecDetailParser *) specParser selected:(BOOL) isSelected;

@end

@interface SpecValueView : UIView
@property (nonatomic, strong) NSArray *specArray;
@property (nonatomic, assign) CGFloat viewHeight;

@property (nonatomic, weak) id<SpecValueViewDelegate> delegate;
@end
