//
//  StandardMainTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/9/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "StandardMainTableViewCell.h"

@interface StandardMainTableViewCell()
@property (nonatomic, strong) UILabel *line;
@end

@implementation StandardMainTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.nameLabel = [UILabel new];
        self.nameLabel.font = font;
        self.nameLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.nameLabel];
        self.editNameButton = [UIButton new];
        self.editNameButton.titleLabel.font = font;
        [self.editNameButton setTitle:Localized(@"edit") forState:UIControlStateNormal];
        [self.editNameButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.editNameButton.backgroundColor = mainColor;
        [self.contentView addSubview:self.editNameButton];
        self.deleteNameButton = [UIButton new];
        self.deleteNameButton.titleLabel.font = font;
        [self.deleteNameButton setTitle:@"删除" forState:UIControlStateNormal];
        [self.deleteNameButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.deleteNameButton.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:self.deleteNameButton];
        
        self.addSpecValueButton = [UIButton new];
        self.addSpecValueButton.titleLabel.font = font;
        [self.addSpecValueButton setTitle:@"增加规格值" forState:UIControlStateNormal];
        [self.addSpecValueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.addSpecValueButton.backgroundColor = mainColor;
        [self.contentView addSubview:self.addSpecValueButton];
        self.deleteSpecValueButton = [UIButton new];
        self.deleteSpecValueButton.titleLabel.font = font;
        [self.deleteSpecValueButton setTitle:@"删除" forState:UIControlStateNormal];
        [self.deleteSpecValueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.deleteSpecValueButton.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:self.deleteSpecValueButton];
        self.line = [UILabel new];
        self.line.backgroundColor = LINE_COLOR;
        [self.contentView addSubview:self.line];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(13);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-130);
            make.top.mas_equalTo(self.contentView);
            make.height.mas_equalTo(40);
        }];
        
        [self.deleteNameButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView).with.mas_offset(-13);
            make.width.mas_equalTo(50);
            make.top.mas_equalTo(self.contentView).with.mas_offset(5);
            make.height.mas_equalTo(30);
        }];
        
        [self.editNameButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.deleteNameButton.mas_left).with.mas_offset(-10);
            make.top.mas_equalTo(self.contentView).with.mas_offset(5);
            make.width.height.mas_equalTo(self.deleteNameButton);
        }];
        [self.deleteSpecValueButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView).with.mas_offset(-13);
            make.width.mas_equalTo(50);
            make.top.mas_equalTo(self.deleteNameButton.mas_bottom).with.mas_offset(5);
            make.height.mas_equalTo(30);
        }];
        
        [self.addSpecValueButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.deleteSpecValueButton.mas_left).with.mas_offset(-10);
            make.top.mas_equalTo(self.deleteSpecValueButton);
            make.height.mas_equalTo(self.deleteSpecValueButton);
            make.width.mas_equalTo(100);
        }];
        [self.editNameButton addTarget:self action:@selector(editNameAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteNameButton addTarget:self action:@selector(deleteNameAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.addSpecValueButton addTarget:self action:@selector(addSpecValueAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteSpecValueButton addTarget:self action:@selector(deleteSpecValueAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.addSpecValueButton.mas_bottom).with.mas_offset(9);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        self.cellHeight = 75;
    }
    return self;
}

-(void)setSpecValueArray:(NSArray *)specValueArray {
    _specValueArray = specValueArray;
    for (UIView *view in self.contentView.subviews) {
        if (view.tag>=1000) {
            [view removeFromSuperview];
        }
    }
    self.specView = [SpecValueView new];
    [self.contentView addSubview:self.specView];
    self.specView.specArray = _specValueArray;
    [self.specView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addSpecValueButton.mas_bottom).with.mas_offset(5);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.specView.viewHeight);
    }];
    [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.specView.mas_bottom).with.mas_offset(9);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    self.cellHeight = self.specView.viewHeight+85;
}

- (void)valueAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    BusinessSpecDetailParser *parser = self.specValueArray[sender.tag-1000];
    if ([self.delegate respondsToSelector:@selector(selectedSpecValue:selected:)]) {
        [self.delegate selectedSpecValue:parser selected:sender.selected];
    }
}

- (void)editNameAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(editSpecName:)]) {
        [self.delegate editSpecName:sender];
    }
}

- (void)deleteNameAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(deleteSpec:)]) {
        [self.delegate deleteSpec:sender];
    }
}

- (void)addSpecValueAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(addSpecValue:)]) {
        [self.delegate addSpecValue:sender];
    }
}

- (void)deleteSpecValueAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(deleteSpecValue:)]) {
        [self.delegate deleteSpecValue:sender];
    }
}

+(NSString *)idString {
    return @"StandardMainTableViewCell";
}


@end
