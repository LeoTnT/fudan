//
//  BusinessOrderDetailTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsAddressView.h"

@interface BusinessOrderDetailTopTableViewCell : UITableViewCell

@property (nonatomic, strong) GoodsAddressView *addressView;

@property (nonatomic, strong) NSArray *addressInformationArray;

@property (nonatomic, assign) CGFloat cellHeight;
@end
