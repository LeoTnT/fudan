//
//  BusinessLogisticTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/9/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessFormModel.h"

@interface BusinessLogisticTableViewCell : UITableViewCell
@property(nonatomic,assign) CGFloat cellHeight;

@property(nonatomic,assign) BOOL isNewInfo;

@property(nonatomic,strong) BusinessLogisticsDataParser *dataParser;
@end
