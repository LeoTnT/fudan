//
//  BusinessOrderDetailBottomTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderNumberCountTimeView.h"
#import "BusinessFormModel.h"


@interface BusinessOrderDetailBottomTableViewCell : UITableViewCell

/**订单编号...*/
@property (nonatomic, strong) OrderNumberCountTimeView *orderNumberView;

/**备注*/
@property (nonatomic, strong) UILabel *remarkLabel;

@property (nonatomic, strong) BusinessFormOrderDetailParser *orderParser;

@property (nonatomic, assign) CGFloat cellHeight;

@end
