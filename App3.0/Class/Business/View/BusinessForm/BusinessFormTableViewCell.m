//
//  BusinessFormTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessFormTableViewCell.h"


@interface BusinessFormTableViewCell()
@property (nonatomic, strong) UILabel *shopNameLabel;
@property (nonatomic, strong) UILabel *orderNumberLabel;
@property (nonatomic, strong) UILabel *orderStatusLabel;
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) UIButton *firstButton;
@property (nonatomic, strong) UIButton *secondButton;
@property (nonatomic, strong) UIView *topView;
@end

@implementation BusinessFormTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = BG_COLOR;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        //topView  商家名称，订单号，状态
        self.topView = [UIView new];
        self.topView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.topView];
        
        self.shopNameLabel = [UILabel new];
        self.shopNameLabel.font = font;
        [self.topView addSubview:self.shopNameLabel];
        self.orderNumberLabel = [UILabel new];
         self.orderNumberLabel.font = font;
        [self.topView addSubview:self.orderNumberLabel];
        self.orderStatusLabel = [UILabel new];
         self.orderStatusLabel.font = font;
        self.orderStatusLabel.textAlignment = NSTextAlignmentRight;
        self.orderStatusLabel.textColor = [UIColor redColor];
        [self.topView addSubview:self.orderStatusLabel];
        
        //中间 goodsView
        self.goodsView = [BusinessFormGoodsView new];
        [self.contentView addSubview:self.goodsView];
        
        //bottom1  总计
        self.totalLabel = [UILabel new];
        self.totalLabel.font = font;
        self.totalLabel.backgroundColor = [UIColor whiteColor];
        self.totalLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.totalLabel];
        //bottom2 按钮（根据状态展示）
        self.firstButton = [UIButton new];
        self.firstButton.titleLabel.font = font;
        self.firstButton.backgroundColor = [UIColor whiteColor];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).with.mas_offset(10);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        CGFloat width = (mainWidth-4*10)/4;
        [self.shopNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.topView);
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(width);
        }];
        
        [self.orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.shopNameLabel.mas_right).with.mas_offset(10);
            make.width.mas_equalTo(width*2);
            make.top.bottom.mas_equalTo(self.topView);
        }];
        
        [self.orderStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.orderNumberLabel.mas_right).with.mas_offset(10);
            make.width.mas_equalTo(width);
            make.top.bottom.mas_equalTo(self.topView);
        }];
        self.cellHeight = 50+10;
    }
    return self;
}

-(void)setOrderParser:(BusinessFormOrderParser *)orderParser {
    _orderParser = orderParser;
     self.backgroundColor = [UIColor whiteColor];
//    [self.shopNameLabel removeFromSuperview];
//    [self.orderNumberLabel removeFromSuperview];
//    [self.orderStatusLabel removeFromSuperview];
    [self.topView removeFromSuperview];
    self.goodsView.orderGoodsType = BusinessGoodsTypeForDetail;
    self.goodsView.parserArray = _orderParser.order_list;
    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.goodsView.viewHeight);
    }];
    BusinessFormOrderDetailParser *orderDetailParser = _orderParser.order;
    self.totalLabel.adjustsFontSizeToFitWidth = YES;
    self.totalLabel.text = [NSString stringWithFormat:@"共计%@件商品，实付¥%.2f(运费：¥%.2f)    ",orderDetailParser.number_total, [orderDetailParser.sell_price_total floatValue]+[orderDetailParser.yunfei floatValue],[orderDetailParser.yunfei floatValue]];
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsView.mas_bottom);
        make.left.mas_equalTo(self.contentView).with.mas_offset(10);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.height.mas_equalTo(50);
    }];
    self.cellHeight = self.goodsView.viewHeight+50;
}

-(void)setDataParser:(BusinessFormListDetailParser *)dataParser {
    _dataParser = dataParser;
    self.shopNameLabel.text = _dataParser.store_name;
    self.orderNumberLabel.text = [NSString stringWithFormat:@"单号:%@",_dataParser.order_no];
    self.orderStatusLabel.text = _dataParser.status_str;
    self.goodsView.backgroundColor = BG_COLOR;
    if ([_dataParser.ID integerValue]==614) {
        NSLog(@"%lu===%@",_dataParser.product_list.count,_dataParser.product_list);
    }
    self.goodsView.orderGoodsType = BusinessGoodsTypeForOrder;
    self.goodsView.parserArray = _dataParser.product_list;
    [self.goodsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.goodsView.viewHeight);
    }];
    self.totalLabel.adjustsFontSizeToFitWidth = YES;
    self.totalLabel.text = [NSString stringWithFormat:@"共计%@件商品，实付¥%.2f(运费：¥%.2f)    ",_dataParser.number_total, [_dataParser.sell_price_total floatValue]+[_dataParser.yunfei floatValue],[_dataParser.yunfei floatValue]];
    [self.totalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsView.mas_bottom);
        make.left.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(49);
    }];
    
    UILabel *line = [UILabel new];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self.contentView addSubview:line];
    [line mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.totalLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.right.mas_equalTo(self.contentView);
    }];

    //动态添加按钮
    if ([_dataParser.status integerValue]==0) {
        
        //待支付
//        [self.firstButton setTitle:@"修改价格" forState:UIControlStateNormal];
//        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.firstButton addTarget:self action:@selector(modifyPriceAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.firstButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.contentView);
//            make.width.mas_equalTo(mainWidth/2-0.5);
//            make.top.mas_equalTo(line.mas_bottom);
//            make.height.mas_equalTo(50);
//        }];
//        UILabel *line2 = [UILabel new];
//        line2.backgroundColor = LINE_COLOR_NORMAL;
//        [self.contentView addSubview:line2];
//        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(line.mas_bottom);
//            make.left.mas_equalTo(self.firstButton.mas_right);
//            make.width.mas_equalTo(1);
//            make.height.mas_equalTo(50);
//        }];
        self.secondButton = [UIButton new];
        self.secondButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        self.secondButton.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.secondButton];
        [self.secondButton setTitle:@"关闭订单" forState:UIControlStateNormal];
        [self.secondButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.secondButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.secondButton addTarget:self action:@selector(closeOrderAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.secondButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(line2.mas_right);
//            make.width.mas_equalTo(mainWidth/2-0.5);
//            make.top.mas_equalTo(line.mas_bottom);
//            make.height.mas_equalTo(50);
//        }];
                [self.secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.contentView);
                    make.width.mas_equalTo(mainWidth);
                    make.top.mas_equalTo(line.mas_bottom);
                    make.height.mas_equalTo(50);
                }];

        
    } else if ([_dataParser.status integerValue]==1) {

        [self.contentView addSubview:self.firstButton];
        //待发货
        [self.firstButton setTitle:@"立即发货" forState:UIControlStateNormal];
        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.firstButton addTarget:self action:@selector(deliverGoodsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView);
            make.width.mas_equalTo(mainWidth);
            make.top.mas_equalTo(line.mas_bottom);
            make.height.mas_equalTo(50);
        }];

    
    } else if ([_dataParser.status integerValue]==2) {
        [self.contentView addSubview:self.firstButton];
        //已发货
        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView);
            make.width.mas_equalTo(mainWidth);
            make.top.mas_equalTo(line.mas_bottom);
            make.height.mas_equalTo(50);
        }];
        
        
    } else if ([_dataParser.status integerValue]==3) {
        [self.contentView addSubview:self.firstButton];
        //待评价
        [self.firstButton setTitle:@"查看物流" forState:UIControlStateNormal];
        [self.firstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.firstButton addTarget:self action:@selector(lookLogisticAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView);
            make.width.mas_equalTo(mainWidth);
            make.top.mas_equalTo(line.mas_bottom);
            make.height.mas_equalTo(50);
        }];
        
        
    }
   
    if ([_dataParser.status integerValue]>3) {
       self.cellHeight = 60+self.goodsView.viewHeight+50;
    } else {
      self.cellHeight = 60+self.goodsView.viewHeight+50+50;
    }
    
}

- (void)modifyPriceAction:(UIButton *) sender {

}

- (void)closeOrderAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(closeOrderWithOrderId:)]) {
        [self.delegate closeOrderWithOrderId:self.dataParser.ID];
    }
}

- (void)deliverGoodsAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(deliverGoodsWithOrderId:)]) {
        [self.delegate deliverGoodsWithOrderId:self.dataParser.ID];
    }
}

- (void)lookLogisticAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(lookLogisticWithOrder:)]) {
        [self.delegate lookLogisticWithOrder:self.dataParser];
    }
}

- (void)deleteOrderAction:(UIButton *) sender {

}


@end
