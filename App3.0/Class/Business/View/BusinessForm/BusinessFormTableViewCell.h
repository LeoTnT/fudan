//
//  BusinessFormTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessFormModel.h"
#import "BusinessFormGoodsView.h"

@protocol BusinessFormTableViewCellDelegate <NSObject>

@optional
- (void) deliverGoodsWithOrderId:(NSString *) orderId;
@optional
- (void) lookLogisticWithOrder:(BusinessFormListDetailParser *) orderDetail;

@optional
- (void) closeOrderWithOrderId:(NSString *) orderId;

//@optional
//- (void) deliverGoodsWithOrderId:(NSString *) orderId;

@end

@interface BusinessFormTableViewCell : UITableViewCell
@property(nonatomic,copy) NSString * is_refund;
@property(nonatomic,copy) NSString * is_reject;
@property(nonatomic,strong) BusinessFormListDetailParser *dataParser;//=》订单界面
@property(nonatomic,assign) CGFloat cellHeight;
@property(nonatomic,weak) id<BusinessFormGoodsViewDelegate> orderDelegate;
@property(nonatomic,weak) id<BusinessFormTableViewCellDelegate> delegate;
@property(nonatomic,strong) BusinessFormOrderParser *orderParser;//=》订单详情界面
@property(nonatomic,strong) BusinessFormGoodsView *goodsView;//商品详情view
@end
