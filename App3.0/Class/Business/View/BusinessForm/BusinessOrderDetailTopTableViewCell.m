//
//  BusinessOrderDetailTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessOrderDetailTopTableViewCell.h"

@implementation BusinessOrderDetailTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
//        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        
        
    }
    return self;
}

-(void)setAddressInformationArray:(NSArray *)addressInformationArray {
    _addressInformationArray = addressInformationArray;
    [self.addressView removeFromSuperview];
    self.addressView = [[GoodsAddressView alloc]init];
    self.addressView.addressType = GoodsAddressViewTypeWithButton;
    [self.contentView addSubview:self.addressView];
    self.cellHeight = self.addressView.viewHeight;
    self.addressView.addressArray = _addressInformationArray;
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.addressView.viewHeight);
    }];
    self.cellHeight = self.addressView.viewHeight;
}
@end
