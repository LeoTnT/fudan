//
//  OrderNumberCountTimeView.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessFormModel.h"


@interface OrderNumberCountTimeView : UIView

@property (nonatomic, strong) BusinessFormOrderDetailParser *detailParser;
/**订单编号*/
@property (nonatomic, strong) UILabel *orderNumberLabel;
/**下单账户*/
@property (nonatomic, strong) UILabel *orderAccountLabel;
/**下单时间*/
@property (nonatomic, strong) UILabel *orderTimeLabel;
@property (nonatomic, assign) CGFloat viewHeight;
@end
