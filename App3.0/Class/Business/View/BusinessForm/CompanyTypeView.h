//
//  CompanyTypeView.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessFormModel.h"

@protocol CompanyTypeViewDelegate<NSObject>

@optional
-(void) hiddenCardView:(BusinessFormOrderLogisticDetailParser *) dataParser;


@end

//typedef NS_ENUM(NSInteger, CompanyTypeViewType) {
//    CompanyTypeView,
//};
@interface CompanyTypeView : UIView
@property (nonatomic, strong) NSArray *typeArray;
@property (nonatomic, copy) NSString *selectedId;
@property (nonatomic, weak) id<CompanyTypeViewDelegate> typeDelegate;
//@property (nonatomic, assign) AlertViewType viewType;//弹出视图类型
@property (nonatomic, strong) UIScrollView *scrollView;
@end
