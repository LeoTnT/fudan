//
//  BusinessVerifyOrderTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessVerifyOrderTableViewCell.h"
#import "XSFormatterDate.h"

@interface BusinessVerifyOrderTableViewCell()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *orderNumberLabel;
@property (nonatomic, strong) UILabel *orderAccountLabel;//下单账户
@property (nonatomic, strong) UILabel *orderTimeLabel;//下单时间
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) GoodsAddressView *addressView;
@property (nonatomic, strong) BusinessFormGoodsView *goodsView;
@end

@implementation BusinessVerifyOrderTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.titleLabel = [UILabel new];
        self.titleLabel.backgroundColor = BG_COLOR;
        self.titleLabel.text = @"    订单详情";
        [self.contentView addSubview:self.titleLabel];
        self.orderNumberLabel = [UILabel new];
        self.orderNumberLabel.backgroundColor = [UIColor whiteColor];
        self.orderNumberLabel.font = font;
        [self.contentView addSubview:self.orderNumberLabel];
        self.orderAccountLabel = [UILabel new];
        self.orderAccountLabel.backgroundColor = [UIColor whiteColor];
        self.orderAccountLabel.font = font;
        [self.contentView addSubview:self.orderAccountLabel];
        self.orderTimeLabel = [UILabel new];
        self.orderTimeLabel.backgroundColor = [UIColor whiteColor];
        self.orderTimeLabel.font = font;
        [self.contentView addSubview:self.orderTimeLabel];
        self.totalLabel = [UILabel new];
        self.totalLabel.backgroundColor = [UIColor whiteColor];
        self.totalLabel.textAlignment = NSTextAlignmentRight;
        self.totalLabel.font = [UIFont qsh_systemFontOfSize:16];
        [self.contentView addSubview:self.totalLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        
        [self.orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(40);
        }];
        [self.orderAccountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.orderNumberLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(40);
        }];
        [self.orderTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.orderAccountLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(40);
        }];
        self.cellHeight = 50;
    }
    return self;
}

-(void)setOrderParser:(BusinessFormOrderParser *)orderParser {
    _orderParser = orderParser;
    BusinessFormOrderDetailParser *detailParser = _orderParser.order;
    
    self.orderNumberLabel.text = [NSString stringWithFormat:@"    订单编号：%@",detailParser.order_no];
    self.orderAccountLabel.text = [NSString stringWithFormat:@"    下单账户：%@",detailParser.username];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"    下单时间：%@",[XSFormatterDate dateWithTimeIntervalString:detailParser.w_time]];

    self.addressView = [[GoodsAddressView alloc] init];
    [self.contentView addSubview:self.addressView];
    self.goodsView = [BusinessFormGoodsView new];
    [self.contentView addSubview:self.goodsView];
    BusinessFormOrderLogisticParser *logisticParser = _orderParser.logistics;
    self.addressView.addressType = GoodsAddressViewTypeNormal;
    self.addressView.addressArray = @[logisticParser.rec_name,logisticParser.rec_tel,logisticParser.rec_addr];
    
    self.goodsView.orderGoodsType = BusinessGoodsTypeForDetail;
    self.goodsView.parserArray = _orderParser.order_list;
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orderTimeLabel.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.addressView.viewHeight);
    }];
    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressView.mas_bottom);
        make.left.right.mas_equalTo(self.addressView);
        make.height.mas_equalTo(self.goodsView.viewHeight);
    }];
    BusinessFormOrderDetailParser *orderDetailParser = _orderParser.order;
    self.totalLabel.text = [NSString stringWithFormat:@"共计%@件商品，实付¥%.2f(运费：¥%.2f)",orderDetailParser.number_total, [orderDetailParser.sell_price_total floatValue]+[orderDetailParser.yunfei floatValue],[orderDetailParser.yunfei floatValue]];
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsView.mas_bottom);
        make.left.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.height.mas_equalTo(50);
    }];
    

    self.cellHeight = 50+40*3+self.addressView.viewHeight+self.goodsView.viewHeight+50;
    
}

@end
