//
//  BusinessFormGoodsView.m
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessFormGoodsView.h"
#import "BusinessFormModel.h"
#import "UIImage+XSWebImage.h"
#import "BusinessRefundModel.h"


@implementation BusinessFormGoodsView

-(void)setParserArray:(NSArray *)parserArray{
    _parserArray = parserArray;
    CGFloat cellSmallHeight=90,imageSize=70,titleHeight=40;
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    for (int i=0; i<_parserArray.count; i++) {
        @autoreleasepool {
            if (self.orderGoodsType==BusinessGoodsTypeForDetail){
                self.backgroundColor = [UIColor whiteColor];
                BusinessFormOrderListDatailParser *dataParser = _parserArray[i];
                self.centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*cellSmallHeight, mainWidth, cellSmallHeight)];
                self.centerView.backgroundColor = [UIColor whiteColor];
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
                //                img.contentMode = UIViewContentModeScaleAspectFit;
                [img getImageWithUrlStr:dataParser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                [ self.centerView addSubview:img];
                //名称
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE*2, CGRectGetMinY(img.frame), imageSize, imageSize)];
                title.numberOfLines = 2;
                //自适应高度和宽度
                NSString *str = dataParser.product_name;
                UIFont *tFont = [UIFont systemFontOfSize:15];
                title.font = tFont;
                title.lineBreakMode = NSLineBreakByTruncatingTail;
                title.text = str;
                CGSize size = CGSizeMake(300, 200);
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
                CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-2*NORMOL_SPACE-titleHeight, actualsize.height);
                [ self.centerView addSubview:title];
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE, imageSize, imageSize)];
                desc.numberOfLines = 2;
                //自适应高度和宽度
                NSString *descStr = dataParser.spec_name;
                UIFont *dFont = [UIFont systemFontOfSize:14];
                desc.font = dFont;
                desc.lineBreakMode = NSLineBreakByTruncatingTail;
                desc.text = descStr;
                CGSize sizet = CGSizeMake(300, 200);
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:dFont,NSFontAttributeName,nil];
                CGSize  actualsizet =[descStr boundingRectWithSize:sizet options:NSStringDrawingUsesLineFragmentOrigin  attributes:dict context:nil].size;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,actualsizet.width+NORMOL_SPACE, actualsizet.height);
                [ self.centerView addSubview:desc];
                
                desc.adjustsFontSizeToFitWidth = YES;
                //价格
                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*3, CGRectGetMinY(title.frame), titleHeight*3-NORMOL_SPACE, NORMOL_SPACE*2)];
                
                NSString *str3 = [NSString stringWithFormat:@"¥%.2f",[dataParser.sell_price floatValue]];
                price.text = str3;
                UIFont *tFont3 = [UIFont systemFontOfSize:15];
                price.font = tFont3;
                price.lineBreakMode = NSLineBreakByTruncatingTail;
                price.text = str3;
                CGSize size3 = CGSizeMake(300, 200);
                NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:tFont3,NSFontAttributeName,nil];
                CGSize  actualsize3 =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic3 context:nil].size;
                price.textColor = [UIColor redColor];
                price.textAlignment = NSTextAlignmentRight;
                //                price.backgroundColor = BG_COLOR;
                price.frame =CGRectMake(mainWidth-actualsize3.width-NORMOL_SPACE, CGRectGetMinY(title.frame), actualsize3.width, CGRectGetHeight(title.frame));
                [ self.centerView addSubview:price];
                price.adjustsFontSizeToFitWidth = YES;
                //数量
                UILabel *number = [[UILabel alloc] init];
                NSString *str4 = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                number.text = str4;
                UIFont *tFont4 = [UIFont systemFontOfSize:15];
                number.font = tFont4;
                number.lineBreakMode = NSLineBreakByTruncatingTail;
                number.text = str4;
                CGSize size4 = CGSizeMake(300, 200);
                NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:tFont4,NSFontAttributeName,nil];
                CGSize  actualsize4 =[str4 boundingRectWithSize:size4 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic4 context:nil].size;
                number.textColor = [UIColor blackColor];
                number.textAlignment = NSTextAlignmentRight;
                
                //            number.text = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                //            number.textAlignment = NSTextAlignmentRight;
                //                number.backgroundColor = BG_COLOR;
                [ self.centerView addSubview:number];
                number.frame = CGRectMake(mainWidth-actualsize4.width-NORMOL_SPACE, CGRectGetMaxY(price.frame)+NORMOL_SPACE, actualsize4.width, actualsize4.height);
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), actualsize.height);
                number.adjustsFontSizeToFitWidth = YES;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(number.frame), actualsizet.height);
                //
                //                self.centerView.tag = i+2000;
                //                //添加手势
                //                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToLookGoodsDetailInfo:)];
                //                self.centerView.userInteractionEnabled = YES;
                //                [ self.centerView addGestureRecognizer:tap];
                
            }else if(self.orderGoodsType==BusinessGoodsTypeForOrder){
                BusinessFormProductListDetailParser *dataParser = _parserArray[i];
                self.centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*cellSmallHeight, mainWidth, cellSmallHeight)];
                self.centerView.backgroundColor = BG_COLOR;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
                //                img.contentMode = UIViewContentModeScaleAspectFit;
                [img getImageWithUrlStr:[dataParser image] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                [ self.centerView addSubview:img];
                //名称
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE*2, CGRectGetMinY(img.frame), imageSize, imageSize)];
                title.numberOfLines = 2;
                //自适应高度和宽度
                NSString *str = dataParser.product_name;
                UIFont *tFont = [UIFont systemFontOfSize:15];
                title.font = tFont;
                title.lineBreakMode = NSLineBreakByTruncatingTail;
                title.text = str;
                CGSize size = CGSizeMake(300, 200);
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
                CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-2*NORMOL_SPACE-titleHeight, actualsize.height);
                [ self.centerView addSubview:title];
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE, imageSize, imageSize)];
                desc.numberOfLines = 2;
                //自适应高度和宽度
                NSString *descStr = dataParser.spec_name;
                UIFont *dFont = [UIFont systemFontOfSize:14];
                desc.font = dFont;
                desc.lineBreakMode = NSLineBreakByTruncatingTail;
                desc.text = descStr;
                CGSize sizet = CGSizeMake(300, 200);
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:dFont,NSFontAttributeName,nil];
                CGSize  actualsizet =[descStr boundingRectWithSize:sizet options:NSStringDrawingUsesLineFragmentOrigin  attributes:dict context:nil].size;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,actualsizet.width+NORMOL_SPACE, actualsizet.height);
                [ self.centerView addSubview:desc];
                
                desc.adjustsFontSizeToFitWidth = YES;
                //价格
                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*3, CGRectGetMinY(title.frame), titleHeight*3-NORMOL_SPACE, NORMOL_SPACE*2)];
                
                NSString *str3 = [NSString stringWithFormat:@"¥%.2f",[dataParser.sell_price floatValue]];
                price.text = str3;
                UIFont *tFont3 = [UIFont systemFontOfSize:15];
                price.font = tFont3;
                price.lineBreakMode = NSLineBreakByTruncatingTail;
                price.text = str3;
                CGSize size3 = CGSizeMake(300, 200);
                NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:tFont3,NSFontAttributeName,nil];
                CGSize  actualsize3 =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic3 context:nil].size;
                price.textColor = [UIColor redColor];
                price.textAlignment = NSTextAlignmentRight;
                //                price.backgroundColor = BG_COLOR;
                price.frame =CGRectMake(mainWidth-actualsize3.width-NORMOL_SPACE, CGRectGetMinY(title.frame), actualsize3.width, CGRectGetHeight(title.frame));
                [ self.centerView addSubview:price];
                price.adjustsFontSizeToFitWidth = YES;
                //数量
                UILabel *number = [[UILabel alloc] init];
                NSString *str4 = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                number.text = str4;
                UIFont *tFont4 = [UIFont systemFontOfSize:15];
                number.font = tFont4;
                number.lineBreakMode = NSLineBreakByTruncatingTail;
                number.text = str4;
                CGSize size4 = CGSizeMake(300, 200);
                NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:tFont4,NSFontAttributeName,nil];
                CGSize  actualsize4 =[str4 boundingRectWithSize:size4 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic4 context:nil].size;
                number.textColor = [UIColor blackColor];
                number.textAlignment = NSTextAlignmentRight;
                
                //            number.text = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                //            number.textAlignment = NSTextAlignmentRight;
                //                number.backgroundColor = BG_COLOR;
                [ self.centerView addSubview:number];
                number.frame = CGRectMake(mainWidth-actualsize4.width-NORMOL_SPACE, CGRectGetMaxY(price.frame)+NORMOL_SPACE, actualsize4.width, actualsize4.height);
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), actualsize.height);
                number.adjustsFontSizeToFitWidth = YES;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(number.frame), actualsizet.height);
            } else if (self.orderGoodsType==BusinessGoodsTypeForRefundList) {
                //                BusinessRefundListDataParser
                
            }
            
            [self addSubview: self.centerView];
            
        }
        self.viewHeight = cellSmallHeight*_parserArray.count;
    }
    
}
//-(void)tapToLookGoodsDetailInfo:(UITapGestureRecognizer *) tap{
//    UIView *view = tap.view;
//    BuyerOrderListDetailParser *dataParser = self.parserArray[view.tag-2000];
//    if ([self.orderDelegate respondsToSelector:@selector(toLookGoodsDetailInfoWithGoodsId:)]) {
//        [self.orderDelegate toLookGoodsDetailInfoWithGoodsId:dataParser.product_id];
//    }
//}

@end

