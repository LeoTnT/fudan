//
//  CompanyTypeView.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CompanyTypeView.h"


@interface CompanyTypeView ()<UIScrollViewDelegate>
{
    BusinessFormOrderLogisticDetailParser *_dataParser;//选中的类型
    NSDictionary *_selectedType;
}
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat celHeight;
@property (nonatomic, assign) CGFloat topSpace;
@end
@implementation CompanyTypeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.celHeight = 50;
        self.topSpace = 150;
        self.height = 39;
    }
    return self;
}
- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.backgroundColor  = [UIColor whiteColor];
        //        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = YES;
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}
-(void)setTypeArray:(NSArray *)typeArray{
    _typeArray = typeArray;
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    UIColor *bColor = [UIColor blackColor];
    self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
    UILabel *please = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2,self.topSpace-self.celHeight , mainWidth-self.celHeight, self.celHeight)];
    please.backgroundColor = mainColor;
    please.text = Localized(@"choose");
    please.textAlignment = NSTextAlignmentCenter;
    please.font = [UIFont boldSystemFontOfSize:20];
    please.textColor = [UIColor whiteColor];
    UITapGestureRecognizer *tempTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(temp:)] ;
    please.userInteractionEnabled = YES;
    [please addGestureRecognizer:tempTap];
    [self addSubview:please];
    self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, CGRectGetHeight(self.frame)-self.topSpace-2*NORMOL_SPACE);
    
    [self addSubview:self.scrollView];
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    //类型view
    for (int i=0;i<_typeArray.count; i++) {
        
        UIView *cView = [[UIView alloc] initWithFrame:CGRectMake(0, i*(self.celHeight+1), CGRectGetWidth(self.scrollView.frame), self.celHeight+1)];
        cView.backgroundColor = [UIColor whiteColor];
        //标题，按钮
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2, 0, CGRectGetWidth(cView.frame)/3*2, self.celHeight)];
        BusinessFormOrderLogisticDetailParser *parser = [_typeArray objectAtIndex:i];
        title.text = parser.com_name;
        [cView addSubview:title];
        UIButton *selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(cView.frame)-self.celHeight,NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
        [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_no"] forState:UIControlStateNormal];
        [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_yes"] forState:UIControlStateSelected];
        selectBtn.tag = 999+i;
        [selectBtn addTarget:self action:@selector(selectedAction:) forControlEvents:UIControlEventTouchUpInside];
        [cView addSubview:selectBtn];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedView:)] ;
        cView.userInteractionEnabled = YES;
        [cView addGestureRecognizer:tap];
        if ([self.selectedId isEqual:parser.ID]) {
            selectBtn.selected = YES;
        }
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.celHeight, CGRectGetWidth(cView.frame), 1)];
        line.backgroundColor = LINE_COLOR;
        [cView addSubview:line];
        [self.scrollView addSubview:cView];
        //        [self addSubview:line];
    }
    if (_typeArray.count*(self.celHeight+1)<=CGRectGetHeight(self.scrollView.frame)) {
        self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, _typeArray.count*(self.celHeight+1));
    }
    self.scrollView.contentSize = CGSizeMake(0, _typeArray.count*(self.celHeight+1));
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenView:)] ;
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tap];
    
}

-(void)temp:(UITapGestureRecognizer *) tap {
    NSLog(@"");
}
-(void)hiddenView:(UITapGestureRecognizer *) tap{
    if (_dataParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:_dataParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:nil];
        }
    }
    
    NSLog(@"消失");
}

- (void)selectedView:(UITapGestureRecognizer *) tap {
    for (UIView *view in tap.view.subviews) {
        if (view.tag>=999) {
            [self selectedAction:(UIButton *) view];
            break;
        }
    }
    
}
-(void)selectedAction:(UIButton *) sender{
    //    sender.selected = !sender.selected;
    for (int i=999; i<999+_typeArray.count; i++) {
        
        UIButton *btn = [self viewWithTag:i];
        if (btn.tag==sender.tag) {
            btn.selected = !btn.selected;
        } else {
            if (btn.selected) {
                btn.selected= !btn.selected;
            }
        }
    }
        _dataParser = _typeArray[sender.tag-999];
    if (_dataParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:_dataParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:nil];
        }
    }
    
    
    
}


@end
