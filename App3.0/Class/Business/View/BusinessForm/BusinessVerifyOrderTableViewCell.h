//
//  BusinessVerifyOrderTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessFormModel.h"
#import "GoodsAddressView.h"
#import "BusinessFormGoodsView.h"

@interface BusinessVerifyOrderTableViewCell : UITableViewCell

@property (nonatomic, strong) BusinessFormOrderParser *orderParser;
@property (nonatomic, assign) CGFloat cellHeight;
@end
