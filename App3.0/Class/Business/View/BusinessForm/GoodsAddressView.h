//
//  GoodsAddressView.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol GoodsAddressViewDelegate <NSObject>
//
//@optional
//- (void) linkBuyer;
//
//@end

typedef NS_ENUM(NSInteger,GoodsAddressViewType) {
    GoodsAddressViewTypeNormal,
    GoodsAddressViewTypeWithButton
};

@interface GoodsAddressView : UIView

/**图片*/
@property (nonatomic, strong) UIImageView *imageView;
/**收货人*/
@property (nonatomic, strong) UILabel *reciverLabel;

/**手机号*/
@property (nonatomic, strong) UILabel *phoneLabel;

/**地址*/
@property (nonatomic, strong) UILabel *addressLabel;

/**联系按钮*/
@property (nonatomic, strong) UIButton *linkButton;

@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, strong) NSArray *addressArray;

@property (nonatomic, assign) GoodsAddressViewType addressType;
@end
