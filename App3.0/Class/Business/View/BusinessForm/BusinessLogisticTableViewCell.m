//
//  BusinessLogisticTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/9/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessLogisticTableViewCell.h"

@interface BusinessLogisticTableViewCell ()
{
    CGFloat _infoWidth;
}
@property(nonatomic,weak) UILabel *infoLbel;
@property(nonatomic,weak) UILabel *verLabel;
@property(nonatomic,weak) UILabel *timeLbel;//18:24
@property(nonatomic,weak) UIImageView *triangle;
@property(nonatomic,weak)  UIView *bg_view;
@property(nonatomic,weak) UIImageView *bot;
@property (nonatomic, assign) CGFloat infoHeight;
@property (nonatomic, strong) UIColor *mainBgColor;
@end
@implementation BusinessLogisticTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.infoHeight = 80;
        self.mainBgColor =  [UIColor hexFloatColor:@"5fc4ff"];
        [self addSubViews];
    }
    return  self;
}

-(void)addSubViews{
    self.backgroundColor = BG_COLOR;
    //内容
    UIImageView *triangle = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE*3+2, NORMOL_SPACE*3, NORMOL_SPACE, NORMOL_SPACE)];
    self.triangle = triangle;
    [self.contentView addSubview:self.triangle];
    
    _infoWidth = mainWidth-NORMOL_SPACE*2-2-NORMOL_SPACE*3;
    UIView *bg_view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(triangle.frame), NORMOL_SPACE, _infoWidth, self.infoHeight)];
    bg_view.backgroundColor = [UIColor grayColor];
    bg_view.layer.cornerRadius = 5;
    bg_view.layer.masksToBounds = YES;
    self.bg_view = bg_view;
    [self.contentView addSubview:self.bg_view];
    
    UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _infoWidth-NORMOL_SPACE*2,self.infoHeight-NORMOL_SPACE*4)];
    self.infoLbel = info;
    
    [bg_view addSubview:self.infoLbel];
    info.textColor = [UIColor whiteColor];
    info.numberOfLines = 0;
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(info.frame), CGRectGetWidth(info.frame),NORMOL_SPACE*2)];
    
    self.timeLbel = time;
    time.font = [UIFont systemFontOfSize:18];
    //    time.backgroundColor = mainColor;
    
    time.textColor = [UIColor whiteColor];
    [bg_view addSubview:self.timeLbel];
    
    UILabel *ver = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 2, self.cellHeight)];
    
    self.verLabel = ver;
    [self.contentView addSubview:self.verLabel];
    
    UIImageView *bot = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(ver.frame)-NORMOL_SPACE/2, NORMOL_SPACE*3, NORMOL_SPACE, NORMOL_SPACE)];
    self.bot = bot;
    [self.contentView addSubview:bot];
    
    
}

-(void)setDataParser:(BusinessLogisticsDataParser *)dataParser{
    _dataParser = dataParser;
    //自适应高度
    UIFont * tfont = [UIFont systemFontOfSize:18];
    self.infoLbel.font = tfont;
    self.infoLbel.lineBreakMode =NSLineBreakByTruncatingTail ;
    NSString *str = _dataParser.context;
    self.infoLbel.text = str;
    // label可设置的最大高度和宽度
    CGSize size =CGSizeMake(200,300);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:tfont,NSFontAttributeName,nil];
    //ios7方法，获取文本需要的size，限制宽度
    CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.infoLbel.frame =CGRectMake(NORMOL_SPACE, 0, _infoWidth-NORMOL_SPACE*2,actualsize.height+NORMOL_SPACE) ;
    
    //自适应高度
    UIFont * tfont2 = [UIFont systemFontOfSize:15];
    self.timeLbel.font = tfont2;
    self.timeLbel.lineBreakMode =NSLineBreakByTruncatingTail ;
    NSString *str2 = _dataParser.js_time;
    self.timeLbel.text = str2;
    // label可设置的最大高度和宽度
    CGSize size2 =CGSizeMake(200,300);
    NSDictionary * tdic2 = [NSDictionary dictionaryWithObjectsAndKeys:tfont2,NSFontAttributeName,nil];
    //ios7方法，获取文本需要的size，限制宽度
    CGSize  actualsize2 =[str2 boundingRectWithSize:size2 options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic2 context:nil].size;
    self.timeLbel.frame = CGRectMake(NORMOL_SPACE, CGRectGetMaxY(self.infoLbel.frame), actualsize2.width+NORMOL_SPACE,NORMOL_SPACE*2);
    
    self.bg_view.frame = CGRectMake(CGRectGetMaxX(self.triangle.frame), NORMOL_SPACE, _infoWidth, CGRectGetMaxY(self.timeLbel.frame));
    self.verLabel.frame = CGRectMake(NORMOL_SPACE*2, 0, 2, self.cellHeight);
    
    if (self.isNewInfo) {
        self.triangle.image = [UIImage imageNamed:@"user_order_triangle"];
        self.bg_view.backgroundColor = self.mainBgColor;
        self.bot.image = [UIImage imageNamed:@"user_order_dot"];
        self.verLabel.backgroundColor = self.mainBgColor;
        
    } else {
        self.triangle.image = [UIImage imageNamed:@"user_order_untriangle"];
        self.bg_view.backgroundColor = [UIColor grayColor];
        self.bot.image = [UIImage imageNamed:@"user_order_undot"];
        self.verLabel.backgroundColor = LINE_COLOR;
    }
    self.cellHeight = CGRectGetMaxY(self.bg_view.frame)+NORMOL_SPACE;
    self.verLabel.frame = CGRectMake(NORMOL_SPACE*2, 0, 2, self.cellHeight);
    self.bot.frame = CGRectMake(CGRectGetMidX(self.verLabel.frame)-NORMOL_SPACE/2, NORMOL_SPACE*3, NORMOL_SPACE, NORMOL_SPACE);
    NSLog(@"%-----------.2f",self.cellHeight);
    
}


@end
