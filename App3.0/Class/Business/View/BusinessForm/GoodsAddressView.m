//
//  GoodsAddressView.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsAddressView.h"

@implementation GoodsAddressView

-(instancetype)init {
   self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.imageView = [UIImageView new];
        self.imageView.image = [UIImage imageNamed:@"user_site_location"];
        [self addSubview:self.imageView];
        self.reciverLabel = [UILabel new];
        self.reciverLabel.font = font;
        [self addSubview:self.reciverLabel];
        self.phoneLabel = [UILabel new];
        self.phoneLabel.font = font;
        self.phoneLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.phoneLabel];
        self.addressLabel = [UILabel new];
        self.addressLabel.font = font;
        self.addressLabel.numberOfLines = 2;
         self.addressLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:self.addressLabel];
        self.linkButton = [UIButton new];
        self.linkButton.titleLabel.font = font;
        [self.linkButton setTitle:@"联系买家" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.linkButton setImage:[UIImage imageNamed:@"user_order_chat"] forState:UIControlStateNormal];
        self.linkButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        [self addSubview:self.linkButton];
        self.viewHeight = 20+10+40+40;
//        self.reciverLabel.backgroundColor = [UIColor yellowColor];
//        self.phoneLabel.backgroundColor = [UIColor purpleColor];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).with.mas_offset(10);
            make.left.mas_equalTo(self).with.mas_offset(10);
            make.height.width.mas_equalTo(20);
        }];
        CGFloat width = (mainWidth-20-33)/3;
        [self.reciverLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imageView.mas_right).with.mas_offset(8);
            make.top.mas_equalTo(self.imageView);
            make.height.mas_equalTo(18);
            make.width.mas_equalTo(width*1.8);
        }];
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.mas_equalTo(self.reciverLabel);
            make.left.mas_equalTo(self.reciverLabel.mas_right).with.mas_offset(5);
            make.width.mas_equalTo(width*1.2);
        }];
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
             make.top.mas_equalTo(self.imageView.mas_bottom);
            make.left.mas_equalTo(self.reciverLabel);
            make.right.mas_equalTo(self.phoneLabel.mas_right);
            make.height.mas_equalTo(40);
           
        }];
        [self.linkButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.addressLabel.mas_bottom);
            make.left.right.mas_equalTo(self);
            make.height.mas_equalTo(40);
        }];
        
        self.viewHeight = 20+10+40+40;
        
    }
    return self;
}

- (void)setAddressArray:(NSArray *)addressArray {
    _addressArray = addressArray;
//    for (UIView *view in self.subviews) {
//        [view removeFromSuperview];
//    }
    if (self.addressType==GoodsAddressViewTypeNormal) {
        [self.linkButton removeFromSuperview];
       self.viewHeight-=40;
    } 
    
    self.reciverLabel.text = [NSString stringWithFormat:@"收货人：%@", [_addressArray firstObject]];
    self.phoneLabel.text = _addressArray[1];
    self.addressLabel.text = [NSString stringWithFormat:@"收货人地址：%@",_addressArray[2]];
}

@end
