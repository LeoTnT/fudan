//
//  OrderNumberCountTimeView.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderNumberCountTimeView.h"
#import "XSFormatterDate.h"

@implementation OrderNumberCountTimeView

-(instancetype)init {
    if (self=[super init]) {
        self.backgroundColor = [UIColor whiteColor];
          UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.orderNumberLabel = [UILabel new];
        self.orderNumberLabel.backgroundColor = [UIColor whiteColor];
        self.orderNumberLabel.font = font;
        [self addSubview:self.orderNumberLabel];
        self.orderAccountLabel = [UILabel new];
        self.orderAccountLabel.backgroundColor = [UIColor whiteColor];
        self.orderAccountLabel.textColor = [UIColor redColor];
        self.orderAccountLabel.font = font;
        [self addSubview:self.orderAccountLabel];
        self.orderTimeLabel = [UILabel new];
        self.orderTimeLabel.backgroundColor = [UIColor whiteColor];
        self.orderTimeLabel.font = font;
        [self addSubview:self.orderTimeLabel];
        [self.orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self);
            make.height.mas_equalTo(40);
        }];
        [self.orderAccountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.orderNumberLabel.mas_bottom);
            make.left.right.mas_equalTo(self);
            make.height.mas_equalTo(40);
        }];
        [self.orderTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.orderAccountLabel.mas_bottom);
            make.left.right.mas_equalTo(self);
            make.height.mas_equalTo(40);
        }];
        self.viewHeight = 40*3;

    }
    return self;
}

-(void)setDetailParser:(BusinessFormOrderDetailParser *)detailParser {
    self.orderNumberLabel.text = [NSString stringWithFormat:@"    订单编号：%@",detailParser.order_no];
    self.orderAccountLabel.text = [NSString stringWithFormat:@"    下单账户：%@",detailParser.username];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"    下单时间：%@",[XSFormatterDate dateWithTimeIntervalString:detailParser.w_time]];
}

@end
