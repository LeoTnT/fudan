//
//  BusinessOrderDetailBottomTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessOrderDetailBottomTableViewCell.h"

@implementation BusinessOrderDetailBottomTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
        //        UIFont *font = [UIFont qsh_systemFontOfSize:16];
        self.orderNumberView = [[OrderNumberCountTimeView alloc]init];

        [self.contentView addSubview:self.orderNumberView];
        self.remarkLabel = [UILabel new];
        self.remarkLabel.backgroundColor = [UIColor whiteColor];
        self.remarkLabel.numberOfLines = 0;
        self.remarkLabel.textColor = mainGrayColor;
        self.remarkLabel.font = [UIFont systemFontOfSize:16];
        self.remarkLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self.contentView addSubview:self.remarkLabel];
        
    }
    return self;
}

-(void)setOrderParser:(BusinessFormOrderDetailParser *)orderParser {
    _orderParser = orderParser;
    self.orderNumberView.detailParser = _orderParser;
    [self.orderNumberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.orderNumberView.viewHeight);
    }];
    self.remarkLabel.text = [NSString stringWithFormat:@"    备注：%@",_orderParser.desc.length==0?Localized(@"无"):_orderParser.desc];

    CGSize size2 = [self.remarkLabel sizeThatFits:CGSizeMake(self.remarkLabel.frame.size.width, MAXFLOAT)];
    if (size2.height<30) {
        size2.height = 30;
    }
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orderNumberView.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(size2.height);
    }];
    self.cellHeight = self.orderNumberView.viewHeight+size2.height;

    
}

@end
