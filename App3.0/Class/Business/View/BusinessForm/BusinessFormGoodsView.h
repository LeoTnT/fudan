//
//  BusinessFormGoodsView.h
//  App3.0
//
//  Created by nilin on 2017/8/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BusinessFormGoodsViewDelegate<NSObject>
@optional
/**进入商品详情界面*/
-(void)toLookGoodsDetailInfoWithGoodsId:(NSString *) goodsId;
@end
typedef  NS_ENUM(NSUInteger,BusinessGoodsType)
{
    BusinessGoodsTypeForOrder,
    BusinessGoodsTypeForDetail,
    BusinessGoodsTypeForRefundList,
    
};
@interface BusinessFormGoodsView : UIView
@property(nonatomic,strong) NSArray *parserArray;//商品数组
@property(nonatomic,assign) CGFloat viewHeight;
@property(nonatomic,strong) UIView   *centerView;
@property(nonatomic,assign) BusinessGoodsType orderGoodsType;
@property(nonatomic,weak) id<BusinessFormGoodsViewDelegate> orderDelegate;

@end

