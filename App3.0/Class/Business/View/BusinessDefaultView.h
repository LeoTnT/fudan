//
//  BusinessDefaultView.h
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSCustomButton.h"
@protocol BusinessDefaultViewDelegate <NSObject>
/**申请*/
- (void)clicktoApplyBusinessEdition;
@end
@interface BusinessDefaultView : UIView
/**图片*/
@property(nonatomic,strong) UIImageView *imageView;

/**原因*/
@property(nonatomic,strong) UILabel *reasonLabel;
/**提示*/
@property(nonatomic,strong) UILabel *tintLabel;
/**申请按钮*/
@property(nonatomic,strong) UIButton *applyBtn;
/**代理*/
@property(nonatomic,weak) id<BusinessDefaultViewDelegate> businessDelegate;
@end
