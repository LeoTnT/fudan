//
//  BusinessQRView.m
//  App3.0
//
//  Created by nilin on 2017/8/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessQRView.h"
#import "UserInstance.h"

@implementation BusinessQRView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat space = 10.0f;
        CGFloat width = frame.size.width;
        CGFloat height = frame.size.height;
        
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5;
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(space, space, width-2*space, 30)];
        title.text = @"收款二维码";
        title.textColor = [UIColor blackColor];
        title.font = [UIFont systemFontOfSize:20];
        title.textAlignment = NSTextAlignmentCenter;
        [self addSubview:title];
        
        self.qrImgView = [[UIImageView alloc] init];//WithFrame:CGRectMake(width/4, (height-width/4)/2, width/2, width/2)];
        UIImage *qrImage = [XSQR createQrImageWithContentString:[UserInstance ShardInstnce].uid type:XSQRTypeUser];
        [self.qrImgView setImage:qrImage];
        [self addSubview:self.qrImgView];
        
        UILabel *desLabel = [[UILabel alloc] initWithFrame:CGRectMake(space, height-space-30, width-2*space, 30)];
        desLabel.text = @"扫一扫二维码，支付";
        desLabel.textColor = mainColor;
        desLabel.font = [UIFont systemFontOfSize:14];
        desLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:desLabel];
        
        [self.qrImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(width/3*2);
            make.centerX.mas_equalTo(self.mas_centerX);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.qrImgView.mas_top).offset(-10);
            make.centerX.mas_equalTo(self.mas_centerX);
        }];
        
        [desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.qrImgView.mas_bottom).offset(10);
            make.centerX.mas_equalTo(self.mas_centerX);
        }];
    }
    return self;
}


@end
