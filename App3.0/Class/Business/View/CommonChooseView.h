//
//  CommonChooseView.h
//  App3.0
//
//  Created by nilin on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessLogisticModel.h"
#import "BusinessClassifyModel.h"

@protocol CommonChooseViewDelegate <NSObject>

- (void)closeCommonChooseView;

@optional
- (void) selectedLogisticWithLogisticParser:(BusinessLogisticDetailParser *) parser;

@optional
- (void) selectedCategoryWithCategoryParser:(BusinessClassifyDetailParser *) parser;

@optional
- (void) toNewViewWithType:(NSUInteger) type;

@end
typedef NS_ENUM(NSInteger,CommonChooseViewType) {
   CommonChooseViewTypeLogistic=1,
    CommonChooseViewTypeCategory

};

@interface CommonChooseView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *oneButton;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listArray;
@property (nonatomic, assign) CommonChooseViewType commonChooseViewType;
@property (nonatomic, weak) id<CommonChooseViewDelegate> chooseDelegate;
@end
