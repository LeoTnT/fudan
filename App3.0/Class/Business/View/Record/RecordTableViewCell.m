//
//  RecordTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RecordTableViewCell.h"
#import "XSFormatterDate.h"

@interface RecordTableViewCell()
@property(nonatomic,strong)UILabel *sourceLabel;
@property(nonatomic,strong)UILabel *remarkLabel;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *inComeLabel;
@end
@implementation RecordTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //来源
        self.sourceLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, mainWidth-2*10, 30)];
        [self.contentView addSubview:self.sourceLabel];
        self.sourceLabel.font=[UIFont systemFontOfSize:15];
        //备注
        self.remarkLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.sourceLabel.frame), mainWidth-2*10, 25)];
        self.remarkLabel.font=[UIFont systemFontOfSize:12];
        self.remarkLabel.textColor=mainGrayColor;
        self.remarkLabel.numberOfLines=0;
        [self.contentView addSubview:self.remarkLabel];
        //时间
        self.timeLabel=[[UILabel alloc] init];
        self.timeLabel.textColor=mainGrayColor;
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.timeLabel];
        //收入、支出
        self.inComeLabel=[[UILabel alloc] init];
        self.inComeLabel.font=[UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.inComeLabel];
        self.inComeLabel.textAlignment=NSTextAlignmentRight;
        self.inComeLabel.textColor=[UIColor redColor];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setRecord:(BusinessRecordListDetailParser *)record{
    _record = record;
    self.sourceLabel.text = [NSString stringWithFormat:@"%@：%@",_record.type_name,_record.number];
    NSInteger length=[[NSString stringWithFormat:@"%@：%@",_record.type_name,_record.number] length];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:self.sourceLabel.text];
    [attributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(0, length)];
    
    self.sourceLabel.attributedText = attributedStr;

    //计算高度
    NSDictionary * dic1=@{NSFontAttributeName:[UIFont systemFontOfSize:12]};
    CGRect frame1= [record.remark boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.remarkLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic1 context:nil];
    self.remarkLabel.text=[NSString stringWithFormat:@"备注：%@",record.remark];
    self.remarkLabel.frame=CGRectMake(10, CGRectGetMaxY(self.sourceLabel.frame), mainWidth-2*10, frame1.size.height);
    self.timeLabel.frame=CGRectMake(10, CGRectGetMaxY(self.remarkLabel.frame), mainWidth, 25);
    self.inComeLabel.frame=CGRectMake(mainWidth-10-50,CGRectGetMinY(self.timeLabel.frame), 50, 25);
    self.timeLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",record.w_time]];
    if ([record.number floatValue]>0) {
        self.inComeLabel.text=Localized(@"收入");
    }else{
        self.inComeLabel.text=Localized(@"支出");
    }
    self.height=CGRectGetMaxY(self.inComeLabel.frame);
}



@end
