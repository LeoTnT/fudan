//
//  RecordTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@interface RecordTableViewCell : UITableViewCell
@property(nonatomic,strong) BusinessRecordListDetailParser *record;
@property(nonatomic,assign)CGFloat height;
@end
