//
//  BusinessBankTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@interface BusinessBankTableViewCell : UITableViewCell
@property (nonatomic, strong) BusinessBankCardDataParser *parser;
@property (nonatomic, assign) BOOL isShow;//是否显示带删除的区域
@end
