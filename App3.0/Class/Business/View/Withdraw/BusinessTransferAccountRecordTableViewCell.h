//
//  BusinessTransferAccountRecordTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@interface BusinessTransferAccountRecordTableViewCell : UITableViewCell
@property(nonatomic,assign)CGFloat height;
//@property(nonatomic,strong) TransferAccountRecord *record;
@property(nonatomic,strong) BusinessTakecashDataParser *withDrawRecord;
@end
