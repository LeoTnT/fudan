//
//  BusinessBankCardTypeView.h
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@protocol BusinessBankCardTypeViewDelegate<NSObject>

@optional
-(void) hiddenCardView:(BusinessCardTypeDataParser *) dataParser;

@end
@interface BusinessBankCardTypeView : UIView
@property (nonatomic, strong) NSMutableArray *typeArray;
@property (nonatomic, copy) NSString *selectedId;
@property (nonatomic, weak) id<BusinessBankCardTypeViewDelegate> typeDelegate;
@property (nonatomic, strong) UIScrollView *scrollView;
@end
