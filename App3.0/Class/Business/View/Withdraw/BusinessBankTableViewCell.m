//
//  BusinessBankTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessBankTableViewCell.h"
@interface BusinessBankTableViewCell(){
    UIImageView *_imgView;
    UILabel *_bankNameLabel;
    UILabel *_nameLabel;
    UILabel *_bankNumber;
    UIImageView *_img;//默认图标
    UIView *_hiddenView;
}
@end

@implementation BusinessBankTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifie {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifie];
    if (self) {
        [self setSubviews];
    }
    return self;
    
}

- (void)setIsShow:(BOOL)isShow {
    _isShow = isShow;
    _hiddenView.hidden = !_isShow;
}

- (void)setParser:(BusinessBankCardDataParser *)parser {
    _parser = parser;
    NSLog(@"_parser.bankname%@,%@,%@",_parser.bankname,_parser.bankuser,_parser.bankcard);
    [_imgView getImageWithUrlStr:_parser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    _bankNameLabel.text = _parser.bankname;
    _nameLabel.text = _parser.bankuser;
    _bankNumber.text = _parser.bankcard;
    _img.hidden = ![_parser.is_default  integerValue];
}

- (void)setSubviews {
    CGFloat imageSize = 50,cellHeight = 120;
    UIView *bg_view = [[UIView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE,mainWidth-2*NORMOL_SPACE ,cellHeight-10 )];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bg_view.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bg_view.bounds;
    maskLayer.path = maskPath.CGPath;
    bg_view.layer.mask = maskLayer;
    bg_view.layer.masksToBounds = YES;
    bg_view.backgroundColor = HighLightColor_Main;
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
    _imgView.contentMode = UIViewContentModeScaleAspectFit;
    [_imgView getImageWithUrlStr:nil andDefaultImage:[UIImage imageNamed:@"chat_single"]];
    [bg_view addSubview:_imgView];
    _bankNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+NORMOL_SPACE, CGRectGetMinY(_imgView.frame), mainWidth-CGRectGetWidth(_imgView.frame)-3*NORMOL_SPACE, CGRectGetHeight(_imgView.frame)/2)];
    _bankNameLabel.font = [UIFont systemFontOfSize:20];
    [bg_view addSubview:_bankNameLabel];
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_bankNameLabel.frame), CGRectGetMaxY(_bankNameLabel.frame), CGRectGetWidth(_bankNameLabel.frame)-NORMOL_SPACE*4, CGRectGetHeight(_bankNameLabel.frame))];
    _nameLabel.font = [UIFont systemFontOfSize:20];
    [bg_view addSubview:_nameLabel];
    _img= [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(bg_view.frame)-4*NORMOL_SPACE, CGRectGetMaxY(_nameLabel.frame)-NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
    _img.image = [UIImage imageNamed:@"user_card_ok"];
    [bg_view addSubview:_img];
    _bankNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_nameLabel.frame), CGRectGetMaxY(_nameLabel.frame)+NORMOL_SPACE, CGRectGetWidth(_nameLabel.frame), CGRectGetHeight(_nameLabel.frame))];
    _bankNumber.font = [UIFont systemFontOfSize:20];
    [bg_view addSubview:_bankNumber];
    
    //设为默认，删除view
    _hiddenView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetHeight(bg_view.frame)-NORMOL_SPACE*4, CGRectGetWidth(bg_view.frame), NORMOL_SPACE*4)];
    _hiddenView.hidden = self.isShow;
    UIColor *hColor = [UIColor blackColor];
    _hiddenView.backgroundColor = [hColor colorWithAlphaComponent:0.5];
    UIButton *defaultBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_hiddenView.frame)/2, CGRectGetHeight(_hiddenView.frame))];
    defaultBtn.backgroundColor = [UIColor clearColor];
//    [defaultBtn addTarget:self action:@selector(toDefaultBankAction:) forControlEvents:UIControlEventTouchUpInside];
    [defaultBtn setTitle:@"设置为默认账号" forState:UIControlStateNormal];
    [_hiddenView addSubview:defaultBtn];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(defaultBtn.frame), 0, 1, CGRectGetHeight(_hiddenView.frame))];
    line.backgroundColor = [UIColor whiteColor];
    [_hiddenView addSubview:line];
    UIButton *deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(defaultBtn.frame)+1, 0, CGRectGetWidth(_hiddenView.frame)/2-1, CGRectGetHeight(_hiddenView.frame))];
    deleteBtn.backgroundColor = [UIColor clearColor];
//    [deleteBtn addTarget:self action:@selector(todeleteBankAction:) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [_hiddenView addSubview:deleteBtn];
    [_hiddenView addSubview:defaultBtn];
    [bg_view addSubview:_hiddenView];
    [self.contentView addSubview:bg_view];
}


@end
