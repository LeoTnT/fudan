//
//  BusinessTransferAccountRecordTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessTransferAccountRecordTableViewCell.h"
#import "XSFormatterDate.h"

@interface BusinessTransferAccountRecordTableViewCell ()
@property(nonatomic,strong)UIView *colorView;
@property(nonatomic,strong)UILabel *toUserNameLabel;
@property(nonatomic,strong)UILabel *operateLabel;
@property(nonatomic,strong)UIImageView *stateImg;
@property(nonatomic,strong)UILabel *toMoneyLabel;
@property(nonatomic,strong)UILabel *fromMoneyLabel;
@property(nonatomic,strong)UILabel *poundageLabel;//手续费
@property(nonatomic,strong)UILabel *reallyToMoneyTipsLabel;
@property(nonatomic,strong)UILabel *remarkLabel;
@property(nonatomic,strong)UILabel *cRemarkLabel;//审核备注
@property(nonatomic,strong)UILabel *checkTimeLabel;//审核时间
@end
@implementation BusinessTransferAccountRecordTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.colorView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 3)];
        [self.contentView addSubview:self.colorView];
        self.toUserNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.colorView.frame)+10, mainWidth/2.0, 20)];
        [self.contentView addSubview:self.toUserNameLabel];
        self.operateLabel=[[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2.0, CGRectGetMaxY(self.colorView.frame)+10, mainWidth/2.0-35, 20)];
        self.operateLabel.textAlignment=NSTextAlignmentRight;
        self.operateLabel.textColor=mainGrayColor;
        self.operateLabel.font=[UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.operateLabel];
        self.stateImg=[[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-30,CGRectGetMaxY(self.colorView.frame)+10 , 20, 20)];
        self.stateImg.backgroundColor=TAB_SELECTED_COLOR;
        [self.contentView addSubview:self.stateImg];
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.operateLabel.frame)+NORMOL_SPACE, mainWidth, 1)];
        line.backgroundColor = LINE_COLOR;
        [self.contentView addSubview:line];
        
        self.toMoneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(line.frame)+10, mainWidth/2.0, 30)];
        self.toMoneyLabel.font=[UIFont systemFontOfSize:20];
        self.toMoneyLabel.textAlignment=NSTextAlignmentLeft;
        [self.contentView addSubview:self.toMoneyLabel];
        self.fromMoneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.operateLabel.frame)+10, mainWidth, 30)];
        self.fromMoneyLabel.textAlignment=NSTextAlignmentRight;
        [self.contentView addSubview:self.fromMoneyLabel];
        self.reallyToMoneyTipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.toMoneyLabel.frame), mainWidth/2.0, 20)];
        [self.contentView addSubview:self.reallyToMoneyTipsLabel];
        self.poundageLabel=[[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2.0, CGRectGetMaxY(self.fromMoneyLabel.frame), mainWidth/2.0-10, 20)];
        self.poundageLabel.textAlignment=NSTextAlignmentRight;
        [self.contentView addSubview:self.poundageLabel];
        self.remarkLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.reallyToMoneyTipsLabel.frame)+10, mainWidth, 20)];
        self.remarkLabel.font=[UIFont systemFontOfSize:15];
        self.remarkLabel.textColor=mainGrayColor;
        self.remarkLabel.numberOfLines=0;
        [self.contentView addSubview:self.remarkLabel];
        self.cRemarkLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.remarkLabel.frame)+10, mainWidth/2.0, 20)];
        [self.contentView addSubview:self.cRemarkLabel];
        self.cRemarkLabel.textColor=mainGrayColor;
        self.checkTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(mainWidth/2.0, CGRectGetMinY(self.cRemarkLabel.frame)+10, mainWidth/2.0, 20)];
        [self.contentView addSubview:self.checkTimeLabel];
        self.checkTimeLabel.textAlignment=NSTextAlignmentRight;
        self.checkTimeLabel.font=[UIFont systemFontOfSize:15];
        self.checkTimeLabel.textColor=mainGrayColor;
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setWithDrawRecord:(BusinessTakecashDataParser *)withDrawRecord{
    _withDrawRecord=withDrawRecord;
    if ([_withDrawRecord.status intValue]==0) {//审核中
        self.colorView.backgroundColor=TAB_SELECTED_COLOR;
        self.stateImg.image=[UIImage imageNamed:@"user_review"];
    }else if ([_withDrawRecord.status intValue]==1){//通过
        self.colorView.backgroundColor=[UIColor greenColor];
        self.stateImg.image=[UIImage imageNamed:@"user_review_success"];
    }else{//未通过
        self.colorView.backgroundColor=[UIColor redColor];
        self.stateImg.image=[UIImage imageNamed:@"user_review_fail"];
    }
    self.toUserNameLabel.text=[NSString stringWithFormat:@"  提现到：%@",_withDrawRecord.name];
    self.operateLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",_withDrawRecord.w_time]];
    self.toMoneyLabel.text=[NSString stringWithFormat:@"  %@",_withDrawRecord.receive];
    long int length1=[@"提现金额：" length];
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"提现金额：¥%@",_withDrawRecord.number]];
    [attri addAttribute:NSForegroundColorAttributeName value:mainGrayColor range:NSMakeRange(0, length1)];
    self.fromMoneyLabel.attributedText=attri;
    self.reallyToMoneyTipsLabel.text=@"  实际到账";
    long int length2=[@"手续费：" length];
    NSMutableAttributedString *attri2=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"手续费：¥%@",_withDrawRecord.poundage]];
    [attri2 addAttribute:NSForegroundColorAttributeName value:mainGrayColor range:NSMakeRange(0, length2)];
    self.poundageLabel.attributedText=attri2;
    self.remarkLabel.text=[NSString stringWithFormat:@"  备注：%@",withDrawRecord.remark];
    NSDictionary * dic=@{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGRect frame= [self.remarkLabel.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.remarkLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    self.remarkLabel.frame=CGRectMake(0, CGRectGetMaxY(self.reallyToMoneyTipsLabel.frame)+10, mainWidth,frame.size.height);
    self.cRemarkLabel.text=[NSString stringWithFormat:@"  审核意见：%@",_withDrawRecord.c_remark];
    self.cRemarkLabel.frame=CGRectMake(0, CGRectGetMaxY(self.remarkLabel.frame)+10, mainWidth/2.0, 20);
    if ([_withDrawRecord.c_time integerValue]==0) {
        self.checkTimeLabel.text=@"审核时间：暂无  ";
    }else{
        self.checkTimeLabel.text=[NSString stringWithFormat:@"审核时间：%@  ",[XSFormatterDate dateWithTimeIntervalString:_withDrawRecord.c_time]];
    }
    self.checkTimeLabel.frame=CGRectMake(mainWidth/2.0, CGRectGetMinY(self.cRemarkLabel.frame), mainWidth/2.0, 20);
    self.height=CGRectGetMaxY(self.checkTimeLabel.frame)+2*10;
}


@end
