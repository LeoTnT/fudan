//
//  ReportCenterTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@interface ReportCenterTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *orderCountLabel;

@property (nonatomic, strong) NSArray *itemArray;
@end
