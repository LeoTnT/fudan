//
//  ReportBottomTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSLineChartView.h"

@interface ReportBottomTableViewCell : UITableViewCell

@property (nonatomic, strong) WSLineChartView *wsLineView;
@property (nonatomic, strong) NSDictionary *incomeWeekDictionary;
@end
