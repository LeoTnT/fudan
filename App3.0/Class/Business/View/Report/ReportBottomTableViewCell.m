//
//  ReportBottomTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReportBottomTableViewCell.h"
@interface ReportBottomTableViewCell()
@property (nonatomic, strong) UIView *backGroundView;
@end


@implementation ReportBottomTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
        self.backGroundView = [UIView new];
        self.backGroundView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.backGroundView];
        
        UILabel *label = [UILabel new];
        label.text = @"收益曲线图";
        label.font = [UIFont qsh_systemFontOfSize:17];
        label.textColor = [UIColor redColor];
        [self.backGroundView addSubview:label];
       
        
        [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        }];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.backGroundView).with.mas_offset(20);
            make.height.mas_equalTo(20);
            make.width.mas_equalTo(mainWidth-40);
            make.left.mas_equalTo(self.backGroundView).with.mas_offset(10);
        }];
        
       
        
        
    }
    return self;
}

-(void)setIncomeWeekDictionary:(NSDictionary *)incomeWeekDictionary {
    _incomeWeekDictionary = incomeWeekDictionary;
//    _incomeWeekDictionary = @{@"08-30":@"0.01",@"08-31":@"0.01",@"09-01":@"0.01",@"07-04":@"0.01"};
   NSString *sortString = [self stringWithDict:_incomeWeekDictionary];
    NSMutableArray *xArray = [NSMutableArray array];
    NSMutableArray *yArray = [NSMutableArray array];
    NSArray *sortedArray = [sortString componentsSeparatedByString:@","];
    for (NSInteger i = 0; i < sortedArray.count; i++) {
        NSString *string = sortedArray[i];
        NSArray *tempArray = [string componentsSeparatedByString:@":"];
        [xArray addObject:[tempArray firstObject]];
        [yArray addObject:[tempArray lastObject]];
        
    }
    NSInteger max = [[yArray valueForKeyPath:@"@max.intValue"] integerValue];
    self.wsLineView = [[WSLineChartView alloc]initWithFrame:CGRectMake(0, 50, mainWidth-15*2, 105) xTitleArray:xArray yValueArray:yArray yMax:max yMin:0];
    [self.backGroundView addSubview:self.wsLineView];
    [self.wsLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.backGroundView.mas_bottom).with.mas_offset(25);
        make.top.mas_equalTo(50);
        make.left.mas_equalTo(self.backGroundView);
        make.width.mas_equalTo(self.backGroundView.mas_width);
    }];
}

-(NSString*)stringWithDict:(NSDictionary*)dict{
    
    NSArray*keys = [dict allKeys];
    
    NSArray*sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1,id obj2) {
        return[obj1 compare:obj2 options:NSNumericSearch];//正序
    }];
    
    NSString*str =@"";
    for(NSString*categoryId in sortedArray) {
        
        id value = [dict objectForKey:categoryId];
        
        if([value isKindOfClass:[NSDictionary class]]) {
            
            value = [self stringWithDict:value];
            
        }
        
        if([str length] !=0) {
            
            str = [str stringByAppendingString:@","];
            
        }
        
        str = [str stringByAppendingFormat:@"%@:%@",categoryId,value];
        
    }
    NSLog(@"str:%@",str);
    return str;
}

@end
