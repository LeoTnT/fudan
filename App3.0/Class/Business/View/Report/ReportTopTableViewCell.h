//
//  ReportTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportTopTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) CGFloat cellHeight;
@end
