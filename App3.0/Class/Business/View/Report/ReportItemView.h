//
//  ReportItemView.h
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportItemView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UILabel *tintLabel;

@property (nonatomic, strong) NSArray *contentArray;//image,0,tint
@end
