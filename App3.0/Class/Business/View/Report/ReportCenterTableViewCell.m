//
//  ReportCenterTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReportCenterTableViewCell.h"
#import "ReportItemView.h"

@implementation ReportCenterTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = BG_COLOR;
    }
    return self;
}

-(void)setItemArray:(NSArray *)itemArray {
    _itemArray = itemArray;
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    UIImageView *topImageView = [UIImageView new];
    topImageView.image = [UIImage imageNamed:@"report_center"];
    [self.contentView addSubview:topImageView];
    
    UIView *bigView = [UIView new];
    bigView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bigView];
    
    self.numberLabel = [UILabel new];
    self.numberLabel.backgroundColor = [UIColor whiteColor];
    self.numberLabel.textColor = [UIColor redColor];
    self.numberLabel.text = @"0";
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.font = [UIFont qsh_systemFontOfSize:25];
    [bigView addSubview:self.numberLabel];
    UILabel *leftTintLabel = [UILabel new];
    leftTintLabel.text = @"今日成交金额";
    leftTintLabel.backgroundColor = [UIColor whiteColor];
    leftTintLabel.font = [UIFont qsh_systemFontOfSize:15];
    leftTintLabel.textColor = [UIColor grayColor];
    leftTintLabel.textAlignment = NSTextAlignmentCenter;
    [bigView addSubview:leftTintLabel];
    
    self.orderCountLabel = [UILabel new];
    self.orderCountLabel.textColor = [UIColor redColor];
    self.orderCountLabel.text = @"0";
    self.orderCountLabel.backgroundColor = [UIColor whiteColor];
    self.orderCountLabel.textAlignment = NSTextAlignmentCenter;
    self.orderCountLabel.font = [UIFont qsh_systemFontOfSize:25];
    [bigView addSubview:self.orderCountLabel];
    UILabel *rightTintLabel = [UILabel new];
    rightTintLabel.text = @"今日订单数";
    rightTintLabel.backgroundColor = [UIColor whiteColor];
    rightTintLabel.font = [UIFont qsh_systemFontOfSize:15];
    rightTintLabel.textColor = [UIColor grayColor];
    rightTintLabel.textAlignment = NSTextAlignmentCenter;
    [bigView addSubview:rightTintLabel];
    
    UILabel *line = [UILabel new];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [bigView addSubview:line];
    
    UIImageView *bottomImageView = [UIImageView new];
    bottomImageView.image = [UIImage imageNamed:@"report_bottom"];
    [self.contentView addSubview: bottomImageView];
    
    //15+100
    [topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
    }];
    
    [bigView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topImageView.mas_bottom);
        make.left.mas_equalTo(self.contentView).with.mas_offset(10);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.height.mas_offset(100);
    }];
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bigView).with.mas_offset(20);
        make.left.mas_equalTo(bigView);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo((mainWidth-30)/2);
    }];
    [self.orderCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.width.mas_equalTo(self.numberLabel);
        make.left.mas_equalTo(self.numberLabel.mas_right);
    }];
    
    [leftTintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.height.width.mas_equalTo(self.numberLabel);
        make.top.mas_equalTo(self.numberLabel.mas_bottom);
    }];
    
    [rightTintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.top.mas_equalTo(leftTintLabel);
        make.left.mas_equalTo(leftTintLabel.mas_right);
    }];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftTintLabel);
        make.top.mas_equalTo(leftTintLabel.mas_bottom).with.offset(19);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo(bigView.mas_width);
    }];
    
    self.numberLabel.text = [itemArray firstObject];
    self.orderCountLabel.text = itemArray[1];
    
    UIView *centerView = [UIView new];
    centerView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:centerView];
    //320+100+20+15
//    NSArray *array = @[@[@"report_goodsNum",itemArray[2],@"今日被浏览宝贝"],@[@"report_collectionNum",itemArray[3],@"今日被收藏宝贝"],@[@"report_cartNum",itemArray[4],@"今日购物车宝贝"],@[@"report_refound",itemArray[5],@"退款中"],@[@"report_payment",itemArray[6],@"待付款"],@[@"report_waitPay",itemArray[7],@"待发货"],@[@"report_oldMoney",itemArray[8],@"昨日成交金额"],@[@"report_nowMoney",itemArray[9],@"今日成交金额"]];
     NSArray *array = @[@[@"report_goodsNum",itemArray[2],@"今日被浏览宝贝"],@[@"report_collectionNum",itemArray[3],@"今日被收藏宝贝"],@[@"report_cartNum",itemArray[4],@"今日购物车宝贝"],@[@"report_payment",itemArray[5],@"待付款"],@[@"report_waitPay",itemArray[6],@"待发货"],@[@"report_oldMoney",itemArray[7],@"昨日成交金额"],@[@"report_nowMoney",itemArray[8],@"今日成交金额"]];
    CGFloat centerViewHeight = array.count%2==0?array.count/2*80:(array.count/2+1)*80;
    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bigView.mas_bottom);
        make.left.mas_equalTo(self.contentView).with.mas_offset(10);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.height.mas_equalTo(centerViewHeight);
    }];
   
    for (int i=0; i<array.count; i++) {
        int row = i/2;
        int col = i%2;
        @autoreleasepool {
            ReportItemView *reportView = [[ReportItemView alloc] initWithFrame:CGRectZero];
            reportView.contentArray = array[i];
            [centerView addSubview:reportView];
            CGFloat viewWidth = (mainWidth-20)/2;
            [reportView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(centerView).with.mas_offset(row*80);
                make.left.mas_equalTo(centerView.mas_left).with.mas_offset(col*viewWidth);
                make.height.mas_equalTo(80);
                make.width.mas_equalTo(viewWidth);
            }];
            
            
        };
    }

    //30
    [bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(20);
    }];

   
}

@end
