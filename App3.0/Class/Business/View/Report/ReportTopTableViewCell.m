//
//  ReportTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReportTopTableViewCell.h"

@implementation ReportTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
        self.cellHeight = 150;
        self.backImageView = [UIImageView new];
        self.backImageView.image = [UIImage imageNamed:@"report_top"];
        [self.contentView addSubview:self.backImageView];
        self.icon = [UIImageView new];
        self.icon.image = [UIImage imageNamed:@"fuDan_logo"];
        [self.contentView addSubview:self.icon];
        self.nameLabel = [UILabel new];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.font = [UIFont qsh_systemFontOfSize:20];
        self.nameLabel.text = @"先锋";
        [self.contentView addSubview:self.nameLabel];
        
        [self.backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.mas_equalTo(self.contentView);
            
        }];
        
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).with.mas_offset(35);
            make.left.mas_equalTo(self.contentView).with.mas_offset(self.cellHeight/5);
            make.height.width.mas_equalTo(80);
        }];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.icon.mas_right).with.mas_offset(20);
            make.top.mas_equalTo(self.icon);
            make.height.mas_equalTo(self.icon.mas_height);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-30);
        }];
        
        self.icon.layer.cornerRadius = 40;
        self.icon.layer.masksToBounds = YES;
    
    }
    return self;
}

-(void)setIconUrl:(NSString *)iconUrl {
    _iconUrl = iconUrl;
    [self.icon getImageWithUrlStr:_iconUrl andDefaultImage:[UIImage imageNamed:@"fuDan_logo"]];
}

-(void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = _name;
}
@end
