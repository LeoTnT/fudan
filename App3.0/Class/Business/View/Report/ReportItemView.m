//
//  ReportItemView.m
//  App3.0
//
//  Created by nilin on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReportItemView.h"

@implementation ReportItemView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.imageView = [UIImageView new];
        [self addSubview:self.imageView];
        self.countLabel = [UILabel new];
        self.countLabel.font = [UIFont qsh_systemFontOfSize:25];
        [self addSubview:self.countLabel];
        self.tintLabel = [UILabel new];
        self.tintLabel.font = [UIFont qsh_systemFontOfSize:13];
        self.tintLabel.textColor = [UIColor grayColor];
        [self addSubview:self.tintLabel];
        CGFloat viewHeight = 80;
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.mas_equalTo(self).with.mas_offset(viewHeight/5);
            make.height.width.mas_equalTo(viewHeight/2);
            
        }];
        
        [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).with.mas_offset(10);
            make.left.mas_equalTo(self.imageView.mas_right).with.mas_offset(10);
            make.right.mas_equalTo(self);
            make.height.mas_equalTo(30);
        }];
        
        [self.tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.countLabel.mas_bottom);
            make.left.mas_equalTo(self.imageView.mas_right).with.mas_offset(10);
            make.right.mas_equalTo(self);
            make.height.mas_equalTo(20);
        }];
    }
    return self;
}

-(void)setContentArray:(NSArray *)contentArray {
    _contentArray = contentArray;
    self.imageView.image = [UIImage imageNamed:[_contentArray firstObject]];
    self.countLabel.text = _contentArray[1];
    self.tintLabel.text = _contentArray[2];
}
@end
