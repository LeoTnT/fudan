//
//  TradeView.m
//  App3.0
//
//  Created by nilin on 2017/5/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TradeView.h"

@implementation TradeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        CGFloat height = 2*NORMOL_SPACE;
        UILabel *tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE+64, 150, height)];
        tintLabel.text = @"请选择行业";
        tintLabel.textAlignment = NSTextAlignmentLeft;
        tintLabel.font = [UIFont systemFontOfSize:19];
        [self addSubview:tintLabel];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.frame)-height-NORMOL_SPACE, NORMOL_SPACE+64, height, height)];
        [closeButton setBackgroundImage:[UIImage imageNamed:@"user_cart_close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeButton];
    }
    return self;
}
-(void)setTradeArray:(NSArray *)tradeArray{
    _tradeArray = tradeArray;
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[XSCustomButton class]]) {
            [view removeFromSuperview];
        }
    }
    for (int i=0; i<_tradeArray.count; i++) {
        BusinessApplyIndustryParser *parser = _tradeArray[i];
        CGFloat width = (mainWidth-4*NORMOL_SPACE)/3;
        CGFloat height = 3*NORMOL_SPACE;
        NSUInteger row = i/3,column = i%3;
        XSCustomButton *titleButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE*(column+1)+width*column, NORMOL_SPACE*5+64+row*(height+NORMOL_SPACE), width, height) title:parser.name titleColor:[UIColor blackColor] fontSize:17 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor highBackgroundColor:[UIColor whiteColor]];
        titleButton.tag = [parser.ID integerValue];
        [titleButton setBorderWith:0.5 borderColor:LINE_COLOR.CGColor cornerRadius:5];
        [titleButton addTarget:self action:@selector(selectedTrade:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:titleButton];
    }
    
}
-(void)closeAction{
    [self.delegate closeTradeView];
}
-(void)selectedTrade:(UIButton *) sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
        for (BusinessApplyIndustryParser *parser in self.tradeArray) {
            if ([parser.ID integerValue]==sender.tag) {
                if ([self.delegate respondsToSelector:@selector(selectedTradeWithTradeParser:)]) {
                     [self.delegate selectedTradeWithTradeParser:parser];
                }
                break;
                
            }
        }
    }
}
@end
