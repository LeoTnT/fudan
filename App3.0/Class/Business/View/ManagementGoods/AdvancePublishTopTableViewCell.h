//
//  AdvancePublishTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdvancePublishTopDelegate <NSObject>

- (void)mainImageWithImage:(UIImage *) image;

@end

@interface AdvancePublishTopTableViewCell : UITableViewCell
/**图片数组*/
@property(nonatomic,strong)NSArray *photosArray;
/**cell的高度*/
@property(nonatomic,assign)CGFloat height;
/**最后一个图片*/
@property(nonatomic,strong)UIImageView *lastImage;

@property (nonatomic, strong) UIButton *photoButton;

@property (nonatomic, strong) UIImageView *bigImageView;

/**所有的删除按钮*/
@property(nonatomic,strong)NSMutableArray *deletBtnArray;
/**所有的除了加号的图片*/
@property(nonatomic,strong)NSMutableArray *imagesArray;

@property (nonatomic, strong) UIImageView *mainImageView;

@property (nonatomic, weak) id<AdvancePublishTopDelegate> delegate;


@end
