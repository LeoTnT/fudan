//
//  BusinessEditGoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/6/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessEditGoodsTableViewCell.h"
#import "UIImage+XSWebImage.h"

@implementation BusinessEditGoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle =UITableViewCellSelectionStyleNone;
        [self setSubViews];
        
    }
    return self;
}

-(void)setSubViews {
    self.height = 100;
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
    tempLabel.backgroundColor = BG_COLOR;
    [self.contentView addSubview:tempLabel];
    CGFloat imagesize = 70,width = (mainWidth-imagesize-3*NORMOL_SPACE)/3 ;
    self.selectedButton = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, self.height/2-NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
    [self.selectedButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
    [self.selectedButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(clickSelectedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.selectedButton];
    self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.selectedButton.frame)+NORMOL_SPACE, NORMOL_SPACE+CGRectGetMaxY(tempLabel.frame), imagesize, imagesize)];
    [self.contentView addSubview:self.icon];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+NORMOL_SPACE, CGRectGetMinY(self.icon.frame), mainWidth-imagesize-3*NORMOL_SPACE, 20)];
    [self.contentView addSubview:self.titleLabel];
    self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), CGRectGetWidth(self.titleLabel.frame), 30)];
    self.priceLabel.textColor = [UIColor redColor];
    self.priceLabel.font = [UIFont systemFontOfSize:20];
    [self.contentView addSubview:self.priceLabel];
    self.soldLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.priceLabel.frame), width, 20)];
    self.soldLabel.font = [UIFont systemFontOfSize:13];
    self.soldLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.soldLabel];
    self.stockLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.soldLabel.frame), CGRectGetMaxY(self.priceLabel.frame), width, 20)];
    self.stockLabel.font = [UIFont systemFontOfSize:13];
    self.stockLabel.textAlignment = NSTextAlignmentRight;
    self.stockLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.stockLabel];
}

- (void)setDetailParser:(BusinessProductDetailParser *)detailParser {
    _detailParser = detailParser;
    [self.icon getImageWithUrlStr:_detailParser.image andDefaultImage:[UIImage imageNamed:@"fuDan_logo"]];
    self.titleLabel.text = _detailParser.product_name;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",_detailParser.sell_price];
    //    _detailParser.stock_num
    self.soldLabel.text = [NSString stringWithFormat:@"已售%@",_detailParser.sell_num];
    self.stockLabel.text = [NSString stringWithFormat:@"库存%@",_detailParser.stock_num];


}

-(void)clickSelectedButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(deleteProductWithId:sure:)]) {
        [self.delegate deleteProductWithId:self.detailParser.product_id sure:sender.selected];
    }
}

+(NSString *)idString {
    return @"businessEditGoods";
}


@end
