//
//  BusinessSpecTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticAreaView.h"
#import "BusinessModel.h"

@protocol BusinessSpecTopDelegate <NSObject>

- (void)changeHeight:(BusinessSpecListParser *) parser isOpen:(BOOL) open;

@end

@interface BusinessSpecTopTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) LogisticAreaView *areaview;
@property (nonatomic, strong) UIButton *upOrDownButton;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) NSMutableArray *selectedArray;

@property (nonatomic, strong) BusinessSpecListParser *specParser;
@property (nonatomic, weak) id<BusinessSpecTopDelegate> delegate;
- (void)upOrDownView:(UIButton *) sender;
+ (NSString *) idString;

@end
