

//
//  BusinessPriceFieldTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessPriceFieldTableViewCell.h"

@interface BusinessPriceFieldTableViewCell()<UITextFieldDelegate>

@end

@implementation BusinessPriceFieldTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
    
}

-(void)setFieldArray:(NSArray *)fieldArray {
    _fieldArray = fieldArray;
    
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    UILabel *line = [UILabel new];
    line.backgroundColor = LINE_COLOR;
    [self.contentView addSubview:line];
    self.titleLabel = [UILabel new];
    [self.contentView addSubview:self.titleLabel];
    self.scanButton = [UIButton new];
    [self.scanButton setTitle:@"条形码扫描" forState:UIControlStateNormal];
    [self.scanButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.scanButton setImage:[UIImage imageNamed:@"a1search"] forState:UIControlStateNormal];
//    self.scanButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.contentView addSubview:self.scanButton];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
        make.left.right.mas_equalTo(self.contentView);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(self.contentView);
        make.width.mas_equalTo(150);
    }];
    [self.scanButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom);
        make.height.mas_equalTo(40);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.width.mas_equalTo(150);
    }];
    
    
    NSMutableArray * listAray = [NSMutableArray arrayWithArray:_fieldArray];
    if (self.cellType==PriceFieldTableViewCellTypeBatch) {
        BusinessPriceFieldDetailParser *parser = [BusinessPriceFieldDetailParser mj_objectWithKeyValues:@{@"name":Localized(@"material_dialog_positive_text"),@"field":@""}];
        [listAray addObject:parser];
    }
    CGFloat width = (mainWidth-15*4)/3,height = 40;
    for (int i=0; i<listAray.count; i++) {
       
        @autoreleasepool {
            NSUInteger row = i/3,col = i%3;
            
            BusinessPriceFieldDetailParser *parser = listAray[i];

            if (i<listAray.count-1) {
                 NSString *text = self.editArray[i];
                UITextField *textField = [UITextField new];
                textField.backgroundColor = BG_COLOR;
                  textField.font = [UIFont qsh_systemFontOfSize:17];
                textField.adjustsFontSizeToFitWidth = YES;
                
//            [textField setValue:[UIFont systemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
                NSString *holderText;
                if ([parser.require integerValue]==1) {
                  holderText = parser.name;
                } else {
                    
                    holderText = [NSString stringWithFormat:@"%@(可选)",parser.name];
                }
                NSMutableAttributedString *placeholder = [[NSMutableAttributedString
                  alloc] initWithString:holderText];
                
                [placeholder
                 addAttribute:NSFontAttributeName
                 value:mainWidth>375?[UIFont
                                      qsh_systemFontOfSize:14]:[UIFont
                                                                qsh_systemFontOfSize:12]
                 range:NSMakeRange(0,
                                   holderText.length)];
                
                textField.attributedPlaceholder = placeholder;
                textField.textAlignment = NSTextAlignmentCenter;
                if ([parser.type isEqualToString:@"string"]) {
                     textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                } else if ([parser.type isEqualToString:@"float"]) {
                     textField.keyboardType = UIKeyboardTypeDecimalPad;
                    
                }
               
                textField.returnKeyType = UIReturnKeyDone;
                textField.textColor = [UIColor redColor];
                textField.tag = 100+i;
                if (text) {
                    textField.text = text;
                }
              
                [self.contentView addSubview:textField];
                [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.titleLabel.mas_bottom).with.mas_offset(10*(row+1)+row*height);
                    make.left.mas_equalTo(self.contentView).with.mas_offset(15*(col+1)+col*width);
                    make.width.mas_equalTo(width);
                    make.height.mas_equalTo(height);
                }];
  
            } else {
                if (self.cellType==PriceFieldTableViewCellTypeBatch) {
                    self.batchWrite = [UIButton new];
                    [self.batchWrite setTitle:parser.name forState:UIControlStateNormal];
                    [self.batchWrite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    self.batchWrite.layer.cornerRadius = 2;
                    self.batchWrite.layer.masksToBounds = YES;
                    self.batchWrite.backgroundColor = mainColor;
                    [self.contentView addSubview:self.batchWrite];
                    [self.batchWrite mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.titleLabel.mas_bottom).with.mas_offset(10*(row+1)+row*height);
                        make.left.mas_equalTo(self.contentView).with.mas_offset(15*(col+1)+col*width);
                        make.width.mas_equalTo(width);
                        make.height.mas_equalTo(height);
                    }];
  
                } else {
                    UITextField *textField = [UITextField new];
                    textField.backgroundColor = BG_COLOR;
                    NSString *text = self.editArray[i];
                    textField.font = [UIFont qsh_systemFontOfSize:17];
                    textField.adjustsFontSizeToFitWidth = YES;
//                    [textField setValue:[UIFont systemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
                    if ([parser.require integerValue]==1) {
                        textField.placeholder = parser.name;
                    } else {
                        textField.placeholder = [NSString stringWithFormat:@"%@(可选)",parser.name];
                    }
                    NSString *holderText;
                    if ([parser.require integerValue]==1) {
                        holderText = parser.name;
                    } else {
                        
                        holderText = [NSString stringWithFormat:@"%@(可选)",parser.name];
                    }
                    NSMutableAttributedString *placeholder = [[NSMutableAttributedString
                                                               alloc] initWithString:holderText];
                    
                    [placeholder
                     addAttribute:NSFontAttributeName
                     value:mainWidth>375?[UIFont
                                          qsh_systemFontOfSize:14]:[UIFont
                                                                    qsh_systemFontOfSize:12]
                     range:NSMakeRange(0,
                                       holderText.length)];
                    
                    textField.attributedPlaceholder = placeholder;
                    textField.returnKeyType = UIReturnKeyDone;
                    textField.textColor = [UIColor redColor];
                    textField.tag = 100+i;
                    if (text) {
                        textField.text = text;
                    }
                    if ([parser.type isEqualToString:@"string"]) {
                        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                    } else if ([parser.type isEqualToString:@"float"]) {
                        textField.keyboardType = UIKeyboardTypeDecimalPad;
                        
                    }
                    textField.textAlignment = NSTextAlignmentCenter;
                    [self.contentView addSubview:textField];
                    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.titleLabel.mas_bottom).with.mas_offset(10*(row+1)+row*height);
                        make.left.mas_equalTo(self.contentView).with.mas_offset(15*(col+1)+col*width);
                        make.width.mas_equalTo(width);
                        make.height.mas_equalTo(height);
                    }];

                
                }
            }  
        }
    }
    NSUInteger row = _fieldArray.count%3==0?_fieldArray.count/3:_fieldArray.count/3+1;
    if (self.cellType ==PriceFieldTableViewCellTypeBatch) {
        row = listAray.count%3==0?listAray.count/3:listAray.count/3+1;
    }
    self.cellHeight = 1+40+(row+1)*10+row*height;
    
}

+(NSString *)idString {
    return @"BusinessPriceFieldTableViewCell";
}
@end
