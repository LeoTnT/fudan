//
//  RecommendView.m
//  App3.0
//
//  Created by nilin on 2017/9/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RecommendView.h"

@implementation RecommendView

-(void)setTitleArray:(NSArray *)titleArray {
    _titleArray = titleArray;

    self.backgroundColor = [UIColor whiteColor];
    UILabel *recommendLabel = [UILabel new];
    recommendLabel.text = @"推荐";
    recommendLabel.textColor = [UIColor blackColor];
    recommendLabel.font = [UIFont qsh_systemFontOfSize:16];
    [self addSubview:recommendLabel];
    [recommendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).with.mas_offset(13);
        make.right.mas_equalTo(self).with.mas_equalTo(-13);
        make.top.mas_equalTo(self);
        make.height.mas_equalTo(40);
    }];
    CGFloat width = (mainWidth-10*5)/4,height = 30;
    for (int i=0; i<titleArray.count; i++) {
        int row = i/4;
        int col = i%4;
        
        @autoreleasepool {
            UIButton *titleButton = [UIButton new];
            [titleButton removeTarget:self action:@selector(titleAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:titleButton];
            [titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(recommendLabel.mas_bottom).with.mas_offset((row+1)*10+row*height);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.left.mas_equalTo(10*(col+1)+col*width);
            }];
            [titleButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [titleButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
            [titleButton setTitle:_titleArray[i] forState:UIControlStateNormal];
            [titleButton setTitle:_titleArray[i] forState:UIControlStateSelected];
            [titleButton addTarget:self action:@selector(titleAction:) forControlEvents:UIControlEventTouchUpInside];
            titleButton.layer.cornerRadius = 5;
            titleButton.layer.masksToBounds = YES;
            titleButton.layer.borderColor = LINE_COLOR.CGColor;
            titleButton.layer.borderWidth = 0.5;
            titleButton.backgroundColor = [UIColor whiteColor];
           
        }

    }
    NSUInteger row = _titleArray.count/4;
    self.viewHeight = 40;
    self.viewHeight += _titleArray.count%4==0?(row*height+(10*(row+1))):((row+1)*height+10*(row+2));

}

- (void)titleAction:(UIButton *) sender {
    if ([self.viewDelegate respondsToSelector:@selector(searchCategoryWithTitle:)]) {
        [self.viewDelegate searchCategoryWithTitle:sender.titleLabel.text];
    }
}

@end
