//
//  TradeView.h
//  App3.0
//
//  Created by nilin on 2017/5/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@protocol  TradeViewDelegate<NSObject>

-(void)selectedTradeWithTradeParser:(BusinessApplyIndustryParser *) industryParser;
-(void)closeTradeView;
@end

@interface TradeView : UIView
/**行业数组*/
@property(nonatomic,strong) NSArray *tradeArray;
/**代理*/
@property(nonatomic,weak) id<TradeViewDelegate> delegate ;
@end
