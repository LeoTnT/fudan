//
//  RecommendView.h
//  App3.0
//
//  Created by nilin on 2017/9/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  RecommendViewDelegate<NSObject>

-(void)searchCategoryWithTitle:(NSString *) title;
@end

@interface RecommendView : UIView
@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, weak) id<RecommendViewDelegate> viewDelegate;
@property (nonatomic, strong) NSArray *titleArray;


@end
