//
//  GoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"
typedef NS_ENUM(NSInteger,BusinessGoodsItemType) {
    BusinessGoodsItemTypeNormal,
    BusinessGoodsItemTypeChecking,
    BusinessGoodsItemTypeUnsold,
};

@protocol BusinessGoodsDelegate <NSObject>

- (void) editProductWithProductParser:(BusinessProductDetailParser *) productParser;

- (void) deleteProductWithId:(NSString *) productId;

- (void) shareProductWithId:(NSString *) productId;
@end


@interface BusinessGoodsTableViewCell : UITableViewCell
/**图片*/
@property (nonatomic, strong) UIImageView *icon;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**价格*/
@property (nonatomic, strong) UILabel *priceLabel;
/**已售*/
@property (nonatomic, strong) UILabel *soldLabel;
/**库存*/
@property (nonatomic, strong) UILabel *stockLabel;
/**时间*/
@property (nonatomic, strong) UILabel *timeLabel;
/**编辑*/
@property (nonatomic, strong) UIButton *editButton;
/**分享*/
@property (nonatomic, strong) UIButton *enjoyButton;
/**删除*/
@property (nonatomic, strong) UIButton *deleteButton;
/**高度*/
@property (nonatomic, assign) CGFloat height;

/**类型*/
@property (nonatomic, assign) BusinessGoodsItemType itemType;
@property (nonatomic, strong) BusinessProductDetailParser *detailParser;

@property (nonatomic, weak) id<BusinessGoodsDelegate> delegate;


+(NSString *)idString;
@end
