//
//  BusinessNormalTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessNormalTableViewCell.h"
#import "AreaButton.h"
@implementation BusinessNormalTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = BG_COLOR;
         self.selectionStyle = UITableViewCellSelectionStyleNone;
       
    }
    return self;
}

-(void)setMenuArray:(NSArray *)menuArray {
    _menuArray = menuArray;
    CGFloat width = (mainWidth-4*NORMOL_SPACE)/3;
    for (int i=0; i<_menuArray.count; i++) {
        int row = i/3;
        int column = i%3;
        AreaButton *button = [[AreaButton alloc] initWithFrame:CGRectMake((width+NORMOL_SPACE)*column+NORMOL_SPACE, row*(width*1.2+NORMOL_SPACE)+NORMOL_SPACE, width, width*1.2) Model:_menuArray[i] Scale:1.0/3.0 fontSize:17];
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = YES;
        [self.contentView addSubview:button];
        button.backgroundColor = [UIColor whiteColor];
        button.tag = i+1000;
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    self.height = _menuArray.count%3==0?_menuArray.count/3*(width*1.2+NORMOL_SPACE)+NORMOL_SPACE:(_menuArray.count/3+1)*(width*1.2+NORMOL_SPACE)+NORMOL_SPACE*3;

}

+ (NSString *) idString {
    return @"nCell";
}

- (void)buttonAction:(UIButton *) sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(buttonActionWithIndex:)]) {
        [self.delegate buttonActionWithIndex:sender.tag];
    }
}
@end
