//
//  GoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/6/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessGoodsTableViewCell.h"
#import "UIImage+XSWebImage.h"
#import "XSFormatterDate.h"

@interface BusinessGoodsTableViewCell ()
@property (nonatomic, strong) UIView *buttonView;

@end

@implementation BusinessGoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle =UITableViewCellSelectionStyleNone;
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
        tempLabel.backgroundColor = BG_COLOR;
        [self.contentView addSubview:tempLabel];
        CGFloat imagesize = 70,width = (mainWidth-imagesize-3*NORMOL_SPACE)/3 ;
        self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE+CGRectGetMaxY(tempLabel.frame), imagesize, imagesize)];
        [self.contentView addSubview:self.icon];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+NORMOL_SPACE, CGRectGetMinY(self.icon.frame), mainWidth-imagesize-3*NORMOL_SPACE, 20)];
        [self.contentView addSubview:self.titleLabel];
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), CGRectGetWidth(self.titleLabel.frame), 30)];
        self.priceLabel.textColor = [UIColor redColor];
        self.priceLabel.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:self.priceLabel];
        self.soldLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.priceLabel.frame), width, 20)];
        self.soldLabel.font = [UIFont systemFontOfSize:13];
        self.soldLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:self.soldLabel];
        self.stockLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.soldLabel.frame), CGRectGetMaxY(self.priceLabel.frame), width, 20)];
        self.stockLabel.font = [UIFont systemFontOfSize:13];
        self.stockLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:self.stockLabel];
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.stockLabel.frame), CGRectGetMaxY(self.priceLabel.frame), width, 20)];
        self.timeLabel.font = [UIFont systemFontOfSize:13];
        self.timeLabel.textColor = [UIColor grayColor];
        self.timeLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.timeLabel];
        self.buttonView  = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.icon.frame)+NORMOL_SPACE, mainWidth, 40)];
        self.buttonView.backgroundColor = [UIColor hexFloatColor:@"FAFAFA"];
        [self.contentView addSubview:self.buttonView];
        self.editButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth/3, 40)];
        [self.editButton setTitle:@"编辑宝贝" forState:UIControlStateNormal];
        [self.editButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.editButton setImage:[UIImage imageNamed:@"user_site_edit_gray"] forState:UIControlStateNormal];
        self.editButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
        [self.buttonView addSubview:self.editButton];
        UILabel *line1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.editButton.frame)-0.5, CGRectGetMinY(self.editButton.frame)+10, 0.5, CGRectGetHeight(self.editButton.frame)-20)];
        line1.backgroundColor = LINE_COLOR;
        [self.buttonView addSubview:line1];
        self.enjoyButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.editButton.frame), 0, mainWidth/3, 40)];
        [self.enjoyButton setTitle:Localized(@"share") forState:UIControlStateNormal];
        [self.enjoyButton setImage:[UIImage imageNamed:@"user_collect_share"] forState:UIControlStateNormal];
        [self.enjoyButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.enjoyButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
        
        [self.buttonView addSubview:self.enjoyButton];
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.enjoyButton.frame)-0.5, CGRectGetMinY(self.enjoyButton.frame)+10, 0.5, CGRectGetHeight(self.enjoyButton.frame)-20)];
        line2.backgroundColor = LINE_COLOR;
        [self.buttonView addSubview:line2];
        self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.enjoyButton.frame), 0, mainWidth/3, 40)];
        [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [self.deleteButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.deleteButton setImage:[UIImage imageNamed:@"user_site_delete_gray"] forState:UIControlStateNormal];
        self.deleteButton.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
        [self.buttonView addSubview:self.deleteButton];
        UILabel *line3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.deleteButton.frame)-0.5, CGRectGetMinY(self.deleteButton.frame), 0.5, CGRectGetHeight(self.deleteButton.frame))];
        line3.backgroundColor = LINE_COLOR;
        [self.buttonView addSubview:line3];
        self.height = CGRectGetMaxY(self.buttonView.frame);
        
    }
    return self;
}

- (void)setDetailParser:(BusinessProductDetailParser *)detailParser {
    _detailParser = detailParser;
    [self.icon getImageWithUrlStr:_detailParser.image andDefaultImage:[UIImage imageNamed:@"fuDan_logo"]];
    self.titleLabel.text = _detailParser.product_name;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",_detailParser.sell_price];
    //    _detailParser.stock_num
    self.soldLabel.text = [NSString stringWithFormat:@"已售%@",_detailParser.sell_num];
    self.stockLabel.text = [NSString stringWithFormat:@"库存%@",_detailParser.stock_num];
    self.timeLabel.text = [XSFormatterDate yearDateWithTimeIntervalString:_detailParser.w_time];
    if (self.itemType==BusinessGoodsItemTypeUnsold) {
        [self.enjoyButton setImage:nil forState:UIControlStateNormal];
        [self.enjoyButton setTitle:Localized(@"has_shelf") forState:UIControlStateNormal];
        [self.editButton addTarget:self  action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    } else if (self.itemType==BusinessGoodsItemTypeChecking) {
        //        [self.enjoyButton setImage:nil forState:UIControlStateNormal];
        //        [self.enjoyButton setTitle:@"待审核" forState:UIControlStateNormal];
        self.buttonView.hidden = YES;
        self.height = CGRectGetMaxY(self.timeLabel.frame)+1;
        
    } else {
        [self.editButton addTarget:self  action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.enjoyButton addTarget:self action:@selector(enjoyAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)editAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(editProductWithProductParser:)]) {
        [self.delegate editProductWithProductParser:self.detailParser];
    }
}

- (void)enjoyAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(shareProductWithId:)]) {
        [self.delegate shareProductWithId:self.detailParser.product_id];
    }
}

- (void)deleteAction:(UIButton *) sender {
    if ([self.delegate respondsToSelector:@selector(deleteProductWithId:)]) {
        [self.delegate deleteProductWithId:self.detailParser.product_id];
    }
}

+(NSString *)idString {
    return @"businessGoods";
}
@end
