//
//  BusinessTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessTopTableViewCell : UITableViewCell

@property (nonatomic, strong) UIButton *qrButton;
/**头像*/
@property (nonatomic, strong) UIImageView *icon;
/**昵称*/
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) NSArray *titleArray;

@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *name;
+ (NSString *) idString;
@end
