//
//  BusinessSpecTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessSpecTopTableViewCell.h"

@interface BusinessSpecTopTableViewCell()
@property (nonatomic, strong) NSMutableArray *listArray;
@end

@implementation BusinessSpecTopTableViewCell

-(NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = BG_COLOR;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.titleLabel = [UILabel new];
        self.titleLabel.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLabel];
        self.areaview = [LogisticAreaView new];
        self.areaview.backgroundColor = [UIColor whiteColor];
        self.areaview.areaViewType = AreaViewTypeSpec;
        [self.contentView addSubview:self.areaview];
        self.upOrDownButton = [UIButton new];
        self.upOrDownButton.backgroundColor = [UIColor whiteColor];
        [self.upOrDownButton setImage:[UIImage imageNamed:@"business_goods_spec_down"] forState:UIControlStateNormal];
        [self.upOrDownButton setImage:[UIImage imageNamed:@"business_goods_spec_up"] forState:UIControlStateSelected];
        [self.upOrDownButton addTarget:self action:@selector(upOrDownView:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:self.upOrDownButton];
        self.cellHeight = 50*2+10*2;
        
        
    } 
    return self;
}

-(void)setSpecParser:(BusinessSpecListParser *)specParser {
    _specParser = specParser;
    
    self.titleLabel.text = [NSString stringWithFormat:@"   %@",_specParser.name];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.mas_offset(10);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    
     self.areaview.selectedArray = self.selectedArray;
//    for (int i=0; i<_specParser.child.count; i++) {
//        BusinessSpecDetailParser *parser = _specParser.child[i];
//        [self.listArray addObject:parser.name];
//    }
    [self.listArray addObjectsFromArray:_specParser.child];
    if (self.listArray.count) {
        if (self.listArray.count<7) {
            self.areaview.areaListArray = self.listArray;
        } else  {
            self.areaview.areaListArray = [self.listArray subarrayWithRange:NSMakeRange(0, 6)];
        }
        self.cellHeight =50*2+10*2+self.areaview.viewHeight;
    }
    [self.upOrDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(50);
        make.left.right.mas_equalTo(self.contentView);
    }];
    
    [self.areaview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom);
        make.bottom.mas_equalTo(self.upOrDownButton.mas_top);
        make.left.right.mas_equalTo(self.contentView);
    }];
//    self.areaview.backgroundColor = [UIColor yellowColor];
   
}

- (void)upOrDownView:(UIButton *) sender {
    sender.selected = !sender.selected;
    self.areaview.selectedArray = self.selectedArray;
    if (sender.selected) {
        self.areaview.areaListArray = self.listArray;
        self.cellHeight = 50*2+self.areaview.viewHeight+10;
    } else {
        if (self.listArray.count<7) {
            self.areaview.areaListArray = self.listArray;
        } else  {
            self.areaview.areaListArray = [self.listArray subarrayWithRange:NSMakeRange(0, 6)];
        }
        
        self.cellHeight = 50*2+10*2+self.areaview.viewHeight;
    }
    if ([self.delegate respondsToSelector:@selector(changeHeight: isOpen:)]) {
        [self.delegate changeHeight:self.specParser isOpen:sender.selected];
    }
    NSLog(@"change=====%f",self.cellHeight);
}

+(NSString *)idString {
    return @"BusinessSpecTopTableViewCell";
}

@end
