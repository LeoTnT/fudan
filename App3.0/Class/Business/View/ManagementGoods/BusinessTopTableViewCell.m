//
//  BusinessTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessTopTableViewCell.h"
#import "UIImage+XSWebImage.h"
@interface BusinessTopTableViewCell ()
@property (nonatomic, strong) UIView *bottomView;
@end
@implementation BusinessTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = mainColor;
         self.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat imageSize = 80;
        CGFloat titleHeight = 40,height = 260;
        self.qrButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 20, 30, 30)];
        [self.qrButton setImage:[UIImage imageNamed:@"user_top_qr"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.qrButton];
        self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth/2-imageSize/2, height/2-imageSize/2-50, imageSize, imageSize)];
        self.icon.image = [UIImage imageNamed:@"fuDan_logo"];
        self.icon.layer.cornerRadius = imageSize/2;
        self.icon.layer.borderColor = [UIColor whiteColor].CGColor;
        self.icon.layer.borderWidth = 1.5;
        self.icon.layer.masksToBounds = YES;
        [self.contentView addSubview:self.icon];
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.icon.frame), mainWidth, titleHeight)];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.font = [UIFont qsh_systemFontOfSize:23];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.text = @"逆鳞";
        [self.contentView addSubview:self.nameLabel];
        self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, height-80, mainWidth, 80)];
        self.bottomView.backgroundColor = BG_COLOR;
        [self.contentView addSubview: self.bottomView];
    }
    return self;
}

- (void)setTitleArray:(NSArray *)titleArray {
    _titleArray = titleArray;
    for (UIView *view in self.bottomView.subviews) {
        [view removeFromSuperview];
    }
    CGFloat width = (mainWidth-2*NORMOL_SPACE)/3;
    for (int i=0; i<_titleArray.count; i++) {
        @autoreleasepool {
            NSDictionary *dic = _titleArray[i];
            UILabel *tintlabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE+width*i, NORMOL_SPACE,width, (CGRectGetHeight( self.bottomView.frame)-NORMOL_SPACE*2)/2)];
            tintlabel.backgroundColor = [UIColor whiteColor];
            tintlabel.text = [dic.allKeys firstObject];
            tintlabel.textAlignment = NSTextAlignmentCenter;
            [ self.bottomView addSubview:tintlabel];
            UILabel *moneyabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(tintlabel.frame), CGRectGetMaxY(tintlabel.frame),width , CGRectGetHeight(tintlabel.frame))];
            moneyabel.text = [dic.allValues firstObject];
            moneyabel.backgroundColor = [UIColor whiteColor];
            moneyabel.textAlignment = NSTextAlignmentCenter;
            [ self.bottomView addSubview:moneyabel];
            if (i<2) {
                UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tintlabel.frame)-0.5, NORMOL_SPACE*2, 0.5,  CGRectGetHeight( self.bottomView.frame)-4*NORMOL_SPACE)];
                line.backgroundColor = LINE_COLOR_NORMAL;
                [ self.bottomView addSubview:line];
            }  
        }
        
    }

    
}

-(void)setIconUrl:(NSString *)iconUrl {
    _iconUrl = iconUrl;
    [self.icon getImageWithUrlStr:_iconUrl andDefaultImage:[UIImage imageNamed:@"fuDan_logo"]];
}

-(void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = _name;
}

+ (NSString *) idString {
    return @"topCell";
}

@end
