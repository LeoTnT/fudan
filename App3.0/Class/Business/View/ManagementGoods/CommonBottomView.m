//
//  CommonButtomView.m
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CommonBottomView.h"

@implementation CommonBottomView

+(instancetype)CommonBottomViewWithTitleArray:(NSArray *)titleArray imageArray:(NSArray *)imageArray viewHeight:(CGFloat)viewHeight viewWidth:(CGFloat)viewWidth viewTop:(CGFloat) viewTop{
    CommonBottomView *commonView = [CommonBottomView new];
        UILabel *topLine = [UILabel new];
        topLine.backgroundColor = LINE_COLOR;
        [commonView addSubview:topLine];
        commonView.leftButton = [UIButton new];
        [commonView.leftButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [commonView.leftButton setTitle:[titleArray firstObject] forState:UIControlStateNormal];
        [commonView.leftButton setImage:[UIImage imageNamed:[imageArray firstObject]] forState:UIControlStateNormal];
        commonView.leftButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [commonView.leftButton addTarget:self action:@selector(clickLeftButton) forControlEvents:UIControlEventTouchUpInside];
        [commonView addSubview: commonView.leftButton];
        commonView.rightButton = [UIButton new];
        [commonView.rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [commonView.rightButton setTitle:[titleArray lastObject] forState:UIControlStateNormal];
        [commonView.rightButton addTarget:self action:@selector(clickRightButton) forControlEvents:UIControlEventTouchUpInside];
        [commonView.rightButton setImage:[UIImage imageNamed:[imageArray lastObject]] forState:UIControlStateNormal];
        commonView.rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [commonView addSubview:commonView.rightButton];
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR;
        [commonView addSubview:line];
        
        [commonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(viewTop);
            make.width.mas_equalTo(viewWidth);
            make.height.mas_equalTo(viewHeight);
        }];
        
        [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(commonView);
            make.height.mas_equalTo(0.5);
        }];
        
        [commonView.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topLine.mas_bottom);
            make.left.mas_equalTo(topLine.mas_left);
            make.width.mas_equalTo(viewWidth/2-0.5);
            make.height.mas_equalTo(viewHeight-0.5);
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(commonView.mas_right);
            make.width.mas_equalTo(1);
            make.top.mas_equalTo(commonView.leftButton.mas_top).with.mas_offset(10);
            make.bottom.mas_equalTo(commonView.leftButton.mas_bottom).with.mas_offset(-10);
        }];
        
        [commonView.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.width.mas_equalTo(commonView.leftButton);
            make.left.mas_equalTo(line.mas_right);
        }];
    return commonView;
}

- (void)clickLeftButton {
    if ([self.delegate respondsToSelector:@selector(leftButtonAction)]) {
        [self.delegate leftButtonAction];
    }
    
}

- (void)clickRightButton {
    if ([self.delegate respondsToSelector:@selector(rightButtonAction)]) {
        [self.delegate rightButtonAction];
    }
}
@end
