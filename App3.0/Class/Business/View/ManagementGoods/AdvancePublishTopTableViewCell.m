//
//  AdvancePublishTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AdvancePublishTopTableViewCell.h"

@interface AdvancePublishTopTableViewCell()

@property (nonatomic, strong) UILabel *mainImageLabel;//主图

@end

@implementation AdvancePublishTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.bigImageView = [UIImageView new];
        self.bigImageView.backgroundColor = [UIColor hexFloatColor:@"efeff4"];
        [self.contentView addSubview:self.bigImageView];
        self.photoButton = [UIButton new];
        [self.photoButton setImage:[UIImage imageNamed:@"business_publish_advance"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.photoButton];
        
        self.mainImageLabel = [UILabel new];
        self.mainImageLabel.backgroundColor = [UIColor redColor];
        self.mainImageLabel.textColor = [UIColor whiteColor];
        self.mainImageLabel.text = @"主图";
        self.mainImageLabel.font = [UIFont qsh_systemFontOfSize:17];
        self.mainImageLabel.textAlignment = NSTextAlignmentCenter;
        self.mainImageView = [UIImageView new];
    }
    return self;
    
}

#pragma mark-刷新界面
-(void)setPhotosArray:(NSArray *)photosArray{
    _photosArray=photosArray;
    self.deletBtnArray=[NSMutableArray array];
    self.imagesArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]||[view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    [self.contentView addSubview:self.bigImageView];
    [self.contentView addSubview:self.photoButton];
    [self.bigImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(self.contentView);
    }];
    
    [self.photoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bigImageView).with.mas_offset(60);
        make.height.width.mas_equalTo(65);
        make.left.mas_equalTo(mainWidth/2-32.5);
    }];
    
    UIImageView *img;
    if (_photosArray.count==0) {
        return;
    }
    if (!self.mainImageView.image) {
        if (_photosArray.count>1) {
            self.mainImageView.image = [_photosArray firstObject];
        }
        
    }
    
    CGFloat imageWidth = (mainWidth-60)/5;
    for (int i=0; i<_photosArray.count; i++) {
        int columns=5;
        //列数
        int col=i%columns;
        //行数
        int row=i/columns;
        @autoreleasepool {
            UIImageView *imgView = [UIImageView new];
            imgView.image=[_photosArray objectAtIndex:i];
            img=imgView;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
            imgView.userInteractionEnabled=YES;
            [imgView addGestureRecognizer:tap];
            [self.contentView addSubview:imgView];
            
            
            [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10*(col+1)+col*imageWidth);
                make.top.mas_equalTo(self.photoButton.mas_bottom).with.mas_offset(40+10*(row+1)+row*imageWidth);
                make.height.width.mas_equalTo(imageWidth);
            }];
            if ([imgView.image isEqual:self.mainImageView.image]) {
                [imgView addSubview:self.mainImageLabel];
                //显示主图
                [self.mainImageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(imgView);
                    make.left.mas_equalTo(imgView);
                    make.height.mas_equalTo(20);
                    make.width.mas_equalTo(imgView.mas_width);
                    
                }];
                [self.bigImageView setImage:imgView.image];
            }
            //添加删除按钮×
            if (![imgView.image isEqual:[UIImage imageNamed:@"user_fans_addphoto_thin"]]) {
                UIButton *deleteBtn=[UIButton new];
                deleteBtn.backgroundColor = [UIColor whiteColor];
                [deleteBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
                deleteBtn.layer.cornerRadius = 10;
                deleteBtn.layer.masksToBounds = YES;
                [self.contentView addSubview:deleteBtn];
                [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(imgView.mas_right).with.mas_offset(5);
                    make.top.mas_equalTo(imgView.mas_top).with.mas_offset(-10);
                    make.height.width.mas_equalTo(20);
                }];
                [self.deletBtnArray addObject:deleteBtn];
            }
            
        }
    }
    self.lastImage=img;
    self.lastImage.userInteractionEnabled=YES;
    CGFloat totalHeight = _photosArray.count%5==0?_photosArray.count/5*(imageWidth+10):(_photosArray.count/5+1)*(imageWidth+10);
    
    self.height=60+65+40+totalHeight+10;
}


- (void) tapAction:(UITapGestureRecognizer *) tap {
    UIImageView *imgView = (UIImageView *)tap.view;
    self.mainImageView = imgView;
    [imgView addSubview:self.mainImageLabel];
    //显示主图
    [self.mainImageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(imgView);
        make.left.mas_equalTo(imgView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(imgView.mas_width);
        
    }];
    [self.bigImageView setImage:imgView.image];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    if ([self.delegate respondsToSelector:@selector(mainImageWithImage:)]) {
        [self.delegate mainImageWithImage:imgView.image];
    }
}
@end
