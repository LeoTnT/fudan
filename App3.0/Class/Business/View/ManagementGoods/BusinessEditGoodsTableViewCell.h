//
//  BusinessEditGoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/6/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@protocol BusinessEditDelegate <NSObject>

- (void) deleteProductWithId:(NSString *) productId sure:(BOOL) isSure;

@end

@interface BusinessEditGoodsTableViewCell : UITableViewCell
/**选择按钮*/
@property (nonatomic, strong) UIButton *selectedButton;
/**图片*/
@property (nonatomic, strong) UIImageView *icon;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**价格*/
@property (nonatomic, strong) UILabel *priceLabel;
/**已售*/
@property (nonatomic, strong) UILabel *soldLabel;
/**库存*/
@property (nonatomic, strong) UILabel *stockLabel;
/**高度*/
@property (nonatomic, assign) CGFloat height;

@property (nonatomic, strong) BusinessProductDetailParser *detailParser;

@property (nonatomic,weak) id<BusinessEditDelegate> delegate;

-(void)clickSelectedButtonAction:(UIButton *)sender;
+(NSString *)idString;
@end
