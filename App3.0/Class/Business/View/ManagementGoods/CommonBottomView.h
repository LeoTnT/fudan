//
//  CommonButtomView.h
//  App3.0
//
//  Created by nilin on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSCustomButton.h"

@protocol CommonBottomDelegate <NSObject>

@optional
- (void)leftButtonAction;

@optional
- (void)rightButtonAction;

@end

@interface CommonBottomView : UIView

@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;

@property (nonatomic, weak) id<CommonBottomDelegate> delegate;


+ (instancetype)CommonBottomViewWithTitleArray:(NSArray *) titleArray imageArray:(NSArray *) imageArray viewHeight:(CGFloat ) viewHeight viewWidth:(CGFloat )viewWidth viewTop:(CGFloat ) viewTop;
@end
