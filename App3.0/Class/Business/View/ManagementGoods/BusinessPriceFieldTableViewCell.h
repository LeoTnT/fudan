//
//  BusinessPriceFieldTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessClassifyModel.h"
typedef NS_ENUM(NSInteger,PriceFieldTableViewCellType) {
PriceFieldTableViewCellTypeNormal,
    PriceFieldTableViewCellTypeBatch,
};

@interface BusinessPriceFieldTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSArray *editArray;
@property (nonatomic, strong) NSArray *fieldArray;
@property (nonatomic, assign) CGFloat cellHeight;

/**批量填写按钮*/
@property (nonatomic, strong) UIButton *batchWrite;

/**条形码扫描*/
@property (nonatomic, strong) UIButton *scanButton;
@property (nonatomic,assign) PriceFieldTableViewCellType cellType;

+(NSString *) idString;
@end
