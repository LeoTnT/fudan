//
//  BusinessNormalTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BusinessNormalDelegate <NSObject>

@optional
- (void)buttonActionWithIndex:(NSUInteger ) index;

@end

@interface BusinessNormalTableViewCell : UITableViewCell

/**图片，标题数组*/
@property (nonatomic, strong) NSArray *menuArray;
/**代理*/
@property (nonatomic, weak) id<BusinessNormalDelegate> delegate;
/**高度*/
@property (nonatomic, assign) CGFloat height;

+ (NSString *) idString;
@end
