//
//  LogisticAreaCenterTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticAreaCenterTableViewCell.h"

@interface LogisticAreaCenterTableViewCell()
@property (nonatomic, strong)  UIView *backGroundView;
@property (nonatomic, strong)  UILabel *line;



@end

@implementation LogisticAreaCenterTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backGroundView = [UIView new];
        self.backGroundView.backgroundColor = [UIColor whiteColor];
     
        [self.contentView addSubview:self.backGroundView];
        self.areaButton = [UIButton new];
        [self.areaButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.areaButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self.areaButton setTitle:@"山东" forState:UIControlStateNormal];
        [self.areaButton setTitle:@"山东" forState:UIControlStateSelected];
        [self.areaButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [self.areaButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        self.areaButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.areaButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        //        self.areaButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);//
        self.areaButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        [self.areaButton addTarget:self action:@selector(areaButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.backGroundView addSubview:self.areaButton];
        
        self.upOrDownButton = [UIButton new];
        [self.upOrDownButton setImage:[UIImage imageNamed:@"business_logiatic_down"] forState:UIControlStateNormal];
        [self.upOrDownButton setImage:[UIImage imageNamed:@"business_logiatic_up"] forState:UIControlStateSelected];
        [self.upOrDownButton addTarget:self action:@selector(upOrDownAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.backGroundView addSubview:self.upOrDownButton];
       
        self.cellHeight = 50;
        
        [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView);
            make.height.mas_equalTo(self.cellHeight);
            make.right.mas_equalTo(self.contentView);
        }];
        
        //小图标
        [self.upOrDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.backGroundView).with.mas_offset(-10);
            make.width.mas_equalTo(50);
            make.top.mas_equalTo(self.backGroundView);
            make.height.mas_equalTo(self.cellHeight);
        }];
        
        [self.areaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.backGroundView);
            make.left.mas_equalTo(self.backGroundView).with.mas_offset(15);
            make.bottom.mas_equalTo(self.backGroundView);
            make.width.mas_equalTo(100);
        }];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openOrCloseAreaView:)];
//        self.backGroundView.userInteractionEnabled = YES;
//        [self.backGroundView addGestureRecognizer:tap];
        self.line = [UILabel new];
         self.line.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview: self.line];
       
        self.areaView = [LogisticAreaView new];
        self.areaView.areaViewType = AreaViewTypeCity;
        self.areaView.selectedArray = @[];
        self.areaView.areaListArray = @[];
        [self.contentView addSubview:self.areaView];
         self.cellHeight = 50;
    }
    return self;
}
-(void)setAreaParser:(LogisticAreaDetailParser *)areaParser {
    _areaParser = areaParser;
    [self.areaButton setTitle:_areaParser.name forState:UIControlStateNormal];
    [self.areaButton setTitle:_areaParser.name forState:UIControlStateSelected];
    
}

-(void)setAreaShowType:(AreaShowType)areaShowType {
    _areaShowType = areaShowType;
     self.areaView.selectedArray = self.selectedArray;
    if (_areaShowType==AreaShowTypeClose) {
        
        [self.backGroundView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(50);
        }];
         self.areaView.areaListArray = @[];
        [self.areaView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.backGroundView.mas_bottom);
            make.height.mas_equalTo(self.areaView.viewHeight);
            make.left.right.mas_equalTo(self.contentView);
        }];

        [self setNeedsLayout];
        [self layoutIfNeeded];
        
        self.cellHeight = 50;
       
        
    } else {
        self.cellHeight = 50+self.areaView.viewHeight;
        self.areaView.areaListArray = self.areaParser.child;
        [self.backGroundView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(50);
        }];
        [self.areaView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.backGroundView.mas_bottom);
            make.height.mas_equalTo(self.areaView.viewHeight);
            make.left.right.mas_equalTo(self.contentView);
        }];
        
        self.cellHeight = 50+self.areaView.viewHeight;
        [self setNeedsLayout];
        [self layoutIfNeeded];
        
    }
    [ self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.areaView.mas_bottom).with.mas_offset(-0.5);
        make.height.mas_equalTo(0.5);
    }];
 
}

- (void)upOrDownAction:(UIButton *) button {
    button.selected = !button.selected;
   
    if ([self.delegate respondsToSelector:@selector(openOrCloseAreaViewWithParentName: open:)]) {
        [self.delegate openOrCloseAreaViewWithParentName:self.areaParser.name open:button.selected];
    }
//    if (self.areaButton.selected) {
//        [self areaButton:self.areaButton];
//    }
}

- (void)openOrCloseAreaView:(UITapGestureRecognizer *) tap {
    [self upOrDownAction:self.upOrDownButton];
}

- (void)areaButton:(UIButton *) sender {
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(selectedAreaWithAreaName: selected:)]) {
        [self.delegate selectedAreaWithAreaName:self.areaParser.name selected:sender.selected];
    }
    if (self.areaView) {
        for (UIView *view in self.areaView.subviews) {
            if ([view isKindOfClass:[UIButton class]]) {
                
                if (((UIButton *)view).selected!=sender.selected) {
                     [self.areaView areaAction:(UIButton *)view];
                }
               
            }
            
        }
    }
    
}

+ (NSString *)idString {
    return @"LogisticAreaCenterTableViewCell";
}

@end
