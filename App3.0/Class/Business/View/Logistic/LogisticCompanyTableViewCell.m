//
//  LogisticCompanyTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticCompanyTableViewCell.h"

@implementation LogisticCompanyTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.companyButton = [UIButton new];
        [self.companyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.companyButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self.companyButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [self.companyButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        self.companyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.companyButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        self.companyButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);//
        self.companyButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        [self.companyButton addTarget:self action:@selector(companyButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.companyButton];
        [self.companyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(15);
            make.top.bottom.mas_equalTo(self.contentView);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        
    }
    return self;
}

- (void)setCompanyName:(NSString *)companyName {
    _companyName = companyName;
     [self.companyButton setTitle:companyName forState:UIControlStateNormal];
    [self.companyButton setTitle:companyName forState:UIControlStateSelected];
}

+ (NSString *)idString {
    return @"LogisticCompanyTableViewCell";
}

- (void)companyButton:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        
    }
}

@end
