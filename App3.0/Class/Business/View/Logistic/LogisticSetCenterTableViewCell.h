//
//  LogisticSetCenterTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LogisticSetCenterDelegate <NSObject>

- (void) selectArea:(NSUInteger ) index;
- (void) selectCompany;
//- (void) selectFirstWeight;
//- (void) selectSecondWeight;
//- (void) selectFillShopMoney;
//- (void) selectLogisticMoney;

@end

typedef NS_ENUM(NSInteger,RuleType) {
    RuleTypeWeight,
    RuleTypeMoney
};

@interface LogisticSetCenterTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *areaSelectedLabel;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UITextField *firstTextField;
@property (nonatomic, strong) UITextField *secondTextField;
@property (nonatomic, strong) UITextField *firstWeightTextField;//首重
@property (nonatomic, strong) UITextField *secondWeightTextField;//次重
@property (nonatomic, assign) RuleType ruleType;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id<LogisticSetCenterDelegate> delegate;

+(NSString *) idString;
@end
