
//
//  LogiaticAreaTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticAreaTopTableViewCell.h"
#import "LogisticAreaModel.h"

@implementation LogisticAreaTopTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.areaView = [LogisticAreaView new];
        self.areaView.areaViewType = AreaViewTypeRegion;
        [self.contentView addSubview:self.areaView];
        [self.areaView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(50);
        }];
        self.cellHeight = 50;
    }
    return self;
}

-(void)setAreaListArray:(NSArray *)areaListArray {
    _areaListArray = areaListArray;
    if (_areaListArray.count) {
        NSMutableArray *list = [NSMutableArray arrayWithObject:@"全国"];
        for (LogisticAreaListParser *parser in _areaListArray) {
            [list addObject:parser.name];
        }
        self.areaView.selectedArray = self.selectedArray;
        self.areaView.areaListArray = list;
        [self.areaView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.areaView.viewHeight);
        }];
        self.cellHeight = self.areaView.viewHeight;
    }
}


+ (NSString *)idString {
    return @"LogisticAreaTopTableViewCell";
}


@end
