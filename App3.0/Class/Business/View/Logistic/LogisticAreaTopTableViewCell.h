//
//  LogiaticAreaTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticAreaView.h"
#import "LogisticsModel.h"



@interface LogisticAreaTopTableViewCell : UITableViewCell

@property (nonatomic,strong) LogisticAreaView *areaView;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) NSArray *areaListArray;
@property (nonatomic, strong) NSArray *selectedArray;
+ (NSString *)idString;
@end
