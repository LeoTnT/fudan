//
//  ManageLogisticTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ManageLogisticTableViewCell.h"


@implementation ManageLogisticTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [self.contentView addSubview:self.titleLabel];
        self.descriptionLabel = [UILabel new];
        self.descriptionLabel.font = [UIFont qsh_systemFontOfSize:14];
        self.descriptionLabel.textColor = LINE_COLOR;
        [self.contentView addSubview:self.descriptionLabel];
        self.rightView = [UIView new];
        self.rightView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.rightView];
        UILabel *rowLine = [UILabel new];
        rowLine.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:rowLine];
        self.editButton = [UIButton new];
//        self.editButton.backgroundColor = [UIColor whiteColor];
//        [self.editButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [self.editButton setTitle:Localized(@"edit") forState:UIControlStateNormal];
        [self.editButton setImage:[UIImage imageNamed:@"user_site_edit_gray"] forState:UIControlStateNormal];
        [self.editButton setImage:[UIImage imageNamed:@"user_site_edit_gray"] forState:UIControlStateNormal];
        [self.editButton addTarget:self action:@selector(clickEditButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:self.editButton];
        self.deleteButton = [UIButton new];
//        self.deleteButton.backgroundColor = [UIColor whiteColor];
        [self.deleteButton setImage:[UIImage imageNamed:@"user_site_delete_gray"] forState:UIControlStateNormal];
        [self.deleteButton addTarget:self action:@selector(clickDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:self.deleteButton];
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self.rightView addSubview:line];
        
        
        
        CGFloat labelHeight = 30;// s 30*2+20+5 = 80
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.mas_equalTo(self.contentView).with.offset(10);
            make.height.mas_equalTo(labelHeight);
            make.right.mas_equalTo(mainWidth-15);
        }];
        [self.descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom).with.offset(5);
            make.left.height.mas_equalTo(self.titleLabel);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(mainWidth/4*3);
            make.top.mas_equalTo(self.titleLabel.mas_top);
            make.bottom.mas_equalTo(self.descriptionLabel.mas_bottom);
            make.right.mas_equalTo(self.contentView);
        }];
        [rowLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.descriptionLabel.mas_bottom).with.offset(9);
            make.width.left.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.rightView);
            make.width.mas_equalTo(mainWidth/4/2-0.25);
            make.left.mas_equalTo(self.rightView);
        }];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.editButton.mas_right);
            make.width.mas_equalTo(0.5);
            make.top.mas_equalTo(self.editButton.mas_top).with.offset(10);
            make.bottom.mas_equalTo(self.editButton.mas_bottom).with.offset(-10);
        }];
        [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(line.mas_right);
            make.top.width.height.mas_equalTo(self.editButton);
        }];
        
//        self.titleLabel.text = @"江苏的运费";
//        self.descriptionLabel.text = [NSString stringWithFormat:@"按运费计算：%@",@"江苏，山东，安慰，浙江，福建，广州"];
       
        self.cellHeight = labelHeight*2+20;
    }
    return self;
}

-(void)setListParser:(BusinessLogisticListParser *)listParser {
    _listParser = listParser;
    self.titleLabel.text = listParser.freight_name;
    NSMutableArray *nameArray = [NSMutableArray array];
    for (int i=0; i<listParser.ext.count; i++) {
       BusinessLogisticDetailParser *detailParser = listParser.ext[i];
        if (isEmptyString(detailParser.area_name)) {
            [nameArray addObject:@"全国"];
        } else {
           [nameArray addObject:detailParser.area_name];
            
        }
        
    }
    
    self.descriptionLabel.text = [nameArray componentsJoinedByString:@","];
}

-(void)setDetailParser:(BusinessClassifyDetailParser *)detailParser {
    _detailParser = detailParser;
    self.titleLabel.text = _detailParser.category_name;
//    self.descriptionLabel.text = [NSString stringWithFormat:@"共%@件商品",_detailParser.num];
}

- (void)clickEditButton:(UIButton *) sender {
    if (self.listParser) {
        if ([self.logisticDelegate respondsToSelector:@selector(editLogisticWithLogisticId:)]) {
            [self.logisticDelegate editLogisticWithLogisticId:self.listParser.ID];
        }
    }
    if (self.detailParser) {
        if ([self.logisticDelegate respondsToSelector:@selector(editClassifyWithClassifyId:)]) {
            [self.logisticDelegate editClassifyWithClassifyId:self.detailParser.category_id];
        }
    }

}

- (void)clickDeleteButton:(UIButton *) sender {
    if (self.listParser) {
        if ([self.logisticDelegate respondsToSelector:@selector(deleteLogisticWithLogisticId:)]) {
            [self.logisticDelegate deleteLogisticWithLogisticId:self.listParser.ID];
        }
    }
    if (self.detailParser) {
        if ([self.logisticDelegate respondsToSelector:@selector(deleteClassifyWithClassifyId:)]) {
            [self.logisticDelegate deleteClassifyWithClassifyId:self.detailParser.category_id];
        }
    }
   
}

@end
