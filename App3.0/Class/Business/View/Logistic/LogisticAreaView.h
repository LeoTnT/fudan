//
//  LogisticAreaView.h
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

@protocol LogisticAreaViewDelegate <NSObject>

@optional
- (void)bigAreaName:(NSString *) areaName  andIndex:(NSUInteger ) index selected:(BOOL) isSelected;

@optional
- (void)specParser:(BusinessSpecDetailParser *) specParser  selected:(BOOL) isSelected;


@end


typedef NS_ENUM(NSInteger,AreaViewType) {
    AreaViewTypeRegion,
    AreaViewTypeCity,
    AreaViewTypeSpec

};

@interface LogisticAreaView : UIView
@property (nonatomic, strong) NSArray *areaListArray;
@property (nonatomic, strong) NSArray *selectedArray;
@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, assign) AreaViewType areaViewType;

@property (nonatomic, weak) id<LogisticAreaViewDelegate> delegate;

- (void)areaAction:(UIButton *) sender;
@end
