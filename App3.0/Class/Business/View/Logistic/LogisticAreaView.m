//
//  LogisticAreaView.m
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticAreaView.h"
#import "LogisticAreaModel.h"

@interface LogisticAreaView()
@property (nonatomic, assign) NSUInteger indexNumber;
@end

@implementation LogisticAreaView

-(instancetype)initWithFrame:(CGRect)frame {
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = BG_COLOR;
    }
    return self;
}

-(void)setAreaListArray:(NSArray *)areaListArray {
    _areaListArray = areaListArray;
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    if (_areaListArray.count) {
        self.indexNumber = 0;
        if (self.areaViewType==AreaViewTypeRegion) {
            self.indexNumber = 200;
        } else {
            self.indexNumber = 500;
        }
        CGFloat width = (mainWidth-10*4)/3,height = 40;
        for (int i=0; i<_areaListArray.count; i++) {
            int row = i/3;
            int col = i%3;
            
            @autoreleasepool {
                UIButton *areaButton = [UIButton new];
                [areaButton removeTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:areaButton];
                areaButton.tag = self.indexNumber;
                [areaButton mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo((row+1)*10+row*height);
                    make.width.mas_equalTo(width);
                    make.height.mas_equalTo(height);
                    make.left.mas_equalTo(10*(col+1)+col*width);
                }];
                areaButton.layer.cornerRadius = 5;
                areaButton.layer.masksToBounds = YES;
                areaButton.layer.borderColor = LINE_COLOR.CGColor;
                areaButton.layer.borderWidth = 0.5;
                areaButton.backgroundColor = [UIColor whiteColor];
                [areaButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [areaButton addTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
                if (self.areaViewType==AreaViewTypeRegion) {
                    [areaButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
                    [areaButton setTitle:_areaListArray[i] forState:UIControlStateNormal];
                    [areaButton setTitle:_areaListArray[i] forState:UIControlStateSelected];
                    if ([self.selectedArray containsObject:_areaListArray[i]]) {
                        if (self.areaViewType==AreaViewTypeRegion) {
                            areaButton.selected = YES;
                            areaButton.backgroundColor = mainColor;
                            areaButton.layer.borderColor = mainColor.CGColor;
                            
                        }
                    }

                } else if (self.areaViewType==AreaViewTypeCity) {
                    [areaButton setTitleColor:mainColor forState:UIControlStateSelected];
                    [areaButton setTitle:_areaListArray[i] forState:UIControlStateNormal];
                    [areaButton setTitle:_areaListArray[i] forState:UIControlStateSelected];
                    if ([self.selectedArray containsObject:_areaListArray[i]]) {
                        areaButton.selected = YES;
                        areaButton.titleLabel.textColor = mainColor;
                        areaButton.backgroundColor = [UIColor whiteColor];
                        areaButton.layer.borderColor = mainColor.CGColor;
                    }

                } else {
                    
                    [areaButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
                    BusinessSpecDetailParser *detailParser = _areaListArray[i];
                    [areaButton setTitle:detailParser.name forState:UIControlStateNormal];
                    [areaButton setTitle:detailParser.name forState:UIControlStateSelected];
                    for ( BusinessSpecDetailParser *parser in self.selectedArray) {
                        if ([parser.ID isEqualToString:detailParser.ID]) {
                            areaButton.selected = YES;
                            areaButton.titleLabel.textColor = [UIColor redColor];
                            areaButton.backgroundColor = [UIColor whiteColor];
                            areaButton.layer.borderColor = [UIColor redColor].CGColor;
                        }
                    }

                }
                    
                
               
                self.indexNumber++;
                
            }
            
        }
        NSUInteger row = _areaListArray.count/3;
        self.viewHeight = _areaListArray.count%3==0?(row*height+(10*(row+1))):((row+1)*height+10*(row+2));
 
    } else {
        self.viewHeight = 0;
    }
}

//-(void)setSelectedArray:(NSArray *)selectedArray {
//    _selectedArray = selectedArray;
//    
//}

- (void)areaAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        if (self.areaViewType==AreaViewTypeRegion) {
            sender.backgroundColor = mainColor;
            sender.layer.borderColor = mainColor.CGColor;
        } else if (self.areaViewType==AreaViewTypeCity) {
            sender.backgroundColor = [UIColor whiteColor];
            sender.layer.borderColor = mainColor.CGColor;
        } else {
            sender.backgroundColor = [UIColor whiteColor];
            sender.layer.borderColor = [UIColor redColor].CGColor;
        }
       
    } else {
        sender.backgroundColor = [UIColor whiteColor];
        sender.layer.borderColor = LINE_COLOR.CGColor;
    }
    if (self.areaViewType==AreaViewTypeRegion ) {
        for (UIButton *button in self.subviews) {
            if (button.tag!=sender.tag) {
                button.selected = NO;
                button.backgroundColor = [UIColor whiteColor];
                button.layer.borderColor = LINE_COLOR.CGColor;
            }
        }
        if ([self.delegate respondsToSelector:@selector(bigAreaName:andIndex: selected:)]) {
            
            [self.delegate bigAreaName:self.areaListArray[sender.tag-200] andIndex:sender.tag selected:sender.selected];
        }
    } else if (self.areaViewType==AreaViewTypeCity ){
        if ([self.delegate respondsToSelector:@selector(bigAreaName:andIndex: selected:)]) {
            
            [self.delegate bigAreaName:self.areaListArray[sender.tag-500] andIndex:sender.tag selected:sender.selected];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(specParser:selected:)]) {
            BusinessSpecDetailParser *detailParser = self.areaListArray[sender.tag-500];
            [self.delegate specParser:detailParser selected:sender.selected];
        }
    }
    
}

@end
