//
//  ManageLogisticTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessClassifyModel.h"
#import "BusinessLogisticModel.h"

@protocol ManageLogisticDelegate <NSObject>

@optional
- (void) editLogisticWithLogisticId:(NSString *) logisticId;

@optional
- (void) deleteLogisticWithLogisticId:(NSString *) logisticId;

@optional
- (void) editClassifyWithClassifyId:(NSString *) classifyId;

@optional
- (void) deleteClassifyWithClassifyId:(NSString *) classifyId;

@end
@interface ManageLogisticTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UIView *rightView;
@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, assign) CGFloat cellHeight;
//模型
@property (nonatomic, strong) BusinessLogisticListParser *listParser;
@property (nonatomic, strong) BusinessClassifyDetailParser *detailParser;

@property (nonatomic, weak) id<ManageLogisticDelegate> logisticDelegate;
@end
