//
//  LogisticAreaCenterTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticAreaView.h"
#import "LogisticAreaModel.h"

@protocol AreaCenterDelegate <NSObject>

- (void)openOrCloseAreaViewWithParentName:(NSString *) parentName open:(BOOL ) isOpen;

-(void) selectedAreaWithAreaName:(NSString *) areaName selected:(BOOL ) isSelected;
@end

typedef NS_ENUM(NSInteger,AreaShowType) {
    AreaShowTypeClose=0,
    AreaShowTypeOpen
};


@interface LogisticAreaCenterTableViewCell : UITableViewCell

@property (nonatomic, assign) AreaShowType areaShowType;
@property (nonatomic, strong) UIButton *areaButton;
@property (nonatomic, strong) LogisticAreaView *areaView;
@property (nonatomic, strong) UIButton *upOrDownButton;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id<AreaCenterDelegate> delegate;
@property (nonatomic, strong) LogisticAreaDetailParser *areaParser;
@property (nonatomic, strong) NSArray *selectedArray;

+(NSString *) idString;
@end
