//
//  LogisticSetCenterTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/8/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticSetCenterTableViewCell.h"



@interface LogisticSetCenterTableViewCell ()
@property (nonatomic, strong) UILabel *firstLabel;
@property (nonatomic, strong) UILabel *secondLabel;
@property (nonatomic, strong) UILabel *firstWeightLabel;//首重
@property (nonatomic, strong) UILabel *secondWeightLabel;//次重
@property (nonatomic, strong) UILabel *line4;
@property (nonatomic, strong) UILabel *line5;
@end

@implementation LogisticSetCenterTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        UILabel *areaLabel = [UILabel new];
        areaLabel.text = Localized(@"地区选择");
        [self.contentView addSubview:areaLabel];
        self.areaSelectedLabel = [UILabel new];
        self.areaSelectedLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.areaSelectedLabel];
        UIButton *rightButton = [UIButton new];
        [rightButton setImage:[UIImage imageNamed:@"logistic_right_nav"] forState:UIControlStateNormal];
        [self.contentView addSubview:rightButton];
        UILabel *line1 = [UILabel new];
        line1.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line1];

        
        self.firstLabel = [UILabel new];
        self.firstLabel.text = @"首重（千克）";
        [self.contentView addSubview:self.firstLabel];
        self.firstTextField = [UITextField new];
        self.firstTextField.textAlignment = NSTextAlignmentRight;
        self.firstTextField.keyboardType = UIKeyboardTypeDecimalPad;//带小数点输入
        self.firstTextField.returnKeyType = UIReturnKeyDone;
        self.firstTextField.placeholder = @"kg";
        self.firstTextField.textColor = [UIColor redColor];
        [self.contentView addSubview:self.firstTextField];
        UILabel *line3 = [UILabel new];
        line3.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line3];
        
        self.secondLabel = [UILabel new];
        self.secondLabel.text = @"首重费用";
        [self.contentView addSubview:self.secondLabel];
        self.secondTextField = [UITextField new];
        self.secondTextField.textAlignment = NSTextAlignmentRight;
        self.secondTextField.keyboardType = UIKeyboardTypeDecimalPad;
        self.secondTextField.returnKeyType = UIReturnKeyDone;
        self.secondTextField.placeholder = @"¥";
        self.secondTextField.textColor = [UIColor redColor];
        [self.contentView addSubview:self.secondTextField];
        
        self.line4 = [UILabel new];
        self.line4.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:self.line4];
        self.firstWeightLabel = [UILabel new];
        self.firstWeightLabel.text = @"次重（千克）";
        [self.contentView addSubview:self.firstWeightLabel];
        self.firstWeightTextField = [UITextField new];
        self.firstWeightTextField.textAlignment = NSTextAlignmentRight;
        self.firstWeightTextField.keyboardType = UIKeyboardTypeDecimalPad;//带小数点输入
        self.firstWeightTextField.returnKeyType = UIReturnKeyDone;
        self.firstWeightTextField.placeholder = @"kg";
        self.firstWeightTextField.textColor = [UIColor redColor];
        [self.contentView addSubview:self.firstWeightTextField];
        self.line5 = [UILabel new];
        self.line5.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:self.line5];
        
        self.secondWeightLabel = [UILabel new];
        self.secondWeightLabel.text = @"次重费用";
        [self.contentView addSubview:self.secondWeightLabel];
        self.secondWeightTextField = [UITextField new];
        self.secondWeightTextField.textAlignment = NSTextAlignmentRight;
        self.secondWeightTextField.keyboardType = UIKeyboardTypeDecimalPad;
        self.secondWeightTextField.returnKeyType = UIReturnKeyDone;
        self.secondWeightTextField.placeholder = @"¥";
        self.secondWeightTextField.textColor = [UIColor redColor];
        [self.contentView addSubview:self.secondWeightTextField];
        
        self.firstLabel.tag = 500;
        self.secondLabel.tag = 1000;
        
        //50*4+3
        CGFloat height = 50;
        [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView).with.mas_offset(15);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(height);
        }];
        
        [self.areaSelectedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView);
            make.left.mas_equalTo(areaLabel.mas_right).with.mas_offset(10);
            make.width.mas_equalTo(mainWidth-100-15-40);
            make.height.mas_equalTo(height);
        }];
        [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView);
            make.width.mas_equalTo(40);
            make.centerY.mas_equalTo(self.areaSelectedLabel);
            
        }];
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(areaLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        
        [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(line1.mas_bottom);
            make.left.mas_equalTo(areaLabel);
            make.width.mas_equalTo(110);
            make.height.mas_equalTo(height);
        }];
        
        [self.firstTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.mas_equalTo(self.firstLabel);
            make.left.mas_equalTo(self.firstLabel.mas_right).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        
        [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.firstLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        
        [self.secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(line3.mas_bottom);
            make.left.mas_equalTo(areaLabel);
            make.width.mas_equalTo(140);
            make.height.mas_equalTo(height);
        }];
        
        [self.secondTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondLabel);
            make.left.mas_equalTo(self.secondLabel.mas_right).with.mas_offset(10);
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        
        [self.line4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        [self.firstWeightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.line4.mas_bottom);
            make.left.mas_equalTo(areaLabel);
            make.width.mas_equalTo(110);
            make.height.mas_equalTo(height);
        }];
        
        [self.firstWeightTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.mas_equalTo(self.firstWeightLabel);
            make.left.mas_equalTo(self.firstWeightLabel.mas_right).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        [self.line5 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.firstWeightLabel.mas_bottom);
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        [self.secondWeightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.line5.mas_bottom);
            make.left.mas_equalTo(areaLabel);
            make.width.mas_equalTo(140);
            make.height.mas_equalTo(height);
        }];
        
        [self.secondWeightTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondWeightLabel);
            make.left.mas_equalTo(self.secondWeightLabel.mas_right).with.mas_offset(10);
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-15);
        }];
        
        
        [self layoutIfNeeded];
        self.cellHeight = 50*3+3;
        
        UITapGestureRecognizer *areaTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSelectArea:)];
        areaLabel.userInteractionEnabled = YES;
        [areaLabel addGestureRecognizer:areaTap];
         UITapGestureRecognizer *areaTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSelectArea:)];
        self.areaSelectedLabel.userInteractionEnabled = YES;
        [self.areaSelectedLabel addGestureRecognizer:areaTap2];
        [rightButton addTarget:self action:@selector(toSelectArea:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
//        UITapGestureRecognizer *companyTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSelectCompany:)];
//        company.userInteractionEnabled = YES;
//        [company addGestureRecognizer:companyTap];
//        UITapGestureRecognizer *companyTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSelectCompany:)];
//        self.companyLabel.userInteractionEnabled = YES;
//        [self.companyLabel addGestureRecognizer:companyTap2];

    }
    return self;
}

-(void)setRuleType:(RuleType)ruleType {
    _ruleType = ruleType;
    if (_ruleType==RuleTypeWeight) {
        
        //按重量计算
        self.line4.hidden = NO;
        self.line5.hidden = NO;
        self.firstWeightLabel.hidden = NO;
        self.firstWeightTextField.hidden = NO;
        self.secondWeightTextField.hidden = NO;
        self.secondWeightLabel.hidden = NO;
         self.cellHeight = 50*5+3;
        [self layoutIfNeeded];
    } else {
        //按金额计算
        NSString *string = @"购物满(购物满多少元，物流费自设)";
        NSMutableAttributedString *totalAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
        [totalAttributedString addAttribute:NSForegroundColorAttributeName
                                      value:[UIColor redColor]
                                      range:NSMakeRange(3, string.length-3)];
        [totalAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(3, string.length-3)];
        self.firstLabel.attributedText = totalAttributedString;
        
        self.secondLabel.text = @"物流费";
        
        //更新约束
        [self.firstLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(mainWidth/3*2);
        }];
        [self.firstTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstLabel.mas_right).with.mas_offset(10);
        }];
        
        self.firstTextField.placeholder = @"¥";
        self.secondTextField.placeholder = @"¥";
        
        self.line4.hidden = YES;
        self.line5.hidden = YES;
        self.firstWeightLabel.hidden = YES;
        self.firstWeightTextField.hidden = YES;
        self.secondWeightTextField.hidden = YES;
        self.secondWeightLabel.hidden = YES;
        
        self.cellHeight = 50*3+3;
        [self layoutIfNeeded];
    
    }
}

+(NSString *)idString{
  return @"LogisticSetCenterTableViewCell";
}

- (void)toSelectArea:(id *) tap{
    if ([self.delegate respondsToSelector:@selector(selectArea:)]) {
        [self.delegate selectArea:self.areaSelectedLabel.tag];
    }
}

- (void)toSelectCompany:(UITapGestureRecognizer *) tap{
    if ([self.delegate respondsToSelector:@selector(selectCompany)]) {
        [self.delegate selectCompany];
    }
}

@end
