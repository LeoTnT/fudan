//
//  LogisticCompanyTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/8/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LogisticSetCenterDelegate <NSObject>

- (void)buttonIsSelected:(BOOL) selected ;

@end


@interface LogisticCompanyTableViewCell : UITableViewCell
@property (nonatomic, strong) UIButton *companyButton;
@property (nonatomic, copy) NSString *companyName;
+(NSString *) idString;
@end
