//
//  BusinessDefaultView.m
//  App3.0
//
//  Created by nilin on 2017/5/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessDefaultView.h"

@implementation BusinessDefaultView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = BG_COLOR;
//        CGFloat imageWidth = NORMOL_SPACE*15;
        self.imageView = [[UIImageView alloc] init];
        self.imageView .image = [UIImage imageNamed:@"empty_logo"];
        [self addSubview:self.imageView];
        
        self.tintLabel = [[UILabel alloc] init];
        self.tintLabel.textColor = COLOR_666666;
        self.tintLabel.textAlignment = NSTextAlignmentCenter;
        self.tintLabel.text = @"主人，您还没开通商家版";
        self.tintLabel.numberOfLines = 0;
        self.tintLabel.font = [UIFont qsh_systemFontOfSize:17];
        [self addSubview:self.tintLabel];
        
        self.applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.applyBtn.backgroundColor = mainColor;
        [self.applyBtn setTitle:@"立即申请" forState:UIControlStateNormal];
        self.applyBtn.titleLabel.font = [UIFont qsh_systemFontOfSize:20];
        [self.applyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.applyBtn.layer.cornerRadius = 5;
        self.applyBtn.layer.borderColor = mainColor.CGColor;
        self.applyBtn.layer.masksToBounds = YES;
        [self.applyBtn addTarget:self action:@selector(applyBusinessAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.applyBtn];
        
        self.reasonLabel = [UILabel new];
        self.reasonLabel.numberOfLines = 0;
        self.reasonLabel.adjustsFontSizeToFitWidth = YES;
        self.reasonLabel.font = [UIFont qsh_systemFontOfSize:17];
        [self addSubview:self.reasonLabel];

        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(self).offset(80);
        }];
        
        [self.reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.width.mas_lessThanOrEqualTo(mainWidth-20);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.imageView.mas_bottom);
        }];
        
        [self.tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.width.mas_lessThanOrEqualTo(mainWidth-20);
            make.top.mas_equalTo(self.imageView.mas_bottom).offset(40);
        }];
        
        [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(self.tintLabel.mas_bottom).offset(12);
            make.size.mas_equalTo(CGSizeMake(120, 36));
        }];
    }
    return self;
}
-(void)applyBusinessAction:(UIButton *) sender{
    sender.selected = !sender.selected;
    [self.businessDelegate clicktoApplyBusinessEdition];
}
@end
