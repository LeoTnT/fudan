//
//  CommonChooseView.m
//  App3.0
//
//  Created by nilin on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CommonChooseView.h"
#import "ManageLogisticTableViewCell.h"

@interface CommonChooseView ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation CommonChooseView
-(instancetype)init {
    if (self=[super init]) {
        self.titleLabel = [UILabel new];
        self.titleLabel.backgroundColor = BG_COLOR ;
//        self.titleLabel.alpha = 0.5;
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont qsh_systemFontOfSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        

        self.closeButton = [UIButton new];
        [self.closeButton setTitle:Localized(@"关闭") forState:UIControlStateNormal];
        [self.closeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.closeButton];
        
        self.oneButton = [UIButton new];
        self.oneButton.backgroundColor = [UIColor whiteColor];
        [self.oneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.oneButton setTitle:@"新建分类" forState:UIControlStateNormal];
        [self.oneButton setImage:[UIImage imageNamed:@"business_add"] forState:UIControlStateNormal];
        [self.oneButton addTarget:self action:@selector(oneAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.oneButton];
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.bounces = NO;
        //去分割线
        self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [self addSubview:self.tableView];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self);
            make.left.right.mas_equalTo(self);
            make.height.mas_equalTo(50);
        }];
        
        [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.mas_equalTo(self);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(self.titleLabel);
        }];
        
        [self.oneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom);
            make.height.mas_equalTo(40);
            make.left.right.mas_equalTo(self);
        }];
        
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.oneButton.mas_bottom);
            make.bottom.mas_equalTo(self);
            make.left.right.mas_equalTo(self);
        }];
    }
    return self;
}

-(void)setListArray:(NSMutableArray *)listArray {
    _listArray = listArray;
    [self.tableView reloadData];

}

- (void) closeAction:(UIButton *) sender {
    if ([self.chooseDelegate respondsToSelector:@selector(closeCommonChooseView)]) {
        [self.chooseDelegate closeCommonChooseView];
    }
}

- (void)oneAction {
    if ([self.chooseDelegate respondsToSelector:@selector(toNewViewWithType:)]) {
        [self.chooseDelegate toNewViewWithType:self.commonChooseViewType];
    }
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"mLogisticCell";
    ManageLogisticTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        cell = [[ManageLogisticTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
    }
    cell.rightView.hidden = YES;
    if (self.commonChooseViewType==CommonChooseViewTypeCategory) {
         cell.detailParser = self.listArray[indexPath.row];
        
    }
     if (self.commonChooseViewType==CommonChooseViewTypeLogistic) {
     cell.listParser = self.listArray[indexPath.row];
    }
   
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.commonChooseViewType==CommonChooseViewTypeLogistic) {
        if ([self.chooseDelegate respondsToSelector:@selector(selectedLogisticWithLogisticParser:)]) {
            [self.chooseDelegate selectedLogisticWithLogisticParser:self.listArray[indexPath.row]];
        }
    }
    if (self.commonChooseViewType==CommonChooseViewTypeCategory) {
        if ([self.chooseDelegate respondsToSelector:@selector(selectedCategoryWithCategoryParser:)]) {
            [self.chooseDelegate selectedCategoryWithCategoryParser:self.listArray[indexPath.row]];
        }    }

    
}

@end
