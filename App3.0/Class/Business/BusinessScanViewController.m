//
//  BusinessScanViewController.m
//  App3.0
//
//  Created by nilin on 2017/9/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BusinessScanViewController.h"
#import "BusinessModel.h"
#import "GoodsStandardViewController.h"

@interface BusinessScanViewController ()
//{
//    __weak id<BusinessScanViewDelegate>delegate;
//}
@end

@implementation BusinessScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    self.style = style;
}

- (void)showError:(NSString *)str
{
    
}

- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1)
    {
        [XSTool showToastWithView:self.view Text:@"识别失败"];
        
        return;
    }
    
    /*
     //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
     for (LBXScanResult *result in array) {
     NSLog(@"scanResult:%@",result.strScanned);
     }
     */
    
    LBXScanResult *scanResult = array[0];
    //type:org.gs1.EAN-13
    //type:org.iso.QRCode
    if ([scanResult.strBarCodeType containsString:@"QRCode"]) {
        [XSTool showToastWithView:self.view Text:@"不是条形码！"];
    } else {
        
        //条形码
    [self popLastViewControllerWithCode:scanResult];
        
    }
    
}

- (void) popLastViewControllerWithCode:(LBXScanResult *) scanResult {
    GoodsStandardViewController *controller = (GoodsStandardViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    NSMutableString *mCodeString = [NSMutableString stringWithString:[scanResult.strScanned substringToIndex:scanResult.strScanned.length-1]];
    if ([scanResult.strBarCodeType isEqualToString:@"org.gs1.EAN-13"]) {
        
        //EAN码符号有标准版（EAN-13）和缩短版（EAN-8）两种，我国的通用商品条码与其等效，日常购买的商品包装上所印的条码一般就是EAN码
        //690 12345 6789 2
        
        //截取商品编码
        controller.barCode =  [mCodeString substringFromIndex:8];
        NSLog(@"controller.barCode===%@",controller.barCode);
    }
    
    //690 1234 1
    if ([scanResult.strBarCodeType isEqualToString:@"org.gs1.EAN-8"]) {
        //截取商品编码
        controller.barCode =  [mCodeString substringFromIndex:3];
        NSLog(@"controller.barCode===%@",controller.barCode);
    }
    [self.navigationController popToViewController:controller animated:YES];

    
}

//- (void) requestCommodityProduct:(NSString *)str {
//    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [HTTPManager getCommodityProduct:str success:^(NSDictionary *dic, resultObject *state) {
//        
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if (state) {
//            
//            NSString *ID = dic[@"data"][@"pid"];
//            GoodsDetailViewController *vc = [GoodsDetailViewController new];
//            vc.goodsID = ID;
//            [XSTool hideProgressHUDWithView:self.view];
//            [self.navigationController pushViewController:vc animated:YES];
//            
//        }else{
//            
//            
//            [MBProgressHUD showMessage:dic[@"info"] view:self.view hideTime:2 doSomeThing:^{
//                [self.navigationController popViewControllerAnimated:YES];
//            }];
//        }
//        
//        
//    } fail:^(NSError *error) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [MBProgressHUD showMessage:NetFailure view:self.view];
//    }];
//}

//- (BOOL)isPureInt:(NSString*)string{
//    NSScanner* scan = [NSScanner scannerWithString:string];
//    int val;
//    return[scan scanInt:&val] && [scan isAtEnd];
//}

/**
 product";//商品
 "supply";//店铺
 "app";//客户端
 "user";//个人,主要用于二维码
 */
//- (void)handleQRCode:(NSString *)QRInformation {
//    //    http://txzy.xsy.dsceshi.cn/mobile/product/details?qrtype=product&id=2&userid=212
//    
//    [QRInformation containsString:@"&"] ? [self QRCondRequest:QRInformation]:[self barCodeRequest:QRInformation];
//    
//}



/**
 条形码
 */
- (void) barCodeRequest:(NSString *)sender {
    
    
}
//
//
///**
// 二维码
// */
//- (void) QRCondRequest:(NSString *)QRInformation {
//    [XSTool hideProgressHUDWithView:self.view];
//    NSArray *arr = [QRInformation componentsSeparatedByString:@"&"];
//    if (![QRInformation containsString:ImageBaseUrl] || arr.count < 2) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
//        return;
//    }
//    __block NSString *goodsID ;
//    NSString *deleteStr = @"id=";
//    
//    
//    NSString *str = arr[1];
//    if ([str  containsString:deleteStr]) {
//        goodsID = [str substringFromIndex:deleteStr.length];
//    }
//    if ([QRInformation containsString:@"product"]) {
//        
//        
//        GoodsDetailViewController *vc = [GoodsDetailViewController new];
//        vc.goodsID = goodsID;
//        [self.navigationController pushViewController:vc animated:YES];
//    } else if ([QRInformation containsString:@"supply"] && ![QRInformation containsString:@"supply_pay"]){
//        S_StoreInformation *infor = [S_StoreInformation new];
//        infor.storeInfor = goodsID;
//        
//        [XSTool hideProgressHUDWithView:self.view];
//        [self.navigationController pushViewController:infor animated:YES];
//        
//    } else if ([QRInformation containsString:@"app"]){
//        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    } else if ([QRInformation containsString:@"user"]){
//        deleteStr = @"id=";
//        str = arr[1];
//        
//        if ([str  containsString:deleteStr]) {
//            goodsID = [str substringFromIndex:deleteStr.length];
//        }
//        if ([goodsID isEqualToString:[UserInstance ShardInstnce].uid]) {
//            PersonViewController *personVC = [[PersonViewController alloc] init];
//            [self.navigationController pushViewController:personVC animated:YES];
//        } else {
//            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:goodsID];
//            [self.navigationController pushViewController:perVC animated:YES];
//        }
//    } else if ([QRInformation containsString:@"supply_pay"]){
//        S_CashierController *cashier =[S_CashierController new];
//        cashier.showTitle = goodsID;
//        cashier.supply_name = goodsID;
//        [self.navigationController pushViewController:cashier animated:YES];
//    } else if ([QRInformation containsString:@"quickpay"]){
//        [XSTool showProgressHUDWithView:self.view];
//        [HTTPManager supplyGetSupplyInfoWithUid:goodsID success:^(NSDictionary *dic, resultObject *state) {
//            [XSTool hideProgressHUDWithView:self.view];
//            if (state.status) {
//                BusinessSupplyParser *supplyParser = [BusinessSupplyParser mj_objectWithKeyValues:dic[@"data"]];
//                
//                //是否开通收银台
//                if ([supplyParser.is_enable_quickpay intValue]==1) {
//                    S_CashierController *cashier =[S_CashierController new];
//                    NSString *str = arr[2],*supplyId;
//                    NSString *delStr = @"intro=";
//                    if ([str  containsString:delStr]) {
//                        supplyId = [str substringFromIndex:delStr.length];
//                    }
//                    cashier.showTitle = [NSString stringWithFormat:@"商家账号：%@",supplyId];
//                    cashier.supply_name = supplyId;
//                    cashier.quick_percent_pay = [supplyParser.quick_pay integerValue];
//                    [self.navigationController pushViewController:cashier animated:YES];
//                } else {
//                    [XSTool showToastWithView:self.view Text:@"尚未开通收银台！"];
//                }
//                
//                
//            } else {
//                [XSTool showToastWithView:self.view Text:state.info];
//                
//            }
//        }fail:^(NSError *error) {
//            [XSTool hideProgressHUDWithView:self.view];
//            [XSTool showToastWithView:self.view Text:NetFailure];
//        }];
//        
//    }
//}



@end
