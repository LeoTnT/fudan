//
//  LoginModel.h
//  App3.0
//
//  Created by mac on 17/3/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    SmsVerifyPhoneBind = 1,
    SmsVerifyPhoneAuth,
    SmsVerifyPayPwdReset,
    SmsVerifyLoginPwdReset,
    SmsVerifyLogin
}SmsVerifyType;

typedef enum
{
    PasswordLogin = 1,
    PasswordPay,
}PasswordType;

typedef enum
{
    ResetPwdByMobile = 1,
    ResetPwdByEmail,
}ResetPwdWay;

@interface LoginDataParser : NSObject
@property(nonatomic,copy) NSString* uid;
@property(nonatomic,copy) NSString* guid;
@property(nonatomic,copy) NSString* username;
@property(nonatomic,copy) NSString* nickname;
@property(nonatomic,copy) NSString* truename;
@property(nonatomic,copy) NSString* mobile;
@property(nonatomic,copy) NSString* logo;
@property(nonatomic,copy) NSString* sex;

@property (nonatomic ,strong)UIImage *headImage;
@end

@interface LoginParser : NSObject
@property (nonatomic,strong)LoginDataParser *data;
@end

@interface CheckMobileExistParser : NSObject
@property (nonatomic,assign)BOOL data;
@end

/**
 openid : o1fbCwOQzy333cF2wjQuu653u6CU,
	sex : 1,
	status : 0,
	nickname : Nilin,
	unionid : oorUxs97ERjdPu7v7roN7yCl_pm8,
	logo : http://wx.qlogo.cn/mmopen/vi_32/GwchRbKxeY4ciabsxJ20REVOBr4hBSY6LQ90Bo05Xw191O39ghajxuTovxPCxl9PICmmgxdtzrNQLpmZB6XoO8g/0
 */
@interface  ThirdPartyParser : NSObject
@property (nonatomic, copy) NSString *openid;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *unionid;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *guid;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *logo;
@end

@interface LoginCheckModel : NSObject
@property (nonatomic, assign) NSInteger is_show_google;
@property (nonatomic, assign) NSInteger is_show_message;
@property (nonatomic, assign) NSInteger is_show_email;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *country_code;


@end

@interface LoginModel : NSObject

@end
