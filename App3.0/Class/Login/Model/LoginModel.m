//
//  LoginModel.m
//  App3.0
//
//  Created by mac on 17/3/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginDataParser

- (id) initWithCoder: (NSCoder *)coder
{
    
    if (self = [super init])
        
    {
        self.uid = [coder decodeObjectForKey:@"uid"];
        self.guid = [coder decodeObjectForKey:@"guid"];
        self.username = [coder decodeObjectForKey:@"username"];
        self.nickname = [coder decodeObjectForKey:@"nickname"];
        self.truename = [coder decodeObjectForKey:@"truename"];
        self.mobile = [coder decodeObjectForKey:@"mobile"];
        self.logo = [coder decodeObjectForKey:@"logo"];
        self.sex = [coder decodeObjectForKey:@"sex"];
    }
    
    return self;
    
}

- (void) encodeWithCoder: (NSCoder *)coder

{
    [coder encodeObject:self.uid forKey:@"uid"];
    [coder encodeObject:self.guid forKey:@"guid"];
    [coder encodeObject:self.username forKey:@"username"];
    [coder encodeObject:self.nickname forKey:@"nickname"];
    [coder encodeObject:self.truename forKey:@"truename"];
    [coder encodeObject:self.mobile forKey:@"mobile"];
    [coder encodeObject:self.logo forKey:@"logo"];
    [coder encodeObject:self.sex forKey:@"sex"];
}
@end

@implementation LoginParser

@end

@implementation CheckMobileExistParser

@end

@implementation ThirdPartyParser
//@synthesize guid,uid,username,unionid,status,truename,sex,mobile,nickname,logo,openid;

@end


@implementation LoginCheckModel

@end
@implementation LoginModel

@end
