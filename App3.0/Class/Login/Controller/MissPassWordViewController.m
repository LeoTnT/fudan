//
//  MissPassWordViewController.m
//  App3.0
//
//  Created by mac on 2017/3/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MissPassWordViewController.h"
#import "LoginModel.h"
#import "ResetPwdViewController.h"

@interface MissPassWordViewController ()
{
    UITextField *_tfPhone;
    UITextField *_tfCode;
    XSCustomButton *_nextBtn;
    UIButton *_getCodeBtn;//获取验证码按钮
    BOOL _isTimer;// 是否在倒计时
}
@end

@implementation MissPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    [self setSubViews];
    
}

#pragma mark - 设置子视图
-(void)setSubViews{
    self.navigationItem.title = Localized(@"forget_pwd");
    self.view.backgroundColor=[UIColor whiteColor];
    // textField-leftView
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    [leftView1 addSubview:leftImg1];
    // 手机号码输入框
    _tfPhone = [[UITextField alloc] init];
    [self.view addSubview:_tfPhone];
    _tfPhone.placeholder = Localized(@"请输入11位手机号码");
    _tfPhone.font = [UIFont systemFontOfSize:15];
    _tfPhone.leftView = leftView1;
    _tfPhone.leftViewMode = UITextFieldViewModeAlways;
    _tfPhone.keyboardType = UIKeyboardTypeNumberPad;
    [_tfPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    UIView *lineView1= [[UIView alloc] init];
    [self.view addSubview:lineView1];
    lineView1.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_code"]];
    [leftView2 addSubview:leftImg2];
    _getCodeBtn =[[UIButton alloc] init];
    [self.view addSubview:_getCodeBtn];
    [_getCodeBtn setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
    [_getCodeBtn setTitleColor:[UIColor hexFloatColor:@"c0c0c0"] forState:UIControlStateDisabled];
    [_getCodeBtn setTitleColor:mainColor forState:UIControlStateNormal];
    _getCodeBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_getCodeBtn addTarget:self action:@selector(getCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    _getCodeBtn.enabled = NO;
    // 验证码输入框
    _tfCode = [[UITextField alloc] init];
    [self.view addSubview:_tfCode];
    _tfCode.placeholder = Localized(@"sms_send_dialog_sms_hint");
    _tfCode.font = [UIFont systemFontOfSize:15];
    _tfCode.leftView = leftView2;
    _tfCode.leftViewMode = UITextFieldViewModeAlways;
    _tfCode.keyboardType = UIKeyboardTypeNumberPad;
    UIView *lineView2= [[UIView alloc] init];
    [self.view addSubview:lineView2];
    lineView2.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    // 登录
    _nextBtn=[[XSCustomButton alloc] initWithTitle:Localized(@"bind_mobile_next") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] cornerRadius:4 backGroundColor:mainColor hBackGroundColor:HighLightColor_Main];
    [_nextBtn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_nextBtn];
    //设置约束
    [_tfPhone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(self.view).offset(121-64);
        make.right.mas_equalTo(self.view).offset(-25);
    }];
    [leftImg1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView1);
        make.centerY.mas_equalTo(leftView1);
    }];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPhone);
        make.bottom.mas_equalTo(_tfPhone.mas_bottom).offset(13);
        make.width.mas_equalTo(_tfPhone);
        make.height.mas_equalTo(0.5);
    }];
    [leftImg2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView2);
        make.centerY.mas_equalTo(leftView2);
    }];
    [_tfCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPhone);
        make.top.mas_equalTo(lineView1).offset(32);
        //防止遮挡获取验证码
        make.width.mas_lessThanOrEqualTo(mainWidth-150);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfCode.mas_bottom).offset(13);
    }];
    [_getCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(lineView2);
        make.centerY.mas_equalTo(_tfCode);
    }];
    [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfCode.mas_bottom).offset(43.5);
        make.left.mas_equalTo(lineView2);
        make.right.mas_equalTo(lineView2);
        make.height.mas_equalTo(50);
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 获取验证码
-(void)getCodeAction:(UIButton *) sender{
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
    [HTTPManager getSmsVerifyWithDic:@{@"mobile":_tfPhone.text} success:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            _isTimer = YES; // 设置倒计时状态为YES
            sender.enabled = NO; // 设置按钮为不可点击
            __block NSInteger time = 59; //倒计时时间
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            
            dispatch_source_set_event_handler(_timer, ^{
                
                if(time <= 0){ //倒计时结束，关闭
                    
                    dispatch_source_cancel(_timer);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //设置按钮的样式
                        [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                        sender.enabled = YES; // 设置按钮可点击
                        
                        _isTimer = NO; // 倒计时状态为NO
                    });
                    
                }else{
                    
                    int seconds = time % 60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //设置按钮显示读秒效果
                        [sender setTitle:[NSString stringWithFormat:@"重新发送( %.2d )", seconds] forState:UIControlStateNormal];
                        
                    });
                    time--;
                }
            });
            dispatch_resume(_timer);
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
            sender.enabled = YES; // 设置按钮为可点击
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
        sender.enabled = YES; // 设置按钮为可点击
    }];
}

#pragma mark - 下一步
-(void)nextAction:(UIButton *) sender{
    if (!(_tfCode.text.length && _tfPhone.text.length)) {
        [XSTool showToastWithView:self.view Text:@"手机号和验证码不能为空"];
        return;
    }
    if(_tfPhone.text.length!=11){
        [XSTool showToastWithView:self.view Text:@"手机号格式有误"];
        return;
    }
    @weakify(self);
    [HTTPManager checkSmsVerifyValidWithMobile:_tfPhone.text smsVerify:_tfCode.text success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            // 验证通过进入下一步
            ResetPwdViewController *resetVC = [[ResetPwdViewController alloc] initWithMobile:_tfPhone.text smsVerify:_tfCode.text];
            [self.navigationController pushViewController:resetVC animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:error.description];
    }];
}
/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    // 手机号码格式判断
    if (_tfPhone.text.length == 11) {
        if (_isTimer == NO) {
            _getCodeBtn.enabled = YES;
        } else {
            _getCodeBtn.enabled = NO;
        }
    } else {
        _getCodeBtn.enabled = NO;
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
