//
//  CodeLoginViewController.m
//  App3.0
//
//  Created by mac on 17/3/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CodeLoginViewController.h"
#import "LoginModel.h"
#import "AppDelegate.h"
#import "UserInstance.h"

#define phonePrefix   @"1[3|4|5|7|8|][0-9]{9}"
@interface CodeLoginViewController ()<UITextFieldDelegate>
{
    UITextField *_tfPhone;
    UITextField *_tfCode;
    UIButton *_codeBtn;
    XSCustomButton *_loginBtn;
}
@end

@implementation CodeLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = Localized(@"enter_login");
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    //返回按钮
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    // textField-leftView
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    [leftView1 addSubview:leftImg1];
    // 手机号码输入框
    _tfPhone = [[UITextField alloc] init];
    [self.view addSubview:_tfPhone];
    _tfPhone.placeholder = Localized(@"请输入11位手机号码");
    _tfPhone.font = [UIFont systemFontOfSize:15];
    _tfPhone.leftView = leftView1;
    _tfPhone.leftViewMode = UITextFieldViewModeAlways;
    _tfPhone.delegate = self;
    _tfPhone.keyboardType = UIKeyboardTypeNumberPad;
    [_tfPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    UIView *lineView1= [[UIView alloc] init];
    [self.view addSubview:lineView1];
    lineView1.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_code"]];
    [leftView2 addSubview:leftImg2];
    _codeBtn =[[UIButton alloc] init];
    [self.view addSubview:_codeBtn];
    [_codeBtn setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
    [_codeBtn setTitleColor:[UIColor hexFloatColor:@"c0c0c0"] forState:UIControlStateDisabled];
    [_codeBtn setTitleColor:mainColor forState:UIControlStateNormal];
    _codeBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_codeBtn addTarget:self action:@selector(getCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    _codeBtn.enabled = NO;
    // 验证码输入框
    _tfCode = [[UITextField alloc] init];
    [self.view addSubview:_tfCode];
    _tfCode.placeholder = Localized(@"sms_send_dialog_sms_hint");
    _tfCode.font = [UIFont systemFontOfSize:15];
    _tfCode.leftView = leftView2;
    _tfCode.leftViewMode = UITextFieldViewModeAlways;
    _tfCode.keyboardType = UIKeyboardTypeNumberPad;
    UIView *lineView2= [[UIView alloc] init];
    [self.view addSubview:lineView2];
    lineView2.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    // 登录
    _loginBtn=[[XSCustomButton alloc] initWithTitle:Localized(@"enter_login") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] cornerRadius:4 backGroundColor:mainColor hBackGroundColor:HighLightColor_Main];
    [_loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_loginBtn];
    //设置约束
    [_tfPhone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(self.view).offset(121-64);
        make.right.mas_equalTo(self.view).offset(-25);
    }];
    [leftImg1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView1);
        make.centerY.mas_equalTo(leftView1);
    }];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPhone);
        make.bottom.mas_equalTo(_tfPhone.mas_bottom).offset(13);
        make.width.mas_equalTo(_tfPhone);
        make.height.mas_equalTo(0.5);
    }];
    [leftImg2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView2);
        make.centerY.mas_equalTo(leftView2);
    }];
    [_tfCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPhone);
        make.top.mas_equalTo(lineView1).offset(32);
        //防止遮挡获取验证码
        make.width.mas_lessThanOrEqualTo(mainWidth-150);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfCode.mas_bottom).offset(13);
    }];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(lineView2);
        make.centerY.mas_equalTo(_tfCode);
    }];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfCode.mas_bottom).offset(43.5);
        make.left.mas_equalTo(lineView2);
        make.right.mas_equalTo(lineView2);
        make.height.mas_equalTo(50);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loginAction
{
    [self.view endEditing:YES];
    if (!(_tfCode.text.length && _tfPhone.text.length)) {
        [XSTool showToastWithView:self.view Text:@"手机号和验证码不能为空"];
        return;
    }
    if(_tfPhone.text.length!=11){
        [XSTool showToastWithView:self.view Text:@"手机号格式有误"];
        return;
    }
    [XSTool showToastWithView:self.view Text:@"登录中..."];
    [HTTPManager loginBySms:_tfPhone.text verify:_tfCode.text success:^(NSDictionary *dic, resultObject *state) {
        LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];
        if (state.status) {
            NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
            NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];
#ifdef ALIYM_AVALABLE
            // 阿里云旺初始化
            [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:parser.data.uid passWord:pwd preloginedBlock:^{
                
            } successBlock:^{
                NSLog(@"登录成功");
                [XSTool hideProgressHUDWithView:self.view];
                
                // 将guid存到本地沙盒
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                //        [ud synchronize];
                
                //将用户信息存储到本地沙盒
                LoginDataParser *dataPaser = parser.data;
                NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                [user setObject:userData forKey:USERINFO_LOGIN];
                [user synchronize];
                
                // 实例化user数据
                [[UserInstance ShardInstnce] setupUserInfo];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            } failedBlock:^(NSError *error) {
                NSLog(@"登录失败 %@",error.description);
                [XSTool hideProgressHUDWithView:self.view];
            }];
            
            // 登录腾讯云
            NSString *tlsPwd = [pwd substringWithRange:NSMakeRange(8, 16)];
            [[ILiveLoginManager getInstance] tlsLogin:[NSString stringWithFormat:@"xsy%@",parser.data.uid] pwd:tlsPwd succ:^{
                NSLog(@"-----> succ");
            } failed:^(NSString *moudle, int errId, NSString *errMsg) {
                NSLog(@"-----> fail %@,%d,%@",moudle,errId,errMsg);
            }];
#elif defined EMIM_AVALABLE
            //登录环信
            [[EMClient sharedClient] loginWithUsername:parser.data.uid
                                              password:pwd
                                            completion:^(NSString *aUsername, EMError *aError) {
                                                if (!aError) {
                                                    NSLog(@"登录成功");
                                                    [XSTool hideProgressHUDWithView:self.view];
                                                    
                                                    // 将guid存到本地沙盒
                                                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                                                    //        [ud synchronize];
                                                    
                                                    //将用户信息存储到本地沙盒
                                                    LoginDataParser *dataPaser = parser.data;
                                                    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                                                    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                                                    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                                                    [user setObject:userData forKey:USERINFO_LOGIN];
                                                    [user synchronize];
                                                    
                                                    // 实例化user数据
                                                    [[UserInstance ShardInstnce] setupUserInfo];
                                                    
                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                } else {
                                                    [XSTool hideProgressHUDWithView:self.view];
                                                    [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"环信登录失败:%i",aError.code]];
                                                }
                                            }];
#else
        [self configInfor:parser];
        [self xmppLogWith:parser];
#endif
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"登录失败"];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"登录失败"];
    }];
}

- (void) configInfor:(LoginParser *)parser{
    NSLog(@"登录成功");
    [XSTool hideProgressHUDWithView:self.view];
    
    // 将guid存到本地沙盒
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
    //        [ud synchronize];
    
    //将用户信息存储到本地沙盒
    LoginDataParser *dataPaser = parser.data;
    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
    [user setObject:userData forKey:USERINFO_LOGIN];
    [user synchronize];
    
    // 实例化user数据
    [[UserInstance ShardInstnce] setupUserInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) xmppLogWith:(LoginParser *)parser {
    
    [XMPPManager xmppUserLogIn:parser.data.uid complment:^(XIM_STREAMSTATE type) {
        
        if (type == XIM_XMPP_CONNECT) {
            NSLog(@"登录成功");
            
        }else{
            
        }
    }];
    
}

- (void)getCodeAction:(UIButton *)sender
{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"code_tip") message:_tfPhone.text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = NO; // 设置按钮为不可点击
        [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
        [HTTPManager getSmsVerifyWithDic:@{@"mobile":_tfPhone.text} success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                __block NSInteger time = 59; //倒计时时间
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
                dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
                dispatch_source_set_event_handler(_timer, ^{
                    
                    if(time <= 0){ //倒计时结束，关闭
                        
                        dispatch_source_cancel(_timer);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //设置按钮的样式
                            [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                            sender.enabled = YES; // 设置按钮可点击
                        });
                        
                    }else{
                        
                        int seconds = time % 60;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //设置按钮显示读秒效果
                            [sender setTitle:[NSString stringWithFormat:@"重新发送( %.2d )", seconds] forState:UIControlStateNormal];
                            
                        });
                        time--;
                    }
                });
                dispatch_resume(_timer);
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                sender.enabled = YES; // 设置按钮为可点击
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
            sender.enabled = YES; // 设置按钮为可点击
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (BOOL)validatePhone:(NSString *)phonestr {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phonePrefix];
    BOOL isphoneValidate = [predicate evaluateWithObject:phonestr];
    return isphoneValidate;
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if ([textField isEqual:_tfPhone]) {
        if (textField.text.length >= 11) {
            textField.text = [textField.text substringToIndex:11];
            if (![self validatePhone:textField.text]) {
                [XSTool showToastWithView:self.view Text:@"请输入正确的手机号码！"];
                textField.text = @"";
            } else {
                [HTTPManager checkMobileExist:textField.text Success:^(NSDictionary *dic, resultObject *state) {
                    CheckMobileExistParser *parser = [CheckMobileExistParser mj_objectWithKeyValues:dic];
                    if (parser.data) {
                        _codeBtn.enabled = YES;
                    } else {
                        _codeBtn.enabled = NO;
                        textField.text = @"";
                        [XSTool showToastWithView:self.view Text:@"手机号不存在"];
                    }
                } fail:^(NSError * _Nonnull error) {
                    
                }];
            }
        } else {
            _codeBtn.enabled = NO;
        }
    }
}

@end
