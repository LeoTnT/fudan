//
//  LoginViewController.m
//  App3.0
//
//  Created by mac on 17/3/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LoginViewController.h"
#import "CodeLoginViewController.h"
#import "AppDelegate.h"
#import "MissPassWordViewController.h"
#import "LoginModel.h"
#import "UserInstance.h"
#import "WalletModel.h"
#import "QuickLandingController.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "RSAEncryptor.h"
#import "AccountCell.h"
#import "XSCustomButton.h"
#import "RegisterModel.h"
#import "XSRegisterViewController.h"
#import "ChangeHostViewController.h"

@interface LoginViewController () <WXApiDelegate,TencentSessionDelegate,UITableViewDelegate, UITableViewDataSource>
{
    UITextField *_tfUser;
    UITextField *_tfPassword;
    TencentOAuth *tencentOAuth;
    BOOL _isTimer;// 是否在倒计时
    BOOL _isTimer_email;// 是否在倒计时
    
}
@property (nonatomic ,strong) dispatch_source_t timer;
@property (nonatomic ,strong) dispatch_source_t timer_email;

@property (assign, nonatomic) BOOL isFillAccount;
@property (strong, nonatomic) UITableView *accountTableView;
@property (strong, nonatomic) NSArray *accountArray;
@property (strong, nonatomic) UIButton *rightBtn; // 选择账号下拉键
@property (strong, nonatomic) UIButton *showPswBtn; // 是否显示密码
@property (strong, nonatomic) NSString *savedPwd;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, assign) NSInteger index;  // for develop model

@property (strong, nonatomic) LoginCheckModel *loginCheckModel;

@property (nonatomic, copy) NSString *code_text;
@property (nonatomic, copy) NSString *google_text;
@property (nonatomic, copy) NSString *email_text;

@end

@implementation LoginViewController
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weiXinToLoginIn:) name:@"WeixinCodeNotification" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WeixinCodeNotification" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    //    self.navigationItem.title = @"登录";
    [self setNavi_title:Localized(@"enter_login")];
    
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"enter_register") action:^{
        XSRegisterViewController *registerVC = [[XSRegisterViewController alloc] init];
        [self.navigationController pushViewController:registerVC animated:YES];
        
    }];
    // 用户名输入框
    _tfUser = [[UITextField alloc] init];
    [self.view addSubview:_tfUser];
    _tfUser.placeholder = Localized(@"请输入手机号/系统编号");
    _tfUser.font = [UIFont systemFontOfSize:15];
    _tfUser.leftViewMode = UITextFieldViewModeAlways;
    _tfUser.rightViewMode = UITextFieldViewModeAlways;
    //    _tfUser.clearButtonMode = UITextFieldViewModeWhileEditing; // 与rightView不能同时存在
    [_tfUser addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    // textField-leftView
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    [leftView1 addSubview:leftImg1];
    _tfUser.leftView=leftView1;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightBtn setImage:[UIImage imageNamed:@"logoin_pull_down"] forState:UIControlStateNormal];
    [_rightBtn setImage:[UIImage imageNamed:@"logoin_pull_up"] forState:UIControlStateSelected];
    [_rightBtn addTarget:self action:@selector(accountChoose:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:_rightBtn];
    _tfUser.rightView=rightView;
    
    UIView *lineView1 = [[UIView alloc] init];
    [self.view addSubview:lineView1];
    lineView1.backgroundColor = [UIColor hexFloatColor:@"dddddd"];
    
    // 密码输入框
    _tfPassword = [[UITextField alloc] init];
    [self.view addSubview:_tfPassword];
    _tfPassword.placeholder = Localized(@"请输入密码");
    _tfPassword.font = [UIFont systemFontOfSize:15];
    _tfPassword.leftViewMode = UITextFieldViewModeAlways;
    _tfPassword.rightViewMode = UITextFieldViewModeAlways;
    _tfPassword.secureTextEntry = YES;
    [_tfPassword addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView2 addSubview:leftImg2];
    _tfPassword.leftView = leftView2;
    
    UIView *rightView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    _showPswBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_showPswBtn setImage:[UIImage imageNamed:@"pwd_hide"] forState:UIControlStateNormal];
    [_showPswBtn setImage:[UIImage imageNamed:@"pwd_show"] forState:UIControlStateSelected];
    [_showPswBtn addTarget:self action:@selector(showPassword:) forControlEvents:UIControlEventTouchUpInside];
    [rightView2 addSubview:_showPswBtn];
    _tfPassword.rightView = rightView2;
    
    UIView *lineView2 = [[UIView alloc] init];
    [self.view addSubview:lineView2];
    lineView2.backgroundColor = [UIColor hexFloatColor:@"dddddd"];
    // 登录
    XSCustomButton *loginBtn=[[XSCustomButton alloc] initWithTitle:Localized(@"enter_login") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] cornerRadius:4 backGroundColor:mainColor hBackGroundColor:HighLightColor_Main];
    [loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    // 手机验证码登录
    UIButton *codeBtn=[[UIButton alloc] init];
    [self.view addSubview:codeBtn];
    [codeBtn setTitle:Localized(@"login_phone") forState:UIControlStateNormal];
    codeBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [codeBtn setTitleColor:mainColor forState:UIControlStateNormal];
    codeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [codeBtn addTarget:self action:@selector(codeLoginAction) forControlEvents:UIControlEventTouchUpInside];
    // 忘记密码
    UIButton *forgetBtn=[[UIButton alloc] init];
    [self.view addSubview:forgetBtn];
    [forgetBtn setTitle:Localized(@"忘记密码?") forState:UIControlStateNormal];
    [forgetBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    forgetBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    forgetBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [forgetBtn addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    
    
#ifdef APP_SHOW_JYS
    
#else
    //第三方登录分界线
    UIImageView *cuttingView;
    if([WXApi isWXAppInstalled]||[TencentOAuth iphoneQQInstalled]){
        cuttingView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_third_cuttingline"]];
        [self.view addSubview:cuttingView];
    }
    
    UIButton *wxLoginButton,*qqLoginButton;
    
    //     第三方登录-微信
    if ([WXApi isWXAppInstalled]) {
        wxLoginButton = [[UIButton alloc] init];
        [wxLoginButton setImage:[UIImage imageNamed:@"login_wechat"] forState:UIControlStateNormal];
        [wxLoginButton addTarget:self action:@selector(wechatLogin:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:wxLoginButton];
    }
    if ([TencentOAuth iphoneQQInstalled]) {
        qqLoginButton = [[UIButton alloc] init];
        [qqLoginButton setImage:[UIImage imageNamed:@"login_qq"] forState:UIControlStateNormal];
        [qqLoginButton addTarget:self action:@selector(qqLogin:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:qqLoginButton];
    }
    
    [cuttingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.view.mas_bottom).offset(-85);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    if (wxLoginButton) {
        [wxLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_centerX).offset(30);
            make.bottom.mas_equalTo(self.view.mas_bottom).offset(-37.5);
        }];
    }
    if (qqLoginButton) {
        [qqLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.view.mas_centerX).offset(-30);
            make.bottom.mas_equalTo(self.view.mas_bottom).offset(-37.5);
        }];
    }
#endif
    
    [_tfUser mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.right.mas_equalTo(self.view).offset(-25);
        make.top.mas_equalTo(self.view).offset(121-64);
    }];
    [leftImg1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView1);
        make.centerY.mas_equalTo(leftView1);
    }];
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(rightView);
        make.centerY.mas_equalTo(rightView);
        //        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfUser);
        make.right.mas_equalTo(_tfUser);
        make.bottom.mas_equalTo(_tfUser).offset(13);
        make.height.mas_equalTo(0.5);
    }];
    [leftImg2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView2);
        make.centerY.mas_equalTo(leftView2);
    }];
    [_tfPassword mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfUser);
        make.right.mas_equalTo(_tfUser);
        make.top.mas_equalTo(lineView1.mas_bottom).offset(32);
    }];
    [_showPswBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(rightView2);
        make.centerY.mas_equalTo(rightView2);
        //        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfUser);
        make.right.mas_equalTo(_tfPassword);
        make.bottom.mas_equalTo(_tfPassword).offset(13);
        make.height.mas_equalTo(0.5);
    }];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfPassword.mas_bottom).offset(43.5);
        make.left.mas_equalTo(_tfPassword);
        make.right.mas_equalTo(_tfPassword);
        make.height.mas_equalTo(50);
    }];
    [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(loginBtn);
        make.top.mas_equalTo(loginBtn.mas_bottom).offset(32);
    }];
    [forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_tfPassword);
        make.top.mas_equalTo(codeBtn);
    }];
    
    /*
     for develop model
     */
    UIButton *devBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [devBtn setTitle:Localized(@"开发者模式") forState:UIControlStateNormal];
    [devBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [devBtn addTarget:self action:@selector(gotoDevelopModel) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:devBtn];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"DevelopModelOpen"]) {
        devBtn.hidden = YES;
    }
    [devBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-20);
    }];
    
    UITapGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        if (!devBtn.hidden) {
            return;
        }
        self.index++;
        if (self.index >=10) {
            self.index =0;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DevelopModelOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            devBtn.hidden = NO;
        }
    }];
    [self.navigationItem.titleView addGestureRecognizer:tapG];
    // 填充已登录过的账号
    [self fillAccount];
}

- (void)gotoDevelopModel {
    ChangeHostViewController *vc = [[ChangeHostViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)weiXinToLoginIn:(NSNotification *) notification {
    NSLog(@"%@",notification.userInfo);
    NSString *code = notification.userInfo[@"weiXinReturnCode"];
    [self getBindUserOrSocialInformationWithType:@"weixin" code:code openid:nil];
}
- (void)getBindUserOrSocialInformationWithType:(NSString *) type code:(NSString *) code openid:(NSString *) openid {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getBindUserOrSocialInformationWithType:type code:code openid:openid success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"-----%@",dic);
        if (state.status ) {
            ThirdPartyParser *partyParser = [ThirdPartyParser mj_objectWithKeyValues:dic[@"data"]];
            if ([partyParser.status integerValue]==1) {
                // 将guid存到本地沙盒
                NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                [user setObject:partyParser.guid forKey:APPAUTH_KEY];
                [user synchronize];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                NSString *guid = [userDefaults objectForKey:APPAUTH_KEY];
                NSLog(@"guid = %@",guid);
                //                if (![userInfo.username isEqualToString:userName]) {
                //将用户信息存储到本地沙盒
                NSUserDefaults *users = [NSUserDefaults standardUserDefaults];
                NSMutableDictionary *param = [NSMutableDictionary dictionary];
                if (partyParser.username) {
                    [param setObject:partyParser.username forKey:@"username"];
                }
                if (partyParser.nickname) {
                    [param setObject:partyParser.nickname forKey:@"nickname"];
                }
                
                if (partyParser.mobile) {
                    [param setObject:partyParser.mobile forKey:@"mobile"];
                }
                if (partyParser.logo) {
                    [param setObject:partyParser.logo forKey:@"logo"];
                }
                if (partyParser.sex) {
                    [param setObject:partyParser.sex forKey:@"sex"];
                }
                if (partyParser.uid) {
                    [param setObject:partyParser.uid forKey:@"uid"];
                }
                if (partyParser.guid) {
                    [param setObject:partyParser.guid forKey:@"guid"];
                }
                
                if (partyParser.truename) {
                    [param setObject:partyParser.truename forKey:@"truename"];
                }
                
                LoginDataParser *dataPaser = [LoginDataParser mj_objectWithKeyValues:param];
                NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                [users setObject:userData forKey:USERINFO_LOGIN];
                [users synchronize];
                
                //                }
                NSData *data = [user objectForKey:USERINFO_LOGIN];
                LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
                NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,userInfo.uid]];
                //直接登录
                if (guid != nil && ![guid isEqualToString:@""]) {
                    //                    [XSTool showToastWithView:self.view Text:@"登录中..."];
                    [XSTool showProgressHUDTOView:self.view withText:@"登录中..."];
                    [HTTPManager autoLoginWithSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
                        //                        [MBProgressHUD showMessage:@"登录中..." view:self.view];
                        if (state.status) {
                            
                            // 实例化user数据
                            [[UserInstance ShardInstnce] setupUserInfo];
#ifdef ALIYM_AVALABLE
                            // 阿里云旺初始化
                            [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:userInfo.uid passWord:pwd preloginedBlock:^{
                                
                            } successBlock:^{
                                NSLog(@"登录成功");
                                [XSTool hideProgressHUDWithView:self.view];
                                
                                [self dismissViewControllerAnimated:YES completion:nil];
                            } failedBlock:^(NSError *error) {
                                NSLog(@"登录失败 %@",error.description);
                                [XSTool hideProgressHUDWithView:self.view];
                            }];
                            
#elif defined EMIM_AVALABLE
                            //登录环信
                            [[EMClient sharedClient] loginWithUsername:userInfo.uid
                                                              password:pwd
                                                            completion:^(NSString *aUsername, EMError *aError) {
                                                                if (!aError) {
                                                                    NSLog(@"登录成功");
                                                                    [XSTool hideProgressHUDWithView:self.view];
                                                                    
                                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                                } else {
                                                                    NSLog(@"登录失败 ---- %@",aError.errorDescription);
                                                                    [XSTool hideProgressHUDWithView:self.view];
                                                                    [XSTool showToastWithView:self.view Text:aError.errorDescription];
                                                                }
                                                            }];
                            
#else
                            [XSTool hideProgressHUDWithView:self.view];
                            [self dismissViewControllerAnimated:YES completion:nil];
                            [XMPPManager xmppUserLogIn:userInfo.uid complment:^(XIM_STREAMSTATE type) {
                                
                                if (type == XIM_XMPP_CONNECT) {
                                    //                                    [XSTool hideProgressHUDWithView:self.view];
                                    //                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }else{
                                    [XSTool hideProgressHUDWithView:self.view];
                                    NSString * str;
                                    if (type == XIM_XMPP_CONNECTDEFILED) {
                                        str = @"登录失败";
                                    }else if (type == XIM_XMPP_TIMEOUT){
                                        str = @"登录超时";
                                    }else if (type == XIM_XMPP_AUTHFAILD){
                                        str = @"登录失败";
                                    }
                                    //                                    [[UserInstance ShardInstnce] logout];
                                    //                                    [XSTool showToastWithView:self.view Text:str];
                                }
                                
                            }];
#endif
                            
                            
                            
                        }
                    } fail:^(NSError * _Nonnull error) {
                        NSLog(@"()()23-%@", error.description);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:error.description];
                    }];
                }
                
                
            } else {
                //注册绑定
                QuickLandingController *landingController = [[QuickLandingController alloc] init];
                if ([type isEqualToString:@"weixin"]) {
                    landingController.type = type;
                    landingController.partyParser = partyParser;
                } else {
                    
                    landingController.type = type;
                    ThirdPartyParser *qqParser = partyParser;
                    qqParser.nickname = self.nickName;
                    qqParser.logo = self.logo;
                    qqParser.sex = self.sex;
                    landingController.partyParser = qqParser;
                    
                }
                [self.navigationController pushViewController:landingController animated:YES];
                
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)accountChoose:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.accountTableView.hidden = !sender.selected;
        self.autoHideKeyboard = !sender.selected;
    }
    
    CGRect startRect = sender.selected?CGRectMake(25, CGRectGetMaxY(_tfUser.frame)+13, (mainWidth-25*2), 0) : CGRectMake(25, CGRectGetMaxY(_tfUser.frame)+13, (mainWidth-25*2), self.accountArray.count*50);
    CGRect endRect = sender.selected ? CGRectMake(25, CGRectGetMaxY(_tfUser.frame)+13, (mainWidth-25*2), self.accountArray.count*50) : CGRectMake(25,CGRectGetMaxY(_tfUser.frame)+13, (mainWidth-25*2), 0);
    self.accountTableView.frame = startRect;
    [UIView animateWithDuration:0.1 animations:^{
        self.accountTableView.frame = endRect;
    } completion:^(BOOL finished) {
        if (!sender.selected) {
            self.accountTableView.hidden = YES;
            self.autoHideKeyboard = YES;
        }
    }];
}

- (void)showPassword:(UIButton *)sender {
    sender.selected = !sender.selected;
    _tfPassword.secureTextEntry = !sender.selected;
}

- (void)fillAccount {
    self.accountArray = [[UserInstance ShardInstnce] getAccounts];
    if (!self.accountArray || self.accountArray.count == 0) {
        self.rightBtn.hidden = YES;
        return;
    }
    self.rightBtn.hidden = NO;
    NSDictionary *dic = [self.accountArray firstObject];
    NSString *username = dic[@"account"];
    //    NSString *password = dic[@"password"];
    _tfUser.text = username;
    //    _tfPassword.text = @"xxxxxx";
    //    self.savedPwd = password;
    self.isFillAccount = YES;
    
    self.accountTableView.frame = CGRectMake(25, CGRectGetMaxY(_tfUser.frame)+1, mainWidth-2*25, self.accountArray.count*50);
    [self.view addSubview:self.accountTableView];
    self.accountTableView.hidden = YES;
}


/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    self.isFillAccount = NO;
    if (!self.accountTableView.hidden) {
        self.accountTableView.hidden = YES;
        self.autoHideKeyboard = YES;
        self.rightBtn.selected = NO;
    }
    
    if ([textField isEqual:_tfUser]) {
        _tfPassword.text = @"";
        //        if (textField.text.length >= 11) {
        //            textField.text = [textField.text substringToIndex:11];
        //        }
    }
}

- (void)forgetPwdAction
{
    MissPassWordViewController *missVC = [[MissPassWordViewController alloc] init];
    [self.navigationController pushViewController:missVC animated:YES];
}

- (void)codeLoginAction
{
    CodeLoginViewController *clVC = [[CodeLoginViewController alloc] init];
    [self.navigationController pushViewController:clVC animated:YES];
}

- (void)loginAction
{
    if (_tfUser.text.length == 0) {
        [XSTool showToastWithView:self.view Text:@"用户名不能为空"];
        return;
    }
    if (_tfPassword.text.length == 0) {
        [XSTool showToastWithView:self.view Text:@"密码不能为空"];
        return;
    }
    //密码加密
    NSString *encryptPassWord = [RSAEncryptor encryptString:_tfPassword.text];
    [self toLoginWithNumber:_tfUser.text passWord:encryptPassWord];
}

- (void)toLoginWithNumber:(NSString *) number passWord:(NSString *) passWord{
    [self.view endEditing:YES];
    [BaseTool getCountryList];
    [XSTool showProgressHUDTOView:self.view withText:@"登录中..."];
    [HTTPManager doLoginin:number
                  password:passWord
                     email:self.email_text
                    mobile:self.code_text
                    google:self.google_text
                   success:^(NSDictionary * _Nullable dic, resultObject *state) {
                       LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];
                       NSLog(@"%@",dic);
                       self.google_text = @"";
                       self.email_text = @"";
                       self.code_text = @"";
                       if (state.status) {
                           NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
                           NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];
#ifdef ALIYM_AVALABLE
                           // 阿里云旺初始化
                           [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:parser.data.uid passWord:pwd preloginedBlock:^{
                               
                           } successBlock:^{
                               NSLog(@"登录成功");
                               [XSTool hideProgressHUDWithView:self.view];
                               
                               // 将guid存到本地沙盒
                               NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                               [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                               //        [ud synchronize];
                               
                               //将用户信息存储到本地沙盒
                               LoginDataParser *dataPaser = parser.data;
                               NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                               NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                               NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                               [user setObject:userData forKey:USERINFO_LOGIN];
                               [user synchronize];
                               
                               // 存储登录账号信息
                               NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":passWord,@"avatar":dataPaser.logo};
                               [[UserInstance ShardInstnce] saveAccount:accountDic];
                               
                               // 实例化user数据
                               [[UserInstance ShardInstnce] setupUserInfo];
                               
                               [self dismissViewControllerAnimated:YES completion:nil];
                           } failedBlock:^(NSError *error) {
                               NSLog(@"登录失败 %@",error.description);
                               [XSTool hideProgressHUDWithView:self.view];
                           }];
                           
                           // 登录腾讯云
                           NSString *tlsPwd = [pwd substringWithRange:NSMakeRange(8, 16)];
                           [[ILiveLoginManager getInstance] tlsLogin:[NSString stringWithFormat:@"xsy%@",parser.data.uid] pwd:tlsPwd succ:^{
                               NSLog(@"-----> succ");
                           } failed:^(NSString *moudle, int errId, NSString *errMsg) {
                               NSLog(@"-----> fail %@,%d,%@",moudle,errId,errMsg);
                           }];
#elif defined EMIM_AVALABLE
                           //登录环信
                           [[EMClient sharedClient] loginWithUsername:parser.data.uid
                                                             password:pwd
                                                           completion:^(NSString *aUsername, EMError *aError) {
                                                               if (!aError) {
                                                                   NSLog(@"登录成功");
                                                                   [XSTool hideProgressHUDWithView:self.view];
                                                                   
                                                                   // 将guid存到本地沙盒
                                                                   NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                                   [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                                                                   //        [ud synchronize];
                                                                   
                                                                   //将用户信息存储到本地沙盒
                                                                   LoginDataParser *dataPaser = parser.data;
                                                                   NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                                                                   NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                                                                   NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                                                                   [user setObject:userData forKey:USERINFO_LOGIN];
                                                                   [user synchronize];
                                                                   
                                                                   // 存储登录账号信息
                                                                   NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":passWord,@"avatar":dataPaser.logo};
                                                                   [[UserInstance ShardInstnce] saveAccount:accountDic];
                                                                   
                                                                   // 实例化user数据
                                                                   [[UserInstance ShardInstnce] setupUserInfo];
                                                                   
                                                                   [self dismissViewControllerAnimated:YES completion:nil];
                                                                   //                                                    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                   //                                                    appDelegate.window.rootViewController = [XSTool enterMainViewController];
                                                               } else {
                                                                   NSLog(@"登录失败 %@",aError.errorDescription);
                                                                   [XSTool hideProgressHUDWithView:self.view];
                                                                   [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"环信登录失败:%i",aError.code]];
                                                               }
                                                           }];
#else
                           [self configInfor:parser password:passWord];
                           
                           [self xmppLogAction:parser password:passWord];
#endif
                           
                           
                       } else {
                           [XSTool hideProgressHUDWithView:self.view];
                           [XSTool showToastWithView:self.view Text:state.info];
                          
                       }
                   } fail:^(NSError * _Nonnull error) {
                       NSLog(@"fail");
                       self.google_text = @"";
                       self.email_text = @"";
                       self.code_text = @"";
                       [XSTool hideProgressHUDWithView:self.view];
                       [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                       NSLog(@"%@",error.description);
                   }];
    
}

- (void) xmppLogAction:(LoginParser *)parser  password:(NSString *)passWord{
//    @weakify(self);
    [XMPPManager xmppUserLogIn:parser.data.uid complment:^(XIM_STREAMSTATE type) {
//        @strongify(self);
        if (type == XIM_XMPP_CONNECT) {
            //            [XSTool hideProgressHUDWithView:self.view];
            //            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            
            //            [XSTool hideProgressHUDWithView:self.view];
            //            NSString * str = [XMPPManager getStreamConnet:type];
            //            [XSTool showToastWithView:self.view Text:str];
        }
    }];
    
}

- (void) configInfor:(LoginParser *)parser password:(NSString *)passWord{
    NSLog(@"登录成功");
    [XSTool hideProgressHUDWithView:self.view];
    
    // 将guid存到本地沙盒
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];

    //        [ud synchronize];
    
    //将用户信息存储到本地沙盒
    LoginDataParser *dataPaser = parser.data;
    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
    [user setObject:userData forKey:USERINFO_LOGIN];
    [user synchronize];
    
    // 存储登录账号信息
    NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.mobile:dataPaser.username,@"password":passWord,@"avatar":dataPaser.logo};
    [[UserInstance ShardInstnce] saveAccount:accountDic];
    
    // 实例化user数据
    [[UserInstance ShardInstnce] setupUserInfo];
    

    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.accountArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.accountArray[indexPath.row];
    self.accountTableView.hidden = YES;
    _tfUser.text = dic[@"account"];
    //    _tfPassword.text = @"xxxxxx";
    //    self.savedPwd = dic[@"password"];;
    self.isFillAccount = YES;
    self.autoHideKeyboard = YES;
    self.accountTableView.hidden = YES;
    self.rightBtn.selected = NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"AccountCell%ld ",(long)indexPath.row];
    AccountCell *cell = (AccountCell *)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[AccountCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.backgroundColor = BG_COLOR;
    }
    
    NSDictionary *dic = self.accountArray[indexPath.row];
    [cell.imageView getImageWithUrlStr:dic[@"avatar"] andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    [cell setDeleteAccountAction:^{
        [[UserInstance ShardInstnce] deleteAccount:dic];
        self.accountArray = [[UserInstance ShardInstnce] getAccounts];
        [self.accountTableView reloadData];
        
        if (!self.accountArray || self.accountArray.count == 0) {
            self.rightBtn.hidden = YES;
            self.accountTableView.hidden = YES;
        }
        
    }];
    
    cell.textLabel.text = dic[@"account"];
    
    return cell;
}

#pragma mark - accountTableView
- (UITableView *)accountTableView {
    if (!_accountTableView) {
        _accountTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _accountTableView.delegate = self;
        _accountTableView.dataSource = self;
        _accountTableView.bounces = NO;
        _accountTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _accountTableView.backgroundColor = [UIColor whiteColor];
        _accountTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        _accountTableView.layer.masksToBounds = YES;
    }
    return _accountTableView;
}

#pragma mark - wechat
- (void) wechatLogin:(UIButton *)sender
{
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = WXAuthScope; // @"post_timeline,sns"
    req.state = WXAuthReqState;
    req.openID = WXAPPID;
    [WXApi sendAuthReq:req viewController:self delegate:self];
    
}

//通过code获取access_token，openid，unionid
- (void)getWeiXinOpenId:(NSString *)code{
    //    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",AppId,AppSerect,code];
    //
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //        NSURL *zoneUrl = [NSURL URLWithString:url];
    //        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
    //        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            if (data){
    //                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    //                NSString *openID = dic[@"openid"];
    //                NSString *unionid = dic[@"unionid"];
    //            }
    //        });
    //    });
    
}


- (void)qqLogin:(UIButton *)sender

{
    
    if ([TencentOAuth iphoneQQInstalled]) {
        
        tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];   //注册
        
        NSArray* permissions = [NSArray arrayWithObjects:
                                kOPEN_PERMISSION_GET_USER_INFO,
                                kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                                nil];
        [tencentOAuth authorize:permissions]; //授权
        [tencentOAuth incrAuthWithPermissions:permissions];
        
    }else{
        
        [MBProgressHUD showMessage:@"尚未安装QQ客户端" view:self.view];
    }
    
    
}


- (void)tencentDidLogin {
    
    if (tencentOAuth.accessToken && 0 != [tencentOAuth.accessToken length]){
        //  记录登录用户的OpenID、Token以及过期时间
        //token =46983A7B5D5B8F8A01257B69AE258B6D    data =Wed Aug 23 11:42:34 2017
        
        NSLog(@"token =%@    data =%@",tencentOAuth.accessToken,tencentOAuth.expirationDate);
        self.token = tencentOAuth.accessToken;
        self.openId = tencentOAuth.openId;
        
        [tencentOAuth getUserInfo];
        [MBProgressHUD showMessage:@"授权成功" view:self.view];
    }else {
        
        [MBProgressHUD showMessage:@"登录不成功" view:self.view];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled {
    if (cancelled){
        
        [MBProgressHUD showMessage:@"用户取消登录" view:self.view];
    }else {
        
        [MBProgressHUD showMessage:@"登录失败" view:self.view];
    }
}


- (void)tencentDidNotNetWork {
    
    [MBProgressHUD showMessage:@"无网络连接，请设置网络" view:self.view];
}

-(void)getUserInfoResponse:(APIResponse *)response {
    [self analysisResponse:response];
    
}

- (void)analysisResponse:(APIResponse *)notify
{
    if (notify)
    {
        APIResponse *response = notify;
        if (URLREQUEST_SUCCEED == response.retCode && kOpenSDKErrorSuccess == response.detailRetCode)
        {
            self.nickName = response.jsonResponse[@"nickname"];
            self.logo = response.jsonResponse[@"figureurl_qq_1"];
            self.sex = [NSString stringWithFormat:@"%@",[response.jsonResponse[@"gender"] isEqualToString:@"男"]?@"0":@"1"];
            [self getBindUserOrSocialInformationWithType:@"qq" code:nil openid:self.openId];
        }
        else
        {
            NSString *errMsg = [NSString stringWithFormat:@"errorMsg:%@\n%@", response.errorMsg, [response.jsonResponse objectForKey:@"msg"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Localized(@"操作失败") message:errMsg delegate:self cancelButtonTitle:Localized(@"我知道啦") otherButtonTitles: nil];
            [alert show];
        }
    }
}


- (void)dealloc {
    
    NSLog(@"%s",__FUNCTION__);
}

@end
