//
//  UserLoginViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface UserLoginViewController : XSBaseViewController

/** 背景是否是视频 */
@property (nonatomic, assign) BOOL isVideo;

/** 背景图名字 */
@property (nonatomic, copy) NSString * imageName;

@end
