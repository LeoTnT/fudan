//
//  ResetPwdViewController.m
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ResetPwdViewController.h"
#import "LoginModel.h"
#import "RSAEncryptor.h"

@interface ResetPwdViewController ()
{
    NSString *_mobile;
    NSString *_smsVerify;
    
    UITextField *_tfPwd;
    UITextField *_tfPwdAgain;
    XSCustomButton *_finishBtn;
}
@end

@implementation ResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = Localized(@"forget_pwd");
    self.autoHideKeyboard = YES; // 点击空白处隐藏键盘
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    // textField-leftView
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView1 addSubview:leftImg1];
    // 手机号码输入框
    _tfPwd = [[UITextField alloc] init];
    [self.view addSubview:_tfPwd];
    _tfPwd.placeholder = Localized(@"请设置6~20位登录密码");
    _tfPwd.font = [UIFont systemFontOfSize:15];
    _tfPwd.leftView = leftView1;
    _tfPwd.secureTextEntry=YES;
    _tfPwd.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView1= [[UIView alloc] init];
    [self.view addSubview:lineView1];
    lineView1.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 28)];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_password"]];
    [leftView2 addSubview:leftImg2];
    // 验证码输入框
    _tfPwdAgain = [[UITextField alloc] init];
    [self.view addSubview:_tfPwdAgain];
    _tfPwdAgain.placeholder = Localized(@"请确认登录密码");
    _tfPwdAgain.font = [UIFont systemFontOfSize:15];
    _tfPwdAgain.leftView = leftView2;
    _tfPwdAgain.secureTextEntry=YES;
    _tfPwdAgain.leftViewMode = UITextFieldViewModeAlways;
    UIView *lineView2= [[UIView alloc] init];
    [self.view addSubview:lineView2];
    lineView2.backgroundColor =[UIColor hexFloatColor:@"dddddd"];
    // 登录
    _finishBtn=[[XSCustomButton alloc] initWithTitle:Localized(@"material_dialog_positive_text") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] cornerRadius:4 backGroundColor:mainColor hBackGroundColor:HighLightColor_Main];
    [_finishBtn addTarget:self action:@selector(finishAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_finishBtn];
    //设置约束
    [_tfPwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(self.view).offset(121-64);
        make.right.mas_equalTo(self.view).offset(-25);
    }];
    [leftImg1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView1);
        make.centerY.mas_equalTo(leftView1);
    }];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.bottom.mas_equalTo(_tfPwd.mas_bottom).offset(13);
        make.width.mas_equalTo(_tfPwd);
        make.height.mas_equalTo(0.5);
    }];
    [_tfPwdAgain mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_tfPwd);
        make.right.mas_equalTo(_tfPwd);
        make.top.mas_equalTo(lineView1.mas_bottom).offset(32);
    }];
    [leftImg2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftView2);
        make.centerY.mas_equalTo(leftView2);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView1);
        make.size.mas_equalTo(lineView1);
        make.bottom.mas_equalTo(_tfPwdAgain.mas_bottom).offset(13);
    }];
    [_finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tfPwdAgain.mas_bottom).offset(43.5);
        make.left.mas_equalTo(lineView2);
        make.right.mas_equalTo(lineView2);
        make.height.mas_equalTo(50);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (instancetype)initWithMobile:(NSString *)mobile smsVerify:(NSString *)smsVerify
{
    self = [super init];
    if (self) {
        _mobile = mobile;
        _smsVerify = smsVerify;
    }
    return self;
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if ([_tfPwd.text isEqualToString:_tfPwdAgain.text]) {
        _finishBtn.enabled = YES;
    } else {
        _finishBtn.enabled = NO;
    }
}

- (void)popToLogin {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)finishAction
{
    if (!(_tfPwd.text.length && _tfPwdAgain.text.length)) {
        return;
    }
    if(![_tfPwd.text isEqualToString:_tfPwdAgain.text]){
        [XSTool showToastWithView:self.view Text:Localized(@"密码和确认密码不一致")];
        return;
    }
    [XSTool showProgressHUDWithView:self.view];
    NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:_tfPwd.text]];
    [HTTPManager resetLoginPwdWithMobile:_mobile pwdType:[NSString stringWithFormat:@"%d",PasswordLogin] smsVerify:_smsVerify  andNewPwd:encryptOldPassWord success:^(NSDictionary *dic, resultObject *state){
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            // 修改密码成功
            [XSTool showToastWithView:self.view Text:Localized(@"密码修改成功，请重新登录")];
            [self performSelector:@selector(popToLogin) withObject:nil afterDelay:2.0f];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"密码修改失败"];
    }];
}

@end
