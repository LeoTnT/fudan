//
//  QuickLandingController.h
//  App3.0
//
//  Created by apple on 2017/5/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginModel.h"

@interface QuickLandingController : XSBaseViewController

/**类型*/
@property (nonatomic,copy) NSString *type;

@property (nonatomic, strong) ThirdPartyParser *partyParser;
@end
