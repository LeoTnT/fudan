//
//  QuickLandingController.m
//  App3.0
//
//  Created by apple on 2017/5/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "QuickLandingController.h"
#import "UIImage+XSWebImage.h"
#import "LoginModel.h"
#import "UserInstance.h"
#import "AppDelegate.h"
#import "RegisterModel.h"
#import "RSAEncryptor.h"
#import "AgreementViewController.h"
typedef NS_ENUM(NSUInteger, SureType) {
    SureTypeCreate,
    SureTypeBind
};
@interface QuickLandingController ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *phone;
@property (nonatomic, strong) UIButton *getCode;
@property (nonatomic, assign) SureType sureType;
@property (nonatomic, strong) UITextField *resetPassword;
@property (nonatomic, strong) UIButton *agreement;

@property (nonatomic ,strong)dispatch_source_t timer;
@end

@implementation QuickLandingController
{
    UIView *backView;
    UIView * backButton;
    UITextField *password;
    UITextField *authCode;
    CGFloat space;
    BOOL _isTimer;// 是否在倒计时
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.autoHideKeyboard = YES;
    
    space = 15;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self creatContentView];
    self.sureType = SureTypeCreate;
    
    
}

- (void)changeAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    for (UIButton *btn in backButton.subviews) {
        if (btn.tag != sender.tag) {
            btn.backgroundColor = [UIColor groupTableViewBackgroundColor];
            btn.selected = NO;
            self.getCode.hidden = NO;
            authCode.hidden = NO;
            self.resetPassword.hidden = NO;
            self.agreement.hidden = NO;
            self.sureType = SureTypeCreate;
        }else{
            self.getCode.hidden = YES;
            authCode.hidden = YES;
            self.resetPassword.hidden = YES;
            self.agreement.hidden = YES;
            sender.backgroundColor = [UIColor whiteColor];
            self.sureType = SureTypeBind;
        }
    }
    
}


- (void) creatContentView {
    
    _isTimer = NO;
    backView = UIView.new;
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    
    backButton = UIView.new;
    backButton.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //    backButton.alpha = .6;
    backButton.layer.masksToBounds = YES;
    [backView addSubview:backButton];
    
    
    UIButton *one = [self creatTopButton:@"创建新的账号" superView:backButton sel:@selector(changeAction:) tag:0];
    one.selected = YES;
    one.backgroundColor = [UIColor whiteColor];
    UIButton *two = [self creatTopButton:@"绑定已有账号" superView:backButton sel:@selector(changeAction:) tag:1];
    
    
    
    
    UIImageView *headImage = UIImageView.new;
    [headImage getImageWithUrlStr:self.partyParser.logo andDefaultImage:[UIImage imageNamed:@"chat_single"]];
    [backView addSubview:headImage];
    
    
    UILabel *nickName = UILabel.new;
    
    nickName.text = self.partyParser.nickname;
    [backView addSubview:nickName];
    
    
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.edges.mas_equalTo(UIEdgeInsetsMake(64+space, space, space, space));
        make.left.mas_equalTo(self.view).offset(space);
        make.right.mas_equalTo(self.view).offset(-space);
        make.top.mas_equalTo(self.view).offset(space);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-space);
    }];
    
    [backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.top.left.mas_equalTo(backView);
        make.height.mas_equalTo(50);
    }];
    
    [one mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backButton.mas_left);
        make.top.mas_equalTo(backButton.mas_top).offset(15);
        make.width.mas_equalTo(backButton).dividedBy(3);
        make.height.mas_equalTo(40);
    }];
    
    [two mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(one.mas_right);
        make.top.width.height.mas_equalTo(one);
        
    }];
    
    
    
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backView).offset(space);
        make.top.mas_equalTo(backView).offset(80);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    [nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headImage.mas_right).offset(space);
        make.centerY.mas_equalTo(headImage.mas_centerY);
        make.right.mas_equalTo(backView);
    }];
    
    [self.view layoutIfNeeded];
    
    self.phone = [self createTextFieldWith:CGRectMake(space, CGRectGetMaxY(headImage.frame) +space, mainWidth - 6 *space, 40) viewFrame:CGRectMake(10, 0, 80, 40) labelFrame:CGRectMake(10, 0, 80, 40) placeHolder:Localized(@"input_mobile") leftViewText:@"手机号:" textAlignment:NSTextAlignmentLeft keyboardType:UIKeyboardTypePhonePad];
    
    [backView addSubview:self.phone];
    
    
    password = [self createTextFieldWith:CGRectMake(space, CGRectGetMaxY(self.phone.frame) +space, mainWidth - 6 *space, 40) viewFrame:CGRectMake(10, 0, 80, 40) labelFrame:CGRectMake(10, 0, 80, 40) placeHolder:@"请输入6-20位初始密码" leftViewText:@"密码：" textAlignment:NSTextAlignmentLeft keyboardType:UIKeyboardTypeDefault];
    password.delegate = self;
    password.secureTextEntry = YES;
    [backView addSubview:password];
    self.resetPassword = [self createTextFieldWith:CGRectMake(space, CGRectGetMaxY(password.frame) +space, mainWidth - 6 *space, 40) viewFrame:CGRectMake(10, 0, 80, 40) labelFrame:CGRectMake(10, 0, 80, 40) placeHolder:@"请再次输入密码" leftViewText:@"确认密码：" textAlignment:NSTextAlignmentLeft keyboardType:UIKeyboardTypeDefault];
    self.resetPassword.delegate = self;
    self.resetPassword.secureTextEntry = YES;
    [backView addSubview:self.resetPassword];
    
    authCode = UITextField.new;
    authCode.delegate = self;
    authCode.placeholder = Localized(@"sms_send_dialog_sms_hint");
    authCode.keyboardType = UIKeyboardTypeNumberPad;
    authCode.borderStyle = UITextBorderStyleRoundedRect;
    [backView addSubview:authCode];
    
    
    self.getCode= [UIButton buttonWithType:UIButtonTypeCustom];
    [self.getCode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.getCode setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
    //    getCode.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.getCode.layer.cornerRadius = 5;
    self.getCode.layer.borderWidth = 1;
    self.getCode.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.getCode.layer.masksToBounds = YES;
    [self.getCode addTarget:self action:@selector(getCodeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:self.getCode];
    
    nickName.font = self.getCode.titleLabel.font =  authCode.font = [UIFont qsh_systemFontOfSize:14];
    
    
    //    UIImageView *ii = UIImageView.new;
    //    ii.image = [UIImage imageNamed:@"user_site_check_yes"];
    //    [backView addSubview:ii];
    //
    self.agreement = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.agreement setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateNormal];
    [self.agreement setTitle:[NSString stringWithFormat:@"我已看过并同意《%@会员注册协议》",APP_NAME] forState:UIControlStateNormal];
    self.agreement.titleLabel.font = [UIFont qsh_systemFontOfSize:14];
    [self.agreement setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.agreement.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.agreement addTarget:self action:@selector(agreementAction) forControlEvents:UIControlEventTouchUpInside    ];
    [backView addSubview:self.agreement];
    
    [self.agreement setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, mainWidth - 30-30)];
    [self.agreement setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
    sureButton.backgroundColor = [UIColor redColor];
    sureButton.layer.cornerRadius = 5;
    sureButton.layer.masksToBounds = YES;
    [sureButton addTarget:self action:@selector(sureToRegister) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:sureButton];
    
    
    [authCode mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.resetPassword.mas_bottom).offset(space);
        make.left.mas_equalTo(self.resetPassword.mas_left);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(self.resetPassword.mas_width).dividedBy(2);
    }];
    
    [self.getCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(authCode.mas_right).offset(space);
        make.height.top.mas_equalTo(authCode);
        make.width.mas_equalTo(authCode).dividedBy(1.5);
    }];
    
    [self.agreement mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(authCode.mas_left);
        make.top.mas_equalTo(authCode.mas_bottom).offset(space);
        make.right.mas_equalTo(backView.mas_right);
    }];
    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backView).offset(space);
        make.right.mas_equalTo(backView.mas_right).offset(-space);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(backView.mas_bottom).offset(-space);
    }];
    
}

- (void)agreementAction {

    //查看协议
    AgreementViewController *controller = [[AgreementViewController alloc] initWithAgreementType:AgreementTypeReg];
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (void)getCodeButtonAction:(UIButton *) sender {
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *phoneString = [[NSString alloc]initWithString:[self.phone.text stringByTrimmingCharactersInSet:whiteSpace]];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"code_tip") message:phoneString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        sender.enabled = NO; // 设置按钮为不可点击
        // 获取验证码
        [HTTPManager regGetSmsVerify:phoneString success:^(NSDictionary *dic, resultObject *state) {
            // 验证码发送成功

            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"发送成功"];
                _isTimer = YES; // 设置倒计时状态为YES
                sender.enabled = NO; // 设置按钮为不可点击
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
                [XSTool setTimeQueue:queue dispatch_source:_timer walltime:59 stop:^{
                    [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                    sender.enabled = YES; // 设置按钮可点击
                    _isTimer = NO; // 倒计时状态为NO
                } otherAction:^(int time) {
                    [sender setTitle:[NSString stringWithFormat:@"重新发送(%.2d)", time] forState:UIControlStateNormal];
                }];

            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                sender.enabled = YES; // 设置按钮为可点击
            }
        } fail:^(NSError * _Nonnull error) {
            NSLog(@"验证码发送失败");
            [XSTool showToastWithView:self.view Text:@"验证码发送失败"];

            sender.enabled = YES; // 设置按钮为可点击
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.timer) dispatch_source_cancel(self.timer);
}

- (void)sureToRegister {
    [XSTool hideProgressHUDWithView:self.view];
    
    if (self.sureType==SureTypeCreate) {
        if ([password.text isEqualToString:@""]||password.text.length==0||[self.phone.text isEqualToString:@""]||self.phone.text.length==0||[authCode.text isEqualToString:@""]||authCode.text.length==0) {
            [XSTool showToastWithView:self.view Text:@"请完善信息"];

        } else {
            if ([password.text isEqualToString:self.resetPassword.text]) {
                [self BindUserInfoWithIscreate:@"1" type:self.type openid:self.partyParser.openid token:nil unionid:self.partyParser.unionid name:self.phone.text pass:[RSAEncryptor encryptString:password.text] nickname:self.partyParser.nickname logo:self.partyParser.logo sex:self.partyParser.sex verify:authCode.text];
            } else {
                [XSTool showToastWithView:self.view Text:@"密码不一致"];

            }
            
        }
 
    } else {
        if ([password.text isEqualToString:@""]||password.text.length==0||[self.phone.text isEqualToString:@""]||self.phone.text.length==0) {
            [XSTool showToastWithView:self.view Text:@"请完善信息"];
        } else {
            [self BindUserInfoWithIscreate:@"0" type:self.type openid:self.partyParser.openid token:nil unionid:self.partyParser.unionid name:self.phone.text pass:[RSAEncryptor encryptString:password.text] nickname:self.partyParser.nickname logo:self.partyParser.logo sex:self.partyParser.sex verify:nil];
        }

    }
}

- (void)BindUserInfoWithIscreate:(NSString *)iscreate type:(NSString *)type openid:(NSString *)openid token:(NSString *)token unionid:unionid name:(NSString *)name pass:(NSString *)pass nickname:(NSString *) nickname logo:(NSString *) logo sex:(NSString *) sex verify:(NSString *)verify {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:iscreate forKey:@"iscreate"];
    [dictionary setObject:type forKey:@"type"];
    [dictionary setObject:unionid forKey:@"unionid"];

    [dictionary setObject:name forKey:@"name"];
    [dictionary setObject:pass forKey:@"pass"];

    if (openid) {
        [dictionary setObject:openid forKey:@"openid"];
        [dictionary setObject:nickname forKey:@"nickname"];
        [dictionary setObject:logo forKey:@"logo"];
        [dictionary setObject:sex forKey:@"sex"];
    }
    if ([iscreate integerValue]==1) {
        [dictionary setObject:verify forKey:@"verify"];
    }
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager bindUserBySocialWithParam:dictionary success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self autoLogin];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    
    }];
    

}

- (void)autoLogin {
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *phoneString = [[NSString alloc]initWithString:[self.phone.text stringByTrimmingCharactersInSet:whiteSpace]];
    NSString *encryptPassWord = [RSAEncryptor encryptString:password.text];
    [XSTool showToastWithView:self.view Text:@"登录中..."];
    
    [HTTPManager doLoginin:phoneString password:encryptPassWord success:^(NSDictionary * _Nullable dic, resultObject *state) {
        LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
            NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];
#ifdef ALIYM_AVALABLE
            // 阿里云旺初始化
            [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:parser.data.uid passWord:pwd preloginedBlock:^{
                
            } successBlock:^{
                [XSTool hideProgressHUDWithView:self.view];
                
                // 将guid存到本地沙盒
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                //        [ud synchronize];
                
                //将用户信息存储到本地沙盒
                LoginDataParser *dataPaser = parser.data;
                NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                [user setObject:userData forKey:USERINFO_LOGIN];
                [user synchronize];
                // 实例化user数据
                [[UserInstance ShardInstnce] setupUserInfo];
                [self dismissViewControllerAnimated:YES completion:nil];
                
            } failedBlock:^(NSError *error) {
                NSLog(@"登录失败 %@",error.description);
                [XSTool hideProgressHUDWithView:self.view];
            }];
            
#elif defined EMIM_AVALABLE
            //登录环信
            [[EMClient sharedClient] loginWithUsername:parser.data.uid
                                              password:pwd
                                            completion:^(NSString *aUsername, EMError *aError) {
                                                if (!aError) {
                                                    NSLog(@"登录成功");
                                                    [XSTool hideProgressHUDWithView:self.view];
                                                    
                                                    // 将guid存到本地沙盒
                                                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                                                    //        [ud synchronize];
                                                    
                                                    //将用户信息存储到本地沙盒
                                                    LoginDataParser *dataPaser = parser.data;
                                                    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                                                    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                                                    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                                                    [user setObject:userData forKey:USERINFO_LOGIN];
                                                    [user synchronize];
                                                    // 实例化user数据
                                                    [[UserInstance ShardInstnce] setupUserInfo];
                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                    
                                                } else {
                                                    NSLog(@"登录失败 %@",aError.errorDescription);
                                                    [XSTool hideProgressHUDWithView:self.view];
                                                }
                                            }];
            
#else
            [self configInfor:parser password:encryptPassWord];
            [self xmppLogWithID:parser.data.uid logModel:parser];
#endif
            
            
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        NSLog(@"fail");
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];

    }];
    
}

- (void) configInfor:(LoginParser *)parser password:(NSString *)passWord{
    NSLog(@"登录成功");
    [XSTool hideProgressHUDWithView:self.view];
    
    // 将guid存到本地沙盒
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
    //        [ud synchronize];
    
    //将用户信息存储到本地沙盒
    LoginDataParser *dataPaser = parser.data;
    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
    [user setObject:userData forKey:USERINFO_LOGIN];
    [user synchronize];
    
    // 实例化user数据
    [[UserInstance ShardInstnce] setupUserInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) xmppLogWithID:(NSString *)ID logModel:(LoginParser*)parser {
    
    [XMPPManager xmppUserLogIn:ID complment:^(XIM_STREAMSTATE type) {
        if (type == XIM_XMPP_CONNECT) {
            
        }else{
            
        }
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.5f];
    CGFloat offf = mainHeight - textField.mj_y - 120;
    if (offf - 250 <= 0) self.view.frame =CGRectMake(0, offf - 250, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    [self textFieldResign];
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent*)event {
    [super touchesBegan:touches withEvent:event];
    
    [self textFieldResign];
}


- (void)textFieldResign {
    [password resignFirstResponder];
    [authCode resignFirstResponder];
}


- (UITextField *) createTextFieldWith:(CGRect)frame viewFrame:(CGRect)viewframe
                           labelFrame:(CGRect)labelFrame
                          placeHolder:(NSString *)placeHolder
                         leftViewText:(NSString *)text
                        textAlignment:(NSTextAlignment)textAlignment
                         keyboardType:(UIKeyboardType)keyboardType{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.text = text;
    [leftLabel sizeToFit];
    UIView *leftView = [[UIView alloc] init];
    [leftView addSubview:leftLabel];
    
    textField.textAlignment = textAlignment;
    textField.keyboardType = keyboardType;
    leftLabel.frame = labelFrame;
    //    CGFloat www = leftLabel.frame.size.width;
    //    CGRectMake(0, 0, www, 50)
    leftView.frame = viewframe;
    
    leftLabel.textColor = [UIColor lightGrayColor];
    leftLabel.font = [UIFont qsh_systemFontOfSize:15];
    [textField setLeftView:leftView];
    
    textField.placeholder = placeHolder;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.borderColor = [UIColor blueColor].CGColor;
    textField.font = leftLabel.font;
    return textField;
}


- (UIButton *) creatTopButton:(NSString *)title superView:(UIView *)superView sel:(SEL)selected tag:(NSInteger)tag{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont qsh_systemFontOfSize:14];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    button.tag = tag;
    [button addTarget:self action:selected forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 2;
    button.layer.masksToBounds = YES;
    [superView addSubview:button];
    
    
    return button;
    
}

@end
