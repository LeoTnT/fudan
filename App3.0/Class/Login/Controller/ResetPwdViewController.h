//
//  ResetPwdViewController.h
//  App3.0
//
//  Created by mac on 17/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPwdViewController : XSBaseViewController
- (instancetype)initWithMobile:(NSString *)mobile smsVerify:(NSString *)smsVerify;
@end
