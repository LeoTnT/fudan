//
//  UserLoginViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserLoginViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AccountLoginView.h"
#import "AccountRegisterView.h"

@interface UserLoginViewController ()<AccountLoginViewDelegate,AccountRegisterViewDelegate>
@property (nonatomic, strong) AVPlayerViewController * loginVideoVC;
/** 登录界面 */
@property (nonatomic, strong) AccountLoginView * userLoginV;
/** 注册界面 */
@property (nonatomic, strong) AccountRegisterView * userRegisterV;
@end

@implementation UserLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    NSString *urlStr = [[NSBundle mainBundle]pathForResource:@"user_Login.mp4" ofType:nil];
    NSURL *url;
    if (urlStr.length > 0) {
        url = [NSURL fileURLWithPath:urlStr];
        
        self.loginVideoVC = [[AVPlayerViewController alloc] init];
        self.loginVideoVC.showsPlaybackControls = NO;
        //        _loginVideoVC.videoGravity = AVLayerVideoGravityResizeAspect;
        self.loginVideoVC.videoGravity = AVLayerVideoGravityResize;
        self.loginVideoVC.view.frame = self.view.frame;
        [self.view addSubview:self.loginVideoVC.view];
        [self addChildViewController:self.loginVideoVC];
        self.loginVideoVC.player = [[AVPlayer alloc] initWithURL:url];
        
        if (self.isVideo) {
            [self.loginVideoVC.player play];
        } else {
            if (self.imageName) {
                [self settingBgImageView];
            }
        }
    } else {
        if (self.imageName) {
            [self settingBgImageView];
        } else {
            self.view.backgroundColor = [UIColor darkGrayColor];
        }
    }
    
    [self makeUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(runLoopTheMovie) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

-(void)settingBgImageView {
    UIImageView * bgImageV = [[UIImageView alloc] init];
    bgImageV.image = [UIImage imageNamed:self.imageName];
    [self.view addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    // 隐藏nav bar
//    self.navigationController.navigationBarHidden = YES;
//}

-(void)backBtnWasClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) makeUI {
    self.userLoginV = [[AccountLoginView alloc] init];
    self.userLoginV.delegate = self;
    [self.view addSubview:self.userLoginV];
    [self.userLoginV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        make.top.left.bottom.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    self.userRegisterV = [[AccountRegisterView alloc] init];
    self.userRegisterV.delegate = self;
    [self.view addSubview:self.userRegisterV];

    [self.userRegisterV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.mas_equalTo(self.userLoginV);
        make.left.mas_equalTo(self.userLoginV.mas_right);
    }];
}

#pragma mark 登录界面代理
-(void)loginRightBtnWasClicked
{
    [self toLoginOrRegister:NO];
}
-(void)loginBtnWasClick
{
    [self toLoginOrRegister:NO];
}

#pragma mark 注册界面代理
-(void)registerRightBtnWasClicked
{
    [self toLoginOrRegister:YES];
}
-(void)registerBtnWasClick
{
    [self toLoginOrRegister:YES];
}

- (void)toLoginOrRegister:(BOOL)isLogin {
    CGFloat leading;
    if (isLogin) {
        leading = 0.0;
    } else {
        leading = -SCREEN_WIDTH;
    }
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        [self.userLoginV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leading);
        }];
        [self.view layoutIfNeeded];
    }];
}

-(void)runLoopTheMovie {
    XSLog(@"播放完成");
    [self.loginVideoVC.player seekToTime:kCMTimeZero];
    [self.loginVideoVC.player play];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
