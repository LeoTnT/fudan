//
//  AccountRegisterView.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AccountRegisterView.h"

@interface AccountRegisterView ()<UITextFieldDelegate>

/** 手机号或系统编号 */
@property (nonatomic, strong) UITextField * phoneNumTF;
/** 验证码 */
@property (nonatomic, strong) UITextField * verifyCodeTF;
/** 密码 */
@property (nonatomic, strong) UITextField * passWordTF;

/** 阅读服务协议勾选按钮 */
@property (nonatomic, strong) UIButton * serviceAgreementSelectBtn;
@end

@implementation AccountRegisterView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
        //        self.isKeyboardShow = NO;
    }
    return self;
}

-(void)setUpUI {
    self.backgroundColor = [UIColor clearColor];
    
    UIView * navView = [[UIView alloc] init];
    navView.backgroundColor = [UIColor clearColor];
    [self addSubview:navView];
    
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(20);
        make.height.mas_equalTo(44);
    }];
    
    //title
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = Localized(@"enter_register");
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    [navView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(navView);
    }];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:Localized(@"enter_login") forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightBtn];
    
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(navView).offset(-8);
        make.centerY.mas_equalTo(navView);
        make.size.mas_equalTo(CGSizeMake(50, 35));
    }];
    
    UIView * phoneNumView = [[UIView alloc] init];
    phoneNumView.backgroundColor = [UIColor clearColor];
    [self addSubview:phoneNumView];
    [phoneNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(navView.mas_bottom).offset(50);
        make.height.mas_equalTo(60);
    }];
    
    //认证通过ImageView
    UIImageView * phoneImgV = [[UIImageView alloc] init];
    phoneImgV.image = [UIImage imageNamed:@"login_phone"];
    [phoneNumView addSubview:phoneImgV];
    
    [phoneImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneNumView).offset(29);
        make.centerY.mas_equalTo(phoneNumView);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
//    UIButton * accountDownBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [accountDownBtn setImage:[UIImage imageNamed:@"logoin_pull_down"] forState:UIControlStateNormal];
//    [accountDownBtn addTarget:self action:@selector(downBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    [phoneNumView addSubview:accountDownBtn];
//    [accountDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(phoneNumView).offset(-29);
//        make.centerY.mas_equalTo(phoneNumView);
//        make.size.mas_equalTo(CGSizeMake(35, 35));
//    }];
    
    //手机号
    self.phoneNumTF = [[UITextField alloc] init];
    self.phoneNumTF.textColor = [UIColor whiteColor];
    self.phoneNumTF.font = [UIFont systemFontOfSize:15];
    self.phoneNumTF.returnKeyType = UIReturnKeyNext;
    self.phoneNumTF.delegate = self;
    [phoneNumView addSubview:self.phoneNumTF];
    
    [self.phoneNumTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneImgV.mas_right).offset(10);
        make.right.mas_equalTo(phoneNumView).offset(-40);
        make.centerY.mas_equalTo(phoneNumView);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    //分割线
    UIView * line1View = [[UIView alloc] init];
    line1View.backgroundColor = [UIColor whiteColor];
    [phoneNumView addSubview:line1View];
    
    [line1View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneNumView).offset(30);
        make.right.mas_equalTo(phoneNumView).offset(-30);
        make.bottom.mas_equalTo(self.phoneNumTF).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    UIView * veriyCodeBgView = [[UIView alloc] init];
    veriyCodeBgView.backgroundColor = [UIColor clearColor];
    [self addSubview:veriyCodeBgView];
    [veriyCodeBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(phoneNumView.mas_bottom);
        make.height.mas_equalTo(60);
    }];
    
    //认证通过ImageView
    UIImageView * veriyImgV = [[UIImageView alloc] init];
    veriyImgV.image = [UIImage imageNamed:@"login_code"];
    [veriyCodeBgView addSubview:veriyImgV];
    
    [veriyImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(veriyCodeBgView).offset(29);
        make.centerY.mas_equalTo(veriyCodeBgView);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    //获取验证码按钮
    UIButton * getVerifyCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [getVerifyCodeBtn setTitle:Localized(@"register_sms") forState:UIControlStateNormal];
    [getVerifyCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    
    [getVerifyCodeBtn addTarget:self action:@selector(getVerifyCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [veriyCodeBgView addSubview:getVerifyCodeBtn];
    
    [getVerifyCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(veriyCodeBgView).offset(-25);
        make.centerY.mas_equalTo(veriyCodeBgView);
        make.size.mas_equalTo(CGSizeMake(80, 25));
    }];
    
    //手机号
    self.verifyCodeTF = [[UITextField alloc] init];
    self.verifyCodeTF.textColor = [UIColor whiteColor];
    self.verifyCodeTF.font = [UIFont systemFontOfSize:15];
    self.verifyCodeTF.returnKeyType = UIReturnKeyNext;
    self.verifyCodeTF.delegate = self;
    [phoneNumView addSubview:self.verifyCodeTF];
    
    [self.verifyCodeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(veriyImgV.mas_right).offset(10);
        make.right.mas_equalTo(getVerifyCodeBtn).offset(-10);
        make.centerY.mas_equalTo(veriyCodeBgView);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    //分割线
    UIView * line2View = [[UIView alloc] init];
    line2View.backgroundColor = [UIColor whiteColor];
    [veriyCodeBgView addSubview:line2View];
    
    [line2View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(veriyCodeBgView).offset(30);
        make.right.mas_equalTo(veriyCodeBgView).offset(-30);
        make.bottom.mas_equalTo(self.verifyCodeTF).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    
    UIView * pwdView = [[UIView alloc] init];
    pwdView.backgroundColor = [UIColor clearColor];
    [self addSubview:pwdView];
    [pwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(veriyCodeBgView.mas_bottom);
        make.height.mas_equalTo(60);
    }];
    
    //认证通过ImageView
    UIImageView * pwdImgV = [[UIImageView alloc] init];
    pwdImgV.image = [UIImage imageNamed:@"login_password"];
    [pwdView addSubview:pwdImgV];
    
    [pwdImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdView).offset(29);
        make.centerY.mas_equalTo(pwdView);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    //密码
    self.passWordTF = [[UITextField alloc] init];
    self.passWordTF.textColor = [UIColor whiteColor];
    self.passWordTF.font = [UIFont systemFontOfSize:15];
    self.passWordTF.returnKeyType = UIReturnKeyDone;
    self.passWordTF.delegate = self;
    [pwdView addSubview:self.passWordTF];
    
    [self.passWordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdImgV.mas_right).offset(10);
        make.right.mas_equalTo(pwdView).offset(-40);
        make.centerY.mas_equalTo(pwdView);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    //分割线
    UIView * line3View = [[UIView alloc] init];
    line3View.backgroundColor = [UIColor whiteColor];
    [pwdView addSubview:line3View];
    
    [line3View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdView).offset(30);
        make.right.mas_equalTo(pwdView).offset(-30);
        make.bottom.mas_equalTo(self.passWordTF).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = RGB(225, 225, 225);
    
    NSAttributedString *placeholder1 = [[NSAttributedString alloc] initWithString:Localized(@"请输入手机号/系统编号") attributes:attrs];
    NSAttributedString *placeholder2 = [[NSAttributedString alloc] initWithString:Localized(@"sms_send_dialog_sms_hint") attributes:attrs];
    NSAttributedString *placeholder3 = [[NSAttributedString alloc] initWithString:Localized(@"请输入密码") attributes:attrs];
    
    self.phoneNumTF.attributedPlaceholder = placeholder1;
    self.verifyCodeTF.attributedPlaceholder = placeholder2;
    self.passWordTF.attributedPlaceholder = placeholder3;
    
    //注册按钮
    UIButton * loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitle:Localized(@"enter_register") forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    loginBtn.layer.cornerRadius = 4;
    loginBtn.layer.masksToBounds = YES;
    [loginBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:loginBtn];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdView.mas_bottom).offset(35);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(FontNum(325), FontNum(50)));
    }];
    
    //阅读服务协议勾选按钮
    self.serviceAgreementSelectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.serviceAgreementSelectBtn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [self.serviceAgreementSelectBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    self.serviceAgreementSelectBtn.selected = YES;
    [self.serviceAgreementSelectBtn addTarget:self action:@selector(serviceAgreementSelectBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.serviceAgreementSelectBtn];
    
    [self.serviceAgreementSelectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loginBtn.mas_bottom).offset(25);
        make.left.mas_equalTo(loginBtn);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    //注册即视为
    UILabel * lab1 = [[UILabel alloc] init];
    lab1.text = Localized(@"注册即视为");
    lab1.font = [UIFont systemFontOfSize:12];
    lab1.textColor = [UIColor whiteColor];
    [self addSubview:lab1];
    [lab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.serviceAgreementSelectBtn.mas_right).offset(1);
        make.centerY.mas_equalTo(self.serviceAgreementSelectBtn);
    }];
    
    //福旦APP服务协议
    UIButton * serviceAgreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [serviceAgreementBtn setTitle:Localized(@"《福旦APP服务协议》") forState:UIControlStateNormal];
    [serviceAgreementBtn setTitleColor:RGB(28, 145, 244) forState:UIControlStateNormal];
    serviceAgreementBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    [serviceAgreementBtn addTarget:self action:@selector(serviceAgreementBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:serviceAgreementBtn];
    
    [serviceAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lab1.mas_right).offset(1);
        make.centerY.mas_equalTo(self.serviceAgreementSelectBtn);
    }];
}

-(void)rightBtnClick {
    if ([self.delegate respondsToSelector:@selector(registerRightBtnWasClicked)]) {
        [self.delegate registerRightBtnWasClicked];
    }
}
-(void)getVerifyCodeBtnClick {
    if ([self.delegate respondsToSelector:@selector(registerGetVerifyCodeBtnWasClick)]) {
        [self.delegate registerGetVerifyCodeBtnWasClick];
    }
}
-(void)registerBtnClick {
    if ([self.delegate respondsToSelector:@selector(registerBtnWasClick)]) {
        [self.delegate registerBtnWasClick];
    }
}
#pragma mark 服务协议
-(void)serviceAgreementSelectBtnClick {
    self.serviceAgreementSelectBtn.selected = !self.serviceAgreementSelectBtn.selected;
}
-(void)serviceAgreementBtnClick
{
    if ([self.delegate respondsToSelector:@selector(registerServiceAgreementBtnWasClick)]) {
        [self.delegate registerServiceAgreementBtnWasClick];
    }
}

@end
