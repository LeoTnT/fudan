//
//  AccountRegisterView.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AccountRegisterViewDelegate <NSObject>

@optional
/** nav登录按钮点击事件 */
-(void)registerRightBtnWasClicked;
/** 获取验证码按钮点击事件 */
-(void)registerGetVerifyCodeBtnWasClick;
/** 注册按钮点击事件 */
-(void)registerBtnWasClick;
/** 服务协议按钮点击事件 */
-(void)registerServiceAgreementBtnWasClick;

@end
@interface AccountRegisterView : UIView

/** 代理 */
@property (nonatomic, weak) id<AccountRegisterViewDelegate>delegate;

@end
