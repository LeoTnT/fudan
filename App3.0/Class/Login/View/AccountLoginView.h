//
//  AccountLoginView.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AccountLoginViewDelegate <NSObject>

@optional
/** nav返回按钮点击事件 */
-(void)backBtnWasClicked;
/** nav右边注册按钮点击事件 */
-(void)loginRightBtnWasClicked;
/** 下拉选择账户按钮点击事件 */
-(void)loginDownBtnWasClick;
/** 登录按钮点击事件 */
-(void)loginBtnWasClick;
/** 验证码登录按钮点击事件 */
-(void)loginVerifyCodeLoginBtnWasClick;
/** 忘记密码按钮点击事件 */
-(void)loginForgetPwdBtnWasClick;
/** 微信登录按钮点击事件 */
-(void)loginWechatLoginBtnWasClick;
/** QQ登录按钮按钮点击事件 */
-(void)loginQQLoginBtnWasClick;
@end

@interface AccountLoginView : UIView

/** 代理 */
@property (nonatomic, weak) id<AccountLoginViewDelegate>delegate;

@end
