//
//  AccountLoginView.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AccountLoginView.h"
#import <TencentOpenAPI/TencentOAuth.h>

@interface AccountLoginView ()<UITextFieldDelegate>

/** 手机号或系统编号 */
@property (nonatomic, strong) UITextField * phoneNumTF;
/** 密码 */
@property (nonatomic, strong) UITextField * passWordTF;

@end

@implementation AccountLoginView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
        //        self.isKeyboardShow = NO;
    }
    return self;
}

-(void)setUpUI {
    self.backgroundColor = [UIColor clearColor];
    
    UIView * navView = [[UIView alloc] init];
    navView.backgroundColor = [UIColor clearColor];
    [self addSubview:navView];
    
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(20);
        make.height.mas_equalTo(44);
    }];
    
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navView).offset(13.5);
        make.centerY.mas_equalTo(navView);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    
    //title
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = Localized(@"enter_login");
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    [navView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(navView);
    }];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:Localized(@"enter_register") forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightBtn];
    
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(navView).offset(-8);
        make.centerY.mas_equalTo(navView);
        make.size.mas_equalTo(CGSizeMake(50, 35));
    }];
    
    UIView * phoneNumView = [[UIView alloc] init];
    phoneNumView.backgroundColor = [UIColor clearColor];
    [self addSubview:phoneNumView];
    [phoneNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(navView.mas_bottom).offset(50);
        make.height.mas_equalTo(60);
    }];
    
    //认证通过ImageView
    UIImageView * phoneImgV = [[UIImageView alloc] init];
    phoneImgV.image = [UIImage imageNamed:@"login_phone"];
    [phoneNumView addSubview:phoneImgV];
    
    [phoneImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneNumView).offset(29);
        make.centerY.mas_equalTo(phoneNumView);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    UIButton * accountDownBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [accountDownBtn setImage:[UIImage imageNamed:@"logoin_pull_down"] forState:UIControlStateNormal];
    [accountDownBtn addTarget:self action:@selector(downBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [phoneNumView addSubview:accountDownBtn];
    [accountDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(phoneNumView).offset(-29);
        make.centerY.mas_equalTo(phoneNumView);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    
    //真实姓名输入框
    self.phoneNumTF = [[UITextField alloc] init];
    self.phoneNumTF.textColor = [UIColor whiteColor];
    self.phoneNumTF.font = [UIFont systemFontOfSize:15];
    self.phoneNumTF.returnKeyType = UIReturnKeyNext;
    self.phoneNumTF.delegate = self;
    [phoneNumView addSubview:self.phoneNumTF];
    
    [self.phoneNumTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneImgV.mas_right).offset(10);
        make.right.mas_equalTo(accountDownBtn).offset(-10);
        make.centerY.mas_equalTo(phoneNumView);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    //分割线
    UIView * line1View = [[UIView alloc] init];
    line1View.backgroundColor = [UIColor whiteColor];
    [phoneNumView addSubview:line1View];
    
    [line1View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneNumView).offset(30);
        make.right.mas_equalTo(phoneNumView).offset(-30);
        make.bottom.mas_equalTo(self.phoneNumTF).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    UIView * pwdView = [[UIView alloc] init];
    pwdView.backgroundColor = [UIColor clearColor];
    [self addSubview:pwdView];
    [pwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(phoneNumView.mas_bottom);
        make.height.mas_equalTo(60);
    }];
    
    //认证通过ImageView
    UIImageView * pwdImgV = [[UIImageView alloc] init];
    pwdImgV.image = [UIImage imageNamed:@"login_password"];
    [pwdView addSubview:pwdImgV];
    
    [pwdImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdView).offset(29);
        make.centerY.mas_equalTo(pwdView);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    //密码
    self.passWordTF = [[UITextField alloc] init];
    self.passWordTF.textColor = [UIColor whiteColor];
    self.passWordTF.font = [UIFont systemFontOfSize:15];
    self.passWordTF.returnKeyType = UIReturnKeyDone;
    self.passWordTF.delegate = self;
    [pwdView addSubview:self.passWordTF];
    
    [self.passWordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdImgV.mas_right).offset(10);
        make.right.mas_equalTo(pwdView).offset(-40);
        make.centerY.mas_equalTo(pwdView);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    //分割线
    UIView * line2View = [[UIView alloc] init];
    line2View.backgroundColor = [UIColor whiteColor];
    [pwdView addSubview:line2View];
    
    [line2View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdView).offset(30);
        make.right.mas_equalTo(pwdView).offset(-30);
        make.bottom.mas_equalTo(self.passWordTF).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = RGB(225, 225, 225);
    
    NSAttributedString *placeholder1 = [[NSAttributedString alloc] initWithString:Localized(@"请输入手机号/系统编号") attributes:attrs];
    NSAttributedString *placeholder2 = [[NSAttributedString alloc] initWithString:Localized(@"请输入密码") attributes:attrs];
    
    self.phoneNumTF.attributedPlaceholder = placeholder1;
    self.passWordTF.attributedPlaceholder = placeholder2;
    
    //登录按钮
    UIButton * loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitle:Localized(@"enter_login") forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    loginBtn.layer.cornerRadius = 4;
    loginBtn.layer.masksToBounds = YES;
    [loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:loginBtn];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdView.mas_bottom).offset(35);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(FontNum(325), FontNum(50)));
    }];
    
    //手机验证码登录按钮
    UIButton * verifyCodeLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeLoginBtn setTitle:Localized(@"login_phone") forState:UIControlStateNormal];
    [verifyCodeLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    verifyCodeLoginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
  
    [verifyCodeLoginBtn addTarget:self action:@selector(verifyCodeLoginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:verifyCodeLoginBtn];
    
    [verifyCodeLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loginBtn.mas_bottom).offset(25);
        make.left.mas_equalTo(loginBtn);
        make.height.mas_equalTo(FontNum(25));
    }];
    
    //忘记密码?按钮
    UIButton * forgetPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetPwdBtn setTitle:Localized(@"忘记密码?") forState:UIControlStateNormal];
    [forgetPwdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [forgetPwdBtn addTarget:self action:@selector(forgetPwdBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:forgetPwdBtn];
    
    [forgetPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.mas_equalTo(verifyCodeLoginBtn);
        make.right.mas_equalTo(loginBtn);
        
    }];
    
    //     第三方登录-微信
    UIButton * wxLoginButton;
    UIButton * qqLoginButton;
    if ([WXApi isWXAppInstalled]) {
        wxLoginButton = [[UIButton alloc] init];
        [wxLoginButton setImage:[UIImage imageNamed:@"login_wechat"] forState:UIControlStateNormal];
        [wxLoginButton addTarget:self action:@selector(wechatLoginBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:wxLoginButton];
        
        [wxLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(48, 48));
            make.bottom.mas_equalTo(self).offset(-24);
            make.centerX.mas_equalTo(self).offset(-54);
        }];
    }
    if ([TencentOAuth iphoneQQInstalled]) {
        qqLoginButton = [[UIButton alloc] init];
        [qqLoginButton setImage:[UIImage imageNamed:@"login_qq"] forState:UIControlStateNormal];
        [qqLoginButton addTarget:self action:@selector(qqLoginBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:qqLoginButton];
        
        [qqLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(48, 48));
            make.bottom.mas_equalTo(self).offset(-24);
            make.centerX.mas_equalTo(self).offset(30);
        }];
    }
    
    if ([WXApi isWXAppInstalled]||[TencentOAuth iphoneQQInstalled]) {
        UIImageView * cuttingView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_third_cuttingline"]];
        [self addSubview:cuttingView];
        
        [cuttingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self).offset(-85);
            make.centerX.mas_equalTo(self);
        }];
    }

    
}

#pragma mark 返回按钮
-(void)backBtnClick
{
    if ([self.delegate respondsToSelector:@selector(backBtnWasClicked)]) {
        [self.delegate backBtnWasClicked];
    }
}
-(void)rightBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginRightBtnWasClicked)]) {
        [self.delegate loginRightBtnWasClicked];
    }
}
-(void)downBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginRightBtnWasClicked)]) {
        [self.delegate loginRightBtnWasClicked];
    }
}
-(void)loginBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginBtnWasClick)]) {
        [self.delegate loginBtnWasClick];
    }
}
#pragma mark 验证码登录&忘记密码
-(void)verifyCodeLoginBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginVerifyCodeLoginBtnWasClick)]) {
        [self.delegate loginVerifyCodeLoginBtnWasClick];
    }
}
-(void)forgetPwdBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginForgetPwdBtnWasClick)]) {
        [self.delegate loginForgetPwdBtnWasClick];
    }
}

#pragma mark 微信登录||qq登录
-(void)wechatLoginBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginWechatLoginBtnWasClick)]) {
        [self.delegate loginWechatLoginBtnWasClick];
    }
}
-(void)qqLoginBtnClick {
    if ([self.delegate respondsToSelector:@selector(loginQQLoginBtnWasClick)]) {
        [self.delegate loginQQLoginBtnWasClick];
    }
}


@end
