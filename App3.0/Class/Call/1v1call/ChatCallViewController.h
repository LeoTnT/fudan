//
//  ChatCallViewController.h
//  App3.0
//
//  Created by mac on 2017/9/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "ChatCallManager.h"

@interface ChatCallViewController : UIViewController

@property (strong, nonatomic, readonly) EMCallSession *callSession;

@property (nonatomic) BOOL isDismissing;
@property (nonatomic ,copy)NSString *headerName;    // 昵称
@property (nonatomic ,copy)NSString *avatar;        // 头像

- (instancetype)initWithCallSession:(EMCallSession *)aCallSession;

- (void)stateToConnected;

- (void)stateToAnswered;

- (void)setNetwork:(EMCallNetworkStatus)aStatus;

- (void)setStreamType:(EMCallStreamingStatus)aType;

- (void)clearData;

- (void)setEndReason:(EMCallEndReason)aReason string:(NSString *)reasonStr;
@end
