//
//  ChatCallViewController.m
//  App3.0
//
//  Created by mac on 2017/9/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatCallViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ChatHelper.h"

@interface ChatCallViewController ()
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *remoteAvatarImgView;     // 对方头像
@property (strong, nonatomic) UILabel *remoteNameLabel;             // 对方昵称
@property (strong, nonatomic) UILabel *statusLabel;                 // 通话状态
@property (strong, nonatomic) UILabel *timeLabel;                   // 计时
@property (strong, nonatomic) UIImageView *remoteImgView;

@property (strong, nonatomic) UILabel *networkLabel;

@property (strong, nonatomic) UIView *actionView;
@property (strong, nonatomic) UIButton *speakerOutButton;           // 免提
@property (strong, nonatomic) UIButton *silenceButton;              // 静音
@property (strong, nonatomic) UIButton *minimizeButton;
@property (strong, nonatomic) UIButton *rejectButton;               // 拒绝
@property (strong, nonatomic) UIButton *hangupButton;               // 取消
@property (strong, nonatomic) UIButton *answerButton;               // 接受
@property (strong, nonatomic) UIButton *switchCameraButton;
@property (strong, nonatomic) UIButton *showVideoInfoButton;

@property (strong, nonatomic) AVAudioPlayer *ringPlayer;
@property (nonatomic) int timeLength;
@property (strong, nonatomic) NSTimer *timeTimer;

@property (strong, nonatomic) ContactDataParser *contactModel;
@end

@implementation ChatCallViewController

- (instancetype)initWithCallSession:(EMCallSession *)aCallSession
{
//    NSString *xibName = @"EMCallViewController";
    self = [super init];
    if (self) {
        _callSession = aCallSession;
        _isDismissing = NO;
        
        if (aCallSession.type == EMCallTypeVideo) {
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
            BOOL ret = [audioSession setActive:NO error:nil];
            if (!ret) {
                NSLog(@"1234567");
            }
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    if (self.isDismissing) {
        return;
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    [self setContentView];
    [self _layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.isDismissing) {
        return;
    }
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.isDismissing) {
        return;
    }
    
    [super viewDidAppear:animated];
}

#pragma mark - private
- (UIButton *)createButtonWithImage:(NSString *)imageName title:(NSString *)title {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(-btn.titleLabel.intrinsicContentSize.height-10, 0, 0, -btn.titleLabel.intrinsicContentSize.width)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(btn.currentImage.size.height+10, -btn.currentImage.size.width, 0, 0)];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.actionView addSubview:btn];
    return btn;
}

- (void)buttonAction:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.hangup", @"")]) {
        [self _stopTimeTimer];
        [self _stopRing];
        
        [self setEndReason:EMCallEndReasonHangup string:nil];
        [[ChatCallManager sharedManager] hangupCallWithReason:EMCallEndReasonHangup];
        
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.answer", @"")]) {
        [self _stopRing];
        [[ChatCallManager sharedManager] answerCall:self.callSession.callId];
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.reject", @"")]) {
        [self _stopTimeTimer];
        [self _stopRing];
        
        [self setEndReason:EMCallEndReasonDecline string:NSLocalizedString(@"call.message.reject", @"")];
        [[ChatCallManager sharedManager] hangupCallWithReason:EMCallEndReasonDecline];
        
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.silence", @"")]) {
        self.silenceButton.selected = !self.silenceButton.selected;
        if (self.silenceButton.selected) {
            EMError *error = [self.callSession pauseVoice];
            NSLog(@"error : %@",error);
        } else {
            EMError *error = [self.callSession resumeVoice];
            NSLog(@"error : %@",error);
        }
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.speakerOut", @"")]) {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        if (self.speakerOutButton.selected) {
            [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
        }else {
            [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
        }
        [audioSession setActive:YES error:nil];
        self.speakerOutButton.selected = !self.speakerOutButton.selected;
    }
}

- (void)swithCameraAction {
    [self.callSession switchCameraPosition:self.switchCameraButton.selected];
    self.switchCameraButton.selected = !self.switchCameraButton.selected;
}

- (void)setContentView {
    // 从数据库获取联系人
    NSArray *arr = [[DBHandler sharedInstance] getContactByUid:self.callSession.remoteName];
    if (arr && arr.count > 0) {
        self.contactModel = arr[0];
    }
    
    // 对方头像
    self.remoteAvatarImgView = [UIImageView new];
    self.remoteAvatarImgView.layer.masksToBounds = YES;
    self.remoteAvatarImgView.layer.cornerRadius = 3;
    [self.remoteAvatarImgView getImageWithUrlStr:self.contactModel.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    [self.view addSubview:self.remoteAvatarImgView];
    
    // 对方昵称
    self.remoteNameLabel = [UILabel new];
    self.remoteNameLabel.text = [self.contactModel getName];
    self.remoteNameLabel.font = [UIFont systemFontOfSize:30];
    self.remoteNameLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.remoteNameLabel];

    // 通话状态
    self.statusLabel = [UILabel new];
    self.statusLabel.text = @"正在等待对方接受邀请..";
    self.statusLabel.font = [UIFont systemFontOfSize:15];
    self.statusLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.statusLabel];
    
    self.actionView = [UIView new];
    [self.view addSubview:self.actionView];
    [self.actionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(35);
        make.right.mas_equalTo(self.view).offset(-35);
        make.bottom.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(150);
    }];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self.actionView addSubview:self.timeLabel];
    self.timeLabel.hidden = YES;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.actionView);
        make.top.mas_equalTo(self.actionView).offset(3);
    }];
    
    self.hangupButton = [self createButtonWithImage:@"call_end" title:NSLocalizedString(@"call.hangup", @"")];
    [self.hangupButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.rejectButton = [self createButtonWithImage:@"call_end" title:NSLocalizedString(@"call.reject", @"")];
    [self.rejectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.answerButton = [self createButtonWithImage:@"call_receive" title:NSLocalizedString(@"call.answer", @"")];
    [self.answerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.silenceButton = [self createButtonWithImage:@"call_mute_normal" title:NSLocalizedString(@"call.silence", @"")];
    [self.silenceButton setImage:[UIImage imageNamed:@"call_mute_press"] forState:UIControlStateSelected];
    self.silenceButton.hidden = YES;
    [self.silenceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.speakerOutButton = [self createButtonWithImage:@"call_speaker_normal" title:NSLocalizedString(@"call.speakerOut", @"")];
    [self.speakerOutButton setImage:[UIImage imageNamed:@"call_speaker_press"] forState:UIControlStateSelected];
    self.speakerOutButton.hidden = YES;
    [self.speakerOutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    if (self.callSession.type == EMCallTypeVideo) {
        self.speakerOutButton.selected = YES;   // 默认开启免提
        
        [self.remoteAvatarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self.view).offset(15);
            make.size.mas_equalTo(CGSizeMake(66, 66));
        }];
        
        [self.remoteNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.remoteAvatarImgView.mas_right).offset(12);
            make.top.mas_equalTo(self.remoteAvatarImgView);
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.remoteNameLabel);
            make.bottom.mas_equalTo(self.remoteAvatarImgView);
        }];
        
        self.switchCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.switchCameraButton setImage:[UIImage imageNamed:@"call_camera"] forState:UIControlStateNormal];
        [self.switchCameraButton addTarget:self action:@selector(swithCameraAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.switchCameraButton];
        self.switchCameraButton.hidden = YES;
        [self.switchCameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view).offset(15);
            make.top.mas_equalTo(self.view).offset(25);
        }];
    } else {
        self.remoteNameLabel.textAlignment = NSTextAlignmentCenter;
        self.statusLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.remoteAvatarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view.mas_centerY).offset(-70);
            make.left.mas_equalTo(self.view.mas_left).offset(127);
            make.right.mas_equalTo(self.view.mas_right).offset(-127);
            make.width.mas_equalTo(self.remoteAvatarImgView.mas_height);
        }];
        
        [self.remoteNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.top.mas_equalTo(self.remoteAvatarImgView.mas_bottom).offset(25);
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.remoteNameLabel);
            make.top.mas_equalTo(self.remoteNameLabel.mas_bottom).offset(17);
        }];
    }
}

- (void)_layoutSubviews
{
    
    BOOL isCaller = self.callSession.isCaller;
    switch (self.callSession.type) {
        case EMCallTypeVoice:
        {
            if (isCaller) {
                self.rejectButton.hidden = YES;
                self.answerButton.hidden = YES;
            } else {
                self.hangupButton.hidden = YES;
            }
        }
            break;
        case EMCallTypeVideo:
        {
            self.showVideoInfoButton.hidden = NO;
            self.speakerOutButton.hidden = YES;
            
            if (isCaller) {
                self.rejectButton.hidden = YES;
                self.answerButton.hidden = YES;
            } else {
                self.hangupButton.hidden = YES;
            }
            
            [self _setupLocalVideoView];
            //            [self.view bringSubviewToFront:self.topView];
            //            [self.view bringSubviewToFront:self.actionView];
        }
            break;
            
        default:
            break;
    }
}

- (void)_setupRemoteVideoView
{
    if (self.callSession.type == EMCallTypeVideo && self.callSession.remoteVideoView == nil) {
        NSLog(@"\n########################_setupRemoteView");
        self.callSession.remoteVideoView = [[EMCallRemoteView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.callSession.remoteVideoView.hidden = YES;
        self.callSession.remoteVideoView.backgroundColor = [UIColor clearColor];
        self.callSession.remoteVideoView.scaleMode = EMCallViewScaleModeAspectFill;
        [self.view addSubview:self.callSession.remoteVideoView];
        [self.view sendSubviewToBack:self.callSession.remoteVideoView];
        
        __weak ChatCallViewController *weakSelf = self;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            weakSelf.callSession.remoteVideoView.hidden = NO;
        });
    }
}

- (void)_setupLocalVideoView
{
    //2.自己窗口
    CGFloat width = 80;
    CGSize size = [UIScreen mainScreen].bounds.size;
    CGFloat height = size.height / size.width * width;
    self.callSession.localVideoView = [[EMCallLocalView alloc] initWithFrame:CGRectMake(size.width - width - 20, 20, width, height)];
    [self.view addSubview:self.callSession.localVideoView];
    [self.view bringSubviewToFront:self.callSession.localVideoView];
}

#pragma mark - private ring

- (void)_beginRing
{
    [self.ringPlayer stop];
    
    NSString *musicPath = [[NSBundle mainBundle] pathForResource:@"callRing" ofType:@"mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:musicPath];
    
    self.ringPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [self.ringPlayer setVolume:1];
    self.ringPlayer.numberOfLoops = -1; //设置音乐播放次数  -1为一直循环
    if([self.ringPlayer prepareToPlay])
    {
        [self.ringPlayer play]; //播放
    }
}

- (void)_stopRing
{
    [self.ringPlayer stop];
}

#pragma mark - private timer

- (void)timeTimerAction:(id)sender
{
    self.timeLength += 1;
    int hour = self.timeLength / 3600;
    int m = (self.timeLength - hour * 3600) / 60;
    int s = self.timeLength - hour * 3600 - m * 60;
    
    if (hour > 0) {
        self.timeLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i", hour, m, s];
    }
    else if(m > 0){
        self.timeLabel.text = [NSString stringWithFormat:@"%02i:%02i", m, s];
    }
    else{
        self.timeLabel.text = [NSString stringWithFormat:@"00:%02i", s];
    }
}

- (void)_startTimeTimer
{
    self.timeLength = 0;
    self.timeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTimerAction:) userInfo:nil repeats:YES];
}

- (void)_stopTimeTimer
{
    if (self.timeTimer) {
        [self.timeTimer invalidate];
        self.timeTimer = nil;
    }
}

#pragma mark - public

- (void)stateToConnecting
{
    if (self.callSession.isCaller) {
        self.statusLabel.text = NSLocalizedString(@"call.connecting", @"Connecting...");
    } else {
        self.statusLabel.text = NSLocalizedString(@"call.connecting", "Incomimg call");
    }
}

- (void)stateToConnected
{
    self.statusLabel.text = NSLocalizedString(@"call.finished", "");
}

- (void)stateToAnswered
{
    self.statusLabel.text = NSLocalizedString(@"call.speak", "Incomimg call");
    [self _startTimeTimer];
    
    
    
//    if (_callSession.connectType == EMCallConnectTypeRelay) {
//        self.statusLabel.text = NSLocalizedString(@"call.speak", "");
//    } else if (_callSession.connectType == EMCallConnectTypeDirect) {
//        self.statusLabel.text = NSLocalizedString(@"call.speak", "");
//    }
    
    self.timeLabel.hidden = NO;
    self.hangupButton.hidden = NO;
    self.statusLabel.hidden = NO;
    self.rejectButton.hidden = YES;
    self.answerButton.hidden = YES;
    self.silenceButton.hidden = NO;
    self.speakerOutButton.hidden = NO;
    
    if (self.callSession.type == EMCallTypeVideo) {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
        [audioSession setActive:YES error:nil];
        [self _setupRemoteVideoView];
        
        self.remoteAvatarImgView.hidden = YES;
        self.remoteNameLabel.hidden = YES;
        self.statusLabel.hidden = YES;
        self.switchCameraButton.hidden = NO;
    }
    
    
}

- (void)setNetwork:(EMCallNetworkStatus)aStatus
{
    if (aStatus == EMCallNetworkStatusUnstable) {
        self.networkLabel.text = @"网络不稳定";
    } else if (aStatus == EMCallNetworkStatusNoData) {
        self.networkLabel.text = @"无数据";
    } else {
        self.networkLabel.text = @"";
    }
}

- (void)setStreamType:(EMCallStreamingStatus)aType
{
    NSString *str = @"Unkonw";
    switch (aType) {
        case EMCallStreamStatusVoicePause:
            str = @"Audio Mute";
            break;
        case EMCallStreamStatusVoiceResume:
            str = @"Audio Unmute";
            break;
        case EMCallStreamStatusVideoPause:
            str = @"Video Pause";
            break;
        case EMCallStreamStatusVideoResume:
            str = @"Video Resume";
            break;
            
        default:
            break;
    }
    
    //    [self showHint:str];
}

- (void)setEndReason:(EMCallEndReason)aReason string:(NSString *)reasonStr {
    // 停止播放音效
    [[ChatHelper shareHelper] stopSystemSound];
    
    self.statusLabel.text = NSLocalizedString(reasonStr, "");
    
    // 发送消息
    NSString *text = reasonStr;
    switch (aReason) {
        case EMCallEndReasonHangup:
        {
            if (!self.silenceButton.hidden) {
                text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"call.message.hangup", ""),self.timeLabel.text];
            } else {
                text = NSLocalizedString(@"call.message.cancel", "");
            }
        }
            
            break;
        default:
            break;
    }
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
    NSDictionary *extDic;
    if (self.callSession.type == EMCallTypeVideo) {
        extDic = @{MSG_TYPE:MESSAGE_ATTR_IS_VIDEO_CALL};
    } else {
        extDic = @{MSG_TYPE:MESSAGE_ATTR_IS_VOICE_CALL};
    }
    
    // 获取会话
    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:self.callSession.remoteName type:EMConversationTypeChat createIfNotExist:YES];
    
    // 生成message
    NSString *from;
    NSString *to;
    if (self.callSession.isCaller) {
        from = self.callSession.localName;
        to = self.callSession.remoteName;
    } else {
        from = self.callSession.remoteName;
        to = self.callSession.localName;
    }
    EMMessage *message = [[EMMessage alloc] initWithConversationID:conver.conversationId from:from to:to body:body ext:extDic];
    message.chatType = EMConversationTypeChat;// 设置消息类型
    message.direction = self.callSession.isCaller?EMMessageDirectionSend:EMMessageDirectionReceive;
    //插入消息
    NSLog(@"conversation ： %@",conver.conversationId);
    [conver insertMessage:message error:nil];
//    [[ChatHelper shareHelper] messagesDidReceive:@[message]];
    if ([ChatHelper shareHelper].chatVC && [[ChatHelper shareHelper].chatVC.conversation.conversationId isEqualToString:self.callSession.remoteName]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
    }
}

- (void)clearData
{
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
    [audioSession setActive:YES error:nil];
    
    self.callSession.remoteVideoView.hidden = YES;
    self.callSession.remoteVideoView = nil;
    _callSession = nil;
    
    [self _stopTimeTimer];
    [self _stopRing];
}

@end
