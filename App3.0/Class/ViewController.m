//
//  ViewController.m
//  App3.0
//
//  Created by mac on 17/2/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ViewController.h"
#import "LoggerTableViewController.h"

@interface ViewController ()
@property (nonatomic ,strong) UILabel *tLabel;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    self.tLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 100, 200, 100)];
//    self.tLabel.textColor = [UIColor blackColor];
//    self.tLabel.font = [UIFont boldSystemFontOfSize:20];
//    self.tLabel.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview:self.tLabel];
//    [self jspatchTest];
//    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(100, 300, 200, 100)];
    [btn setTitle:@"test" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(testLogger) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 200, 100)];
    [btn1 setTitle:@"日志" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(openLogger) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)jspatchTest {
    self.tLabel.text = @"测试前!";
}

- (void)test
{
    NSArray *arr = @[@"1",@"2",@"3"];
    for (int i = 0; i < 5; i++) {
        NSString *temp = arr[i];
        NSLog(@"%@",temp);
    }
}

- (void)openLogger
{
    LoggerTableViewController *vc = [[LoggerTableViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)testLogger
{
    DDLogError(@"数组越界");
    DDLogWarn(@"警告！！！");
    DDLogInfo(@"testLogger");
//    NSArray *test=@[@"123",@"444"];
//    id testID=test[4];
}


@end
