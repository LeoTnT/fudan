//
//  AgainGoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/11/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AgainGoodsTableViewCell.h"
#import "UIImage+XSWebImage.h"

@implementation AgainGoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.logo = [UIImageView new];
        [self.contentView addSubview:self.logo];
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.titleLabel];
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line];
        
        [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(44, 44));
            make.left.mas_equalTo(self.contentView).with.mas_offset(12);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.logo.mas_right).with.mas_offset(11.5);
            make.right.mas_equalTo(self.contentView.mas_right).with.mas_offset(-12.5);
            make.centerY.mas_equalTo(self.contentView);
            make.height.mas_equalTo(self.logo);
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.height.mas_equalTo(1);
            make.width.mas_equalTo(self.contentView);
        }];
    }
    
    return self;
    
}

-(void)setGoodsString:(NSString *)goodsString {
    
    _goodsString = goodsString;
    
    [self.logo getImageWithUrlStr:[[goodsString componentsSeparatedByString:@","] lastObject] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    
    self.titleLabel.text = [[goodsString componentsSeparatedByString:@","] firstObject];
}

@end
