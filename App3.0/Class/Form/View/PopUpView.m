//
//  PopUpView.m
//  App3.0
//
//  Created by nilin on 2017/4/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PopUpView.h"

@implementation PopUpView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        CGFloat height = 50;
       
        
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        
        //取消
       self.cancelBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, mainHeight-height-10.5, mainWidth-NORMOL_SPACE*2, height) title:Localized(@"cancel_btn") titleColor:mainColor fontSize:17 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor  highBackgroundColor:[UIColor whiteColor]];
        [self.cancelBtn setBorderWith:0 borderColor:nil cornerRadius:12];
        [self.cancelBtn addTarget:self action:@selector(toCancelLink:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelBtn];
        
        UIView *bigView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMinY(self.cancelBtn.frame)-100-6.5, mainWidth-2*10, 100)];
        bigView.backgroundColor = [UIColor whiteColor];
        
        //发短信
        self.oneBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(bigView.frame), height) title:@"发送短信" titleColor:mainColor fontSize:17 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor highBackgroundColor:[UIColor whiteColor]];
        [bigView addSubview:self.oneBtn];
        
        //打电话
        self.otherBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.oneBtn.frame), CGRectGetWidth(bigView.frame), height) title:@"拨打电话" titleColor:mainColor fontSize:17 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor highBackgroundColor:[UIColor whiteColor]];
        [bigView addSubview:self.otherBtn];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0,  CGRectGetMaxY(self.oneBtn.frame)-0.25, CGRectGetWidth(bigView.frame), 0.5)];
        line.backgroundColor = [UIColor hexFloatColor:@"D9D9DE"];
        [bigView addSubview:line];
        bigView.layer.cornerRadius = 12;
        bigView.layer.masksToBounds = YES;
        
        [self addSubview:bigView];
        
    }
    
    return self;
}

-(void)setPopUpViewType:(PopUpViewType)popUpViewType {
    _popUpViewType = popUpViewType;
    if (popUpViewType==PopUpViewTypeOrder) {
        [self.oneBtn addTarget:self action:@selector(toMassageMan:) forControlEvents:UIControlEventTouchUpInside];
        [self.otherBtn addTarget:self action:@selector(toCallMan:) forControlEvents:UIControlEventTouchUpInside];
    } else if (popUpViewType==PopUpViewTypePhoto) {
        
    }
    
}

-(void)toCallMan:(UIButton *) sender{
    if ([self.delegate respondsToSelector:@selector(clickToCallMan)]) {
        [self.delegate clickToCallMan];
    }
}

-(void)toMassageMan:(UIButton *) sender{
    if ([self.delegate respondsToSelector:@selector(clickToMassageMan)]) {
        [self.delegate clickToMassageMan];
    }
}

-(void)toCancelLink:(UIButton *) sender{
    if ([self.delegate respondsToSelector:@selector(clickToCancelLink)]) {
        [self.delegate clickToCancelLink];
    }
}

@end
