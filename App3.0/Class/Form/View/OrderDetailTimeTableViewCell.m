//
//  OrderDetailTimeTableViewCell.m
//  App3.0
//
//  Created by nilin on 2817/9/11.
//  Copyright © 2817年 mac. All rights reserved.
//

#import "OrderDetailTimeTableViewCell.h"
#import "XSFormatterDate.h"

@implementation OrderDetailTimeTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
-(void)setDetailParser:(BuyerOrderDetailParser *)detailParser {
    
    _detailParser = detailParser;
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    UIColor *textColor = COLOR_999999;
    UILabel *orderNum = [[UILabel alloc] init];
    orderNum.text = [NSString stringWithFormat:@"订单编号：%@",_detailParser.order_no];
    orderNum.textColor = textColor;
    UIFont *font = [UIFont systemFontOfSize:13];
    orderNum.font = font;
    [self.contentView addSubview:orderNum];
    [orderNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
        make.top.mas_equalTo(self.contentView).with.mas_offset(6);
        make.height.mas_equalTo(28);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
    }];
    self.cellHeight = 6+28;
    
     UILabel *wTime = [[UILabel alloc] init];
    if ([_detailParser.w_time doubleValue]>0) {
       [self.contentView addSubview:wTime];
        wTime.textColor = textColor;
        wTime.font = font;
        wTime.text = [NSString stringWithFormat:@"下单时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.w_time]];
        [wTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
            make.top.mas_equalTo(orderNum.mas_bottom);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        }];
        self.cellHeight+=28;
    }
    
     UILabel *orderPayTime = [[UILabel alloc] init];
    if ([_detailParser.pay_time doubleValue]>0) {
        [self.contentView addSubview:orderPayTime];
        orderPayTime.textColor = textColor;
        orderPayTime.font = font;
        orderPayTime.text = [NSString stringWithFormat:@"付款时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.pay_time]];
        [orderPayTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
            make.top.mas_equalTo(wTime.mas_bottom);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        }];
        self.cellHeight+=28;
    }
    
     UILabel *orderDeliverTime = [[UILabel alloc] init];
    if ([_detailParser.transmit_time doubleValue]>0) {
       [self.contentView addSubview:orderDeliverTime];
        orderDeliverTime.textColor = textColor;
        orderDeliverTime.font = font;
        orderDeliverTime.text = [NSString stringWithFormat:@"发货时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.transmit_time]];
        [orderDeliverTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
            make.top.mas_equalTo(orderPayTime.mas_bottom);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        }];
        self.cellHeight+=28;
    }
    
    UILabel *receiveTime = [[UILabel alloc] init];
    if ([_detailParser.confirm_time doubleValue]>0) {
        [self.contentView addSubview:receiveTime];
        receiveTime.textColor = textColor;
        receiveTime.font = font;
        receiveTime.text = [NSString stringWithFormat:@"收货时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.confirm_time]];
        if ([_detailParser.transmit_time doubleValue]>0) {
            [receiveTime mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
                make.top.mas_equalTo(orderDeliverTime.mas_bottom);
                make.height.mas_equalTo(28);
                make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
            }];
        } else {
            [receiveTime mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
                make.top.mas_equalTo(orderPayTime.mas_bottom);
                make.height.mas_equalTo(28);
                make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
            }];
        }
       
        self.cellHeight+=28;
    }
    
    UILabel *orderSalesReturnTime = [[UILabel alloc] init];
    if ([_detailParser.app_time doubleValue]>0) {
        [self.contentView addSubview:orderSalesReturnTime];
        orderSalesReturnTime.textColor = textColor;
        orderSalesReturnTime.font = font;
        orderSalesReturnTime.text = [NSString stringWithFormat:@"申请退货时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.app_time]];
         if ([_detailParser.transmit_time doubleValue]>0) {
             [orderSalesReturnTime mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
                 make.top.mas_equalTo(orderDeliverTime.mas_bottom);
                 make.height.mas_equalTo(28);
                 make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
             }];
         } else {
             [orderSalesReturnTime mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
                 make.top.mas_equalTo(orderPayTime.mas_bottom);
                 make.height.mas_equalTo(28);
                 make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
             }];
         }
       
        self.cellHeight+=28;
    }
    
    UILabel *payReturnTime = [[UILabel alloc] init];
    
    //未发货，退款时间
    if ([_detailParser.return_time doubleValue]>0) {
        [self.contentView addSubview:payReturnTime];
        payReturnTime.textColor = textColor;
        payReturnTime.font = font;
        payReturnTime.text = [NSString stringWithFormat:@"退款时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.return_time]];
        [payReturnTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
            make.top.mas_equalTo(orderPayTime.mas_bottom);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        }];
        self.cellHeight+=28;
    }
    UILabel *cancelTime = [[UILabel alloc] init];
    if ([_detailParser.close_time doubleValue]>0) {
        [self.contentView addSubview:cancelTime];
        cancelTime.textColor = textColor;
        cancelTime.font = font;
        cancelTime.text = [NSString stringWithFormat:@"取消时间：%@",[XSFormatterDate dateWithTimeIntervalString:_detailParser.close_time]];
        [cancelTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
            make.top.mas_equalTo(wTime.mas_bottom);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        }];
        self.cellHeight+=28;
    }
    
    self.cellHeight+=8;
    UILabel *remarkLabel = [UILabel new];
    remarkLabel.backgroundColor = [UIColor whiteColor];
    remarkLabel.text = [NSString stringWithFormat:@"备注：%@",self.detailParser.desc.length==0?Localized(@"无"):_detailParser.desc];
    remarkLabel.numberOfLines = 0;
    remarkLabel.textColor = textColor;
    remarkLabel.font = [UIFont systemFontOfSize:14];
    remarkLabel.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size2 = [remarkLabel sizeThatFits:CGSizeMake(remarkLabel.frame.size.width, MAXFLOAT)];
    if (size2.height<44) {
        size2.height = 44;
    }
    [self.contentView addSubview:remarkLabel];
    [remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.mas_offset(self.cellHeight);
        make.left.mas_equalTo(self.contentView).with.mas_offset(11.5);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-11.5);
        make.height.mas_equalTo(size2.height);
    }];
    
    UILabel *line1 = [UILabel new];
    line1.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
    [self.contentView addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remarkLabel.mas_top).with.mas_offset(-2);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
    self.cellHeight +=size2.height;

}

@end
