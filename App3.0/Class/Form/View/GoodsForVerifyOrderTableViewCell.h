//
//  GoodsForVerifyOrderTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartModel.h"
#import "OrderFormModel.h"
#import "VerifyDeliveryView.h"
@protocol GoodsForVerifyOrderDelegate<NSObject>
@optional
-(void) linkSupplyWithSupplyName:(NSString *) supplyName;
@optional
-(void) lookShopMainPageWithSupplyId:(NSString *) supplyId;

@optional
- (void) changeLogisticWithParser:(PreviewVerifyWuliuParser *) logisticParser supplyName:(NSString *) supplyName;
@optional
- (void) changeDeliveryWithSupplyName:(NSString *) supplyName;

@optional
- (void) lookCoupon;

@end
@interface GoodsForVerifyOrderTableViewCell : UITableViewCell

@property(nonatomic,strong) CartProductParser *productParser;//购物车的商品信息
@property(nonatomic,assign) CGFloat totalPrice;
@property(nonatomic,assign) NSUInteger totalNum;//件数
@property(nonatomic,assign) CGFloat cellHeight;
@property(nonatomic,strong) UITextView *remark;//留言
@property(nonatomic,strong) UILabel *tintInfo;//提示
@property (nonatomic, assign) CGFloat logisticMoney;//运费
@property(nonatomic,weak) id<GoodsForVerifyOrderDelegate> verifyOrderDelegate;
@property (nonatomic, assign) NSUInteger selectedLogisticType;
@property (nonatomic, strong) VerifyDeliveryView *deliveryView;

@property (nonatomic, strong) ValidDeliveryObtainDetailParser *deliveryParser;

@property(nonatomic,strong) PreviewVerifyOrderProductsParser *shopParser;
@property (nonatomic, strong) UILabel *couponLabel;
@end
