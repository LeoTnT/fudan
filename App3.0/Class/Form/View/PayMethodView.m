//
//  PayMethodView.m
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PayMethodView.h"
#import "PayModel.h"
@interface PayMethodView()

@property(nonatomic,strong) UIImageView *payImageView;
@property(nonatomic,strong) UILabel *titleLabel;
//@property(nonatomic,strong) UIButton *methodButton;//选择支付按钮
@property(nonatomic,strong) NSMutableArray *btnArry;

@end

@implementation PayMethodView

-(void)setMethodArray:(NSArray *)methodArray{
    
    _methodArray = methodArray;

    CGFloat imageSize = 40,buttonSize = 20,height=60,titleWidth;
    titleWidth = mainWidth-imageSize-buttonSize-NORMOL_SPACE*4;
    UIView *centerView;
    for (int i=0; i<_methodArray.count; i++) {
            PayMethodParser *parser = _methodArray[i];
        @autoreleasepool {
            centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*height, mainWidth, height)];
            UIImageView *payImageView = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
            payImageView.image = [UIImage imageNamed:@"user_wallet_alipay"];
            //微信
            UIImage *defaultImage = nil;
            if ([parser.key isEqualToString:@"WeiXinApp"]) {
                defaultImage = [UIImage imageNamed:@"user_wallet_wxpay"];
            }
            if ([parser.key isEqualToString:@"UnionPayApp"]) {
                defaultImage = [UIImage imageNamed:@"user_wallet_uppay"];
            }
            [payImageView getImageWithUrlStr:parser.logo andDefaultImage:defaultImage];
            [centerView addSubview:payImageView];
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(payImageView.frame)+NORMOL_SPACE, CGRectGetMinY(payImageView.frame), titleWidth, imageSize)];
            titleLabel.text = parser.title;
            [centerView addSubview:titleLabel];
            UIButton *methodButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE-buttonSize, height/2-buttonSize/2 , buttonSize, buttonSize)];
            [methodButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
            [methodButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
            methodButton.tag = i+400;
//            [methodButton addTarget:self action:@selector(ClickMethedButton:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:methodButton];
            [self.btnArry addObject:methodButton];

            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(payImageView.frame)+NORMOL_SPACE, mainWidth, 1)];
            line.backgroundColor = LINE_COLOR;
            [centerView addSubview:line];
            [self addSubview:centerView];
            self.viewHeight = CGRectGetMaxY(centerView.frame);
            
            UIButton*btn = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0 , mainWidth-NORMOL_SPACE, self.frame.size.height)];
            btn.tag = i+500;
            
            [btn addTarget:self action:@selector(ClickMethedButton:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:btn];

        }
        
    }
}


-(void)ClickMethedButton:(UIButton *) sender{
    
    NSInteger methodTag = sender.tag-100;
    UIButton*seltedBtn;
    for (UIButton*methodButton in self.btnArry) {
        if (methodButton.tag == methodTag) {
            seltedBtn = methodButton;
        }
    }
    seltedBtn.selected = !seltedBtn.selected;
    PayMethodParser *parser = _methodArray[seltedBtn.tag-400];
    NSString *method = parser.key;
//    if (seltedBtn.selected) {
//        if ([self.methodDelegate respondsToSelector:@selector(selectedPayMethod:)]) {
//            [self.methodDelegate selectedPayMethod:method];
//            
//        }
//        
//        for (int i=400; i<self.methodArray.count+400; i++) {
//            if (i!=seltedBtn.tag) {
//                UIButton *button = (UIButton *)[self viewWithTag:i];
//                if (button.selected) {
//                    button.selected = !button.selected;
//                }
//            }
//        }
//        
//    } else {
//        if ([self.methodDelegate respondsToSelector:@selector(unSelectedPayMethod:)]) {
//            [self.methodDelegate unSelectedPayMethod:method];
//        }
//        
//    }
}


-(NSMutableArray *)btnArry
{
    if (!_btnArry) {
        _btnArry = [NSMutableArray array];
    }
    return _btnArry;
}

@end
