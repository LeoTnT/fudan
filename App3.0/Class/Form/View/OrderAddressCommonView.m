//
//  OrderAddressCommonView.m
//  App3.0
//
//  Created by nilin on 2017/9/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderAddressCommonView.h"

@implementation OrderAddressCommonView
{
    UIImageView *rightView;
}
-(instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIFont *font = [UIFont systemFontOfSize:15];
        self.imageView = [UIImageView new];
        self.imageView.image = [UIImage imageNamed:@"order_address"];
        [self addSubview:self.imageView];
        self.reciverLabel = [UILabel new];
        self.reciverLabel.font = font;
        self.reciverLabel.numberOfLines = 0;
        [self addSubview:self.reciverLabel];
        self.phoneLabel = [UILabel new];
        self.phoneLabel.font = font;
        self.phoneLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.phoneLabel];
        self.addressLabel = [UILabel new];
        self.addressLabel.textColor = COLOR_666666;
        self.addressLabel.font = [UIFont systemFontOfSize:13];
        self.addressLabel.numberOfLines = 0;

        [self addSubview:self.addressLabel];
        self.linkButton = [UIButton new];
        self.linkButton.titleLabel.font = font;
        [self.linkButton setTitle:@"联系卖家" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:COLOR_666666 forState:UIControlStateNormal];
        [self.linkButton setImage:[UIImage imageNamed:@"user_order_chat"] forState:UIControlStateNormal];
        self.linkButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
//        [self addSubview:self.linkButton];
        //        self.reciverLabel.backgroundColor = [UIColor yellowColor];
        //        self.phoneLabel.backgroundColor = [UIColor purpleColor];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).with.mas_offset(14);
            make.left.mas_equalTo(self).with.mas_offset(12);
            make.width.mas_lessThanOrEqualTo(20);
            make.height.mas_equalTo(20);
        }];
        CGFloat width = (mainWidth-25.5-32.5-16)/3;
        [self.reciverLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imageView.mas_right).with.mas_offset(9);
            make.top.mas_equalTo(13);
            make.height.mas_equalTo(14.5);
            make.width.mas_lessThanOrEqualTo(width*1.8);
        }];
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(14.5);
            make.left.mas_equalTo(self.reciverLabel.mas_right).with.mas_offset(25.5);
            make.width.mas_lessThanOrEqualTo(width*1.2);
            make.height.mas_equalTo(11.5);
        }];
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.reciverLabel.mas_bottom).with.mas_offset(10.5);
            make.left.mas_equalTo(self.reciverLabel);
            make.right.mas_equalTo(self).with.mas_offset(-16);
//            make.bottom.mas_equalTo(self).with.mas_offset(-12);
            
        }];
//        [self.linkButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self.addressLabel.mas_bottom);
//            make.left.right.mas_equalTo(self);
//            make.height.mas_equalTo(40);
//        }];
        
        self.viewHeight = 38;
//        self.reciverLabel.text  =
        
    }
    return self;
}
-(void)viewDidLayoutSubviews{
    [self.addressLabel sizeToFit];
}
- (void)setAddressArray:(NSArray *)addressArray {
    _addressArray = addressArray;
    if (self.addressType==OrderAddressCommonViewTypeVerify) {
//        [self.linkButton removeFromSuperview];
        UIFont *font = [UIFont systemFontOfSize:13];
        rightView = [UIImageView new];
         rightView.image = [UIImage imageNamed:@"mall_verify_back"];
        [self addSubview:rightView];
        [rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.phoneLabel.mas_bottom);
            make.right.mas_equalTo(-10);
            make.size.mas_equalTo(CGSizeMake(10, 10));
        }];
        [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.height.mas_equalTo(self.reciverLabel);
            make.left.mas_equalTo(self.reciverLabel.mas_right).with.mas_offset(5);
            make.right.mas_equalTo(rightView.mas_left);
        }];
        self.addressLabel.font = font;
        self.reciverLabel.font = font;
        self.phoneLabel.font = font;
        self.addressLabel.textColor = mainGrayColor;
        self.reciverLabel.textColor = mainGrayColor;
        self.phoneLabel.textColor = mainGrayColor;
        self.reciverLabel.text =  [_addressArray firstObject];
        NSString *str = [NSString stringWithFormat:@"%@",_addressArray[2]];
    
        self.addressLabel.font = font;
        self.addressLabel.lineBreakMode = NSLineBreakByCharWrapping;
        self.addressLabel.text = str;
        CGSize size = CGSizeMake(300, 200);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil];
        CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
        [self.addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.reciverLabel.mas_bottom).with.mas_offset(10.5);
            make.left.mas_equalTo(self.reciverLabel);
            make.right.mas_equalTo(self.phoneLabel.mas_right);
            make.height.mas_equalTo(actualsize.height+12);
        }];
        self.viewHeight += actualsize.height+12;
//        self.viewHeight-=40;
    } else if (self.addressType==OrderAddressCommonViewTypeDetailOrder) {
        self.reciverLabel.text = [NSString stringWithFormat:@"%@", [_addressArray firstObject]];
        NSString *str = [NSString stringWithFormat:@"%@",_addressArray[2]];
        UIFont *tFont = [UIFont systemFontOfSize:13];
        self.addressLabel.font = tFont;
        self.addressLabel.lineBreakMode = NSLineBreakByCharWrapping;
        self.addressLabel.text = str;
        CGSize size = CGSizeMake(300, 200);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
        CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
        [self.addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.reciverLabel.mas_bottom).with.mas_offset(10.5);
            make.left.mas_equalTo(self.reciverLabel);
            make.right.mas_equalTo(self).with.mas_offset(-16);
            make.height.mas_equalTo(actualsize.height+12);
        }];
        self.viewHeight += actualsize.height+12+10;
    }
    
   
    self.phoneLabel.text = _addressArray[1];
    
}


@end
