//
//  PayMethodTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PayMethodTableViewCell.h"
#import "PayMethodView.h"
@interface PayMethodTableViewCell ()
@property (nonatomic, strong) UIImageView *payImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation PayMethodTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        CGFloat imageSize = 40,buttonSize = 20,height=60,titleWidth;
        titleWidth = mainWidth-imageSize-buttonSize-NORMOL_SPACE*4;
         self.payImageView = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
        [self.contentView addSubview:self.payImageView];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.payImageView.frame)+NORMOL_SPACE, CGRectGetMinY(self.payImageView.frame), titleWidth, imageSize)];
        [self.contentView addSubview:self.titleLabel];
        
        self.methodButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE-buttonSize, height/2-buttonSize/2 , buttonSize, buttonSize)];
        [self.methodButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [self.methodButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.methodButton];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.payImageView.frame)+NORMOL_SPACE, mainWidth, 1)];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line];
        
    }
    return self;
}
-(void)setPayParser:(PayMethodParser *)payParser {
    _payParser = payParser;
  
    self.payImageView.image = [UIImage imageNamed:@"user_wallet_alipay"];
    //微信
    UIImage *defaultImage = nil;
    if ([payParser.key isEqualToString:@"WeiXinApp"]) {
        defaultImage = [UIImage imageNamed:@"user_wallet_wxpay"];
    }
    if ([payParser.key isEqualToString:@"UnionPayApp"]) {
        defaultImage = [UIImage imageNamed:@"user_wallet_uppay"];
    }
    [self.payImageView getImageWithUrlStr:payParser.logo andDefaultImage:defaultImage];

    self.titleLabel.text = payParser.title;
    UIButton*btn = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0 , mainWidth-NORMOL_SPACE, self.frame.size.height)];
    
    [btn addTarget:self action:@selector(ClickMethedButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btn];
}
-(void)ClickMethedButton:(UIButton *) sender{
    
    self.methodButton.selected = !self.methodButton.selected;
    NSString *method = self.payParser.key;
    if (self.methodButton.selected) {
        if ([self.methodDelegate respondsToSelector:@selector(selectedPayMethod:)]) {
            [self.methodDelegate selectedPayMethod:method];
            
        }
        
    } else {
        if ([self.methodDelegate respondsToSelector:@selector(unSelectedPayMethod:)]) {
            [self.methodDelegate unSelectedPayMethod:method];
        }
        
    }
}

//-(void)setMethodArray:(NSArray *)methodArray{
//    _methodArray = methodArray;
//    for (UIView *view in self.contentView.subviews) {
//        if ([view isKindOfClass:[PayMethodView class]]) {
//            [view removeFromSuperview];
//        }
//    }
//    self.methodView = [[PayMethodView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 200)];
//     self.methodView.methodArray = _methodArray;
//
//     self.methodView.frame = CGRectMake(0, 0, mainWidth,  self.methodView.viewHeight);
//    [self.contentView addSubview: self.methodView];
//    self.cellHeight = CGRectGetMaxY( self.methodView.frame);
//}



@end
