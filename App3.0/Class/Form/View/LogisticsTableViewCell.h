//
//  LogisticsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticsModel.h"

@interface LogisticsTableViewCell : UITableViewCell


@property(nonatomic,assign) CGFloat cellHeight;

@property(nonatomic,assign) BOOL isNewInfo;

@property(nonatomic,strong) LogisticsDataParser *dataParser;
@end
