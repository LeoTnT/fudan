//
//  PayTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PayTableViewCell.h"
@interface PayTableViewCell()<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *limitMoneyLabel;
@property (nonatomic, strong) UILabel *wantPayLabel;
@end
@implementation PayTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
      

        CGFloat titleHeight = 20,imageSize = 40;
        self.payImageView = [UIImageView new];
        self.payImageView.image = [UIImage imageNamed:@"user_wallet_bindingcard"];
        [self.contentView addSubview:self.payImageView];
        
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.titleLabel];
        
        self.currentMoney = [UILabel new];
        UIFont *textFont = [UIFont systemFontOfSize:13];
        self.currentMoney.font = textFont;
        self.currentMoney.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.currentMoney];
        
        self.selectedMethodButton = [UIButton new];
        [self.selectedMethodButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [self.selectedMethodButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.selectedMethodButton];

        UIButton *selectButton= [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, mainWidth-NORMOL_SPACE, self.contentView.frame.size.height)];
        [selectButton addTarget:self action:@selector(methedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:selectButton];
        self.wantPayLabel = [UILabel new];
        [self.contentView addSubview:self.wantPayLabel];
        self.payCount = [UITextField new];
        self.payCount.font = [UIFont systemFontOfSize:12];
        self.payCount.keyboardType = UIKeyboardTypeDecimalPad;//带小数点输入
        self.payCount.returnKeyType = UIReturnKeyDone;
        self.payCount.layer.borderWidth = 1.0f;
        self.payCount.layer.cornerRadius = 2;
        self.payCount.layer.borderColor = LINE_COLOR.CGColor;
        self.payCount.returnKeyType = UIReturnKeyDone;
        
        [self.contentView addSubview:self.payCount];
        
        
        self.limitMoneyLabel = [UILabel new];
        self.limitMoneyLabel.font = [UIFont qsh_systemFontOfSize:12];
        self.limitMoneyLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.limitMoneyLabel];
        
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR;
        [self.contentView addSubview:line];
        
      
        [selectButton  mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView);
            make.top.bottom.mas_equalTo(self.contentView);
        }];

        [self.payImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).with.mas_offset(10);
            make.top.mas_equalTo(self.contentView).with.mas_offset(20);
            make.width.height.mas_equalTo(imageSize);
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.payImageView.mas_right).with.mas_offset(10);
            make.top.mas_equalTo(self.contentView).with.mas_offset(10);
            make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
            make.height.mas_equalTo(titleHeight);
        }];
        
        [self.selectedMethodButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
            make.width.height.mas_equalTo(titleHeight);
            make.top.mas_equalTo(self.titleLabel.mas_bottom);
        }];
        
        [self.currentMoney mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLabel);
            make.right.mas_equalTo(self.selectedMethodButton.mas_left).with.mas_offset(-10);
            make.top.mas_equalTo(self.titleLabel.mas_bottom);
            make.height.mas_equalTo(titleHeight);
        }];
        
        
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self.currentMoney.mas_bottom).with.mas_offset(9+titleHeight);
            make.height.mas_equalTo(1);
        }];
        
        
    }
    return  self;
    
}

-(void)methedButtonAction:(UIButton *) sender{
    self.selectedMethodButton.selected = !self.selectedMethodButton.selected;
    if (self.selectedMethodButton.selected) {
        if ([self.payDelegate respondsToSelector:@selector(toPayWithMethodName:ruleId:status: balance:)]) {
            [self.payDelegate toPayWithMethodName:self.detailParser.wallet_en ruleId:self.ruleId status:[NSString stringWithFormat:@"%@",self.detailParser.status] balance:self.detailParser.balance];
        }
    } else {
        if ([self.payDelegate respondsToSelector:@selector(toCancelPayWithMethodName:ruleId:status:balance:)]) {
            
            [self.payDelegate toCancelPayWithMethodName:self.detailParser.wallet_en ruleId:self.ruleId status:[NSString stringWithFormat:@"%@",self.detailParser.status]balance:self.detailParser.balance];
        }
    }
    
}

-(void)setDetailParser:(PayConfigDetailParser *)detailParser{
    _detailParser = detailParser;
    self.payCount.tag = [self.ruleId integerValue];
    self.titleLabel.text = _detailParser.wallet_cn;
    CGFloat titleHeight = 20;
    NSString *currentString;
    currentString = [NSString stringWithFormat:@"当前余额:%@ (1支付额=%@%@)",_detailParser.balance,_detailParser.ratio,_detailParser.wallet_cn];
    
    
    UIFont *textFont = [UIFont systemFontOfSize:13];
    self.currentMoney.text = currentString;
    if ([_detailParser.is_edit integerValue]==0) {
        self.wantPayLabel.hidden = YES;
        self.payCount.hidden = YES;
        self.limitMoneyLabel.hidden = NO;
        [self.limitMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLabel);
            make.right.mas_equalTo(self.currentMoney);
            make.top.mas_equalTo(self.currentMoney.mas_bottom);
            make.height.mas_equalTo(self.titleLabel);
        }];
        
    } else {
        self.wantPayLabel.hidden = NO;
        self.limitMoneyLabel.hidden = YES;
        self.payCount.hidden = NO;
        CGSize size = CGSizeMake(300, 100);
        NSString *string = @"我要支付";
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName,nil];
        CGSize  actualsize =[string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
        self.wantPayLabel.text = string;
        self.wantPayLabel.font = textFont;
        self.wantPayLabel.adjustsFontSizeToFitWidth = YES;
        
        [self.wantPayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.currentMoney.mas_bottom);
            make.left.mas_equalTo(self.currentMoney);
            make.width.mas_equalTo(actualsize.width);
            make.height.mas_equalTo(titleHeight);
        }];
        
        [self.payCount mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.wantPayLabel.mas_right);
            make.right.mas_equalTo(self.currentMoney);
            make.top.mas_equalTo(self.currentMoney.mas_bottom);
            make.height.mas_equalTo(titleHeight);
        }];
        
    }
    
    if ([_detailParser.limit_max floatValue]) {
        
        if ([_detailParser.limit_min floatValue]) {
            self.payCount.placeholder = [NSString stringWithFormat:@"最多使用%.2f,最少使用%.2f",[_detailParser.limit_max floatValue],[_detailParser.limit_min floatValue]];
            self.limitMoneyLabel.text = [NSString stringWithFormat:@"最多使用%.2f,最少使用%.2f",[_detailParser.limit_max floatValue],[_detailParser.limit_min floatValue]];
        } else {
            self.payCount.placeholder = [NSString stringWithFormat:@"最多使用%.2f",[_detailParser.limit_max floatValue]];
            self.limitMoneyLabel.text = [NSString stringWithFormat:@"最多使用%.2f",[_detailParser.limit_max floatValue]];
            
        }
    }
   
    
}

@end
