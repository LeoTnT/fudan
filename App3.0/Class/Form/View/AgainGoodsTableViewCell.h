//
//  AgainGoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/11/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgainGoodsTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, copy) NSString *goodsString;
@end
