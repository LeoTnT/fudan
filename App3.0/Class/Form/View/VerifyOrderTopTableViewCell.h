//
//  VerifyOrderTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/9/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderAddressCommonView.h"
@interface VerifyOrderTopTableViewCell : UITableViewCell
@property (nonatomic, strong) OrderAddressCommonView *addressView;

@property (nonatomic, strong) NSArray *addressInformationArray;

@property (nonatomic, assign) CGFloat cellHeight;
@end
