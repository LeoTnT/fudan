//
//  DeliveryListTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DeliveryListTableViewCell.h"

@implementation DeliveryListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
         self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.nameLabel = [UILabel new];
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont qsh_systemFontOfSize:17];
        [self.contentView addSubview:self.nameLabel];
        
        self.addressLabel = [UILabel new];
        self.addressLabel.textColor = COLOR_666666;
        self.addressLabel.font = [UIFont qsh_systemFontOfSize:16];
        [self.contentView addSubview:self.addressLabel];
        
        self.phoneLabel = [UILabel new];
        self.phoneLabel.textColor = COLOR_666666;
        self.phoneLabel.font = [UIFont qsh_systemFontOfSize:16];
        [self.contentView addSubview:self.phoneLabel];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView);
            make.height.mas_equalTo(30);
            make.left.mas_equalTo(self.contentView).with.mas_offset(13);
            make.width.mas_lessThanOrEqualTo(mainWidth-13*2);
        }];
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLabel.mas_bottom);
            make.left.height.mas_equalTo(self.nameLabel);
            make.width.mas_lessThanOrEqualTo(mainWidth-13*2);
        }];
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.addressLabel.mas_bottom);
            make.left.height.mas_equalTo(self.nameLabel);
            make.width.mas_lessThanOrEqualTo(mainWidth-13*2);
        }];
    }
    return self;
}

-(void)setDetailParser:(ValidDeliveryObtainDetailParser *)detailParser {
    _detailParser = detailParser;
    self.nameLabel.text = _detailParser.name;
    self.addressLabel.text = [NSString stringWithFormat:@"地址：%@ %@ %@ %@ %@",_detailParser.province,_detailParser.city,_detailParser.county,_detailParser.town,_detailParser.address];
    self.phoneLabel.text = [NSString stringWithFormat:@"电话：%@",_detailParser.mobile];
}

@end
