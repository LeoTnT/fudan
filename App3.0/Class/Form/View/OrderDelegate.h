//
//  OrderDelegate.h
//  App3.0
//
//  Created by nilin on 2017/4/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderFormModel.h"

@protocol OrderDelegate <NSObject>
@optional
/** 取消交易*/
-(void)cancelTransaction:(NSString *) orderId;
@optional
/**  联系卖家*/
-(void) linkSeller:(NSString *) nameAndTelephone;
@optional
/**  支付*/
-(void) payMoney:(OrderDataParser *) orderParser;
@optional
/** 退款*/
-(void)refundMoney:(NSString *) orderId;
@optional
/** 确认收货*/
-(void)verifyReceived:(NSString *) orderId;
@optional
/** 查看物流*/
-(void)lookLogistics:(OrderDataParser *) orderParser;
@optional
/** 退货*/
-(void)returnSales:(NSString *) orderId money:(NSString *) money;
@optional
/** 我要评价*/
-(void)evaluateOrder:(OrderDataParser *) orderParser;
@optional
/**点击view进入订单详情页*/
-(void)toLookOrderDetail:(NSString *) orderId;

/**补差价*/
-(void)toFillPrice:(NSString *)goodId toId:(NSString *)toId;

/**提醒发货*/
- (void) toRemindOrderSend:(NSString *) orderId;

/**选中订单-》合并支付*/
- (void) selectedOrderWithOrderId:(NSString *) orderId  selected:(BOOL) isSelected;

/**追加评论*/
- (void) againEvaluateWithEvaluateIds:(NSString *) evaluateIds goodsNameAndLogo:(NSArray *) goodsInfoArray;

/**再次购买*/
- (void) againBuyWithOrderId:(NSString *) orderId;

@end
