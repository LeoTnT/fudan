//
//  OrderDetailTimeTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/9/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"

@interface OrderDetailTimeTableViewCell : UITableViewCell
@property (nonatomic, strong) BuyerOrderDetailParser *detailParser;

@property (nonatomic, assign) CGFloat cellHeight;
@end
