//
//  OrderDetailTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/9/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderDetailTopTableViewCell.h"

@implementation OrderDetailTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.cellHeight = 78;
    }
    return self;
}

-(void)setAddressInformationArray:(NSArray *)addressInformationArray {
    _addressInformationArray = addressInformationArray;
    [self.addressView removeFromSuperview];
    self.addressView = [[OrderAddressCommonView alloc]init];
    self.addressView.addressType = OrderAddressCommonViewTypeDetailOrder;
    [self.contentView addSubview:self.addressView];
    self.cellHeight = self.addressView.viewHeight;
    self.addressView.addressArray = _addressInformationArray;
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.addressView.viewHeight);
    }];
    self.cellHeight = self.addressView.viewHeight;
}

@end
