//
//  OrderGoodsView.h
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDelegate.h"
@protocol GoodsDetailDelegate<NSObject>
@optional
/**进入商品详情界面*/
-(void)toLookGoodsDetailInfoWithGoodsId:(NSString *) goodsId;
/**进入商品详情界面*/
-(void)toAgainEvaluateWithEvaId:(NSString *) evaId  goodsNameAndLogo:(NSArray *) goodsInfoArray;

/**退款/退货*/
-(void)refundMoneyOrGoodsWithId:(NSString *) orderId  product_ext_id:(NSString *)product_ext_id;

/**退款，退货详情*/
- (void) refundDetailWithRefoundId:(NSString *) refund_id;
@end
typedef  NS_ENUM(NSUInteger,OrderGoodsType)
{
    OrderGoodsTypeForOrder,
    OrderGoodsTypeForVerifyOrder,
    OrderGoodsTypeForDetail

};

@interface OrderGoodsView : UIView
@property(nonatomic,strong) NSArray *parserArray;//商品数组
@property(nonatomic,assign) CGFloat viewHeight;
@property(nonatomic,strong) UIView   *centerView;
@property(nonatomic,assign) OrderGoodsType  orderGoodsType;
@property(nonatomic,weak) id<GoodsDetailDelegate> orderDelegate;

@property (nonatomic, assign) BOOL againEvaluate;
@end
