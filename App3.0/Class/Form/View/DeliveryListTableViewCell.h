//
//  DeliveryListTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"

@interface DeliveryListTableViewCell : UITableViewCell
/**标题*/
@property (nonatomic, strong) UILabel *nameLabel;
/**地址*/
@property (nonatomic, strong) UILabel *addressLabel;
/**电话*/
@property (nonatomic, strong) UILabel *phoneLabel;

/**查看地图*/
//@property (nonatomic, strong) UIButton *lookMap;
@property (nonatomic, strong) ValidDeliveryObtainDetailParser *detailParser;
@end
