//
//  PopUpView.h
//  App3.0
//
//  Created by nilin on 2017/4/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopViewDelegate <NSObject>
@optional
-(void)clickToCallMan;
@optional
-(void)clickToMassageMan;
@optional
-(void)clickToCancelLink;
@end

typedef NS_ENUM(NSInteger,PopUpViewType) {
    PopUpViewTypeOrder,
    PopUpViewTypePhoto,
    
};
@interface PopUpView : UIView

@property (nonatomic, weak) id<PopViewDelegate> delegate;
@property (nonatomic, assign) PopUpViewType popUpViewType;
@property (nonatomic, strong) XSCustomButton *cancelBtn;
@property (nonatomic, strong) XSCustomButton *oneBtn;
@property (nonatomic, strong) XSCustomButton *otherBtn;
@end
