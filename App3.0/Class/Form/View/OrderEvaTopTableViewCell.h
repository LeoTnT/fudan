//
//  OrderEvaTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OrderFormModel.h"
@interface OrderEvaTopTableViewCell : UITableViewCell

@property(nonatomic,strong) UIImageView *img;

@property(nonatomic,strong) UILabel *titleLabel;

@property(nonatomic,strong) UILabel *payLabel;

@property (nonatomic,strong) UIButton *btn;

@property (nonatomic,copy) NSString *logo;

@property (nonatomic,copy) NSString *product_name;

@property (nonatomic,copy) NSString *totalMoney;



@end
