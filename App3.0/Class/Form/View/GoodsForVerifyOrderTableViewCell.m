//
//  GoodsForVerifyOrderTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsForVerifyOrderTableViewCell.h"
#import "OrderGoodsView.h"
#import "XSCustomButton.h"
@interface GoodsForVerifyOrderTableViewCell()<UITextViewDelegate>
{
    UIView *_bgView;
    UIButton *_officialBtn;
    UILabel *_carriageLabel;//运费
    
    UIView *_bottomView;
    UILabel *_total;
    UIButton *_linkSellerButton;//联系卖家
    UIButton *_shopMainPage;//店铺首页
    UIView *_nextView;
    
}
@property (nonatomic, strong) UIView *couponView;//抵扣券view
@property (nonatomic, strong) UILabel *couponTintLabel;

@property (nonatomic, strong) UIImageView  *couponImageView;
@end
@implementation GoodsForVerifyOrderTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setSubviews];
    }
    return self;
}
-(void)toLinkSupply:(UIButton *) sender{
    
    PreviewDeliveryListParser *delivery = [self.shopParser.delivery_list firstObject];
    PreviewVerifyOrderDetailProductParser *productParser = [delivery.product_list firstObject];
    if ([self.verifyOrderDelegate respondsToSelector:@selector(linkSupplyWithSupplyName:)]) {
        [self.verifyOrderDelegate linkSupplyWithSupplyName: productParser.user_id];
    }
    
}
-(void)toLookSupplyShop:(UIButton *) sender{
    if ([self.verifyOrderDelegate respondsToSelector:@selector(lookShopMainPageWithSupplyId:)]) {
        
        [self.verifyOrderDelegate lookShopMainPageWithSupplyId:[NSString stringWithFormat:@"%@",self.shopParser.supply_id]];
    }
    
}
-(void)setSubviews{
    CGFloat bgViewHeight = 40;
    _bgView= [[UIView alloc] initWithFrame:CGRectMake(0, 0,mainWidth, bgViewHeight)];
    _bgView.backgroundColor = [UIColor whiteColor];
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
    topView.backgroundColor = BG_COLOR;
    [_bgView addSubview:topView];
    CGFloat normalButtonWidth = 8*NORMOL_SPACE;
    _officialBtn = [[UIButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(topView.frame),normalButtonWidth+4*NORMOL_SPACE , bgViewHeight-CGRectGetHeight(topView.frame))];
    _officialBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _officialBtn.titleLabel.font = [UIFont qsh_systemFontOfSize:15];
    [_officialBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_officialBtn setImage:[UIImage imageNamed:@"user_cart_store"] forState:UIControlStateNormal];
    [_officialBtn setImage:[UIImage imageNamed:@"user_cart_store"] forState:UIControlStateSelected];
    _officialBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(_officialBtn.imageView.frame)+NORMOL_SPACE*2, 0, 0);
    _officialBtn.titleEdgeInsets = UIEdgeInsetsMake(0, NORMOL_SPACE, 0, 0);
    [_bgView addSubview:_officialBtn];
    //    _officialBtn.backgroundColor = [UIColor blueColor];
    //    _officialBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    _linkSellerButton= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE-normalButtonWidth*2, CGRectGetMaxY(topView.frame), normalButtonWidth, bgViewHeight-CGRectGetHeight(topView.frame))];
    _linkSellerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _linkSellerButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [_linkSellerButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_linkSellerButton setTitle:@"联系卖家" forState:UIControlStateNormal];
    [_linkSellerButton setImage:[UIImage imageNamed:@"mall_verify_link"] forState:UIControlStateNormal];
    [_linkSellerButton setImage:[UIImage imageNamed:@"mall_verify_link"] forState:UIControlStateSelected];
    _linkSellerButton.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(_linkSellerButton.imageView.frame)+NORMOL_SPACE*2, 0, 0);
    _linkSellerButton.titleEdgeInsets = UIEdgeInsetsMake(0, NORMOL_SPACE/2, 0, 0);
    [_linkSellerButton addTarget:self action:@selector(toLinkSupply:) forControlEvents:UIControlEventTouchUpInside];
    _linkSellerButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_bgView addSubview:_linkSellerButton];
    //    _linkSellerButton.backgroundColor = mainColor;
    _shopMainPage= [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_linkSellerButton.frame), CGRectGetMaxY(topView.frame), normalButtonWidth, bgViewHeight-CGRectGetHeight(topView.frame))];
    _shopMainPage.titleLabel.textAlignment = NSTextAlignmentLeft;
    _shopMainPage.titleLabel.adjustsFontSizeToFitWidth = YES;
    _shopMainPage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _shopMainPage.titleLabel.font = [UIFont systemFontOfSize:12];
    [_shopMainPage setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_shopMainPage setTitle:@"店铺主页" forState:UIControlStateNormal];
    [_shopMainPage setImage:[UIImage imageNamed:@"mall_verify_shop"] forState:UIControlStateNormal];
    [_shopMainPage setImage:[UIImage imageNamed:@"mall_verify_shop"] forState:UIControlStateSelected];
    _shopMainPage.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(_shopMainPage.imageView.frame)+NORMOL_SPACE*2, 0, 0);
    _shopMainPage.titleEdgeInsets = UIEdgeInsetsMake(0, NORMOL_SPACE/2, 0, 0);
    [_shopMainPage setTitle:@"店铺主页" forState:UIControlStateNormal];
    [_shopMainPage addTarget:self action:@selector(toLookSupplyShop:) forControlEvents:UIControlEventTouchUpInside];
    //    _shopMainPage.backgroundColor = [UIColor yellowColor];
    [_bgView addSubview:_shopMainPage];
    [self.contentView addSubview:_bgView];
    _shopMainPage.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    CGFloat nextViewHeight = 100;
    _nextView = [[UIView alloc] initWithFrame:CGRectMake(0, 200+2*NORMOL_SPACE, mainWidth, nextViewHeight)];
    _nextView.backgroundColor = [UIColor whiteColor];
    _carriageLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, 120, nextViewHeight/2-NORMOL_SPACE)];
    _carriageLabel.font = [UIFont systemFontOfSize:15];
    _carriageLabel.adjustsFontSizeToFitWidth = YES;
    [_nextView addSubview:_carriageLabel];
    
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_carriageLabel.frame)+NORMOL_SPACE, mainWidth, 1)];
    
    [_nextView addSubview:line];
    line.backgroundColor = LINE_COLOR_NORMAL;
    self.remark = [[UITextView alloc] initWithFrame:CGRectMake(NORMOL_SPACE,CGRectGetMaxY(line.frame)+NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, 4*NORMOL_SPACE)];
    self.remark.scrollEnabled = YES;
    self.remark.font = [UIFont systemFontOfSize:15];
    self.remark.layer.borderColor = mainGrayColor.CGColor;
    self.remark.layer.borderWidth = 1.0;
    self.remark.layer.cornerRadius = 3.0f;
    self.remark.returnKeyType = UIReturnKeyDone;
    //    UITapGestureRecognizer *tap = [UITapGestureRecognizer alloc] initWithTarget:self action:@selector(<#selector#>);
    self.tintInfo = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.remark.frame)+NORMOL_SPACE/3, CGRectGetMinY(self.remark.frame), 120, CGRectGetHeight(self.remark.frame))];
    self.tintInfo.backgroundColor = [UIColor clearColor];
    self.tintInfo.textColor = LINE_COLOR;
    self.tintInfo.font = [UIFont systemFontOfSize:15];
    self.tintInfo.text = @"给卖家留言";
    
    [self.remark.layer setMasksToBounds:YES];
    
    [_nextView addSubview:self.remark];
    [_nextView addSubview:self.tintInfo];
    //     self.tintInfo.hidden = NO;
    
    [self.contentView addSubview:_nextView];
    CGFloat bottomHeight = 50;
    _total= [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(_nextView.frame)+NORMOL_SPACE, mainWidth-NORMOL_SPACE*2, bottomHeight)];
    _total.textAlignment = NSTextAlignmentRight;
    //    _total.backgroundColor = [UIColor grayColor];
    _total.font = [UIFont systemFontOfSize:14];
    _total.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:_total];
    
    
    self.couponView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    [self.contentView addSubview:self.couponView];
    self.couponView.hidden = YES;
    self.couponTintLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, 120, CGRectGetHeight(self.couponView.frame))];
    self.couponTintLabel.text = @"使用抵扣券";
    self.couponTintLabel.font = [UIFont systemFontOfSize:14];
    self.couponTintLabel.textColor = [UIColor hexFloatColor:@"111111"];
    [self.couponView addSubview:self.couponTintLabel];
    
    self.couponImageView = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-10-15, 15, 10, 14)];
    self.couponImageView.image = [UIImage imageNamed:@"logistic_right_nav"];
    [self.couponView addSubview:self.couponImageView];
    
    self.couponLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.couponTintLabel.frame)+5, 0, CGRectGetMinX(self.couponImageView.frame)-5-17-CGRectGetMaxX(self.couponTintLabel.frame), CGRectGetHeight(self.couponView.frame))];
    self.couponLabel.text = @"请选择抵扣券";
    self.couponLabel.textAlignment = NSTextAlignmentRight;
    self.couponLabel.font = [UIFont systemFontOfSize:14];
    self.couponLabel.textColor = COLOR_666666;
    [self.couponView addSubview:self.couponLabel];
    
    UILabel *cLine = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.couponView.frame)-1, mainWidth, 1)];
    cLine.backgroundColor = LINE_COLOR_NORMAL;
    [self.couponView addSubview:cLine];
    
    
    
    
}

-(void)setShopParser:(PreviewVerifyOrderProductsParser *)shopParser{
    _shopParser = shopParser;
    self.remark.tag = [_shopParser.supply_id integerValue];
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass: [OrderGoodsView class]]) {
            [view removeFromSuperview];
        }
    }
    NSString *name = _shopParser.sup_name;
    [_officialBtn setTitle:name forState:UIControlStateNormal];
    CGFloat nextViewHeight = 100, couponViewHeight = 44;
    PreviewDeliveryListParser *deliveryParser = [_shopParser.delivery_list firstObject];
    CGFloat logisButtonWidth = deliveryParser.logis_type.count>2?(mainWidth-140-NORMOL_SPACE*deliveryParser.logis_type.count)/deliveryParser.logis_type.count:80;
    for (int i=0;i<deliveryParser.logis_type.count;i++) {
        @autoreleasepool {
            PreviewVerifyWuliuParser *paser = deliveryParser.logis_type[i];
            
            XSCustomButton *button = [[XSCustomButton alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE*(i+1)-logisButtonWidth*(i+1), 0, logisButtonWidth, 3.5*NORMOL_SPACE) title:paser.logis_name titleColor:mainGrayColor fontSize:15 backgroundColor:[UIColor whiteColor] higTitleColor:mainGrayColor highBackgroundColor:[UIColor whiteColor]];
            [button removeTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [button setBorderWith:1 borderColor:mainGrayColor.CGColor cornerRadius:5];
            [button setTitleColor:mainColor forState:UIControlStateSelected];
            button.tag = [paser.logis_id integerValue]+100;
            if (button.tag==self.selectedLogisticType) {
                //                [self buttonClick:button];
                button.selected = YES;
                [button setBorderWith:1 borderColor:mainColor.CGColor cornerRadius:5];
                button.userInteractionEnabled = NO;
            }
            button.titleLabel.adjustsFontSizeToFitWidth = YES;
            PreviewVerifyWuliuParser *parser = deliveryParser.logis_type[i];
            button.titleLabel.text = parser.logis_name;
            [_nextView addSubview:button];
            
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    //商品展示view
    OrderGoodsView *orderView = [[OrderGoodsView alloc] init];
    //商品数组
    orderView.orderGoodsType = OrderGoodsTypeForVerifyOrder;
    PreviewDeliveryListParser *delivery = [_shopParser.delivery_list firstObject];
    orderView.parserArray = delivery.product_list;
    orderView.frame = CGRectMake(0, CGRectGetMaxY(_bgView.frame), mainWidth, orderView.viewHeight);
    [self.contentView addSubview:orderView];
    //添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToLookGoodsDetail)];
    orderView.userInteractionEnabled = YES;
    [orderView addGestureRecognizer:tap];
    
    BOOL  isShowCoupon = NO;
    for (PreviewVerifyOrderDetailProductParser *productParser in delivery.product_list) {
        if ([productParser.coupon intValue]>0&&[productParser.sell_type integerValue]==1) {
            //目前只有普通商品显示抵扣券
            isShowCoupon = YES;
            break;
        }
    }
    if (isShowCoupon) {
        self.couponView.frame = CGRectMake(0, CGRectGetMaxY(orderView.frame), mainWidth, couponViewHeight);
        self.couponView.hidden = NO;
//        self.couponLabel.text = [NSString stringWithFormat:@"%@元",@"100"];
        UITapGestureRecognizer *couponTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponAction:)];
        self.couponView.userInteractionEnabled = YES;
        [self.couponView addGestureRecognizer:couponTap];
        
    } else {
         self.couponView.hidden = YES;
    }
    
    
    if (self.deliveryParser) {
        self.deliveryView = [[VerifyDeliveryView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_carriageLabel.frame)+20, mainWidth, 60)];
        self.deliveryView.nameLabel.text = self.deliveryParser.name;
        self.deliveryView.addressLabel.text = [NSString stringWithFormat:@"地址：%@ %@ %@ %@ %@",self.deliveryParser.province,self.deliveryParser.city,self.deliveryParser.county,self.deliveryParser.town,self.deliveryParser.address];
        [self.deliveryView.changeButton addTarget:self action:@selector(clickToChangeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_nextView addSubview:self.deliveryView];

        self.remark.frame = CGRectMake(NORMOL_SPACE,CGRectGetMaxY(self.deliveryView.frame)+2*NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, 4*NORMOL_SPACE);
        self.tintInfo.frame = CGRectMake(CGRectGetMinX(self.remark.frame)+NORMOL_SPACE/3, CGRectGetMinY(self.remark.frame), 120, CGRectGetHeight(self.remark.frame));
        
        _nextView.frame  = CGRectMake(0,  isShowCoupon?CGRectGetMaxY(self.couponView.frame)+2*NORMOL_SPACE: CGRectGetMaxY(orderView.frame)+2*NORMOL_SPACE, mainWidth, nextViewHeight+80);
        
    } else {
        self.remark.frame = CGRectMake(NORMOL_SPACE,CGRectGetMaxY(_carriageLabel.frame)+2*NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, 4*NORMOL_SPACE);
        self.tintInfo.frame = CGRectMake(CGRectGetMinX(self.remark.frame)+NORMOL_SPACE/3, CGRectGetMinY(self.remark.frame), 120, CGRectGetHeight(self.remark.frame));
        _nextView.frame  = CGRectMake(0, isShowCoupon?CGRectGetMaxY(self.couponView.frame)+2*NORMOL_SPACE:CGRectGetMaxY(orderView.frame)+2*NORMOL_SPACE, mainWidth, nextViewHeight);
        
    }

    NSString *yunfeiStr;
    if ([delivery.yunfei_total integerValue]==0) {
        if (self.selectedLogisticType==0) {
            yunfeiStr = [NSString stringWithFormat:@"运费:"];
        } else if (self.selectedLogisticType-100==8) {
             yunfeiStr = [NSString stringWithFormat:@"运费:运费到付"];
        } else {
            yunfeiStr = [NSString stringWithFormat:@"运费:免邮"];
        }
       
    } else {
        yunfeiStr = [NSString stringWithFormat:@"运费:¥%.2f",[delivery.yunfei_total floatValue]];
    }
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:yunfeiStr];
    [attStr addAttribute:NSForegroundColorAttributeName
                   value:[UIColor redColor]
                   range:NSMakeRange(3, yunfeiStr.length-3)];
    _carriageLabel.attributedText = attStr;
    
    CGFloat bottomHeight = 50;
    _total.frame = CGRectMake(NORMOL_SPACE, CGRectGetMaxY(_nextView.frame)+NORMOL_SPACE, mainWidth-NORMOL_SPACE*2, bottomHeight);
    _total.textAlignment = NSTextAlignmentRight;
    //    _total.backgroundColor = [UIColor grayColor];
    _total.font = [UIFont systemFontOfSize:14];
    
    _total.text = [NSString stringWithFormat:@"共计%@件商品，合计金额¥%.2f", _shopParser.total_num, [_shopParser.total_sell_price floatValue]+[delivery.yunfei_total floatValue]];
    self.cellHeight = CGRectGetMaxY(_total.frame);
//    self.frame = CGRectMake(0, 0, mainWidth, self.cellHeight);
    
}

- (void)couponAction:(UITapGestureRecognizer *) tap {
    if (self.verifyOrderDelegate&&[self.verifyOrderDelegate respondsToSelector: @selector(lookCoupon)]) {
        [self.verifyOrderDelegate lookCoupon];
    }
}

-(void) tapToLookGoodsDetail{
    //     PreviewVerifyOrderDetailProductParser *productParser =  [self.shopParser.product_list firstObject];
    //    [self.verifyOrderDelegate lookGoodsDetailWithGoodsId:[NSString stringWithFormat:@"%@",productParser.product_id]];
}

- (void)clickToChangeAction:(UIButton *) sender {
    if ([self.verifyOrderDelegate respondsToSelector:@selector(changeDeliveryWithSupplyName:)]) {
        [self.verifyOrderDelegate changeDeliveryWithSupplyName:self.shopParser.sup_uname];
    }
}
- (void)buttonClick:(XSCustomButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [sender setBorderWith:1 borderColor:mainColor.CGColor cornerRadius:5];
        sender.userInteractionEnabled = NO;
        for (UIView *view in _nextView.subviews) {
            if ([view isKindOfClass:[XSCustomButton class]]) {
                if (view.tag!=sender.tag) {
                    ((XSCustomButton *)view).selected = NO;
                    [((XSCustomButton *)view) setBorderWith:1 borderColor:mainGrayColor.CGColor cornerRadius:5];
                    ((XSCustomButton *)view).userInteractionEnabled = YES;
                }
            }
        }
        
        PreviewDeliveryListParser *deliveryParser = [self.shopParser.delivery_list firstObject];
        PreviewVerifyWuliuParser *wuliuPaser;
        for (PreviewVerifyWuliuParser *parser in deliveryParser.logis_type) {
            if ([parser.logis_id integerValue]==sender.tag-100) {
                wuliuPaser = parser;
                break;
            }
        }
        if ([self.verifyOrderDelegate respondsToSelector:@selector(changeLogisticWithParser:supplyName:)]) {
            [self.verifyOrderDelegate changeLogisticWithParser:wuliuPaser supplyName:self.shopParser.sup_uname];
        }
    } else {
        [sender setBorderWith:1 borderColor:mainGrayColor.CGColor cornerRadius:5];
        sender.userInteractionEnabled = YES;
        //        [sender setTitleColor:mainGrayColor forState:UIControlStateNormal];
    }
}
@end
