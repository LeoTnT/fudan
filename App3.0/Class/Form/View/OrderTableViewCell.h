//
//  OrderTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"
#import "OrderDelegate.h"
#import "OrderGoodsView.h"
typedef NS_ENUM(NSInteger, OrderStatusType) {
    OrderStatusTypeNormal,
    OrderStatusTypeWaitPay,
    OrderStatusTypeWaitSend
};

@interface OrderTableViewCell : UITableViewCell<OrderDelegate>

@property(nonatomic,copy) NSString * is_refund;
@property(nonatomic,copy) NSString * is_reject;
@property(nonatomic,strong) OrderDataParser *dataParser;//=》订单界面
@property(nonatomic,assign) CGFloat cellHeight;
@property(nonatomic,weak) id<OrderDelegate> orderDelegate;
@property(nonatomic,strong) BuyerOrderParser *orderParser;//=》订单详情界面
@property(nonatomic,strong) OrderGoodsView *goodsView;//商品详情view

@property (nonatomic, assign) OrderStatusType statusType;

@property (nonatomic ,copy)void (^supplyTapAction)(NSString *supId);
@property (nonatomic, strong) UIButton *selectedButton;//待付款的选中按钮
@end
