
//
//  OrderGoodsView.m
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderGoodsView.h"
#import "OrderFormModel.h"
#import "UIImage+XSWebImage.h"


@implementation OrderGoodsView

-(void)setParserArray:(NSArray *)parserArray{
    _parserArray = parserArray;
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    CGFloat cHeight = 90,imageSize = 70,titleHeight = 40;
    UIColor *wordColor = [UIColor hexFloatColor:@"888888"];
    for (int i=0; i<_parserArray.count; i++) {
        @autoreleasepool {
            if (self.orderGoodsType==OrderGoodsTypeForVerifyOrder) {
                PreviewVerifyOrderDetailProductParser *verifyDataParser = _parserArray[i];
                self.centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*cHeight+NORMOL_SPACE, mainWidth, cHeight)];
                self.centerView.backgroundColor = BG_COLOR;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageSize, imageSize)];
                //                img.contentMode = UIViewContentModeScaleAspectFit;
                [img getImageWithUrlStr:[verifyDataParser.image containsString:@"http"]?verifyDataParser.image:[NSString stringWithFormat:@"%@,%@",ImageBaseUrl,verifyDataParser.image] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                [ self.centerView addSubview:img];
                //名称
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE*2, CGRectGetMinY(img.frame), imageSize, imageSize)];
                title.numberOfLines = 2;
                //自适应高度和宽度
                NSString *str = verifyDataParser.product_name;
                UIFont *tFont = [UIFont systemFontOfSize:15];
                title.font = tFont;
                title.lineBreakMode = NSLineBreakByTruncatingTail;
                title.text = str;
                CGSize size = CGSizeMake(300, 200);
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
                CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-2*NORMOL_SPACE-titleHeight, actualsize.height);
                [ self.centerView addSubview:title];
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE, imageSize, imageSize)];
                //自适应高度和宽度
                
                NSString *descStr = [NSString stringWithFormat:@"属性:%@ 库存:%@件",verifyDataParser.spec_name,verifyDataParser.stock];
                UIFont *dFont = [UIFont systemFontOfSize:13];
                desc.font = dFont;
                desc.lineBreakMode = NSLineBreakByTruncatingTail;
                desc.text = descStr;
                CGSize sizet = CGSizeMake(300, 200);
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:dFont,NSFontAttributeName,nil];
                CGSize  actualsizet =[descStr boundingRectWithSize:sizet options:NSStringDrawingUsesLineFragmentOrigin  attributes:dict context:nil].size;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,actualsizet.width+NORMOL_SPACE, actualsizet.height);
                [ self.centerView addSubview:desc];
                desc.adjustsFontSizeToFitWidth = YES;
                //价格
                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*3, CGRectGetMinY(title.frame), titleHeight*3-NORMOL_SPACE, NORMOL_SPACE*2)];
                NSString *str3;
                if ([verifyDataParser.sell_type integerValue]==2) {
                    if ([verifyDataParser.score floatValue]>0&&[verifyDataParser.sell_price floatValue]>0) {
                        str3 = [NSString stringWithFormat:@"积分%@ + ¥%@",verifyDataParser.score,verifyDataParser.sell_price];
                    } else {
                        if ([verifyDataParser.score floatValue]>0) {
                            str3 = [NSString stringWithFormat:@"积分%@",verifyDataParser.score];
                        }
                    }
                    
                } else {
                    
                    str3 = [NSString stringWithFormat:@"¥%.2f",[verifyDataParser.sell_price floatValue]];
                }
                
                price.text = str3;
                UIFont *tFont3 = [UIFont systemFontOfSize:15];
                price.font = tFont3;
                price.lineBreakMode = NSLineBreakByTruncatingTail;
                price.text = str3;
                CGSize size3 = CGSizeMake(300, 200);
                NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:tFont3,NSFontAttributeName,nil];
                CGSize  actualsize3 =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic3 context:nil].size;
                price.textColor = [UIColor redColor];
                price.textAlignment = NSTextAlignmentRight;
                //                price.backgroundColor = BG_COLOR;
                price.frame =CGRectMake(mainWidth-actualsize3.width-NORMOL_SPACE, CGRectGetMinY(title.frame), actualsize3.width, CGRectGetHeight(title.frame));
                [ self.centerView addSubview:price];
                price.adjustsFontSizeToFitWidth = YES;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, NORMOL_SPACE,mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), 40);
                //数量
                UILabel *number = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(price.frame), CGRectGetMaxY(price.frame), CGRectGetWidth(price.frame), NORMOL_SPACE*2)];
                NSString *str4 = [NSString stringWithFormat:@"×%@",verifyDataParser.total_num];
                number.text = str4;
                UIFont *tFont4 = [UIFont systemFontOfSize:15];
                number.font = tFont4;
                number.lineBreakMode = NSLineBreakByTruncatingTail;
                number.text = str4;
                CGSize size4 = CGSizeMake(300, 200);
                NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:tFont4,NSFontAttributeName,nil];
                CGSize  actualsize4 =[str4 boundingRectWithSize:size4 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic4 context:nil].size;
                number.textColor = [UIColor blackColor];
                number.textAlignment = NSTextAlignmentRight;
                //                number.backgroundColor = BG_COLOR;
                number.frame = CGRectMake(mainWidth-actualsize4.width-NORMOL_SPACE, CGRectGetMaxY(price.frame)+NORMOL_SPACE, actualsize4.width, actualsize4.height);
                [ self.centerView addSubview:number];
                number.adjustsFontSizeToFitWidth = YES;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), actualsize.height);
                number.adjustsFontSizeToFitWidth = YES;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(number.frame), actualsizet.height);
                [self addSubview: self.centerView];
                self.viewHeight = cHeight*_parserArray.count+NORMOL_SPACE;
                
            }
            if (self.orderGoodsType==OrderGoodsTypeForDetail){
                self.backgroundColor = [UIColor whiteColor];
                cHeight = 94;
                BuyerOrderListDetailParser *dataParser = _parserArray[i];
                self.centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*cHeight, mainWidth, cHeight)];
                self.centerView.backgroundColor = BG_COLOR;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, imageSize, imageSize)];
                //                img.contentMode = UIViewContentModeScaleAspectFit;
                [img getImageWithUrlStr:[dataParser image] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                [ self.centerView addSubview:img];
                //名称
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+12, 14.5, imageSize, 40)];
                //自适应高度和宽度
                NSString *str = dataParser.product_name;
                UIFont *tFont = [UIFont systemFontOfSize:15];
                title.font = tFont;
                title.lineBreakMode = NSLineBreakByTruncatingTail;
                title.text = str;
                title.textAlignment = NSTextAlignmentLeft;
                CGSize size = CGSizeMake(300, 200);
                title.numberOfLines = 2;
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
                CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+12, 14.5,mainWidth- imageSize-2*NORMOL_SPACE-titleHeight, actualsize.height);
                [ self.centerView addSubview:title];
                title.adjustsFontSizeToFitWidth = YES;
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE, imageSize, imageSize)];
                //自适应高度和宽度
                NSString *descStr = dataParser.spec_name;
                desc.numberOfLines = 2;
                UIFont *dFont = [UIFont systemFontOfSize:12];
                desc.font = dFont;
                desc.lineBreakMode = NSLineBreakByTruncatingTail;
                desc.text = descStr;
                CGSize sizet = CGSizeMake(300, 200);
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:dFont,NSFontAttributeName,nil];
                CGSize  actualsizet =[descStr boundingRectWithSize:sizet options:NSStringDrawingUsesLineFragmentOrigin  attributes:dict context:nil].size;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame),actualsizet.width+NORMOL_SPACE, actualsizet.height);
                [ self.centerView addSubview:desc];
                desc.adjustsFontSizeToFitWidth = YES;
                //价格
                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*3, CGRectGetMinY(title.frame), titleHeight*3-NORMOL_SPACE, NORMOL_SPACE*2)];
                NSString *str3 = [NSString stringWithFormat:@"¥%.2f",[dataParser.sell_price floatValue]];
                if ([dataParser.sell_type integerValue] == 2) {
                    str3 = [NSString stringWithFormat:@"积分价:%@",dataParser.score];
                }
                price.text = str3;
                UIFont *tFont3 = [UIFont systemFontOfSize:15];
                price.font = tFont3;
                price.lineBreakMode = NSLineBreakByTruncatingTail;
                price.text = str3;
                CGSize size3 = CGSizeMake(300, 200);
                NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:tFont3,NSFontAttributeName,nil];
                CGSize  actualsize3 =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic3 context:nil].size;
                price.textColor = [UIColor blackColor];
                price.textAlignment = NSTextAlignmentRight;
                //                price.backgroundColor = BG_COLOR;
                price.frame =CGRectMake(mainWidth-actualsize3.width-NORMOL_SPACE, CGRectGetMinY(title.frame), actualsize3.width, CGRectGetHeight(title.frame));
                
                [ self.centerView addSubview:price];
                price.adjustsFontSizeToFitWidth = YES;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), imageSize/3*2);
                //数量
                UILabel *number = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(price.frame), CGRectGetMaxY(price.frame), CGRectGetWidth(price.frame), NORMOL_SPACE*2)];
                NSString *str4 = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                number.text = str4;
                UIFont *tFont4 = [UIFont systemFontOfSize:13];
                number.font = tFont4;
                number.lineBreakMode = NSLineBreakByTruncatingTail;
                number.text = str4;
                CGSize size4 = CGSizeMake(300, 200);
                NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:tFont4,NSFontAttributeName,nil];
                CGSize  actualsize4 =[str4 boundingRectWithSize:size4 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic4 context:nil].size;
                number.textColor = wordColor;
                number.textAlignment = NSTextAlignmentRight;
                //                number.backgroundColor = BG_COLOR;
                number.frame = CGRectMake(mainWidth-actualsize4.width-NORMOL_SPACE, CGRectGetMaxY(price.frame)+NORMOL_SPACE, actualsize4.width, actualsize4.height);
                [ self.centerView addSubview:number];
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+NORMOL_SPACE, CGRectGetMinY(img.frame),mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(price.frame), actualsize.height);
                number.adjustsFontSizeToFitWidth = YES;
                
                desc.textColor = wordColor;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,mainWidth- imageSize-3*NORMOL_SPACE-CGRectGetWidth(number.frame), actualsizet.height);
                if (([dataParser.status integerValue]==1||[dataParser.status integerValue]==2)) {
                    if (!isEmptyString(dataParser.can_refund) ) {
                        cHeight+=40;
                        self.centerView.frame = CGRectMake(0, i*cHeight, mainWidth, cHeight);
                        XSCustomButton *refundButton;
                        if ([dataParser.can_refund integerValue]==1) {
                            refundButton = [[XSCustomButton alloc] initWithTitle:dataParser.refund_status_cn titleColor:[UIColor blackColor] font:[UIFont systemFontOfSize:12] cornerRadius:2 backGroundColor:BG_COLOR hBackGroundColor:BG_COLOR];
                            refundButton.frame = CGRectMake(CGRectGetWidth(self.centerView.frame)-80-10, CGRectGetMaxY(img.frame)+10, 80, 27);
                            [refundButton setBorderWith:0.5 borderColor:COLOR_666666.CGColor cornerRadius:2];
                            [refundButton addTarget:self action:@selector(toRefundAction:) forControlEvents:UIControlEventTouchUpInside];
                        } else if ([dataParser.can_refund integerValue]==0) {
                            
                            refundButton = [[XSCustomButton alloc] initWithTitle:dataParser.refund_status_cn titleColor:[UIColor blackColor] font:[UIFont systemFontOfSize:12] cornerRadius:2 backGroundColor:BG_COLOR hBackGroundColor:BG_COLOR];
                            refundButton.frame = CGRectMake(CGRectGetWidth(self.centerView.frame)-80-10, CGRectGetMaxY(img.frame)+10, 80, 27);
                            [refundButton setBorderWith:0.5 borderColor:COLOR_666666.CGColor cornerRadius:2];
                            [refundButton addTarget:self action:@selector(toLookRefundDetail:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                        [self.centerView addSubview:refundButton];
                    }
                    
                }
                
                self.centerView.tag = i+2000;
                //添加手势
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToLookGoodsDetailInfo:)];
                self.centerView.userInteractionEnabled = YES;
                [ self.centerView addGestureRecognizer:tap];
                [self addSubview: self.centerView];
                self.viewHeight = cHeight*_parserArray.count;
                
            }
            
            if (self.orderGoodsType==OrderGoodsTypeForOrder) {
                cHeight = 94;
                OrderDetailDataParser *dataParser = _parserArray[i];
                self.centerView = [[UIView alloc] initWithFrame:CGRectMake(0, i*cHeight, mainWidth, cHeight)];
                self.centerView.backgroundColor = ORDER_BG_COLOR;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, imageSize, imageSize)];
                //                img.contentMode = UIViewContentModeScaleAspectFit;
                [img getImageWithUrlStr:[dataParser image] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                [ self.centerView addSubview:img];
                //名称
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+12, 14.5, imageSize, imageSize)];
                title.numberOfLines = 2;
                //自适应高度和宽度
                NSString *str = dataParser.product_name;
                UIFont *tFont = [UIFont systemFontOfSize:15];
                title.font = tFont;
                title.lineBreakMode = NSLineBreakByTruncatingTail;
                title.text = str;
                CGSize size = CGSizeMake(300, 200);
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tFont,NSFontAttributeName,nil];
                CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+12, 14.5,mainWidth- imageSize-3*12-titleHeight, actualsize.height>32?32:actualsize.height);
                [ self.centerView addSubview:title];
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+10, imageSize, imageSize)];
                desc.numberOfLines = 2;
                //自适应高度和宽度
                NSString *descStr = dataParser.spec_name;
                UIFont *dFont = [UIFont systemFontOfSize:12];
                desc.font = dFont;
                desc.lineBreakMode = NSLineBreakByTruncatingTail;
                desc.text = descStr;
                desc.textColor = wordColor;
                CGSize sizet = CGSizeMake(300, 200);
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:dFont,NSFontAttributeName,nil];
                CGSize  actualsizet =[descStr boundingRectWithSize:sizet options:NSStringDrawingUsesLineFragmentOrigin  attributes:dict context:nil].size;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+10,actualsizet.width+10, actualsizet.height);
                [ self.centerView addSubview:desc];
                
                desc.adjustsFontSizeToFitWidth = YES;
                //价格
                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-titleHeight*3, 16, titleHeight*3-NORMOL_SPACE, NORMOL_SPACE*2)];
                
                NSString *str3 = [NSString stringWithFormat:@"¥%.2f",[dataParser.sell_price floatValue]];
                if ([dataParser.sell_type integerValue] == 2) {
                    str3 = [NSString stringWithFormat:@"积分价:%@",dataParser.score];
                }
                price.text = str3;
                UIFont *tFont3 = [UIFont systemFontOfSize:15];
                price.font = tFont3;
                price.lineBreakMode = NSLineBreakByTruncatingTail;
                price.text = str3;
                CGSize size3 = CGSizeMake(300, 200);
                NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:tFont3,NSFontAttributeName,nil];
                CGSize  actualsize3 =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic3 context:nil].size;
                price.textColor = [UIColor blackColor];
                price.textAlignment = NSTextAlignmentRight;
                //                price.backgroundColor = BG_COLOR;
                price.frame =CGRectMake(mainWidth-actualsize3.width-12, 16, actualsize3.width, 12);
                [ self.centerView addSubview:price];
                price.adjustsFontSizeToFitWidth = YES;
                //数量
                UILabel *number = [[UILabel alloc] init];
                NSString *str4 = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                number.text = str4;
                UIFont *tFont4 = [UIFont systemFontOfSize:13];
                number.font = tFont4;
                number.lineBreakMode = NSLineBreakByTruncatingTail;
                number.text = str4;
                CGSize size4 = CGSizeMake(300, 200);
                NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:tFont4,NSFontAttributeName,nil];
                CGSize  actualsize4 =[str4 boundingRectWithSize:size4 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic4 context:nil].size;
                number.textColor = wordColor;
                number.textAlignment = NSTextAlignmentRight;
                
                //            number.text = [NSString stringWithFormat:@"×%@",dataParser.product_num];
                //            number.textAlignment = NSTextAlignmentRight;
                //                number.backgroundColor = BG_COLOR;
                
                [ self.centerView addSubview:number];
                number.frame = CGRectMake(mainWidth-actualsize4.width-12.5, CGRectGetMaxY(price.frame)+NORMOL_SPACE, actualsize4.width, actualsize4.height);
                title.frame = CGRectMake(CGRectGetMaxX(img.frame)+12, 14.5,mainWidth- imageSize-3*12-CGRectGetWidth(price.frame), actualsize.height);
                number.adjustsFontSizeToFitWidth = YES;
                desc.frame = CGRectMake(CGRectGetMinX(title.frame), CGRectGetMaxY(title.frame)+NORMOL_SPACE,mainWidth- imageSize-3*12-CGRectGetWidth(number.frame), actualsizet.height);
                
                UILabel *evaluateStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-10-100, CGRectGetMaxY(desc.frame), 100, 20)];
                evaluateStatusLabel.font = [UIFont systemFontOfSize:12];
                evaluateStatusLabel.textAlignment = NSTextAlignmentRight;
                CGFloat eachHeight = 0;
                if (self.againEvaluate) {
                    [self.centerView addSubview:evaluateStatusLabel];
                    if ([dataParser.can_eva_ext integerValue]==0) {
                        
                        //已追加评价
                        evaluateStatusLabel.text = @"已追加评价";
                        evaluateStatusLabel.textColor = COLOR_666666;
                    } else {
                        //追加评价
                        evaluateStatusLabel.tag = i+120;
                        evaluateStatusLabel.text = @"追加评价";
                        evaluateStatusLabel.textColor = [UIColor redColor];
                        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(againEvaluteAction:)];
                        
                        self.userInteractionEnabled = YES;
                        evaluateStatusLabel.userInteractionEnabled = YES;
                        [evaluateStatusLabel addGestureRecognizer:tap];
                        
                    }
                    eachHeight = (cHeight>=CGRectGetMaxY(evaluateStatusLabel.frame))?cHeight:CGRectGetMaxY(evaluateStatusLabel.frame)+10;
                    self.viewHeight += (cHeight>=CGRectGetMaxY(evaluateStatusLabel.frame))?cHeight:CGRectGetMaxY(evaluateStatusLabel.frame)+10;
                } else {
                    eachHeight = cHeight;
                    self.viewHeight = cHeight*_parserArray.count;
                }
                
                self.centerView.frame = CGRectMake(0, i*eachHeight, mainWidth, eachHeight);
                [self addSubview: self.centerView];
                
            }
            
        }
        
    }
    
}
-(void)tapToLookGoodsDetailInfo:(UITapGestureRecognizer *) tap{
    UIView *view = tap.view;
    BuyerOrderListDetailParser *dataParser = self.parserArray[view.tag-2000];
    if ([self.orderDelegate respondsToSelector:@selector(toLookGoodsDetailInfoWithGoodsId:)]) {
        [self.orderDelegate toLookGoodsDetailInfoWithGoodsId:dataParser.product_id];
    }
}

- (void)toRefundAction:(UIButton *) sender {
    UIView *view = sender.superview;
    BuyerOrderListDetailParser *dataParser = self.parserArray[view.tag-2000];
    if ([self.orderDelegate respondsToSelector:@selector(refundMoneyOrGoodsWithId:product_ext_id:)]) {
        [self.orderDelegate refundMoneyOrGoodsWithId:dataParser.order_id product_ext_id:dataParser.product_ext_id];
    }
    
}

- (void)toLookRefundDetail:(UIButton *) sender {
    UIView *view = sender.superview;
    BuyerOrderListDetailParser *dataParser = self.parserArray[view.tag-2000];
    if ([self.orderDelegate respondsToSelector:@selector(refundDetailWithRefoundId:)]) {
        [self.orderDelegate refundDetailWithRefoundId:dataParser.refund_id];
    }
    
}

- (void) againEvaluteAction:(UITapGestureRecognizer *) tap {
    UIView *view = tap.view;
    OrderDetailDataParser *dataParser = self.parserArray[view.tag-120];
    if ([self.orderDelegate respondsToSelector:@selector(toAgainEvaluateWithEvaId:goodsNameAndLogo:)]) {
        [self.orderDelegate toAgainEvaluateWithEvaId:dataParser.eval_id goodsNameAndLogo:@[[NSString stringWithFormat:@"%@,%@",dataParser.product_name,dataParser.image]]];
    }
    
}
@end
