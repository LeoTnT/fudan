//
//  EvaluateView.h
//  EvaluateDemo
//
//  Created by 潘振泽 on 2017/2/18.
//  Copyright © 2017年 Panzz. All rights reserved.
//

#import <UIKit/UIKit.h>

#define     UnSelectedImg   [UIImage imageNamed:@"user_collect_unstar"]  //未选中时图片
#define     SelectedImg     [UIImage imageNamed:@"user_collect_star"]    //选中时图片

#define     ScreenWidth     [UIScreen mainScreen].bounds.size.width     //屏幕宽度
#define     ScreenHeight    [UIScreen mainScreen].bounds.size.height    //屏幕高度

@protocol EvaDelegate;

@interface EvaluateView : UIView

- (instancetype)initWithFrame:(CGRect)frame count:(int)count;

@property (nonatomic,weak) id<EvaDelegate> delegate;

@property (nonatomic,assign) CGFloat imgWidth;
@property (nonatomic,assign) NSUInteger count;//选中几个星星
@property (nonatomic,assign) NSUInteger evaTag;//标识

@end

//创建协议，创建一个协议方法
@protocol EvaDelegate <NSObject>

- (void)selectedStras:(NSUInteger )count andEvaTag:(NSUInteger ) tag;

@end
