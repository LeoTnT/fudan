//
//  PayMethodView.h
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayModel.h"
//@protocol PayMethodDelegate<NSObject>;
//@optional
//-(void)selectedPayMethod:(NSString *) method ;
//@optional
//-(void)unSelectedPayMethod:(NSString *) method ;
//@end
@interface PayMethodView : UIView
/**
 自定义的：
 "pay_method":"WeiXin"
 
 "name":"微信扫码支付",
 
 "memo":"345345",
 
 "pay_type":"pc"
 
 },*/
@property(nonatomic,strong) NSArray *methodArray;//第三方支付方式
@property(nonatomic,assign) CGFloat viewHeight;
@property (nonatomic, strong) PayMethodParser *payParser;
//@property(nonatomic,weak) id<PayMethodDelegate> methodDelegate;
@end
