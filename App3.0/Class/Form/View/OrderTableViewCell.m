//
//  OrderTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderTableViewCell.h"
#import "UIImage+XSWebImage.h"
#import "XSCustomButton.h"

@interface OrderTableViewCell()
@property (nonatomic, assign) CGFloat titleHeight;
@property (nonatomic, assign) CGFloat titleWidth;
@property (nonatomic, assign) CGFloat cHeight;
@property (nonatomic, assign) CGFloat imageSize;
@property (nonatomic, assign) CGFloat buttonWidth;
@property (nonatomic, assign) CGFloat buttonHwight;
@property (nonatomic, strong) UIColor *wordColor;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) UIView *line1;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *officialBtn;
@property (nonatomic, strong) UILabel *waitPay;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *total;
@property (nonatomic, strong) UILabel *fillPriceLabel;  // 补差价
@property (nonatomic, strong) XSCustomButton *firstBtn;
@property (nonatomic, strong) XSCustomButton *secondBtn;
@property (nonatomic, strong) XSCustomButton *thirdBtn;
@property (nonatomic, strong) XSCustomButton *fourBtn;

@end
@implementation OrderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.titleHeight = 40;
        self.titleWidth = 100;
        self.cHeight = 90;
        self.imageSize = 60;
        self.buttonWidth = (mainWidth-NORMOL_SPACE*5)/4;
        self.buttonHwight = self.buttonWidth/2.74;
        self.wordColor = [UIColor hexFloatColor:@"444444"];
        self.cellHeight = 0;
        [self setSubviews];
    }
    return self;
}

-(void)setSubviews{
    
    self.bgView= [[UIView alloc] initWithFrame:CGRectMake(0, 0,mainWidth, self.titleHeight)];
    self.bgView.backgroundColor = [UIColor whiteColor];
    
    self.officialBtn = [[UIButton alloc] initWithFrame:CGRectMake(11.5, 0, 15*NORMOL_SPACE, self.titleHeight)];
    self.officialBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.officialBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.officialBtn  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.officialBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.officialBtn setImage:[UIImage imageNamed:@"user_cart_store"] forState:UIControlStateNormal];
    self.officialBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(self.officialBtn.imageView.frame)+NORMOL_SPACE*2, 0, 0);
    self.officialBtn.titleEdgeInsets = UIEdgeInsetsMake(0, NORMOL_SPACE, 0, 0);
    [self.officialBtn addTarget:self action:@selector(supplyClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.officialBtn];
    
    //待付款
    self.waitPay= [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE-self.titleWidth, 0, self.titleWidth, self.titleHeight)];
    self.waitPay.backgroundColor = [UIColor whiteColor];
    self.waitPay.textColor = mainColor;
    self.waitPay.font = [UIFont systemFontOfSize:14];
    self.waitPay.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.waitPay];
    [self.contentView addSubview:self.bgView];
    
    //底部
    self.bottomView= [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bgView.frame), mainWidth, self.cHeight)];
    self.total= [[UILabel alloc] initWithFrame:CGRectMake(11.5, 0, mainWidth-12.5-11.5, 40)];
    self.total.textAlignment = NSTextAlignmentRight;
    self.total.textColor = self.wordColor;
    self.total.font = [UIFont systemFontOfSize:14];
    [self.bottomView addSubview:self.total];
    self.total.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.bottomView];
    
    self.line = [[UIView alloc] init];
    [self.bottomView addSubview:self.line];
    self.line.backgroundColor = LINE_COLOR_NORMAL;
    
    self.line1 = [[UIView alloc] init];
    [self.bottomView addSubview:self.line1];
    self.line1.backgroundColor = LINE_COLOR_NORMAL;
    
    self.fillPriceLabel = [UILabel new];
    self.fillPriceLabel.textAlignment = NSTextAlignmentRight;
    self.fillPriceLabel.textColor = self.wordColor;
    self.fillPriceLabel.font = [UIFont systemFontOfSize:14];
    [self.bottomView addSubview:self.fillPriceLabel];
    
    self.selectedButton = [[UIButton alloc] init];
    [self.selectedButton setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [self.selectedButton setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(selectedAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)setDataParser:(OrderDataParser *)dataParser{
    _dataParser = dataParser;
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[OrderGoodsView class]]) {
            [view removeFromSuperview];
        }
        
    }
    //顶部
    self.waitPay.text = _dataParser.statusName;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 6)];
    topView.backgroundColor = BG_COLOR;
    [self.bgView addSubview:topView];
    self.officialBtn.frame = CGRectMake(11.5, CGRectGetMaxY(topView.frame), mainWidth-11.5-10-self.titleWidth-10, self.titleHeight);
    [self.officialBtn setTitle:_dataParser.supply_name forState:UIControlStateNormal];
    [self.officialBtn setTitleColor:self.wordColor forState:UIControlStateNormal];
    //待付款
    self.waitPay.frame= CGRectMake(mainWidth-NORMOL_SPACE-self.titleWidth, CGRectGetMaxY(topView.frame), self.titleWidth, self.titleHeight);
    self.bgView.frame = CGRectMake(0, 0,mainWidth, self.titleHeight+6);
    if ([_dataParser.statusName isEqualToString:Localized(@"交易完成")]) {
        
    }
    
    //商品展示view
    OrderGoodsView *orderView = [[OrderGoodsView alloc] init];
    orderView.orderGoodsType = OrderGoodsTypeForOrder;
    orderView.againEvaluate = NO;
    if ([_dataParser.status integerValue]==3&&[_dataParser.is_eval integerValue]==1) {
        orderView.againEvaluate = YES;
    }
    orderView.parserArray = _dataParser.ext;
    orderView.frame = CGRectMake(0, CGRectGetMaxY(self.bgView.frame), mainWidth, orderView.viewHeight);
    [self.contentView addSubview:orderView];
    //添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTOLookOrderDetail:)];
    orderView.userInteractionEnabled = YES;
    [orderView addGestureRecognizer:tap];
    self.total.text = [NSString stringWithFormat:@"共计%@件商品，实付¥%.2f(运费：¥%.2f)",_dataParser.number_total, [_dataParser.sell_price_total floatValue],[_dataParser.yunfei floatValue]];
    
    self.line.frame = CGRectMake(0, CGRectGetMaxY(self.total.frame)+0.5, mainWidth, 0.5);
    
    if ([dataParser.status integerValue] == 1 && dataParser.fill_price_difference_status) {
        // 当订单状态为待发货，fill_price_difference_status为1 时，显示补款按钮，补款原因，补款金额
        self.bottomView.frame = CGRectMake(0,CGRectGetMaxY(orderView.frame), mainWidth,self.cHeight+44);
        
        self.fillPriceLabel.frame = CGRectMake(12, CGRectGetMaxY(self.line.frame), mainWidth-24, 43);
        
        self.fillPriceLabel.text = [NSString stringWithFormat:@"需要补款：%@（%@）",dataParser.fill_price_difference_price,dataParser.fill_price_difference_reason];
        
        self.line1.frame = CGRectMake(0, CGRectGetMaxY(self.fillPriceLabel.frame), mainWidth, 0.5);
    } else {
        self.line1.frame = CGRectZero;
        self.fillPriceLabel.frame = CGRectZero;
        self.fillPriceLabel.text = @"";
        self.bottomView.frame = CGRectMake(0,CGRectGetMaxY(orderView.frame), mainWidth,self.cHeight);
    }
    
    if ([_dataParser.status integerValue]==5) {
        self.bottomView.frame = CGRectMake(0,CGRectGetMaxY(orderView.frame)+NORMOL_SPACE, mainWidth,self.cHeight);
    }
    
    /**
     待付款： 取消交易、联系卖家、支付
     待发货：申请退款，联系卖家、提醒发货
     待收货：申请退款，确认收货、查看物流、联系卖家
     待评价：申请退货，联系卖家，去评价
     已完成：申请退货，联系卖家，追加评价，再次购买
     退款售后：联系卖家
     */
    //最左边
    for (UIView *view in self.bottomView.subviews) {
        if ([view isKindOfClass:[XSCustomButton class]]) {
            [view removeFromSuperview];
        }
        if ([view isEqual:self.selectedButton]) {
            [view removeFromSuperview];
        }
        
    }
    CGFloat beginX = mainWidth-4*self.buttonWidth-4*NORMOL_SPACE;
    CGFloat buttonTop = CGRectGetHeight(self.bottomView.frame) - (44 - self.buttonHwight)/2 - self.buttonHwight;
    UIColor *borderColor = [UIColor hexFloatColor:@"BABABA"];
    if ([_dataParser.status integerValue]==0) {
        //待付款
        if (self.statusType==OrderStatusTypeWaitPay) {
            self.selectedButton.selected = NO;
            [self.contentView addSubview:self.selectedButton];
            self.selectedButton.frame = CGRectMake(11.5, CGRectGetMaxY(topView.frame), 40, self.titleHeight);
            self.officialBtn.frame = CGRectMake(CGRectGetMaxX(self.selectedButton.frame), CGRectGetMaxY(topView.frame), 15*NORMOL_SPACE, self.titleHeight);
            
        }
        self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth+NORMOL_SPACE, buttonTop, self.buttonWidth, self.buttonHwight) title:@"取消交易" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
        [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
        
        [self.bottomView addSubview:self.secondBtn];
        
        self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
        [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
        
        [self.bottomView addSubview:self.thirdBtn];
        
        self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"支付" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
        
        [self.bottomView addSubview:self.fourBtn];
        
        [self.secondBtn addTarget:self action:@selector(cancelTransactionInfo:) forControlEvents:UIControlEventTouchUpInside];
        [self.thirdBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
        [self.fourBtn addTarget:self action:@selector(payMoneyInfo:) forControlEvents:UIControlEventTouchUpInside];
    }else if ([_dataParser.status integerValue]==1) {
        BOOL isNeedRemind = YES;
        for (OrderDetailDataParser *extParser in _dataParser.ext) {
            if ([extParser.logistics_type integerValue]==2||[extParser.logistics_type integerValue]==3) {
                //上门自提，免发货  无需发货
                isNeedRemind = NO;
                break;
            }
        }
        if ([self.is_refund integerValue]==1) {

            if (isNeedRemind) {
                if (dataParser.fill_price_difference_status) {
                    self.firstBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX, buttonTop, self.buttonWidth, self.buttonHwight) title:@"补差价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.firstBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    
                    [self.bottomView addSubview:self.firstBtn];
                    [self.firstBtn addTarget:self action:@selector(fillPrice:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth+NORMOL_SPACE, buttonTop, self.buttonWidth, self.buttonHwight) title:@"申请退款" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                [self.bottomView addSubview:self.secondBtn];
                [self.secondBtn addTarget:self action:@selector(refundMoneyInfo:) forControlEvents:UIControlEventTouchUpInside];
                
                self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                [self.bottomView addSubview:self.thirdBtn];
                [self.thirdBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                if ([_dataParser.is_remind integerValue]==0) {
                    //未提醒过
                    self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+3*NORMOL_SPACE, buttonTop, self.buttonWidth, self.buttonHwight) title:@"提醒发货" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                    [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                    [self.bottomView addSubview:self.fourBtn];
                    [self.fourBtn addTarget:self action:@selector(remindOrderSend:) forControlEvents:UIControlEventTouchUpInside];
                    
                } else {
                    
                    self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"已经提醒" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.fourBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    [self.bottomView addSubview:self.fourBtn];
                    
                }
                
                
            } else {
                if (dataParser.fill_price_difference_status) {
                    self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE+self.buttonWidth, buttonTop, self.buttonWidth, self.buttonHwight) title:@"补差价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    
                    [self.bottomView addSubview:self.secondBtn];
                    [self.secondBtn addTarget:self action:@selector(fillPrice:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"申请退款" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                [self.bottomView addSubview:self.thirdBtn];
                [self.thirdBtn addTarget:self action:@selector(refundMoneyInfo:) forControlEvents:UIControlEventTouchUpInside];
                
                self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                [self.bottomView addSubview:self.fourBtn];
                [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            
            
            
        }else{
            if (isNeedRemind) {
                
                if (dataParser.fill_price_difference_status) {
                    self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE+self.buttonWidth, buttonTop, self.buttonWidth, self.buttonHwight) title:@"补差价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    
                    [self.bottomView addSubview:self.secondBtn];
                    [self.secondBtn addTarget:self action:@selector(fillPrice:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                [self.bottomView addSubview:self.thirdBtn];
                if ([_dataParser.is_remind integerValue]==0) {
                    //未提醒过
                    self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*3+self.buttonWidth*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"提醒发货" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                    [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                    [self.fourBtn addTarget:self action:@selector(remindOrderSend:) forControlEvents:UIControlEventTouchUpInside];
                    [self.bottomView addSubview:self.fourBtn];
                } else {
                    
                    self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"已经提醒" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.fourBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    [self.bottomView addSubview:self.fourBtn];
                }
                [self.thirdBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            } else {
                if (dataParser.fill_price_difference_status) {
                    self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*2+self.buttonWidth*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"补差价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                    [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                    
                    [self.bottomView addSubview:self.thirdBtn];
                    [self.thirdBtn addTarget:self action:@selector(fillPrice:) forControlEvents:UIControlEventTouchUpInside];
                }
                self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*3+self.buttonWidth*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                
                [self.bottomView addSubview:self.fourBtn];
                [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            
        }
        
    } else if ([_dataParser.status integerValue]==2) {
        if ([self.is_reject integerValue]==1) {
            
            self.firstBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX, buttonTop, self.buttonWidth, self.buttonHwight) title:@"确认收货" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
            [self.firstBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
            
            [self.bottomView addSubview:self.firstBtn];
            self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth+NORMOL_SPACE, buttonTop, self.buttonWidth, self.buttonHwight) title:@"查看物流" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
            [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
            
            [self.bottomView addSubview:self.secondBtn];
            
            self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:[UIColor blackColor] highBackgroundColor:self.wordColor];
            [self.thirdBtn setBorderWith:1 borderColor:borderColor.CGColor cornerRadius:2];
            //
            [self.bottomView addSubview:self.thirdBtn];
            
            self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"退货" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
            [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
            
            [self.bottomView addSubview:self.fourBtn];
            
            [self.firstBtn addTarget:self action:@selector(verifyReceivedInfo:) forControlEvents:UIControlEventTouchUpInside];
            [self.secondBtn addTarget:self action:@selector(lookLogisticsInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.thirdBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.fourBtn addTarget:self action:@selector(returnSalesInfo:) forControlEvents:UIControlEventTouchUpInside];
            
        }else{
            self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE+self.buttonWidth, buttonTop, self.buttonWidth, self.buttonHwight) title:@"确认收货" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
            [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
            
            [self.bottomView addSubview:self.secondBtn];
            
            self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*2+NORMOL_SPACE*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"查看物流" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
            [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
            
            [self.bottomView addSubview:self.thirdBtn];
            
            self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
            [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
            
            [self.bottomView addSubview:self.fourBtn];
            
            [self.secondBtn addTarget:self action:@selector(verifyReceivedInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.thirdBtn  addTarget:self action:@selector(lookLogisticsInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    else if ([_dataParser.status integerValue]==3) {
        if ([_dataParser.is_eval integerValue]==0 ) {
            
            //待评价
            self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*2+self.buttonWidth*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"我要评价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
            [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
            
            [self.bottomView addSubview:self.thirdBtn];
            
            self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
            [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
            
            [self.bottomView addSubview:self.fourBtn];
            
            
            [self.thirdBtn addTarget:self action:@selector(evaluateOrderInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            
            //已完成
            if ([_dataParser.can_eva_ext integerValue]==1) {
                //追加评论
                
                
                //            _firstBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX, CGRectGetMaxY(_total.frame)+11.5, self.buttonWidth, self.buttonHwight) title:@"申请退货" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                //            [_firstBtn setBorderWith:1 borderColor:borderColor.CGColor cornerRadius:2];
                //
                //            [self.bottomView addSubview:_firstBtn];
                self.secondBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth+NORMOL_SPACE, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:self.wordColor highBackgroundColor:[UIColor whiteColor]];
                [self.secondBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                
                [self.bottomView addSubview:self.secondBtn];
                
                self.thirdBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*2+self.buttonWidth*2, buttonTop, self.buttonWidth, self.buttonHwight) title:@"追加评价" titleColor:self.wordColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:[UIColor blackColor] highBackgroundColor:self.wordColor];
                [self.thirdBtn setBorderWith:0.5 borderColor:borderColor.CGColor cornerRadius:2];
                //
                [self.bottomView addSubview:self.thirdBtn];
                
                self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+self.buttonWidth*3+NORMOL_SPACE*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"再次购买" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                
                [self.bottomView addSubview:self.fourBtn];
                
                //            [_firstBtn addTarget:self action:@selector(verifyReceivedInfo:) forControlEvents:UIControlEventTouchUpInside];
                [self.secondBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                
                [self.thirdBtn addTarget:self action:@selector(againAddEvaluteAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [self.fourBtn addTarget:self action:@selector(againBuy:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*3+self.buttonWidth*3, buttonTop, self.buttonWidth, self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
                
                [self.bottomView addSubview:self.fourBtn];
                [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            }
        }
       
    } else {
        
        self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(beginX+NORMOL_SPACE*3+self.buttonWidth*3, buttonTop, self.buttonWidth,self.buttonHwight) title:@"联系卖家" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.fourBtn setBorderWith:0 borderColor:nil cornerRadius:2];
        
        [self.bottomView addSubview:self.fourBtn];
        [self.fourBtn addTarget:self action:@selector(linkSellerInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    

    
    self.cellHeight = CGRectGetMaxY(self.bottomView.frame);
    
}

-(void)setOrderParser:(BuyerOrderParser *)orderParser{
    //订单详情界面
    _orderParser = orderParser;
    [self.waitPay removeFromSuperview];
    BuyerOrderSupplyDetailParser *supplyParser = _orderParser.supply;
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[OrderGoodsView class]]) {
            [view removeFromSuperview];
        }
        
    }
    [self.officialBtn setTitle:supplyParser.name forState:UIControlStateNormal];
    [self.officialBtn setTitleColor:self.wordColor forState:UIControlStateNormal];
    //商品展示view
    self.goodsView = [[OrderGoodsView alloc] init];
    self.goodsView.orderGoodsType = OrderGoodsTypeForDetail;
    self.goodsView.parserArray = _orderParser.order_list;
    self.goodsView.frame = CGRectMake(0, CGRectGetMaxY(self.bgView.frame), mainWidth, self.goodsView.viewHeight);
    self.goodsView.userInteractionEnabled = YES;
    self.goodsView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.goodsView];
    
    BuyerOrderDetailParser *detailParser = _orderParser.order;
    self.total.textColor = [UIColor hexFloatColor:@"444444"];
    self.total.font = [UIFont systemFontOfSize:14];
    self.total.text = [NSString stringWithFormat:@"共计%@件商品，实付¥%.2f(运费：¥%.2f)",detailParser.number_total, [detailParser.sell_price_total floatValue]+[detailParser.yunfei floatValue],[detailParser.yunfei floatValue]];
    //最左边
    for (UIView *view in self.bottomView.subviews) {
        if ([view isKindOfClass:[XSCustomButton class]]) {
            [view removeFromSuperview];
        }
        if ([view isEqual:self.selectedButton]) {
            [view removeFromSuperview];
        }
        
    }
    self.line.frame = CGRectMake(0, CGRectGetMaxY(self.total.frame), mainWidth, 0.5);
    
    self.fourBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(self.total.frame)+13.5, mainWidth-12*2,(mainWidth-12*2)/10.63) title:@"联系卖家" titleColor:COLOR_666666 fontSize:14 backgroundColor:[UIColor whiteColor] higTitleColor:COLOR_666666 highBackgroundColor:[UIColor whiteColor]];
    [self.fourBtn setBorderWith:0.5 borderColor:[UIColor hexFloatColor:@"BABABA"].CGColor cornerRadius:2];
    self.fourBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.fourBtn setImage:[UIImage imageNamed:@"order_link"] forState:UIControlStateNormal];
    [self.fourBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];
    
    [self.bottomView addSubview:self.fourBtn];
    [self.fourBtn addTarget:self action:@selector(toLinkSeller:) forControlEvents:UIControlEventTouchUpInside];
    //联系人按妞
    self.bottomView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame), mainWidth, 40+60);
    self.cellHeight = CGRectGetMaxY(self.bottomView.frame);
}


- (void) selectedAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        if ([self.orderDelegate respondsToSelector:@selector(selectedOrderWithOrderId:selected:)]) {
            [self.orderDelegate selectedOrderWithOrderId:self.dataParser.ID selected:YES];
        }
        
    } else {
        
        if ([self.orderDelegate respondsToSelector:@selector(selectedOrderWithOrderId:selected:)]) {
            [self.orderDelegate selectedOrderWithOrderId:self.dataParser.ID selected:NO];
        }
    }
    
}
- (void)supplyClick {
    if (self.supplyTapAction) {
        self.supplyTapAction(self.dataParser.supply_id);
    }
}

#pragma mark - Delegate

-(void)fillPrice:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(toFillPrice:toId:)]) {
        [self.orderDelegate toFillPrice:self.dataParser.fill_price_difference_product_id toId:self.dataParser.ID];
    }
    
}

-(void)cancelTransactionInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(cancelTransaction:)]) {
        [self.orderDelegate cancelTransaction:self.dataParser.ID];
    }
    
}

-(void) linkSellerInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(linkSeller:)]) {
        [self.orderDelegate linkSeller:[NSString stringWithFormat:@"%@,%@,%@",self.dataParser.supply_name,self.dataParser.supply_tel,self.dataParser.supply_id]];
    }
    
}

-(void) toLinkSeller:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(linkSeller:)]) {
        [self.orderDelegate linkSeller:nil];
    }
    
}

-(void) payMoneyInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(payMoney:)]) {
        [self.orderDelegate payMoney:self.dataParser];
    }
}

-(void)refundMoneyInfo:(UIButton * ) sender{
    if ([self.orderDelegate respondsToSelector:@selector(refundMoney:)]) {
        [self.orderDelegate refundMoney:self.dataParser.ID];
    }
}

-(void)verifyReceivedInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(verifyReceived:)]) {
        [self.orderDelegate verifyReceived:self.dataParser.ID];
    }
}

-(void)lookLogisticsInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(lookLogistics:)]) {
        [self.orderDelegate lookLogistics:self.dataParser];
    }
}

-(void)returnSalesInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(returnSales:money:)]) {
        [ self.orderDelegate returnSales:self.dataParser.ID money:self.dataParser.sell_price_total];
    }
    
}

-(void)evaluateOrderInfo:(UIButton *) sender{
    if ([self.orderDelegate respondsToSelector:@selector(evaluateOrder:)]) {
        [self.orderDelegate evaluateOrder:self.dataParser];
    }
    
}

- (void) againAddEvaluteAction:(UIButton *) sender {
    if ([self.orderDelegate respondsToSelector:@selector(againEvaluateWithEvaluateIds:goodsNameAndLogo:)]) {
        NSMutableArray *evaluateIdsArray = [NSMutableArray array];
        NSMutableArray *goodsNameAndLogoArray = [NSMutableArray array];
        for (int i=0; i<self.dataParser.ext.count; i++) {
            OrderDetailDataParser *detailParser = self.dataParser.ext[i];
            if ([detailParser.can_eva_ext integerValue]==1) {
                [evaluateIdsArray addObject:detailParser.eval_id];
                [goodsNameAndLogoArray addObject:[NSString stringWithFormat:@"%@,%@",detailParser.product_name,detailParser.image]];
            }
          
        }
        
        [self.orderDelegate againEvaluateWithEvaluateIds:[evaluateIdsArray componentsJoinedByString:@","] goodsNameAndLogo:goodsNameAndLogoArray];
    }
}
-(void)tapTOLookOrderDetail:(UIGestureRecognizer *) tap{
    if ([self.orderDelegate respondsToSelector:@selector(toLookOrderDetail:)]) {
        [self.orderDelegate toLookOrderDetail:self.dataParser.ID];
    }
}

- (void)remindOrderSend:(UIButton *) sender {
    if ([self.orderDelegate respondsToSelector:@selector(toRemindOrderSend:)]) {
        [self.orderDelegate toRemindOrderSend:self.dataParser.ID];
    }
}

- (void)againBuy:(UIButton *) sender {
    if ([self.orderDelegate respondsToSelector:@selector(againBuyWithOrderId:)]) {
        [self.orderDelegate againBuyWithOrderId:self.dataParser.ID];
    }
}

@end
