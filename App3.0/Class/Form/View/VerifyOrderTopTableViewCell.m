//
//  VerifyOrderTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/9/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VerifyOrderTopTableViewCell.h"

@implementation VerifyOrderTopTableViewCell
{
    UIImageView *lineImage;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        lineImage = [[UIImageView alloc] init];
        lineImage.image = [UIImage imageNamed:@"user_order_line"];
        [self.contentView addSubview:lineImage];

    }
    return self;
}
-(void)setAddressInformationArray:(NSArray *)addressInformationArray {
    _addressInformationArray = addressInformationArray;
    [self.addressView removeFromSuperview];
    self.addressView = [[OrderAddressCommonView alloc]init];
    self.addressView.addressType = OrderAddressCommonViewTypeVerify;
    [self.contentView addSubview:self.addressView];
    self.cellHeight = self.addressView.viewHeight;
    self.addressView.addressArray = _addressInformationArray;
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.addressView.viewHeight);
    }];
    [lineImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressView.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(7);
    }];
    self.cellHeight = self.addressView.viewHeight+7;
}

@end
