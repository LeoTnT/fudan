//
//  VerifyDeliveryView.h
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"

//@protocol  VerifyDeliveryDelegate<NSObject>
//
//- (void) changeDelivery;
//
//
//@end
@interface VerifyDeliveryView : UIView
/**标题*/
@property (nonatomic, strong) UILabel *nameLabel;
/**地址*/
@property (nonatomic, strong) UILabel *addressLabel;

/**换一站*/
@property (nonatomic, strong) UIButton *changeButton;


//@property (nonatomic, weak) id<VerifyDeliveryDelegate> delegate;
@end
