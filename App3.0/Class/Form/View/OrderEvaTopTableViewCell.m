//
//  OrderEvaTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderEvaTopTableViewCell.h"
#import "UIImage+XSWebImage.h"
#define IMG_SIZE 90
#define BTN_HEIGHT 20
@implementation OrderEvaTopTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
       
        [self setSubviews];
    }
    return self;
}

-(void)setSubviews{
    self.backgroundColor = [UIColor whiteColor];
    UIView *top = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
    top.backgroundColor = BG_COLOR;
    [self.contentView addSubview:top];
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, CGRectGetMaxY(top.frame),IMG_SIZE , IMG_SIZE)];
    [self.contentView addSubview:self.img];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    [self.img getImageWithUrlStr:nil andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    
//      [self.img getImageWithUrlStr:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,self.logo] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.img.frame)+NORMOL_SPACE, CGRectGetMinY(self.img.frame)+NORMOL_SPACE, mainWidth-4*NORMOL_SPACE-IMG_SIZE-BTN_HEIGHT, IMG_SIZE-NORMOL_SPACE*4)];
    self.titleLabel.textColor = [UIColor grayColor];
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    self.titleLabel.numberOfLines = 0;
    self.product_name = @"2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣2017新款上衣";
    self.titleLabel.text = self.product_name;
    [self.contentView addSubview:self.titleLabel];
    
    self.payLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame)+NORMOL_SPACE, CGRectGetWidth(self.titleLabel.frame), NORMOL_SPACE)];
    [self.contentView addSubview:self.payLabel];
    self.payLabel.font = [UIFont systemFontOfSize:13];
    self.totalMoney = [NSString stringWithFormat:@"实付款：¥%.2f",218.00];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:self.totalMoney];
    [attStr addAttribute:NSForegroundColorAttributeName
                   value:[UIColor redColor]
                   range:NSMakeRange(3, self.totalMoney.length-3)];
    self.payLabel.attributedText = attStr;
    
}
@end
