//
//  EvaluateView.m
//  EvaluateDemo
//
//  Created by 潘振泽 on 2017/2/18.
//  Copyright © 2017年 Panzz. All rights reserved.
//

#import "EvaluateView.h"

@interface EvaluateView ()
{
    CGFloat _topSpace;
    CGFloat _leftSpace;
}
@end

@implementation EvaluateView

- (instancetype)initWithFrame:(CGRect)frame count:(int)count
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
       
        [self createStarsWithCounts:count];
    }
    
    return self;
}

/**
 创建星星的imageview
 */
- (void)createStarsWithCounts:(int)counts
{
    //计算左右间距
    _leftSpace = 20;
    CGFloat width = (self.frame.size.width-2*_leftSpace-_leftSpace/2*(counts-1))/counts;
    self.imgWidth = width;
    _topSpace = (CGRectGetHeight(self.frame)-width)/2;
    for (int i = 0; i < counts; i ++)
    {
        UIImageView *imgView = [[UIImageView alloc] initWithImage:UnSelectedImg];
        imgView.tag = i;
        [imgView setFrame:CGRectMake(_leftSpace+i*(width+_leftSpace/2),_topSpace, width, width)];
        [self addSubview:imgView];
    }
}

-(void)setCount:(NSUInteger)count {
    _count = count;
    [self refreshUI];
}

/**
 手指滑动时判选中第几个星星
 */
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [[touches anyObject] locationInView:self];
    int x = point.x;
    int y = point.y;
    
    if (y >= _topSpace && y <= self.imgWidth + _topSpace)     //Y在星星同一行
    {
        if (x >_leftSpace  && x <= self.frame.size.width - _leftSpace)   //在星星范围内
        {
            self.count = (x - _leftSpace) / (self.imgWidth+_leftSpace/2) + 1;
            
            [self refreshUI];
        }
       
    }
}

/**
 点击结束时判断选择的星星位置
 */
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [[touches anyObject] locationInView:self];
    int x = point.x;
    int y = point.y;
    
    if (y >= _topSpace && y <= self.imgWidth + _topSpace)     //Y在星星同一行
    {
        if (x >_leftSpace && x < self.frame.size.width - _leftSpace)   //在星星范围内
        {
            self.count = (x - _leftSpace) / (self.imgWidth+_leftSpace/2) + 1;
            
            [self refreshUI];
        }
       
    }
}

/**
 根据选中星星数重新展示图片
 */
- (void)refreshUI
{
    //回调选中星星结果给VC
    if ([self.delegate respondsToSelector:@selector(selectedStras: andEvaTag:)])
    {
        [self.delegate selectedStras:self.count andEvaTag:self.evaTag];
    }
    
    for (UIImageView *imgView in self.subviews)
    {
        if (imgView.tag<self.count)
        {
            [imgView setImage:SelectedImg];
        }
        else
        {
            [imgView setImage:UnSelectedImg];
        }
    }
}

@end
