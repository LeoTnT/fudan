//
//  AddPhotosTableViewCell.m
//  App3.0
//
//  Created by mac on 2017/4/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddPhotosTableViewCell.h"
@implementation AddPhotosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}
#pragma mark-刷新界面
-(void)setPhotosArray:(NSArray *)photosArray{
    _photosArray=photosArray;
    self.deletBtnArray=[NSMutableArray array];
    self.imagesArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]||[view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    UIImageView *img;
    if (self.photosArray.count==0) {
        return;
    }
    for (int i=0; i<self.photosArray.count; i++) {
        int columns=4;
        //列数
        int col=i%columns;
        //行数
        int row=i/columns;
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10+col*((mainWidth-5*10)/4.0+10),14.5+row*((mainWidth-5*10)/4.0+10), (mainWidth-5*10)/4.0, (mainWidth-5*10)/4.0)];
        imgView.image=[self.photosArray objectAtIndex:i];
        img=imgView;
        imgView.userInteractionEnabled=YES;
        [self.contentView addSubview:imgView];
        //添加删除按钮×
        if (i<self.photosArray.count-1) {
            UIButton *deleteBtn=[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)-10, CGRectGetMinY(img.frame)-10, 20, 20)];
            deleteBtn.backgroundColor = [UIColor whiteColor];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
            deleteBtn.layer.cornerRadius = CGRectGetHeight(deleteBtn.frame)/2;
            deleteBtn.layer.masksToBounds = YES;
            [self.contentView addSubview:deleteBtn];
            [self.deletBtnArray addObject:deleteBtn];
        }
    }
    self.lastImage=img;
    self.lastImage.userInteractionEnabled=YES;
    if (photosArray.count==0) {
        self.height=NORMOL_SPACE*15;
    }else{
        self.height=CGRectGetMaxY(img.frame)+15.5;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
