//
//  VerifyDeliveryView.m
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VerifyDeliveryView.h"

@implementation VerifyDeliveryView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.nameLabel = [UILabel new];
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont qsh_systemFontOfSize:17];
        [self addSubview:self.nameLabel];
        
        self.addressLabel = [UILabel new];
        self.addressLabel.textColor = COLOR_666666;
        self.addressLabel.font = [UIFont qsh_systemFontOfSize:15];
        [self addSubview:self.addressLabel];
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self addSubview:line];
        
        self.changeButton = [UIButton new];
        [self.changeButton setTitle:@"换一站" forState:UIControlStateNormal];
        self.changeButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [self.changeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self addSubview:self.changeButton];
        
        [self.changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).with.mas_offset(-13);
            make.width.mas_equalTo(80);
            make.top.bottom.mas_equalTo(self);
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self);
            make.height.mas_equalTo(30);
            make.left.mas_equalTo(self).with.mas_offset(13);
            make.width.mas_lessThanOrEqualTo(mainWidth-13*2-80);
        }];
        
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLabel.mas_bottom);
            make.left.height.mas_equalTo(self.nameLabel);
            make.width.mas_lessThanOrEqualTo(mainWidth-13*2-80);
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.addressLabel.mas_right);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(50);
            make.centerY.mas_equalTo(self);
        }];
     
    }
    return self;
}


@end
