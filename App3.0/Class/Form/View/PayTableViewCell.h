//
//  PayTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PayModel.h"
@protocol PayDelegate<NSObject>
@optional
-(void)toPayWithMethodName:(NSString *) methodName ruleId:(NSString *) ruleId status:(NSString *) status balance:(NSString *) balance;
@optional
-(void)toCancelPayWithMethodName:(NSString *) methodName ruleId:(NSString *) ruleId status:(NSString *) status balance:(NSString *) balance;
@optional
-(void)toPayWithMethodInputInfo:(NSString *) inputInfo  methodName:(NSString *) methodName ruleId:(NSString *) ruleId ;

@end

@interface PayTableViewCell : UITableViewCell

@property(nonatomic,strong) UIImageView *payImageView;//图标
@property(nonatomic,strong) UILabel *titleLabel;//标题
@property(nonatomic,strong) UILabel *currentMoney;//当前余额
@property(nonatomic,strong) UITextField *payCount;//支付多少？可选
@property(nonatomic,strong) UILabel *minLabel;//最少使用
@property(nonatomic,strong) UILabel *maxLabel;//最多使用
@property(nonatomic,strong) UILabel *ratioLabel;//比率
@property(nonatomic,strong) PayConfigDetailParser *detailParser;
@property(nonatomic,copy) NSString *ruleId;//钱包id
@property(nonatomic,strong) UIButton *selectedMethodButton;//支付方式选择
@property(nonatomic,weak) id<PayDelegate> payDelegate;
@end
