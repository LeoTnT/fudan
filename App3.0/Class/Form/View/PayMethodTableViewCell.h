//
//  PayMethodTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayModel.h"
#import "PayMethodView.h"
@protocol PayMethodDelegate<NSObject>;
@optional
-(void)selectedPayMethod:(NSString *) method ;
@optional
-(void)unSelectedPayMethod:(NSString *) method ;
@end
@interface PayMethodTableViewCell : UITableViewCell

//@property(nonatomic,strong) NSArray *methodArray;//第三方支付方式

@property (nonatomic, strong) UIButton *methodButton;
@property (nonatomic, strong) PayMethodParser *payParser;
@property(nonatomic,weak) id<PayMethodDelegate> methodDelegate;
//@property(nonatomic,strong) PayMethodView *methodView;
@end
