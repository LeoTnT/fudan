//
// OrderFormModel.h
// App3.0
//
// Created by nilin on 2017/3/27.
// Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuyerOrderSupplyDetailParser : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *addr;
@end

@interface BuyerOrderScoreDetailParser : NSObject
//@property (nonatomic, retain) NSDictionary *score;
@property (nonatomic, copy) NSString *wallet_name;
@property (nonatomic, copy) NSString *value;
@end

@interface BuyerOrderDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *supply_username;
@property (nonatomic, copy) NSString *is_private;
@property (nonatomic, copy) NSString *desc;

/**下单时间*/
@property (nonatomic, copy) NSString *w_time;
/**付款时间*/
@property (nonatomic, copy) NSString *pay_time;
/**发货时间*/
@property (nonatomic, copy) NSString *transmit_time;
/**确认收货时间*/
@property (nonatomic, copy) NSString *confirm_time;
/**发货申请退货*/
@property (nonatomic, copy) NSString *app_time;
/**未发货退款时间*/
@property (nonatomic, copy) NSString *return_time;
/**关闭订单时间*/
@property (nonatomic, copy) NSString *close_time;
/**出库时间*/
@property (nonatomic, copy) NSString *ck_time;
/**完成时间*/
@property (nonatomic, copy) NSString *suc_time;

@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *pay_info;
@property (nonatomic, copy) NSString *pay_yunfei;
@property (nonatomic, copy) NSString *number_total;

@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *reward_coupon_total;
@property (nonatomic, copy) NSString *reward_score_total;
@property (nonatomic, copy) NSString *pay_no;
@property (nonatomic, strong) BuyerOrderScoreDetailParser *pay_wallet;
@end

@interface BuyerOrderListDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;//颜色
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *product_ext_id;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *transmit_time;
@property (nonatomic, copy) NSString *confirm_time;
@property (nonatomic, copy) NSString *app_time;
@property (nonatomic, copy) NSString *return_time;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *suc_time;
@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *is_cod;
@property (nonatomic, copy) NSString *pay_info;
@property (nonatomic, copy) NSString *pay_yunfei;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *supply_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *pv;
@property (nonatomic, copy) NSString *coupon;
@property (nonatomic, copy) NSString *product_weight;
@property (nonatomic, copy) NSString *product_no;
@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *reward_coupon_total;
@property (nonatomic, copy) NSString *reward_coupon;
@property (nonatomic, copy) NSString *reward_score_total;
@property (nonatomic, copy) NSString *reward_score;
@property (nonatomic, copy) NSString *can_refund;//是否可退款，退货
@property (nonatomic, copy) NSString *refund_status_cn;
@property (nonatomic, copy) NSString *refund_id;//退款退货的ID
@end

@interface BuyerOrderLogisticsDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *com_code;
@property (nonatomic, copy) NSString *com_no;
@property (nonatomic, copy) NSString *com_name;
@property (nonatomic, copy) NSString *com_id;
@property (nonatomic, copy) NSString *rec_name;
@property (nonatomic, copy) NSString *rec_tel;
@property (nonatomic, copy) NSString *rec_addr;
@property (nonatomic, copy) NSString *comments;//备注留言
@end

@interface BuyerOrderParser : NSObject
@property (nonatomic, strong) BuyerOrderSupplyDetailParser *supply;//商家信息
@property (nonatomic, strong) BuyerOrderDetailParser *order;//订单信息
@property (nonatomic, strong) NSArray *order_list;//订单信息
@property (nonatomic, copy) NSString *orderStatus;//已发货
@property (nonatomic, strong) BuyerOrderLogisticsDetailParser *logistics;//订单信息
@property (nonatomic, copy) NSString *is_refund;//是否开启退款, 1可退
@property (nonatomic, copy) NSString *is_reject;//是否开启退货, 1可退
@property (nonatomic, strong) NSDictionary *show_fileds;//优惠
@property (nonatomic, copy) NSString *coupon;//例如：购物券抵劵
@property (nonatomic, copy) NSString *tname;
@end

@interface OrderDetailDataParser : NSObject
@property (nonatomic, copy) NSString *is_refund;//是否开启退款, 1可退
@property (nonatomic, copy) NSString *is_reject;//是否开启退货, 1可退
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *order_id;//颜色
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_num;
@property (nonatomic, copy) NSString *product_ext_id;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *transmit_time;
@property (nonatomic, copy) NSString *confirm_time;
@property (nonatomic, copy) NSString *app_time;
@property (nonatomic, copy) NSString *return_time;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *suc_time;
@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *is_cod;
@property (nonatomic, copy) NSString *pay_info;
@property (nonatomic, copy) NSString *pay_yunfei;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *supply_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *pv;
@property (nonatomic, copy) NSString *coupon;
@property (nonatomic, copy) NSString *product_weight;
@property (nonatomic, copy) NSString *product_no;
@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *eval_id;//评价id
//@property (nonatomic, copy) NSString *is_eval;//是否可追加评价
@property (nonatomic, copy) NSString *can_eva_ext;//是否可追加评价

@end

@interface OrderDetailPaser : NSObject
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSArray *ext;
@end

@interface OrderDataParser : NSObject
@property (nonatomic, strong) NSArray *ext;
@property (nonatomic, copy) NSString *tname;//表名，用于支付
@property (nonatomic, copy) NSString *ID;//订单id
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *number_total;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *supply_username;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *yunfei;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *supply_name;
@property (nonatomic, copy) NSString *supply_tel;
@property (nonatomic, copy) NSString *statusName;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *is_refund;//是否开启退款, 1可退
@property (nonatomic, copy) NSString *is_reject;//是否开启退货, 1可退
@property (nonatomic, copy) NSString *is_eval;//=0；待评价；=1 已完成
@property (nonatomic, copy) NSString *can_eva_ext;//是否可追加评价
@property (nonatomic, copy) NSString *is_remind;//=0 未提醒过  =1 提醒过

@property (nonatomic, assign) unsigned int fill_price_difference_status; // 0 不要补款 1 需要补款
@property (nonatomic, copy) NSString *fill_price_difference_reason;      // 补款原因
@property (nonatomic, copy) NSString *fill_price_difference_price;      // 补款金额
@property (nonatomic, copy) NSString *fill_price_difference_product_id;      // 补款商品id
@end

@interface OrderParser : NSObject
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *current_page;
//@property (nonatomic, copy) NSArray *kefuinfo;//logo, nickname, id
//@property (nonatomic, copy) NSString *count;
@end

//确认订单展示信息
@interface PreviewVerifyOrderDetailProductParser : NSObject
@property (nonatomic, copy) NSString *product_ext_id;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_no;
@property (nonatomic, copy) NSString *product_weight;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *spec_value;
@property (nonatomic, copy) NSString *stock;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *supply_price;
@property (nonatomic, copy) NSString *pv;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *coupon;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *reward_coupon;
@property (nonatomic, copy) NSString *reward_score;
@property (nonatomic, copy) NSString *sup_name;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *is_cod;
@property (nonatomic, copy) NSString *logistics_type;
@property (nonatomic, copy) NSString *total_num;
@property (nonatomic, copy) NSString *total_supply_price;
@property (nonatomic, copy) NSString *total_market_price;
@property (nonatomic, copy) NSString *total_sell_price;
@property (nonatomic, copy) NSString *total_pv;
@property (nonatomic, copy) NSString *total_score;
@property (nonatomic, copy) NSString *total_coupon;
@property (nonatomic, copy) NSString *total_product_weight;
@property (nonatomic, copy) NSString *total_reward_coupon;
@property (nonatomic, copy) NSString *total_reward_score;
//@property (nonatomic, retain) NSArray *spec;
@end

@interface PreviewVerifyOrderProductsParser : NSObject
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, strong) NSNumber *total_num;
@property (nonatomic, copy) NSString *total_supply_price;
@property (nonatomic, copy) NSString *total_market_price;
@property (nonatomic, copy) NSString *total_sell_price;
@property (nonatomic, copy) NSString *total_pv;
@property (nonatomic, copy) NSString *total_score;
@property (nonatomic, copy) NSString *total_coupon;
@property (nonatomic, copy) NSString *total_product_weight;
@property (nonatomic, copy) NSString *total_reward_coupon;
@property (nonatomic, copy) NSString *total_reward_score;
@property (nonatomic, copy) NSString *total_delivery_price;
@property (nonatomic, copy) NSString *sup_name;//商家昵称
@property (nonatomic, copy) NSString *sup_uname;//商家编号
@property (nonatomic, copy) NSString *yunfei_total;//本商家的所有商品的运费
@property (nonatomic, strong) NSArray *delivery_list;//商品类型
//@property (nonatomic, retain) NSArray *wuliu;//物流
@end

@interface PreviewDeliveryListParser : NSObject
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *yunfei_total;
@property (nonatomic, strong) NSArray *product_list;
@property (nonatomic, strong) NSArray *logis_type;

@end

@interface PreviewVerifyWuliuParser : NSObject
@property (nonatomic, copy) NSString *logis_id;
@property (nonatomic, copy) NSString *logis_name;
@end

@interface PreviewVerifyOrderParser : NSObject
@property (nonatomic, copy) NSString *num_total;
@property (nonatomic, copy) NSString *yunfei_total;
@property (nonatomic, copy) NSString *supply_price_total;
@property (nonatomic, copy) NSString *market_price_total;
@property (nonatomic, copy) NSString *sell_price_total;
@property (nonatomic, copy) NSString *pv_total;
@property (nonatomic, copy) NSString *score_total;
@property (nonatomic, copy) NSString *coupon_total;
@property (nonatomic, copy) NSString *product_weight_total;
@property (nonatomic, copy) NSString *reward_coupon_total;
@property (nonatomic, copy) NSString *reward_score_total;
@property (nonatomic, copy) NSString *delivery_price_total;
@property (nonatomic, strong) NSArray *products;//商家
@end

/** 地址id,规格id,数量
 
 
 @"addrid":addrid,
 @"peids":peids,
 @"nums":nums,
 @"suid":suid,
 @"duid":duid,
*/

@interface ValidDeliverySubmitModel : NSObject
@property (nonatomic, copy) NSString *addrid;
@property (nonatomic, copy) NSString *peids;
@property (nonatomic, copy) NSString *nums;
@property (nonatomic, copy) NSString *suid;
@property (nonatomic, copy) NSString *duid;

@end

@interface ValidDeliveryObtainParser : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface ValidDeliveryObtainDetailParser : NSObject
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *province;


@end

@interface OrderCouponDataParser : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface OrderCouponDataDetailParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *deduction_coupon_id;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *is_limit;
@property (nonatomic, copy) NSString *limit_amount;
@property (nonatomic, copy) NSString *valid_time_start;
@property (nonatomic, copy) NSString *valid_time_end;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *limit_amount_str;
@end

@interface OrderFormModel : NSObject

@end
