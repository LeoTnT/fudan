//
//  OrderFormModel.m
//  App3.0
//
//  Created by nilin on 2017/3/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderFormModel.h"
#import "XSApi.h"
@implementation BuyerOrderScoreDetailParser

@end

@implementation BuyerOrderDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation BuyerOrderSupplyDetailParser

@end

@implementation BuyerOrderListDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end

@implementation BuyerOrderLogisticsDetailParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation BuyerOrderParser

+(NSDictionary *)mj_objectClassInArray {

    return @{
             @"order_list" : @"BuyerOrderListDetailParser"
             };

}

@end

@implementation OrderDataParser


+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"ext" : @"OrderDetailDataParser"
             };
}
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end

@implementation OrderParser

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"OrderDataParser"
             };
}
@end
@implementation OrderDetailDataParser

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end

@implementation OrderDetailPaser


+(NSDictionary *)mj_objectClassInArray {
    return @{
              @"ext" : @"OrderDetailDataParser"
             };
}
@end

@implementation PreviewVerifyOrderDetailProductParser

@end

@implementation PreviewVerifyOrderProductsParser


+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"delivery_list":@"PreviewDeliveryListParser"
             };
}

@end

@implementation PreviewDeliveryListParser

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"product_list":@"PreviewVerifyOrderDetailProductParser",
             @"logis_type":@"PreviewVerifyWuliuParser"
             };
}
@end

@implementation PreviewVerifyWuliuParser

@end

@implementation PreviewVerifyOrderParser


+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"products":@"PreviewVerifyOrderProductsParser"
             };
}
@end


@implementation ValidDeliverySubmitModel


@end

@implementation ValidDeliveryObtainParser
+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"ValidDeliveryObtainDetailParser"};
}

@end

@implementation ValidDeliveryObtainDetailParser


@end

@implementation OrderCouponDataParser
+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"OrderCouponDataDetailParser"};
}

@end

@implementation OrderCouponDataDetailParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}

@end


@implementation OrderFormModel

@end
