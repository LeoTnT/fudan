//
//  PayModel.h
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface PayConfigDetailParser : NSObject
@property (nonatomic, copy) NSString *wallet_en;
@property (nonatomic, copy) NSString *wallet_cn;
@property (nonatomic, copy) NSString *balance;
@property (nonatomic, copy) NSString *ratio;
@property (nonatomic, strong) NSNumber *limit_min;
@property (nonatomic, strong) NSNumber *limit_max;
@property (nonatomic, copy) NSString *is_edit;
@property (nonatomic, copy) NSString *status;//钱包限制(0必须|1单选|2多选)
@end

@interface PayAllConfigurationParser : NSObject
@property (nonatomic, strong) NSNumber *rule_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSNumber *is_deduct;
@property (nonatomic, copy) NSArray *conf_detail;
@end

@interface PayDataParser : NSObject
@property (nonatomic, copy) NSString *tname;//表名
@property (nonatomic, copy) NSString *ids;//订单id
@property (nonatomic, strong) NSNumber *number_all;//总价
@property (nonatomic, copy) NSString *online_only;//钱包和第三方是否可以同时选择
@property (nonatomic, copy) NSArray *conf_all;
@property (nonatomic, copy) NSArray *pay_method;//支付方式
@property (nonatomic, copy) NSDictionary *banklist;//银行卡类型
@end

@interface PayMethodParser : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *memo;
@property (nonatomic, copy) NSString *logo;
@end

@interface PayParser : NSObject
@property (nonatomic, strong) PayDataParser * data;
@end

@interface PayModel : NSObject

@end
