//
//  PayModel.m
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PayModel.h"
@implementation PayConfigDetailParser
@synthesize wallet_cn,wallet_en,balance,ratio,limit_max,limit_min,is_edit,status;
@end

@implementation PayAllConfigurationParser
@synthesize rule_id,title,name,is_deduct,conf_detail;

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"conf_detail":@"PayConfigDetailParser"
             };
}
@end

@implementation PayDataParser

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"conf_all":@"PayAllConfigurationParser",
             @"pay_method":@"PayMethodParser"
             };
}
@end

@implementation PayMethodParser


@end

@implementation PayParser
@synthesize data;
@end

@implementation PayModel


@end
