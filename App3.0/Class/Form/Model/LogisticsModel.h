//
//  LogisticsModel.h
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface  LogisticsDataParser: NSObject

@property(nonatomic,copy) NSString *time;
@property(nonatomic,copy) NSString *context;
@property(nonatomic,copy) NSString *js_time;

@end

@interface LogisticsParser : NSObject

@property(nonatomic,copy) NSString *company;
@property(nonatomic,copy) NSString *no;
@property(nonatomic,copy) NSString *tel;
@property(nonatomic,copy) NSString *status;


@end

@interface LogisticsModel : NSObject


@end
