//
//  LogisticsViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticsModel.h"
#import "OrderFormModel.h"

@interface LogisticsViewController : XSBaseViewController

/**订单model*/
@property (nonatomic, strong) OrderDataParser *dataParser;

@end
