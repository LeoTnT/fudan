//
//  DeliveryListViewController.h
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"

@interface DeliveryListViewController : XSBaseTableViewController

/** 可用配送站提交模型*/
//@property (nonatomic, strong) ValidDeliverySubmitModel *model;

/**配送站数组*/
@property (nonatomic, strong) NSMutableArray *deliveryListArray;

/**商家编号*/
@property (nonatomic, copy) NSString *supplyId;
@end
