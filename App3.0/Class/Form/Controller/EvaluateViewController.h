//
//  EvaluateViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"
@interface EvaluateViewController : XSBaseTableViewController

/**订单model*/
@property (nonatomic,strong) OrderDataParser *dataParser;

@end
