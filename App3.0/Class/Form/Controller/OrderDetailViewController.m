//
//  OrderDetailViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderFormModel.h"
#import "OrderTableViewCell.h"
#import "XSFormatterDate.h"
#import "PopUpView.h"
#import "ChatViewController.h"
#import "GoodsDetailViewController.h"
#import "OrderTableViewCell.h"
#import "GoodsDetailViewController.h"
#import "FormsVC.h"
#import "OrderDetailTopTableViewCell.h"
#import "OrderDetailTimeTableViewCell.h"
#import "OrderPayViewController.h"
#import "RefundWebViewController.h"

@interface OrderDetailViewController ()<PopViewDelegate,OrderDelegate,GoodsDetailDelegate>
@property (nonatomic, strong) NSMutableArray *goodsArray;
@property (nonatomic, strong) BuyerOrderParser  *parser;
@property (nonatomic, strong) BuyerOrderDetailParser  *detailParser;
@property (nonatomic, strong) BuyerOrderSupplyDetailParser  *supplyParser;
@property (nonatomic, strong) BuyerOrderLogisticsDetailParser  *logisticsParser;
@property (nonatomic, strong) PopUpView *sheetView;
@property (nonatomic, strong) UserInfoDataParser *userParser;
@end

@implementation OrderDetailViewController
//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    self.view.backgroundColor = [UIColor whiteColor];

    self.tableViewStyle = UITableViewStyleGrouped;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        for (UIViewController *vc in self.navigationController.viewControllers) {
#ifdef ALIYM_AVALABLE
            if ([vc isKindOfClass:NSClassFromString(@"YWChatViewController")]) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
#elif defined EMIM_AVALABLE
            if ([vc isKindOfClass:NSClassFromString(@"ChatViewController")]) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
#else
            if ([vc isKindOfClass:NSClassFromString(@"BaseChatController")]) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
#endif
            
        }
        FormsVC *controller = (FormsVC *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
        controller.formType = self.orderType;
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self getInfo];
}

- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getDetailOrderInfoWithId:self.orderId supply:@"0" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.parser = [BuyerOrderParser mj_objectWithKeyValues:dic[@"data"]];
            self.detailParser = self.parser.order;
            
            self.goodsArray = [NSMutableArray arrayWithArray:self.parser.order_list];
            self.supplyParser = self.parser.supply;
            self.logisticsParser = self.parser.logistics;
            [self setSubviews];
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}
- (void)setSubviews {
    
    self.tableView.backgroundColor = BG_COLOR;
    //底部
//    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, mainHeight-49.5, mainWidth, 0.5)];
//    line.textColor = LINE_COLOR_NORMAL;
//    [self.view addSubview:line];
//
//    CGFloat buttonWidth = (mainWidth-12*2-10*3)/4,buttonHeight = 27,topSpace = 11;
//    if (self.orderType==1) {
//        //待付款
//
//        XSCustomButton *payButton = [[XSCustomButton alloc] initWithTitle:@"支付" titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:12] cornerRadius:2 backGroundColor:mainColor hBackGroundColor:mainColor];
//        payButton.frame = CGRectMake(mainWidth-12-buttonWidth, CGRectGetMaxY(line.frame)+11, buttonWidth, buttonHeight);
//        [payButton addTarget:self action:@selector(payMoney) forControlEvents:UIControlEventTouchUpInside];
//        XSCustomButton *cancelButton = [[XSCustomButton alloc] initWithTitle:@"支付" titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:12] cornerRadius:2 backGroundColor:mainColor hBackGroundColor:mainColor];
//        cancelButton.frame = CGRectMake(mainWidth-12-buttonWidth*2-10, CGRectGetMaxY(line.frame)+11, buttonWidth, buttonHeight);
//        [cancelButton addTarget:self action:@selector(payMoney) forControlEvents:UIControlEventTouchUpInside];
//    }
    
}

- (void)cancelTransaction {
    UIAlertController *alertContoller = [UIAlertController alertControllerWithTitle:nil message:@"您确定要取消订单吗？" preferredStyle:UIAlertControllerStyleAlert] ;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [HTTPManager closeOrderWithOrderId:self.orderId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
               
                //刷新界面
                
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertContoller addAction:cancelAction];
    [alertContoller addAction:okAction];
    [self presentViewController:alertContoller animated:YES completion:nil];
}

- (void)payMoney {
    
    OrderPayViewController *payVC = [[OrderPayViewController alloc] init];
    payVC.orderId = self.orderId;
    if ([self.detailParser.logistics_type integerValue]==4) {
        payVC.orderLogisticstatus = OrderLogisticStatusFree;
    } else {
        payVC.orderLogisticstatus = OrderLogisticStatusRequiredeliver;
    }
    [self.navigationController pushViewController:payVC animated:YES];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        NSString *idString = @"dCell";
        OrderDetailTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[OrderDetailTopTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.logisticsParser) {
            cell.addressInformationArray = @[self.logisticsParser.rec_name,self.logisticsParser.rec_tel,self.logisticsParser.rec_addr];
//            [cell.addressView.linkButton addTarget:self action:@selector(tolinkSeller:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
        
    } else if (indexPath.section==1) {
        OrderTableViewCell *cell = [[OrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"gCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.parser) {
            cell.orderParser = self.parser;
        }
        cell.orderDelegate = self;
        cell.goodsView.orderDelegate = self;
        return cell;
        
    } else {
        NSString *idString = @"timeCells";
        OrderDetailTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[OrderDetailTimeTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        }
        if (self.detailParser) {
            cell.detailParser = self.detailParser;
        }
        return cell;
    }
    
}

- (NSInteger ) numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        OrderDetailTopTableViewCell *cell = (OrderDetailTopTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
        
    } else if (indexPath.section==1) {
        OrderTableViewCell *cell = (OrderTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
        
    } else {
        OrderDetailTimeTableViewCell *cell = (OrderDetailTimeTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)linkSeller:(NSString *)nameAndTelephone {
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypeOrder;
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView ] ;
}

#pragma mark -  PopViewDelegate
- (void)clickToCallMan {
    [UIView animateWithDuration:0.1 animations:^{
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        if ([self.supplyParser.tel isEqualToString:@""]||[self.supplyParser.tel isEqualToString:@" "]) {
            [XSTool showToastWithView:self.view Text:@"卖家电话错误"];
        } else {
            UIWebView*callWebview =[[UIWebView alloc] init];
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",self.supplyParser.tel]];// 貌似tel:// 或者 tel: 都行
            [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
            //记得添加到view上
            [self.view addSubview:callWebview];
        }
        
    }];
}

- (void)clickToMassageMan {
    [UIView animateWithDuration:0.2 animations:^{
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        [XSTool showProgressHUDWithView:self.view];
        if (self.userParser) {
#ifdef ALIYM_AVALABLE
            YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [self.userParser getName];
            chatVC.avatarUrl = self.userParser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
            [XSTool hideProgressHUDWithView:self.view];
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [self.userParser getName];
            chatVC.avatarUrl = self.userParser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:chatVC animated:YES];
#else
            XMChatController *chatVC = [XMChatController new];
            SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
            chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
            [self.navigationController pushViewController:chatVC animated:YES];
#endif
            
        } else {
            [HTTPManager getUserInfoWithUid:self.detailParser.supply_id success:^(NSDictionary * dic, resultObject *state) {
              
                if (state.status) {
                    self.userParser = [UserInfoParser mj_objectWithKeyValues:dic].data;
                    [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
                    YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
                    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = [self.userParser getName];
                    chatVC.avatarUrl = self.userParser.logo;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
                    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
                    [XSTool hideProgressHUDWithView:self.view];
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = [self.userParser getName];
                    chatVC.avatarUrl = self.userParser.logo;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    
                    [self.navigationController pushViewController:chatVC animated:YES];
#else
                    XMChatController *chatVC = [XMChatController new];
                    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
                    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
                    [self.navigationController pushViewController:chatVC animated:YES];
#endif
                } else {
                     [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                 [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
        
    }];
    
}

- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}

#pragma mark - orderDelegate
- (void)toLookGoodsDetailInfoWithGoodsId:(NSString *)goodsId {
    GoodsDetailViewController *goodsDetailController = [GoodsDetailViewController new];
    goodsDetailController.goodsID = goodsId;
    [self.navigationController pushViewController:goodsDetailController animated:YES];
}

-(void)refundDetailWithRefoundId:(NSString *)refund_id {
    RefundWebViewController *adWeb=[[RefundWebViewController alloc] init];
    NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
   
    if (VUE_ON) {
        adWeb.urlStr =
        [NSString stringWithFormat:@"%@/wap/#/order/refund/detail?id=%@&device=%@",ImageBaseUrl,refund_id,gui];
         adWeb.urlType = WKWebViewURLRefundDetail;
    } else {
        //        adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
    }
    
    adWeb.navigationItem.title = @"退款/退货详情";
    [self.navigationController pushViewController:adWeb animated:YES];
}

-(void)refundMoneyOrGoodsWithId:(NSString *)orderId product_ext_id:(NSString *)product_ext_id {
    RefundWebViewController *adWeb=[[RefundWebViewController alloc] init];
    NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
    if (VUE_ON) {
        adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/order/refund?oid=%@&sid=%@&device=%@",ImageBaseUrl,orderId,product_ext_id,gui];
         adWeb.urlType = WKWebViewURLRefundDetail;
    } else {
//        adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
    }
    
    adWeb.navigationItem.title = @"退款/退货";
    [self.navigationController pushViewController:adWeb animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
