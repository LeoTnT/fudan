//
//  RefundWebViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RefundWebViewController.h"
#import "OrderDetailViewController.h"
#import "UserModel.h"
#import "ChatViewController.h"

@interface RefundWebViewController ()<WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler>

@end

@implementation RefundWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backClick {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    } else {
        if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[OrderDetailViewController class]]) {
            OrderDetailViewController *controller = (OrderDetailViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            [controller getInfo];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
//    if ([message.name isEqualToString:@"back"]) {
//        
//        [self.navigationController popViewControllerAnimated:YES];
//    }else if ([message.name isEqualToString:@"toCall"]){
//        NSString *arr = message.body;
//        [self clickToCallMan:arr];
//    }else if ([message.name isEqualToString:@"toChat"]){
//        NSArray *arr = message.body;
//        [self clickToMassageMan:arr];
//    }
//    
//}
//- (void)clickToCallMan:(NSString *)phone {
//
//    if ([phone isEqualToString:@""]||[phone isEqualToString:@" "]) {
//        [XSTool showToastWithView:self.view Text:@"电话错误"];
//    } else {
//        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",phone]];
//        [[UIApplication sharedApplication] openURL:telURL];
//        //        UIWebView *callWebview =[[UIWebView alloc] init];
//        //        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",phone]];
//        //        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
//        //        [self.view addSubview:callWebview];
//    }
//
//}
//
//- (void)clickToMassageMan:(NSArray *) array {
//
//    [XSTool showProgressHUDWithView:self.view];
//    @weakify(self);
//    [HTTPManager getUserInfoWithUid:[array firstObject] success:^(NSDictionary *dic, resultObject *state) {
//        @strongify(self);
//        [XSTool hideProgressHUDWithView:self.view];
//        if (state.status) {
//            UserInfoDataParser *parser = [UserInfoParser mj_objectWithKeyValues:dic].data;
//            [XSTool hideProgressHUDWithView:self.view];
//            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:parser.uid type:EMConversationTypeChat createIfNotExist:YES];
//            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
//            NSString *title = parser.remark;
//            if ([parser.remark isEqualToString:@""]||[parser.remark isEqualToString:@" "]||parser.remark.length==0) {
//                title = parser.nickname;
//            }
//            chatVC.title = title;
//            chatVC.avatarUrl = parser.logo;
//            chatVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:chatVC animated:YES];
//        } else {
//            [XSTool showToastWithView:self.view Text:state.info];
//        }
//    } fail:^(NSError *error) {
//        @strongify(self);
//        [XSTool hideProgressHUDWithView:self.view];
//        [XSTool showToastWithView:self.view Text:NetFailure];
//    }];
//
//}

@end
