//
//  AgainEvaluateViewController.h
//  App3.0
//
//  Created by nilin on 2017/11/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgainEvaluateViewController : XSBaseTableViewController

/**商品信息+评价id*/
@property (nonatomic, copy) NSString *evaluateIds;
@property (nonatomic, strong) NSArray *goodsArray;
@end
