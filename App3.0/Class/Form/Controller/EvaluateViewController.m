//
//  EvaluateViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "EvaluateViewController.h"
#import "EvaluateView.h"
#import "OrderEvaTopTableViewCell.h"
#import "AddPhotosTableViewCell.h"
#import "FansCircleModel.h"
#import "EvaluateModel.h"
#import "ZYQAssetPickerController.h"
#import <CoreLocation/CoreLocation.h>
#import "FormsVC.h"

@interface EvaluateViewController ()<UITextViewDelegate,EvaDelegate,UITableViewDelegate,UITableViewDataSource,ZYQAssetPickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) UITextView *remark;
@property (nonatomic, strong) UILabel *hiddenLabel;
@property (nonatomic, strong) UILabel *tintLabel;
@property (nonatomic, strong) XSCustomButton *submitEvaBtn;//提交
@property (nonatomic, assign) CGFloat photoCellHeight;//图片的cell的高度
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) EvaluateView *wlView;
@property (nonatomic, strong) EvaluateView *msView;
@property (nonatomic, strong) EvaluateView *fhView;
@property (nonatomic, strong) EvaluateView *fwView;
@property (nonatomic, assign) NSUInteger wl_lev;//物流服务
@property (nonatomic, assign) NSUInteger ms_lev;//描述服务
@property (nonatomic, assign) NSUInteger fh_lev;//发货速度
@property (nonatomic, assign) NSUInteger fw_lev;//服务态度
@property (nonatomic, copy) NSString *isPrivate;//是否匿名
@property (nonatomic, assign) CGFloat remarkHeight;
@property (nonatomic, assign) CGFloat normalHeight;
@end

@implementation EvaluateViewController
#pragma mark - lazy loadding
- (NSMutableArray *)imageArray {
    if (_imageArray==nil) {
        _imageArray = [NSMutableArray array];
    }
    return _imageArray;
}
- (UITextView *)remark {
    if (!_remark) {
        _remark = [[UITextView alloc] init];
        _remark.delegate = self;
        _remark.returnKeyType = UIReturnKeyDone;
        _remark.tintColor = mainGrayColor;
        _remark.autocorrectionType = UITextAutocorrectionTypeNo;
        _remark.font = [UIFont systemFontOfSize:16];
        
    }
    return _remark;
}
- (UILabel *)hiddenLabel {
    if (!_hiddenLabel) {
        _hiddenLabel = [UILabel new];
        _hiddenLabel.text = @"猛戳这里说下你的感受";
        _hiddenLabel.font = [UIFont systemFontOfSize:16];
        _hiddenLabel.textColor = LINE_COLOR;
    }
    return _hiddenLabel;
    
}
- (UILabel *)tintLabel {
    if (!_tintLabel) {
        _tintLabel = [UILabel new];
        _tintLabel.backgroundColor = [UIColor whiteColor];
        _tintLabel.text = @"还可以输入100个字";
        _tintLabel.textColor = LINE_COLOR;
        _tintLabel.textAlignment = NSTextAlignmentRight;
        _tintLabel.font = [UIFont systemFontOfSize:14];
    }
    return _tintLabel;
}
- (XSCustomButton *)submitEvaBtn {
    
    if (!_submitEvaBtn) {
        _submitEvaBtn = [XSCustomButton new];
        [_submitEvaBtn setBorderWith:0 borderColor:0 cornerRadius:5];
        [_submitEvaBtn addTarget:self action:@selector(submitEvaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitEvaBtn;
}
#pragma mark - life circle
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.isPrivate = @"0";
    self.remarkHeight = 120;
    self.normalHeight = 50;
    self.navigationItem.title = @"订单评价";
    self.view.backgroundColor = BG_COLOR;
    self.wl_lev = 5;
    self.ms_lev = 5;
    self.fh_lev = 5;
    self.fw_lev = 5;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [self.view endEditing:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"确定取消评价吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        @strongify(self);
        UIAlertAction *ok = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        
    }];
    [self addSubViews];
    self.photoArray=[NSMutableArray arrayWithObject:[UIImage imageNamed:@"user_fans_addphoto_thin"]];
}
#pragma mark - private
- (void)addSubViews {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.bounces = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)selectedStras:(NSUInteger)count andEvaTag:(NSUInteger)tag {
    if (tag==999) {
        self.wl_lev = count;
    }
    if (tag==1000){
        self.ms_lev = count;
    }
    if (tag==1001){
        self.fh_lev = count;
    }
    if (tag==1002) {
        self.fw_lev = count;
    }
    
}

- (void)submitEvaAction:(UIButton *) sender {
    if ([self.remark.text isEqualToString:@""]||self.remark.text.length==0) {
        [XSTool showToastWithView:self.view Text:@"请完善评价信息"];
    } else {
        
        //商品id+评价文字+星级+隐私+图片
        //去掉最后一张加号图片
        
        NSMutableArray *tempPhotoArray = [NSMutableArray arrayWithArray:self.photoArray];
        [tempPhotoArray removeLastObject];
        if (tempPhotoArray.count==0) {
            [self vertifySubmit];
        } else {
           NSDictionary *param=@{@"type":@"evaluation",@"formname":@"file"};
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager upLoadPhotosWithDic:param andDataArray:tempPhotoArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                if([dic[@"status"] integerValue]==1){
                    NSArray *array = dic[@"data"];
                    [self.imageArray addObject:[array componentsJoinedByString:@","]];
                    //提交
                    [self vertifySubmit];
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
    }
    
}

- (void)vertifySubmit {
    NSMutableArray *paramArray = [NSMutableArray array];
    NSArray *goodsArray = self.dataParser.ext;
    NSDictionary *paramDictionary;
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *remarkString = [[NSString alloc]initWithString:[self.remark.text stringByTrimmingCharactersInSet:whiteSpace]];
    for (OrderDetailDataParser *data in goodsArray) {
        
        paramDictionary = @{
                            @"oeid":[NSString stringWithFormat:@"%@",data.ID],
                            @"ms_lev":[NSString stringWithFormat:@"%lu",(unsigned long)self.ms_lev],
                            @"wl_lev":[NSString stringWithFormat:@"%lu",(unsigned long)self.wl_lev],
                            @"fh_lev":[NSString stringWithFormat:@"%lu",(unsigned long)self.fh_lev],
                            @"fw_lev":[NSString stringWithFormat:@"%lu",(unsigned long)self.fw_lev],
                            @"comments":remarkString,
                            @"is_private":self.isPrivate,
                            @"img":self.imageArray
                            
                            };
        [paramArray addObject:paramDictionary];
    }
    
    [HTTPManager addEvaluteUrlWithOrderId:self.dataParser.ID eva:paramArray success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if(state.status){
            [XSTool showToastWithView:self.view Text:@"成功提交"];
            [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)popViewController {
    NSMutableArray *temporaryArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    FormsVC  *tempController;
    for (UIViewController *controller in temporaryArray) {
        if ([controller isKindOfClass:[FormsVC class]]) {
            tempController = (FormsVC *)controller;
            break;
        }
    }
    tempController.formType = FormTypeDone;
    [self.navigationController popToViewController:tempController animated:YES];
    
}

- (void)anoAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        //匿名
        self.isPrivate = @"1";
    }else{
        self.isPrivate = @"0";
    }
    
}

#pragma mark - UITextViewDelegate
- (void) textViewDidChange:(UITextView*)textView {
    if ([textView.text length] == 0) {
        self.hiddenLabel.hidden = NO;
    } else {
        self.hiddenLabel.hidden = YES;
        
    }
    if (textView.markedTextRange == nil) {
        if ([textView.text length] == 0) {
            self.tintLabel.text = @"还可以输入100个字";

        } else {
            self.tintLabel.text = @"";//这里给空
            
        }
        NSString *nsTextCotent = textView.text;
        NSUInteger existTextNum = [nsTextCotent length];
        NSUInteger remainTextNum = 100 - existTextNum;
        if ((int )remainTextNum<0) {
            remainTextNum = 0;
            self.remark.text = [self.remark.text substringToIndex:100];
        }
        self.tintLabel.text = [NSString stringWithFormat:@"还可以输入%lu个字",remainTextNum];
    }
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        //这里"\n"对应的是键盘的 return 回收键盘之用
        
        [textView resignFirstResponder];
        
        return YES;
        
    }
    
    if (range.location >= 100) {
        
        return  NO;
    } else {
        
        return YES;
    }
    
}

#pragma mark-选择照片
- (void)choosePhotos {
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //打开相册
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-拍照完毕
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    //保存图片到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [self.photoArray insertObject:image atIndex:self.photoArray.count-1];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //刷新表格
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
}

#pragma mark - ZYQAssetPickerController Delegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            //压缩0.8
            UIImage *newImage;
            NSData *imageData=UIImageJPEGRepresentation(result, 0.8);
            if (imageData.length>1024*1024*3) {//3M以及以上
//                [self dismissViewControllerAnimated:YES completion:^{
//                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
//                }];
                NSData *imageData=UIImageJPEGRepresentation(result, 0.2);
                newImage = [UIImage imageWithData:imageData];
                
            } else {
                imageData=UIImageJPEGRepresentation(result, 0.4);
                newImage = [UIImage imageWithData:imageData];
                
            }
            [self.photoArray insertObject:newImage atIndex:self.photoArray.count-1];
            NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择九张图片"];
}

#pragma mark-删除图片
- (void)deletePicture:(UIButton *)button {
    AddPhotosTableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    [self.photoArray removeObjectAtIndex:index];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark-拍照
- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat normalWidth = 80;
    if (indexPath.row==0) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"nCell"];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        line.backgroundColor = BG_COLOR;
        [cell.contentView addSubview:line];
        self.remark.frame = CGRectMake(0,CGRectGetMaxY(line.frame) , mainWidth, self.remarkHeight-10);
        self.hiddenLabel.frame = CGRectMake(NORMOL_SPACE, CGRectGetMinY(self.remark.frame)+NORMOL_SPACE, mainWidth-2*NORMOL_SPACE, NORMOL_SPACE);
        self.tintLabel.frame = CGRectMake(0, CGRectGetMaxY(self.remark.frame), mainWidth-NORMOL_SPACE, NORMOL_SPACE*2);
        [cell.contentView addSubview:self.remark];
        [cell.contentView addSubview:self.hiddenLabel];
        
        [cell.contentView addSubview:self.tintLabel];
        UILabel *line2 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tintLabel.frame)+NORMOL_SPACE, mainWidth, 1)];
        line2.backgroundColor = LINE_COLOR;
        [cell.contentView addSubview:line2];
        return cell;
    } else if (indexPath.row==1) {
        NSString *str=@"topCell";
        AddPhotosTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:str];
        if (!cell) {
            cell=[[AddPhotosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.photosArray=self.photoArray;
        
        //给cell的删除按钮绑定方法
        for (int i=0; i<cell.deletBtnArray.count; i++) {
            [[cell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
        }
        [cell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos)]];
        
        //绑定手势  收起键盘
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)]];
        self.photoCellHeight=cell.height;
        return cell;
    } else if (indexPath.row==6) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anoCell"];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *anoLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0,normalWidth , self.normalHeight)];
        anoLabel.text = @"匿名评论";
        [cell.contentView addSubview:anoLabel];
        UIButton *anoBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(anoLabel.frame)+NORMOL_SPACE*2, 0, normalWidth+4*NORMOL_SPACE, self.normalHeight)];
        [anoBtn setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [anoBtn setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [anoBtn setTitle:@"匿名提交" forState:UIControlStateNormal];
        [anoBtn setTitle:@"匿名提交" forState:UIControlStateSelected];
        [anoBtn setTitleColor:LINE_COLOR forState:UIControlStateNormal];
        [anoBtn setTitleColor:LINE_COLOR forState:UIControlStateSelected];
        anoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(anoBtn.imageView.frame), 0, 0);
        anoBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(anoBtn.imageView.frame)+NORMOL_SPACE, 0, 0);
        [anoBtn addTarget:self action:@selector(anoAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:anoBtn];
        anoBtn.selected = [self.isPrivate integerValue];
        return cell;
    } else if (indexPath.row==7) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"subCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat submitHeight = 40;
        self.submitEvaBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, mainWidth-2*NORMOL_SPACE, submitHeight) title:@"提交评价" titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.submitEvaBtn setBorderWith:0 borderColor:0 cornerRadius:5];
        [self.submitEvaBtn addTarget:self action:@selector(submitEvaAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = BG_COLOR;
        [cell.contentView addSubview:self.submitEvaBtn];
        return cell;
    } else if (indexPath.row==2) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"starCell"];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *logisLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0,normalWidth , self.normalHeight)];
        logisLabel.text = @"物流服务";
        [cell.contentView addSubview:logisLabel];
        CGRect frame = CGRectMake(CGRectGetMaxX(logisLabel.frame), logisLabel.frame.origin.y, mainWidth/2, CGRectGetHeight(logisLabel.frame));
        self.wlView = [[EvaluateView alloc] initWithFrame:frame count:5];
        self.wlView.evaTag = 999;
        self.wlView.count = self.wl_lev;
        self.wlView.delegate = self;
        [cell.contentView addSubview:self.wlView];
        return cell;
    } else if (indexPath.row==3) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"starCell"];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0,normalWidth , self.normalHeight)];
        descLabel.text = @"描述相符";
        [cell.contentView addSubview:descLabel];
        CGRect frame2 = CGRectMake(CGRectGetMaxX(descLabel.frame), CGRectGetMinY(descLabel.frame), mainWidth/2, CGRectGetHeight(descLabel.frame));
        self.msView = [[EvaluateView alloc] initWithFrame:frame2 count:5];
        self.msView.delegate = self;
        self.msView.count = self.ms_lev;
        self.msView.evaTag = 1000;
        [cell.contentView addSubview:self.msView];
        return cell;
        
    } else if (indexPath.row==4) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"starCell"];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *deliverLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0,normalWidth , self.normalHeight)];
        deliverLabel.text = @"发货速度";
        [cell.contentView addSubview:deliverLabel];
        CGRect frame3 = CGRectMake(CGRectGetMaxX(deliverLabel.frame), CGRectGetMinY(deliverLabel.frame), mainWidth/2, CGRectGetHeight(deliverLabel.frame));
        self.fhView = [[EvaluateView alloc] initWithFrame:frame3 count:5];
        self.fhView.delegate = self;
        self.fhView.count = self.fh_lev;
        self.fhView.evaTag = 1001;
        [cell.contentView addSubview:self.fhView];
        return cell;
    } else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"starCell"];
        cell.backgroundColor = BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *serviceLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0,normalWidth , self.normalHeight)];
        serviceLabel.text = @"服务态度";
        [cell.contentView addSubview:serviceLabel];
        CGRect frame4 = CGRectMake(CGRectGetMaxX(serviceLabel.frame), CGRectGetMinY(serviceLabel.frame), mainWidth/2, CGRectGetHeight(serviceLabel.frame));
        self.fwView = [[EvaluateView alloc] initWithFrame:frame4 count:5];
        self.fwView.delegate = self;
        self.fwView.count = self.fw_lev;
        self.fwView.evaTag = 1002;
        [cell.contentView addSubview:self.fwView];
        return cell;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        return self.remarkHeight+5*NORMOL_SPACE;
    } else if (indexPath.row==1) {
        return self.photoCellHeight;
    } else {
        return self.normalHeight;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        
        //弹出选择评价哪个商品
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
