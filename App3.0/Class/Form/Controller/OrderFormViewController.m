//
//  OrderFormViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderFormViewController.h"
#import "EvaluateViewController.h"
#import "LogisticsViewController.h"
#import "OrderPayViewController.h"
#import "ReturnGoodsViewController.h"
#import "OrderDetailViewController.h"
#import "ModifyOldPayViewController.h"
#import "ChatViewController.h"
#import "OrderTableViewCell.h"
#import "DefaultView.h"
#import "UserInstance.h"
#import "OrderFormModel.h"
#import "UserModel.h"
#import "RSAEncryptor.h"
#import "MJRefresh.h"
#import "XSCustomButton.h"
#import "FormsVC.h"
#import "S_StoreInformation.h"
#import "AgainEvaluateViewController.h"
#import "PopUpView.h"
#import "VerifyOrderViewController.h"
#import "GoodsDetailViewController.h"

@interface OrderFormViewController ()<UITableViewDelegate,UITableViewDataSource,OrderDelegate,GoodsDetailDelegate,DefaultViewDelegate,PopViewDelegate>

@property (nonatomic, strong) NSMutableArray *typeOrderArray;//订单类型数组
//@property (nonatomic, strong) UITableView *tableView;//展示订单信息的表格
@property (nonatomic, strong) DefaultView *defaultView;//默认界面
@property (nonatomic, strong) PopUpView *sheetView;//弹出的view（联系卖家）
@property (nonatomic, copy) NSString *supplyName;//商家名称
@property (nonatomic, copy) NSString *supplyTelephone;//商家电话
@property (nonatomic, copy) NSString *supplyId;//商家id
@property (nonatomic, copy) NSString *statusString;//状态
@property (nonatomic, copy) NSString *payPassWord;//密码
@property (nonatomic, assign) OrderType orderType;//订单类型
@property (nonatomic, assign) NSUInteger page;//刷新的页数
@property (nonatomic, strong) NSMutableArray *orderIdsArray;//选中合并支付的订单id数组
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UserInfoDataParser *userParser;

@end

@implementation OrderFormViewController

#pragma mark - lazy loadding
- (NSMutableArray *)orderIdsArray {
    if (!_orderIdsArray) {
        _orderIdsArray = [NSMutableArray array];
    }
    return _orderIdsArray;
    
}

-(UIView *)bottomView {
    
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, MAIN_VC_HEIGHT-44-50, mainWidth, 50)];
        _bottomView.backgroundColor = [UIColor whiteColor];
        UIButton *selectedButton = [[UIButton alloc] initWithFrame:CGRectMake(11.5, 0, 80, CGRectGetHeight(_bottomView.frame))];
        selectedButton.titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        [selectedButton setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
        [selectedButton setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
        [selectedButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [selectedButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [selectedButton setTitle:@"全选" forState:UIControlStateNormal];
        [selectedButton setTitle:@"全选" forState:UIControlStateSelected];
        [selectedButton addTarget:self action:@selector(allSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
        selectedButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
        [_bottomView addSubview:selectedButton];
        
        UIButton *payTogetherButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-(mainWidth-NORMOL_SPACE*5)/4-NORMOL_SPACE, 10, (mainWidth-NORMOL_SPACE*5)/4, CGRectGetHeight(_bottomView.frame)-10*2)];
        payTogetherButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [payTogetherButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [payTogetherButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [payTogetherButton setTitle:@"合并付款" forState:UIControlStateNormal];
        [payTogetherButton setTitle:@"合并付款" forState:UIControlStateSelected];
        payTogetherButton.backgroundColor =  mainColor;
        payTogetherButton.layer.cornerRadius = 2;
        payTogetherButton.layer.masksToBounds = YES;
        [payTogetherButton addTarget:self action:@selector(togetherAction:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:payTogetherButton];
    }
    
    return _bottomView;
}

- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT)];
        _defaultView.defaultViewType = DefaultViewTypeForOrderList;
        //        _defaultView.button.hidden = YES;
        _defaultView.defaultDelegete = self;
        _defaultView.backgroundColor = BG_COLOR;
        
    }
    return _defaultView;
}

- (NSMutableArray *)typeOrderArray {
    if (!_typeOrderArray) {
        _typeOrderArray = [NSMutableArray array];
    }
    return _typeOrderArray;
}


#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.view.backgroundColor = BG_COLOR;
    [self setOrderStatus];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self uploadOrderInformationIsCancelSuccess:NO];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self pullOrderInformation];
    }];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    [self.view addSubview:self.bottomView];
    self.bottomView.hidden = YES;
    [self uploadOrderInformationIsCancelSuccess:NO];
    
}

#pragma mark - private
- (void)allSelectedAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    [self.orderIdsArray removeAllObjects];
    if (sender.selected) {
        
        if (self.typeOrderArray.count) {
            for (int i=0; i<self.typeOrderArray.count; i++) {
                OrderDataParser *dataParser = self.typeOrderArray[i];
                if (![self.orderIdsArray containsObject:dataParser.ID]) {
                    [self.orderIdsArray addObject:dataParser.ID];
                }
            }
        }
    } else {
        [self.orderIdsArray removeAllObjects];
        self.bottomView.hidden = YES;
        
    }
    [self.tableView reloadData];
    
}

- (void) togetherAction:(UIButton *) sender {
    if (self.orderIdsArray.count) {
        OrderPayViewController *payVC = [[OrderPayViewController alloc] init];
        payVC.orderId = [self.orderIdsArray componentsJoinedByString:@","];
        [self.navigationController pushViewController:payVC animated:YES];
    } else {
        
        [XSTool showToastWithView:self.view Text:@"请选择订单!"];
    }
    
    
}

- (void)setOrderStatus {
    switch (self.orderType) {
        case OrderTypeAll: {
            self.statusString = @" ";
            self.defaultView.titleLabel.text = @"当前您还没有任何订单信息";
        }
            break;
        case OrderTypeWaitPay: {
            self.statusString = @"waitPay";
            self.defaultView.titleLabel.text = @"当前您还没有待付款订单信息";
            
        }
            break;
        case OrderTypeWaitSend: {
            self.statusString = @"waitSend";
            self.defaultView.titleLabel.text = @"当前您还没有待发货订单信息";
            
        }
            break;
        case OrderTypeWaitReceive: {
            self.statusString = @"waitConfirm";
            self.defaultView.titleLabel.text = @"当前您还没有待收货订单信息";
        }
            break;
        case OrderTypeWaitEvaluate: {
            self.statusString = @"waitRate";
            self.defaultView.titleLabel.text = @"当前您还没有待评价订单信息";
        }
            break;
        case OrderTypeDone: {
            self.statusString = @"success";
            self.defaultView.titleLabel.text = @"当前您还没有已完成订单信息";
        }
            break;
    }
    
}

- (void)getUserInformation {
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord = parser.pay_password;
        } else {
            self.payPassWord = @"";
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

static  NSString *limitPage = @"8";
- (void)uploadOrderInformationIsCancelSuccess:(BOOL)isCancel {
    
    self.page = 1;
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getOrderInfoWithOrderStatus:self.statusString andPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:limitPage success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (isCancel) {
            [XSTool showToastWithView:self.view Text:@"取消成功"];
        }
        if (state.status) {
            [self.typeOrderArray removeAllObjects];
            OrderParser *parser = [OrderParser mj_objectWithKeyValues:dic[@"data"][@"list"]];
            if (parser.data.count) {
                self.defaultView.hidden = YES;
                if (self.tableView.hidden) {
                    self.tableView.hidden = NO;
                }
                
                [ self.typeOrderArray addObjectsFromArray: parser.data];
                [self.tableView reloadData];
                self.page++;
                
            } else {
                self.defaultView.hidden = NO;
                self.tableView.hidden = YES;
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)pullOrderInformation {
    @weakify(self);
    [HTTPManager getOrderInfoWithOrderStatus:self.statusString andPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:limitPage success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            OrderParser *parser = [OrderParser mj_objectWithKeyValues:dic[@"data"][@"list"]];
            NSArray *tempArray = self.typeOrderArray;
            NSMutableArray *indexAry = [NSMutableArray arrayWithArray:tempArray];
            [indexAry addObjectsFromArray:parser.data];
            NSUInteger num = 0;
            for (int i=(int)tempArray.count ; i<indexAry.count; i++) {
                [self.typeOrderArray addObject:parser.data[num]];
                [self.tableView reloadData];
                num++;
            }
            self.page++;
        } else {
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (instancetype)initWithOrderType:(OrderType)type {
    self = [super init];
    if (self) {
        self.orderType = type;
    }
    return self;
}


#pragma mark - DefaultViewDelegate
- (void)buttonClicktoMall {
    self.tabBarController.selectedIndex = 2;
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - OrderDelegate
- (void)toFillPrice:(NSString *)goodId toId:(NSString *)toId {
    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc] init];
    vc.goodsID = goodId;
    vc.toId = toId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)cancelTransaction:(NSString *)orderId {
    UIAlertController *alertContoller = [UIAlertController alertControllerWithTitle:nil message:@"您确定要取消订单吗？" preferredStyle:UIAlertControllerStyleAlert] ;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [HTTPManager closeOrderWithOrderId:orderId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                [self uploadOrderInformationIsCancelSuccess:YES];
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertContoller addAction:cancelAction];
    [alertContoller addAction:okAction];
    [self presentViewController:alertContoller animated:YES completion:nil];
}

- (void)linkSeller:(NSString *)nameAndTelephone {
    NSArray *ary = [NSArray arrayWithArray:[nameAndTelephone componentsSeparatedByString:@","]];
    self.supplyName = ary[0];
    self.supplyTelephone = ary[1];
    self.supplyId = ary[2];
    self.sheetView = [[PopUpView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.sheetView.delegate = self;
    self.sheetView.popUpViewType = PopUpViewTypeOrder;
    [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.sheetView ] ;
}

#pragma mark -  PopViewDelegate
- (void)clickToCallMan{
    [UIView animateWithDuration:0.1 animations:^ {
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        if ([self.supplyTelephone isEqualToString:@""]||[self.supplyTelephone isEqualToString:@" "]) {
            [XSTool showToastWithView:self.view Text:@"卖家电话错误"];
        } else {
            UIWebView*callWebview =[[UIWebView alloc] init];
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",self.supplyTelephone]];
            [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
            [self.view addSubview:callWebview];
        }
    }];
}

- (void)clickToMassageMan{
    [UIView animateWithDuration:0.2 animations:^{
        [self clickToCancelLink];
    } completion:^(BOOL finished) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getUserInfoWithUid:self.supplyId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            if (state) {
                [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
                YWPerson *person = [[YWPerson alloc] initWithPersonId:parser.data.uid];
                YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                chatVC.title = [parser.data getName];
                chatVC.avatarUrl = parser.data.logo;
                chatVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
                EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:parser.data.uid type:EMConversationTypeChat createIfNotExist:YES];
                ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                chatVC.title = [parser.data getName];
                chatVC.avatarUrl = parser.data.logo;
                chatVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:chatVC animated:YES];
#else
                XMChatController *chatVC = [XMChatController new];
                SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:parser.data.uid title:[parser.data getName] avatarURLPath:parser.data.logo];
                chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
                [self.navigationController pushViewController:chatVC animated:YES];
#endif
                
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
}

- (void)clickToCancelLink {
    [self.sheetView removeFromSuperview];
}
- (void)evaluateOrder:(OrderDataParser *)orderParser {
    EvaluateViewController *evaVC = [[EvaluateViewController alloc] init];
    evaVC.dataParser = orderParser;
    [self.navigationController pushViewController:evaVC animated:YES];
}

- (void)refundMoney:(NSString *)orderId {
    UIAlertController *alertContoller = [UIAlertController alertControllerWithTitle:nil message:@"您确定要进行退款吗？" preferredStyle:UIAlertControllerStyleAlert] ;
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [HTTPManager refoundMoneyWithOrderId:orderId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                    [XSTool showToastWithView:self.view Text:@"退款成功"];
                for (int i=0; i<self.typeOrderArray.count; i++) {
                    OrderDataParser *dataParser = self.typeOrderArray[i];
                    if ([dataParser.ID isEqualToString:orderId]) {
                        
                        //设置非0 -》已提醒
                        [self.tableView reloadData];
                        break;
                    }
                }
                
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertContoller addAction:cancel];
    [alertContoller addAction:ok];
    [self presentViewController:alertContoller animated:YES completion:nil];
}

- (void)verifyReceived:(NSString *)orderId {
    if ([self.payPassWord isEqualToString:@""]) {
        
        //未设置支付密码,弹出警告框
        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"您还没有设置支付密码，是否要设置支付密码？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //跳转到设置支付密码界面
            ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
            [self.navigationController pushViewController:oldPayViewController animated:YES];
            
        }];
        [alertControl addAction:cancelAction];
        [alertControl addAction:okAction];
        [self presentViewController:alertControl animated:YES completion:nil];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"input_pay_pwd") preferredStyle:UIAlertControllerStyleAlert];
        //添加按钮
        __weak typeof(alert) weakAlert = alert; 
        [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //支付
            NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
            if ([weakAlert.textFields.lastObject text].length==0||[[weakAlert.textFields.lastObject text] isEqualToString:@""]) {
                [XSTool showToastWithView:self.view Text:@"请填写支付密码！"];
            } else {
                NSString *encryptPassWord = [RSAEncryptor encryptString:[weakAlert.textFields.lastObject text]];
                NSLog(@"encryptOldPassWord:::%@",encryptPassWord);
                [HTTPManager receiveGoodsWithId:orderId andPayPwd:encryptPassWord success:^(NSDictionary * _Nullable dic, resultObject *state) {
                    if (state.status) {
                        [XSTool showToastWithView:self.view Text:@"收货成功"];
                        FormsVC *formVC = (FormsVC *)self.parentViewController;
                        formVC.formType = FormTypeWaitEvaluate;
                    }else{
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                } failure:^(NSError * _Nonnull error) {
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
                
            }
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        
        // 添加文本框
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.secureTextEntry = YES;
            textField.textColor = [UIColor blackColor];
        }];
        // 弹出对话框
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

- (void)lookLogistics:(OrderDataParser *)orderParser {
    LogisticsViewController *logisticController = [[LogisticsViewController alloc] init];
    logisticController.dataParser = orderParser;
    logisticController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:logisticController animated:YES];
}

- (void)returnSales:(NSString *)orderId money:(NSString *)money {
    ReturnGoodsViewController *returnController = [[ReturnGoodsViewController alloc] init];
    returnController.orderId = orderId;
    [self.navigationController pushViewController:returnController animated:YES];
    
}

- (void)payMoney:(OrderDataParser *)orderParser {
    OrderPayViewController *payVC = [[OrderPayViewController alloc] init];
    payVC.orderId = orderParser.ID;
    OrderDetailDataParser *detailParser = [orderParser.ext firstObject];
    if ([detailParser.logistics_type integerValue]==4) {
        payVC.orderLogisticstatus = OrderLogisticStatusFree;
    } else {
        payVC.orderLogisticstatus = OrderLogisticStatusRequiredeliver;
    }
    [self.navigationController pushViewController:payVC animated:YES];
    
}

- (void)toLookOrderDetail:(NSString *)orderId {
    OrderDetailViewController *detail = [[OrderDetailViewController alloc] init];
    detail.orderId = orderId;
    detail.orderType = self.orderType;
    [self.navigationController pushViewController:detail animated:YES];
}

-(void)selectedOrderWithOrderId:(NSString *)orderId selected:(BOOL)isSelected{
    
    if (isSelected) {
        if (![self.orderIdsArray containsObject:orderId]) {
            [self.orderIdsArray addObject:orderId];
        }
    } else {
        if ([self.orderIdsArray containsObject:orderId]) {
            [self.orderIdsArray removeObject:orderId];
        }
        
    }
    for (int i=0; i<self.typeOrderArray.count; i++) {
        OrderDataParser *parser = self.typeOrderArray[i];
        if ([parser.ID isEqualToString:orderId]) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
    if (self.orderIdsArray.count) {
        self.bottomView.hidden = NO;
    } else {
        self.bottomView.hidden = YES;
    }
    
}

- (void)toRemindOrderSend:(NSString *)orderId {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager orderRemindOrderSendWithOrderId:orderId success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"已提醒商家发货，请耐心等待！"];
            if (self.typeOrderArray.count) {
                for (int i=0; i<self.typeOrderArray.count; i++) {
                    OrderDataParser *dataParser = self.typeOrderArray[i];
                    if ([dataParser.ID isEqualToString:orderId]) {
                        
                        //设置非0 -》已提醒
                        dataParser.is_remind = @"111";
                        NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:0];
                        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
                        break;
                    }
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

-(void)againEvaluateWithEvaluateIds:(NSString *)evaluateIds goodsNameAndLogo:(NSArray *)goodsInfoArray {
    AgainEvaluateViewController *controller = [[AgainEvaluateViewController alloc] init];
    controller.evaluateIds = evaluateIds;
    controller.goodsArray = goodsInfoArray;
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)toAgainEvaluateWithEvaId:(NSString *)evaId goodsNameAndLogo:(NSArray *)goodsInfoArray {
    AgainEvaluateViewController *controller = [[AgainEvaluateViewController alloc] init];
    controller.evaluateIds = evaId;
    controller.goodsArray = goodsInfoArray;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)againBuyWithOrderId:(NSString *)orderId {
    NSDictionary *param = @{@"id":orderId,@"return_params":@"0"};
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager orderBuyAgainWithParams:param success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSDictionary *list = dic[@"data"][@"list"];
            PreviewVerifyOrderParser *previewParser;
            if (list.allKeys>0) {
                previewParser = [PreviewVerifyOrderParser mj_objectWithKeyValues:list];
                //确认订单界面
                VerifyOrderViewController *controller = [[VerifyOrderViewController alloc] init];
               
                [controller.shopArray setArray:previewParser.products];//商家数组
                controller.totalPrice = [NSString stringWithFormat:@"%.2f",[previewParser.sell_price_total floatValue]+[previewParser.yunfei_total floatValue]];
                controller.logisticMoney = previewParser.yunfei_total;
                controller.addressId =[NSString stringWithFormat:@"%@",dic[@"data"][@"address_id"]];
                 controller.verifyOrderType = VerifyOrderMore;
                [self.navigationController pushViewController:controller animated:YES];
                
            }
           
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str= [NSString stringWithFormat:@"%@",@"orderCell"];
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell==nil) {
        cell = [[OrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    
    cell.orderDelegate = self;
    OrderDataParser *dataParser;
    if (self.typeOrderArray.count>0) {
        dataParser = self.typeOrderArray[indexPath.row];
    }
    cell.is_reject = dataParser.is_reject;
    cell.is_refund = dataParser.is_refund;
    if (self.orderType==OrderTypeWaitPay) {
        cell.statusType = OrderStatusTypeWaitPay;
    }
    cell.dataParser = dataParser;
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[OrderGoodsView class]]) {
            ((OrderGoodsView *)view).orderDelegate = self;
        }
    }
    
    cell.selectedButton.selected = NO;
    if ([self.orderIdsArray containsObject:dataParser.ID]) {
        cell.selectedButton.selected = YES;
    } else {
        cell.selectedButton.selected = NO;
    }
    
    [cell setSupplyTapAction:^(NSString *supId) {
        S_StoreInformation *infoVC=[[S_StoreInformation alloc] init];
        infoVC.storeInfor=supId;
        [self.navigationController pushViewController:infoVC animated:YES];
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderTableViewCell *cell = (OrderTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeOrderArray.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
