//
//  OrderDetailViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailViewController : XSBaseTableViewController

/**订单id*/
@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, assign) NSUInteger orderType;
- (void)getInfo;
@end
