//
//  AgainEvaluateViewController.m
//  App3.0
//
//  Created by nilin on 2017/11/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AgainEvaluateViewController.h"
#import "AgainGoodsTableViewCell.h"
#import "AddPhotosTableViewCell.h"
#import "ZYQAssetPickerController.h"
#import "FormsVC.h"

@interface AgainEvaluateViewController ()<UITableViewDelegate,UITableViewDataSource,ZYQAssetPickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

@property (nonatomic, strong) NSMutableDictionary *photoDictionary;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (nonatomic, assign) CGFloat photoCellHeight;//图片的cell的高度
@property (nonatomic, assign) NSUInteger currentSection;
@property (nonatomic, strong) NSMutableDictionary *remarkDictionary;//记录内容
@property (nonatomic,assign) CGFloat currentY;//当前纵坐标
@property (nonatomic, assign) BOOL keyBoardIsShow;//键盘是否显示
@end

@implementation AgainEvaluateViewController

#pragma mark - lazyLoadding

-(NSMutableDictionary *)remarkDictionary {
    if (!_remarkDictionary) {
        _remarkDictionary = [NSMutableDictionary dictionary];
    }
    return _remarkDictionary;
    
}
-(NSMutableDictionary *)photoDictionary {
    if (!_photoDictionary) {
        _photoDictionary = [NSMutableDictionary dictionary];
    }
    return _photoDictionary;
}
#pragma mark - UITextViewDelegate
- (void) textViewDidChange:(UITextView*)textView {
    
    UITableViewCell *cell= (UITableViewCell *)textView.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    UILabel *hiddenLabel;
//    UILabel *tintLabel;
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            if (view.tag == index.section+200 ) {
                hiddenLabel = (UILabel *) view;
            }
//            if (view.tag == index.section+300 ) {
//                tintLabel = (UILabel *) view;
//            }
        }
    }
    if ([textView.text length] == 0) {
        hiddenLabel.hidden = NO;
    } else {
        hiddenLabel.hidden = YES;
        
    }
    if (textView.markedTextRange == nil) {
        if ([textView.text length] == 0) {
//            tintLabel.text = @"0/140";
            
        } else {
//            tintLabel.text = @"";//这里给空
            
        }
        NSString *nsTextCotent = textView.text;
        NSUInteger existTextNum = [nsTextCotent length];
        NSUInteger remainTextNum = 140 - existTextNum;
        if ((int )remainTextNum<0) {
            remainTextNum = 0;
            textView.text = [textView.text substringToIndex:140];
        }
//        tintLabel.text = [NSString stringWithFormat:@"%lu/140",remainTextNum];
    }
     [self.remarkDictionary setObject:textView.text forKey:[NSString stringWithFormat:@"%lu",index.section]];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView {
    UITableViewCell *cell= (UITableViewCell *)textView.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    [self.remarkDictionary setObject:textView.text forKey:[NSString stringWithFormat:@"%lu",index.section]];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
//    [self.tableView layoutIfNeeded];
    UITableViewCell *cell =(UITableViewCell *)textView.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    //    NSLog(@"%@,%@",rectInSuperview ,rectInTableView);
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    NSLog(@"90909090909===%f",height);
    self.currentY = height;
    return YES;
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        //这里"\n"对应的是键盘的 return 回收键盘之用
        
        [textView resignFirstResponder];
        
        return YES;
        
    }
    
    if (range.location >= 140) {
        
        return  NO;
    } else {
        
        return YES;
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyBoardIsShow) {
        [self.view endEditing:YES];
    }
}
-(void)keyboardWillShow:(NSNotification *)notification
{
    //这样就拿到了键盘的位置大小信息frame，然后根据frame进行高度处理之类的信息
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>mainHeight) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+30)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
}
- (void)keyboardDidShow {
        self.keyBoardIsShow=YES;
   
}
-(void)keyboardWillHidden:(NSNotification *)notification
{
    self.keyBoardIsShow = NO;
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}
#pragma mark - life
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    
}
-(void)dealloc {
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"追加评价";
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"bug_submit_do") action:^{
        @strongify(self);
        [self.view endEditing:YES];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSMutableArray *eva_exts = [NSMutableArray array];
        NSMutableArray *totalPhotos = [NSMutableArray array];
        BOOL isComplement = YES;
        NSArray *ids = [self.evaluateIds componentsSeparatedByString:@","];
        for (int i=0; i<self.goodsArray.count; i++) {
            UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:i]];
            NSMutableDictionary *evaDictionary = [NSMutableDictionary dictionary];
            
            [evaDictionary setObject:ids[i] forKey:@"eval_id"];
             [evaDictionary setObject:@"" forKey:@"zhui_img"];
            NSString *remarkString = [self.remarkDictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
            if (isEmptyString(remarkString)) {
                 isComplement = NO;
            } else {
              [evaDictionary setObject:remarkString forKey:@"zhui_content"];
            }
            [eva_exts addObject:evaDictionary];
            
        }

        if (isComplement) {
            for (int i=0; i<self.goodsArray.count; i++) {
                NSMutableArray *temp = [NSMutableArray arrayWithArray:[self.photoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
                [temp removeLastObject];
                if (temp.count) {
                    [totalPhotos addObjectsFromArray:temp];
                }
                
            }
            
            if (totalPhotos.count) {
                
                //上传图片
                NSDictionary *param=@{@"type":@"evaluation",@"formname":@"file"};
                [XSTool showProgressHUDWithView:self.view];
                [HTTPManager upLoadPhotosWithDic:param andDataArray:totalPhotos WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
                    if ([dic[@"status"] integerValue]==1) {
                        
                        //区分追评商品评价图片
                        NSMutableArray *imgs = [NSMutableArray arrayWithArray:dic[@"data"]];
                        NSUInteger sum = 0;
                        for (int i=0; i<eva_exts.count; i++) {
                            NSMutableArray *temps = [NSMutableArray arrayWithArray:[self.photoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
                            [temps removeLastObject];
                            if (temps.count) {
                                NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithDictionary:eva_exts[i]];
                                [tempDictionary setObject:[[imgs subarrayWithRange:NSMakeRange(sum, temps.count)] componentsJoinedByString:@","] forKey:@"zhui_img"];
                                [eva_exts replaceObjectAtIndex:i withObject:tempDictionary];
                                
                                
                            }
                          sum+=temps.count;
                        }
                            NSError *error = nil;
                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:eva_exts
                                                                               options:kNilOptions
                                                                                 error:&error];
                            NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                                         encoding:NSUTF8StringEncoding];
                            [params  setObject:jsonString forKey:@"eva_exts"];
                            [self submitEvaluateWithParam:params];
                        
                    } else {
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                    
                } fail:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            } else {
                NSError *error = nil;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:eva_exts
                                                                   options:kNilOptions
                                                                     error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                             encoding:NSUTF8StringEncoding];
                [params  setObject:jsonString forKey:@"eva_exts"];
                
                [self submitEvaluateWithParam:params];
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:@"请完善评价内容！"];
        }
    }];
    self.navRightBtn.titleLabel.textColor = [UIColor hexFloatColor:@"777777"];
    self.navRightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.bounces = NO;
    
    
    for (int i=0; i<self.goodsArray.count; i++) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithObject:[UIImage imageNamed:@"eva_add"]];
        [self.photoDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%d",i]];
        
        [self.remarkDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
    }
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)popViewController {
    NSMutableArray *temporaryArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    FormsVC  *tempController;
    for (UIViewController *controller in temporaryArray) {
        if ([controller isKindOfClass:[FormsVC class]]) {
            tempController = (FormsVC *)controller;
            break;
        }
    }
    tempController.formType = FormTypeDone;
    [self.navigationController popToViewController:tempController animated:YES];
    
}
- (void)submitEvaluateWithParam:(NSDictionary *) param {
    [HTTPManager evaluateZhuiEvaluationsWithParam:param success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"成功追加评价！"];
            [self popViewController];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

#pragma mark-选择照片
- (void)choosePhotos:(UITapGestureRecognizer *) tap {
    [self.view endEditing:YES];
    AddPhotosTableViewCell *cell = (AddPhotosTableViewCell *) tap.view.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    self.currentSection = index.section;
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //打开相册
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-拍照完毕
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    //保存图片到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    NSMutableArray *tempArray = [self.photoDictionary objectForKey:[NSString stringWithFormat:@"%lu",self.currentSection]];
    [tempArray insertObject:image atIndex:tempArray.count-1];
    [self.photoDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%lu",self.currentSection]];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //刷新表格
    NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:self.currentSection];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
}

#pragma mark - ZYQAssetPickerController Delegate
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            //压缩0.8
            UIImage *newImage;
            NSData *imageData=UIImageJPEGRepresentation(result, 0.8);
            if (imageData.length>1024*1024*3) {//3M以及以上
                //                [self dismissViewControllerAnimated:YES completion:^{
                //                    [XSTool showToastWithView:self.view Text:@"图片大于3M!"];
                //                }];
                NSData *imageData=UIImageJPEGRepresentation(result, 0.2);
                newImage = [UIImage imageWithData:imageData];
                
            } else {
                imageData=UIImageJPEGRepresentation(result, 0.4);
                newImage = [UIImage imageWithData:imageData];
                
            }
            NSMutableArray *tempArray = [self.photoDictionary objectForKey:[NSString stringWithFormat:@"%lu",self.currentSection]];
            [tempArray insertObject:newImage atIndex:tempArray.count-1];
            [self.photoDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%lu",self.currentSection]];
            NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:self.currentSection];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}

- (void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker {
    [XSTool showToastWithView:self.view Text:@"一次最多选择九张图片"];
}

#pragma mark-删除图片
- (void)deletePicture:(UIButton *)button {
    AddPhotosTableViewCell *cell= (AddPhotosTableViewCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSInteger index=[cell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    NSMutableArray *tempArray = [self.photoDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
    [tempArray removeObjectAtIndex:index];
    [self.photoDictionary setObject:tempArray forKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark-拍照
- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 6;
    } else {
        return 12;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        return 68;
    } else if (indexPath.row==1) {
        return 100;
        
    } else {
        AddPhotosTableViewCell *cell = (AddPhotosTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.height;
        
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"";
    if (indexPath.row==0) {
        idString = @"AgainGoodsTableViewCell";
        AgainGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[AgainGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            
        }
        cell.goodsString  = self.goodsArray[indexPath.section];
        return cell;
    } else if (indexPath.row==1){
        
        idString = @"nomal2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
        }
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        UITextView *remark = [[UITextView alloc] init];
        remark.delegate = self;
        remark.returnKeyType = UIReturnKeyDone;
        remark.tintColor = mainGrayColor;
        remark.autocorrectionType = UITextAutocorrectionTypeNo;
        remark.font = [UIFont systemFontOfSize:16];
        remark.tag = indexPath.section+100;
      
        [cell.contentView addSubview:remark];
        UILabel *hiddenLabel = [UILabel new];
        hiddenLabel.text = @"赶快点评一下吧~";
        hiddenLabel.font = [UIFont systemFontOfSize:15];
        hiddenLabel.textColor = [UIColor hexFloatColor:@"DCDCDC"];
        hiddenLabel.tag = indexPath.section+200;
        [cell.contentView addSubview:hiddenLabel];
       hiddenLabel.hidden = YES;
        
        NSString *remarkString = [NSString stringWithFormat:@"%@",[self.remarkDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]]];
        if (isEmptyString(remarkString)) {
            hiddenLabel.hidden = NO;
        } else {
            remark.text = remarkString;
            hiddenLabel.hidden = YES;
           
            
        }
//        UILabel *tintLabel = [UILabel new];
//        tintLabel.backgroundColor = [UIColor whiteColor];
//        tintLabel.text = @"0/140";
//        tintLabel.textColor = LINE_COLOR;
//        tintLabel.textAlignment = NSTextAlignmentRight;
//        tintLabel.font = [UIFont systemFontOfSize:14];
//        tintLabel.tag = indexPath.section+300;
//        [cell.contentView addSubview:tintLabel];
        [remark mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo (cell.contentView).with.mas_offset(15);
            make.left.mas_equalTo(cell.contentView).with.mas_offset(12.5);
            make.width.mas_equalTo(mainWidth-2*12.5);
            make.bottom.mas_equalTo(cell.contentView).with.mas_offset(-15);
        }];
        
        [hiddenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(remark).with.mas_offset(3);
            make.top.mas_equalTo(remark).with.mas_offset(10);
            make.height.mas_equalTo(14);
        }];
        
//        [tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(remark.mas_bottom);
//            make.right.left.mas_equalTo (remark);
//            make.bottom.mas_equalTo(cell.contentView);
//        }];
        return cell;
    } else {
        idString = @"AddPhotosTableViewCell";
        AddPhotosTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:idString];
        if (!cell) {
            cell=[[AddPhotosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.photosArray=[self.photoDictionary objectForKey:[NSString stringWithFormat:@"%lu",indexPath.section]];
        
        //给cell的删除按钮绑定方法
        for (int i=0; i<cell.deletBtnArray.count; i++) {
            [[cell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
        }
        [cell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos:)]];
        
        //绑定手势  收起键盘
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)]];
        
        return cell;
        
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.goodsArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

@end
