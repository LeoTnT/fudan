//
//  SelectedAddressViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SelectedAddressViewController.h"
#import "ReceiverAddressViewController.h"
#import "VerifyOrderViewController.h"
#import "AddressTableViewCell.h"
#import "ReceiverAddressModel.h"
#import "OrderFormModel.h"

@interface SelectedAddressViewController ()

@property (nonatomic, strong) ReceiverAddressViewController *addressController;//收货地址控制器
@property (nonatomic, strong) NSMutableArray *addressArray;//地址数组
@property (nonatomic, strong) VerifyOrderViewController *verifyController;//确认订单控制器
@end

@implementation SelectedAddressViewController

#pragma mark - lazy loadding
- (NSMutableArray *)addressArray {
    if (!_addressArray) {
        _addressArray = [NSMutableArray array];
    }
    return _addressArray;
}



#pragma mark - life circle
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getAddressInformation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBarHidden = NO;
    self.addressController = [[ReceiverAddressViewController alloc] init];
    self.navigationItem.title = @"选择收货地址";
    self.view.backgroundColor = BG_COLOR;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    @weakify(self);
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"管理" action:^{
        @strongify(self);
        [self.navigationController pushViewController:self.addressController animated:YES];
    }];
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{

    }];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getAddressInformation {
    [XSTool showProgressHUDTOView:self.view withText:@"正在加载中"];
    @weakify(self);
    [HTTPManager getAddressListsSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        
        if (state.status) {
            [self.addressArray removeAllObjects];
            [self.addressArray  addObjectsFromArray:[AddressParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            BOOL isSelectedDefault = YES;
            if (self.addressArray.count==1) {
                AddressParser *addressParser = [self.addressArray firstObject];
                self.addressId = addressParser.ID;
            } else if (self.addressArray.count==0) {
                NSLog(@"没有收货地址");
            } else {
                for (AddressParser *parser in self.addressArray) {
                    if ([parser.ID integerValue]==[self.addressId integerValue]) {
                        isSelectedDefault = NO;
                    }
                }
            }
            //            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            //判断是否有收藏地址
            if (self.addressArray.count==0) {
                [XSTool showToastWithView:self.view Text:@"您还没有添加收货地址，赶紧添加一个吧"];
                self.tableView.hidden = YES;
            } else {
                self.tableView.hidden = NO;
                if (isSelectedDefault) {
                    for (AddressParser *parser in self.addressArray) {
                        if ([parser.is_default integerValue]==1) {
                            self.addressId = parser.ID;
                            break;
                        }
                    }
                }
                //刷新表格
                [self.tableView reloadData];
            }
            [self.navLeftBtn addTarget:self action:@selector(backTolastPage) forControlEvents:UIControlEventTouchUpInside];
        }else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)backTolastPage {
    if (self.addressArray.count==0) {
        self.addressId = @"";
    }
    VerifyOrderViewController *verifyController;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[VerifyOrderViewController class]]) {
            verifyController = (VerifyOrderViewController *)controller;
        }
    }
    verifyController.addressId = self.addressId;
    if (self.addressArray.count==0) {
        verifyController.nameString = @"收货人:无";
        verifyController.phoneString = Localized(@"无");
        verifyController.addressString = @"收货地址:无";
    } else {
        AddressParser *defaultAddress;
        for (AddressParser *address in self.addressArray) {
            if ([self.addressId integerValue]==[address.ID integerValue]) {
                defaultAddress = address;
                break;
            } else {
                if ([address.is_default integerValue]==1) {
                    defaultAddress = address;
                }
            }
        }
        verifyController.nameString = [NSString stringWithFormat:@"收货人:%@",defaultAddress.name];
        verifyController.phoneString = defaultAddress.mobile;
        verifyController.addressString = [NSString stringWithFormat:@"收货地址:%@-%@-%@-%@-%@",[defaultAddress.province name],[defaultAddress.city name],[defaultAddress.county name],[defaultAddress.town name],[defaultAddress detail]];
        verifyController.addressId = defaultAddress.ID;
    }
    verifyController.isBackFromReceiver = NO;
    [self.navigationController popToViewController:verifyController animated:YES];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"addressCell";
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (!cell) {
        cell = [[AddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    //设置内容
    AddressParser *parser = self.addressArray[indexPath.row];
    if ([parser.ID integerValue] ==[self.addressId integerValue]) {
        cell.fontColor = [UIColor redColor];
    }
    if ([parser.is_default integerValue]==1) {
        cell.isShowDefaultButton = YES;
    }
    cell.addressButtonType = AddressButtonTypeForOrder;
    cell.address = parser;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addressArray.count;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressTableViewCell *cell = (AddressTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressTableViewCell *cell = (AddressTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    AddressParser *temp = cell.address;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[VerifyOrderViewController class]]) {
            self.verifyController = (VerifyOrderViewController*)controller;
            self.verifyController.nameString = [NSString stringWithFormat:@"收货人:%@",temp.name];
            self.verifyController.phoneString = temp.mobile;
            self.verifyController.addressString = [NSString stringWithFormat:@"收货地址:%@-%@-%@-%@-%@",[temp.province name],[temp.city name],[temp.county name],[temp.town name],[temp detail]];
            //修改收货地址，刷新运费
            self.verifyController.addressId = temp.ID;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

@end

