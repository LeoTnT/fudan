//
//  ReturnGoodsViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReturnGoodsViewController.h"
#import "XSCustomButton.h"
#import "OrderFormModel.h"
#import "BankCardTypeView.h"
#import "OrderFormViewController.h"
#import "FormsVC.h"

@interface ReturnGoodsViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,CardTypeDelegate>
{
    
    
}

@property(nonatomic,strong)  UITextField *returnMoney;
@property(nonatomic,strong) UITextView *remark;
@property(nonatomic,strong) UILabel *hiddenLabel;
@property(nonatomic,strong) UILabel *tintLabel;
@property(nonatomic,strong) BankCardTypeView  *reasonView;
@property(nonatomic,strong) NSDictionary *rejectReason;
@property(nonatomic,strong) NSString *walletName;//退款去向
@property(nonatomic,strong) NSString *returnValue;//可退金额
@property(nonatomic,strong) NSMutableArray *typeArray;//类型数组
@property(nonatomic,strong) NSString *typeId;
@property(nonatomic,strong) NSDictionary *dataDictionary;
@end

@implementation ReturnGoodsViewController

-(NSMutableArray *)typeArray {
    if (!_typeArray) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}
-(UITextField *)returnMoney{
    if (_returnMoney==nil) {
        _returnMoney = [UITextField new];
        _returnMoney.textAlignment = NSTextAlignmentRight;
        _returnMoney.returnKeyType = UIReturnKeyDone;
        _returnMoney.delegate = self;
        _returnMoney.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return _returnMoney;
}
-(UITextView *)remark{
    if (!_remark) {
        _remark = [UITextView new];
        _remark.delegate = self;
        _remark.tintColor = mainGrayColor;
        _remark.returnKeyType = UIReturnKeyDone;
        _remark.autocorrectionType = UITextAutocorrectionTypeNo;
        _remark.font = [UIFont systemFontOfSize:16];
        
    }
    return _remark;
}
-(UILabel *)hiddenLabel{
    if (!_hiddenLabel) {
        _hiddenLabel = [UILabel new];
        _hiddenLabel.font = [UIFont systemFontOfSize:16];
        _hiddenLabel.textColor = LINE_COLOR;
    }
    return _hiddenLabel;
    
}
-(UILabel *)tintLabel{
    if (!_tintLabel) {
        _tintLabel = [UILabel new];
        _tintLabel.backgroundColor = [UIColor whiteColor];
        _tintLabel.text = @"还可以输入50个字";
        _tintLabel.textColor = LINE_COLOR;
        _tintLabel.textAlignment = NSTextAlignmentRight;
        _tintLabel.font = [UIFont systemFontOfSize:14];
    }
    return _tintLabel;
}
-(BankCardTypeView *)reasonView {
    if (!_reasonView) {
        _reasonView = [[BankCardTypeView alloc] init];
        _reasonView.viewType = AlertTypeBankOrderRejectReason;
        _reasonView.typeDelegate = self;
    }
    return _reasonView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"退货申请";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self getInfo];
}
-(void) getInfo{
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRejectOrderInfoWithOrderId:self.orderId success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if ([dic[@"status"] integerValue]==1) {
            _dataDictionary = dic[@"data"];
            NSDictionary *payDictionnary = _dataDictionary[@"pay_wallet"];
            NSDictionary *temp = [payDictionnary.allValues firstObject];
            self.walletName = [payDictionnary.allKeys firstObject];
            for (NSString *str in temp.allKeys) {
                if ([str isEqualToString:@"value"]) {
                    self.returnValue = temp[str];
                }
            }
            NSDictionary *data = _dataDictionary[@"reasons"];
            // 字典key首字母排序
            NSMutableArray *mArr = [NSMutableArray array];
            NSArray *keysArray = [data allKeys];
            NSArray *sortedArray = [keysArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2 options:NSDiacriticInsensitiveSearch];
            }];
            // 把排序后的key和对应的value放到新的字典中
            for (NSString *key in sortedArray) {
                [mArr addObject:[data objectForKey:key]];
                NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                [mDic setValue:[data objectForKey:key] forKey:key];
                [self.typeArray addObject:mDic];
            }
            self.rejectReason = self.typeArray.firstObject;
            [self setSubviews];
            [self.tableView reloadData];
            
        } else {
            
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
-(void) textViewDidChange:(UITextView*)textView
{
    if ([textView.text length] == 0) {
        self.hiddenLabel.hidden = NO;
    } else {
        self.hiddenLabel.hidden = YES;
        
    }
    if (textView.markedTextRange == nil) {
        if ([textView.text length] == 0) {
            self.tintLabel.text = @"还可以输入50个字";
            
        } else {
            self.tintLabel.text = @"";//这里给空
            
        }
        NSString *nsTextCotent = textView.text;
        NSUInteger existTextNum = [nsTextCotent length];
        NSUInteger remainTextNum = 50 - existTextNum;
        if ((int )remainTextNum<0) {
            remainTextNum = 0;
            self.remark.text = [self.remark.text substringToIndex:50];
        }
        
        self.tintLabel.text = [NSString stringWithFormat:@"还可以输入%lu个字",remainTextNum];
    }
}
-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range

replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        [textView resignFirstResponder];
        
        return YES;
        
    }
    
    if (range.location >= 50)
    {
        
        return  NO;
    }else{
        
        return YES;
    }
    
}

-(void) setSubviews{
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //退款原因
    CGFloat height = self.typeArray.count*40;
    CGFloat width = NORMOL_SPACE*20;
    self.reasonView.frame = CGRectMake(mainWidth-NORMOL_SPACE*2-width, navBarHeight+StatusBarHeight+NORMOL_SPACE+20, width, height);
    self.reasonView.typeArray = self.typeArray;
    
}
#pragma mark - 退款类型协议
-(void)hiddenReasonView:(NSDictionary *)selectedType{
    self.rejectReason = selectedType;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [UIView animateWithDuration:1 animations:^{
        [self.view endEditing:YES];
        self.remark.userInteractionEnabled = YES;
        self.returnMoney.userInteractionEnabled = YES;
        
    } completion:^(BOOL finished) {
        //消失
        [self.reasonView removeFromSuperview];
        
    }];
    
    
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = @"rCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = @"退款原因";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text = self.rejectReason.allValues.firstObject;
            
        }
            break;
        case 1:
        {
            cell.textLabel.text = @"退款金额";
            self.returnMoney.frame = CGRectMake(CGRectGetMaxX(cell.textLabel.frame)+NORMOL_SPACE,0,mainWidth-CGRectGetMaxX(cell.textLabel.frame)-NORMOL_SPACE*2, 50);
            
            
            self.returnMoney.placeholder = [NSString stringWithFormat:@"最多可退：¥%@元",self.returnValue];
            [cell.contentView addSubview:self.returnMoney];
            
        }
            break;
        case 2:
        {
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, 50, NORMOL_SPACE*2)];
            titleLabel.text = Localized(@"note");
            [cell.contentView addSubview:titleLabel];
            //文本输入
            self.remark.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame),NORMOL_SPACE /2, mainWidth-CGRectGetMaxX(titleLabel.frame), 200-NORMOL_SPACE*5);
            self.hiddenLabel.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame), CGRectGetMinY(self.remark.frame)+NORMOL_SPACE, mainWidth-2*NORMOL_SPACE-CGRectGetWidth(titleLabel.frame), NORMOL_SPACE);
            self.tintLabel.frame = CGRectMake(0, CGRectGetMaxY(self.remark.frame), mainWidth-NORMOL_SPACE, NORMOL_SPACE*2);
            [cell.contentView addSubview:self.remark];
            [cell.contentView addSubview:self.hiddenLabel];
            
            [cell.contentView addSubview:self.tintLabel];
        }
            break;
            
        default:
            break;
    }
    return cell;
    
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.view endEditing:YES];
            self.remark.userInteractionEnabled = NO;
            self.returnMoney.userInteractionEnabled = NO;
        } completion:^(BOOL finished) {
            [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.reasonView ] ;
        }];
        
        
    } else {
        
        [self.reasonView removeFromSuperview];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==2) {
        return 200;
    }
    return 50;
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return NORMOL_SPACE;
}
-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 100;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CGFloat height = 100;
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, height)];
    XSCustomButton *submitButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE*2, mainWidth-2*NORMOL_SPACE, height/2) title:Localized(@"bug_submit_do") titleColor:[UIColor whiteColor] fontSize:18 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [submitButton setBorderWith:0 borderColor:nil cornerRadius:5];
    [footer addSubview:submitButton];
    [submitButton addTarget:self action:@selector(clickToSubmitReturnGoodsInfo:) forControlEvents:UIControlEventTouchUpInside];
    
    return footer;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - ReturnGoods
-(void)clickToSubmitReturnGoodsInfo:(UIButton *) sender{
    if ([self.returnMoney.text isEqualToString:@""]||self.returnMoney.text.length==0) {
        [XSTool showToastWithView:self.view Text:@"请输入退款金额！"];
    } else {
        NSString *moneyString;
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        moneyString = [[NSString alloc]initWithString:[self.returnMoney.text stringByTrimmingCharactersInSet:whiteSpace]];
        NSString *remarkString = @"";
        if (self.remark.text.length>0) {
            remarkString = [[NSString alloc]initWithString:[self.remark.text stringByTrimmingCharactersInSet:whiteSpace]];
        }
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager cancelOrderGoodsWithId:self.orderId ReasonId:self.rejectReason.allKeys.firstObject moneyType:self.walletName money:moneyString remark: remarkString success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"退货成功"];
                [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
    
}
-(void)popViewController{
    NSMutableArray *temporaryArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    FormsVC  *tempController;
    for (UIViewController *controller in temporaryArray) {
        if ([controller isKindOfClass:[FormsVC class]]) {
            tempController = (FormsVC *)controller;
            break;
        }
    }
//    tempController.formType = FormTypeRefundOrAfterSale;
    [self.navigationController popToViewController:tempController animated:YES];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ( textField==self.returnMoney) {
        return   [self.returnMoney resignFirstResponder];
    }
    return YES;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self.reasonView removeFromSuperview];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.reasonView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
