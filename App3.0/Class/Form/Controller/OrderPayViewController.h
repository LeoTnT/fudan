//
//  OrderPayViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"

typedef NS_ENUM(NSInteger,OrderLogisticStatus) {
    OrderLogisticStatusRequiredeliver,
    OrderLogisticStatusFree,//免发货
};

typedef NS_ENUM(NSInteger, ReturnType)
{
    ReturnTypeNone,
    ReturnTypeOrder,
    ReturnTypeStore,
    ReturnTypeChat,
    ReturnTypeSupply,
    ReturnTypeOffLine,
    ReturnTypeNormal,//普通的，直接返回上个界面
};
@interface OrderPayViewController : XSBaseTableViewController
/**订单id*/
@property (nonatomic, copy) NSString *orderId;
/**快速买单返回的表名*/
@property (nonatomic, copy) NSString *tableName;

// 从哪个控制器跳转过来的（目前发红包专用）
@property (nonatomic, weak) UIViewController *parentVC;
/**充值界面返回*/
@property (nonatomic, assign) BOOL isRecharge;

/**返回界面类型*/
@property (nonatomic, assign) ReturnType returnType;

/**支付信息*/
- (void)getPayInformation;

/**是否是免发货=>判断订单状态（待发货/待评价）*/
@property (nonatomic, assign) OrderLogisticStatus orderLogisticstatus;

@property (nonatomic, assign) BOOL isSupply;//使用交易所的域名
@end
