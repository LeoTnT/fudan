//
//  LogisticsViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LogisticsViewController.h"
#import "UIImage+XSWebImage.h"
#import "LogisticsTableViewCell.h"
@interface LogisticsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *logisticsArray;
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) LogisticsParser *parser;
@property (nonatomic, strong) UILabel *logLabel;
@property (nonatomic, strong) UILabel *comLabel;
@property (nonatomic, strong) UILabel *expressLabel;
@property (nonatomic, strong) UILabel *telLabel;
@end

@implementation LogisticsViewController
#pragma mark - lazy loadding
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStylePlain];
        _tableView.backgroundColor = BG_COLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
    }
    return _tableView;
}
- (UILabel *)infoLabel {
    if (!_infoLabel) {
        _infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mainHeight/2-30, mainWidth, 60)];
        _infoLabel.textAlignment = NSTextAlignmentCenter;
        _infoLabel.font = [UIFont systemFontOfSize:18];
        _infoLabel.textColor = [UIColor blackColor];
    }
    return _infoLabel;
}
- (NSMutableArray *)logisticsArray {
    if (_logisticsArray==nil) {
        _logisticsArray = [NSMutableArray array];
    }
    return _logisticsArray;
    
}

-(UILabel *)logLabel {
    if (!_logLabel) {
      _logLabel = [[UILabel alloc] init];
    }
    return _logLabel;
}

-(UILabel *)comLabel {
    if (!_comLabel) {
        _comLabel = [[UILabel alloc] init];
    }
    return _comLabel;
}

-(UILabel *)expressLabel {
    if (!_expressLabel) {
        _expressLabel = [[UILabel alloc] init];
    }
    return _expressLabel;
}

-(UILabel *)telLabel {
    if (!_telLabel) {
        
        _telLabel = [[UILabel alloc] init];
    }
    return _telLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"物流详情";
    self.view.backgroundColor = BG_COLOR;
    self.automaticallyAdjustsScrollViewInsets = NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self setSubviews];
    [self getInfo];
}

- (void)getInfo {
    [XSTool showProgressHUDWithView:self.view];//420self.dataParser.id
    [HTTPManager getLogisticsInfoWithId:self.dataParser.ID success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.parser = [LogisticsParser mj_objectWithKeyValues:dic[@"data"]];
            [self reloadTopLogistic];
            NSDictionary *expressDic = dic[@"data"][@"express"];
            if ([expressDic count]>0) {
                NSMutableArray *logArray = [NSMutableArray array];
                for (NSArray *subAry in expressDic.allValues) {
                    
                    for (NSDictionary *dataDic in subAry) {
                        LogisticsDataParser *dParser = [LogisticsDataParser mj_objectWithKeyValues:dataDic];
                        [logArray addObject:dParser];
                        
                    }
                }
                //排序
                NSArray *ary = [logArray sortedArrayUsingComparator:^NSComparisonResult(LogisticsDataParser *data1, LogisticsDataParser *data2) {
                    
                    return [data2.time compare:data1.time];//降序排列
                }];
                [self.logisticsArray  addObjectsFromArray:ary];
               
                if (self.logisticsArray.count>0) {
                    [self.tableView reloadData];
                }
            } else {
                
                [self.view addSubview:self.infoLabel];
                self.infoLabel.text = @"暂时没有物流信息";
            }
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
         [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
-(void)reloadTopLogistic{
    UIFont *font = [UIFont systemFontOfSize:16];
    NSString *logStr = [NSString stringWithFormat:@"物流状态：%@",self.parser.status?self.parser.status:@"暂无"];
    
    // 添加富文本
    NSMutableAttributedString *attStr1 = [[NSMutableAttributedString alloc] initWithString:logStr];
    [attStr1 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor greenColor]
                    range:NSMakeRange(5, logStr.length-5)];
    self.logLabel.attributedText = attStr1;
    self.logLabel.font = font;
    
    NSString *comStr = [NSString stringWithFormat:@"物流公司：%@",self.parser.company?self.parser.company:@"暂无"];
    
    // 添加富文本
    NSMutableAttributedString *attStr2 = [[NSMutableAttributedString alloc] initWithString:comStr];
    [attStr2 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor blueColor]
                    range:NSMakeRange(5, comStr.length-5)];
    self.comLabel.attributedText = attStr2;
    self.comLabel.font = font;
    
    NSString *expressLabelStr = [NSString stringWithFormat:@"快递单号：%@",self.parser.no?self.parser.no:@"暂无"];
    // 添加富文本
    NSMutableAttributedString *attStr3 = [[NSMutableAttributedString alloc] initWithString:expressLabelStr];
    [attStr3 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor blackColor]
                    range:NSMakeRange(0, expressLabelStr.length)];
    self.expressLabel.attributedText = attStr3;
    self.expressLabel.font = font;
    
    NSString *telLabelStr = [NSString stringWithFormat:@"官方电话：%@",self.parser.tel?self.parser.tel:@"暂无"];
    // 添加富文本
    NSMutableAttributedString *attStr4 = [[NSMutableAttributedString alloc] initWithString:telLabelStr];
    [attStr4 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor blueColor]
                    range:NSMakeRange(5, telLabelStr.length-5)];
    self.telLabel.attributedText = attStr4;
    self.telLabel.font = font;
    

}
- (void) setSubviews {
    CGFloat headerHeight = 120, imageHeight = 90;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, headerHeight)];
    topView.backgroundColor = [UIColor whiteColor];
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, imageHeight, imageHeight)];
    OrderDetailDataParser *data = [self.dataParser ext][0];
//    logo.contentMode = UIViewContentModeScaleAspectFit;
    [logo getImageWithUrlStr:data.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    UIFont *font = [UIFont systemFontOfSize:16];
    UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, imageHeight-NORMOL_SPACE*2, CGRectGetWidth(logo.frame), NORMOL_SPACE*2)];
    bottomLabel.text = [NSString stringWithFormat:@"%@件",self
                        .dataParser.number_total];
    bottomLabel.textAlignment = NSTextAlignmentCenter;
    bottomLabel.textColor = [UIColor whiteColor];
    UIColor *bColor = [UIColor blackColor];
    bottomLabel.backgroundColor = [bColor colorWithAlphaComponent:0.5];
    [logo addSubview:bottomLabel];
    [topView addSubview:logo];
    
    CGFloat width = mainWidth-imageHeight-NORMOL_SPACE*3;
    CGFloat height = (imageHeight-NORMOL_SPACE)/4;
    //物流状态
    self.logLabel.frame = CGRectMake(CGRectGetMaxX(logo.frame)+NORMOL_SPACE/2, CGRectGetMinY(logo.frame)+NORMOL_SPACE, width, height);
    self.logLabel.font = font;
    self.logLabel.text = @"物流状态：";
    [topView addSubview:self.logLabel];
    
    //物流公司
    self.comLabel.frame = CGRectMake(CGRectGetMaxX(logo.frame)+NORMOL_SPACE/2, CGRectGetMaxY(self.logLabel.frame), width, height);
    self.comLabel.text = @"物流公司：";
    self.comLabel.font = font;
    [topView addSubview:self.comLabel];
    
    
    //快递单号
    self.expressLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(logo.frame)+NORMOL_SPACE/2, CGRectGetMaxY(self.comLabel.frame), width, height)];
    self.expressLabel.text = @"快递单号：";
    self.expressLabel.font = font;
    [topView addSubview:self.expressLabel];
    
    //官方电话
    self.telLabel.frame =CGRectMake(CGRectGetMaxX(logo.frame)+NORMOL_SPACE/2, CGRectGetMaxY(self.expressLabel.frame), width, height);
    self.telLabel.text = @"官方电话：";
    self.telLabel.font = font;
    [topView addSubview:self.telLabel];

    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(logo.frame)+NORMOL_SPACE, mainWidth, NORMOL_SPACE-2)];
    img.image = [UIImage imageNamed:@"user_order_line"];
    [topView addSubview:img];
    
    [self.view addSubview:topView];
   
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(CGRectGetMaxY(topView.frame), 0, 0, 0));
    }];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str = @"lCell";
    
    LogisticsTableViewCell *cell = (LogisticsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:str];
    if (cell==nil) {
        
        cell = [[LogisticsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    
    LogisticsDataParser *dparser = self.logisticsArray[indexPath.row];
    if (indexPath.row==0) {
        cell.isNewInfo = YES;
    }
    else{
        cell.isNewInfo = NO;
    }
    cell.dataParser = dparser;
    if (cell.cellHeight==0) {
        cell.cellHeight = 150;
    }
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logisticsArray.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    LogisticsTableViewCell *cell = (LogisticsTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
