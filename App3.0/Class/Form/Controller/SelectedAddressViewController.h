//
//  SelectedAddressViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedAddressViewController : XSBaseTableViewController
/**地址id*/
@property (nonatomic, copy) NSString *addressId;
/**商品数量（多个逗号分割）*/
@property (nonatomic, strong) NSString *goodsNum;
/**商品扩展id（多个逗号分割）*/
@property (nonatomic, strong) NSString *extendId;
@end
