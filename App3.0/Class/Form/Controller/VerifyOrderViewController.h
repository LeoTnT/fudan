//
//  VerrifyOrderViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFormModel.h"
#import "GoodsDetailModel.h"

typedef NS_ENUM(NSInteger, VerifyOrderType) {
    VerifyOrderFirst,
    VerifyOrderMore,
};

@interface VerifyOrderViewController : XSBaseTableViewController
//@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *goodsNum;//,分割
@property (nonatomic, strong) NSString *extendId;//,分割
@property (nonatomic, strong) NSString *addressId;
@property (nonatomic, copy) NSString *nameString;
@property (nonatomic, copy) NSString *phoneString;
@property (nonatomic, copy) NSString *addressString;
@property(nonatomic,strong) NSMutableArray *shopArray;//商家数组
@property (nonatomic, copy) NSString *logisticMoney;//运费
@property(nonatomic, copy) NSString *totalPrice;//总价
@property(nonatomic, copy) NSString *sellTotalPrice;//总价不包含运费
@property(nonatomic, assign) BOOL isBackFromReceiver;
@property(nonatomic, assign) BOOL isBackDeliverList;
/**选中的自提地址索引*/
@property (nonatomic, assign) NSUInteger index;

@property(nonatomic,assign)GoodSellType goodSellType; // 团购 秒杀

@property (nonatomic, assign) VerifyOrderType verifyOrderType;//是否是第一次购买


/***/
@property (nonatomic, strong) NSDictionary *paramsDictionary;

- (void)getAddressInformation;
@end
