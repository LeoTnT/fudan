//
//  FormsVC.h
//  App3.0
//
//  Created by mac on 17/3/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FormType)
{
    FormTypeAll = 0,
    FormTypeWaitPay,
    FormTypeWaitSend,
    FormTypeWaitReceive,
    FormTypeWaitEvaluate,
    FormTypeDone,
//    FormTypeRefundOrAfterSale
};

@interface FormsVC : XSBaseViewController
- (instancetype)initWithFormType:(FormType)type;
@property (nonatomic, assign) FormType formType;
@end
