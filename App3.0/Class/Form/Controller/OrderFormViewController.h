//
//  OrderFormViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, OrderType)
{
    OrderTypeAll,
    OrderTypeWaitPay,
    OrderTypeWaitSend,
    OrderTypeWaitReceive,
    OrderTypeWaitEvaluate,
    OrderTypeDone,
//    OrderTypeRefundOrAfterSale
};

@interface OrderFormViewController : XSBaseTableViewController
- (instancetype)initWithOrderType:(OrderType)type;
- (void)uploadOrderInformationIsCancelSuccess:(BOOL)isCancel;
@end
