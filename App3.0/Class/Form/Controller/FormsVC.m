//
//  FormsVC.m
//  App3.0
//
//  Created by mac on 17/3/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FormsVC.h"
#import "OrderFormViewController.h"
#import "GoodsDetailViewController.h"
#import "CartVC.h"
#import "S_StoreInformation.h"
#import "BusinessMainViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "OrderDetailViewController.h"

@interface FormsVC () <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) OrderFormViewController *orderFormVC1;
@property (nonatomic, strong) OrderFormViewController *orderFormVC2;
@property (nonatomic, strong) OrderFormViewController *orderFormVC3;
@property (nonatomic, strong) OrderFormViewController *orderFormVC4;
@property (nonatomic, strong) OrderFormViewController *orderFormVC5;
@property (nonatomic, strong) OrderFormViewController *orderFormVC6;
@property (nonatomic, assign) CGFloat segmentHeight;
@end

@implementation FormsVC
-(OrderFormViewController *)orderFormVC1 {
    if (!_orderFormVC1) {
        _orderFormVC1= [[OrderFormViewController alloc] initWithOrderType:OrderTypeAll];
        _orderFormVC1.view.frame = CGRectMake(0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC1;
    
}
-(OrderFormViewController *)orderFormVC2 {
    if (!_orderFormVC2) {
        _orderFormVC2= [[OrderFormViewController alloc] initWithOrderType:OrderTypeWaitPay];
        _orderFormVC2.view.frame = CGRectMake(mainWidth, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC2;
    
}
-(OrderFormViewController *)orderFormVC3 {
    if (!_orderFormVC3) {
        _orderFormVC3= [[OrderFormViewController alloc] initWithOrderType:OrderTypeWaitSend];
        _orderFormVC3.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC3;
    
}
-(OrderFormViewController *)orderFormVC4 {
    if (!_orderFormVC4) {
        _orderFormVC4= [[OrderFormViewController alloc] initWithOrderType:OrderTypeWaitReceive];
        _orderFormVC4.view.frame = CGRectMake(mainWidth*3, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC4;
    
}
-(OrderFormViewController *)orderFormVC5 {
    if (!_orderFormVC5) {
        _orderFormVC5= [[OrderFormViewController alloc] initWithOrderType:OrderTypeWaitEvaluate];
        _orderFormVC5.view.frame = CGRectMake(mainWidth*4, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC5;
    
}
-(OrderFormViewController *)orderFormVC6 {
    if (!_orderFormVC6) {
        _orderFormVC6= [[OrderFormViewController alloc] initWithOrderType:OrderTypeDone];
        _orderFormVC6.view.frame = CGRectMake(mainWidth*5, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _orderFormVC6;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.segmentHeight = 44;
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"订单管理";

    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        NSUInteger returnPage = 0;
        UIViewController *tempController;
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[GoodsDetailViewController class]]) {
                //从商品详情跳转过来的
                tempController = (GoodsDetailViewController *) controller;
                returnPage = 1;
            }
            if ([controller isKindOfClass:[CartVC class]]) {
                //从购物车跳转过来的
                tempController = (CartVC *) controller;
                returnPage = 2;
            }
            if ([controller isKindOfClass:[BusinessMainViewController class]]) {
                tempController = (BusinessMainViewController *) controller;
                returnPage = 3;
            }
        }
        if (returnPage) {
             [self.navigationController popToViewController:tempController animated:YES];
        } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    [self setSubViews];
}
-(void)setFormType:(FormType)formType {
    _formType = formType;
    self.segmentControl.selectedSegmentIndex = _formType;
    [self segmentedChangedValue:self.segmentControl];
    if (![[self.navigationController.viewControllers lastObject] isKindOfClass:[OrderDetailViewController class]]) {
        for (UIViewController *controller in self.childViewControllers) {
            if ([controller isKindOfClass:[OrderFormViewController class]]) {
                if (_formType==FormTypeWaitEvaluate||_formType==FormTypeWaitSend||_formType==FormTypeDone) {
                    [((OrderFormViewController *)controller) uploadOrderInformationIsCancelSuccess:NO];
                }
                
            }
        }
 
    }
    
}
-(void) setSubViews{
    //根据选项变化位置和内容
    [self addChildViewController:self.orderFormVC1];
    [self.scrollView addSubview:self.orderFormVC1.view];
    [self addChildViewController:self.orderFormVC2];
    [self.scrollView addSubview:self.orderFormVC2.view];
    [self addChildViewController:self.orderFormVC3];
    [self.scrollView addSubview:self.orderFormVC3.view];
    [self addChildViewController:self.orderFormVC4];
    [self.scrollView addSubview:self.orderFormVC4.view];
    [self addChildViewController:self.orderFormVC5];
    [self.scrollView addSubview:self.orderFormVC5.view];
    [self addChildViewController:self.orderFormVC6];
    [self.scrollView addSubview:self.orderFormVC6.view];
}
- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
}

#pragma mark - self cycle

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        self.segmentHeight = 44;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.segmentHeight, mainWidth, mainHeight-self.segmentHeight)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*6, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        self.segmentHeight = 44;
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.segmentHeight)];
        _segmentControl.sectionTitles = @[Localized(@"alls"),@"待付款",@"待发货",@"待收货",Localized(@"me_corder_evaluate"),Localized(@"have_finish")];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor hexFloatColor:@"3F8EF7"]};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = [UIColor hexFloatColor:@"3F8EF7"];
        _segmentControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:COLOR_666666,NSForegroundColorAttributeName,[UIFont systemFontOfSize:14],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}
- (id)initWithFormType:(FormType)type
{
    self = [super init];
    if (self) {
        self.formType = type;
    }
    return self;
}

- (void)searchFrom
{
    
}
//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
