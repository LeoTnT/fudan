//
//  VerifyOrderViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VerifyOrderViewController.h"
#import "SelectedAddressViewController.h"
#import "ReceiverAddressViewController.h"
#import "ReceiverAddressModel.h"
#import "GoodsForVerifyOrderTableViewCell.h"

#import "AreaModel.h"
#import "ChatViewController.h"
#import "OrderPayViewController.h"
#import "S_StoreInformation.h"
#import "GoodsDetailViewController.h"
#import "VerifyOrderTopTableViewCell.h"
#import "DeliveryListViewController.h"
#import "BankCardTypeView.h"


@interface VerifyOrderViewController ()<GoodsForVerifyOrderDelegate,UITextViewDelegate,CardTypeDelegate>
@property(nonatomic,assign) BOOL isAnonymity;//是否匿名
@property(nonatomic,strong) SelectedAddressViewController *selectedController;//管理收货地址控制器
@property(nonatomic,strong) ReceiverAddressViewController *receiverController;//选择收货地址控制器
@property(nonatomic,strong) NSMutableArray *addressInformationArray;//收货地址数组
@property(nonatomic,strong) NSMutableDictionary *descriptionDictionary;//留言
@property(nonatomic,strong) OrderPayViewController *orderPayController;//支付控制器
@property(nonatomic,assign) BOOL keyBoardIsShow;//键盘是否显示
@property(nonatomic,strong) UIView *bottomView;//底部view
@property (nonatomic,assign) CGFloat currentY;//当前纵坐标
@property (nonatomic, strong) UILabel *totalLabel;//总价label
@property (nonatomic, assign) CGPoint original;
@property (nonatomic, strong) NSMutableDictionary *selectedLogisticDictionary;
@property (nonatomic, strong) NSMutableArray *deliveryListArray;//自提地址数组
@property (nonatomic, strong) NSMutableDictionary *selectedDeliveryDictionary;//选中的自提地址
/**哪个商家*/
@property (nonatomic, copy) NSString *deliverySupplyName;
@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, strong) NSMutableDictionary *onlyDeliverySupplyDictionary;//配送类型只有自提的商家 key:uname value:logistype_id
@property (nonatomic, strong) UserInfoDataParser *userParser;
@property (nonatomic, strong) NSMutableArray *couponArray;//抵扣券
@property (nonatomic, strong) BankCardTypeView *typeView;
@property (nonatomic, copy) NSString *couponId;
@property (nonatomic, copy) NSString *coupon;
@end

@implementation VerifyOrderViewController

#pragma mark - lazyLoadding
-(NSMutableArray *)couponArray {
    if (!_couponArray) {
        _couponArray = [NSMutableArray array];
    }
    return _couponArray;
}
- (BankCardTypeView *)typeView {
    if (!_typeView) {
        _typeView = [[BankCardTypeView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _typeView.viewType = AlertTypeCouponType ;
        _typeView.typeDelegate = self;
    }
    return _typeView;
}
-(NSMutableArray *)deliveryListArray {
    if (!_deliveryListArray) {
        _deliveryListArray = [NSMutableArray array];
    }
    return _deliveryListArray;
}

-(NSMutableDictionary *)onlyDeliverySupplyDictionary {
    if (!_onlyDeliverySupplyDictionary) {
        _onlyDeliverySupplyDictionary = [NSMutableDictionary dictionary];
    }
    return _onlyDeliverySupplyDictionary;
}

-(NSMutableDictionary *)selectedDeliveryDictionary {
    if (!_selectedDeliveryDictionary) {
        _selectedDeliveryDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedDeliveryDictionary;
}

-(NSMutableDictionary *)selectedLogisticDictionary {
    if (!_selectedLogisticDictionary) {
        _selectedLogisticDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedLogisticDictionary;
}

- (NSMutableDictionary *)descriptionDictionary {
    if (!_descriptionDictionary) {
        _descriptionDictionary = [NSMutableDictionary dictionary];
    }
    return _descriptionDictionary;
}
- (NSMutableArray *)addressInformationArray {
    if (!_addressInformationArray) {
        _addressInformationArray = [NSMutableArray array];
    }
    return _addressInformationArray;
}
- (NSMutableArray *)shopArray {
    if (!_shopArray) {
        _shopArray = [NSMutableArray array];
    }
    return _shopArray;
}

-(UIView *)bottomView {
    if (!_bottomView) {
        CGFloat bottomHeight = 60;
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, MAIN_VC_HEIGHT-bottomHeight, mainWidth, bottomHeight)];
        XSCustomButton *payButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(mainWidth-120-NORMOL_SPACE, NORMOL_SPACE,120, CGRectGetHeight(self.bottomView.frame)-2*NORMOL_SPACE) title:@"提交订单" titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [payButton setBorderWith:0 borderColor:nil cornerRadius:5];
        [payButton addTarget:self action:@selector(clickToPayMoney:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:payButton];
        self.bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}
- (UILabel *)totalLabel {
    if (!_totalLabel) {
        _totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, mainWidth/2, CGRectGetHeight(self.bottomView.frame))];
        _totalLabel.font = [UIFont systemFontOfSize:16];
        
    }
    return _totalLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isAnonymity = NO;
    self.isBackFromReceiver = YES;
    self.isBackDeliverList = NO;
    self.selectedIndex = 0;
    self.navigationItem.title = @"确认订单";
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.selectedController = [[SelectedAddressViewController alloc] init];
    self.receiverController = [[ReceiverAddressViewController alloc] init];
    self.orderPayController = [[OrderPayViewController alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    self.navigationController.navigationBarHidden=NO;
    
    [self getAddressInformation];
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
//-(void)setIndex:(NSUInteger)index {
//    _index = index;
//
//}

- (void)setSubViews {
    self.tableViewStyle = UITableViewStylePlain;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 60+kTabbarSafeBottomMargin, 0));
    }];
    [self.view layoutIfNeeded];
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    NSString *totalString = [NSString stringWithFormat:@"实付款:¥%.2f",[self.totalPrice floatValue]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:totalString];
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(4, totalString.length-4)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:NSMakeRange(4, totalString.length-4)];
    self.totalLabel.attributedText = attributedString;
    [self.bottomView addSubview:self.totalLabel];
    self.bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame), mainWidth, 60);
    
    [self.view addSubview:self.bottomView];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    //这样就拿到了键盘的位置大小信息frame，然后根据frame进行高度处理之类的信息
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        CGFloat space = self.currentY-CGRectGetMinY(frame);
        if (space>=0) {
            
            //移动
            self.view.transform = CGAffineTransformMakeTranslation(0, -fabs(space));
            if (self.currentY>mainHeight-CGRectGetHeight(self.bottomView.frame)) {
                CGPoint point = self.tableView.contentOffset;
                [self.tableView setContentOffset:CGPointMake(0, point.y+30)];
                NSLog(@"%f,%f",point.y,self.tableView.contentOffset.y);
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
}
//-(void)keyboardDidShow{
//    self.keyBoardIsShow=YES;
//}
-(void)keyboardWillHidden:(NSNotification *)notification
{
    
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
        self.tableView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}

-(void)getAddressInformation{
    [self.addressInformationArray removeAllObjects];
    //    [self.descriptionDictionary removeAllObjects];
    self.nameString = @"收货人:";
    self.phoneString = @"";
    self.addressString = @"收货地址:";
    if (self.verifyOrderType==VerifyOrderMore) {
        
        //非第一次购买
        [self setSubViews];
        [self getLogisticInformation];
    } else {
        NSString *purchaseType = @"";
        if (self.goodSellType == GoodSellGroupBuy) {
            purchaseType = @"shareOrder";
        }
        //批发商品
        NSMutableDictionary *param;
        if (self.goodSellType == GoodSellWholesale) {
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.paramsDictionary
                                                               options:kNilOptions
                                                                 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                         encoding:NSUTF8StringEncoding];
            param = [@{
                       @"preview_name":@"wholesale",
                       @"params":jsonString,
                       @"address_id":self.addressId?self.addressId:@""
                       
                       } mutableCopy];
            
        } else {
            //普通商品
            param = [@{
                       @"nums":self.goodsNum,
                       @"ext_ids":self.extendId,
                       @"address_id":self.addressId?self.addressId:@""
                       } mutableCopy];
            if (!isEmptyString(purchaseType)) {
                [param setObject:purchaseType forKey:@"purchaseType"];
            }
            
        }
        //订单预处理
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager previewOrderWithParams:param success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                
                [self.shopArray removeAllObjects];
                
                
                NSDictionary *list = dic[@"data"][@"list"];
                if (list.allKeys>0) {
                    PreviewVerifyOrderParser *previewParser = [PreviewVerifyOrderParser mj_objectWithKeyValues:list];
                    [self.shopArray addObjectsFromArray:previewParser.products];//商家数组
                    self.totalPrice = [NSString stringWithFormat:@"%.2f",[previewParser.sell_price_total floatValue]+[previewParser.yunfei_total floatValue]];
                    self.sellTotalPrice = previewParser.sell_price_total;
                    self.logisticMoney = previewParser.yunfei_total;
                    [self setSubViews];
                    
                }
                self.addressId = dic[@"data"][@"address_id"];
                NSLog(@"=====%@",self.addressId);
                [self getLogisticInformation];
                
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
                if ([state.info containsString:@"1次"]||[state.info containsString:@"一次"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
    
    
    
}

- (void) getLogisticInformation {
    if ((NSNull *)self.addressId == [NSNull null]||(!self.addressId)||[[NSString stringWithFormat:@"%@",self.addressId] isEqualToString:@""]) {
        if (self.isBackFromReceiver) {
            //跳转到收货地址界面
            [self.navigationController pushViewController:self.receiverController animated:YES];
        } else {
            [self.selectedLogisticDictionary removeAllObjects];
            [self.tableView reloadData];
        }
        
    } else {
        
        //获取物流信息
        [HTTPManager getAppointAddressWithId:self.addressId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                AppointAddressParser *appointParser = [AppointAddressParser mj_objectWithKeyValues:dic[@"data"]];
                self.nameString = [NSString stringWithFormat:@"收货人:%@",appointParser.name];
                self.phoneString = [NSString stringWithFormat:@"%@",appointParser.mobile];
                NSString *detail =[NSString stringWithFormat:@"%@",appointParser.detail];
                self.addressString = [NSString stringWithFormat:@"收货地址:%@-%@-%@-%@-%@",appointParser.province,appointParser.city,appointParser.county,appointParser.town,detail];
                for (int i=0; i<self.shopArray.count; i++) {
                    PreviewVerifyOrderProductsParser *productParser = self.shopArray[i];
                    PreviewDeliveryListParser *deliveryParser = [productParser.delivery_list firstObject];
                    PreviewVerifyWuliuParser *wuliuParser = [deliveryParser.logis_type firstObject];
                    
                    
                    if (!self.isBackDeliverList) {
                        
                        [self.selectedLogisticDictionary setObject:[NSString stringWithFormat:@"%lu",[wuliuParser.logis_id integerValue]+100]  forKey:productParser.sup_uname];
                        //上门自提
                        if ([wuliuParser.logis_id integerValue]==2) {
                            
                            [self.onlyDeliverySupplyDictionary setObject:[NSString stringWithFormat:@"%lu",[wuliuParser.logis_id integerValue]+100] forKey:productParser.sup_uname];
                            [self changeLogisticWithParser:wuliuParser supplyName:productParser.sup_uname];
                        }
                        [self.deliveryListArray removeAllObjects];
                        [self.selectedDeliveryDictionary removeAllObjects];
                    } else {
                        if (!self.index) {
                            if (self.selectedIndex&&self.deliverySupplyName) {
                                [self.selectedDeliveryDictionary setObject:self.deliveryListArray[self.selectedIndex-1] forKey:self.deliverySupplyName];
                                //                            [self.tableView reloadData];
                            }
                        } else {
                            [self.selectedDeliveryDictionary setObject:self.deliveryListArray[self.index-1] forKey:self.deliverySupplyName];
                            self.selectedIndex = self.index;
                        }
                        
                    }
                    
                }
                NSLog(@"===-----%@",self.descriptionDictionary);
                [self.tableView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
    
    
}

- (void)anonymityAction:(UIButton *) sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.isAnonymity = YES;
    } else {
        self.isAnonymity = NO;
    }
}

- (void)couponButtonAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        NSLog(@"使用购物券");
    }
}

- (void)clickToPayMoney:(UIButton *) sender {
    NSMutableString *extString  = [NSMutableString string];
    NSMutableString *numberString  = [NSMutableString string];
    NSMutableDictionary *logisticDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *deliveryLogisticDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *logisticWithPaserDictionary = [NSMutableDictionary dictionary];
    
    NSMutableArray *logisticArray = [NSMutableArray array];
    for (PreviewVerifyOrderProductsParser *products in self.shopArray) {
        PreviewDeliveryListParser *delivery =  [products.delivery_list firstObject];
        
        
        if (![self.descriptionDictionary.allKeys containsObject:products.supply_id]) {
            [self.descriptionDictionary setObject:@"" forKey:products.supply_id];
        }
        
        NSString *idString = [NSString stringWithFormat:@"%@",products.supply_id];
        
        NSMutableDictionary *logisticTemp = [NSMutableDictionary dictionary];
        NSMutableDictionary *deliveryTemp = [NSMutableDictionary dictionary];
        
        NSString *logisticType ;
        if (self.selectedLogisticDictionary.allKeys.count) {
            logisticType = [self.selectedLogisticDictionary objectForKey:products.sup_uname];
            
        } else {
            logisticType = [self.onlyDeliverySupplyDictionary objectForKey:products.sup_uname];
        }
        //        logtype            商家1 配送7 =  方式2
        //        logtype_duid   商家1 配送7 =  配送8
        [logisticTemp setObject:[NSString stringWithFormat:@"%lu",[logisticType integerValue]-100] forKey:delivery.uid];
        for (PreviewVerifyWuliuParser *parser in delivery.logis_type) {
            if ([[NSString stringWithFormat:@"%lu",[logisticType integerValue]-100] isEqualToString:parser.logis_id]) {
                [logisticWithPaserDictionary setObject:[NSString stringWithFormat:@"%@%@",parser.logis_name,parser.logis_id] forKey:[NSString stringWithFormat:@"%@",products.supply_id]];
                
            }
            
        }
        
        [logisticDictionary setObject:logisticTemp forKey:idString];
        if ([self.selectedDeliveryDictionary.allKeys containsObject:products.sup_uname]) {
            ValidDeliveryObtainDetailParser *deliveryParser = [self.selectedDeliveryDictionary objectForKey:products.sup_uname];
            NSString *deliveryId = deliveryParser.user_id;
            [deliveryTemp setObject:deliveryId forKey:delivery.uid];
            [deliveryLogisticDictionary setObject:deliveryTemp forKey:idString];
        }
        
        [logisticArray addObject:logisticTemp];
        for (PreviewVerifyOrderDetailProductParser *detailProduct in delivery.product_list) {
            [extString appendString:[NSString stringWithFormat:@"%@,",detailProduct.product_ext_id]];
            [numberString appendString:[NSString stringWithFormat:@"%@,",detailProduct.total_num]];
            
        }
        
        
    }
    [extString deleteCharactersInRange:NSMakeRange([extString length]-1, 1)];
    [numberString deleteCharactersInRange:NSMakeRange([numberString length]-1, 1)];
    if ((NSNull *)self.addressId == [NSNull null]||(!self.addressId)) {
        [XSTool showToastWithView:self.view Text:@"请选择收货地址！"];
    } else {
        
        NSMutableDictionary *param;
        // 创建订单参数
        NSError *error = nil;
        NSData *logisticJsonData = [NSJSONSerialization dataWithJSONObject:logisticDictionary
                                                                   options:kNilOptions
                                                                     error:&error];
        NSString *logisticJsonString = [[NSString alloc] initWithData:logisticJsonData
                                                             encoding:NSUTF8StringEncoding];
        
        NSData *descJsonData = [NSJSONSerialization dataWithJSONObject:self.descriptionDictionary
                                                               options:kNilOptions
                                                                 error:&error];
        NSString *descJsonString = [[NSString alloc] initWithData:descJsonData
                                                         encoding:NSUTF8StringEncoding];
        if (self.goodSellType==GoodSellWholesale) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.paramsDictionary
                                                               options:kNilOptions
                                                                 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                         encoding:NSUTF8StringEncoding];
            param = [@{
                       @"order_name":@"wholesale",
                       @"wholesale_params":jsonString,
                       @"address_id":self.addressId,
                       @"logtype":logisticJsonString,
                       @"desc":descJsonString,
                       @"is_private":[NSString stringWithFormat:@"%d",self.isAnonymity]
                       } mutableCopy];
            if (deliveryLogisticDictionary.count) {
                NSData *logisticDuidJsonData = [NSJSONSerialization dataWithJSONObject:deliveryLogisticDictionary
                                                                               options:kNilOptions
                                                                                 error:&error];
                NSString *logisticDuidJsonString = [[NSString alloc] initWithData:logisticDuidJsonData
                                                                         encoding:NSUTF8StringEncoding];
                [param setObject:logisticDuidJsonString forKey:@"logtype_duid"];
            }
            
            
        } else {
            
            param = [@{
                       @"op_ids":extString,
                       @"num":numberString,
                       @"address_id":self.addressId,
                       @"logtype":logisticJsonString,
                       @"desc":descJsonString,
                       @"is_private":[NSString stringWithFormat:@"%d",self.isAnonymity]
                       } mutableCopy];
            //抵扣券
            if (!isEmptyString(self.couponId)) {
                if ([self.couponId isEqualToString:@"no_use"]) {
                    
                } else {
                    [param setObject:self.couponId forKey:@"deduction_coupon_user_id"];//提交订单时所选择的用户抵扣券id(20171024抵扣券追加参数)
                }
            }
            
            if (deliveryLogisticDictionary.count) {
                NSData *logisticDuidJsonData = [NSJSONSerialization dataWithJSONObject:deliveryLogisticDictionary
                                                                               options:kNilOptions
                                                                                 error:&error];
                NSString *logisticDuidJsonString = [[NSString alloc] initWithData:logisticDuidJsonData
                                                                         encoding:NSUTF8StringEncoding];
                [param setObject:logisticDuidJsonString forKey:@"logtype_duid"];
            }
            if (self.goodSellType == GoodSellGroupBuy) {
                [param setObject:@"shareOrder" forKey:@"purchaseType"];
            }
            
        }
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager createOrderWithParams:param success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.orderPayController.orderId = dic[@"data"];
                BOOL isFree = NO;
                for (NSString *item in logisticWithPaserDictionary.allValues) {
                    if ([item containsString:@"4"]|| [item containsString:@"免发货"]) {
                        isFree = YES;
                        break;
                    }
                }
                if (isFree) {
                    self.orderPayController.orderLogisticstatus = OrderLogisticStatusFree;
                    
                } else {
                    
                    self.orderPayController.orderLogisticstatus = OrderLogisticStatusRequiredeliver;
                }
                [self.navigationController pushViewController:self.orderPayController animated:YES];
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
}

#pragma mark - CardTypeDelegate
- (void)hiddenCouponView:(NSString *)couponId coupon:(NSString *)coupon{
    self.couponId = isEmptyString(couponId)?@"":couponId;
    self.coupon = isEmptyString(coupon)?@"":coupon;
    NSLog(@"couponId===%@;;;;coupon====%@",self.couponId,self.coupon);
    [self.typeView removeFromSuperview];
    [self.tableView reloadData];
}
-(void)hiddenCouponView {
    [self.typeView removeFromSuperview];
}
#pragma mark - UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self.tableView layoutIfNeeded];
    GoodsForVerifyOrderTableViewCell *goodsCell =(GoodsForVerifyOrderTableViewCell *)textView.superview.superview.superview;
    NSIndexPath *index = [self.tableView indexPathForCell:goodsCell];
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:index];//获取cell在tableView中的位置
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    //    NSLog(@"%@,%@",rectInSuperview ,rectInTableView);
    CGFloat height = 0;
    height = rectInSuperview.origin.y+rectInSuperview.size.height;
    self.currentY = height;
    return YES;
}

- (void)textViewDidChange:(UITextView*)textView {
    for (PreviewVerifyOrderProductsParser *products in self.shopArray) {
        if (textView.tag==[products.supply_id integerValue]) {
            GoodsForVerifyOrderTableViewCell *cell =(GoodsForVerifyOrderTableViewCell *)textView.superview.superview.superview;
            if ([textView.text length]==0) {
                
                cell.tintInfo.hidden = NO;
                
            } else {
                cell.tintInfo.hidden = YES;
            }
        }
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    for (PreviewVerifyOrderProductsParser *products in self.shopArray) {
        if (textView.tag==[products.supply_id integerValue]) {
            //判断是否已经存储了此商家的留言
            if ([self.descriptionDictionary.allKeys containsObject:[NSString stringWithFormat:@"%@",products.supply_id]]) {
                [self.descriptionDictionary removeObjectForKey:[NSString stringWithFormat:@"%@",products.supply_id]];
            }
            NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *descString = [[NSString alloc]initWithString:[textView.text stringByTrimmingCharactersInSet:whiteSpace]];
            [self.descriptionDictionary setObject:descString forKey:[NSString stringWithFormat:@"%@",products.supply_id]];
        }
    }
    [textView resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    if ([text isEqualToString:@"\n"]) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldInteractWithTextAttachment:(NSTextAttachment *)textAttachment inRange:(NSRange)characterRange {
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
    
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        NSString *idString = @"dCell";
        VerifyOrderTopTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[VerifyOrderTopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
        }
        cell.addressInformationArray = @[self.nameString,self.phoneString,self.addressString];
        
        return cell;
        
    }
    else if (indexPath.row==2+_shopArray.count-1) {
        NSString *idString = @"aCell";
        UITableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = BG_COLOR;
        //按钮
        UIButton *anonymityButton = [[UIButton alloc] initWithFrame:CGRectMake(0, NORMOL_SPACE, mainWidth, NORMOL_SPACE*5)];
        [anonymityButton setTitle:@"匿名购买" forState:UIControlStateNormal];
        anonymityButton.backgroundColor = [UIColor whiteColor];
        //向左对齐
        anonymityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        anonymityButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [anonymityButton setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [anonymityButton setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [anonymityButton setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        anonymityButton.imageEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(anonymityButton.imageView.frame), 0, 0);
        anonymityButton.titleEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(anonymityButton.imageView.frame)+5, 0, 0);
        anonymityButton.userInteractionEnabled = YES;
        [anonymityButton addTarget:self action:@selector(anonymityAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:anonymityButton];
        cell.frame = CGRectMake(0, 0, mainWidth, CGRectGetMaxY(anonymityButton.frame));
        return cell;
        
        
    }
    //    else if (indexPath.row==3+self.shopArray.count-1){//1+1=2
    //        NSString *idString = @"aCell";
    //        UITableViewCell  *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idString];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        cell.backgroundColor = BG_COLOR;
    //        //按钮
    //        UIButton *couponBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, NORMOL_SPACE, mainWidth, NORMOL_SPACE*5)];
    //        [couponBtn setTitle:@"最多使用0购物券抵现金" forState:UIControlStateNormal];
    //        couponBtn.backgroundColor = [UIColor whiteColor];
    //        //向左对齐
    //        couponBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //        couponBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    //        [couponBtn setTitleColor:mainGrayColor forState:UIControlStateNormal];
    //        [couponBtn setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
    //        [couponBtn setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
    //
    //        couponBtn.imageEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(couponBtn.imageView.frame), 0, 0);
    //        couponBtn.titleEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(couponBtn.imageView.frame)+5, 0, 0);
    //        couponBtn.userInteractionEnabled = YES;
    //        [couponBtn addTarget:self action:@selector(couponBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //        [cell.contentView addSubview:couponBtn];
    //        cell.frame = CGRectMake(0, 0, mainWidth, CGRectGetMaxY(couponBtn.frame)+NORMOL_SPACE);
    //        return cell;
    //    }
    else  {
        NSString *string = @"GoodsForVerifyOrderTableViewCell";
        GoodsForVerifyOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[GoodsForVerifyOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
            cell.verifyOrderDelegate = self;
            
        }
        //        cell.totalPrice = [self.totalPrice floatValue];
        //        cell.logisticMoney = [self.logisticMoney floatValue];
        if (self.shopArray.count>indexPath.row-1) {
            PreviewVerifyOrderProductsParser *parser = self.shopArray[indexPath.row-1];
            cell.selectedLogisticType = [[self.selectedLogisticDictionary objectForKey:parser.sup_uname] integerValue];
            if ([self.selectedDeliveryDictionary.allKeys containsObject:parser.sup_uname]) {
                cell.deliveryParser = [self.selectedDeliveryDictionary objectForKey:parser.sup_uname];
            }
            cell.remark.delegate = self;
            cell.shopParser = self.shopArray[indexPath.row-1];
            
            PreviewVerifyOrderProductsParser *products = self.shopArray[indexPath.row-1];
            if (self.descriptionDictionary.count>0) {
                
                for (NSString *key in self.descriptionDictionary.allKeys) {
                    if ([key integerValue]==[products.supply_id integerValue]) {
                        
                        //判断是否已经存储了此商家的留言
                        cell.remark.text = [self.descriptionDictionary objectForKey:key];
                        //                        cell.remark.delegate = self;
                        [self textViewDidChange:cell.remark];
                    }
                }
            }
            
        }
        if (!isEmptyString(self.couponId)) {
             cell.couponLabel.textColor = [UIColor hexFloatColor:@"FC4343"];
            if ([self.couponId isEqualToString:@"no_use"]) {
                cell.couponLabel.text = [NSString stringWithFormat:@"%@",self.coupon];
            } else {
                cell.couponLabel.text = [NSString stringWithFormat:@"%@元",self.coupon];
            }
            
        }
        
        return cell;
    }
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2+self.shopArray.count;
}

#pragma mark - UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        self.selectedController.addressId = self.addressId;
        self.selectedController.goodsNum = self.goodsNum;
        self.selectedController.extendId = self.extendId;
        [self.navigationController pushViewController:self.selectedController animated:YES];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height=0;
    if (indexPath.row==0) {
        VerifyOrderTopTableViewCell *cell = (VerifyOrderTopTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
        height = cell.cellHeight;
    } else if (indexPath.row>0&&indexPath.row<1+self.shopArray.count) {
        GoodsForVerifyOrderTableViewCell *cell = (GoodsForVerifyOrderTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        height = cell.cellHeight;
    } else {
        UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
        height = CGRectGetHeight(cell.frame);
    }
    return height;
    
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, NORMOL_SPACE)];
    view.backgroundColor = BG_COLOR;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return NORMOL_SPACE;
}

//
//#pragma mark - VerifyDeliveryDelegate

#pragma mark - VerifyOrderDelegate
//-(void)getRemakeIndex:(NSUInteger)index {
//    NSLog(@"%lu_____",(unsigned long)index);
//    for (PreviewVerifyOrderProductsParser *products in self.shopArray) {
//        if (index==[products.supply_id integerValue]) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.shopArray indexOfObject:products]+1 inSection:0];
//            GoodsForVerifyOrderTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            self.currentCellRect = cell.frame;
//        }
//    }
//
//}
-(void)changeDeliveryWithSupplyName:(NSString *)supplyName {
    self.deliverySupplyName = supplyName;
    //跳转到自提点界面
    DeliveryListViewController *controller = [[DeliveryListViewController alloc] init];
    controller.deliveryListArray = self.deliveryListArray;
    controller.supplyId = self.deliverySupplyName;
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)changeLogisticWithParser:(PreviewVerifyWuliuParser *)logisticParser supplyName:(NSString *)supplyName{
    if ([logisticParser.logis_name isEqualToString:@"上门自提"]||[logisticParser.logis_id integerValue]==2) {
        PreviewVerifyOrderProductsParser *productParser;
        NSMutableArray *extIds = [NSMutableArray array];
        NSMutableArray *nums = [NSMutableArray array];
        
        NSString *suppy_id,*duid;
        BOOL isAutoDelivery = NO;
        PreviewDeliveryListParser *deliveryParser;
        for (int i=0; i<self.shopArray.count; i++) {
            PreviewVerifyOrderProductsParser *parser = self.shopArray[i];
            deliveryParser = [parser.delivery_list firstObject];
            //匹配商家id
            if ([parser.sup_uname isEqualToString:supplyName]) {
                productParser = parser;
                suppy_id = parser.supply_id;
                isAutoDelivery = YES;
                break;
                
            }
        }
        if (isAutoDelivery) {
            for (int k=0; k<productParser.delivery_list.count; k++) {
                PreviewDeliveryListParser *deliveryList = productParser.delivery_list[k];
                duid = deliveryList.uid;
                for (int j=0; j<deliveryList.product_list.count; j++) {
                    PreviewVerifyOrderDetailProductParser *detailProduct = deliveryList.product_list[j];
                    
                    [extIds addObject:detailProduct.product_ext_id];
                    [nums addObject:detailProduct.total_num];
                }
                
            }
            ValidDeliverySubmitModel *model = [ValidDeliverySubmitModel mj_objectWithKeyValues:@{@"addrid":self.addressId,@"peids":[extIds componentsJoinedByString:@","],@"nums":[nums componentsJoinedByString:@","],@"suid":suppy_id,@"duid":duid}];
            //获取自提信息
            [XSTool showProgressHUDWithView:self.view];
            @weakify(self);
            [HTTPManager orderGetValidDeliveryWithModel:model.mj_keyValues success:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    ValidDeliveryObtainParser *parser = [ValidDeliveryObtainParser mj_objectWithKeyValues:dic];
                    if (parser.data.count) {
                        [self.deliveryListArray removeAllObjects];
                        [self.deliveryListArray addObjectsFromArray:parser.data];
                        [self.selectedDeliveryDictionary setObject:[self.deliveryListArray firstObject] forKey:supplyName];
                        [self.selectedLogisticDictionary setObject:[NSString stringWithFormat:@"%lu",[logisticParser.logis_id integerValue]+100] forKey:supplyName];
                        
                    } else {
                        if (deliveryParser.logis_type.count==1) {
                            [self.onlyDeliverySupplyDictionary setObject:[NSString stringWithFormat:@"%lu",[logisticParser.logis_id integerValue]+100] forKey:productParser.sup_uname];
                            [self.selectedLogisticDictionary removeObjectForKey:supplyName];
                        }
                        [XSTool showToastWithView:self.view Text:@"无自提点"];
                        //按钮变为物流配送的
                        
                    }
                    [self.tableView reloadData];
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
            
        }
        
    } else {
        [self.selectedLogisticDictionary setObject:[NSString stringWithFormat:@"%lu",[logisticParser.logis_id integerValue]+100] forKey:supplyName];
        [self.deliveryListArray removeAllObjects];
        [self.selectedDeliveryDictionary removeObjectForKey:supplyName];
        [self.tableView reloadData];
    }
    
}

- (void)linkSupplyWithSupplyName:(NSString *)supplyName {
    [XSTool showProgressHUDWithView:self.view];
    if (self.userParser) {
#ifdef ALIYM_AVALABLE
        YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
        YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
        YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
        chatVC.title = [self.userParser getName];
        chatVC.avatarUrl = self.userParser.logo;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
        [XSTool hideProgressHUDWithView:self.view];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
        chatVC.title = [self.userParser getName];
        chatVC.avatarUrl = self.userParser.logo;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#else
        XMChatController *chatVC = [XMChatController new];
        SiganlChatModel *signalModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
        ConversationModel *model = [[ConversationModel alloc] initWithSiganlChatModel:signalModel];
        chatVC.conversationModel = model;
        [self.navigationController pushViewController:chatVC animated:YES];
#endif
        
    } else {
        [HTTPManager getUserInfoWithUid:supplyName success:^(NSDictionary * dic, resultObject *state) {
            if (state.status) {
                self.userParser = [UserInfoParser mj_objectWithKeyValues:dic].data;
                [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
                YWPerson *person = [[YWPerson alloc] initWithPersonId:self.userParser.uid];
                YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                chatVC.title = [self.userParser getName];
                chatVC.avatarUrl = self.userParser.logo;
                chatVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
                EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.userParser.uid type:EMConversationTypeChat createIfNotExist:YES];
                [XSTool hideProgressHUDWithView:self.view];
                ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                chatVC.title = [self.userParser getName];
                chatVC.avatarUrl = self.userParser.logo;
                chatVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:chatVC animated:YES];
#else
                XMChatController *chatVC = [XMChatController new];
                SiganlChatModel *signalModel = [[SiganlChatModel alloc] initWithChatWithJID:self.userParser.uid title:[self.userParser getName] avatarURLPath:self.userParser.logo];
                ConversationModel *model = [[ConversationModel alloc] initWithSiganlChatModel:signalModel];
                chatVC.conversationModel = model;
                [self.navigationController pushViewController:chatVC animated:YES];
#endif
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }
}
- (void)lookShopMainPageWithSupplyId:(NSString *)supplyId {
    S_StoreInformation *storeController = [S_StoreInformation new];
    storeController.storeInfor = supplyId;
    [self.navigationController pushViewController:storeController animated:YES];
}

-(void)lookCoupon {
    [self.view endEditing:YES];
    if (self.couponArray.count) {
        if (!isEmptyString(self.couponId)) {
            self.typeView.selectedId = self.couponId;
        }
        self.typeView.typeArray = [NSMutableArray arrayWithArray:self.couponArray];
        
        [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView];
    } else {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getValidDeductionCouponListWithParams:@{@"pay_amount":self.sellTotalPrice} success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                OrderCouponDataParser *couponDataParser = [OrderCouponDataParser mj_objectWithKeyValues:dic];
                //                if (self.couponDataParser.data.count) {
                //有券
                //不使用抵扣券
                OrderCouponDataDetailParser *detailParser = [OrderCouponDataDetailParser new];
                detailParser.ID = @"no_use";
                detailParser.limit_amount = @"不使用抵扣券";
                detailParser.limit_amount_str = @"";
                [self.couponArray addObject:detailParser];
                [self.couponArray addObjectsFromArray:couponDataParser.data];
                if (!isEmptyString(self.couponId)) {
                    self.typeView.selectedId = self.couponId;
                }
                self.typeView.typeArray = self.couponArray;
                
                [[ [ UIApplication  sharedApplication ]  keyWindow ] addSubview :self.typeView];
                //                }
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } failure:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
}
@end
