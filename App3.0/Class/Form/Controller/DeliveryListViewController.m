//
//  DeliveryListViewController.m
//  App3.0
//
//  Created by nilin on 2017/10/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DeliveryListViewController.h"
#import "DeliveryListTableViewCell.h"
#import "VerifyOrderViewController.h"

@interface DeliveryListViewController ()<UITableViewDelegate,UITableViewDataSource>

//@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *topViewLabel;
@end

@implementation DeliveryListViewController
#pragma mark - lazyLoadding
//-(NSMutableArray *)deliveryListArray {
//    if (_deliveryListArray) {
//        _deliveryListArray = [NSMutableArray array];
//    }
//    return _deliveryListArray;
//}

-(UILabel *)topViewLabel {
    if (!_topViewLabel) {
        _topViewLabel = [UILabel new];
        _topViewLabel.font = [UIFont qsh_systemFontOfSize:16];
        _topViewLabel.numberOfLines = 0;
        _topViewLabel.backgroundColor = [UIColor orangeColor];
        _topViewLabel.text = @"平台将根据您的收货地址显示其范围内的自提点，请确保您的收货地址正确填写。";
        
    }
    return _topViewLabel;
}

//- (UITableView *)tableView {
//    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
////        _tableView.showsVerticalScrollIndicator = NO;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
////        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
//        _tableView.backgroundColor = BG_COLOR;
//        UIView *view = [UIView new];
//        _tableView.tableFooterView = view;
//    }
//    return _tableView;
//}
#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"自提地址";
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[VerifyOrderViewController class]]) {
            VerifyOrderViewController *controller =  self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            controller.isBackDeliverList = YES;
            [self.navigationController popToViewController:controller animated:YES];
        }

        
    }];

    self.tableViewStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorStyle = UITableViewCellEditingStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
//    [self getListInformation];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private


-(void)setDeliveryListArray:(NSMutableArray *)deliveryListArray {
    _deliveryListArray = deliveryListArray;
    if (_deliveryListArray) {
        [self.tableView reloadData];
    }

}
- (void) getListInformation {
//    [XSTool showProgressHUDWithView:self.view];
//    @weakify(self);
//    [HTTPManager orderGetValidDeliveryWithModel:self.model.mj_keyValues success:^(NSDictionary *dic, resultObject *state) {
//        @strongify(self);
//        [XSTool hideProgressHUDWithView:self.view];
//        if (state.status) {
//            ValidDeliveryObtainParser *parser = [ValidDeliveryObtainParser mj_objectWithKeyValues:dic];
//            [self.deliveryListArray removeAllObjects];
//            [self.deliveryListArray addObjectsFromArray:parser.data];
//            [self.tableView reloadData];
//        } else {
//            [XSTool showToastWithView:self.view Text:state.info];
//        }
//        
//    } failure:^(NSError *error) {
//        [XSTool hideProgressHUDWithView:self.view];
//         [XSTool showToastWithView:self.view Text:NetFailure];
//    }];

}


#pragma mark - TableviewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idString = @"nomalCell";
    
    if (indexPath.section==0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor orangeColor];
        }
        [cell.contentView addSubview:self.topViewLabel];
        [self.topViewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(cell.contentView).with.mas_offset(13);
            make.right.mas_equalTo(cell.contentView).with.mas_equalTo(-13);
        }];
        return cell;
        
    } else {
        idString = @"DeliveryListTableViewCell";
        DeliveryListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
        if (cell==nil) {
            cell = [[DeliveryListTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
           
        }
        if (self.deliveryListArray.count>indexPath.row) {
            cell.detailParser = self.deliveryListArray[indexPath.row];
        }
        return cell;

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==1) {
        //返回上一个界面
        if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[VerifyOrderViewController class]]) {
            VerifyOrderViewController *controller =  self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            controller.isBackDeliverList = YES;
            controller.index = indexPath.row+1;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
   
   
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return 60;
    } else {
        return 90;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    } else {
     return self.deliveryListArray.count;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==1) {
        return 10;
    } else {
        return 0.01;
    }
}
@end
