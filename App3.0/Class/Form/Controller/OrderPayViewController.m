//
//  OrderPayViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OrderPayViewController.h"
#import "AppDelegate.h"
#import "FormsVC.h"
#import "QuickPayingViewController.h"
#import "S_StoreInformation.h"
#import "ModifyOldPayViewController.h"
#import "XSCustomButton.h"
#import "PayTableViewCell.h"
#import "PayMethodTableViewCell.h"
#import "PayMethodView.h"
#import "UserModel.h"
#import "PayModel.h"
#import "RSAEncryptor.h"
#import "RechargeViewController.h"
#import "RedPacketSendViewController.h"
#import "ChatTransferViewController.h"
#import "ChatViewController.h"
#import "TabChatVC.h"
#import "UPPaymentControl.h"
#import "OffLineWebViewController.h"
#import "XSShoppingWebViewController.h"


@interface OrderPayViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,PayDelegate,PayMethodDelegate,UITextFieldDelegate,WXApiManagerDelegate>
@property (nonatomic, strong) NSMutableArray *allConfigurationArray;//钱包支付规则
@property (nonatomic, strong) NSMutableArray *walletNameArray;//钱包名称数组
@property (nonatomic, strong) NSString *payName;//支付订单对应的表名
@property (nonatomic, strong) NSString *payId;//支付订单id
@property (nonatomic, strong) NSMutableArray *payMethodArray;//支持的在线支付
@property (nonatomic, strong) NSMutableDictionary *selectedPayMethodDictionary;//选择的支付方式
@property (nonatomic, strong) UIView *bottomView;//底部view,含合计，支付按钮
@property (nonatomic, strong) XSCustomButton *payButton;//支付按钮
@property (nonatomic, copy) NSString *totalMoney;//总价
@property (nonatomic, strong) NSMutableArray *editPayCountArray;//可编辑的输入cell数组
@property (nonatomic, strong) NSDictionary *payList;//支付方式字典
@property (nonatomic, strong) NSString *payType;//在线支付方式
@property (nonatomic, copy) NSString *payPassWord;//支付密码
@property (nonatomic, strong) FormsVC *orderFormController;//订单控制器
@property (nonatomic, copy) NSString *tradeNumber;//第三方支付时返回的订单编号
@property (nonatomic, assign) BOOL isSelectedWallet;
@property (nonatomic, strong) NSMutableArray *availableBalance;//可用余额
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, copy) NSString *online_only;//钱包和第三方是否可以混用
@end

@implementation OrderPayViewController

#pragma mark - lazy loadding
-(NSMutableArray *)availableBalance {
    if (!_availableBalance) {
        _availableBalance = [NSMutableArray array];
    }
    return _availableBalance;
}
- (NSMutableArray *)walletNameArray {
    if (!_walletNameArray) {
        _walletNameArray = [NSMutableArray array];
    }
    return _walletNameArray;
}

- (NSDictionary *)payList {
    if (!_payList) {
        _payList = [NSDictionary dictionary];
    }
    return _payList;
}

- (NSMutableDictionary *)selectedPayMethodDictionary {
    if (!_selectedPayMethodDictionary) {
        _selectedPayMethodDictionary = [NSMutableDictionary dictionary];
    }
    return _selectedPayMethodDictionary;
}

- (NSMutableArray *)allConfigurationArray {
    if (!_allConfigurationArray) {
        _allConfigurationArray = [NSMutableArray array];
    }
    return _allConfigurationArray;
}

- (NSMutableArray *)payMethodArray {
    if (!_payMethodArray) {
        _payMethodArray = [NSMutableArray array];
    }
    return _payMethodArray;
}

- (NSMutableArray *)editPayCountArray {
    if (!_editPayCountArray) {
        _editPayCountArray = [NSMutableArray array];
    }
    return _editPayCountArray;
}

-(UILabel *)totalLabel {
    if (!_totalLabel) {
        _totalLabel = [[UILabel alloc] init];
        _totalLabel.font = [UIFont systemFontOfSize:17];
        _totalLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _totalLabel;
}

#pragma mark - life circle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isSelectedWallet = NO;
    [self getUserInformation];
    
}

-(void)setIsRecharge:(BOOL)isRecharge {
    _isRecharge = isRecharge;
    if (_isRecharge) {
        [self getPayInformation];
    }
    
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择支付方式";
    self.returnType = ReturnTypeNone;
    self.view.backgroundColor = BG_COLOR;
    self.autoHideKeyboard = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    //    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
    //        self.edgesForExtendedLayout = UIRectEdgeNone;
    //    }
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [self.view endEditing:YES];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定要放弃支付吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            if (self.isSupply) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
            //待支付界面
            UIViewController *tempController;
            self.returnType = ReturnTypeNone;
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[FormsVC class]]) {
                    tempController = (FormsVC *) controller;
                    self.returnType = ReturnTypeOrder;
                }
                if ([controller isKindOfClass:[S_StoreInformation class]]) {
                    self.returnType = ReturnTypeStore;
                    tempController = (S_StoreInformation *) controller;
                }
#ifdef ALIYM_AVALABLE
                if ([controller isKindOfClass:[YWChatViewController class]]) {
                    self.returnType = ReturnTypeChat;
                    tempController = (YWChatViewController *) controller;
                }
#elif defined EMIM_AVALABLE
                if ([controller isKindOfClass:[ChatViewController class]]) {
                    self.returnType = ReturnTypeChat;
                    tempController = (ChatViewController *) controller;
                }
#else
                if ([controller isKindOfClass:NSClassFromString(@"BaseChatController")]) {
                    self.returnType = ReturnTypeChat;
                    tempController = (BaseChatController *) controller;
                }
#endif
                
                if ([controller isKindOfClass:[TabChatVC class]]) {
                    self.returnType = ReturnTypeSupply;
                    tempController = (TabChatVC *) controller;
                    
                }
                if ([controller isKindOfClass:[OffLineWebViewController class]]||[controller isKindOfClass:[XSShoppingWebViewController class]]||[controller isKindOfClass:NSClassFromString(@"UserQuickPayViewController")]) {
                    self.returnType = ReturnTypeNormal;
                }
            }
            @strongify(self);
            if (self.returnType==ReturnTypeNone) {
                self.orderFormController = [[FormsVC alloc] initWithFormType:FormTypeWaitPay];
                [self.navigationController pushViewController:self.orderFormController animated:YES];
            } else if(self.returnType==ReturnTypeOrder) {
                //                ((FormsVC *)tempController).formType = FormTypeWaitPay;
                [self.navigationController popToViewController:tempController animated:YES];
            }else if(self.returnType==ReturnTypeNormal) {
                //从网页交互返回
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self.navigationController popToViewController:tempController animated:YES];
            }
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }];
    [self setSubviews];
    //获取支付信息
    [self getPayInformation];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)getPayInformation {
    if (self.tableName) {
        //快速买单
        [self prepareForPayRule:self.orderId tableName:self.tableName];
    } else {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getDetailOrderInfoWithId:self.orderId supply:@"0" success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                BuyerOrderParser *buyerparser = [BuyerOrderParser mj_objectWithKeyValues:dic[@"data"]];
                [self prepareForPayRule:self.orderId tableName:buyerparser.tname];
                
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
}

- (void)getUserInformation {
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord = parser.pay_password;
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)popToLastViewController {
    //查看支付状态
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager lookPayResultStatusWithTradeNumber:self.tradeNumber success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"-----%@",dic);
        if (state.status) {
            //            [XSTool showToastWithView:self.view Text:@"支付成功！"];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        [self performSelector:@selector(popViewController) withObject:self afterDelay:0.3];
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)payAction:(NSDictionary *)dictionary type:(NSString *)type {
    if (self.tradeNumber) {
        if ([type isEqualToString:@"UnionPayApp"]) {
            [self doUPPay:dictionary];
        } else if ([type isEqualToString:@"WeiXinApp"]) {
            [self weChatPay:dictionary];
        } else if ([type isEqualToString:@"ZhiFuBaoApp"]) {
            [self doAlipayPay:dictionary];
        }
        
    } else {
        self.tradeNumber = dictionary[@"out_trade_no"];
        NSLog(@"%@",self.tradeNumber);
        if ([type isEqualToString:@"UnionPayApp"]) {
            [self doUPPay:dictionary];
        } else if ([type isEqualToString:@"WeiXinApp"]) {
            [self weChatPay:dictionary];
        } else if ([type isEqualToString:@"ZhiFuBaoApp"]) {
            [self doAlipayPay:dictionary];
        }
        
    }
}

- (void)weChatPay:(NSDictionary *)dictionary {
    if (!([WXApi isWXAppInstalled]&&[WXApi isWXAppSupportApi])) {
        [XSTool showToastWithView:self.view Text:@"尚未安装微信客户端"];
        return;
    }
    payRequestModel *reqModel = [payRequestModel mj_objectWithKeyValues:dictionary];
    
    PayReq *request = [[PayReq alloc] init];
    request.openID = reqModel.openID;
    request.partnerId = reqModel.partnerId;
    request.prepayId = reqModel.prepayId;
    request.nonceStr = reqModel.nonceStr;
    request.timeStamp = reqModel.timeStamp;
    request.package = reqModel.package;
    request.sign = reqModel.sign;
    [WXApi sendReq:request];
}

- (void)doAlipayPay:(NSDictionary *)dictionary {
    [[AlipaySDK defaultService]payOrder:dictionary[@"pay_str"] fromScheme:AliPayScheme callback:^(NSDictionary *resultDic) {
        NSString *title=[NSString stringWithFormat:@"%@",resultDic[@"resultStatus"]];
        
        if ([title isEqualToString:@"9000"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
            return;
        }else if ([title isEqualToString:@"6001"]){
            title = @"您中途取消了订单";
        }else if ([title isEqualToString:@"4000"]){
            title = @"您的订单支付失败";
        }else if ([title isEqualToString:@"6002"]){
            title = @"网络连接出错";
        }else{
            title = @"未知错误";
        }
        [XSTool showToastWithView:self.view Text:title];
    }];
}

- (void)doUPPay:(NSDictionary *)dictionary {
    //当获得的tn不为空时，调用支付接口
    NSString *tn = dictionary[@"tn"];
    if (tn != nil && tn.length > 0)
    {
        [[UPPaymentControl defaultControl]
         startPay:tn
         fromScheme:@"xsyUPPay"
         mode:@"00"
         viewController:self];
    }
    
}

- (void)prepareForPayRule:(NSString *) orderId  tableName:(NSString *) tableName {
    [HTTPManager getPayInfoWithIds:orderId tname:tableName success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        PayParser *parser = [PayParser mj_objectWithKeyValues:dic];
        if (state.status) {
            self.payName = parser.data.tname;
            self.payId = parser.data.ids;
            [self.allConfigurationArray removeAllObjects];
            [self.selectedPayMethodDictionary removeAllObjects];
            [self.allConfigurationArray addObjectsFromArray:parser.data.conf_all];
            for (PayAllConfigurationParser *allParser in self.allConfigurationArray) {
                for (PayConfigDetailParser *detailParser in allParser.conf_detail) {
                    [self.walletNameArray addObject:detailParser.wallet_en];
                }
            }
            self.online_only = parser.data.online_only;
            self.totalMoney = [NSString stringWithFormat:@"%.2f",[parser.data.number_all doubleValue]];
            NSString *totalString = [NSString stringWithFormat:@"合计:¥%.2f",[self.totalMoney doubleValue]];
            NSMutableAttributedString *totalAttributedString = [[NSMutableAttributedString alloc] initWithString:totalString];
            [totalAttributedString addAttribute:NSForegroundColorAttributeName
                                          value:[UIColor redColor]
                                          range:NSMakeRange(3, totalString.length-3)];
            [totalAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:22] range:NSMakeRange(3, totalString.length-3)];
            self.totalLabel.attributedText = totalAttributedString;
            
            //支付方式
            [self.payMethodArray removeAllObjects];
            [self.payMethodArray addObjectsFromArray:parser.data.pay_method];
            
            [self.view addSubview:self.tableView];
            
            [self.tableView reloadData];
            if (self.allConfigurationArray.count||self.payMethodArray.count) {
                [self.bottomView addSubview:self.payButton];
            }
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)setSubviews {
    //提示
    UILabel *tintTitle = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, mainWidth-NORMOL_SPACE*4, 200)];
    tintTitle.numberOfLines = 8;
    UIFont * tfont = [UIFont systemFontOfSize:13];
    tintTitle.font = tfont;
    tintTitle.lineBreakMode =NSLineBreakByTruncatingTail ;
    NSString *str = @"【温馨提示】为保障货款安全，担保交易服务默认为七天后自动收货，七天内未收到货物请手动申请延时收货，确认收货后卖家将收到订单货数，务必关注物流状况，谨防欺诈。";
    tintTitle.text = str;
    CGSize size = CGSizeMake(mainWidth-NORMOL_SPACE*4,400);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:tfont,NSFontAttributeName,nil];
    CGSize  actualsize =[str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    tintTitle.frame = CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, mainWidth-NORMOL_SPACE*4, actualsize.height+NORMOL_SPACE) ;
    UIImageView *backImage = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE, mainWidth-NORMOL_SPACE*2, actualsize.height+NORMOL_SPACE)];
    backImage.image = [UIImage imageNamed:@"user_order_back_line"];
    [self.view addSubview:backImage];
    [self.view addSubview:tintTitle];
    CGFloat bottomSpaceHeight = 70;//下边距
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(CGRectGetMaxY(tintTitle.frame)+NORMOL_SPACE, 0, bottomSpaceHeight+kTabbarSafeBottomMargin, 0));
    }];
    self.tableView.bounces = NO;
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = BG_COLOR;
    [self.view addSubview: self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(bottomSpaceHeight);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-kTabbarSafeBottomMargin);
    }];
    [self.view layoutIfNeeded];
    
    //合计
    self.totalLabel.frame = CGRectMake(NORMOL_SPACE, 0, mainWidth/2, CGRectGetHeight( self.bottomView.frame));
    [self.bottomView addSubview:self.totalLabel];
    CGFloat cellHeight = 80;
    self.payButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(mainWidth-cellHeight*3/2-NORMOL_SPACE, NORMOL_SPACE*2, cellHeight*3/2, 40) title:@"去支付" titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [self.payButton setBorderWith:0 borderColor:nil cornerRadius:5];
    [self.payButton addTarget:self action:@selector(clickToPay:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)editCellIndexPathArrayWithSave:(BOOL) isSave walletName:(NSString *) walletName ruleId:(NSString *) ruleId {
    for (int j=0;j<self.allConfigurationArray.count;j++) {
        PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[j];
        //同一钱包
        if ([ruleId integerValue]==[allConfigurationParser.rule_id integerValue]) {
            for (int i=0;i<allConfigurationParser.conf_detail.count;i++) {
                PayConfigDetailParser *confDetailParser = allConfigurationParser.conf_detail[i];
                if ([confDetailParser.wallet_en isEqualToString:walletName]) {
                    NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:j];
                    if (isSave==1) {
                        if ([confDetailParser.is_edit integerValue]==1) {
                            if (![self.editPayCountArray containsObject:index]) {
                                [self.editPayCountArray addObject:index];
                                NSLog(@"add:j:%d;i:%d,save:%d",j,i,isSave);
                            }
                        }
                    }
                    if (isSave==0) {
                        [self.editPayCountArray removeObject:index];
                        [self.selectedPayMethodDictionary removeObjectForKey:[NSString stringWithFormat:@"wallet_%@_luru_%@",confDetailParser.wallet_en,allConfigurationParser.rule_id]];
                        NSLog(@"remove::j:%d;i:%d,save:%d",j,i,isSave);
                    }
                }
            }
        }
    }
}

- (void)clickToPay:(UIButton *) sender {
    //1.判断是否选择支付方式
    //2.是否编辑了
    //3.余额是否充足
    //4.是否设置了支付密码
    if (self.selectedPayMethodDictionary.allKeys.count) {
        
        //必选
        for (int j=0;j<self.allConfigurationArray.count;j++) {
            PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[j];
            for (int i=0;i<allConfigurationParser.conf_detail.count;i++) {
                PayConfigDetailParser *confDetailParser = allConfigurationParser.conf_detail[i];
                if ([confDetailParser.status integerValue]==0) {
                    [self.selectedPayMethodDictionary setObject:confDetailParser.wallet_en forKey:[NSString stringWithFormat:@"wallet_%@_check_%@",confDetailParser.wallet_en,allConfigurationParser.rule_id]];
                    [self editCellIndexPathArrayWithSave:YES walletName:confDetailParser.wallet_en ruleId:[NSString stringWithFormat:@"%@",allConfigurationParser.rule_id]];
                    break;
                }
            }
        }
        BOOL isToPay = YES,isEnough = YES;
        CGFloat sumMoney = 0,enableMoney = 0;
        NSDictionary *submitDictionary = self.selectedPayMethodDictionary;
        if (submitDictionary.allKeys.count) {
            for (NSIndexPath *index in self.editPayCountArray) {
                PayTableViewCell *cell = (PayTableViewCell *)[self.tableView cellForRowAtIndexPath:index];
                PayConfigDetailParser *confDetailParser = cell.detailParser;
                if (cell.payCount.text.length==0||[cell.payCount.text isEqualToString:@""]) {
                    [XSTool showToastWithView:self.view Text:@"请输入支付金额！"];
                    return;
                } else {
                    //去除空格
                    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                    NSString *moneyString = [[NSString alloc]initWithString:[cell.payCount.text stringByTrimmingCharactersInSet:whiteSpace]];
                    CGFloat money =  [moneyString doubleValue]/[confDetailParser.ratio doubleValue];
                    NSString *truemoney = [NSString stringWithFormat:@"%.2f",floor(money*100)/100];
                    PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[index.section];
                    [self.selectedPayMethodDictionary setValue:truemoney forKey:[NSString stringWithFormat:@"wallet_%@_luru_%@",confDetailParser.wallet_en,allConfigurationParser.rule_id]];
                    sumMoney+= [truemoney doubleValue];
                    enableMoney+= [truemoney doubleValue];
                    
                }
            }
            for (NSString *balance in self.availableBalance) {
                for (PayAllConfigurationParser *confAllParser in self.allConfigurationArray) {
                    for (PayConfigDetailParser *parser in confAllParser.conf_detail) {
                        if (([parser.is_edit integerValue]==0)&&[parser.balance isEqualToString:balance]) {
                            sumMoney+= [balance doubleValue];
                            NSLog(@"=====%f,%f,%f",sumMoney,[balance doubleValue],[self.totalMoney doubleValue]);
                            CGFloat tempMoney = [parser.limit_max doubleValue]>[balance doubleValue]?[balance doubleValue]:[parser.limit_max doubleValue];
                            enableMoney+= tempMoney;
                            
                        }
                        
                    }
                }
            }
            if (sumMoney<[self.totalMoney doubleValue]) {
                isToPay = NO;
                
            } else {
                if (enableMoney<[self.totalMoney doubleValue]) {
                    isEnough = NO;
                } else {
                    isToPay = YES;
                }
                
            }
            for (NSString *walletName in self.walletNameArray) {
                
                if ([submitDictionary.allValues containsObject:walletName]&&self.selectedPayMethodDictionary.count) {
                    self.isSelectedWallet = YES;
                    break;
                    
                } else {
                    self.isSelectedWallet = NO;
                }
            }
            BOOL truePay = YES;
            
            if (self.isSelectedWallet&&[self.selectedPayMethodDictionary.allKeys containsObject:@"pay_check"]) {
                if ([self.online_only integerValue]==1) {
                    truePay = NO;
                    [XSTool showToastWithView:self.view Text:@"钱包和第三方支付不可以同时选择！"];
                }
            }
            
            if (truePay) {
                if (isToPay) {
                    //总金额足够
                    
                    //可用余额不足
                    if (!isEnough) {
                        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:nil message:@"所选钱包的最多使用金额小于支付总金额！" preferredStyle:UIAlertControllerStyleAlert];
                        //                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        //
                        //
                        //                        }];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        //                        [alertControl addAction:cancelAction];
                        [alertControl addAction:okAction];
                        [self presentViewController:alertControl animated:YES completion:nil];
                    } else {
                        
                        // 可用余额足够
                        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popToLastViewController) name:PAY_SUCCESS_LIST object:nil];
                        // 输入密码
                        [self showPasswordToPay];
                    }
                    
                } else {
                    
                    if ([self.selectedPayMethodDictionary.allKeys containsObject:@"pay_check"]) {
                        //第三方支付返回结果处理
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToLastViewController) name:PAY_SUCCESS_LIST object:nil];
                        //支付
                        [self showPasswordToPay];
                    } else {
                        
                        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:nil message:@"所选钱包余额不足，是否要进行充值" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            
                            
                        }];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            //跳转到设置支付密码界面
                            RechargeViewController *rechargeViewController = [[RechargeViewController alloc] init];
                            [self.navigationController pushViewController:rechargeViewController animated:YES];
                            
                        }];
                        [alertControl addAction:cancelAction];
                        [alertControl addAction:okAction];
                        [self presentViewController:alertControl animated:YES completion:nil];
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    } else {
        [XSTool showToastWithView:self.view Text:@"请选择支付方式！"];
    }
    
}

-(void)showPasswordToPay{
    if (!self.isSelectedWallet) {
        if ([self.selectedPayMethodDictionary.allKeys containsObject:@"pay_pwd_set"]) {
            [self.selectedPayMethodDictionary removeObjectForKey:@"pay_pwd_set"];
        }
        //支付
        [self submitPayInformation];
    } else {
        if ([self.payPassWord isEqualToString:@""]) {
            //未设置支付密码
            //弹出警告框
            UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"您还没有设置支付密码，是否要设置支付密码？" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                //跳转到设置支付密码界面
                ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
                [self.navigationController pushViewController:oldPayViewController animated:YES];
                
            }];
            [alertControl addAction:cancelAction];
            [alertControl addAction:okAction];
            [self presentViewController:alertControl animated:YES completion:nil];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"input_pay_pwd") preferredStyle:UIAlertControllerStyleAlert];
            //添加按钮
            __weak typeof(alert) weakAlert = alert;
            [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                //支付
                NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
                if ([weakAlert.textFields.lastObject text].length==0||[[weakAlert.textFields.lastObject text] isEqualToString:@""]) {
                    [XSTool showToastWithView:self.view Text:@"请填写支付密码！"];
                } else {
                    
                    NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:[weakAlert.textFields.lastObject text]]];
                    while (encryptOldPassWord.length<3) {
                        encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:[NSString stringWithFormat:@"%@",[weakAlert.textFields.lastObject text]]]];
                    }
                    NSLog(@"encryptOldPassWord:::%@",encryptOldPassWord);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //支付
                        [self.selectedPayMethodDictionary setValue:encryptOldPassWord forKey:@"pay_pwd_set"];
                        [self submitPayInformation];
                    });
                }
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                
            }]];
            
            // 添加文本框
            [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.secureTextEntry = YES;
                textField.textColor = [UIColor blackColor];
            }];
            // 弹出对话框
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        //        }
        
    }
    
}

- (void)submitPayInformation {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager payMoneyWithPayname:self.payName payids:self.payId otherInfo:self.selectedPayMethodDictionary success:^(NSDictionary * _Nullable dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSDictionary *resultDictionary = dic[@"data"];
            BOOL isIncludeUrl = NO;
            if ([resultDictionary.allKeys containsObject:@"url"]) {
                isIncludeUrl = YES;
            }
            if (isIncludeUrl) {
                [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"成功支付：¥%.2f",[self.totalMoney doubleValue]]];
                [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
            } else {
                self.tradeNumber = dic[@"data"][@"out_trade_no"];
                NSLog(@"%@",self.tradeNumber);
                if (self.tradeNumber) {
                    
                    if ([resultDictionary.allKeys containsObject:@"pay_link"]) {
                        
                        //网页支付
                        NSString *payLink = [resultDictionary objectForKey:@"pay_link"];
                        XSBaseWKWebViewController *controller = [[XSBaseWKWebViewController alloc] init];
//                        controller.navigationItem.titl e = self.payType;
                        controller.urlStr = payLink;
                        controller.urlType = WKWebViewURLNormal;
                        [self.navigationController pushViewController:controller animated:YES];
                        
                    } else {
                        [self payAction:dic[@"data"] type:self.payType];
                    }
                    
                } else {
                    [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
                }
                
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)popViewController {
    
    if ([self.parentVC isKindOfClass:[RedPacketSendViewController class]]) {
        [((RedPacketSendViewController *)self.parentVC) canSendRedpacket];
        return;
    }
    if ([self.parentVC isKindOfClass:[ChatTransferViewController class]]) {
        [((ChatTransferViewController *)self.parentVC) canSendTransfer];
        return;
    }
    if (self.isSupply) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    self.returnType = ReturnTypeNone;
    UIViewController *tempController;
    //    NSMutableArray *temporaryArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        
        if ([controller isKindOfClass:[FormsVC class]]) {
            tempController = (FormsVC *) controller;
            self.returnType = ReturnTypeOrder;
        }
        if ([controller isKindOfClass:[S_StoreInformation class]]) {
            self.returnType = ReturnTypeStore;
            tempController = (S_StoreInformation *) controller;
        }
        if ([controller isKindOfClass:[TabChatVC class]]) {
            self.returnType = ReturnTypeChat;
            tempController = (TabChatVC *) controller;
        }
        if ([controller isKindOfClass:[OffLineWebViewController class]]||[controller isKindOfClass:[XSShoppingWebViewController class]]) {
            self.returnType = ReturnTypeNormal;
            if ([self.tableName isEqualToString:@"quick_order"]) {//快速买单
                
            }else if ([self.tableName isEqualToString:@"offline_orders"]) {//线下商家
                tempController = (OffLineWebViewController *) controller;
                self.returnType = ReturnTypeOffLine;
            }
            
        }
        if ([controller isKindOfClass:[XSShoppingWebViewController class]]) {
            self.returnType = ReturnTypeNormal;
        }
    }
    if (  self.returnType==ReturnTypeNone) {
        FormsVC *formcontroller = [[FormsVC alloc] initWithFormType:FormTypeWaitSend];
        if (self.orderLogisticstatus==OrderLogisticStatusRequiredeliver) {
            formcontroller.formType = FormTypeWaitSend;
        } else if (self.orderLogisticstatus==OrderLogisticStatusFree) {
            formcontroller.formType = FormTypeWaitEvaluate;
        }
        
        [self.navigationController pushViewController:formcontroller animated:YES];
    } else if(self.returnType==ReturnTypeOrder) {
        if (self.orderLogisticstatus==OrderLogisticStatusRequiredeliver) {
            ((FormsVC *)tempController).formType = FormTypeWaitSend;
        } else if (self.orderLogisticstatus==OrderLogisticStatusFree) {
            ((FormsVC *)tempController).formType = FormTypeWaitEvaluate;
        }
        
        [self.navigationController popToViewController:tempController animated:YES];
        
    } else if(self.returnType==ReturnTypeOffLine) {
        //从网页交互返回
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (guid.length <= 0) {
            guid = @"app";
        }
        NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/offline/coupons?device=%@",ImageBaseUrl,guid];
        ((OffLineWebViewController *)tempController).urlStr = urlStr;
        ((OffLineWebViewController *)tempController).urlType = WKWebViewURLOffLine;
        ((OffLineWebViewController *)tempController).navigationItem.title= Localized(@"我的兑换券");
        [self.navigationController popToViewController:tempController animated:YES];
    }else if(self.returnType==ReturnTypeNormal) {
        //从网页交互返回
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popToViewController:tempController animated:YES];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - PayDelegate
- (void)toPayWithMethodName:(NSString *)methodName ruleId:(NSString *)ruleId status:(NSString *)status balance:(NSString *)balance {
    if ([status integerValue]==1) {
        
        //单选
        for (int j=0;j<self.allConfigurationArray.count;j++) {
            PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[j];
            
            //同一个钱包
            if ([allConfigurationParser.rule_id integerValue]==[ruleId integerValue]) {
                
                //规则
                for (int i=0;i<allConfigurationParser.conf_detail.count;i++) {
                    PayConfigDetailParser *confDetailParser = allConfigurationParser.conf_detail[i];
                    if ([confDetailParser.status integerValue]==[status integerValue]&&![confDetailParser.wallet_en isEqualToString:methodName]) {
                        
                        //单选
                        NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:j];
                        PayTableViewCell *cell = (PayTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
                        if (cell.selectedMethodButton.selected) {
                            cell.selectedMethodButton.selected = !cell.selectedMethodButton.selected;
                            [self toCancelPayWithMethodName:methodName ruleId:ruleId status:status balance:balance];
                        }
                    }
                }
                break;
            }
        }
        [self.selectedPayMethodDictionary setObject:methodName forKey:[NSString stringWithFormat:@"wallet_check_%@",ruleId]];
    } else {
        
        //必选和多选
        [self.selectedPayMethodDictionary setObject:methodName forKey:[NSString stringWithFormat:@"wallet_%@_check_%@",methodName,ruleId]];
    }
    
    //选中后如果可编辑，存入可编辑数组
    [self editCellIndexPathArrayWithSave:YES walletName:methodName ruleId:ruleId];
    [self.availableBalance addObject:balance];
    [self.tableView reloadData];
}

- (void)toCancelPayWithMethodName:(NSString *)methodName ruleId:(NSString *)ruleId status:(NSString *)status balance:(NSString *)balance {
    
    //取消选择
    if ([status integerValue]==1) {
        [self.selectedPayMethodDictionary removeObjectForKey:[NSString stringWithFormat:@"wallet_check_%@",ruleId]];
    } else {
        [self.selectedPayMethodDictionary removeObjectForKey:[NSString stringWithFormat:@"wallet_%@_check_%@",methodName,ruleId]];
    }
    
    //取消选中后如果可编辑，从可编辑数组删除
    [self editCellIndexPathArrayWithSave:NO walletName:methodName ruleId:ruleId];
    [self.availableBalance removeObject:balance];
    [self.tableView reloadData];
}

- (void)toPayWithMethodInputInfo:(NSString *)inputInfo methodName:(NSString *)methodName ruleId:(NSString *)ruleId{
    
    NSLog(@"%@==%@==%@",inputInfo,methodName,ruleId);
}

#pragma mark - PayMethodDelegate
- (void)selectedPayMethod:(NSString *)method {
    NSLog(@"%@",method);
    [self.selectedPayMethodDictionary setValue:method forKey:@"pay_check"];
    self.payType = method;
    NSInteger index = 0;
    for (PayMethodParser *parser in self.payMethodArray) {
        if ([parser.key isEqualToString:method]) {
            index = [self.payMethodArray indexOfObject:parser];
            break;
        }
    }
    [self.tableView reloadData];
}

- (void)unSelectedPayMethod:(NSString *)method {
    self.payType = nil;
    [self.selectedPayMethodDictionary removeObjectForKey:@"pay_check"];
    [self.tableView reloadData];
}

#pragma mark - tableView Delegate
- (CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 80;
    if (indexPath.section<self.allConfigurationArray.count) {
        return cellHeight;
    } else {
        return   60;
    }
}

- (CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return NORMOL_SPACE*3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section<self.allConfigurationArray.count) {
        NSString *string = @"payCell";
        PayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[PayTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:string];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[indexPath.section];
        PayConfigDetailParser *detailParser = allConfigurationParser.conf_detail[indexPath.row];
        cell.payCount.delegate = self;
        cell.payDelegate = self;
        cell.detailParser = detailParser;
        cell.ruleId = [NSString stringWithFormat:@"%lu",(long)[allConfigurationParser.rule_id integerValue]];
        [cell.payCount addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
       
        if ([detailParser.status integerValue]==0) {
            //必选
            [self.selectedPayMethodDictionary setObject:detailParser.wallet_en forKey:[NSString stringWithFormat:@"wallet_%@_check_%@",detailParser.wallet_en,cell.ruleId]];
            [self editCellIndexPathArrayWithSave:YES walletName:detailParser.wallet_en ruleId:cell.ruleId];
            [self.availableBalance addObject: detailParser.balance];
        }
        if ([detailParser.status integerValue]==1) {
            if ([self.selectedPayMethodDictionary.allKeys containsObject:[NSString stringWithFormat:@"wallet_check_%@",cell.ruleId]]) {
                NSString *valueName = [self.selectedPayMethodDictionary objectForKey:[NSString stringWithFormat:@"wallet_check_%@",cell.ruleId]];
                if ([valueName isEqualToString: detailParser.wallet_en]) {
                  cell.selectedMethodButton.selected = YES;
                } else {
                    cell.selectedMethodButton.selected = NO;
                }
            } else {
                cell.selectedMethodButton.selected = NO;
            }
        } else {
            if ([self.selectedPayMethodDictionary.allKeys containsObject:[NSString stringWithFormat:@"wallet_%@_check_%@",detailParser.wallet_en,cell.ruleId]]) {
                cell.selectedMethodButton.selected = YES;
                if ([detailParser.status integerValue]==0) {
                    cell.selectedMethodButton.userInteractionEnabled = NO;
                }
            } else {
                cell.selectedMethodButton.selected = NO;
            }
        }
       
        return cell;
    } else {
        NSString *string = @"nCell";
        PayMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
        if (!cell) {
            cell = [[PayMethodTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (self.payMethodArray.count) {
            cell.payParser = self.payMethodArray[indexPath.row];
        }
        
        cell.methodDelegate = self;
        PayMethodParser *parser = self.payMethodArray[indexPath.row];
        if ([self.selectedPayMethodDictionary.allValues containsObject: parser.key]) {
            cell.methodButton.selected = YES;
        } else {
            cell.methodButton.selected = NO;
        }
        return cell;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.payMethodArray.count>0) {
        return self.allConfigurationArray.count+1;
    } else {
        return self.allConfigurationArray.count;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section>self.allConfigurationArray.count-1) {
        return self.payMethodArray.count;
    } else {
        PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[section];
        return allConfigurationParser.conf_detail.count;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.textColor = COLOR_666666;
    leftLabel.adjustsFontSizeToFitWidth = YES;
    [headerView addSubview:leftLabel];
    
    [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_lessThanOrEqualTo((mainWidth-2*10-5)/3*2);
        make.top.bottom.mas_equalTo(headerView);
    }];
    
    if (section>self.allConfigurationArray.count-1) {
        leftLabel.text = @"在线支付";
    } else {
        UILabel *rightLabel = [[UILabel alloc] init];
        rightLabel.textColor = [UIColor blackColor];
        rightLabel.adjustsFontSizeToFitWidth = YES;
        rightLabel.textAlignment = NSTextAlignmentRight;
        rightLabel.font = [UIFont qsh_systemFontOfSize:16];
        [headerView addSubview:rightLabel];
        
        [rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftLabel.mas_right).with.mas_offset(5);
            make.right.mas_equalTo(headerView).with.mas_offset(-10);
            make.top.bottom.mas_equalTo(headerView);
        }];
        
        PayAllConfigurationParser *allConfigurationParser = self.allConfigurationArray[section];
        leftLabel.text = allConfigurationParser.title;
        
        rightLabel.text = [NSString stringWithFormat:@"¥%.2f",[allConfigurationParser.number floatValue]];
    }
    return headerView;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    PayTableViewCell *cell = (PayTableViewCell *) [[textField superview] superview];
    cell.payCount.text = textField.text;
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [textField resignFirstResponder];
}
- (void)textFieldDidChange:(UITextField *)textField {
    NSMutableString *tempString = [NSMutableString stringWithString:textField.text];
    if ([textField.text isEqualToString:@"."]) {
        [tempString deleteCharactersInRange:NSMakeRange(tempString.length-1, 1)];
        textField.text = tempString;
    } else {
        PayTableViewCell *cell = (PayTableViewCell *)textField.superview.superview;
        CGFloat maxInputMoney = [cell.detailParser.limit_max doubleValue]>[cell.detailParser.balance doubleValue]?[cell.detailParser.balance doubleValue]:[cell.detailParser.limit_max doubleValue];
        if ([textField.text doubleValue]>maxInputMoney) {
            [tempString deleteCharactersInRange:NSMakeRange(tempString.length-1, 1)];
            textField.text = tempString;
            NSLog(@"%f,%f",maxInputMoney,[cell.detailParser.limit_max doubleValue]);
            if (maxInputMoney<[cell.detailParser.limit_max doubleValue]) {
                [XSTool showToastWithView:self.view Text:@"当前余额不足!"];
            } else {
                [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"最少使用：%.2f,最多使用：%.2f",[cell.detailParser.limit_min doubleValue],[cell.detailParser.limit_max doubleValue]]];
            }
        }
        
    }
}
@end
