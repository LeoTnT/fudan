//
//  S_BankCell.m
//  App3.0
//
//  Created by apple on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_BankCell.h"


@implementation S_BankModel



@end

@implementation S_BankCell



+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *ID = @"identifier";
    S_BankCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        
        cell = [[S_BankCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
//        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    
    _selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-40, 12, 20, 20)];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
    [self addSubview:_selectBtn];
    
}

- (void)setContentWithModel:(S_BankModel *)model {
//    BOOL selectStatus = model.selectStatus;
//    if (selectStatus == YES) {
//        _selectBtn.selected = YES;
//    } else {
//        _selectBtn.selected = NO;
//    }
}

@end
