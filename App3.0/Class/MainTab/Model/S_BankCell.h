//
//  S_BankCell.h
//  App3.0
//
//  Created by apple on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface S_BankModel : NSObject

@property (nonatomic,assign) BOOL selectStatus;

@end


@interface S_BankCell : UITableViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic,retain) UIButton *selectBtn;

- (void)setContentWithModel:(S_BankModel *)model;

@end


