//
//  DefaultView.h
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,DefaultViewType) {
    DefaultViewTypeNormal,
    DefaultViewTypeForOrderList,
    DefaultViewTypeForAddressList,
};

@protocol DefaultViewDelegate <NSObject>
/**进入商城*/
@optional
- (void)buttonClicktoMall;
@end
@interface DefaultView : UIView

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIButton *button;
@property(nonatomic,strong) UIButton *baseButton;

@property (nonatomic, assign) DefaultViewType defaultViewType;

@property(nonatomic,weak) id<DefaultViewDelegate>defaultDelegete;


@end
