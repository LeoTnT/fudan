//
//  EmptyView.h
//  App3.0
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, EmptyViewStyle) {
    EmptyViewStyleChat = 1,
    EmptyViewStyleContact,
    EmptyViewStyleGroup
};

@interface EmptyView : UIView
- (instancetype)initWithStyle:(EmptyViewStyle)style frame:(CGRect)frame;
@end
