//
//  DefaultView.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DefaultView.h"


@implementation DefaultView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubViews];
    }
    return self;
    
}
-(void)setDefaultViewType:(DefaultViewType)defaultViewType {
    _defaultViewType = defaultViewType;
    CGFloat baseSpace = 20;
    if (_defaultViewType==DefaultViewTypeForOrderList) {
        self.imageView.frame = CGRectMake(CGRectGetWidth(self.frame)/2-72/2, 141, 72, 78);
        self.imageView.image = [UIImage imageNamed:@"order_default"];
        
        self.titleLabel.frame = CGRectMake(baseSpace, CGRectGetMaxY(self.imageView.frame)+17.5, CGRectGetWidth(self.frame)-2*baseSpace, baseSpace);
        self.titleLabel.textColor = COLOR_999999;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        
        self.button.frame =CGRectMake(mainWidth/2-122/2, 24.5+CGRectGetMaxY(self.titleLabel.frame), 122, 36);
        self.button.backgroundColor = mainColor;
        self.button.layer.cornerRadius = 5;
        self.button.layer.borderColor = [mainColor CGColor];
        self.button.layer.masksToBounds = YES;
        self.button.titleLabel.font = [UIFont systemFontOfSize:14];
       
        [self.button setTitle:Localized(@"去逛逛") forState:UIControlStateNormal];
        
        [self.button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    } else  if (_defaultViewType ==DefaultViewTypeForAddressList) {
        self.backgroundColor = BG_COLOR;
        self.imageView.frame = CGRectMake(CGRectGetWidth(self.frame)/2-61/2, 286-64, 61, 78);
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.image = [UIImage imageNamed:@"address_default"];
        
        self.titleLabel.frame = CGRectMake(baseSpace, CGRectGetMaxY(self.imageView.frame)+18.5, CGRectGetWidth(self.frame)-2*baseSpace, baseSpace);
        self.titleLabel.textColor = COLOR_999999;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.text = Localized(@"您还没有添加任何收货地址");
        self.baseButton.hidden = YES;
        self.button.hidden = YES;
        
    } else {
        //偏移
        [self.button setImage:[UIImage imageNamed:@"user_cart_gotobuy"] forState:UIControlStateNormal];
        [self.button setTitle:Localized(@"去逛逛") forState:UIControlStateNormal];
        [self.button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        CGSize imageSize = self.button.imageView.frame.size;
        CGSize titleSize = self.button.titleLabel.frame.size;
        [self.button setTitleEdgeInsets:UIEdgeInsetsMake(0, titleSize.width-baseSpace, 0, 0)];
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(0, -imageSize.width/2, 0, 0)];
    }
    
}
- (void)addSubViews{
    CGFloat topSpace = 80;
    CGFloat baseSpace = 20;
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.frame)/2-80, topSpace, 160, 160)];
    self.imageView.image = [UIImage imageNamed:@"empty_logo"];
    [self addSubview:self.imageView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(baseSpace, CGRectGetMaxY(self.imageView.frame)+17.5, CGRectGetWidth(self.frame)-2*baseSpace, baseSpace)];
    self.titleLabel.textColor = COLOR_999999;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = Localized(@"您还没有收藏过商品哦");
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.titleLabel];
    
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(2*baseSpace, baseSpace+CGRectGetMaxY(self.titleLabel.frame), CGRectGetWidth(self.frame)-4*baseSpace, baseSpace*2)];
    self.button.backgroundColor = mainColor;
    self.button.layer.cornerRadius = 5;
    self.button.layer.borderColor = [mainColor CGColor];
    self.button.layer.masksToBounds = YES;
    self.button.titleLabel.font = [UIFont systemFontOfSize:17];
    self.button.titleLabel.textColor = [UIColor whiteColor];
  
    [self addSubview:self.button];
    
    self.baseButton = [[UIButton alloc] initWithFrame:CGRectMake(2*baseSpace, baseSpace+CGRectGetMaxY(self.titleLabel.frame), CGRectGetWidth(self.frame)-4*baseSpace, baseSpace*2)];
    self.baseButton.backgroundColor = mainColor;
    self.baseButton.layer.cornerRadius = 5;
    self.baseButton.layer.borderColor = [mainColor CGColor];
    self.baseButton.layer.masksToBounds = YES;
    self.baseButton.titleLabel.font = [UIFont systemFontOfSize:17];
    self.baseButton.hidden = YES;
    [self addSubview:self.baseButton];
}

- (void)buttonClick:(UIButton *) sender {
    sender.backgroundColor = HighLightColor_Main;
    if (self.defaultDelegete&&[self.defaultDelegete respondsToSelector:@selector(buttonClicktoMall)]) {
        [self.defaultDelegete buttonClicktoMall];
    }
}
@end
