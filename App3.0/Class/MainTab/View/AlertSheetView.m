//
//  AlertSheetView.m
//  App3.0
//
//  Created by nilin on 2017/4/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AlertSheetView.h"
#import "XSCustomButton.h"
#define btnHeight 50
@implementation AlertSheetView

+(instancetype)AlertSheetViewWithBtnTitleArray:(NSArray *) titleArray andBtnCount:(NSUInteger ) btnCount{
    AlertSheetView *view = [[AlertSheetView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    UIColor *bColor = [UIColor blackColor];
    view.backgroundColor = [bColor colorWithAlphaComponent:0.5];

    CGFloat beginY = mainHeight-btnHeight*btnCount-NORMOL_SPACE*2-1*(btnCount-2);
    CGFloat width = mainWidth-NORMOL_SPACE*2;
    for (int i=0; i<btnCount; i++) {
        @autoreleasepool {
            //取消
            XSCustomButton *sender = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, beginY+i*(btnHeight+1), width, btnHeight) title:titleArray[i] titleColor:mainColor fontSize:20 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor highBackgroundColor:[UIColor whiteColor] ];
            [sender setBorderWith:0 borderColor:nil cornerRadius:5];
            sender.tag = 1000+i;
            [sender addTarget:self action:@selector(senderAction:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:sender];
            if (i==btnCount-2) {
                
            }else if (i==btnCount-1) {
                sender = [[XSCustomButton alloc] initWithFrame:CGRectMake(NORMOL_SPACE, beginY+i*(btnHeight+1), width, btnHeight) title:titleArray[i] titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:mainColor ]; ;
            }else{
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE+4, CGRectGetMaxY(sender.frame), mainWidth-2*NORMOL_SPACE-8, 1)];
                label.backgroundColor = LINE_COLOR;
                [view addSubview:label];
            }
        }
    }
    return view;
}

-(void)senderAction:(UIButton *) sender{

}
-(void)setBtnCountArray:(NSArray *)btnCountArray{
    _btnCountArray = btnCountArray;
    for (int i=0; i<_btnCountArray.count; i++) {
        
    }
    
}
@end
