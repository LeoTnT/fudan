//
//  EmptyView.m
//  App3.0
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "EmptyView.h"

@implementation EmptyView
- (instancetype)initWithStyle:(EmptyViewStyle)style frame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = NO;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        UIImageView *imgView = [[UIImageView alloc] init];
        [self addSubview:imgView];
        
        UILabel *label = [[UILabel alloc] init];
        label.textColor = mainGrayColor;
        label.font = [UIFont systemFontOfSize:16];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        
        switch (style) {
            case EmptyViewStyleChat:
            {
                [imgView setImage:[UIImage imageNamed:@"empty_chat"]];
                label.text = Localized(@"暂时没有新消息");
            }
                break;
            case EmptyViewStyleContact:
            {
                [imgView setImage:[UIImage imageNamed:@"empty_firends"]];
                label.text = Localized(@"您还没有此类型联系人");
            }
                break;
            case EmptyViewStyleGroup:
            {
                [imgView setImage:[UIImage imageNamed:@"empty_group"]];
                label.text = Localized(@"您尚未加入任何群组");
            }
                break;
            default:
                break;
        }
        
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(imgView.mas_bottom).offset(18);
        }];
    }
    return self;
}
@end
