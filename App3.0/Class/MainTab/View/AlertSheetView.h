//
//  AlertSheetView.h
//  App3.0
//
//  Created by nilin on 2017/4/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertSheetDelegate <NSObject>
@optional
-(void) btnAction:(UIButton *) sender;

@end

@interface AlertSheetView : UIView


@property(nonatomic,strong) NSArray *btnCountArray;


+(instancetype)AlertSheetViewWithBtnTitleArray:(NSArray *) titleArray andBtnCount:(NSUInteger ) btnCount;


@end
