//
//  AddFriendsViewController.m
//  App3.0
//
//  Created by mac on 17/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddFriendsViewController.h"
#import "SearchVC.h"
#import "AreaButton.h"
#import "FriendCheckVC.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "XSShareView.h"
#import "ScanViewController.h"
#import "UserQRViewController.h"
#import "LBXScanViewStyle.h"

@interface AddFriendsViewController ()<WXApiDelegate, QQApiInterfaceDelegate>
@property(nonatomic, strong)UIButton *searchBtn;
@property (nonatomic, strong) XSShareModel *shareModel;
@end

@implementation AddFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = Localized(@"添加好友");
    
    [self.view addSubview:self.searchBtn];
    
    self.tableViewStyle = UITableViewStyleGrouped;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setSubViews];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData {
    NSDictionary *dic = @{@"type":@"app",
                          @"target":@"ios"};
    [HTTPManager getShareInfo:dic Succrss:^(NSDictionary *dic, resultObject *state) {
        self.shareModel = [XSShareModel mj_objectWithKeyValues:dic[@"data"]];
        NSLog(@"shareModel   %@",self.shareModel);
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
    
}

- (void)setSubViews {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 80)];
    headerView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headerView;
    
    UIView *qrBgView = [UIView new];
    [headerView addSubview:qrBgView];
    [qrBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(headerView);
        make.width.mas_equalTo(mainWidth/2);
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        //我的二维码
        UserQRViewController *userQRVC = [[UserQRViewController alloc] init];
        [self.navigationController pushViewController:userQRVC animated:YES];
    }];
    [qrBgView addGestureRecognizer:tap];
    
    UIButton *qrBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    qrBtn.userInteractionEnabled = NO;
    [qrBtn setTitle:Localized(@"person_info_qr") forState:UIControlStateNormal];
    [qrBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    qrBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [qrBtn setImage:[UIImage imageNamed:@"contact_qr"] forState:UIControlStateNormal];
    [qrBtn setImageEdgeInsets:UIEdgeInsetsMake(-qrBtn.titleLabel.intrinsicContentSize.height-3, 0, 0, -qrBtn.titleLabel.intrinsicContentSize.width)];
    [qrBtn setTitleEdgeInsets:UIEdgeInsetsMake(qrBtn.currentImage.size.height+3, -qrBtn.currentImage.size.width, 0, 0)];
    [qrBgView addSubview:qrBtn];
    [qrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(qrBgView).offset(18);
        make.centerX.mas_equalTo(qrBgView);
    }];
    
    UILabel *qrDetail = [UILabel new];
    qrDetail.text = Localized(@"扫一扫，加我为好友");
    qrDetail.textColor = [UIColor hexFloatColor:@"A5A5A5"];
    qrDetail.font = [UIFont systemFontOfSize:12];
    [qrBgView addSubview:qrDetail];
    [qrDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(qrBgView).offset(-10);
        make.centerX.mas_equalTo(qrBgView);
    }];
    
    UIView *scanBgView = [UIView new];
    [headerView addSubview:scanBgView];
    [scanBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(headerView);
        make.width.mas_equalTo(mainWidth/2);
    }];
    UITapGestureRecognizer *tapScan = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        //创建参数对象
        LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
        
        //矩形区域中心上移，默认中心点为屏幕中心点
        style.centerUpOffset = 44;
        
        //扫码框周围4个角的类型,设置为外挂式
        style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
        
        //扫码框周围4个角绘制的线条宽度
        style.photoframeLineW = 6;
        
        //扫码框周围4个角的宽度
        style.photoframeAngleW = 24;
        
        //扫码框周围4个角的高度
        style.photoframeAngleH = 24;
        
        //扫码框内 动画类型 --线条上下移动
        style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
        
        //线条上下移动图片
        style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
        
        //SubLBXScanViewController继承自LBXScanViewController
        //添加一些扫码或相册结果处理
        ScanViewController *scanVC = [[ScanViewController alloc] init];
        scanVC.style = style;
        scanVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:scanVC animated:YES];
    }];
    [scanBgView addGestureRecognizer:tapScan];
    
    UIButton *scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    scanBtn.userInteractionEnabled = NO;
    [scanBtn setTitle:Localized(@"扫一扫") forState:UIControlStateNormal];
    scanBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [scanBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scanBtn setImage:[UIImage imageNamed:@"contact_scan"] forState:UIControlStateNormal];
    [scanBtn setImageEdgeInsets:UIEdgeInsetsMake(-scanBtn.titleLabel.intrinsicContentSize.height-3, 0, 0, -scanBtn.titleLabel.intrinsicContentSize.width)];
    [scanBtn setTitleEdgeInsets:UIEdgeInsetsMake(scanBtn.currentImage.size.height+3, -scanBtn.currentImage.size.width, 0, 0)];
    [scanBgView addSubview:scanBtn];
    [scanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(scanBgView).offset(18);
        make.centerX.mas_equalTo(scanBgView);
    }];
    
    UILabel *scanDetail = [UILabel new];
    scanDetail.text = Localized(@"扫描二维码添加好友");
    scanDetail.textColor = [UIColor hexFloatColor:@"A5A5A5"];
    scanDetail.font = [UIFont systemFontOfSize:12];
    [scanBgView addSubview:scanDetail];
    [scanDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(scanBgView).offset(-10);
        make.centerX.mas_equalTo(scanBgView);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headerView).offset(7);
        make.bottom.mas_equalTo(headerView).offset(-7);
        make.centerX.mas_equalTo(headerView);
        make.width.mas_equalTo(0.5);
    }];
}


#pragma mark - WXApi Delegate
- (void)onReq:(BaseReq *)req
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)onResp:(BaseResp *)resp
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, mainWidth-20, 30)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"search") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 3;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    return _searchBtn;
}

-(void)isOnlineResponse:(NSDictionary *)response{
    
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *images = @[@[@"contact_add_book"], @[@"contact_add_qq", @"contact_add_wechat"]];
    NSArray *texts = @[@[Localized(@"通讯录好友")],@[Localized(@"QQ好友"), Localized(@"微信好友")]];
    NSArray *details = @[@[Localized(@"邀请或添加你的通讯录好友")],@[Localized(@"邀请或添加你的QQ好友"), Localized(@"邀请或添加你的微信好友")]];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.section][indexPath.row]];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.text = texts[indexPath.section][indexPath.row];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
    cell.detailTextLabel.text = details[indexPath.section][indexPath.row];
    cell.detailTextLabel.textColor = COLOR_999999;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.preservesSuperviewLayoutMargins = NO;
//    cell.separatorInset = UIEdgeInsetsZero;
//    cell.layoutMargins = UIEdgeInsetsZero;
    if (indexPath.section == 1 && indexPath.row == 0) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 55.5, mainWidth-12, 0.5)];
        lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
        [cell addSubview:lineView];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        // 通讯录好友
        FriendCheckVC *fcVC = [[FriendCheckVC alloc] init];
        [self.navigationController pushViewController:fcVC animated:YES];
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            if (![TencentOAuth iphoneQQInstalled]) {
                
                [XSTool showToastWithView:self.view Text:@"请移步App Store去下载腾讯QQ客户端"];
            }else {
                NSString *title = self.shareModel.title?self.shareModel.title:APP_NAME;
                NSString *content = self.shareModel.content?self.shareModel.content:APP_NAME;
                NSString *link = self.shareModel.link?self.shareModel.link:@"";
                
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,self.shareModel.logo]]];
                
                QQApiNewsObject *newsObj = [QQApiNewsObject
                                            objectWithURL:[NSURL URLWithString:link]
                                            title:![title isEqualToString:@""] ? title:Localized(@"标题缺失")
                                            description:![content isEqualToString:@""]?content:Localized(@"内容缺失")
                                            previewImageData:data];
                SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
                [QQApiInterface sendReq:req];
            }
        } else if (indexPath.row == 1) {
            // 邀请微信好友
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"分享到通讯录") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                // 邀请微信好友
                NSString *title = self.shareModel.title?self.shareModel.title:APP_NAME;
                NSString *content = self.shareModel.content?self.shareModel.content:APP_NAME;
                NSString *link = self.shareModel.link?self.shareModel.link:@"";
                UIImage *image = [UIImage imageNamed:@"fuDan_logo"];
                
                WXMediaMessage *message = [WXMediaMessage message];
                message.title = title;
                message.description = content;
                [message setThumbImage:image];
                
                // 多媒体消息中包含的网页数据对象
                WXWebpageObject *webpageObject = [WXWebpageObject object];
                // 网页的url地址
                webpageObject.webpageUrl = link;
                message.mediaObject = webpageObject;
                
                SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
                req.bText = NO;
                req.message = message;
                
                req.scene = WXSceneSession;
                [WXApi sendReq:req];
            }];
            UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"分享到朋友圈") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *title = self.shareModel.title?self.shareModel.title:APP_NAME;
                NSString *content = self.shareModel.content?self.shareModel.content:APP_NAME;
                NSString *link = self.shareModel.link?self.shareModel.link:@"";
                UIImage *image = [UIImage imageNamed:@"fuDan_logo"];
                
                WXMediaMessage *message = [WXMediaMessage message];
                message.title = title;
                message.description = content;
                [message setThumbImage:image];
                
                // 多媒体消息中包含的网页数据对象
                WXWebpageObject *webpageObject = [WXWebpageObject object];
                // 网页的url地址
                webpageObject.webpageUrl = link;
                message.mediaObject = webpageObject;
                
                SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
                req.bText = NO;
                req.message = message;
                
                req.scene = WXSceneTimeline;
                [WXApi sendReq:req];
                
            }];
            UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:photograph];
            [alert addAction:select];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
@end
