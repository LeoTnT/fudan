//
//  RightPanelVC.m
//  App3.0
//
//  Created by mac on 17/3/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RightPanelVC.h"
#import "ScanViewController.h"

@interface RightPanelVC () 
{
    CGRect _frame;
    UILabel *_city;
    UIImageView *_weather;
    UILabel *_temperature;
    UILabel *_date;
}
@end

@implementation RightPanelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _frame = CGRectMake(mainWidth*0.3, 0, mainWidth*0.7, mainHeight);
    self.view.backgroundColor = mainColor;
    [self addPanelView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addPanelView
{
    _city = [[UILabel alloc] initWithFrame:CGRectMake(_frame.origin.x+_frame.size.width/2-50, 40, 100, 30)];
    _city.text = @"临沂";
    _city.font = [UIFont systemFontOfSize:30];
    _city.textAlignment = NSTextAlignmentCenter;
    _city.textColor = [UIColor whiteColor];
    [self.view addSubview:_city];
    
    _weather = [[UIImageView alloc] initWithFrame:CGRectMake(_frame.origin.x+_frame.size.width/4-25, 90, 50, 50)];
    [_weather setImage:[UIImage imageNamed:@"panel_weather"]];
    [self.view addSubview:_weather];
    
    _temperature = [[UILabel alloc] initWithFrame:CGRectMake(mainWidth-_frame.size.width/4-50, 100, 100, 30)];
    _temperature.text = @"20℃";
    _temperature.textColor = [UIColor whiteColor];
    _temperature.textAlignment = NSTextAlignmentCenter;
    _temperature.font = [UIFont systemFontOfSize:30];
    [self.view addSubview:_temperature];
    
    _date = [[UILabel alloc] initWithFrame:CGRectMake(_frame.origin.x, 140, _frame.size.width, 30)];
    _date.text = @"2017年4月14日 星期五";
    _date.textColor = [UIColor whiteColor];
    _date.textAlignment = NSTextAlignmentCenter;
    _date.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:_date];
    
    UIView *top_line = [[UIView alloc] initWithFrame:CGRectMake(_frame.origin.x, 200, _frame.size.width, 0.5)];
    top_line.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:top_line];
    
    NSArray *imgArr = @[@"",@"",@"",@"",@""];
    NSArray *titleArr = @[Localized(@"扫一扫"),@"黄历",@"天气",@"签到二维码",Localized(@"我的客服")];
    for (int i = 0; i < 5; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(_frame.origin.x, 200+50*i, _frame.size.width, 50)];
        btn.tag = i;
        [btn addTarget:self action:@selector(panelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        UIImageView *btnImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
        [btnImg setImage:[UIImage imageNamed:imgArr[i]]];
        [btn addSubview:btnImg];
        
        UILabel *btnTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, _frame.size.width-90, 30)];
        btnTitle.text = titleArr[i];
        btnTitle.textColor = [UIColor whiteColor];
        btnTitle.font = [UIFont systemFontOfSize:20];
        [btn addSubview:btnTitle];
        
        UIImageView *arrows = [[UIImageView alloc] initWithFrame:CGRectMake(_frame.size.width-30, 17, 16, 16)];
        [arrows setImage:[UIImage imageNamed:@"arrows"]];
        [btn addSubview:arrows];
        
        if (i > 0 && i < 5) {
            UIView *center_line = [[UIView alloc] initWithFrame:CGRectMake(_frame.origin.x+60, 200+50*i, _frame.size.width, 0.5)];
            center_line.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:center_line];
        }
    }
    
    UIView *bottom_line = [[UIView alloc] initWithFrame:CGRectMake(_frame.origin.x, 450, _frame.size.width, 0.5)];
    bottom_line.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottom_line];
    
    // 序列号
    UILabel *xuliehao = [[UILabel alloc] initWithFrame:CGRectMake(_frame.origin.x+30, 460, _frame.size.width-60, 30)];
    xuliehao.text = @"正版序列号";
    xuliehao.textColor = [UIColor whiteColor];
    xuliehao.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:xuliehao];
    
    UILabel *userNo = [[UILabel alloc] initWithFrame:CGRectMake(_frame.origin.x+30, 500, _frame.size.width-60, 40)];
    userNo.backgroundColor = [UIColor grayColor];
    userNo.text = @"164298";
    NSNumber *kern = [NSNumber numberWithFloat:userNo.frame.size.width/8-15];
    userNo.attributedText = [[NSAttributedString alloc] initWithString:userNo.text attributes:@{NSKernAttributeName:kern}];
    userNo.textColor = [UIColor whiteColor];
    userNo.font = [UIFont systemFontOfSize:30];
    userNo.textAlignment = NSTextAlignmentCenter;
    userNo.layer.masksToBounds = YES;
    userNo.layer.cornerRadius = 5;
    [self.view addSubview:userNo];
}

- (void)panelBtnClick:(UIButton *)pSender
{
    switch (pSender.tag) {
        case 0:
        {
            // 扫一扫
            [self qqStyle];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -模仿qq界面
- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    ScanViewController *scanVC = [[ScanViewController alloc] init];
    scanVC.delegate = self;
    scanVC.style = style;
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
}

@end
