//
//  PopViewController.h
//  App3.0
//
//  Created by mac on 17/2/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopViewDelegate <NSObject>

- (void)menuClick:(NSInteger)index;

@end

@interface PopViewController : UIViewController
@property (nonatomic ,strong)NSArray *dataSource;

@property(nonatomic, weak) id<PopViewDelegate>delegate;
@end
