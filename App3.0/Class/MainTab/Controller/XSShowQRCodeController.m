//
//  XSShowQRCodeController.m
//  App3.0
//
//  Created by apple on 2017/5/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSShowQRCodeController.h"
#import "TranspondListViewController.h"
@interface XSShowQRCodeController ()

@end

@implementation XSShowQRCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"..." style:UIBarButtonItemStylePlain target:self action:@selector(showChouse)];
}


- (void) showChouse {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    // Create the actions.
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"save") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.qrCodelImage]];
            UIImage *aimage = [[UIImage alloc] initWithData:data];
            [self loadImageFinished:aimage];
        });
        
    }];
    
    UIAlertAction *otherButtonOneAction = [UIAlertAction actionWithTitle:Localized(@"qr_send") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.qrCodelImage]];
        
#ifdef ALIYM_AVALABLE
        YWMessageBodyImage *body = [[YWMessageBodyImage alloc] initWithMessageImageData:data];
        TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:body];
        trVC.isShare = YES;
        [self.navigationController pushViewController:trVC animated:YES];
#elif defined EMIM_AVALABLE
        EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:data displayName:@"image.jpeg"];
        NSString *from = [[EMClient sharedClient] currentUsername];
        //生成Message
        EMMessage *message = [[EMMessage alloc] initWithConversationID:nil from:from to:nil body:body ext:nil];
        TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:message];
        [self.navigationController pushViewController:trVC animated:YES];
#else
        XM_TransModel *transModel = [[XM_TransModel alloc] initShareImage:data];
        TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:transModel];
        [self.navigationController pushViewController:trVC animated:YES];
#endif
        
        
    }];
    
    UIAlertAction *otherButtonTwoAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:otherButtonOneAction];
    [alertController addAction:otherButtonTwoAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)loadImageFinished:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
    if (!error) {
        
        [XSTool showToastWithView:self.view Text:Localized(@"my_skill_sava_success")];
        
        
    }else{
        [XSTool showToastWithView:self.view Text:Localized(@"person_qr_save_fail")];
    }
}


- (void)setQrCodelImage:(NSString *)qrCodelImage {
    qrCodelImage = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,qrCodelImage];
    
    
    _qrCodelImage = qrCodelImage;
    
    UIImageView *imageView = [UIImageView new];
    [imageView sd_setImageWithURL:[NSURL URLWithString:qrCodelImage] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    [self.view addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_W_Width(200), SCREEN_W_Width(200)));
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];


    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
}

@end
