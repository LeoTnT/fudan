//
//  XSShowQRCodeController.h
//  App3.0
//
//  Created by apple on 2017/5/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSShowQRCodeController : UIViewController


@property (nonatomic,copy)NSString *qrCodelImage;

@end
