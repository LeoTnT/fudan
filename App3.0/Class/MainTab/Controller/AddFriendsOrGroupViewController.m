//
//  AddFriendsOrGroupViewController.m
//  App3.0
//
//  Created by mac on 17/5/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddFriendsOrGroupViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "AddFriendsViewController.h"
#import "AddGroupViewController.h"

#define SEGMENT_HEIGHT 44.0f

@interface AddFriendsOrGroupViewController () <UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@end

@implementation AddFriendsOrGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.navigationItem.title = Localized(@"添加");
    
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT-0.5, mainWidth, 0.5)];
    lineView.backgroundColor = BG_COLOR;
    [self.view addSubview:lineView];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    AddFriendsViewController *friendsVC = [[AddFriendsViewController alloc] init];
    [self addChildViewController:friendsVC];
    friendsVC.view.frame = CGRectMake(0, 0, mainWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:friendsVC.view];
    AddGroupViewController *groupVC = [[AddGroupViewController alloc] init];
    [self addChildViewController:groupVC];
    groupVC.view.frame = CGRectMake(mainWidth, 0, mainWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:groupVC.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];

}

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT, mainWidth, mainHeight-SEGMENT_HEIGHT)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*2, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, SEGMENT_HEIGHT)];
        _segmentControl.sectionTitles = @[Localized(@"找人"),Localized(@"找群")];
        _segmentControl.titleTextAttributes = @{NSForegroundColorAttributeName : COLOR_666666, NSFontAttributeName : [UIFont systemFontOfSize:14]};
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR, NSFontAttributeName : [UIFont systemFontOfSize:14]};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 3;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        [_segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

@end
