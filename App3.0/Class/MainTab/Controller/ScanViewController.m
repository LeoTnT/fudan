//
//  ScanViewController.m
//  App3.0
//
//  Created by mac on 17/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ScanViewController.h"
#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "PersonalViewController.h"
#import "XSShareView.h"
#import "S_CashierController.h"
#import "PersonViewController.h"
#import "BusinessModel.h"
#import "GroupQRViewController.h"
#import "UserQuickPayViewController.h"
#import "XMGroupChatController.h"

@interface ScanViewController ()

@end

@implementation ScanViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title=Localized(@"扫一扫");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:Localized(@"相册") style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonClick)];
    
    UILabel *tips = [[UILabel alloc] initWithFrame:CGRectMake(12, 60, mainWidth-24, 30)];
    tips.text = Localized(@"将二维码/条形码放入框内");
    tips.textColor = [UIColor whiteColor];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:tips];
    [self.view bringSubviewToFront:tips];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rightBarButtonClick {
    [self openLocalPhoto:YES];
}

- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    self.style = style;
}

- (void)showError:(NSString *)str
{
    
}

- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1)
    {
        [XSTool showToastWithView:self.view Text:@"识别失败"];
        
        return;
    }
    
    /*
     //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
     for (LBXScanResult *result in array) {
     NSLog(@"scanResult:%@",result.strScanned);
     }
     */
    
    LBXScanResult *scanResult = array[0];
    NSString*strResult = scanResult.strScanned;
    NSArray *arr = [strResult componentsSeparatedByString:@"?"];
    
    NSString *head = @"";
    if (arr.count >= 1) {
        head = arr.firstObject;
        NSLog(@"head  =%@",arr.firstObject);
    }else{
        head = strResult;
    }
    if (self.isCoinAdrress) {
        [self.navigationController popViewControllerAnimated:YES];
        self.qrBlock(head);
        return;
    }
    if ([self isPureInt:head]){
        
        [self requestCommodityProduct:head];
    } else {
        
        [XSTool showProgressHUDWithView:self.view];
        //震动提醒
//        [LBXScanWrapper systemVibrate];
//
//        //声音提醒
//        [LBXScanWrapper systemSound];
        if (arr.count >= 1) {
            [self QRCondRequest:strResult];
        }else{
            [XSTool hideProgressHUDWithView:self.view];
        }
        
    }
}


- (void) requestCommodityProduct:(NSString *)str {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getCommodityProduct:str success:^(NSDictionary *dic, resultObject *state) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state) {
            
            NSString *ID = dic[@"data"][@"pid"];
            GoodsDetailViewController *vc = [GoodsDetailViewController new];
            vc.goodsID = ID;
            [XSTool hideProgressHUDWithView:self.view];
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            
            
            [MBProgressHUD showMessage:dic[@"info"] view:self.view hideTime:2 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}

- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

/**
 product";//商品
 "supply";//店铺
 "app";//客户端
 "user";//个人,主要用于二维码
 */
- (void)handleQRCode:(NSString *)QRInformation {
    //    http://txzy.xsy.dsceshi.cn/mobile/product/details?qrtype=product&id=2&userid=212

    [QRInformation containsString:@"&"] ? [self QRCondRequest:QRInformation]:[self barCodeRequest:QRInformation];
    
}

- (void)scanFail {
    [self reStartDevice];
}

/**
 条形码
 */
- (void) barCodeRequest:(NSString *)sender {
    
    
}


/**
 二维码
 */
- (void) QRCondRequest:(NSString *)QRInformation {
    [XSTool hideProgressHUDWithView:self.view];
    NSArray *paramArr = [QRInformation componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![QRInformation containsString:ImageBaseUrl] || arr.count < 2) {
        if ([QRInformation hasPrefix:@"http"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
        } else {
            [XSTool showToastWithView:self.view Text:@"无法识别"];
            [self performSelector:@selector(scanFail) withObject:nil afterDelay:2];
        }
        
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"%@%@",Localized(@"您已安装"),APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"TYPE_NORMAL_GROUP"]){
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        // 从服务器拉取群信息
        [HTTPManager CheckUserInGroupWithGroupId:qrid success:^(NSDictionary * dic, resultObject *state){
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                GroupDataModel *groupModel = [GroupDataModel mj_objectWithKeyValues:dic[@"data"]];
                if (groupModel.in_group) {
#ifdef ALIYM_AVALABLE
                    YWTribe *tribe = [YWTribe new];
                    tribe.tribeId = groupModel.gid;
                    YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = groupModel.name;
                    chatVC.avatarUrl = groupModel.avatar;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
                    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:groupModel.gid type:EMConversationTypeGroupChat createIfNotExist:YES];
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = groupModel.name;
                    chatVC.avatarUrl = groupModel.avatar;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
#else
                    XMPPRoomManager *model = [XMPPSignal roomConfigWithRoomName:groupModel.gid];
                    if (!model) return;
                    ConversationModel *conversation = [[ConversationModel alloc] initWithConversation:model];
                    conversation.title = groupModel.name;
                    XMGroupChatController *vc = [[XMGroupChatController alloc] initWithRoomJID:conversation];
                    [self.navigationController pushViewController:vc animated:YES];
#endif
                    
                } else {
                    GroupQRViewController *vc = [[GroupQRViewController alloc] init];
                    vc.groupModel = groupModel;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else if ([qrtype isEqualToString:@"supply_pay"]){
        S_CashierController *cashier =[S_CashierController new];
        cashier.showTitle = qrid;
        cashier.supply_name = qrid;
        [self.navigationController pushViewController:cashier animated:YES];
    } else if ([qrtype isEqualToString:@"quickpay"]){
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager supplyGetSupplyInfoWithUid:qrid success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                BusinessSupplyParser *supplyParser = [BusinessSupplyParser mj_objectWithKeyValues:dic[@"data"]];
               
                //是否开通收银台
                if ([supplyParser.is_enable_quickpay intValue]==1) {
                    S_CashierController *cashier =[S_CashierController new];
                    NSString *str = arr[2],*supplyId;
                    NSString *delStr = @"intro=";
                    if ([str  containsString:delStr]) {
                        supplyId = [str substringFromIndex:delStr.length];
                    }
                    
                    cashier.showTitle = supplyParser.name;//[NSString stringWithFormat:@"商家账号：%@",supplyId];
                    cashier.supply_name = supplyId;
                    cashier.quick_percent_pay = [supplyParser.quick_pay integerValue];
                   [self.navigationController pushViewController:cashier animated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:@"尚未开通收银台！"];
                }
               

            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                
            }
        }fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    } else if ([qrtype isEqualToString:@"userquickpay"]) {
        NSString *param3 = arr[2];
        NSString *username, *total, *remark;
        NSString *deleteStr3 = @"intro=";
        if ([param3 containsString:deleteStr3]) {
            username = [param3 substringFromIndex:deleteStr3.length];
        }
        
        if (arr.count >= 4) {
            NSString *param4 = arr[3];
            NSString *deleteStr4 = @"total=";
            if ([param4 containsString:deleteStr4]) {
                total = [param4 substringFromIndex:deleteStr4.length];
            }
        }
        
        if (arr.count >= 5) {
            NSString *param5 = arr[4];
            NSString *deleteStr5 = @"remark=";
            if ([param5 containsString:deleteStr5]) {
                remark = [param5 substringFromIndex:deleteStr5.length];
            }
        }
        
        
        // 个人收款码
        UserQuickPayViewController *vc = [[UserQuickPayViewController alloc] init];
        vc.uid = qrid;
        vc.username = username;
        vc.money = total;
        vc.remark = remark;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
    }
}

@end
