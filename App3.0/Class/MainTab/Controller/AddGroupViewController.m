//
//  AddGroupViewController.m
//  App3.0
//
//  Created by mac on 17/5/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddGroupViewController.h"
#import "SearchVC.h"
#import "ContactListSelectViewController.h"

@interface AddGroupViewController () <UISearchBarDelegate>
@property(nonatomic, strong)UIButton *searchBtn;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation AddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = @[@{@"image":@"contact_create_group",@"title":Localized(@"创建群组")},@{@"image":@"contact_create_chat",@"title":Localized(@"创建多人会话")}];
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
    [self.view addSubview:self.searchBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }
            break;
        case 1:
        {
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }
            break;
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"addGroupCell";
    UITableViewCell *cell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row == 0) {
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 49.5, mainWidth-12, 0.5)];
            lineView.backgroundColor = LINE_COLOR_NORMAL;
            [cell addSubview:lineView];
        }
    }
    NSDictionary *dataDic = self.dataArray[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:dataDic[@"image"]];
    cell.textLabel.text = dataDic[@"title"];
    cell.textLabel.textColor = COLOR_666666;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    return cell;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, mainWidth-20, 30)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"输入群组关键字或者群号") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 3;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    return _searchBtn;
}
@end
