//
//  PreLoadViewController.m
//  App3.0
//
//  Created by mac on 17/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PreLoadViewController.h"

@interface PreLoadViewController ()

@end

@implementation PreLoadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgView setImage:[UIImage imageNamed:@"background_image"]];
    [self.view addSubview:bgView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
