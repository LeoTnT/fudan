//
//  ScanViewController.h
//  App3.0
//
//  Created by mac on 17/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <LBXScan/LBXScanViewController.h>

@interface ScanViewController : LBXScanViewController

@property (nonatomic,copy) void(^qrBlock)(NSString *content);
@property (nonatomic,assign) BOOL isCoinAdrress;

@end
