//
//  LoggerDetailViewController.h
//  App3.0
//
//  Created by mac on 17/2/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoggerDetailViewController : UIViewController
- (id)initWithLog:(NSString *)logText forDateString:(NSString *)logDate;
@end
