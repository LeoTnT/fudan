//
//  MemberView.m
//  App3.0
//
//  Created by mac on 17/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MemberView.h"

@interface MemberView()
{
    UIImageView *_avatar;
    UILabel *_name;
    __weak id<MemberViewDelegate>delegate;
}
@end
@implementation MemberView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat space = 10;
        CGFloat width = frame.size.width-2*space;
        _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(space, space, width, width)];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 5;
        [self addSubview:_avatar];
        
        _name = [[UILabel alloc] initWithFrame:CGRectMake(space, space+width, width, 20)];
        _name.textColor = [UIColor hexFloatColor:@"454545"];
        _name.font = [UIFont systemFontOfSize:12];
        _name.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_name];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)tapAction:(UITapGestureRecognizer *)pSender
{
    if (delegate) {
        [delegate memberClickOfIndex:self.indexString];
    }
}

- (void)setupAvatarWithUrlStr:(NSString *)urlStr name:(NSString *)name nickName:(NSString *)nickName
{
    [_avatar getImageWithUrlStr:urlStr andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _name.text = nickName;
    self.indexString = name;
}

- (void)setupAvatarWithImage:(UIImage *)image
{
    [_avatar setImage:image];
}
@end
