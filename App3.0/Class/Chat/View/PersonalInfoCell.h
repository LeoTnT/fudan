//
//  PersonalInfoCell.h
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactModel.h"

@protocol PersonalInfoCellDelegate <NSObject>
- (void)remarkClick;
@end

@interface PersonalInfoCell : UITableViewCell
@property (weak, nonatomic) id<PersonalInfoCellDelegate>delegate;
- (void)setCellData:(UserInfoDataParser *)model;
@end
