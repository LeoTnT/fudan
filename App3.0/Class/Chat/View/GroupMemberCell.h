//
//  GroupMemberCell.h
//  App3.0
//
//  Created by mac on 17/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GroupMemberCellDelegate
- (void)groupMemberClickOfIndex:(NSString *)indexString;
@optional
- (void)checkAllClick;
@end

@interface GroupMemberCell : UITableViewCell
@property (nonatomic, weak) id<GroupMemberCellDelegate>delegate;
- (void)setupMembersWithData:(GroupDataModel *)model;
@property (nonatomic, assign) BOOL showCheckAll;
@property (assign, nonatomic) BOOL isGroupOwner;
@end
