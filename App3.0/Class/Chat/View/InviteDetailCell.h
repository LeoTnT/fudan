//
//  InviteDetailCell.h
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatModel.h"

@interface InviteDetailCell : UITableViewCell
@property (strong, nonatomic) InviteInfoModel *inviteModel;
+ (instancetype)createInviteDetailCellWithTableView:(UITableView *)tableView;
@end
