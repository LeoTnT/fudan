//
//  InviteDetailCell.m
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "InviteDetailCell.h"
#import "MemberView.h"

@implementation InviteDetailCell

+ (instancetype)createInviteDetailCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"inviteDetailCell";
    InviteDetailCell *cell = (InviteDetailCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[InviteDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setInviteModel:(InviteInfoModel *)inviteModel {
    _inviteModel = inviteModel;
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[MemberView class]]) {
            [view removeFromSuperview];
        }
    }
    CGFloat width = (mainWidth-24)/5;
    for (int i = 0; i < inviteModel.target_users.count; i++) {
        MemberView *view = [[MemberView alloc] initWithFrame:CGRectMake(12+width*(i%5), 20+(20+width)*(i/5), width, width)];

        InvitePersonModel *model = inviteModel.target_users[i];
        [view setupAvatarWithUrlStr:model.user_avatar name:nil nickName:model.user_nick];
        [self.contentView addSubview:view];
    }
}
@end
