//
//  MemberView.h
//  App3.0
//
//  Created by mac on 17/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MemberViewDelegate
- (void)memberClickOfIndex:(NSString *)indexString;
@end

@interface MemberView : UIView
- (void)setupAvatarWithUrlStr:(NSString *)urlStr name:(NSString *)name nickName:(NSString *)nickName;
- (void)setupAvatarWithImage:(UIImage *)image;
@property (nonatomic, weak) id<MemberViewDelegate>delegate;
@property (nonatomic) NSString *indexString;
@end
