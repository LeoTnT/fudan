//
//  GroupMemberCell.m
//  App3.0
//
//  Created by mac on 17/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupMemberCell.h"
#import "MemberView.h"

@interface GroupMemberCell() <MemberViewDelegate>
{
    NSMutableArray *_memberArr;
    __weak id<GroupMemberCellDelegate>delegate;
}
@property (nonatomic, strong) UIButton *checkAllButton;
@end
@implementation GroupMemberCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _memberArr = [NSMutableArray array];
        _showCheckAll = YES;
        
        _checkAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkAllButton setTitle:@"查看全部群成员" forState:UIControlStateNormal];
        [_checkAllButton setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        [_checkAllButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_checkAllButton addTarget:self action:@selector(checkAllAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_checkAllButton];
    }
    return self;
}

- (void)setupMembersWithData:(GroupDataModel *)model
{
    NSArray *data = model.members;
    CGFloat width = mainWidth/5;
    CGFloat topSpace = 10;
    NSInteger count = data.count;
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[MemberView class]]) {
            [view removeFromSuperview];
        }
    }
    NSInteger trueCount = count+2;
    if (!self.isGroupOwner) {
        if (model.allowinvites) {
            trueCount = count+1;
        } else {
            trueCount = count;
        }
    }
    for (int i = 0; i < trueCount; i++) {
        MemberView *view = [[MemberView alloc] initWithFrame:CGRectMake(width*(i%5), (topSpace+width)*(i/5), width, width)];
        if (i == count) {
            [view setupAvatarWithImage:[UIImage imageNamed:@"chat_group_add"]];
            view.indexString = @"add";
        } else if (i == count+1) {
            [view setupAvatarWithImage:[UIImage imageNamed:@"chat_group_del"]];
            view.indexString = @"del";
        } else {
            NickNameDataParser *parser = data[i];
            NSString *showName = [parser getName];
            if ([parser.relation integerValue] > 0) {
                showName = [[ChatHelper shareHelper] displayNameByUid:parser.uid];
            }
            [view setupAvatarWithUrlStr:parser.logo name:parser.uid nickName:showName];
        }
        view.delegate = self;
        [self addSubview:view];
    }
    
    [self.checkAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(mainWidth-2*topSpace, 40));
    }];
}

- (void)setShowCheckAll:(BOOL)showCheckAll
{
    _showCheckAll = showCheckAll;
    self.checkAllButton.hidden = !_showCheckAll;
}

#pragma mark - MemberDelegate
- (void)memberClickOfIndex:(NSString *)indexString
{
    if (delegate) {
        [delegate groupMemberClickOfIndex:indexString];
    }
}

- (void)checkAllAction:(UIButton *)sender
{
    if (delegate) {
        [delegate checkAllClick];
    }
}
@end
