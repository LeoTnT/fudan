//
//  ChatListCell.m
//  App3.0
//
//  Created by mac on 17/2/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatListCell.h"
#import "ChatHelper.h"
#import "UserInstance.h"

@interface ChatListCell ()
{
    __weak id<ChatListCellDelegate> delegate;
    
    UIImageView *_avatar;
    UILabel *_title;
    UILabel *_descript;
    UILabel *_time;
    UILabel *_bageNumber;
    NSMutableArray *_dataArr;
    UILabel *roomMembers;
}
@end

@implementation ChatListCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _dataArr = [NSMutableArray array];
        
        _avatar = [[UIImageView alloc] init];
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 5;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        _descript = [[UILabel alloc] init];
        _descript.textColor = COLOR_999999;
        _descript.font = [UIFont systemFontOfSize:14];
        [self addSubview:_descript];
        
        _time = [[UILabel alloc] init];
        _time.textColor = [UIColor hexFloatColor:@"CDCDCD"];
        _time.textAlignment = NSTextAlignmentRight;
        _time.font = [UIFont systemFontOfSize:12];
        [self addSubview:_time];
        
        _bageNumber = [[UILabel alloc] init];
        _bageNumber.layer.cornerRadius = 9;
        _bageNumber.layer.masksToBounds = YES;
        _bageNumber.font = [UIFont systemFontOfSize:14];
        _bageNumber.textAlignment = NSTextAlignmentCenter;
        _bageNumber.textColor = [UIColor whiteColor];
        _bageNumber.backgroundColor = [UIColor hexFloatColor:@"f43531"];
        _bageNumber.hidden = YES;
        [self addSubview:_bageNumber];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
        [self.contentView addSubview:lineView];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        [_time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-12);
            make.top.mas_equalTo(self.mas_top).offset(16);
        }];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8);
            make.bottom.mas_equalTo(self.mas_centerY).offset(-4);
            make.right.mas_equalTo(_time.mas_left).offset(-12);
        }];
        [_descript mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(8);
            make.top.mas_equalTo(self.mas_centerY).offset(4);
            make.right.mas_equalTo(self.mas_right).offset(-12);
        }];
        
        [_bageNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_avatar.mas_right);
            make.centerY.mas_equalTo(_avatar.mas_top).offset(2);
            make.height.mas_equalTo(18);
            make.width.mas_greaterThanOrEqualTo(18);
        }];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.right.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

// TODO:获取个人信息
- (void) getUserInfor:(NSString *)ID {
    
    if ([ID isEqualToString:[UserInstance ShardInstnce].uid]) {
        _title.text = isEmptyString([UserInstance ShardInstnce].nickName)?[UserInstance ShardInstnce].userName:[UserInstance ShardInstnce].nickName;
        [_avatar sd_setImageWithURL:[NSURL URLWithString:[UserInstance ShardInstnce].avatarImgUrl] placeholderImage:[UIImage imageNamed:@"user_fans_avatar"]];
        return;
    }
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:ID];
    if (contacts != nil && contacts.count > 0) {
        ContactDataParser *contact = contacts[0];
        _title.text =[contact getName];
        [_avatar sd_setImageWithURL:[NSURL URLWithString:contact.avatar] placeholderImage:[UIImage imageNamed:@"user_fans_avatar"]];
    }
}


// TODO:获取群信息
- (void) getGroupInfor:(NSString *)ID {
    NSArray *arr = [[DBHandler sharedInstance] getGroupByGroupId:ID];
    if (arr && arr.count > 0) {
        GroupDataModel *group = arr[0];
        _title.text =[group name];
        [_avatar sd_setImageWithURL:[NSURL URLWithString:group.avatar] placeholderImage:[UIImage imageNamed:@"user_fans_avatar"]];
        roomMembers.text =[NSString stringWithFormat:NSLocalizedString(@"%@人", nil),group.member_count];
    }
}

- (void)setChatListCellData:(ConversationModel *)model
{
    
#ifdef ALIYM_AVALABLE
    _title.text = model.title;
    [_avatar getImageWithUrlStr:model.avatarURLPath andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    YWConversation *conversation = model.conversation;
    if ([conversation.conversationUnreadMessagesCount integerValue] > 0) {
        if ([conversation.conversationUnreadMessagesCount integerValue] <= 99) {
            _bageNumber.text = [NSString stringWithFormat:@"%@",conversation.conversationUnreadMessagesCount];
        } else {
            _bageNumber.text = @"99+";
        }
        
        _bageNumber.hidden = NO;
    } else {
        _bageNumber.hidden = YES;
    }
    
    id <IYWMessage> latestMes = conversation.conversationLatestMessage;
    _descript.text = [[ChatHelper shareHelper] latestMessageTitleWithMessage:latestMes];
    
    // 显示最后会话时间
//    NSString *str = [self timeWithTimeIntervalString:[NSString stringWithFormat:@"%ld", (long)[latestMes.time timeIntervalSince1970]]];
//    NSString *subString = [str substringWithRange:NSMakeRange(0, 19)];
    NSDate *lastDate = latestMes.time;
    NSLog(@"%@",lastDate);
    NSString *dateStr;  //年月日
    NSString *hour;     //时
    
    if ([lastDate year] == [[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        NSLog(@"days === %li",(long)days);
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
            if (isEmptyString(dateStr)) {
                hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
                _time.text = [NSString stringWithFormat:@"%@:%02d",hour,(int)[lastDate minute]];
            } else {
                _time.text = [NSString stringWithFormat:@"%@",dateStr];
            }
        }else{
            dateStr = [lastDate stringMonthDay];
            _time.text = [NSString stringWithFormat:@"%@",dateStr];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
        _time.text = [NSString stringWithFormat:@"%@",dateStr];
    }
#elif defined EMIM_AVALABLE
    _title.text = model.title;
    [_avatar getImageWithUrlStr:model.avatarURLPath andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    EMConversation *conversation = model.conversation;
    if (conversation.unreadMessagesCount > 0) {
        if (conversation.unreadMessagesCount <= 99) {
            _bageNumber.text = [NSString stringWithFormat:@"%d",conversation.unreadMessagesCount];
        } else {
            _bageNumber.text = @"99+";
        }
        
        _bageNumber.hidden = NO;
    } else {
        _bageNumber.hidden = YES;
    }
    
    EMMessage *latestMes = conversation.latestMessage;
    _descript.text = [[ChatHelper shareHelper] latestMessageTitleWithMessage:latestMes];
    
    // 显示最后会话时间
    NSString *str = [self timeWithTimeIntervalString:[NSString stringWithFormat:@"%lld",latestMes.timestamp]];
    NSString *subString = [str substringWithRange:NSMakeRange(0, 19)];
    NSDate *lastDate = [NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@",lastDate);
    NSString *dateStr;  //年月日
    NSString *hour;     //时
    
    if ([lastDate year] == [[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        NSLog(@"days === %li",(long)days);
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
            if (isEmptyString(dateStr)) {
                hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
                _time.text = [NSString stringWithFormat:@"%@:%02d",hour,(int)[lastDate minute]];
            } else {
                _time.text = [NSString stringWithFormat:@"%@",dateStr];
            }
        }else{
            dateStr = [lastDate stringMonthDay];
            _time.text = [NSString stringWithFormat:@"%@",dateStr];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
        _time.text = [NSString stringWithFormat:@"%@",dateStr];
    }
    
#else
    if (model.chatType == UUChatTypeChatRoom) {
        [self getGroupInfor:model.bareJID.user];
    }else{
        
        if (isEmptyString(model.title) || isEmptyString(model.avatarURLPath)) {
            
            [self getUserInfor:model.bareJID.user] ;
        }else{
            _title.text = model.title;
            [_avatar sd_setImageWithURL:[NSURL URLWithString:model.avatarURLPath] placeholderImage:[UIImage imageNamed:@"user_fans_avatar"]];
        }
    }
    
    if (model.messageCount != 0 && model.messageCount >0) {
        if (model.messageCount <= 99) {
            _bageNumber.text = [NSString stringWithFormat:@"%ld",(long)model.messageCount];
        } else {
            _bageNumber.text = @"99+";
        }
        _bageNumber.hidden = NO;
    } else {
        _bageNumber.hidden = YES;
    }

    NSString *type = nil;
    if ([model.lastestMessageStr containsString:@"uploadFolder"]) {
        if ( [model.lastestMessageStr containsString:@".m4a"]) {
            type = @"[语音]";
        }else if ([model.lastestMessageStr containsString:@".jpg"]){
            type = @"[图片]";
        }else {
            type = @"[文件]";
        }
    }else{
        NSString*messString = model.lastestMessageStr;
        NSDictionary *dic = [messString mj_JSONObject];
        if (dic) {
            if ([model.conversation containsString:@"robot"]) {
                _title.text = Localized(@"系统消息");
                [_avatar getImageWithUrlStr:dic[@"ico"] andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
            }
            
            if ([dic.allKeys containsObject:@"body"]) {
                XMPP_RoomMessage *message = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
                type = message.body;
            } else {
                type = dic[@"type_title"];
            }
            
        }else{
            type = model.lastestMessageStr;
        }
        
    }
    
    _descript.text = type;
    
    // 显示最后会话时间
    NSDate *lastDate = model.lastestDate;//[NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *dateStr;  //年月日
    NSString *hour;     //时
    
    if ([lastDate year] == [[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        //        NSLog(@"days === %li",(long)days);
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
            if (isEmptyString(dateStr)) {
                hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
                _time.text = [NSString stringWithFormat:@"%@:%02d",hour,(int)[lastDate minute]];
            } else {
                _time.text = [NSString stringWithFormat:@"%@",dateStr];
            }
        }else{
            dateStr = [lastDate stringMonthDay];
            _time.text = [NSString stringWithFormat:@"%@",dateStr];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
        _time.text = [NSString stringWithFormat:@"%@",dateStr];
    }
#endif
    
    
}

- (void)setFindChatData:(id)message {
#ifdef ALIYM_AVALABLE
    id<IYWMessage> ywMessage = message;
    if (ywMessage.outgoing) {
        _title.text = [UserInstance ShardInstnce].nickName;
        [_avatar getImageWithUrlStr:[UserInstance ShardInstnce].avatarImgUrl andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    } else {
        if (ywMessage.conversationType == YWConversationTypeP2P) {
            NSArray *array = [[DBHandler sharedInstance] getContactByUid:ywMessage.messageFromPerson.personId];
            if (array != nil && array.count > 0) {
                ContactDataParser *model = array[0];
                _title.text = [model getName];
                [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                
            }
        } else if (ywMessage.conversationType == YWConversationTypeTribe) {
            NSArray *array = [[DBHandler sharedInstance] getMemberByMemberId:ywMessage.messageFromPerson.personId];
            if (array != nil && array.count > 0) {
                NickNameDataParser *model = array[0];
                _title.text = model.nickname;
                [_avatar getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                
            }
        }
    }
    
    
    YWMessageBodyText *messageBody = (YWMessageBodyText *)ywMessage.messageBody;
    _descript.text = messageBody.messageText;;
    
    // 显示最后会话时间
//    NSString *str = [self timeWithTimeIntervalString:[NSString stringWithFormat:@"%lld",message.timestamp]];
//    NSString *subString = [str substringWithRange:NSMakeRange(0, 19)];
    NSDate *lastDate = ywMessage.time;//[NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@",lastDate);
    NSString *dateStr;  //年月日
    NSString *hour;     //时
    
    if ([lastDate year]==[[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        NSLog(@"days === %li",(long)days);
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
        }else{
            dateStr = [lastDate stringMonthDay];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
    }
    hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
    _time.text = [NSString stringWithFormat:@"%@ %@:%02d",dateStr,hour,(int)[lastDate minute]];
#elif defined EMIM_AVALABLE
    EMMessage *emMessage = message;
    if ([emMessage.from isEqualToString:[UserInstance ShardInstnce].uid]) {
        _title.text = [UserInstance ShardInstnce].nickName;
        [_avatar getImageWithUrlStr:[UserInstance ShardInstnce].avatarImgUrl andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    } else {
        if (emMessage.chatType == EMChatTypeChat) {
            NSArray *array = [[DBHandler sharedInstance] getContactByUid:emMessage.from];
            if (array != nil && array.count > 0) {
                ContactDataParser *model = array[0];
                _title.text = [model getName];
                [_avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                
            }
        } else if (emMessage.chatType == EMChatTypeGroupChat) {
            NSArray *array = [[DBHandler sharedInstance] getMemberByMemberId:emMessage.from];
            if (array != nil && array.count > 0) {
                NickNameDataParser *model = array[0];
                _title.text = model.nickname;
                [_avatar getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                
            }
        }
    }
    
    
    EMMessageBody *messageBody = emMessage.body;
    _descript.text = ((EMTextMessageBody *)messageBody).text;;
    
    // 显示最后会话时间
    NSString *str = [self timeWithTimeIntervalString:[NSString stringWithFormat:@"%lld",emMessage.timestamp]];
    NSString *subString = [str substringWithRange:NSMakeRange(0, 19)];
    NSDate *lastDate = [NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@",lastDate);
    NSString *dateStr;  //年月日
    NSString *hour;     //时
    
    if ([lastDate year]==[[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        NSLog(@"days === %li",(long)days);
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
        }else{
            dateStr = [lastDate stringMonthDay];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
    }
    hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
    _time.text = [NSString stringWithFormat:@"%@ %@:%02d",dateStr,hour,(int)[lastDate minute]];
#else
    
#endif
    
}


- (NSString *)timeWithTimeIntervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]/ 1000.0];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

@end
