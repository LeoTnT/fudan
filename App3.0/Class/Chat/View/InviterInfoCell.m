//
//  InviterInfoCell.m
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "InviterInfoCell.h"

@interface InviterInfoCell ()
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *desc;
@end
@implementation InviterInfoCell

+ (instancetype)createInviterInfoCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"inviterInfoCell";
    InviterInfoCell *cell = (InviterInfoCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[InviterInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    _avatar = [UIImageView new];
    _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
    _avatar.layer.masksToBounds = YES;
    _avatar.layer.cornerRadius = 5;
    [self.contentView addSubview:_avatar];
    
    _name = [UILabel new];
    _name.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview:_name];
    
    _desc = [UILabel new];
    _desc.textColor = COLOR_999999;
    _desc.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_desc];
    
    [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(12);
        make.top.mas_equalTo(self).offset(9);
        make.bottom.mas_equalTo(self).offset(-9);
        make.width.mas_equalTo(_avatar.mas_height);
    }];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_avatar.mas_right).offset(8);
        make.top.mas_equalTo(self).offset(14.5);
    }];
    
    [_desc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_name);
        make.bottom.mas_equalTo(self).offset(-15);
    }];
}

- (void)setInviteModel:(InviteInfoModel *)inviteModel {
    _inviteModel = inviteModel;
    [self.avatar getImageWithUrlStr:inviteModel.user_avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.name.text = inviteModel.user_nick;
    
    NSInteger length = inviteModel.invite_num.length;
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"想邀请%@位朋友加入群聊",inviteModel.invite_num]];
    [attri addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(3, length)];
    self.desc.attributedText = attri;
}

@end
