//
//  ChatDetailCell.h
//  App3.0
//
//  Created by mac on 17/3/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatDetailCell : UITableViewCell
- (void)setMessage:(EMMessage *)message;
@end
