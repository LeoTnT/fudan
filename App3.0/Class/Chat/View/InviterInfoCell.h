//
//  InviterInfoCell.h
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatModel.h"

@interface InviterInfoCell : UITableViewCell
@property (strong, nonatomic) InviteInfoModel *inviteModel;
+ (instancetype)createInviterInfoCellWithTableView:(UITableView *)tableView;
@end
