//
//  ChatListCell.h
//  App3.0
//
//  Created by mac on 17/2/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConversationModel.h"

@protocol ChatListCellDelegate

@end

@interface ChatListCell : UITableViewCell
@property (nonatomic, weak) id<ChatListCellDelegate> delegate;
- (void)setChatListCellData:(ConversationModel *)model;
- (void)setFindChatData:(id)message;
@end
