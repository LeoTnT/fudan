//
//  PersonalInfoCell.m
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PersonalInfoCell.h"
#import "UUImageAvatarBrowser.h"

@interface PersonalInfoCell()
{
    UIImageView *_avatar;
    UILabel *_userNo;
    UILabel *_userName;
    UIButton *_remark;
    __weak id<PersonalInfoCellDelegate>delegate;
}
@end

@implementation PersonalInfoCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        CGFloat leftSpace = 10;
        // 头像
        _avatar = [[UIImageView alloc] init];
        _avatar.userInteractionEnabled = YES;
        [_avatar setImage:[UIImage imageNamed:@"user_fans_avatar"]];
        _avatar.contentMode = UIViewContentModeScaleAspectFit;
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 5;
        [self addSubview:_avatar];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            [UUImageAvatarBrowser showImage:_avatar title:@""];
        }];
        [_avatar addGestureRecognizer:tap];
        
        // 用户编号
        _userNo = [[UILabel alloc] init];
        _userNo.text = @"";
        _userNo.textColor = [UIColor blackColor];
        _userNo.font = [UIFont systemFontOfSize:16];
        [self addSubview:_userNo];
        
        //用户昵称
        _userName = [[UILabel alloc] init];
        _userName.text = @"";
        _userName.textColor = COLOR_999999;
        _userName.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userName];
        
        // 设置备注
        _remark = [UIButton buttonWithType:UIButtonTypeCustom];
        [_remark setBackgroundImage:[UIImage imageNamed:@"remark_set"] forState:UIControlStateNormal];
        [_remark addTarget:self action:@selector(remarkAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_remark];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.top.mas_equalTo(self).offset(leftSpace);
            make.size.mas_equalTo(CGSizeMake(65, 65));
        }];
        
        [_userNo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(leftSpace);
            make.top.mas_equalTo(self).offset(22.5);
            make.right.mas_lessThanOrEqualTo(self).offset(-12);
        }];
        
        [_userName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(leftSpace);
            make.bottom.mas_equalTo(self).offset(-22);
            make.right.mas_lessThanOrEqualTo(self).offset(-12);
        }];
        
        [_remark mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_userName.mas_right).offset(10);
            make.centerY.mas_equalTo(_userName.mas_centerY);
        }];
    }
    return self;
}

- (void)setCellData:(UserInfoDataParser *)model
{
    [_avatar getImageWithUrlStr:isEmptyString(model.logo_source)?model.logo:model.logo_source andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    _userNo.text = isEmptyString(model.nickname)?model.username:model.nickname;
    _userName.text = isEmptyString(model.remark)?@"设置备注":model.remark;
}

- (void)remarkAction:(UIButton *)sender {
    if (delegate && [delegate respondsToSelector:@selector(remarkClick)]) {
        [delegate remarkClick];
    }
}
@end
