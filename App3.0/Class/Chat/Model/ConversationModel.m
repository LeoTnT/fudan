//
//  ConversationModel.m
//  App3.0
//
//  Created by mac on 17/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ConversationModel.h"
#import "XMPPCoredataChatContact+CoreDataProperties.h"

@implementation ConversationModel

- (instancetype)initWithConversation:(id)conversation
{
    self = [super init];
    if (self) {
        _conversation = conversation;
        
        if ([conversation isKindOfClass:[XMPPMessageArchiving_Contact_CoreDataObject class]]) {
            
            XMPPMessageArchiving_Contact_CoreDataObject *contact = conversation;
            @try {
                _conversation = contact.bareJidStr;
                _lastestDate = contact.mostRecentMessageTimestamp;
                _lastestMessageStr = contact.mostRecentMessageBody;
                _bareJID = contact.bareJid;
                
            }@catch (NSException *exception){
                
                
            }
            if ([contact.bareJidStr containsString:XMMPP_BASESTR]) {
                _chatType = UUChatTypeChat;
            }else{
                _chatType = UUChatTypeChatRoom;
                
            }
            
        }else {
            if ([conversation isKindOfClass:[XMPPRoomManager class]]) {
                XMPPRoomManager *model = conversation;
                XMPPJID *jid = model.xmppRoom.roomJID;
                _conversation = jid.full;
                _chatType = UUChatTypeChatRoom;
                _bareJID = jid;
                _xmppRoomManager = model;
            }
            
            
            if ([conversation isKindOfClass:[SiganlChatModel class]]) {
                SiganlChatModel *model = conversation;
                _conversation = model.conversationID;
                _title = model.title;
                _avatarURLPath = model.avatarURLPath;
                _bareJID = model.bareJID;
                _userName = model.userName;
                _relation = model.relation;
                
            }
            
            
        }
    }
    
    return self;
}



- (instancetype)initWithMessage:(XMPPMessageArchiving_Message_CoreDataObject *)object {
    
    if (self = [super init]) {
        
        _conversation = object.bareJidStr;
        UUMessage *uumesage = object.uumessage;
        _lastestMessageStr = uumesage.strContent;
        if (object.isOutgoing) {
            _bareJID = [XMPPJID jidWithString:getJID([UserInstance ShardInstnce].uid)];
        } else {
            _bareJID = object.bareJid;
        }

        _relation = uumesage.relation;
        _lastestDate = object.timestamp;
        
    }
    return self;
}


- (instancetype)initWithSiganlChatModel:(SiganlChatModel *)model {
    if (self = [super init]) {
        
        _conversation = model.conversationID;
        _title = model.title;
        _avatarURLPath = model.avatarURLPath;
        _bareJID = model.bareJID;
        
    }
    return self;
}

- (instancetype) initWithContact:(XMPPMessageArchiving_Contact_CoreDataObject *)contact {
    if (self = [super init]) {
        
        _conversation = contact.bareJidStr;
        _lastestMessageStr = contact.mostRecentMessageBody;
        [contact.bareJidStr containsString:XMMPP_BASESTR] ? [self signalWithContactID:contact.bareJid.user]:[self groupWithContactID:contact.bareJid.user];
        
    }
    return self;
}




- (void) signalWithContactID:(NSString *)UID  {
    ConversationModel *model = self;
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:UID];
    if (contacts != nil && contacts.count > 0) {
        // 本地数据库存在
        ContactDataParser *contact = contacts[0];
        model.title = [contact getName];
        model.avatarURLPath = contact.avatar;
        model.userName = contact.username;
        model.relation = contact.relation;
        //        [_dataArr  addObject:model];
        
    } else {
        // 本地数据库不存在
        
        [HTTPManager getUserInfoWithUid:UID success:^(NSDictionary * dic, resultObject *state) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            
            if (state.status) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                    
                    // 会话对象是机器人特殊处理
                    if ([UID isEqualToString:@"robot"]) {
                        cdParser.username = Localized(@"系统消息");
                        cdParser.remark = Localized(@"系统消息");
                        cdParser.nickname = Localized(@"系统消息");
                        model.title = Localized(@"系统消息");
                        
                    } else {
                        cdParser.username = parser.data.username;
                        cdParser.remark = parser.data.remark;
                        cdParser.nickname = parser.data.nickname;
                        model.title = [parser.data getName];
                    }
                    cdParser.uid = UID;
                    cdParser.avatar = parser.data.logo;
                    cdParser.relation = parser.data.relation;
                    cdParser.mobile = parser.data.mobile;
                    
                    model.avatarURLPath = parser.data.logo;
                    model.userName = parser.data.username;
                    model.relation = parser.data.relation;
                    // 防止同一时间刷新两次列表造成数据重复添加
                    NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                    if (contactT.count == 0) {
                        //                        [_dataArr  addObject:model];
                    }
                    // 添加数据库操作放在最后
                    [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                });
            }
        } fail:^(NSError * _Nonnull error) {
            
        }];
    }
}



- (void) groupWithContactID:(NSString *)UID  {
    
    ConversationModel *model  = self;
    NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:UID];
    if (groupDBArray != nil && groupDBArray.count > 0) {
        GroupDataModel *parser = groupDBArray[0];
        model.title = parser.name;
        model.avatarURLPath = parser.avatar;
        model.member_count =parser.member_count;
        
    } else {
        [self reloadGroupMenberWithID:UID];
    }
}



- (void) reloadGroupMenberWithID:(NSString *)UID {
    
    if (isEmptyString(UID)) return;
    ConversationModel *model  = self;
    [HTTPManager fetchGroupInfoWithGroupId:UID getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
        XMPP_LOG(@"state infor =%@ UID =%@",state.info,UID)
        GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
        if (state.status) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                model.title = parser.data.name;
                model.avatarURLPath = parser.data.avatar;
                model.member_count =parser.data.member_count;
                // 防止同一时间刷新两次列表造成数据重复添加
                [[DBHandler sharedInstance] getGroupByGroupId:parser.data.gid];
                // 添加数据库操作放在最后
                [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
            });
            
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}

- (void) getUserInfor:(NSString *)ID {
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:ID];
    if (contacts != nil && contacts.count > 0) {
        ContactDataParser *contact = contacts[0];
        self.title =[contact getName];
        self.avatarURLPath = contact.avatar;
        self.userName = contact.username;
        self.relation = contact.relation;
        
        
    }
}


- (void) getGroupInfor:(NSString *)UID {
    NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:UID];
    if (groupDBArray != nil && groupDBArray.count > 0) {
        GroupDataModel *parser = groupDBArray[0];
        self.title = parser.name;
        self.avatarURLPath = parser.avatar;
        self.member_count =parser.member_count;
    }
}

@end

@implementation SiganlChatModel

- (instancetype)init {
    if (self = [super init]) {
        _chatType = UUChatTypeChat;
    }
    return self;
}


- (instancetype)initWithChatWithJID:(NSString *)conversationID title:(NSString *)title avatarURLPath:(NSString *)avatarURLPath{
    
    if (self = [super init]) {
        
        _title = title;
        _conversationID = conversationID;
        _avatarURLPath = avatarURLPath;
        _bareJID = xmppJID(conversationID);
        _chatType = UUChatTypeChat;
    }
    return self;
    //    @property (strong, nonatomic) NSString *title;
    //
    //    @property (strong, nonatomic) NSString *avatarURLPath;
    //
    //    @property (strong, nonatomic) XMPPJID *bareJID;
}
@end
