//
//  ChatModel.h
//  UUChatTableView
//
//  Created by shake on 15/1/6.
//  Copyright (c) 2015年 uyiuyao. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UUMessage.h"
#import "UUMessageFrame.h"
@interface InvitePersonModel : NSObject
@property (copy, nonatomic) NSString *user_nick;        // 被邀请人
@property (copy, nonatomic) NSString *user_avatar;      // 被邀请人头像
@property (nonatomic,copy) NSString *user_id;  // UID
@end

@interface InviteInfoModel : NSObject
@property (copy, nonatomic) NSString *invite_id;
@property (copy, nonatomic) NSString *user_nick;        // 邀请人
@property (copy, nonatomic) NSString *user_avatar;      // 邀请人头像
@property (copy, nonatomic) NSString *invite_num;
@property (strong, nonatomic) NSArray *target_users;    // 所有被邀请人
@end

@interface ChatModel : NSObject
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic) BOOL isGroupChat;

- (void)addMessageItems:(NSDictionary *)dic;
- (void)refreshImageMessageItems:(NSDictionary *)dic;
- (void)refreshVoiceMessageItems:(NSDictionary *)dic;
- (void)insertMessageItems:(NSDictionary *)dic index:(NSInteger)index;

- (UUMessageFrame *) messageFrameWith:(XMPPRoomMessageCoreDataStorageObject *)oject refreshTime:(BOOL)refresh ;

- (UUMessageFrame *) messageWithSignaleMessage:(XMPPMessageArchiving_Message_CoreDataObject *)object  refreshTime:(BOOL)refresh ;

- (UUMessageFrame *) messageWithUUmessage:(UUMessage *)uumessage refreshTime:(BOOL)refresh;
@end
