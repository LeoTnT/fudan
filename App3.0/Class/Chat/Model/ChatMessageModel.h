//
//  ChatMessageModel.h
//  App3.0
//
//  Created by mac on 17/3/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatMessageModel : NSObject

// 发送文字消息
+ (void)sendTextMessageWithConversationID:(NSString *)aId
                                     text:(NSString *)aText
                                       to:(NSString *)aTo
                                 progress:(void (^)(int progress))aProgressBlock
                               completion:(void (^)(EMMessage *message, EMError *error))aCompletionBlock;

// 发送图片消息
+ (void)sendImageMessageWithConversationID:(NSString *)aId
                                 imageData:(NSData *)aData
                               displayName:(NSString *)aName
                                        to:(NSString *)aTo
                                  progress:(void (^)(int progress))aProgressBlock
                                completion:(void (^)(EMMessage *message, EMError *error))aCompletionBlock;

// 发送语音消息
+ (void)sendVoiceMessageWithConversationID:(NSString *)aId
                                 localPath:(NSString *)aPath
                               displayName:(NSString *)aName
                                  duration:(NSString *)duration
                                        to:(NSString *)aTo
                                  progress:(void (^)(int progress))aProgressBlock
                                completion:(void (^)(EMMessage *message, EMError *error))aCompletionBlock;

// 发送位置消息
+ (void)sendLocationMessageWithConversationID:(NSString *)aId
                                     latitude:(double)latitude
                                    longitude:(double)longitude
                                   andAddress:(NSString *)address
                                           to:(NSString *)aTo
                                     progress:(void (^)(int progress))aProgressBlock
                                   completion:(void (^)(EMMessage *message, EMError *error))aCompletionBlock;
@end
