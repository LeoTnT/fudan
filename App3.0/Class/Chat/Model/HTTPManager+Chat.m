//
//  HTTPManager+Chat.m
//  App3.0
//
//  Created by mac on 2017/12/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HTTPManager+Chat.h"

@implementation HTTPManager (Chat)
+ (void)getMemberInviteInfoWithInviteId:(NSString *)invite_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_MemberInviteInfo parameters:@{@"invite_id":invite_id} success:success failure:fail];
}

+ (void)verifyMemberInviteWithInviteId:(NSString *)invite_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_MemberInviteAudit parameters:@{@"invite_id":invite_id} success:success failure:fail];
}
@end
