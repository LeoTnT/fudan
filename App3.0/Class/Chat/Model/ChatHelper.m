//
//  ChatHelper.m
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatHelper.h"

#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ChatModel.h"
#import "RedPacketModel.h"
#import "AESCrypt.h"
#import "UUMessage.h"
#import "ChatCallManager.h"

#import <UserNotifications/UserNotifications.h>
@interface ChatHelper()<UNUserNotificationCenterDelegate>
{
    SystemSoundID soundID;
}
@property (nonatomic, strong) NSDateFormatter* formatter;
@property (nonatomic, strong) NSTimer* protectTimer;
@end
static ChatHelper *helper = nil;
@implementation ChatHelper
{
    NSInteger badge;
}


+ (instancetype)shareHelper
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[ChatHelper alloc] init];
    });
    return helper;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

#pragma mark - init

- (void)initHelper
{
#ifdef REDPACKET_AVALABLE
    [[RedPacketUserConfig sharedConfig] beginObserveMessage];
#endif
    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].groupManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].contactManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].roomManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    
    // 添加红包丢失信号监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(haveLostRedpacket) name:@"haveLostRedpacket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(systemMaintenance) name:@"systemMaintenance" object:nil];
    
    [ChatCallManager sharedManager];
    
    [self _registerRemoteNotification];
    
    self.previousTime = nil;
    // 保活处理
//    self.protectTimer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(protectMyApp) userInfo:nil repeats:YES];
}

- (void)protectMyApp {
    
}

- (void)setBadgeNumber:(NSInteger)number {
    badge = number;
}

- (NSInteger)badgeNumber {
    return badge;
}

- (void)setReplyVC:(FDReplyListController *)replyVC {
    _replyVC = replyVC;
}

//- (void)setTabChatVC:(TabChatVC *)tabChatVC {
//    _tabChatVC = tabChatVC;
////    [[ChatCallManager sharedManager] setMainController:tabChatVC];
//}

- (void)sendSystemPushMessage:(NSDictionary *)data {
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:data[@"content"]];
    NSDictionary *extDic = @{MSG_TYPE:MESSAGE_SYSTEM_PUSH};
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:@"robot" from:@"robot" to:[UserInstance ShardInstnce].uid body:body ext:extDic];
    message.direction = EMMessageDirectionReceive;
    message.chatType = EMChatTypeChat;// 设置消息类型
    
//    @weakify(self);
    //插入消息
    
    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:@"robot" type:EMConversationTypeChat createIfNotExist:YES];
    [conver insertMessage:message error:nil];
    [self messagesDidReceive:@[message]];
    if (_chatVC && [_chatVC.conversation.conversationId isEqualToString:@"robot"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
    }
    
//    [[EMClient sharedClient].chatManager importMessages:@[message] completion:^(EMError *aError) {
//        if (!aError) {
//            @strongify(self);
//            // 导入消息成功
//            if (self.tabChatVC) {
//                [_tabChatVC refreshConversationLists];
//            }
//            if (_chatVC) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
//            }
//        }
//    }];
}

- (void)systemMaintenance {
    // 系统维护
    NSString *str = isEmptyString([AppConfigManager ShardInstnce].appConfig.app_disable_msg)?@"系统维护中":[AppConfigManager ShardInstnce].appConfig.app_disable_msg;
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:str preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        exit(0);
    }];
    [alert addAction:action1];
    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)haveLostRedpacket {
    [HTTPManager getRedPacketConfirmListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            LostRedPacketModel *model = [LostRedPacketModel mj_objectWithKeyValues:dic[@"data"]];
            if (!model) {
                return;
            }
            // 补发红包消息
            NSString *text = model.remark;
            EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
            NSDictionary *extDic;
            NSString *from = model.user_id;
            if (model.flag == 1) {
                // 红包丢失
                if (model.is_transfer) {
                    extDic = @{MSG_TYPE:TRANSFER_BANLANCE,CUSTOM_MSG_ID:model.red_packet_id,TRANSFER_BANLANCE_TAG:model.money};
                } else {
                    extDic = @{MSG_TYPE:READ_PACKET,CUSTOM_MSG_ID:model.red_packet_id};
                }
            } else {
                // 领取红包消息丢失
                from = model.from_uid;
                if (model.is_transfer) {
                    extDic = @{MSG_TYPE:TRANSFER_MONEY_COLLECTION,CUSTOM_MSG_ID:model.red_packet_id,TRANSFER_BANLANCE_TAG:model.money};
                } else {
                    extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:model.red_packet_id};
                }
            }
            
            // 生成message
            EMMessage *message = [[EMMessage alloc] initWithConversationID:from from:from to:[UserInstance ShardInstnce].uid body:body ext:extDic];
            message.chatType = (model.target==1?EMChatTypeChat:EMChatTypeGroupChat);// 设置消息类型
            
            // 导入消息到DB不会及时刷新，只有重新登录才能刷新
//            @weakify(self);
//            //发送消息
//            [[EMClient sharedClient].chatManager importMessages:@[message] completion:^(EMError *aError) {
//                if (!aError) {
//                    @strongify(self);
//                    // 导入消息成功
//                    // 向服务器发送确认消息
//                    [self confirmMessageReceive:message];
//                }
//            }];
            // 所以用插入消息
            EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:model.user_id type:model.target-1 createIfNotExist:YES];
            [conver insertMessage:message error:nil];
            [self messagesDidReceive:@[message]];
            if (_chatVC && [_chatVC.conversation.conversationId isEqualToString:model.user_id]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }
            [self confirmMessageReceive:message];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (NSString *)latestMessageTitleWithMessage:(id)message
{
#ifdef ALIYM_AVALABLE
    
#elif defined EMIM_AVALABLE
    EMMessage *emMessage = (EMMessage *)message;
    if (emMessage) {
        NSString *latestMessageTitle = @"";
        
        if (emMessage.ext[MSG_TYPE]) {
            if ([emMessage.ext[MSG_TYPE] isEqualToString:REVOKE_ACTION]) {
                latestMessageTitle = [self noticeStringConvertByMessage:emMessage];
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:MESSAGE_FOCUS_TYPE]) {
                latestMessageTitle = [self noticeStringConvertByMessage:emMessage];
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:READ_PACKET_BACK]) {
                latestMessageTitle = [self noticeStringConvertByMessage:emMessage];
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:GROUP_INVITE_NEED_CONFIRM]) {
                latestMessageTitle = [self noticeStringConvertByMessage:emMessage];
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
                latestMessageTitle = [self noticeStringConvertByMessage:emMessage];
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:MESSAGE_SYSTEM_PUSH]) {
                UUMessageSystemPushModel *model = [UUMessageSystemPushModel mj_objectWithKeyValues:((EMTextMessageBody *)emMessage.body).text];
                latestMessageTitle = model.title;
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VOICE_CALL]) {
                
                latestMessageTitle = @"[语音消息]";
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VIDEO_CALL]) {
                
                latestMessageTitle = @"[视频消息]";
            } else if ([emMessage.ext[MSG_TYPE] isEqualToString:MESSAGE_SHARE_USER]) {
                latestMessageTitle = @"[名片]";
            } else {
                NSString *txt = [AESCrypt decrypt:((EMTextMessageBody *)emMessage.body).text password:[NSString stringWithFormat:@"%@%@",AES_KEY,emMessage.to]];
                if (isEmptyString(txt)) {
                    txt = ((EMTextMessageBody *)emMessage.body).text;
                }
                latestMessageTitle = txt;
            }
            return latestMessageTitle;
        }
        
        EMMessageBody *messageBody = emMessage.body;
        switch (messageBody.type) {
            case EMMessageBodyTypeImage:{
                latestMessageTitle = @"[图片]";
            } break;
            case EMMessageBodyTypeText:{
                if ([emMessage.from isEqualToString:@"robot"]) {
                    // 系统消息
                    UUMessageSystemNoticeModel *model = [UUMessageSystemNoticeModel mj_objectWithKeyValues:((EMTextMessageBody *)messageBody).text];
                    if (model == nil) {
                        latestMessageTitle = ((EMTextMessageBody *)messageBody).text;
                    } else {
                        latestMessageTitle = model.type_title;
                    }
                    
                    break;
                }
                NSString *txt = [AESCrypt decrypt:((EMTextMessageBody *)messageBody).text password:[NSString stringWithFormat:@"%@%@",AES_KEY,emMessage.to]];
                if (isEmptyString(txt)) {
                    txt = ((EMTextMessageBody *)messageBody).text;
                }
                latestMessageTitle = txt;
            } break;
            case EMMessageBodyTypeVoice:{
                latestMessageTitle = @"[语音]";
            } break;
            case EMMessageBodyTypeLocation: {
                latestMessageTitle = @"[位置]";
            } break;
            case EMMessageBodyTypeVideo: {
                latestMessageTitle = @"[视频]";
            } break;
            case EMMessageBodyTypeFile: {
                latestMessageTitle = @"[文件]";
            } break;
            default: {
            } break;
        }
        
        return latestMessageTitle;
    }
    return nil;
#else
    return nil;
#endif
    
}

- (NSString *)noticeStringConvertByMessage:(id)message
{
    NSString *str = @"";
#ifdef ALIYM_AVALABLE
    
#elif defined EMIM_AVALABLE
    EMMessage *emMessage = (EMMessage *)message;
    NSDictionary *ext = emMessage.ext;
    if (!ext) {
        return @"";
    }
    if ([emMessage.body isKindOfClass:[EMCmdMessageBody class]]) {
        if ([((EMCmdMessageBody *)emMessage.body).action isEqualToString:REVOKE_ACTION]) {
            if (emMessage.direction == EMMessageDirectionSend) {
                str = @"你撤回了一条消息";
            } else {
                str = [NSString stringWithFormat:@"%@撤回了一条消息",[self displayNameByUid:emMessage.from]];
            }
        }
    } else {
        if ([ext[MSG_TYPE] isEqualToString:REVOKE_ACTION]) {
            if (emMessage.direction == EMMessageDirectionSend) {
                str = @"你撤回了一条消息";
            } else {
                str = [NSString stringWithFormat:@"%@撤回了一条消息",[self displayNameByUid:emMessage.from]];
            }
        } else if ([ext[MSG_TYPE] isEqualToString:READ_PACKET_BACK]) {
            EMTextMessageBody *textBody = (EMTextMessageBody *)emMessage.body;
            if (emMessage.chatType == EMChatTypeChat) {
                if (emMessage.direction == EMMessageDirectionSend) {
                    str = [NSString stringWithFormat:@"你领取了%@的红包",[self displayNameByUid:emMessage.from]];
                } else {
                    str = [NSString stringWithFormat:@"%@领取了你的红包",[self displayNameByUid:emMessage.from]];
                }
            } else {
                if (emMessage.direction == EMMessageDirectionSend) {
                    str = [NSString stringWithFormat:@"你%@",textBody.text];
                } else {
                    // 谁领取了我的红包只能自己看见
                    if ([ext[MESSAGE_PACK_FROM] isEqualToString:[UserInstance ShardInstnce].uid]) {
                        str = [NSString stringWithFormat:@"%@%@",[self displayNameByUid:emMessage.from],textBody.text];
                    } else {
                        str = nil;
                    }
                }
            }
        } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_FOCUS_TYPE]) {
            EMTextMessageBody *textBody = (EMTextMessageBody *)emMessage.body;
            if (emMessage.direction == EMMessageDirectionSend) {
                str = [NSString stringWithFormat:@"你关注了%@,快来聊天吧",[self displayNameByUid:emMessage.to]];
            } else {
                str = textBody.text;
            }
        } else if ([ext[MSG_TYPE] isEqualToString:GROUP_INVITE_NEED_CONFIRM] || [ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
            if ([ext[GROUP_INVITE_OWNER] isEqualToString:[UserInstance ShardInstnce].uid]) {
                // 是群主
                EMTextMessageBody *textBody = (EMTextMessageBody *)emMessage.body;
                
                str = [NSString stringWithFormat:@"%@ 去确认",textBody.text];
                if ([ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
                    str = [NSString stringWithFormat:@"%@ 已确认",textBody.text];
                }
            } else {
                str = nil;
            }
            
        } else {
            EMTextMessageBody *textBody = (EMTextMessageBody *)emMessage.body;
            str = textBody.text;
        }
    }
#else
    
#endif
    
    
    
    return str;
}

- (NSString *)displayNameByUid:(NSString *)uid
{
    NSString *disname = uid;
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:uid];
    if (contacts != nil && contacts.count > 0) {
        // 本地数据库存在
        ContactDataParser *contact = contacts[0];
        disname = [contact getName];
    } else {
        NSMutableArray *mutArray = [[DBHandler sharedInstance] getMemberByMemberId:uid];
        if (mutArray != nil && mutArray.count > 0) {
            // 本地数据库存在
            NickNameDataParser *model = mutArray[0];
            disname = model.nickname;
        }
    }
    return disname;
}

+ (NSNumber *) isfriendWithUid:(NSString *)uid {
    
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:uid];
    if (contacts != nil && contacts.count > 0) {
        // 本地数据库存在
        ContactDataParser *contact = contacts[0];
        return contact.relation;
    } else {
        return @0;
    }
    
}

#pragma mark - EMClientDelegate
// 账号在其他设备登录
- (void)userAccountDidLoginFromOtherDevice
{
    [self _clearHelper];
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"err_user_login_another_device") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }];
    [alert addAction:action1];
    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

// 账号被服务器删除
- (void)userAccountDidRemoveFromServer
{
    [self _clearHelper];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"账号被删除" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }];
    [alert addAction:action1];
    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)connectionStateDidChange:(EMConnectionState)aConnectionState
{
//    [self.tabChatVC networkChanged:aConnectionState];
    [self.replyVC networkChanged:aConnectionState];
}

- (ChatViewController*)getCurrentChatView
{
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.replyVC.navigationController.viewControllers];
    ChatViewController *chatViewContrller = nil;
    for (id viewController in viewControllers)
    {
        if ([viewController isKindOfClass:[ChatViewController class]])
        {
            chatViewContrller = viewController;
            break;
        }
    }
    return chatViewContrller;
}
//调用震动
-(void)systemShake
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

//调用系统铃声
-(void)createSystemSoundWithName:(NSString *)soundName soundType:(NSString *)soundType vibrate:(BOOL)vibrate
{
    // 如果正在播放声音则返回
    if (soundID) {
        return;
    }
    
    //NSBundle来返回音频文件路径
    NSString *soundFile = [[NSBundle mainBundle] pathForResource:soundName ofType:soundType];
    //建立SystemSoundID对象，但是这里要传地址(加&符号)。 第一个参数需要一个CFURLRef类型的url参数，要新建一个NSString来做桥接转换(bridge)，而这个NSString的值，就是上面的音频文件路径
//    SystemSoundID soundID;
    
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof (doChangeDefaultRoute), &doChangeDefaultRoute);
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundFile], &soundID);
    
    if (vibrate) {
        //播放提示音 带震动
        if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0f) {
            AudioServicesPlayAlertSoundWithCompletion(soundID, ^{
                soundID = 0;
            });
        } else {
            AudioServicesPlayAlertSound(soundID);
            soundID = 0;
        }
        
    } else {
        //播放提示音 不带震动
        if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0f) {
            AudioServicesPlaySystemSoundWithCompletion(soundID, ^{
                soundID = 0;
            });
        } else {
            AudioServicesPlaySystemSound(soundID);
            soundID = 0;
        }
        
    }
    
}

- (void)stopSystemSound {
    if (!soundID) {
        return;
    }
    AudioServicesDisposeSystemSoundID(soundID);
}

- (void)confirmMessageReceive:(EMMessage *)message {
    // 收到红包消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
    if (message.chatType == EMChatTypeChat && message.ext[MSG_TYPE]) {
        NSLog(@"chat helper : msg_type:%@",message.ext[MSG_TYPE]);
        
        // 收到红包消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
        if ([message.ext[MSG_TYPE] isEqualToString:READ_PACKET]  && !message.ext[READ_PACKET_HANDLE]) {
            NSLog(@"chat helper : 收到红包id是-- %@",message.ext[CUSTOM_MSG_ID]);
            NSString *rpId = message.ext[CUSTOM_MSG_ID];
            [HTTPManager confirmSendRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 服务器已确认红包发到
                    NSLog(@"chat helper : 服务器已确认红包发到");
                    message.ext = @{MSG_TYPE:READ_PACKET,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
                    
                    // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
                    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:YES];
                    [conver updateMessageChange:message error:nil];
                } else{
                    
                }
            } fail:^(NSError *error) {
                
            }];
        }
        
        // 收到红包已被领取消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
        if ([message.ext[MSG_TYPE] isEqualToString:READ_PACKET_BACK]  && !message.ext[READ_PACKET_HANDLE]) {
            NSLog(@"chat helper : 收到已领取红包id是-- %@",message.ext[CUSTOM_MSG_ID]);
            NSString *rpId = message.ext[CUSTOM_MSG_ID];
            [HTTPManager confirmReceiveRedPacketWithId:rpId receiver:message.from success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 服务器已确认红包被领取
                    NSLog(@"chat helper : 服务器已确认红包被领取");
                    message.ext = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
                    
                    // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
                    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:YES];
                    [conver updateMessageChange:message error:nil];
                } else{
                    
                }
            } fail:^(NSError *error) {
                
            }];
        }
        
        // 收到转账消息向服务器确认(只确认一次，需要判断此条转账消息是否已经确认)
        if ([message.ext[MSG_TYPE] isEqualToString:TRANSFER_BANLANCE]  && !message.ext[READ_PACKET_HANDLE]) {
            NSLog(@"chat helper : 收到红包id是-- %@",message.ext[CUSTOM_MSG_ID]);
            NSString *rpId = message.ext[CUSTOM_MSG_ID];
            NSString *money = message.ext[TRANSFER_BANLANCE_TAG];
            [HTTPManager confirmSendRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 服务器已确认红包发到
                    NSLog(@"chat helper : 服务器已确认红包发到");
                    message.ext = @{MSG_TYPE:TRANSFER_BANLANCE,TRANSFER_BANLANCE_TAG:money,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
                    
                    // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
                    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:YES];
                    [conver updateMessageChange:message error:nil];
                } else{
                    
                }
            } fail:^(NSError *error) {
                
            }];
        }
        
        // 收到转账已被领取消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
        if ([message.ext[MSG_TYPE] isEqualToString:TRANSFER_MONEY_COLLECTION]  && !message.ext[READ_PACKET_HANDLE]) {
            NSLog(@"chat helper : 收到已领取红包id是-- %@",message.ext[CUSTOM_MSG_ID]);
            NSString *rpId = message.ext[CUSTOM_MSG_ID];
            NSString *money = message.ext[TRANSFER_BANLANCE_TAG];
            [HTTPManager confirmReceiveRedPacketWithId:rpId receiver:message.from success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 服务器已确认红包被领取
                    NSLog(@"chat helper : 服务器已确认红包被领取");
                    message.ext = @{MSG_TYPE:READ_PACKET_BACK,TRANSFER_BANLANCE_TAG:money,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
                    
                    // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
                    EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:YES];
                    [conver updateMessageChange:message error:nil];
                } else{
                    
                }
            } fail:^(NSError *error) {
                
            }];
        }
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages
{
    BOOL isRefreshCons = YES;
    for(EMMessage *message in aMessages) {
        if (message.ext[MSG_TYPE]) {
            // 如果是自己不能看见的消息，就删除
            if (isEmptyString([self noticeStringConvertByMessage:message])) {
                // 删除
                EMConversation *con = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:NO];
                [con deleteMessageWithId:message.messageId error:nil];
                continue;
            }
        }
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];

        // 免打扰判断
        NSInteger isNotDisturb = 0;
        
        // 先判断总体设置
        ChatSetModel *model = [[UserInstance ShardInstnce] getChatSet];
        if (model.is_not_disturb==1) {
            NSDate *currentDate = [NSDate date];
            NSString *currentStr = [self.formatter stringFromDate:currentDate];
            NSString *hourTime = [currentStr substringWithRange:NSMakeRange(0, 2)];
            NSString *mintTime = [currentStr substringWithRange:NSMakeRange(currentStr.length-2, 2)];
            int currtTime = hourTime.intValue*60+mintTime.intValue;
            
            int  disturb_end =[model.not_disturb_end substringWithRange:NSMakeRange(0, 2)].intValue*60+[model.not_disturb_end substringWithRange:NSMakeRange(model.not_disturb_end.length-2, 2)].intValue;
            
            
            int  disturb_begin =[model.not_disturb_begin substringWithRange:NSMakeRange(0, 2)].intValue*60+[model.not_disturb_begin substringWithRange:NSMakeRange(model.not_disturb_begin.length-2, 2)].intValue;

            if (disturb_end >disturb_begin) {
                if (disturb_end >currtTime && disturb_begin <currtTime ) {
                    isNotDisturb =1;
                }
            }else{
                if (disturb_begin < currtTime) {
                    isNotDisturb =1;

                }else if (disturb_end >currtTime){
                    isNotDisturb =1;

                }
                
            }
        
        }
        
        // 在判断单独联系人和群设置
        if (message.chatType == EMChatTypeChat) {
            // 从数据库获取联系人，判断是否免打扰
            NSArray *arr = [[DBHandler sharedInstance] getContactByUid:message.from];
            if (arr && arr.count > 0) {
                ContactDataParser *contact = arr[0];
                isNotDisturb = [contact.is_not_disturb integerValue];
            }
        } else if (message.chatType == EMChatTypeGroupChat) {
            // 从数据库获取群组，判断是否免打扰
            NSArray *arr = [[DBHandler sharedInstance] getGroupByGroupId:message.conversationId];
            if (arr && arr.count > 0) {
                GroupDataModel *group = arr[0];
                isNotDisturb = [group.is_not_disturb integerValue];
            }
        }
        
        if (isNotDisturb == 0) {
            switch (state) {
                case UIApplicationStateActive:
                case UIApplicationStateInactive:
                {
                    
                    BOOL isVibrate = NO;
                    if (model.is_rec_msg) {
                        
                                if (model.is_voice == 1) {
                                    isVibrate = YES;
                                    if (model.is_vibrate == 0) {
                                        isVibrate = NO;
                                    }else{
                                        isVibrate = YES;
                    
                                    }
                    
                                    [[ChatHelper shareHelper] createSystemSoundWithName:@"message" soundType:@"wav" vibrate:isVibrate];
                                }else{
                                    if (model.is_vibrate == 1) {
                                        [self systemShake];

                                    }
                                    isVibrate = NO;
                            
                                }
                    
                    }

                }
                    break;
                case UIApplicationStateBackground:
                {
                    //发送本地推送
                    [self localNotificationWithAlertBody:[self latestMessageTitleWithMessage:message] soundName:@""];
                }
                    break;
                default:
                    break;
            }
        }

        [self confirmMessageReceive:message];
        
        if (_chatVC == nil) {
            _chatVC = [self getCurrentChatView];
        }
        BOOL isChatting = NO;
        if (_chatVC) {
            isChatting = [message.conversationId isEqualToString:_chatVC.conversation.conversationId];
        }
        if (_chatVC == nil || !isChatting || state == UIApplicationStateBackground) {
            
            if (self.replyVC) {
                [self.replyVC refreshConversationLists];
            }
            return;
        }
        if (isChatting) {
            isRefreshCons = NO;
            [_chatVC.conversation markAllMessagesAsRead:nil];
        }
        
        
    }
    if (isRefreshCons) {
        if (self.replyVC) {
            [self.replyVC refreshConversationLists];
        }
        
    }
}
-(NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter =[[NSDateFormatter alloc] init];
        [_formatter setDateFormat:@"HH:mm"];
        
    }
    return _formatter;
}
// 接收到一条及以上cmd(透传)消息
- (void)cmdMessagesDidReceive:(NSArray *)aCmdMessages
{
    for (EMMessage *message in aCmdMessages) {
        EMCmdMessageBody *body = (EMCmdMessageBody *)message.body;
        NSLog(@"收到的action是 -- %@",body.action);
        if ([body.action isEqualToString:MESSAGE_RELATION_CHANGE]) {
            // 关系改变
            [self.friendVC getRelationsData];
        }
    }
}

- (void)_clearHelper
{
    self.replyVC = nil;
    self.chatVC = nil;
    self.transVC = nil;
    self.friendVC = nil;
    [[EMClient sharedClient] logout:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)localNotificationWithAlertBody:(NSString *)alertBody soundName:(NSString *)sound {
    
    NSString *version = [UIDevice currentDevice].systemVersion;
    if ([version integerValue] >=10) {
        
        [self locationUNUserNotificationCenterWithAlertBody:alertBody soundName:sound];
    }else{
        
        [self registerLocalNotificationInOldWayWithAlertBody:alertBody soundName:sound];
    }
}

#pragma mark - register apns

/** @brief 注册远程通知 */
- (void)_registerRemoteNotification
{
    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    
    if (NSClassFromString(@"UNUserNotificationCenter")) {
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert completionHandler:^(BOOL granted, NSError *error) {
            if (granted) {
#if !TARGET_IPHONE_SIMULATOR
                dispatch_async(dispatch_get_main_queue(), ^{
                    [application registerForRemoteNotifications];
                });
                
#endif
            }
        }];
        return;
    }
    
    if([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
#if !TARGET_IPHONE_SIMULATOR
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
    }else{
        /// 去除warning
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound |
        UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
#pragma clang diagnostic pop
    }
#endif
}

//:(NSString *)contentString
- (void)registerLocalNotificationInOldWayWithAlertBody:(NSString *)alertBody soundName:(NSString *)sound {
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    NSDate *fireDate = [NSDate new];
    notification.fireDate = fireDate;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertTitle = APP_NAME;
    notification.alertBody = alertBody;
    notification.applicationIconBadgeNumber = 1;
    if (isEmptyString(sound)) {
        notification.soundName = UILocalNotificationDefaultSoundName;
    } else {
        notification.soundName = sound;
    }
    
    
    // 消息字典
    //    NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"contentString" forKey:@"key"];
    //    notification.userInfo = userDict;
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType type = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:type
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
#pragma clang diagnostic pop
    
}
//:(NSString *)title content:(NSString *)contentSring
- (void)locationUNUserNotificationCenterWithAlertBody:(NSString *)alertBody soundName:(NSString *)sound {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        
        if (!granted) return ;
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        
        // 标题
        content.title = APP_NAME;
        // 次标题
        //        content.subtitle = @"次标题";
        // 内容
        content.body = alertBody;
        badge++;
        // app显示通知数量的角标
        content.badge = @(badge);
        if (isEmptyString(sound)) {
            content.sound = [UNNotificationSound defaultSound];
        } else {
            content.sound = [UNNotificationSound soundNamed:sound];
        }
        content.categoryIdentifier = @"categoryIndentifier";
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        
        UNNotificationRequest *notificationRequest = [UNNotificationRequest requestWithIdentifier:@"KFGroupNotification" content:content trigger:trigger];
        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:notificationRequest withCompletionHandler:^(NSError * _Nullable error) {
            
            if (error == nil) {
                NSLog(@"已成功加推送%@",notificationRequest.identifier);
            }
            
            
            
        }];
        
    }];
}

// 前台受到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    
    
    NSLog(@"%s,c  - -%@",__FUNCTION__,notification.request);
    
    
    
}


//  后台受到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler {
    
    [center removeAllPendingNotificationRequests];
    
    
    NSLog(@"%s,c  - -%@",__FUNCTION__,response.notification);
    
    
    
    
}
@end
