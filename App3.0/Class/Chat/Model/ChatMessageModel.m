//
//  ChatMessageModel.m
//  App3.0
//
//  Created by mac on 17/3/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatMessageModel.h"

@implementation ChatMessageModel

+ (void)sendTextMessageWithConversationID:(NSString *)aId text:(NSString *)aText to:(NSString *)aTo progress:(void (^)(int))aProgressBlock completion:(void (^)(EMMessage *, EMError *))aCompletionBlock
{
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:aText];
    NSString *from = [[EMClient sharedClient] currentUsername];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:aId from:from to:aTo body:body ext:nil];
    message.chatType = EMChatTypeChat;// 设置为单聊消息
    
    [[EMClient sharedClient].chatManager sendMessage:message progress:aProgressBlock completion:aCompletionBlock];
}

+ (void)sendImageMessageWithConversationID:(NSString *)aId imageData:(NSData *)aData displayName:(NSString *)aName to:(NSString *)aTo progress:(void (^)(int))aProgressBlock completion:(void (^)(EMMessage *, EMError *))aCompletionBlock
{
    // 构造图片消息
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:aData displayName:aName];
    NSString *from = [[EMClient sharedClient] currentUsername];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:aId from:from to:aTo body:body ext:nil];
    message.chatType = EMChatTypeChat;// 设置为单聊消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:aProgressBlock completion:aCompletionBlock];
}

+ (void)sendVoiceMessageWithConversationID:(NSString *)aId localPath:(NSString *)aPath displayName:(NSString *)aName duration:(NSString *)duration to:(NSString *)aTo progress:(void (^)(int))aProgressBlock completion:(void (^)(EMMessage *, EMError *))aCompletionBlock
{
    // 构造语音消息
    EMVoiceMessageBody *body = [[EMVoiceMessageBody alloc] initWithLocalPath:aPath displayName:aName];
    body.duration = [duration intValue];
    NSString *from = [[EMClient sharedClient] currentUsername];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:aId from:from to:aTo body:body ext:nil];
    message.chatType = EMChatTypeChat;// 设置为单聊消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:aProgressBlock completion:aCompletionBlock];
}

+ (void)sendLocationMessageWithConversationID:(NSString *)aId latitude:(double)latitude longitude:(double)longitude andAddress:(NSString *)address to:(NSString *)aTo progress:(void (^)(int))aProgressBlock completion:(void (^)(EMMessage *, EMError *))aCompletionBlock
{
    EMLocationMessageBody *body = [[EMLocationMessageBody alloc] initWithLatitude:latitude longitude:longitude address:address];
    NSString *from = [[EMClient sharedClient] currentUsername];
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:aId from:from to:aTo body:body ext:nil];
    message.chatType = EMChatTypeChat;// 设置为单聊消息

    [[EMClient sharedClient].chatManager sendMessage:message progress:aProgressBlock completion:aCompletionBlock];
}
@end
