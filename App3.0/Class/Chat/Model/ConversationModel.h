//
//  ConversationModel.h
//  App3.0
//
//  Created by mac on 17/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UUMessage.h"
#import "XMPPRoomManager.h"

@interface SiganlChatModel : NSObject

@property (copy, nonatomic) NSString *userName; // 用户编号
@property (nonatomic,strong) NSNumber *relation;


@property (nonatomic ,copy)NSString *conversationID;

@property (nonatomic, assign) ChatType chatType;

@property (copy, nonatomic) NSString *title;

@property (copy, nonatomic) NSString *avatarURLPath;

@property (strong, nonatomic) XMPPJID *bareJID;

- (instancetype)initWithChatWithJID:(NSString *)conversationID title:(NSString *)title avatarURLPath:(NSString *)avatarURLPath;

@end

@interface ConversationModel : NSObject

/** @brief 会话对象 */
@property (strong, nonatomic) id conversation;
/** @brief 会话的标题(主要用户UI显示) */
@property (strong, nonatomic) NSString *title;
/** @brief conversationId的头像url */
@property (strong, nonatomic) NSString *avatarURLPath;
/** @brief conversationId的头像 */
@property (strong, nonatomic) UIImage *avatarImage;

@property (strong, nonatomic) NSDate *lastestDate;

@property (strong, nonatomic) NSString *lastestMessageStr;

@property (nonatomic ,copy)NSString *member_count;

@property (nonatomic, assign) ChatType chatType;

@property (nonatomic ,strong )XMPPRoomManager *xmppRoomManager;

@property (nonatomic ,strong)XMPPJID *bareJID;


@property (nonatomic ,assign)NSInteger messageCount;

@property (nonatomic,copy) NSString *userName; //  单聊
@property (nonatomic,strong)NSNumber *relation ; //  单聊

/*!
 @method
 @brief 初始化会话对象模型
 @param conversation    会话对象
 @return 会话对象模型
 */
- (instancetype)initWithConversation:(id)conversation;

/**
 单聊
 
 @param model model description
 @return return value description
 */
- (instancetype)initWithSiganlChatModel:(SiganlChatModel *)model;

- (instancetype) initWithContact:(XMPPMessageArchiving_Contact_CoreDataObject *)object;


/**
 历史记录
 */
- (instancetype)initWithMessage:(XMPPMessageArchiving_Message_CoreDataObject *)object;

- (void) signalWithContactID:(NSString *)UID;

- (void) groupWithContactID:(NSString *)UID;

- (void) getGroupInfor:(NSString *)UID;
- (void) getUserInfor:(NSString *)ID ;
@end
