//
//  HTTPManager+Chat.h
//  App3.0
//
//  Created by mac on 2017/12/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"

// 群聊邀请审核
#define Url_MemberInviteAudit   @"/api/v1/user/groupchat/MemberInviteAudit"

// 群聊邀请详情
#define Url_MemberInviteInfo   @"/api/v1/user/groupchat/MemberInviteInfo"
@interface HTTPManager (Chat)

/*
 群聊邀请详情
 请求参数
 invite_id    必须    integer    邀请记录
 
 返回字段说明
 
 字段           类型及范围    说明
 gid            string      群id
 type           int         群类型
 owner          int         群主id
 name           string      群名称
 desc           string      群描述
 avatar         string      群头像
 member_count   int         群成员数量
 w_time         int         建群时间
 user_nick      string      邀请人
 nick_str       string      被邀请人
 invite_num     integer     被邀请人数
 */
+ (void)getMemberInviteInfoWithInviteId:(NSString *)invite_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 群聊邀请审核通过
 请求参数
 invite_id    必须    integer    邀请记录
 
 返回字段说明
 同上
 */
+ (void)verifyMemberInviteWithInviteId:(NSString *)invite_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

@end
