//
//  ChatHelper.h
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TabChatVC.h"
#import "ChatViewController.h"
#import "TabChatVC.h"
#import "TranspondListViewController.h"
#import "TabFriendsVC.h"
#import "ChatCallManager.h"
#import "XMChatController.h"

#import "FDReplyListController.h"

#define kHaveUnreadAtMessage    @"kHaveAtMessage"
#define kAtYouMessage           1
#define kAtAllMessage           2

@interface ChatHelper : NSObject<EMClientDelegate,EMChatManagerDelegate,EMContactManagerDelegate,EMGroupManagerDelegate,EMChatroomManagerDelegate>

@property (nonatomic, weak) FDReplyListController *replyVC;

//@property (nonatomic, weak) TabChatVC *tabChatVC;
@property (nonatomic, weak) ChatViewController *chatVC;
@property (nonatomic, weak) BaseChatController *xmchatVC;
@property (nonatomic, weak) TranspondListViewController *transVC;
@property (nonatomic, weak) TabFriendsVC *friendVC;

@property (nonatomic, assign) NSInteger badgeNumber;
@property (nonatomic, assign) BOOL isSendingCard;
@property (nonatomic, copy) NSString *previousTime;

+ (instancetype)shareHelper;

- (NSString *)latestMessageTitleWithMessage:(id)message;

-(void)createSystemSoundWithName:(NSString *)soundName soundType:(NSString *)soundType vibrate:(BOOL)vibrate;
- (void)stopSystemSound;

- (void)sendSystemPushMessage:(NSDictionary *)data;

// 根据message获取应该显示的通知内容
- (NSString *)noticeStringConvertByMessage:(id)message;

// 本地通知
- (void)localNotificationWithAlertBody:(NSString *)alertBody soundName:(NSString *)sound;

- (NSString *)displayNameByUid:(NSString *)uid;

// 获取好友关系
+ (NSNumber *) isfriendWithUid:(NSString *)uid;
@end
