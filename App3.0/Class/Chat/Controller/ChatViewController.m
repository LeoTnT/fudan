//
//  ChatViewController.m
//  App3.0
//
//  Created by mac on 17/3/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatKeyBoard.h"
#import "FaceSourceManager.h"
#import "FaceSourceManager.h"
#import <AVFoundation/AVFoundation.h>
//#import "MapLocationViewController.h"
#import "PersonalViewController.h"
#import "GroupInfoViewController.h"
#import "ChatHelper.h"
#import "TranspondListViewController.h"
#import "LoginModel.h"
#import "XSFormatterDate.h"
#import "GoodsTypeViewController.h"
#import "ChatViewController.h"
#import "UUImageAvatarBrowser.h"
#import "GroupSetTitleViewController.h"

// 红包
#import "OpenRedPacketView.h"
#import "RedPacketSendViewController.h"
#import "RedPacketDetailViewController.h"

// 转账
#import "ChatTransferViewController.h"
#import "ChatTransferDetailViewController.h"

#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "PersonViewController.h"
#import "S_CashierController.h"

// 气泡
#import "UUMessageCell.h"
#import "UUMessageFrame.h"
#import "UUMessage.h"
#import "ChatModel.h"
// 录音
#import "Mp3Recorder.h"
#import "UUProgressHUD.h"

// 图片查看
#import "XLPhotoBrowser.h"
#import "S_BaiduMap.h"
#import "AESCrypt.h"

#import "InviteDetailViewController.h"
#import "ADDetailWebViewController.h"

// 系统消息跳转
#import "WalletAccountViewController.h"
#import "OrderDetailViewController.h"


#define kLines 20
#define kRecordAudioFile @"myRecord.m4a"



@interface ChatViewController () <ChatKeyBoardDelegate,ChatKeyBoardDataSource,EMChatManagerDelegate,UUMessageCellDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVAudioRecorderDelegate,MapLocationViewDelegate, XLPhotoBrowserDelegate, XLPhotoBrowserDatasource, UIDocumentPickerDelegate, UIDocumentInteractionControllerDelegate>
{
    //    EMConversation *_conversation;
    NSString *_chatterName;
    NSString *_mineAvatar;
    EMConversationType _typeChat;
    NSMutableArray *_chatArray;
    NSInteger _addHistroyNumber; //下拉加载更多数据量
    // 录音
    BOOL isbeginVoiceRecord;
    Mp3Recorder *MP3;
    NSInteger playTime;
    NSTimer *playTimer;
    NSTimer *recordLevelTimer;
    
    // 长按事件
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    UIMenuItem *_transpondMenuItem;
    UIMenuItem *_recallMenuItem;
    UIMenuItem *_detectorMenuItem;
    __block XLPhotoBrowser *_photoBrowser;
    BOOL canScrollBottom;
}
@property (nonatomic, strong) ChatKeyBoard *chatKeyBoard;
@property (nonatomic, strong) ChatModel *chatModel;
@property (nonatomic, strong) AVAudioRecorder *audioRecorder;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;//音频播放器，用于播放录音文件
@property (nonatomic, strong) UIMenuController *menuController;
@property (nonatomic, strong) NSIndexPath *menuIndexPath; //选中消息菜单索引
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) OpenRedPacketView *openRPView;
@property (nonatomic, strong) NSTimer *recordingTimer;
@property (nonatomic, strong) UIDocumentInteractionController *fileInteractionController; // 文件预览实例
@property (nonatomic, strong) NSArray *moreItems;
@end

@implementation ChatViewController
@synthesize conversation = _conversation;
@synthesize title = _title;;
@synthesize avatarUrl = _avatarUrl;
- (id)initWithConversation:(EMConversation *)conversation
{
    self = [super init];
    if (self) {
        _conversation = conversation;
        _chatterName = conversation.conversationId;
        _typeChat = conversation.type;
    }
    return self;
}

- (NSMutableArray *)photoArray {
    if (!_photoArray) {
        _photoArray = [NSMutableArray array];
    }
    return _photoArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isViewDidAppear = YES;
    
    // 在此刷新消息会造成发送图片和定位消息不刷新
//    [self refreshMessages];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.isViewDidAppear = NO;
    
    // 标为已读
    _conversation.latestMessage.isRead = YES;
    [_conversation updateMessageChange:_conversation.latestMessage error:nil];
    [_conversation markAllMessagesAsRead:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = _title;
    [self setHidesBottomBarWhenPushed:YES];
    __weak typeof(self) wSelf= self;
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kChatToolBarHeight, 0));
    }];
    self.showRefreshHeader = YES;
    [self setRefreshHeader:^{
        [wSelf addHistroyMessages];
    }];
    
    self.chatKeyBoard.associateTableView = self.tableView;
    [self.view addSubview:self.chatKeyBoard];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.view addGestureRecognizer:tap];
    self.navigationController.interactivePopGestureRecognizer.delaysTouchesBegan=NO;
    
    if (_avatarUrl == nil) {
        _avatarUrl = @"";
    }
    
    _chatArray = [NSMutableArray array];
    _addHistroyNumber = 0;
    //注册消息回调
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [self loadBaseViewsAndData];
    
//    [self setAudioSession];
    [ChatHelper shareHelper].chatVC = self;
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        for (UIViewController *controller in wSelf.navigationController.viewControllers) {
            if ([controller isKindOfClass:[GroupSetTitleViewController class]]) {
                [wSelf.navigationController popToRootViewControllerAnimated:YES];
                break;
            }
            if ([controller isKindOfClass:[GoodsTypeViewController class]]) {
//                [wSelf.navigationController popToViewController:controller animated:YES];
            }
            if ([controller isKindOfClass:[ChatViewController class]]) {
                [wSelf.navigationController popViewControllerAnimated:YES];
                break;
            }
        }
        [ChatHelper shareHelper].chatVC = nil;
    }];
    if (![self.conversation.conversationId isEqualToString:@"robot"]) {
        // 不是机器人
        if (_conversation.type == EMConversationTypeGroupChat) {
            [self actionCustomRightBtnWithNrlImage:@"chat_group" htlImage:nil title:nil action:^{
                GroupInfoViewController *groupVC = [[GroupInfoViewController alloc] initWithGroupId:_chatterName];
                [wSelf.navigationController pushViewController:groupVC animated:YES];
            }];
        } else {
            [self actionCustomRightBtnWithNrlImage:@"chat_single" htlImage:nil title:nil action:^{
                PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:_chatterName];
                [wSelf.navigationController pushViewController:perVC animated:YES];
            }];
        }
    } else {
        self.chatKeyBoard.hidden = YES;
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:USERINFO_LOGIN];
    LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    _mineAvatar = userInfo.logo;
    
    canScrollBottom = YES;
    [self refreshMessages];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMessages) name:@"refreshChatMessages" object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    NSLog(@" -  - - - - - - -- - ");
    // Dispose of any resources that can be recreated.
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.navigationItem.title = _title;
}

#pragma mark - private
- (NSString *)detectorImage:(UIImage *)image
{
    //1.初始化扫描仪，设置设别类型和识别质量
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy:CIDetectorAccuracyHigh}];
    //2.扫描获取的特征组
    NSArray*features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    if (features.count > 0) {
        CIQRCodeFeature *feature = [features objectAtIndex:0];
        NSString *scannedResult = feature.messageString;
        NSLog(@"扫描结果------%@",scannedResult);
//        [XSTool showToastWithView:self.view Text:scannedResult];
        return scannedResult;
    }
    return nil;
}

- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(MessageType)messageType
{
    if (self.menuController == nil) {
        self.menuController = [UIMenuController sharedMenuController];
    }
    
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(deleteMenuAction:)];
    }
    
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyMenuAction:)];
    }
    
    if (_transpondMenuItem == nil) {
        _transpondMenuItem = [[UIMenuItem alloc] initWithTitle:Localized(@"qr_send") action:@selector(transpondMenuAction:)];
    }
    
    if (_recallMenuItem == nil) {
        _recallMenuItem = [[UIMenuItem alloc] initWithTitle:@"撤回" action:@selector(recallMenuAction:)];
    }
    
    if (_detectorMenuItem == nil) {
        _detectorMenuItem = [[UIMenuItem alloc] initWithTitle:@"识别二维码" action:@selector(detectorMenuAction:)];
    }
    
    UUMessageFrame *mf = [self.chatModel.dataSource objectAtIndex:indexPath.row];
    EMMessage *message = mf.message.emMessage;
    NSMutableArray *items;
    if (messageType == UUMessageTypeText) {
        if (message.direction == EMMessageDirectionSend) {
            items = [@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem, _recallMenuItem] mutableCopy];
        } else {
            items = [@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem] mutableCopy];
        }
        
    } else if (messageType == UUMessageTypePicture){
        NSString *qrUrl = [self detectorImage:mf.message.picture];
        if (qrUrl) {
            mf.message.qrUrl = qrUrl;
            items = [@[_deleteMenuItem,_transpondMenuItem, _detectorMenuItem] mutableCopy];
        } else {
            items = [@[_deleteMenuItem,_transpondMenuItem] mutableCopy];
        }
        if (message.direction == EMMessageDirectionSend) {
            [items addObject:_recallMenuItem];
        }
        
    } else if (messageType == UUMessageTypeRedPacket) {
        items = [@[_deleteMenuItem] mutableCopy];
    } else if (messageType == UUMessageTypeVoice) {
        items = [@[_deleteMenuItem] mutableCopy];
        if (message.direction == EMMessageDirectionSend) {
            [items addObject:_recallMenuItem];
        }
    } else {
        items = [@[_deleteMenuItem, _transpondMenuItem] mutableCopy];
    }
    [self.menuController setMenuItems:items];
    [self.menuController setTargetRect:showInView.frame inView:showInView.superview];
    [self.menuController setMenuVisible:YES animated:YES];
}

- (void)loadBaseViewsAndData
{
    self.chatModel = [[ChatModel alloc]init];
    self.chatModel.isGroupChat = NO;
}

// 刷新消息
- (void)refreshMessages
{
    NSLog(@"conversation ： %@",_conversation.conversationId);
    EMMessage *latestMes = [_conversation latestMessage];
    if (latestMes == nil) {
        return;
    }
    [_chatArray removeAllObjects];
    __weak typeof(self) wSelf = self;
    
    // id为nil为从最新消息获取
    [_conversation loadMessagesStartFromId:nil count:10 searchDirection:EMMessageSearchDirectionUp completion:^(NSArray *aMessages, EMError *aError) {
        NSLog(@"message count : %lu",(unsigned long)aMessages.count);
        for (EMMessage *message in aMessages) {
            [_chatArray addObject:message];
        }
        if (_chatArray.count > 0) {
            [self.chatModel.dataSource removeAllObjects];
            [wSelf dealMessages:_chatArray needScrollToBottom:YES];
        }

    }];
    
}

// 下拉加载历史消息
- (void)addHistroyMessages
{
    if (_chatArray.count == 0) {
        [self.tableView.mj_header endRefreshing];
        return;
    }
    EMMessage *latestMes = [_chatArray firstObject];
    NSLog(@"%@",latestMes.messageId);
//    [_chatArray removeAllObjects];
    NSMutableArray *tempArray = [NSMutableArray array];
    __weak typeof(self) wSelf = self;
    [_conversation loadMessagesStartFromId:latestMes.messageId count:10 searchDirection:EMMessageSearchDirectionUp completion:^(NSArray *aMessages, EMError *aError) {
        _addHistroyNumber = aMessages.count;
        int index = 0;
        for (EMMessage *message in aMessages) {
            [tempArray addObject:message];
            [_chatArray insertObject:message atIndex:index];
            index++;
        }
        
        if (aMessages.count > 0) {
            [wSelf dealMessages:tempArray needScrollToBottom:NO];
        }
        [wSelf.tableView.mj_header endRefreshing];
    }];
    
}

- (void)tapAction
{
    [self.chatKeyBoard keyboardDown];
}
// 添加并滑动到新cell
- (void)insertAndScrollToBottom
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_chatArray.count-1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}

- (void)tableViewScrollToBottom
{
    if (self.chatModel.dataSource.count==0)
        return;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatModel.dataSource.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (void)tableViewScrollToLatestRefresh
{
    if (self.chatModel.dataSource.count==0)
        return;
    NSLog(@"_addHistroyNumber==%d",(int)_addHistroyNumber);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_addHistroyNumber inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

// 发送消息显示处理
- (void)dealTheFunctionData:(NSDictionary *)dic
{
    [self.chatModel addMessageItems:dic];
    [self.tableView reloadData];
    [self tableViewScrollToBottom];
    
}

- (UIViewController * )getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

#pragma mark - UUMessage delegate
- (void)systemNoticeContentDidClick:(UUMessageSystemNoticeModel *)sysNoticeModel {
    NSString *link_url = sysNoticeModel.link_url;
    if (![link_url containsString:ImageBaseUrl]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link_url]];
        return;
    }
    NSString *paramStr = [link_url componentsSeparatedByString:@"?"][1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"wallet"]) {
        WalletAccountViewController *vc=[[WalletAccountViewController alloc] init];
        vc.walletType = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"order"]) {
        OrderDetailViewController *vc=[[OrderDetailViewController alloc] init];
        vc.orderId = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"chat"]) {
        // 客服
        [self.navigationController pushViewController:[self getNameWithClass:@"CustomerViewController"] animated:YES];
    } else {
        ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@&device=%@",sysNoticeModel.link_url,gui];
        adWeb.title = sysNoticeModel.link_title;
        [self.navigationController pushViewController:adWeb animated:YES];
    }
    
    
}

- (void)noticeContentDidClick:(UUMessageCell *)cell {
    // 邀请群成员群主确认
    InviteDetailViewController *vc = [[InviteDetailViewController alloc] init];
    vc.invite_id = cell.messageFrame.message.ext[GROUP_INVITE_ID];
    vc.message = cell.messageFrame.message.emMessage;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendFailTap:(UUMessageCell *)cell {
    @weakify(self);
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"重新发送此消息" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [[EMClient sharedClient].chatManager resendMessage:cell.messageFrame.message.emMessage progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            [_chatArray addObject:message];
            if (!error) {
                NSLog(@"重新发送消息成功");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([message.messageId isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusSuccessed;
                    }
                }
                [self.tableView reloadData];
            } else {
                NSLog(@"重新发送消息失败");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([message.messageId isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusFailed;
                    }
                }
                [self.tableView reloadData];
            }
        }];
    }];
    UIAlertAction *canAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertVC addAction:canAct];
    [alertVC addAction:conAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)cellContentCopy:(UUMessageCell *)cell
{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = cell.messageFrame.message.strContent;
}

- (void)cellContentDelete:(UUMessageCell *)cell
{
    NSLog(@"删除");
    EMMessage *message = cell.messageFrame.message.emMessage;
    [_chatArray removeObject:message];
    [self.chatModel.dataSource removeObject:cell.messageFrame];
    [_conversation deleteMessageWithId:message.messageId error:nil];
    [self.tableView reloadData];
}

- (void)cellContentTranspond:(UUMessageCell *)cell
{
    NSLog(@"转发");
    EMMessage *message = cell.messageFrame.message.emMessage;
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:message];
    [self.navigationController pushViewController:trVC animated:YES];
}

- (void)cellContentRecall:(UUMessageCell *)cell
{
    NSLog(@"撤回");
    EMMessage *seleteMessage = cell.messageFrame.message.emMessage;
    // 判断时间是否在2分钟之内
    NSString *startStr = [NSString stringWithFormat:@"%lld",seleteMessage.timestamp];
    NSString *subStart = [XSFormatterDate timeWithTimeIntervalString:startStr];
    NSDate *startDate = [NSDate dateFromString:subStart withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *subEnd = [XSFormatterDate currentTime];
    NSDate *endDate = [NSDate dateFromString:subEnd withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //这个是相隔的秒数
    NSTimeInterval timeInterval = [startDate timeIntervalSinceDate:endDate];
    NSLog(@"%f",timeInterval);
    //相距5分钟显示时间Label
    if (fabs (timeInterval) > 2*60) {
        [XSTool showToastWithView:self.view Text:@"无法撤回超过两分钟的消息"];
        return;
    }
    
    // 删除本地消息
//    [self cellContentDelete:cell];
    //更新本地消息
    cell.messageFrame.showNotice = YES;
    EMMessage *emMessage = cell.messageFrame.message.emMessage;
    emMessage.ext = @{MSG_TYPE:REVOKE_ACTION};
    cell.messageFrame.message.strNotice = [[ChatHelper shareHelper] noticeStringConvertByMessage:cell.messageFrame.message.emMessage];
    NSLog(@"%@",[[ChatHelper shareHelper] noticeStringConvertByMessage:cell.messageFrame.message.emMessage]);
    [cell.messageFrame setMessage:cell.messageFrame.message];
    [[EMClient sharedClient].chatManager updateMessage:cell.messageFrame.message.emMessage completion:^(EMMessage *aMessage, EMError *aError) {
        [self.tableView reloadData];
    }];
    
    // 发送透传消息
    EMCmdMessageBody *body = [[EMCmdMessageBody alloc] initWithAction:REVOKE_ACTION];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    NSDictionary *extDic = @{CUSTOM_MSG_ID:seleteMessage.messageId,MSG_TYPE:REVOKE_ACTION};
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:extDic];
    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
    //发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
//        [_chatArray addObject:message];
    }];
    
}

- (void)cellContentDetector:(UUMessageCell *)cell
{
    NSLog(@"识别图中二维码");
    [self cellContentUrlDidClick:cell.messageFrame.message.qrUrl];
}

- (void)shareContentClick:(NSString *)link {
    [self cellContentUrlDidClick:link];
}

- (void)cellContentUrlDidClick:(NSString *)url {
    NSArray *paramArr = [url componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![url containsString:ImageBaseUrl] || arr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"supply_pay"]){
        S_CashierController *cashier =[S_CashierController new];
        cashier.showTitle = qrid;
        cashier.supply_name = qrid;
        [self.navigationController pushViewController:cashier animated:YES];
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}

- (void)videoDidClick:(UUMessageCell *)cell {
    EMMessage *seleteMessage = cell.messageFrame.message.emMessage;
    EMVideoMessageBody *fileMesBody = (EMVideoMessageBody *)seleteMessage.body;
    if (fileMesBody.downloadStatus == EMDownloadStatusSuccessed) {
        NSLog(@"local path is exsit");
        NSURL *fileUrl = [NSURL fileURLWithPath:fileMesBody.localPath];
        if (fileUrl != nil) {
            if (_fileInteractionController == nil) {
                //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                _fileInteractionController.delegate = self;
            } else {
                _fileInteractionController.URL = fileUrl;
            }
            [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"文件打开失败"];
        }
        return;
    } else {
        [XSTool showProgressHUDTOView:self.view withText:@"正在下载"];
        [[EMClient sharedClient].chatManager downloadMessageAttachment:cell.messageFrame.message.emMessage progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"下载完成"];
                // 下载完成
                cell.messageFrame.message.emMessage = message;
                NSIndexPath *indePath= [self.tableView indexPathForCell:cell];
                [self.tableView reloadRowsAtIndexPaths:@[indePath] withRowAnimation:UITableViewRowAnimationNone];
                
                // 打开文件
                EMFileMessageBody *fileBody = (EMFileMessageBody *)message.body;
                NSURL *fileUrl = [NSURL fileURLWithPath:fileBody.localPath];
                if (fileUrl != nil) {
                    if (_fileInteractionController == nil) {
                        //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                        _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                        _fileInteractionController.delegate = self;
                    } else {
                        _fileInteractionController.URL = fileUrl;
                    }
                    [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:@"文件打开失败"];
                }
            }
        }];
    }
}

- (void)voiceDidClick:(UUMessageCell *)cell
{
    EMMessage *seleteMessage = cell.messageFrame.message.emMessage;
    if (seleteMessage.chatType == EMChatTypeChat && !seleteMessage.isReadAcked && seleteMessage.direction == EMMessageDirectionReceive) {
        
        [[EMClient sharedClient].chatManager sendMessageReadAck:seleteMessage completion:^(EMMessage *aMessage, EMError *aError) {
            NSLog(@"消息已读回执发送成功");
            // 消息已读回执发送成功
            aMessage.isRead = YES;
            aMessage.isReadAcked = YES;
        }];
    }
}

- (void)locationDidClickLatitude:(double)latitude longitude:(double)longitue adddress:(NSString *)adddress
{
    
    S_BaiduMap *locationController = [S_BaiduMap new];
    locationController.showType = BMKShowMap;
    locationController.delegate = self;
    CLLocationCoordinate2D cc = {latitude,longitue} ;
    locationController.locationCoordinate = cc;
    locationController.pathShowAddress = adddress;
    [self.navigationController pushViewController:locationController animated:YES];
}

- (void)redPacketClick:(UUMessageCell *)cell {
    // 服务器拉取红包数据
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            
            if (self.conversation.type == EMConversationTypeChat) {
                if (model.is_can_rec) {
                    // 我可以领（收红包）
//                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversation;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            } else {
                if (model.is_can_rec || (!model.is_can_rec && !model.is_received)) {
                    // 我可以领（群红包）或者 我已经不可领并且我还没领
//                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversation;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
   
}

- (void)transferClick:(UUMessageCell *)cell {
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getTransferInfoWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            TransferDetailModel *model = [TransferDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转转账详情
            ChatTransferDetailViewController *trans = [[ChatTransferDetailViewController alloc] init];
            trans.model = model;
            trans.conversation = self.conversation;
            trans.transferId = rpId;
            trans.resendMessage = cell.messageFrame.message.emMessage;
            [self.navigationController pushViewController:trans animated:YES];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)openRedPacketWithId:(NSString *)rpId {
    [XSTool showProgressHUDWithView:self.view];
    if (self.conversation.type == EMConversationTypeChat) {
        [HTTPManager getRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId};
                        // 生成message
                        EMMessage *message = [[EMMessage alloc] initWithConversationID:self.conversation.conversationId from:[UserInstance ShardInstnce].uid to:self.conversation.conversationId body:body ext:extDic];
                        message.chatType = (EMChatType)self.conversation.type;// 设置消息类型
                        //发送消息
                        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                            
                        } completion:^(EMMessage *message, EMError *error) {
                            [self refreshMessages];
                        }];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversation;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        [HTTPManager getGroupRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 刷新用户钱包
                [HTTPManager updateWalletWithSuccess:^(NSDictionary *dic, resultObject *state) {
                    
                } fail:^(NSError *error) {
                    
                }];
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId,MESSAGE_PACK_FROM:model.uid};
                        // 生成message
                        EMMessage *message = [[EMMessage alloc] initWithConversationID:self.conversation.conversationId from:[UserInstance ShardInstnce].uid to:self.conversation.conversationId body:body ext:extDic];
                        message.chatType = (EMChatType)self.conversation.type;// 设置消息类型
                        //发送消息
                        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                            
                        } completion:^(EMMessage *message, EMError *error) {
                            [self refreshMessages];
                        }];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversation;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }

        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }
    
}

- (void)detailRedPacketWithId:(NSString *)rpId {
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.openRPView dismiss];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转红包详情
            RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
            rpd.model = model;
            rpd.conversation = self.conversation;
            [self.navigationController pushViewController:rpd animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [self.openRPView dismiss];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)cellContentDidClick:(UUMessageCell *)cell imageView:(UIImageView *)contentImageView
{
    NSArray* sorted = [self.photoArray sortedArrayUsingComparator:
                       ^(PhotoBrowserImage *obj1, PhotoBrowserImage* obj2){
                           if(obj1.imageTime < obj2.imageTime) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    [self.photoArray removeAllObjects];
    [self.photoArray addObjectsFromArray:sorted];
    NSInteger index = 0;
    EMMessage *emMessage = cell.messageFrame.message.emMessage;
    for (PhotoBrowserImage *image1 in self.photoArray) {
        if ([image1.imageId isEqualToString:emMessage.messageId]) {
            index = [self.photoArray indexOfObject:image1];
            break;
        }
    }
    
    EMImageMessageBody *imageMesBody = (EMImageMessageBody *)emMessage.body;
    if (imageMesBody.downloadStatus == EMDownloadStatusSuccessed) {
        NSLog(@"local path is exsit");
        NSData *data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:imageMesBody.localPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,emMessage.to]];
        // 解密
        UIImage *image = [UIImage imageWithData:data];
        [contentImageView setImage:image];
        cell.messageFrame.message.picture = image;
        
        // 解密
        PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:data];
        pbImage.imageId = emMessage.messageId;
        pbImage.imageTime = emMessage.timestamp;
        if (pbImage) {
            [self.photoArray replaceObjectAtIndex:index withObject:pbImage];
        }
        
        _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
        _photoBrowser.delegate = self;
        [_photoBrowser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
        _photoBrowser.isCusctomAdd = YES;
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [self.tabBarController.view addSubview:_photoBrowser];
        return;
    }
    
    _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
    _photoBrowser.delegate = self;
    [_photoBrowser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    _photoBrowser.isCusctomAdd = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.tabBarController.view addSubview:_photoBrowser];
    if (cell.messageFrame.message.from == UUMessageFromMe) {
        return;
    }
    
    [XSTool showProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
    [[EMClient sharedClient].chatManager downloadMessageAttachment:cell.messageFrame.message.emMessage progress:^(int progress) {
//        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"%d%%",progress]];
    } completion:^(EMMessage *message, EMError *error) {
        if (!error) {
            EMImageMessageBody *imageMesBody = (EMImageMessageBody *)message.body;
            NSData *data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:imageMesBody.localPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
            // 解密
            UIImage *image = [UIImage imageWithData:data];
            [contentImageView setImage:image];
            cell.messageFrame.message.picture = image;
            
            // 解密
            PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:data];
            pbImage.imageId = message.messageId;
            pbImage.imageTime = message.timestamp;
            if (pbImage) {
                [self.photoArray replaceObjectAtIndex:index withObject:pbImage];
            }
            
            [_photoBrowser dismiss];
            _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
            _photoBrowser.delegate = self;
            [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
        } else {
            [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
            [XSTool showToastWithView:self.view Text:@"原图下载失败"];
        }
        
    }];
}

- (void)callContentDidClick:(UUMessageCell *)cell {
    if (cell.messageFrame.message.isVideo) {
        EMCallOptions *options = [[EMClient sharedClient].callManager getCallOptions];
        //当对方不在线时，是否给对方发送离线消息和推送，并等待对方回应
        options.isSendPushIfOffline = NO;
        [[EMClient sharedClient].callManager setCallOptions:options];
        
        [[ChatCallManager sharedManager] makeCallWithUsername:self.conversation.conversationId type:EMCallTypeVideo];
    } else {
        EMCallOptions *options = [[EMClient sharedClient].callManager getCallOptions];
        //当对方不在线时，是否给对方发送离线消息和推送，并等待对方回应
        options.isSendPushIfOffline = NO;
        [[EMClient sharedClient].callManager setCallOptions:options];
        
        [[ChatCallManager sharedManager] makeCallWithUsername:self.conversation.conversationId type:EMCallTypeVoice];
    }
}

- (void)cardContentDidClick:(UUMessageCell *)cell {
    PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:cell.messageFrame.message.cardId];
    [self.navigationController pushViewController:perVC animated:YES];
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

-(void)photoBrowserDismiss{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
//    [self.vc.tabBarController.tabBar setHidden:NO];
}

//- (UIImage *)photoBrowser:(XLPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index {
//    
//}

- (void)photoBrowserScroll:(PhotoBrowserImage *)image {
    NSInteger index = 0;
    for (PhotoBrowserImage *image1 in self.photoArray) {
        if ([image1.imageId isEqualToString:image.imageId]) {
            index = [self.photoArray indexOfObject:image1];
            break;
        }
    }
    for (UUMessageFrame *mesF in self.chatModel.dataSource) {
        EMMessage *mes = mesF.message.emMessage;
        if ([mes.messageId isEqualToString:image.imageId]) {
            EMImageMessageBody *imageMesBody = (EMImageMessageBody *)mes.body;
            if (imageMesBody.downloadStatus == EMDownloadStatusSuccessed) {
                NSLog(@"local path is exsit");
                return;
            }
            if (mes.direction == EMMessageDirectionSend) {
                return;
            }
            [XSTool showProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
            [[EMClient sharedClient].chatManager downloadMessageAttachment:mes progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                if (!error) {
                    EMImageMessageBody *imageMesBody = (EMImageMessageBody *)message.body;
                    NSData *data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:imageMesBody.localPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
                    UIImage *image = [UIImage imageWithData:data];
                    PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:data];
                    pbImage.imageId = message.messageId;
                    pbImage.imageTime = message.timestamp;
                    [self.photoArray replaceObjectAtIndex:index withObject:pbImage];
                    mesF.message.picture = image;
                    [self.tableView reloadData];
                    [_photoBrowser dismiss];
                    _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
                    _photoBrowser.delegate = self;
                    [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
                } else {
                    [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
                    [XSTool showToastWithView:self.view Text:@"原图下载失败"];
                }
                
            }];
        }
    }
}

- (void)cellContentFileDidClick:(UUMessageCell *)cell {
    EMMessage *emMessage = cell.messageFrame.message.emMessage;
    EMFileMessageBody *fileMesBody = (EMFileMessageBody *)emMessage.body;
    if (fileMesBody.downloadStatus == EMDownloadStatusSuccessed) {
        NSLog(@"local path is exsit");
        NSURL *fileUrl = [NSURL fileURLWithPath:fileMesBody.localPath];
        if (fileUrl != nil) {
            if (_fileInteractionController == nil) {
                //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                _fileInteractionController.delegate = self;
            } else {
                _fileInteractionController.URL = fileUrl;
            }
            [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"文件打开失败"];
        }
        
        return;
    } else {
        [XSTool showProgressHUDTOView:self.view withText:@"正在下载"];
        [[EMClient sharedClient].chatManager downloadMessageAttachment:cell.messageFrame.message.emMessage progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"下载完成"];
                // 下载完成
                cell.messageFrame.message.emMessage = message;
                NSIndexPath *indePath= [self.tableView indexPathForCell:cell];
                [self.tableView reloadRowsAtIndexPaths:@[indePath] withRowAnimation:UITableViewRowAnimationNone];
                
                // 打开文件
                EMFileMessageBody *fileBody = (EMFileMessageBody *)message.body;
                NSURL *fileUrl = [NSURL fileURLWithPath:fileBody.localPath];
                if (fileUrl != nil) {
                    if (_fileInteractionController == nil) {
                        //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                        _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                        _fileInteractionController.delegate = self;
                    } else {
                        _fileInteractionController.URL = fileUrl;
                    }
                    [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:@"文件打开失败"];
                }
            }
        }];
    }

}
#pragma mark - UIDocumentInteractionControllerDelegate
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller {
    return self.view.frame;
}

- (void)cellContentLongPress:(UUMessageCell *)cell
{
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    [cell.btnContent becomeFirstResponder];
    self.menuIndexPath = indexPath;
    [self showMenuViewController:cell.btnContent andIndexPath:indexPath messageType:cell.messageFrame.message.type];
}

- (void)headImageDidClick:(UUMessageCell *)cell
{
    EMMessage *message = cell.messageFrame.message.emMessage;
    if (message.direction == EMMessageDirectionSend) {
        return;
    }
    
    PersonalViewController *personalVC = [[PersonalViewController alloc] initWithUid:message.from];
    [self.navigationController pushViewController:personalVC animated:YES];
}

- (BOOL)shouldSendHasReadAckForMessage:(EMMessage *)message
{
    NSString *account = [[EMClient sharedClient] currentUsername];
    if (message.chatType != EMChatTypeChat || message.isReadAcked || [account isEqualToString:message.from] || ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) || !self.isViewDidAppear)
    {
        NSLog(@"read no 111111");
        return NO;
    }
    return YES;
//    EMMessageBody *body = message.body;
//    if (((body.type == EMMessageBodyTypeVideo) ||
//         (body.type == EMMessageBodyTypeVoice) ||
//         (body.type == EMMessageBodyTypeImage)))
//    {
//        NSLog(@"read no 222222");
//        return NO;
//    }
//    else
//    {
//        return YES;
//    }
}

- (void)dealMessages:(NSArray *)aMessages needScrollToBottom:(BOOL)scroll
{
    NSInteger index = 0;
    
    for (EMMessage *message in aMessages) {
        // 判断接收的消息状态
        MessageStatus status = UUMessageStatusSuccessed;
        if (message.status == EMMessageStatusFailed) {
            status = UUMessageStatusFailed;
        }
        NSString *avatarStr = @"";
        NSString *strName = @"";
        if (message.isDeliverAcked) {
            status = UUMessageStatusSuccessed;
        }

        if (message.direction == EMMessageDirectionReceive) {
            if (_conversation.type == EMConversationTypeChat) {
                avatarStr = _avatarUrl; // 对方头像
                strName = message.from;
            } else if (_conversation.type == EMConversationTypeGroupChat) {
                strName = message.from;
                NSMutableArray *mutArray = [[DBHandler sharedInstance] getContactByUid:message.from];
                if (mutArray == nil || mutArray.count == 0) {
                    mutArray = [[DBHandler sharedInstance] getMemberByMemberId:message.from];
                    if (mutArray == nil || mutArray.count == 0) {
                        [HTTPManager getUserInfoWithUid:message.from success:^(NSDictionary * dic, resultObject *state) {
                            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
                            if (state.status) {
                                NickNameDataParser *model = [[NickNameDataParser alloc] init];
                                model.uid = message.from;
                                model.nickname = parser.data.nickname;
                                model.logo = parser.data.logo;
                                [[DBHandler sharedInstance] addOrUpdateGroupMember:model];
                            }
                        } fail:^(NSError * _Nonnull error) {
                            
                        }];
                    } else {
                        NickNameDataParser *model = mutArray[0];
                        avatarStr = model.logo;
                        strName = model.nickname;
                    }
                } else {
                    ContactDataParser *model = mutArray[0];
                    avatarStr = model.avatar;
                    strName = [model getName];
                }
                
            }
            
            if ([self shouldSendHasReadAckForMessage:message]) {
                
                [[EMClient sharedClient].chatManager sendMessageReadAck:message completion:^(EMMessage *aMessage, EMError *aError) {
                    // 消息已读回执发送成功
//                    aMessage.isRead = YES;
                    aMessage.isReadAcked = YES;
                }];
            }
        } else {
            avatarStr = _mineAvatar; // 我的头像
            if (message.isReadAcked) {
                status = UUMessageStatusRead;   // 标注为已读
            }
        }
        
        EMMessageBody *msgBody = message.body;
        __block NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        switch (msgBody.type) {
            case EMMessageBodyTypeText:
            {
                // 收到的文字消息
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *txt = [AESCrypt decrypt:textBody.text password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
                if (isEmptyString(txt)) {
                    txt = textBody.text;
                }
                NSLog(@"收到的文字是 txt -- %@",txt);
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"strContent": txt,
                         @"type": @(UUMessageTypeText),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            case EMMessageBodyTypeImage:
            {
                // 得到一个图片消息body
                EMImageMessageBody *body = ((EMImageMessageBody *)msgBody);
                NSLog(@"大图remote路径 -- %@"   ,body.remotePath);
                NSLog(@"大图local路径 -- %@"    ,body.localPath); // // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"大图的secret -- %@"    ,body.secretKey);
                NSLog(@"大图的W -- %f ,大图的H -- %f",body.size.width,body.size.height);
                NSLog(@"大图的下载状态 -- %u",body.downloadStatus);
                
                
                // 缩略图sdk会自动下载
                NSLog(@"小图remote路径 -- %@"   ,body.thumbnailRemotePath);
                NSLog(@"小图local路径 -- %@"    ,body.thumbnailLocalPath);
                NSLog(@"小图的secret -- %@"    ,body.thumbnailSecretKey);
                NSLog(@"小图的W -- %f ,大图的H -- %f",body.thumbnailSize.width,body.thumbnailSize.height);
                NSLog(@"小图的下载状态 -- %u",body.thumbnailDownloadStatus);
                
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                UIImage *defaultImage = [UIImage imageNamed:@"chat_load_pic"];
                NSData *data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:body.localPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
                UIImage *image = [UIImage imageWithData:data];
                PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:data];
                pbImage.imageId = message.messageId;
                pbImage.imageTime = message.timestamp;
                if (image == nil) {
                    
                    // 下载大图
                    [[EMClient sharedClient].chatManager downloadMessageAttachment:message progress:nil completion:^(EMMessage *message, EMError *error) {
                        
                    }];
                    
                    data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:body.thumbnailLocalPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
                    image = [UIImage imageWithData:data];
                    pbImage = [[PhotoBrowserImage alloc] initWithData:data];
                    pbImage.imageId = message.messageId;
                    pbImage.imageTime = message.timestamp;
                    if (image == nil) {
                        dic = [@{@"emMessage":message,
                                 @"messageId":message.messageId,
                                 @"picture": defaultImage,
                                 @"type": @(UUMessageTypePicture),
                                 @"from": @(message.direction),
                                 @"strName":strName,
                                 @"strTime":strTime,
                                 @"strIcon":avatarStr,
                                 @"status":@(status),
                                 @"chatType":@(_typeChat)} mutableCopy];
                        
                        // 下载缩略图
                        [[EMClient sharedClient].chatManager downloadMessageThumbnail:message progress:nil completion:^(EMMessage *message, EMError *error) {
                            if (!error) {
                                EMImageMessageBody *imageMesBody = (EMImageMessageBody *)message.body;
                                NSData *data = [AESCrypt decryptData:[NSData dataWithContentsOfFile:imageMesBody.thumbnailLocalPath] password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.to]];
                                UIImage *image = [UIImage imageWithData:data];
                                
                                // 添加图片到数据源
                                PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:data];
                                pbImage.imageId = message.messageId;
                                pbImage.imageTime = message.timestamp;
                                if (pbImage) {
                                    [self.photoArray addObject:pbImage];
                                }
                                [dic setValue:image forKey:@"picture"];
                                [self.chatModel refreshImageMessageItems:dic];
                                [self.tableView reloadData];
                            }
                        }];
                        break;
                    }
                    
                }
                // 默认图片占位
                
                // 添加图片到数据源
                
                [self.photoArray addObject:pbImage];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"picture": image,
                         @"type": @(UUMessageTypePicture),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"strIcon":avatarStr,
                         @"status":@(status),
                         @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            case EMMessageBodyTypeLocation:
            {
                EMLocationMessageBody *body = (EMLocationMessageBody *)msgBody;
                NSLog(@"纬度-- %f",body.latitude);
                NSLog(@"经度-- %f",body.longitude);
                NSLog(@"地址-- %@",body.address);
                NSString *strLat = [NSString stringWithFormat:@"%f",body.latitude];
                NSString *strLon = [NSString stringWithFormat:@"%f",body.longitude];
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                        @"messageId":message.messageId,
                        @"strLocation": body.address,
                        @"strLatitude": strLat,
                        @"strLongitude": strLon,
                        @"type": @(UUMessageTypeLocation),
                        @"from": @(message.direction),
                        @"strName":strName,
                        @"strTime":strTime,
                        @"status":@(status),
                        @"strIcon":avatarStr,
                        @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            case EMMessageBodyTypeVoice:
            {
                // 音频sdk会自动下载
                EMVoiceMessageBody *body = (EMVoiceMessageBody *)msgBody;
                NSLog(@"音频remote路径 -- %@"      ,body.remotePath);
                NSLog(@"音频local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在（音频会自动调用）
                NSLog(@"音频的secret -- %@"        ,body.secretKey);
                NSLog(@"音频文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"音频文件的下载状态 -- %u"   ,body.downloadStatus);
                NSLog(@"音频的时间长度 -- %d"      ,body.duration);
                
//                NSURL *voiceUrl = [NSURL fileURLWithPath:body.localPath];
//                if (voiceUrl == nil) {
//                    voiceUrl = [NSURL URLWithString:body.remotePath];
//                }
                NSURL *voiceUrl = [NSURL URLWithString:body.remotePath];
                NSString *voiceTime = [NSString stringWithFormat:@"%d",body.duration];
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                        @"messageId":message.messageId,
                        @"voiceUrl": voiceUrl,
                        @"strVoiceTime": voiceTime,
                        @"type": @(UUMessageTypeVoice),
                        @"from": @(message.direction),
                        @"strName":strName,
                        @"strTime":strTime,
                        @"status":@(status),
                        @"strIcon":avatarStr,
                        @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            case EMMessageBodyTypeVideo:
            {
                EMVideoMessageBody *body = (EMVideoMessageBody *)msgBody;
                
                NSLog(@"视频remote路径 -- %@"      ,body.remotePath);
                NSLog(@"视频local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"视频的secret -- %@"        ,body.secretKey);
                NSLog(@"视频文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"视频文件的下载状态 -- %u"   ,body.downloadStatus);
                NSLog(@"视频的时间长度 -- %d"      ,body.duration);
                NSLog(@"视频的W -- %f ,视频的H -- %f", body.thumbnailSize.width, body.thumbnailSize.height);
                
                // 缩略图sdk会自动下载
                NSLog(@"缩略图的remote路径 -- %@"     ,body.thumbnailRemotePath);
                NSLog(@"缩略图的local路径 -- %@"      ,body.thumbnailLocalPath);
                NSLog(@"缩略图的secret -- %@"        ,body.thumbnailSecretKey);
                NSLog(@"缩略图的下载状态 -- %u"      ,body.thumbnailDownloadStatus);
                
                UIImage *defaultImage = [UIImage yy_imageWithColor:[UIColor blackColor]];
                UIImage *image = [UIImage imageWithContentsOfFile:body.thumbnailLocalPath];
                if (image == nil) {
                    image = defaultImage;
                }
                NSString *videoTime = [XSFormatterDate getOvertime:body.duration];
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"picture": image,
                         @"videoTime":videoTime,
                         @"type": @(UUMessageTypeVideo),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            case EMMessageBodyTypeFile:
            {
                EMFileMessageBody *body = (EMFileMessageBody *)msgBody;
                NSLog(@"文件remote路径 -- %@"      ,body.remotePath);
                NSLog(@"文件local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"文件的secret -- %@"        ,body.secretKey);
                NSLog(@"文件文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"文件文件的下载状态 -- %u"   ,body.downloadStatus);
                
                NSString *size = [NSByteCountFormatter stringFromByteCount:body.fileLength countStyle:NSByteCountFormatterCountStyleFile];
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeFile),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"fileTitle":body.displayName,
                         @"fileDesc":size,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"chatType":@(_typeChat)} mutableCopy];
            }
                break;
            default:
                break;
        }
        if (message.ext[MSG_TYPE]) {
            NSLog(@"msg_type:%@",message.ext[MSG_TYPE]);
            if ([message.ext[MSG_TYPE] isEqualToString:READ_PACKET]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *txt = textBody.text;
                NSLog(@"收到红包id是-- %@",message.ext[CUSTOM_MSG_ID]);
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"strRedMessage": txt,
                         @"rpId": message.ext[CUSTOM_MSG_ID],
                         @"type": @(UUMessageTypeRedPacket),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:TRANSFER_BANLANCE] || [message.ext[MSG_TYPE] isEqualToString:TRANSFER_MONEY_COLLECTION] || [message.ext[MSG_TYPE] isEqualToString:TRANSFER_MONEY_COLLECTION_REJECT]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *txt = textBody.text;
                NSLog(@"收到转账id是-- %@",message.ext[CUSTOM_MSG_ID]);
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"strRedMessage": txt,
                         @"rpId": message.ext[CUSTOM_MSG_ID],
                         @"moneyStr":message.ext[TRANSFER_BANLANCE_TAG],
                         @"type": @(UUMessageTypeTransfer),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:MESSAGE_SHARE_TYPE]) {
                NSString *shareData = message.ext[MESSAGE_SHARE_DATA];
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeShare),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"shareJosnString":shareData,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:MESSAGE_SYSTEM_PUSH]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *shareData = textBody.text;
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeSystemPush),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"shareJosnString":shareData,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VOICE_CALL]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                NSString *text = textBody.text;
                if (message.direction == EMMessageDirectionReceive && [textBody.text isEqualToString:NSLocalizedString(@"call.offline", @"")]) {
                    text = NSLocalizedString(@"call.message.cancel", @"");
                }
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeCall),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"isVideo":@(0),
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VIDEO_CALL]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                NSString *text = textBody.text;
                if (message.direction == EMMessageDirectionReceive && [textBody.text isEqualToString:NSLocalizedString(@"call.offline", @"")]) {
                    text = NSLocalizedString(@"call.message.cancel", @"");
                }
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeCall),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"isVideo":@(1),
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([message.ext[MSG_TYPE] isEqualToString:MESSAGE_SHARE_USER]) {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *strTime = [NSString stringWithFormat:@"%lld",message.timestamp];
                NSString *text = textBody.text;
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"cardId":message.ext[MESSAGE_SHARE_USER_ID],
                         @"cardIcon":message.ext[MESSAGE_SHARE_USER_ICO],
                         @"cardName":text,
                         @"type": @(UUMessageTypeCard),
                         @"from": @(message.direction),
                         @"strName":strName,
                         @"strTime":strTime,
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":message.ext[MSG_TYPE],
                         @"ext":message.ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else {
                if (isEmptyString([[ChatHelper shareHelper] noticeStringConvertByMessage:message])) {
                    continue;
                }
                [dic setValue:[NSString stringWithFormat:@"%lld",message.timestamp] forKey:@"strTime"];
                [dic setValue:[[ChatHelper shareHelper] noticeStringConvertByMessage:message] forKey:@"strNotice"];
            }
        }
        
        NSLog(@"%@",dic);
        // 插入数据位置+1
        
        if (scroll) {
            // 增加最新消息
            [self.chatModel addMessageItems:dic];
        } else {
            // 插入历史消息
            [self.chatModel insertMessageItems:dic index:index];
        }
        index++;
        
    }
    [self.tableView reloadData];
    if (canScrollBottom) {
        // 跳转到最底部
        [self tableViewScrollToBottom];
    } else if (!scroll) {
        // 跳转到刷新的历史消息
        [self tableViewScrollToLatestRefresh];
    }
}

#pragma mark - EMChatManagerDelegate
// 接收到一条及以上非cmd消息
- (void)messagesDidReceive:(NSArray *)aMessages
{
    NSMutableArray *mutArray = [NSMutableArray array];
    for (EMMessage *mes in aMessages) {
        if ([mes.conversationId isEqualToString:_conversation.conversationId]) {
            [mutArray addObject:mes];
            
            NSLog(@"%@",mes.ext);
            // 收到红包消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
            if (mes.ext[MSG_TYPE]) {
                NSLog(@"chat view : msg_type:%@",mes.ext[MSG_TYPE]);
                if ([mes.ext[MSG_TYPE] isEqualToString:READ_PACKET] && !mes.ext[READ_PACKET_HANDLE]) {
                    NSLog(@"chat view : 收到红包id是-- %@",mes.ext[CUSTOM_MSG_ID]);
                    NSString *rpId = mes.ext[CUSTOM_MSG_ID];
                    [HTTPManager confirmSendRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                        if (state.status) {
                            // 服务器已确认红包发到
                            NSLog(@"chat view : 服务器已确认红包发到");
                            mes.ext = @{MSG_TYPE:READ_PACKET,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
                            
                            // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
                            [[EMClient sharedClient].chatManager updateMessage:mes completion:^(EMMessage *aMessage, EMError *aError) {
                                NSLog(@"更新本地消息完成，%@",aMessage.ext);
                            }];
                        } else{
                            
                        }
                    } fail:^(NSError *error) {
                        
                    }];
                }
            }
        }
    }
    if (mutArray == nil || mutArray.count == 0) {
        return;
    }
    [self dealMessages:mutArray needScrollToBottom:YES];
    [_chatArray addObjectsFromArray:mutArray];
}

// 接收到一条及以上cmd(透传)消息
- (void)cmdMessagesDidReceive:(NSArray *)aCmdMessages
{
    for (EMMessage *message in aCmdMessages) {
        EMCmdMessageBody *body = (EMCmdMessageBody *)message.body;
        NSLog(@"收到的action是 -- %@",body.action);
        if ([body.action isEqualToString:REVOKE_ACTION]) {
            // 消息撤回
            NSString *messageId = [message.ext objectForKey:CUSTOM_MSG_ID];
            for (EMMessage *mes in _chatArray) {
                if ([mes.messageId isEqualToString:messageId]) {
                    mes.ext = @{MSG_TYPE:REVOKE_ACTION};
                    [[EMClient sharedClient].chatManager updateMessage:mes completion:^(EMMessage *aMessage, EMError *aError) {
                        
                    }];
                }
            }
            
            
            for (UUMessageFrame *mesFrame in self.chatModel.dataSource) {
                EMMessage *emMessage = mesFrame.message.emMessage;
                if ([emMessage.messageId isEqualToString:messageId]) {
                    mesFrame.showNotice = YES;
                    EMMessage *emMessage = mesFrame.message.emMessage;
                    emMessage.ext = @{MSG_TYPE:REVOKE_ACTION};
                    mesFrame.message.strNotice = [[ChatHelper shareHelper] noticeStringConvertByMessage:message];
                    [mesFrame setMessage:mesFrame.message];
                }
            }
            [self.tableView reloadData];
        }
    }
}

/*!
 *  接收到一条及以上已读回执
 *
 *  @param aMessages  消息列表<EMMessage>
 */
- (void)messagesDidRead:(NSArray *)aMessages
{
    NSLog(@"----------已读回执");
    __weak typeof(self) wSelf = self;
    for (EMMessage *message in aMessages) {
        message.isReadAcked = YES;
        [[EMClient sharedClient].chatManager updateMessage:message completion:^(EMMessage *aMessage, EMError *aError) {
            // 更新消息为已读
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([aMessage.messageId isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusRead;
                }
            }
            [wSelf.tableView reloadData];
        }];
    }
    
}

/*!
 @method
 @brief 接收到一条及以上已送达回执
 */
- (void)messagesDidDeliver:(NSArray *)aMessages
{
    NSLog(@"----------送达回执");
    __weak typeof(self) wSelf = self;
    for (EMMessage *message in aMessages) {
        message.isDeliverAcked = YES;
        [[EMClient sharedClient].chatManager updateMessage:message completion:^(EMMessage *aMessage, EMError *aError) {
            // 更新消息为已送达
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([aMessage.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [wSelf.tableView reloadData];
        }];
    }
}

/*!
 @method
 @brief 消息状态改变
 */
- (void)messageStatusDidChange:(EMMessage *)aMessage error:(EMError *)aError
{
    NSLog(@"message : %@ status did change : %i",aMessage.messageId,aMessage.status);
}

#pragma mark tableview delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    if (point.y+scrollView.bounds.size.height < scrollView.contentSize.height-200) {
        // 如果在浏览之前的聊天内容，收到新消息不让跳到最底部 
        canScrollBottom = NO;
    } else {
        canScrollBottom = YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatModel.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.chatModel.dataSource[indexPath.row] cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *identifier = [NSString stringWithFormat:@"chat%ld",(long)indexPath.row];
    static NSString *identifier = @"chatCell";
    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
    }
    [cell setMessageFrame:self.chatModel.dataSource[indexPath.row]];
    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.chatKeyBoard keyboardDown];
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage]?:info[UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.8);
    UIImage *image = [UIImage imageWithData:imageData];
    // 构造图片消息
    NSData *encryptData = [AESCrypt encryptData:imageData password:[NSString stringWithFormat:@"%@%@",AES_KEY,_conversation.conversationId]];
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:encryptData displayName:@"image.jpeg"];
    NSString *from = [[EMClient sharedClient] currentUsername];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:nil];
    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
    
    NSDictionary *dic = @{@"emMessage":message,
                          @"messageId":message.messageId,
                          @"picture": image,
                          @"type": @(UUMessageTypePicture),
                          @"from": @(UUMessageFromMe),
                          @"strName":from,
                          @"strIcon":_mineAvatar,
                          @"chatType":@(_typeChat)};
    [self dealTheFunctionData:dic];
    
    
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        [_chatArray addObject:message];
        // 添加图片到数据源
        PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
        pbImage.imageId = message.messageId;
        pbImage.imageTime = message.timestamp;
        [self.photoArray addObject:pbImage];
        if (!error) {
            NSLog(@"图片消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"图片消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- ChatKeyBoardDataSource
- (NSArray<MoreItem *> *)chatKeyBoardMorePanelItems
{
    MoreItem *item1 = [MoreItem moreItemWithPicName:@"sharemore_photo" highLightPicName:nil itemName:Localized(@"拍照")];
    MoreItem *item2 = [MoreItem moreItemWithPicName:@"sharemore_pic" highLightPicName:nil itemName:@"图片"];
    MoreItem *item3 = [MoreItem moreItemWithPicName:@"sharemore_location" highLightPicName:nil itemName:Localized(@"位置")];
    MoreItem *item4 = [MoreItem moreItemWithPicName:@"sharemore_redpacket" highLightPicName:nil itemName:Localized(@"红包")];
    MoreItem *item5 = [MoreItem moreItemWithPicName:@"sharemore_transfer" highLightPicName:nil itemName:Localized(@"转账")];
    MoreItem *item6 = [MoreItem moreItemWithPicName:@"sharemore_file" highLightPicName:nil itemName:Localized(@"文件")];

    MoreItem *item7 = [MoreItem moreItemWithPicName:@"sharemore_voice" highLightPicName:nil itemName:@"语音"];
    MoreItem *item8 = [MoreItem moreItemWithPicName:@"sharemore_video" highLightPicName:nil itemName:@"视频"];
    
    MoreItem *item9 = [MoreItem moreItemWithPicName:@"sharemore_card" highLightPicName:nil itemName:@"名片"];
    if (self.conversation.type == EMConversationTypeChat) {
        self.moreItems = @[item1, item2, item3, item4, item5, item6, item7, item8, item9];
    } else {
        self.moreItems = @[item1, item2, item3, item4, item6];
    }
    return self.moreItems;
}
- (NSArray<ChatToolBarItem *> *)chatKeyBoardToolbarItems
{
    ChatToolBarItem *item1 = [ChatToolBarItem barItemWithKind:kBarItemFace normal:@"face" high:@"face_HL" select:@"keyboard"];
    ChatToolBarItem *item2 = [ChatToolBarItem barItemWithKind:kBarItemVoice normal:@"voice" high:@"voice_HL" select:@"keyboard"];
    ChatToolBarItem *item3 = [ChatToolBarItem barItemWithKind:kBarItemMore normal:@"more_ios" high:@"more_ios_HL" select:nil];
    ChatToolBarItem *item4 = [ChatToolBarItem barItemWithKind:kBarItemSwitchBar normal:@"switchDown" high:nil select:nil];
    return @[item1, item2, item3, item4];
}

- (NSArray<FaceThemeModel *> *)chatKeyBoardFacePanelSubjectItems
{
    return [FaceSourceManager loadFaceSource];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    BOOL fileUrlAuthozied = [url startAccessingSecurityScopedResource];
    if (fileUrlAuthozied) {
        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
        NSError *error;
        
        [fileCoordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL * _Nonnull newURL) {
            NSData *data = [NSData dataWithContentsOfURL:newURL];
            
            // 构造文件消息
            NSString *displayName = [[url path] lastPathComponent];
            EMFileMessageBody *filebody = [[EMFileMessageBody alloc] initWithData:data displayName:displayName];
            NSString *from = [[EMClient sharedClient] currentUsername];
            //生成Message
            EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:filebody ext:nil];
            message.chatType = (EMChatType)_conversation.type;// 设置消息类型
            
            NSString *size = [NSByteCountFormatter stringFromByteCount:filebody.fileLength countStyle:NSByteCountFormatterCountStyleFile];
            NSDictionary *dic = @{@"emMessage":message,
                                  @"messageId":message.messageId,
                                  @"type": @(UUMessageTypeFile),
                                  @"from": @(UUMessageFromMe),
                                  @"strName":from,
                                  @"fileTitle":displayName,
                                  @"fileDesc":size,
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            
            
            [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                [_chatArray addObject:message];
                if (!error) {
                    NSLog(@"文件消息发送成功");
                    for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                        UUMessageFrame *frame = self.chatModel.dataSource[i];
                        EMMessage *emMessage = frame.message.emMessage;
                        if ([message.messageId isEqualToString:emMessage.messageId]) {
                            frame.message.status = UUMessageStatusSuccessed;
                        }
                    }
                    [self.tableView reloadData];
                } else {
                    NSLog(@"文件消息发送失败");
                    for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                        UUMessageFrame *frame = self.chatModel.dataSource[i];
                        EMMessage *emMessage = frame.message.emMessage;
                        if ([message.messageId isEqualToString:emMessage.messageId]) {
                            frame.message.status = UUMessageStatusFailed;
                        }
                    }
                    [self.tableView reloadData];
                }
            }];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [url stopAccessingSecurityScopedResource];
    } else {
        // Error handling
    }
}

- (void)chatKeyBoard:(ChatKeyBoard *)chatKeyBoard didSelectMorePanelItemIndex:(NSInteger)index
{
    NSLog(@"Select %ld",(long)index);
    MoreItem *item = self.moreItems[index];
    if ([item.itemName isEqualToString:Localized(@"拍照")]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    } else if ([item.itemName isEqualToString:@"图片"]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    } else if ([item.itemName isEqualToString:Localized(@"位置")]) {
        S_BaiduMap *locationController = [S_BaiduMap new];
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        [self.navigationController pushViewController:locationController animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"红包")]) {
        RedPacketSendViewController *redPacketVC = [[RedPacketSendViewController alloc] init];
        redPacketVC.conversation = _conversation;
        [self.navigationController pushViewController:redPacketVC animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"转账")]) {
        ChatTransferViewController *transVC = [[ChatTransferViewController alloc] init];
        NickNameDataParser *parser = [NickNameDataParser new];
        parser.logo = self.avatarUrl;
        parser.nickname = self.title;
        parser.uid = self.conversation.conversationId;
        
        transVC.userInfo = parser;
        transVC.conversation = _conversation;
        [self.navigationController pushViewController:transVC animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"文件")]) {
        NSArray *documentTypes = @[@"public.content",@"public.text",@"public.source-code",@"public.image",@"public.audiovisual-content",@"com.adobe.pdf",@"com.apple.keynote.key",@"com.microsoft.word.doc",@"com.microsoft.excel.xls",@"com.microsoft.powerpoint.ppt"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
        docPicker.delegate = self;
        docPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:docPicker animated:YES completion:nil];
    } else if ([item.itemName isEqualToString:@"语音"]) {
        EMCallOptions *options = [[EMClient sharedClient].callManager getCallOptions];
        //当对方不在线时，是否给对方发送离线消息和推送，并等待对方回应
        options.isSendPushIfOffline = NO;
        [[EMClient sharedClient].callManager setCallOptions:options];

        [[ChatCallManager sharedManager] makeCallWithUsername:self.conversation.conversationId type:EMCallTypeVoice];
        
    } else if ([item.itemName isEqualToString:@"视频"]) {
        EMCallOptions *options = [[EMClient sharedClient].callManager getCallOptions];
        //当对方不在线时，是否给对方发送离线消息和推送，并等待对方回应
        options.isSendPushIfOffline = NO;
        [[EMClient sharedClient].callManager setCallOptions:options];
        
        [[ChatCallManager sharedManager] makeCallWithUsername:self.conversation.conversationId type:EMCallTypeVideo];

    } else if ([item.itemName isEqualToString:@"名片"]) {
        [ChatHelper shareHelper].isSendingCard = YES;
        TabFriendsVC *friendsVC = [[TabFriendsVC alloc] init];
        [self.navigationController pushViewController:friendsVC animated:YES];
    }
}

#pragma mark - MapLocationViewController Delegate

- (void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address {
    
    
    NSString *from = [[EMClient sharedClient] currentUsername];
    NSString *strLat = [NSString stringWithFormat:@"%f",coordinate.latitude];
    NSString *strLon = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
    EMLocationMessageBody *body = [[EMLocationMessageBody alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude address:address];
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:nil];
    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
    
    // 更新消息列表
    NSDictionary *dic = @{@"emMessage":message,
                          @"messageId":message.messageId,
                          @"strLocation": address,
                          @"strLatitude": strLat,
                          @"strLongitude": strLon,
                          @"type": @(UUMessageTypeLocation),
                          @"from": @(UUMessageFromMe),
                          @"strName":from,
                          @"strIcon":_mineAvatar,
                          @"chatType":@(_typeChat)};
    [self dealTheFunctionData:dic];
    
    //发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        [_chatArray addObject:message];
        if (!error) {
            NSLog(@"位置消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([message.messageId isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"位置消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([message.messageId isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - 输入状态
- (void)chatKeyBoardTextViewDidBeginEditing:(UITextView *)textView
{
    [self.menuController setMenuItems:@[]];
}
- (void)chatKeyBoardSendText:(NSString *)text
{
//    NSString *str = @"123456";
//    NSString *password = @"12345644";
//    NSString *encryptedData = [AESCrypt encrypt:str password:password];
//    str = [AESCrypt decrypt:encryptedData password:password];
//    NSLog(@"%@-----%@",encryptedData,str);
//    return;
    NSString *from = [[EMClient sharedClient] currentUsername];
    NSLog(@"from：%@ to %@",from,_chatterName);
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:[AESCrypt encrypt:text password:[NSString stringWithFormat:@"%@%@",AES_KEY,_conversation.conversationId]]];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:nil];
    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
    
    // 更新消息列表
    NSDictionary *dic = @{@"emMessage":message,
                          @"messageId":message.messageId,
                          @"strContent": text,
                          @"type": @(UUMessageTypeText),
                          @"from": @(UUMessageFromMe),
                          @"strName":from,
                          @"strIcon":_mineAvatar,
                          @"chatType":@(_typeChat)};
    [self dealTheFunctionData:dic];
    
    // 发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        [_chatArray addObject:message];
        if (!error) {
            NSLog(@"文字消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"文字消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
    
    
}
- (void)chatKeyBoardTextViewDidChange:(UITextView *)textView
{
    
}

#pragma mark - 录音状态

- (void)startRecording {
    [self setAudioSessionWithCategory:AVAudioSessionCategoryRecord];
    if (_audioRecorder) {
        [_audioRecorder stop];
        _audioRecorder.delegate = nil;
        _audioRecorder = nil;
    }
    if (![self.audioRecorder isRecording]) {
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder record];
        recordLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(levelTimerCallback:) userInfo:nil repeats:YES];
    }
    playTime = 0;
    playTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countVoiceTime) userInfo:nil repeats:YES];
    [UUProgressHUD show];
}

/* 该方法确实会随环境音量变化而变化，但具体分贝值是否准确暂时没有研究 */
- (void)levelTimerCallback:(NSTimer *)timer {
    [self.audioRecorder updateMeters];
    
    float   level;                // The linear 0.0 .. 1.0 value we need.
    float   minDecibels = -80.0f; // Or use -60dB, which I measured in a silent room.
    float   decibels    = [self.audioRecorder averagePowerForChannel:0];
    
    if (decibels < minDecibels)
    {
        level = 0.0f;
    }
    else if (decibels >= 0.0f)
    {
        level = 1.0f;
    }
    else
    {
        float   root            = 2.0f;
        float   minAmp          = powf(10.0f, 0.05f * minDecibels);
        float   inverseAmpRange = 1.0f / (1.0f - minAmp);
        float   amp             = powf(10.0f, 0.05f * decibels);
        float   adjAmp          = (amp - minAmp) * inverseAmpRange;
        
        level = powf(adjAmp, 1.0f / root);
    }
    
    /* level 范围[0 ~ 1], 转为[0 ~6] 之间 */
    dispatch_async(dispatch_get_main_queue(), ^{
        [UUProgressHUD changeRecordLevel:level*7+1];
    });
}
/**
 *  语音状态
 */
- (void)chatKeyBoardDidStartRecording:(ChatKeyBoard *)chatKeyBoard
{
    playTime = 0;
    [[ChatHelper shareHelper] createSystemSoundWithName:@"talkroom_begin" soundType:@"mp3" vibrate:YES];
    [self performSelector:@selector(startRecording) withObject:nil afterDelay:0.5];
    
}

- (void)chatKeyBoardDidCancelRecording:(ChatKeyBoard *)chatKeyBoard
{
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    if (playTimer) {
        [self.audioRecorder stop];
        [self.audioRecorder deleteRecording];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    }
    [UUProgressHUD dismissWithError:Localized(@"has_been_cancelled")];
}

- (void)chatKeyBoardDidFinishRecoding:(ChatKeyBoard *)chatKeyBoard
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRecording) object:nil];//可以取消成功。

    //此处需要恢复设置回放标志，否则会导致其它播放声音也会变小
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    } else {
        [UUProgressHUD showShort:RECORD_TIME_SHORT];
        return;
    }
    if (playTime == 0) {
        [UUProgressHUD showShort:RECORD_TIME_SHORT];
        return;
    }
    [UUProgressHUD dismissWithSuccess:Localized(@"录音成功")];
    NSLog(@"%@--%@",[self getSavePath],[self getSavePath].path);
    NSString *from = [[EMClient sharedClient] currentUsername];
    NSString *voiceTime = [NSString stringWithFormat:@"%d",(int)playTime];
    
//    NSString *name = [NSString stringWithFormat:@"recorder"];
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
    
    // 构造语音消息
    EMVoiceMessageBody *body = [[EMVoiceMessageBody alloc] initWithLocalPath:urlStr displayName:kRecordAudioFile];
    body.duration = [voiceTime intValue];
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:nil];
    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
    // 更新消息列表
    NSDictionary *dic = @{@"emMessage":message,
                          @"messageId":message.messageId,
                          @"voiceUrl": [self getSavePath],
                          @"strVoiceTime": voiceTime,
                          @"type": @(UUMessageTypeVoice),
                          @"from": @(UUMessageFromMe),
                          @"strName":from,
                          @"strIcon":_mineAvatar,
                          @"chatType":@(_typeChat)};
    [self dealTheFunctionData:dic];
    [[ChatHelper shareHelper] createSystemSoundWithName:@"sent_message" soundType:@"mp3" vibrate:NO];
    // 发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        [_chatArray addObject:message];
        if (!error) {
            NSLog(@"语音消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"语音消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                EMMessage *emMessage = frame.message.emMessage;
                if ([message.messageId isEqualToString:emMessage.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
    NSLog(@"录音完成!");
}

- (void)chatKeyBoardWillCancelRecoding:(ChatKeyBoard *)chatKeyBoard
{
    [UUProgressHUD changeSubTitle:RECORD_TOUCHOUT_CANCEL];
}

- (void)chatKeyBoardContineRecording:(ChatKeyBoard *)chatKeyBoard
{
    [UUProgressHUD changeSubTitle:RECORD_SLIDUP_CANCEL];
}

- (void)countVoiceTime
{
    playTime ++;
    if (playTime>=60) {
        [self chatKeyBoardDidFinishRecoding:nil];
        [UUProgressHUD dismissWithSuccess:nil];
//        [self endRecordVoice:nil];
    }
}

- (void)endRecordVoice:(UIButton *)button
{
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        playTime = 0;
        
        
        
//        //缓冲消失时间 (最好有block回调消失完成)
//        self.btnVoiceRecord.enabled = NO;
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            self.btnVoiceRecord.enabled = YES;
//        });
    }
}

/**
 *  设置音频会话
 */
-(void)setAudioSessionWithCategory:(NSString *)category {
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    //设置为录音状态
    [audioSession setCategory:category error:nil];
    [audioSession setActive:YES error:nil];
}

/**
 *  取得录音文件保存路径
 *
 *  @return 录音文件路径
 */
-(NSURL *)getSavePath{
//    NSString *name = [NSString stringWithFormat:@"recorder"];
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
    NSLog(@"file path:%@",urlStr);
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}
/**
 *  取得录音文件设置
 *
 *  @return 录音设置
 */
-(NSDictionary *)getAudioSetting{
    NSMutableDictionary *dicM=[NSMutableDictionary dictionary];
    //设置录音格式
    [dicM setObject:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
    //设置录音采样率，8000是电话采样率，对于一般录音已经够了
    [dicM setObject:@(8000) forKey:AVSampleRateKey];
    //设置通道,这里采用单声道
    [dicM setObject:@(1) forKey:AVNumberOfChannelsKey];
    //每个采样点位数,分为8、16、24、32
    [dicM setObject:@(8) forKey:AVLinearPCMBitDepthKey];
    //是否使用浮点数采样
    [dicM setObject:@(YES) forKey:AVLinearPCMIsFloatKey];
    //录音的质量
    [dicM setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    //....其他设置等
    return dicM;
}

#pragma mark - self cycle

- (ChatKeyBoard *)chatKeyBoard
{
    if (_chatKeyBoard == nil) {
        _chatKeyBoard = [ChatKeyBoard keyBoardWithNavgationBarTranslucent:NO];
        _chatKeyBoard.delegate = self;
        _chatKeyBoard.dataSource = self;
        _chatKeyBoard.keyBoardStyle = KeyBoardStyleChat;
        _chatKeyBoard.placeHolder = @"请输入消息";
    }
    return _chatKeyBoard;
}

- (AVAudioRecorder *)audioRecorder
{
    if (_audioRecorder == nil) {
        //创建录音文件保存路径
        NSURL *url=[self getSavePath];
        //创建录音格式设置
        NSDictionary *setting=[self getAudioSetting];
        NSError *error;
        _audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate = self;
        if (error) {
            NSLog(@"创建录音机对象时发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}

- (AVAudioPlayer *)audioPlayer
{
    if (!_audioPlayer) {
        NSURL *url = [self getSavePath];
        NSError *error;
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        _audioPlayer.numberOfLoops = 0;
        [_audioPlayer prepareToPlay];
        if (error) {
            NSLog(@"创建播放器过程中发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioPlayer;
}

@end
