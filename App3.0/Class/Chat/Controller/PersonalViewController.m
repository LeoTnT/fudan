
//
//  PersonalViewController.m
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PersonalViewController.h"
#import "PersonalInfoCell.h"
#import "PersonalMoreViewController.h"
#import "ChatViewController.h"
#import "ContactModel.h"
#import "UserInstance.h"
#import "FindChatContentViewController.h"
#import "UserMainViewController.h"
#import "FansCircleViewController.h"
#import "XSFormatterDate.h"

@interface PersonalViewController ()<PersonalInfoCellDelegate>
{
    NSArray *_dataArr;
    NSString *_uid;
    UserInfoDataParser *_userInfo;
    NSInteger baseIndex;
}
@property (assign, nonatomic) BOOL isShield;
@property (assign, nonatomic) BOOL isDisturb;
@property (strong, nonatomic) UISwitch *shieldSwitch;
@property (strong, nonatomic) UISwitch *disturbSwitch;
@end

@implementation PersonalViewController

- (instancetype)initWithUid:(NSString *)uid
{
    self = [super init];
    if (self) {
        _uid = uid;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"详细资料";
    _dataArr = @[@"",@[@""],@[@"查找聊天内容",Localized(@"清空聊天记录")]];
    baseIndex = 3;
    self.tableViewStyle = UITableViewStyleGrouped;
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        if (self.navigationController.viewControllers.count>=2) {
            UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
            if ([vc isKindOfClass:[FansCircleViewController class]]) {
                ((FansCircleViewController *)vc).updateDic=self.updateDic;
                self.updateDic=nil;
            }
        }
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getUserData];
}

- (void)getUserData
{
    // 是否免打扰和屏蔽
    NSArray *arr = [[DBHandler sharedInstance] getContactByUid:_uid];
    if (arr && arr.count > 0) {
        ContactDataParser *contact = arr[0];
        self.isDisturb = [contact.is_not_disturb integerValue];
        self.isShield = [contact.is_shield integerValue];
    }
    
    @weakify(self);
    [HTTPManager getUserInfoWithUid:_uid success:^(NSDictionary * dic, resultObject *state) {
        NSLog(@"==========%@",dic);
        @strongify(self);
        if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            _userInfo = parser.data;
            
            // 刷新数据库
            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
            cdParser.uid = _userInfo.uid;
            cdParser.username = _userInfo.username;
            cdParser.nickname = _userInfo.nickname;
            cdParser.avatar = _userInfo.logo;
            cdParser.relation = _userInfo.relation;
            cdParser.mobile = _userInfo.mobile;
            cdParser.remark = _userInfo.remark;
            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            [[ChatHelper shareHelper].friendVC reloadContacts];
            
            if ([[ChatHelper shareHelper].chatVC.conversation.conversationId isEqualToString:cdParser.uid]) {
                [ChatHelper shareHelper].chatVC.title = [cdParser getName];
                [ChatHelper shareHelper].chatVC.avatarUrl = cdParser.avatar;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }
            
            if ([[ChatHelper shareHelper].xmchatVC.conversationModel.conversation isEqualToString:cdParser.uid]) {
                [ChatHelper shareHelper].xmchatVC.title = [cdParser getName];
                [ChatHelper shareHelper].xmchatVC.avatarUrl = cdParser.avatar;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }
            
#ifdef APP_SHOW_JYS
            if ([[UserInstance ShardInstnce].uid isEqualToString:_uid]) {
                // 是自己
                _dataArr = @[@"",@[@""]];
            } else if ([_userInfo.relation integerValue] == ContactRelationFriend) {
                _dataArr = @[@"",@[@"电话"],@[@"查找聊天内容",Localized(@"清空聊天记录")],@"对话",@"取消关注"];
            } else if ([_userInfo.relation integerValue] == ContactRelationAttention) {
                _dataArr = @[@"",@[@""],@[@"查找聊天内容",Localized(@"清空聊天记录")],@"对话",@"取消关注"];
            } else {
                _dataArr = @[@"",@[@""],@[@"查找聊天内容",Localized(@"清空聊天记录")],@"对话",Localized(@"me_contact_focus")];
            }
            baseIndex = 3;
#else
            if ([[UserInstance ShardInstnce].uid isEqualToString:_uid]) {
                // 是自己
                _dataArr = @[@"",@[@""]];
            } else if ([_userInfo.relation integerValue] == ContactRelationFriend) {
                _dataArr = @[@"",@[@"电话"],@[@"查找聊天内容",Localized(@"清空聊天记录"),@"个人动态"],@[@"消息免打扰",@"加入黑名单"],@"对话",@"取消关注"];
            } else if ([_userInfo.relation integerValue] == ContactRelationAttention) {
                _dataArr = @[@"",@[@""],@[@"查找聊天内容",Localized(@"清空聊天记录"),@"个人动态"],@[@"消息免打扰",@"加入黑名单"],@"对话",@"取消关注"];
            } else {
                _dataArr = @[@"",@[@""],@[@"查找聊天内容",Localized(@"清空聊天记录"),@"个人动态"],@[@"消息免打扰",@"加入黑名单"],@"对话",Localized(@"me_contact_focus")];
            }
            baseIndex = 4;
#endif

            [self.tableView reloadData];
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
    
    
}

// 开始会话
- (void)chatAction:(UIButton *)pSender
{
    // 获取上一个页面class类型
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    NSLog(@"%d-%@", (int)index,[self.navigationController.viewControllers[index - 1] class]);
#ifdef ALIYM_AVALABLE
    if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[YWChatViewController class]] && ((YWChatViewController*)self.navigationController.viewControllers[index - 1]).conversation.conversationType == YWConversationTypeP2P) {
        // 如果上一个页面是聊天界面则直接pop返回
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        if ([_userInfo.relation integerValue] == ContactRelationFriend || [_userInfo.relation integerValue] == ContactRelationAttention || [_userInfo.relation integerValue] == ContactRelationFans) {
            YWPerson *person = [[YWPerson alloc] initWithPersonId:_userInfo.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [_userInfo getName];
            chatVC.avatarUrl = _userInfo.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"对方还不是您的好友"];
        }
    }
#elif defined EMIM_AVALABLE
    if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[ChatViewController class]] && ((ChatViewController*)self.navigationController.viewControllers[index - 1]).conversation.type == EMChatTypeChat) {
        // 如果上一个页面是聊天界面则直接pop返回
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        if ([_userInfo.relation integerValue] == ContactRelationFriend || [_userInfo.relation integerValue] == ContactRelationAttention || [_userInfo.relation integerValue] == ContactRelationFans) {
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:_userInfo.uid type:EMConversationTypeChat createIfNotExist:YES];
            
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [_userInfo getName];
            chatVC.avatarUrl = _userInfo.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"对方还不是您的好友"];
        }
    }
#else
    if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[XMChatController class]]) {
        // 如果上一个页面是聊天界面则直接pop返回
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        SiganlChatModel *chatModel = [SiganlChatModel new];
        chatModel.conversationID = getJID(_userInfo.uid);
        chatModel.title = [_userInfo getName];
        chatModel.avatarURLPath = _userInfo.logo;
        chatModel.userName = _userInfo.username;
        chatModel.relation = _userInfo.relation;
        ConversationModel *mode = [[ConversationModel alloc] initWithConversation:chatModel];
        
        XMChatController *chatVC = [XMChatController new];
        chatVC.conversationModel = mode;
        [self.navigationController pushViewController:chatVC animated:YES];
        
        
        chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    }
    
#endif
    
}

- (void) getUserInfor:(NSString *)uid {
    [HTTPManager getUserInfoWithUid:uid success:^(NSDictionary * dic, resultObject *state) {
        
        if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            _userInfo = parser.data;
            
            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
            cdParser.uid = _userInfo.uid;
            cdParser.username = _userInfo.username;
            cdParser.nickname = _userInfo.nickname;
            cdParser.avatar = _userInfo.logo;
            cdParser.relation = _userInfo.relation;
            cdParser.mobile = _userInfo.mobile;
            cdParser.remark = _userInfo.remark;
            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            [[ChatHelper shareHelper].friendVC reloadContacts];
            
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}

// 关注
- (void)attentionAction:(UIButton *)pSender
{
    @weakify(self);
    if ([_userInfo.relation integerValue] == ContactRelationFriend || [_userInfo.relation integerValue] == ContactRelationAttention) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"不再关注此人?" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager attentionFansCancelWithUid:_uid success:^(NSDictionary * dic, resultObject *state) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:state.info];
                if (state.status) {
                    [pSender setTitle:Localized(@"me_contact_focus") forState:UIControlStateNormal];
//                    [self getUserData];
                    // 刷新联系人列表
                    [[ChatHelper shareHelper].friendVC getRelationsData];
                    
                    // 发送透传消息
                    NSDictionary *cmdExtDic = @{MSG_TYPE:MESSAGE_RELATION_CHANGE};
#ifdef ALIYM_AVALABLE
                    YWMessageBodyCustomize *cmdBody = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:cmdExtDic.mj_JSONString summary:MESSAGE_RELATION_CHANGE isTransparent:YES];
                    YWPerson *person = [[YWPerson alloc] initWithPersonId:_uid];
                    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                    [conversation asyncSendMessageBody:cmdBody progress:^(CGFloat progress, NSString *messageID) {
                        
                    } completion:^(NSError *error, NSString *messageID) {
                        
                    }];
                    [[[SPKitExample sharedInstance].ywIMKit.IMCore getConversationService] removeConversationByConversationId:conversation.conversationId error:nil];
                    [self.navigationController popToRootViewControllerAnimated:YES];
#elif defined EMIM_AVALABLE
                    NSString *from = [[EMClient sharedClient] currentUsername];
                    // 生成message
                    EMCmdMessageBody *cmdBody = [[EMCmdMessageBody alloc] initWithAction:MESSAGE_RELATION_CHANGE];
                    EMMessage *cmdMessage = [[EMMessage alloc] initWithConversationID:_uid from:from to:_uid body:cmdBody ext:cmdExtDic];
                    cmdMessage.chatType = EMChatTypeChat;// 设置消息类型
                    //发送消息
                    [[EMClient sharedClient].chatManager sendMessage:cmdMessage progress:^(int progress) {
                        
                    } completion:^(EMMessage *message, EMError *error) {
                        
                    }];
                    
                    [[EMClient sharedClient].chatManager deleteConversation:_uid isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError) {
                        @strongify(self);
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }];
#else
                    [XMPPAddFriendsManager removeBuddyWithJid:_uid];
//                    [self getUserInfor:_uid];
                    [self.navigationController popToRootViewControllerAnimated:YES];
#endif
                    
                    
                }
                [XSTool hideProgressHUDWithView:self.view];
            } fail:^(NSError * _Nonnull error) {
                [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
                [XSTool hideProgressHUDWithView:self.view];
            }];
            
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager attentionFansWithUid:_uid success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [pSender setTitle:@"取消关注" forState:UIControlStateNormal];

                // 刷新联系人列表
                [[ChatHelper shareHelper].friendVC getRelationsData];
                
                // 发送透传消息
                NSDictionary *cmdExtDic = @{MSG_TYPE:MESSAGE_RELATION_CHANGE};
#ifdef ALIYM_AVALABLE
                YWMessageBodyCustomize *cmdBody = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:cmdExtDic.mj_JSONString summary:MESSAGE_RELATION_CHANGE isTransparent:YES];
                YWPerson *person = [[YWPerson alloc] initWithPersonId:_uid];
                YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                [conversation asyncSendMessageBody:cmdBody progress:^(CGFloat progress, NSString *messageID) {
                    
                } completion:^(NSError *error, NSString *messageID) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }];
#elif defined EMIM_AVALABLE
                EMCmdMessageBody *cmdBody = [[EMCmdMessageBody alloc] initWithAction:MESSAGE_RELATION_CHANGE];
                NSString *from = [[EMClient sharedClient] currentUsername];
                
                // 生成message
                EMMessage *cmdMessage = [[EMMessage alloc] initWithConversationID:_uid from:from to:_uid body:cmdBody ext:cmdExtDic];
                cmdMessage.chatType = EMChatTypeChat;// 设置消息类型
                //发送消息
                [[EMClient sharedClient].chatManager sendMessage:cmdMessage progress:^(int progress) {
                    
                } completion:^(EMMessage *message, EMError *error) {
                    
                }];
#else
                [XMPPAddFriendsManager addBuddyWithJid:_uid];
//                [self getUserInfor:_uid];
//                [self.navigationController popToRootViewControllerAnimated:YES];
#endif
                
                
                // 发送关注消息
                NSString *text = [NSString stringWithFormat:@"%@关注了你，快来聊天吧。",[UserInstance ShardInstnce].nickName];
                NSDictionary *extDic = @{MSG_TYPE:MESSAGE_FOCUS_TYPE};
                
#ifdef ALIYM_AVALABLE
                YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                    
                } completion:^(NSError *error, NSString *messageID) {
                    // 获取上一个页面class类型
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
                    NSLog(@"%d-%@", (int)index,[self.navigationController.viewControllers[index - 1] class]);
                    if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[YWChatViewController class]]) {
                        // 如果上一个页面是聊天界面则直接pop返回
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        // 开始会话
                        YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                        chatVC.title = [_userInfo getName];
                        chatVC.avatarUrl = _userInfo.logo;
                        chatVC.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:chatVC animated:YES];
                    }
                }];
#elif defined EMIM_AVALABLE
                // 生成message
                EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                EMMessage *message = [[EMMessage alloc] initWithConversationID:_uid from:[UserInstance ShardInstnce].uid to:_uid body:body ext:extDic];
                message.chatType = EMChatTypeChat;// 设置消息类型
                [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *message, EMError *error) {
                    // 获取上一个页面class类型
                    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
                    NSLog(@"%d-%@", (int)index,[self.navigationController.viewControllers[index - 1] class]);
                    if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[ChatViewController class]]) {
                        // 如果上一个页面是聊天界面则直接pop返回
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        // 开始会话
                        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:_userInfo.uid type:EMConversationTypeChat createIfNotExist:YES];
                        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                        chatVC.title = [_userInfo getName];
                        chatVC.avatarUrl = _userInfo.logo;
                        chatVC.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:chatVC animated:YES];
                    }
                }];
#else
                [XMPPSignal sendMessage:text to:_uid ext:extDic complete:nil];
                // 获取上一个页面class类型
                NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
                NSLog(@"%d-%@", (int)index,[self.navigationController.viewControllers[index - 1] class]);
                if ([self.navigationController.viewControllers[index - 1] isKindOfClass:[XMChatController class]]) {
                    // 如果上一个页面是聊天界面则直接pop返回
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    // 开始会话
                    XMChatController *chatVC = [XMChatController new];
                    SiganlChatModel *chatModel = [SiganlChatModel new];
                    chatModel.conversationID = getJID(_uid);
                    chatModel.title = [_userInfo getName];
                    chatModel.avatarURLPath = _userInfo.logo;
                    chatModel.userName = _userInfo.username;
                    chatModel.relation = _userInfo.relation;
                    ConversationModel *mode = [[ConversationModel alloc] initWithConversation:chatModel];
                    chatVC.conversationModel = mode;
                    [self.navigationController pushViewController:chatVC animated:YES];
                }
#endif
                
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
        }];
    }
}

// 消息免打扰
- (void)disturbAction:(UISwitch *)pSender
{
    [XSTool showProgressHUDTOView:self.view withText:nil];
    if (pSender.on) {
        [HTTPManager setContact:_uid isShield:-1 isNotDisturb:1 success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.isDisturb = YES;
                [XSTool showToastWithView:self.view Text:@"开启免打扰"];
                [[DBHandler sharedInstance] updateContactWithKey:@"disturb" value:@"1" byUid:_uid];
            } else {
                self.isDisturb = NO;
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = NO;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
            pSender.on = NO;
            self.isDisturb = NO;
        }];
    } else {
        [HTTPManager setContact:_uid isShield:-1 isNotDisturb:0 success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"关闭免打扰"];
                self.isDisturb = NO;
                [[DBHandler sharedInstance] updateContactWithKey:@"disturb" value:@"0" byUid:_uid];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = YES;
                self.isDisturb = YES;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
            pSender.on = YES;
            self.isDisturb = YES;
        }];
    }
}

// 屏蔽消息
- (void)shieldAction:(UISwitch *)pSender
{
    [XSTool showProgressHUDTOView:self.view withText:nil];
    if (pSender.on) {
        [HTTPManager setContact:_uid isShield:1 isNotDisturb:-1 success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"已将对方加入黑名单"];
                self.isShield = YES;
                [[DBHandler sharedInstance] updateContactWithKey:@"shield" value:@"1" byUid:_uid];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = NO;
                self.isShield = NO;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            pSender.on = NO;
            self.isShield = NO;
        }];
    } else {
        [HTTPManager setContact:_uid isShield:0 isNotDisturb:-1 success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"已将对方移出黑名单"];
                self.isShield = NO;
                [[DBHandler sharedInstance] updateContactWithKey:@"shield" value:@"0" byUid:_uid];
            } else {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = YES;
                self.isShield = YES;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
            pSender.on = YES;
            self.isShield = YES;
        }];
    }
}

#pragma mark - PersonalInfoCellDelegate
- (void)remarkClick {
    //修改昵称
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"请输入备注名称" preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alert) weakAlert = alert;
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //获取输入的昵称
        NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
        
        __block NSString *remarkName = [weakAlert.textFields.lastObject text];
        //提交昵称
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager setUserRemarkNameWithUid:_userInfo.uid remarkName:[weakAlert.textFields.lastObject text] success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"修改成功"];
                _userInfo.remark = remarkName;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                [ChatHelper shareHelper].chatVC.title = remarkName;
                [ChatHelper shareHelper].xmchatVC.title = remarkName;
                
                ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                cdParser.uid = _userInfo.uid;
                cdParser.username = _userInfo.username;
                cdParser.nickname = _userInfo.nickname;
                cdParser.avatar = _userInfo.logo;
                cdParser.relation = _userInfo.relation;
                cdParser.remark = remarkName;
                cdParser.mobile = _userInfo.mobile;
                [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                [[ChatHelper shareHelper].friendVC reloadContacts];
                if (self.getRemar) {
                    self.getRemar([cdParser getName]);
                }
                
                [[XMPPManager sharedManager].deletedMessageSubject sendNext:RACTuplePack(@"reloadRoomMember",nil)];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
        }];
        
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"点击了取消按钮");
    }]];
    // 添加文本框
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}

- (void)textFieldDidChange:(UITextField *)textField
{

    if (textField.text.length > 16) {
        textField.text = [textField.text substringToIndex:16];
    }
}

- (void)longTap:(UILongPressGestureRecognizer *)ges {
    if (ges.state == UIGestureRecognizerStateBegan && !isEmptyString(_userInfo.mobile)) {
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = _userInfo.mobile;
        [XSTool showToastWithView:self.view Text:@"已复制到剪贴板"];
    }
    
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_dataArr[section] isKindOfClass:[NSArray class]]) {
        NSArray *arr = _dataArr[section];
        return arr.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 85;
    } else if (indexPath.section == baseIndex || indexPath.section == baseIndex+1) {
        return 50;
    } else if (indexPath.section == 1 && [_userInfo.relation integerValue] != ContactRelationFriend) {
        return 0.1;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1 && [_userInfo.relation integerValue] != ContactRelationFriend) {
        return 0.1;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell.textLabel.text isEqualToString:@"查找聊天内容"]) {
#ifdef ALIYM_AVALABLE
        YWPerson *person = [[YWPerson alloc] initWithPersonId:_uid];
        YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
        FindChatContentViewController *findVC = [[FindChatContentViewController alloc] init];
        findVC.conversation = conversation;
        [self.navigationController pushViewController:findVC animated:YES];
#elif defined EMIM_AVALABLE
        FindChatContentViewController *findVC = [[FindChatContentViewController alloc] init];
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:_uid type:EMConversationTypeChat createIfNotExist:YES];
        findVC.conversation = conversation;
        [self.navigationController pushViewController:findVC animated:YES];
#else
        FindChatContentViewController *findVC = [[FindChatContentViewController alloc] init];
        findVC.conversation = _userInfo.uid;
        [self.navigationController pushViewController:findVC animated:YES];
#endif
    } else if ([cell.textLabel.text isEqualToString:Localized(@"清空聊天记录")]) {
        @weakify(self);
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定删除聊天记录？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
#ifdef ALIYM_AVALABLE
            YWPerson *person = [[YWPerson alloc] initWithPersonId:_uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            [conversation removeAllLocalMessages];
            [self.navigationController popToRootViewControllerAnimated:YES];
#elif defined EMIM_AVALABLE
            [[EMClient sharedClient].chatManager deleteConversation:_uid isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError) {
                @strongify(self);
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
#else
            [XMPPSignal deletedChatHistoryWithUserID:_userInfo.uid];
            [self.navigationController popToRootViewControllerAnimated:YES];
#endif
            
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([cell.textLabel.text isEqualToString:@"个人动态"]) {
        if(self.navigationController.viewControllers.count>=2){
            UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
            if ([vc isKindOfClass:[UserMainViewController class]]) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
        }
        // 朋友圈个人主页
        UserMainViewController *userMainVC=[[UserMainViewController alloc] init];
        userMainVC.userName=_userInfo.username;
        userMainVC.uid=_userInfo.uid;
        [self.navigationController pushViewController:userMainVC animated:YES];
    }  else if ([cell.textLabel.text isEqualToString:@"电话"]) {
        // 调用打电话功能
        NSString *telStr = [NSString stringWithFormat:@"tel://%@",_userInfo.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telStr]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat leftSpace = 10;
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    static NSString *CellIdentifier = @"personal_cell";
    if (section >= 1 && section <= baseIndex-1) {
        UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.font = [UIFont systemFontOfSize:14];;

            if (row > 0) {
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, 0, mainWidth-leftSpace, 0.5)];
                lineView.backgroundColor = LINE_COLOR_NORMAL;
                [cell addSubview:lineView];
            }
            if (section == 2) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else if (section == 3) {
                UISwitch *swith = [[UISwitch alloc] initWithFrame:CGRectMake(mainWidth-leftSpace-60, 7, 60, 30)];
                swith.on = NO;
                if (row == 0) {
                    self.disturbSwitch = swith;
                    [swith addTarget:self action:@selector(disturbAction:) forControlEvents:UIControlEventValueChanged];
                    swith.on = self.isDisturb;
                } else if (row == 1) {
                    self.shieldSwitch = swith;
                    [swith addTarget:self action:@selector(shieldAction:) forControlEvents:UIControlEventValueChanged];
                    swith.on = self.isShield;
                }
                [cell addSubview:swith];
            } else {
                UILongPressGestureRecognizer *gestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap:)];
                [cell addGestureRecognizer:gestureRecognizer];
                gestureRecognizer.minimumPressDuration = 1.0;
                if ([_userInfo.relation integerValue] == ContactRelationFriend) {
                    if (!_userInfo.mobile || [_userInfo.mobile isEqualToString:@""]) {
                        cell.detailTextLabel.text = Localized(@"no_bind");
                    } else {
                        cell.detailTextLabel.text = _userInfo.mobile;
                        
                    }
                    
                }
            }
        }
        cell.textLabel.text = _dataArr[section][row];
        if (section == 1) {
            if ([_userInfo.relation integerValue] == ContactRelationFriend) {
                if (!_userInfo.mobile || [_userInfo.mobile isEqualToString:@""]) {
                    cell.detailTextLabel.text = Localized(@"no_bind");
                } else {
                    cell.detailTextLabel.text = _userInfo.mobile;
                }
                
            } else {
                cell.detailTextLabel.text = @"";
            }
        }
        return cell;
    }
    else if (section == baseIndex || section == baseIndex+1) {
        UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            XSCustomButton *btn;
            if (section == baseIndex) {
                btn = [[XSCustomButton alloc] initWithFrame:CGRectMake(20, 0, mainWidth-40, 44) title:_dataArr[section] titleColor:[UIColor whiteColor] fontSize:15 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
                [btn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
                [btn addTarget:self action:@selector(chatAction:) forControlEvents:UIControlEventTouchUpInside];
            } else if (section == baseIndex+1) {
                btn = [[XSCustomButton alloc] initWithFrame:CGRectMake(20, 0, mainWidth-40, 44) title:_dataArr[section] titleColor:[UIColor blackColor] fontSize:15 backgroundColor:[UIColor whiteColor] higTitleColor:[UIColor blackColor] highBackgroundColor:[UIColor whiteColor]];
                [btn setBorderWith:1 borderColor:[LINE_COLOR_NORMAL CGColor] cornerRadius:5];
                [btn addTarget:self action:@selector(attentionAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [cell addSubview:btn];
        }
        return cell;
    }
    static NSString *CellIdentifier1 = @"personal_info_cell";
    PersonalInfoCell *cell = (PersonalInfoCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier1];
    if(cell == nil)
    {
        cell = [[PersonalInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    
    //从本地数据库获取信息
//    NSArray *contacts = [[DBHandler sharedInstance] getContactByUserName:_userName];
    if (_userInfo != nil) {
        [cell setCellData:_userInfo];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


@end
