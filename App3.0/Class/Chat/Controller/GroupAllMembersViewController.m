//
//  GroupAllMembersViewController.m
//  App3.0
//
//  Created by mac on 17/4/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupAllMembersViewController.h"
#import "GroupMemberCell.h"
#import "GroupMemberManagerViewController.h"
#import "PersonalViewController.h"
#import "PersonViewController.h"

@interface GroupAllMembersViewController () < GroupMemberCellDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation GroupAllMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"全部群成员";
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    self.dataArray = [NSMutableArray array];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)setDataSource:(GroupDataModel *)dataSource
//{
//    _dataSource = dataSource;
//    [self.tableView reloadData];
//}

- (void)requestData {
    [XSTool showProgressHUDWithView:self.view];
    [XSHTTPManager post:Url_New_GroupMemberList parameters:@{@"id":self.groupId} success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.dataArray = [NickNameDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            self.dataSource.members = [NSArray arrayWithArray:self.dataArray];
            [self.tableView reloadData];
        } else {
            
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
    }];
}

#pragma mark - groupMemberCellDelegate
- (void)groupMemberClickOfIndex:(NSString *)indexString
{
    if ([indexString isEqualToString:@"add"]) {
        NSLog(@"++++++");
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *array = [[DBHandler sharedInstance] getAllContact];
        for (ContactDataParser *model in array) {
            BOOL has = NO;
            for (NickNameDataParser *nParser in self.dataSource.members) {
                if ([model.uid isEqualToString:nParser.uid]) {
                    has = YES;
                    break;
                }
            }
            if (!has) {
                [tempArr addObject:model];
            }
        }
        GroupMemberManagerViewController *gmVC = [[GroupMemberManagerViewController alloc] initWithGroupId:self.groupId memberArray:tempArr];
        gmVC.gmType = GMManagerAdd;
        [self.navigationController pushViewController:gmVC animated:YES];
    } else if ([indexString isEqualToString:@"del"]) {
        NSLog(@"--------");
        NSMutableArray *tempArr = [NSMutableArray array];
        for (NickNameDataParser *nParser in self.dataSource.members) {
            ContactDataParser *parser = [[ContactDataParser alloc] init];
            parser.uid = nParser.uid;
            parser.avatar = nParser.logo;
            parser.nickname = nParser.nickname;
            [tempArr addObject:parser];
        }
        GroupMemberManagerViewController *gmVC = [[GroupMemberManagerViewController alloc] initWithGroupId:self.groupId memberArray:tempArr];
        gmVC.gmType = GMManagerSub;
        [self.navigationController pushViewController:gmVC animated:YES];
    } else {
        if ([indexString isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:indexString];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    }
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isGroupOwner) {
        return 40+(mainWidth/5+20)*(1+(self.dataSource.members.count+1)/5); // + -
    }
    if (self.dataSource.allowinvites) {
        return 40+(mainWidth/5+20)*(1+(self.dataSource.members.count)/5); // +
    } else {
        return 40+(mainWidth/5+20)*(1+(self.dataSource.members.count-1)/5); //无
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"personal_info";
    GroupMemberCell *cell = (GroupMemberCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[GroupMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        cell.showCheckAll = NO;
    }
    cell.isGroupOwner = self.isGroupOwner;
    [cell setupMembersWithData:self.dataSource];
    return cell;
}


@end
