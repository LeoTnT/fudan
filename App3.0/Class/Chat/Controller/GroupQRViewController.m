//
//  GroupQRViewController.m
//  App3.0
//
//  Created by mac on 2018/1/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "GroupQRViewController.h"

@interface GroupQRViewController ()
@property (strong, nonatomic) UIImage *qrImage;
@property (strong, nonatomic) UIView *shadeView;    // 如果开启进群验证，加遮罩
@end

@implementation GroupQRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title = @"群二维码名片";
    self.view.backgroundColor = BG_COLOR;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubviews {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(21);
        make.right.mas_equalTo(self.view).offset(-21);
        make.top.mas_equalTo(self.view).offset(35);
        make.height.mas_equalTo(400);
    }];
    
    UIImageView *qrAvatar = [UIImageView new];
    [qrAvatar getImageWithUrlStr:self.groupModel.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    qrAvatar.layer.masksToBounds = YES;
    qrAvatar.layer.cornerRadius = 3;
    [bgView addSubview:qrAvatar];
    [qrAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(bgView).offset(21);
        make.size.mas_equalTo(CGSizeMake(62, 62));
    }];
    
    UILabel *qrTitle = [UILabel new];
    qrTitle.text = self.groupModel.name;
    qrTitle.font = [UIFont systemFontOfSize:18];
    [bgView addSubview:qrTitle];
    [qrTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(qrAvatar.mas_right).offset(10);
        make.centerY.mas_equalTo(qrAvatar);
        make.right.mas_lessThanOrEqualTo(bgView).offset(-21);
    }];
    
    UIImageView *qrImageView = [[UIImageView alloc] initWithImage:self.qrImage];
    [bgView addSubview:qrImageView];
    [qrImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(qrAvatar.mas_bottom).offset(21);
        make.centerX.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(240, 240));
    }];
    
    if (self.groupModel.invite_need_confirm) {
        _shadeView = [UIView new];
        _shadeView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.98];
        [bgView addSubview:_shadeView];
        [_shadeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(qrImageView);
        }];
        
        UILabel *shadeTitle = [UILabel new];
        shadeTitle.backgroundColor = [UIColor whiteColor];
        shadeTitle.text = @"群主已开启进群验证 \n只可通过邀请进群";
        shadeTitle.textColor = COLOR_999999;
        shadeTitle.textAlignment = NSTextAlignmentCenter;
        shadeTitle.font = [UIFont systemFontOfSize:20];
        shadeTitle.numberOfLines = 2;
        [_shadeView addSubview:shadeTitle];
        [shadeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_shadeView);
            make.centerY.mas_equalTo(_shadeView);
        }];
    }
    
}

- (void)setGroupModel:(GroupDataModel *)groupModel {
    _groupModel = groupModel;
    self.qrImage = [XSQR createQrImageWithContentString:groupModel.gid type:XSQRTypeGroup];
    if (groupModel.in_group) {
        [self setSubviews];
    } else {
        [self setInviteSubviews];
    }
    
}

#pragma mark - 群聊邀请

- (void)setInviteSubviews {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.height.mas_equalTo(185);
    }];
    
    UIImageView *qrAvatar = [UIImageView new];
    [qrAvatar getImageWithUrlStr:self.groupModel.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    qrAvatar.layer.masksToBounds = YES;
    qrAvatar.layer.cornerRadius = 3;
    [bgView addSubview:qrAvatar];
    [qrAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView).offset(33.5);
        make.centerX.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(62, 62));
    }];
    
    UILabel *qrTitle = [UILabel new];
    qrTitle.text = self.groupModel.name;
    qrTitle.font = [UIFont systemFontOfSize:18];
    [bgView addSubview:qrTitle];
    [qrTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(qrAvatar.mas_bottom).offset(16.5);
    }];
    
    UILabel *groupCount = [UILabel new];
    groupCount.text = [NSString stringWithFormat:@"%@人",self.groupModel.member_count];
    groupCount.textColor = COLOR_999999;
    groupCount.font = [UIFont systemFontOfSize:16];
    [bgView addSubview:groupCount];
    [groupCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(qrTitle.mas_bottom).offset(10.5);
    }];
    
    UILabel *confirm = [UILabel new];
    confirm.text = @"是否加入该群聊";
    confirm.textColor = COLOR_666666;
    confirm.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:confirm];
    [confirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(bgView.mas_bottom).offset(41.5);
    }];
    
    UIButton *join = [UIButton buttonWithType:UIButtonTypeCustom];
    [join setTitle:@"加入群聊" forState:UIControlStateNormal];
    [join setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    join.titleLabel.font = [UIFont systemFontOfSize:16];
    [join setBackgroundColor:mainColor];
    join.layer.masksToBounds = YES;
    join.layer.cornerRadius = 3;
    [join addTarget:self action:@selector(joinAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:join];
    [join mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(21);
        make.right.mas_equalTo(self.view).offset(-21);
        make.top.mas_equalTo(confirm.mas_bottom).offset(32);
        make.height.mas_equalTo(50);
    }];
}

- (void)joinAction {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager addMembers:[UserInstance ShardInstnce].uid forGroup:self.groupModel.gid success:^(NSDictionary * dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            // 添加群成员成功
            GroupMemberAddResultModel *model = [GroupMemberAddResultModel mj_objectWithKeyValues:dic[@"data"]];
            if ([model.invite_id integerValue] != 0) {
                // 需要群主审核
                
                // 发送添加群成员申请消息
                NSString *text = [NSString stringWithFormat:@"%@想邀请%@位朋友加入群聊",[UserInstance ShardInstnce].nickName,model.invite_num];
                NSDictionary *extDic = @{MSG_TYPE:GROUP_INVITE_NEED_CONFIRM,GROUP_INVITE_ID:model.invite_id,GROUP_INVITE_OWNER:model.owner};
                
#ifdef ALIYM_AVALABLE
                YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                YWTribe *tribe = [YWTribe new];
                tribe.tribeId = self.groupModel.gid;
                YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                    
                } completion:^(NSError *error, NSString *messageID) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }];
#elif defined EMIM_AVALABLE
                // 生成message
                EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                EMMessage *message = [[EMMessage alloc] initWithConversationID:self.groupModel.gid from:[UserInstance ShardInstnce].uid to:self.groupModel.gid body:body ext:extDic];
                message.chatType = EMChatTypeGroupChat;// 设置消息类型
                //发送消息
                [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                    
                } completion:^(EMMessage *message, EMError *error) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }];
#else
                [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
                
            } else {
                // 发送添加群成员消息
                NSString *text = [NSString stringWithFormat:@"%@邀请%@加入群聊",[UserInstance ShardInstnce].nickName,model.nick_str];
                NSDictionary *extDic = @{MSG_TYPE:GROUP_INVITE_ACTION};
#ifdef ALIYM_AVALABLE
                YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                YWTribe *tribe = [YWTribe new];
                tribe.tribeId = self.groupModel.gid;
                YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                    
                } completion:^(NSError *error, NSString *messageID) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }];
#elif defined EMIM_AVALABLE
                // 生成message
                EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                EMMessage *message = [[EMMessage alloc] initWithConversationID:self.groupModel.gid from:[UserInstance ShardInstnce].uid to:self.groupModel.gid body:body ext:extDic];
                message.chatType = EMChatTypeGroupChat;// 设置消息类型
                //发送消息
                [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                    
                } completion:^(EMMessage *message, EMError *error) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }];
#else
                [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
            }

        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}
@end
