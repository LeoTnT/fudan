//
//  GroupInfoViewController.h
//  App3.0
//
//  Created by mac on 17/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface GroupInfoViewController : XSBaseTableViewController
- (instancetype)initWithGroupId:(NSString *)gId;

@property (nonatomic ,copy)void (^roomInfor)(NSString *title);
@property (nonatomic ,strong)XMPPRoomManager *roomManager;
@end
