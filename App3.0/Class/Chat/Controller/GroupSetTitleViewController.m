//
//  AddGroupViewController.m
//  App3.0
//
//  Created by mac on 2017/6/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupSetTitleViewController.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "UserInstance.h"
#import "XMGroupChatController.h"

@interface GroupSetTitleViewController ()
@property (strong, nonatomic) UITextField *groupTitleTF;

@property (nonatomic ,strong) XMPPRoom *xmppRoom;
@property (nonatomic ,strong) XMPPRoomManager *roomManager;
@end

@implementation GroupSetTitleViewController
{
    NSString *_nick_str;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.navigationItem.title = @"设置群名称";
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubviews {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, mainWidth, 50)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgView];
    
    _groupTitleTF = [[UITextField alloc] initWithFrame:CGRectMake(12, 10, mainWidth-24, 30)];
    _groupTitleTF.placeholder = @"请设置群名称";
    _groupTitleTF.font = [UIFont systemFontOfSize:14];
    [_groupTitleTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_groupTitleTF];
    
    // 创建
    XSCustomButton *loginBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(25, CGRectGetMaxY(bgView.frame)+45, mainWidth-50, 50) title:Localized(@"material_dialog_positive_text") titleColor:[UIColor whiteColor] fontSize:15 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [loginBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
    [loginBtn addTarget:self action:@selector(createAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)createAction {
    [self.view endEditing:YES];
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    NSMutableDictionary *groupInfo = self.groupOptions.mj_keyValues;
    [groupInfo setValue:self.groupTitleTF.text forKey:@"name"];
    [HTTPManager addGroup:groupInfo success:^(NSDictionary * dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        NSLog(@"%@",dic);
        @strongify(self);
        if (state.status) {
            // 创建群组成功！
            [XSTool showToastWithView:self.view Text:@"创建成功"];
            GroupAddResultModel *garModel = [GroupAddResultModel mj_objectWithKeyValues:dic[@"data"]];
            
            // 将群信息加入本地数据库
            GroupDataModel *model = [GroupDataModel new];
            model.gid = garModel.gid;
            model.name = garModel.group_name;
            model.avatar = garModel.avatar;
            _nick_str = garModel.nick_str;
            NSArray *members = [garModel.nick_str componentsSeparatedByString:@","];
            model.member_count = [NSString stringWithFormat:@"%lu",members.count+1];
            [[DBHandler sharedInstance] addOrUpdateGroup:model];
            
            // 生成群头像
            [HTTPManager generateAvatarForGroup:garModel.gid success:^(NSDictionary * dic, resultObject *state) {
                
            } fail:^(NSError * _Nonnull error) {
                
            }];
            
#ifdef ALIYM_AVALABLE
            // 创建会话
            YWTribe *tribe = [YWTribe new];
            tribe.tribeId = garModel.gid;
            YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            
            // 发送建群消息
            NSString *text = [NSString stringWithFormat:@"%@邀请%@加入群组",[UserInstance ShardInstnce].nickName,dic[@"data"][@"nick_str"]];
            NSDictionary *extDic = @{MSG_TYPE:CREATE_GROUP};
            
            YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
            [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                
            } completion:^(NSError *error, NSString *messageID) {
                @strongify(self);
                if (![ChatHelper shareHelper].transVC) {
                    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = garModel.group_name;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
                }
            }];
#elif defined EMIM_AVALABLE
            // 创建会话
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:garModel.gid type:EMConversationTypeGroupChat createIfNotExist:YES];
            
            // 发送建群消息
            NSString *text = [NSString stringWithFormat:@"%@邀请%@加入群组",[UserInstance ShardInstnce].nickName,dic[@"data"][@"nick_str"]];
            EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
            NSDictionary *extDic = @{MSG_TYPE:CREATE_GROUP};
            // 生成message
            EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:[UserInstance ShardInstnce].uid to:conversation.conversationId body:body ext:extDic];
            message.chatType = (EMChatType)conversation.type;// 设置消息类型
            //发送消息
            [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                @strongify(self);
                if (![ChatHelper shareHelper].transVC) {
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
                    chatVC.title = garModel.group_name;
                    chatVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatVC animated:YES];
                }
            }];
#else
            XMPPJID *roomID = roomJID(garModel.gid);
            [self getSuccessInforWithJID:roomID];
#endif
            
            
            if ([ChatHelper shareHelper].transVC) {
                //生成Message
#ifdef ALIYM_AVALABLE
                id <IYWMessage> ywMessage = [ChatHelper shareHelper].transVC.message;
                [conversation asyncForwardMessage:ywMessage progress:^(CGFloat progress, NSString *messageID) {
                    
                } completion:^(NSError *error, NSString *messageID) {
                    if (!error) {
                        [XSTool showToastWithView:self.view Text:@"发送成功"];
                    }else{
                        [XSTool showToastWithView:self.view Text:error.description];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
                        [self.navigationController popToViewController:self.navigationController.viewControllers[index - 3] animated:YES];
                    });
                }];
                
#elif defined EMIM_AVALABLE
                NSString *from = [[EMClient sharedClient] currentUsername];
                EMMessage *emMessgae = [ChatHelper shareHelper].transVC.message;
                EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:from to:conversation.conversationId body:emMessgae.body ext:emMessgae.ext];
                message.chatType = (EMChatType)conversation.type;
                [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                    
                } completion:^(EMMessage *message, EMError *error) {
                    if (!error) {
                        [XSTool showToastWithView:self.view Text:@"发送成功"];
                    }else{
                        [XSTool showToastWithView:self.view Text:error.description];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
                        [self.navigationController popToViewController:self.navigationController.viewControllers[index - 3] animated:YES];
                    });
                }];
#else
             [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
                
                return;
            }
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
    }];
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    
}

- (void) getSuccessInforWithJID:(XMPPJID *)jid {
    
    
    self.roomManager =  [[XMPPRoomManager alloc] initCreatRoomWith:jid];
    self.roomManager.menberString = self.groupOptions.id_str;
    @weakify(self);
    [self.roomManager setCreatRommSuccess:^{
        @strongify(self);
        [self didJoinedRoom:self.roomManager];
        [[XMPPManager sharedManager].roomList addObject:self.roomManager];
        
    }];
    
}

- (void) didJoinedRoom:(XMPPRoomManager *)roomManager {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        ConversationModel *conversation = [[ConversationModel alloc] initWithConversation:roomManager];
        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
        message.type = @"txt";
        message.body = [NSString stringWithFormat:@"%@邀请%@加入群聊",[UserInstance ShardInstnce].nickName,_nick_str];
        message.ext = @{MSG_TYPE:GROUP_INVITE_ACTION}.mj_JSONString;
        
        XMGroupChatController *vc = [[XMGroupChatController alloc] initWithRoomJID:conversation];
        vc.title = self.groupTitleTF.text;
        [self.navigationController pushViewController:vc animated:YES];
        NSString *messageString = message.mj_JSONString;
        [roomManager.xmppRoom sendMessageWithBody:messageString];
        
    });
}
@end

