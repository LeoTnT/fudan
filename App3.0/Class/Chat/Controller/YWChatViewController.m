//
//  YWChatViewController.m
//  App3.0
//
//  Created by mac on 2018/1/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "YWChatViewController.h"
#import "ChatKeyBoard.h"
#import "FaceSourceManager.h"
#import "FaceSourceManager.h"
#import <AVFoundation/AVFoundation.h>
//#import "MapLocationViewController.h"
#import "PersonalViewController.h"
#import "GroupInfoViewController.h"
#import "ChatHelper.h"
#import "TranspondListViewController.h"
#import "LoginModel.h"
#import "XSFormatterDate.h"
#import "GoodsTypeViewController.h"
#import "ChatViewController.h"
#import "UUImageAvatarBrowser.h"
#import "GroupSetTitleViewController.h"

// 红包
#import "OpenRedPacketView.h"
#import "RedPacketSendViewController.h"
#import "RedPacketDetailViewController.h"

// 转账
#import "ChatTransferViewController.h"
#import "ChatTransferDetailViewController.h"

#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "PersonViewController.h"
#import "S_CashierController.h"

// 气泡
#import "UUMessageCell.h"
#import "UUMessageFrame.h"
#import "UUMessage.h"
#import "ChatModel.h"
// 录音
#import "Mp3Recorder.h"
#import "UUProgressHUD.h"

// 图片查看
#import "XLPhotoBrowser.h"
#import "S_BaiduMap.h"
#import "AESCrypt.h"

#import "InviteDetailViewController.h"
#import "ADDetailWebViewController.h"

// 系统消息跳转
#import "WalletAccountViewController.h"
#import "OrderDetailViewController.h"

#define kLines 20
#define kRecordAudioFile @"myRecord.m4a"

@interface YWChatViewController ()
#ifdef ALIYM_AVALABLE
<ChatKeyBoardDelegate,ChatKeyBoardDataSource,UUMessageCellDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVAudioRecorderDelegate,MapLocationViewDelegate, XLPhotoBrowserDelegate, XLPhotoBrowserDatasource, UIDocumentPickerDelegate, UIDocumentInteractionControllerDelegate>
{
    //    EMConversation *_conversation;
    NSString *_chatterName;
    NSString *_mineAvatar;
    YWConversationType _typeChat;
    NSMutableArray *_chatArray;
    NSInteger _addHistroyNumber; //下拉加载更多数据量
    // 录音
    BOOL isbeginVoiceRecord;
    Mp3Recorder *MP3;
    NSInteger playTime;
    NSTimer *playTimer;
    NSTimer *recordLevelTimer;
    
    // 长按事件
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    UIMenuItem *_transpondMenuItem;
    UIMenuItem *_recallMenuItem;
    UIMenuItem *_detectorMenuItem;
    __block XLPhotoBrowser *_photoBrowser;
    BOOL canScrollBottom;
}
@property (nonatomic, strong) ChatKeyBoard *chatKeyBoard;
@property (nonatomic, strong) ChatModel *chatModel;
@property (nonatomic, strong) AVAudioRecorder *audioRecorder;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;//音频播放器，用于播放录音文件
@property (nonatomic, strong) UIMenuController *menuController;
@property (nonatomic, strong) NSIndexPath *menuIndexPath; //选中消息菜单索引
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) OpenRedPacketView *openRPView;
@property (nonatomic, strong) NSTimer *recordingTimer;
@property (nonatomic, strong) UIDocumentInteractionController *fileInteractionController; // 文件预览实例
@property (nonatomic, strong) NSArray *moreItems;
#endif
@end

@implementation YWChatViewController
#ifdef ALIYM_AVALABLE
@synthesize conversation = _conversation;
@synthesize title = _title;;
@synthesize avatarUrl = _avatarUrl;

/*
 注意：阿里云旺_conversation.conversationId != uid；_chatterName == uid
 */
- (id)initWithConversation:(YWConversation *)conversation
{
    self = [super init];
    if (self) {
        _conversation = conversation;
        if (_conversation.conversationType == YWConversationTypeP2P) {
            _chatterName = ((YWP2PConversation *)_conversation).person.personId;
        } else {
            _chatterName = ((YWTribeConversation *)_conversation).tribe.tribeId;
        }
        _typeChat = conversation.conversationType;
    }
    return self;
}

- (NSMutableArray *)photoArray {
    if (!_photoArray) {
        _photoArray = [NSMutableArray array];
    }
    return _photoArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isViewDidAppear = YES;
    
    // 在此刷新消息会造成发送图片和定位消息不刷新
    //    [self refreshMessages];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.isViewDidAppear = NO;
    
    // 标为已读
    [_conversation markConversationAsRead];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = _title;
    __weak typeof(self) wSelf= self;
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kChatToolBarHeight, 0));
    }];
    self.showRefreshHeader = YES;
    [self setRefreshHeader:^{
        [wSelf addHistroyMessages];
    }];
    
    self.chatKeyBoard.associateTableView = self.tableView;
    [self.view addSubview:self.chatKeyBoard];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.view addGestureRecognizer:tap];
    self.navigationController.interactivePopGestureRecognizer.delaysTouchesBegan=NO;
    
    if (_avatarUrl == nil) {
        _avatarUrl = @"";
    }
    
    _chatArray = [NSMutableArray array];
    _addHistroyNumber = 0;
    canScrollBottom = YES;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:USERINFO_LOGIN];
    LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    _mineAvatar = userInfo.logo;
    
    //注册消息回调
    [self loadBaseViewsAndData];
    
    //    [self setAudioSession];
//    [ChatHelper shareHelper].chatVC = self;
    [SPKitExample sharedInstance].chatVC = self;
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        for (UIViewController *controller in wSelf.navigationController.viewControllers) {
            if ([controller isKindOfClass:[GroupSetTitleViewController class]]) {
                [wSelf.navigationController popToRootViewControllerAnimated:YES];
                [ChatHelper shareHelper].chatVC = nil;
                return;
            }
        }
        [wSelf.navigationController popViewControllerAnimated:YES];
        [SPKitExample sharedInstance].chatVC = nil;
    }];
    if (![_chatterName isEqualToString:@"robot"]) {
        // 不是机器人
        if (_typeChat == YWConversationTypeTribe) {
            [self actionCustomRightBtnWithNrlImage:@"chat_group" htlImage:nil title:nil action:^{
                GroupInfoViewController *groupVC = [[GroupInfoViewController alloc] initWithGroupId:_chatterName];
                [wSelf.navigationController pushViewController:groupVC animated:YES];
            }];
        } else {
            [self actionCustomRightBtnWithNrlImage:@"chat_single" htlImage:nil title:nil action:^{
                PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:_chatterName];
                [wSelf.navigationController pushViewController:perVC animated:YES];
            }];
        }
    } else {
        self.chatKeyBoard.hidden = YES;
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMessages) name:@"refreshChatMessages" object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (!isEmptyString(self.productMessageId)) {
        [_conversation removeMessageWithMessageId:self.productMessageId];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    NSLog(@" -  - - - - - - -- - ");
    // Dispose of any resources that can be recreated.
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.navigationItem.title = _title;
}

#pragma mark - private
- (NSString *)detectorImage:(UIImage *)image
{
    //1.初始化扫描仪，设置设别类型和识别质量
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy:CIDetectorAccuracyHigh}];
    //2.扫描获取的特征组
    NSArray*features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    if (features.count > 0) {
        CIQRCodeFeature *feature = [features objectAtIndex:0];
        NSString *scannedResult = feature.messageString;
        NSLog(@"扫描结果------%@",scannedResult);
        //        [XSTool showToastWithView:self.view Text:scannedResult];
        return scannedResult;
    }
    return nil;
}

- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(MessageType)messageType
{
    if (self.menuController == nil) {
        self.menuController = [UIMenuController sharedMenuController];
    }
    
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(deleteMenuAction:)];
    }
    
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyMenuAction:)];
    }
    
    if (_transpondMenuItem == nil) {
        _transpondMenuItem = [[UIMenuItem alloc] initWithTitle:Localized(@"qr_send") action:@selector(transpondMenuAction:)];
    }
    
    if (_recallMenuItem == nil) {
        _recallMenuItem = [[UIMenuItem alloc] initWithTitle:@"撤回" action:@selector(recallMenuAction:)];
    }
    
    if (_detectorMenuItem == nil) {
        _detectorMenuItem = [[UIMenuItem alloc] initWithTitle:@"识别二维码" action:@selector(detectorMenuAction:)];
    }
    
    UUMessageFrame *mf = [self.chatModel.dataSource objectAtIndex:indexPath.row];
    id <IYWMessage> message = mf.message.emMessage;
    NSMutableArray *items;
    if (messageType == UUMessageTypeText) {
        if (message.outgoing) {
            items = [@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem, _recallMenuItem] mutableCopy];
        } else {
            items = [@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem] mutableCopy];
        }
        
    } else if (messageType == UUMessageTypePicture){
        NSString *qrUrl = [self detectorImage:mf.message.picture];
        if (qrUrl) {
            mf.message.qrUrl = qrUrl;
            items = [@[_deleteMenuItem,_transpondMenuItem, _detectorMenuItem] mutableCopy];
        } else {
            items = [@[_deleteMenuItem,_transpondMenuItem] mutableCopy];
        }
        if (message.outgoing) {
            [items addObject:_recallMenuItem];
        }
        
    } else if (messageType == UUMessageTypeRedPacket) {
        items = [@[_deleteMenuItem] mutableCopy];
    } else if (messageType == UUMessageTypeVoice) {
        items = [@[_deleteMenuItem] mutableCopy];
        if (message.outgoing) {
            [items addObject:_recallMenuItem];
        }
    } else {
        items = [@[_deleteMenuItem, _transpondMenuItem] mutableCopy];
    }
    [self.menuController setMenuItems:items];
    [self.menuController setTargetRect:showInView.frame inView:showInView.superview];
    [self.menuController setMenuVisible:YES animated:YES];
}

- (void)loadBaseViewsAndData
{
    self.chatModel = [[ChatModel alloc]init];
    self.chatModel.isGroupChat = NO;
    
    // 开始会话加载数据
    [_chatArray removeAllObjects];
    __weak typeof(self) wSelf = self;
    [_conversation startConversationWithMessageCount:10 completion:^(BOOL existMore) {
        [_chatArray addObjectsFromArray:_conversation.fetchedObjects];
        [wSelf dealMessages:_conversation.fetchedObjects needScrollToBottom:YES];
        
        if (!isEmptyString(self.productJsonString)) {
            NSDictionary *extDic = @{MSG_TYPE:@"111",MESSAGE_SHARE_DATA:self.productJsonString};
            NSDictionary *controlParameters = @{kYWMsgCtrlKeyClientLocal:@{kYWMsgCtrlKeyClientLocalKeyOnlySave:@(YES)}}; /// 控制字段
            YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:nil];
            
            __block BOOL hasAdd = NO;
            [_conversation asyncSendMessageBody:body controlParameters:controlParameters progress:^(CGFloat progress, NSString *messageID) {
                if (!hasAdd) {
                    id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
                    // 更新消息列表
                    NSDictionary *dic = @{@"emMessage":messageRef,
                                          @"messageId":messageID,
                                          @"shareJosnString":self.productJsonString,
                                          @"type": @(UUMessageTypeProduct),
                                          @"from": @(UUMessageFromMe),
                                          @"strIcon":_mineAvatar,
                                          @"chatType":@(_typeChat)};
                    [self dealTheFunctionData:dic];
                    hasAdd = YES;
                }
            } completion:^(NSError *error, NSString *messageID) {
                id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
                [_chatArray addObject:messageRef];
                
                self.productMessageId = messageID;
                
            }];
        }
    }];
    
    
}

// 刷新消息
- (void)refreshMessages
{

    [_chatArray removeAllObjects];
    __weak typeof(self) wSelf = self;
    
    [_conversation loadMoreMessages:10 completion:^(BOOL existMore) {
        [_chatArray addObjectsFromArray:_conversation.fetchedObjects];
        [self.chatModel.dataSource removeAllObjects];
        [wSelf dealMessages:_conversation.fetchedObjects needScrollToBottom:YES];
    }];
    
}

// 下拉加载历史消息
- (void)addHistroyMessages
{
    if (_chatArray.count == 0) {
        [self.tableView.mj_header endRefreshing];
        return;
    }

    __weak typeof(self) wSelf = self;
    [_conversation loadMoreMessages:10 completion:^(BOOL existMore) {
        _addHistroyNumber = _conversation.countOfFetchedObjects - _chatArray.count;
        [_chatArray removeAllObjects];
        [self.chatModel.dataSource removeAllObjects];
        [_chatArray addObjectsFromArray:_conversation.fetchedObjects];
        [wSelf dealMessages:_conversation.fetchedObjects needScrollToBottom:NO];
        [wSelf.tableView.mj_header endRefreshing];
    }];
    
}

- (void)tapAction
{
    [self.chatKeyBoard keyboardDown];
}
// 添加并滑动到新cell
- (void)insertAndScrollToBottom
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_chatArray.count-1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}

- (void)tableViewScrollToBottom
{
    if (self.chatModel.dataSource.count==0)
        return;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatModel.dataSource.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (void)tableViewScrollToLatestRefresh
{
    if (self.chatModel.dataSource.count==0)
        return;
    NSLog(@"_addHistroyNumber==%d",(int)_addHistroyNumber);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_addHistroyNumber inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

// 发送消息显示处理
- (void)dealTheFunctionData:(NSDictionary *)dic
{
    [self.chatModel addMessageItems:dic];
    [self.tableView reloadData];
    [self tableViewScrollToBottom];
    
}

- (UIViewController * )getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

#pragma mark - UUMessage delegate
- (void)productShareClick:(NSString *)productId {
    GoodsDetailViewController *vc = [GoodsDetailViewController new];
    vc.goodsID = productId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendProductLinkClick:(NSString *)jsonString {
    NSDictionary *extDic = @{MSG_TYPE:MESSAGE_PRODUCT_TYPE,MESSAGE_SHARE_DATA:jsonString};
    UUMessageProductShareModel *model = [UUMessageProductShareModel mj_objectWithKeyValues:jsonString];
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:model.goodTitle];
    
    __block BOOL hasAdd = NO;
    [_conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        if (!hasAdd) {
            id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
            // 更新消息列表
            NSDictionary *dic = @{@"emMessage":messageRef,
                                  @"messageId":messageID,
                                  @"shareJosnString":jsonString,
                                  @"type": @(UUMessageTypeProductShare),
                                  @"from": @(UUMessageFromMe),
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            hasAdd = YES;
        }
    } completion:^(NSError *error, NSString *messageID) {
        id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
        [_chatArray addObject:messageRef];
        
        if (!error) {
            NSLog(@"文字消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"文字消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
        
    }];
}

- (void)systemNoticeContentDidClick:(UUMessageSystemNoticeModel *)sysNoticeModel {
    NSString *link_url = sysNoticeModel.link_url;
    if (![link_url containsString:ImageBaseUrl]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link_url]];
        return;
    }
    NSString *paramStr = [link_url componentsSeparatedByString:@"?"][1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"wallet"]) {
        WalletAccountViewController *vc=[[WalletAccountViewController alloc] init];
        vc.walletType = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"order"]) {
        OrderDetailViewController *vc=[[OrderDetailViewController alloc] init];
        vc.orderId = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"chat"]) {
        //        EMConversation *con = [[EMClient sharedClient].chatManager getConversation:qrid type:EMConversationTypeChat createIfNotExist:YES];
        //        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:con];
        //        chatVC.title = @"客服";
        //        [self.navigationController pushViewController:chatVC animated:YES];
        // 客服
        [self.navigationController pushViewController:[self getNameWithClass:@"CustomerViewController"] animated:YES];
    } else {
        ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@&device=%@",sysNoticeModel.link_url,gui];
        adWeb.title = sysNoticeModel.link_title;
        [self.navigationController pushViewController:adWeb animated:YES];
    }
    
    
}

- (void)noticeContentDidClick:(UUMessageCell *)cell {

    // 邀请群成员群主确认
    InviteDetailViewController *vc = [[InviteDetailViewController alloc] init];
    vc.invite_id = cell.messageFrame.message.ext[GROUP_INVITE_ID];
    vc.message = cell.messageFrame.message.emMessage;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendFailTap:(UUMessageCell *)cell {
    @weakify(self);
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"重新发送此消息" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self.conversation asyncResendMessage:cell.messageFrame.message.emMessage progress:^(CGFloat progress, NSString *messageID) {
            
        } completion:^(NSError *error, NSString *messageID) {
            id<IYWMessage> message  = [self.conversation fetchMessageWithMessageId:messageID];
            [_chatArray addObject:message];
            if (!error) {
                NSLog(@"重新发送消息成功");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([message.messageId isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusSuccessed;
                    }
                }
            } else {
                NSLog(@"重新发送消息失败");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([message.messageId isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusFailed;
                    }
                }
            }
            [self.tableView reloadData];
        }];

    }];
    UIAlertAction *canAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertVC addAction:canAct];
    [alertVC addAction:conAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)cellContentCopy:(UUMessageCell *)cell
{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = cell.messageFrame.message.strContent;
}

- (void)cellContentDelete:(UUMessageCell *)cell
{
    NSLog(@"删除");
    id<IYWMessage> message = cell.messageFrame.message.emMessage;
    [_chatArray removeObject:message];
    [self.chatModel.dataSource removeObject:cell.messageFrame];
    [_conversation removeMessageWithMessageId:message.messageId];
    [self.tableView reloadData];
}

- (void)cellContentTranspond:(UUMessageCell *)cell
{
    NSLog(@"转发");
    id<IYWMessage> message = cell.messageFrame.message.emMessage;
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:message];
    [self.navigationController pushViewController:trVC animated:YES];
}

- (void)cellContentRecall:(UUMessageCell *)cell
{
    NSLog(@"撤回");
    id<IYWMessage> seleteMessage = cell.messageFrame.message.emMessage;
    // 判断时间是否在2分钟之内
    NSString *startStr = [NSString stringWithFormat:@"%lld",[[[XSTool shareYMDHMS] stringFromDate:seleteMessage.time] longLongValue]];
    NSString *subStart = [XSFormatterDate timeWithTimeIntervalString:startStr];
    NSDate *startDate = [NSDate dateFromString:subStart withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *subEnd = [XSFormatterDate currentTime];
    NSDate *endDate = [NSDate dateFromString:subEnd withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //这个是相隔的秒数
    NSTimeInterval timeInterval = [startDate timeIntervalSinceDate:endDate];
    NSLog(@"%f",timeInterval);
    //相距5分钟显示时间Label
    if (fabs (timeInterval) > 2*60) {
        [XSTool showToastWithView:self.view Text:@"无法撤回超过两分钟的消息"];
        return;
    }
    
    // TODO：需要刷新DB消息
    [self.conversation setDidChangeContentBlock:^{
        
    }];
    
    // 发送透传消息
    NSDictionary *ext = @{MSG_TYPE:REVOKE_ACTION};
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:ext.mj_JSONString summary:REVOKE_ACTION isTransparent:YES];
    [self.conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        
    } completion:^(NSError *error, NSString *messageID) {
        
    }];
}

- (void)cellContentDetector:(UUMessageCell *)cell
{
    NSLog(@"识别图中二维码");
    [self cellContentUrlDidClick:cell.messageFrame.message.qrUrl];
}

- (void)shareContentClick:(NSString *)link {
    [self cellContentUrlDidClick:link];
}

- (void)cellContentUrlDidClick:(NSString *)url {
    NSArray *paramArr = [url componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![url containsString:ImageBaseUrl] || arr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }

    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"supply_pay"]){
        S_CashierController *cashier =[S_CashierController new];
        cashier.showTitle = qrid;
        cashier.supply_name = qrid;
        [self.navigationController pushViewController:cashier animated:YES];
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}

- (void)videoDidClick:(UUMessageCell *)cell {
    EMMessage *seleteMessage = cell.messageFrame.message.emMessage;
    EMVideoMessageBody *fileMesBody = (EMVideoMessageBody *)seleteMessage.body;
    if (fileMesBody.downloadStatus == EMDownloadStatusSuccessed) {
        NSLog(@"local path is exsit");
        NSURL *fileUrl = [NSURL fileURLWithPath:fileMesBody.localPath];
        if (fileUrl != nil) {
            if (_fileInteractionController == nil) {
                //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                _fileInteractionController.delegate = self;
            } else {
                _fileInteractionController.URL = fileUrl;
            }
            [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"文件打开失败"];
        }
        return;
    } else {
        [XSTool showProgressHUDTOView:self.view withText:@"正在下载"];
        [[EMClient sharedClient].chatManager downloadMessageAttachment:cell.messageFrame.message.emMessage progress:^(int progress) {
            
        } completion:^(EMMessage *message, EMError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            if (!error) {
                [XSTool showToastWithView:self.view Text:@"下载完成"];
                // 下载完成
                cell.messageFrame.message.emMessage = message;
                NSIndexPath *indePath= [self.tableView indexPathForCell:cell];
                [self.tableView reloadRowsAtIndexPaths:@[indePath] withRowAnimation:UITableViewRowAnimationNone];
                
                // 打开文件
                EMFileMessageBody *fileBody = (EMFileMessageBody *)message.body;
                NSURL *fileUrl = [NSURL fileURLWithPath:fileBody.localPath];
                if (fileUrl != nil) {
                    if (_fileInteractionController == nil) {
                        //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                        _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                        _fileInteractionController.delegate = self;
                    } else {
                        _fileInteractionController.URL = fileUrl;
                    }
                    [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:@"文件打开失败"];
                }
            }
        }];
    }
}

- (void)voiceDidClick:(UUMessageCell *)cell
{
    id<IYWMessage> message = cell.messageFrame.message.emMessage;
    if (message.conversationType == YWConversationTypeP2P && !message.hasReaded && !message.outgoing) {
        [self.conversation markMessageAsReadWithMessageIds:@[message.messageId]];
    }
}

- (void)locationDidClickLatitude:(double)latitude longitude:(double)longitue adddress:(NSString *)adddress
{
    
    S_BaiduMap *locationController = [S_BaiduMap new];
    locationController.showType = BMKShowMap;
    locationController.delegate = self;
    CLLocationCoordinate2D cc = {latitude,longitue} ;
    locationController.locationCoordinate = cc;
    locationController.pathShowAddress = adddress;
    [self.navigationController pushViewController:locationController animated:YES];
}

- (void)redPacketClick:(UUMessageCell *)cell {
    // 服务器拉取红包数据
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            
            if (_typeChat == EMConversationTypeChat) {
                if (model.is_can_rec) {
                    // 我可以领（收红包）
                    //                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversation;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            } else {
                if (model.is_can_rec || (!model.is_can_rec && !model.is_received)) {
                    // 我可以领（群红包）或者 我已经不可领并且我还没领
                    //                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversation;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
    
}

- (void)transferClick:(UUMessageCell *)cell {
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getTransferInfoWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            TransferDetailModel *model = [TransferDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转转账详情
            ChatTransferDetailViewController *trans = [[ChatTransferDetailViewController alloc] init];
            trans.model = model;
            trans.conversation = self.conversation;
            trans.transferId = rpId;
            trans.resendMessage = cell.messageFrame.message.emMessage;
            [self.navigationController pushViewController:trans animated:YES];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)openRedPacketWithId:(NSString *)rpId {
    [XSTool showProgressHUDWithView:self.view];
    if (_typeChat == EMConversationTypeChat) {
        [HTTPManager getRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId};
                        
                        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                        @weakify(self);
                        //发送消息
                        [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
                            @strongify(self);
                            [self refreshMessages];
                        }];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversation;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        [HTTPManager getGroupRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 刷新用户钱包
                [HTTPManager updateWalletWithSuccess:^(NSDictionary *dic, resultObject *state) {
                    
                } fail:^(NSError *error) {
                    
                }];
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId,MESSAGE_PACK_FROM:model.uid};
                        
                        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                        @weakify(self);
                        //发送消息
                        [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
                            @strongify(self);
                            [self refreshMessages];
                        }];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversation;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }
    
}

- (void)detailRedPacketWithId:(NSString *)rpId {
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.openRPView dismiss];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转红包详情
            RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
            rpd.model = model;
            rpd.conversation = self.conversation;
            [self.navigationController pushViewController:rpd animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [self.openRPView dismiss];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)cellContentDidClick:(UUMessageCell *)cell imageView:(UIImageView *)contentImageView
{
    NSArray* sorted = [self.photoArray sortedArrayUsingComparator:
                       ^(PhotoBrowserImage *obj1, PhotoBrowserImage* obj2){
                           if(obj1.imageTime < obj2.imageTime) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    [self.photoArray removeAllObjects];
    [self.photoArray addObjectsFromArray:sorted];
    NSInteger index = 0;
    for (PhotoBrowserImage *image1 in self.photoArray) {
        if ([image1.imageId isEqualToString:cell.messageFrame.message.messageId]) {
            index = [self.photoArray indexOfObject:image1];
            break;
        }
    }
    _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
    if (!_photoBrowser) {
        [XSTool showToastWithView:self.view Text:@"图片获取失败"];
        return;
    }
    _photoBrowser.delegate = self;
    [_photoBrowser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    _photoBrowser.isCusctomAdd = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.tabBarController.view addSubview:_photoBrowser];
    
    id<IYWMessage> mes = cell.messageFrame.message.emMessage;
    YWMessageBodyImage *imageMesBody = (YWMessageBodyImage *)mes.messageBody;
    if (imageMesBody.cachedOriginal) {
        NSLog(@"local path is exsit");
        return;
    }
    if (cell.messageFrame.message.from == UUMessageFromMe) {
        return;
    }
    [XSTool showProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
    [imageMesBody asyncGetOriginalImageWithProgress:^(CGFloat progress) {
        
    } completion:^(NSData *imageData, NSError *aError) {
        if (!aError) {
            // 解密
            UIImage *image = [UIImage imageWithData:imageData];
            [contentImageView setImage:image];
            cell.messageFrame.message.picture = image;
            
            // 解密
            PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
            pbImage.imageId = mes.messageId;
            pbImage.imageTime = [[[XSTool shareYMDHMS] stringFromDate:mes.time] longLongValue];
            if (pbImage) {
                [self.photoArray replaceObjectAtIndex:index withObject:pbImage];
            }
            
            [_photoBrowser dismiss];
            _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
            _photoBrowser.delegate = self;
            [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
        } else {
            [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
            [XSTool showToastWithView:self.view Text:@"原图下载失败"];
        }
    }];

}

- (void)callContentDidClick:(UUMessageCell *)cell {
    if (cell.messageFrame.message.isVideo) {
        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_VIDEO];
    } else {
        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_AUDIO];
    }
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

-(void)photoBrowserDismiss{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)photoBrowserScroll:(PhotoBrowserImage *)image {
    NSInteger index = 0;
    for (PhotoBrowserImage *image1 in self.photoArray) {
        if ([image1.imageId isEqualToString:image.imageId]) {
            index = [self.photoArray indexOfObject:image1];
            break;
        }
    }
    for (UUMessageFrame *mesF in self.chatModel.dataSource) {
        id<IYWMessage> mes = mesF.message.emMessage;
        if ([mes.messageId isEqualToString:image.imageId]) {
            YWMessageBodyImage *imageMesBody = (YWMessageBodyImage *)mes.messageBody;
            if (imageMesBody.cachedOriginal) {
                NSLog(@"local path is exsit");
                return;
            }
            if (mes.outgoing) {
                return;
            }
            [XSTool showProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
            [imageMesBody asyncGetOriginalImageWithProgress:^(CGFloat progress) {
                
            } completion:^(NSData *imageData, NSError *aError) {
                if (!aError) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
                    pbImage.imageId = mes.messageId;
                    pbImage.imageTime = [[[XSTool shareYMDHMS] stringFromDate:mes.time] longLongValue];
                    [self.photoArray replaceObjectAtIndex:index withObject:pbImage];
                    mesF.message.picture = image;
                    [self.tableView reloadData];
                    [_photoBrowser dismiss];
                    _photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
                    _photoBrowser.delegate = self;
                    [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
                } else {
                    [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
                    [XSTool showToastWithView:self.view Text:@"原图下载失败"];
                }
            }];

        }
    }
}

- (void)cellContentFileDidClick:(UUMessageCell *)cell {
    
}
#pragma mark - UIDocumentInteractionControllerDelegate
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller {
    return self.view.frame;
}

- (void)cellContentLongPress:(UUMessageCell *)cell
{
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    [cell.btnContent becomeFirstResponder];
    self.menuIndexPath = indexPath;
    [self showMenuViewController:cell.btnContent andIndexPath:indexPath messageType:cell.messageFrame.message.type];
}

- (void)headImageDidClick:(UUMessageCell *)cell
{
    if (cell.messageFrame.message.from == UUMessageFromMe) {
        return;
    }
    id<IYWMessage>message = cell.messageFrame.message.emMessage;
    YWPerson *person = message.messageFromPerson;
    PersonalViewController *personalVC = [[PersonalViewController alloc] initWithUid:person.personId];
    [self.navigationController pushViewController:personalVC animated:YES];
}

- (BOOL)shouldSendHasReadAckForMessage:(id <IYWMessage> )message
{
    NSString *account = [[EMClient sharedClient] currentUsername];
    if (message.conversationType != YWConversationTypeP2P || message.hasReaded || [account isEqualToString:message.messageFromPerson.personId] || ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) || !self.isViewDidAppear)
    {
        return NO;
    }
    return YES;
    //    EMMessageBody *body = message.body;
    //    if (((body.type == EMMessageBodyTypeVideo) ||
    //         (body.type == EMMessageBodyTypeVoice) ||
    //         (body.type == EMMessageBodyTypeImage)))
    //    {
    //        NSLog(@"read no 222222");
    //        return NO;
    //    }
    //    else
    //    {
    //        return YES;
    //    }
}

- (void)dealMessages:(NSArray *)aMessages needScrollToBottom:(BOOL)scroll
{
    NSInteger index = 0;
    
    for (id <IYWMessage> message in aMessages) {
        // 判断接收的消息状态
        MessageStatus status = UUMessageStatusSuccessed;
        if (message.messageSendStatus == YWMessageSendStatusFailed) {
            status = UUMessageStatusFailed;
        }
        NSString *avatarStr = @"";
        NSString *strName = @"";
        
        if (!message.outgoing) {
            if (_typeChat == YWConversationTypeP2P) {
                avatarStr = _avatarUrl; // 对方头像
                strName = message.messageFromPerson.personId;
            } else if (_typeChat == YWConversationTypeTribe) {
                strName = message.messageFromPerson.personId;
                NSMutableArray *mutArray = [[DBHandler sharedInstance] getContactByUid:message.messageFromPerson.personId];
                if (mutArray == nil || mutArray.count == 0) {
                    mutArray = [[DBHandler sharedInstance] getMemberByMemberId:message.messageFromPerson.personId];
                    if (mutArray == nil || mutArray.count == 0) {
                        [HTTPManager getUserInfoWithUid:message.messageFromPerson.personId success:^(NSDictionary * dic, resultObject *state) {
                            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
                            if (state.status) {
                                NickNameDataParser *model = [[NickNameDataParser alloc] init];
                                model.uid = message.messageFromPerson.personId;
                                model.nickname = parser.data.nickname;
                                model.logo = parser.data.logo;
                                [[DBHandler sharedInstance] addOrUpdateGroupMember:model];
                            }
                        } fail:^(NSError * _Nonnull error) {
                            
                        }];
                    } else {
                        NickNameDataParser *model = mutArray[0];
                        avatarStr = model.logo;
                        strName = model.nickname;
                    }
                } else {
                    ContactDataParser *model = mutArray[0];
                    avatarStr = model.avatar;
                    strName = [model getName];
                }
                
            }
            
            if ([self shouldSendHasReadAckForMessage:message]) {
                [_conversation markMessageAsReadWithMessageIds:@[message.messageId]];
            }
        } else {
            avatarStr = _mineAvatar; // 我的头像
            if (message.receiverHasReaded) {
                status = UUMessageStatusRead;   // 标注为已读
            }
        }
        
        YWMessageBody *msgBody = message.messageBody;
        __block NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        NSString *from = message.outgoing?@"0":@"1";
        if ([msgBody isKindOfClass:[YWMessageBodyText class]]) {
            // 收到的文字消息
            YWMessageBodyText *textBody = (YWMessageBodyText *)msgBody;
            NSString *txt = [AESCrypt decrypt:textBody.messageText password:[NSString stringWithFormat:@"%@%@",AES_KEY,message.messageToPerson.personId]];
            if (isEmptyString(txt)) {
                txt = textBody.messageText;
            }
            NSLog(@"收到的文字是 txt -- %@",txt);
//            NSString *strTime = [NSString stringWithFormat:@"%ld", [message.time timeIntervalSince1970]];
            
            NSLog(@"%@ ===== %@",message.messageFromPerson.personId,[UserInstance ShardInstnce].uid);
            dic = [@{@"emMessage":message,
                     @"messageId":message.messageId,
                     @"strContent": txt,
                     @"type": @(UUMessageTypeText),
                     @"from": from,
                     @"strName":strName,
                     @"strTime":message.time,
                     @"status":@(status),
                     @"strIcon":avatarStr,
                     @"chatType":@(_typeChat)} mutableCopy];
        } else if ([msgBody isKindOfClass:[YWMessageBodyImage class]]) {
            // 收到的图片消息
            YWMessageBodyImage *imageBody = (YWMessageBodyImage *)msgBody;
            UIImage *defaultImage = [UIImage imageNamed:@"chat_load_pic"];
            dic = [@{@"emMessage":message,
                     @"messageId":message.messageId,
                     @"picture": defaultImage,
                     @"type": @(UUMessageTypePicture),
                     @"from": from,
                     @"strName":strName,
                     @"strTime":message.time,
                     @"strIcon":avatarStr,
                     @"status":@(status),
                     @"chatType":@(_typeChat)} mutableCopy];
            
            [imageBody asyncGetBigImageWithProgress:nil completion:^(NSData *imageData, NSError *aError) {
                // 添加图片到数据源
                PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
                pbImage.imageId = message.messageId;
                pbImage.imageTime = [[[XSTool shareYMDHMS] stringFromDate:message.time] longLongValue];
                if (pbImage) {
                    [self.photoArray addObject:pbImage];
                }
                UIImage *image = [UIImage imageWithData:imageData];
                if (image) {
                    [dic setObject:image forKey:@"picture"];
                    [self.chatModel refreshImageMessageItems:dic];
                }
                
                [self.tableView reloadData];
            }];
        } else if ([msgBody isKindOfClass:[YWMessageBodyLocation class]]) {
            YWMessageBodyLocation *locaBody = (YWMessageBodyLocation *)msgBody;
            NSString *strLat = [NSString stringWithFormat:@"%f",locaBody.location.latitude];
            NSString *strLon = [NSString stringWithFormat:@"%f",locaBody.location.longitude];
            dic = [@{@"emMessage":message,
                     @"messageId":message.messageId,
                     @"strLocation": locaBody.locationName,
                     @"strLatitude": strLat,
                     @"strLongitude": strLon,
                     @"type": @(UUMessageTypeLocation),
                     @"from": from,
                     @"strName":strName,
                     @"strTime":message.time,
                     @"status":@(status),
                     @"strIcon":avatarStr,
                     @"chatType":@(_typeChat)} mutableCopy];
        } else if ([msgBody isKindOfClass:[YWMessageBodyVoice class]]) {
            YWMessageBodyVoice *voiceBody = (YWMessageBodyVoice *)msgBody;
            NSString *voiceTime = [NSString stringWithFormat:@"%d",(int)voiceBody.messageVoiceDuration];
            dic = [@{@"emMessage":message,
                     @"messageId":message.messageId,
                     @"strVoiceTime": voiceTime,
                     @"type": @(UUMessageTypeVoice),
                     @"from": from,
                     @"strName":strName,
                     @"strTime":message.time,
                     @"status":@(status),
                     @"strIcon":avatarStr,
                     @"chatType":@(_typeChat)} mutableCopy];
            [voiceBody asyncGetVoiceDataWithProgress:nil completion:^(NSData *data, NSError *aError) {
                if (data) {
                    [dic setObject:data forKey:@"voice"];
                    [self.chatModel refreshVoiceMessageItems:dic];
                }
                [self.tableView reloadData];
            }];
        } else if ([msgBody isKindOfClass:[YWMessageBodyCustomize class]]) {
            YWMessageBodyCustomize *customBody = (YWMessageBodyCustomize *)msgBody;
            
            NSData *jsonData = [customBody.content dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
            NSDictionary *ext = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
            
            if ([ext[MSG_TYPE] isEqualToString:READ_PACKET]) {
                NSString *txt = customBody.summary;
                NSLog(@"收到红包id是-- %@",ext[CUSTOM_MSG_ID]);
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"strRedMessage": txt,
                         @"rpId": ext[CUSTOM_MSG_ID],
                         @"type": @(UUMessageTypeRedPacket),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            }  else if ([ext[MSG_TYPE] isEqualToString:TRANSFER_BANLANCE] || [ext[MSG_TYPE] isEqualToString:TRANSFER_MONEY_COLLECTION] || [ext[MSG_TYPE] isEqualToString:TRANSFER_MONEY_COLLECTION_REJECT]) {
                NSString *txt = customBody.summary;
                NSLog(@"收到转账id是-- %@",ext[CUSTOM_MSG_ID]);
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"strRedMessage": txt,
                         @"rpId": ext[CUSTOM_MSG_ID],
                         @"moneyStr":ext[TRANSFER_BANLANCE_TAG],
                         @"type": @(UUMessageTypeTransfer),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_SHARE_TYPE]) {
                NSString *shareData = ext[MESSAGE_SHARE_DATA];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeShare),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"shareJosnString":shareData,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_PRODUCT_TYPE]) {
                NSString *shareData = ext[MESSAGE_SHARE_DATA];
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeProductShare),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"shareJosnString":shareData,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_SYSTEM_PUSH]) {
                NSString *shareData = customBody.summary;
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeSystemPush),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"shareJosnString":shareData,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_SHARE_USER]) {
                NSString *text = customBody.summary;
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"cardId":ext[MESSAGE_SHARE_USER_ID],
                         @"cardIcon":ext[MESSAGE_SHARE_USER_ICO],
                         @"cardName":text,
                         @"type": @(UUMessageTypeCard),
                         @"from": from,
                         @"strName":strName,
                         @"strTime":message.time,
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VOICE_CALL]) {
                NSString *text = customBody.summary;
                if (!message.outgoing && [text isEqualToString:NSLocalizedString(@"call.offline", @"")]) {
                    text = NSLocalizedString(@"call.message.cancel", @"");
                }
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeCall),
                         @"from": from,
                         @"strName":strName,
                         @"isVideo":@(0),
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_ATTR_IS_VIDEO_CALL]) {
                NSString *text = customBody.summary;
                if (!message.outgoing && [text isEqualToString:NSLocalizedString(@"call.offline", @"")]) {
                    text = NSLocalizedString(@"call.message.cancel", @"");
                }
                dic = [@{@"emMessage":message,
                         @"messageId":message.messageId,
                         @"type": @(UUMessageTypeCall),
                         @"from": from,
                         @"strName":strName,
                         @"isVideo":@(1),
                         @"strContent": text,
                         @"status":@(status),
                         @"strIcon":avatarStr,
                         @"extType":ext[MSG_TYPE],
                         @"ext":ext,
                         @"chatType":@(_typeChat)} mutableCopy];
            } else {
                if (isEmptyString([[ChatHelper shareHelper] noticeStringConvertByMessage:message])) {
                    continue;
                }
                [dic setObject:message.time forKey:@"strTime"];
                [dic setObject:[[ChatHelper shareHelper] noticeStringConvertByMessage:message] forKey:@"strNotice"];
            }
        } else {
            continue;
        }
        
        NSLog(@"%@",dic);
        // 插入数据位置+1
        
        if (scroll) {
            // 增加最新消息
            [self.chatModel addMessageItems:dic];
        } else {
            // 插入历史消息
            [self.chatModel insertMessageItems:dic index:index];
        }
        index++;
        
    }
    [self.tableView reloadData];
    if (canScrollBottom) {
        // 跳转到最底部
        [self tableViewScrollToBottom];
    } else if (!scroll) {
        // 跳转到刷新的历史消息
        [self tableViewScrollToLatestRefresh];
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages
{
    NSMutableArray *mutArray = [NSMutableArray array];
    for (id<IYWMessage> mes in aMessages) {
        if ([mes.conversationId isEqualToString:_conversation.conversationId]) {
            [mutArray addObject:mes];
            
//            NSLog(@"%@",mes.ext);
//            // 收到红包消息向服务器确认(只确认一次，需要判断此条红包消息是否已经确认)
//            if (mes.ext[MSG_TYPE]) {
//                NSLog(@"chat view : msg_type:%@",mes.ext[MSG_TYPE]);
//                if ([mes.ext[MSG_TYPE] isEqualToString:READ_PACKET] && !mes.ext[READ_PACKET_HANDLE]) {
//                    NSLog(@"chat view : 收到红包id是-- %@",mes.ext[CUSTOM_MSG_ID]);
//                    NSString *rpId = mes.ext[CUSTOM_MSG_ID];
//                    [HTTPManager confirmSendRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
//                        if (state.status) {
//                            // 服务器已确认红包发到
//                            NSLog(@"chat view : 服务器已确认红包发到");
//                            mes.ext = @{MSG_TYPE:READ_PACKET,CUSTOM_MSG_ID:rpId,READ_PACKET_HANDLE:@(1)};
//
//                            // 更新本地消息，增加消息被处理字段（READ_PACKET_HANDLE）
//                            [[EMClient sharedClient].chatManager updateMessage:mes completion:^(EMMessage *aMessage, EMError *aError) {
//                                NSLog(@"更新本地消息完成，%@",aMessage.ext);
//                            }];
//                        } else{
//
//                        }
//                    } fail:^(NSError *error) {
//
//                    }];
//                }
//            }
        }
    }
    if (mutArray == nil || mutArray.count == 0) {
        return;
    }
    [self dealMessages:mutArray needScrollToBottom:YES];
    [_chatArray addObjectsFromArray:mutArray];
}

#pragma mark tableview delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    if (point.y+scrollView.bounds.size.height < scrollView.contentSize.height-200) {
        // 如果在浏览之前的聊天内容，收到新消息不让跳到最底部
        canScrollBottom = NO;
    } else {
        canScrollBottom = YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatModel.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.chatModel.dataSource[indexPath.row] cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSString *identifier = [NSString stringWithFormat:@"chat%ld",(long)indexPath.row];
    static NSString *identifier = @"chatCell";
    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
    }
    [cell setMessageFrame:self.chatModel.dataSource[indexPath.row]];
    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.chatKeyBoard keyboardDown];
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage]?:info[UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.8);
    UIImage *image = [UIImage imageWithData:imageData];
    // 构造图片消息
//    NSData *encryptData = [AESCrypt encryptData:imageData password:[NSString stringWithFormat:@"%@%@",AES_KEY,_conversation.conversationId]];
    
    YWMessageBodyImage *body = [[YWMessageBodyImage alloc] initWithMessageImageData:imageData isOriginal:YES];
    __block BOOL hasAdd = NO;
    [_conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        NSLog(@"!!!!!!!! %f",progress);
        if (!hasAdd) {
            id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
            NSDictionary *dic = @{@"emMessage":messageRef,
                                  @"messageId":messageID,
                                  @"picture": image,
                                  @"type": @(UUMessageTypePicture),
                                  @"from": @(UUMessageFromMe),
                                  @"strName":[UserInstance ShardInstnce].uid,
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            hasAdd = YES;
        }
        
    } completion:^(NSError *error, NSString *messageID) {
        id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
        [_chatArray addObject:messageRef];
        // 添加图片到数据源
        PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
        pbImage.imageId = messageID;
        pbImage.imageTime = [[[XSTool shareYMDHMS] stringFromDate:messageRef.time] longLongValue];
        [self.photoArray addObject:pbImage];

        if (!error) {
                NSLog(@"图片消息发送成功");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([messageID isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusSuccessed;
                    }
                }
                [self.tableView reloadData];
            } else {
                NSLog(@"图片消息发送失败");
                for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                    UUMessageFrame *frame = self.chatModel.dataSource[i];
                    if ([messageID isEqualToString:frame.message.messageId]) {
                        frame.message.status = UUMessageStatusFailed;
                    }
                }
                [self.tableView reloadData];
            }
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- ChatKeyBoardDataSource
- (NSArray<MoreItem *> *)chatKeyBoardMorePanelItems
{
    MoreItem *item1 = [MoreItem moreItemWithPicName:@"sharemore_photo" highLightPicName:nil itemName:Localized(@"拍照")];
    MoreItem *item2 = [MoreItem moreItemWithPicName:@"sharemore_pic" highLightPicName:nil itemName:@"图片"];
    MoreItem *item3 = [MoreItem moreItemWithPicName:@"sharemore_location" highLightPicName:nil itemName:Localized(@"位置")];
    MoreItem *item4 = [MoreItem moreItemWithPicName:@"sharemore_redpacket" highLightPicName:nil itemName:Localized(@"红包")];
    MoreItem *item5 = [MoreItem moreItemWithPicName:@"sharemore_transfer" highLightPicName:nil itemName:Localized(@"转账")];
    MoreItem *item6 = [MoreItem moreItemWithPicName:@"sharemore_file" highLightPicName:nil itemName:Localized(@"文件")];
    
    MoreItem *item7 = [MoreItem moreItemWithPicName:@"sharemore_voice" highLightPicName:nil itemName:@"语音"];
    MoreItem *item8 = [MoreItem moreItemWithPicName:@"sharemore_video" highLightPicName:nil itemName:@"视频"];
    
    if (_typeChat == EMConversationTypeChat) {
        self.moreItems = @[item1, item2, item3, item4, item5, item7, item8];
    } else {
        self.moreItems = @[item1, item2, item3, item4, item6];
    }
    return self.moreItems;
}
- (NSArray<ChatToolBarItem *> *)chatKeyBoardToolbarItems
{
    ChatToolBarItem *item1 = [ChatToolBarItem barItemWithKind:kBarItemFace normal:@"face" high:@"face_HL" select:@"keyboard"];
    ChatToolBarItem *item2 = [ChatToolBarItem barItemWithKind:kBarItemVoice normal:@"voice" high:@"voice_HL" select:@"keyboard"];
    ChatToolBarItem *item3 = [ChatToolBarItem barItemWithKind:kBarItemMore normal:@"more_ios" high:@"more_ios_HL" select:nil];
    ChatToolBarItem *item4 = [ChatToolBarItem barItemWithKind:kBarItemSwitchBar normal:@"switchDown" high:nil select:nil];
    return @[item1, item2, item3, item4];
}

- (NSArray<FaceThemeModel *> *)chatKeyBoardFacePanelSubjectItems
{
    return [FaceSourceManager loadFaceSource];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    BOOL fileUrlAuthozied = [url startAccessingSecurityScopedResource];
    if (fileUrlAuthozied) {
        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
        NSError *error;
        
        [fileCoordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL * _Nonnull newURL) {
            NSData *data = [NSData dataWithContentsOfURL:newURL];
            
            // 构造文件消息
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [url stopAccessingSecurityScopedResource];
    } else {
        // Error handling
    }
}

- (void)chatKeyBoard:(ChatKeyBoard *)chatKeyBoard didSelectMorePanelItemIndex:(NSInteger)index
{
    NSLog(@"Select %ld",(long)index);
    MoreItem *item = self.moreItems[index];
    if ([item.itemName isEqualToString:Localized(@"拍照")]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    } else if ([item.itemName isEqualToString:@"图片"]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    } else if ([item.itemName isEqualToString:Localized(@"位置")]) {
        S_BaiduMap *locationController = [S_BaiduMap new];
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        [self.navigationController pushViewController:locationController animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"红包")]) {
        RedPacketSendViewController *redPacketVC = [[RedPacketSendViewController alloc] init];
        redPacketVC.conversation = _conversation;
        [self.navigationController pushViewController:redPacketVC animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"转账")]) {
        ChatTransferViewController *transVC = [[ChatTransferViewController alloc] init];
        NickNameDataParser *parser = [NickNameDataParser new];
        parser.logo = self.avatarUrl;
        parser.nickname = self.title;
        parser.uid = _chatterName;
        
        transVC.userInfo = parser;
        transVC.conversation = _conversation;
        [self.navigationController pushViewController:transVC animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"文件")]) {
        NSArray *documentTypes = @[@"public.content",@"public.text",@"public.source-code",@"public.image",@"public.audiovisual-content",@"com.adobe.pdf",@"com.apple.keynote.key",@"com.microsoft.word.doc",@"com.microsoft.excel.xls",@"com.microsoft.powerpoint.ppt"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
        docPicker.delegate = self;
        docPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:docPicker animated:YES completion:nil];
    } else if ([item.itemName isEqualToString:@"语音"]) {
        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_AUDIO];
        
    } else if ([item.itemName isEqualToString:@"视频"]) {
        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_VIDEO];
    }
}

#pragma mark - MapLocationViewController Delegate

- (void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address {

    NSString *strLat = [NSString stringWithFormat:@"%f",coordinate.latitude];
    NSString *strLon = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
    YWMessageBodyLocation *body = [[YWMessageBodyLocation alloc] initWithMessageLocation:coordinate locationName:address];
    __block BOOL hasAdd = NO;
    [_conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        if (!hasAdd) {
            id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
            // 更新消息列表
            NSDictionary *dic = @{@"emMessage":messageRef,
                                  @"messageId":messageID,
                                  @"strLocation": address,
                                  @"strLatitude": strLat,
                                  @"strLongitude": strLon,
                                  @"type": @(UUMessageTypeLocation),
                                  @"from": @(UUMessageFromMe),
                                  @"strName":[UserInstance ShardInstnce].uid,
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            hasAdd = YES;
        }
    } completion:^(NSError *error, NSString *messageID) {
        id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
        [_chatArray addObject:messageRef];
        if (!error) {
            NSLog(@"位置消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"位置消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
    
}

#pragma mark - 输入状态
- (void)chatKeyBoardTextViewDidBeginEditing:(UITextView *)textView
{
    [self.menuController setMenuItems:@[]];
}
- (void)chatKeyBoardSendText:(NSString *)text
{
    NSString *from = [UserInstance ShardInstnce].uid;
    NSLog(@"from：%@ to %@",from,_chatterName);
    
    YWMessageBodyText *body = [[YWMessageBodyText alloc] initWithMessageText:[AESCrypt encrypt:text password:[NSString stringWithFormat:@"%@%@",AES_KEY,_chatterName]]];
    __block BOOL hasAdd = NO;
    [_conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        if (!hasAdd) {
            id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
            // 更新消息列表
            NSDictionary *dic = @{@"emMessage":messageRef,
                                  @"messageId":messageID,
                                  @"strContent": text,
                                  @"type": @(UUMessageTypeText),
                                  @"from": @(UUMessageFromMe),
                                  @"strName":from,
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            hasAdd = YES;
        }
    } completion:^(NSError *error, NSString *messageID) {
        id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
        [_chatArray addObject:messageRef];

        if (!error) {
            NSLog(@"文字消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"文字消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
        
    }];
    
    
    
}
- (void)chatKeyBoardTextViewDidChange:(UITextView *)textView
{
    
}

#pragma mark - 录音状态

- (void)startRecording {
    [self setAudioSessionWithCategory:AVAudioSessionCategoryRecord];
    if (_audioRecorder) {
        [_audioRecorder stop];
        _audioRecorder.delegate = nil;
        _audioRecorder = nil;
    }
    if (![self.audioRecorder isRecording]) {
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder record];
        recordLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(levelTimerCallback:) userInfo:nil repeats:YES];
    }
    playTime = 0;
    playTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countVoiceTime) userInfo:nil repeats:YES];
    [UUProgressHUD show];
}

/* 该方法确实会随环境音量变化而变化，但具体分贝值是否准确暂时没有研究 */
- (void)levelTimerCallback:(NSTimer *)timer {
    [self.audioRecorder updateMeters];
    
    float   level;                // The linear 0.0 .. 1.0 value we need.
    float   minDecibels = -80.0f; // Or use -60dB, which I measured in a silent room.
    float   decibels    = [self.audioRecorder averagePowerForChannel:0];
    
    if (decibels < minDecibels)
    {
        level = 0.0f;
    }
    else if (decibels >= 0.0f)
    {
        level = 1.0f;
    }
    else
    {
        float   root            = 2.0f;
        float   minAmp          = powf(10.0f, 0.05f * minDecibels);
        float   inverseAmpRange = 1.0f / (1.0f - minAmp);
        float   amp             = powf(10.0f, 0.05f * decibels);
        float   adjAmp          = (amp - minAmp) * inverseAmpRange;
        
        level = powf(adjAmp, 1.0f / root);
    }
    
    /* level 范围[0 ~ 1], 转为[0 ~6] 之间 */
    dispatch_async(dispatch_get_main_queue(), ^{
        [UUProgressHUD changeRecordLevel:level*7+1];
    });
}
/**
 *  语音状态
 */
- (void)chatKeyBoardDidStartRecording:(ChatKeyBoard *)chatKeyBoard
{
    playTime = 0;
    [[ChatHelper shareHelper] createSystemSoundWithName:@"talkroom_begin" soundType:@"mp3" vibrate:YES];
    [self performSelector:@selector(startRecording) withObject:nil afterDelay:0.5];
    
}

- (void)chatKeyBoardDidCancelRecording:(ChatKeyBoard *)chatKeyBoard
{
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    if (playTimer) {
        [self.audioRecorder stop];
        [self.audioRecorder deleteRecording];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    }
    [UUProgressHUD dismissWithError:Localized(@"has_been_cancelled")];
}

- (void)chatKeyBoardDidFinishRecoding:(ChatKeyBoard *)chatKeyBoard
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRecording) object:nil];//可以取消成功。
    
    //此处需要恢复设置回放标志，否则会导致其它播放声音也会变小
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    } else {
        [UUProgressHUD showShort:RECORD_TIME_SHORT];
        return;
    }
    if (playTime == 0) {
        [UUProgressHUD showShort:RECORD_TIME_SHORT];
        return;
    }
    [UUProgressHUD dismissWithSuccess:Localized(@"录音成功")];
    NSLog(@"%@--%@",[self getSavePath],[self getSavePath].path);
    NSString *from = [UserInstance ShardInstnce].uid;
    NSString *voiceTime = [NSString stringWithFormat:@"%d",(int)playTime];
    
//    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
//    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
    NSData *voiceData = [NSData dataWithContentsOfURL:[self getSavePath]];
    
    __block BOOL hasAdd = NO;
    YWMessageBodyVoice *body = [[YWMessageBodyVoice alloc] initWithMessageVoiceData:voiceData duration:playTime];
    [self.conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
        if (!hasAdd) {
            id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
            // 更新消息列表
            NSDictionary *dic = @{@"emMessage":messageRef,
                                  @"messageId":messageID,
                                  @"voiceUrl": [self getSavePath],
                                  @"strVoiceTime": voiceTime,
                                  @"type": @(UUMessageTypeVoice),
                                  @"from": @(UUMessageFromMe),
                                  @"strName":from,
                                  @"strIcon":_mineAvatar,
                                  @"chatType":@(_typeChat)};
            [self dealTheFunctionData:dic];
            hasAdd = YES;
        }
    } completion:^(NSError *error, NSString *messageID) {
        id<IYWMessage> messageRef  = [_conversation fetchMessageWithMessageId:messageID];
        [_chatArray addObject:messageRef];
        
        if (!error) {
            NSLog(@"语音消息发送成功");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusSuccessed;
                }
            }
            [self.tableView reloadData];
        } else {
            NSLog(@"语音消息发送失败");
            for (int i = 0; i < self.chatModel.dataSource.count; i++) {
                UUMessageFrame *frame = self.chatModel.dataSource[i];
                if ([messageID isEqualToString:frame.message.messageId]) {
                    frame.message.status = UUMessageStatusFailed;
                }
            }
            [self.tableView reloadData];
        }
    }];
    
    NSLog(@"录音完成!");
}

- (void)chatKeyBoardWillCancelRecoding:(ChatKeyBoard *)chatKeyBoard
{
    [UUProgressHUD changeSubTitle:RECORD_TOUCHOUT_CANCEL];
}

- (void)chatKeyBoardContineRecording:(ChatKeyBoard *)chatKeyBoard
{
    [UUProgressHUD changeSubTitle:RECORD_SLIDUP_CANCEL];
}

- (void)countVoiceTime
{
    playTime ++;
    if (playTime>=60) {
        [self chatKeyBoardDidFinishRecoding:nil];
        [UUProgressHUD dismissWithSuccess:nil];
        //        [self endRecordVoice:nil];
    }
}

- (void)endRecordVoice:(UIButton *)button
{
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        playTime = 0;
        
        
        
        //        //缓冲消失时间 (最好有block回调消失完成)
        //        self.btnVoiceRecord.enabled = NO;
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //            self.btnVoiceRecord.enabled = YES;
        //        });
    }
}

/**
 *  设置音频会话
 */
-(void)setAudioSessionWithCategory:(NSString *)category {
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    //设置为录音状态
    [audioSession setCategory:category error:nil];
    [audioSession setActive:YES error:nil];
}

/**
 *  取得录音文件保存路径
 *
 *  @return 录音文件路径
 */
-(NSURL *)getSavePath{
    //    NSString *name = [NSString stringWithFormat:@"recorder"];
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
    NSLog(@"file path:%@",urlStr);
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}
/**
 *  取得录音文件设置
 *
 *  @return 录音设置
 */
-(NSDictionary *)getAudioSetting{
    NSMutableDictionary *dicM=[NSMutableDictionary dictionary];
    //设置录音格式
    [dicM setObject:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
    //设置录音采样率，8000是电话采样率，对于一般录音已经够了
    [dicM setObject:@(8000) forKey:AVSampleRateKey];
    //设置通道,这里采用单声道
    [dicM setObject:@(1) forKey:AVNumberOfChannelsKey];
    //每个采样点位数,分为8、16、24、32
    [dicM setObject:@(8) forKey:AVLinearPCMBitDepthKey];
    //是否使用浮点数采样
    [dicM setObject:@(YES) forKey:AVLinearPCMIsFloatKey];
    //录音的质量
    [dicM setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    //....其他设置等
    return dicM;
}

#pragma mark - self cycle

- (ChatKeyBoard *)chatKeyBoard
{
    if (_chatKeyBoard == nil) {
        _chatKeyBoard = [ChatKeyBoard keyBoard];
        _chatKeyBoard.delegate = self;
        _chatKeyBoard.dataSource = self;
        _chatKeyBoard.keyBoardStyle = KeyBoardStyleChat;
        _chatKeyBoard.placeHolder = @"请输入消息";
    }
    return _chatKeyBoard;
}

- (AVAudioRecorder *)audioRecorder
{
    if (_audioRecorder == nil) {
        //创建录音文件保存路径
        NSURL *url=[self getSavePath];
        //创建录音格式设置
        NSDictionary *setting=[self getAudioSetting];
        NSError *error;
        _audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate = self;
        if (error) {
            NSLog(@"创建录音机对象时发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}

- (AVAudioPlayer *)audioPlayer
{
    if (!_audioPlayer) {
        NSURL *url = [self getSavePath];
        NSError *error;
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        _audioPlayer.numberOfLoops = 0;
        [_audioPlayer prepareToPlay];
        if (error) {
            NSLog(@"创建播放器过程中发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioPlayer;
}
#endif

@end
