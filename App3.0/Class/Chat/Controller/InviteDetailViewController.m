//
//  InviteDetailViewController.m
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "InviteDetailViewController.h"
#import "HTTPManager+Chat.h"
#import "ChatModel.h"
#import "InviterInfoCell.h"
#import "InviteDetailCell.h"

@interface InviteDetailViewController ()
@property (strong, nonatomic) InviteInfoModel *inviteModel;
@end

@implementation InviteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setInvite_id:(NSString *)invite_id {
    _invite_id = invite_id;
    
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getMemberInviteInfoWithInviteId:invite_id success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.inviteModel = [InviteInfoModel mj_objectWithKeyValues:dic[@"data"]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)setSubviews {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 70)];
    self.tableView.tableFooterView = footerView;
    
    UIButton *confirm = [UIButton buttonWithType:UIButtonTypeCustom];
    confirm.backgroundColor = mainColor;
    [confirm setTitle:@"确认邀请" forState:UIControlStateNormal];
    [confirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirm.titleLabel.font = [UIFont systemFontOfSize:17];
    confirm.layer.masksToBounds = YES;
    confirm.layer.cornerRadius = 3;
    [confirm addTarget:self action:@selector(confirmClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:confirm];
    [confirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(footerView).offset(12);
        make.right.mas_equalTo(footerView).offset(-12);
        make.bottom.mas_equalTo(footerView);
        make.height.mas_equalTo(50);
    }];
    
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)confirmClick {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager verifyMemberInviteWithInviteId:self.invite_id success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state) {
            [XSTool showToastWithView:self.view Text:@"邀请成功"];
            
#ifdef ALIYM_AVALABLE
            id<IYWMessage> message = self.message;
            YWConversation *conversation = [YWConversation fetchConversationByConversationId:message.conversationId creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            
#elif defined EMIM_AVALABLE
            // 更新消息
            EMMessage *message = self.message;
            EMConversation *conver = [[EMClient sharedClient].chatManager getConversation:message.conversationId type:(EMConversationType)message.chatType createIfNotExist:YES];
            NSMutableDictionary *mutD = [NSMutableDictionary dictionaryWithDictionary:message.ext];
            [mutD setObject:GROUP_INVITE_CONFIRM forKey:MSG_TYPE];
            message.ext = mutD;
            [conver updateMessageChange:message error:nil];
#else
            [XMPPSignal groupUpMessageWith:self.xmppRoom.roomJID.full messageID:self.messageID finash:nil];
            //            [XMPPSignal groupUpMessageWith:self.xmppRoom.roomJID.full data:self.remoteTimestamp body:self.message.body finash:nil];
            
            [self.inviteModel.target_users.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                InvitePersonModel *model = x;
                XMPPJID *jid = [XMPPJID getUserJIDWithString:model.user_id];
                [self.xmppRoom inviteUser:jid withMessage:nil];
                
            }completed:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSelector:@selector(popViewController) withObject:nil afterDelay:1.0];
                });
            }];
#endif
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            [self performSelector:@selector(popViewController) withObject:nil afterDelay:1.0];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 12;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 68;
    }
    return ceil(self.inviteModel.target_users.count/5.0) * ((mainWidth-24)/5.0+20)+20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        InviterInfoCell *cell = [InviterInfoCell createInviterInfoCellWithTableView:tableView];
        cell.inviteModel = self.inviteModel;
        return cell;
    }
    InviteDetailCell *cell = [InviteDetailCell createInviteDetailCellWithTableView:tableView];
    cell.inviteModel = self.inviteModel;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
@end
