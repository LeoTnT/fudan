//
//  GroupAllMembersViewController.h
//  App3.0
//
//  Created by mac on 17/4/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface GroupAllMembersViewController : XSBaseTableViewController
@property (nonatomic, strong) GroupDataModel *dataSource;
@property (nonatomic, copy) NSString *groupId;
@property (nonatomic, assign) BOOL isGroupOwner;
@end
