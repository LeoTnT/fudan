//
//  FindChatContentViewController.m
//  App3.0
//
//  Created by mac on 2017/6/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FindChatContentViewController.h"
#import "ChatModel.h"
#import "LoginModel.h"
#import "UUMessage.h"
#import "UUMessageFrame.h"
#import "UUMessageCell.h"
#import "UserInstance.h"
#import "SearchBarView.h"
#import "ChatListCell.h"
#import "XMMessageModel.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface FindChatContentViewController () <SearchBarViewDelegate>
{
    long long _timestamp;
    UIView *_hotSearchView;
    UILabel *_hotSearchLabel;
    NSString *_searchStr;
}
@property (copy, nonatomic) NSString *startMessageId;
@property (copy, nonatomic) NSString *endMessageId;
@property (strong, nonatomic) NSMutableArray *chatListArray;
@property (strong, nonatomic) ChatModel *chatModel;

@property (strong, nonatomic) SearchBarView *searchBarView;
@end

@implementation FindChatContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationItem.title = @"消息记录";

//    self.autoHideKeyboard = YES;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    _timestamp = -1; //初始化时间戳，参考的时间戳为负数，则从最新消息向前取
    self.chatModel = [[ChatModel alloc] init];

    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.navigationController.navigationBar addSubview:self.searchBarView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    if (self.searchBarView) {
//        [self.searchBarView removeFromSuperview];
//    }
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    //TODO: 页面appear 禁用
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    if (self.searchBarView) {
        [self.searchBarView removeFromSuperview];
    }
}

#pragma mark - 懒加载
- (NSMutableArray *)chatListArray {
    if (_chatListArray == nil) {
        _chatListArray = [NSMutableArray array];
    }
    return _chatListArray;
}

- (SearchBarView *)searchBarView {
    if (_searchBarView == nil) {
        _searchBarView = [[SearchBarView alloc] initWithFrame:CGRectMake(40, 7, mainWidth-50, 30)];
        _searchBarView.searchBarStyle = SearchBarStyleLine;
        _searchBarView.delegate = self;
        
        // 添加自动显示的搜索按钮
        _hotSearchView = [[UIView alloc] init];
        _hotSearchView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_hotSearchView];
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageNamed:@"search_big"];
        [_hotSearchView addSubview:imgView];
        _hotSearchLabel = [[UILabel alloc] init];
        _hotSearchLabel.font = [UIFont systemFontOfSize:14];
        _hotSearchLabel.textColor = [UIColor blackColor];
        [_hotSearchView addSubview:_hotSearchLabel];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchChatContentWithKeyword)];
        [_hotSearchView addGestureRecognizer:tap];
        _hotSearchView.hidden = YES;
        
        [_hotSearchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_equalTo(68);
        }];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_hotSearchView).offset(11.5);
            make.centerY.mas_equalTo(_hotSearchView);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        [_hotSearchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imgView.mas_right).offset(8);
            make.centerY.mas_equalTo(_hotSearchView);
            make.right.mas_equalTo(_hotSearchView).offset(-12);
        }];
    }
    return _searchBarView;
}

- (void)setBottomView {
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UIButton *prePageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [prePageBtn setTitle:@"上一页" forState:UIControlStateNormal];
    [prePageBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [prePageBtn addTarget:self action:@selector(getEarlierChatContent) forControlEvents:UIControlEventTouchUpInside];
    prePageBtn.layer.masksToBounds = YES;
    prePageBtn.layer.cornerRadius = 3;
    [bottomView addSubview:prePageBtn];
    
    UIButton *nextPageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextPageBtn setTitle:@"下一页" forState:UIControlStateNormal];
    [nextPageBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [nextPageBtn addTarget:self action:@selector(getLaterChatContent) forControlEvents:UIControlEventTouchUpInside];
    nextPageBtn.layer.masksToBounds = YES;
    nextPageBtn.layer.cornerRadius = 3;
    [bottomView addSubview:nextPageBtn];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    [prePageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bottomView.mas_centerX).offset(-40);
        make.centerY.mas_equalTo(bottomView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    
    [nextPageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bottomView.mas_centerX).offset(40);
        make.centerY.mas_equalTo(bottomView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
}

// 获取之前的消息记录
- (void)getEarlierChatContent {
    [self.chatListArray removeAllObjects];
#ifdef ALIYM_AVALABLE
    @weakify(self);
    [self.conversation loadMoreMessages:10 completion:^(BOOL existMore) {
        @strongify(self);
        [self.chatListArray addObjectsFromArray:((YWConversation *)self.conversation).fetchedObjects];
    }];
#elif defined EMIM_AVALABLE
    @weakify(self);
    [self.conversation loadMessagesStartFromId:self.startMessageId count:10 searchDirection:EMMessageSearchDirectionUp completion:^(NSArray *aMessages, EMError *aError) {
        if (!aError) {
            @strongify(self);
            EMMessage *startMes = aMessages.firstObject;
            self.startMessageId = startMes.messageId;
            EMMessage *endMes = aMessages.lastObject;
            self.endMessageId = endMes.messageId;
            [self.chatListArray addObjectsFromArray:aMessages];
        } else {
            [XSTool showToastWithView:self.view Text:aError.errorDescription];
        }
        
    }];
#else
    [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
    
}

// 获取后来的消息记录
- (void)getLaterChatContent {
    [self.chatListArray removeAllObjects];
#ifdef ALIYM_AVALABLE
    @weakify(self);
    [self.conversation loadMoreMessages:10 completion:^(BOOL existMore) {
        @strongify(self);
        NSArray *data = ((YWConversation *)self.conversation).fetchedObjects;
        for (int i = (int)data.count-11; i < data.count; i++) {
            [self.chatListArray addObject:data[i]];
        }
    }];
#elif defined EMIM_AVALABLE
    @weakify(self);
    [self.conversation loadMessagesStartFromId:self.endMessageId count:10 searchDirection:EMMessageSearchDirectionDown completion:^(NSArray *aMessages, EMError *aError) {
        if (!aError) {
            @strongify(self);
            EMMessage *startMes = aMessages.firstObject;
            self.startMessageId = startMes.messageId;
            EMMessage *endMes = aMessages.lastObject;
            self.endMessageId = endMes.messageId;
            [self.chatListArray addObjectsFromArray:aMessages];
        } else {
            [XSTool showToastWithView:self.view Text:aError.errorDescription];
        }
        
    }];
#else
    [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
    
}

- (void)searchChatContentWithKeyword {
    _hotSearchView.hidden = YES;
    [self.searchBarView endEditing:YES];
    [self.chatListArray removeAllObjects];
#ifdef ALIYM_AVALABLE
    [self.chatListArray addObjectsFromArray:[self.conversation searchMessagesWithKeyword:_searchStr]];
    [self.tableView reloadData];
#elif defined EMIM_AVALABLE
    @weakify(self);
    [self.conversation loadMessagesWithKeyword:_searchStr timestamp:-1 count:50 fromUser:nil searchDirection:EMMessageSearchDirectionUp completion:^(NSArray *aMessages, EMError *aError) {
        if (!aError) {
            @strongify(self);
            EMMessage *startMes = aMessages.firstObject;
            self.startMessageId = startMes.messageId;
            EMMessage *endMes = aMessages.lastObject;
            self.endMessageId = endMes.messageId;
            _timestamp = endMes.timestamp;
            [self.chatListArray addObjectsFromArray:aMessages];
            [self.tableView reloadData];
            //            [self dealMessages:self.chatListArray];
        } else {
            [XSTool showToastWithView:self.view Text:aError.errorDescription];
        }
    }];
#else
    XMMessageModel *model = [[XMMessageModel alloc] initSearchText:_searchStr bareJidStr:self.conversation];
    
    [model.searchTextSignal subscribeNext:^(NSArray *arr) {
        [self.chatListArray addObjectsFromArray:arr];
        XMPPMessageArchiving_Message_CoreDataObject *object = arr.firstObject;
        NSLog(@"object  =%@",object.body);
        
    }completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
#endif
    
}

#pragma mark -  SearchBarViewDelegate
- (void)searchTextChanged:(NSString *)text
{
    [self.chatListArray removeAllObjects];
    [self.tableView reloadData];
    if (text.length > 0) {
        _searchStr = text;
        _hotSearchView.hidden = NO;
        _hotSearchLabel.text = [NSString stringWithFormat:@"搜索：%@",text];
    } else {
        _hotSearchView.hidden = YES;
    }
}

- (void)searchCancel {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)search:(NSString *)text {
    _searchStr = text;
    [self searchChatContentWithKeyword];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [NSString stringWithFormat:@"chat%ld",(long)indexPath.row];
//    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    [cell setMessageFrame:self.chatModel.dataSource[indexPath.row]];
    ChatListCell *cell = (ChatListCell*)[tableView  dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
#ifdef XSIM_AVALABLE
    XMPPMessageArchiving_Message_CoreDataObject *object = self.chatListArray[indexPath.row];
    ConversationModel *model = [[ConversationModel alloc] initWithMessage:object];
    [cell setChatListCellData:model];
#else
    [cell setFindChatData:self.chatListArray[indexPath.row]];
#endif
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.chatModel.dataSource.count;
    return self.chatListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return [self.chatModel.dataSource[indexPath.row] cellHeight];
    return 68;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

@end
