//
//  YWChatViewController.h
//  App3.0
//
//  Created by mac on 2018/1/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface YWChatViewController : XSBaseTableViewController
#ifdef ALIYM_AVALABLE
- (id)initWithConversation:(YWConversation *)conversation;
@property (strong, nonatomic) YWConversation *conversation;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *avatarUrl;

/*
 商品详情进入会话显示商品
 */
@property (nonatomic, copy) NSString *productJsonString;
@property (nonatomic, copy) NSString *productMessageId;

/*!
 @property
 @brief 页面是否处于显示状态
 */
@property (nonatomic) BOOL isViewDidAppear;

// 接收到新消息
- (void)messagesDidReceive:(NSArray *)aMessages;

- (void)openRedPacketWithId:(NSString *)rpId;
- (void)detailRedPacketWithId:(NSString *)rpId;
#endif

@end
