//
//  PersonalViewController.h
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface PersonalViewController : XSBaseTableViewController
- (instancetype)initWithUid:(NSString *)uid;
@property(nonatomic,strong)NSDictionary *updateDic;
 @property (nonatomic ,copy) void (^getRemar)(NSString *remark);
@end
