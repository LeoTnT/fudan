//
//  GroupQRViewController.h
//  App3.0
//
//  Created by mac on 2018/1/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@class GroupDataModel;
@interface GroupQRViewController : XSBaseViewController
@property (strong, nonatomic) GroupDataModel *groupModel;
@end
