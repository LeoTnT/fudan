//
//  GroupMemberManagerViewController.m
//  App3.0
//
//  Created by mac on 17/3/30.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupMemberManagerViewController.h"
#import "NSString+PinYin.h"
#import "ContactModel.h"
#import "ContactSeletCell.h"
#import "ChatViewController.h"
#import "UserInstance.h"

@interface GroupMemberManagerViewController ()<UISearchBarDelegate>
{
    NSString *_groupId;
    NSArray *_memberArr;
    
    NSMutableArray *_contactDataArr;
    NSMutableArray *_selectedArr;
}
@property(nonatomic, strong)UISearchBar *searchBar;
@end

@implementation GroupMemberManagerViewController
- (instancetype)initWithGroupId:(NSString *)gId memberArray:(NSArray *)mArr
{
    self = [super init];
    if (self) {
        _groupId = gId;
        _memberArr = [NSArray arrayWithArray:mArr];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
    [self.view addSubview:self.searchBar];
    [self setupDataArray];
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"material_dialog_positive_text") action:^{
        if (wSelf.gmType == GMManagerAdd) {
            // 邀请群员
            if(!_selectedArr.count){
                [XSTool showToastWithView:self.view Text:@"未选择群成员"];
                return ;
            }
            // 创建群员编号数组
            NSMutableArray *idsArr = [NSMutableArray array];
#ifdef XSIM_AVALABLE
            NSMutableArray *jidArr = [NSMutableArray array];
#endif
            
            for (ContactDataParser *parser in _selectedArr) {
                [idsArr addObject:parser.uid];
#ifdef XSIM_AVALABLE
                NSString *userID = getJID(parser.uid);
                [jidArr addObject:[XMPPJID jidWithString:userID]];
#endif
            }
            NSString *memberString = [idsArr componentsJoinedByString:@","];
            [XSTool showProgressHUDTOView:self.view withText:@"设置中"];
            [HTTPManager addMembers:memberString forGroup:_groupId success:^(NSDictionary * dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    // 添加群成员成功
                    GroupMemberAddResultModel *model = [GroupMemberAddResultModel mj_objectWithKeyValues:dic[@"data"]];
                    if ([model.invite_id integerValue] != 0) {
                        // 需要群主审核
                        
                        // 发送添加群成员申请消息
                        NSDictionary *extDic = @{MSG_TYPE:GROUP_INVITE_NEED_CONFIRM,GROUP_INVITE_ID:model.invite_id,GROUP_INVITE_OWNER:model.owner};
                        NSString *text = [NSString stringWithFormat:@"%@想邀请%@位朋友加入群聊",[UserInstance ShardInstnce].nickName,model.invite_num];
#ifdef ALIYM_AVALABLE
                        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                        YWTribe *tribe = [YWTribe new];
                        tribe.tribeId = _groupId;
                        YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                        [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                            
                        } completion:^(NSError *error, NSString *messageID) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        }];
#elif defined EMIM_AVALABLE
                        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                        // 生成message
                        EMMessage *message = [[EMMessage alloc] initWithConversationID:_groupId from:[UserInstance ShardInstnce].uid to:_groupId body:body ext:extDic];
                        message.chatType = EMChatTypeGroupChat;// 设置消息类型
                        //发送消息
                        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                            
                        } completion:^(EMMessage *message, EMError *error) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        }];
#else
                        // 发送添加群成员消息
                        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
                        message.type = @"txt";
                        message.body = text;
                        message.ext = extDic.mj_JSONString;
                        NSString *messageString = message.mj_JSONString;
                        [self.roomManager.xmppRoom sendMessageWithBody:messageString];
//                        [self.roomManager.xmppRoom inviteUsers:jidArr withMessage:@"加入群"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
#endif
                        
                    } else {
                        // 发送添加群成员消息
                        NSString *text = [NSString stringWithFormat:@"%@邀请%@加入群聊",[UserInstance ShardInstnce].nickName,model.nick_str];
                        NSDictionary *extDic = @{MSG_TYPE:GROUP_INVITE_ACTION};
#ifdef ALIYM_AVALABLE
                        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                        YWTribe *tribe = [YWTribe new];
                        tribe.tribeId = _groupId;
                        YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                        [conversation asyncSendMessageBody:body progress:^(CGFloat progress, NSString *messageID) {
                            
                        } completion:^(NSError *error, NSString *messageID) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        }];
#elif defined EMIM_AVALABLE
                        EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                        EMMessage *message = [[EMMessage alloc] initWithConversationID:_groupId from:[UserInstance ShardInstnce].uid to:_groupId body:body ext:extDic];
                        message.chatType = EMChatTypeGroupChat;// 设置消息类型
                        //发送消息
                        [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                            
                        } completion:^(EMMessage *message, EMError *error) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                        }];
#else
                        // 发送添加群成员消息
                        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
                        message.type = @"txt";
                        message.body = text;
                        message.ext = extDic.mj_JSONString;
                        NSString *messageString = message.mj_JSONString;
                        [self.roomManager.xmppRoom sendMessageWithBody:messageString];
                        [self.roomManager.xmppRoom inviteUsers:jidArr withMessage:@"加入群"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
#endif
                        
                    }
                    
                    
                    
                    [wSelf.navigationController popViewControllerAnimated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                 [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            }];
        } else if (wSelf.gmType == GMManagerSub) {
            // 移除群员
            if(!_selectedArr.count){
                [XSTool showToastWithView:self.view Text:@"未选择群成员"];
                return ;
            }
            // 创建群员编号数组
            NSMutableArray *idsArr = [NSMutableArray array];
            for (ContactDataParser *parser in _selectedArr) {
                [idsArr addObject:parser.uid];
            }
            NSString *memberString = [idsArr componentsJoinedByString:@","];
            [XSTool showProgressHUDTOView:self.view withText:@"设置中"];
            [HTTPManager deleteMembers:memberString forGroup:_groupId success:^(NSDictionary * dic, resultObject *state) {
                  [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    // 移除群成员成功
                    [wSelf.navigationController popViewControllerAnimated:YES];
//                    [idsArr.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
//                        [XMPPSignal removeUsersWithUID:x withJID:[XMPPJID jidWithString:[NSString stringWithFormat:@"%@%@",_groupId,XMPPR_ROOM_Regulation]]];
//
//                    } completed:^{
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self.navigationController popViewControllerAnimated:YES];
//                        });
//                    }];
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                 [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            }];
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupDataArray
{
    _selectedArr = [NSMutableArray array];
    _contactDataArr = [[_memberArr contactArrayWithPinYinFirstLetterFormat] mutableCopy];
}

#pragma mark - searchbar delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"%s",__FUNCTION__);
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    for (NSIndexPath *indexPath in [self.tableView indexPathsForVisibleRows]) {
        ContactSeletCell *cell = (ContactSeletCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.isSelect = NO;
    }
    [_contactDataArr removeAllObjects];
//    [_selectedArr removeAllObjects];
    if (searchText.length > 0) {
        NSMutableArray *tempArr = [NSMutableArray array];
        for (ContactDataParser *model in _memberArr) {
            if ([[model getName] containsString:searchText]) {
                [tempArr addObject:model];
            }
        }
        [_contactDataArr addObjectsFromArray:[tempArr contactArrayWithPinYinFirstLetterFormat]];
    } else {
        [_contactDataArr addObjectsFromArray:[_memberArr contactArrayWithPinYinFirstLetterFormat]];
    }
    [self.tableView reloadData];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _contactDataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *contentArr = [_contactDataArr[section] objectForKey:@"content"];
    return contentArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = mainGrayColor;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = _contactDataArr[section][@"firstLetter"];
    titleLabel.text=title;
    [myView  addSubview:titleLabel];
    
    return myView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *contentArr = [_contactDataArr[indexPath.section] objectForKey:@"content"];
    ContactDataParser *parser = contentArr[indexPath.row];
    ContactSeletCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.isSelect = !cell.isSelect;
    if (cell.isSelect) {
        [_selectedArr addObject:parser];
    } else {
        [_selectedArr removeObject:parser];
    }
    NSLog(@"%lu",(unsigned long)_selectedArr.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"ContactSeletCell%ld ",(long)indexPath.row];
    ContactSeletCell *cell = (ContactSeletCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[ContactSeletCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSArray *contentArr = [_contactDataArr[indexPath.section] objectForKey:@"content"];
    ContactDataParser *parser = contentArr[indexPath.row];
    [cell setCellData:parser];
    cell.isSelect = NO;
    for (ContactDataParser *parser1 in _selectedArr) {
        if ([parser.uid isEqualToString:parser1.uid]) {
            cell.isSelect = YES;
        }
    }
    return cell;
}

- (UISearchBar *)searchBar
{
    if (_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.placeholder = Localized(@"search");
        _searchBar.translucent = YES; // 设置是否透明
        
        // 去掉灰色边框
        for (UIView *view in self.searchBar.subviews) {
            if ([view isKindOfClass:NSClassFromString(@"UIView")]&&view.subviews.count>0) {
                view.backgroundColor = BG_COLOR;
                [[view.subviews objectAtIndex:0] removeFromSuperview];
                break;
            }
        }
    }
    return _searchBar;
}
@end
