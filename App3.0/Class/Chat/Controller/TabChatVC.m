//
//  TabChatVC.m
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabChatVC.h"
#import "SearchVC.h"
#import "ChatListCell.h"
#import "PopViewController.h"
#import "EnterViewController.h"
#import "ChatHelper.h"
#import "ConversationModel.h"
#import "AddFriendsViewController.h"
#import "ScanViewController.h"
#import "ContactModel.h"
#import "ContactListSelectViewController.h"
#import "PersonalViewController.h"
#import "EmptyView.h"

#ifdef ALIYM_AVALABLE
#import "YWChatViewController.h"
#elif defined EMIM_AVALABLE
#import "ChatViewController.h"
#else
#import "XMChatController.h"
#import "XMGroupChatController.h"
#import "XMPPCoredataChatContact+CoreDataClass.h"
#import "XMPPCoredataChatContact+CoreDataProperties.h"
#endif

@interface TabChatVC () <UIPopoverPresentationControllerDelegate, PopViewDelegate, NSFetchedResultsControllerDelegate>
{
    PopViewController *_popVC;
    EmptyView *_emptyView;
}
@property(nonatomic, strong)UIButton *searchBtn;
@property(nonatomic, strong)UIView *networkStateView;

@property (nonatomic ,strong)NSMutableArray *dataArr;
@property (nonatomic ,strong)NSFetchedResultsController *resultController;
@end

@implementation TabChatVC
{
    NSInteger unreadCount;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    self.navigationItem.title = Localized(@"me_item_chat");
    _dataArr = [NSMutableArray array];
    
    @weakify(self);
    [self actionCustomRightBtnWithNrlImage:@"nav_add" htlImage:nil title:nil action:^{
        @strongify(self);
        [self popView];
    }];
    self.view.backgroundColor = BG_COLOR;
    
    self.tableViewStyle = UITableViewStyleGrouped;
    //    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
#ifdef XSIM_AVALABLE
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(50, 0, 0, 0));
    }];
#endif
#ifdef EMIM_AVALABLE
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
#endif
    
    [self.view layoutIfNeeded];
    self.tableView.backgroundColor = BG_COLOR;
    self.showRefreshHeader = YES;
    
    [self setRefreshHeader:^{
        @strongify(self);
#ifdef XSIM_AVALABLE
        [self.resultController performFetch:nil];
        [self reloadTableView];
#endif
#ifdef EMIM_AVALABLE
        [self refreshConversationLists];
#endif
        
    }];
    [self.view addSubview:self.searchBtn];
    [self.tableView.mj_header beginRefreshing];
    
//    // 设置chatHelper
//    [ChatHelper shareHelper].tabChatVC = self;
#ifdef ALIYM_AVALABLE
    [SPKitExample sharedInstance].tabChatVC = self;
#endif
    
    // 添加空页面
    _emptyView = [[EmptyView alloc] initWithStyle:EmptyViewStyleChat frame:self.view.bounds];
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    
#ifdef XSIM_AVALABLE
    // 群变化操作
    [[XMPPManager sharedManager].deletedMessageSubject subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        RACTupleUnpack(NSString *type,XMPPRoom *xmppRoom) = x;
        if ([type isEqualToString:@"deleteRoom"]) {
            [self.dataArr.rac_sequence.signal subscribeNext:^(ConversationModel *mode) {
                XMPP_LOG(@" 循环群  - - - --%@" ,mode.xmppRoomManager.xmppRoom.roomJID)
                if ([mode.conversation isEqual:xmppRoom.roomJID.full]) {
                    [self deletedDataWithContact:mode];
                }
            } ];
        } else if ([type isEqualToString:@"reloadRoomMember"]){
            [self.tableView reloadData];
        }
        
    }];
    
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
#ifdef EMIM_AVALABLE
    [self refreshConversationLists];
#endif
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -模仿qq界面
- (void)qqStyle
{
    //设置扫码区域参数设置
    
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    ScanViewController *scanVC = [[ScanViewController alloc] init];
    scanVC.style = style;
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
}

#pragma mark - navigationItem Action
-(void)popView{
    
    _popVC = [[PopViewController alloc] init];
    _popVC.modalPresentationStyle = UIModalPresentationPopover;
    _popVC.dataSource = @[@{@"title":Localized(@"发起群聊"),
                            @"image":@"chat_add_group"
                            },
                          @{@"title":Localized(@"添加朋友"),
                            @"image":@"chat_add_friend"
                            },
                          @{@"title":Localized(@"扫一扫"),
                            @"image":@"chat_add_qr"
                            }];
    
    //设置依附的按钮
    _popVC.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    //可以指示小箭头颜色
    _popVC.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    
    //content尺寸
    _popVC.preferredContentSize = CGSizeMake(400, 400);
    
    //pop方向
    _popVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    //delegate
    _popVC.popoverPresentationController.delegate = self;
    _popVC.delegate = self;
    [self presentViewController:_popVC animated:YES completion:nil];
}

//代理方法 ,点击即可dismiss掉每次init产生的PopViewController
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}

#pragma mark - 云旺
#ifdef ALIYM_AVALABLE

-(void)refreshAndSortView
{
    if ([_dataArr count] > 1) {
        if ([[_dataArr objectAtIndex:0] isKindOfClass:[ConversationModel class]]) {
            NSArray* sorted = [_dataArr sortedArrayUsingComparator:
                               ^(ConversationModel *obj1, ConversationModel* obj2){
                                   id<IYWMessage> message1 = [((YWConversation*)obj1.conversation) conversationLatestMessage];
                                   id<IYWMessage> message2 = [((YWConversation*)obj2.conversation) conversationLatestMessage];
                                   return [message2.time compare:message1.time];
                               }];
            [_dataArr removeAllObjects];
            [_dataArr addObjectsFromArray:sorted];
        }
    }
    [self.tableView reloadData];
}

- (void)refreshConversationLists {
    [[[SPKitExample sharedInstance].ywIMKit.IMCore getConversationService] asyncFetchAllConversationsWithCompletionBlock:^(NSArray *aConversationsArray) {
        [_dataArr removeAllObjects];
        unreadCount = 0;
        NSArray* sorted = [aConversationsArray sortedArrayUsingComparator:
                           ^(YWConversation *obj1, YWConversation* obj2){
                               id<IYWMessage> message1 = [obj1 conversationLatestMessage];
                               id<IYWMessage> message2 = [obj2 conversationLatestMessage];
                               return [message2.time compare:message1.time];
                           }];
        for (YWConversation *conversation in sorted) {
            if (conversation.conversationLatestMessage == nil) {
                continue;
            }
            unreadCount += [conversation.conversationUnreadMessagesCount integerValue];
            ConversationModel *model = [[ConversationModel alloc] initWithConversation:conversation];
            if (conversation.conversationType == YWConversationTypeP2P) {
                YWPerson *person = [(YWP2PConversation *)conversation person];
                NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:person.personId];
                if (contacts != nil && contacts.count > 0) {
                    // 本地数据库存在
                    ContactDataParser *contact = contacts[0];
                    model.title = [contact getName];
                    model.avatarURLPath = contact.avatar;
                    [_dataArr addObject:model];
                } else {
                    // 本地数据库不存在
                    [HTTPManager getUserInfoWithUid:person.personId success:^(NSDictionary * dic, resultObject *state) {
                        UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
                        if (state.status) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                                
                                // 会话对象是机器人特殊处理
                                if ([person.personId isEqualToString:@"robot"]) {
                                    cdParser.username = Localized(@"系统消息");
                                    cdParser.remark = Localized(@"系统消息");
                                    cdParser.nickname = Localized(@"系统消息");
                                    model.title = Localized(@"系统消息");
                                    
                                } else {
                                    cdParser.username = parser.data.username;
                                    cdParser.remark = parser.data.remark;
                                    cdParser.nickname = parser.data.nickname;
                                    model.title = [parser.data getName];
                                }
                                cdParser.uid = person.personId;
                                cdParser.avatar = parser.data.logo;
                                cdParser.relation = parser.data.relation;
                                cdParser.mobile = parser.data.mobile;
                                
                                
                                model.avatarURLPath = parser.data.logo;
                                
                                // 防止同一时间刷新两次列表造成数据重复添加
                                NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                                if (contactT.count == 0) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        _emptyView.hidden = YES;
                                        [_dataArr addObject:model];
                                        [self refreshAndSortView];  // 排序
                                        [self.tableView reloadData];
                                    });
                                }
                                
                                // 添加数据库操作放在最后
                                [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                            });
                            
                        }
                    } fail:^(NSError * _Nonnull error) {
                        
                    }];
                }
                
            } else if (conversation.conversationType == YWConversationTypeTribe) {
                YWTribe *tribe = [(YWTribeConversation *)conversation tribe];
                NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:tribe.tribeId];
                if (groupDBArray != nil && groupDBArray.count > 0) {
                    GroupDataModel *parser = groupDBArray[0];
                    model.title = parser.name;
                    model.avatarURLPath = parser.avatar;
                    [_dataArr addObject:model];
                } else {
                    [HTTPManager fetchGroupInfoWithGroupId:tribe.tribeId getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
                        GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
                        if (state.status) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                model.title = parser.data.name;
                                model.avatarURLPath = parser.data.avatar;
                                
                                // 防止同一时间刷新两次列表造成数据重复添加
                                NSArray *groupT = [[DBHandler sharedInstance] getGroupByGroupId:parser.data.gid];
                                if (groupT.count == 0) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        _emptyView.hidden = YES;
                                        [_dataArr addObject:model];
                                        [self refreshAndSortView];  // 排序
                                        [self.tableView reloadData];
                                    });
                                }
                                
                                // 添加数据库操作放在最后
                                [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
                            });
                            
                        }
                    } fail:^(NSError * _Nonnull error) {
                        
                    }];
                }
            }
            
        }
        if (_dataArr.count == 0) {
            _emptyView.hidden = NO;
        } else {
            _emptyView.hidden = YES;
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self setupUnreadMessageCount];
    }];
}

// 统计未读消息
- (void)setupUnreadMessageCount
{
    if (unreadCount > 0) {
        self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d",(int)unreadCount];
    } else {
        self.navigationController.tabBarItem.badgeValue = nil;
    }
    
    [[ChatHelper shareHelper] setBadgeNumber:unreadCount];
    // 设置应用角标
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}
#elif defined EMIM_AVALABLE
-(void)refreshAndSortView
{
    if ([_dataArr count] > 1) {
        if ([[_dataArr objectAtIndex:0] isKindOfClass:[ConversationModel class]]) {
            NSArray* sorted = [_dataArr sortedArrayUsingComparator:
                               ^(ConversationModel *obj1, ConversationModel* obj2){
                                   EMMessage *message1 = [obj1.conversation latestMessage];
                                   EMMessage *message2 = [obj2.conversation latestMessage];
                                   if(message1.timestamp > message2.timestamp) {
                                       return(NSComparisonResult)NSOrderedAscending;
                                   }else {
                                       return(NSComparisonResult)NSOrderedDescending;
                                   }
                               }];
            [_dataArr removeAllObjects];
            [_dataArr addObjectsFromArray:sorted];
        }
    }
    [self.tableView reloadData];
}

// 刷新消息列表
- (void)refreshConversationLists
{
    unreadCount = 0;
    [_dataArr removeAllObjects];
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSArray* sorted = [conversations sortedArrayUsingComparator:
                       ^(EMConversation *obj1, EMConversation* obj2){
                           EMMessage *message1 = [obj1 latestMessage];
                           EMMessage *message2 = [obj2 latestMessage];
                           if(message1.timestamp > message2.timestamp) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    for (EMConversation *conversation in sorted) {
        if (conversation.latestMessage == nil) {
            continue;
        }
        
        // 累加消息未读数
        unreadCount += conversation.unreadMessagesCount;
        
        NSLog(@"conversation:%@-%d-%d",conversation.conversationId,conversation.unreadMessagesCount,conversation.type);
        ConversationModel *model = [[ConversationModel alloc] initWithConversation:conversation];
        if (conversation.type == EMConversationTypeChat) {
            NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:conversation.conversationId];
            if (contacts != nil && contacts.count > 0) {
                // 本地数据库存在
                ContactDataParser *contact = contacts[0];
                model.title = [contact getName];
                model.avatarURLPath = contact.avatar;
                [_dataArr addObject:model];
            } else {
                // 本地数据库不存在
                [HTTPManager getUserInfoWithUid:conversation.conversationId success:^(NSDictionary * dic, resultObject *state) {
                    UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
                    if (state.status) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                            
                            // 会话对象是机器人特殊处理
                            if ([conversation.conversationId isEqualToString:@"robot"]) {
                                cdParser.username = Localized(@"系统消息");
                                cdParser.remark = Localized(@"系统消息");
                                cdParser.nickname = Localized(@"系统消息");
                                model.title = Localized(@"系统消息");
                                
                            } else {
                                cdParser.username = parser.data.username;
                                cdParser.remark = parser.data.remark;
                                cdParser.nickname = parser.data.nickname;
                                model.title = [parser.data getName];
                            }
                            cdParser.uid = conversation.conversationId;
                            cdParser.avatar = parser.data.logo;
                            cdParser.relation = parser.data.relation;
                            cdParser.mobile = parser.data.mobile;
                            
                            
                            model.avatarURLPath = parser.data.logo;
                            
                            // 防止同一时间刷新两次列表造成数据重复添加
                            NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                            if (contactT.count == 0) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    _emptyView.hidden = YES;
                                    [_dataArr addObject:model];
                                    [self refreshAndSortView];  // 排序
                                    [self.tableView reloadData];
                                });
                            }
                            
                            // 添加数据库操作放在最后
                            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                        });
                        
                    }
                } fail:^(NSError * _Nonnull error) {
                    
                }];
            }
            
        } else if (conversation.type == EMConversationTypeGroupChat) {
            NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:conversation.conversationId];
            if (groupDBArray != nil && groupDBArray.count > 0) {
                GroupDataModel *parser = groupDBArray[0];
                model.title = parser.name;
                model.avatarURLPath = parser.avatar;
                [_dataArr addObject:model];
            } else {
                [HTTPManager fetchGroupInfoWithGroupId:conversation.conversationId getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
                    GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
                    if (state.status) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                            model.title = parser.data.name;
                            model.avatarURLPath = parser.data.avatar;
                            
                            // 防止同一时间刷新两次列表造成数据重复添加
                            NSArray *groupT = [[DBHandler sharedInstance] getGroupByGroupId:parser.data.gid];
                            if (groupT.count == 0) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    _emptyView.hidden = YES;
                                    [_dataArr addObject:model];
                                    [self refreshAndSortView];  // 排序
                                    [self.tableView reloadData];
                                });
                            }
                            
                            // 添加数据库操作放在最后
                            [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
                        });
                        
                    }
                } fail:^(NSError * _Nonnull error) {
                    
                }];
            }
        }
        
    }
    if (_dataArr.count == 0) {
        _emptyView.hidden = NO;
    } else {
        _emptyView.hidden = YES;
    }
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self setupUnreadMessageCount];
}

// 统计未读消息
- (void)setupUnreadMessageCount
{
    // 设置底部角标
    if (unreadCount > 0) {
        self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d",(int)unreadCount];
    } else {
        self.navigationController.tabBarItem.badgeValue = nil;
    }
    
    [[ChatHelper shareHelper] setBadgeNumber:unreadCount];
    // 设置应用角标
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}

#else

- (void) setupUnreadMessageCount {
    [XMPPSignal setupUnreadMessageCount:^(NSInteger messageCount){
        if (messageCount > 0) {
            self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld",(long)messageCount];
        } else {
            self.navigationController.tabBarItem.badgeValue = nil;
        }
    }];
}

- (void) reloadTableView {
    
    
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self setupUnreadMessageCount];
}


/**
 数据排序 刷新数据
 */
- (void) willReloadTableView {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadTableView];
    });
}

- (NSFetchedResultsController *)resultController {
    
    if (!_resultController) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==%@", [XMPPManager sharedManager].stream.myJID.bare];
        NSFetchRequest *peopleRequest = [XMPPCoredataChatContact MR_requestAllWithPredicate:predicate];
        peopleRequest.predicate = predicate;
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mostRecentMessageTimestamp" ascending:NO];
        peopleRequest.sortDescriptors = @[sortDescriptor];
        _resultController = [XMPPCoredataChatContact MR_fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"mostRecentMessageTimestamp" ascending:NO delegate:self];
        
    }
    return _resultController;
}



// 更新数据库信息
- (void) requestRoomInfor:(NSString *)roomID {
    
    [HTTPManager fetchGroupInfoWithGroupId:roomID getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
        GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
        if (state.status) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            });
            
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}


- (void) contactWithDelete:(XMPPMessageArchiving_Contact_CoreDataObject *)object {
    
    for (NSInteger x = 0; x < _dataArr.count; x ++) {
        ConversationModel *model = _dataArr[x];
        if ([model.bareJID isEqualToJID:object.bareJid]) {
            [_dataArr removeObject:model];
        }
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(XMPPCoredataChatContact *)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    [self setupUnreadMessageCount];
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] withCategory:anObject];
            break;
            
        default:
            break;
    }
    
}



- (void)configureCell:(ChatListCell *)cell withCategory:(XMPPCoredataChatContact *)contact {
    
    ConversationModel *model = [self conversionModel:contact];;
    [cell setChatListCellData:model];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView beginUpdates];
}

-(void) controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView endUpdates];
    
}



- (ConversationModel *) conversionModel:(XMPPCoredataChatContact *)contact {
    ConversationModel *model = [ConversationModel mj_objectWithKeyValues:contact.contactString];
    if (!isEmptyString(model.conversation)) {
        model.bareJID = [XMPPJID jidWithString:model.conversation];
    }
    
    if (model.chatType == UUChatTypeChat) {
        [model getUserInfor:model.bareJID.user];
    }else if (model.chatType == UUChatTypeChatRoom){
        [model getGroupInfor:model.bareJID.user];
    }
    
    model.lastestDate = contact.mostRecentMessageTimestamp;
    model.chatType = [contact.bareJidStr containsString:XMMPP_BASESTR] ? UUChatTypeChat:UUChatTypeChatRoom;
    model.bareJID = [XMPPJID jidWithString:contact.bareJidStr];
    model.messageCount =[contact.messageCount integerValue];
    return model;
}




/**
 删除会话 删除列表数组
 
 @param model model description
 */
- (void) deletedDataWithContact:(ConversationModel *)model {
    
    [XMPPSignal deletedContactListWith:model.conversation];
    [XMPPSignal MR_deletedContactID:model.conversation];
    if (model.chatType == UUChatTypeChat) {
        [XMPPSignal deletedChatHistoryWithUserID:model.conversation];
    }else if(model.chatType ==UUChatTypeChatRoom){
        [XMPPSignal deleteGroupHistoryWithRoomID:model.conversation];
    }
    
}
#endif

- (void)isConnect:(BOOL)isConnect{
    if (!isConnect) {
        self.tableView.tableHeaderView = self.networkStateView;
    }
    else{
        self.tableView.tableHeaderView = nil;
    }
    
}

- (void)networkChanged:(EMConnectionState)connectionState
{
    if (connectionState == EMConnectionDisconnected) {
        self.tableView.tableHeaderView = self.networkStateView;
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        if (window) {
            
            [MBProgressHUD showMessage:@"网络已断开,请检查您的网络" view:window];
        }
    }
    else{
        self.tableView.tableHeaderView = nil;
    }
}

#pragma mark -popView delegate
- (void)menuClick:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }
            break;
        case 1:
        {
            // 添加朋友
            AddFriendsViewController *addVC = [[AddFriendsViewController alloc] init];
            addVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:addVC animated:YES];
        }
            break;
        case 2:
        {
            // 扫一扫
            [self qqStyle];
        }
            break;
        default:
            break;
    }
    [_popVC dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#ifdef XSIM_AVALABLE
    return self.resultController.fetchedObjects.count;
#endif
    
#ifdef EMIM_AVALABLE
    return self.dataArr.count;
#endif
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
#ifdef ALIYM_AVALABLE
    ConversationModel *model = _dataArr[indexPath.row];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:model.conversation];
    chatVC.title = model.title;
    chatVC.avatarUrl = model.avatarURLPath;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    ConversationModel *model = _dataArr[indexPath.row];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:model.conversation];
    chatVC.title = model.title;
    chatVC.avatarUrl = model.avatarURLPath;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    XMPPCoredataChatContact *contact = self.resultController.fetchedObjects[indexPath.row];
    ConversationModel *model = [self conversionModel:contact];
    if (isEmptyString(model.conversation)) {
        [MBProgressHUD showMessage:@"获取会话信息有误" view:self.view];
        return;
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    if (model.chatType == UUChatTypeChatRoom) {
        ConversationModel *conversionModel = [XMPPSignal roomConfigWithConversation:model];
        if (conversionModel) {
            XMGroupChatController *vc = [[XMGroupChatController alloc] initWithRoomJID:conversionModel];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            [XMPPSignal deleteGroupHistoryWithRoomID:model.xmppRoomManager.xmppRoom.roomJID.full];
            NSString *roomID;
            roomID = model.conversation;
            if ([self.dataArr containsObject:model]) {
                [self.dataArr removeObject:model];
                [self willReloadTableView];
                [XMPPSignal deletedContactListWith:roomID];
            }
        }
        
    }else if (model.chatType == UUChatTypeChat){
        XMChatController *chatVC = [XMChatController new];
        
        chatVC.conversationModel = [self conversionModel:contact];;
        chatVC.isBurn = [contact.isBurnMessage boolValue];
        [self.navigationController pushViewController:chatVC animated:YES];
    }
#endif
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"chatCell%ld",(long)indexPath.row];
    ChatListCell *cell = (ChatListCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
#ifdef XSIM_AVALABLE
    XMPPCoredataChatContact *contact  = self.resultController.fetchedObjects[indexPath.row];
    [cell setChatListCellData:[self conversionModel:contact]];
#else
    [cell setChatListCellData:_dataArr[indexPath.row]];
#endif
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - 添加索引
/*
 - (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
 {
 NSMutableArray *arr = [NSMutableArray arrayWithObjects:@"#",nil];
 for (char ch='A'; ch<='Z'; ch++) {
 if (ch=='I' || ch=='O' || ch=='U' || ch=='V')
 continue;
 [arr addObject:[NSString stringWithFormat:@"%c",ch]];
 }
 return arr;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
 {
 //这里是为了指定索引index对应的是哪个section的，默认的话直接返回index就好。其他需要定制的就针对性处理
 if ([title isEqualToString:UITableViewIndexSearch])
 {
 [tableView setContentOffset:CGPointZero animated:NO];//tabview移至顶部
 return NSNotFound;
 }
 else
 {
 return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index] - 1; // -1 添加了搜索标识
 }
 }
 */

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) wSelf = self;
    //添加一个删除按钮
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        NSLog(@"点击了删除");
#ifdef ALIYM_AVALABLE
        ConversationModel *model = _dataArr[indexPath.row];
        [[[SPKitExample sharedInstance].ywIMKit.IMCore getConversationService] removeConversationByConversationId:((YWConversation *)model.conversation).conversationId error:nil];
        [wSelf refreshConversationLists];
#elif defined EMIM_AVALABLE
        ConversationModel *model = _dataArr[indexPath.row];
        [[EMClient sharedClient].chatManager deleteConversation:((EMConversation *)model.conversation).conversationId isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError){
            //code
            [wSelf refreshConversationLists];
        }];
#else
        XMPPCoredataChatContact  *contact = self.resultController.fetchedObjects[indexPath.row];
        ConversationModel *model = [self conversionModel:contact];
        [self deletedDataWithContact:model];
#endif
        
    }];
    //删除按钮颜色
    deleteAction.backgroundColor = [UIColor hexFloatColor:@"f43531"];
    
    //添加一个已读按钮
    
    BOOL hasUnreadMes = NO;
#ifdef ALIYM_AVALABLE
    //    if ([((YWConversation *)model.conversation).conversationUnreadMessagesCount intValue] > 0 || !((YWConversation *)model.conversation).conversationLatestMessage.receiverHasReaded) {
    hasUnreadMes = YES;
    //    }
#elif defined EMIM_AVALABLE
    ConversationModel *model = _dataArr[indexPath.row];
    if (((EMConversation *)model.conversation).unreadMessagesCount > 0 || !((EMConversation *)model.conversation).latestMessage.isRead) {
        hasUnreadMes = YES;
    }
#else
    
#endif
    
    UITableViewRowAction *readAction =[UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:hasUnreadMes?@"标为已读":@"标为未读" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
#ifdef ALIYM_AVALABLE
        if (hasUnreadMes) {
            // 标为已读
            [((YWConversation *)model.conversation) markConversationAsRead];
            [self refreshConversationLists];
        } else {
            // 标为未读
            
        }
#elif defined EMIM_AVALABLE
        if (hasUnreadMes) {
            // 标为已读
            ((EMConversation *)model.conversation).latestMessage.isRead = YES;
            [((EMConversation *)model.conversation) updateMessageChange:((EMConversation *)model.conversation).latestMessage error:nil];
            [((EMConversation *)model.conversation) markAllMessagesAsRead:nil];
            [wSelf refreshConversationLists];
        } else {
            // 标为未读
            ((EMConversation *)model.conversation).latestMessage.isRead = NO;
            [((EMConversation *)model.conversation) updateMessageChange:((EMConversation *)model.conversation).latestMessage error:nil];
            [wSelf refreshConversationLists];
        }
#else
        
#endif
        
        
    }];
    //已读按钮颜色
    readAction.backgroundColor = [UIColor hexFloatColor:@"c7c7cc"];
    
    //将设置好的按钮方到数组中返回
#ifdef ALIYM_AVALABLE
    return @[deleteAction,readAction];
#elif defined EMIM_AVALABLE
    return @[deleteAction,readAction];
#else
    return @[deleteAction];
#endif
    
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}
#pragma mark - self cycle

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 9, mainWidth-24, 32)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"搜索联系人/群组") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 5;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -2.5, 0, 0)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2.5, 0, 0)];
    }
    return _searchBtn;
}

- (UIView *)networkStateView
{
    if (_networkStateView == nil) {
        _networkStateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
        _networkStateView.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:199 / 255.0 blue:199 / 255.0 alpha:0.5];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (_networkStateView.frame.size.height - 20) / 2, 20, 20)];
        imageView.image = [UIImage imageNamed:@"messageSendFail"];
        [_networkStateView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 5, 0, _networkStateView.frame.size.width - (CGRectGetMaxX(imageView.frame) + 15), _networkStateView.frame.size.height)];
        label.font = [UIFont systemFontOfSize:15.0];
        label.textColor = [UIColor grayColor];
        label.backgroundColor = [UIColor clearColor];
        label.text = @"网络连接失败，请检查网络设置";
        [_networkStateView addSubview:label];
    }
    
    return _networkStateView;
}
@end
