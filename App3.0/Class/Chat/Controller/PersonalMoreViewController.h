//
//  PersonalMoreViewController.h
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface PersonalMoreViewController : XSBaseTableViewController
@property (copy, nonatomic) UserInfoDataParser *userInfo;
@end
