//
//  InviteDetailViewController.h
//  App3.0
//
//  Created by mac on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface InviteDetailViewController : XSBaseTableViewController
@property (copy, nonatomic) NSString *invite_id;
@property (strong, nonatomic) id message;
@property (nonatomic,strong) XMPPRoom *xmppRoom;
@property (nonatomic,copy)NSString *messageID;
@end
