//
//  PersonalMoreViewController.m
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PersonalMoreViewController.h"
#import "ChatHelper.h"

@interface PersonalMoreViewController ()
{
    NSArray *_dataArr;
}
@end

@implementation PersonalMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"详细资料";
    _dataArr = @[@"设置备注名"];
    self.tableViewStyle = UITableViewStyleGrouped;
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //修改昵称
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"请输入备注名称" preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alert) weakAlert = alert;
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //获取输入的昵称
        NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);

        __block NSString *remarkName = [weakAlert.textFields.lastObject text];
        //提交昵称
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager setUserRemarkNameWithUid:self.userInfo.uid remarkName:[weakAlert.textFields.lastObject text] success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"修改成功"];
                cell.detailTextLabel.text = remarkName;
                self.userInfo.remark = remarkName;
                
                [ChatHelper shareHelper].xmchatVC.title = remarkName;
//                [[ChatHelper shareHelper] refreshChatTitle:remarkName];
                
                ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                cdParser.uid = self.userInfo.uid;
                cdParser.username = self.userInfo.username;
                cdParser.nickname = self.userInfo.nickname;
                cdParser.avatar = self.userInfo.logo;
                cdParser.relation = self.userInfo.relation;
                cdParser.remark = remarkName;
                cdParser.mobile = self.userInfo.mobile;
                [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            }
            
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
        }];
        
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"点击了取消按钮");
    }]];
    // 添加文本框
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
    }];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    static NSString *CellIdentifier = @"personal_remark";
    UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = _dataArr[row];
    }
    if (self.userInfo) {
        cell.detailTextLabel.text = self.userInfo.remark;
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)setUserInfo:(UserInfoDataParser *)userInfo
{
    _userInfo = userInfo;
    [self.tableView reloadData];
}

@end
