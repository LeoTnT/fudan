//
//  GroupInfoViewController.m
//  App3.0
//
//  Created by mac on 17/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupInfoViewController.h"
#import "GroupMemberCell.h"
#import "ClipViewViewController.h"
#import "LoginModel.h"
#import "UserInstance.h"
#import "GroupMemberManagerViewController.h"
#import "PersonalViewController.h"
#import "ChatHelper.h"
#import "GroupAllMembersViewController.h"
#import "PersonViewController.h"
#import "UUImageAvatarBrowser.h"
#import "GroupQRViewController.h"

@interface GroupInfoViewController ()< GroupMemberCellDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ClipViewControllerDelegate>
{
    NSArray *_dataArr;
    NSInteger _memberCount; //成员数
    NSString *_groupId;     //群组ID
    EMGroup *_mGroup;
//    GroupDataModel *_gData;
//    NSMutableArray *_memberArr;
    BOOL _flag;//是否是现拍的照片
}
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (assign, nonatomic) BOOL isShield;
@property (assign, nonatomic) BOOL isDisturb;
@property (strong, nonatomic) UIImageView *groupAvatar;
@property (strong, nonatomic) GroupDataModel *gData;
@end

@implementation GroupInfoViewController
- (instancetype)initWithGroupId:(NSString *)gId
{
    self = [super init];
    if (self) {
        _groupId = gId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    __weak typeof(self) wSelf= self;
    // 设置left nav item
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    _gData = nil;
    _memberCount = 0; // 测试数据
//    _memberArr = [NSMutableArray array];
    
//    _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群二维码",@"群聊名称",Localized(@"清空聊天记录")],@[@"群消息免打扰",@"屏蔽群消息"]];
    
    
    _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群聊名称",Localized(@"清空聊天记录")]];

    self.tableViewStyle = UITableViewStyleGrouped;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getGroupInfo];
}

- (void)getGroupInfo {
    // 是否免打扰和屏蔽
    NSArray *arr = [[DBHandler sharedInstance] getGroupByGroupId:_groupId];
    if (arr && arr.count > 0) {
        GroupDataModel *group = arr[0];
        self.isDisturb = [group.is_not_disturb integerValue];
        self.isShield = [group.is_shield integerValue];
        self.title = group.name;

    }
    
    @weakify(self);
    // 从服务器拉取群信息
    [HTTPManager fetchGroupInfoWithGroupId:_groupId getMembers:1 order:@"time" success:^(NSDictionary * dic, resultObject *state){
        if (state.status) {
            _gData = [GroupDataModel mj_objectWithKeyValues:dic[@"data"]];
            if ([_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
                // 是群主
                // cell标题数据来源
//                _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群二维码",@"群聊名称",Localized(@"清空聊天记录")],@[@"群消息免打扰",@"屏蔽群消息"],@"解散群聊"];
                _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群聊名称",Localized(@"清空聊天记录")],@"解散群聊"];

            } else {
                // cell标题数据来源
//                _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群二维码",@"群聊名称",Localized(@"清空聊天记录")],@[@"群消息免打扰",@"屏蔽群消息"],@"退出群聊"];
                _dataArr = @[@"",@[@"群头像",@"自由邀请",@"群聊名称",Localized(@"清空聊天记录")],@"退出群聊"];

            }
            
            @strongify(self);
            self.isDisturb = [_gData.is_not_disturb integerValue];
            self.isShield = [_gData.is_shield integerValue];
            [self.tableView reloadData];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                for (NickNameDataParser *nnd in _gData.members) {
                    [[DBHandler sharedInstance] addOrUpdateGroupMember:nnd];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
            
            self.title = _gData.name;
        } else {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)choosePhotos{
    //打开相册
    self.imagePickVC = [[UIImagePickerController alloc] init];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            self.imagePickVC.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePickVC.sourceType];
            //允许修改
            self.imagePickVC.allowsEditing = YES;
        }
        self.imagePickVC.delegate = self;
        self.imagePickVC.allowsEditing = NO;
        _flag = NO;
        [self presentViewController:self.imagePickVC animated:YES completion:nil];
        
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark-拍照
-(void)takeAPhoto{
    //拍照模式是否可用
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }else{
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
        _flag = YES;
    }];
}
#pragma mark-拍照完毕
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    if (_flag) {
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        ClipViewViewController * clipView = [[ClipViewViewController alloc]initWithImage:image];
        clipView.delegate = self;
        [picker pushViewController:clipView animated:YES];
    }else{
        
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"])
        {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            ClipViewViewController * clipView = [[ClipViewViewController alloc]initWithImage:image];
            clipView.delegate = self;
            [picker pushViewController:clipView animated:YES];
        }
    }
}

#pragma mark - ClipViewControllerDelegate

-(void)clipViewController:(ClipViewViewController *)clipViewController FinishClipImage:(UIImage *)editImage
{
    __weak typeof(self) wSelf = self;
    [clipViewController dismissViewControllerAnimated:YES completion:^{
        NSMutableString *tempStr= [NSMutableString string];
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager upLoadUserInformationWithIconImage:editImage progress:^(NSProgress * _Nonnull uploadProgress) {
            NSUInteger  pro =  1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
            if (pro==1) {
                
            }
            
        } success:^(NSDictionary * _Nullable dic, BOOL state) {
            [tempStr appendString:[NSString stringWithFormat:@"%@",dic[@"data"]]];
            [XSTool hideProgressHUDWithView:self.view];
            NSLog(@"+++++%@",tempStr);
            // 更新群信息
            GroupDataModel *parser = _gData;
            parser.avatar = tempStr;
            NSDictionary *params = @{@"id":parser.gid,@"avatar":parser.avatar};
            [HTTPManager updateGroupInfoWithParams:params success:^(NSDictionary * dic, resultObject *state) {
                if (state.status) {
                    _gData.avatar = tempStr;
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                    [wSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            } fail:^(NSError * _Nonnull error) {
                
            }];
        } failure:^(NSError * _Nonnull error) {
            
            NSLog(@"%@",error.description);
        }];
        
    }];
}

#pragma mark - data
- (void)setupGroupInfoData
{
    
}

- (void)popToView {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dismissAction:(UIButton *)pSender
{
    [XSTool showProgressHUDWithView:self.view];
    __weak typeof(self) wSelf = self;
    if ([_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
        // 是群主 解散群聊
        [HTTPManager deleteGroupWithGroupId:_gData.gid success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                // 删除成功
                
                [XSTool showToastWithView:self.view Text:Localized(@"del_success")];
                [[DBHandler sharedInstance] deleteGroupByGroupId:_gData.gid];
                [[XMPPManager sharedManager].deletedMessageSubject sendNext:RACTuplePack(@"deleteRoom",self.roomManager.xmppRoom)];
                [XMPPSignal deletedContactListWith:self.roomManager.xmppRoom.roomJID.full];
                [XMPPSignal deleteGroupHistoryWithRoomID:self.roomManager.xmppRoom.roomJID.full];
                
                if ([[XMPPManager sharedManager].roomList containsObject:self.roomManager]) {
                    [self.roomManager.xmppRoom leaveRoom];
                    [[XMPPManager sharedManager].roomList removeObject:self.roomManager];
                    [self.roomManager.xmppRoom removeDelegate:self.roomManager delegateQueue:dispatch_get_global_queue(0, 0)];
                }
                [wSelf performSelector:@selector(popToView) withObject:nil afterDelay:0.5f];
            } else{
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
        }];
    } else {
        // 是群员 退出群聊
        [HTTPManager deleteMembers:[UserInstance ShardInstnce].uid forGroup:_gData.gid success:^(NSDictionary * dic, resultObject *state) {
            if (state.status) {
                // 移除群成员成功
                [XSTool showToastWithView:self.view Text:@"您已退出群组"];
                [[DBHandler sharedInstance] deleteGroupByGroupId:_gData.gid];
                [[XMPPManager sharedManager].deletedMessageSubject sendNext:RACTuplePack(@"outRoom",self.roomManager.xmppRoom)];
                [XMPPSignal deletedContactListWith:self.roomManager.xmppRoom.roomJID.full];
                [XMPPSignal deleteGroupHistoryWithRoomID:self.roomManager.xmppRoom.roomJID.full];
                
                if ([[XMPPManager sharedManager].roomList containsObject:self.roomManager]) {
                    [self.roomManager.xmppRoom leaveRoom];
                    [[XMPPManager sharedManager].roomList removeObject:self.roomManager];
                    [self.roomManager.xmppRoom removeDelegate:self.roomManager delegateQueue:dispatch_get_global_queue(0, 0)];
                }
                
                [wSelf performSelector:@selector(popToView) withObject:nil afterDelay:0.5f];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            [XSTool hideProgressHUDWithView:self.view];
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
        }];
    }
}

// 免打扰设置
- (void)disturbAction:(UISwitch *)pSender{
    [XSTool showProgressHUDTOView:self.view withText:nil];
    if (pSender.on) {
        [HTTPManager setShield:-1 notDisturb:1 forGroup:_groupId success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.isDisturb = YES;
                [XSTool showToastWithView:self.view Text:@"开启免打扰"];
                [[DBHandler sharedInstance] updateGroupWithKey:@"disturb" value:@"1" byGid:_groupId];
            } else {
                self.isDisturb = NO;
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = NO;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
            pSender.on = NO;
            self.isDisturb = NO;
        }];
    } else {
        [HTTPManager setShield:-1 notDisturb:0 forGroup:_groupId success:^(NSDictionary * dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"关闭免打扰"];
                self.isDisturb = NO;
                [[DBHandler sharedInstance] updateGroupWithKey:@"disturb" value:@"0" byGid:_groupId];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                pSender.on = YES;
                self.isDisturb = YES;
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            [XSTool hideProgressHUDWithView:self.view];
            pSender.on = YES;
            self.isDisturb = YES;
        }];
    }
}

// 屏蔽设置
- (void)shieldAction:(UISwitch *)pSender {
    if ([_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
        [XSTool showToastWithView:self.view Text:@"群主不可以屏蔽群消息"];
        pSender.on = NO;
        self.isShield = NO;
        return;
    }
    [XSTool showProgressHUDTOView:self.view withText:nil];
    if (pSender.on) {
#ifdef ALIYM_AVALABLE
        YWTribe *tribe = [YWTribe new];
        tribe.tribeId = _groupId;
        [[[SPKitExample sharedInstance].ywIMKit.IMCore getSettingService] asyncSetMessageReceive:YWMessageFlagNotReceive ForTribe:tribe completion:^(NSError *aError, NSDictionary *aResult) {
            if (!aError) {
                [XSTool showToastWithView:self.view Text:@"您已屏蔽群消息"];
                self.isShield = YES;
                [[DBHandler sharedInstance] updateGroupWithKey:@"shield" value:@"1" byGid:_groupId];
            } else {
                [XSTool showToastWithView:self.view Text:aError.description];
                pSender.on = NO;
                self.isShield = NO;
            }
        }];
#elif defined EMIM_AVALABLE
        [[EMClient sharedClient].groupManager blockGroup:_groupId completion:^(EMGroup *aGroup, EMError *aError) {
            [XSTool hideProgressHUDWithView:self.view];
            if (!aError) {
                [XSTool showToastWithView:self.view Text:@"您已屏蔽群消息"];
                self.isShield = YES;
                [[DBHandler sharedInstance] updateGroupWithKey:@"shield" value:@"1" byGid:_groupId];
            } else {
                [XSTool showToastWithView:self.view Text:aError.errorDescription];
                pSender.on = NO;
                self.isShield = NO;
            }
        }];
#else
        [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
        
        
    } else {
#ifdef ALIYM_AVALABLE
        YWTribe *tribe = [YWTribe new];
        tribe.tribeId = _groupId;
        [[[SPKitExample sharedInstance].ywIMKit.IMCore getSettingService] asyncSetMessageReceive:YWMessageFlagReceive ForTribe:tribe completion:^(NSError *aError, NSDictionary *aResult) {
            if (!aError) {
                [XSTool showToastWithView:self.view Text:@"您已解除屏蔽"];
                self.isShield = NO;
                [[DBHandler sharedInstance] updateGroupWithKey:@"shield" value:@"0" byGid:_groupId];
            } else {
                [XSTool showToastWithView:self.view Text:aError.description];
                pSender.on = YES;
                self.isShield = YES;
            }
        }];
#elif defined EMIM_AVALABLE
        [[EMClient sharedClient].groupManager unblockGroup:_groupId completion:^(EMGroup *aGroup, EMError *aError) {
            [XSTool hideProgressHUDWithView:self.view];
            if (!aError) {
                [XSTool showToastWithView:self.view Text:@"您已解除屏蔽"];
                self.isShield = NO;
                [[DBHandler sharedInstance] updateGroupWithKey:@"shield" value:@"0" byGid:_groupId];
            } else {
                [XSTool showToastWithView:self.view Text:aError.errorDescription];
                pSender.on = YES;
                self.isShield = YES;
            }
        }];
#else
        [XSTool showToastWithView:self.view Text:@"暂不支持"];
#endif
        
    }
}

#pragma mark - groupMemberCellDelegate
- (void)groupMemberClickOfIndex:(NSString *)indexString
{
    if ([indexString isEqualToString:@"add"]) {
        NSLog(@"++++++");
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *array = [[DBHandler sharedInstance] getAllContact];
        for (ContactDataParser *model in array) {
            BOOL has = NO;
            for (NickNameDataParser *nParser in _gData.members) {
                if ([model.uid isEqualToString:nParser.uid]) {
                    has = YES;
                    break;
                }
            }
            if (!has) {
                [tempArr addObject:model];
            }
        }
        GroupMemberManagerViewController *gmVC = [[GroupMemberManagerViewController alloc] initWithGroupId:_gData.gid memberArray:tempArr];
        gmVC.gmType = GMManagerAdd;
        gmVC.roomManager= self.roomManager;
        [self.navigationController pushViewController:gmVC animated:YES];
    } else if ([indexString isEqualToString:@"del"]) {
        NSLog(@"--------");
        NSMutableArray *tempArr = [NSMutableArray array];
        for (NickNameDataParser *nParser in _gData.members) {
            if (![nParser.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
                ContactDataParser *parser = [[ContactDataParser alloc] init];
                parser.uid = nParser.uid;
                parser.avatar = nParser.logo;
                parser.nickname = nParser.nickname;
                [tempArr addObject:parser];
            }
            
        }
        GroupMemberManagerViewController *gmVC = [[GroupMemberManagerViewController alloc] initWithGroupId:_gData.gid memberArray:tempArr];
        gmVC.gmType = GMManagerSub;
        gmVC.roomManager= self.roomManager;
        [self.navigationController pushViewController:gmVC animated:YES];
    } else {
        if ([indexString isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:indexString];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    }
}

- (void)checkAllClick
{
    GroupAllMembersViewController *gaVC = [[GroupAllMembersViewController alloc] init];
    gaVC.dataSource = _gData;
    gaVC.groupId = _gData.gid;
    gaVC.isGroupOwner = [_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]?YES :NO;
    [self.navigationController pushViewController:gaVC animated:YES];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1 ) {
//        if (section == 1 || section == 2) {
        NSArray *arr = _dataArr[section];
        return arr.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (!_gData) {
            return 0;
        }
        if ([_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
            return 40+(mainWidth/5+20)*(1+(_gData.members.count+1)/5); // + -
        } else {
            if (_gData.allowinvites) {
                return 40+(mainWidth/5+20)*(1+(_gData.members.count)/5); // +
            } else {
                return 40+(mainWidth/5+20)*(1+(_gData.members.count-1)/5); //无
            }
            
        }
        
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        return 65;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 3) {
        return 20;
    }
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        switch (indexPath.row) {
            case 0:
            {
//                [self choosePhotos];
            }
                break;
            case 1:
            {
                // 自由邀请设置
//                [XSTool showToastWithView:self.view Text:@"该功能暂未开通"];
//                break;
                if (![_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
                    [XSTool showToastWithView:self.view Text:@"只有群主可以更改设置"];
                    break;
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否允许自由邀请" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                [alert addAction:[UIAlertAction actionWithTitle:@"仅群主可以邀请" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [XSTool showToastWithView:self.view Text:@"设置中"];
                    NSDictionary *params = @{@"id":_gData.gid,@"allowinvites":@(0)};
                    [HTTPManager updateGroupInfoWithParams:params success:^(NSDictionary * dic, resultObject *state) {
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"修改成功"];
                            _gData.allowinvites = 0;
                            [[DBHandler sharedInstance] addOrUpdateGroup:_gData];
                            //刷新表格
                            cell.detailTextLabel.text = @"仅群主可以邀请";
                        } else {
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError * _Nonnull error) {
                        [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
                    }];
                    
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"邀请需群主审核" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [XSTool showToastWithView:self.view Text:@"设置中"];
                    NSDictionary *params = @{@"id":_gData.gid,@"allowinvites":@(1),@"invite_need_confirm":@(1)};
                    [HTTPManager updateGroupInfoWithParams:params success:^(NSDictionary * dic, resultObject *state) {
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"修改成功"];
                            _gData.allowinvites = 1;
                            _gData.invite_need_confirm = 1;
                            [[DBHandler sharedInstance] addOrUpdateGroup:_gData];
                            //刷新表格
                            cell.detailTextLabel.text = @"邀请需群主审核";
                        } else {
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError * _Nonnull error) {
                        [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
                    }];
                    
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"自由入群" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [XSTool showToastWithView:self.view Text:@"设置中"];
                    NSDictionary *params = @{@"id":_gData.gid,@"allowinvites":@(1),@"invite_need_confirm":@(0),@"allowapply":@(1)};
                    [HTTPManager updateGroupInfoWithParams:params success:^(NSDictionary * dic, resultObject *state) {
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"修改成功"];
                            _gData.allowinvites = 1;
                            _gData.invite_need_confirm = 0;
                            _gData.allowapply = 1;
                            [[DBHandler sharedInstance] addOrUpdateGroup:_gData];
                            //刷新表格
                            cell.detailTextLabel.text = @"自由入群";
                        } else {
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError * _Nonnull error) {
                        [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
                    }];
                    
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil]];
                
                // 弹出对话框
                [self presentViewController:alert animated:true completion:nil];
                 
            }
                break;
//            case 2:
//            {
//                // 群二维码
//                GroupQRViewController *vc = [[GroupQRViewController alloc] init];
//                self.gData.in_group = 1;
//                vc.groupModel = self.gData;
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//                break;
            case 2:
            {
                //修改昵称
                if (![_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]) {
                    [XSTool showToastWithView:self.view Text:@"只有群主可以修改群名称"];
                    break;
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"请输入群名称" preferredStyle:UIAlertControllerStyleAlert];
                //添加按钮
                __weak typeof(alert) weakAlert = alert;
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                    //获取输入的昵称
                    NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
                    
                    
                    //提交昵称
                    _gData.name = [weakAlert.textFields.lastObject text];
                    [XSTool showToastWithView:self.view Text:@"设置中"];
                    NSDictionary *params = @{@"id":_gData.gid,@"name":_gData.name};
                    [HTTPManager updateGroupInfoWithParams:params success:^(NSDictionary * dic, resultObject *state) {
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"修改成功"];
                            [ChatHelper shareHelper].chatVC.title = _gData.name;
                            [ChatHelper shareHelper].xmchatVC.title = _gData.name;
                            [[DBHandler sharedInstance] addOrUpdateGroup:_gData];
                            //刷新表格
                            cell.detailTextLabel.text = _gData.name;
                            self.title = _gData.name;

                        } else {
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError * _Nonnull error) {
                        [XSTool showToastWithView:self.view Text:@"修改失败，请重试"];
                    }];
                    
                    
                    
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    NSLog(@"点击了取消按钮");
                }]];
                
                // 添加文本框
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    
                }];
                // 弹出对话框
                [self presentViewController:alert animated:true completion:nil];
            }
                break;
            case 3:
            {
                // 清空聊天记录
                __weak typeof(self) wSelf = self;
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定删除聊天记录？" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
#ifdef ALIYM_AVALABLE
                    YWTribe *tribe = [YWTribe new];
                    tribe.tribeId = _groupId;
                    YWTribeConversation *conversation = [YWTribeConversation fetchConversationByTribe:tribe createIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
                    [conversation removeAllLocalMessages];
#elif defined EMIM_AVALABLE
                    [[EMClient sharedClient].chatManager deleteConversation:_groupId isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError) {
                        [wSelf.navigationController popToRootViewControllerAnimated:YES];
                    }];
#else
                    [XMPPSignal deleteGroupHistoryWithRoomID:getRoomString(_groupId)];
#endif
                    
                    
                }];
                UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:action1];
                [alert addAction:action2];
                [self presentViewController:alert animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat leftSpace = 10;
    CGFloat topSpace = 12;
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    if (section == 1) {
        NSString *CellIdentifier = @"group_cell";
        UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (row == 0) {
                topSpace = 30;
                self.groupAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-12-50, 7.5, 50, 50)];
                [self.groupAvatar getImageWithUrlStr:_gData.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                self.groupAvatar.layer.masksToBounds = YES;
                self.groupAvatar.layer.cornerRadius = 5;
                self.groupAvatar.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
                    [UUImageAvatarBrowser showImage:self.groupAvatar title:@""];
                }];
                [self.groupAvatar addGestureRecognizer:tap];
                [cell addSubview:self.groupAvatar];
                
            }
            //设置内容
            cell.textLabel.text = _dataArr[section][row];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
            if (row > 0) {
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, 0, mainWidth-leftSpace, 0.5)];
                lineView.backgroundColor = LINE_COLOR_NORMAL;
                [cell addSubview:lineView];
                if (row == 1) {
                    cell.detailTextLabel.text = @"";
                } else if (row == 3) {
                    if (_gData != nil) {
                        cell.detailTextLabel.text = _gData.name;
                    }
                    
                }
                cell.detailTextLabel.textColor = mainGrayColor;
            }
        } else {
            if (row == 0) {
                if (_gData != nil) {
                    [self.groupAvatar getImageWithUrlStr:_gData.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
                }
            }
            if (row > 0) {
                if (row == 1) {
                    if (_gData.allowinvites) {
                        if (_gData.invite_need_confirm) {
                            cell.detailTextLabel.text = @"邀请需群主审核";
                        } else {
                            cell.detailTextLabel.text = @"自由入群";
                        }
                    } else {
                        cell.detailTextLabel.text = @"仅群主可以邀请";
                    }
                } else if (row == 2) {
                    if (_gData != nil) {
                        cell.detailTextLabel.text = _gData.name;
                    }
                }else{
                    cell.detailTextLabel.text =@"";
                }
            }
        }
        
        return cell;
    }
//    else if (section == 2) {
//        NSString *CellIdentifier = @"group_cell";
//        UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
//        if(cell == nil)
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//            //设置内容
//            cell.textLabel.text = _dataArr[section][row];
//            cell.textLabel.font = [UIFont systemFontOfSize:14];
//            cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
//            if (row == 0) {
//                UISwitch *swith = [[UISwitch alloc] initWithFrame:CGRectMake(mainWidth-leftSpace-60, 7, 60, 30)];
//                swith.on = self.isDisturb;
//                [swith addTarget:self action:@selector(disturbAction:) forControlEvents:UIControlEventValueChanged];
//                [cell addSubview:swith];
//            }
//            if (row == 1) {
//                UISwitch *swith = [[UISwitch alloc] initWithFrame:CGRectMake(mainWidth-leftSpace-60, 7, 60, 30)];
//                swith.on = self.isShield;
//                [swith addTarget:self action:@selector(shieldAction:) forControlEvents:UIControlEventValueChanged];
//                [cell addSubview:swith];
//            }
//            return cell;
//        }
//
//        return cell;
//    }
    else if (section == 2) {
        NSString *CellIdentifier = @"group_cell";
        UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            XSCustomButton *btn = [[XSCustomButton alloc] initWithFrame:CGRectMake(20, 0, mainWidth-40, 44) title:_dataArr[section] titleColor:[UIColor whiteColor] fontSize:15 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
            [btn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
            [btn addTarget:self action:@selector(dismissAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }
        return cell;
    }
    static NSString *CellIdentifier = @"personal_info";
    GroupMemberCell *cell = (GroupMemberCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[GroupMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    if (_gData != nil) {
        cell.isGroupOwner = [_gData.owner isEqualToString:[UserInstance ShardInstnce].uid]?YES :NO;
        [cell setupMembersWithData:_gData];
    }
   
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

@end
