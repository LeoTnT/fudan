//
//  GroupMemberManagerViewController.h
//  App3.0
//
//  Created by mac on 17/3/30.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef NS_ENUM(NSInteger, GMManagerType) {
    GMManagerAdd = 1,
    GMManagerSub
};

@interface GroupMemberManagerViewController : XSBaseTableViewController
- (instancetype)initWithGroupId:(NSString *)gId memberArray:(NSArray *)mArr;

@property (nonatomic ,strong)XMPPRoomManager *roomManager;

@property (nonatomic) GMManagerType gmType;
@end
