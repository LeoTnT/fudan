//
//  ChatViewController.h
//  App3.0
//
//  Created by mac on 17/3/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RedPacketDetailModel;

@interface ChatViewController : XSBaseTableViewController
- (id)initWithConversation:(EMConversation *)conversation;
@property (strong, nonatomic) EMConversation *conversation;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *avatarUrl;

/*!
 @property
 @brief 页面是否处于显示状态
 */
@property (nonatomic) BOOL isViewDidAppear;

- (void)openRedPacketWithId:(NSString *)rpId;
- (void)detailRedPacketWithId:(NSString *)rpId;
@end
