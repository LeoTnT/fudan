//
//  UUMessage.m
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-26.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUMessage.h"
#import "NSDate+Utils.h"

@implementation UUMessageShareModel
@end

@implementation UUMessageSystemPushModel
@end

@implementation UUMessageProductShareModel
@end

@implementation UUMessageSystemNoticeModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"ext" : @"NSDictionary"
             };
}
@end

@implementation UUMessage
- (void)setWithDict:(NSDictionary *)dict{

    self.messageId = dict[@"messageId"];
    self.strIcon = dict[@"strIcon"];
    self.strName = dict[@"strName"];
    self.strId = dict[@"strId"];
    self.strTime = [self changeTheDateString:dict[@"strTime"]];
    self.from = [dict[@"from"] integerValue];
    self.status = [dict[@"status"] integerValue];
    self.chatType = [dict[@"chatType"] integerValue];
    self.emMessage = dict[@"emMessage"];
    self.strNotice = dict[@"strNotice"];
    if ([dict.allKeys containsObject:@"extType"]) {
        self.extType = dict[@"extType"];
        self.ext = dict[@"ext"];
    }
    switch ([dict[@"type"] integerValue]) {

        case UUMessageTypeText:
            self.type = UUMessageTypeText;
            self.strContent = dict[@"strContent"];
            break;
        case UUMessageTypePicture:
            self.type = UUMessageTypePicture;
            self.picture = dict[@"picture"];
            break;
        case UUMessageTypeVideo:
            self.type = UUMessageTypeVideo;
            self.picture = dict[@"picture"];
            self.videoTime = dict[@"videoTime"];
            break;
        case UUMessageTypeVoice:
            self.type = UUMessageTypeVoice;
            self.voice = dict[@"voice"];
            self.voiceUrl = dict[@"voiceUrl"];
            self.strVoiceTime = dict[@"strVoiceTime"];
            break;
        case UUMessageTypeLocation:
            self.type = UUMessageTypeLocation;
            self.strLocation = dict[@"strLocation"];
            self.strLatitude = dict[@"strLatitude"];
            self.strLongitude = dict[@"strLongitude"];
            break;
        case UUMessageTypeRedPacket:
            self.type = UUMessageTypeRedPacket;
            self.rpId = dict[@"rpId"];
            self.strRedMessage = dict[@"strRedMessage"];
            break;
        case UUMessageTypeTransfer:
            self.type = UUMessageTypeTransfer;
            self.rpId = dict[@"rpId"];
            self.strRedMessage = dict[@"strRedMessage"];
            self.moneyStr = dict[@"moneyStr"];
            break;
        case UUMessageTypeShare:
            self.type = UUMessageTypeShare;
            self.shareJosnString = dict[@"shareJosnString"];
            break;
        case UUMessageTypeProduct:
            self.type = UUMessageTypeProduct;
            self.shareJosnString = dict[@"shareJosnString"];
            break;
        case UUMessageTypeProductShare:
            self.type = UUMessageTypeProductShare;
            self.shareJosnString = dict[@"shareJosnString"];
            break;
        case UUMessageTypeFile:
            self.type = UUMessageTypeFile;
            self.fileTitle = dict[@"fileTitle"];
            self.fileDesc = dict[@"fileDesc"];
            break;
        case UUMessageTypeSystemPush:
            self.type = UUMessageTypeSystemPush;
            self.shareJosnString = dict[@"shareJosnString"];
            break;
        case UUMessageTypeCall:
            self.type = UUMessageTypeCall;
            self.isVideo = [dict[@"isVideo"] boolValue];
            self.strContent = dict[@"strContent"];
            break;
        case UUMessageTypeCard:
            self.type = UUMessageTypeCard;
            self.cardId = dict[@"cardId"];
            self.cardIcon = dict[@"cardIcon"];
            self.cardName = dict[@"cardName"];
            break;
        default:
            break;
    }
}


- (void)downloadVoiceWith:(NSURL*)url finash:(void(^)(NSURL *url))finash{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSString *cachePath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
        
        NSString *fileName=[cachePath stringByAppendingPathComponent:response.suggestedFilename];
        
        return [NSURL fileURLWithPath:fileName];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        XMPP_LOG(@" _ _ _ _ _File downloaded to: %@", filePath);
        if (finash) {
            finash(filePath);
        }
        //                    [XMPPSignal deletedFileWith:response.suggestedFilename];
    }];
    [downloadTask resume];
}

- (NSString *)noticeStringConvertByMessage:(XMPPMessage *)message ext:(NSDictionary *)ext {
    NSString *str = @"";
    if (!ext) {
        return @"";
    }
    BOOL myMes = NO;
    if (!self.from) {
        myMes = YES;
    }
    if ([ext[MSG_TYPE] isEqualToString:REVOKE_ACTION]) {
        if (myMes) {
            str = @"你撤回了一条消息";
        } else {
            
            str = [NSString stringWithFormat:@"%@撤回了一条消息",[[ChatHelper shareHelper] displayNameByUid:message.xm_messageUser]];
        }
    } else if ([ext[MSG_TYPE] isEqualToString:READ_PACKET_BACK]) {
        if (self.chatType == UUChatTypeChat) {
            if (myMes) {
                str = [NSString stringWithFormat:@"你领取了%@的红包",[[ChatHelper shareHelper] displayNameByUid:message.xm_messageUser]];
            } else {
                str = [NSString stringWithFormat:@"%@领取了你的红包",[[ChatHelper shareHelper] displayNameByUid:message.xm_messageUser]];
            }
        } else {
            NSDictionary *dic = [message.body mj_JSONObject];
            XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
            if (myMes) {
                
                str = [NSString stringWithFormat:@"你%@",roomMessage.body];
            } else {
                // 谁领取了我的红包只能自己看见
                if ([ext[MESSAGE_PACK_FROM] isEqualToString:[UserInstance ShardInstnce].uid]) {
                    str = [NSString stringWithFormat:@"%@%@",[[ChatHelper shareHelper] displayNameByUid:message.xm_messageUser],roomMessage.body];
                } else {
                    str = @"";
                }
            }
        }
    } else if ([ext[MSG_TYPE] isEqualToString:MESSAGE_FOCUS_TYPE]) {
        if (myMes) {
            if ([message.toStr containsString:@"@"]) {
                
                NSString *uid = [message.toStr componentsSeparatedByString:@"@"].firstObject;
                str = [NSString stringWithFormat:@"你添加了%@,快来聊天吧",[[ChatHelper shareHelper] displayNameByUid:uid]];
            } else {
                str = [NSString stringWithFormat:@"你添加了%@,快来聊天吧",[[ChatHelper shareHelper] displayNameByUid:message.toStr]];
                
            }
        } else {

            str = [NSString stringWithFormat:@"%@添加了您为好友, 快来聊天吧。",[[ChatHelper shareHelper] displayNameByUid:message.xm_messageUser]];

        }
    } else if ([ext[MSG_TYPE] isEqualToString:GROUP_INVITE_NEED_CONFIRM] || [ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
        if ([ext[GROUP_INVITE_OWNER] isEqualToString:[UserInstance ShardInstnce].uid]) {
            // 是群主
            NSDictionary *dic = [message.body mj_JSONObject];
            XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
            
            str = [NSString stringWithFormat:@"%@ 去确认",roomMessage.body];
            
            if ([ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
                str = [NSString stringWithFormat:@"%@ 已确认",roomMessage.body];
            }
        } else {
            NSDictionary *dic = [message.body mj_JSONObject];
            XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
            
            if ([roomMessage.body containsString:[[UserInstance ShardInstnce] showName]]) {
                
                str = [roomMessage.body stringByReplacingOccurrencesOfString:[[UserInstance ShardInstnce] showName] withString:@"您"];
            }else{
                
                str = roomMessage.body;
            }
            
            
        }
        
    } else {
        if (self.chatType == UUChatTypeChat) {
            str = message.body;
        } else {
            NSDictionary *dic = [message.body mj_JSONObject];
            XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
            str = roomMessage.body;
        }
    }
    
    return str;
}

//"08-10 晚上08:09:41.0" ->
//"昨天 上午10:09"或者"2012-08-10 凌晨07:09"
- (NSString *)changeTheDateString:(NSString *)Str
{
    NSLog(@"str ==== %@",Str);
    NSString *subString = [Str substringWithRange:NSMakeRange(0, 19)];
    NSDate *lastDate = [NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
//	NSTimeZone *zone = [NSTimeZone systemTimeZone];
//	NSInteger interval = [zone secondsFromGMTForDate:lastDate];
//	lastDate = [lastDate dateByAddingTimeInterval:interval];
    
    NSString *dateStr;  //年月日
//    NSString *period;   //时间段
    NSString *hour;     //时
    
    if ([lastDate year]==[[NSDate date] year]) {
        NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
        if (days <= 2) {
            dateStr = [lastDate stringYearMonthDayCompareToday];
        }else{
            dateStr = [lastDate stringMonthDay];
        }
    }else{
        dateStr = [lastDate stringYearMonthDay];
    }
    
    
//    if ([lastDate hour]>=5 && [lastDate hour]<12) {
//        period = @"上午";
//        hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
//    }else if ([lastDate hour]>=12 && [lastDate hour]<=18){
//        period = @"下午";
//        hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]-12];
//    }else if ([lastDate hour]>18 && [lastDate hour]<=23){
//        period = @"晚上";
//        hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]-12];
//    }else{
//        period = Localized(@"凌晨");
//        hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
//    }
    hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
    return [NSString stringWithFormat:@"%@ %@:%02d",dateStr,hour,(int)[lastDate minute]];
//    return [NSString stringWithFormat:@"%@ %@ %@:%02d",dateStr,period,hour,(int)[lastDate minute]];
}

- (void)minuteOffSetStart:(NSString *)start end:(NSString *)end
{
    if (!start) {
        self.showDateLabel = YES;
        return;
    }
    
    NSString *subStart = [start substringWithRange:NSMakeRange(0, 19)];
    NSDate *startDate = [NSDate dateFromString:subStart withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *subEnd = [end substringWithRange:NSMakeRange(0, 19)];
    NSDate *endDate = [NSDate dateFromString:subEnd withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //这个是相隔的秒数
    NSTimeInterval timeInterval = [startDate timeIntervalSinceDate:endDate];
    
    //相距5分钟显示时间Label
    if (fabs (timeInterval) > 5*60) {
        self.showDateLabel = YES;
    }else{
        self.showDateLabel = NO;
    }
    
}
#pragma mark - 对于数据模型中缺少的、不能与任何键配对的属性的时候，系统会自动调用setValue:forUndefinedKey:这个方法，该方法默认的实现会引发一个NSUndefinedKeyExceptiony异常。
//-(void)setValue:(id)value forKey:(NSString *)key{
//
//}
@end
