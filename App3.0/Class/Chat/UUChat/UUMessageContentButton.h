//
//  UUMessageContentButton.h
//  BloodSugarForDoc
//
//  Created by shake on 14-8-27.
//  Copyright (c) 2014年 shake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UUMessage.h"

@protocol UUMessageContentButtonDelegate <NSObject>
@optional
- (void)contentCopy;
- (void)contentDelete;
- (void)contentTranspond;
- (void)contentRecall;
- (void)contentDetector;
@end

@interface UUMessageContentButton : UIButton

//bubble imgae
@property (nonatomic, strong) UIImageView *backImageView;

//address
@property (nonatomic, strong) UIImageView *locatBackView;
@property (nonatomic, strong) UILabel *address;

//redpacket
@property (nonatomic, strong) UIImageView *rpBackView;
@property (nonatomic, strong) UIImageView *rpIcon;
@property (nonatomic, strong) UILabel *rpMessgae;
@property (nonatomic, strong) UILabel *rpOpen;
@property (nonatomic, strong) UILabel *rpDesc;

//Transfer
@property (nonatomic, strong) UIImageView *transBackView;
@property (nonatomic, strong) UILabel *transMessgae;
@property (nonatomic, strong) UILabel *transMoney;
@property (nonatomic, strong) UILabel *transDesc;

//share
@property (nonatomic, strong) UIImageView *shareBackView;
@property (nonatomic, strong) UILabel *shareTitle;
@property (nonatomic, strong) UILabel *shareDesc;
@property (nonatomic, strong) UIImageView *shareLogo;

// product
@property (nonatomic, strong) UIImageView *productBackView;
@property (nonatomic, strong) UIButton *sendLinkBtn;

// product share
@property (nonatomic, strong) UIImageView *productShareBackView;

//file
@property (nonatomic, strong) UIImageView *fileBackView;
@property (nonatomic, strong) UIImageView *fileIcon;
@property (nonatomic, strong) UILabel *fileTitle;
@property (nonatomic, strong) UILabel *fileDesc;
@property (nonatomic, strong) UILabel *fileState;

//audio
@property (nonatomic, strong) UIView *voiceBackView;
@property (nonatomic, strong) UILabel *second;
@property (nonatomic, strong) UIImageView *voice;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;

// video
@property (nonatomic, strong) UIImageView *videoBackView;
@property (nonatomic, strong) UIImageView *videoIcon;
@property (nonatomic, strong) UILabel *videoTime;

// 图文推送消息模板
@property (nonatomic, strong) UIImageView *sysPushBackView;
@property (nonatomic, strong) UILabel *sysPushTitle;
@property (nonatomic, strong) UILabel *sysPushTime;
@property (nonatomic, strong) UILabel *sysPushDesc;
@property (nonatomic, strong) UIImageView *sysPushLogo;

/*
 普通系统消息模板
 */
@property (nonatomic, strong) UIImageView *sysNoticeBackView;
@property (nonatomic, strong) UIImageView *sysNoticeIcon;
@property (nonatomic, strong) UILabel *sysNoticeTypeTitle;
@property (nonatomic, strong) UILabel *sysNoticeTitle;
@property (nonatomic, strong) UILabel *sysNoticeSubTitle;
@property (nonatomic, strong) UIImageView *sysNoticeImage;
@property (nonatomic, strong) UILabel *sysNoticeLinkTitle;              //
@property (nonatomic, strong) UILabel *sysNoticeWalletType;             // 钱包类型
@property (nonatomic, strong) UILabel *sysNoticeWalletChange;           // 钱包变化
@property (nonatomic, strong) UUMessageSystemNoticeModel *noticeModel;  // 通知模型

/*
 名片模板
 */
@property (nonatomic, strong) UIImageView *cardBackView;
@property (nonatomic, strong) UIImageView *cardIcon;
@property (nonatomic, strong) UILabel *cardTitle;
@property (nonatomic, strong) UILabel *cardDesc;

@property (nonatomic, assign) BOOL isMyMessage;
@property (nonatomic, assign)id<UUMessageContentButtonDelegate>delegate;

- (void)benginLoadVoice;

- (void)didLoadVoice;

- (void)stopPlay;

- (void)setAllBackViewHidden;

@end
