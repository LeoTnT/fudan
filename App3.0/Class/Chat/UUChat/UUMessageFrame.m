//
//  UUMessageFrame.m
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-26.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUMessageFrame.h"
#import "UUMessage.h"
#import "NSString + LBD.h"

@implementation UUMessageFrame

- (CGSize)sizeLabelToFit:(NSAttributedString *)aString width:(CGFloat)width height:(CGFloat)height {
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    tempLabel.attributedText = aString;
    tempLabel.numberOfLines = 0;
    [tempLabel sizeToFit];
    CGSize size = tempLabel.frame.size;
    return size;
}

- (void)setMessage:(UUMessage *)message{
    
    _message = message;
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    
    // 1、计算时间的位置
    if (_showTime){
        CGFloat timeY = ChatMargin;
        CGSize size = CGSizeMake(300, 100); //设置一个行高上限
        NSDictionary *attribute = @{NSFontAttributeName: ChatTimeFont};
        CGSize timeSize = [_message.strTime boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil].size;
        
        CGFloat timeX = (screenW - timeSize.width) / 2;
        _timeF = CGRectMake(timeX-5, timeY, timeSize.width + 10, timeSize.height + ChatTimeMarginH);
    }
    
    // 计算通知栏位置
    if (_showNotice) {
        CGFloat noticeY = CGRectGetMaxY(_timeF) + ChatMargin;
        CGSize size = CGSizeMake(mainWidth-2*ChatIconWH, 999); //设置一个行高上限
        NSDictionary *attribute = @{NSFontAttributeName: ChatTimeFont};
        CGSize noticeSize = [_message.strNotice boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil].size;
        
        CGFloat noticeX = (screenW - noticeSize.width) / 2;
        _noticeF = CGRectMake(noticeX-5, noticeY, noticeSize.width+10, noticeSize.height + ChatTimeMarginH);
        _cellHeight = MAX(CGRectGetMaxY(_noticeF), 0)  + ChatMargin;
        return;
    }
    
    // 2、计算头像位置
    CGFloat iconX = ChatMargin;
    if (_message.from == UUMessageFromMe) {
        iconX = screenW - ChatMargin - ChatIconWH;
    }
    CGFloat iconY = CGRectGetMaxY(_timeF) + ChatMargin;
    _iconF = CGRectMake(iconX, iconY, ChatIconWH, ChatIconWH);
    
    // 3、计算ID位置
    _nameF = CGRectMake(CGRectGetMaxX(_iconF)+ChatMargin+2, iconY, ChatContentW, 20);
    
    // 4、计算内容位置
    CGFloat contentX = CGRectGetMaxX(_iconF)+2;
    CGFloat contentY = iconY;
    if (_message.chatType == UUChatTypeGroupChat && _message.from == UUMessageFromOther) {
        contentY = CGRectGetMaxY(_nameF);
    }
    //根据种类分
    CGSize contentSize;
    switch (_message.type) {
        case UUMessageTypeText:
        {
            CGSize size = CGSizeMake([_message.strName isEqualToString:@"robot"]?ChatSystemNoticeW:ChatContentW, CGFLOAT_MAX); //设置一个行高上限
//            NSDictionary *attribute = @{NSFontAttributeName: ChatContentFont};
//            contentSize = [[message.strContent changeToEmojiStringWithHaveReply:NO] boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
            contentSize = [self sizeLabelToFit:[message.strContent changeToEmojiStringWithHaveReply:NO] width:size.width height:size.height];
            if ([_message.strName isEqualToString:@"robot"]) {
                // 系统推送消息
                UUMessageSystemNoticeModel *model = [UUMessageSystemNoticeModel mj_objectWithKeyValues:message.strContent];
                if (model.ext.count > 0) {
                    // 金额变动
                    contentSize = CGSizeMake(ChatSystemNoticeW, 210+20);
                } else if (!isEmptyString(model.img)) {
                    // 订单
                    contentSize = CGSizeMake(ChatSystemNoticeW, 190+20);
                } else {
                    // 普通文本
                    contentSize = CGSizeMake(ChatSystemNoticeW, 150+20);
                }
                
            }
        }
            break;
        case UUMessageTypePicture:
            contentSize = CGSizeMake(ChatPicWH, ChatPicWH);
            break;
        case UUMessageTypeVideo:
            contentSize = CGSizeMake(ChatPicWH, ChatPicWH);
            break;
        case UUMessageTypeVoice:
            contentSize = CGSizeMake(80, 20);
            break;
        case UUMessageTypeLocation:
            contentSize = CGSizeMake(ChatContentW, ChatLocationPicWH);
            break;
        case UUMessageTypeRedPacket:
            contentSize = CGSizeMake(ChatRedPacketW, ChatRedPacketH);
            break;
        case UUMessageTypeTransfer:
            contentSize = CGSizeMake(ChatRedPacketW, ChatRedPacketH);
            break;
        case UUMessageTypeShare:
            contentSize = CGSizeMake(ChatShareW, ChatShareH);
            break;
        case UUMessageTypeProductShare:
            contentSize = CGSizeMake(ChatShareW, 60);
            break;
        case UUMessageTypeProduct:
            contentSize = CGSizeMake(mainWidth, 115);
            break;
        case UUMessageTypeFile:
            contentSize = CGSizeMake(ChatFileW, ChatFileH);
            break;
        case UUMessageTypeSystemPush:
            contentSize = CGSizeMake(ChatSystemPushW, ChatSystemPushH);
            break;
        case UUMessageTypeCall:
        {
            CGSize size = CGSizeMake(ChatContentW, CGFLOAT_MAX); //设置一个行高上限
            CGSize tempSize = [[message.strContent changeToEmojiStringWithHaveReply:NO] boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
            contentSize = CGSizeMake(tempSize.width+40, tempSize.height);
        }
            break;
        case UUMessageTypeCard:
        {
            contentSize = CGSizeMake(ChatRedPacketW, ChatRedPacketH);
        }
            break;
    }
    if (_message.from == UUMessageFromMe) {
        contentX = iconX - contentSize.width - ChatContentLeft - ChatContentRight - 2;
    }

    _contentF = CGRectMake(contentX, contentY, contentSize.width + ChatContentLeft + ChatContentRight, contentSize.height + ChatContentTop + ChatContentBottom);
    
    if (_message.type == UUMessageTypeProduct) {
        _iconF = CGRectZero;
        _nameF = CGRectZero;
        _contentF = CGRectMake(0, contentY, mainWidth, 115);
    }
    
    if ([_message.strName isEqualToString:@"robot"] || _message.type == UUMessageTypeSystemPush) {
        // 系统推送消息
        _iconF = CGRectZero;
        _contentF = CGRectMake(12, contentY, contentSize.width, contentSize.height);
    }
    
    _cellHeight = MAX(CGRectGetMaxY(_contentF), 0)  + ChatMargin;
    
    
    // 5、计算消息状态位置
    if (_message.from == UUMessageFromMe && _message.chatType == UUChatTypeChat) {
        _statusF = CGRectMake(CGRectGetMinX(_contentF)-40-5, CGRectGetMaxY(_contentF)-20, 40, 20);
        NSLog(@"%f,%f,%f,%f",_statusF.origin.x,_statusF.origin.y,_statusF.size.width,_statusF.size.height);
    }
    
}

@end
