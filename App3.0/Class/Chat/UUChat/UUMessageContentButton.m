//
//  UUMessageContentButton.m
//  BloodSugarForDoc
//
//  Created by shake on 14-8-27.
//  Copyright (c) 2014年 shake. All rights reserved.
//

#import "UUMessageContentButton.h"
@implementation UUMessageContentButton

#pragma mark - lazy
- (UIImageView *)cardBackView {
    if (!_cardBackView) {
        _cardBackView = [UIImageView new];
        _cardBackView.contentMode = UIViewContentModeScaleAspectFill;
        _cardBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_cardBackView];
        
        _cardIcon = [UIImageView new];
        [_cardBackView addSubview:_cardIcon];
        
        _cardTitle = [UILabel new];
        _cardTitle.font = [UIFont systemFontOfSize:15];
        [_cardBackView addSubview:_cardTitle];
        
        _cardDesc = [UILabel new];
        _cardDesc.textColor = COLOR_999999;
        _cardDesc.font = [UIFont systemFontOfSize:13];
        [_cardBackView addSubview:_cardDesc];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = LINE_COLOR_NORMAL;
        [_cardBackView addSubview:lineView];
        
        UILabel *tips = [UILabel new];
        tips.text = @"个人名片";
        tips.textColor = COLOR_999999;
        tips.font = [UIFont systemFontOfSize:10];
        [_cardBackView addSubview:tips];
        
        [_cardBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
        }];
        [_cardIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cardBackView).offset(15);
            make.top.mas_equalTo(_cardBackView).offset(12);
            make.size.mas_equalTo(CGSizeMake(45, 50));
        }];
        [_cardTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cardIcon.mas_right).offset(12);
            make.top.mas_equalTo(_cardBackView).offset(20);
        }];
        [_cardDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cardTitle);
            make.top.mas_equalTo(_cardTitle.mas_bottom).offset(8);
        }];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(_cardBackView);
            make.bottom.mas_equalTo(_cardBackView).offset(-20);
            make.height.mas_equalTo(0.5);
        }];
        [tips mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_cardIcon);
            make.centerY.mas_equalTo(_cardBackView.mas_bottom).offset(-10);
        }];
    }
    return _cardBackView;
}

- (UIImageView *)sysNoticeBackView {
    if (!_sysNoticeBackView) {
        _sysNoticeBackView = [UIImageView new];
        _sysNoticeBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_sysNoticeBackView];
        
        self.sysNoticeIcon = [UIImageView new];
        self.sysNoticeIcon.layer.masksToBounds = YES;
        self.sysNoticeIcon.layer.cornerRadius = 3;
        [_sysNoticeBackView addSubview:self.sysNoticeIcon];
        
        self.sysNoticeTypeTitle = [UILabel new];
        self.sysNoticeTypeTitle.textColor = COLOR_666666;
        self.sysNoticeTypeTitle.font = [UIFont systemFontOfSize:16];
        [_sysNoticeBackView addSubview:self.sysNoticeTypeTitle];
        
        self.sysNoticeTitle = [UILabel new];
        self.sysNoticeTitle.font = [UIFont systemFontOfSize:14];
        self.sysNoticeTitle.numberOfLines = 4;
        [_sysNoticeBackView addSubview:self.sysNoticeTitle];
        
        self.sysNoticeSubTitle = [UILabel new];
        self.sysNoticeSubTitle.textColor = COLOR_999999;
        self.sysNoticeSubTitle.font = [UIFont systemFontOfSize:14];
        [_sysNoticeBackView addSubview:self.sysNoticeSubTitle];
        
        self.sysNoticeImage = [UIImageView new];
        [_sysNoticeBackView addSubview:self.sysNoticeImage];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = [UIColor hexFloatColor:@"E7E7E7"];
        [_sysNoticeBackView addSubview:lineView];
        
        self.sysNoticeLinkTitle = [UILabel new];
        self.sysNoticeLinkTitle.textColor = COLOR_666666;
        self.sysNoticeLinkTitle.font = [UIFont systemFontOfSize:14];
        [_sysNoticeBackView addSubview:self.sysNoticeLinkTitle];
        
        self.sysNoticeWalletType = [UILabel new];
        self.sysNoticeWalletType.textColor = COLOR_999999;
        self.sysNoticeWalletType.font = [UIFont systemFontOfSize:14];
        [_sysNoticeBackView addSubview:self.sysNoticeWalletType];
        
        self.sysNoticeWalletChange = [UILabel new];
        self.sysNoticeWalletChange.font = [UIFont systemFontOfSize:24];
        [_sysNoticeBackView addSubview:self.sysNoticeWalletChange];
        
        [_sysNoticeBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
        }];
        [self.sysNoticeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_sysNoticeBackView).offset(10);
            make.top.mas_equalTo(_sysNoticeBackView).offset(10);
            make.size.mas_equalTo(CGSizeMake(25, 25));
        }];
        [self.sysNoticeTypeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sysNoticeIcon.mas_right).offset(10);
            make.centerY.mas_equalTo(self.sysNoticeIcon);
        }];
        
        [self.sysNoticeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sysNoticeIcon);
            make.top.mas_equalTo(_sysNoticeBackView).offset(58);
            make.right.mas_lessThanOrEqualTo(self.sysNoticeImage.mas_left).offset(-10);
        }];
        [self.sysNoticeSubTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sysNoticeIcon);
            make.bottom.mas_equalTo(_sysNoticeBackView).offset(-55);
            make.right.mas_lessThanOrEqualTo(_sysNoticeBackView).offset(-12);
        }];
        [self.sysNoticeImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_sysNoticeBackView).offset(-10);
            make.top.mas_equalTo(self.sysNoticeTitle);
            make.size.mas_equalTo(CGSizeMake(60, 60));
        }];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_sysNoticeBackView).offset(10);
            make.right.mas_equalTo(_sysNoticeBackView).offset(-10);
            make.bottom.mas_equalTo(_sysNoticeBackView).offset(-44);
            make.height.mas_equalTo(0.5);
        }];
        [self.sysNoticeLinkTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_sysNoticeBackView).offset(-15);
            make.centerX.mas_equalTo(_sysNoticeBackView);
        }];
        [self.sysNoticeWalletType mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_sysNoticeBackView).offset(50);
            make.centerX.mas_equalTo(_sysNoticeBackView);
        }];
        [self.sysNoticeWalletChange mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sysNoticeWalletType.mas_bottom).offset(10);
            make.centerX.mas_equalTo(_sysNoticeBackView);
        }];
    }
    return _sysNoticeBackView;
}

- (void)setNoticeModel:(UUMessageSystemNoticeModel *)noticeModel {
    _noticeModel = noticeModel;
    [self.sysNoticeIcon getImageWithUrlStr:noticeModel.ico andDefaultImage:nil];
    self.sysNoticeTypeTitle.text = noticeModel.type_title;
    self.sysNoticeSubTitle.text = noticeModel.sub_title;
    self.sysNoticeLinkTitle.text = noticeModel.link_title;
    if (noticeModel.ext.count > 0) {
        // 有拓展内容（财务类型）
        self.sysNoticeTitle.hidden = YES;
        self.sysNoticeImage.hidden = YES;
        self.sysNoticeWalletType.hidden = NO;
        self.sysNoticeWalletChange.hidden = NO;
        self.sysNoticeWalletType.text = noticeModel.ext[0][@"_key"];
        self.sysNoticeWalletChange.text = noticeModel.ext[0][@"_value"];
    } else {
        self.sysNoticeTitle.hidden = NO;
        self.sysNoticeImage.hidden = NO;
        self.sysNoticeWalletType.hidden = YES;
        self.sysNoticeWalletChange.hidden = YES;
        self.sysNoticeTitle.text = noticeModel.title;
        if (isEmptyString(noticeModel.img)) {
            // 没有图片
            self.sysNoticeImage.hidden = YES;
            [self.sysNoticeTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.sysNoticeIcon);
                make.top.mas_equalTo(_sysNoticeBackView).offset(58);
                make.right.mas_lessThanOrEqualTo(_sysNoticeBackView).offset(-12);
            }];
            [self.sysNoticeSubTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.sysNoticeIcon);
                make.bottom.mas_equalTo(_sysNoticeBackView).offset(-55);
                make.right.mas_lessThanOrEqualTo(_sysNoticeBackView).offset(-12);
            }];
        } else {
            self.sysNoticeImage.hidden = NO;
            [self.sysNoticeImage getImageWithUrlStr:noticeModel.img andDefaultImage:nil];
            [self.sysNoticeTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.sysNoticeIcon);
                make.top.mas_equalTo(_sysNoticeBackView).offset(58);
                make.right.mas_lessThanOrEqualTo(self.sysNoticeImage.mas_left).offset(-10);
            }];
            [self.sysNoticeSubTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.sysNoticeIcon);
                make.bottom.mas_equalTo(_sysNoticeBackView).offset(-55);
                make.right.mas_lessThanOrEqualTo(self.sysNoticeImage.mas_left).offset(-10);
            }];
        }
    }
}

- (UIImageView *)rpBackView {
    if (!_rpBackView) {
        //红包消息
        _rpBackView = [[UIImageView alloc] init];
        _rpBackView.layer.cornerRadius = 3;
        _rpBackView.layer.masksToBounds  = YES;
        _rpBackView.contentMode = UIViewContentModeScaleAspectFill;
        _rpBackView.backgroundColor = BG_COLOR;
        [self addSubview:_rpBackView];
        
        self.rpIcon = [[UIImageView alloc]init];
        self.rpIcon.userInteractionEnabled = YES;
        [_rpBackView addSubview:self.rpIcon];
        
        self.rpMessgae = [[UILabel alloc] init];
        self.rpMessgae.textColor = [UIColor whiteColor];
        self.rpMessgae.font = [UIFont systemFontOfSize:16];
        [_rpBackView addSubview:self.rpMessgae];
        
        self.rpOpen = [[UILabel alloc] init];
        self.rpOpen.textColor = [UIColor whiteColor];
        self.rpOpen.font = [UIFont systemFontOfSize:14];
        [_rpBackView addSubview:self.rpOpen];
        
        self.rpDesc = [[UILabel alloc] init];
        self.rpDesc.textColor = mainGrayColor;
        self.rpDesc.font = [UIFont systemFontOfSize:12];
        [_rpBackView addSubview:self.rpDesc];
    }
    return _rpBackView;
}

- (UIImageView *)fileBackView {
    if (!_fileBackView) {
        //文件消息
        _fileBackView = [[UIImageView alloc]init];
        _fileBackView.layer.cornerRadius = 3;
        _fileBackView.layer.masksToBounds  = YES;
        _fileBackView.contentMode = UIViewContentModeScaleAspectFill;
        _fileBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_fileBackView];
        
        self.fileTitle = [[UILabel alloc] init];
        self.fileTitle.numberOfLines = 2;
        self.fileTitle.textColor = [UIColor blackColor];
        self.fileTitle.font = [UIFont systemFontOfSize:14];
        [_fileBackView addSubview:self.fileTitle];
        
        self.fileDesc = [[UILabel alloc] init];
        self.fileDesc.textColor = mainGrayColor;
        self.fileDesc.font = [UIFont systemFontOfSize:12];
        [_fileBackView addSubview:self.fileDesc];
        
        self.fileState = [[UILabel alloc] init];
        self.fileState.textColor = mainGrayColor;
        self.fileState.textAlignment = NSTextAlignmentRight;
        self.fileState.font = [UIFont systemFontOfSize:12];
        [_fileBackView addSubview:self.fileState];
        
        self.fileIcon = [[UIImageView alloc] init];
        self.fileIcon.image = [UIImage imageNamed:@"chat_item_file"];
        [_fileBackView addSubview:self.fileIcon];
        
//        [_fileBackView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.right.bottom.mas_equalTo(self);
//        }];
        [self.fileIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(_fileBackView).offset(10);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        [self.fileTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.fileIcon.mas_right).offset(5);
            make.top.mas_equalTo(self.fileIcon);
            make.right.mas_lessThanOrEqualTo(_fileBackView).offset(-10);
        }];
        [self.fileDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.fileIcon.mas_right).offset(5);
            make.bottom.mas_equalTo(self.fileIcon);
            make.right.mas_lessThanOrEqualTo(_fileBackView).offset(-10);
        }];
        [self.fileState mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.left.mas_equalTo(self.fileDesc.mas_right).offset(5);
            make.bottom.mas_equalTo(self.fileIcon);
            make.right.mas_equalTo(_fileBackView).offset(-10);
        }];
    }
    return _fileBackView;
}

- (UIImageView *)shareBackView {
    if (!_shareBackView) {
        //分享消息
        _shareBackView = [[UIImageView alloc]init];
        _shareBackView.layer.cornerRadius = 3;
        _shareBackView.layer.masksToBounds  = YES;
        _shareBackView.contentMode = UIViewContentModeScaleAspectFill;
        _shareBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_shareBackView];
        
        self.shareTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 200, 40)];
        self.shareTitle.numberOfLines = 2;
        self.shareTitle.textColor = [UIColor blackColor];
        self.shareTitle.font = [UIFont systemFontOfSize:14];
        [_shareBackView addSubview:self.shareTitle];
        
        self.shareDesc = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 150, 45)];
        self.shareDesc.numberOfLines = 3;
        self.shareDesc.textColor = mainGrayColor;
        self.shareDesc.font = [UIFont systemFontOfSize:12];
        [_shareBackView addSubview:self.shareDesc];
        
        self.shareLogo = [[UIImageView alloc] initWithFrame:CGRectMake(165, 50, 40, 40)];
        [_shareBackView addSubview:self.shareLogo];
    }
    return _shareBackView;
}

- (UIImageView *)productBackView {
    if (!_productBackView) {
        // 商品
        _productBackView = [[UIImageView alloc] init];
        _productBackView.contentMode = UIViewContentModeScaleAspectFill;
        _productBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_productBackView];
        
        self.shareLogo = [[UIImageView alloc] init];
        [_productBackView addSubview:self.shareLogo];
        [self.shareLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(12);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        self.shareTitle = [[UILabel alloc] init];
        self.shareTitle.numberOfLines = 2;
        self.shareTitle.textColor = Color(@"333333");
        self.shareTitle.font = [UIFont systemFontOfSize:12];
        [_productBackView addSubview:self.shareTitle];
        [self.shareTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.shareLogo.mas_right).offset(9);
            make.top.mas_equalTo(self.shareLogo);
            make.right.mas_lessThanOrEqualTo(_productBackView).offset(-12);
        }];
        
        self.shareDesc = [[UILabel alloc] init];
        self.shareDesc.textColor = [UIColor blackColor];
        self.shareDesc.font = [UIFont boldSystemFontOfSize:12];
        [_productBackView addSubview:self.shareDesc];
        [self.shareDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.shareTitle);
            make.top.mas_equalTo(self.shareTitle.mas_bottom).offset(6);
        }];
        
        self.sendLinkBtn = [BaseUITool buttonWithTitle:@"发送宝贝链接" titleColor:mainColor font:SYSTEM_FONT(12) superView:_productBackView];
        self.sendLinkBtn.layer.masksToBounds = YES;
        self.sendLinkBtn.layer.cornerRadius = 14;
        self.sendLinkBtn.layer.borderColor = mainColor.CGColor;
        self.sendLinkBtn.layer.borderWidth = 1;
        [_productBackView addSubview:self.sendLinkBtn];
        [self.sendLinkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_productBackView);
            make.bottom.mas_equalTo(-13);
            make.size.mas_equalTo(CGSizeMake(110, 28));
        }];
        
    }
    return _productBackView;
}

- (UIImageView *)productShareBackView {
    if (!_productShareBackView) {
        // 商品
        _productShareBackView = [[UIImageView alloc] init];
        _productShareBackView.contentMode = UIViewContentModeScaleAspectFill;
        _productShareBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_productShareBackView];
        
        self.shareLogo = [[UIImageView alloc] init];
        [_productShareBackView addSubview:self.shareLogo];
        [self.shareLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.centerY.mas_equalTo(_productShareBackView);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        self.shareTitle = [[UILabel alloc] init];
        self.shareTitle.numberOfLines = 2;
        self.shareTitle.textColor = Color(@"333333");
        self.shareTitle.font = [UIFont systemFontOfSize:12];
        [_productShareBackView addSubview:self.shareTitle];
        [self.shareTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.shareLogo.mas_right).offset(10);
            make.top.mas_equalTo(self.shareLogo);
            make.right.mas_lessThanOrEqualTo(_productShareBackView).offset(-20);
        }];
        
        self.shareDesc = [[UILabel alloc] init];
        self.shareDesc.textColor = [UIColor blackColor];
        self.shareDesc.font = [UIFont boldSystemFontOfSize:12];
        [_productShareBackView addSubview:self.shareDesc];
        [self.shareDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.shareTitle);
            make.top.mas_equalTo(self.shareTitle.mas_bottom).offset(6);
        }];
        
    }
    return _productShareBackView;
}

- (UIImageView *)sysPushBackView {
    if (!_sysPushBackView) {
        // 系统消息
        _sysPushBackView = [[UIImageView alloc]init];
        _sysPushBackView.layer.cornerRadius = 3;
        _sysPushBackView.layer.masksToBounds  = YES;
        _sysPushBackView.contentMode = UIViewContentModeScaleAspectFill;
        _sysPushBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_sysPushBackView];
        
        self.sysPushTime = [[UILabel alloc] init];
        self.sysPushTime.textColor = mainGrayColor;
        self.sysPushTime.font = [UIFont systemFontOfSize:12];
        [_sysPushBackView addSubview:self.sysPushTime];
        
        self.sysPushTitle = [[UILabel alloc] init];
        self.sysPushTitle.numberOfLines = 2;
        self.sysPushTitle.textColor = [UIColor blackColor];
        self.sysPushTitle.font = [UIFont systemFontOfSize:14];
        [_sysPushBackView addSubview:self.sysPushTitle];
        
        self.sysPushDesc = [[UILabel alloc] init];
        self.sysPushDesc.numberOfLines = 3;
        self.sysPushDesc.textColor = mainGrayColor;
        self.sysPushDesc.font = [UIFont systemFontOfSize:12];
        [_sysPushBackView addSubview:self.sysPushDesc];
        
        self.sysPushLogo = [[UIImageView alloc] init];
        self.sysPushLogo.contentMode = UIViewContentModeScaleAspectFill;
        self.sysPushLogo.clipsToBounds = YES;
        [_sysPushBackView addSubview:self.sysPushLogo];
        
        UILabel *sysPushCheck = [[UILabel alloc] init];
        sysPushCheck.text = @"查看全文";
        sysPushCheck.font = [UIFont systemFontOfSize:14];
        sysPushCheck.textColor = [UIColor blackColor];
        [_sysPushBackView addSubview:sysPushCheck];
        
        [_sysPushBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self);
        }];
        [self.sysPushTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_sysPushBackView).offset(10);
            make.left.mas_equalTo(_sysPushBackView).offset(15);
            make.right.lessThanOrEqualTo(_sysPushBackView).offset(-10);
        }];
        [self.sysPushTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sysPushTitle.mas_bottom).offset(10);
            make.left.mas_equalTo(self.sysPushTitle);
            make.right.lessThanOrEqualTo(_sysPushBackView).offset(-10);
        }];
        [self.sysPushLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sysPushTime.mas_bottom).offset(10);
            make.left.mas_equalTo(self.sysPushTitle);
            make.right.mas_equalTo(_sysPushBackView).offset(-10);
            make.height.mas_equalTo(150);
        }];
        [self.sysPushDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sysPushLogo.mas_bottom).offset(10);
            make.left.mas_equalTo(self.sysPushTitle);
            make.right.lessThanOrEqualTo(_sysPushBackView).offset(-10);
        }];
        [sysPushCheck mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_sysPushBackView).offset(-10);
            make.left.mas_equalTo(self.sysPushTitle);
        }];
    }
    return _sysPushBackView;
}

- (UIImageView *)videoBackView {
    if (!_videoBackView) {
        // 视频
        _videoBackView = [[UIImageView alloc]init];
        _videoBackView.layer.cornerRadius = 3;
        _videoBackView.layer.masksToBounds  = YES;
        _videoBackView.contentMode = UIViewContentModeScaleAspectFill;
        _videoBackView.backgroundColor = [UIColor blackColor];
        [self addSubview:_videoBackView];
        
        self.videoIcon = [[UIImageView alloc]init];
        self.videoIcon.image = [UIImage imageNamed:@"user_fans_play"];
        [_videoBackView addSubview:self.videoIcon];
        
        self.videoTime = [[UILabel alloc] init];
        self.videoTime.text = @"0:00";
        self.videoTime.textColor = [UIColor whiteColor];
        self.videoTime.font = [UIFont systemFontOfSize:12];
        [_videoBackView addSubview:self.videoTime];
        
        [_videoBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self);
        }];
        [self.videoIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(_videoBackView);
        }];
        [self.videoTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.mas_equalTo(_videoBackView).offset(-5);
        }];
    }
    
    return _videoBackView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5;
        //图片
        self.backImageView = [[UIImageView alloc]init];
        self.backImageView.layer.cornerRadius = 3;
        self.backImageView.layer.masksToBounds  = YES;
        self.backImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backImageView.backgroundColor = BG_COLOR;
        [self addSubview:self.backImageView];
        
        //位置
        self.locatBackView = [[UIImageView alloc]init];
        self.locatBackView.layer.cornerRadius = 3;
        self.locatBackView.layer.masksToBounds  = YES;
        self.locatBackView.contentMode = UIViewContentModeScaleAspectFit;
        self.locatBackView.backgroundColor = BG_COLOR;
        [self addSubview:self.locatBackView];
        
        self.address = [[UILabel alloc] init];
        self.address.text = @"";
        self.address.textAlignment = NSTextAlignmentLeft;
        self.address.textColor = [UIColor blackColor];
        self.address.font = [UIFont systemFontOfSize:14];
        self.address.numberOfLines = 2;
        [self.locatBackView addSubview:self.address];
        
        //语音
        self.voiceBackView = [[UIView alloc]init];
        [self addSubview:self.voiceBackView];
        self.second = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 40, 30)];
        self.second.textAlignment = NSTextAlignmentCenter;
        self.second.font = [UIFont systemFontOfSize:14];
        self.voice = [[UIImageView alloc]initWithFrame:CGRectMake(60, 5, 20, 20)];
        self.voice.image = [UIImage imageNamed:@"chat_animation_white3"];
        self.voice.animationImages = [NSArray arrayWithObjects:
                                      [UIImage imageNamed:@"chat_animation_white1"],
                                      [UIImage imageNamed:@"chat_animation_white2"],
                                      [UIImage imageNamed:@"chat_animation_white3"],nil];
        self.voice.animationDuration = 1;
        self.voice.animationRepeatCount = 0;
        self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicator.center=CGPointMake(60, 15);
        [self.voiceBackView addSubview:self.indicator];
        [self.voiceBackView addSubview:self.voice];
        [self.voiceBackView addSubview:self.second];
        
        self.voiceBackView.userInteractionEnabled = NO;
        self.second.userInteractionEnabled = NO;
        self.voice.userInteractionEnabled = NO;
        
        self.second.backgroundColor = [UIColor clearColor];
        self.voice.backgroundColor = [UIColor clearColor];
        self.voiceBackView.backgroundColor = [UIColor clearColor];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
        [self addGestureRecognizer:longPress];
    }
    return self;
}

- (void)setAllBackViewHidden {
    _voiceBackView.hidden = YES;
    _backImageView.hidden = YES;
    _locatBackView.hidden = YES;
    _rpBackView.hidden = YES;
    _shareBackView.hidden = YES;
    _fileBackView.hidden = YES;
    _videoBackView.hidden = YES;
    _sysPushBackView.hidden = YES;
    _cardBackView.hidden = YES;
    _productBackView.hidden = YES;
    _productShareBackView.hidden = YES;
}

- (void)benginLoadVoice
{
    self.voice.hidden = YES;
    [self.indicator startAnimating];
}
- (void)didLoadVoice
{
    self.voice.hidden = NO;
    [self.indicator stopAnimating];
    [self.voice startAnimating];
}
-(void)stopPlay
{
//    if(self.voice.isAnimating){
        [self.voice stopAnimating];
//    }
}

- (void)setIsMyMessage:(BOOL)isMyMessage
{
    _isMyMessage = isMyMessage;
    if (isMyMessage) {
        self.backImageView.frame = CGRectMake(5, 5, 220, 220);
        self.voiceBackView.frame = CGRectMake(0, 5, 120, 30);
        self.second.textColor = [UIColor whiteColor];
        self.second.frame = CGRectMake(20, 0, 40, 30);
        self.voice.frame = CGRectMake(80, 5, 20, 20);
        self.voice.image = [UIImage imageNamed:@"chat_animation_white3"];
        self.voice.animationImages = [NSArray arrayWithObjects:
                                      [UIImage imageNamed:@"chat_animation_white1"],
                                      [UIImage imageNamed:@"chat_animation_white2"],
                                      [UIImage imageNamed:@"chat_animation_white3"],nil];
    }else{
        self.backImageView.frame = CGRectMake(15, 5, 220, 220);
        self.voiceBackView.frame = CGRectMake(0, 5, 120, 30);
        self.second.textColor = [UIColor grayColor];
        self.second.frame = CGRectMake(60, 0, 40, 30);
        self.voice.frame = CGRectMake(20, 5, 20, 20);
        self.voice.image = [UIImage imageNamed:@"chat_animation3"];
        self.voice.animationImages = [NSArray arrayWithObjects:
                                      [UIImage imageNamed:@"chat_animation1"],
                                      [UIImage imageNamed:@"chat_animation2"],
                                      [UIImage imageNamed:@"chat_animation3"],nil];
    }
}

- (void)longPressAction:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan) {
        
        NSLog(@"tou tou tou");
        [self becomeFirstResponder];
        [self popMenu];
        
    }
    
}

- (void)popMenu {
    UIMenuItem * deleteMenuItem = [[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(deleteMenuAction:)];
    UIMenuItem * copyMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyMenuAction:)];
    UIMenuItem * transpondMenuItem = [[UIMenuItem alloc] initWithTitle:Localized(@"qr_send") action:@selector(transpondMenuAction:)];
    UIMenuItem * recallMenuItem = [[UIMenuItem alloc] initWithTitle:@"撤回" action:@selector(recallMenuAction:)];
    UIMenuItem * detectorMenuItem = [[UIMenuItem alloc] initWithTitle:@"识别二维码" action:@selector(detectorMenuAction:)];
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    NSMutableArray *items;
    items = [@[deleteMenuItem, transpondMenuItem] mutableCopy];
    [menuController setMenuItems:items];
    [menuController setTargetRect:self.frame inView:self.superview];
    [menuController setMenuVisible:YES animated:YES];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(deleteMenuAction:) || action == @selector(copyMenuAction:) || action == @selector(transpondMenuAction:) || action == @selector(recallMenuAction:) || action == @selector(detectorMenuAction:)) {
        return YES;
    }
    return NO;
}

-(void)copyMenuAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(contentCopy)])  {
        [self.delegate contentCopy];
    }
}

-(void)deleteMenuAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(contentDelete)])  {
        [self.delegate contentDelete];
    }
}

-(void)transpondMenuAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(contentTranspond)])  {
        [self.delegate contentTranspond];
    }
}

-(void)recallMenuAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(contentRecall)])  {
        [self.delegate contentRecall];
    }
}

-(void)detectorMenuAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(contentDetector)])  {
        [self.delegate contentDetector];
    }
}

@end
