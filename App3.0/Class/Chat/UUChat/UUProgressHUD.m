//
//  UUProgressHUD.m
//  1111
//
//  Created by shake on 14-8-6.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUProgressHUD.h"

@interface UUProgressHUD ()
{
    NSTimer *myTimer;
    int angle;
    
    UILabel *centerLabel;
    UIImageView *edgeImageView;
    UIImageView *speakImageView;
    UIImageView *recoderImageView;
    UIImageView *cancelImageView;
    UIImageView *shortImageView;
}
@property (nonatomic, strong, readonly) UIWindow *overlayWindow;

@end

@implementation UUProgressHUD

@synthesize overlayWindow;

+ (UUProgressHUD*)sharedView {
    static dispatch_once_t once;
    static UUProgressHUD *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[UUProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        sharedView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    });
    return sharedView;
}

+ (void)show {
    [[UUProgressHUD sharedView] show:nil];
}

+ (void)showShort:(NSString *)str {
    [[UUProgressHUD sharedView] show:str];
}

- (void)show:(NSString *)str {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!self.superview)
            [self.overlayWindow addSubview:self];
        
        if (!edgeImageView) {
            edgeImageView = [[UIImageView alloc]init];
            edgeImageView.frame = CGRectMake(0, 0, 125, 125);
            edgeImageView.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2,[[UIScreen mainScreen] bounds].size.height/2);
            edgeImageView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
            edgeImageView.layer.masksToBounds = YES;
            edgeImageView.layer.cornerRadius = 5;
        }
        
        if (!self.subTitleLabel){
            self.subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 20)];
            self.subTitleLabel.backgroundColor = [UIColor clearColor];
        }
        self.subTitleLabel.frame = CGRectMake(0, 98, 125, 20);
        self.subTitleLabel.text = RECORD_SLIDUP_CANCEL;
        self.subTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.subTitleLabel.font = [UIFont boldSystemFontOfSize:12];
        self.subTitleLabel.textColor = [UIColor whiteColor];
        
        [self addSubview:edgeImageView];
        [edgeImageView addSubview:self.subTitleLabel];
        
        if ([str isEqualToString:RECORD_TIME_SHORT]) {
            if (!shortImageView) {
                shortImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recoder_voice_short"]];
                shortImageView.frame = CGRectMake(22.5, 21, 80, 60);
                
            }
            self.subTitleLabel.text = RECORD_TIME_SHORT;
            [edgeImageView addSubview:shortImageView];
            
            if (recoderImageView) {
                [recoderImageView removeFromSuperview];
                recoderImageView = nil;
            }
            if (speakImageView) {
                [speakImageView removeFromSuperview];
                speakImageView = nil;
            }
            if (cancelImageView) {
                [cancelImageView removeFromSuperview];
                cancelImageView = nil;
            }
            
            
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 self.alpha = 1;
                             }
                             completion:^(BOOL finished){
                             }];
            
            [UIView animateWithDuration:0.5
                                  delay:1
                                options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 self.alpha = 0;
                             }
                             completion:^(BOOL finished){
                                 [shortImageView removeFromSuperview];
                                 shortImageView = nil;
                                 [self.subTitleLabel removeFromSuperview];
                                 self.subTitleLabel = nil;
                                 [edgeImageView removeFromSuperview];
                                 edgeImageView = nil;
                                 
                                 NSMutableArray *windows = [[NSMutableArray alloc] initWithArray:[UIApplication sharedApplication].windows];
                                 [windows removeObject:overlayWindow];
                                 overlayWindow = nil;
                                 
                                 [windows enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(UIWindow *window, NSUInteger idx, BOOL *stop) {
                                     if([window isKindOfClass:[UIWindow class]] && window.windowLevel == UIWindowLevelNormal) {
                                         [window makeKeyWindow];
                                         *stop = YES;
                                     }
                                 }];
                             }];
        } else {
            
            if (!speakImageView) {
                speakImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recorder_tube"]];
                speakImageView.frame = CGRectMake(21, 21, 40, 60);
            }
            if (!cancelImageView) {
                cancelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recoder_cancel"]];
                cancelImageView.frame = CGRectMake(22.5, 21, 80, 60);
            }
            if (!recoderImageView) {
                recoderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recoder_lv1"]];
                recoderImageView.frame = CGRectMake(73, 21, 40, 60);
                
            }
            
            [edgeImageView addSubview:speakImageView];
            [edgeImageView addSubview:recoderImageView];
            [edgeImageView addSubview:cancelImageView];
            cancelImageView.hidden = YES;
            
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 self.alpha = 1;
                             }
                             completion:^(BOOL finished){
                             }];
        }
        
        
        [self setNeedsDisplay];
    });
}

-(void) startAnimation
{
//    angle -= 3;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.09];
//    UIView.AnimationRepeatAutoreverses = YES;
//    edgeImageView.transform = CGAffineTransformMakeRotation(angle * (M_PI / 180.0f));
//    float second = [centerLabel.text floatValue];
//    if (second <= 10.0f) {
//        centerLabel.textColor = [UIColor redColor];
//    }else{
//        centerLabel.textColor = [UIColor yellowColor];
//    }
//    centerLabel.text = [NSString stringWithFormat:@"%.1f",second-0.1];
//    [recoderImageView startAnimating];
//    [UIView commitAnimations];
}

+ (void)changeSubTitle:(NSString *)str
{
    [[UUProgressHUD sharedView] setState:str];
}

- (void)setState:(NSString *)str
{
    self.subTitleLabel.text = str;
    if ([str isEqualToString:RECORD_SLIDUP_CANCEL]) {
        recoderImageView.hidden = NO;
        speakImageView.hidden = NO;
        cancelImageView.hidden = YES;
    } else if ([str isEqualToString:RECORD_TOUCHOUT_CANCEL]) {
        recoderImageView.hidden = YES;
        speakImageView.hidden = YES;
        cancelImageView.hidden = NO;
    }
}

+ (void)changeRecordLevel:(CGFloat)level {
    [[UUProgressHUD sharedView] setRecordLevel:level];
}

- (void)setRecordLevel:(CGFloat)level {
    NSString *imgStr = [NSString stringWithFormat:@"recoder_lv%d",(int)level];
    recoderImageView.image = [UIImage imageNamed:imgStr];
}

+ (void)dismissWithSuccess:(NSString *)str {
	[[UUProgressHUD sharedView] dismiss:str];
}

+ (void)dismissWithError:(NSString *)str {
	[[UUProgressHUD sharedView] dismiss:str];
}

- (void)dismiss:(NSString *)state {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [myTimer invalidate];
        myTimer = nil;
        self.subTitleLabel.text = nil;
        self.titleLabel.text = nil;
        centerLabel.text = state;
        centerLabel.textColor = [UIColor whiteColor];
        
        CGFloat timeLonger;
        if ([state isEqualToString:Localized(@"时间过短")]) {
            timeLonger = 1;
        }else{
            timeLonger = 0.6;
        }
        [UIView animateWithDuration:timeLonger
                              delay:0
                            options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.alpha = 0;
                         }
                         completion:^(BOOL finished){
                             if(self.alpha == 0) {
                                 [recoderImageView removeFromSuperview];
                                 recoderImageView = nil;
                                 [speakImageView removeFromSuperview];
                                 speakImageView = nil;
                                 [cancelImageView removeFromSuperview];
                                 cancelImageView = nil;
                                 [self.subTitleLabel removeFromSuperview];
                                 self.subTitleLabel = nil;
                                 [edgeImageView removeFromSuperview];
                                 edgeImageView = nil;
                                 

                                 NSMutableArray *windows = [[NSMutableArray alloc] initWithArray:[UIApplication sharedApplication].windows];
                                 [windows removeObject:overlayWindow];
                                 overlayWindow = nil;
                                 
                                 [windows enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(UIWindow *window, NSUInteger idx, BOOL *stop) {
                                     if([window isKindOfClass:[UIWindow class]] && window.windowLevel == UIWindowLevelNormal) {
                                         [window makeKeyWindow];
                                         *stop = YES;
                                     }
                                 }];
                             }
                         }];
    });
}

- (UIWindow *)overlayWindow {
    if(!overlayWindow) {
        overlayWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        overlayWindow.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        overlayWindow.userInteractionEnabled = NO;
        [overlayWindow makeKeyAndVisible];
    }
    return overlayWindow;
}


@end
