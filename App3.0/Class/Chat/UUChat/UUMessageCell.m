//
//  UUMessageCell.m
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-27.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUMessageCell.h"
#import "UUMessage.h"
#import "UUMessageFrame.h"
#import "UUAVAudioPlayer.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "UUImageAvatarBrowser.h"
#import "NSString + LBD.h"
#import "XSFormatterDate.h"

@interface UUMessageCell ()<UUAVAudioPlayerDelegate, UUMessageContentButtonDelegate>
{
    AVAudioPlayer *player;
    NSURL *voiceURL;
    NSData *songData;
    
    UUAVAudioPlayer *audio;
    
    UIView *headImageBackView;
    BOOL contentVoiceIsPlaying;
}
@end

@implementation UUMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        // 1、创建时间
        self.labelTime = [[UILabel alloc] init];
        self.labelTime.textAlignment = NSTextAlignmentCenter;
        self.labelTime.textColor = [UIColor whiteColor];
        self.labelTime.backgroundColor = LINE_COLOR;
        self.labelTime.font = ChatTimeFont;
        self.labelTime.layer.masksToBounds = YES;
        self.labelTime.layer.cornerRadius = 2;
        [self.contentView addSubview:self.labelTime];
        
        // 创建通知背景
        self.noticeBgView = [[UIView alloc] init];
        [self.noticeBgView setBackgroundColor:LINE_COLOR];
        self.noticeBgView.layer.masksToBounds = YES;
        self.noticeBgView.layer.cornerRadius = 2;
        [self.contentView addSubview:self.noticeBgView];
        
        // 创建通知图标（红包需要）
        self.imageNotice = [[UIImageView alloc] init];
        [self.noticeBgView addSubview:self.imageNotice];
        
        // 创建通知
        self.labelNotice = [[UILabel alloc] init];
        self.labelNotice.textAlignment = NSTextAlignmentCenter;
        self.labelNotice.textColor = [UIColor whiteColor];
        self.labelNotice.font = ChatTimeFont;
        self.labelNotice.numberOfLines = 0;
        UITapGestureRecognizer *noticeTap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            if ([self.messageFrame.message.extType isEqualToString:GROUP_INVITE_NEED_CONFIRM]) {
                if ([self.delegate respondsToSelector:@selector(noticeContentDidClick:)]) {
                    [self.delegate noticeContentDidClick:self];
                }
            }
        }];
        self.labelNotice.userInteractionEnabled = YES;
        [self.labelNotice addGestureRecognizer:noticeTap];
        [self.noticeBgView addSubview:self.labelNotice];
        
        // 2、创建头像
        headImageBackView = [[UIView alloc]init];
        headImageBackView.layer.cornerRadius = 2;
        headImageBackView.layer.masksToBounds = YES;
        headImageBackView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.4];
        [self.contentView addSubview:headImageBackView];
        self.btnHeadImage = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnHeadImage.layer.cornerRadius = 2;
        self.btnHeadImage.layer.masksToBounds = YES;
        [self.btnHeadImage addTarget:self action:@selector(btnHeadImageClick:)  forControlEvents:UIControlEventTouchUpInside];
        [headImageBackView addSubview:self.btnHeadImage];
        
        // 3、创建头像下标
        self.labelNum = [[UILabel alloc] init];
        self.labelNum.textColor = [UIColor grayColor];
        self.labelNum.textAlignment = NSTextAlignmentCenter;
        self.labelNum.font = ChatTimeFont;
        [self.contentView addSubview:self.labelNum];
        self.labelNum.hidden = YES;
        
        // 4、创建内容
        self.btnContent = [UUMessageContentButton buttonWithType:UIButtonTypeCustom];
        self.btnContent.delegate = self;
        [self.btnContent setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnContent.titleLabel.font = ChatContentFont;
        self.btnContent.titleLabel.numberOfLines = 0;
        [self.btnContent addTarget:self action:@selector(btnContentClick)  forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.btnContent];
        
        // 5、创建消息状态
        self.labelStatus = [[UILabel alloc] init];
        self.labelStatus.textAlignment = NSTextAlignmentRight;
        self.labelStatus.textColor = mainColor;
        self.labelStatus.font = ChatTimeFont;
        [self.contentView addSubview:self.labelStatus];
        
        self.statusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"messageSendFail"]];
        self.statusImageView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.statusImageView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendFailTap)];
        [self.statusImageView addGestureRecognizer:tap];
        [self.contentView addSubview:self.statusImageView];
        self.statusImageView.hidden = YES;
        
        self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.contentView addSubview:self.indicator];
        
        // 长按事件
//        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
//        [self.btnContent addGestureRecognizer:longPress];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(UUAVAudioPlayerDidFinishPlay) name:@"VoicePlayHasInterrupt" object:nil];
        
        //红外线感应监听
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(sensorStateChange:)
                                                     name:UIDeviceProximityStateDidChangeNotification
                                                   object:nil];
        contentVoiceIsPlaying = NO;

    }
    return self;
}

- (void)sendFailTap {
    if ([self.delegate respondsToSelector:@selector(sendFailTap:)]) {
        [self.delegate sendFailTap:self];
    }
}

//头像点击
- (void)btnHeadImageClick:(UIButton *)button{
    if ([self.delegate respondsToSelector:@selector(headImageDidClick:)])  {
        [self.delegate headImageDidClick:self];
    }
}

- (void)longPressAction:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        if ([self.delegate respondsToSelector:@selector(cellContentLongPress:)])  {
            [self.delegate cellContentLongPress:self];
        }
    }
}


- (void)btnContentClick{
    //play audio
    if (self.messageFrame.message.type == UUMessageTypeVoice) {
        if(!contentVoiceIsPlaying){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"VoicePlayHasInterrupt" object:nil];
            contentVoiceIsPlaying = YES;
            audio = [UUAVAudioPlayer sharedInstance];
            audio.delegate = self;
            if (self.messageFrame.message.voice) {
                [audio playSongWithData:self.messageFrame.message.voice];
            } else {
                NSString *name = [self.messageFrame.message.voiceUrl.path componentsSeparatedByString:@"/"].lastObject;
                BOOL isExist = [XMPPSignal isFileExist:name];
                if (!isExist) {
                    [self.messageFrame.message downloadVoiceWith:self.messageFrame.message.downVoicePath finash:^(NSURL *path){
                        [audio playSongWithUrl:path];
                    }];
                }else{
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *path = [paths objectAtIndex:0];
                    NSString *filePath = [path stringByAppendingPathComponent:name];
                    [audio playSongWithUrl:[NSURL URLWithString:filePath]];
                }
            }
            
        }else{
            [self UUAVAudioPlayerDidFinishPlay];
        }
        if ([self.delegate respondsToSelector:@selector(voiceDidClick:)]) {
            [self.delegate voiceDidClick:self];
        }
    }
    //show the picture
    else if (self.messageFrame.message.type == UUMessageTypePicture)
    {
        if ([self.delegate isKindOfClass:[UIViewController class]]) {
            [[(UIViewController *)self.delegate view] endEditing:YES];
        }
        if ([self.delegate respondsToSelector:@selector(cellContentDidClick:imageView:)]) {
            [self.delegate cellContentDidClick:self imageView:self.btnContent.backImageView];
        }
    }
    //show the video
    else if (self.messageFrame.message.type == UUMessageTypeVideo)
    {
        if ([self.delegate isKindOfClass:[UIViewController class]]) {
            [[(UIViewController *)self.delegate view] endEditing:YES];
        }
        if ([self.delegate respondsToSelector:@selector(videoDidClick:)]) {
            [self.delegate videoDidClick:self];
        }
    }
    else if (self.messageFrame.message.type == UUMessageTypeFile)
    {
        if ([self.delegate respondsToSelector:@selector(cellContentFileDidClick:)]) {
            [self.delegate cellContentFileDidClick:self];
        }
    }
    // show text and gonna copy that
    else if (self.messageFrame.message.type == UUMessageTypeText)
    {
        NSString *url = [self.messageFrame.message.strContent urlSubString];
        if (url) {
            if ([self.delegate respondsToSelector:@selector(cellContentUrlDidClick:)]) {
                [self.delegate cellContentUrlDidClick:url];
            }
        }
    }
    else if (self.messageFrame.message.type == UUMessageTypeLocation)
    {
        if (self.delegate) {
            double lat = [self.messageFrame.message.strLatitude doubleValue];
            double lon = [self.messageFrame.message.strLongitude doubleValue];
            [self.delegate locationDidClickLatitude:lat longitude:lon adddress:self.messageFrame.message.strLocation];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeRedPacket) {
        if ([self.delegate respondsToSelector:@selector(redPacketClick:)]) {
            [self.delegate redPacketClick:self];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeTransfer) {
        if ([self.delegate respondsToSelector:@selector(transferClick:)]) {
            [self.delegate transferClick:self];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeShare) {
        if ([self.delegate respondsToSelector:@selector(shareContentClick:)]) {
            UUMessageShareModel *model = [UUMessageShareModel mj_objectWithKeyValues:self.messageFrame.message.shareJosnString];
            [self.delegate shareContentClick:model.mLink];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeProductShare) {
        if ([self.delegate respondsToSelector:@selector(productShareClick:)]) {
            UUMessageProductShareModel *model = [UUMessageProductShareModel mj_objectWithKeyValues:self.messageFrame.message.shareJosnString];
            [self.delegate productShareClick:model.goodId];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeSystemPush) {
        if ([self.delegate respondsToSelector:@selector(shareContentClick:)]) {
            UUMessageSystemPushModel *model = [UUMessageSystemPushModel mj_objectWithKeyValues:self.messageFrame.message.shareJosnString];
            [self.delegate shareContentClick:model.link];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeCall) {
        if ([self.delegate respondsToSelector:@selector(callContentDidClick:)]) {
            [self.delegate callContentDidClick:self];
        }
    } else if (self.messageFrame.message.type == UUMessageTypeCard) {
        if ([self.delegate respondsToSelector:@selector(cardContentDidClick:)]) {
            [self.delegate cardContentDidClick:self];
        }
    }
}

- (void)UUAVAudioPlayerBeiginLoadVoice
{
    [self.btnContent benginLoadVoice];
}
- (void)UUAVAudioPlayerBeiginPlay
{
    //开启红外线感应
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    [self.btnContent didLoadVoice];
}
- (void)UUAVAudioPlayerDidFinishPlay
{
    //关闭红外线感应
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    contentVoiceIsPlaying = NO;
    [self.btnContent stopPlay];
    [[UUAVAudioPlayer sharedInstance]stopSound];
}

- (void) changeMessageState:(MessageStatus)status {
    
    self.statusImageView.center = CGPointMake(self.labelStatus.center.x+10, self.labelStatus.center.y);
    self.statusImageView.hidden = YES;
    switch (status) {
        case UUMessageStatusPending:
            [self.indicator stopAnimating];
            self.labelStatus.text = @"";
            self.statusImageView.hidden = YES;
            break;
        case UUMessageStatusDelivering:
            self.labelStatus.text = @"";
            self.statusImageView.hidden = YES;
//            [self.indicator startAnimating];
            break;
        case UUMessageStatusSuccessed:
            [self.indicator stopAnimating];
            self.statusImageView.hidden = YES;
//            self.labelStatus.text = @"送达";
            self.labelStatus.textColor = mainColor;
            break;
        case UUMessageStatusFailed:
            [self.indicator stopAnimating];
            self.labelStatus.text = @"";
            self.statusImageView.hidden = NO;
            break;
        case UUMessageStatusUnRead:
            [self.indicator stopAnimating];
            self.statusImageView.hidden = YES;
//            self.labelStatus.text = @"未读";
            self.labelStatus.textColor = mainColor;
            break;
        case UUMessageStatusRead:
            [self.indicator stopAnimating];
            self.statusImageView.hidden = YES;
//            self.labelStatus.text = @"已读";
            self.labelStatus.textColor = mainGrayColor;
            break;
        default:
            break;
    }
    
}

//内容及Frame设置
- (void)setMessageFrame:(UUMessageFrame *)messageFrame{

    _messageFrame = messageFrame;
    UUMessage *message = messageFrame.message;
    
    // 1、设置时间
    self.labelTime.text = message.strTime;
    self.labelTime.frame = messageFrame.timeF;
    
    if (messageFrame.showNotice) {
        // 设置通知
        self.labelNotice.text = message.strNotice;
        self.noticeBgView.frame = messageFrame.noticeF;
        if (message.extType && [message.extType isEqualToString:READ_PACKET_BACK]) {
            NSInteger length = message.strNotice.length;
            NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:message.strNotice];
            [attri addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"e83e3e"] range:NSMakeRange(length-2, 2)];
            self.labelNotice.attributedText = attri;
            
            self.noticeBgView.frame = CGRectMake(messageFrame.noticeF.origin.x-13, messageFrame.noticeF.origin.y, messageFrame.noticeF.size.width+26, messageFrame.noticeF.size.height);
            self.imageNotice.image = [UIImage imageNamed:@"red_packet_ico"];
            [self.imageNotice mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.noticeBgView).offset(5);
                make.centerY.mas_equalTo(self.noticeBgView);
                make.size.mas_equalTo(CGSizeMake(15, 19));
            }];
            [self.labelNotice mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.imageNotice.mas_right).offset(5);
                make.right.mas_equalTo(self.noticeBgView).offset(-5);
                make.centerY.mas_equalTo(self.noticeBgView);
            }];
        } else {
            if (message.extType && [message.extType isEqualToString:GROUP_INVITE_NEED_CONFIRM]) {
                NSInteger length = message.strNotice.length;
                NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:message.strNotice];
                [attri addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"e83e3e"] range:NSMakeRange(length-3, 3)];
                self.labelNotice.attributedText = attri;
            }
            if (message.extType && [message.extType isEqualToString:GROUP_INVITE_CONFIRM]) {
                NSInteger length = message.strNotice.length;
                NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:message.strNotice];
                [attri addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(length-3, 3)];
                self.labelNotice.attributedText = attri;
            }
            self.imageNotice.image = nil;
            self.imageNotice.frame = CGRectZero;
            
            [self.labelNotice mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.noticeBgView).offset(4);
                make.right.mas_equalTo(self.noticeBgView).offset(-4);
                make.centerY.mas_equalTo(self.noticeBgView);
            }];
        }
        
        // 隐藏所有界面
        [self.btnContent setAllBackViewHidden];
//        self.labelTime.hidden = YES;
        headImageBackView.hidden = YES;
        self.labelNum.hidden = YES;
        self.btnContent.hidden = YES;
        self.labelStatus.hidden = YES;
        self.statusImageView.hidden = YES;
        self.indicator.hidden = YES;
        return;
    } else{
        self.labelNotice.text = @"";
        self.labelNotice.attributedText = [[NSMutableAttributedString alloc] initWithString:@""];
        self.noticeBgView.frame = CGRectZero;
        self.labelNotice.frame = CGRectZero;
        self.labelTime.hidden = NO;
        headImageBackView.hidden = NO;
        self.labelNum.hidden = NO;
        self.btnContent.hidden = NO;
        self.labelStatus.hidden = NO;
        self.statusImageView.hidden = NO;
    }
    
    
    
    // 2、设置头像
    headImageBackView.frame = messageFrame.iconF;
    self.btnHeadImage.frame = CGRectMake(0, 0, ChatIconWH, ChatIconWH);
    NSURL *imageUrl;
    if ([message.strIcon hasPrefix:@"http"]) {
        imageUrl = [NSURL URLWithString:message.strIcon];
    } else {
        imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,message.strIcon]];
    }
    [self.btnHeadImage setBackgroundImageForState:UIControlStateNormal
                                          withURL:imageUrl
                                 placeholderImage:[UIImage imageNamed:@"user_fans_avatar"]];

    
    // 3、设置名称
    if (message.chatType == UUChatTypeGroupChat && message.from == UUMessageFromOther) {
        self.labelNum.hidden = NO;
        self.labelNum.text = message.strName;
        if (messageFrame.nameF.origin.x > 160) {
            self.labelNum.frame = CGRectMake(messageFrame.nameF.origin.x, messageFrame.nameF.origin.y, messageFrame.nameF.size.width, messageFrame.nameF.size.height);
            self.labelNum.textAlignment = NSTextAlignmentRight;
        }else{
            self.labelNum.frame = CGRectMake(messageFrame.nameF.origin.x, messageFrame.nameF.origin.y, messageFrame.nameF.size.width, messageFrame.nameF.size.height);
            self.labelNum.textAlignment = NSTextAlignmentLeft;
        }
    } else {
        self.labelNum.hidden = YES;
    }
    

    // 4、设置内容
    
    //prepare for reuse
    [self.btnContent setTitle:@"" forState:UIControlStateNormal];
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:@""];
    [self.btnContent setAttributedTitle:string forState:UIControlStateNormal];
    
    // 隐藏所有界面
    [self.btnContent setAllBackViewHidden];
    self.btnContent.frame = messageFrame.contentF;
    
    if (message.from == UUMessageFromMe) {
        self.btnContent.isMyMessage = YES;
        [self.btnContent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.btnContent.contentEdgeInsets = UIEdgeInsetsMake(ChatContentTop, ChatContentRight, ChatContentBottom, ChatContentLeft);
    }else{
        self.btnContent.isMyMessage = NO;
        [self.btnContent setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.btnContent.contentEdgeInsets = UIEdgeInsetsMake(ChatContentTop, ChatContentLeft, ChatContentBottom, ChatContentRight);
    }
    
    // 5、设置消息状态
    if (message.from == UUMessageFromMe && message.chatType == UUChatTypeChat && !messageFrame.statusHidden) {
        self.labelStatus.frame = messageFrame.statusF;
        self.indicator.center = CGPointMake(self.labelStatus.center.x+8, self.labelStatus.center.y);
        if ([self.indicator isAnimating]) {
            [self.indicator stopAnimating];
        }
        [self changeMessageState:message.status];
    } else {
        self.statusImageView.hidden = YES;
        self.labelStatus.hidden = YES;
    }
    
    
    //背景气泡图
    UIImage *normal;
    UIColor *textColor;
    if (message.from == UUMessageFromMe) {
        normal = [UIImage imageNamed:@"chatto_bg_normal"];
//        normal = [normal resizableImageWithCapInsets:UIEdgeInsetsMake(26, 10, 10, 10)];
        textColor = [UIColor blackColor];
    }
    else{
        normal = [UIImage imageNamed:@"chatfrom_bg_normal"];
//        normal = [normal resizableImageWithCapInsets:UIEdgeInsetsMake(26, 10, 10, 10)];
        textColor = [UIColor blackColor];
    }
    if ([message.strName isEqualToString:@"robot"] || message.type == UUMessageTypeSystemPush || message.type == UUMessageTypeProduct) {
        // 系统推送消息
        normal = [UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeZero];
    }
    [self.btnContent setBackgroundImage:normal forState:UIControlStateNormal];
    [self.btnContent setBackgroundImage:normal forState:UIControlStateHighlighted];

    [self.btnContent setImage:nil forState:UIControlStateNormal];
    [self.btnContent setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.btnContent setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    switch (message.type) {
        case UUMessageTypeText:
        {
            if ([message.strName isEqualToString:@"robot"]) {
                // 系统消息
                self.btnContent.sysNoticeBackView.hidden = NO;
                UUMessageSystemNoticeModel *model = [UUMessageSystemNoticeModel mj_objectWithKeyValues:message.strContent];
                if (model == nil) {
                    model = [UUMessageSystemNoticeModel new];
                    model.title = message.strContent;
                    model.type_title = Localized(@"系统消息");
                    model.ico = @"http://shangcheng.xsy.dsceshi.cn/static/app/images/ico/msg/changepwd.png";
                    model.link_title = @"这是一条老版本的系统消息";
                }
                self.btnContent.noticeModel = model;
                UITapGestureRecognizer *noticeTap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
                    if ([self.delegate respondsToSelector:@selector(systemNoticeContentDidClick:)]) {
                        [self.delegate systemNoticeContentDidClick:model];
                    }
                }];
                self.btnContent.sysNoticeBackView.userInteractionEnabled = YES;
                [self.btnContent.sysNoticeBackView addGestureRecognizer:noticeTap];
            } else {
                NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithAttributedString:[message.strContent changeToEmojiStringWithHaveReply:NO]];
                [att addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(0, att.length)];
                [self.btnContent setAttributedTitle:att forState:UIControlStateNormal];
            }
            
        }
            
            break;
        case UUMessageTypePicture:
        {
            self.btnContent.backImageView.hidden = NO;
            if (message.picture) {
                self.btnContent.backImageView.image = message.picture;
            } else if (!isEmptyString(message.pictureURL)) {
                
                [self.btnContent.backImageView yy_setImageWithURL:[NSURL URLWithString:message.pictureURL] options:YYWebImageOptionProgressive|YYWebImageOptionIgnoreAnimatedImage];
            }
            self.btnContent.backImageView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            [self makeMaskView:self.btnContent.backImageView withImage:normal];
        }
            break;
        case UUMessageTypeVideo:
        {
            self.btnContent.videoBackView.hidden = NO;
            if (message.picture) {
                self.btnContent.videoBackView.image = message.picture;
            }
            self.btnContent.videoTime.text = message.videoTime;
            self.btnContent.videoBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            [self makeMaskView:self.btnContent.videoBackView withImage:normal];
        }
            break;
        case UUMessageTypeVoice:
        {
            self.btnContent.voiceBackView.hidden = NO;
            self.btnContent.second.text = [NSString stringWithFormat:@"%@'s ",message.strVoiceTime];
//            songData = message.voice;
            voiceURL = message.voiceUrl;
        }
            break;
        case UUMessageTypeLocation:
        {
            self.btnContent.locatBackView.hidden = NO;
            UIImage *locaImg = [UIImage imageNamed:@"chat_location_preview"];
//            locaImg = [locaImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 35, 10) resizingMode:UIImageResizingModeStretch];
//            locaImg = [locaImg stretchableImageWithLeftCapWidth:10 topCapHeight:10];
            self.btnContent.locatBackView.image = locaImg;
            self.btnContent.locatBackView.contentMode = UIViewContentModeScaleToFill;
            self.btnContent.locatBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            self.btnContent.address.text = message.strLocation;
            self.btnContent.address.frame = CGRectMake(10, 0, self.btnContent.frame.size.width-30, 40);
            [self makeMaskView:self.btnContent.locatBackView withImage:normal];
        }
            break;
        case UUMessageTypeRedPacket:
        {
            self.btnContent.rpBackView.hidden = NO;
            self.btnContent.rpBackView.image = [UIImage imageNamed:@"red_packet_sent"];
            self.btnContent.rpBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            self.btnContent.rpIcon.image = [UIImage imageNamed:@""];
            self.btnContent.rpIcon.frame = CGRectZero;
            self.btnContent.rpMessgae.text = message.strRedMessage;
            self.btnContent.rpMessgae.frame = CGRectMake(55, 15, self.btnContent.frame.size.width-70, 20);
            self.btnContent.rpOpen.text = NSLocalizedString(@"redPacket_open", @"");
            self.btnContent.rpOpen.frame = CGRectMake(55, 35, self.btnContent.frame.size.width-70, 20);
            self.btnContent.rpDesc.text = NSLocalizedString(@"redPacket_desc", @"");
            self.btnContent.rpDesc.frame = CGRectMake(15, 80, self.btnContent.frame.size.width-70, 20);
            [self makeMaskView:self.btnContent.rpBackView withImage:normal];
        }
            break;
        case UUMessageTypeTransfer:
        {
            self.btnContent.rpBackView.hidden = NO;
            self.btnContent.rpBackView.image = [UIImage imageNamed:@"chat_transfer_sent"];
            self.btnContent.rpBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
//            self.btnContent.rpIcon.center = CGPointMake(90, 110);
            NSString *imageName;
            if ([message.strRedMessage isEqualToString:NSLocalizedString(@"chat_transfer_rec", @"")]) {
                imageName = @"chat_transfer_rec";
            } else if ([message.strRedMessage isEqualToString:NSLocalizedString(@"chat_transfer_back", @"")]) {
                imageName = @"chat_transfer_back";
            } else {
                imageName = @"chat_transfer_normal";
            }
            self.btnContent.rpIcon.image = [UIImage imageNamed:imageName];
            self.btnContent.rpIcon.frame = CGRectMake(15, 15, 40, 40);
            self.btnContent.rpMessgae.text = message.strRedMessage;
            self.btnContent.rpMessgae.frame = CGRectMake(60, 15, self.btnContent.frame.size.width-75, 20);
            self.btnContent.rpOpen.text = [NSString stringWithFormat:@"¥%@",message.moneyStr];
            self.btnContent.rpOpen.frame = CGRectMake(60, 35, self.btnContent.frame.size.width-75, 20);
            self.btnContent.rpDesc.text = NSLocalizedString(@"chat_transfer_desc", @"");
            self.btnContent.rpDesc.frame = CGRectMake(15, 80, self.btnContent.frame.size.width-70, 20);
            [self makeMaskView:self.btnContent.rpBackView withImage:normal];
        }
            break;
        case UUMessageTypeShare:
        {
            self.btnContent.shareBackView.hidden = NO;
            self.btnContent.shareBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            UUMessageShareModel *model = [UUMessageShareModel mj_objectWithKeyValues:message.shareJosnString];
            self.btnContent.shareTitle.text = model.mTitle;
            self.btnContent.shareDesc.text = model.mDigst;
            [self.btnContent.shareLogo getImageWithUrlStr:model.mIocLink andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            [self makeMaskView:self.btnContent.shareBackView withImage:normal];
        }
            break;
        case UUMessageTypeProduct:
        {
            self.btnContent.productBackView.hidden = NO;
            self.btnContent.productBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            UUMessageProductShareModel *model = [UUMessageProductShareModel mj_objectWithKeyValues:message.shareJosnString];
            self.btnContent.shareTitle.text = model.goodTitle;
            self.btnContent.shareDesc.text = model.goodPrice;
            [self.btnContent.shareLogo getImageWithUrlStr:model.goodICo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            self.btnContent.productBackView.userInteractionEnabled = YES;
//            self.btnContent.sendLinkBtn.userInteractionEnabled = YES;
            [self.btnContent.sendLinkBtn addTarget:self action:@selector(sendProductLinkAction) forControlEvents:UIControlEventTouchUpInside];
            [self.btnContent.productBackView layoutIfNeeded];
            [self makeMaskView:self.btnContent.productBackView withImage:normal];
        }
            break;
        case UUMessageTypeProductShare:
        {
            self.btnContent.productShareBackView.hidden = NO;
            self.btnContent.productShareBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            UUMessageProductShareModel *model = [UUMessageProductShareModel mj_objectWithKeyValues:message.shareJosnString];
            self.btnContent.shareTitle.text = model.goodTitle;
            self.btnContent.shareDesc.text = model.goodPrice;
            [self.btnContent.shareLogo getImageWithUrlStr:model.goodICo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            [self.btnContent.productBackView layoutIfNeeded];
            [self makeMaskView:self.btnContent.productShareBackView withImage:normal];
        }
            break;
        case UUMessageTypeFile:
        {
            self.btnContent.fileBackView.hidden = NO;
//            self.btnContent.fileBackView.image = [UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeZero];
            self.btnContent.fileBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            self.btnContent.fileTitle.text = message.fileTitle;
            self.btnContent.fileDesc.text = message.fileDesc;
            EMMessage *emMessage = message.emMessage;
            EMFileMessageBody *fileMesBody = (EMFileMessageBody *)emMessage.body;
            if (message.from == UUMessageFromMe) {
                self.btnContent.fileState.text = @"";
            } else {
                self.btnContent.fileState.text = fileMesBody.downloadStatus == EMDownloadStatusSuccessed?@"已下载":@"未下载";
            }
            
            [self makeMaskView:self.btnContent.fileBackView withImage:normal];
        }
            break;
        case UUMessageTypeSystemPush:
        {
            self.btnContent.sysPushBackView.hidden = NO;
//            self.btnContent.shareBackView.image = [UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeZero];
//            self.btnContent.sysPushBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            UUMessageSystemPushModel *model = [UUMessageSystemPushModel mj_objectWithKeyValues:message.shareJosnString];
            self.btnContent.sysPushTitle.text = model.title;
            self.btnContent.sysPushDesc.text = model.content;
            self.btnContent.sysPushTime.text = [XSFormatterDate dateWithTimeIntervalString:model.time];//model.time;
            [self.btnContent.sysPushLogo getImageWithUrlStr:model.img andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            [self makeMaskView:self.btnContent.sysPushBackView withImage:normal];
        }
            break;
        case UUMessageTypeCall:
        {
            NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithAttributedString:[message.strContent changeToEmojiStringWithHaveReply:NO]];
            [att addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(0, att.length)];
            [self.btnContent setAttributedTitle:att forState:UIControlStateNormal];
            NSString *imageName;
            if (message.isVideo) {
                if (message.from == UUMessageFromMe) {
                    imageName = @"chat_video_call_self";
                } else {
                    imageName = @"chat_video_call_receive";
                }
            } else {
                if (message.from == UUMessageFromMe) {
                    imageName = @"chat_voice_call_self";
                } else {
                    imageName = @"chat_voice_call_receive";
                }
            }
            [self.btnContent setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            self.btnContent.imageView.contentMode = UIViewContentModeScaleAspectFill;
            
            if (message.from == UUMessageFromMe) {
                [self.btnContent setImageEdgeInsets:UIEdgeInsetsMake(0, self.btnContent.titleLabel.bounds.size.width+5, 0, -self.btnContent.titleLabel.bounds.size.width-5)];
                [self.btnContent setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.btnContent.currentImage.size.width-5, 0, self.btnContent.currentImage.size.width+5)];
            }
            else{
                [self.btnContent setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
                [self.btnContent setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, -5)];
            }
        }
            
            break;
        case UUMessageTypeCard:
        {
            self.btnContent.cardBackView.hidden = NO;
            self.btnContent.cardBackView.frame = CGRectMake(0, 0, self.btnContent.frame.size.width, self.btnContent.frame.size.height);
            self.btnContent.cardTitle.text = message.cardName;
            [self.btnContent.cardIcon getImageWithUrlStr:message.cardIcon andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
            [self makeMaskView:self.btnContent.cardBackView withImage:normal];
        }
            break;
    }
}

- (void)makeMaskView:(UIView *)view withImage:(UIImage *)image
{
    UIImageView *imageViewMask = [[UIImageView alloc] initWithImage:image];
    imageViewMask.frame = CGRectInset(view.frame, 0.0f, 0.0f);
    view.layer.mask = imageViewMask.layer;
}

- (void)sendProductLinkAction {
    if ([self.delegate respondsToSelector:@selector(sendProductLinkClick:)]) {
        [self.delegate sendProductLinkClick:self.messageFrame.message.shareJosnString];
    }
}

//处理监听触发事件
-(void)sensorStateChange:(NSNotificationCenter *)notification;
{
    if ([[UIDevice currentDevice] proximityState] == YES){
        NSLog(@"Device is close to user");
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    }
    else{
        NSLog(@"Device is not close to user");
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    }
}

#pragma mark - UUMessageContentButtonDelegate
- (void)contentDelete
{
    if ([self.delegate respondsToSelector:@selector(cellContentDelete:)]) {
        [self.delegate cellContentDelete:self];
    }
}

- (void)contentTranspond
{
    if ([self.delegate respondsToSelector:@selector(cellContentTranspond:)]) {
        [self.delegate cellContentTranspond:self];
    }
}

- (void)contentCopy
{
    if ([self.delegate respondsToSelector:@selector(cellContentCopy:)]) {
        [self.delegate cellContentCopy:self];
    }
}

- (void)contentRecall
{
    if ([self.delegate respondsToSelector:@selector(cellContentRecall:)]) {
        [self.delegate cellContentRecall:self];
    }
}

- (void)contentDetector
{
    if ([self.delegate respondsToSelector:@selector(cellContentDetector:)]) {
        [self.delegate cellContentDetector:self];
    }
}
@end



