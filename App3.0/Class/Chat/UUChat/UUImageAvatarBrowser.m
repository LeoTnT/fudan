//
//  UUAVAudioPlayer.m
//  BloodSugarForDoc
//
//  Created by shake on 14-9-1.
//  Copyright (c) 2014年 shake. All rights reserved.
//

#import "UUImageAvatarBrowser.h"
#define NUMLABELWIDTH 80
#define NUMLABELHEIGHT 30
#define VIEW_TAG 1001
static UIImageView *orginImageView;
@implementation UUImageAvatarBrowser

+(void)showImage:(UIImageView *)avatarImageView title:(NSString *)title{
    UIImage *image=avatarImageView.image;
    orginImageView = avatarImageView;
//    orginImageView.alpha = 0;
    UIWindow *window=[UIApplication sharedApplication].keyWindow;
    for (UIView *view in window.subviews) {
        if (view.tag == VIEW_TAG) {
            [view removeFromSuperview];
        }
    }
    UIView *backgroundView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    CGRect oldframe=[avatarImageView convertRect:avatarImageView.bounds toView:window];
    backgroundView.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:1];
    backgroundView.alpha=1;
    backgroundView.tag = VIEW_TAG;
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:oldframe];
    imageView.image=image;
    imageView.tag=1;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [backgroundView addSubview:imageView];
    //标题
    UILabel *titleLabel=[[UILabel alloc] init];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont systemFontOfSize:15];
    titleLabel.text=title;
    [backgroundView addSubview:titleLabel];
    [window addSubview:backgroundView];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    [backgroundView addGestureRecognizer: tap];
    [UIView animateWithDuration:0.3 animations:^{
        imageView.frame=CGRectMake(0,([UIScreen mainScreen].bounds.size.height-image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width)/2, [UIScreen mainScreen].bounds.size.width, image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width);
        backgroundView.alpha=1;
    } completion:^(BOOL finished) {
        titleLabel.frame=CGRectMake(0, CGRectGetMaxY(imageView.frame)+10, mainWidth, 30);
    }];
}

+(void)hideImage:(UITapGestureRecognizer*)tap{
    UIView *backgroundView=tap.view;
    UIImageView *imageView=(UIImageView*)[tap.view viewWithTag:1];
    [UIView animateWithDuration:0.3 animations:^{
        imageView.frame=[orginImageView convertRect:orginImageView.bounds toView:[UIApplication sharedApplication].keyWindow];
         backgroundView.alpha=0;
         orginImageView.alpha = 1;
    } completion:^(BOOL finished) {
        [backgroundView removeFromSuperview];
    }];
}
@end
