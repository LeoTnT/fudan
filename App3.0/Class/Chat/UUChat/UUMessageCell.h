//
//  UUMessageCell.h
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-27.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UUMessageContentButton.h"
@class UUMessageFrame;
@class UUMessageCell;

@protocol UUMessageCellDelegate <NSObject>
@optional
- (void)cellContentUrlDidClick:(NSString *)url;
- (void)headImageDidClick:(UUMessageCell *)cell;
- (void)cellContentDidClick:(UUMessageCell *)cell imageView:(UIImageView *)contentImageView;
- (void)cellContentFileDidClick:(UUMessageCell *)cell;
- (void)locationDidClickLatitude:(double)latitude longitude:(double)longitue adddress:(NSString *)adddress;
- (void)voiceDidClick:(UUMessageCell *)cell;
- (void)videoDidClick:(UUMessageCell *)cell;
- (void)cellContentLongPress:(UUMessageCell *)cell;
- (void)cellContentDelete:(UUMessageCell *)cell;
- (void)cellContentTranspond:(UUMessageCell *)cell;
- (void)cellContentCopy:(UUMessageCell *)cell;
- (void)cellContentRecall:(UUMessageCell *)cell;
- (void)cellContentDetector:(UUMessageCell *)cell;
- (void)sendFailTap:(UUMessageCell *)cell;
- (void)redPacketClick:(UUMessageCell *)cell;
- (void)transferClick:(UUMessageCell *)cell;
- (void)shareContentClick:(NSString *)link;
- (void)callContentDidClick:(UUMessageCell *)cell;
- (void)noticeContentDidClick:(UUMessageCell *)cell;
- (void)systemNoticeContentDidClick:(UUMessageSystemNoticeModel *)sysNoticeModel;
- (void)cardContentDidClick:(UUMessageCell *)cell;
- (void)sendProductLinkClick:(NSString *)jsonString;
- (void)productShareClick:(NSString *)productId;
@end


@interface UUMessageCell : UITableViewCell

@property (nonatomic, retain)UILabel *labelTime;
@property (nonatomic, retain)UIView *noticeBgView;
@property (nonatomic, retain)UIImageView *imageNotice;
@property (nonatomic, retain)UILabel *labelNotice;
@property (nonatomic, retain)UILabel *labelNum;
@property (nonatomic, retain)UILabel *labelStatus;
@property (nonatomic, retain)UIImageView *statusImageView;
@property (nonatomic, retain)UIActivityIndicatorView *indicator;
@property (nonatomic, retain)UIButton *btnHeadImage;

@property (nonatomic, retain)UUMessageContentButton *btnContent;

@property (nonatomic, retain)UUMessageFrame *messageFrame;

@property (nonatomic, assign)id<UUMessageCellDelegate>delegate;

- (void) changeMessageState:(MessageStatus)state;

@end

