//
//  UUMessageFrame.h
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-26.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#define ChatMargin 10       //间隔
#define ChatIconWH 40       //头像宽高height、width
#define ChatPicWH 200       //图片宽高
#define ChatLocationPicWH 140 //定位图片高
#define ChatContentW 180    //内容宽度

#define ChatRedPacketH 84    //红包高
#define ChatRedPacketW 180    //红包宽

#define ChatShareH 80       //分享内容高
#define ChatShareW 180      //分享内容宽

#define ChatFileH 50       //文件内容高
#define ChatFileW 180      //文件内容宽

#define ChatSystemNoticeH 190       // 系统消息模板高
#define ChatSystemNoticeW [UIScreen mainScreen].bounds.size.width-24      // 系统消息模板宽

#define ChatSystemPushH 280       // 系统推送活动模板高
#define ChatSystemPushW [UIScreen mainScreen].bounds.size.width-24      // 系统推送活动模板宽

#define ChatTimeMarginW 15  //时间文本与边框间隔宽度方向
#define ChatTimeMarginH 10  //时间文本与边框间隔高度方向

#define ChatContentTop 10   //文本内容与按钮上边缘间隔
#define ChatContentLeft 25  //文本内容与按钮左边缘间隔
#define ChatContentBottom 10 //文本内容与按钮下边缘间隔
#define ChatContentRight 15 //文本内容与按钮右边缘间隔

#define ChatTimeFont [UIFont systemFontOfSize:12]   //时间字体
#define ChatContentFont [UIFont systemFontOfSize:16]//内容字体

#import <Foundation/Foundation.h>
@class UUMessage;

@interface UUMessageFrame : NSObject

@property (nonatomic, assign, readonly) CGRect nameF;
@property (nonatomic, assign, readonly) CGRect noticeF;
@property (nonatomic, assign, readonly) CGRect iconF;
@property (nonatomic, assign, readonly) CGRect timeF;
@property (nonatomic, assign, readonly) CGRect contentF;
@property (nonatomic, assign, readonly) CGRect statusF;

@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, strong) UUMessage *message;
@property (nonatomic, assign) BOOL showTime;
@property (nonatomic, assign) BOOL showNotice;
@property (nonatomic, assign) BOOL statusHidden;

@end
