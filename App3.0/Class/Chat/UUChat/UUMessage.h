//
//  UUMessage.h
//  UUChatDemoForTextVoicePicture
//
//  Created by shake on 14-8-26.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MessageType) {
    UUMessageTypeText       = 0 , // 文字
    UUMessageTypePicture    = 1 , // 图片
    UUMessageTypeVoice      = 2 , // 语音
    UUMessageTypeLocation   = 3 , // 位置
    UUMessageTypeRedPacket  = 4 , // 红包
    UUMessageTypeTransfer   = 5 , // 转账
    UUMessageTypeShare      = 6 , // 分享
    UUMessageTypeFile       = 7 , // 文件
    UUMessageTypeVideo      = 8 , // 视频
    UUMessageTypeSystemPush = 9 , // 系统消息推送消息
    UUMessageTypeCall       = 10, // 语音视频通话
    UUMessageTypeCard       = 11, // 名片
    UUMessageTypeProduct    = 12, // 商品
    UUMessageTypeProductShare    = 13  // 发送商品
};


typedef NS_ENUM(NSInteger, MessageFrom) {
    UUMessageFromMe    = 0,   // 自己发的
    UUMessageFromOther = 1    // 别人发得
};

typedef NS_ENUM(NSInteger, ChatType) {
    UUChatTypeChat   = 0,   /*! \~chinese 单聊消息 \~english one to one chat type */
    UUChatTypeGroupChat,    /*! \~chinese 群聊消息 \~english Group chat type */
    UUChatTypeChatRoom,     /*! \~chinese 聊天室消息 \~english Chatroom chat type */
};

typedef NS_ENUM(NSInteger, MessageStatus) {
    UUMessageStatusPending  = -1,    /*! \~chinese 发送未开始 \~english Pending */
    UUMessageStatusDelivering = 0,      /*! \~chinese 正在发送 \~english Delivering */
    UUMessageStatusSuccessed,       /*! \~chinese 发送成功 \~english Successed */
    UUMessageStatusFailed,          /*! \~chinese 发送失败 \~english Failed */
    UUMessageStatusUnRead,          /*! \~chinese 未读 \~english Failed */
    UUMessageStatusRead,            /*! \~chinese 已读 \~english Failed */
};

@interface UUMessageShareModel : NSObject
@property (nonatomic, copy) NSString *mIocLink; //ico链接
@property (nonatomic, copy) NSString *mLink;    //链接
@property (nonatomic, copy) NSString *mTitle;   //标题
@property (nonatomic, copy) NSString *mDigst;   //信息摘要
@property (nonatomic, copy) NSString *mTime;   //时间
@end

@interface UUMessageProductShareModel : NSObject
@property (nonatomic, copy) NSString *goodId;       // 产品id
@property (nonatomic, copy) NSString *goodTitle;    // 产品标题
@property (nonatomic, copy) NSString *goodPrice;    // 产品价格
@property (nonatomic, copy) NSString *goodICo;      // 产品图片，一张
@end

@interface UUMessageSystemPushModel : NSObject
@property (nonatomic, copy) NSString *img; //ico链接
@property (nonatomic, copy) NSString *link;    //链接
@property (nonatomic, copy) NSString *title;   //标题
@property (nonatomic, copy) NSString *content;   //信息摘要
@property (nonatomic, copy) NSString *time;   //时间
@property (nonatomic, copy) NSString *type;   //类型（new_push）
@end

@interface UUMessageSystemNoticeModel : NSObject
@property (nonatomic, copy) NSString *ico;          //ico链接
@property (nonatomic, copy) NSString *link_url;     //链接
@property (nonatomic, copy) NSString *link_app;     //app专属参数解析
@property (nonatomic, copy) NSString *title;        //标题
@property (nonatomic, copy) NSString *sub_title;    //副标题
@property (nonatomic, copy) NSString *img;          //图片
@property (nonatomic, copy) NSString *link_title;   //链接标题
@property (nonatomic, copy) NSString *type_title;   //类型标题
@property (nonatomic, strong) NSArray *ext;    //拓展
@end

@interface UUMessage : NSObject

@property (nonatomic, strong) NSDate * remoteTimestamp;
@property (nonatomic, copy) NSString *body;

@property (nonatomic, retain) id emMessage;
@property (nonatomic, copy) NSString *messageId;
@property (nonatomic, copy) NSString *strIcon;
@property (nonatomic, copy) NSString *strId;
@property (nonatomic, copy) NSString *strTime;
@property (nonatomic, copy) NSString *strNotice;
@property (nonatomic, copy) NSString *strName;
//    关系(所有0|好友1|关注的2|粉丝3)
@property (nonatomic, strong)NSNumber *relation;

@property (nonatomic, copy) NSString *strContent;
@property (nonatomic, copy) UIImage  *picture;
@property (nonatomic ,copy) NSString *pictureURL;
@property (nonatomic, copy) NSData   *voice;
@property (nonatomic, copy) NSURL    *voiceUrl;
@property (nonatomic, copy) NSURL    *downVoicePath;
@property (nonatomic, copy) NSString *strVoiceTime;

@property (nonatomic, copy) NSString *strLocation;
@property (nonatomic, copy) NSString *strLatitude;
@property (nonatomic, copy) NSString *strLongitude;

@property (nonatomic, copy) NSString *qrUrl;

@property (nonatomic, copy) NSString *extType;
@property (nonatomic, strong) NSDictionary *ext;
// 红包or转账
@property (nonatomic, copy) NSString *rpId; // 红包id
@property (nonatomic, copy) NSString *strRedMessage;
@property (nonatomic, copy) NSString *moneyStr;

// 文件
@property (nonatomic, copy) NSString *fileUrl;
@property (nonatomic, copy) NSString *fileTitle;
@property (nonatomic, copy) NSString *fileDesc;

// 分享
@property (nonatomic, copy) NSString *shareJosnString;

// 视频
@property (nonatomic, copy) NSString *videoTime;

// 语音视频通话
@property (nonatomic, assign) BOOL isVideo;

// 名片
@property (nonatomic, copy) NSString *cardId; // 名片id
@property (nonatomic, copy) NSString *cardIcon;
@property (nonatomic, copy) NSString *cardName;

@property (nonatomic, assign) MessageType type;
@property (nonatomic, assign) MessageFrom from;
@property (nonatomic, assign) MessageStatus status;
@property (nonatomic, assign) ChatType chatType;

@property (nonatomic, assign) BOOL showDateLabel;

/**
 时间
 */
@property (nonatomic,strong) NSDate *timestamp;

- (void)setWithDict:(NSDictionary *)dict;

- (void)minuteOffSetStart:(NSString *)start end:(NSString *)end;


/**
 自定义通知消息
 */
- (NSString *)noticeStringConvertByMessage:(XMPPMessage *)message ext:(NSDictionary *)ext;

/*
 下载语音
 */
- (void)downloadVoiceWith:(NSURL*)url finash:(void(^)(NSURL *url))finash;
@end
