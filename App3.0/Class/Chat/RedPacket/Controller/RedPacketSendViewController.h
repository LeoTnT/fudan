//
//  RedPacketSendViewController.h
//  App3.0
//
//  Created by mac on 2017/6/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef NS_ENUM(NSInteger, RedPacketType) {
    RPTypeNormal = 1,
    RPTypeRandom,
};

@interface RedPacketSendViewController : XSBaseViewController
@property (strong, nonatomic) id conversation;
- (void)canSendRedpacket;
@end
