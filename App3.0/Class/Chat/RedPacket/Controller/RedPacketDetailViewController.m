//
//  RedPacketDetailViewController.m
//  App3.0
//
//  Created by mac on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketDetailViewController.h"
#import "RedPacketTopCell.h"
#import "RedPacketPickCell.h"
#import "RedPacketListViewController.h"
#import "WalletVC.h"

@interface RedPacketDetailViewController () <RedPacketTopCellDelegate>
@property (getter=isFromMe, nonatomic) BOOL fromMe;
@property (getter=isHideList, nonatomic) BOOL hideList;
@property (strong, nonatomic) UIView *navBgView;
@end

@implementation RedPacketDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.fd_prefersNavigationBarHidden = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = NSLocalizedString(@"redPacket_detail", @"");
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    [self addNavBarView];
    
#ifdef ALIYM_AVALABLE
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P && ![self.model.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
        _hideList = YES;
    } else {
        _hideList = NO;
    }
#elif defined EMIM_AVALABLE
    if (((EMConversation *)self.conversation).type == EMConversationTypeChat && ![self.model.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
        _hideList = YES;
    } else {
        _hideList = NO;
    }
#else
    if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat && ![self.model.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
        _hideList = YES;
    } else {
        _hideList = NO;
    }
#endif
    
    if ([self.model.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
        _fromMe = YES;
    } else {
        _fromMe = NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}

- (void)addNavBarView {
    self.navBgView = [[UIView alloc] init];
    self.navBgView.backgroundColor = [UIColor hexFloatColor:@"e83e3e"];
    [self.view addSubview:self.navBgView];
    self.navBgView.hidden = YES;
    
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    [close setTitle:Localized(@"关闭") forState:UIControlStateNormal];
    [close setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    close.titleLabel.font = [UIFont systemFontOfSize:15];
    [close addTarget:self action:@selector(popView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:close];
    
    UILabel *title = [[UILabel alloc] init];
    title.text = Localized(@"红包");
    title.textColor = [UIColor whiteColor];
    title.font = [UIFont systemFontOfSize:17];
    [self.view addSubview:title];
    
    [self.navBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(66);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(35);
        make.centerX.mas_equalTo(self.view);
    }];
    [close mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(9.5);
        make.centerY.mas_equalTo(title);
    }];
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    self.navBgView.hidden = point.y >  40 ? NO:YES;
}

// 查看红包记录
- (void)checkRedPacketList {
    RedPacketListViewController *rplVC = [[RedPacketListViewController alloc] init];
    rplVC.type = RedPacketListReceive;
    [self.navigationController pushViewController:rplVC animated:YES];
}

- (void)checkWalletClick {
    // 查看钱包
    WalletVC *wallet = [[WalletVC alloc] init];
    [self.navigationController pushViewController:wallet animated:YES];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isHideList) {
        return 3;
    }
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return self.model.list.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
#ifdef ALIYM_AVALABLE
        if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
#elif defined EMIM_AVALABLE
        if (((EMConversation *)self.conversation).type == EMConversationTypeChat) {
#else
        if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat) {
#endif
            if (self.isFromMe) {
                return 260;
            } else {
                return 380;
            }
        } else {
            if (!self.model.is_received) {
                return 260;
            } else {
                return 380;
            }
        }
        
    }
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
#ifdef ALIYM_AVALABLE
        if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
#elif defined EMIM_AVALABLE
        if (((EMConversation *)self.conversation).type == EMConversationTypeChat) {
#else
        if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat) {
#endif
            if (self.isFromMe) {
                return 44;
            }
        } else {
            return 44;
        }
        
    }
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section != 1) {
        return nil;
    }
#ifdef ALIYM_AVALABLE
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P && !self.isFromMe) {
        return nil;
    }
#elif defined EMIM_AVALABLE
    if (((EMConversation *)self.conversation).type == EMConversationTypeChat && !self.isFromMe) {
        return nil;
    }
#else
    if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat && !self.isFromMe) {
        return nil;
    }
#endif
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 11, mainWidth-24, 22)];
    titleLabel.textColor = COLOR_666666;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
#ifdef ALIYM_AVALABLE
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
#elif defined EMIM_AVALABLE
    if (((EMConversation *)self.conversation).type == EMConversationTypeChat) {
#else
    if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat) {
#endif
        // 判断红包状态
        if (self.model.status == 1) {
            // 未领
            titleLabel.text = [NSString stringWithFormat:@"红包金额%@，等待对方领取",self.model.money];
        } else if (self.model.status == 2) {
            // 已领
            titleLabel.text = [NSString stringWithFormat:@"1个红包，共%@元",self.model.money];
        }
    } else {
        if (self.isFromMe) {
            titleLabel.text = [NSString stringWithFormat:@"已领取%d/%d个，共%@/%@元",self.model.count_rec,self.model.count,self.model.money_rec,self.model.money];
        } else {
            titleLabel.text = [NSString stringWithFormat:@"领取%d/%d个",self.model.count_rec,self.model.count];
        }
    }
    
    
    [myView addSubview:titleLabel];
    
    return myView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        RedPacketTopCell *cell = [RedPacketTopCell redPacketTopCellWithTableView:tableView model:self.model conversation:self.conversation];
        cell.delegate = self;
        return cell;
    } else if (indexPath.section == (self.isHideList?1:2)) {
        NSString *CellIdentifier = @"redPacketBottom";
        UITableViewCell *cell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2-60, 10, 120, 40)];
            [btn setTitle:@"查看红包记录" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor hexFloatColor:@"6177a8"] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            [btn addTarget:self action:@selector(checkRedPacketList) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
        }
        return cell;
    } else if (indexPath.section == (self.isHideList?2:3)) {
        NSString *CellIdentifier = @"redPacketBottomTips";
        UITableViewCell *cell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *tips = [[UILabel alloc] initWithFrame:CGRectMake(12, 10, mainWidth-24, 40)];
            tips.text = NSLocalizedString(@"redPacket_tips", @"");
            tips.font = [UIFont systemFontOfSize:12];
            tips.textAlignment = NSTextAlignmentCenter;
            tips.textColor = COLOR_999999;
            [cell addSubview:tips];
        }
        return cell;
    }
    
    RedPacketPickCell *cell = [RedPacketPickCell redPacketPickCellWithTableView:tableView model:self.model.list[indexPath.row]];
    return cell;
}

@end
