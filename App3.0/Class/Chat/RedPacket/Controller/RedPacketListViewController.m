//
//  RedPacketListViewController.m
//  App3.0
//
//  Created by mac on 2017/6/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketListViewController.h"
#import "RedPacketListTopCell.h"
#import "RedPacketListCell.h"
#import "XSFormatterDate.h"
#import "XSDatePickerView.h"

@interface RedPacketListViewController ()<XSDatePickerDelegate, RedPacketListTopCellDelegate>
@property (strong, nonatomic) RedPacketMineModel *model;
@property (strong, nonatomic) XSDatePickerView *datePicker;
@property (copy, nonatomic) NSString *currentYear;
@end

@implementation RedPacketListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.type == RedPacketListSend) {
        self.navigationItem.title = @"发出的红包";
    } else {
        self.navigationItem.title = @"收到的红包";
    }
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];
    self.currentYear=[NSString stringWithFormat:@"%li",(long)[[formatter stringFromDate:currentDate] integerValue]];
    
    
    [self getListDataWithYear:self.currentYear];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:nil htlImage:nil title:Localized(@"关闭") action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"···" action:^{
        @strongify(self);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"收到的红包" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (self.type != RedPacketListReceive) {
                self.navigationItem.title = @"收到的红包";
                self.type = RedPacketListReceive;
                [self getListDataWithYear:self.currentYear];
            }
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"发出的红包" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (self.type != RedPacketListSend) {
                self.navigationItem.title = @"发出的红包";
                self.type = RedPacketListSend;
                [self getListDataWithYear:self.currentYear];
            }
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil]];
        
        // 弹出对话框
        [self presentViewController:alert animated:true completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getListDataWithYear:(NSString *)year {
    [XSTool showProgressHUDWithView:self.view];
    NSString *dateString = [NSString stringWithFormat:@"%@-01-01",year];
    NSString *timeSp = [XSFormatterDate timeSpWithDateString:dateString];
    NSLog(@"%@",timeSp);
    @weakify(self);
    if (self.type == RedPacketListSend) {
        // 我发出的红包
        [HTTPManager getRedPacketSendListWithYear:timeSp success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.model = [RedPacketMineModel mj_objectWithKeyValues:dic[@"data"]];
                self.model.type = 0;
                self.model.year = year;
                [self.tableView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        // 我接收的红包
        [HTTPManager getRedPacketReceiveListWithYear:timeSp success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.model = [RedPacketMineModel mj_objectWithKeyValues:dic[@"data"]];
                self.model.type = 1;
                self.model.year = year;
                [self.tableView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }
    
}

#pragma mark RedPacketListTopCellDelegate
- (void)chooseYearClick {
    CGFloat pickerHeight = 200;
    if (!self.datePicker) {
        self.datePicker = [[XSDatePickerView alloc] init];
    }
    
    [self.datePicker showInView:self.view withFrame:CGRectMake(0, mainHeight-pickerHeight, mainWidth, pickerHeight)];
    self.datePicker.delegate = self;
}

#pragma mark XSDatePickerDelegate
- (void)pickerView:(XSDatePickerView *)picker ValueChanged:(NSString *)value {
    NSLog(@"%@",value);
    [self getListDataWithYear:value];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return self.model?self.model.list.data.count:0;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 380;
    }
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        RedPacketListTopCell *cell = [RedPacketListTopCell redPacketListTopCellWithTableView:tableView model:self.model];
        cell.delegate = self;
        return cell;
    }
    RedPacketListCell *cell = [RedPacketListCell redPacketListCellWithTableView:tableView model:self.model.list.data[indexPath.row]];
    return cell;
}

@end
