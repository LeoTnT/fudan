//
//  ChatTransferDetailViewController.h
//  App3.0
//
//  Created by mac on 2017/8/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "RedPacketModel.h"

@interface ChatTransferDetailViewController : XSBaseViewController
@property (strong, nonatomic) TransferDetailModel *model;
@property (strong, nonatomic) id conversation;
@property (copy, nonatomic) NSString *transferId;
@property (strong, nonatomic) id resendMessage;
@end
