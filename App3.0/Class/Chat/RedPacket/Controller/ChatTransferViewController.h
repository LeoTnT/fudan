//
//  ChatTransferViewController.h
//  App3.0
//
//  Created by mac on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface ChatTransferViewController : XSBaseViewController
@property (strong, nonatomic) NickNameDataParser *userInfo;
@property (strong, nonatomic) id conversation;

- (void)canSendTransfer;
@end
