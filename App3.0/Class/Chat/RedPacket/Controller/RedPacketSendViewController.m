//
//  RedPacketSendViewController.m
//  App3.0
//
//  Created by mac on 2017/6/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketSendViewController.h"
#import "RedPacketListViewController.h"
#import "OrderPayViewController.h"
#import "RedPacketModel.h"

@interface RedPacketSendViewController () <UITextFieldDelegate>
@property (strong, nonatomic) XSCustomButton *sendButton;
@property (strong, nonatomic) UITextField *numTextField;
@property (strong, nonatomic) UITextField *moneyTextField;
@property (strong, nonatomic) UITextField *messageTextField;
@property (strong, nonatomic) UILabel *totalLabel;
@property (copy, nonatomic) NSString *redMessage;
@property (assign, nonatomic) RedPacketType rpType; // 	金额类型(1定额|2随机)
@property (strong, nonatomic) UILabel *moneyTitleLabel;
@property (strong, nonatomic) UIImageView *randomIcon;
@property (strong, nonatomic) UILabel *typeLabel;
@property (copy, nonatomic) NSString *redpacketId;
@property (assign, nonatomic) NSInteger target; // 目标类型(1个人|2群组)
@property (copy, nonatomic) NSString *rpCount; // 红包个数
@end

@implementation RedPacketSendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.navigationItem.title = NSLocalizedString(@"sendPacket", @"");
    self.autoHideKeyboard = YES;
    self.view.backgroundColor = [UIColor hexFloatColor:@"eeeeee"];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:NSLocalizedString(@"redPacketRecord", @"") action:^{
        // 红包记录
        @strongify(self);
        RedPacketListViewController *rpList = [[RedPacketListViewController alloc] init];
        rpList.type = RedPacketListReceive;
        [self.navigationController pushViewController:rpList animated:YES];
    }];
    
    self.rpType = RPTypeRandom;
#ifdef ALIYM_AVALABLE
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
#elif defined EMIM_AVALABLE
    if (((EMConversation *)self.conversation).type == EMConversationTypeChat) {
#else
    if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat) {
#endif
        self.target = 1;
        self.rpCount = @"1";
        [self sendSingleChat];
    } else {
        self.target = 2;
        [self sendGroupChat];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendGroupChat {
    NSString *gId;
#ifdef ALIYM_AVALABLE
    gId = ((YWTribeConversation *)self.conversation).tribe.tribeId;
#elif defined EMIM_AVALABLE
    gId = ((EMConversation *)self.conversation).conversationId;
#else
    gId = ((ConversationModel *)self.conversation).bareJID.user;
#endif
    NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:gId];
    GroupDataModel *gModel = groupDBArray[0];
    // 红包个数
    UIView *bgViewNum = [[UIView alloc] init];
    bgViewNum.backgroundColor = [UIColor whiteColor];
    bgViewNum.layer.masksToBounds = YES;
    bgViewNum.layer.cornerRadius = 3;
    [self.view addSubview:bgViewNum];
    
    UILabel *numTitle = [[UILabel alloc] init];
    numTitle.text = NSLocalizedString(@"redPacket_number", @"");
    numTitle.font = [UIFont systemFontOfSize:14];
    [bgViewNum addSubview:numTitle];
    
    _numTextField = [[UITextField alloc] init];
    _numTextField.placeholder = NSLocalizedString(@"redPacket_import_number", @"");
    _numTextField.font = [UIFont systemFontOfSize:14];
    UILabel * placeholderLabel = [_numTextField valueForKey:@"_placeholderLabel"];
    placeholderLabel.textAlignment = NSTextAlignmentRight;
    _numTextField.textAlignment = NSTextAlignmentRight;
    _numTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_numTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgViewNum addSubview:_numTextField];
    
    UILabel *numUnit = [[UILabel alloc] init];
    numUnit.textAlignment = NSTextAlignmentRight;
    numUnit.text = NSLocalizedString(@"redPacket_number_unit", @"");
    numUnit.font = [UIFont systemFontOfSize:14];
    [bgViewNum addSubview:numUnit];
    
    UILabel *gNumblabel = [[UILabel alloc] init];
    gNumblabel.textColor = [UIColor hexFloatColor:@"a1a1a1"];
    gNumblabel.text = [NSString stringWithFormat:@"本群共%@人",gModel.member_count];
    gNumblabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:gNumblabel];
    
    [bgViewNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(20);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    [numTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgViewNum).offset(10);
        make.centerY.mas_equalTo(bgViewNum);
    }];
    [_numTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgViewNum).offset(80);
        make.right.mas_equalTo(numUnit.mas_left).offset(-10);
        make.centerY.mas_equalTo(bgViewNum);
    }];
    [numUnit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgViewNum).offset(-10);
        make.centerY.mas_equalTo(bgViewNum);
    }];
    [gNumblabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(bgViewNum.mas_bottom).offset(8);
    }];
    
    // 金额
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 3;
    [self.view addSubview:bgView];
    
    self.moneyTitleLabel = [[UILabel alloc] init];
    self.moneyTitleLabel.text = NSLocalizedString(@"redPacket_money_total", @"");
    self.moneyTitleLabel.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:self.moneyTitleLabel];
    
    _moneyTextField = [[UITextField alloc] init];
    _moneyTextField.placeholder = NSLocalizedString(@"redPacket_import_money", @"");
    _moneyTextField.font = [UIFont systemFontOfSize:14];
    placeholderLabel = [_moneyTextField valueForKey:@"_placeholderLabel"];
    placeholderLabel.textAlignment = NSTextAlignmentRight;
    _moneyTextField.textAlignment = NSTextAlignmentRight;
    _moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    [_moneyTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_moneyTextField];
    
    UILabel *unit = [[UILabel alloc] init];
    unit.textAlignment = NSTextAlignmentRight;
    unit.text = NSLocalizedString(@"redPacket_unit", @"");
    unit.font = [UIFont systemFontOfSize:14];;
    [bgView addSubview:unit];
    
    self.typeLabel = [[UILabel alloc] init];
    self.typeLabel.textColor = [UIColor hexFloatColor:@"a1a1a1"];
    self.typeLabel.text = @"当前为拼手气红包，";
    self.typeLabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:self.typeLabel];
    
    UIButton *normalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [normalBtn setTitle:@"改为普通红包" forState:UIControlStateNormal];
    [normalBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [normalBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [normalBtn addTarget:self action:@selector(changeType:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:normalBtn];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgViewNum.mas_bottom).offset(40);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    [self.moneyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(10);
        make.centerY.mas_equalTo(bgView);
    }];
    [_moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(70);
        make.right.mas_equalTo(bgView).offset(-34);
        make.centerY.mas_equalTo(bgView);
    }];
    [unit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-10);
        make.centerY.mas_equalTo(bgView);
    }];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(25);
        make.top.mas_equalTo(bgView.mas_bottom).offset(8);
    }];
    [normalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.typeLabel.mas_right);
        make.centerY.mas_equalTo(self.typeLabel);
    }];
    
    // 留言
    UIView *bgView2 = [[UIView alloc] init];
    bgView2.backgroundColor = [UIColor whiteColor];
    bgView2.layer.masksToBounds = YES;
    bgView2.layer.cornerRadius = 3;
    [self.view addSubview:bgView2];
    
    UILabel *title = [[UILabel alloc] init];
    title.text = NSLocalizedString(@"redPacket_message", @"");
    title.font = [UIFont systemFontOfSize:14];
    [bgView2 addSubview:title];
    
    self.redMessage = NSLocalizedString(@"redPacket_defaultMessage", @"");;
    _messageTextField = [[UITextField alloc] init];
    _messageTextField.placeholder = NSLocalizedString(@"redPacket_defaultMessage", @"");
    _messageTextField.font = [UIFont systemFontOfSize:14];
    placeholderLabel = [_messageTextField valueForKey:@"_placeholderLabel"];
    placeholderLabel.textAlignment = NSTextAlignmentRight;
    _messageTextField.textAlignment = NSTextAlignmentRight;
    [_messageTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView2 addSubview:_messageTextField];
    
    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_bottom).offset(40);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView2.mas_left).offset(10);
        make.centerY.mas_equalTo(bgView2);
    }];
    
    [_messageTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title.mas_right).offset(10);
        make.right.mas_equalTo(bgView2).offset(-10);
        make.centerY.mas_equalTo(bgView2);
    }];
    
    self.totalLabel = [[UILabel alloc] init];
    self.totalLabel.text = @"¥ 0.00";
    self.totalLabel.textAlignment = NSTextAlignmentCenter;
    self.totalLabel.font = [UIFont systemFontOfSize:30];
    self.totalLabel.textColor = [UIColor hexFloatColor:@"e83e3e"];
    [self.view addSubview:self.totalLabel];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView2.mas_bottom).offset(29.5);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
    }];
    
    // 发送
    _sendButton = [[XSCustomButton alloc] initWithFrame:CGRectZero title:NSLocalizedString(@"redPacket_send", @"") titleColor:[UIColor whiteColor] fontSize:18 backgroundColor:[UIColor hexFloatColor:@"e83e3e"] higTitleColor:[UIColor whiteColor] highBackgroundColor:[UIColor hexFloatColor:@"e83e3e"]];
    [_sendButton setBorderWith:0 borderColor:[BG_COLOR CGColor] cornerRadius:5];
    [_sendButton addTarget:self action:@selector(sendRedPacketAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendButton];
    _sendButton.enabled = NO;
    
    [_sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.top.mas_equalTo(self.totalLabel.mas_bottom).offset(20.5);
        make.height.mas_equalTo(50);
    }];
    
    UILabel *tips = [[UILabel alloc] init];
    tips.text = NSLocalizedString(@"redPacket_tips", @"");
    tips.font = [UIFont systemFontOfSize:12];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.textColor = COLOR_999999;
    [self.view addSubview:tips];
    
    [tips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view).offset(-33);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
    }];
}

- (void)sendSingleChat {
    // 金额
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 3;
    [self.view addSubview:bgView];
    
    UILabel *title = [[UILabel alloc] init];
    title.text = NSLocalizedString(@"redPacket_money", @"");
    title.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:title];
    
    _moneyTextField = [[UITextField alloc] init];
    _moneyTextField.delegate = self;
    _moneyTextField.placeholder = NSLocalizedString(@"redPacket_import_money", @"");
    _moneyTextField.font = [UIFont systemFontOfSize:14];
    UILabel * placeholderLabel = [_moneyTextField valueForKey:@"_placeholderLabel"];
    placeholderLabel.textAlignment = NSTextAlignmentRight;
    _moneyTextField.textAlignment = NSTextAlignmentRight;
    _moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    [_moneyTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:_moneyTextField];
    
    UILabel *unit = [[UILabel alloc] init];
    unit.textAlignment = NSTextAlignmentRight;
    unit.text = NSLocalizedString(@"redPacket_unit", @"");
    unit.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:unit];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(20);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(10);
        make.centerY.mas_equalTo(bgView);
    }];
    [_moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(80);
        make.right.mas_equalTo(bgView).offset(-34);
        make.centerY.mas_equalTo(bgView);
    }];
    [unit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-10);
        make.centerY.mas_equalTo(bgView);
    }];
    
    
    
    // 留言
    UIView *bgView2 = [[UIView alloc] init];
    bgView2.backgroundColor = [UIColor whiteColor];
    bgView2.layer.masksToBounds = YES;
    bgView2.layer.cornerRadius = 3;
    [self.view addSubview:bgView2];
    
    title = [[UILabel alloc] init];
    title.text = NSLocalizedString(@"redPacket_message", @"");
    title.font = [UIFont systemFontOfSize:14];
    [bgView2 addSubview:title];
    
    self.redMessage = NSLocalizedString(@"redPacket_defaultMessage", @"");
    _messageTextField = [[UITextField alloc] init];
    _messageTextField.placeholder = NSLocalizedString(@"redPacket_defaultMessage", @"");
    _messageTextField.font = [UIFont systemFontOfSize:14];
    placeholderLabel = [_messageTextField valueForKey:@"_placeholderLabel"];
    placeholderLabel.textAlignment = NSTextAlignmentRight;
    _messageTextField.textAlignment = NSTextAlignmentRight;
    [_messageTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView2 addSubview:_messageTextField];
    
    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_bottom).offset(40);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView2.mas_left).offset(10);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(40);
    }];
    
    [_messageTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title.mas_right).offset(10);
        make.right.mas_equalTo(bgView2).offset(-10);
        make.centerY.mas_equalTo(bgView2);
    }];
    
    self.totalLabel = [[UILabel alloc] init];
    self.totalLabel.text = @"¥ 0.00";
    self.totalLabel.textAlignment = NSTextAlignmentCenter;
    self.totalLabel.font = [UIFont boldSystemFontOfSize:30];
    self.totalLabel.textColor = [UIColor hexFloatColor:@"e83e3e"];
    [self.view addSubview:self.totalLabel];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView2.mas_bottom).offset(29.5);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
    }];
    
    // 发送
    _sendButton = [[XSCustomButton alloc] initWithFrame:CGRectZero title:NSLocalizedString(@"redPacket_send", @"") titleColor:[UIColor whiteColor] fontSize:18 backgroundColor:[UIColor hexFloatColor:@"e83e3e"] higTitleColor:[UIColor whiteColor] highBackgroundColor:[UIColor hexFloatColor:@"e83e3e"]];
    [_sendButton setBorderWith:0 borderColor:[BG_COLOR CGColor] cornerRadius:5];
    [_sendButton addTarget:self action:@selector(sendRedPacketAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendButton];
    _sendButton.enabled = NO;
    
    [_sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
        make.top.mas_equalTo(self.totalLabel.mas_bottom).offset(20.5);
        make.height.mas_equalTo(50);
    }];
    
    UILabel *tips = [[UILabel alloc] init];
    tips.text = NSLocalizedString(@"redPacket_tips", @"");
    tips.font = [UIFont systemFontOfSize:12];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.textColor = COLOR_999999;
    [self.view addSubview:tips];
    
    [tips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view).offset(-33);
        make.left.mas_equalTo(self.view).offset(15);
        make.right.mas_equalTo(self.view).offset(-15);
    }];
}

- (void)changeType:(UIButton *)sender {
    self.totalLabel.text = @"¥ 0.00";
    self.moneyTextField.text = @"";
    self.numTextField.text = @"";
    self.sendButton.enabled = NO;
    
    if (self.rpType == RPTypeNormal) {
        self.rpType = RPTypeRandom;
        [sender setTitle:@"改为普通红包" forState:UIControlStateNormal];
        self.typeLabel.text = @"当前为拼手气红包，";
        self.moneyTitleLabel.text = NSLocalizedString(@"redPacket_money_total", @"");
    } else if (self.rpType == RPTypeRandom) {
        self.rpType = RPTypeNormal;
        [sender setTitle:@"改为拼手气红包" forState:UIControlStateNormal];
        self.typeLabel.text = @"当前为普通红包，";
        self.moneyTitleLabel.text = NSLocalizedString(@"redPacket_money", @"");
    }
}

- (void)sendRedPacketAction:(UIButton *)sender {
    [self.view endEditing:YES];
    NSString *money = self.moneyTextField.text;
    if (self.rpType == RPTypeNormal) {
        money = [NSString stringWithFormat:@"%.2f",[self.moneyTextField.text floatValue]*[self.rpCount intValue]];
    }
    NSString *cId;
#ifdef ALIYM_AVALABLE
    cId = ((YWConversation *)self.conversation).conversationId;
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
        cId = ((YWP2PConversation *)self.conversation).person.personId;
    } else {
        cId = ((YWTribeConversation *)self.conversation).tribe.tribeId;
    }
#elif defined EMIM_AVALABLE
    cId = ((EMConversation *)self.conversation).conversationId;
#else
    cId = ((ConversationModel *)self.conversation).bareJID.user;
#endif
    NSDictionary *params = @{@"target":@(self.target),@"target_id":cId,@"type":@(self.rpType),@"count":self.rpCount,@"money":money,@"remark":self.messageTextField.text};
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager sendRedPacketWithParams:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        @strongify(self);
        if (state.status) {
            OrderPayViewController *opVC = [[OrderPayViewController alloc] init];
            opVC.tableName = dic[@"data"][@"tname"];
            opVC.orderId = dic[@"data"][@"id"];
            opVC.parentVC = self;
            self.redpacketId = dic[@"data"][@"id"];
            [self.navigationController pushViewController:opVC animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
    

}

- (void)canSendRedpacket {
    // 发送红包消息
    
    // 拓展
    NSDictionary *extDic = @{MSG_TYPE:READ_PACKET,CUSTOM_MSG_ID:self.redpacketId};
    
    // 红包文字
    NSString *text = self.redMessage;
#ifdef ALIYM_AVALABLE
    
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
    @weakify(self);
    //发送消息
    [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
        @strongify(self);
        [self performSelector:@selector(popView) withObject:nil afterDelay:0];
    }];
#elif defined EMIM_AVALABLE
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)self.conversation).conversationId from:[UserInstance ShardInstnce].uid to:((EMConversation *)self.conversation).conversationId body:body ext:extDic];
    message.chatType = (EMChatType)((EMConversation *)self.conversation).type;// 设置消息类型
    
    @weakify(self);
    //发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        @strongify(self);
        [self performSelector:@selector(popView) withObject:nil afterDelay:0];
    }];
#else
    if (((ConversationModel *)self.conversation).chatType == UUChatTypeChat) {
        [XMPPSignal sendMessage:text to:((ConversationModel *)self.conversation).bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
            [self performSelector:@selector(popView) withObject:nil afterDelay:0];
        }];
    }else{
        
        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
        message.type = @"txt";
        message.body = text;
        message.ext = extDic.mj_JSONString;
        NSString *messageString = message.mj_JSONString;
        [((ConversationModel *)self.conversation).xmppRoomManager.xmppRoom sendMessageWithBody:messageString];
        
        [self performSelector:@selector(popView) withObject:nil afterDelay:1];
    }
#endif
    
    
}

- (void)popView {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 1] animated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
}
/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if ([textField isEqual:self.moneyTextField]) {
        if (textField.text.length > 6) {
            [XSTool showToastWithView:self.view Text:@"最多输入6位"];
            textField.text = [textField.text substringToIndex:6];
        }
        if (textField.text.length > 0) {
            if (self.target == 1  || self.rpType == RPTypeRandom) {
                self.totalLabel.text = [NSString stringWithFormat:@"¥ %.2f",[self.moneyTextField.text floatValue]];
            } else if (self.rpType == RPTypeNormal && self.numTextField.text.length > 0) {
                self.totalLabel.text = [NSString stringWithFormat:@"¥ %.2f",[self.moneyTextField.text floatValue]*[self.rpCount intValue]];
            } else {
                self.totalLabel.text = @"¥ 0.00";
            }
            
        } else {
            self.totalLabel.text = @"¥ 0.00";
        }
        
    } else if ([textField isEqual:self.numTextField]) {
        if (textField.text.length > 3) {
            textField.text = [textField.text substringToIndex:3];
        }
        self.rpCount = textField.text;
        
        if (self.rpType == RPTypeNormal) {
            if (self.moneyTextField.text.length > 0) {
                self.totalLabel.text = [NSString stringWithFormat:@"¥ %.2f",[self.moneyTextField.text floatValue]*[self.rpCount intValue]];
            } else {
                self.totalLabel.text = @"¥ 0.00";
            }
        }
    } else if ([textField isEqual:self.messageTextField]) {
        if (textField.text.length > 16) {
            textField.text = [textField.text substringToIndex:16];
        }
        self.redMessage = textField.text;
    }
    

    if (self.moneyTextField.text.length >0) {
        if (self.target == 2 && self.numTextField.text.length == 0) {
            self.sendButton.enabled = NO;
        } else {
            self.sendButton.enabled = YES;
        }
    } else {
        self.sendButton.enabled = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%@====%@",textField.text,_moneyTextField.text);
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location==NSNotFound) {
        isHaveDian=NO;
    }
    if ([string length]>0)
    {
        unichar single=[string characterAtIndex:0];//当前输入的字符
        if ((single >='0' && single<='9') || single=='.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length]==0){
                if(single == '.'){
//                    [self alertView:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
//                if (single == '0') {
////                    [self alertView:@"亲，第一个数字不能为0"];
//                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
//                    return NO;
//                    
//                }
            }
            if (single=='.')
            {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian=YES;
                    return YES;
                }else
                {
//                    [self alertView:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            else
            {
                if (isHaveDian)//存在小数点
                {
                    //判断小数点的位数
                    NSRange ran=[textField.text rangeOfString:@"."];
                    NSUInteger tt=range.location-ran.location;
                    if (tt <= 2){
                        return YES;
                    }else{
//                        [self alertView:@"亲，您最多输入两位小数"];
                        return NO;
                    }
                }
                else
                {
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
//            [self alertView:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }  
    
}
@end
