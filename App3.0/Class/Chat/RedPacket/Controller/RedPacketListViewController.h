//
//  RedPacketListViewController.h
//  App3.0
//
//  Created by mac on 2017/6/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef NS_ENUM (NSInteger,RedPacketListType) {
    RedPacketListSend = 1,
    RedPacketListReceive
};

@interface RedPacketListViewController : XSBaseTableViewController
@property (assign, nonatomic) RedPacketListType type;
@end
