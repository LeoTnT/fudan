//
//  ChatTransferDetailViewController.m
//  App3.0
//
//  Created by mac on 2017/8/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatTransferDetailViewController.h"
#import "XSFormatterDate.h"
#import "UserInstance.h"
#import "WalletVC.h"

@interface ChatTransferDetailViewController ()
@property (strong, nonatomic) UIView *backgroudView;
@end

@implementation ChatTransferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"交易详情";
    self.autoHideKeyboard = YES;
    self.view.backgroundColor = [UIColor hexFloatColor:@"eeeeee"];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self addSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSubviews {
    _backgroudView = [[UIView alloc] init];
    [self.view addSubview:_backgroudView];
    
    UIImageView *stateImg = [[UIImageView alloc] init];
    [_backgroudView addSubview:stateImg];
    
    UILabel *stateLabel = [[UILabel alloc] init];
    stateLabel.font = [UIFont systemFontOfSize:20];
    stateLabel.textAlignment = NSTextAlignmentCenter;
    [_backgroudView addSubview:stateLabel];
    
    UILabel *moneyLabel = [[UILabel alloc] init];
    moneyLabel.text = [NSString stringWithFormat:@"¥%@",self.model.money];
    moneyLabel.font = [UIFont boldSystemFontOfSize:45];
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    [_backgroudView addSubview:moneyLabel];
    
    UILabel *tips = [[UILabel alloc] init];
    tips.text = @"1天内朋友未确认，将退还给你。";
    tips.font = [UIFont systemFontOfSize:14];
    tips.textColor = COLOR_666666;
    tips.textAlignment = NSTextAlignmentCenter;
    [_backgroudView addSubview:tips];
    
    UIButton *resend = [UIButton buttonWithType:UIButtonTypeCustom];
    [resend setTitle:@"重发转账消息" forState:UIControlStateNormal];
    [resend setTitleColor:mainColor forState:UIControlStateNormal];
    resend.titleLabel.font = [UIFont systemFontOfSize:14];
    [resend addTarget:self action:@selector(resendTransferAction:) forControlEvents:UIControlEventTouchUpInside];
    [_backgroudView addSubview:resend];
    
    UIButton *recieveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [recieveBtn setBackgroundColor:[UIColor hexFloatColor:@"e83e3e"]];
    [recieveBtn setTitle:@"确认收款" forState:UIControlStateNormal];
    [recieveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    recieveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [recieveBtn addTarget:self action:@selector(recieveTransferAction:) forControlEvents:UIControlEventTouchUpInside];
    recieveBtn.layer.masksToBounds = YES;
    recieveBtn.layer.cornerRadius = 3;
    [_backgroudView addSubview:recieveBtn];
    recieveBtn.hidden = YES;
    
    UILabel *sendTimeLabel = [[UILabel alloc] init];
    sendTimeLabel.textColor = COLOR_666666;
    sendTimeLabel.font = [UIFont systemFontOfSize:14];
    [_backgroudView addSubview:sendTimeLabel];
    
    UILabel *recTimeLabel = [[UILabel alloc] init];
    recTimeLabel.textColor = COLOR_666666;
    recTimeLabel.font = [UIFont systemFontOfSize:14];
    [_backgroudView addSubview:recTimeLabel];
    
    NSString *sendTime = [XSFormatterDate dateWithTimeIntervalString:self.model.w_time];
    sendTimeLabel.text = [NSString stringWithFormat:@"转账时间：%@", sendTime];
    if ([self.model.target_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        // 接收者是我自己
        if (self.model.status == 1) {
            // 已支付，未领取
            stateImg.image = [UIImage imageNamed:@"transfer_wait"];
            stateLabel.text = @"待确认收款";
            tips.text = @"1天内未确认，将退还给对方。";
            [resend setTitle:@"立即退还" forState:UIControlStateNormal];
            resend.hidden = NO;
            recTimeLabel.hidden = YES;
            recieveBtn.hidden = NO;
            
        } else if (self.model.status == 2) {
            // 已领取
            stateImg.image = [UIImage imageNamed:@"transfer_recieve"];
            stateLabel.text = @"已收钱";
            tips.hidden = YES;
            [resend setTitle:@"查看钱包" forState:UIControlStateNormal];
            NSString *recTime = [XSFormatterDate dateWithTimeIntervalString:self.model.u_time];
            recTimeLabel.text = [NSString stringWithFormat:@"收钱时间：%@", recTime];
        } else if (self.model.status == 3) {
            // 已退还（过期）
            stateImg.image = [UIImage imageNamed:@"transfer_back"];
            stateLabel.text = @"已退还（过期）";
            tips.hidden = YES;
            resend.hidden = YES;
            NSString *recTime = [XSFormatterDate dateWithTimeIntervalString:self.model.u_time];
            recTimeLabel.text = [NSString stringWithFormat:@"退还时间：%@", recTime];
        }
    } else {
        // 发送者是我自己
        if (self.model.status == 1) {
            // 已支付，未领取
            stateImg.image = [UIImage imageNamed:@"transfer_wait"];
            stateLabel.text = [NSString stringWithFormat:@"待%@确认收款",self.model.nickname];
            tips.text = @"1天内朋友未确认，将退还给你。";
            resend.hidden = NO;
            recTimeLabel.hidden = YES;
            
        } else if (self.model.status == 2) {
            // 已领取
            stateImg.image = [UIImage imageNamed:@"transfer_recieve"];
            stateLabel.text = [NSString stringWithFormat:@"%@已收钱",self.model.nickname];
            tips.text = @"已存入对方钱包中";
            resend.hidden = YES;
            NSString *recTime = [XSFormatterDate dateWithTimeIntervalString:self.model.u_time];
            recTimeLabel.text = [NSString stringWithFormat:@"收钱时间：%@", recTime];
        } else if (self.model.status == 3) {
            // 已退还（过期）
            stateImg.image = [UIImage imageNamed:@"transfer_fail"];
            stateLabel.text = @"已退还（过期）";
            tips.text = @"已退款到零钱";
            [resend setTitle:@"查看钱包" forState:UIControlStateNormal];
            NSString *recTime = [XSFormatterDate dateWithTimeIntervalString:self.model.u_time];
            recTimeLabel.text = [NSString stringWithFormat:@"退还时间：%@", recTime];
        }
    }
    [_backgroudView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(self.view);
    }];
    [stateImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backgroudView).offset(100);
        make.centerX.mas_equalTo(_backgroudView);
    }];
    [stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(stateImg.mas_bottom).offset(20);
        make.centerX.mas_equalTo(_backgroudView);
    }];
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(stateLabel.mas_bottom).offset(20);
        make.centerX.mas_equalTo(_backgroudView);
    }];
    [recieveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(moneyLabel.mas_bottom).offset(30);
        make.left.mas_equalTo(_backgroudView).offset(12);
        make.right.mas_equalTo(_backgroudView).offset(-12);
        make.height.mas_equalTo(50);
    }];
    [tips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(recieveBtn.hidden?moneyLabel.mas_bottom:recieveBtn.mas_bottom).offset(recieveBtn.hidden?30:10);
        make.centerX.mas_equalTo(_backgroudView).offset(resend.hidden?0:-50);
    }];
    [resend mas_makeConstraints:^(MASConstraintMaker *make) {
        tips.hidden?make.centerX.mas_equalTo(_backgroudView):make.left.mas_equalTo(tips.mas_right);
        tips.hidden?make.top.mas_equalTo(moneyLabel.mas_bottom).offset(30):make.centerY.mas_equalTo(tips);
//        make.width.mas_equalTo(90);
    }];
    [sendTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(recTimeLabel.mas_top).offset(-10);
        make.centerX.mas_equalTo(_backgroudView);
    }];
    [recTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_backgroudView).offset(-20);
        make.centerX.mas_equalTo(_backgroudView);
    }];
    
    
    
}

- (void)resendTransferAction:(UIButton *)sender {
    if ([self.model.target_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        // 接收者是我自己
        if (self.model.status == 1) {
            // 已支付，未领取
            // 立即退还
            [XSTool showProgressHUDWithView:self.view];
            [HTTPManager chatTransferReturnWithId:self.transferId success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 退款成功
                    
                    // 发送退款消息
                    NSString *text = NSLocalizedString(@"chat_transfer_back", @"");
                    
                    NSDictionary *extDic = @{MSG_TYPE:TRANSFER_MONEY_COLLECTION_REJECT,CUSTOM_MSG_ID:self.transferId,TRANSFER_BANLANCE_TAG:self.model.money};
#ifdef ALIYM_AVALABLE
                    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
                    //发送消息
                    [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                    }];
#elif defined EMIM_AVALABLE
                    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
                    // 生成message
                    EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)self.conversation).conversationId from:[UserInstance ShardInstnce].uid to:((EMConversation *)self.conversation).conversationId body:body ext:extDic];
                    message.chatType = (EMChatType)((EMConversation *)self.conversation).type;// 设置消息类型
                    
                    //发送消息
                    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                        
                    } completion:^(EMMessage *message, EMError *error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                    }];
#else
                    [XMPPSignal sendMessage:text to:((ConversationModel *)self.conversation).bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                    }];
#endif
                    
                    
                    // 刷新数据
                    [HTTPManager getTransferInfoWithId:self.transferId success:^(NSDictionary *dic, resultObject *state) {
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state.status) {
                            self.model = [TransferDetailModel mj_objectWithKeyValues:dic[@"data"]];
                            [self reloadSubviews];
                        } else {
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:state.info];
                        }
                    } fail:^(NSError *error) {
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                    }];
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            }];
            
        } else if (self.model.status == 2) {
            // 已领取
            // 查看钱包
            WalletVC *wallet = [[WalletVC alloc] init];
            [self.navigationController pushViewController:wallet animated:YES];
        } else if (self.model.status == 3) {
            // 已退还（过期）
            // 无按钮操作
        }
    } else {
        // 发送者是我自己
        if (self.model.status == 1) {
            // 已支付，未领取
            // 重发转账消息
            
#ifdef ALIYM_AVALABLE
            YWMessageBodyCustomize *oleBody = (YWMessageBodyCustomize *)((id<IYWMessage>)self.resendMessage).messageBody;
            YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:oleBody.content summary:oleBody.summary];
            //发送消息
            [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
                [XSTool showToastWithView:self.view Text:@"已重发"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }];
#elif defined EMIM_AVALABLE
            // 生成message
            EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMMessage *)self.resendMessage).to from:((EMMessage *)self.resendMessage).from to:((EMMessage *)self.resendMessage).to body:((EMMessage *)self.resendMessage).body ext:((EMMessage *)self.resendMessage).ext];
            message.chatType = ((EMMessage *)self.resendMessage).chatType;// 设置消息类型
            [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                if (!error) {
                    [XSTool showToastWithView:self.view Text:@"已重发"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
                }
            }];
#else
            NSString *text = ((UUMessage *)self.resendMessage).strRedMessage;
            NSDictionary *extDic = @{MSG_TYPE:TRANSFER_BANLANCE,CUSTOM_MSG_ID:((UUMessage *)self.resendMessage).rpId,TRANSFER_BANLANCE_TAG:((UUMessage *)self.resendMessage).moneyStr};
            
            @weakify(self);
            [XMPPSignal sendMessage:text to:((ConversationModel *)self.conversation).bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:@"已重发"];
            }];
#endif

            
//            [[EMClient sharedClient].chatManager resendMessage:self.emMessage progress:^(int progress) {
//
//            } completion:^(EMMessage *message, EMError *error) {
//                if (!error) {
//                    [XSTool showToastWithView:self.view Text:@"已重发"];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
//                }
//            }];
        } else if (self.model.status == 2) {
            // 已领取
            // 无按钮操作
            
        } else if (self.model.status == 3) {
            // 已退还（过期）
            // 查看钱包
            WalletVC *wallet = [[WalletVC alloc] init];
            [self.navigationController pushViewController:wallet animated:YES];
        }
    }

}

- (void)reloadSubviews {
    [self.backgroudView removeFromSuperview];
    [self addSubviews];
}

- (void)recieveTransferAction:(UIButton *)sender {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRedPacketWithId:self.transferId success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 确认收款成功
            [HTTPManager getTransferInfoWithId:self.transferId success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    self.model = [TransferDetailModel mj_objectWithKeyValues:dic[@"data"]];
                    [self reloadSubviews];
                } else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            }];
            
            // 发送收款消息
            NSString *text = NSLocalizedString(@"chat_transfer_rec", @"");
            
            NSDictionary *extDic = @{MSG_TYPE:TRANSFER_MONEY_COLLECTION,CUSTOM_MSG_ID:self.transferId,TRANSFER_BANLANCE_TAG:self.model.money};
            
#ifdef ALIYM_AVALABLE
            YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
            //发送消息
            [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }];
#elif defined EMIM_AVALABLE
            EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
            // 生成message
            EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)self.conversation).conversationId from:[UserInstance ShardInstnce].uid to:((EMConversation *)self.conversation).conversationId body:body ext:extDic];
            message.chatType = (EMChatType)((EMConversation *)self.conversation).type;// 设置消息类型
            
            //发送消息
            [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                
            } completion:^(EMMessage *message, EMError *error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
            }];
#else
            [XMPPSignal sendMessage:text to:((ConversationModel *)self.conversation).bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
                
            }];
#endif
            
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

@end
