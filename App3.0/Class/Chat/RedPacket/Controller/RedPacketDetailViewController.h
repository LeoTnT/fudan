//
//  RedPacketDetailViewController.h
//  App3.0
//
//  Created by mac on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "RedPacketModel.h"

@interface RedPacketDetailViewController : XSBaseTableViewController
@property (strong, nonatomic) RedPacketDetailModel *model;
@property (strong, nonatomic) id conversation;
@end
