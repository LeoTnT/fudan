//
//  ChatTransferViewController.m
//  App3.0
//
//  Created by mac on 2017/8/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChatTransferViewController.h"
#import "OrderPayViewController.h"
#import "RedPacketModel.h"

@interface ChatTransferViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) UITextField *moneyTF;
@property (strong, nonatomic) UILabel *remarkLabel;
@property (strong, nonatomic) UIButton *remarkBtn;
@property (copy, nonatomic) NSString *transferId;
@end

@implementation ChatTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.navigationItem.title = Localized(@"转账");
    self.autoHideKeyboard = YES;
    self.view.backgroundColor = [UIColor hexFloatColor:@"eeeeee"];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self addSubviews];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.moneyTF becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSubviews {
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = BG_COLOR;
    [self.view addSubview:bgView];
    
    UIImageView *avatar = [[UIImageView alloc] init];
    [avatar getImageWithUrlStr:self.userInfo.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    [bgView addSubview:avatar];
    
    UILabel *nickName = [[UILabel alloc] init];
    nickName.text = self.userInfo.nickname;
    nickName.font = [UIFont systemFontOfSize:14];
    nickName.textColor = COLOR_666666;
    nickName.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:nickName];
    
    UIView *transBgView = [[UIView alloc] init];
    transBgView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:transBgView];
    
    UILabel *transTitle = [[UILabel alloc] init];
    transTitle.text = @"转账金额";
    transTitle.font = [UIFont systemFontOfSize:14];
    [transBgView addSubview:transTitle];
    
    UILabel *unit = [[UILabel alloc] init];
    unit.text = @"¥";
    unit.font = [UIFont boldSystemFontOfSize:20];
    [transBgView addSubview:unit];
    
    _moneyTF = [[UITextField alloc] init];
    _moneyTF.delegate = self;
    _moneyTF.font = [UIFont boldSystemFontOfSize:40];
    _moneyTF.borderStyle = UITextBorderStyleNone;
    _moneyTF.keyboardType = UIKeyboardTypeDecimalPad;
    [_moneyTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [transBgView addSubview:_moneyTF];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = BG_COLOR;
    [transBgView addSubview:lineView];
    
    _remarkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_remarkBtn setTitle:@"添加转账说明" forState:UIControlStateNormal];
    [_remarkBtn setTitleColor:mainColor forState:UIControlStateNormal];
    _remarkBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_remarkBtn addTarget:self action:@selector(remarkAction:) forControlEvents:UIControlEventTouchUpInside];
    [transBgView addSubview:_remarkBtn];
    
    _remarkLabel = [[UILabel alloc] init];
    _remarkLabel.text = @"";
    _remarkLabel.font = [UIFont systemFontOfSize:14];
    _remarkLabel.textColor = COLOR_666666;
    [transBgView addSubview:_remarkLabel];
    
    
    UIButton *transBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    transBtn.backgroundColor = [UIColor hexFloatColor:@"e83e3e"];
    [transBtn setTitle:Localized(@"转账") forState:UIControlStateNormal];
    [transBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    transBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [transBtn addTarget:self action:@selector(transAction:) forControlEvents:UIControlEventTouchUpInside];
    [transBgView addSubview:transBtn];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(14);
        make.right.mas_equalTo(self.view).offset(-14);
        make.top.mas_equalTo(self.view).offset(20);
        make.bottom.mas_equalTo(transBgView);
    }];
    [avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView).offset(5);
        make.centerX.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(avatar.mas_bottom).offset(10);
        make.centerX.mas_equalTo(bgView);
    }];
    [transBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(nickName.mas_bottom).offset(20);
        make.left.right.mas_equalTo(bgView);
        make.bottom.mas_equalTo(transBtn).offset(40);
    }];
    [transTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(transBgView).offset(20);
        make.left.mas_equalTo(transBgView).offset(30);
    }];
    [unit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(transTitle.mas_bottom).offset(30);
        make.left.mas_equalTo(transTitle);
        make.width.mas_equalTo(20);
    }];
    [_moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(unit).offset(-5);
        make.left.mas_equalTo(unit.mas_right);
        make.right.mas_equalTo(transBgView).offset(-30);
    }];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_moneyTF.mas_bottom).offset(3);
        make.left.mas_equalTo(transBgView).offset(30);
        make.right.mas_equalTo(transBgView).offset(-30);
        make.height.mas_equalTo(1);
    }];
    [_remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_remarkBtn);
        make.left.mas_equalTo(transTitle);
    }];
    [_remarkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).offset(5);
        make.left.mas_equalTo(_remarkLabel.mas_right);
    }];
    [transBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_remarkBtn.mas_bottom).offset(20);
        make.centerX.mas_equalTo(transBgView);
        make.left.mas_equalTo(transBgView).offset(30);
        make.right.mas_equalTo(transBgView).offset(-30);
        make.height.mas_equalTo(50);
    }];
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length > 10) {
        textField.text = [textField.text substringToIndex:10];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%@====%@",textField.text,_moneyTF.text);
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location==NSNotFound) {
        isHaveDian=NO;
    }
    if ([string length]>0)
    {
        unichar single=[string characterAtIndex:0];//当前输入的字符
        if ((single >='0' && single<='9') || single=='.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length]==0){
                if(single == '.'){
                    //                    [self alertView:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
                //                if (single == '0') {
                ////                    [self alertView:@"亲，第一个数字不能为0"];
                //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                //                    return NO;
                //
                //                }
            }
            if (single=='.')
            {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian=YES;
                    return YES;
                }else
                {
                    //                    [self alertView:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            else
            {
                if (isHaveDian)//存在小数点
                {
                    //判断小数点的位数
                    NSRange ran=[textField.text rangeOfString:@"."];
                    NSUInteger tt=range.location-ran.location;
                    if (tt <= 2){
                        return YES;
                    }else{
                        //                        [self alertView:@"亲，您最多输入两位小数"];
                        return NO;
                    }
                }
                else
                {
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            //            [self alertView:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }  
    
}

- (void)remarkAction:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"添加转账说明" message:nil preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alert) weakAlert = alert;
    @weakify(self);
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        //获取输入的昵称
        NSLog(@"点击了确定按钮%@", [weakAlert.textFields.lastObject text]);
        if ([weakAlert.textFields.lastObject text].length > 10) {
            [XSTool showToastWithView:self.view Text:@"最多输入10个字"];
        } else {
            self.remarkLabel.text = [weakAlert.textFields.lastObject text];
            
            if (isEmptyString(self.remarkLabel.text)) {
                [self.remarkBtn setTitle:@"添加转账说明" forState:UIControlStateNormal];
            } else {
                [self.remarkBtn setTitle:@"修改" forState:UIControlStateNormal];
            }
        }
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"点击了取消按钮");
    }]];
    // 添加文本框
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"最多输入10个字";
        textField.text = self.remarkLabel.text;
    }];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}

- (void)transAction:(UIButton *)sender {
    [self.view endEditing:YES];
    NSString *cId;
#ifdef ALIYM_AVALABLE
    cId = ((YWConversation *)self.conversation).conversationId;
    if (((YWConversation *)self.conversation).conversationType == YWConversationTypeP2P) {
        cId = ((YWP2PConversation *)self.conversation).person.personId;
    } else {
        cId = ((YWTribeConversation *)self.conversation).tribe.tribeId;
    }
#elif defined EMIM_AVALABLE
    cId = ((EMConversation *)self.conversation).conversationId;
#else
    cId = ((ConversationModel *)self.conversation).bareJID.user;
#endif
    NSDictionary *params = @{@"target_id":cId,@"money":self.moneyTF.text,@"remark":self.remarkLabel.text};
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager sendTransferWithParams:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        @strongify(self);
        if (state.status) {
            OrderPayViewController *opVC = [[OrderPayViewController alloc] init];
            opVC.tableName = dic[@"data"][@"tname"];
            opVC.orderId = dic[@"data"][@"id"];
            opVC.parentVC = self;
            self.transferId = dic[@"data"][@"id"];
            [self.navigationController pushViewController:opVC animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)canSendTransfer {
    // 发送转账消息
    
    // 拓展
    NSDictionary *extDic = @{MSG_TYPE:TRANSFER_BANLANCE,CUSTOM_MSG_ID:self.transferId,TRANSFER_BANLANCE_TAG:[NSString stringWithFormat:@"%.2f",[self.moneyTF.text floatValue]]};
    
    // 转账文字
    NSString *text = isEmptyString(self.remarkLabel.text)? [NSString stringWithFormat:@"转账给%@",self.userInfo.nickname]:self.remarkLabel.text;
#ifdef ALIYM_AVALABLE
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:text];
    @weakify(self);
    //发送消息
    [((YWConversation *)self.conversation) asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
        @strongify(self);
        [self performSelector:@selector(popView) withObject:nil afterDelay:0];
    }];
#elif defined EMIM_AVALABLE
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
    
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:((EMConversation *)self.conversation).conversationId from:[UserInstance ShardInstnce].uid to:((EMConversation *)self.conversation).conversationId body:body ext:extDic];
    message.chatType = (EMChatType)((EMConversation *)self.conversation).type;// 设置消息类型
    
    @weakify(self);
    //发送消息
    [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
        
    } completion:^(EMMessage *message, EMError *error) {
        @strongify(self);
        [self performSelector:@selector(popView) withObject:nil afterDelay:0];
    }];
#else
    @weakify(self);
    [XMPPSignal sendMessage:text to:((ConversationModel *)self.conversation).bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
        @strongify(self);
        [self performSelector:@selector(popView) withObject:nil afterDelay:0];
    }];
#endif
    
}

- (void)popView {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 1] animated:YES];
}
@end
