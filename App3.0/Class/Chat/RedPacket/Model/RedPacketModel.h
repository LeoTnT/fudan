//
//  RedPacketModel.h
//  App3.0
//
//  Created by mac on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPManager+RedPacketApi.h"

@interface RedPacketListDataMineModel : NSObject
@property (nonatomic, copy) NSString *date_rec;    //红包日期(M-d) 接收
@property (nonatomic, copy) NSString *date_send;    //红包日期(M-d) 发送
@property (nonatomic, assign) unsigned int type;    //红包类型(1定额|2随机)
@property (nonatomic, copy) NSString *money;    //领取金额
@property (nonatomic, copy) NSString *nickname;    //发红包人昵称
@property (nonatomic, assign) unsigned int count_rec;    //已领红包个数
@property (nonatomic, assign) unsigned int count;    //红包个数
@end

@interface RedPacketListMineModel : NSObject
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *total;
@end

@interface RedPacketMineModel : NSObject
@property (nonatomic, copy) NSString *year;    //年份
@property (nonatomic, copy) NSString *avatar;    //我的头像
@property (nonatomic, copy) NSString *nickname;    //我的昵称
@property (nonatomic, copy) NSString * money_all;    //红包总额
@property (nonatomic, assign) unsigned int count_all;    //红包总数
@property (nonatomic, assign) unsigned int count_best;    //最佳红包总数
@property (nonatomic, assign) unsigned int type;    //类型（0发出的|1接收的）
@property (nonatomic, strong) RedPacketListMineModel *list;    //红包记录
@end

@interface RedPacketPersonModel : NSObject
@property (nonatomic, copy) NSString *avatar;    //拆红包用户头像
@property (nonatomic, copy) NSString *nickname;    //拆红包用户昵称
@property (nonatomic, copy) NSString *money;    //拆红包获得金额
@end

@interface RedPacketPersonListModel : NSObject
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, copy) NSString *per_page;
@property (nonatomic, copy) NSString *total;
@end

@interface RedPacketDetailModel : NSObject
@property (nonatomic, copy) NSString *uid;    //发红包用户id
@property (nonatomic, copy) NSString *avatar;    //发红包用户头像
@property (nonatomic, copy) NSString *nickname;    //发红包用户昵称
@property (nonatomic, copy) NSString *remark;    //红包备注
@property (nonatomic, assign) unsigned int type;    //红包类型(1定额|2随机)
@property (nonatomic, assign) unsigned int status;    //红包状态(0未支付|1已支付|2领完|3退回)
@property (nonatomic, copy) NSString *money_me;    //我领取金额
@property (nonatomic, assign) unsigned int count_rec;    //已领红包个数
@property (nonatomic, assign) unsigned int count;    //红包总数
@property (nonatomic, assign) unsigned int is_can_rec;    //我是否可领
@property (nonatomic, assign) unsigned int is_received;    //我是否领过
@property (nonatomic, copy) NSString *money_rec;    //已领红包金额
@property (nonatomic, copy) NSString *money;    //红包总额
@property (nonatomic, strong) NSArray *list;    //拆红包记录
@end

@interface TransferDetailModel : NSObject
@property (nonatomic, assign) unsigned int status;    //红包状态(0未支付|1已支付|2领完|3退回)
@property (nonatomic, copy) NSString *target_id;    //接收者用户id
@property (nonatomic, copy) NSString *nickname;    //接收者昵称
@property (nonatomic, copy) NSString *money;    //转账总额
@property (nonatomic, copy) NSString *w_time;    //发起时间
@property (nonatomic, copy) NSString *u_time;    //更新时间
@end

@interface LostRedPacketModel : NSObject
@property (nonatomic, assign) unsigned int status;    //红包状态(0未支付|1已支付|2领完|3退回)
@property (nonatomic, assign) unsigned int is_transfer;    //是否转账
@property (nonatomic, assign) unsigned int type;    //红包类型(1定额|2随机)
@property (nonatomic, assign) unsigned int target;    //1个人|2群组
@property (nonatomic, assign) unsigned int target_id;    //个人id|群组id
@property (nonatomic, assign) unsigned int flag;    //1待领取确认|2被领取确认
@property (nonatomic, copy) NSString *red_packet_id;    //记录id
@property (nonatomic, copy) NSString *user_id;    //发送者用户id
@property (nonatomic, copy) NSString *nickname;    //发送者用户昵称
@property (nonatomic, copy) NSString *logo;    //发送者用户头像
@property (nonatomic, copy) NSString *remark;    //备注
@property (nonatomic, copy) NSString *money;    //总额
@property (nonatomic, copy) NSString *from_uid;    //来源用户
@property (nonatomic, copy) NSString *from_nickname;    //来源用户昵称
@end

@interface RedPacketModel : NSObject

@end
