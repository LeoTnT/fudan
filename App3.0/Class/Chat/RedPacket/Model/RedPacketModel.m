//
//  RedPacketModel.m
//  App3.0
//
//  Created by mac on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketModel.h"

@implementation RedPacketListDataMineModel

@end

@implementation RedPacketListMineModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"RedPacketListDataMineModel",
             };
}
@end

@implementation RedPacketMineModel

@end

@implementation RedPacketPersonModel

@end

@implementation RedPacketPersonListModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"RedPacketPersonModel",
             };
}
@end

@implementation RedPacketDetailModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"list" : @"RedPacketPersonModel",
             };
}
@end

@implementation TransferDetailModel

@end

@implementation LostRedPacketModel

@end

@implementation RedPacketModel

@end
