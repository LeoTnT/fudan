//
//  HTTPManager+RedPacketApi.m
//  App3.0
//
//  Created by mac on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HTTPManager+RedPacketApi.h"

@implementation HTTPManager (RedPacketApi)

/*
 我领到的红包
 timestamp_year	时间
 */
+ (void)getRedPacketReceiveListWithYear:(NSString *)timestamp_year success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_GetReceiveList parameters:@{@"timestamp_year":timestamp_year} success:success failure:fail];
}

/*
 我发出的红包
 timestamp_year	时间
 */
+ (void)getRedPacketSendListWithYear:(NSString *)timestamp_year success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_GetSendList parameters:@{@"timestamp_year":timestamp_year} success:success failure:fail];
}

/*
 领红包
 id	必须	int	红包id
 */
+ (void)getRedPacketWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_Receive parameters:@{@"id":rp_id} success:success failure:fail];
}

+ (void)getGroupRedPacketWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_QuickReceive parameters:@{@"id":rp_id} success:success failure:fail];
}

+ (void)updateWalletWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_UpdateWallet parameters:nil success:success failure:fail];
}

/*
 发红包
 target     必须	int     目标类型(1个人|2群组)
 target_id	必须	int     目标id
 type       必须	int     金额类型(1定额|2随机)
 count      必须	int     红包个数
 money      必须	float	红包总额
 remark         string	红包备注
 */
+ (void)sendRedPacketWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_Send parameters:params success:success failure:fail];
}

/*
 待确认红包列表
 */
+ (void)getRedPacketConfirmListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_ConfirmList parameters:nil success:success failure:fail];
}

/*
 确认红包被领取
 */
+ (void)confirmReceiveRedPacketWithId:(NSString *)rid receiver:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_ConfirmReceive parameters:@{@"rid":rid,@"uid":uid} success:success failure:fail];
}

/*
 确认红包已发到
 */
+ (void)confirmSendRedPacketWithId:(NSString *)rid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_ConfirmSend parameters:@{@"rid":rid} success:success failure:fail];
}

/*
 红包详情
 */
+ (void)getRedPacketDetailWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_RedPacket_GetInfo parameters:@{@"id":rp_id} success:success failure:fail];
}

+ (void)sendTransferWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_Transfer_Send parameters:params success:success failure:fail];
}

+ (void)getTransferInfoWithId:(NSString *)trans_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_Transfer_GetInfo parameters:@{@"id":trans_id} success:success failure:fail];
}

+ (void)chatTransferReturnWithId:(NSString *)trans_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_Transfer_Return parameters:@{@"id":trans_id} success:success failure:fail];
}

@end
