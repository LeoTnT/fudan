//
//  HTTPManager+RedPacketApi.h
//  App3.0
//
//  Created by mac on 2017/8/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"

// 我领到的红包
#define Url_RedPacket_GetReceiveList            @"/api/v1/user/redpacket/GetReceiveList"
// 我发出的红包
#define Url_RedPacket_GetSendList             @"/api/v1/user/redpacket/GetSendList"
// 领红包
#define Url_RedPacket_Receive             @"/api/v1/user/redpacket/Receive"
// 发红包
#define Url_RedPacket_Send             @"/api/v1/user/redpacket/Send"
// 待确认红包列表
#define Url_RedPacket_ConfirmList             @"/api/v1/user/redpacket/ConfirmList"
// 确认红包被领取
#define Url_RedPacket_ConfirmReceive             @"/api/v1/user/redpacket/ConfirmReceive"
// 确认红包已发到
#define Url_RedPacket_ConfirmSend            @"/api/v1/user/redpacket/ConfirmSend"
// 红包详情
#define Url_RedPacket_GetInfo             @"/api/v1/user/redpacket/GetInfo"

// 聊天转账
#define Url_Transfer_Send             @"/api/v1/user/redpacket/Transfer"

// 聊天转账详情
#define Url_Transfer_GetInfo             @"/api/v1/user/redpacket/GetInfoTransfer"

// 聊天转账退款
#define Url_Transfer_Return           @"/api/v1/user/redpacket/TransferReturn"

/* 新的红包接口 
 */
// 新领红包
#define Url_RedPacket_QuickReceive           @"/api/v1/user/redpacket/QuickReceive"
// 更新已经登录户钱包|转账
#define Url_RedPacket_UpdateWallet          @"/api/v1/user/redpacket/UpdateWallet"


@interface HTTPManager (RedPacketApi)

/*
 我领到的红包
 timestamp_year	时间
 */
+ (void)getRedPacketReceiveListWithYear:(NSString *)timestamp_year success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 我发出的红包
 timestamp_year	时间
 */
+ (void)getRedPacketSendListWithYear:(NSString *)timestamp_year success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 领红包
 id	必须	int	红包id
 */
+ (void)getRedPacketWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 新领红包（目前群用）
 id	必须	int	红包id
 */
+ (void)getGroupRedPacketWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 更新已经登录户钱包
 */
+ (void)updateWalletWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 发红包
 target     必须	int     目标类型(1个人|2群组)
 target_id	必须	int     目标id
 type       必须	int     金额类型(1定额|2随机)
 count      必须	int     红包个数
 money      必须	float	红包总额
 remark         string	红包备注
 */
+ (void)sendRedPacketWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 待确认红包列表，丢失的红包列表
 返回红包详情
 
 */
+ (void)getRedPacketConfirmListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 确认红包被领取
 id_str	必须	string	红包id
 */
+ (void)confirmReceiveRedPacketWithId:(NSString *)rid receiver:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 确认红包已发到
 id_str	必须	string	红包id
 */
+ (void)confirmSendRedPacketWithId:(NSString *)rid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 红包详情
 id	必须	int	红包id
 */
+ (void)getRedPacketDetailWithId:(NSString *)rp_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 发送转账
 target_id	必须	int	目标id
 money      必须	float	总额
 remark     可选	string	备注
 */
+ (void)sendTransferWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 转账详情
 status     int	状态(0未支付|1已支付|2领完|3退回)
 nickname	string	接收者昵称
 money      float	总额
 w_time     int	发起时间
 u_time     int	更新时间
 */
+ (void)getTransferInfoWithId:(NSString *)trans_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 聊天转账退款
 id	必须	int	记录id
 */
+ (void)chatTransferReturnWithId:(NSString *)trans_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


@end
