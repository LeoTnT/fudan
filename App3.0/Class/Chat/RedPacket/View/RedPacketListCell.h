//
//  RedPacketListCell.h
//  App3.0
//
//  Created by mac on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedPacketModel.h"

@interface RedPacketListCell : UITableViewCell

+ (instancetype)redPacketListCellWithTableView:(UITableView *)tableView model:(RedPacketListDataMineModel *)model;
@end
