//
//  OpenRedPacketView.h
//  App3.0
//
//  Created by mac on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedPacketModel.h"

@interface OpenRedPacketView : UIView
+(instancetype)creatViewWithRedpacketId:(NSString *)rpId model:(RedPacketDetailModel *)model viewController:(UIViewController *)vc;

- (void)dismiss;
@end
