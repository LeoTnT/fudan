//
//  OpenRedPacketView.m
//  App3.0
//
//  Created by mac on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OpenRedPacketView.h"

@interface OpenRedPacketView()
@property (strong, nonatomic) UIViewController *viewController;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, strong) RedPacketDetailModel *model;
@property (nonatomic, copy) NSString *rpId;
@end

@implementation OpenRedPacketView

+ (instancetype)creatViewWithRedpacketId:(NSString *)rpId model:(RedPacketDetailModel *)model viewController:(UIViewController *)vc {
    return [[self alloc] initWithRedpacketId:rpId model:model viewController:vc];
}

- (instancetype)initWithRedpacketId:(NSString *)rpId model:(RedPacketDetailModel *)model viewController:(UIViewController *)vc {
    if (self = [super init]) {
        self.rpId = rpId;
        self.model = model;
        self.viewController = vc;
        [self creatBgView];
        [self creatContentView];
    }
    return self;
}

- (void)creatBgView {
    self.window = [UIApplication sharedApplication].keyWindow;
    self.parentView = [UIView new];
    self.parentView.frame = self.window.bounds;
    self.frame = self.window.bounds;
    self.parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3];
    [self.window addSubview:self.parentView];
    [self.parentView addSubview:self];
}

- (void)creatContentView {
    UIImageView *bgView = [[UIImageView alloc] init];
    [bgView setImage:[UIImage imageNamed:@"rp_bg"]];
    bgView.userInteractionEnabled = YES;
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 3;
    [self addSubview:bgView];
    
    UIImageView *avatar = [[UIImageView alloc] init];
    [avatar getImageWithUrlStr:self.model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    avatar.layer.masksToBounds = YES;
    avatar.layer.cornerRadius = 25;
    [bgView addSubview:avatar];
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.text = self.model.nickname;
    nameLabel.font = [UIFont systemFontOfSize:12];
    nameLabel.textColor = [UIColor hexFloatColor:@"fff6e6"];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:nameLabel];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"给你发了一个红包";
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [UIColor hexFloatColor:@"fff6e6"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:titleLabel];
    
    UILabel *desLabel = [[UILabel alloc] init];
    desLabel.text = self.model.remark;
    desLabel.textColor = [UIColor hexFloatColor:@"ffd999"];
    desLabel.textAlignment = NSTextAlignmentCenter;
    desLabel.numberOfLines = 2;
    desLabel.font = [UIFont systemFontOfSize:24];
    [bgView addSubview:desLabel];
    
    UIButton *openBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [openBtn setBackgroundImage:[UIImage imageNamed:@"rp_open"] forState:UIControlStateNormal];
    [openBtn setBackgroundImage:[UIImage imageNamed:@"rp_open"] forState:UIControlStateDisabled];
    [openBtn addTarget:self action:@selector(openAction:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:openBtn];
    
    UIButton *openDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [openDetailBtn setTitle:@"看看大家的手气>" forState:UIControlStateNormal];
    [openDetailBtn setTitleColor:[UIColor hexFloatColor:@"ffd999"] forState:UIControlStateNormal];
    openDetailBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [openDetailBtn addTarget:self action:@selector(openDetailAction:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:openDetailBtn];
    openDetailBtn.hidden = YES;
    
    UIButton *offBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [offBtn setBackgroundImage:[UIImage imageNamed:@"rp_off"] forState:UIControlStateNormal];
    [offBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:offBtn];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
        make.left.lessThanOrEqualTo(self).offset(20);
        make.right.lessThanOrEqualTo(self).offset(-20);
    }];
    [avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(bgView).offset(44.5);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(avatar.mas_bottom).offset(5);
    }];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(nameLabel.mas_bottom).offset(20.5);
    }];
    [desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(20);
        make.right.mas_equalTo(bgView).offset(-20);
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(titleLabel.mas_bottom).offset(30);
    }];
    
    [openDetailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.bottom.mas_equalTo(bgView).offset(-20.5);
    }];
    [openBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.bottom.mas_equalTo(openDetailBtn.mas_top).offset(-43);
    }];
    
    [offBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView).offset(10);
        make.right.mas_equalTo(bgView).offset(-10);
    }];
    
    if (self.model.status == 2) {
        titleLabel.hidden = YES;
        openBtn.hidden = YES;
        desLabel.text = @"手慢了，红包被领完";
        openDetailBtn.hidden = NO;
    } else if (self.model.status == 3) {
        titleLabel.hidden = YES;
        openBtn.hidden = YES;
        desLabel.text = @"超过24小时未领取，红包已被退回";
        openDetailBtn.hidden = NO;
    }
}

- (void)openAction:(UIButton *)sender {
    // 设置按键不可点击
    sender.enabled = NO;
    
    CABasicAnimation *animation =  [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
                                    //默认是顺时针效果，若将fromValue和toValue的值互换，则为逆时针效果
                                    animation.fromValue = [NSNumber numberWithFloat:0.f];
                                    animation.toValue =  [NSNumber numberWithFloat: M_PI *2];
                                    animation.duration  = 1.5;
                                    animation.autoreverses = NO;
                                    animation.fillMode =kCAFillModeForwards;
                                    animation.repeatCount = MAXFLOAT; //如果这里想设置成一直自旋转，可以设置为MAXFLOAT，否则设置具体的数值则代表执行多少次
                                    [sender.layer addAnimation:animation forKey:nil];
#ifdef ALIYM_AVALABLE
    [((YWChatViewController *)self.viewController) openRedPacketWithId:self.rpId];
#elif defined EMIM_AVALABLE
    [((ChatViewController *)self.viewController) openRedPacketWithId:self.rpId];
#else
    [((BaseChatController *)self.viewController) openRedPacketWithId:self.rpId];
#endif
    
}

- (void)openDetailAction:(UIButton *)sender {
    // 设置按键不可点击
    sender.enabled = NO;
    
#ifdef ALIYM_AVALABLE
    [((YWChatViewController *)self.viewController) detailRedPacketWithId:self.rpId];
#elif defined EMIM_AVALABLE
    [((ChatViewController *)self.viewController) detailRedPacketWithId:self.rpId];
#else
    [((BaseChatController *)self.viewController) detailRedPacketWithId:self.rpId];
#endif
}

- (void)dismiss {
    [self removeFromSuperview];
    [self.parentView removeFromSuperview];
    self.window = nil;
}
@end
