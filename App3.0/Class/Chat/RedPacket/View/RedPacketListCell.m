//
//  RedPacketListCell.m
//  App3.0
//
//  Created by mac on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketListCell.h"

@interface RedPacketListCell()
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *money;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *desc;
@property (strong, nonatomic) UIImageView *typeImg;
@end

@implementation RedPacketListCell

+ (instancetype)redPacketListCellWithTableView:(UITableView *)tableView model:(RedPacketListDataMineModel *)model {
    static NSString *cellidentifier =@"redPacketListCell";
    RedPacketListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (!cell) {
        cell = [[RedPacketListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    [cell setupModelData:model];
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:15];
        [self addSubview:_title];
        
        _money = [[UILabel alloc] init];
        _money.font = [UIFont systemFontOfSize:15];
        [self addSubview:_money];
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.textColor = COLOR_999999;
        _dateLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:_dateLabel];
        
        _desc = [[UILabel alloc] init];
        _desc.textColor = COLOR_999999;
        _desc.font = [UIFont systemFontOfSize:15];
        _desc.hidden = YES;
        [self addSubview:_desc];
        
        _typeImg = [[UIImageView alloc] init];
        _typeImg.image = [UIImage imageNamed:@"rp_pin"];
        [self addSubview:_typeImg];
        _typeImg.hidden = YES;
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(11.5);
            make.left.mas_equalTo(self).offset(14.5);
        }];
        [_typeImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_title.mas_right);
            make.centerY.mas_equalTo(_title);
        }];
        [_money mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_title);
            make.right.mas_equalTo(self).offset(-14.5);
        }];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self).offset(-12);
            make.left.mas_equalTo(_title);
        }];
        [_desc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_dateLabel);
            make.right.mas_equalTo(_money);
        }];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor hexFloatColor:@"d9d9d9"];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

- (void)setupModelData:(RedPacketListDataMineModel *)model {
    
    self.money.text = model.money;
    if (model.date_send.length > 0) {
        self.title.text = model.type==1?@"普通红包":@"拼手气红包";
        self.dateLabel.text = model.date_send;
        self.desc.hidden = NO;
        _typeImg.hidden = YES;
        if (model.count == model.count_rec) {
            self.desc.text = [NSString stringWithFormat:@"已领完%d/%d个",model.count_rec,model.count];
        } else {
            self.desc.text = [NSString stringWithFormat:@"%d/%d个",model.count_rec,model.count];
        }
        
    } else if (model.date_rec.length > 0) {
        self.title.text = model.nickname;
        self.dateLabel.text = model.date_rec;
        self.desc.hidden = YES;
        _typeImg.hidden = (model.type==1?YES:NO);
    }
}
@end
