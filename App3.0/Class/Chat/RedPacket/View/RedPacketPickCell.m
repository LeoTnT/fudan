//
//  RedPacketPickCell.m
//  
//
//  Created by mac on 2017/6/26.
//
//

#import "RedPacketPickCell.h"
#import "RedPacketModel.h"

@interface RedPacketPickCell ()
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *money;
@end

@implementation RedPacketPickCell

+ (instancetype)redPacketPickCellWithTableView:(UITableView *)tableView model:(RedPacketPersonModel *)model{
    static NSString *cellidentifier =@"redPacketDetailPickCell";
    RedPacketPickCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (!cell) {
        cell = [[RedPacketPickCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    [cell setupModel:model];
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _avatar = [[UIImageView alloc] init];;
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 3;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:16];
        [self addSubview:_title];
        
        _money = [[UILabel alloc] init];
        _money.font = [UIFont systemFontOfSize:16];
        [self addSubview:_money];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_avatar.mas_right).offset(10);
            make.centerY.mas_equalTo(self);
        }];
        
        [_money mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-15);
            make.centerY.mas_equalTo(self);
        }];
    }
    return self;
}

- (void)setupModel:(RedPacketPersonModel *)model {
    [self.avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.title.text = model.nickname;
    self.money.text = [NSString stringWithFormat:@"%@元",model.money];
}
@end
