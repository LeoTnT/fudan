//
//  RedPacketListTopCell.h
//  App3.0
//
//  Created by mac on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedPacketModel.h"

@protocol RedPacketListTopCellDelegate <NSObject>

- (void)chooseYearClick;

@end

@interface RedPacketListTopCell : UITableViewCell

+ (instancetype)redPacketListTopCellWithTableView:(UITableView *)tableView model:(RedPacketMineModel *)model;

@property (weak, nonatomic) id<RedPacketListTopCellDelegate>delegate;
@end
