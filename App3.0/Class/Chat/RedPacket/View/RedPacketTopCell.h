//
//  RedPacketTopCell.h
//  App3.0
//
//  Created by mac on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedPacketModel.h"

@protocol RedPacketTopCellDelegate <NSObject>

- (void)checkWalletClick;

@end

@interface RedPacketTopCell : UITableViewCell

+ (instancetype)redPacketTopCellWithTableView:(UITableView *)tableView model:(RedPacketDetailModel *)model conversation:(id)conversation;

/**
 *  传入每一行cell数据，返回行高，提供接口
 *
 *  @param tableView 当前展示的tableView
 *  @param object cell的展示数据内容
 */
+ (CGFloat)tableView:(UITableView *)tableView rowHeightForObject:(id)object;

@property (weak, nonatomic) id<RedPacketTopCellDelegate>delegate;
@end
