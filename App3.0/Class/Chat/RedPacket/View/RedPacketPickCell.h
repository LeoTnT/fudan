//
//  RedPacketPickCell.h
//  
//
//  Created by mac on 2017/6/26.
//
//

#import <UIKit/UIKit.h>
@class RedPacketPersonModel;
@interface RedPacketPickCell : UITableViewCell
+ (instancetype)redPacketPickCellWithTableView:(UITableView *)tableView model:(RedPacketPersonModel *)model;
@end
