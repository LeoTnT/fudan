//
//  RedPacketTopCell.m
//  App3.0
//
//  Created by mac on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketTopCell.h"

@interface RedPacketTopCell()
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *desc;
@property (strong, nonatomic) UILabel *money;
@property (strong, nonatomic) UILabel *unit;
@property (strong, nonatomic) UIButton *withdraw;
@end

@implementation RedPacketTopCell

+ (instancetype)redPacketTopCellWithTableView:(UITableView *)tableView model:(RedPacketDetailModel *)model conversation:(id)conversation {
    static NSString *cellidentifier =@"redPacketDetailTopCell";
    RedPacketTopCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (!cell) {
        cell = [[RedPacketTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    [cell setupModel:model conversation:conversation];
    return cell;
}

+ (CGFloat)tableView:(UITableView *)tableView rowHeightForObject:(id)object
{
    return 300;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
        UIImageView *bgView = [[UIImageView alloc] init];
        [bgView setImage:[UIImage imageNamed:@"rp_top_bg"]];
        [self addSubview:bgView];
        
        _avatar = [[UIImageView alloc] init];
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 30;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:15];
        [self addSubview:_title];
        
        _desc = [[UILabel alloc] init];
        _desc.textColor = COLOR_999999;
        _desc.font = [UIFont systemFontOfSize:14];
        [self addSubview:_desc];
        
        _money = [[UILabel alloc] init];
        _money.text = @"0.00";
        _money.font = [UIFont systemFontOfSize:55];
        [self addSubview:_money];
        _money.hidden = YES;
        
        _unit = [[UILabel alloc] init];
        _unit.text = @"元";
        _unit.font = [UIFont systemFontOfSize:14];
        [self addSubview:_unit];
        _unit.hidden = YES;
        
        _withdraw = [UIButton buttonWithType:UIButtonTypeCustom];
        [_withdraw setTitle:@"已存入钱包，可直接提现" forState:UIControlStateNormal];
        [_withdraw setTitleColor:[UIColor hexFloatColor:@"6177a8"] forState:UIControlStateNormal];
        _withdraw.titleLabel.font = [UIFont systemFontOfSize:12];
        [_withdraw addTarget: self action:@selector(checkWallet) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_withdraw];
        _withdraw.hidden = YES;
        
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self);
        }];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bgView.mas_bottom).offset(-30);
            make.centerX.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(60, 60));
        }];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_avatar.mas_bottom).offset(3.5);
            make.centerX.mas_equalTo(self);
        }];
        [_desc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_title.mas_bottom).offset(9);
            make.centerX.mas_equalTo(self);
        }];
        [_money mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_desc.mas_bottom).offset(31.5);
            make.centerX.mas_equalTo(self);
        }];
        [_unit mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_money).offset(-10);
            make.left.mas_equalTo(_money.mas_right);
        }];
        [_withdraw mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_unit.mas_bottom).offset(22.5);
            make.centerX.mas_equalTo(self);
        }];
    }
    return self;
}

- (void)setupModel:(RedPacketDetailModel *)model conversation:(id)conversation {
    [self.avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.title.text = model.nickname;
    self.desc.text = model.remark;
#ifdef ALIYM_AVALABLE
    if (((YWConversation *)conversation).conversationType == YWConversationTypeP2P) {
#elif defined EMIM_AVALABLE
    if (((EMConversation *)conversation).type == EMConversationTypeChat) {
#else
    if (((ConversationModel *)conversation).chatType == UUChatTypeChat) {
#endif
        self.money.text = model.money;
        if ([model.uid isEqualToString:[UserInstance ShardInstnce].uid]) {
            self.money.hidden = YES;
            self.withdraw.hidden = YES;
            self.unit.hidden = YES;
        } else {
            self.money.hidden = NO;
            self.withdraw.hidden = NO;
            self.unit.hidden = NO;
        }
    } else {
        if (!model.is_received) {
            self.money.hidden = YES;
            self.withdraw.hidden = YES;
            self.unit.hidden = YES;
        } else {
            self.money.hidden = NO;
            self.withdraw.hidden = NO;
            self.unit.hidden = NO;
            for (RedPacketPersonModel *p in model.list) {
                if ([p.nickname isEqualToString:[UserInstance ShardInstnce].nickName]) {
                    self.money.text = p.money;
                }
            }
        }
        
    }
   
}

- (void)checkWallet {
    if ([self.delegate respondsToSelector:@selector(checkWalletClick)]) {
        [self.delegate checkWalletClick];
    }
}

@end
