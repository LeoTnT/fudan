//
//  PayRedPacketView.m
//  App3.0
//
//  Created by mac on 2017/8/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PayRedPacketView.h"

@interface PayRedPacketView()
@property (strong, nonatomic) UIViewController *viewController;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *parentView;
@property (assign, nonatomic) CGFloat money;
@end

@implementation PayRedPacketView
+ (instancetype)creatViewWithMoney:(CGFloat)money viewController:(UIViewController *)vc {
    return [[self alloc] initWithMoney:money viewController:vc];
}

- (instancetype)initWithMoney:(CGFloat)money viewController:(UIViewController *)vc {
    if (self = [super init]) {
        [self creatBgView];
        [self creatContentView];
    }
    return self;
}

- (void)creatBgView {
    self.window = [UIApplication sharedApplication].keyWindow;
    self.parentView = [UIView new];
    self.parentView.frame = self.window.bounds;
    
    self.frame = CGRectMake(37.5, mainHeight, mainWidth-75, 0);
    [UIView animateWithDuration:.25 animations:^{
        self.frame = CGRectMake(37.5, mainHeight-236-248, mainWidth-75, 236);
    }];
    
    self.parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3];
    [self.window addSubview:self.parentView];
    [self.parentView addSubview:self];
}

- (void)creatContentView {
    UILabel *title = [[UILabel alloc] init];
    title.text = Localized(@"input_pay_pwd");
    title.font = [UIFont systemFontOfSize:17];
    [self addSubview:title];
    
    
}
@end
