//
//  RedPacketListTopCell.m
//  App3.0
//
//  Created by mac on 2017/8/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RedPacketListTopCell.h"

@interface RedPacketListTopCell()
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *sendNum; // 发出的个数
@property (strong, nonatomic) UILabel *money;
@property (strong, nonatomic) UILabel *recNum; // 收到的个数
@property (strong, nonatomic) UILabel *bestNum;// 手气最佳个数
@property (strong, nonatomic) UIButton *dateBtn;  // 红包列表年份
@end

@implementation RedPacketListTopCell

+ (instancetype)redPacketListTopCellWithTableView:(UITableView *)tableView model:(RedPacketMineModel *)model {
    static NSString *cellidentifier =@"redPacketListTopCell";
    RedPacketListTopCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (!cell) {
        cell = [[RedPacketListTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    [cell setupModelData:model];
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        
        _avatar = [[UIImageView alloc] init];
        _avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 30;
        [self addSubview:_avatar];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:15];
        [self addSubview:_title];
        
        _money = [[UILabel alloc] init];
        _money.font = [UIFont systemFontOfSize:55];
        [self addSubview:_money];
        
        _sendNum = [[UILabel alloc] init];
        _sendNum.textColor = COLOR_999999;
        _sendNum.font = [UIFont systemFontOfSize:14];
        _sendNum.hidden = YES;
        [self addSubview:_sendNum];
        
        _recNum = [[UILabel alloc] init];
        _recNum.textColor = COLOR_999999;
        _recNum.font = [UIFont systemFontOfSize:14];
        _recNum.textAlignment = NSTextAlignmentCenter;
        _recNum.numberOfLines = 2;
        _recNum.hidden = YES;
        [self addSubview:_recNum];
        
        _bestNum = [[UILabel alloc] init];
        _bestNum.textColor = COLOR_999999;
        _bestNum.font = [UIFont systemFontOfSize:14];
        _bestNum.textAlignment = NSTextAlignmentCenter;
        _bestNum.numberOfLines = 2;
        _bestNum.hidden = YES;
        [self addSubview:_bestNum];
        
        _dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _dateBtn.frame = CGRectMake(mainWidth-80, 20, 60, 60);
        [_dateBtn setTitle:@"2017" forState:UIControlStateNormal];
        [_dateBtn setTitleColor:[UIColor hexFloatColor:@"909090"] forState:UIControlStateNormal];
        _dateBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_dateBtn setImage:[UIImage imageNamed:@"logoin_pull_down"] forState:UIControlStateNormal];
        [_dateBtn setImageEdgeInsets:UIEdgeInsetsMake(_dateBtn.titleLabel.intrinsicContentSize.height+10, 0, 0, -_dateBtn.titleLabel.intrinsicContentSize.width)];
        [_dateBtn setTitleEdgeInsets:UIEdgeInsetsMake(-_dateBtn.currentImage.size.height-10, -_dateBtn.currentImage.size.width, 0, 0)];
        
        [_dateBtn addTarget:self action:@selector(chooseYear) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_dateBtn];
        
//        [_dateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self).offset(19.5);
//            make.right.mas_equalTo(self).offset(-10);
//            make.size.mas_equalTo(CGSizeMake(60, 60));
//        }];
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(68);
            make.centerX.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(60, 60));
        }];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_avatar.mas_bottom).offset(3.5);
            make.centerX.mas_equalTo(self);
        }];
        [_money mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_title.mas_bottom).offset(18.5);
            make.centerX.mas_equalTo(self);
        }];
        [_sendNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_money.mas_bottom).offset(42.5);
            make.centerX.mas_equalTo(self);
        }];
        [_recNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_money.mas_bottom).offset(42.5);
            make.left.mas_equalTo(79.5);
        }];
        [_bestNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_money.mas_bottom).offset(42.5);
            make.right.mas_equalTo(-79.5);
        }];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor hexFloatColor:@"d9d9d9"];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        
    }
    return self;
}

- (void)setupModelData:(RedPacketMineModel *)model {
    [self.avatar getImageWithUrlStr:model.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.money.text = model.money_all;
    [self.dateBtn setTitle:model.year forState:UIControlStateNormal];
    if (model.type == 0) {
        // 我发出的
        self.sendNum.hidden = NO;
        self.recNum.hidden = YES;
        self.bestNum.hidden = YES;
        self.title.text = [NSString stringWithFormat:@"%@共发出",isEmptyString(model.nickname)?@"":model.nickname];
        
        NSString *base = [NSString stringWithFormat:@"发出红包%d个",model.count_all];
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:base];
        [att addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"e83e3e"] range:NSMakeRange(4, base.length-5
                                                                                                                   )];
        self.sendNum.attributedText = att;
        
    } else {
        // 我收到的
        self.sendNum.hidden = YES;
        self.recNum.hidden = NO;
        self.bestNum.hidden = NO;
        self.title.text = [NSString stringWithFormat:@"%@共收到",isEmptyString(model.nickname)?@"":model.nickname];
        
        NSString *base = [NSString stringWithFormat:@"收到红包\n%d",model.count_all];
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:base];
        [att addAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"e83e3e"],NSFontAttributeName:[UIFont systemFontOfSize:16]} range:NSMakeRange(4, base.length-4)];
        self.recNum.attributedText = att;
        
        base = [NSString stringWithFormat:@"手气最佳\n%d",model.count_best];
        att = [[NSMutableAttributedString alloc] initWithString:base];
        [att addAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"e83e3e"],NSFontAttributeName:[UIFont systemFontOfSize:16]} range:NSMakeRange(4, base.length-4)];
        self.bestNum.attributedText = att;
    }
    
}

- (void)chooseYear {
    if ([self.delegate respondsToSelector:@selector(chooseYearClick)]) {
        [self.delegate chooseYearClick];
    }
}
@end
