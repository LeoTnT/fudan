//
//  TILChatCallManager.h
//  App3.0
//
//  Created by mac on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TILCallSDK/TILCallSDK.h>
#import "TILCallC2CViewController.h"

@interface TILChatCallManager : NSObject <TILCallIncomingCallListener>
@property (strong, nonatomic) UIViewController *mainController;

+ (instancetype)sharedManager;

- (void)makeCallWithUsername:(NSString *)aUsername
                        type:(TILCallType)aType;
@end
