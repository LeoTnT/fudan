//
//  TILChatCallManager.m
//  App3.0
//
//  Created by mac on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TILChatCallManager.h"

@implementation TILChatCallManager

static TILChatCallManager *callManager = nil;

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initManager];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        callManager = [[TILChatCallManager alloc] init];
    });
    
    return callManager;
}

- (void)initManager
{
    [[TILCallManager sharedInstance] setIncomingCallListener:self];
}

- (void)makeCallWithUsername:(NSString *)aUsername type:(TILCallType)aType {
    TILCallC2CViewController *vc = [[TILCallC2CViewController alloc] init];
    vc.peerId = aUsername;
    vc.callType = aType;
    vc.isCaller = YES;
    [self.mainController presentViewController:vc animated:YES completion:nil];
}

- (void)onC2CCallInvitation:(TILCallInvitation *)invitation
{
    //收到双人通话邀请
    TILCallC2CViewController *vc = [[TILCallC2CViewController alloc] init];
    vc.peerId = invitation.inviterId;
    vc.callType = invitation.callType;
    vc.isCaller = NO;
    vc.invite = invitation;
    [self.mainController presentViewController:vc animated:YES completion:nil];
}
@end
