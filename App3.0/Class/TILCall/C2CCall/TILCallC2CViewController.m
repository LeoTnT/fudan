//
//  TILCallC2CViewController.m
//  App3.0
//
//  Created by mac on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TILCallC2CViewController.h"
#import <ILiveSDK/ILiveLoginManager.h>
#import <ILiveSDK/ILiveRoomManager.h>
#import "ChatHelper.h"

@interface TILCallC2CViewController ()<TILCallNotificationListener,TILCallStatusListener, TILCallMemberEventListener>
@property (nonatomic, strong) TILC2CCall *call;
@property (nonatomic, strong) NSString *myId;

@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *remoteAvatarImgView;     // 对方头像
@property (strong, nonatomic) UILabel *remoteNameLabel;             // 对方昵称
@property (strong, nonatomic) UILabel *statusLabel;                 // 通话状态
@property (strong, nonatomic) UILabel *timeLabel;                   // 计时
@property (strong, nonatomic) UIImageView *remoteImgView;

@property (strong, nonatomic) UILabel *networkLabel;

@property (strong, nonatomic) UIView *actionView;
@property (strong, nonatomic) UIButton *speakerOutButton;           // 免提
@property (strong, nonatomic) UIButton *silenceButton;              // 静音
@property (strong, nonatomic) UIButton *minimizeButton;
@property (strong, nonatomic) UIButton *rejectButton;               // 拒绝
@property (strong, nonatomic) UIButton *hangupButton;               // 取消
@property (strong, nonatomic) UIButton *answerButton;               // 接受
@property (strong, nonatomic) UIButton *switchCameraButton;
@property (strong, nonatomic) UIButton *showVideoInfoButton;

@property (strong, nonatomic) AVAudioPlayer *ringPlayer;
@property (nonatomic) int timeLength;
@property (strong, nonatomic) NSTimer *timeTimer;
@property (strong, nonatomic) NSTimer *outTimeTimer;
@property (strong, nonatomic) NSTimer *soundTimer;
@property (assign, nonatomic) BOOL isCalling;       // 已建立聊天

@property (strong, nonatomic) ContactDataParser *contactModel;
@end

@implementation TILCallC2CViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    self.isCalling = NO;
    if (self.isCaller) {
        [self makeCall];
    } else {
        [self receiveCall];
        
    }
    _myId = [[ILiveLoginManager getInstance] getLoginId];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIButton *)createButtonWithImage:(NSString *)imageName title:(NSString *)title {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(-btn.titleLabel.intrinsicContentSize.height-10, 0, 0, -btn.titleLabel.intrinsicContentSize.width)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(btn.currentImage.size.height+10, -btn.currentImage.size.width, 0, 0)];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.actionView addSubview:btn];
    return btn;
}

- (void)buttonAction:(UIButton *)sender {
    __weak typeof(self) ws = self;
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.hangup", @"")]) {
        if (self.isCalling) {
            [_call hangup:^(TILCallError *err) {
                if(err){
                    [ws setText:[NSString stringWithFormat:@"挂断失败:%@-%d-%@",err.domain,err.code,err.errMsg]];
                }
                else{
                    [ws setText:@"挂断成功"];
                }
                [ws selfDismiss];
            }];
        } else {
            [_call cancelCall:^(TILCallError *err) {
                if(err){
                    [ws setText:[NSString stringWithFormat:@"取消通话邀请失败:%@-%d-%@",err.domain,err.code,err.errMsg]];
                }
                else{
                    [ws setText:@"取消通话邀请成功"];
                }
                [ws selfDismiss];
            }];
        }
        
        
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.answer", @"")]) {
        if (self.callType == TILCALL_TYPE_VIDEO) {
            [_call createRenderViewIn:self.view];
            ILiveRoomManager *manager = [ILiveRoomManager getInstance];
            [manager setAudioMode:QAVOUTPUTMODE_SPEAKER];
        }
        __weak typeof(self) ws = self;
        [_call accept:^(TILCallError *err) {
            if(err){
                NSLog(@"接受失败");
            }
            else{
                NSLog(@"接受成功");
                [ws setEnableButton:YES];
                
                if (ws.callType == TILCALL_TYPE_VIDEO) {
                    ILiveRoomManager *manager = [ILiveRoomManager getInstance];
                    [manager setAudioMode:QAVOUTPUTMODE_SPEAKER];
                } else {
                    ILiveRoomManager *manager = [ILiveRoomManager getInstance];
                    [manager setAudioMode:QAVOUTPUTMODE_EARPHONE];
                }
            }
        }];
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.reject", @"")]) {
        [_call refuse:^(TILCallError *err) {
            if(err){
                [ws setText:[NSString stringWithFormat:@"拒绝失败:%@-%d-%@",err.domain,err.code,err.errMsg]];
            }
            else{
                [ws setText:@"拒绝成功"];
                [ws selfDismiss];
            }
        }];
        
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.silence", @"")]) {
        self.silenceButton.selected = !self.silenceButton.selected;
        
        ILiveRoomManager *manager = [ILiveRoomManager getInstance];
        BOOL isOn = [manager getCurMicState];
        __weak typeof(self) ws = self;
        [manager enableMic:!isOn succ:^{
            NSString *text = !isOn?@"打开麦克风成功":@"关闭麦克风成功";
            [ws setText:text];
        } failed:^(NSString *moudle, int errId, NSString *errMsg) {
            NSString *text = !isOn?@"打开麦克风失败":@"关闭麦克风失败";
            [ws setText:[NSString stringWithFormat:@"%@:%@-%d-%@",text,moudle,errId,errMsg]];
        }];
    } else if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"call.speakerOut", @"")]) {
        
        self.speakerOutButton.selected = !self.speakerOutButton.selected;
        
        ILiveRoomManager *manager = [ILiveRoomManager getInstance];
        QAVOutputMode mode = [manager getCurAudioMode];
        if(mode == QAVOUTPUTMODE_EARPHONE){
            [manager setAudioMode:QAVOUTPUTMODE_SPEAKER];
        }
        else{
            [manager setAudioMode:QAVOUTPUTMODE_EARPHONE];
        }
    }
}

- (void)swithCameraAction {
    ILiveRoomManager *manager = [ILiveRoomManager getInstance];
    __weak typeof(self) ws = self;
    [manager switchCamera:^{
        [ws setText:@"切换摄像头成功"];
    } failed:^(NSString *moudle, int errId, NSString *errMsg) {
        [ws setText:[NSString stringWithFormat:@"切换摄像头失败:%@-%d-%@",moudle,errId,errMsg]];
    }];
}

- (void)setContentView {
    // 从数据库获取联系人
    NSArray *arr = [[DBHandler sharedInstance] getContactByUid:self.peerId];
    if (arr && arr.count > 0) {
        self.contactModel = arr[0];
    }
    
    // 对方头像
    self.remoteAvatarImgView = [UIImageView new];
    self.remoteAvatarImgView.layer.masksToBounds = YES;
    self.remoteAvatarImgView.layer.cornerRadius = 3;
    [self.remoteAvatarImgView getImageWithUrlStr:self.contactModel.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    [self.view addSubview:self.remoteAvatarImgView];
    
    // 对方昵称
    self.remoteNameLabel = [UILabel new];
    self.remoteNameLabel.text = [self.contactModel getName];
    self.remoteNameLabel.font = [UIFont systemFontOfSize:30];
    self.remoteNameLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.remoteNameLabel];
    
    // 通话状态
    self.statusLabel = [UILabel new];
    self.statusLabel.text = @"正在等待对方接受邀请..";
    self.statusLabel.font = [UIFont systemFontOfSize:15];
    self.statusLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.statusLabel];
    
    self.actionView = [UIView new];
    [self.view addSubview:self.actionView];
    [self.actionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(35);
        make.right.mas_equalTo(self.view).offset(-35);
        make.bottom.mas_equalTo(self.view).offset(-15);
        make.height.mas_equalTo(150);
    }];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self.actionView addSubview:self.timeLabel];
    self.timeLabel.hidden = YES;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.actionView);
        make.top.mas_equalTo(self.actionView).offset(3);
    }];
    
    self.hangupButton = [self createButtonWithImage:@"call_end" title:NSLocalizedString(@"call.hangup", @"")];
    [self.hangupButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.rejectButton = [self createButtonWithImage:@"call_end" title:NSLocalizedString(@"call.reject", @"")];
    [self.rejectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.answerButton = [self createButtonWithImage:@"call_receive" title:NSLocalizedString(@"call.answer", @"")];
    [self.answerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.silenceButton = [self createButtonWithImage:@"call_mute_normal" title:NSLocalizedString(@"call.silence", @"")];
    [self.silenceButton setImage:[UIImage imageNamed:@"call_mute_press"] forState:UIControlStateSelected];
    self.silenceButton.hidden = YES;
    [self.silenceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    self.speakerOutButton = [self createButtonWithImage:@"call_speaker_normal" title:NSLocalizedString(@"call.speakerOut", @"")];
    [self.speakerOutButton setImage:[UIImage imageNamed:@"call_speaker_press"] forState:UIControlStateSelected];
    self.speakerOutButton.hidden = YES;
    [self.speakerOutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.actionView);
        make.bottom.mas_equalTo(self.actionView);
        make.size.mas_equalTo(CGSizeMake(65, 95));
    }];
    
    if (self.callType == TILCALL_TYPE_VIDEO) {
        self.speakerOutButton.selected = YES;   // 默认开启免提
        
        [self.remoteAvatarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self.view).offset(15);
            make.size.mas_equalTo(CGSizeMake(66, 66));
        }];
        
        [self.remoteNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.remoteAvatarImgView.mas_right).offset(12);
            make.top.mas_equalTo(self.remoteAvatarImgView);
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.remoteNameLabel);
            make.bottom.mas_equalTo(self.remoteAvatarImgView);
        }];
        
        self.switchCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.switchCameraButton setImage:[UIImage imageNamed:@"call_camera"] forState:UIControlStateNormal];
        [self.switchCameraButton addTarget:self action:@selector(swithCameraAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.switchCameraButton];
        self.switchCameraButton.hidden = YES;
        [self.switchCameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view).offset(15);
            make.top.mas_equalTo(self.view).offset(25);
        }];
    } else {
        self.remoteNameLabel.textAlignment = NSTextAlignmentCenter;
        self.statusLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.remoteAvatarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view).offset(-70);
            make.left.mas_equalTo(self.view).offset(127);
            make.right.mas_equalTo(self.view).offset(-127);
            make.width.mas_equalTo(self.remoteAvatarImgView.mas_height);
        }];
        
        [self.remoteNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.top.mas_equalTo(self.remoteAvatarImgView.mas_bottom).offset(25);
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.remoteNameLabel);
            make.top.mas_equalTo(self.remoteNameLabel.mas_bottom).offset(17);
        }];
    }
}

- (void)_layoutSubviews
{
    
    switch (self.callType) {
        case TILCALL_TYPE_AUDIO:
        {
            if (self.isCaller) {
                self.rejectButton.hidden = YES;
                self.answerButton.hidden = YES;
            } else {
                self.hangupButton.hidden = YES;
            }
        }
            break;
        case TILCALL_TYPE_VIDEO:
        {
            self.showVideoInfoButton.hidden = NO;
            self.speakerOutButton.hidden = YES;
            
            if (self.isCaller) {
                self.rejectButton.hidden = YES;
                self.answerButton.hidden = YES;
            } else {
                self.hangupButton.hidden = YES;
            }
            
//            [self _setupLocalVideoView];
            //            [self.view bringSubviewToFront:self.topView];
            //            [self.view bringSubviewToFront:self.actionView];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - private timer

- (void)timeTimerAction:(id)sender
{
    self.timeLength += 1;
    int hour = self.timeLength / 3600;
    int m = (self.timeLength - hour * 3600) / 60;
    int s = self.timeLength - hour * 3600 - m * 60;
    
    if (hour > 0) {
        self.timeLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i", hour, m, s];
    }
    else if(m > 0){
        self.timeLabel.text = [NSString stringWithFormat:@"%02i:%02i", m, s];
    }
    else{
        self.timeLabel.text = [NSString stringWithFormat:@"00:%02i", s];
    }
}

- (void)_startTimeTimer
{
    self.timeLength = 0;
    self.timeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTimerAction:) userInfo:nil repeats:YES];
}

- (void)_stopTimeTimer
{
    if (self.timeTimer) {
        [self.timeTimer invalidate];
        self.timeTimer = nil;
    }
}

- (void)_soundPlay {
    [[ChatHelper shareHelper] createSystemSoundWithName:@"call" soundType:@"mp3" vibrate:NO];
}

- (void)_timeoutBeforeCallAnswered
{
    [self selfDismiss];
}


- (void)_startCallTimer
{
    self.outTimeTimer = [NSTimer scheduledTimerWithTimeInterval:50 target:self selector:@selector(_timeoutBeforeCallAnswered) userInfo:nil repeats:NO];
    
    [[ChatHelper shareHelper] createSystemSoundWithName:@"call" soundType:@"mp3" vibrate:YES];
    self.soundTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(_soundPlay) userInfo:nil repeats:YES];
    
}

- (void)_stopCallTimer
{
    // 停止播放音效
    [[ChatHelper shareHelper] stopSystemSound];
    if (self.outTimeTimer) {
        [self.outTimeTimer invalidate];
        self.outTimeTimer = nil;
    }
    
    if (self.soundTimer) {
        [self.soundTimer invalidate];
        self.soundTimer = nil;
    }
    
}

#pragma mark - 通话接口相关
- (void)makeCall{
    TILCallConfig * config = [[TILCallConfig alloc] init];
    TILCallBaseConfig * baseConfig = [[TILCallBaseConfig alloc] init];
    baseConfig.callType = self.callType;
    baseConfig.isSponsor = YES;
    baseConfig.peerId = _peerId;
    baseConfig.heartBeatInterval = 0;
    config.baseConfig = baseConfig;
    
    TILCallListener * listener = [[TILCallListener alloc] init];
    //注意：
    //［通知回调］可以获取通话的事件通知，建议双人和多人都走notifListener
    // [通话状态回调] 也可以获取通话的事件通知
    listener.callStatusListener = self;
    listener.memberEventListener = self;
    listener.notifListener = self;
    
    config.callListener = listener;
    TILCallSponsorConfig *sponsorConfig = [[TILCallSponsorConfig alloc] init];
    sponsorConfig.waitLimit = 0;
    sponsorConfig.callId = (int)([[NSDate date] timeIntervalSince1970]) % 1000 * 1000 + arc4random() % 1000;
    sponsorConfig.onlineInvite = NO;
    sponsorConfig.controlRole = @"hostTest";
    config.sponsorConfig = sponsorConfig;
    
    _call = [[TILC2CCall alloc] initWithConfig:config];
    
    if (self.callType == TILCALL_TYPE_VIDEO) {
        [_call createRenderViewIn:self.view];
    }
    
    __weak typeof(self) ws = self;
    [_call makeCall:nil custom:nil result:^(TILCallError *err) {
        if(err){
//            [ws setText:[NSString stringWithFormat:@"呼叫失败:%@-%d-%@",err.domain,err.code,err.errMsg]];
            [ws selfDismiss];
        }
        else{
//            [ws setText:@"呼叫成功"];
            [ws setContentView];
            [ws _layoutSubviews];
            [ws _startCallTimer];
            if (ws.callType == TILCALL_TYPE_VIDEO) {
                ILiveRoomManager *manager = [ILiveRoomManager getInstance];
                [manager setAudioMode:QAVOUTPUTMODE_SPEAKER];
            } else {
                ILiveRoomManager *manager = [ILiveRoomManager getInstance];
                [manager setAudioMode:QAVOUTPUTMODE_EARPHONE];
            }
        }
    }];
}

- (void)receiveCall {
    //初始化配置参数
    TILCallConfig * config = [[TILCallConfig alloc] init];
    TILCallBaseConfig * baseConfig = [[TILCallBaseConfig alloc] init];
    baseConfig.callType = self.callType;
    baseConfig.isSponsor = NO;
    baseConfig.peerId = self.invite.sponsorId;
    baseConfig.heartBeatInterval = 15;
    config.baseConfig = baseConfig;
    
    TILCallListener * listener = [[TILCallListener alloc] init];
    listener.memberEventListener = self;
    listener.notifListener = self;
    listener.callStatusListener = self;
    config.callListener = listener;
    
    TILCallResponderConfig * responderConfig = [[TILCallResponderConfig alloc] init];
    responderConfig.callInvitation = self.invite;
    config.responderConfig = responderConfig;
    
    _call = [[TILC2CCall alloc] initWithConfig:config];
    
    [self setContentView];
    [self _layoutSubviews];
    [self _startCallTimer];
}

#pragma mark - 通话状态监听器
/**
 建立通话成功
 */
- (void)onCallEstablish
{
    NSLog(@"onCallEstablish");
}

/**
 通话结束
 
 @param code 结束原因
 */
- (void)onCallEnd:(TILCallEndCode)code
{
    NSLog(@"onCallEnd:%ld",(long)code);
    
}

#pragma mark - 音视频事件回调
- (void)onMemberAudioOn:(BOOL)isOn members:(NSArray *)members
{
    NSLog(@"11111");
}

- (void)onMemberCameraVideoOn:(BOOL)isOn members:(NSArray *)members
{
    if(isOn){
        for (TILCallMember *member in members) {
            NSString *identifier = member.identifier;
            if([identifier isEqualToString:_myId]){
                [_call addRenderFor:_myId atFrame:self.view.bounds];
                [_call sendRenderViewToBack:_myId];
            }
            else{
                [_call addRenderFor:identifier atFrame:CGRectMake(20, 20, 120, 160)];
            }
        }
    }
    else{
        for (TILCallMember *member in members) {
            NSString *identifier = member.identifier;
            [_call removeRenderFor:identifier];
        }
    }
}

#pragma mark - 通知回调
//注意：
//［通知回调］可以获取通话的事件通知
// [通话状态回调] 也可以获取通话的事件通知
- (void)onRecvNotification:(TILCallNotification *)notify
{
    //    TILCALL_NOTIF_ACCEPTED      = 0x82,
    //    TILCALL_NOTIF_CANCEL,
    //    TILCALL_NOTIF_TIMEOUT,
    //    TILCALL_NOTIF_REFUSE,
    //    TILCALL_NOTIF_HANGUP,
    //    TILCALL_NOTIF_LINEBUSY,
    //    TILCALL_NOTIF_HEARTBEAT     = 0x88,
    //    TILCALL_NOTIF_INVITE        = 0x89,
    //    TILCALL_NOTIF_DISCONNECT    = 0x8A,
    
    NSInteger notifId = notify.notifId;
    NSString *sender = notify.sender;
    switch (notifId) {
        case TILCALL_NOTIF_ACCEPTED:
            [self setText:@"通话建立成功"];
            [self setEnableButton:YES];
            break;
        case TILCALL_NOTIF_CANCEL:
            [self setText:@"对方已取消"];
            [self selfDismiss];
            break;
        case TILCALL_NOTIF_TIMEOUT:
            [self setText:self.isCaller?@"对方没有接听":@"对方呼叫超时"];
            [self selfDismiss];
            break;
        case TILCALL_NOTIF_REFUSE:
            [self setText:@"对方拒绝接听"];
            [self selfDismiss];
            break;
        case TILCALL_NOTIF_HANGUP:
            [self setText:@"对方已挂断"];
            [self selfDismiss];
            break;
        case TILCALL_NOTIF_LINEBUSY:
            [self setText:@"对方占线"];
            [self selfDismiss];
            break;
        case TILCALL_NOTIF_HEARTBEAT:
            [self setText:[NSString stringWithFormat:@"%@发来心跳",sender]];
            break;
        case TILCALL_NOTIF_DISCONNECT:
            [self setText:@"对方失去连接"];
            [self selfDismiss];
            break;
        default:
            break;
    }
}

#pragma mark - 界面管理
- (void)setEnableButton:(BOOL)isMake
{
    [self _stopCallTimer];
    self.statusLabel.text = NSLocalizedString(@"call.speak", "Incomimg call");
    [self _startTimeTimer];
    
    self.isCalling = YES;
    self.timeLabel.hidden = NO;
    self.hangupButton.hidden = NO;
    self.statusLabel.hidden = NO;
    self.rejectButton.hidden = YES;
    self.answerButton.hidden = YES;
    self.silenceButton.hidden = NO;
    self.speakerOutButton.hidden = NO;
    
    if (self.callType == TILCALL_TYPE_VIDEO) {
        
        self.remoteAvatarImgView.hidden = YES;
        self.remoteNameLabel.hidden = YES;
        self.statusLabel.hidden = YES;
        self.switchCameraButton.hidden = NO;
    }
}

- (void)setText:(NSString *)text
{
    self.statusLabel.text = text;
}

- (void)selfDismiss
{
    [self _stopCallTimer];
    [self _stopTimeTimer];
    //为了看到关闭打印的信息，demo延迟1秒关闭
    __weak typeof(self) ws = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ws dismissViewControllerAnimated:YES completion:nil];
    });
    
    if (self.isCaller) {
        NSDictionary *extDic;
        if (self.callType == TILCALL_TYPE_VIDEO) {
            extDic = @{MSG_TYPE:MESSAGE_ATTR_IS_VIDEO_CALL};
        } else {
            extDic = @{MSG_TYPE:MESSAGE_ATTR_IS_VOICE_CALL};
        }
#ifdef ALIYM_AVALABLE
        YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:@"通话结束"];
        //发送消息
        NSString *uid = [self.peerId substringFromIndex:3];
        YWPerson *person = [[YWPerson alloc] initWithPersonId:uid];
        YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
        [conversation asyncSendMessageBody:body progress:nil completion:^(NSError *error, NSString *messageID) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessages" object:nil];
        }];
#else
        
#endif
        
    }
    
}

@end
