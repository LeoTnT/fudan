//
//  TILCallC2CViewController.h
//  App3.0
//
//  Created by mac on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TILCallSDK/TILCallSDK.h>

@interface TILCallC2CViewController : UIViewController
@property (copy, nonatomic) NSString *peerId;
@property (assign, nonatomic) TILCallType callType;
@property (assign, nonatomic) BOOL isCaller;
@property (strong, nonatomic) TILCallInvitation *invite;
@end
