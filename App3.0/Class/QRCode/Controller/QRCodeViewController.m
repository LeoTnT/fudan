//
//  QRCodeViewController.m
//  App3.0
//
//  Created by mac on 2018/4/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "QRCodeViewController.h"
#import "QRCodeSetViewController.h"
#import "WalletAccountViewController.h"
#import "XSQR.h"

@interface QRCodeViewController ()
@property (strong, nonatomic) UIImageView *qrImageView;
@property (strong, nonatomic) UILabel *remark;
@property (strong, nonatomic) UILabel *money;
@end

@implementation QRCodeViewController
{
    BOOL isUserCode;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!isUserCode) {
        self.navigationItem.title = Localized(@"商家收款二维码");
    } else {
        self.navigationItem.title = Localized(@"个人收款二维码");
    }
    self.view.backgroundColor = mainColor;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self actionCustomRightBtnWithNrlImage:@"qrcode_set" htlImage:nil title:nil action:^{
        @strongify(self);
        QRCodeSetViewController *vc = [[QRCodeSetViewController alloc] init];
        vc.isUserCode = isUserCode;
        [vc setSettingFinished:^(NSString *money, NSString *other, NSString *qrImageUrl) {
            if (isUserCode) {
                self.tableView.tableHeaderView.frame = CGRectMake(0, 0, mainWidth-24, 475+32);
                self.money.text = [NSString stringWithFormat:@"¥ %.2f", [money floatValue]];
                self.remark.text = isEmptyString(other)?@"备注内容":other;
            } else {
                self.tableView.tableHeaderView.frame = CGRectMake(0, 0, mainWidth-24, 415+32);
            }
            [self.tableView reloadData];
            self.qrImageString = qrImageUrl;
            self.qrImageView.image = [XSQR createQrImageWithContentString:self.qrImageString type:XSQRTypeSupply];
//            [self.qrImageView getImageWithUrlStr:self.qrImageString andDefaultImage:nil];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = mainColor;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.bounces = NO;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 14, 0, 12));
    }];
    
    [self setContentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setIs_shop:(NSString *)is_shop {
    _is_shop = is_shop;
    if (![is_shop isEqualToString:@"offline"]) {
        isUserCode = YES;
    } else {
        isUserCode = NO;
    }
}

- (void)setContentView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(12, 0, mainWidth-24, 415+32)];
    self.tableView.tableHeaderView = headerView;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 4;
    [headerView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(32, 0, 0, 0));
    }];
    
    UILabel *title = [UILabel new];
    title.text = !isUserCode? Localized(@"福旦扫一扫，向商家付款"):Localized(@"福旦扫一扫，向我付款");
    title.font = SYSTEM_FONT(15);
    [bgView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(46);
    }];
    
    self.qrImageView = [UIImageView new];
    self.qrImageView.image = [XSQR createQrImageWithContentString:self.qrImageString type:XSQRTypeSupply];
    [bgView addSubview:self.qrImageView];
    [self.qrImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(226, 226));
        make.top.mas_equalTo(100);
    }];
    
    if (isUserCode) {
        self.money = [UILabel new];
        self.money.font = [UIFont boldSystemFontOfSize:30];
        [bgView addSubview:self.money];
        [self.money mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(self.qrImageView.mas_bottom).offset(16);
        }];
    }
    
    self.remark = [UILabel new];
    self.remark.font = SYSTEM_FONT(14);
    [bgView addSubview:self.remark];
    [self.remark mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(isUserCode?self.money.mas_bottom:self.qrImageView.mas_bottom).offset(16);
    }];
    
    UIButton *saveImageBtn = [BaseUITool buttonWithTitle:Localized(@"person_qr_save") titleColor:mainColor font:SYSTEM_FONT(14) superView:bgView];
    [saveImageBtn addTarget:self action:@selector(saveImageAction) forControlEvents:UIControlEventTouchUpInside];
    [saveImageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(self.remark.mas_bottom).offset(20);
    }];
    
}

- (void)saveImageAction {
    [XSTool showProgressHUDWithView:self.view];
    UIImageWriteToSavedPhotosAlbum(self.qrImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [XSTool hideProgressHUDWithView:self.view];
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
    if (!error) {
        Alert(Localized(@"my_skill_sava_success"));
    } else {
        Alert(Localized(@"person_qr_save_fail"));
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
        cell.textLabel.font =SYSTEM_FONT(14);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        //设置切哪个直角
        //    UIRectCornerTopLeft     = 1 << 0,  左上角
        //    UIRectCornerTopRight    = 1 << 1,  右上角
        //    UIRectCornerBottomLeft  = 1 << 2,  左下角
        //    UIRectCornerBottomRight = 1 << 3,  右下角
        //    UIRectCornerAllCorners  = ~0UL     全部角
        //得到view的遮罩路径
        UIBezierPath *maskPath;
        if (indexPath.row == 0) {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, mainWidth-24, 44) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(4,4)];
        } else {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, mainWidth-24, 44) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(4,4)];
        }
        //创建 layer
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = cell.bounds;
        //赋值
        maskLayer.path = maskPath.CGPath;
        cell.layer.mask = maskLayer;
    }
    cell.imageView.image = [UIImage imageNamed:indexPath.row==0? @"qrcode_introduce":@"qrcode_record"];
    cell.textLabel.text = indexPath.row==0? Localized(@"收款码介绍"):Localized(@"收款记录");
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/user/receivables/intro?device=%@",ImageBaseUrl,gui];
        adWeb.urlType = WKWebViewURLNormal;
        adWeb.navigationItem.title = Localized(@"收款码介绍");
        [self.navigationController pushViewController:adWeb animated:YES];
    } else {
        if (isUserCode) {
            WalletAccountViewController *vc = [[WalletAccountViewController alloc] init];
            vc.walletType = @"money";
            vc.navTitle = Localized(@"现金记录");
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [self.navigationController pushViewController:[self getNameWithClass:@"QuickPayRecordsVC"] animated:YES];
        }
        
    }
}

- (UIViewController * )getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}
@end
