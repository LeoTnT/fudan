//
//  QRCodeSetViewController.h
//  App3.0
//
//  Created by mac on 2018/4/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface QRCodeSetViewController : XSBaseTableViewController

@property (assign, nonatomic) BOOL isUserCode;

@property (copy, nonatomic) void (^settingFinished)(NSString *money, NSString *other, NSString *qrImageUrl);
@end
