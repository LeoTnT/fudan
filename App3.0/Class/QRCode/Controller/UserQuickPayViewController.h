//
//  UserQuickPayViewController.h
//  App3.0
//
//  Created by mac on 2018/4/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface UserQuickPayViewController : XSBaseViewController

@property (copy, nonatomic) NSString *uid;
@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *money;
@property (copy, nonatomic) NSString *remark;

@end
