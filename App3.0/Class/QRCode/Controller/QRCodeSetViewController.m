//
//  QRCodeSetViewController.m
//  App3.0
//
//  Created by mac on 2018/4/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "QRCodeSetViewController.h"
#import "HTTPManager+QRCode.h"

@interface QRCodeSetViewController ()
@property (strong, nonatomic) UITextField *textFieldOne;
@property (strong, nonatomic) UITextField *textFieldTwo;
@end

@implementation QRCodeSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = Localized(@"me_item_setting");
    self.view.backgroundColor = BG_COLOR;
    self.autoHideKeyboard = YES;
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.bounces = NO;
    
    [self setContentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setContentView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 88)];
    self.tableView.tableFooterView = footerView;
    
    UIButton *confirm = [BaseUITool buttonWithTitle:Localized(@"material_dialog_positive_text") titleColor:[UIColor whiteColor] font:SYSTEM_FONT(15) superView:footerView];
    [confirm setBackgroundColor:mainColor];
    confirm.layer.masksToBounds = YES;
    confirm.layer.cornerRadius = 5;
    [confirm addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [confirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(44, 20, 0, 20));
    }];
    
}

- (void)confirmAction {
    if (isEmptyString(self.textFieldOne.text)) {
        [XSTool showToastWithView:self.view Text:@"请输入金额"];
        return;
    }
    [XSTool showProgressHUDWithView:self.view];
    if (self.isUserCode) {
        NSMutableDictionary *params = [@{@"total":self.textFieldOne.text} mutableCopy];
        if (!isEmptyString(self.textFieldTwo.text)) {
            [params setObject:self.textFieldTwo.text forKey:@"remark"];
        }
        [HTTPManager createUserQuickPayQrcodeWithParams:params success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                if (self.settingFinished) {
                    self.settingFinished(self.textFieldOne.text, self.textFieldTwo.text, dic[@"data"]);
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        NSMutableDictionary *params = [@{@"total":self.textFieldOne.text} mutableCopy];
        if (!isEmptyString(self.textFieldTwo.text)) {
            [params setObject:self.textFieldTwo.text forKey:@"fixed"];
        }
        [HTTPManager createOfflineSupplyQuickPayQrcodeWithParams:params success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                if (self.settingFinished) {
                    self.settingFinished(self.textFieldOne.text, self.textFieldTwo.text, dic[@"data"]);
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 12;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
        cell.textLabel.textColor = COLOR_666666;
        cell.textLabel.font = SYSTEM_FONT(14);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(120, 12, mainWidth-120-12, 20)];
        tf.textAlignment = NSTextAlignmentRight;
        tf.font = SYSTEM_FONT(14);
        [tf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [cell.contentView addSubview:tf];
        if (indexPath.row == 0) {
            tf.keyboardType = UIKeyboardTypeDecimalPad;
            self.textFieldOne = tf;
        } else {
            if (self.isUserCode) {
                tf.keyboardType = UIKeyboardTypeDefault;
            } else {
                tf.keyboardType = UIKeyboardTypeDecimalPad;
            }
            self.textFieldTwo = tf;
        }
    }
    if (self.isUserCode) {
        cell.textLabel.text = indexPath.row==0? Localized(@"金额"):Localized(@"note");
    } else {
        cell.textLabel.text = indexPath.row==0? Localized(@"消费金额"):Localized(@"不参与优惠金额");
    }
    
    return cell;
}

/*监听输入状态*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if (self.isUserCode) {
        if ([textField isEqual:self.textFieldOne]) {
            
        } else {
            
        }
    } else {
        if ([textField isEqual:self.textFieldOne]) {
            
        } else {
            
        }
    }
}
@end
