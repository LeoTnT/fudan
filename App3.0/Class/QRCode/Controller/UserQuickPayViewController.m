//
//  UserQuickPayViewController.m
//  App3.0
//
//  Created by mac on 2018/4/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "UserQuickPayViewController.h"
#import "OrderPayViewController.h"
#import "HTTPManager+QRCode.h"
#import "QuickPayModel.h"

@interface UserQuickPayViewController ()
@property (strong, nonatomic) QuickOrderInfoModel *userModel;
@property (strong, nonatomic) UITextField *moneyTF;
@property (strong, nonatomic) UITextField *remarkTF;
@end

@implementation UserQuickPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"付款");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.autoHideKeyboard = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setContentView {
    UIView *headerView = [[UIView alloc] init];
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(340);
    }];
    
    UIImageView *logo = [UIImageView new];
    [logo getImageWithUrlStr:self.userModel.logo andDefaultImage:DefaultAvatarImage];
    [headerView addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(headerView);
        make.top.mas_equalTo(15);
        make.size.mas_equalTo(CGSizeMake(64, 64));
    }];
    
    UILabel *payTo = [UILabel new];
    payTo.font = SYSTEM_FONT(16);
    [headerView addSubview:payTo];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:Localized(@"付款给 个人账户")];
    [att addAttribute:NSForegroundColorAttributeName value:Color(@"EE7B31") range:NSMakeRange(4, 4)];
    payTo.attributedText = att;
    [payTo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(headerView);
        make.top.mas_equalTo(logo.mas_bottom).offset(7);
    }];
    
    UILabel *name = [UILabel new];
    if (isEmptyString(self.userModel.truename)) {
        name.text = self.userModel.name;
    } else {
        name.text = [NSString stringWithFormat:@"%@(*%@)", self.userModel.name, [self.userModel.truename substringFromIndex:1]];
    }
    
    name.font = SYSTEM_FONT(16);
    [headerView addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(headerView);
        make.top.mas_equalTo(payTo.mas_bottom).offset(17);
    }];
    
    UILabel *mobile = [UILabel new];
    mobile.text = self.userModel.mobile;//[NSString stringWithFormat:@"%@******%@", [self.userModel.mobile substringToIndex:3], [self.userModel.mobile substringFromIndex:10]];
    mobile.font = SYSTEM_FONT(13);
    [headerView addSubview:mobile];
    [mobile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(headerView);
        make.top.mas_equalTo(name.mas_bottom).offset(7);
    }];
    
    UILabel *moneyTitle = [UILabel new];
    moneyTitle.text = Localized(@"付款金额");
    moneyTitle.font = SYSTEM_FONT(14);
    [headerView addSubview:moneyTitle];
    [moneyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(mobile.mas_bottom).offset(29);
    }];
    
    UILabel *unit = [UILabel new];
    unit.text = @"¥";
    unit.font = SYSTEM_FONT(26);
    [headerView addSubview:unit];
    [unit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(moneyTitle.mas_bottom).offset(30);
    }];
    
    _moneyTF = [UITextField new];
    _moneyTF.text = isEmptyString(self.money)?@"":[NSString stringWithFormat:@"%.2f", [self.money floatValue]];
    [headerView addSubview:_moneyTF];
    _moneyTF.font = SYSTEM_FONT(26);
    _moneyTF.keyboardType = UIKeyboardTypeDecimalPad;
    [_moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(unit.mas_right).offset(8);
        make.centerY.mas_equalTo(unit);
        make.right.mas_equalTo(headerView).offset(-12);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = Color(@"E8E8E8");
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(unit.mas_bottom).offset(18);
    }];
    
    _remarkTF = [UITextField new];
    _remarkTF.placeholder = Localized(@"添加备注(20字以内)");
    _remarkTF.text = isEmptyString(self.remark)?@"":self.remark;
    [headerView addSubview:_remarkTF];
    _remarkTF.font = SYSTEM_FONT(14);
    [_remarkTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(lineView.mas_bottom).offset(14);
        make.right.mas_equalTo(headerView).offset(-12);
    }];
    
    UIButton *confirm = [BaseUITool buttonWithTitle:Localized(@"material_dialog_positive_text") titleColor:[UIColor whiteColor] font:SYSTEM_FONT(15) superView:self.view];
    [confirm setBackgroundColor:mainColor];
    confirm.layer.masksToBounds = YES;
    confirm.layer.cornerRadius = 5;
    [confirm addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [confirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.height.mas_equalTo(44);
    }];
}

- (void)confirmAction {
    if (isEmptyString(self.moneyTF.text)) {
        Alert(@"请输入金额！");
        return;
    }
    NSMutableDictionary *params = [@{@"supply_name":self.username,@"pay_total":self.moneyTF.text} mutableCopy];
    if (!isEmptyString(self.remarkTF.text)) {
        [params setObject:self.remarkTF.text forKey:@"remark"];
    }
    
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager quickorderCreateQuickOrder:params Succrss:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            OrderPayViewController *order= [OrderPayViewController new];
            NSString *ord = dic[@"data"][@"order_id"];
            order.orderId = ord;
            order.tableName = dic[@"data"][@"table_name"];
            [self.navigationController pushViewController:order animated:YES];
        } else {
            Alert(state.info);
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        Alert(TOAST_REQUEST_FAIL);
    }];
}

- (void)requestData {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getQuickOrderInfoWithUid:self.uid success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        self.userModel = [QuickOrderInfoModel mj_objectWithKeyValues:dic[@"data"]];
        if (state.status) {
            [self setContentView];
        } else {
            Alert(state.info);
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        Alert(TOAST_REQUEST_FAIL);
    }];
}

@end
