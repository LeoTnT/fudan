//
//  QRCodeViewController.h
//  App3.0
//
//  Created by mac on 2018/4/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface QRCodeViewController : XSBaseTableViewController

@property (copy, nonatomic) NSString *is_shop;
@property (copy, nonatomic) NSString *qrImageString;

@end
