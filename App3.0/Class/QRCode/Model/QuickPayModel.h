//
//  QuickPayModel.h
//  App3.0
//
//  Created by mac on 2018/4/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuickOrderInfoModel : NSObject
@property (nonatomic, copy) NSString *username;     // 收款方用户名
@property (nonatomic, copy) NSString *name;         // 收款方名称
@property (nonatomic, copy) NSString *industryname; // 店铺的行业
@property (nonatomic, copy) NSString *truename;     // 个人的姓名
@property (nonatomic, copy) NSString *logo;         // 收款方Logo
@property (nonatomic, copy) NSString *mobile;       // 个人的手机号
@property (nonatomic, assign) CGFloat quick_pay;    // 收款方收款比例
@end

@interface QuickPayModel : NSObject

@end
