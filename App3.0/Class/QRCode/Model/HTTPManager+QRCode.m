//
//  HTTPManager+QRCode.m
//  App3.0
//
//  Created by mac on 2018/4/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HTTPManager+QRCode.h"

@implementation HTTPManager (QRCode)
+ (void)getQuickOrderInfoWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GetQuickOrderInfo parameters:@{@"uid":uid} success:success failure:fail];
}

+ (void)createUserQuickPayQrcodeWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_CreateQrcode_User parameters:params success:success failure:fail];
}

+ (void)createOfflineSupplyQuickPayQrcodeWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_CreateQrcode_Supply parameters:params success:success failure:fail];
}

@end
