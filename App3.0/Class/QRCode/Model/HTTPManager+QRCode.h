//
//  HTTPManager+QRCode.h
//  App3.0
//
//  Created by mac on 2018/4/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSApi.h"

// 获取快速买单确认页信息
#define Url_GetQuickOrderInfo      @"/api/v1/buyer/quickorder/GetQuickOrderInfo"

// 生成线下店铺快速买单收款码
#define Url_CreateQrcode_Supply      @"/api/v1/supply/quickorder/CreateQrcode"

// 生成个人快速买单收款码
#define Url_CreateQrcode_User      @"/api/v1/user/quickorder/CreateQrcode"

@interface HTTPManager (QRCode)

/* 获取快速买单确认页信息
 */
+ (void)getQuickOrderInfoWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/* 生成个人快速买单收款码
 */
+ (void)createUserQuickPayQrcodeWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/* 生成线下店铺快速买单收款码
 */
+ (void)createOfflineSupplyQuickPayQrcodeWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

@end
