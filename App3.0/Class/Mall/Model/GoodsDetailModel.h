//
//  GoodsDetailModel.h
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TabMallModel.h"

typedef NS_ENUM(NSUInteger, GoodSellType) {
    GoodSellNormal = 0,
    GoodSellWholesale,        //批发商品
    GoodSellScore,              //积分商品     普通 积分  批发的promotion都是normal
    GoodSellGroupBuy,       // 团购（创建订单需要额外参数 purchaseType=shareOrder）
    GoodSellSecondKill,      // 秒杀
    GoodSellWeShop,           // 微店商品（创建订单需要额外参数 shop_id）目前是深渊康项目的
    GoodSellBook,              //预售
    GoodSellFillPrice,              // 补差价商品
};

@interface GoodsStoreStatisticsModel:NSObject
@property(nonatomic,strong)NSNumber *productNumAll;
@property(nonatomic,strong)NSNumber *productNumNew;
@property(nonatomic,strong)NSNumber *favNumSupply;
@end


//用于判断促销类型(团购  秒杀等)
@interface  Promotion: GroupBuyDetailModel
@property(nonatomic,copy)NSString *promotion;  //normal未促销(普通  批发  积分都属于normal,但是sell_type不同)   second秒杀    group团购   book预售
@end

@interface Spec_valueItem : NSObject
@property(nonatomic,copy)NSString *spec_value_id;
@property(nonatomic,copy)NSString *spec_value_name;
@end

@interface SpecItem : NSObject
@property(nonatomic,copy)NSString *spec_id;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,strong)NSArray *spec_value;
@end

@interface WholesalePriceModel : NSObject
@property(nonatomic,copy)NSString *max_number;
@property(nonatomic,copy)NSString *min_number;
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *bonus_percent;
@property(nonatomic,copy)NSString *origin_price;
@end

@interface WholesaleConfigModel : NSObject
@property(nonatomic,copy)NSString *moq;
@property(nonatomic,copy)NSString *rrp;
@property(nonatomic,copy)NSString *max_origin_price;
@property(nonatomic,copy)NSString *max_price;
@property(nonatomic,copy)NSString *min_origin_price;
@property(nonatomic,copy)NSString *min_price;
@property(nonatomic,strong)NSArray *price;
@end

@interface WholesaleConfigAppModel : NSObject
@property(nonatomic,copy)NSString *moq;
@property(nonatomic,strong)NSArray *price;
@end

@interface WholesaleNumberModel : NSObject
@property(nonatomic,copy)NSString *min_number;
@property(nonatomic,copy)NSString *max_number;
@property(nonatomic,strong)NSArray *fields;
@end

@interface WholesaleSpecModel : NSObject   //这个模型根据数量的也可以用
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *value;
@property(nonatomic,copy)NSString *min_price;
@property(nonatomic,copy)NSString *max_price;
@end

@interface ProductInfo : NSObject
@property(nonatomic,strong)NSString *spec_info;//json  需特殊处理
@property(nonatomic,copy)NSString *product_id;
@property(nonatomic,copy)NSString *product_name;
@property(nonatomic,strong)NSNumber *category_id;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *image_list;
@property(nonatomic,copy)NSString *look_num;
@property(nonatomic,copy)NSString *eva_num;
@property(nonatomic,copy)NSString *is_cod;
@property(nonatomic,copy)NSString *product_ext_id;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,copy)NSString *spec_value;
@property(nonatomic,copy)NSString *product_no;
@property(nonatomic,copy)NSString *product_weight;
@property(nonatomic,copy)NSString *stock;
@property(nonatomic,copy)NSString *sell_price;
@property(nonatomic,copy)NSString *market_price;
@property(nonatomic,copy)NSString *supply_price;
@property(nonatomic,copy)NSString *pv;
@property(nonatomic,copy)NSString *score;
@property(nonatomic,copy)NSString *eva;//评分
@property(nonatomic,copy)NSString *coupon;
@property(nonatomic,strong)NSNumber *user_id;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,strong)NSNumber *sell_type;
@property(nonatomic,copy)NSString *reward_coupon;
@property(nonatomic,copy)NSString *reward_score;
@property(nonatomic,strong)NSArray *spec;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *reward_power;
@property (nonatomic, copy) NSString *delivery_price;
@property(nonatomic, strong) WholesaleConfigModel *wholesale_config;        // 旧的，弃用
@property(nonatomic, strong) WholesaleConfigAppModel *wholesale_config_app; // 新的批发配置
@property (nonatomic, assign) unsigned int wholesale_price_way;                  // 1是按数量计价 2 是按所选规格计价
@property (nonatomic, copy) NSString *wholesale_unit;
// 团购专用
@property (nonatomic, copy) NSString *share_sell_price;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *active_table;
@property (nonatomic, copy) NSString *active_id;
@property (nonatomic, copy) NSString *num_limit;
@property (nonatomic, copy) NSString *remain;
@property (nonatomic, copy) NSString *num;
@property (nonatomic, copy) NSString *book_start_time;
@property (nonatomic, copy) NSString *book_end_time;
@end

@interface SupplyInfo : NSObject
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *tel;
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,strong)NSNumber *province;
@property(nonatomic,strong)NSNumber *city;
@property(nonatomic,strong)NSNumber *county;
@property(nonatomic,strong)NSNumber *town;
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *user_id;
@property(nonatomic,copy)NSString *mobile;
@property(nonatomic,copy)NSString *showname;
@end

@interface RecommendProduct : NSObject
@property(nonatomic,copy)NSString *total;
@property(nonatomic,copy)NSString *per_page;
@property(nonatomic,copy)NSString *current_page;
@property(nonatomic,strong)NSArray *data;
@end

@interface RecommendProductDataItem : NSObject
@property(nonatomic,copy)NSString * image;
 @property(nonatomic,copy)NSString *image_thumb;
 @property(nonatomic,copy)NSString *look_num;
 @property(nonatomic,copy)NSString *market_price;
@property(nonatomic,copy)NSString *product_id;
@property(nonatomic,copy)NSString *product_name;
@property(nonatomic,copy)NSString *score;
@property(nonatomic,copy)NSString *sell_num_month;
@property(nonatomic,copy)NSString *sell_price;
@property(nonatomic,copy)NSString *sell_type;
@property(nonatomic,copy)NSString *sell_num;
@end

@interface SupplyCategoryItem : NSObject
@property(nonatomic,strong)NSNumber *category_supply_id;
@property(nonatomic,copy)NSString *category_supply_name;
@property(nonatomic,strong)NSNumber *user_id;
@end

@interface Lev : NSObject
@property(nonatomic,copy)NSString *desc;
@property(nonatomic,strong)NSNumber *score;
@property(nonatomic,copy)NSString *change;
@end

@interface SupplyEvaluation : NSObject
@property(nonatomic,strong)Lev *ms_lev;//描述
@property(nonatomic,strong)Lev *fh_lev;//发货
@property(nonatomic,strong)Lev *fw_lev;//服务
@property(nonatomic,strong)Lev *wl_lev;//物流
@end

@interface ProductEvaSum : NSObject
@property(nonatomic,copy)NSString *sum;
@property(nonatomic,copy)NSString *good;
@property(nonatomic,copy)NSString *normal;
@property(nonatomic,copy)NSString *bad;
@property(nonatomic,copy)NSString *img;//有图
@end

@interface ProductEvaluationItem : NSObject
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,strong)NSNumber *user_id;
@property(nonatomic,strong)NSNumber *is_private;
@property(nonatomic,copy)NSString *comments;
@property(nonatomic,copy)NSString *pic;
@property(nonatomic,copy)NSString *reply;
@property(nonatomic,strong)NSNumber *w_time;
@property(nonatomic,strong)NSNumber *ms_lev;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,copy)NSString *nickname;
//追加评论
@property(nonatomic,copy)NSString *zhui_content;
@property(nonatomic,copy)NSString *zhui_img;
@property(nonatomic,copy)NSString *zhui_time;
@end

@interface productOrderListItem : NSObject
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,strong)NSNumber *product_num;
@property(nonatomic,copy)NSString *sell_price;
@property(nonatomic,strong)NSNumber *is_private;
@property(nonatomic,strong)NSNumber *pay_time;
@end

@interface Showfield : NSObject
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *title;
@end

@interface GoodsDetailInfo:NSObject
@property(nonatomic,copy)NSString *recAddress;//收货地址
@property(nonatomic,strong)NSArray *recAddressAry;//收货地址数组
@property(nonatomic,strong)ProductInfo *productInfo;
@property(nonatomic,strong)SupplyInfo *supplyInfo;
@property (nonatomic, strong)Promotion *promotion;
@property(nonatomic,copy)NSString *sendAddress;
//@property(nonatomic,copy)NSArray *productField;
@property(nonatomic,strong)NSArray *showfield;
@property(nonatomic,copy)NSString *favNum;
@property(nonatomic,copy)NSString *isFavorite;
@property(nonatomic,copy)NSString *productQrcode;
@property(nonatomic,copy)NSString *supplyQrcode;
@property(nonatomic,strong)RecommendProduct *recommendProduct;
@property(nonatomic,strong)NSArray *supplyCategory;
@property(nonatomic,strong)SupplyEvaluation *supplyEvaluation;
@property(nonatomic,strong)ProductEvaSum *productEvaSum;
@property(nonatomic,strong)NSArray *productEvaluation;
@property(nonatomic,strong)NSArray *productOrderList;
@property(nonatomic,strong)RecommendProduct *lookHistory;
@property(nonatomic,copy)NSString *menu_cate_list;

@property(nonatomic,strong)GroupBuyDetailModel *share;  // 团购
@property(nonatomic,strong)GroupBuyDetailModel *group;  // 秒杀
@end

@interface ProductMapModel : NSObject
@property(nonatomic,copy)NSString *product_ext_id;
@property(nonatomic,copy)NSString *product_id;
@property(nonatomic,copy)NSString *product_no;
@property(nonatomic,copy)NSString *product_weight;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,copy)NSString *spec_value;
@property(nonatomic,copy)NSString *stock;
@property(nonatomic,copy)NSString *sell_price;
@property(nonatomic,copy)NSString *market_price;
@property(nonatomic,copy)NSString *supply_price;
@property(nonatomic,copy)NSString *pv;
@property(nonatomic,copy)NSString *score;
@property(nonatomic,copy)NSString *coupon;
@property(nonatomic,copy)NSString *user_id;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *sell_type;
@property(nonatomic,copy)NSString *reward_coupon;
@property(nonatomic,copy)NSString *reward_score;
@property(nonatomic,copy)NSString *delivery_price;
@property(nonatomic,copy)NSString *spec_id;
@property(nonatomic,copy)NSString *source;
@property(nonatomic,copy)NSString *rrp;
@property(nonatomic,copy)NSString *bonus_percent;
@property(nonatomic,copy)NSString *score_percent;
@property(nonatomic,copy)NSString *other_percent;
@property(nonatomic,copy)NSString *other_account_type;
@property(nonatomic,copy)NSString *book_isbook;
@property(nonatomic,copy)NSString *book_start_time;
@property(nonatomic,copy)NSString *book_end_time;
@property(nonatomic,copy)NSString *origin_price;
@property(nonatomic,copy)NSString *spec_category_value;
@end

@interface GoodsDetailModel : NSObject

@end
//注释
//* @return_format string sendAddress 发货地址
//* @return_format string recAddress 用户收货地址(需要登录)
//* @return_format array recAddressAry 用户收货地址(需要登录)
//* @return_format array productInfo 商品信息
//* @return_format string productInfo[stock] 库存
//* @return_format string productInfo[freight_price] 运费
//* @return_format string productInfo[sell_price] 售价
//* @return_format string productInfo[market_price] 市场价
//* @return_format string productInfo[spec_name] 规格名
//* @return_format string productInfo[spec_value] 规格值
//* @return_format array productField 商品显示字段
//* @return_format array showfield 商品显示字段
//* @return_format array supplyInfo 商家信息
//* @return_format array supplyEvaSum 评价总数
//* @return_format array supplyEvaluation 店铺评分
//* @return_format integer favNum 关注人数
//* @return_format integer productNumAll 店铺商品总数
//* @return_format integer productNumNew 店铺新品数
//* @return_format array recommendProduct 推荐商品
//* @return_format string productQrcode 商品二维码
//* @return_format string supplyQrcode 店铺二维码
