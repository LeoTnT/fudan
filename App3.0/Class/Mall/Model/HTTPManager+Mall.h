//
//  HTTPManager+Mall.h
//  App3.0
//
//  Created by mac on 2017/10/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"

// 获取商家拼团列表
#define Url_GroupBuy_List           @"/api/v1/buyer/shareorder/Lists"

// 获取秒杀商品列表
#define Url_Secondkill_List           @"/api/v1/buyer/groupbuy/Lists"

@interface HTTPManager (Mall)


/* 获取商家拼团列表
 */
+ (void)getGroupBuyListWithPage:(NSInteger)page success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/* 获取秒杀商品列表
 */
+ (void)getSecondKillListWithPage:(NSInteger)page success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
@end
