//
//  GoodsTypeModel.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsTypeModel.h"
#import "XSApi.h"
@implementation BaseTypeGoodsParser
@synthesize data;
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"data":@"BaseTypeGoodsDataParser"
             };
}
@end

@implementation BaseTypeGoodsDataParser
@end

@implementation GoodsTypeDataParser

@end

@implementation GoodsTypeParser
@synthesize data;

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"data":@"GoodsTypeDataParser"
             };
}
@end

@implementation GoodsTypeModel

@end
