//
//  GoodsTypeModel.h
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright  2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseTypeGoodsParser : NSObject
@property (nonatomic, retain) NSArray *data;
@end

@interface BaseTypeGoodsDataParser : NSObject
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_ext_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *image_thumb;
@property (nonatomic, copy) NSString *look_num;
@property (nonatomic, copy) NSString *product_no;
@property (nonatomic, copy) NSString *product_weight;
@property (nonatomic, copy) NSString *spec_name;
@property (nonatomic, copy) NSString *spec_value;
@property (nonatomic, copy) NSString *stock;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *market_price;
@property (nonatomic, copy) NSString *supply_price;
@property (nonatomic, copy) NSString *pv;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *coupon;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *sell_type;
@property (nonatomic, copy) NSString *reward_coupon;
@property (nonatomic, copy) NSString *reward_score;
@property (nonatomic, copy) NSString *supply_user_id;
@property (nonatomic, copy) NSString *supply_name;
@property (nonatomic, copy) NSString *supply_tel;
@end


@interface GoodsTypeDataParser : NSObject
@property (nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *category_name;
@property (nonatomic, copy) NSString *top_id;
@property (nonatomic, copy) NSString *parent_id;
@property (nonatomic, copy) NSString *parent_path;
@property (nonatomic, copy) NSString *ceng;
@property (nonatomic, copy) NSString *is_index;
@property (nonatomic, copy) NSString *is_nav;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *u_time;
@end

@interface GoodsTypeParser : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface GoodsTypeModel : NSObject


@end
