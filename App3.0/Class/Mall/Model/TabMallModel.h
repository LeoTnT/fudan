//
//  TabMallModel.h
//  App3.0
//
//  Created by mac on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
//商城首界面滚动视图
@interface TabMallADItem :NSObject
@property(nonatomic,copy)NSString *backcolor;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *url;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *link_in;
@property(nonatomic,copy)NSString *link_objid;
@end


//商城首界面分类
@interface TabMallMenuItem :NSObject
@property(nonatomic,copy)NSString *target;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *url;
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,copy)NSString *link_in;
@property(nonatomic,copy)NSString *link_objid;
@end
//商城首界面公告
@interface TabMallNoticeItem : NSObject
@property(nonatomic,copy)NSString *w_time;
@property(nonatomic,copy)NSString *adver_id;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *u_time;
@end
//商城首界面猜你喜欢商品
@interface TabMallGoodsItem : NSObject
@property(nonatomic,copy)NSString *product_id;
@property(nonatomic,strong)NSNumber *product_ext_id;
@property(nonatomic,copy)NSString *product_name;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *image_thumb;
@property(nonatomic,strong)NSNumber *look_num;
@property(nonatomic,copy)NSString *product_no;
@property(nonatomic,copy)NSString *product_weight;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,copy)NSString *spec_value;
@property(nonatomic,copy)NSString *stock;
@property(nonatomic,copy)NSString *sell_price;
@property(nonatomic,copy)NSString *market_price;
@property(nonatomic,copy)NSString *supply_price;
@property(nonatomic,copy)NSString *pv;
@property(nonatomic,copy)NSString *score;
@property(nonatomic,strong)NSNumber *coupon;
@property(nonatomic,copy)NSString *user_id;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,strong)NSNumber *sell_type;
@property(nonatomic,copy)NSString *reward_coupon;
@property(nonatomic,copy)NSString *reward_score;
@property(nonatomic,strong)NSNumber *recommend_type;
@property (nonatomic ,strong)UIImage *prodouctImage;
@end

//商城首界面分类-商品
@interface TabMallCategory_productItem : NSObject
@property(nonatomic,copy)NSString *product_id;
@property(nonatomic,copy)NSString *product_name;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *sell_price;
@end
//商城首界面分类-品牌
@interface TabMallCategory_brandItem : NSObject
@property(nonatomic,strong)NSNumber *brand_id;
@property(nonatomic,copy)NSString *brand_name;
@property(nonatomic,copy)NSString *image;
@end
//商城首界面分类
@interface TabMallCategoryItem : NSObject
@property(nonatomic,copy)NSString *category_id;
@property(nonatomic,copy)NSString *category_name;
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,strong)NSArray *sub_products;
@property(nonatomic,strong)NSArray *sub_brands;
@property(nonatomic,strong)TabMallADItem *sub_advert;
@property(nonatomic,copy)NSString *sub_products_total;  // 总数
@end
//商城首界面返回字典
@interface  TabMallCategoryInfo: NSObject
@property(nonatomic,strong)NSArray *data;
@end
//品牌
@interface Brand : NSObject
@property(nonatomic,strong)NSNumber *brand_id;
@property(nonatomic,copy)NSString *brand_name;
@property(nonatomic,copy)NSString *image;
@end

// 团购
@interface GroupBuyDetailModel : NSObject
@property (nonatomic, copy)NSString *product_id;
@property (nonatomic, copy)NSString *product_name;
@property (nonatomic, copy)NSString *audit_time;    // 审核时间（用不到）
@property (nonatomic, copy)NSString *begin_time;
@property (nonatomic, copy)NSString *end_time;
@property (nonatomic, copy)NSString *group_min;
@property (nonatomic, copy)NSString *img;
@property (nonatomic, assign)unsigned int is_show;
@property (nonatomic, assign)unsigned int is_audit;
@property (nonatomic, copy)NSString *market_price;
@property (nonatomic, copy)NSString *num;
@property (nonatomic, copy)NSString *num_limit;
@property (nonatomic, copy)NSString *remain;
@property (nonatomic, copy)NSString *sell_price;
@property (nonatomic, assign)unsigned int status;
@property (nonatomic, copy)NSString *title;
@property (nonatomic, copy)NSString *uid;
@end

@interface GroupBuyPageModel : NSObject
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, copy)NSNumber *current_page;
@property (nonatomic, copy)NSNumber *per_page;
@property (nonatomic, copy)NSNumber *total;
@end

@interface GroupBuyModel : NSObject
@property (nonatomic, strong) GroupBuyPageModel *data;
@end

#pragma mark - 商城楼层相关
/*
 商品数量
 */
@interface MallProductCountModel : NSObject
@property (nonatomic, copy) NSString *product_new_total;
@property (nonatomic, copy) NSString *product_total;
@end

@interface MallFloorModel : NSObject
@property (nonatomic, strong) NSArray *list_category;
@property (nonatomic, strong) MallProductCountModel *product_count;
@end

@interface TabMallModel : NSObject

@end
