//
//  GoodsDetailModel.m
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailModel.h"

@implementation GoodsStoreStatisticsModel

@end

@implementation Promotion

@end

@implementation  Spec_valueItem

@end

@implementation SpecItem
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"spec_value":@"Spec_valueItem"
             };
}
@end

@implementation  WholesalePriceModel

@end

@implementation  WholesaleConfigModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"price":@"WholesalePriceModel"
             };
}
@end


@implementation  WholesaleConfigAppModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"price":@"NSDictionary"
             };
}
@end

@implementation  WholesaleNumberModel
@end

@implementation  WholesaleSpecModel
@end




@implementation ProductInfo
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"spec":@"SpecItem"
             };
}
@end

@implementation SupplyInfo

@end

@implementation RecommendProductDataItem

@end

@implementation RecommendProduct
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"RecommendProductDataItem"
             };
}
@end

@implementation SupplyCategoryItem

@end

@implementation Lev

@end

@implementation SupplyEvaluation

@end

@implementation ProductEvaSum

@end

@implementation ProductEvaluationItem

@end

@implementation productOrderListItem

@end

@implementation Showfield

@end

@implementation GoodsDetailInfo
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"recAddressAry":@"NSString",
             @"supplyCategory":@"SupplyCategoryItem",
             @"productEvaluation":@"ProductEvaluationItem",
             @"productOrderList":@"productOrderListItem",
             @"showfield":@"Showfield"
             };
}
@end

@implementation ProductMapModel
@end
@implementation GoodsDetailModel
@end
