//
//  HTTPManager+Mall.m
//  App3.0
//
//  Created by mac on 2017/10/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HTTPManager+Mall.h"

@implementation HTTPManager (Mall)
+ (void)getGroupBuyListWithPage:(NSInteger)page success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GroupBuy_List parameters:@{@"page":@(page)} success:success failure:fail];
}

+ (void)getSecondKillListWithPage:(NSInteger)page success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_Secondkill_List parameters:@{@"page":@(page)} success:success failure:fail];
}
@end
