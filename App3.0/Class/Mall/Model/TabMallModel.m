//
//  TabMallModel.m
//  App3.0
//
//  Created by mac on 2017/4/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallModel.h"

@implementation TabMallADItem

@end

@implementation TabMallMenuItem

@end

@implementation TabMallNoticeItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"adver_id":@"id"};
}
@end

@implementation TabMallGoodsItem

@end

@implementation TabMallCategory_productItem

@end

@implementation TabMallCategory_brandItem

@end

@implementation TabMallCategoryItem
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"sub_products":@"TabMallGoodsItem",
             @"sub_brands":@"TabMallCategory_brandItem"
             };
}
@end

@implementation  TabMallCategoryInfo
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"TabMallCategoryItem",
             };
}
@end
@implementation Brand
@end

@implementation GroupBuyDetailModel
@end

@implementation GroupBuyPageModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"GroupBuyDetailModel"
             };
}
@end

@implementation GroupBuyModel
@end

@implementation MallProductCountModel
@end

@implementation MallFloorModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"list_category" : @"TabMallCategoryItem"
             };
}
@end

@implementation TabMallModel

@end
