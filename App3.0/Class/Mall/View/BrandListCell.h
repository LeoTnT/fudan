//
//  BrandListCell.h
//  App3.0
//
//  Created by mac on 2017/6/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
@interface BrandListCell : UITableViewCell
@property(nonatomic,strong)Brand *brand;
@end
