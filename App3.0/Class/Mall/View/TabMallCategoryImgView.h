//
//  TabMallCategoryImgView.h
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
@interface TabMallCategoryImgView : UIImageView
@property(nonatomic,strong)TabMallCategory_productItem *productItem;
@property(nonatomic,strong)TabMallCategory_brandItem *brandItem;
@property(nonatomic,strong)TabMallADItem *adItem;
@end
