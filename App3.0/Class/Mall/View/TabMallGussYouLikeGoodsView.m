//
//  TabMallGussYouLikeGoodsView.m
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallGussYouLikeGoodsView.h"
@interface TabMallGussYouLikeGoodsView()
@property(nonatomic,strong)UIImageView *goodsImage;
@property(nonatomic,strong)UILabel *goodsNameLabel;
@property(nonatomic,strong)UILabel *shopNameLabel;
@property(nonatomic,strong)UILabel *goodsPriceLabel;
@property(nonatomic,strong)UILabel *interestedNumLabel;
@end
@implementation TabMallGussYouLikeGoodsView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor whiteColor];
        self.userInteractionEnabled=YES;
        CGFloat imgHeight =200,space=10;
        self.goodsImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,frame.size.width , imgHeight)];
        self.goodsImage.contentMode = UIViewContentModeScaleAspectFit;//关键在此
        
        [self addSubview:self.goodsImage];
        self.goodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(space,CGRectGetMaxY(self.goodsImage.frame) , frame.size.width-2*space, 50)];
        self.goodsNameLabel.numberOfLines = 2;
        self.goodsNameLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview:self.goodsNameLabel];
        self.shopNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(self.goodsNameLabel.frame),frame.size.width-2*space , 30)];
        self.shopNameLabel.font=[UIFont systemFontOfSize:15];
        self.shopNameLabel.textColor=mainGrayColor;
        [self addSubview:self.shopNameLabel];
        self.goodsPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(self.shopNameLabel.frame), (frame.size.width-2*space)/2.0, 30)];
        self.goodsPriceLabel.textAlignment=NSTextAlignmentLeft;
        self.goodsPriceLabel.textColor=[UIColor redColor];
        self.goodsPriceLabel.font=[UIFont systemFontOfSize:12];
        [self addSubview:self.goodsPriceLabel];
        self.interestedNumLabel=[[UILabel alloc] initWithFrame:CGRectMake( (frame.size.width-2*space)/2.0+space,CGRectGetMaxY(self.shopNameLabel.frame) ,  (frame.size.width-2*space)/2.0, 30)];
        self.interestedNumLabel.font=[UIFont systemFontOfSize:12];
        self.interestedNumLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:self.interestedNumLabel];

    }
    return self;
}
-(void)setGoodsItem:(TabMallGoodsItem *)goodsItem{
    _goodsItem=goodsItem;

    if (goodsItem.image) {
        [self.goodsImage getImageWithUrlStr:goodsItem.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    }else{
        self.goodsImage.image=[UIImage imageNamed:@"no_pic"];
    }
    self.goodsNameLabel.text=goodsItem.product_name;
    NSString *str1=goodsItem.sell_price;
    long length1=str1.length;
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@元",str1]];
    [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, length1)];
    self.goodsPriceLabel.attributedText=attri;
    self.shopNameLabel.text=goodsItem.username;
    self.interestedNumLabel.text=[NSString stringWithFormat:@"%@人感兴趣",goodsItem.look_num];
}
@end
