//
//  HotSaleGoodsView.h
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
@interface HotSaleGoodsView : UIView
@property(nonatomic,strong)TabMallGoodsItem *goodsItem;
@end
