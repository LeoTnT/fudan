//
//  HotSaleTableViewCell.h
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotSaleGoodsView.h"

@interface HotSaleTableViewCell : UITableViewCell
@property(nonatomic,strong)NSArray *goodsArray;
@property(nonatomic,strong)HotSaleGoodsView *firstGoodsView;
@property(nonatomic,strong)HotSaleGoodsView *secondGoodsView;
@end
