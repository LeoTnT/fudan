//
//  HotSaleGoodsCell.m
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HotSaleTableViewCell.h"

@interface HotSaleTableViewCell()
@end
@implementation HotSaleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor=BG_COLOR;
        CGFloat space=3,height=200;
        self.firstGoodsView=[[HotSaleGoodsView alloc] initWithFrame:CGRectMake(0,space, (mainWidth-space)/2.0,height-space)];
        [self.contentView addSubview:self.firstGoodsView];
        self.secondGoodsView=[[HotSaleGoodsView alloc] initWithFrame:CGRectMake((mainWidth+space)/2.0, space, (mainWidth-space)/2.0 , height-space)];
        [self.contentView addSubview:self.secondGoodsView];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setGoodsArray:(NSArray *)goodsArray{
    _goodsArray=goodsArray;
    if (goodsArray.count>1) {
        self.firstGoodsView.goodsItem=goodsArray.firstObject;
        self.secondGoodsView.goodsItem=goodsArray.lastObject;
    }else{
        self.firstGoodsView.goodsItem=goodsArray.firstObject;
    }
}
@end
