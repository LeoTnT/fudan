//
//  HotSaleGoodsView.m
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HotSaleGoodsView.h"
@interface HotSaleGoodsView()
@property(nonatomic,strong)UIImageView *productImgView;
@property(nonatomic,strong)UILabel *productNameLabel;
@property(nonatomic,strong)UILabel *productPriceLabel;
@end
@implementation HotSaleGoodsView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor whiteColor];
        CGFloat space=10;
        self.productImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0 , frame.size.width, frame.size.height*0.7)];
        self.productImgView.contentMode = UIViewContentModeScaleAspectFit;
        self.productImgView.image=[UIImage imageNamed:@"no_pic"];
        [self addSubview:self.productImgView];
        self.productNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(self.productImgView.frame), frame.size.width-2*space, frame.size.height*0.15)];
        [self addSubview:self.productNameLabel];
        self.productPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(self.productNameLabel.frame), frame.size.width-2*space, frame.size.height*0.15)];
        self.productPriceLabel.textColor=[UIColor redColor];
        [self addSubview:self.productPriceLabel];
    }
    return self;
}
-(void)setGoodsItem:(TabMallGoodsItem *)goodsItem{
    _goodsItem=goodsItem;
    [self.productImgView getImageWithUrlStr:goodsItem.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.productNameLabel.text=goodsItem.product_name;
    self.productPriceLabel.text=goodsItem.sell_price;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
