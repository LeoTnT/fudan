//
//  GrayBackView.h
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GrayBackViewDelegate<NSObject>
@optional
-(void)grayViewClicked;
@end
@interface GrayBackView : UIView
@property(nonatomic,weak)id <GrayBackViewDelegate> delegate;
@end
