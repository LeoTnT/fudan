//
//  TabMallGussYouLikeGoodsCell.m
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallGussYouLikeGoodsCell.h"
@implementation TabMallGussYouLikeGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor=BG_COLOR;
        CGFloat space=3,width=(mainWidth-3)/2.0,height=310;
        self.firstGoodsView=[[TabMallGussYouLikeGoodsView alloc] initWithFrame:CGRectMake(0, space, width, height)];
        [self.contentView addSubview:self.firstGoodsView];
        self.secondGoodsView=[[TabMallGussYouLikeGoodsView alloc] initWithFrame:CGRectMake(space+width, space, width, height)];
        [self.contentView addSubview:self.secondGoodsView];
    }
    return self;
}
-(void)setGoodsItemArray:(NSArray *)goodsItemArray{
    _goodsItemArray=goodsItemArray;
    if (goodsItemArray.count==1) {//一个商品
        self.firstGoodsView.goodsItem=[goodsItemArray firstObject];
        self.secondGoodsView.hidden=YES;
    }else{//两个商品
        self.firstGoodsView.goodsItem=[goodsItemArray firstObject];
        self.secondGoodsView.goodsItem=[goodsItemArray lastObject];
        self.secondGoodsView.hidden=NO;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
