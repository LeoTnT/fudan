//
//  TabMallCategoryImgView.m
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallCategoryImgView.h"

@implementation TabMallCategoryImgView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.userInteractionEnabled=YES;
    }
    return self;
}
-(void)setBrandItem:(TabMallCategory_brandItem *)brandItem{
    _brandItem=brandItem;
}
-(void)setAdItem:(TabMallADItem *)adItem{
    _adItem=adItem;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
