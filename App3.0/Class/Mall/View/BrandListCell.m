//
//  BrandListCell.m
//  App3.0
//
//  Created by mac on 2017/6/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BrandListCell.h"
@interface BrandListCell()
@property(nonatomic,strong)UIImageView *brandImage;
@property(nonatomic,strong)UILabel *brandNameLabel;
@end

@implementation BrandListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.brandImage=[UIImageView new];
        [self.contentView addSubview:self.brandImage];
        [self.brandImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        self.brandNameLabel = [UILabel new];
        self.brandNameLabel.font = [UIFont systemFontOfSize:15];
        self.brandNameLabel.textColor = mainGrayColor;
        [self.contentView addSubview:self.brandNameLabel];
        [self.brandNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.brandImage.mas_right).offset(12);
            make.centerY.mas_equalTo(self);
        }];
    }
    return self;
}
-(void)setBrand:(Brand *)brand{
    _brand=brand;
    [self.brandImage getImageWithUrlStr:brand.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.brandNameLabel.text=brand.brand_name;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
