//
//  TypeTopView.h
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TypeTopViewDelegate<NSObject>
- (void)lookProductListWithCategoryId:(NSString *)categoryId;
@end

@interface TypeTopView : UIView
@property (nonatomic, strong) NSArray *typeArray;//子类数组
@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, weak) id<TypeTopViewDelegate> delegate;
@end
