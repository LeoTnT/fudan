//
//  TypeGoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/11.
//  Copyright  2017年 mac. All rights reserved.
//

#import "TypeGoodsTableViewCell.h"
#import "UIImage+XSWebImage.h"
#import "XSCustomButton.h"
@interface TypeGoodsTableViewCell ()
@property (nonatomic, strong) UIImageView *goodsImage;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *shopImage;
@property (nonatomic, strong) UILabel *shopName;
@property (nonatomic, strong) XSCustomButton *chatButton;//会话
@property (nonatomic, strong) UILabel *line;
@end

@implementation TypeGoodsTableViewCell

- (instancetype)initWithStyle: (UITableViewCellStyle)style reuseIdentifier: (NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if  (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    CGFloat cellWidth = mainWidth*0.75;
    CGFloat goodsSize = 50;
    self.goodsImage = [[UIImageView alloc] initWithFrame:CGRectMake (NORMOL_SPACE,  NORMOL_SPACE,  goodsSize,  goodsSize)];
    self.goodsImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.goodsImage];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake (CGRectGetMaxX (self.goodsImage.frame)+NORMOL_SPACE,  NORMOL_SPACE,  cellWidth-goodsSize-NORMOL_SPACE*3,  NORMOL_SPACE*3)];
    self.titleLabel.font = [UIFont systemFontOfSize:15];
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:self.titleLabel];
    CGFloat shopImageSize = goodsSize-CGRectGetHeight (self.titleLabel.frame);
    self.shopImage = [[UIImageView alloc] initWithFrame:CGRectMake (CGRectGetMaxX (self.goodsImage.frame)+NORMOL_SPACE,  CGRectGetMaxY (self.titleLabel.frame),  shopImageSize,  shopImageSize)];
    self.shopImage.contentMode = UIViewContentModeScaleAspectFit;
    self.shopImage.image = [UIImage imageNamed:@"mall_category_shop"];
    [self.contentView addSubview:self.shopImage];
    CGFloat chatButtonWidth = 60;
    CGFloat chatButtonHeight = 25;
    self.shopName = [[UILabel alloc] initWithFrame:CGRectMake (CGRectGetMaxX (self.shopImage.frame)+NORMOL_SPACE/2,  CGRectGetMinY (self.shopImage.frame),  cellWidth-CGRectGetMaxX (self.shopImage.frame)-NORMOL_SPACE*3-chatButtonWidth,  shopImageSize)];
    self.shopName.textColor = [UIColor grayColor];
//    self.shopName.lineBreakMode = NSLineBreakByCharWrapping;
    self.shopName.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.shopName];
    self.chatButton = [[XSCustomButton alloc] initWithFrame:CGRectMake (cellWidth-chatButtonWidth-NORMOL_SPACE,  35,  chatButtonWidth,  chatButtonHeight) title:@"发起会话" titleColor:[UIColor whiteColor] fontSize:12 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
    [self.chatButton setBorderWith:0 borderColor:nil cornerRadius:3];
    [self.chatButton addTarget:self action:@selector (clickChatbutton:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.chatButton];
    self.line = [[UILabel alloc] initWithFrame:CGRectMake (NORMOL_SPACE,  CGRectGetMaxY (self.goodsImage.frame)+NORMOL_SPACE, cellWidth,  0.5)];
    self.line.backgroundColor = LINE_COLOR_NORMAL;
    [self.contentView addSubview:self.line];
}

- (void)clickChatbutton:(UIButton *)sender {
    if ([self.typeGoodsDelegate respondsToSelector:@selector(chatSupplyWithId:)]) {
        [self.typeGoodsDelegate chatSupplyWithId:self.dataParser.supply_user_id];
    } 
}
- (void)setDataParser:(BaseTypeGoodsDataParser *)dataParser {
    _dataParser = dataParser;
    [self.goodsImage getImageWithUrlStr:_dataParser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.titleLabel.text = _dataParser.product_name;
    self.shopName.text = _dataParser.supply_name;
}

@end
