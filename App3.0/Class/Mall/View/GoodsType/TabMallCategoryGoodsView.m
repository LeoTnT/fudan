//
//  TabMallCategoryGoodsView.m
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallCategoryGoodsView.h"
@interface TabMallCategoryGoodsView ()
@property (nonatomic, strong) UIImageView *productImgView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *productPriceLabel;
@end
@implementation TabMallCategoryGoodsView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        
        //图片
        self.productImgView = [[UIImageView alloc] init];
        self.productImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.productImgView getImageWithUrlStr:nil andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self addSubview:self.productImgView];
        
        //名称
        self.productNameLabel = [UILabel new];
        self.productNameLabel.font = [UIFont systemFontOfSize:14];
        self.productNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:self.productNameLabel];
        
        //价格
        self.productPriceLabel = [UILabel new];
        self.productPriceLabel.font = [UIFont systemFontOfSize:12];
        self.productPriceLabel.textColor = mainColor;
        [self addSubview:self.productPriceLabel];
    
        [self.productImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(8);
            make.left.mas_equalTo(self).offset(10);
            make.right.mas_equalTo(self).offset(-10);
            make.bottom.mas_equalTo(self).offset(-58);
        }];
        [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.productImgView.mas_bottom).offset(14);
            make.left.mas_equalTo(self).offset(8);
            make.right.mas_equalTo(self).offset(-8);
        }];
        [self.productPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.productNameLabel.mas_bottom).offset(8);
            make.left.right.mas_equalTo(self.productNameLabel);
        }];
    }
    return self;
}

- (void)setProductItem:(ProductDetailParser *)productItem {
    _productItem=productItem;

    [self.productImgView getImageWithUrlStr:productItem.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.productNameLabel.text = productItem.product_name;
    self.productPriceLabel.text = [NSString stringWithFormat:@"¥%@",productItem.sell_price];
}
@end
