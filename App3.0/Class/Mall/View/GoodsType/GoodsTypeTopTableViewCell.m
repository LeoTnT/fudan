//
//  GoodsTypeTopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsTypeTopTableViewCell.h"
#import "TypeTopView.h"

@implementation GoodsTypeTopTableViewCell

- (void)setTypeArray:(NSArray *)typeArray {
    _typeArray = typeArray;
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[TypeTopView class]]) {
            [view removeFromSuperview];
        }
    }
    if (_typeArray.count==0) {
        self.cellHeight = 0;
        return;
    }
    self.typeView = [[TypeTopView alloc] init];
    self.typeView.typeArray = _typeArray;
    self.typeView.frame = CGRectMake(0, 0, mainWidth*0.75, self.typeView.viewHeight);
    [self.contentView addSubview:self.typeView];
    self.cellHeight = CGRectGetMaxY(self.typeView.frame);
}

@end
