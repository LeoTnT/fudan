//
//  TabMallCategoryGoodsView.h
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
@interface TabMallCategoryGoodsView : UIView
@property (nonatomic, strong) ProductDetailParser *productItem;
@end
