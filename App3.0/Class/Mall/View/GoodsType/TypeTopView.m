//
//  TypeTopView.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TypeTopView.h"
#import "UIImage+XSWebImage.h"
#import "GoodsTypeModel.h"

@implementation TypeTopView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setTypeArray:(NSArray *)typeArray {
    _typeArray = typeArray;
    CGFloat width = (mainWidth*0.75-NORMOL_SPACE*3)/3;
    if (_typeArray.count==0) {
        self.viewHeight = 0;
        return;
    }
    for (int i=0; i<_typeArray.count; i++) {
        @autoreleasepool {
            GoodsTypeDataParser *parser = _typeArray[i];
            NSUInteger row = i/3;
            NSUInteger col = i%3;
            UIView *bg_view = [[UIView alloc] initWithFrame:CGRectMake(NORMOL_SPACE+col*(width+NORMOL_SPACE/2),NORMOL_SPACE+row*(100+NORMOL_SPACE), width, 100)];
            bg_view.layer.borderWidth = 0.5;
            bg_view.layer.borderColor = LINE_COLOR.CGColor;
            bg_view.layer.masksToBounds = YES;
            [self addSubview:bg_view];
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, width, 100*0.6)];
            img.contentMode = UIViewContentModeScaleAspectFit;
            [img getImageWithUrlStr:parser.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            [bg_view addSubview:img];
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(img.frame), width, 100*0.3)];
            title.text = parser.category_name;
            title.textAlignment = NSTextAlignmentCenter;
            title.lineBreakMode = NSLineBreakByCharWrapping;
            title.font = [UIFont systemFontOfSize:11];
            [bg_view addSubview:title];
            bg_view.tag = i+1000;
            //添加手势
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
            bg_view.userInteractionEnabled = YES;
            [bg_view addGestureRecognizer:tap];
        }
    }
    CGFloat height = (_typeArray.count%3==0?_typeArray.count/3:_typeArray.count/3+1)*(100+NORMOL_SPACE);
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE,height+NORMOL_SPACE , mainWidth*0.75, 0.5)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self addSubview:line];
    self.viewHeight = CGRectGetMaxY(line.frame);
}

-(void)tapAction:(UITapGestureRecognizer *)tap {
    NSUInteger index = tap.view.tag-1000;
    GoodsTypeDataParser *parser = self.typeArray[index];
    if (self.delegate&&[self.delegate respondsToSelector:@selector(lookProductListWithCategoryId:)]) {
        [self.delegate lookProductListWithCategoryId:parser.category_id];
    }
}

@end
