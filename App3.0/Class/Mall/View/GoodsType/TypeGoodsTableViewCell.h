//
//  TypeGoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/11.
//  Copyright  2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsTypeModel.h"

@protocol TypeGoodsDelegate <NSObject>
@optional
- (void)chatSupplyWithId:(NSString *)supplyUserId;
@end
@interface TypeGoodsTableViewCell : UITableViewCell
@property (nonatomic, strong) BaseTypeGoodsDataParser *dataParser;
@property (nonatomic, weak) id<TypeGoodsDelegate> typeGoodsDelegate;

@end
