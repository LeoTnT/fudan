//
//  GoodsTypeTopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypeTopView.h"
@interface GoodsTypeTopTableViewCell : UITableViewCell
@property (nonatomic, strong) NSArray *typeArray;//子类数组
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, strong) TypeTopView *typeView;
@end
