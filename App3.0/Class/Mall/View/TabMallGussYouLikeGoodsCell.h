//
//  TabMallGussYouLikeGoodsCell.h
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallGussYouLikeGoodsView.h"
@interface TabMallGussYouLikeGoodsCell : UITableViewCell
@property(nonatomic,strong)TabMallGussYouLikeGoodsView *firstGoodsView;
@property(nonatomic,strong)TabMallGussYouLikeGoodsView *secondGoodsView;
@property(nonatomic,strong)NSArray *goodsItemArray;
@end
