//
//  GoodsEvaluationCell.h
//  App3.0
//
//  Created by syn on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
@interface GoodsEvaluationCell : UITableViewCell
@property(nonatomic,strong)ProductEvaluationItem *evaItem;
@property(nonatomic,strong)UIViewController *currentVC;
@property(nonatomic,assign)CGFloat height;
@end
