//
//  GoodsEvaluationCell.m
//  App3.0
//
//  Created by syn on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsEvaluationCell.h"
#import "XSFormatterDate.h"
#import "XLPhotoBrowser.h"
#import "ReuseImageView.h"
#import "GoodsDetailViewController.h"
#import "GoodsAgainEvaluationView.h"

@interface GoodsEvaluationCell()<XLPhotoBrowserDelegate,XLPhotoBrowserDatasource>
@property(nonatomic,strong)NSMutableArray *starArray;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *commentLabel,*line;
@property(nonatomic,strong)NSArray *imgUrlArray;
@property(nonatomic,strong)UIImageView *headImg;
@end

@implementation GoodsEvaluationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.line=[UILabel new];
        [self.contentView addSubview:self.line];
        self.line.backgroundColor=[UIColor hexFloatColor:@"DDDDDD"];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.height.mas_equalTo(0.5);
            make.top.mas_equalTo(self);
        }];
        self.headImg=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no_pic"]];
        [self.contentView addSubview:self.headImg];
        self.nameLabel=[[UILabel alloc] init];
        self.nameLabel.textColor=[UIColor hexFloatColor:@"4C6786"];
        self.nameLabel.font=[UIFont systemFontOfSize:13];
        [self.contentView addSubview:self.nameLabel];
        self.timeLabel=[[UILabel alloc] init];
        self.timeLabel.textColor=COLOR_999999;
        self.timeLabel.font=[UIFont systemFontOfSize:13];
        [self.contentView addSubview:self.timeLabel];
        self.commentLabel=[[UILabel alloc] init];
        self.commentLabel.font=[UIFont systemFontOfSize:15];
        self.commentLabel.numberOfLines=0;
        self.commentLabel.textColor=[UIColor hexFloatColor:@"333333"];
        [self.contentView addSubview:self.commentLabel];
        [self.headImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(14);
            make.width.height.mas_equalTo(35);
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.headImg.mas_right).mas_offset(13);
            make.top.mas_equalTo(12.5);
            make.width.mas_lessThanOrEqualTo(mainWidth-165);
        }];
        self.starArray=[NSMutableArray array];
        for (int i=0; i<5; i++) {
            UIImageView *star=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_collect_unstar"]];
            [self.starArray addObject:star];
            [self.contentView addSubview:star];
            [star mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.nameLabel.mas_left).mas_offset(14*i);
                make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(10);
                make.width.height.mas_equalTo(12);
            }];
        }
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(15);
        }];
        [self.commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(41);
            make.right.mas_equalTo(-15.5);
            make.height.mas_greaterThanOrEqualTo(10);
        }];
    }
    return self;
}
-(void)setEvaItem:(ProductEvaluationItem *)evaItem{
    _evaItem=evaItem;
    //清空颜色星星
    for (UIImageView *star in self.starArray) {
        star.image=[UIImage imageNamed:@"user_collect_unstar"];
    }
    //删除图片
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[ReuseImageView class]] || [view isKindOfClass:[GoodsAgainEvaluationView class]]) {
            [view removeFromSuperview];
        }
    }
    [self.headImg getImageWithUrlStr:evaItem.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    for (int i=0; i<[evaItem.ms_lev intValue]; i++) {
        UIImageView *img=[self.starArray objectAtIndex:i];
        img.image=[UIImage imageNamed:@"user_collect_star"];
    }
    if ([evaItem.is_private integerValue]==1) {
        self.nameLabel.text=@"匿名用户";
    }else{
        self.nameLabel.text=evaItem.nickname;
    }
    self.timeLabel.text=[XSFormatterDate dateAccurateToDayWithTimeIntervalString:[NSString stringWithFormat:@"%@",evaItem.w_time]];
    self.commentLabel.text=evaItem.comments;
    //添加图片
    UIView *tempView;
    if (evaItem.pic.length) {
        if ([evaItem.pic containsString:@","]) {
            self.imgUrlArray=[evaItem.pic componentsSeparatedByString:@","];
        }else{
            //一张图片
            self.imgUrlArray=[NSArray arrayWithObject:evaItem.pic];
        }
        CGFloat width=(mainWidth-75-5*4)/5.0;
        for (int i=0; i<self.imgUrlArray.count; i++) {
            int line=i/5;
            int column=i%5;
            ReuseImageView *image=[[ReuseImageView alloc] init];
            image.userInteractionEnabled=YES;
            [image getImageWithUrlStr:self.imgUrlArray[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            image.contentMode = UIViewContentModeScaleAspectFill;
            image.clipsToBounds=YES;//超出裁剪
            [self.contentView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.nameLabel.mas_left).mas_offset(column*(width+5));
                make.top.mas_equalTo(self.commentLabel.mas_bottom).mas_offset(21.5+line*(width+5));
                make.width.height.mas_equalTo(width);
            }];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
            [image addGestureRecognizer:tap];
            image.tag=i;
            tempView=image;
        }
    }
    [self.contentView layoutIfNeeded];
    if (evaItem.pic.length) {
        self.height=tempView.mj_y+tempView.mj_size.height+13.5;
    }else{
        self.height=self.commentLabel.mj_y+self.commentLabel.mj_size.height+13.5;
    }
    if ([evaItem.zhui_time integerValue]>0) {//有追评
        GoodsAgainEvaluationView *view=[[GoodsAgainEvaluationView alloc] initWithFrame:CGRectMake(0, self.height-13.5+5, mainWidth, 0)];
        [self.contentView addSubview:view];
        view.againDataDic=@{@"content":evaItem.zhui_content,@"time":evaItem.zhui_time,@"img":evaItem.zhui_img};
        view.currentVC=self.currentVC;
        view.frame=CGRectMake(0, self.height-13.5+5, mainWidth, view.height);
        self.height+=view.height;
    }
}
#pragma mark-放大图片
-(void)enLarge:(UITapGestureRecognizer *)tap{
    NSInteger imageTag = tap.view.tag;
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:self.imgUrlArray currentImageIndex:imageTag pageControlHidden:YES];
    [browser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    browser.isCusctomAdd = YES;
    browser.delegate = self;
    [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
    [self.currentVC.tabBarController.view addSubview:browser];
}
-(void)photoBrowserDismiss{
    if ([self.currentVC isKindOfClass:[GoodsDetailViewController class]]) {
          [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    }else{
        [self.currentVC.navigationController setNavigationBarHidden:NO animated:NO];
    }
    [self.currentVC.tabBarController.tabBar setHidden:YES];
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
