//
//  GoodsDetailTopInfoCell.m
//  App3.0
//
//  Created by mac on 2017/4/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailTopInfoCell.h"
#import "XSFormatterDate.h"

@interface GoodsDetailTopInfoCell()
{
    long countDown;
    BOOL isStarted;
}
@property(nonatomic,strong)UILabel *goodsNameLabel;
@property(nonatomic,strong)UILabel *sellPriceLabel;
@property(nonatomic,strong)UILabel *marketPriceLabel;
@property(nonatomic,strong)UILabel *couponLabel;        // 抵扣券
@property(nonatomic,strong)UILabel *interestNumLabel;
@property(nonatomic,strong)UILabel *locationLabel;
@property(nonatomic,strong)UILabel *sendPriceLabel;
@property(nonatomic,strong)UILabel *stockNumLabel;
@property (nonatomic,strong) UILabel *privilegeLabel;//特优活动

@property(nonatomic,strong)UILabel *sellTypeLabel;      // 价格类型
@property(nonatomic,strong)UILabel *timeLabel;          // 团购倒计时
@property(nonatomic,strong)UILabel *groupBuyLabel;      // 团购单数
@property (strong, nonatomic) NSTimer *timer;

@property (nonatomic, strong) NSMutableArray *wholesaleLabelArray;
@property(nonatomic,strong)UILabel *wholesaleLowestLabel;      // 最低起订价
@property(nonatomic,strong)UIView *shareView;
@end
@implementation GoodsDetailTopInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.goodsNameLabel = [UILabel new];
        self.goodsNameLabel.numberOfLines = 2;
        self.goodsNameLabel.font=[UIFont systemFontOfSize:16];
        [self.contentView addSubview:self.goodsNameLabel];
        [self.goodsNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(10);
            make.right.mas_lessThanOrEqualTo(self).offset(-71);
            make.top.mas_equalTo(self).offset(10);
        }];
        
        self.shareView=[[UIView alloc] init];
        [self.contentView addSubview:self.shareView];
        [self.shareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(10);
            make.right.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(59, 39));
        }];
        
        UIView *cuttingLine=[[UIView alloc] init];
        cuttingLine.backgroundColor=[UIColor hexFloatColor:@"E3E3E3"];
        [self.shareView addSubview:cuttingLine];
        [cuttingLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(self.shareView);
            make.width.mas_equalTo(1);
        }];
        
        UIImageView *shareImg=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_collect_share"]];
        shareImg.userInteractionEnabled=YES;
        [self.shareView addSubview:shareImg];
        [shareImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.shareView);
            make.top.mas_equalTo(self.shareView);
        }];
        
        UILabel *shareLabel=[[UILabel alloc] init];
        shareLabel.text=Localized(@"share");
        shareLabel.textAlignment=NSTextAlignmentCenter;
        shareLabel.font=[UIFont systemFontOfSize:12];
        shareLabel.userInteractionEnabled=YES;
        shareLabel.textColor=COLOR_999999;
        [self.shareView addSubview:shareLabel];
        [shareLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.shareView);
            make.top.mas_equalTo(shareImg.mas_bottom).offset(6);
        }];
        [self.shareView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(share)]) {
                [self.delegate share];
            }
        }]];
        
        self.sellPriceLabel=[[UILabel alloc] init];
        self.sellPriceLabel.font=[UIFont systemFontOfSize:12];
        self.sellPriceLabel.textColor =mainColor;
        [self.contentView addSubview:self.sellPriceLabel];
        
        self.marketPriceLabel=[[UILabel alloc] init];
        self.marketPriceLabel.textColor=[UIColor hexFloatColor:@"A1A1A1"];
        self.marketPriceLabel.font=[UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.marketPriceLabel];
        
        self.couponLabel = [[UILabel alloc] init];
        self.couponLabel.font = [UIFont systemFontOfSize:12];
        self.couponLabel.textColor = mainColor;
        [self.contentView addSubview:self.couponLabel];
        
        self.interestNumLabel=[[UILabel alloc] init];
        self.interestNumLabel.textColor=[UIColor hexFloatColor:@"A1A1A1"];
        self.interestNumLabel.font=[UIFont systemFontOfSize:12];
        self.interestNumLabel.textAlignment=NSTextAlignmentRight;
        [self.contentView addSubview:self.interestNumLabel];
        
        self.wholesaleLowestLabel=[[UILabel alloc] init];
        self.wholesaleLowestLabel.textColor=[UIColor hexFloatColor:@"EA3323"];
        self.wholesaleLowestLabel.font=[UIFont systemFontOfSize:12];
        self.wholesaleLowestLabel.textAlignment=NSTextAlignmentRight;
        [self.contentView addSubview:self.wholesaleLowestLabel];
        
        self.locationLabel=[[UILabel alloc] init];
        self.locationLabel.font=[UIFont systemFontOfSize:13];
        self.locationLabel.adjustsFontSizeToFitWidth = YES;
        self.locationLabel.textColor=[UIColor hexFloatColor:@"A1A1A1"];
        [self.contentView addSubview:self.locationLabel];
        
        self.sendPriceLabel=[[UILabel alloc] init];
        self.sendPriceLabel.font=[UIFont systemFontOfSize:12];
        self.sendPriceLabel.textAlignment=NSTextAlignmentCenter;
        self.sendPriceLabel.textColor=[UIColor hexFloatColor:@"EA3323"];
        [self.contentView addSubview:self.sendPriceLabel];
        
        self.stockNumLabel=[[UILabel alloc] init];
        self.stockNumLabel.font=[UIFont systemFontOfSize:13];
        self.stockNumLabel.textColor=[UIColor hexFloatColor:@"A1A1A1"];
        self.stockNumLabel.textAlignment=NSTextAlignmentRight;
        [self.contentView addSubview:self.stockNumLabel];
        
        self.privilegeLabel = [[UILabel alloc] init];
        self.privilegeLabel.font = [UIFont systemFontOfSize:12];
        self.privilegeLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.privilegeLabel];
        
        self.sellTypeLabel = [[UILabel alloc] init];
        self.sellTypeLabel.font = [UIFont systemFontOfSize:12];
        self.sellTypeLabel.textColor = [UIColor hexFloatColor:@"A1A1A1"];
        [self.contentView addSubview:self.sellTypeLabel];
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.textColor = mainGrayColor;
        [self.contentView addSubview:self.timeLabel];
        
        self.groupBuyLabel = [[UILabel alloc] init];
        self.groupBuyLabel.font = [UIFont systemFontOfSize:12];
        self.groupBuyLabel.textColor = mainGrayColor;
        [self.contentView addSubview:self.groupBuyLabel];
        
        [self.sellTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.goodsNameLabel);
            make.top.mas_equalTo(self.self).offset(50);
        }];
        [self.sellPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.goodsNameLabel);
            make.top.mas_equalTo(self).offset(72);
        }];
        [self.marketPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sellPriceLabel.mas_right).mas_offset(5);
            make.centerY.mas_equalTo(self.sellPriceLabel);
        }];
        [self.couponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sellPriceLabel);
            make.top.mas_equalTo(self.sellPriceLabel.mas_bottom).mas_offset(6);
        }];
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-12);
            make.centerY.mas_equalTo(self.sellPriceLabel);
        }];
        [self.groupBuyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.timeLabel);
            make.top.mas_equalTo(self.timeLabel.mas_bottom).offset(10);
        }];
        [self.stockNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-10);
            make.top.mas_equalTo(self.sellPriceLabel.mas_bottom).offset(30);
          
        }];
        [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.goodsNameLabel);
            make.right.mas_equalTo(self.stockNumLabel.mas_left).mas_offset(-10);
           make.centerY.mas_equalTo(self.stockNumLabel);
        }];
        [self.interestNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-10);
            make.bottom.mas_equalTo(self.sellPriceLabel.mas_bottom);
        }];
        [self.wholesaleLowestLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-10);
            make.centerY.mas_equalTo(self.sellPriceLabel);
        }];
        [self.sendPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.stockNumLabel.mas_left).offset(-10);
            make.bottom.mas_equalTo(self.stockNumLabel);
        }];
        [self.privilegeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.goodsNameLabel);
            make.top.mas_equalTo(self.sellPriceLabel.mas_bottom).offset(6);
        }];
        
        // 批发价格
        self.wholesaleLabelArray = [NSMutableArray array];
        for (int i = 0; i < 3; i++) {
            UILabel *number = [UILabel new];
            number.font = [UIFont systemFontOfSize:12];
            number.textColor = COLOR_666666;
            [self.contentView addSubview:number];
            number.hidden = YES;
            [self.wholesaleLabelArray addObject:number];
            
            UILabel *price = [UILabel new];
            price.font = [UIFont systemFontOfSize:16];
            price.textColor = mainColor;
            [self.contentView addSubview:price];
            price.hidden = YES;
            [self.wholesaleLabelArray addObject:price];
            
            [number mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.sellPriceLabel).offset(i*100);
                make.top.mas_equalTo(self.sellPriceLabel);
            }];
            
            [price mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(number);
                make.top.mas_equalTo(number.mas_bottom).offset(6);
            }];
        }
    }
    return self;
}
-(void)setGoodsDetailInfo:(GoodsDetailInfo *)goodsDetailInfo{
    _goodsDetailInfo=goodsDetailInfo;
    ProductInfo *goodsInfo=goodsDetailInfo.productInfo;
    
    self.goodsNameLabel.text = goodsInfo.product_name.length?goodsInfo.product_name:@"";
    //价格
    NSInteger length=[NSString stringWithFormat:@"¥%i",[goodsInfo.sell_price intValue]].length;
    NSMutableAttributedString *attStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",goodsInfo.sell_price.length?goodsInfo.sell_price:@"0.00"]];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:21] range:NSMakeRange(0, length)];
    self.sellPriceLabel.attributedText=attStr;
    
    //市场价
    NSString *textStr = [NSString stringWithFormat:@"¥%@",goodsInfo.market_price.length?goodsInfo.market_price:@"0.00"];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:textStr attributes:attribtDic];
    // 赋值
    self.marketPriceLabel.attributedText = attribtStr;
    
    if ([goodsInfo.sell_type integerValue] == 2) {
        // 积分商品
        self.sellPriceLabel.text = [NSString stringWithFormat:@"积分价：%@",goodsInfo.score];
    }
    
    for (Showfield *sf in goodsDetailInfo.showfield) {
        if ([sf.name isEqualToString:@"coupon"]) {
            
            self.couponLabel.text = [NSString stringWithFormat:@"%@：%@",sf.title,goodsInfo.coupon];
        }
    }
    
    //感兴趣
    self.interestNumLabel.text=[NSString stringWithFormat:@"%@人感兴趣",goodsInfo.look_num.length?goodsInfo.look_num:@""];
    
    // 位置
    self.locationLabel.text=goodsDetailInfo.sendAddress;
    
    // 运费
    //    self.sendPriceLabel.text=[NSString stringWithFormat:@"运费:%@",goodsInfo.delivery_price];
    
    // 库存
    self.stockNumLabel.text=[NSString stringWithFormat:@"库存%@件",goodsInfo.stock.length?goodsInfo.stock:@"0"];
    
    [self.timer invalidate];
    self.timer = nil;
    self.timeLabel.hidden = YES;
    
    
    [self.contentView layoutIfNeeded];
    self.height=self.stockNumLabel.mj_y+self.stockNumLabel.mj_size.height+14;
}

- (void)setGroupBuyDetailInfo:(GroupBuyDetailModel *)groupBuyDetailInfo {
    _groupBuyDetailInfo = groupBuyDetailInfo;
    if (self.goodSellType == GoodSellGroupBuy || self.goodSellType == GoodSellSecondKill) {
        // 团购
        self.privilegeLabel.hidden = YES;
        self.interestNumLabel.hidden = YES;
        self.sendPriceLabel.hidden = YES;
        self.goodsNameLabel.text = groupBuyDetailInfo.title;
        self.sellTypeLabel.text = self.goodSellType == GoodSellGroupBuy?@"参团价：":@"秒杀价：";
        self.sellPriceLabel.text = [NSString stringWithFormat:@"¥%@",groupBuyDetailInfo.sell_price];
        self.groupBuyLabel.text = self.goodSellType == GoodSellGroupBuy?[NSString stringWithFormat:@"剩余%@单 | 已团%d单",groupBuyDetailInfo.remain,[groupBuyDetailInfo.num intValue]-[groupBuyDetailInfo.remain intValue]]:[NSString stringWithFormat:@"剩余%@",groupBuyDetailInfo.remain];
        
        //市场价
        NSString *textStr = [NSString stringWithFormat:@"市场价：¥%@",groupBuyDetailInfo.market_price];
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:textStr attributes:attribtDic];
        // 赋值
        self.marketPriceLabel.attributedText = attribtStr;
        
        self.timeLabel.hidden = NO;
        NSDate *currentData = [NSDate date];
        countDown = [groupBuyDetailInfo.begin_time longLongValue] - [currentData timeIntervalSince1970];
        if (countDown > 0) {
            // 未开始
            isStarted = NO;
            NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
            self.timeLabel.text = [NSString stringWithFormat:@"距活动开始：%@",timeRemin];
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        } else {
            // 已开始
            isStarted = 1;
            countDown = [groupBuyDetailInfo.end_time longLongValue] - [currentData timeIntervalSince1970];
            if (countDown > 0) {
                NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
                self.timeLabel.text = [NSString stringWithFormat:@"倒计时：%@",timeRemin];
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
            } else {
                isStarted = 0;
                self.timeLabel.text = @"活动已经结束";
            }
            
        }
    }
}

- (void)countDown {
    countDown--;
    if (isStarted) {
        NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
        self.timeLabel.text = [NSString stringWithFormat:@"倒计时：%@",timeRemin];
    } else {
        NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
        self.timeLabel.text = [NSString stringWithFormat:@"距活动开始：%@",timeRemin];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
