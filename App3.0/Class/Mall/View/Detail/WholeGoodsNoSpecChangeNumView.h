//
//  WholeGoodsNoSpecChangeNumView.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WholeGoodsNoSpecChangeNumViewDelegate <NSObject>
-(void)changeGoodsNum:(int)num totalMoney:(CGFloat)totalMoney;
@end

@interface WholeGoodsNoSpecChangeNumView : UIView
@property(nonatomic,assign)CGFloat price;//单价和库存
@property(nonatomic,assign)int stockNum;
@property(nonatomic,strong)UITextField *goodsNumField;
@property(nonatomic,weak)id<WholeGoodsNoSpecChangeNumViewDelegate>delegate;
@end
