//
//  WholesaleCell.h
//  App3.0
//
//  Created by mac on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@interface WholesaleItem :UICollectionViewCell
@property (nonatomic, copy) NSString *itemTitle;
@end;

@interface WholesaleView :QuickCollectionView
@property (nonatomic, strong) NSArray *wholesaleDataSource;
@end;

@interface WholesaleCell : UITableViewCell
@property (nonatomic, strong) WholesaleView *wholesaleView;
@property (nonatomic, strong) GoodsDetailInfo *goodDetailInfo;
+ (instancetype)createWholesaleCellWithTableVie:(UITableView *)tableView;
+ (CGFloat)cellHeight:(GoodsDetailInfo *)info;
@end
