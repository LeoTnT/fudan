//
//  GoodsDetailEvaluationCell.h
//  App3.0
//
//  Created by 孙亚男 on 2017/12/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
@interface GoodsDetailSumEvaluationCell : UITableViewCell
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@end
