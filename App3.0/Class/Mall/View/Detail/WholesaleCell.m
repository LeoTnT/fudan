//
//  WholesaleCell.m
//  App3.0
//
//  Created by mac on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WholesaleCell.h"

@implementation WholesaleCell
{
    CGFloat chartHeight;
    UILabel *title;
}
+ (instancetype)createWholesaleCellWithTableVie:(UITableView *)tableView {
    static NSString *identifier = @"wholesaleCell";
    WholesaleCell *cell = (WholesaleCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[WholesaleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        chartHeight = 30;
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    title = [UILabel new];
    title.text = @"价格方案";
    title.font = [UIFont systemFontOfSize:12];
    title.textColor = COLOR_666666;
    [self.contentView addSubview:title];
    title.hidden = YES;
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        make.top.mas_equalTo(self).offset(10);
    }];
    
    self.wholesaleView = [[WholesaleView alloc] init];
    [self.contentView addSubview:self.wholesaleView];
}

- (void)setGoodDetailInfo:(GoodsDetailInfo *)goodDetailInfo {
    if (!goodDetailInfo || _goodDetailInfo == goodDetailInfo) {
        return;
    }
    
    _goodDetailInfo = goodDetailInfo;
    WholesaleConfigAppModel *wholsesaleApp = goodDetailInfo.productInfo.wholesale_config_app;
    if (!wholsesaleApp.price) {
        title.hidden = YES;
        return;
    }
    title.hidden = NO;
    
    // 钱包中英文对应
    NSMutableDictionary *sfDic = [NSMutableDictionary dictionary];
    for (Showfield *sf in goodDetailInfo.showfield) {
        [sfDic setObject:sf.title forKey:sf.name];
    }
    
    NSMutableArray *dataArr = [NSMutableArray array];
    if (goodDetailInfo.productInfo.wholesale_price_way == 1) {
        // 按数量计价
        NSUInteger row = wholsesaleApp.price.count;
        self.wholesaleView.frame = CGRectMake(10, 30, mainWidth-20, chartHeight*(row+1));
        
        NSMutableArray *tempArr1 = [NSMutableArray array];
        [tempArr1 addObject:@"起批量"];
        for (NSDictionary *dic in wholsesaleApp.price) {
            WholesaleNumberModel *model = [WholesaleNumberModel mj_objectWithKeyValues:dic];
            NSString *str;
            if ([model.max_number integerValue] == 0) {
                str = [NSString stringWithFormat:@"%@件以上",model.min_number];
            } else {
                str = [NSString stringWithFormat:@"%@-%@件",model.min_number,model.max_number];
            }
            [tempArr1 addObject:str];
        }
        [dataArr addObject:tempArr1];
        
        
        WholesaleNumberModel *model = [WholesaleNumberModel mj_objectWithKeyValues:wholsesaleApp.price[0]];
        for (NSDictionary *dic in model.fields) {
            NSString *key = [dic objectForKey:@"name"];
            if (![sfDic objectForKey:key]) {
                continue;
            }
            NSMutableArray *tempArr2 = [NSMutableArray array];
            [tempArr2 addObject:[sfDic objectForKey:key]];
            for (NSDictionary *dic in wholsesaleApp.price) {
                WholesaleNumberModel *model = [WholesaleNumberModel mj_objectWithKeyValues:dic];
                for (NSDictionary *dic1 in model.fields) {
                    if ([[dic1 objectForKey:@"name"] isEqualToString:key]) {
                        NSString *value = [NSString stringWithFormat:@"%@",[dic1 objectForKey:@"value"]];
                        [tempArr2 addObject:value];
                    }
                    
                }
            }
            [dataArr addObject:tempArr2];
        }

        self.wholesaleView.wholesaleDataSource = dataArr;
        
    } else if (goodDetailInfo.productInfo.wholesale_price_way == 2) {
        // 按规格计价
        self.wholesaleView.frame = CGRectMake(10, 30, mainWidth-20, chartHeight*2);
        
        NSMutableArray *tempArr1 = [NSMutableArray array];
        [tempArr1 addObject:@"起批量"];
        [tempArr1 addObject:[NSString stringWithFormat:@"%@套以上",wholsesaleApp.moq]];
        [dataArr addObject:tempArr1];
        
        NSMutableArray *tempArr2 = [NSMutableArray array];
        for (NSDictionary *dic in wholsesaleApp.price) {
            WholesaleSpecModel *model = [WholesaleSpecModel mj_objectWithKeyValues:dic];
            [tempArr2 addObject:[sfDic objectForKey:model.name]];
            
            NSString *str = [NSString stringWithFormat:@"%@-%@",model.min_price,model.max_price];
            [tempArr2 addObject:str];
        }
        
        [dataArr addObject:tempArr2];
        self.wholesaleView.wholesaleDataSource = dataArr;
    }
}

+ (CGFloat)cellHeight:(GoodsDetailInfo *)info {
    if (info.productInfo.wholesale_price_way == 1) {
        NSUInteger row = info.productInfo.wholesale_config_app.price.count;
        return (row+1)*30+40;
    } else if (info.productInfo.wholesale_price_way == 2) {
        return 2*30+40;
    }
    return 0.1;
}
@end

@implementation WholesaleView

- (void)setContentView {
    [super setContentView];
}

- (void)setWholesaleDataSource:(NSArray *)wholesaleDataSource {
    if (!wholesaleDataSource || _wholesaleDataSource == wholesaleDataSource) {
        return;
    }
    _wholesaleDataSource = wholesaleDataSource;
    NSInteger count = self.wholesaleDataSource.count;
    self.cellSize = CGSizeMake((mainWidth-20)/count-0.5, 29);
    self.flowLayout.minimumLineSpacing = 0.5;
    self.flowLayout.minimumInteritemSpacing = 0.5;
    [self.collectionView registerClass:[WholesaleItem class] forCellWithReuseIdentifier:@"cell"];
    self.collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.collectionView reloadData];
    
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    WholesaleItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSArray *arr = self.wholesaleDataSource[0];
    NSInteger row = indexPath.row/arr.count;
    NSInteger colum = indexPath.row%arr.count;
    cell.itemTitle = self.wholesaleDataSource[row][colum];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSArray *arr = self.wholesaleDataSource[0];
    return self.wholesaleDataSource.count*arr.count;
}

//定义每个section的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0.5, 0.5, 0.5, 0.5);
}

//设置每个 UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count = self.wholesaleDataSource.count;
    return CGSizeMake((mainWidth-20)/count-1, 29);
}
@end

@implementation WholesaleItem
{
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)setContentView {
    titleLabel = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:12] titleColor:[UIColor blackColor]];
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
}

- (void)setItemTitle:(NSString *)itemTitle {
    titleLabel.text = itemTitle;
}
@end
