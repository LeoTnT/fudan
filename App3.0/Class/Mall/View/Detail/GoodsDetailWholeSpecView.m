//
//  GoodsDetailWholeSpecView.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailWholeSpecView.h"
#import "WholeCategoryScrollview.h"
#import "UUImageAvatarBrowser.h"
#import "GoodsDetailChangeNumScrollView.h"
@implementation SpecAndNumCommonPriceModel

@end

@interface GoodsDetailWholeSpecView()<UIScrollViewDelegate,WholeCategoryScrollviewDelegate,GoodsDetailChangeNumScrollViewDelegate,WholeGoodsNoSpecChangeNumViewDelegate>
@property(nonatomic,strong)UIImageView *goodsImg;
@property(nonatomic,strong)UILabel*leftLabel,*middleLabel,*rightLabel,*allHaveChoosedNumLabel,*allTotalLabel;
@property(nonatomic,strong)UIButton *buyNowBtn;
@property(nonatomic,copy)NSString *currentStockNum;
@property(nonatomic,strong)NSString *currentPrice;
@property(nonatomic,strong)WholeCategoryScrollview *leftScroll,*middleScroll;
@property(nonatomic,strong)GoodsDetailChangeNumScrollView *rightScroll;
@property(nonatomic,assign)int haveChoosedNum;
@property(nonatomic,strong)UIScrollView *leftTopPriceScrollView;
@property(nonatomic,strong)NSMutableArray *leftPriceLabelArray,*specPriceModelArray,*numPriceModelArray;
@end

@implementation GoodsDetailWholeSpecView
 CGFloat space=10;
-(instancetype)init{
    if (self=[super init]) {
        self.backgroundColor=[UIColor whiteColor];
        //商品图片
        self.goodsImg=[[UIImageView alloc] initWithFrame:CGRectMake(space, -mainWidth*0.2*0.3, mainWidth*0.21, mainWidth*0.2)];
        self.goodsImg.contentMode = UIViewContentModeScaleAspectFit;
        self.goodsImg.layer.borderWidth=0.5;
        self.goodsImg.backgroundColor=[UIColor whiteColor];
        self.goodsImg.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.goodsImg.image=[UIImage imageNamed:@"no_pic"];
        self.goodsImg.userInteractionEnabled=YES;
        [self.goodsImg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enlargePicture)]];
        [self addSubview:self.goodsImg];
        //价格滚动式图
        self.leftTopPriceScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImg.frame)+5, 12, mainWidth-CGRectGetMaxX(self.goodsImg.frame)-5, mainWidth*0.2*0.7-12)];
        [self addSubview:self.leftTopPriceScrollView];
        //关闭  透明按钮好关闭
        UIImageView *closeImage=[[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-space-20, space, 20, 20)];
        closeImage.image=[UIImage imageNamed:@"mall_detail_spec_close"];
        [self addSubview:closeImage];
        UIButton *clearCloseBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-40, 0, 40, 40)];
        [clearCloseBtn setBackgroundColor:[UIColor clearColor]];
        [self addSubview:clearCloseBtn];
        [clearCloseBtn addTarget:self action:@selector(closeSpecView) forControlEvents:UIControlEventTouchUpInside];
        for (int i=0; i<3; i++) {
            UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(mainWidth/3.0*i, CGRectGetMaxY(self.goodsImg.frame)+space, mainWidth/3.0, 40)];
            label.textAlignment=NSTextAlignmentCenter;
            label.font=[UIFont systemFontOfSize:15];
            [self addSubview:label];
            if (i==0) {
                self.leftLabel=label;
            }else if (i==1){
                self.middleLabel=label;
            }else{
                self.rightLabel=label;
            }
        }
        self.leftScroll=[[WholeCategoryScrollview alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.rightLabel.frame),0,200)];
        [self addSubview:self.leftScroll];
        self.leftScroll.isLeft=YES;
        self.leftScroll.showsVerticalScrollIndicator=NO;
        self.middleScroll=[[WholeCategoryScrollview alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.rightLabel.frame),0,200)];
        [self addSubview:self.middleScroll];
        self.middleScroll.isLeft=NO;
        self.middleScroll.delegate=self;
        self.middleScroll.showsVerticalScrollIndicator=NO;
        self.rightScroll=[[GoodsDetailChangeNumScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.rightLabel.frame),0,200)];
        [self addSubview:self.rightScroll];
        self.rightScroll.showsVerticalScrollIndicator=NO;
        self.rightScroll.delegate=self;
        //无规格直接选择数量
        self.noSpecchangeNumView=[[WholeGoodsNoSpecChangeNumView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.goodsImg.frame)+space, mainWidth, 50)];
        self.noSpecchangeNumView.delegate=self;
        [self addSubview:self.noSpecchangeNumView];
        self.noSpecchangeNumView.hidden=YES;
        //已选择  总计
        self.allTotalLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.rightScroll.frame), mainWidth-20, 25)];
        self.allTotalLabel.textAlignment=NSTextAlignmentRight;
        self.allTotalLabel.font=[UIFont systemFontOfSize:13];
        self.allTotalLabel.textColor=mainGrayColor;
        [self addSubview:self.allTotalLabel];
        self.buyNowBtn=[[UIButton alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.allTotalLabel.frame), mainWidth, 40)];
        [ self.buyNowBtn setTitle:@"立即购买" forState:UIControlStateNormal];
        [self.buyNowBtn setBackgroundColor:mainColor];
        [self.buyNowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview: self.buyNowBtn];
        [self.buyNowBtn addTarget:self action:@selector(buyNow) forControlEvents:UIControlEventTouchUpInside];
        self.height=CGRectGetMaxY(self.buyNowBtn.frame);
        }
    return self;
}
-(void)setDetailInfo:(GoodsDetailInfo *)detailInfo{
    _detailInfo=detailInfo;
    for (UIView *view in self.subviews) {
        if (view.tag==1000) {
            [view removeFromSuperview];
        }
    }
    self.haveChoosedNum=0;
    self.currentStockNum=self.detailInfo.productInfo.stock;
    self.currentPrice=self.detailInfo.productInfo.sell_price;
    self.specPriceModelArray=[NSMutableArray array];
    self.numPriceModelArray=[NSMutableArray array];
    self.leftPriceLabelArray=[NSMutableArray array];
    self.specDicArray=[NSMutableArray array];
    [self addPriceLabelArrayAndSetModelArray];
    [self.goodsImg getImageWithUrlStr:detailInfo.productInfo.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    if (detailInfo.productInfo.spec.count) {//有规格
        self.noSpecchangeNumView.hidden=YES;
        self.leftLabel.hidden=NO;
        self.leftScroll.hidden=NO;
        self.middleLabel.hidden=NO;
        self.middleScroll.hidden=NO;
        self.rightLabel.hidden=NO;
        self.rightScroll.hidden=NO;
        if (detailInfo.productInfo.spec.count==1) {
            self.middleLabel.text=((SpecItem *)detailInfo.productInfo.spec.firstObject).spec_name;
            self.rightLabel.text=@"购买数量";
            self.middleLabel.frame=CGRectMake(0, CGRectGetMaxY(self.goodsImg.frame)+space, mainWidth/2.0, 40);
            self.leftLabel.frame=CGRectZero;
            self.rightLabel.frame=CGRectMake(mainWidth/2.0, CGRectGetMaxY(self.goodsImg.frame)+space,mainWidth/2.0, 40);
            self.middleScroll.frame=CGRectMake(0, CGRectGetMaxY(self.middleLabel.frame),mainWidth/2.0,200);
            self.middleScroll.item=detailInfo.productInfo.spec.firstObject;
            self.leftScroll.frame=CGRectZero;
            self.rightScroll.frame=CGRectMake(mainWidth/2.0, CGRectGetMaxY(self.rightLabel.frame),mainWidth/2.0, 200);
            self.rightScroll.detailInfo=self.detailInfo;
            self.rightScroll.productMap=[detailInfo.productInfo.spec_info mj_JSONObject][@"product_map"];
            self.rightScroll.item=detailInfo.productInfo.spec.firstObject;
        }else{
            self.leftLabel.text=((SpecItem *)detailInfo.productInfo.spec.firstObject).spec_name;
            self.middleLabel.text=((SpecItem *)detailInfo.productInfo.spec.lastObject).spec_name;
            self.rightLabel.text=@"购买数量";
            self.leftLabel.frame=CGRectMake(0, CGRectGetMaxY(self.goodsImg.frame)+space, mainWidth/3.0, 40);
            self.middleLabel.frame=CGRectMake(mainWidth/3.0, CGRectGetMaxY(self.goodsImg.frame)+space, mainWidth/3.0, 40);
            self.rightLabel.frame=CGRectMake(mainWidth/3.0*2, CGRectGetMaxY(self.goodsImg.frame)+space,mainWidth/3.0, 40);
            self.leftScroll.frame=CGRectMake(0, CGRectGetMaxY(self.leftLabel.frame),mainWidth/3.0,200);
            self.leftScroll.selectDelegate=self;
            self.middleScroll.frame=CGRectMake(mainWidth/3.0, CGRectGetMaxY(self.leftLabel.frame),mainWidth/3.0,200);
            self.rightScroll.frame=CGRectMake(mainWidth/3.0*2, CGRectGetMaxY(self.rightLabel.frame),mainWidth/3.0, 200);
            self.leftScroll.item=detailInfo.productInfo.spec.firstObject;
            self.middleScroll.item=detailInfo.productInfo.spec[1];
            //顺序一致
            self.rightScroll.detailInfo=self.detailInfo;
            self.rightScroll.productMap=[detailInfo.productInfo.spec_info mj_JSONObject][@"product_map"];
            Spec_valueItem *valueItem=((SpecItem *)detailInfo.productInfo.spec.firstObject).spec_value.firstObject;
            self.rightScroll.selectValueItem=valueItem;
            self.rightScroll.item=detailInfo.productInfo.spec[1];
        }
        self.rightScroll.changeNumDelegate=self;
        self.rightScroll.minStartWhole=[detailInfo.productInfo.wholesale_config_app.moq intValue];
    }else{
        self.noSpecchangeNumView.hidden=NO;
        self.noSpecchangeNumView.stockNum=[detailInfo.productInfo.stock intValue];
        self.noSpecchangeNumView.price=[detailInfo.productInfo.sell_price floatValue];
        self.leftLabel.hidden=YES;
        self.leftScroll.hidden=YES;
        self.middleLabel.hidden=YES;
        self.middleScroll.hidden=YES;
        self.rightLabel.hidden=YES;
        self.rightScroll.hidden=YES;
    }
}
-(void)enlargePicture{
    [UUImageAvatarBrowser showImage:self.goodsImg title:@"测试名称"];
}
-(void)closeSpecView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickCloseBtnWithSpecDic:num:)]) {
        [self.delegate clickCloseBtnWithSpecDic:self.specDicArray num:self.haveChoosedNum];
    }
}
-(void)buyNow{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickBuyBtnWithSpecDic:num:)]) {
        [self.delegate clickBuyBtnWithSpecDic:self.specDicArray num:self.haveChoosedNum];
    }
}
-(void)addPriceLabelArrayAndSetModelArray{
    //清界面
    for (UIView *view in self.leftTopPriceScrollView.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
    self.leftTopPriceScrollView.contentSize=CGSizeMake(CGRectGetWidth(self.leftTopPriceScrollView.frame), self.detailInfo.showfield.count*((mainWidth*0.2*0.7-12)/3.0));
    NSMutableAttributedString *tempBottomStr=[[NSMutableAttributedString alloc] initWithString:@"已选择0件 "];
    [tempBottomStr addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(3, 1)];
    for (int i=0; i<self.detailInfo.showfield.count; i++) {
        Showfield *field=[self.detailInfo.showfield objectAtIndex:i];
        if (self.detailInfo.productInfo.wholesale_price_way==1) {//数量计价   无规格也是
            for (int j=0;j<self.detailInfo.productInfo.wholesale_config_app.price.count;j++) {
                NSDictionary *tempDic=self.detailInfo.productInfo.wholesale_config_app.price[j];
                for (NSDictionary *tempDic2 in tempDic[@"fields"]) {
                    if ([field.name isEqualToString:tempDic2[@"name"]]) {
                        SpecAndNumCommonPriceModel *model=[SpecAndNumCommonPriceModel new];
                        model.min_number=tempDic[@"min_number"];
                        model.max_number=tempDic[@"max_number"];
                        model.name=field.name;
                        model.title=field.title;
                        model.value=tempDic2[@"value"];
                        [self.numPriceModelArray addObject:model];//这里面的数据是重复的  包括价格还有钱包类型
                    }
                }
            }
            UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, ((mainWidth*0.2*0.7-12)/3.0)*i, CGRectGetWidth(self.leftTopPriceScrollView.frame), (mainWidth*0.2*0.7-12)/3.0)];
            [self.leftPriceLabelArray addObject:label];
            [self.leftTopPriceScrollView addSubview:label];
            label.font=[UIFont systemFontOfSize:14];
            label.textColor=mainColor;
            NSMutableAttributedString *tempStr;
            if ([field.name isEqualToString:@"sell_price"]) {
                tempStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:%@",field.title,@"¥0.0"]];
            }else{
                tempStr=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@:%@",field.title,@"0.0"]];
            }
            [tempStr addAttribute:NSForegroundColorAttributeName value:COLOR_999999 range:NSMakeRange(0, field.title.length+1)];
            label.attributedText=tempStr;
        }else{//规格计价
            for (int j=0;j<self.detailInfo.productInfo.wholesale_config_app.price.count;j++) {
                NSDictionary *tempDic=self.detailInfo.productInfo.wholesale_config_app.price[j];
                if([field.name isEqualToString:tempDic[@"name"]]){
                    SpecAndNumCommonPriceModel *model=[SpecAndNumCommonPriceModel new];
                    model.name=field.name;
                    model.title=field.title;
                    model.min_price=tempDic[@"min_price"];
                    model.max_price=tempDic[@"max_price"];
                    [self.numPriceModelArray addObject:model];//这里面的数据是重复的  包括价格还有钱包类型
                    
                    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, ((mainWidth*0.2*0.7-12)/3.0)*i, CGRectGetWidth(self.leftTopPriceScrollView.frame),(mainWidth*0.2*0.7-12)/3.0)];
                    [self.leftPriceLabelArray addObject:label];
                    [self.leftTopPriceScrollView addSubview:label];
                    label.font=[UIFont systemFontOfSize:14];
                    label.textColor=mainColor;
                    NSMutableAttributedString *tempStr;
                    if ([field.name isEqualToString:@"sell_price"]) {
                        tempStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:%@%.1f-%.1f",field.title,@"¥",[model.min_price floatValue],[model.max_price floatValue]]];
                    }else{
                        tempStr=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@:%.1f-%.1f",field.title,[model.min_price floatValue],[model.max_price floatValue]]];
                    }
                    [tempStr addAttribute:NSForegroundColorAttributeName value:COLOR_999999 range:NSMakeRange(0, field.title.length+1)];
                    label.attributedText=tempStr;
                }
            }
        }
        [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: ",field.title] attributes:@{NSForegroundColorAttributeName:mainGrayColor}]];
        if ([field.name isEqualToString:@"sell_price"]) {
            [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥0.0 "] attributes:@{NSForegroundColorAttributeName:mainColor}]];
        }else{
          [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"0.0 "] attributes:@{NSForegroundColorAttributeName:mainColor}]];
        }
        self.allTotalLabel.attributedText=tempBottomStr;
    }
}
#pragma mark-ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.middleScroll)
    {
        self.rightScroll.delegate = nil;
        [self.rightScroll setContentOffset:self.middleScroll.contentOffset];
        self.rightScroll.delegate = self;
    }else {
        self.middleScroll.delegate = nil;
        [self.middleScroll setContentOffset:self.rightScroll.contentOffset];
        self.middleScroll.delegate = self;
    }
}
#pragma mark-WholeScrollView Delegate
-(void)didSelectSpec:(SpecItem *)specItem with:(Spec_valueItem *)valueItem{
    if (self.detailInfo.productInfo.spec.count==2) {
        self.rightScroll.selectValueItem=valueItem;
    }
}
-(void)numChangedWithSpecArray:(NSArray *)specArray andTotalNum:(int)num andTotalMoney:(CGFloat)money{
    self.haveChoosedNum=num;
    self.specDicArray=[NSMutableArray arrayWithArray:specArray];
    [self changePriceAndNumShowWithTotalNum:num andTotalMoney:money];
}
#pragma mark-NoSpecView Delegate
-(void)changeGoodsNum:(int)num totalMoney:(CGFloat)totalMoney{
    self.haveChoosedNum=num;
    [self changePriceAndNumShowWithTotalNum:num andTotalMoney:totalMoney];
   }
#pragma mark-Private
-(void)changePriceAndNumShowWithTotalNum:(int)num andTotalMoney:(CGFloat)money{
        if (self.detailInfo.productInfo.wholesale_price_way==1){//按数量计价  左边和底部价格都修改
            NSMutableAttributedString *tempBottomStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"已选择%i件 ",num]];
            [tempBottomStr addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(3, [NSString stringWithFormat:@"%i",num].length)];
            NSMutableArray *tempArray=[NSMutableArray array];
            for (int i=0; i<self.numPriceModelArray.count; i++) {//挨个对比属于哪个区间
                SpecAndNumCommonPriceModel *model=[self.numPriceModelArray objectAtIndex:i];
                if (num>=[model.min_number integerValue] &&num<=[model.max_number integerValue]) {
                    [tempArray addObject:model];
                }
            }
            if (tempArray.count==0) {//哪个区间也不属于  用最后一个
                for (int i=0; i<self.numPriceModelArray.count; i++) {
                    SpecAndNumCommonPriceModel *model=[self.numPriceModelArray objectAtIndex:i];
                    if (((i+1)%self.detailInfo.showfield.count)==0) { //如果大于几个正常的数量  分别用最后一个
                        [tempArray addObject:model];
                    }
                }
            }
            //改变底部显示
            for (int j=0; j<tempArray.count; j++) {
                SpecAndNumCommonPriceModel *model=tempArray[j];
                [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: ",model.title] attributes:@{NSForegroundColorAttributeName:mainGrayColor}]];
                if ([model.name isEqualToString:@"sell_price"]) {
                    [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%.1f ",num>=[self.detailInfo.productInfo.wholesale_config_app.moq integerValue]? [model.value floatValue]*num:0.0] attributes:@{NSForegroundColorAttributeName:mainColor}]];
                }else{
                    [tempBottomStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.1f ",num>=[self.detailInfo.productInfo.wholesale_config_app.moq integerValue]?[model.value floatValue]*num:0.0] attributes:@{NSForegroundColorAttributeName:mainColor}]];
                }
            }
            self.allTotalLabel.attributedText=tempBottomStr;
            //更新单价
                for (int k=0; k<self.leftPriceLabelArray.count; k++) {
                    UILabel *label=[self.leftPriceLabelArray objectAtIndex:k];
                    SpecAndNumCommonPriceModel *model=[tempArray objectAtIndex:k];
                    NSMutableAttributedString *tempStr;
                    if ([model.name isEqualToString:@"sell_price"]) {
                        tempStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:%@%.1f ",model.title,@"¥",num>=[self.detailInfo.productInfo.wholesale_config_app.moq integerValue]?[model.value floatValue]:0.0]];
                    }else{
                        tempStr=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@:%.1f ",model.title,num>=[self.detailInfo.productInfo.wholesale_config_app.moq integerValue]?[model.value floatValue]:0.0]];
                    }
                    [tempStr addAttribute:NSForegroundColorAttributeName value:COLOR_999999 range:NSMakeRange(0, model.title.length+1)];
                    label.attributedText=tempStr;
                }
        }else{
            int tempNum=0;
            for (NSDictionary *tempic in self.specDicArray) {//先算数量
                tempNum+=[tempic[@"num"] intValue];
            }
            self.haveChoosedNum=tempNum;
            NSMutableAttributedString *tempBottomStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"已选择%i件 ",tempNum]];
            [tempBottomStr addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(3, [NSString stringWithFormat:@"%i",tempNum].length)];
            NSMutableDictionary *specAndNumDic=[NSMutableDictionary dictionary];
            for (int i=0; i<self.specDicArray.count; i++) {
                NSDictionary *tempDic=self.specDicArray[i];
                NSArray *arr = tempDic[@"price"];
                for (int j = 0; j < arr.count; j++) {
                    SpecAndNumCommonPriceModel *model=tempDic[@"price"][j];
                    CGFloat money;
                    if ([specAndNumDic.allKeys containsObject:model.title]) {
                        money=[specAndNumDic[model.title] floatValue]+[model.value floatValue]*[tempDic[@"num"] floatValue];
                    }else{
                        money=[model.value floatValue]*[tempDic[@"num"] floatValue];
                    }
                    [specAndNumDic setValue:@(money) forKey:model.title];
                }
            }
            //显示
            for (int k=0; k<self.detailInfo.showfield.count; k++) {
                Showfield *field=self.detailInfo.showfield[k];
                for (NSString *key in specAndNumDic.allKeys) {
                    if ([field.title isEqualToString:key]) {
                        NSMutableAttributedString *tempStr;
                        if ([field.name isEqualToString:@"sell_price"]) {
                            tempStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:%@%.1f ",field.title,@"¥",(tempNum>=[self.detailInfo.productInfo.wholesale_config_app.moq intValue]?[specAndNumDic[field.title] floatValue]:0.00)]];
                        }else{
                            tempStr=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@:%.1f ",field.title,(tempNum>=[self.detailInfo.productInfo.wholesale_config_app.moq intValue]?[specAndNumDic[field.title] floatValue]:0.00)]];
                        }
                        [tempStr addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(field.title.length+1, (tempStr.length-1-field.title.length))];
                        [tempBottomStr appendAttributedString:tempStr];
                    }
                }
            }
              self.allTotalLabel.attributedText=tempBottomStr;
        }
    }

@end
