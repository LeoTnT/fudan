//
//  GoodsDetailRecommendGoodsCell.h
//  App3.0
//
//  Created by 孙亚男 on 2017/12/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
@protocol GoodsDetailRecommendGoodsCellDelegate <NSObject>
-(void)didClickGoods:(RecommendProductDataItem *)item;
@end

@interface GoodsDetailRecommendView : UIView
@property(nonatomic,strong)RecommendProductDataItem *item;
@end

@interface GoodsDetailRecommendGoodsCell : UITableViewCell
@property(nonatomic,strong)NSArray *recommendArray;
@property(nonatomic,weak)id<GoodsDetailRecommendGoodsCellDelegate>delegate;
@end
