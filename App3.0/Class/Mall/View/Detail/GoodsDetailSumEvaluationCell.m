//
//  GoodsDetailEvaluationCell.m
//  App3.0
//
//  Created by 孙亚男 on 2017/12/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailSumEvaluationCell.h"

@interface GoodsDetailSumEvaluationCell()
@property(nonatomic,strong)NSMutableArray *starArray;
@property(nonatomic,strong)UILabel *scoreLabel,*sumLabel;
@end

@implementation GoodsDetailSumEvaluationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.starArray=[NSMutableArray array];
        for (int i=0; i<5; i++) {
            UIImageView *image=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_collect_unstar"]];
            [self.contentView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(12+(14+1.5)*i);
                make.centerY.mas_equalTo(self.contentView);
                make.width.height.mas_equalTo(14);
            }];
            [self.starArray addObject:image];
        }
        self.scoreLabel=[UILabel new];
        [self.contentView addSubview:self.scoreLabel];
        self.scoreLabel.textColor=[UIColor hexFloatColor:@"FA952F"];
        self.scoreLabel.font=[UIFont systemFontOfSize:16];
        [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(((UIImageView *)self.starArray.lastObject).mas_right).mas_offset(6);
            make.centerY.mas_equalTo(self.contentView);
        }];
        self.scoreLabel.userInteractionEnabled=YES;
        self.sumLabel=[UILabel new];
        [self.contentView addSubview:self.sumLabel];
        self.sumLabel.font=[UIFont systemFontOfSize:14];
        self.sumLabel.textColor=COLOR_666666;
        self.sumLabel.textAlignment=NSTextAlignmentRight;
        [self.sumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).mas_offset(-30);
            make.centerY.mas_equalTo(self.contentView);
        }];
        self.sumLabel.userInteractionEnabled=YES;
    }
    return self;
}
-(void)setDetailInfo:(GoodsDetailInfo *)detailInfo{
    _detailInfo=detailInfo;
    if ([detailInfo.productInfo.eva integerValue]==0) {
        self.scoreLabel.text=@"5.0";
        for (int i=0;i<5; i++) {
            UIImageView *image=[self.starArray objectAtIndex:i];
            image.image=[UIImage imageNamed:@"user_collect_star"];
        }
    }else{
        self.scoreLabel.text=[NSString stringWithFormat:@"%.1f",[detailInfo.productInfo.eva floatValue]];
        for (int i=0;i<[detailInfo.productInfo.eva integerValue]; i++) {
            if (i < self.starArray.count) {
                UIImageView *image=[self.starArray objectAtIndex:i];
                image.image=[UIImage imageNamed:@"user_collect_star"];
            }
        }
    }
    self.sumLabel.text=[NSString stringWithFormat:@"%@人已评价",detailInfo.productEvaSum.sum.length?detailInfo.productEvaSum.sum:@"0"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
