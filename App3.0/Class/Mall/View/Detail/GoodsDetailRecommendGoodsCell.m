//
//  GoodsDetailRecommendGoodsCell.m
//  App3.0
//
//  Created by 孙亚男 on 2017/12/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailRecommendGoodsCell.h"

@interface GoodsDetailRecommendView()
@property(nonatomic,strong)UIImageView *goodsImage;
@property(nonatomic,strong)UILabel *goodsNameLabel,*goodsPriceLabel,*goodsSellNumLabel;
@end
@implementation GoodsDetailRecommendView
-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        self.backgroundColor=[UIColor whiteColor];
        self.goodsImage=[UIImageView new];
        self.goodsImage.userInteractionEnabled=YES;
        [self addSubview:self.goodsImage];
        self.goodsNameLabel=[UILabel new];
        self.goodsNameLabel.numberOfLines=2;
        self.goodsNameLabel.textColor=[UIColor hexFloatColor:@"333333"];
        self.goodsNameLabel.font=[UIFont systemFontOfSize:14];
        [self addSubview:self.goodsNameLabel];
        self.goodsNameLabel.userInteractionEnabled=YES;
        self.goodsPriceLabel=[UILabel new];
        [self addSubview:self.goodsPriceLabel];
        self.goodsPriceLabel.font=[UIFont systemFontOfSize:11];
        self.goodsPriceLabel.textColor=[UIColor hexFloatColor:@"3286FF"];
        self.goodsSellNumLabel=[UILabel new];
        [self addSubview:self.goodsSellNumLabel];
        self.goodsSellNumLabel.textColor=[UIColor hexFloatColor:@"7E7E7E"];
        self.goodsSellNumLabel.font=[UIFont systemFontOfSize:11];
        [self.goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self);
            make.right.mas_equalTo(-1.5);
            make.height.mas_equalTo((mainWidth-3)/2.0);
        }];
        [self.goodsNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.goodsImage.mas_bottom).mas_offset(6.5);
        }];
        [self.goodsPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.goodsNameLabel.mas_bottom).mas_offset(11.5);
        }];
        [self.goodsSellNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.mas_equalTo(self.goodsPriceLabel);
        }];
    }
    return self;
}
-(void)setItem:(RecommendProductDataItem *)item{
    _item=item;
    [self.goodsImage getImageWithUrlStr:item.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.goodsNameLabel.text=item.product_name;
    NSInteger length=[NSString stringWithFormat:@"%i",[item.sell_price intValue]].length;
    NSMutableAttributedString *tempStr=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.1f",[item.sell_price floatValue]]];
    [tempStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, length)];
    self.goodsPriceLabel.attributedText=tempStr;
    self.goodsSellNumLabel.text=[NSString stringWithFormat:@"已售%@件",item.sell_num];
}
@end

#pragma mark-cell
@interface GoodsDetailRecommendGoodsCell()

@end

@implementation GoodsDetailRecommendGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor=BG_COLOR;
    }
    return self;
}
-(void)setRecommendArray:(NSArray *)recommendArray{
    for (UIView *view in self.contentView.subviews) {
        if (view.tag>0) {
            [view removeFromSuperview];
        }
    }
    if (recommendArray.count==2) {
        for (int i=0; i<2; i++) {
            GoodsDetailRecommendView *view=[[GoodsDetailRecommendView alloc] initWithFrame:CGRectMake(i*(mainWidth/2.0), 0, mainWidth/2.0, mainWidth/2.0+75)];
            view.tag=i+1;
            view.item=recommendArray[i];
            [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(didClickGoods:)]) {
                    [self.delegate didClickGoods:view.item];
                }
            }]];
            [self.contentView addSubview:view];
        }
    }else{
        GoodsDetailRecommendView *view=[[GoodsDetailRecommendView alloc] initWithFrame:CGRectMake(0, 0, mainWidth/2.0, mainWidth/2.0+75)];
        view.tag=1;
        view.item=recommendArray.firstObject;
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didClickGoods:)]) {
                [self.delegate didClickGoods:view.item];
            }
        }]];
        [self.contentView addSubview:view];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
