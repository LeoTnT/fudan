//
//  ShowIfHaveEvaluationCell.m
//  App3.0
//
//  Created by syn on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ShowIfHaveEvaluationCell.h"

@implementation ShowIfHaveEvaluationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setDetailInfo:(GoodsDetailInfo *)detailInfo{
    _detailInfo=detailInfo;
    //清界面
    for (UIView *view in self.contentView.subviews) {
        if (view.tag>0) {
            [view removeFromSuperview];
        }
    }
   if([detailInfo.productEvaSum.sum integerValue]==0){//没有评价
        UILabel *noneLabel=[[UILabel alloc] initWithFrame:CGRectMake((mainWidth-100)/2.0, 10,100 , 35)];
        noneLabel.textAlignment=NSTextAlignmentCenter;
        noneLabel.text=@"暂无评价";
        noneLabel.font=[UIFont systemFontOfSize:14];
        noneLabel.tag=100;
        [self.contentView addSubview:noneLabel];
    }else{
        //更多评价
        self.getMoreEvaBtn=[[UIButton alloc] initWithFrame:CGRectMake((mainWidth-150)/2.0,10, 150, 35)];
        self.getMoreEvaBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        self.getMoreEvaBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.getMoreEvaBtn setTitle:@"查看全部评价" forState:UIControlStateNormal];
        [self.getMoreEvaBtn setTitleColor:[UIColor colorWithRed:255/255.0 green:101/255.0 blue:36/255.0 alpha:1]forState:UIControlStateNormal];
        [self.contentView addSubview:self.getMoreEvaBtn];
        self.getMoreEvaBtn.layer.cornerRadius=5;
        self.getMoreEvaBtn.layer.borderWidth=1.5;
        self.getMoreEvaBtn.layer.borderColor=[UIColor colorWithRed:255/255.0 green:101/255.0 blue:36/255.0 alpha:1].CGColor;
        self.getMoreEvaBtn.tag=100;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
