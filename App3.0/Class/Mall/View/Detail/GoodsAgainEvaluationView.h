//
//  GoodsAgainEvaluationView.h
//  App3.0
//
//  Created by syn on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsAgainEvaluationView : UIView
@property(nonatomic,strong)NSDictionary *againDataDic;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,strong)UIViewController *currentVC;
@end
