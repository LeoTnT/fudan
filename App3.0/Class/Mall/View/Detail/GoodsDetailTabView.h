//
//  GoodsDetailTabView.h
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@protocol GoodsDetailTabViewDelegate <NSObject>
-(void)clickTabContact;
-(void)clickTabCollect;
-(void)clickTabAddToCart;
-(void)clickTabBuyNow;
-(void)clickTabGroupBuy;
@end

@interface GoodsDetailTabView : UIView
@property(nonatomic,assign)GoodSellType type;
@property(nonatomic,strong)NSString *isFavorite;
@property(nonatomic,weak)id<GoodsDetailTabViewDelegate>delegate;
@end
