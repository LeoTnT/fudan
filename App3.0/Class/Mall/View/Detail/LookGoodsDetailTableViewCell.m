//
//  LookGoodsDetailTableViewCell.m
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "LookGoodsDetailTableViewCell.h"

@implementation LookGoodsDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 10, mainWidth, 30)];
        tipsLabel.text=@"点击这里，查看图文详情";
        [self.contentView addSubview:tipsLabel];
        tipsLabel.textAlignment=NSTextAlignmentCenter;
        tipsLabel.font=[UIFont systemFontOfSize:14];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
