//
//  GoodsDetailSpecView.h
//  App3.0
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
#import "CartModel.h"

@protocol GoodsDetailSpecViewDelegate <NSObject>
@optional
//点击确定取消按钮
-(void)clickCloseBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum;
-(void)clickConfirmBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum;
-(void)clickConfirmBtnWithNewPeid:(NSString *)peid goodsPrice:(CGFloat)goodsPrice stockNum:(NSUInteger)stockNum specStr:(NSString *)specStr specShowStr:(NSString *)showStr;
//点击加入购物车 立即购买
-(void)clickAddGoodsToShoppingCartBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum;
-(void)clickBuyNowBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum;
@end

@interface GoodsDetailSpecView : UIView
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@property(nonatomic,assign)BOOL isClickedBuyNowBtn;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,weak)id <GoodsDetailSpecViewDelegate > delegate;
@property(nonatomic,strong)UIButton *addBtn;
@property(nonatomic,strong)UIButton *subBtn;
@property(nonatomic,strong)UITextField *goodsNumField;
@property(nonatomic,strong)UILabel *remainNumLabel;//剩余
@property(nonatomic,strong)UIButton  *confirmBtn;
@property(nonatomic,strong)UIButton  *addShoppingCartBtn;
@property(nonatomic,strong)UIButton  *buyNowBtn;
@property(nonatomic,strong)UIScrollView *specScroll;
@property(nonatomic,strong) NSMutableDictionary *specDic;//规格字典
@property(nonatomic,strong)NSAttributedString *showSpecStr;
@property(nonatomic,strong) UIViewController *vc;
@property(nonatomic,assign)GoodSellType goodSellType; // 团购 秒杀等商品类型
@property(nonatomic,copy)NSString *goodsNum;
@property(nonatomic,copy)NSString *currentStockNum;
-(instancetype)initWithProductParser:(CartProductListParser *)parser;
@end
