//
//  GoodsDetailTabView.m
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailTabView.h"

@interface GoodsDetailTabView()
@property(nonatomic,strong)UIButton *cartBtn;
@property(nonatomic,strong)UIButton *buyNowBtn;
@property(nonatomic,strong)UIButton *collectBtn;
@property(nonatomic,strong)UIButton *contactBtn;
@end

@implementation GoodsDetailTabView
-(instancetype)init{
    if (self=[super init]) {
        self.backgroundColor=[UIColor whiteColor];
        self.frame=CGRectMake(0, mainHeight-49-kTabbarSafeBottomMargin, mainWidth, 49);
        self.contactBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, mainWidth/3.0/2.0, 49)];
        [self addSubview:self.contactBtn];
        [self.contactBtn setImage:[UIImage imageNamed:@"mall_detail_contact_customer"] forState:UIControlStateNormal];
        [self.contactBtn setTitle:Localized(@"客服") forState:UIControlStateNormal];
        [self.contactBtn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
        self.contactBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [self.contactBtn setTitleEdgeInsets:UIEdgeInsetsMake(self.contactBtn.imageView.frame.size.height+4 ,-self.contactBtn.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
        [self.contactBtn setImageEdgeInsets:UIEdgeInsetsMake(-15, 0.0,0.0, -self.contactBtn.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
        self.collectBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth/3.0/2.0, 0, mainWidth/3.0/2.0, 49)];
        [self addSubview:self.collectBtn];
        [self.collectBtn setImage:[UIImage imageNamed:@"mall_detail_collect"] forState:UIControlStateNormal];
        [self.collectBtn setTitle:@"收  藏" forState:UIControlStateNormal];
        [self.collectBtn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
        self.collectBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [self.collectBtn setTitleEdgeInsets:UIEdgeInsetsMake(self.collectBtn.imageView.frame.size.height+4 ,-self.collectBtn.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
        [self.collectBtn setImageEdgeInsets:UIEdgeInsetsMake(-15, 0.0,0.0, -self.collectBtn.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
        self.cartBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth/3.0, 0, mainWidth/3.0, 49)];
        [self addSubview:self.cartBtn];
        [self.cartBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
        [self.cartBtn setBackgroundColor:[UIColor hexFloatColor:@"3F8EF7"]];
        [self.cartBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.cartBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        self.buyNowBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth/3.0*2, 0, mainWidth/3.0, 49)];
        [self addSubview:self.buyNowBtn];
        [self.buyNowBtn setTitle:@"立即购买" forState:UIControlStateNormal];
        [self.buyNowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.buyNowBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.buyNowBtn setBackgroundColor:[UIColor hexFloatColor:@"026EFE"]];
        [self.contactBtn addTarget:self action:@selector(contact) forControlEvents:UIControlEventTouchUpInside];
        [self.collectBtn addTarget:self action:@selector(collect) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)setIsFavorite:(NSString *)isFavorite{
    _isFavorite=isFavorite;
    if ([isFavorite integerValue]) {
        [self.collectBtn setImage:[UIImage imageNamed:@"mall_detail_collected"] forState:UIControlStateNormal];
        [self.collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
    }else{
          [self.collectBtn setImage:[UIImage imageNamed:@"mall_detail_collect"] forState:UIControlStateNormal];
        [self.collectBtn setTitle:@"收  藏" forState:UIControlStateNormal];
    }
}
-(void)contact{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTabContact)]) {
        [self.delegate clickTabContact];
    }
}
-(void)collect{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTabCollect)]) {
        [self.delegate clickTabCollect];
    }
}
-(void)addToCart{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTabAddToCart)]) {
        [self.delegate clickTabAddToCart];
    }
}
-(void)buy{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTabBuyNow)]) {
        [self.delegate clickTabBuyNow];
    }
}
-(void)groupBuy{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTabGroupBuy)]) {
        [self.delegate clickTabGroupBuy];
    }
}
-(void)setType:(GoodSellType)type{
    _type=type;
     if (type==GoodSellWholesale || type == GoodSellFillPrice) {
        self.cartBtn.hidden=YES;
        self.buyNowBtn.frame=CGRectMake(mainWidth/3.0, 0, mainWidth/3.0*2, 49);
        [self.buyNowBtn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
    }else if (type==GoodSellGroupBuy) {
        [self.cartBtn setTitle:@"单独购买" forState:UIControlStateNormal];
        [self.buyNowBtn setTitle:@"一键拼单" forState:UIControlStateNormal];
        [self.cartBtn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
        [self.buyNowBtn addTarget:self action:@selector(groupBuy) forControlEvents:UIControlEventTouchUpInside];
    }else if (type == GoodSellSecondKill) {
        self.cartBtn.hidden = YES;
        [self.buyNowBtn setTitle:@"一键秒杀" forState:UIControlStateNormal];
        [self.buyNowBtn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [self.cartBtn addTarget:self action:@selector(addToCart) forControlEvents:UIControlEventTouchUpInside];
        [self.buyNowBtn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
    }
}
@end
