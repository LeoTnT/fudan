//
//  GoodsDetailTopScrollCell.m
//  App3.0
//
//  Created by mac on 2017/4/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailTopScrollCell.h"
@interface GoodsDetailTopScrollCell()<SDCycleScrollViewDelegate>
@property(nonatomic,strong)SDCycleScrollView *scrollView;
@property(nonatomic,strong)NSMutableArray *imgUrlArray;
@end

@implementation GoodsDetailTopScrollCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.userInteractionEnabled = YES;
        self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, mainWidth) delegate:self placeholderImage:nil];
        self.scrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        self.scrollView.hidesForSinglePage = YES;
        
        self.scrollView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
        self.scrollView.pageDotColor = [UIColor whiteColor];
        self.scrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.scrollView.placeholderImage = [UIImage imageNamed:@"no_pic"];
        self.scrollView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        
        [self addSubview: self.scrollView];
    }
    return self;
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(hidtipsView)]) {
        [self.delegate hidtipsView];
    }
    [self enLarge:index];
}

//刷新滚动图片
-(void)setImageList:(NSString *)imageList{
    _imageList=imageList;
    self.imgUrlArray=[NSMutableArray array];
    NSArray *imageArray;
    if ([imageList containsString:@","]) {
        //截取图片
        imageArray=[imageList componentsSeparatedByString:@","];
        for (int i=0; i<imageArray.count; i++) {
            NSString *str=[imageArray objectAtIndex:i];
            [self.imgUrlArray addObject:str];
        }
    }else if(imageList.length){
        imageArray=@[imageList];
        [self.imgUrlArray addObject:imageList];
    }
    self.scrollView.imageURLStringsGroup = self.imgUrlArray;
}
#pragma mark-放大图片
-(void)enLarge:(NSInteger )index{
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:self.imgUrlArray currentImageIndex:index pageControlHidden:YES];
    [browser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    browser.isCusctomAdd = YES;
    [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
    [self.currentVC.tabBarController.view addSubview:browser];
}

-(void)photoBrowserDismiss{
    //    [self.vc.navigationController setNavigationBarHidden:NO animated:NO];
    //    [self.vc.tabBarController.tabBar setHidden:NO];
}
#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
