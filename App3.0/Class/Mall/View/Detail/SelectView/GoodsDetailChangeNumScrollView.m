//
//  GoodsDetailChangeNumScrollView.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailChangeNumScrollView.h"
#import "GoodsDetailChangeNumView.h"

@interface GoodsDetailChangeNumScrollView()<GoodsDetailChangeNumViewDelegate>
@property(nonatomic,strong)NSMutableArray *changeViewArray,*chooseSpecNumArray;
@property(nonatomic,assign)CGFloat totalMoney,totalNum;

@end

@implementation GoodsDetailChangeNumScrollView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=BG_COLOR;
        self.bounces=NO;
    }
    return self;
}
-(void)setItem:(SpecItem *)item{
    _item=item;
    for (UIView *view in self.subviews) {
        if (view.tag>0) {
            [view removeFromSuperview];
        }
    }
    self.changeViewArray=[NSMutableArray array];
    self.chooseSpecNumArray=[NSMutableArray array];
    for (int i=0; i<item.spec_value.count; i++) {
        GoodsDetailChangeNumView *numView=[[GoodsDetailChangeNumView alloc] initWithFrame:CGRectMake(0, 70*i, self.frame.size.width, 70)];
        numView.tag=i+1;
        numView.detailInfo=self.detailInfo;
        numView.productMap=self.productMap;
        numView.valueItem=item.spec_value[i];
        numView.selectValueItem=self.selectValueItem;
        if (self.detailInfo.productInfo.wholesale_price_way==1) {
            numView.isShowPrice=NO;
        }else{
            numView.isShowPrice=YES;
        }
        [self addSubview:numView];
        [self.changeViewArray addObject:numView];
        numView.delegate=self;
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 69.5, self.frame.size.width, 0.5)];
        line.backgroundColor=[UIColor lightGrayColor];
        line.tag=10000;
        line.alpha=0.6;
        [numView addSubview:line];
    }
    self.contentSize=CGSizeMake(0, 70*item.spec_value.count);
}

-(void)numViewChangedWithNum:(int)num andPrice:(NSArray *)priceArray valueItem:(Spec_valueItem *)valueItem extId:(NSString *)extId{
    if (self.changeNumDelegate && [self.changeNumDelegate respondsToSelector:@selector(numChangedWithSpecArray:andTotalNum:andTotalMoney:)]) {
        for (NSMutableDictionary *tempDic in self.chooseSpecNumArray) {
            if ([tempDic[@"ids"] isEqualToString:extId]) {//之前加入过
                if (num==0) {//不再购买  数量为0
                    [self.chooseSpecNumArray removeObject:tempDic];
                    [self getRefreshInfo];
                    return;
                }else{
                    [tempDic setValue:@(num) forKey:@"num"];
                    [self getRefreshInfo];
                    return;
                }
                
            }
        }
        //之前没有加入过
        [self.chooseSpecNumArray addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"ids":extId,@"num":@(num),@"price":priceArray}]];
        [self getRefreshInfo];
    }
}
-(void)setSelectValueItem:(Spec_valueItem *)selectValueItem{
    _selectValueItem=selectValueItem;
    for (GoodsDetailChangeNumView *view in self.changeViewArray) {
        view.selectValueItem=selectValueItem;
    }
}
-(void)setProductMap:(NSDictionary *)productMap{
    _productMap=productMap;
}
-(void)getRefreshInfo{
    int tempNum=0;
    CGFloat tempMoney=0;
    NSLog(@"%@",self.chooseSpecNumArray);
    for (NSDictionary *tempic in self.chooseSpecNumArray) {
        tempNum+=[tempic[@"num"] intValue];
//        tempMoney+=[tempic[@"num"] intValue]*[tempic[@"price"] floatValue];
    }
    if (self.detailInfo.productInfo.wholesale_price_way==2) {  //根据不同规格计算
    [self.changeNumDelegate numChangedWithSpecArray:self.chooseSpecNumArray andTotalNum:0 andTotalMoney:0.00];//临时值,去规格总区域计算数据
    }else{
        [self.changeNumDelegate numChangedWithSpecArray:self.chooseSpecNumArray andTotalNum:tempNum andTotalMoney:tempMoney];
    }
}
@end
