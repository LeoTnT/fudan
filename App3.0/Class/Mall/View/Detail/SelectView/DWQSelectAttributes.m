//
//  DWQSelectAttributes.m
//  DWQSelectAttributes
//
//  Created by 杜文全 on 15/5/21.
//  Copyright © 2015年 com.sdzw.duwenquan. All rights reserved.
//

#import "DWQSelectAttributes.h"
#import "GoodsDetailModel.h"
#import "SpecItemBtn.h"

@implementation DWQSelectAttributes

-(instancetype)initWithSpec:(SpecItem *)specItem valueArray:(NSArray *)specValueArray andFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.frame = frame;
        self.specItem =specItem ;
        
        self.specValueArray = [NSArray arrayWithArray:specValueArray];
        
        [self rankView];
    }
    return self;
}


-(void)rankView{
    
    self.packView = [[UIView alloc] initWithFrame:self.frame];
    self.packView.dwq_y = 0;

    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, mainWidth, 25)];
    titleLB.text = self.specItem.spec_name;
    titleLB.font =[UIFont systemFontOfSize:12];
    [self.packView addSubview:titleLB];
    
    self.btnView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(titleLB.frame), mainWidth, 40)];
    [self.packView addSubview:self.btnView];
    
    int count = 0;
    float btnWidth = 0;
    float viewHeight = 0;
    for (int i = 0; i < self.specValueArray.count; i++) {
        Spec_valueItem *item=self.specValueArray[i];
        SpecItemBtn *btn = [SpecItemBtn buttonWithType:UIButtonTypeCustom];
        btn.specItem=self.specItem;
        btn.specValueItem=item;
        if (self.specValueArray.count==1) {
            [btn setBackgroundColor:TAB_SELECTED_COLOR];
            btn.selected = YES;
            self.selectBtn = btn;
        }else{
            [btn setBackgroundColor:BG_COLOR];
            btn.selected=NO;
        }
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn setTitle:item.spec_value_name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        btn.layer.cornerRadius = 5;
        btn.layer.masksToBounds = YES;
        
        NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:13] forKey:NSFontAttributeName];
        CGSize btnSize = [item.spec_value_name sizeWithAttributes:dict];
        
        btn.dwq_width = btnSize.width + 15;
        btn.dwq_height = btnSize.height + 12;
        
        if (i==0)
        {
            btn.dwq_x = 20;
            btnWidth += CGRectGetMaxX(btn.frame);
        }
        else{
            btnWidth += CGRectGetMaxX(btn.frame)+20;
            if (btnWidth > mainWidth) {
                count++;
                btn.dwq_x = 20;
                btnWidth = CGRectGetMaxX(btn.frame);
            }
            else{
                
                btn.dwq_x += btnWidth - btn.dwq_width;
            }
        }
        btn.dwq_y += count * (btn.dwq_height+10)+10;
        
        viewHeight = CGRectGetMaxY(btn.frame)+10;
        
        [self.btnView addSubview:btn];
        
        btn.tag = 10000+i;
    }
    self.btnView.dwq_height = viewHeight;
    self.packView.dwq_height = self.btnView.dwq_height+CGRectGetMaxY(titleLB.frame);
    
    self.dwq_height = self.packView.dwq_height;
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.dwq_height, mainWidth, 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.packView addSubview:line];
    [self addSubview:self.packView];
}


-(void)btnClick:(SpecItemBtn *)btn{
    
    
    if (![self.selectBtn isEqual:btn]) {
        
        self.selectBtn.backgroundColor = BG_COLOR;
        self.selectBtn.selected = NO;
    }
    btn.backgroundColor = TAB_SELECTED_COLOR;
    btn.selected = YES;
    self.selectBtn = btn;
    
    if ([self.delegate respondsToSelector:@selector(clickSpecBtnWithSpecId:specValueId:)]) {
        
        [self.delegate clickSpecBtnWithSpecId:self.specItem.spec_id specValueId:self.selectBtn.specValueItem.spec_value_id];
    }
}

@end
