//
//  WholeCategoryScrollview.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WholeCategoryScrollview.h"
#import "SpecButton.h"

@implementation WholeCategoryScrollview
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.bounces=NO;
    }
    return self;
}
-(void)setItem:(SpecItem *)item{
    _item=item;
    for (UIView *view in self.subviews) {
        if (view.tag>0) {
            [view removeFromSuperview];
        }
    }
    CGFloat btnHeight=self.isLeft?35:70,tempHeight=0;
    if(self.isLeft){
        for (int i=0; i<item.spec_value.count; i++) {
            Spec_valueItem *valueItem=[item.spec_value objectAtIndex:i];
            SpecButton *btn=[[SpecButton alloc] initWithFrame:CGRectMake(10, 15*(i+1)+btnHeight*i, self.frame.size.width-20, btnHeight)];
            btn.tag=i+1;
            btn.valueItem=valueItem;
            btn.selected=NO;
            btn.titleLabel.font=[UIFont systemFontOfSize:13];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundImage:[UIImage imageNamed:@"wholeSpecGoodsBtnUnClickedBg"] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"wholeSpecGoodsBtnClickedBg"] forState:UIControlStateSelected];
            [self addSubview:btn];
            tempHeight=CGRectGetMaxY(btn.frame)+15;
            if (i==0) {
                btn.selected=YES;
            }
        }
    }else{
        for (int i=0; i<item.spec_value.count; i++) {
            Spec_valueItem *valueItem=[item.spec_value objectAtIndex:i];
            SpecButton *btn=[[SpecButton alloc] initWithFrame:CGRectMake(0, btnHeight*i, self.frame.size.width, btnHeight)];
            btn.tag=i+1;
            btn.valueItem=valueItem;
            btn.userInteractionEnabled=NO;
            btn.titleLabel.font=[UIFont systemFontOfSize:15];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundColor:BG_COLOR];
            [self addSubview:btn];
            UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0,69.5, self.frame.size.width, 0.5)];
            line.backgroundColor=[UIColor lightGrayColor];
            line.tag=10000;
            line.alpha=0.6;
            [btn addSubview:line];
             tempHeight=CGRectGetMaxY(btn.frame);
        }
    }
    self.contentSize=CGSizeMake(0,tempHeight);
  
}

-(void)btnClick:(SpecButton *)btn{
    if (self.isLeft) {
        btn.selected=YES;
        if (btn.selected) {
            //其他的非选中
            for (UIView *view in self.subviews) {
                if (view.tag>0 && (![view isEqual:btn])) {
                    ((UIButton *)view).selected=NO;
                }
            }
            if (self.selectDelegate && [self.selectDelegate respondsToSelector:@selector(didSelectSpec:with:)]) {
                [self.selectDelegate didSelectSpec:self.item with:btn.valueItem];
            }
        }
    }
}
-(void)setIsLeft:(BOOL)isLeft{
    _isLeft=isLeft;
    if (isLeft) {
        self.backgroundColor=[UIColor whiteColor];
    }else{
        self.backgroundColor=BG_COLOR;
    }
}
@end
