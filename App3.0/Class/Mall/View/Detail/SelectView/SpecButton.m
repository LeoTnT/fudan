//
//  SpecButton.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SpecButton.h"

@implementation SpecButton

-(void)setValueItem:(Spec_valueItem *)valueItem{
    _valueItem=valueItem;
    [self setTitle:valueItem.spec_value_name forState:UIControlStateNormal];
}

@end
