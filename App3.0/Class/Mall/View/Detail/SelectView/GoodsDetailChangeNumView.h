//
//  GoodsDetailChangeNumView.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
#import "GoodsDetailWholeSpecView.h"
@protocol GoodsDetailChangeNumViewDelegate <NSObject>
-(void)numViewChangedWithNum:(int)num andPrice:(NSArray *)priceArray valueItem:(Spec_valueItem *)valueItem extId:(NSString *)extId;
@end

@interface GoodsDetailChangeNumView : UIView
@property(nonatomic,strong)Spec_valueItem *valueItem,*selectValueItem;
@property(nonatomic,strong)NSDictionary *productMap;
@property(nonatomic,strong)UITextField *goodsNumField;
@property(nonatomic,strong)UILabel *goodsPriceLabel;
@property(nonatomic,strong)UILabel *stockNumLabel;
@property(nonatomic,weak)id<GoodsDetailChangeNumViewDelegate>delegate;
@property(nonatomic,assign)BOOL isShowPrice;
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@end
