//
//  DWQSelectAttributes.h
//  DWQSelectAttributes
//
//  Created by 杜文全 on 15/5/21.
//  Copyright © 2015年 com.sdzw.duwenquan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+DWQExtension.h"
#import "GoodsDetailModel.h"
#import "SpecItemBtn.h"

@protocol SelectAttributesDelegate <NSObject>
@required
-(void)clickSpecBtnWithSpecId:(NSString *)specId specValueId:(NSString *)specValueId;

@end

@interface DWQSelectAttributes : UIView

@property(nonatomic,strong)SpecItem *specItem;
@property(nonatomic,strong)NSArray *specValueArray;

@property(nonatomic,strong)SpecItemBtn *selectBtn;

@property(nonatomic,strong)UIView *packView;
@property(nonatomic,strong)UIView *btnView;
@property(nonatomic,assign)id<SelectAttributesDelegate> delegate;



-(instancetype)initWithSpec:(SpecItem *)specItem valueArray:(NSArray *)specValueArray andFrame:(CGRect)frame;


@end
