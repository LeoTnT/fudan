//
//  SpecButton.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@interface SpecButton : UIButton
@property(nonatomic,strong)Spec_valueItem *valueItem;
@end
