//
//  SpecItemBtn.h
//  App3.0
//
//  Created by syn on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
@interface SpecItemBtn : UIButton
@property(nonatomic,strong)Spec_valueItem *specValueItem;
@property(nonatomic,strong)SpecItem *specItem;
@end
