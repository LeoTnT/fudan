//
//  GoodsDetailChangeNumScrollView.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@protocol GoodsDetailChangeNumScrollViewDelegate <NSObject>
-(void)numChangedWithSpecArray:(NSArray *)specArray andTotalNum:(int)num andTotalMoney:(CGFloat)money;
@end

@interface GoodsDetailChangeNumScrollView : UIScrollView
@property(nonatomic,strong)Spec_valueItem *selectValueItem;
@property(nonatomic,strong)NSDictionary *productMap;
@property(nonatomic,strong)SpecItem *item;
@property(nonatomic,assign)int minStartWhole;//最小起批量
@property(nonatomic,weak)id<GoodsDetailChangeNumScrollViewDelegate>changeNumDelegate;
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@end
