//
//  WholeCategoryScrollview.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@protocol WholeCategoryScrollviewDelegate <NSObject>
-(void)didSelectSpec:(SpecItem *)specItem with:(Spec_valueItem *)valueItem;
@end


@interface WholeCategoryScrollview : UIScrollView
@property(nonatomic,strong)SpecItem *item;
@property(nonatomic,weak)id <WholeCategoryScrollviewDelegate>selectDelegate;
@property(nonatomic,assign)BOOL isLeft;
@end
