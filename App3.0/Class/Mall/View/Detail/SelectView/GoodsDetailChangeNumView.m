//
//  GoodsDetailChangeNumView.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailChangeNumView.h"

@interface GoodsDetailChangeNumView()
@property(nonatomic,strong)UIButton *addBtn,*subBtn;
@property(nonatomic,strong)NSDictionary *map;
@property(nonatomic,assign)int stockNum;//库存  最小起批量
@property(nonatomic,assign)CGFloat price;//单价
@property(nonatomic,strong)NSMutableDictionary *haveChoosedDic;//之前选择过的数量记忆
@property(nonatomic,strong)NSMutableArray *tempPriceArray;
@end

@implementation GoodsDetailChangeNumView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        //加号按钮
        self.addBtn=[[UIButton alloc] init];
        [self addSubview: self.addBtn];
        [ self.addBtn addTarget:self action:@selector(addGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.addBtn setImage:[UIImage imageNamed:@"mall_cart_add"] forState:UIControlStateNormal];
        [self.addBtn setImage:[UIImage imageNamed:@"mall_cart_add_h"] forState:UIControlStateDisabled];
        [self.addBtn setBackgroundColor:[UIColor whiteColor]];
        //展示框
        self.goodsNumField=[[UITextField alloc] init];
        self.goodsNumField.font=[UIFont systemFontOfSize:12];
        self.goodsNumField.text=@"0";
        self.goodsNumField.backgroundColor=[UIColor whiteColor];
        self.goodsNumField.keyboardType=UIKeyboardTypeDecimalPad;
        self.goodsNumField.textAlignment=NSTextAlignmentCenter;
        [self.goodsNumField addTarget:self action:@selector(goodsNumChanged:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:self.goodsNumField];
        //减号按钮
        self.subBtn=[[UIButton alloc] init];
        [self addSubview: self.subBtn];
        [ self.subBtn addTarget:self action:@selector(deleteGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub"] forState:UIControlStateNormal];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub_h"] forState:UIControlStateDisabled];
        [self.subBtn setBackgroundColor:[UIColor whiteColor]];
        self.stockNumLabel=[[UILabel alloc] init];
        [self addSubview:self.stockNumLabel];
        self.stockNumLabel.font=[UIFont systemFontOfSize:10];
        self.stockNumLabel.textColor=mainGrayColor;
        self.goodsPriceLabel=[[UILabel alloc] init];
        [self addSubview:self.goodsPriceLabel];
        self.goodsPriceLabel.font=[UIFont systemFontOfSize:10];
        self.goodsPriceLabel.textColor=mainGrayColor;
        //位置
        self.addBtn.frame=CGRectMake(self.frame.size.width-5-30, 10, 30, 25);
        self.goodsNumField.frame=CGRectMake(CGRectGetMinX(self.addBtn.frame)-40, CGRectGetMinY(self.addBtn.frame), 40, 25);
        self.subBtn.frame=CGRectMake(CGRectGetMinX(self.goodsNumField.frame)-30, CGRectGetMinY(self.addBtn.frame), 30, 25);
        self.stockNumLabel.frame=CGRectMake(self.frame.size.width-110, CGRectGetMaxY(self.addBtn.frame), 100, 17);
        self.stockNumLabel.textAlignment=NSTextAlignmentRight;
        self.goodsPriceLabel.textAlignment=NSTextAlignmentRight;
        self.goodsPriceLabel.frame=CGRectMake(10, CGRectGetMaxY(self.stockNumLabel.frame), self.frame.size.width-20, 18);
        self.stockNum=0;
    }
    return self;
}
-(void)addGoodsNum{
    int num=[self.goodsNumField.text intValue];
    if(num<self.stockNum){
        num++;
        self.goodsNumField.text=[NSString stringWithFormat:@"%i",num];
        if (self.delegate &&[self.delegate respondsToSelector:@selector(numViewChangedWithNum:andPrice:valueItem:extId:)]) {
            [self.delegate numViewChangedWithNum:[self.goodsNumField.text intValue] andPrice:self.tempPriceArray valueItem:self.valueItem extId:self.map[@"product_ext_id"]];
        }
    }
    if ([self.goodsNumField.text intValue]==self.stockNum) {
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
    if ([self.goodsNumField.text intValue]>0) {
        self.subBtn.enabled=YES;
    }else{
        self.subBtn.enabled=NO;
    }
    if(self.map[@"product_ext_id"]){
        [self.haveChoosedDic setValue:self.goodsNumField.text forKey:self.map[@"product_ext_id"]];
    }
}
-(void)deleteGoodsNum{
    int num=[self.goodsNumField.text intValue];
    if (num>0) {
        num--;
        self.goodsNumField.text=[NSString stringWithFormat:@"%i",num];
    }
    if (num==0) {
        self.subBtn.enabled=NO;
    }else{
        self.subBtn.enabled=YES;
    }
    if ([self.goodsNumField.text intValue]<self.stockNum) {
        self.addBtn.enabled=YES;
    }else{
         self.addBtn.enabled=NO;
    }
    if (self.delegate &&[self.delegate respondsToSelector:@selector(numViewChangedWithNum:andPrice:valueItem:extId:)]) {
        [self.delegate numViewChangedWithNum:[self.goodsNumField.text intValue] andPrice:self.tempPriceArray valueItem:self.valueItem extId:self.map[@"product_ext_id"]];
    }
    if (self.map[@"product_ext_id"]) {//目前批发最多支持两种规格
        if ([self.goodsNumField.text intValue]==0) {
            [self.haveChoosedDic removeObjectForKey:self.map[@"product_ext_id"]];
        }else{
            [self.haveChoosedDic setValue:self.goodsNumField.text forKey:self.map[@"product_ext_id"]];
        }
    }
}

-(void)setValueItem:(Spec_valueItem *)valueItem{
    _valueItem=valueItem;
    self.haveChoosedDic=[NSMutableDictionary dictionary];
    for (NSString *key in self.productMap.allKeys) {
        if ([key isEqualToString:valueItem.spec_value_name]) {
            self.map=self.productMap[key];
            [self refreshView];
            return;
        }
    }
}
-(void)setSelectValueItem:(Spec_valueItem *)selectValueItem{
    _selectValueItem=selectValueItem;
    for (NSString *key in self.productMap.allKeys) {
        if ([[[NSString stringWithFormat:@"%@,",selectValueItem.spec_value_name] stringByAppendingString:self.valueItem.spec_value_name] isEqualToString:key]) {
            self.map=self.productMap[key];
            [self refreshView];
            return;
        }
    }
}
-(void)refreshView{
    self.stockNum=[self.map[@"stock"] intValue];
    self.tempPriceArray=[NSMutableArray array];
    NSMutableAttributedString *tempStr=[[NSMutableAttributedString alloc] init];
    for (int i=0; i<self.detailInfo.showfield.count; i++) {
        Showfield *field=[self.detailInfo.showfield objectAtIndex:i];
        if ([self.map.allKeys containsObject:field.name] && [self.map[field.name] floatValue]) {
            [tempStr appendAttributedString:[[NSAttributedString alloc] initWithString:field.title attributes:@{NSForegroundColorAttributeName:mainGrayColor}]];
            if ([field.name isEqualToString:@"sell_price"]) {
                        [tempStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%.1f ",[self.map[field.name] floatValue]] attributes:@{NSForegroundColorAttributeName:mainColor}]];
                    }else{
                        [tempStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.1f ",[self.map[field.name] floatValue]] attributes:@{NSForegroundColorAttributeName:mainColor}]];
                    }
            SpecAndNumCommonPriceModel *model=[SpecAndNumCommonPriceModel new];
            model.value=self.map[field.name];
            model.name=field.name;
            model.title=field.title;
            [self.tempPriceArray addObject:model];
        }
    }
    self.goodsNumField.text=@"0";
    for (NSString *key in self.haveChoosedDic.allKeys) {
        if ([self.map[@"product_ext_id"] isEqualToString:key]) {
            self.goodsNumField.text=self.haveChoosedDic[key];
            break;
        }
    }
    if ([self.goodsNumField.text intValue]<self.stockNum) {
        self.addBtn.enabled=YES;
    }else{
        self.addBtn.enabled=NO;
    }
    if([self.goodsNumField.text intValue]>0) {
        self.subBtn.enabled=YES;
    }else{
        self.subBtn.enabled=NO;
    }
    self.stockNumLabel.text=[NSString stringWithFormat:@"库存:%@",self.map[@"stock"]];
    self.goodsPriceLabel.attributedText=tempStr;
}
-(void)goodsNumChanged:(UITextField *)field{
    if ([field.text intValue]>self.stockNum) {
        field.text=[NSString stringWithFormat:@"%i",self.stockNum];
    }
    if([field.text intValue]>0){
        self.subBtn.enabled=YES;
    }else{
        self.subBtn.enabled=NO;
    }
    if (field.text.intValue<self.stockNum) {
        self.addBtn.enabled=YES;
    }else{
        self.addBtn.enabled=NO;
    }
    if (self.delegate &&[self.delegate respondsToSelector:@selector(numViewChangedWithNum:andPrice:valueItem:extId:)]) {
        [self.delegate numViewChangedWithNum:[self.goodsNumField.text intValue] andPrice:self.tempPriceArray valueItem:self.valueItem extId:self.map[@"product_ext_id"]];
    }
}
-(void)setIsShowPrice:(BOOL)isShowPrice{
    _isShowPrice=isShowPrice;
    if (isShowPrice) {
        self.goodsPriceLabel.hidden=NO;
        self.stockNumLabel.hidden=NO;
        self.stockNumLabel.frame=CGRectMake(self.frame.size.width-110, CGRectGetMaxY(self.addBtn.frame), 100, 17);
        self.goodsPriceLabel.frame=CGRectMake(10, CGRectGetMaxY(self.stockNumLabel.frame), self.frame.size.width-20, 18);
    }else{
        self.goodsPriceLabel.hidden=YES;
        self.stockNumLabel.hidden=NO;
        self.stockNumLabel.frame=CGRectMake(self.frame.size.width-110, CGRectGetMaxY(self.addBtn.frame)+10, 100, 20);
    }
}
@end
