//
//  GoodsDetailWholeSpecView.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
#import "WholeGoodsNoSpecChangeNumView.h"

@protocol GoodsDetailWholeSpecViewDelegate <NSObject>
-(void)clickCloseBtnWithSpecDic:(NSMutableArray *)specDicArray num:(int)num;
-(void)clickBuyBtnWithSpecDic:(NSMutableArray *)specDicArray num:(int)num;
@end

@interface SpecAndNumCommonPriceModel : NSObject
@property(nonatomic,copy)NSString *name,*value,*min_number,*max_number,*title,*max_price,*min_price;//name为英文名字  title为中文名字
@end

@interface GoodsDetailWholeSpecView : UIView
@property(nonatomic,strong)WholeGoodsNoSpecChangeNumView *noSpecchangeNumView;
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,strong)NSMutableArray *specDicArray;
@property(nonatomic,weak)id<GoodsDetailWholeSpecViewDelegate>delegate;
@end
