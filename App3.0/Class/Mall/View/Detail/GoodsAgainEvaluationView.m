//
//  GoodsAgainEvaluationView.m
//  App3.0
//
//  Created by syn on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "GoodsAgainEvaluationView.h"
#import "XSFormatterDate.h"
#import "ReuseImageView.h"
#import "XLPhotoBrowser.h"
#import "GoodsDetailViewController.h"

@interface GoodsAgainEvaluationView()<XLPhotoBrowserDelegate,XLPhotoBrowserDatasource>
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)UILabel *againTipsLabel,*againTimeLabel,*againContentLabel;
@property(nonatomic,strong)NSArray *imgUrlArray;
@end


@implementation GoodsAgainEvaluationView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.lineView=[UILabel new];
        [self addSubview:self.lineView];
        self.lineView.backgroundColor=[UIColor hexFloatColor:@"DDDDDD"];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.height.mas_equalTo(0.5);
            make.top.mas_equalTo(self);
        }];
        self.againTipsLabel=[[UILabel alloc] init];
        self.againTipsLabel.font=[UIFont systemFontOfSize:15];
        self.againTipsLabel.textColor=[UIColor redColor];
        self.againTipsLabel.text=@"追加评论:";
        [self addSubview:self.againTipsLabel];
        
        self.againTimeLabel=[[UILabel alloc] init];
        self.againTimeLabel.textColor=COLOR_999999;
        self.againTimeLabel.font=[UIFont systemFontOfSize:13];
        [self addSubview:self.againTimeLabel];
        
        self.againContentLabel=[[UILabel alloc] init];
        self.againContentLabel.font=[UIFont systemFontOfSize:15];
        self.againContentLabel.numberOfLines=0;
        self.againContentLabel.textColor=[UIColor hexFloatColor:@"333333"];
        [self addSubview:self.againContentLabel];
        [self.againTipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12+35+13);
            make.top.mas_equalTo(12.5);
        }];
        [self.againTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.mas_equalTo(self.againTipsLabel);
        }];
        [self.againContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.againTipsLabel);
            make.top.mas_equalTo(self.againTipsLabel.mas_bottom).mas_offset(12.5);
            make.right.mas_equalTo(-15.5);
        }];
    }
    return self;
}
-(void)setAgainDataDic:(NSDictionary *)againDataDic{
    _againDataDic=againDataDic;
    //清界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[ReuseImageView class]]) {
            [view removeFromSuperview];
        }
    }
    self.againTimeLabel.text=[XSFormatterDate dateAccurateToDayWithTimeIntervalString:againDataDic[@"time"]];
    self.againContentLabel.text=againDataDic[@"content"];
    if ([againDataDic[@"img"] length]) {//有图片
        NSArray *imgArray=[againDataDic[@"img"] componentsSeparatedByString:@","];
        self.imgUrlArray=imgArray;
        CGFloat width=(mainWidth-75-5*4)/5.0;
        UIView *tempView;
        for (int i=0; i<imgArray.count; i++) {
            int line=i/5;
            int column=i%5;
             ReuseImageView *image=[[ReuseImageView alloc] init];
            image.userInteractionEnabled=YES;
            [image getImageWithUrlStr:imgArray[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            image.contentMode = UIViewContentModeScaleAspectFill;
            image.clipsToBounds=YES;//超出裁剪
            [self addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.againTipsLabel.mas_left).mas_offset(column*(width+5));
                make.top.mas_equalTo(self.againContentLabel.mas_bottom).mas_offset(11.5+line*(width+5));
                make.width.height.mas_equalTo(width);
            }];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
            [image addGestureRecognizer:tap];
            image.tag=i;
            tempView=image;
        }
         [self layoutIfNeeded];
        self.height=tempView.mj_y+tempView.mj_size.height;
    }else{
        [self layoutIfNeeded];
        self.height=self.againContentLabel.mj_y+self.againContentLabel.mj_size.height;
    }
}
#pragma mark-放大图片
-(void)enLarge:(UITapGestureRecognizer *)tap{
    NSInteger imageTag = tap.view.tag;
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:self.imgUrlArray currentImageIndex:imageTag pageControlHidden:YES];
    [browser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    browser.isCusctomAdd = YES;
    browser.delegate = self;
    [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
    [self.currentVC.tabBarController.view addSubview:browser];
}
-(void)photoBrowserDismiss{
    if ([self.currentVC isKindOfClass:[GoodsDetailViewController class]]) {
        [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    }else{
        [self.currentVC.navigationController setNavigationBarHidden:NO animated:NO];
    }
    [self.currentVC.tabBarController.tabBar setHidden:YES];
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

@end
