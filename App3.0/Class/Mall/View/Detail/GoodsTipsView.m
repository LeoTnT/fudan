//
//  GoodsTipsView.m
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsTipsView.h"

@implementation GoodsTipsView
-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        self.backgroundColor=[UIColor colorWithRed:80/255.0 green:80/255.0 blue:80/255.0 alpha:1];
        //小三角
        UIImageView *trangle=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bounds)-20, -8, 10, 8)];
        trangle.image=[UIImage imageNamed:@"mall_detail_trangle_gray"];
        [self addSubview:trangle];
        NSArray *titleAry=@[@"首页",@"购物车",@"商品二维码",Localized(@"share")];
        NSArray *imgAry=@[@"mall_detail_home",@"mall_detail_shoppingCart",@"mall_detail_qrCode",@"mall_detail_share"];
        for(int i=0;i<4;i++){
            @autoreleasepool {
                UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, i*mainHeight/4.0/4.0,  mainWidth/2.7, mainHeight/4.0/4.0)];
                [self addSubview:view];
                UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(10, (mainHeight/4.0/4.0-15)/2.0, 15, 15)];
                img.image=[UIImage imageNamed:imgAry[i]];
                [view addSubview:img];
                UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img.frame)+10, 0, mainWidth/2.7-2*10-20,mainHeight/4.0/4.0 )];
                label.text=titleAry[i];
                label.textColor=[UIColor whiteColor];
                [view addSubview:label];
                label.font=[UIFont systemFontOfSize:15];
                if (i<3) {
                    UIView *cuttingView=[[UIView alloc] initWithFrame:CGRectMake(0,(i+1)*mainHeight/4.0/4.0-0.5, mainWidth/2.7, 0.5)];
                    cuttingView.backgroundColor=[UIColor whiteColor];
                    [self addSubview:cuttingView];
                }
               [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
                   if (self.delegate && [self.delegate respondsToSelector:@selector(clickViewWithIndex:)]) {
                       [self.delegate clickViewWithIndex:i];
                   }
               }]];
            }
        }
    }
    return self;
}
@end
