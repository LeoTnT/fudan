//
//  ShowIfHaveEvaluationCell.h
//  App3.0
//
//  Created by syn on 2017/7/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@interface ShowIfHaveEvaluationCell : UITableViewCell
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@property(nonatomic,strong)UIButton *getMoreEvaBtn;
@end
