//
//  WholeGoodsNoSpecChangeNumView.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WholeGoodsNoSpecChangeNumView.h"

@interface WholeGoodsNoSpecChangeNumView()
@property(nonatomic,strong)UILabel *tipsLabel;
@property(nonatomic,strong)UIButton *addBtn,*subBtn;
@end

@implementation WholeGoodsNoSpecChangeNumView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,10, 80, 25)];
        self.tipsLabel.text=@"购买数量";
        [self addSubview:self.tipsLabel];
        self.tipsLabel.textColor=[UIColor blackColor];
        self.tipsLabel.font=[UIFont systemFontOfSize:15];
        //加号按钮
        self.addBtn=[[UIButton alloc] init];
        [self addSubview: self.addBtn];
        [ self.addBtn addTarget:self action:@selector(addGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.addBtn setImage:[UIImage imageNamed:@"mall_cart_add"] forState:UIControlStateNormal];
        [self.addBtn setImage:[UIImage imageNamed:@"mall_cart_add_h"] forState:UIControlStateDisabled];
        [self.addBtn setBackgroundColor:[UIColor whiteColor]];
        //展示框
        self.goodsNumField=[[UITextField alloc] init];
        self.goodsNumField.font=[UIFont systemFontOfSize:12];
        self.goodsNumField.text=@"0";
        [self.goodsNumField addTarget:self action:@selector(goodsNumChanged:) forControlEvents:UIControlEventEditingChanged];
        self.goodsNumField.backgroundColor=[UIColor whiteColor];
        self.goodsNumField.keyboardType=UIKeyboardTypeDecimalPad;
        self.goodsNumField.textAlignment=NSTextAlignmentCenter;
        [self addSubview:self.goodsNumField];
        //减号按钮
        self.subBtn=[[UIButton alloc] init];
        [self addSubview: self.subBtn];
        [self.subBtn addTarget:self action:@selector(deleteGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub"] forState:UIControlStateNormal];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub_h"] forState:UIControlStateDisabled];
        self.subBtn.enabled=NO;
        [self.subBtn setBackgroundColor:[UIColor whiteColor]];
        self.subBtn.layer.cornerRadius=3;
        self.addBtn.frame=CGRectMake(self.frame.size.width-10-30, 10, 30, 25);
        self.goodsNumField.frame=CGRectMake(CGRectGetMinX(self.addBtn.frame)-40, CGRectGetMinY(self.addBtn.frame), 40, 25);
        self.subBtn.frame=CGRectMake(CGRectGetMinX(self.goodsNumField.frame)-30, CGRectGetMinY(self.addBtn.frame), 30, 25);
    }
    return self;
}
-(void)addGoodsNum{
    int num=[self.goodsNumField.text intValue];
    if(num<self.stockNum){
        num++;
        self.goodsNumField.text=[NSString stringWithFormat:@"%i",num];
        if (self.delegate &&[self.delegate respondsToSelector:@selector(changeGoodsNum:totalMoney:)]) {
            [self.delegate changeGoodsNum:[self.goodsNumField.text intValue] totalMoney:[self.goodsNumField.text intValue]*self.price];
        }
    }
    if ([self.goodsNumField.text intValue]==self.stockNum) {
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
    self.subBtn.enabled=YES;
}
-(void)deleteGoodsNum{
    int num=[self.goodsNumField.text intValue];
    if (num>0) {
        num--;
        self.goodsNumField.text=[NSString stringWithFormat:@"%i",num];
    }
    if (num==0) {
        self.subBtn.enabled=NO;
    }else{
        self.subBtn.enabled=YES;
    }
    if (self.delegate &&[self.delegate respondsToSelector:@selector(changeGoodsNum:totalMoney:)]) {
        [self.delegate changeGoodsNum:[self.goodsNumField.text intValue] totalMoney:[self.goodsNumField.text intValue]*self.price];
    }
}
-(void)setPrice:(CGFloat)price{
    _price=price;
}
-(void)setStockNum:(int)stockNum{
    _stockNum=stockNum;
    if (stockNum==0) {
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
}
-(void)goodsNumChanged:(UITextField *)field{
    if ([field.text intValue]>self.stockNum) {
        field.text=[NSString stringWithFormat:@"%i",self.stockNum];
    }
    if([field.text intValue]>0){
        self.subBtn.enabled=YES;
    }else{
        self.subBtn.enabled=NO;
    }
    if (field.text.intValue<self.stockNum) {
        self.addBtn.enabled=YES;
    }else{
        self.addBtn.enabled=NO;
    }
    if (self.delegate &&[self.delegate respondsToSelector:@selector(changeGoodsNum:totalMoney:)]) {
        [self.delegate changeGoodsNum:[self.goodsNumField.text intValue] totalMoney:[self.goodsNumField.text intValue]*self.price];
    }
}
@end
