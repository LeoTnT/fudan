//
//  GoodsDetailSellerInfoCell.h
//  App3.0
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@protocol  GoodsDetailSellerInfoCellDelegate<NSObject>
-(void)clickStoreName;
-(void)clickAllGoods;
-(void)clickNewGoods;
-(void)clickContactCustomer;
-(void)clickGoStore;
@end

@interface GoodsDetailSellerInfoCell : UITableViewCell
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@property(nonatomic,strong)GoodsStoreStatisticsModel *statisticsModel;
@property(nonatomic,weak)id<GoodsDetailSellerInfoCellDelegate>delegate;
@end
