//
//  GoodsDetailTopInfoCell.h
//  App3.0
//
//  Created by mac on 2017/4/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@protocol GoodsDetailTopInfoCellDelegate <NSObject>
-(void)share;
@end

@interface GoodsDetailTopInfoCell : UITableViewCell
@property(nonatomic,assign)GoodSellType goodSellType;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,strong)GoodsDetailInfo *goodsDetailInfo;
@property(nonatomic,strong)GroupBuyDetailModel *groupBuyDetailInfo;
@property(nonatomic,weak)id <GoodsDetailTopInfoCellDelegate>delegate;
@end
