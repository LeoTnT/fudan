//
//  GoodsDetailSpecView.m
//  App3.0
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailSpecView.h"
#import "DWQSelectAttributes.h"
#import "UUImageAvatarBrowser.h"

@interface GoodsDetailSpecView()<SelectAttributesDelegate>
@property(nonatomic,strong)UIImageView *goodsImg;
@property(nonatomic,strong)UILabel *goodsPriceLabel;
@property(nonatomic,strong)UILabel *stockNumLabel,*haveChoosedLabel;
@property(nonatomic,strong) UIView *cuttingView;
@property(nonatomic,strong) UILabel *numLabel;
@property(nonatomic,copy)NSString *currentPrice;
@property(nonatomic,strong)NSArray *specArray;
@property(nonatomic,strong)NSArray *specValueArray;//二维数组
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *pro_extId;
@property(nonatomic,strong)CartProductListParser *cartGoodsParser;
@end
@implementation GoodsDetailSpecView
-(instancetype)initWithProductParser:(CartProductListParser *)parser{
    GoodsDetailSpecView *specView=[[GoodsDetailSpecView alloc] init];
    specView.cartGoodsParser=parser;
    //隐藏
    specView.numLabel.hidden=YES;
    specView.haveChoosedLabel.hidden=YES;
    specView.remainNumLabel.hidden=YES;
    specView.subBtn.hidden=YES;
    specView.goodsNumField.hidden=YES;
    specView.addBtn.hidden=YES;
    specView.specDic=[NSMutableDictionary dictionary];
    specView.confirmBtn.hidden=NO;
    specView.currentStockNum=parser.stock;
    [specView.goodsImg getImageWithUrlStr:parser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    specView.goodsPriceLabel.text=[NSString stringWithFormat:@"¥%@",parser.sell_price?parser.sell_price:@"0.00"];
    specView.stockNumLabel.text=[NSString stringWithFormat:@"库存：%@件",specView.currentStockNum?specView.currentStockNum:@"0"];
    NSMutableArray *tempSpecArray=[NSMutableArray array];
    NSMutableArray *tempSpecValueArray=[NSMutableArray array];
    for (int i=0; i<parser.spec.count; i++) {
        SpecItem *item=parser.spec[i];
        [tempSpecArray addObject:item];
        NSMutableArray *tempArray=[NSMutableArray array];
        for (int j=0; j<item.spec_value.count; j++) {
            Spec_valueItem *item1=item.spec_value[j];
            [tempArray addObject:item1];
        }
        [tempSpecValueArray addObject:tempArray];
    }
    specView.specArray=tempSpecArray;
    specView.specValueArray=tempSpecValueArray;
    CGFloat maxY = 0;
    CGFloat height = 0;
    for (int i = 0; i < specView.specArray.count; i ++)
    {
        DWQSelectAttributes *selectAttributes=[[DWQSelectAttributes alloc] initWithSpec:specView.specArray[i] valueArray:specView.specValueArray[i] andFrame:CGRectMake(0, maxY, mainWidth, 40)];
        maxY = CGRectGetMaxY(selectAttributes.frame);
        height +=selectAttributes.dwq_height;
        selectAttributes.delegate=specView;
        [specView.specScroll addSubview:selectAttributes];
    }
    if (height>200) {
        specView.specScroll.frame=CGRectMake(0, CGRectGetMaxY(specView.cuttingView.frame), mainWidth, 200);
        specView.specScroll.contentSize=CGSizeMake(0, height);
    }else{
        specView.specScroll.frame=CGRectMake(0, CGRectGetMaxY(specView.cuttingView.frame), mainWidth, height);
    }
    specView.confirmBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(specView.specScroll.frame), mainWidth, 40)];
    [specView.confirmBtn setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
    [specView addSubview:specView.confirmBtn];
    [specView.confirmBtn addTarget:specView action:@selector(clickConfirmBtn) forControlEvents:UIControlEventTouchUpInside];
    specView.height=CGRectGetMaxY(specView.confirmBtn.frame);
    //判断库存
    if ([specView.currentStockNum integerValue]) {
        specView.confirmBtn.backgroundColor=[UIColor hexFloatColor:@"026EFE"];
        specView.confirmBtn.enabled=YES;
    }else{
         specView.confirmBtn.backgroundColor=[UIColor colorWithRed:189/255.0 green:188/255.0 blue:188/255.0 alpha:1];
        specView.confirmBtn.enabled=NO;
    }
    //如果有单个规格  直接赋值
    for (SpecItem *item in parser.spec) {
        if (item.spec_value.count==1) {
            Spec_valueItem *valueItem=item.spec_value.firstObject;
            [specView.specDic setValue:[NSString stringWithFormat:@"%@",valueItem.spec_value_id] forKey:item.spec_id];
        }
    }
    return specView;
}
-(instancetype)init{
    if (self=[super init]) {
        self.backgroundColor=[UIColor whiteColor];
        CGFloat space=10;
        
        //商品图片
        self.goodsImg=[[UIImageView alloc] initWithFrame:CGRectMake(space, -mainWidth*0.06, mainWidth*0.31, mainWidth*0.31)];
        self.goodsImg.contentMode = UIViewContentModeScaleAspectFit;
        self.goodsImg.layer.cornerRadius=5;
        self.goodsImg.layer.borderWidth=1.5;
        self.goodsImg.layer.borderColor=[UIColor whiteColor].CGColor;
        self.goodsImg.image=[UIImage imageNamed:@"no_pic"];
        self.goodsImg.userInteractionEnabled=YES;
        self.goodsImg.backgroundColor=[UIColor whiteColor];
        [self.goodsImg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enlargePicture)]];
        [self addSubview:self.goodsImg];
        
        //分割线
        self.cuttingView=[[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.goodsImg.frame)+15, mainWidth-10, 0.5)];
        self.cuttingView.backgroundColor=[UIColor hexFloatColor:@"DDDDDD"];
        [self addSubview: self.cuttingView];
        
        //价格
        self.goodsPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImg.frame)+13, 30, mainWidth-CGRectGetMaxX(self.goodsImg.frame)-26, 20)];
        self.goodsPriceLabel.textColor=mainColor;
        self.goodsPriceLabel.font=[UIFont systemFontOfSize:19];
        [self addSubview:self.goodsPriceLabel];
        
        //库存
        self.stockNumLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImg.frame)+13, CGRectGetMaxY(self.goodsPriceLabel.frame)+7, mainWidth-CGRectGetMaxX(self.goodsImg.frame)-26,  15)];
        self.stockNumLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview: self.stockNumLabel];
        self.haveChoosedLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImg.frame)+13, CGRectGetMaxY(self.stockNumLabel.frame)+7, mainWidth-CGRectGetMaxX(self.goodsImg.frame)-26,  15)];
        self.haveChoosedLabel.textColor=COLOR_666666;
        self.haveChoosedLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview: self.haveChoosedLabel];
        
        //关闭  透明按钮好关闭
        UIImageView *closeImage=[[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-space-20, space, 20, 20)];
        closeImage.image=[UIImage imageNamed:@"mall_detail_spec_close"];
        [self addSubview:closeImage];
        UIButton *clearCloseBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-40, 0, 40, 40)];
        [clearCloseBtn setBackgroundColor:[UIColor clearColor]];
        [self addSubview:clearCloseBtn];
        [clearCloseBtn addTarget:self action:@selector(closeSpecView) forControlEvents:UIControlEventTouchUpInside];
        self.height=CGRectGetMaxY(self.cuttingView.frame);
        self.specScroll=[[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.cuttingView.frame), mainWidth, 0)];
        [self addSubview:self.specScroll];
        
        //数量
        self.numLabel=[[UILabel alloc] init];
        self.numLabel.text=@"购买数量";
        self.numLabel.font=[UIFont systemFontOfSize:13];
        [self.specScroll addSubview:self.numLabel];
        
        //分割线
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.numLabel.frame), mainWidth-10, 0.5)];
        line.backgroundColor=[UIColor hexFloatColor:@"DDDDDD"];
        [self.specScroll addSubview:line];
        
        //加号按钮
        self.addBtn=[[UIButton alloc] init];
        [self.specScroll addSubview: self.addBtn];
        [ self.addBtn addTarget:self action:@selector(addGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.addBtn setBackgroundImage:[UIImage imageNamed:@"mall_cart_add"] forState:UIControlStateNormal];
        [self.addBtn setBackgroundImage:[UIImage imageNamed:@"mall_cart_add_h"] forState:UIControlStateDisabled];
        
        //展示框
        self.goodsNumField=[[UITextField alloc] init];
        self.goodsNumField.font=[UIFont systemFontOfSize:15];
        self.goodsNumField.backgroundColor=[UIColor hexFloatColor:@"F5F5F5"];
        self.goodsNumField.keyboardType=UIKeyboardTypeNumberPad;
        self.goodsNumField.textAlignment=NSTextAlignmentCenter;
        [self.goodsNumField addTarget:self action:@selector(checkGoodsIfRemain) forControlEvents:UIControlEventEditingChanged];
        [self.specScroll addSubview:self.goodsNumField];
        
        //减号按钮
        self.subBtn=[[UIButton alloc] init];
        [self.specScroll addSubview: self.subBtn];
        self.subBtn.enabled=NO;
        [self.subBtn addTarget:self action:@selector(deleteGoodsNum) forControlEvents:UIControlEventTouchUpInside];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub"] forState:UIControlStateNormal];
        [self.subBtn setImage:[UIImage imageNamed:@"mall_cart_sub_h"] forState:UIControlStateDisabled];
        
        self.addShoppingCartBtn=[[UIButton alloc] init];
        [self.addShoppingCartBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
        [self addSubview:self.addShoppingCartBtn];
        [self.addShoppingCartBtn addTarget:self action:@selector(addShoppingCart) forControlEvents:UIControlEventTouchUpInside];
        self.addShoppingCartBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.addShoppingCartBtn setBackgroundColor:[UIColor hexFloatColor:@"3F8EF7"]];
        
        self.buyNowBtn=[[UIButton alloc] init];
        [self.buyNowBtn setTitle:@"立即购买" forState:UIControlStateNormal];
        self.buyNowBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.buyNowBtn setBackgroundColor:[UIColor hexFloatColor:@"026EFE"]];
        [self addSubview: self.buyNowBtn];
        [self.buyNowBtn addTarget:self action:@selector(buyNow) forControlEvents:UIControlEventTouchUpInside];
        
        self.confirmBtn=[[UIButton alloc] init];
        [self.confirmBtn setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        [self addSubview:self.confirmBtn];
        [self.confirmBtn addTarget:self action:@selector(clickConfirmBtn) forControlEvents:UIControlEventTouchUpInside];
        self.confirmBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.confirmBtn setBackgroundColor:[UIColor hexFloatColor:@"026EFE"]];
    }
    return self;
}
-(void)setDetailInfo:(GoodsDetailInfo *)detailInfo{
    _detailInfo=detailInfo;
    //清空之前新增的view
    for (UIView *view in self.specScroll.subviews) {
        if ([view isKindOfClass:[DWQSelectAttributes class]] || view.tag==1000) {
            [view removeFromSuperview];
        }
    }
    self.specDic=[NSMutableDictionary dictionary];
    self.currentStockNum=self.detailInfo.productInfo.stock;
    self.currentPrice=self.detailInfo.productInfo.sell_price;
    if (self.goodSellType == GoodSellGroupBuy || self.goodSellType == GoodSellSecondKill) {
        self.currentPrice=self.detailInfo.productInfo.share_sell_price;
    }
    self.goodsNum=@"1";
    [self.goodsImg getImageWithUrlStr:self.detailInfo.productInfo.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.goodsPriceLabel.text=[NSString stringWithFormat:@"¥%@",self.currentPrice?self.currentPrice:@"0.00"];
    self.stockNumLabel.text=[NSString stringWithFormat:@"库存%@件",self.currentStockNum?self.currentStockNum:@"0"];
    NSMutableArray *tempSpecArray=[NSMutableArray array];
    NSMutableArray *tempSpecValueArray=[NSMutableArray array];
    for (int i=0; i<detailInfo.productInfo.spec.count; i++) {
        SpecItem *item=detailInfo.productInfo.spec[i];
        [tempSpecArray addObject:item];
        NSMutableArray *tempArray=[NSMutableArray array];
        for (int j=0; j<item.spec_value.count; j++) {
            Spec_valueItem *item1=item.spec_value[j];
            [tempArray addObject:item1];
        }
        [tempSpecValueArray addObject:tempArray];
    }
    self.specArray=tempSpecArray;
    self.specValueArray=tempSpecValueArray;
    CGFloat maxY = 0;
    for (int i = 0; i < self.specArray.count; i ++)
    {
        DWQSelectAttributes *selectAttributes=[[DWQSelectAttributes alloc] initWithSpec:self.specArray[i] valueArray:self.specValueArray[i] andFrame:CGRectMake(0, maxY, mainWidth, 80)];
        maxY = CGRectGetMaxY(selectAttributes.frame);
        selectAttributes.delegate=self;
        [self.specScroll addSubview:selectAttributes];
        //分割线
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,maxY , mainWidth-10, 0.5)];
        line.backgroundColor=[UIColor hexFloatColor:@"DDDDDD"];
        line.tag=1000;
        [self.specScroll addSubview:line];
    }
    self.numLabel.frame=CGRectMake(12,maxY , 100, 56);
    
    self.addBtn.frame=CGRectMake(mainWidth-10-33, maxY+(56-33)/2.0, 33, 30);
    self.goodsNumField.frame=CGRectMake(CGRectGetMinX(self.addBtn.frame)-38, CGRectGetMinY(self.addBtn.frame), 38, 30);
    self.subBtn.frame=CGRectMake(CGRectGetMinX(self.goodsNumField.frame)-33, CGRectGetMinY(self.addBtn.frame), 33, 30);
    if (maxY>300) {
        self.specScroll.frame=CGRectMake(0, CGRectGetMaxY(self.cuttingView.frame), mainWidth, 300+56);
        self.specScroll.contentSize=CGSizeMake(0, 356);
    }else{
        self.specScroll.frame=CGRectMake(0, CGRectGetMaxY(self.cuttingView.frame), mainWidth, maxY+56);
    }
    self.addShoppingCartBtn.frame=CGRectMake(0, CGRectGetMaxY(self.specScroll.frame)+33, mainWidth/2.0, 47);
    self.buyNowBtn.frame=CGRectMake(mainWidth/2.0, CGRectGetMaxY(self.specScroll.frame)+33, mainWidth/2.0, 47);
    
    if (self.goodSellType == GoodSellFillPrice) {
        // 补差价商品不能加入购物车
        self.addShoppingCartBtn.frame = CGRectZero;
        self.buyNowBtn.frame=CGRectMake(0, CGRectGetMaxY(self.specScroll.frame)+33, mainWidth, 47);
    }
    
    self.confirmBtn.frame=CGRectMake(0, CGRectGetMaxY(self.specScroll.frame)+33, mainWidth, 47);
    self.height=CGRectGetMaxY(self.buyNowBtn.frame);
    [self refreshView];
    //如果有单个规格  直接赋值
    for (SpecItem *item in self.detailInfo.productInfo.spec) {
        if (item.spec_value.count==1) {
            Spec_valueItem *valueItem=item.spec_value.firstObject;
            [self.specDic setValue:[NSString stringWithFormat:@"%@",valueItem.spec_value_id] forKey:item.spec_id];
        }
    }
    [self getSpecShowStr];
}
-(void)closeSpecView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickCloseBtnWithSpecDic:goodsNum:stockNum:)]) {
        [self.delegate clickCloseBtnWithSpecDic:self.specDic goodsNum:[self.goodsNumField.text integerValue] stockNum:[self.currentStockNum integerValue]];
    }
}
-(void)clickSpecBtnWithSpecId:(NSString *)specId specValueId:(NSString *)specValueId{
    [self.specDic setValue:[NSString stringWithFormat:@"%@",specValueId] forKey:[NSString stringWithFormat:@"%@",specId]];
    //判断规格是否完善 请求接口
    if ([self ifSpecIsCompleted]) {
        //重新获取数量
        @weakify(self);
        [XSTool showProgressHUDWithView:self];
        [HTTPManager getGoodsInfoWithGoodsID:self.detailInfo?self.detailInfo.productInfo.product_id:self.cartGoodsParser.product_id spec:[self sortSpecDictionaryWithSpecDic:self.specDic] success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self];
            if (state.status) {
                //只有规格完善的时候才会返回真正的库存
                NSArray *arr = dic[@"data"];
                if (arr.count) {//有数据
                    [self.goodsImg getImageWithUrlStr:dic[@"data"][@"spec_colorimg"] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                    self.title=dic[@"data"][@"spec_colorimg_desc"];
                    self.currentStockNum=dic[@"data"][@"stock"];
                    self.goodsNum=@"1";
                    self.currentPrice=dic[@"data"][@"sell_price"];
                    self.stockNumLabel.text=[NSString stringWithFormat:@"库存%@件",self.currentStockNum];
                    self.goodsPriceLabel.text=[NSString stringWithFormat:@"¥%@",self.currentPrice];
                    //获取规格展示字符串
                    [self getSpecShowStr];
                    self.pro_extId = dic[@"data"][@"product_ext_id"];
                    [self refreshView];
                }
            } else {
                [XSTool showToastWithView:self Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self];
            [XSTool showToastWithView:self Text:@"网络异常，请稍后重试"];
        }];
    }
}
-(void)addGoodsNum{
    //获取目前数量
    int num=[self.goodsNumField.text intValue];
    self.goodsNumField.text=[NSString stringWithFormat:@"%d",num+1];
    [self checkGoodsIfRemain];
}
-(void)deleteGoodsNum{
    //获取目前数量
    int num=[self.goodsNumField.text intValue];
    self.goodsNumField.text=[NSString stringWithFormat:@"%d",num-1];
    [self checkGoodsIfRemain];
}
-(void)addShoppingCart{
    if ([self ifSpecIsCompleted]) {
        if ([self.currentStockNum integerValue]>=[self.goodsNumField.text integerValue]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickAddGoodsToShoppingCartBtnWithSpecDic:goodsNum:stockNum:)]) {
                [self.delegate clickAddGoodsToShoppingCartBtnWithSpecDic:self.specDic goodsNum:[self.goodsNumField.text integerValue] stockNum:[self.currentStockNum integerValue]];
            }
        }else{
            [XSTool showToastWithView:self Text:@"库存不足"];
        }
    }else{
        [XSTool showToastWithView:self Text:@"请完善规格"];
    }
    
}
-(void)buyNow{
    //判断规格是否选择完整
    if ([self ifSpecIsCompleted]) {
        if ([self.currentStockNum integerValue]>=[self.goodsNumField.text integerValue]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickBuyNowBtnWithSpecDic:goodsNum:stockNum:)]) {
                [self.delegate clickBuyNowBtnWithSpecDic:self.specDic goodsNum:[self.goodsNumField.text integerValue] stockNum:[self.currentStockNum integerValue]];
            }
        }else{
            [XSTool showToastWithView:self Text:@"库存不足"];
        }
    }else{
        [XSTool showToastWithView:self Text:@"请完善规格"];
    }
    
}
-(void)clickConfirmBtn{
    if ([self ifSpecIsCompleted]) {
        if (self.delegate &&[self.delegate respondsToSelector:@selector(clickConfirmBtnWithNewPeid:goodsPrice:stockNum:specStr:specShowStr:)]) {
            [self.delegate clickConfirmBtnWithNewPeid:self.pro_extId goodsPrice:[self.currentPrice floatValue] stockNum:[self.currentStockNum integerValue] specStr:[self sortSpecDictionaryWithSpecDic:self.specDic] specShowStr:[self getSpecShowStr]];
            return;
        }
        if ([self.currentStockNum integerValue]>=[self.goodsNumField.text integerValue]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickConfirmBtnWithSpecDic:goodsNum:stockNum:)]) {
                [self.delegate clickConfirmBtnWithSpecDic:self.specDic goodsNum:[self.goodsNumField.text integerValue] stockNum:[self.currentStockNum integerValue]];
            }
        }else{
            [XSTool showToastWithView:self Text:@"库存不足"];
        }
    }else{
        [XSTool showToastWithView:self Text:@"请完善规格"];
    }
    
}
-(BOOL)ifSpecIsCompleted{
    return self.specDic.allKeys.count==(self.detailInfo?self.detailInfo.productInfo.spec.count:self.cartGoodsParser.spec.count);
}
//检测加号减号按钮的可用以及剩余数量
-(void)checkGoodsIfRemain{
    //加号按钮
    if([self.goodsNumField.text intValue]>[self.currentStockNum intValue]){
        [XSTool showToastWithView:self Text:@"库存不足"];
    }
    if ([self.goodsNumField.text intValue]>=[self.currentStockNum intValue]) {
        //设为库存值
        self.goodsNumField.text=[NSString stringWithFormat:@"%@",self.currentStockNum];
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
    if ([self.goodsNumField.text intValue]==0) {
        if ([self.currentStockNum intValue]>0) {
            self.goodsNumField.text=@"1";
        }else{
            self.goodsNumField.text=@"0";
        }
    }
    //减号按钮
    if ([self.goodsNumField.text intValue]>1) {
        self.subBtn.enabled=YES;
    }else{
        self.subBtn.enabled=NO;
    }
    if ([self.goodsNumField.text intValue]>=[self.currentStockNum intValue]) {
        self.addBtn.enabled=NO;
    }
    self.goodsNum=self.goodsNumField.text;
    [self setBtnIfEnabled];
}
//对规格字典排序生成字符串  可直接向后台传
-(NSString *)sortSpecDictionaryWithSpecDic:(NSDictionary *) specDic{
    NSString *specString = @"";
    if (specDic.allKeys.count>0) {
        NSMutableString *tempString = [NSMutableString string];
        //顺序为接口返回顺序
        for (int i=0;i<(self.detailInfo?self.detailInfo.productInfo.spec.count:self.cartGoodsParser.spec.count);i++) {
            SpecItem *item =self.detailInfo?self.detailInfo.productInfo.spec[i]:self.cartGoodsParser.spec[i];
            [tempString appendString:[NSString stringWithFormat:@"%@:%@;",item.spec_id,[specDic objectForKey:[NSString stringWithFormat:@"%@",item.spec_id]]]];
        }
        //去除最后的分号
        specString = [tempString substringToIndex:[tempString length]-1];
    }
    return specString;
}
//获取规格选择结束后在界面显示的文字
-(NSAttributedString *)getSpecShowStr{
    if ([self ifSpecIsCompleted]) {
        if (self.detailInfo.productInfo.spec.count) {//有规格
            NSMutableArray *tempArray=[NSMutableArray array];
                for (int i=0;i<(self.detailInfo?self.detailInfo.productInfo.spec.count:self.cartGoodsParser.spec.count);i++) {
                    for (NSString *key in self.specDic.allKeys) {
                        SpecItem *item =self.detailInfo?self.detailInfo.productInfo.spec[i]:self.cartGoodsParser.spec[i];
                        if ([[NSString stringWithFormat:@"%@",item.spec_id] isEqualToString:key]) {
                            for (Spec_valueItem *valueItem in item.spec_value) {
                                if ([[NSString stringWithFormat:@"%@",valueItem.spec_value_id] isEqualToString:[self.specDic valueForKey:key]]) {
                                    [tempArray addObject:[NSString stringWithFormat:@"%@%@%@",@"\"",valueItem.spec_value_name,@"\""]];
                                }
                            }
                        }
                    }
                }
            NSMutableString *tempStr=[NSMutableString string];
            for (int i=0; i<tempArray.count; i++) {
                [tempStr appendString:tempArray[i]];
            }
            self.haveChoosedLabel.text=[NSString stringWithFormat:@"已选择%@",tempStr];
            NSAttributedString *str=[[NSAttributedString alloc] initWithString:self.haveChoosedLabel.text];
            self.showSpecStr=str;
            return str;
        }else{//无规格
            self.haveChoosedLabel.text=@"选择:";
            NSAttributedString *str=[[NSAttributedString alloc] initWithString:self.haveChoosedLabel.text];
            self.showSpecStr=str;
            return str;
        }
    }else{
        NSMutableAttributedString *tempStr=[[NSMutableAttributedString alloc] initWithString:@"选择:"];
        for (int i=0; i<self.detailInfo.productInfo.spec.count; i++) {
            SpecItem *item=[self.detailInfo.productInfo.spec objectAtIndex:i];
            [tempStr appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@,",item.spec_name] attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}]];
        }
        [tempStr deleteCharactersInRange:NSMakeRange(tempStr.length-1, 1)];
        self.haveChoosedLabel.attributedText=tempStr;
        self.showSpecStr=tempStr;
        return tempStr;
    }
}

-(void)refreshView{
    if ([self.currentStockNum integerValue]>0) {
        self.goodsNumField.text=@"1";
    }else{
        self.goodsNumField.text=@"0";
    }
    if ([self.currentStockNum integerValue]>1) {
        self.addBtn.enabled=YES;
    }else{
        self.addBtn.enabled=NO;
    }
    self.subBtn.enabled=NO;
    [self setBtnIfEnabled];
}
-(void)enlargePicture{
    [UUImageAvatarBrowser showImage:self.goodsImg title:self.title];
}
-(void)setBtnIfEnabled{
    if ([self.currentStockNum integerValue]) {
        self.addShoppingCartBtn.backgroundColor=[UIColor hexFloatColor:@"3F8EF7"];
        self.buyNowBtn.backgroundColor=[UIColor hexFloatColor:@"026EFE"];
        self.confirmBtn.backgroundColor=[UIColor hexFloatColor:@"026EFE"];
        self.addShoppingCartBtn.enabled=YES;
        self.buyNowBtn.enabled=YES;
        self.confirmBtn.enabled=YES;
    }else{
        self.addShoppingCartBtn.backgroundColor=[UIColor colorWithRed:189/255.0 green:188/255.0 blue:188/255.0 alpha:1];
        self.buyNowBtn.backgroundColor=[UIColor colorWithRed:189/255.0 green:188/255.0 blue:188/255.0 alpha:1];
        self.confirmBtn.backgroundColor=[UIColor colorWithRed:189/255.0 green:188/255.0 blue:188/255.0 alpha:1];
        self.addShoppingCartBtn.enabled=NO;
        self.buyNowBtn.enabled=NO;
        self.confirmBtn.enabled=NO;
    }
    
    //购物车
    if (self.cartGoodsParser) {
        if ([self.currentStockNum integerValue]) {
            self.confirmBtn.backgroundColor=[UIColor hexFloatColor:@"026EFE"];
            self.confirmBtn.enabled=YES;
        }else{
            self.confirmBtn.backgroundColor=[UIColor colorWithRed:189/255.0 green:188/255.0 blue:188/255.0 alpha:1];
            self.confirmBtn.enabled=NO;
        }
    }
}
-(void)setIsClickedBuyNowBtn:(BOOL)isClickedBuyNowBtn{
    _isClickedBuyNowBtn=isClickedBuyNowBtn;
    if (isClickedBuyNowBtn) {
        self.confirmBtn.hidden=NO;
    }else{
        self.confirmBtn.hidden=YES;
    }
}
@end
