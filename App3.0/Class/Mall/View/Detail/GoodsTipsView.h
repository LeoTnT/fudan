//
//  GoodsTipsView.h
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsTipsViewDelegate <NSObject>
-(void)clickViewWithIndex:(NSInteger)index;
@end
@interface GoodsTipsView : UIView
@property(nonatomic,weak)id <GoodsTipsViewDelegate>delegate;
@end
