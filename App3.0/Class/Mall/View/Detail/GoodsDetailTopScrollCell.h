//
//  GoodsDetailTopScrollCell.h
//  App3.0
//
//  Created by mac on 2017/4/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLPhotoBrowser.h"
@protocol GoodsDetailTopScrollCellDelegate <NSObject>
-(void)hidtipsView;
@end

@interface GoodsDetailTopScrollCell : UITableViewCell<XLPhotoBrowserDelegate>
/**滚动式图数据*/
@property(nonatomic,strong)NSString *imageList;
/**当前控制器*/
@property(nonatomic,strong)UIViewController *currentVC;
@property(nonatomic,weak)id<GoodsDetailTopScrollCellDelegate> delegate;

@end

