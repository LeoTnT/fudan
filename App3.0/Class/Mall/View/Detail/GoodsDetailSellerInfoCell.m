//
//  GoodsDetailSellerInfoCell.m
//  App3.0
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailSellerInfoCell.h"
@interface GoodsDetailSellerInfoCell ()
@property(nonatomic,strong)UIImageView *sellerImg;
@property(nonatomic,strong)UILabel *goodsDescLabel;//商品描述
@property(nonatomic,strong)UILabel *logisticsServiceLabel;//物流服务
@property(nonatomic,strong)UILabel *sendSpeedLabel;//发货速度
@property(nonatomic,strong)UILabel *serviceAttitudeLabel;//服务态度
@property(nonatomic,strong)UILabel *allGoodsNumLabel;
@property(nonatomic,strong)UILabel *goodsNewNumLabel;
@property(nonatomic,strong)UILabel *attentionNumLabel;
@property(nonatomic,strong)UILabel *sellerNameLabel;
@property(nonatomic,strong)UIButton *contactCustomerBtn;
@property(nonatomic,strong)UIButton *goShopBtn;
@property(nonatomic,strong)UIView *allGoodsView,*GoodsNewView;
@end
@implementation GoodsDetailSellerInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.sellerImg=[[UIImageView alloc] init];
        self.sellerImg.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.sellerImg];
        self.sellerImg.image=[UIImage imageNamed:@"no_pic"];
        self.sellerNameLabel=[[UILabel alloc] init];
        self.sellerNameLabel.userInteractionEnabled=YES;
        self.sellerNameLabel.textColor=[UIColor blackColor];
        self.sellerNameLabel.font=[UIFont systemFontOfSize:16];
        [self.contentView addSubview:self.sellerNameLabel];
        self.goodsDescLabel=[[UILabel alloc] init];
        self.goodsDescLabel.font=[UIFont systemFontOfSize:12];
        self.goodsDescLabel.textColor=mainColor;
        [self.contentView addSubview:self.goodsDescLabel];
        self.logisticsServiceLabel=[[UILabel alloc] init];
        self.logisticsServiceLabel.font=[UIFont systemFontOfSize:12];
        self.logisticsServiceLabel.textColor=mainColor;
        [self.contentView addSubview:self.logisticsServiceLabel];
        self.sendSpeedLabel=[[UILabel alloc] init];
        self.sendSpeedLabel.font=[UIFont systemFontOfSize:12];
        self.sendSpeedLabel.textColor=mainColor;
        [self.contentView addSubview:self.sendSpeedLabel];
        self.serviceAttitudeLabel=[[UILabel alloc] init];
        self.serviceAttitudeLabel.font=[UIFont systemFontOfSize:12];
        self.serviceAttitudeLabel.textColor=mainColor;
        [self.contentView addSubview:self.serviceAttitudeLabel];
        self.allGoodsView=[[UIView alloc] init];
        self.allGoodsView.userInteractionEnabled=YES;
        [self.contentView addSubview:self.allGoodsView];
        self.allGoodsNumLabel=[[UILabel alloc] init];
        self.allGoodsNumLabel.font=[UIFont systemFontOfSize:16];
        self.allGoodsNumLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:self.allGoodsNumLabel];
        UILabel *allGoodsLabel=[[UILabel alloc] init];
        allGoodsLabel.text=@"全部宝贝";
        allGoodsLabel.textColor=[UIColor hexFloatColor:@"838383"];
        allGoodsLabel.textAlignment=NSTextAlignmentCenter;
        allGoodsLabel.font=[UIFont systemFontOfSize:12];
        [self.contentView addSubview:allGoodsLabel];
        self.GoodsNewView=[[UIView alloc] init];
        self.GoodsNewView.userInteractionEnabled=YES;
        [self.contentView addSubview:self.GoodsNewView];
        self.goodsNewNumLabel=[[UILabel alloc] init];
        self.goodsNewNumLabel.font=[UIFont systemFontOfSize:16];
        self.goodsNewNumLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:self.goodsNewNumLabel];
        UILabel *goodsNewLabel=[[UILabel alloc] init];
        goodsNewLabel.text=@"上新宝贝";
        goodsNewLabel.textColor=[UIColor hexFloatColor:@"838383"];
        goodsNewLabel.textAlignment=NSTextAlignmentCenter;
        goodsNewLabel.font=[UIFont systemFontOfSize:12];
         [self.contentView addSubview:goodsNewLabel];
        self.attentionNumLabel=[[UILabel alloc] init];
        self.attentionNumLabel.font=[UIFont systemFontOfSize:16];
        self.attentionNumLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:self.attentionNumLabel];
        UILabel *attentionLabel=[[UILabel alloc] init];
        attentionLabel.text=@"关注人数";
        attentionLabel.textColor=[UIColor hexFloatColor:@"838383"];
        attentionLabel.textAlignment=NSTextAlignmentCenter;
        attentionLabel.font=[UIFont systemFontOfSize:12];
        [self.contentView addSubview:attentionLabel];
        //联系客服  进店逛逛
        self.contactCustomerBtn=[[UIButton alloc] init];
        self.contactCustomerBtn.layer.borderWidth=0.5;
        self.contactCustomerBtn.layer.cornerRadius=15;
        self.contactCustomerBtn.layer.borderColor=mainColor.CGColor;
        [self.contactCustomerBtn setImage:[UIImage imageNamed:@"mall_detail_contact_customer"] forState:UIControlStateNormal];
        [self.contactCustomerBtn setTitle:@"联系客服" forState:UIControlStateNormal];
         self.contactCustomerBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [self.contactCustomerBtn setTitleColor:mainColor forState:UIControlStateNormal];
        [self.contentView addSubview:self.contactCustomerBtn];
        self.goShopBtn=[[UIButton alloc] init];
        self.goShopBtn.layer.borderWidth=0.5;
        self.goShopBtn.layer.cornerRadius=15;
        self.goShopBtn.layer.borderColor=mainColor.CGColor;
        [self.goShopBtn setImage:[UIImage imageNamed:@"mall_detail_shop"] forState:UIControlStateNormal];
        [self.goShopBtn setTitle:@"进店逛逛" forState:UIControlStateNormal];
        self.goShopBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [self.goShopBtn setTitleColor:mainColor forState:UIControlStateNormal];
        [self.contentView addSubview:self.goShopBtn];
        [self.sellerImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(45);
            make.top.mas_equalTo(18);
            make.width.height.mas_equalTo(56);
        }];
        [self.sellerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sellerImg.mas_right).mas_offset(10);
            make.top.mas_equalTo(self.sellerImg);
            make.right.mas_equalTo(-10);
        }];
        [self.goodsDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sellerNameLabel);
            make.top.mas_equalTo(self.sellerNameLabel.mas_bottom).mas_offset(9);
        }];
        [self.logisticsServiceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.goodsDescLabel.mas_right).mas_offset(39);
            make.top.mas_equalTo(self.sellerNameLabel.mas_bottom).mas_offset(9);
        }];
        [self.sendSpeedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sellerNameLabel);
            make.top.mas_equalTo(self.goodsDescLabel.mas_bottom).mas_offset(9);
        }];
        [self.serviceAttitudeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.sendSpeedLabel.mas_right).mas_offset(39);
            make.top.mas_equalTo(self.logisticsServiceLabel.mas_bottom).mas_offset(9);
        }];
        [self.allGoodsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(self.sendSpeedLabel.mas_bottom).mas_offset(15);
            make.height.mas_equalTo(50);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [self.GoodsNewView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(mainWidth/3.0);
            make.top.mas_equalTo(self.sendSpeedLabel.mas_bottom).mas_offset(15);
            make.height.mas_equalTo(50);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [self.allGoodsNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sellerImg.mas_bottom).mas_offset(22);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [self.goodsNewNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sellerImg.mas_bottom).mas_offset(22);
            make.left.mas_equalTo(mainWidth/3.0);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [self.attentionNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.sellerImg.mas_bottom).mas_offset(22);
            make.left.mas_equalTo(mainWidth/3.0*2);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [allGoodsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.allGoodsNumLabel.mas_bottom).mas_offset(13);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [goodsNewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.goodsNewNumLabel.mas_bottom).mas_offset(13);
            make.left.mas_equalTo(mainWidth/3.0);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [attentionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.attentionNumLabel.mas_bottom).mas_offset(13);
            make.left.mas_equalTo(mainWidth/3.0*2);
            make.width.mas_equalTo(mainWidth/3.0);
        }];
        [self.contactCustomerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-17);
            make.right.mas_equalTo(self.contentView.mas_centerX).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(93, 30));
        }];
        [self.goShopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-17);
            make.left.mas_equalTo(self.contentView.mas_centerX).mas_offset(15);
            make.size.mas_equalTo(CGSizeMake(93, 30));
        }];
        //分割线
        for (int i=0; i<2; i++) {
            UIView *view=[UIView new];
            [self.contentView addSubview:view];
            view.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                if (i==0) {
                    make.left.mas_equalTo(mainWidth/3.0);
                }else{
                    make.left.mas_equalTo(mainWidth/3.0*2);
                }
                make.top.mas_equalTo(self.sendSpeedLabel.mas_bottom).mas_offset(15);
                make.size.mas_equalTo(CGSizeMake(0.5, 50));
            }];
        }
    }
    return self;
}
-(void)setDetailInfo:(GoodsDetailInfo *)detailInfo{
    _detailInfo=detailInfo;
    [self.sellerImg getImageWithUrlStr:detailInfo.supplyInfo.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.sellerNameLabel.text=detailInfo.supplyInfo.name;
    NSInteger length=@"商品描述：".length;
    //富文本
    NSMutableAttributedString *attri1=  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"商品描述：%@",detailInfo.supplyEvaluation.ms_lev.score?detailInfo.supplyEvaluation.ms_lev.score:@"0.00"]];
    [attri1 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"828282"] range:NSMakeRange(0, length)];
    self.goodsDescLabel.attributedText=attri1;
    NSMutableAttributedString *attri2=  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"物流服务：%@",detailInfo.supplyEvaluation.wl_lev.score?detailInfo.supplyEvaluation.wl_lev.score:@"0.00"]];
    [attri2 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"828282"] range:NSMakeRange(0, length)];
    self.logisticsServiceLabel.attributedText=attri2;
    NSMutableAttributedString *attri3=  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"发货速度：%@",detailInfo.supplyEvaluation.fh_lev.score?detailInfo.supplyEvaluation.fh_lev.score:@"0.00"]];
    [attri3 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"828282"] range:NSMakeRange(0, length)];
    self.sendSpeedLabel.attributedText=attri3;
    NSMutableAttributedString *attri4=  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"服务态度：%@",detailInfo.supplyEvaluation.fw_lev.score?detailInfo.supplyEvaluation.fw_lev.score:@"0.00"]];
    [attri4 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"828282"] range:NSMakeRange(0, length)];
    self.serviceAttitudeLabel.attributedText=attri4;
    [self.sellerNameLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickStoreName)]) {
            [self.delegate clickStoreName];
        }
    }]];
    [self.allGoodsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickAllGoods)]) {
            [self.delegate clickAllGoods];
        }
    }]];
    [self.GoodsNewView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickNewGoods)]) {
            [self.delegate clickNewGoods];
        }
    }]];
    [self.contactCustomerBtn addTarget:self action:@selector(contact) forControlEvents:UIControlEventTouchUpInside];
    [self.goShopBtn addTarget:self action:@selector(goShop) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)setStatisticsModel:(GoodsStoreStatisticsModel *)statisticsModel{
    _statisticsModel=statisticsModel;
    self.allGoodsNumLabel.text=[NSString stringWithFormat:@"%@",statisticsModel.productNumAll?statisticsModel.productNumAll:@"0"];
    self.goodsNewNumLabel.text=[NSString stringWithFormat:@"%@",statisticsModel.productNumNew?statisticsModel.productNumNew:@"0"];
    self.attentionNumLabel.text=[NSString stringWithFormat:@"%@",statisticsModel.favNumSupply?statisticsModel.favNumSupply:@"0"];
}
-(void)contact{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickContactCustomer)]) {
        [self.delegate clickContactCustomer];
    }
}
-(void)goShop{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickGoStore)]) {
        [self.delegate clickGoStore];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
