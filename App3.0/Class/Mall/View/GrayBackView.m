//
//  GrayBackView.m
//  App3.0
//
//  Created by syn on 2017/6/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GrayBackView.h"

@implementation GrayBackView
-(instancetype)init{
    if (self=[super init]) {
        self.frame=CGRectMake(0, 0, mainWidth, mainHeight);
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        self.hidden=YES;
        @weakify(self);
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(grayViewClicked)]) {
                [self.delegate grayViewClicked];
            }
        }]];
    }
    return self;
}

@end
