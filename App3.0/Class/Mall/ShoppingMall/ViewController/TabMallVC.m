//
//  xxxxViewController.m
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//


#import "XXCollectionViewCell.h"
#import "TabMallMenuCell.h"
#import "TabMallADCell.h"
#import "TabMallCategoryCell.h"
#import "GussYoulikeCell.h"
#import "S_StoreInformation.h"
#import "ADDetailWebViewController.h"
#import "GoodsDetailViewController.h"

#import "TabMallCategoryImgView.h"
#import "ProductListViewController.h"
#import "TabMallGussYouLikeGoodsView.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"
#import "GoodsTypeViewController.h"
#import "SearchVC.h"
#import "ScanViewController.h"
#import "ADListViewController.h"

#import "XSShoppingWebViewController.h"//详情webView
#import "MallProductCountCell.h"
#import "MallFloorHeaderView.h"

@interface TabMallVC ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
TabMallMenuCellDelegate,
SDCycleScrollViewDelegate,
SectionHeaderDelegate,
UIScrollViewDelegate,
UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger noticeIndex;
@property (nonatomic, strong) NSMutableArray *gussYouLikeGoodsArray;
@property (nonatomic, strong) NSMutableArray *mallMenuArray;
@property (nonatomic, strong) NSMutableArray *topNoticeItemArray;
@property (nonatomic, strong) ProductParser *productParser;
@property (nonatomic, strong) SDCycleScrollView *shopAdvier;// 顶部录播图
@property (nonatomic, strong) UIView *adverInfor;// 滚动广告

@property (nonatomic, strong) TabMallCategoryCell *section_Two_cell;
@property (nonatomic, strong) GussYoulikeCell *secntion_Three_Header;

@property (nonatomic ,strong) NSMutableArray *adverDataSource;
@property (nonatomic, strong) UIButton *goTopBtn;
@property (nonatomic, strong) UIView *navBgView;

@property (nonatomic, strong) MallFloorModel *floorModel;
@end


@implementation TabMallVC
{
    CGFloat itemHeight;
    NSInteger page;
    AdvertisementModel * adverItemModel;
    UIView *footerScrollerView;
    NSMutableArray *adverArr;
    BOOL isLoad;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.fd_prefersNavigationBarHidden = YES;
    
    [self creatCollectionView];
    [self naviSubviews];
    [self setRefreshStyle];
    [self.collectionView.mj_header beginRefreshing];
    [self checkCurrentNetworkStatus];
}

#pragma mark 检测是否有网，无网则提示刷新
- (void)checkCurrentNetworkStatus {
    if (![XSHTTPManager isHaveNetwork]) {
        UIAlertController * netStatusAlert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"当前无网络，请刷新" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:@"刷新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self setRefreshStyle];
            [self.collectionView.mj_header beginRefreshing];
        }];
        [netStatusAlert addAction:action1];
        [self presentViewController:netStatusAlert animated:YES completion:nil];
    }
}

- (void) requestData {
    
    [self requestMallBannerAndMenuData];
    dispatch_group_t requesyGroup = dispatch_group_create();
    dispatch_queue_t requestQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_group_async(requesyGroup, requestQueue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });

        [self requestMallNoticeData];
    });
    dispatch_group_async(requesyGroup, requestQueue, ^{

        [self requestSection_Two_HeaderInfor];
        [self getMallNewProducts];
        [self getMallFloorData];
    });
    dispatch_group_notify(requesyGroup, requestQueue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRefresh];
            [self.collectionView reloadData];
        });

        [self changeGoodsList];

    });
   
}


- (UIButton *)goTopBtn {
    if (!_goTopBtn) {
        //回到顶部按钮
        _goTopBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-10-44, mainHeight-120, 44, 44)];
        [_goTopBtn setBackgroundImage:[UIImage imageNamed:@"mall_go_top_new"] forState:UIControlStateNormal];
        _goTopBtn.adjustsImageWhenHighlighted=NO;
        _goTopBtn.backgroundColor=[UIColor clearColor];
        [self.view addSubview:_goTopBtn];
        _goTopBtn.hidden = YES;
        [_goTopBtn addTarget:self action:@selector(goTop) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goTopBtn;
}

- (void) goTop{
    [self.collectionView setContentOffset:CGPointZero];
}


- (void) naviSubviews {
    CGFloat navHeight = kStatusBarAndNavigationBarHeight;
    self.navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, navHeight)];
    self.navBgView.backgroundColor = mainColor;
    self.navBgView.userInteractionEnabled = YES;
    [self.view addSubview:self.navBgView];
    self.navBgView.alpha = 0.01;

    //输入搜索
    UIView *searchField = [[UIView alloc] init];
    searchField.backgroundColor = BG_COLOR;
    searchField.layer.cornerRadius = 15;
    searchField.layer.masksToBounds = YES;
    [self.view addSubview:searchField];
    
    //添加手势搜索
    [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchGoods)]];
    [searchField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(48);
        make.right.mas_equalTo(self.view).offset(-48);
        make.top.mas_equalTo(self.view).offset(kStatusBarHeight+8);
        make.height.mas_equalTo(28);
    }];

    //搜索图片
    UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(12, (28-13)/2.0, 13, 13)];
    searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
    searchImg.userInteractionEnabled = YES;
    [searchField addSubview:searchImg];
    
    //搜索文字
    UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(28-20)/2.0 , 100, 20)];
    searchLabel.userInteractionEnabled = YES;
    searchLabel.text=@"搜索宝贝";
    searchLabel.font=[UIFont systemFontOfSize:12];
    searchLabel.textColor=mainGrayColor;
    [searchField addSubview:searchLabel];
    
    //购物车按钮
    UIButton *shopCartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shopCartBtn setImage:[UIImage imageNamed:@"mall_detail_shoppingCart"] forState:UIControlStateNormal];
    [shopCartBtn addTarget:self action:@selector(goShoppingCart) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shopCartBtn];
    [shopCartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.left.mas_equalTo(searchField.mas_right);
        make.right.mas_equalTo(self.view);
        
    }];

    UIImageView *mallLogo = [UIImageView new];
    [mallLogo getImageWithUrlStr:[AppConfigManager ShardInstnce].appConfig.logo_mobile andDefaultImage:[UIImage imageNamed:@"mall_nav_logo"]];
    [self.view addSubview:mallLogo];
    [mallLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.left.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
//    UIButton *QRCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [QRCodeBtn setImage:[UIImage imageNamed:@"mall_nav_logo"] forState:UIControlStateNormal];
//    [self.view addSubview:QRCodeBtn];
//    [QRCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(searchField);
//        make.left.mas_equalTo(self.view);
//        make.right.mas_equalTo(searchField.mas_left);
//    }];
}

- (void)qrCodeAction {

    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    ScanViewController *scanVC = [[ScanViewController alloc] init];
    scanVC.style = style;
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
        //设置扫码区域参数设置
}


-(void)searchGoods{
    NSLog(@"搜索商品");
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
    [self.navigationController pushViewController:search animated:YES];
}

- (void) goShoppingCart {
    [self xs_pushViewController:[self getNameWithClass:@"CartVC"]];
//    [self.navigationController pushViewController:[self getNameWithClass:@"CartVC"] animated:YES];
}


- (void) setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
            [self requestData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.collectionView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getMoreGoodsListInfo];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    self.collectionView.mj_footer=footer;
}


// 广告透视图
NSString *const Section_One_Header = @"Section_One_Header";
NSString *const Section_One_Cell = @"Section_One_Cell";
NSString *const Section_one_Footer = @"Section_one_Hooter";
NSString *const Section_Two_Header = @"Section_Two_Header";
NSString *const Section_Two_Cell = @"Section_Two_Cell";

- (void) creatCollectionView {
    
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 5;
    flowLayout.minimumInteritemSpacing = 2.5;
    _collectionView= [[UICollectionView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight-kStatusBarAndNavigationBarHeight-kTabbarHeight) collectionViewLayout:flowLayout];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [_collectionView registerClass:[OneItem class] forCellWithReuseIdentifier:Section_One_Cell];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:Section_One_Header];
    [_collectionView registerClass:[UIView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:Section_one_Footer];
    
    [_collectionView registerClass:[TabMallADCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:Section_Two_Header];
    [_collectionView registerClass:[TabMallCategoryCell class] forCellWithReuseIdentifier:Section_Two_Cell];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.userInteractionEnabled = YES;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:_collectionView];
    
    
}


-(SDCycleScrollView *)shopAdvier {
    if (!_shopAdvier) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 198) delegate:self placeholderImage:nil];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        cycleScrollView.hidesForSinglePage = YES;
        cycleScrollView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
        cycleScrollView.pageDotColor = [UIColor whiteColor];
        cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        cycleScrollView.placeholderImage = [UIImage imageNamed:@"no_pic"];
        cycleScrollView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        _shopAdvier =  cycleScrollView;
        
    }
    return _shopAdvier;
}

- (void) goNoticeDetail:(NSInteger )tag{
    
    if (self.topNoticeItemArray.count ==0) return;
    
    //公告列表
    ADListViewController *controller = [[ADListViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
    
}

#pragma  mark --
#pragma  mark --  TabMallCategoryCellDelegate


- (void) didSelectedCategoryImgView:(TabMallCategory_brandItem *)itemDataSource {
    
    if (itemDataSource.brand_id) {
        ProductListViewController *pVC = [ProductListViewController new];
        ProductModel *model = [ProductModel new];
        model.brand_id=itemDataSource.brand_id;
        pVC.model = model;
        [self.navigationController pushViewController:pVC animated:YES];
    }else{
        
        [MBProgressHUD showMessage:@"品牌不存在" view:self.view];
    }
}


#pragma mark - - - - - SectionHeaderDelegate

- (void)didSelectedItem:(UITapGestureRecognizer *)tap {
    
    NSInteger index=tap.view.tag;
    
    TabMallADItem *selectedItem;
    switch (index) {
        case 1:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_left_up[0];
            break;
        case 2:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_right_up[0];
            break;
        case 3:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_left_down[0];
            break;
        case 4:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_right_down[0];
            break;
            
        default:
            break;
    }
    [self goDifferentADDetail:selectedItem];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    [self goDifferentADDetail:self.adverDataSource[index]];
}

-(void)goDifferentADDetail:(TabMallADItem *)item{
    TabMallMenuItem *mItem = [TabMallMenuItem new];
    mItem.link_in = item.link_in;
    mItem.link_objid = item.link_objid;
    mItem.url = item.url;
    mItem.name = item.name;
    [self didSelectedMenuCell:mItem];
}

-(void)goDetailWebViewWithUrlStr:(NSString *)urlStr title:(NSString *)title placeTitle:(NSString *)placeTitle{
    ADDetailWebViewController *adWeb = [[ADDetailWebViewController alloc] init];
    adWeb.urlStr = urlStr;
    adWeb.navigationItem.title = title.length?title:placeTitle;
    [self.navigationController pushViewController:adWeb animated:YES];
}


-(void)goGoodsDetailWithGoodsId:(NSString *)goodsId{
    if (goodsId.length) {
        
        GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
        goodsDetailVC.goodsID=goodsId;
        [self.navigationController pushViewController:goodsDetailVC animated:YES];
    }else{
        [XSTool showToastWithView:self.view Text:@"商品不存在"];
    }
}


#pragma mark 类目等选择
/**
 选择顶部item
 */
- (void)didSelectedMenuCell:(TabMallMenuItem *)model {
    TabMallMenuItem *item = model;
    // 如果url不为空并且是完整外链，则直接跳转网页
    if (!isEmptyString(item.url) && [item.url hasPrefix:@"http"]) {
        XSShoppingWebViewController * shoppingWebVC = [[XSShoppingWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@?device=%@",item.url,gui];
        shoppingWebVC.titleString = item.name;
        [self.navigationController pushViewController:shoppingWebVC animated:YES];
        return;
    }
    
    // 否则根据link_in判断
    if ([item.link_in isEqualToString:@"store"]) {
        // 店铺首页
        if (item.link_objid.length) {
            S_StoreInformation *infoVC=[[S_StoreInformation alloc] init];
            infoVC.storeInfor=item.link_objid;
            
            [self.navigationController pushViewController:infoVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"店铺不存在"];
        }

    } else if ([item.link_in isEqualToString:@"storelists"]) {
        // 店铺列表
        [self.navigationController pushViewController:[self getNameWithClass:@"S_StoreCollectionView"] animated:YES];
    } else if ([item.link_in isEqualToString:@"product"]) {
        // 商品首页
        [self goGoodsDetailWithGoodsId:item.link_objid];
        
    } else if ([item.link_in isEqualToString:@"productlists"]) {
        // 商品列表
        NSMutableDictionary *paraDic=[NSMutableDictionary dictionary];
        NSArray *listArray=[item.link_objid componentsSeparatedByString:@";"];
        for (NSString *listStr in listArray) {
            NSArray *tempArray=[listStr componentsSeparatedByString:@"="];
            [paraDic setValue:[tempArray lastObject] forKey:[tempArray firstObject]];
        }
        ProductListViewController *listVC = [[ProductListViewController alloc] init];
        listVC.model = [ProductModel mj_objectWithKeyValues:paraDic];
        [self.navigationController pushViewController:listVC animated:YES];
    } else if ([item.link_in isEqualToString:@"category"]) {
        // 分类
        GoodsTypeViewController *controller = [[GoodsTypeViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([item.link_in isEqualToString:@"brand"]) {
        // 品牌
        [self.navigationController pushViewController:[self getNameWithClass:@"BrandListViewController"] animated:YES];
    } else if ([item.link_in isEqualToString:@"cart"]) {
        // 购物车
        [self xs_pushViewController:[self getNameWithClass:@"CartVC"]];
    } else if ([item.link_in isEqualToString:@"order"]) {
        // 我的订单
        [self xs_pushViewController:[self getNameWithClass:@"FormsVC"]];
    } else if ([item.link_in isEqualToString:@"webpage"]) {
        // 活动页
        ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@?guid=%@",item.url,gui];
        adWeb.navigationItem.title = item.name;
        [self.navigationController pushViewController:adWeb animated:YES];
    } else if ([item.link_in isEqualToString:@"productlistsbook"]) {
        XSBaseWKWebViewController * shoppingWebVC = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (gui.length > 0) {
            
        } else {
            gui = @"app";
        }
        shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/booking/list?device=%@",ImageBaseUrl,gui];
        shoppingWebVC.urlType = WKWebViewURLPresell;
        shoppingWebVC.navigationItem.title = item.name;
        [self xs_pushViewController:shoppingWebVC];
    } else if ([item.link_in isEqualToString:@"collect"]) {
        XSBaseWKWebViewController * shoppingWebVC = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (gui.length > 0) {
            
        } else {
            gui = @"app";
        }
        shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/crowdfund/list?device=%@",ImageBaseUrl,gui];
        shoppingWebVC.urlType = WKWebViewURLPresell;
        shoppingWebVC.navigationItem.title = item.name;
        [self xs_pushViewController:shoppingWebVC];
    } else if ([item.link_in isEqualToString:@"offlinestore"] || [item.link_in isEqualToString:@"offline"] || [item.link_in isEqualToString:@"offlinelists"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"OfflineShopHomeViewController"] animated:YES];
    } else if ([item.link_in isEqualToString:@"seckill"]) {
        // 秒杀
        [self xs_pushViewController:[self getNameWithClass:@"SecondKillViewController"]];
    } else if ([item.link_in isEqualToString:@"groupbuy"]) {
        // 拼团 对接Vue
        //        [self xs_pushViewController:[self getNameWithClass:@"GroupBuyListViewController"]];
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/groupbuy?device=%@",ImageBaseUrl,gui];
        adWeb.urlType = WKWebViewURLGroupBuy;
        adWeb.navigationItem.title = @"拼团";
        [self xs_pushViewController:adWeb];
    } else{
        [self goDetailWebViewWithUrlStr:item.url title:item.name placeTitle:@"广告"];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (isLoad) {
        @weakify(self);
        [RACObserve(self, self.adverDataSource) subscribeNext:^(NSArray *arr) {
            @strongify(self);
            if ((arr.count ==0 || !arr)) {
                [self requestMallBannerAndMenuData];
            }
        }];
    }
}

- (UIViewController * ) getNameWithClass:(NSString *)className {
    
    Class cla = NSClassFromString(className);
    
    return [cla new];
}

#pragma mark - request
- (void)requestMallBannerAndMenuData {
    
    RACSignal *signal = [XSHTTPManager rac_POSTURL:Url_GetMallADInfo params:@{@"flag_str":@"mobile_index_slide"}];
    [signal subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSMutableArray *imageUrlDataSource = [NSMutableArray array];
            NSArray *aaa = state.data[@"mobile_index_slide"];
            [aaa.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                TabMallADItem *item=[TabMallADItem mj_objectWithKeyValues:x];
                [arr addObject:item];
                [imageUrlDataSource addObject:item.image];
            }completed:^{
                self.adverDataSource = arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.shopAdvier.imageURLStringsGroup = imageUrlDataSource;
                });
                
            }];
        }
        
        
    }];
    
   NSMutableArray *itemDataSource = [UserInstance ShardInstnce].mallMenuArray;
    
    if (itemDataSource.count <= 4) {
        itemHeight = 100;
    } else {
        if ([itemDataSource[4] isKindOfClass:[NSString class]]) {
            itemHeight = 100;
        } else {
            itemHeight = 202;
        }
    }
    
    if (itemDataSource.count == 0 || !itemDataSource) {
        
        RACSignal *signal = [XSHTTPManager  rac_POSTURL:Url_GetMallMenuList params:@{@"type":@"mobile_index",@"limit":@(16)}];
        [signal subscribeNext:^(resultObject *state) {
            if (state.status) {
                NSMutableArray *arr = [NSMutableArray array];
                NSArray *data = state.data[@"data"];
                [data.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                    TabMallMenuItem *item=[TabMallMenuItem mj_objectWithKeyValues:x];
                    [arr addObject:item];
                }completed:^{
                    [UserInstance ShardInstnce].mallMenuArray = arr;
                    self.mallMenuArray = arr;
                    if (arr.count <= 4) {
                        itemHeight = 100;
                    } else {
                        itemHeight = 202;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isLoad = YES;
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                        OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
                        cell.isPlaceholderImage = NO;
                        cell.dataSource = arr;
                    });
                }];
            }
        }];
        
        [signal subscribeError:^(NSError * _Nullable error) {
            NSLog(@" 没数据 ");
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
            cell.isPlaceholderImage = YES;
            isLoad = YES;
            [self endRefresh];
            
            [self checkCurrentNetworkStatus];
        }];
        
    }else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
        cell.dataSource = itemDataSource;
    }

}

- (void)getMallFloorData {
    [HTTPManager getMallFloorInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.floorModel = [MallFloorModel mj_objectWithKeyValues:dic[@"data"]];
            [self.collectionView reloadData];
        } else {
            
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void) endRefresh {
    
    [self.collectionView.mj_footer endRefreshing];
    [self.collectionView.mj_header endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



/**
 请求 section == 0 footerView  公告
 */
- (void)requestMallNoticeData {
    
    NSMutableArray *arr = [NSMutableArray array];
    NSMutableArray *arr1 = [NSMutableArray array];
    [HTTPManager getMallNoticeInfoWithSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
//         [self endRefresh];
        if (state.status) {
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                TabMallNoticeItem *item=[TabMallNoticeItem mj_objectWithKeyValues:tempDic];
                [arr1 addObject:item];
                [arr addObject:item.title];
            }
            self.topNoticeItemArray = arr1;
//            footerScrollerView.arr = arr;
        }
    } fail:^(NSError * _Nonnull error) {
         [self endRefresh];
    }];
    
}

/**
 请求 section == 1 cell数据
 */
- (void)getMallNewProducts {
    ProductModel *model = [ProductModel new];
    model.recommend = @"4";
    [HTTPManager getProductListWithOrder:model.mj_keyValues Success:^(NSDictionary *dic, resultObject *state) {
        
        if (state.status) {
            self.productParser = [ProductParser mj_objectWithKeyValues:dic];
            [self.collectionView reloadData];
        }
        [self endRefresh];
    } fail:^(NSError *error) {
        [self endRefresh];
    }];
}




/**
 section == 1 headerView 广告  上 下 左 右
 */
- (void) requestSection_Two_HeaderInfor {

    [HTTPManager getMobile_All_InfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
//         [self endRefresh];
        if (state.status) {
            NSDictionary *itemDic = dic[@"data"];
            adverItemModel = [AdvertisementModel mj_objectWithKeyValues:itemDic];
            [self.collectionView reloadData];
        }
        
    } fail:^(NSError *error) {
         [self endRefresh];
    }];

}



-(void)changeGoodsList{
    page = 1;
    NSMutableArray *arr = [NSMutableArray array];
    [HTTPManager getMallGussYouLikeGoodsListInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        
        if (state.status) {
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                TabMallGoodsItem *item=[TabMallGoodsItem mj_objectWithKeyValues:tempDic];
                [arr addObject:item];
            }
            self.gussYouLikeGoodsArray = arr;
            [self.collectionView reloadData];
        }
        [self endRefresh];
    } fail:^(NSError *error) {
         [self endRefresh];
    }];
}




-(void)getMoreGoodsListInfo{
    page++;
    [HTTPManager getMallMoreGussYouLikeGoodsListInfoWithPage:@(page) Success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArray=[NSArray arrayWithArray:dic[@"data"][@"data"]];
                for (NSDictionary *tempDic in tempArray) {
                    TabMallGoodsItem *item=[TabMallGoodsItem mj_objectWithKeyValues:tempDic];
                    [self.gussYouLikeGoodsArray addObject:item];
                }
                [self.collectionView reloadData];
        }else{
            [MBProgressHUD showMessage:dic[@"info"] view:self.view];
        }
        [self endRefresh];
    } fail:^(NSError *error) {
        [self endRefresh];
        [MBProgressHUD showMessage:@"网络异常，请稍后重试" view:self.view];
    }];
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    if (indexPath.section == 2 && self.gussYouLikeGoodsArray.count - indexPath.row == 2){
        [self getMoreGoodsListInfo];
    }
}


//设置每个 UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        return CGSizeMake(mainWidth, itemHeight);
    } else if (indexPath.section == 1){
        
        return CGSizeMake(mainWidth, 200);
    }
    return CGSizeZero;
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(mainWidth, 200);
    } else if (section == 1){
        return CGSizeMake(mainWidth, 220+12);
    }
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if (section == 0) return CGSizeMake(mainWidth, 50);
    return CGSizeZero;
}

//定义每个section的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 1) return UIEdgeInsetsMake(12, 0, 0, 0);
    return UIEdgeInsetsZero;
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return 1;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        if (indexPath.section == 0) {
            
            UICollectionReusableView *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:Section_One_Header forIndexPath:indexPath];
            advertisement.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [advertisement addSubview:self.shopAdvier];
            advertisement.userInteractionEnabled = YES;
            return advertisement;
            
        }else if (indexPath.section == 1){
            
            TabMallADCell *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:Section_Two_Header forIndexPath:indexPath];
            advertisement.backgroundColor = [UIColor groupTableViewBackgroundColor];
            
            advertisement.itemDataSource = adverItemModel;
            advertisement.sectionDelegate = self;
            return advertisement;
        }
        
        
    }else if (kind == UICollectionElementKindSectionFooter){
        
//        footerScrollerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:Section_one_Footer forIndexPath:indexPath];
//        footerScrollerView.backgroundColor = [UIColor whiteColor];
//        footerScrollerView.userInteractionEnabled = YES;
//        @weakify(self);
//        [footerScrollerView setTapAction:^(){
//            @strongify(self);
//            [self goNoticeDetail:0];
//
//        }];
//        return footerScrollerView;
        
    }
    
    return nil;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        OneItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:Section_One_Cell forIndexPath:indexPath];
        cell.userInteractionEnabled = YES;
        cell.delegate = self;
        return cell;
        
    } else if(indexPath.section == 1) {
        TabMallCategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:Section_Two_Cell forIndexPath:indexPath];
        cell.productData = self.productParser.data;
        @weakify(self);
        [cell.topView setTapMoreAction:^(){
            @strongify(self);
            ProductModel *model = [ProductModel new];
            model.recommend = @"4";
            ProductListViewController *vc = [[ProductListViewController alloc] init];
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        
        [cell.item setTapProductAction:^(ProductDetailParser *productItem) {
            @strongify(self);
            [self goGoodsDetailWithGoodsId:productItem.product_id];
        }];
        
        return cell;
        
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGPoint point = scrollView.contentOffset;
    self.goTopBtn.hidden = point.y >  200 ? NO:YES;
    self.navBgView.alpha = point.y / 200.0f;
}


@end

@implementation AdvertisementModel

+ (NSDictionary *)objectClassInArray{
    

    return @{
             @"mobile_index_left_up" : @"TabMallADItem",
             @"mobile_index_left_down" : @"TabMallADItem",
             @"mobile_index_right_up" : @"TabMallADItem",
             @"mobile_index_right_down" : @"TabMallADItem",
             @"mobile_index_left_up" : @"TabMallADItem",
             @"mobile_index_left" : @"TabMallADItem",
             @"mobile_index_topright" : @"TabMallADItem",
             @"mobile_index_bottomright_left" : @"TabMallADItem"
             };
}

@end
