//
//  XSShoppingWebViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSShoppingWebViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "OrderPayViewController.h"
#import "XSBaseWKWebViewController.h"
#import <WebKit/WebKit.h>

#define IOS8x ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
#define progressBarColor XSYCOLOR(0x23a7f1)
#define NAVHEIGHT (SCREEN_HEIGHT == 812.0 ? 88 : 64)

@interface XSShoppingWebViewController ()<UIWebViewDelegate,WKNavigationDelegate,WKScriptMessageHandler>

@property (assign, nonatomic) NSUInteger loadCount;
@property (strong, nonatomic) UIProgressView *progressView;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) WKWebView *wkWebView;
@property (strong,nonatomic) JSContext *jscontext;
/** 当前webView */
@property (nonatomic, copy) NSString * currentUrlString;

/** 刷新按钮 */
@property (nonatomic, strong) UIButton * refreshBtn;

/**  */
@property (nonatomic, strong) WKWebViewConfiguration * wkConfig;

@end

@implementation XSShoppingWebViewController

- (UIProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.frame = CGRectMake(0, NAVHEIGHT, SCREEN_WIDTH, 0);
        _progressView.tintColor = progressBarColor;
        _progressView.trackTintColor = [UIColor whiteColor];
        [self.view addSubview:_progressView];
    }
    return _progressView;
}

- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    
    // 添加device参数
    if (![_urlStr containsString:@"device="]) {
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (isEmptyString(guid)) {
            guid = @"app";
        }
        if ([_urlStr containsString:@"?"]) {
            _urlStr = [NSString stringWithFormat:@"%@&device=%@",_urlStr,guid];
        } else {
            _urlStr = [NSString stringWithFormat:@"%@?device=%@",_urlStr,guid];
        }
    }
    
    // 添加多语言参数
    if (![_urlStr containsString:@"lang="]) {
        NSString *language = SelectLang;
        if (!language || [language isEqualToString:@""]) {
            language = @"zh-Hans";
            [[NSUserDefaults standardUserDefaults] setObject:language forKey:MyLanguage];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        //英文传en,中文传zh-cn;繁体传zh-tw
        if ([language isEqualToString:@"zh-Hans"]) {
            language = @"zh-cn";
        } else if ([language isEqualToString:@"en"]) {
            language = @"en";
        } else if ([language isEqualToString:@"zh-Hant"]) {
            language = @"zh-tw";
        }
        
        if ([_urlStr containsString:@"?"]) {
            _urlStr = [NSString stringWithFormat:@"%@&lang=%@",_urlStr,language];
        } else {
            _urlStr = [NSString stringWithFormat:@"%@?lang=%@",_urlStr,language];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNavView];
    [self setUpUI];
}

- (void)setUpNavView {

    if (self.titleString.length) {
        self.navigationItem.title = self.titleString;
    }
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left1 = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    [closeBtn setImage:[UIImage imageNamed:@"nav_close"] forState:UIControlStateNormal];
//    [closeBtn setBackgroundImage:[UIImage imageNamed:@"nav_close"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left2 = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    
    UIBarButtonItem *fixBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixBar.width = 10;
    
    self.navigationItem.leftBarButtonItems = @[left1,fixBar,left2];
    
    UIButton * reloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reloadBtn.frame = CGRectMake(0, 0, 30, 30);
    [reloadBtn setImage:[UIImage imageNamed:@"mall_exchange"] forState:UIControlStateNormal];
//     [reloadBtn  setBackgroundImage:[UIImage imageNamed:@"mall_exchange"] forState:UIControlStateNormal];
    [reloadBtn  addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
    self.refreshBtn = reloadBtn;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:reloadBtn];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)backClick {
    if (self.webStyle == XSWebStyleWKWeb) {
        // 网页
        if (IOS8x) {
            if (self.wkWebView.canGoBack) {
                [self.wkWebView goBack];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } else {
        if (self.webView.canGoBack) {
            [self.webView goBack];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
- (void)closeClick {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)refreshData {
    [UIView animateWithDuration:0.7 animations:^{
        self.refreshBtn.transform=CGAffineTransformRotate(self.refreshBtn.transform, M_PI);
    }];
    if (self.webStyle == XSWebStyleWKWeb) {
        [self.wkWebView reload];
    } else {
        [self.webView reload];
    }
}

- (void)setUpUI {
    //网址
    NSURL * theWebUrl = [NSURL URLWithString:self.urlStr];
    self.currentUrlString = self.urlStr;
    
    if (self.webStyle == XSWebStyleWKWeb) {
        // 网页
        if (IOS8x) {
            WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
            config.preferences.minimumFontSize = 18;
            self.wkConfig = config;
            
            WKWebView *wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, NAVHEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVHEIGHT) configuration:config];
            
//            if (@available(iOS 11.0, *)) {
//                wkWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//            }
            
            wkWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            wkWebView.backgroundColor = [UIColor whiteColor];
            wkWebView.navigationDelegate = self;
            [self.view insertSubview:wkWebView belowSubview:self.progressView];
            
            //wkWebView.sc
            [wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
            [wkWebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:theWebUrl];
            [wkWebView loadRequest:request];
            
            WKUserContentController *userCC = config.userContentController;
            //JS调用OC 添加处理脚本
//            [userCC addScriptMessageHandler:self name:@"showName"];
//            [userCC addScriptMessageHandler:self name:@"storeName"];
//            [userCC addScriptMessageHandler:self name:@"toPay"];
            [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"toPay"];
            
            self.wkWebView = wkWebView;
        } else {
            [self creatUIWebViewWithURL:theWebUrl];
        }
    } else {
        [self creatUIWebViewWithURL:theWebUrl];
    }
}

-(void)creatUIWebViewWithURL:(NSURL *)theWebUrl {
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, NAVHEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVHEIGHT)];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//    if (@available(iOS 11.0, *)) {
//        webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    }
    webView.scalesPageToFit = YES;
    webView.backgroundColor = [UIColor whiteColor];
    webView.delegate = self;
    [self.view insertSubview:webView belowSubview:self.progressView];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:theWebUrl];
    [webView loadRequest:request];
    self.webView = webView;
}

-(void)webView:(UIWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(nonnull NSError *)error {
    XSLog(@"%@",error);
}

#pragma mark - WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    XSLog(@"%@",NSStringFromSelector(_cmd));
    XSLog(@"%@",message.body);
    
    if ([message.name isEqualToString:@"showName"]) {
        XSLog(@"%@",message.body);
        
//        if (self.share_block) {
//            self.share_block(message.body);
//        }
    }
    
    if ([message.name isEqualToString:@"storeName"]) {
//        //        QSBLog(@"进入店铺");
//        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([message.name isEqualToString:@"toPay"]){
        NSArray *arr = message.body;
        [self clickToPayMoney:arr];
    }
}
- (void) clickToPayMoney:(NSArray *) array {
    
    OrderPayViewController *controller = [[OrderPayViewController alloc] init];
    controller.orderId = [NSString stringWithFormat:@"%@",[array firstObject]];
    controller.tableName = [NSString stringWithFormat:@"%@",[array lastObject]];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - wkWebView代理
// 如果不添加这个，那么wkwebview跳转不了AppStore
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if ([webView.URL.absoluteString hasPrefix:@"https://itunes.apple.com"]) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        decisionHandler(WKNavigationActionPolicyCancel);
    }else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}
-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        
        if (object == self.wkWebView) {
            CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
            if (newprogress == 1) {
                self.progressView.hidden = YES;
                [self.progressView setProgress:0 animated:NO];
                
            }else {
                self.progressView.hidden = NO;
                [self.progressView setProgress:newprogress animated:YES];
            }
        }
    } else if ([keyPath isEqualToString:@"title"]){
        
        if (object == self.wkWebView) {
            
            self.navigationItem.title = self.wkWebView.title;
        }
    }
}

#pragma mark - webView代理
// 计算webView进度条
- (void)setLoadCount:(NSUInteger)loadCount {
    _loadCount = loadCount;
    if (loadCount == 0) {
        self.progressView.hidden = YES;
        [self.progressView setProgress:0 animated:NO];
    }else {
        self.progressView.hidden = NO;
        CGFloat oldP = self.progressView.progress;
        CGFloat newP = (1.0 - oldP) / (loadCount + 1) + oldP;
        if (newP > 0.95) {
            newP = 0.95;
        }
        [self.progressView setProgress:newP animated:YES];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    self.loadCount ++;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.loadCount --;
    self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    //js调用iOS
    JSContext *context=[webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.jscontext = context;
//    __weak typeof(self) weakSelf = self;
    //此方法最终将打印出所有接收到的参数，js参数是不固定的
    /** 商品详情页*/
    context[@"JS方法名"] = ^(NSString * shopId) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
//    context[@"JS方法名"] = ^() {
//        dispatch_async(dispatch_get_main_queue(), ^{
//        });
//    };
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitOfflineWebApplicationCacheEnabled"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    self.loadCount --;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = request.URL.absoluteString;
    self.currentUrlString = url;
    XSLog(@"currentURL=%@",url);
    return YES;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.webView stopLoading];
    NSURLCache *cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSURLCache *cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
}

// 记得取消监听
- (void)dealloc {
    if (IOS8x) {
        [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
        [self.wkWebView removeObserver:self forKeyPath:@"title"];
        [self.wkConfig.userContentController removeScriptMessageHandlerForName:@"toPay"];
    }
    
    self.jscontext = nil;
}


@end



