//
//  TabMallVC.h
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabMallVC : XSBaseViewController

@end

@class TabMallADItem;
@interface AdvertisementModel : NSObject
@property (nonatomic ,strong)NSArray *mobile_index_left;
@property (nonatomic ,strong)NSArray *mobile_index_topright;
@property (nonatomic ,strong)NSArray *mobile_index_bottomright_left;
@property (nonatomic ,strong)NSArray *mobile_index_left_up;
@property (nonatomic ,strong)NSArray *mobile_index_left_down;
@property (nonatomic ,strong)NSArray *mobile_index_right_up;
@property (nonatomic ,strong)NSArray *mobile_index_right_down;

@end
