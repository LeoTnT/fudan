//
//  XSShoppingWebViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef NS_ENUM(NSUInteger, XSWebStyle) {
    XSWebStyleWKWeb,//WKWebView
    XSWebStyleUIWeb,//UIWebView
};

@interface XSShoppingWebViewController : XSBaseViewController

/** 标题 */
@property (nonatomic, copy) NSString * titleString;

/** 网址链接 */
@property (nonatomic, copy) NSString * urlStr;

/**
 *  webStyle
 */
@property (nonatomic ,assign) XSWebStyle webStyle;

@end

//@interface WeakScriptMessageDelegate : NSObject <WKScriptMessageHandler>
//
//@property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;
//
//- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;
//
//@end

