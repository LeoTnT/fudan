//
//  GroupBuyListViewController.m
//  App3.0
//
//  Created by mac on 2017/10/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupBuyListViewController.h"
#import "GroupBuyCell.h"
#import "GoodsDetailViewController.h"
#import "HTTPManager+Mall.h"
#import "TabMallModel.h"

@interface GroupBuyListViewController ()
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) NSInteger page;
@property (nonatomic, strong) UILabel *tableFooterView;
@end

@implementation GroupBuyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"团购专区";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    [self setSubViews];
    
    [self setRefreshHeader:^{
        @strongify(self);
        self.page = 1;
        [self requestData];
    }];
    
    [self setRefreshFooter:^{
        @strongify(self);
        self.page++;
        [self requestData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)requestData {
    [HTTPManager getGroupBuyListWithPage:self.page success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            GroupBuyModel *model = [GroupBuyModel mj_objectWithKeyValues:dic];
            if (self.page == 1) {
                // 下拉刷新
                [self.dataArray removeAllObjects];
                [self.dataArray addObjectsFromArray:model.data.data];
                if (self.dataArray.count) {
                    self.tableView.tableFooterView = nil;
                    [self.tableView reloadData];
                } else {
                    self.tableView.tableFooterView = self.tableFooterView;
                }
            } else {
                // 上拉加载更多
                if (model.data.data.count) {
                    self.tableView.tableFooterView = nil;
                    [self.dataArray addObjectsFromArray:model.data.data];
                    [self.tableView reloadData];
                } else {
                    self.tableView.tableFooterView = self.tableFooterView;
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)setSubViews {
    self.tableFooterView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    self.tableFooterView.text = @"没有更多了";
    self.tableFooterView.textColor = COLOR_666666;
    self.tableFooterView.font = [UIFont systemFontOfSize:14];
    self.tableFooterView.textAlignment = NSTextAlignmentCenter;
    
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupBuyCell *cell = [GroupBuyCell createGroupBuyCellWithTableView:tableView];
    GroupBuyDetailModel *model = self.dataArray[indexPath.section];
    cell.parser = model;
    [cell setGotoBuyAction:^(NSString *pId) {
        GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
        goodsDetailVC.goodsID = pId;
        [self.navigationController pushViewController:goodsDetailVC animated:YES];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
@end
