//
//  GroupBuyCell.h
//  App3.0
//
//  Created by mac on 2017/10/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"

@interface GroupBuyCell : UITableViewCell
@property (strong ,nonatomic) GroupBuyDetailModel *parser;
@property (nonatomic ,copy)void (^gotoBuyAction)(NSString *pId);

+ (instancetype)createGroupBuyCellWithTableView:(UITableView *)tableView;
@end
