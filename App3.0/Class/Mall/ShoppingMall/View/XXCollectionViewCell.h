//
//  XXCollectionViewCell.h
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
@interface XXCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)TabMallGoodsItem *goodsItem;
@property (nonatomic ,copy)void (^gotoShopAction)(NSString *supId);
@property (nonatomic, assign) BOOL isFloorGood; // yes则隐藏感兴趣和进入店铺
//+(instancetype)cellWithCollectionCell:(UICollectionView *)collection indetifier:(NSString *)identifier forIndexPath:(NSIndexPath *)indexPath;


@end
