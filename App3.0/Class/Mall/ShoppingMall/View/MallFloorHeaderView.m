//
//  MallFloorHeaderView.m
//  App3.0
//
//  Created by mac on 2018/1/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MallFloorHeaderView.h"

@interface MallFloorHeaderView()
@property (strong, nonatomic) UILabel *floorNumLabel;
@property (strong, nonatomic) UILabel *floorTitleLabel;
@property (strong, nonatomic) UILabel *countLabel;
@end

@implementation MallFloorHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView {
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *floor = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_floor_site"]];
    [self addSubview:floor];
    [floor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(12);
        make.centerY.mas_equalTo(self);
    }];
    
    _floorNumLabel = [UILabel new];
    _floorNumLabel.textColor = Color(@"FC4343");
    _floorNumLabel.font = [UIFont boldSystemFontOfSize:12];
    [self addSubview:_floorNumLabel];
    [_floorNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(floor.mas_right).offset(5);
        make.centerY.mas_equalTo(self);
    }];
    
    _floorTitleLabel = [UILabel new];
    _floorTitleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self addSubview:_floorTitleLabel];
    [_floorTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(12);
    }];
    
    UIImageView *leftLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_floor_left"]];
    [self addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_floorTitleLabel.mas_left).offset(-12);
        make.centerY.mas_equalTo(_floorTitleLabel);
    }];
    
    UIImageView *rightLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_floor_right"]];
    [self addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_floorTitleLabel.mas_right).offset(12);
        make.centerY.mas_equalTo(_floorTitleLabel);
    }];
    
    _countLabel = [UILabel new];
    _countLabel.textColor = Color(@"FC4343");
    _countLabel.font = [UIFont systemFontOfSize:10];
    _countLabel.layer.masksToBounds = YES;
    _countLabel.layer.cornerRadius = 8;
    _countLabel.layer.borderColor = [Color(@"FC4343") CGColor];
    _countLabel.layer.borderWidth = 1;
    [self addSubview:_countLabel];
    [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(_floorTitleLabel.mas_bottom).offset(7);
        make.height.mas_equalTo(18);
    }];
    
    UIButton *more = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-50, 10, 40, 40)];
    
    [more setTitle:@"更多" forState:UIControlStateNormal];
    more.titleLabel.font = [UIFont systemFontOfSize:12];
    [more setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    [more setImage:[UIImage imageNamed:@"more_arrow"] forState:UIControlStateNormal]; //logistic_right_nav  cart_gostore_arrow
    [more addTarget:self action:@selector(moreClick) forControlEvents:UIControlEventTouchUpInside];
    [more setTitleEdgeInsets:UIEdgeInsetsMake(0, -more.imageView.bounds.size.width-1, 0, more.imageView.bounds.size.width+1)];
    [more setImageEdgeInsets:UIEdgeInsetsMake(0, more.titleLabel.bounds.size.width+1, 0, -more.titleLabel.bounds.size.width-1)];
    [self addSubview:more];
//    [more mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self);
//        make.right.mas_equalTo(self).offset(-12);
//    }];
}

- (void)setFloorItem:(TabMallCategoryItem *)floorItem {
    if (!floorItem) {
        return;
    }
    _floorItem = floorItem;
    self.floorTitleLabel.text = floorItem.category_name;
    self.countLabel.text = [NSString stringWithFormat:@" 共%@款 ",floorItem.sub_products_total];
}

- (void)setFloorNum:(NSInteger)floorNum {
    _floorNum = floorNum;
    self.floorNumLabel.text = [NSString stringWithFormat:@"%liF",(long)floorNum];
}

- (void)moreClick {
    if (self.moreAction) {
        self.moreAction(self.floorItem);
    }
}
@end
