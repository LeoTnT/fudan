//
//  TabMallCategoryCell.h
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
#import "Shop_BaseView.h"

@interface TopHeaderView : UIView
@property (nonatomic ,copy)void (^ TapMoreAction)();
@end

@interface NewItemView :QuickCollectionView

@property (nonatomic, copy) void (^ tapProductAction)(ProductDetailParser *productItem);
@property (nonatomic ,strong)NSArray *itemDataSource;


@end;

@interface TabMallCategoryCell : UICollectionViewCell

@property (nonatomic, strong) ProductDataParser *productData;
@property (nonatomic, strong) UIButton *categoryMoreButton;
@property (nonatomic, strong) TopHeaderView *topView;
@property (nonatomic ,strong)NewItemView *item;
//@property (nonatomic, copy) void (^ tapProductAction)(ProductDetailParser *productItem);
@end




@interface NewItem :XSBaseCollectionCell

@property (nonatomic ,strong)ProductDetailParser *model;

@end
