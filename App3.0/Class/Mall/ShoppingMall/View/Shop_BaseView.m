//
//  Shop_BaseView.m
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "Shop_BaseView.h"

@implementation Shop_BaseView


-(instancetype)init {
    if (self = [super init]) {
    
        [self creatContentView];
    }
    return self;
}


- (void) creatContentView {
    
    self.userInteractionEnabled = YES;
}

- (instancetype)initWithDelegate:(id)delegate {
    
    if (self = [super init]) {
        
        [self creatContentView];
    }
    return self;
}





- (UIView *) creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    
    return view;
}

- (UIView *) creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    
    return lineView;
}
@end
