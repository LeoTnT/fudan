//
//  Shop_BaseView.h
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Shop_BaseView : UIView


- (void) creatContentView;

- (instancetype)initWithDelegate:(id)delegate;
//- (UIImageView *) creatImageView:(CGRect )frame tag:(NSInteger )tag;
- (UIView *) creatInforView:(CGRect )frame superView:(UIView *)superView ;
- (UIView *) creatLineView:(CGRect)frame ;
@end
