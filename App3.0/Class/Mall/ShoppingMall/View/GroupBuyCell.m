//
//  GroupBuyCell.m
//  App3.0
//
//  Created by mac on 2017/10/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GroupBuyCell.h"
#import "XSFormatterDate.h"

@interface GroupBuyCell ()
{
    long countDown;
    BOOL isStarted;
}
@property (strong, nonatomic) UIImageView *banner;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *price;
@property (strong, nonatomic) UILabel *originPrice;
@property (strong, nonatomic) UILabel *limit;
@property (strong, nonatomic) UIButton *groupBuy;
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation GroupBuyCell

+ (instancetype)createGroupBuyCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"groupBuyCell";
    GroupBuyCell *cell = (GroupBuyCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[GroupBuyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.banner = [UIImageView new];
        [self.contentView addSubview:self.banner];
        [self.banner mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self);
            make.height.mas_equalTo(150);
        }];
        
        UIView *grayView = [UIView new];
        grayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [self.banner addSubview:grayView];
        [grayView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.mas_equalTo(self.banner);
            make.height.mas_equalTo(20);
        }];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.textColor = [UIColor whiteColor];
        self.timeLabel.font = [UIFont systemFontOfSize:13];
        [grayView addSubview:self.timeLabel];
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(grayView).offset(12);
            make.centerY.mas_equalTo(grayView);
        }];
        
        self.nameLabel = [self createLabelWithFontSize:16];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.right.mas_lessThanOrEqualTo(self).offset(-12);
            make.top.mas_equalTo(self.banner.mas_bottom).offset(6);
        }];
        
        self.price = [self createLabelWithFontSize:18];
        self.price.textColor = [UIColor hexFloatColor:@"FF3355"];
        [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(12);
        }];
        
        self.originPrice = [self createLabelWithFontSize:12];
        self.originPrice.textColor = COLOR_999999;
        [self.originPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.price.mas_right).offset(2);
            make.bottom.mas_equalTo(self.price);
        }];
        
        self.groupBuy = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.groupBuy setBackgroundImage:[UIImage xl_imageWithColor:[UIColor hexFloatColor:@"3F8EF7"] size:CGSizeZero] forState:UIControlStateNormal];
        [self.groupBuy setBackgroundImage:[UIImage xl_imageWithColor:HighLightColor_Gray size:CGSizeZero] forState:UIControlStateDisabled];
        [self.groupBuy setTitle:@"去参团>" forState:UIControlStateNormal];
        [self.groupBuy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.groupBuy.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.groupBuy addTarget:self action:@selector(gotoBuy:) forControlEvents:UIControlEventTouchUpInside];
        self.groupBuy.layer.masksToBounds = YES;
        self.groupBuy.layer.cornerRadius = 3;
        [self.contentView addSubview:self.groupBuy];
        [self.groupBuy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-12);
            make.centerY.mas_equalTo(self.price);
            make.size.mas_equalTo(CGSizeMake(66, 27));
        }];
        
        self.limit = [self createLabelWithFontSize:12];
        self.limit.textColor = COLOR_999999;
        [self.limit mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.groupBuy.mas_left).offset(-6);
            make.centerY.mas_equalTo(self.price);
        }];
        
    }
    return self;
}

- (void)setParser:(GroupBuyDetailModel *)parser {
    _parser = parser;
    [self.banner getImageWithUrlStr:parser.img andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.nameLabel.text = parser.title;
    self.price.text = [NSString stringWithFormat:@"¥%@",parser.sell_price];
    
    // 原价
    NSString *textStr = [NSString stringWithFormat:@"¥%@",parser.market_price];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:textStr attributes:attribtDic];
    // 赋值
    self.originPrice.attributedText = attribtStr;
    
    self.limit.text = [NSString stringWithFormat:@"还剩%@单拼团成功",parser.remain];
    
    [self.timer invalidate];
    self.timer = nil;
    
    NSDate *currentData = [NSDate date];
    countDown = [parser.begin_time longLongValue] - [currentData timeIntervalSince1970];
    if (countDown > 0) {
        // 未开始
        isStarted = NO;
        NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
        self.timeLabel.text = [NSString stringWithFormat:@"距活动开始：%@",timeRemin];
        self.groupBuy.enabled = NO;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    } else {
        // 已开始
        isStarted = 1;
        countDown = [parser.end_time longLongValue] - [currentData timeIntervalSince1970];
        if (countDown > 0) {
            NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
            self.timeLabel.text = [NSString stringWithFormat:@"倒计时：%@",timeRemin];
            self.groupBuy.enabled = YES;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        } else {
            isStarted = 0;
            self.timeLabel.text = @"活动已经结束";
            self.groupBuy.enabled = NO;
        }
        
    }
    
    
}

- (void)countDown {
    countDown--;
    if (isStarted) {
        NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
        self.timeLabel.text = [NSString stringWithFormat:@"倒计时：%@",timeRemin];
    } else {
        NSString *timeRemin = [XSFormatterDate timeRemainingWithSeconds:countDown];
        self.timeLabel.text = [NSString stringWithFormat:@"距活动开始：%@",timeRemin];
    }
}

- (void)gotoBuy:(UIButton *)sender {
    if (self.gotoBuyAction) {
        self.gotoBuyAction(self.parser.product_id);
    }
}

- (UILabel *)createLabelWithFontSize:(CGFloat)size {
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:size];
    [self.contentView addSubview:label];
    return label;
}

@end
