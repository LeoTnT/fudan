//
//  MallProductCountCell.m
//  App3.0
//
//  Created by mac on 2018/1/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MallProductCountCell.h"

@interface MallProductCountCell()
@property (strong, nonatomic) UILabel *totalLabel;
@property (strong, nonatomic) UILabel *addLabel;
@end

@implementation MallProductCountCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView {
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *totalBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_floor_totalBg"]];
    [self.contentView addSubview:totalBg];
    [totalBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(13, 12, 13, mainWidth/2+6));
    }];
    
    UIImageView *addBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_floor_addBg"]];
    [self.contentView addSubview:addBg];
    [addBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(13, mainWidth/2+6, 13, 12));
    }];
    
    UILabel *totalTitle = [UILabel new];
    totalTitle.text = @"商品总计";
    totalTitle.textColor = [UIColor whiteColor];
    totalTitle.font = [UIFont systemFontOfSize:15];
    [totalBg addSubview:totalTitle];
    [totalTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(totalBg).offset(13);
        make.top.mas_equalTo(totalBg).offset(22);
    }];
    
    _totalLabel = [UILabel new];
    _totalLabel.textColor = [UIColor whiteColor];
    _totalLabel.font = [UIFont systemFontOfSize:24];
    [totalBg addSubview:_totalLabel];
    [_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(totalTitle);
        make.top.mas_equalTo(totalTitle.mas_bottom).offset(13);
    }];
    
    UILabel *addeTitle = [UILabel new];
    addeTitle.text = @"今日新增";
    addeTitle.textColor = [UIColor whiteColor];
    addeTitle.font = [UIFont systemFontOfSize:15];
    [addBg addSubview:addeTitle];
    [addeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(addBg).offset(13);
        make.top.mas_equalTo(addBg).offset(22);
    }];
    
    _addLabel = [UILabel new];
    _addLabel.textColor = [UIColor whiteColor];
    _addLabel.font = [UIFont systemFontOfSize:24];
    [addBg addSubview:_addLabel];
    [_addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(addeTitle);
        make.top.mas_equalTo(addeTitle.mas_bottom).offset(13);
    }];
}

- (void)setCountModel:(MallProductCountModel *)countModel {
    if (!countModel) {
        return;
    }
    _countModel = countModel;
    NSString *str = [NSString stringWithFormat:@"%@件",countModel.product_total];
    NSMutableAttributedString *mutStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(str.length-1, 1)];
    self.totalLabel.attributedText = mutStr;
    
    str = [NSString stringWithFormat:@"%@件",countModel.product_new_total];
    mutStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(str.length-1, 1)];
    self.addLabel.attributedText = mutStr;
}
@end
