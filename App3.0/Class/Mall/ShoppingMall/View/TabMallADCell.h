//
//  TabMallADCell.h
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"
@class AdvertisementModel;
@protocol SectionHeaderDelegate <NSObject>

- (void)didSelectedItem:(UITapGestureRecognizer *)tap;

@end

@interface TabMallADCell : UICollectionReusableView

@property (nonatomic ,weak)id <SectionHeaderDelegate > sectionDelegate;

@property(nonatomic,strong)UIImageView *leftImage;
@property(nonatomic,strong)UIImageView *rightTopImage;
@property(nonatomic,strong)UIImageView *rightBottomLeftImage;
@property(nonatomic,strong)UIImageView *rightBottomRightImage;

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic ,strong)AdvertisementModel *itemDataSource;
@end
