//
//  TabMallMenuCell.h
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDBaseCollectionCell.h"
#import "Shop_BaseView.h"
#import "TabMallModel.h"


@protocol TabMallMenuCellDelegate <NSObject>

- (void)didSelectedMenuCell:(TabMallMenuItem*)model;

@end


@interface OneItem : FDBaseCollectionCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong)UICollectionView *collectionView;
@property (nonatomic ,strong)UICollectionViewFlowLayout *flowLayout;
@property (nonatomic ,strong)UIPageControl *pageControl;
@property (nonatomic ,strong)NSMutableArray *dataSource;
@property (nonatomic ,weak)id<TabMallMenuCellDelegate> delegate;
@property (nonatomic ,assign)BOOL isPlaceholderImage;

@end


@interface ItemCell : FDBaseCollectionCell

@property (nonatomic ,strong)TabMallMenuItem *model;

@end

