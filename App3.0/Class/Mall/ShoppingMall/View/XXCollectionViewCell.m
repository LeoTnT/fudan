//
//  XXCollectionViewCell.m
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XXCollectionViewCell.h"

@interface XXCollectionViewCell ()

@property(nonatomic,strong)UIImageView *goodsImage;
@property(nonatomic,strong)UILabel *goodsNameLabel;
@property(nonatomic,strong)UILabel *shopNameLabel;
@property(nonatomic,strong)UILabel *goodsPriceLabel;
@property(nonatomic,strong)UILabel *marketPriceLabel;
@property(nonatomic,strong)UILabel *interestedNumLabel;
@end

@implementation XXCollectionViewCell


//+(instancetype)cellWithCollectionCell:(UICollectionView *)collection indetifier:(NSString *)identifier forIndexPath:(NSIndexPath *)indexPath{
//
//    XXCollectionViewCell *cell = [collection dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    return cell;
//}

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void) creatContentView {
    self.backgroundColor = [UIColor whiteColor];
    self.goodsImage = [UIImageView new];

    [self.contentView addSubview:self.goodsImage];
    
    [self.goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.height.mas_equalTo(172);
    }];
    
//    self.shopNameLabel = [self creatLabel:NSTextAlignmentLeft fontSize:12];
//    self.shopNameLabel.userInteractionEnabled = YES;
//    self.shopNameLabel.text = @"进入店铺";
//    self.shopNameLabel.textColor = COLOR_666666;
//    @weakify(self);
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
//        @strongify(self);
//        if (self.gotoShopAction) {
//            self.gotoShopAction(self.goodsItem.user_id);
//        }
//    }];
//    [self.shopNameLabel addGestureRecognizer:tap];
    
    self.goodsNameLabel = [self creatLabel:NSTextAlignmentLeft fontSize:14];
    self.goodsNameLabel.lineBreakMode = NSLineBreakByCharWrapping;

    self.goodsPriceLabel = [self creatLabel:NSTextAlignmentLeft fontSize:18];
    self.goodsPriceLabel.textColor = mainColor;
    
    self.marketPriceLabel = [self creatLabel:NSTextAlignmentLeft fontSize:11];
    self.marketPriceLabel.textColor = COLOR_999999;

//    self.interestedNumLabel = [self creatLabel:NSTextAlignmentRight fontSize:12];
//    self.interestedNumLabel.textColor = COLOR_666666;
    [self layoutIfNeeded];
}

- (void)layoutSubviews {
    [self.goodsNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsImage.mas_bottom).mas_offset(11);
        make.left.mas_equalTo(self).offset(12);
        make.right.mas_equalTo(self).offset(-12);
        make.height.mas_equalTo(14);
    }];
    
    [self.goodsPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsNameLabel.mas_bottom).offset(9);
        make.left.mas_equalTo(self.goodsNameLabel);
        make.right.mas_equalTo(self.goodsNameLabel);
        make.height.mas_equalTo(14);
    }];
    
    [self.marketPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsPriceLabel.mas_bottom).offset(8);
        make.left.mas_equalTo(self.goodsPriceLabel);
        make.right.mas_equalTo(self.goodsPriceLabel);
        make.height.mas_equalTo(9);
    }];
    
//    [self.interestedNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.goodsNameLabel);
//        make.top.mas_equalTo(self.goodsPriceLabel.mas_bottom).offset(6);
//    }];
    
//    [self.shopNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.goodsNameLabel);
//        make.centerY.mas_equalTo(self.interestedNumLabel);
//    }];
}

- (UILabel *)creatLabel:(NSTextAlignment)alignment fontSize:(CGFloat) size{
 
    UILabel *label = [UILabel new] ;
    label.font = [UIFont systemFontOfSize:size];
    label.textAlignment = alignment;
    [self.contentView addSubview:label];
    return label;
}

- (void)setGoodsItem:(TabMallGoodsItem *)goodsItem{
    
    if (_goodsItem == goodsItem) return;
    
    _goodsItem = goodsItem;
    
    NSString *ii = goodsItem.image_thumb ? goodsItem.image_thumb:goodsItem.image;
    
    [self.goodsImage getImageWithUrlStr:ii andDefaultImage:[UIImage imageNamed:@"no_pic"]];

    if ([goodsItem.sell_type integerValue] == 100) {
        // 批发商品
        NSString *str1 = [NSString stringWithFormat:@"[批发]%@",goodsItem.product_name];
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",str1]];
        [attri addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(0, 4)];
        
        self.goodsNameLabel.attributedText=attri;
    } else {
        self.goodsNameLabel.text = goodsItem.product_name;
    }
    
    if ([goodsItem.sell_type integerValue] == 2) {
        self.goodsPriceLabel.text = goodsItem.score;
    } else {
        self.goodsPriceLabel.text = goodsItem.sell_price;

//        NSString *str1 = goodsItem.sell_price;
//        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",str1]];
//        [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10] range:NSMakeRange(0, 1)];
//        self.goodsPriceLabel.attributedText=attri;
    }
    
    // 原价
    NSString *textStr = [NSString stringWithFormat:@"¥%@",goodsItem.market_price];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:textStr attributes:attribtDic];
    // 赋值
    self.marketPriceLabel.attributedText = attribtStr;
    
//    self.interestedNumLabel.text=[NSString stringWithFormat:@"%@人感兴趣",goodsItem.look_num];

}

- (void)setIsFloorGood:(BOOL)isFloorGood {
    _isFloorGood = isFloorGood;
    self.interestedNumLabel.hidden = isFloorGood;
    self.shopNameLabel.hidden = isFloorGood;
}
@end
