//
//  MallFloorHeaderView.h
//  App3.0
//
//  Created by mac on 2018/1/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabMallModel.h"

@interface MallFloorHeaderView : UICollectionReusableView
@property (strong, nonatomic) TabMallCategoryItem *floorItem;
@property (nonatomic ,copy) void (^moreAction)(TabMallCategoryItem *item);
@property (assign, nonatomic) NSInteger floorNum;
@end
