
//
//  TabMallCategoryCell.m
//  App3.0
//
//  Created by mac on 2017/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallCategoryCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"
@interface TabMallCategoryCell()
//@property (strong, nonatomic) UIScrollView *scrollView;
@end
@implementation TabMallCategoryCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void) creatContentView {
    self.layer.masksToBounds = YES;
    
//    self.backgroundColor = [UIColor whiteColor];
    _topView = [TopHeaderView new];
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(50);
    }];
    
    _item = [[NewItemView alloc] initWithFrame:CGRectMake(0, 50, mainWidth, 150)];
//    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_item];
    
    
}


-(void)setProductData:(ProductDataParser *)productData {
    _productData = productData;
    _item.itemDataSource = productData.data;

}

- (TabMallCategoryImgView *) getImageViewWithTag:(NSInteger )tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    
    return view;
}


- (UIView *) creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    
    return view;
}

- (UIView *) creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    
    return lineView;
}


@end



@implementation TopHeaderView
{
    UIImageView *imageView;
    UILabel *title;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.masksToBounds = YES;
    UIView *leftLine = [UIView new];
    [self addSubview:leftLine];
    
    UIView *rightLine = [UIView new];
    [self addSubview:rightLine];
    
    UIColor *color = [UIColor blackColor];
    
    leftLine.backgroundColor = color;
    rightLine.backgroundColor = color;
    
    title = [UILabel new];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:15];
    title.textColor = color;
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"新品推荐"];
    [att addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(0, 2)];
    title.attributedText = att;
    [self addSubview:title];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-50, 10, 40, 40)];
    [btn addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [btn setImage:[UIImage imageNamed:@"more_arrow"] forState:UIControlStateNormal];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.bounds.size.width-1, 0, btn.imageView.bounds.size.width+1)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.bounds.size.width+1, 0, -btn.titleLabel.bounds.size.width-1)];
    [self addSubview:btn];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
    
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(title.mas_left).offset(-8);
        make.size.mas_equalTo(CGSizeMake(12, 0.5));
    }];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(title.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(12, 0.5));
    }];
    
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self).offset(-10);
//        make.centerY.mas_equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(40, 40));
//    }];
    
}

- (void) moreAction:(UIButton *)sender {
    
    if (self.TapMoreAction) {
        self.TapMoreAction();
    }
}

@end


@implementation NewItemView

- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(110, self.mj_h);
    [self.collectionView registerClass:[NewItem class] forCellWithReuseIdentifier:@"cell"];
    
    
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
    
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    NewItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.tapProductAction) {
        ProductDetailParser *model = self.itemDataSource[indexPath.row];
        self.tapProductAction(model);
    }
}

@end


@implementation NewItem
{
    UILabel *name;
    UILabel *price;
}

- (void)setContentView {
    [super setContentView];
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self);
        make.height.mas_equalTo(95);
        make.width.mas_equalTo(110);
    }];
    
    
    name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"111111"]];
     name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(7.5);
        make.width.mas_equalTo(self.headerImageView);
    }];
    
    price = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:11] titleColor:mainColor];
    [self addSubview:price];
    
    [price mas_makeConstraints:^(MASConstraintMaker *make) {
       make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(name.mas_bottom).offset(7.5);

    }];
}

- (void)setModel:(ProductDetailParser *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.image_thumb] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    name.text = model.product_name;
    NSString *sss = [NSString stringWithFormat:@"¥%@",model.sell_price];
    if (!isEmptyString(sss)) {
        price.attributedText = [NSAttributedString lbd_changeFontAndColor:[UIFont systemFontOfSize:14] Color:mainColor andlineSpace:2 TotalString:sss SubStringArray:@[model.sell_price]];
    }else{
        price.text = @"";
    }
    
}

@end

