//
//  TabMallMenuCell.m
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabHomeMenuCell.h"
#import "TabMallModel.h"

@implementation FDHomeOneItem

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout= [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.minimumInteritemSpacing = 0;
        _flowLayout.minimumLineSpacing = 0;
        _flowLayout.itemSize = CGSizeMake(mainWidth/4, 183.5/2);
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return _flowLayout;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        _collectionView= [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 183.5 ) collectionViewLayout:self.flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

- (UIPageControl *)pageControl {
    
    if (!_pageControl) {
    
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame), mainWidth, 18);
        _pageControl.pageIndicatorTintColor = [UIColor hexFloatColor:@"f4f4f4"];
        _pageControl.backgroundColor = [UIColor whiteColor];
        _pageControl.currentPageIndicatorTintColor = mainColor;
        [self addSubview:_pageControl];

    }
    
    return _pageControl;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
#ifdef __IPHONE_11_0
        if (@available(iOS 11.0, *)) {
            self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            self.collectionView.scrollIndicatorInsets = self.collectionView.contentInset;
        }
#endif
    
        [self.collectionView registerClass:[FDHomeItemCell class] forCellWithReuseIdentifier:@"cell"];
        
       NSMutableArray *itemCount = [UserInstance ShardInstnce].mallMenuArray;
        
        if (itemCount.count != 0) {
            self.dataSource = itemCount;
        }else{
            [self setDataSource:[NSMutableArray arrayWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8"]]];
        }
        
    }
    return self;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = self.dataSource.count;
    return count;
}

- (void)setDataSource:(NSMutableArray *)dataSource {
    NSMutableArray *itemCount = dataSource;
    NSMutableArray *oldArr = [NSMutableArray array];
    if (itemCount.count != 0) {

        NSInteger sss = (8 - itemCount.count % 8);
        if (sss != 8) {
            for (int index = 0; index < sss ; index ++) {
                [itemCount addObject:@""];
            }
        }
        for (NSInteger x = 0; x < itemCount.count / 8; x ++) {
            NSMutableArray *rangArr = [NSMutableArray arrayWithArray:[itemCount objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(x*8, 8)]]];
            @try {
                [oldArr addObject:[self getOrderWithArr:rangArr]];
            }@catch (NSException *exception){
                [oldArr addObject:rangArr];
            }
            
        }
        _dataSource = oldArr;
    }
    self.pageControl.numberOfPages = _dataSource.count > 1 ? _dataSource.count :0;
    if (_dataSource.count >= 2) self.pageControl.currentPage = 0;
    [self.collectionView reloadData];
}

- (NSMutableArray *) getOrderWithArr:(NSMutableArray *)rangArr {
    NSMutableArray *arr1 = [NSMutableArray array];
    NSMutableArray *arr2 = [NSMutableArray array];
    [arr1 addObjectsFromArray:[rangArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 4)]]];
    [arr2 addObjectsFromArray:[rangArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(4, 4)]]];
    [rangArr removeAllObjects];
    [arr1 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [rangArr addObject:arr1[idx]];
        [rangArr addObject:arr2[idx]];
    }];
    
    return rangArr;
}

- (void)setIsPlaceholderImage:(BOOL)isPlaceholderImage {
    _isPlaceholderImage = isPlaceholderImage;
    if (isPlaceholderImage) {
        [self setDataSource:[NSMutableArray arrayWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8"]]];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *arr = self.dataSource[section];
    return arr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FDHomeItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.dataSource[indexPath.section][indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.pageControl.currentPage = page;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedMenuCell:)]) {
        TabMallMenuItem *model = self.dataSource[indexPath.section][indexPath.row];
        if ([model isKindOfClass:[TabMallMenuItem class]]) {
            [self.delegate didSelectedMenuCell:model];
        }
    }
}

@end

@implementation FDHomeItemCell {
    UIImageView *imageView;
    UILabel *title;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {

        
        imageView = [BaseUITool imageWithName:@"no_pic" superView:self];
        title = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:13] titleColor:COLOR_666666];

        [self addSubview:title];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(22);
            make.size.mas_equalTo(CGSizeMake(47, 47));
        }];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(imageView.mas_bottom).offset(9);
            make.bottom.mas_equalTo(self);
        }];
    }
    return self;
}



- (void)setModel:(TabMallMenuItem *)model {
    
    if ([model isKindOfClass:[TabMallMenuItem class]]) {
        self.hidden = NO;
//        [imageView getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [imageView sd_setImageWithURL:model.logo placeholderImage:[UIImage imageNamed:@"no_pic"]];
        XSLog(@"%@",model.name);
        title.text=model.name;
    }else if ([model isEqual:@""]){
        self.hidden = YES;
    }else{
        self.hidden = NO;
//        title.text =model;
    }
}


@end
