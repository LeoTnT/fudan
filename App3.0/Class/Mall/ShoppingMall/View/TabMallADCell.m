//
//  TabMallADCell.m
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabMallADCell.h"
@implementation TabMallADCell


-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    if (self.isMall) {
        self.leftImage =  [self creatImageView:CGRectZero tag:1];
        self.rightTopImage = [self creatImageView:CGRectZero tag:2];
        self.rightBottomLeftImage = [self creatImageView:CGRectZero tag:3];
        self.rightBottomRightImage = [self creatImageView:CGRectZero tag:4];
        
        CGFloat space  = 1;
        [self.leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.top.mas_equalTo(self.mas_top).offset(12);
            make.height.mas_equalTo(110);
            make.width.mas_equalTo(mainWidth/2-space/2);
        }];
        
        [self.rightTopImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.leftImage);
            make.left.mas_equalTo(self.leftImage.mas_right).offset(space);
            make.height.mas_equalTo(110);
            make.right.mas_equalTo(self);
        }];
        
        
        [self.rightBottomLeftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.top.mas_equalTo(self.rightTopImage.mas_bottom).offset(space);
            make.size.mas_equalTo(self.leftImage);
        }];
        
        
        [self.rightBottomRightImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.rightBottomLeftImage.mas_right).offset(space);
            make.top.mas_equalTo(self.rightBottomLeftImage);
            make.size.mas_equalTo(self.rightTopImage);
        }];
    } else {
        
        UIView *topLineView = [UIView new];
        topLineView.backgroundColor = [UIColor hexFloatColor:@"F4F4F4"];
        [self addSubview:topLineView];
        [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self);
            make.top.mas_equalTo(self);
            make.height.mas_equalTo(10);
        }];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = mainColor;
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(5, 16));
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(topLineView.mas_bottom).offset(15);
        }];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = Localized(@"公告专区");
        titleLabel.textColor = COLOR_111111;
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topLineView.mas_bottom).offset(15);
            make.left.mas_equalTo(lineView.mas_right).offset(5);
            make.height.mas_equalTo(16);
        }];
        
        self.leftImage =  [self creatImageView:CGRectZero tag:1];
        self.rightTopImage = [self creatImageView:CGRectZero tag:2];
        self.rightBottomLeftImage = [self creatImageView:CGRectZero tag:3];
        
        [self.leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.top.mas_equalTo(44+10);
            make.height.mas_equalTo(160);
            make.width.mas_equalTo(120);
        }];
        
        [self.rightTopImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.leftImage);
            make.left.mas_equalTo(self.leftImage.mas_right).offset(5);
            make.height.mas_equalTo(78);
            make.right.mas_equalTo(-15);
        }];
        
        
        [self.rightBottomLeftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.rightTopImage);
            make.bottom.mas_equalTo(self.leftImage.mas_bottom);
            make.size.mas_equalTo(self.rightTopImage);
        }];
    }
}


- (void)setItemDataSource:(AdvertisementModel *)itemDataSource {
    
    if (_itemDataSource == itemDataSource) return;
    
    _itemDataSource = itemDataSource;
    if (self.isMall) {
        TabMallADItem *model = (TabMallADItem *)itemDataSource.mobile_index_left_up[0];
        [self.leftImage getImageWithUrlStr:model.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        TabMallADItem *model1 = (TabMallADItem *)itemDataSource.mobile_index_right_up[0];
        TabMallADItem *model2 = (TabMallADItem *)itemDataSource.mobile_index_left_down[0];
        TabMallADItem *model3 = (TabMallADItem *)itemDataSource.mobile_index_right_down[0];
        [self.rightTopImage getImageWithUrlStr:model1.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self.rightBottomLeftImage getImageWithUrlStr:model2.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self.rightBottomRightImage getImageWithUrlStr:model3.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    } else {
        TabMallADItem *model = (TabMallADItem *)itemDataSource.mobile_index_left[0];
        TabMallADItem *model1 = (TabMallADItem *)itemDataSource.mobile_index_topright[0];
        TabMallADItem *model2 = (TabMallADItem *)itemDataSource.mobile_index_bottomright_left[0];
        [self.leftImage getImageWithUrlStr:model.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self.rightTopImage getImageWithUrlStr:model1.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self.rightBottomLeftImage getImageWithUrlStr:model2.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    }
    
}



- (UIImageView *) creatImageView:(CGRect )frame tag:(NSInteger )tag{
    
    UIImageView *imageView=[UIImageView new];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled=YES;
    imageView.backgroundColor = [UIColor whiteColor];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.tag=tag;
    [imageView yy_setImageWithURL:nil placeholder:[UIImage imageNamed:@"no_pic"]];
    @weakify(self);
    [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(UITapGestureRecognizer * sender) {
     
        @strongify(self);
        
        if (self.sectionDelegate && [self.sectionDelegate respondsToSelector:@selector(didSelectedItem:)]) {
            [self.sectionDelegate didSelectedItem:sender];
        }
        
    }]];
    [self addSubview:imageView];
    return imageView;
}

@end
