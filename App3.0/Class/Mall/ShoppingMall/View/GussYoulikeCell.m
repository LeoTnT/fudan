//
//  GussYoulikeCell.m
//  App3.0
//
//  Created by syn on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GussYoulikeCell.h"

@implementation GussYoulikeCell


-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

-(void)creatContentView {

    self.backgroundColor = [UIColor whiteColor];
    UIView *leftLine = [UIView new];
    [self addSubview:leftLine];
    
    UIView *rightLine = [UIView new];
    [self addSubview:rightLine];
    
    UIColor *color = [UIColor blackColor];
    
    leftLine.backgroundColor = color;
    rightLine.backgroundColor = color;
    
    UILabel *title = [UILabel new];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:15];
    title.textColor = color;
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"猜你喜欢"];
    [att addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2, 2)];
    title.attributedText = att;
    [self addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(title.mas_left).offset(-8);
        make.size.mas_equalTo(CGSizeMake(12, 0.5));
    }];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(title.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(12, 0.5));
        
    }];

}

@end
