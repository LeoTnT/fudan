//
//  GoodsDetailViewController.m
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailViewController.h"
#import "GoodsDetailModel.h"
#import "GoodsDetailTopScrollCell.h"
#import "GoodsDetailTopInfoCell.h"
#import "GoodsTipsView.h"
#import "CartVC.h"
#import "GoodsQRCodeViewController.h"
#import "XSShareView.h"
#import "GoodsDetailTopInfoCell.h"
#import "GoodsDetailSpecView.h"
#import "GrayBackView.h"
#import "GoodsDetailSumEvaluationCell.h"
#import "GoodsEvaluationCell.h"
#import "GoodsDetailEvaluateVC.h"
#import "GoodsDetailSellerInfoCell.h"
#import "GoodsDetailRecommendGoodsCell.h"
#import "GoodsDetailTabView.h"
#import "S_StoreInformation.h"
#import "ProductListViewController.h"
#import "VerifyOrderViewController.h"
#import "GoodsDetailWholeSpecView.h"
#import "WholesaleCell.h"
#import "UUMessage.h"

@interface GoodsDetailViewController ()<GoodsTipsViewDelegate,GoodsDetailTopInfoCellDelegate,GoodsDetailSpecViewDelegate,UIWebViewDelegate,GoodsDetailTabViewDelegate,GoodsDetailSellerInfoCellDelegate,GoodsDetailRecommendGoodsCellDelegate,UITextFieldDelegate,GoodsDetailWholeSpecViewDelegate>
@property(nonatomic,assign)GoodSellType goodSellType;   // 商品类型
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@property(nonatomic,strong)GoodsStoreStatisticsModel *statisticsModel;
@property(nonatomic,strong)GoodsTipsView *tipsView;
@property(nonatomic,strong)GoodsDetailSpecView *specView;//普通商品的规格框
@property(nonatomic,strong)GoodsDetailWholeSpecView *wholeSpecView;//批发商品的规格框
@property(nonatomic,strong)NSMutableDictionary *wholeSpecDic;
@property(nonatomic,strong)UIView *wholeGrayView;
@property(nonatomic,strong)GrayBackView *grayBackView;
@property(nonatomic,strong)NSMutableDictionary *heightDic;
@property(nonatomic,strong)UIWebView *webView;
@property(nonatomic,strong)NSDictionary *detailPictureDic;
@property(nonatomic,strong)NSMutableArray *recommendArray;
@property(nonatomic,strong)GoodsDetailTabView *tabView;
@property(nonatomic,assign)BOOL isclickedAddToCart,isClickedGroupBuy;//点击的加入购物车和立即购买   确定按钮的处理不同    一键拼单和正常处理不同
@end

@implementation GoodsDetailViewController
#pragma mark-Lazy Loading
- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 1)];
        _webView.delegate = self;
        _webView.scrollView.scrollEnabled = NO;
    }
    return _webView;
}
-(GoodsTipsView *)tipsView{
    if (!_tipsView) {
        _tipsView=[[GoodsTipsView alloc] initWithFrame:CGRectMake(mainWidth-10-mainWidth/2.7,70, mainWidth/2.7, mainHeight/4.0)];
        _tipsView.delegate=self;
        [self.view addSubview:_tipsView];
        _tipsView.hidden=YES;
    }
    return _tipsView;
}
-(NSMutableArray *)recommendArray{
    if(!_recommendArray){
        _recommendArray=[NSMutableArray array];
    }
    return _recommendArray;
}

-(GoodsDetailSpecView *)specView{
    if (!_specView) {
        _specView=[[GoodsDetailSpecView alloc] init];
        _specView.vc=self;
        _specView.delegate=self;
        _specView.goodSellType = self.goodSellType;
        _specView.detailInfo=self.detailInfo;
        _specView.hidden=YES;
        [self.grayBackView addSubview:_specView];
        _specView.frame=CGRectMake(0, mainHeight-_specView.height-kTabbarSafeBottomMargin, mainWidth,_specView.height);
    }
    return _specView;
}
-(GoodsDetailWholeSpecView *)wholeSpecView{
    if (!_wholeSpecView) {
        _wholeSpecView=[[GoodsDetailWholeSpecView alloc] init];
        _wholeSpecView.detailInfo=self.detailInfo;
        _wholeSpecView.frame=CGRectMake(0, mainHeight-_wholeSpecView.height-kTabbarSafeBottomMargin, mainWidth, _wholeSpecView.height);
        [self.wholeGrayView addSubview:_wholeSpecView];
        _wholeSpecView.specDicArray=[NSMutableArray array];
        _wholeSpecView.noSpecchangeNumView.goodsNumField.text=@"0";
        _wholeSpecView.delegate=self;
    }
    return _wholeSpecView;
}
-(UIView *)wholeGrayView{
    if (!_wholeGrayView) {
        _wholeGrayView=[[UIView alloc] initWithFrame:self.view.bounds];
        UIColor *bColor = [UIColor blackColor];
        _wholeGrayView.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        [self.view addSubview:_wholeGrayView];
//        [_wholeGrayView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
//            [_wholeGrayView endEditing:YES];
//            self.wholeGrayView.hidden=YES;
//            self.wholeSpecView.hidden=YES;
//        }]];
    }
    return _wholeGrayView;
}

-(NSMutableDictionary *)wholeSpecDic{
    if (!_wholeSpecDic) {
        _wholeSpecDic=[NSMutableDictionary dictionary];
    }
    return _wholeSpecDic;
}
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}
-(GrayBackView *)grayBackView{
    if(!_grayBackView){
        _grayBackView=[[GrayBackView alloc] init];
        _grayBackView.frame=self.view.bounds;
        _grayBackView.hidden=YES;
        [self.view addSubview:_grayBackView];
//        [_grayBackView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
//            [self hideSpecView];
//        }]];
    }
    return _grayBackView;
}
#pragma mark-Lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //注册键盘出现 消失
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor=BG_COLOR;
    self.tableViewStyle=UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50+kTabbarSafeBottomMargin, 0));
    }];
    [self getDetailInfo];
    [self setSubViews];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//     self.navigationController.navigationBarHidden=NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [self.specView.goodsNumField resignFirstResponder];
     self.specView.frame=CGRectMake(0, mainHeight-_specView.height, mainWidth,_specView.height);
    self.grayBackView.hidden=YES;
    self.specView.hidden=YES;
    _wholeSpecView.hidden=YES;
    _wholeSpecView.frame=CGRectMake(0, mainHeight-_wholeSpecView.height, mainWidth, _wholeSpecView.height);
    [_wholeSpecView endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
-(void)keyboardWillShow:(NSNotification *)notification
{
    //这样就拿到了键盘的位置大小信息frame，然后根据frame进行高度处理之类的信息
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if(self.goodSellType==GoodSellWholesale){
        CGRect tempFrame=_wholeSpecView.frame;
        [UIView animateWithDuration:0.1 animations:^{
            _wholeSpecView.frame=CGRectMake(tempFrame.origin.x,tempFrame.origin.y-frame.size.height , tempFrame.size.width, tempFrame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }else{
        CGRect tempFrame=self.specView.frame;
        [UIView animateWithDuration:0.1 animations:^{
            self.specView.frame=CGRectMake(tempFrame.origin.x,tempFrame.origin.y-frame.size.height , tempFrame.size.width, tempFrame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }
}
-(void)keyboardWillHidden:(NSNotification *)notification
{
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if(self.goodSellType==GoodSellWholesale){
        CGRect tempFrame=_wholeSpecView.frame;
        [UIView animateWithDuration:0.1 animations:^{
            _wholeSpecView.frame=CGRectMake(tempFrame.origin.x,tempFrame.origin.y+frame.size.height , tempFrame.size.width, tempFrame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }else{
        CGRect tempFrame=self.specView.frame;
        [UIView animateWithDuration:0.1 animations:^{
            self.specView.frame=CGRectMake(tempFrame.origin.x,tempFrame.origin.y+frame.size.height , tempFrame.size.width, tempFrame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }
}

//最终的购买
-(void)buyNowWithNum:(NSString *)goodsNum stock:(NSString *)stockNum{
    if ([stockNum integerValue]<=0) {
        [XSTool showToastWithView:self.view Text:@"库存不足"];
        return;
    }
     NSString *specString = [self sortSpecDictionaryWithSpecDic:self.specView.specDic];
    if (specString.length) {//有规格
        [XSTool showProgressHUDWithView:self.view];
        //获取某规格的商品信息
        @weakify(self);
        [ HTTPManager getProductExtForPreviewOrderWithId:[NSString stringWithFormat:@"%@",self.detailInfo.productInfo.product_id] spec:specString success:^(NSDictionary * _Nullable dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                NSDictionary *ext =[[NSDictionary alloc] initWithDictionary:dic[@"data"]];
                if (ext.allKeys.count>0) {
                    NSString *extId = [NSString stringWithFormat:@"%@",dic[@"data"][@"product_ext_id"]];
                    VerifyOrderViewController *verifyOrderController = [[VerifyOrderViewController alloc] init];
                    verifyOrderController.goodsNum =goodsNum;
                    verifyOrderController.extendId = extId;
                    verifyOrderController.addressId = @"";
                    if (self.goodSellType == GoodSellGroupBuy && self.isClickedGroupBuy) {
                        verifyOrderController.goodSellType = GoodSellGroupBuy;
                    } else{
                        verifyOrderController.goodSellType=self.goodSellType;
                    }
                    NSLog(@"addresss：：：：：%@",verifyOrderController.addressId);
                    [self.navigationController pushViewController:verifyOrderController animated:YES];
                } else {
                    [XSTool showToastWithView:self.view Text:@"信息不匹配！"];
                }
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常,请稍后重试"];
        }];
    }else{
        VerifyOrderViewController *verifyOrderController = [[VerifyOrderViewController alloc] init];
        verifyOrderController.goodsNum =goodsNum;
        verifyOrderController.extendId =[NSString stringWithFormat:@"%@",self.detailInfo.productInfo.product_ext_id];
        verifyOrderController.addressId = @"";
        if (self.goodSellType == GoodSellGroupBuy && self.isClickedGroupBuy) {
            verifyOrderController.goodSellType = GoodSellGroupBuy;
        }  else{
            verifyOrderController.goodSellType=self.goodSellType;
        }
        NSLog(@"addresss：：：：：%@",verifyOrderController.addressId);
        [self.navigationController pushViewController:verifyOrderController animated:YES];
    }
}
//对规格排序
-(NSString *)sortSpecDictionaryWithSpecDic:(NSDictionary *) specDic{
    NSString *specString = @"";
    if (specDic.allKeys.count>0) {
        NSMutableString *tempString = [NSMutableString string];
        //顺序为接口返回顺序
        for (int i=0;i<self.detailInfo.productInfo.spec.count;i++) {
            SpecItem *item = self.detailInfo.productInfo.spec[i];
            [tempString appendString:[NSString stringWithFormat:@"%@:%@;",item.spec_id,[specDic objectForKey:[NSString stringWithFormat:@"%@",item.spec_id]]]];
        }
        //去除最后的分号
        specString = [tempString substringToIndex:[tempString length]-1];
    }
    return specString;
}
//最终的加入购物车
-(void)addToCartWithNum:(NSString *)goodsNum stock:(NSString *)stockNum{
    if ([stockNum integerValue]<=0) {
        [XSTool showToastWithView:self.view Text:@"库存不足"];
        return;
    }
    NSString *specString=[self sortSpecDictionaryWithSpecDic:self.specView.specDic];
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getProductExtForPreviewOrderWithId:[NSString stringWithFormat:@"%@",self.detailInfo.productInfo.product_id] spec:specString success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            NSString *extId = [NSString stringWithFormat:@"%@",dic[@"data"][@"product_ext_id"]];
            [HTTPManager addGoodsToCartWithgoodsId:self.detailInfo.productInfo.product_id goodsNum:goodsNum extId:extId success:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:state.info];
                } else {
                    [XSTool showToastWithView:self.view  Text:state.info];
                }
                
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                NSLog(@"%@",error);
            }];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:_specView Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)hideSpecView{
    _specView.hidden=YES;
    _grayBackView.hidden=YES;
    [_specView.goodsNumField resignFirstResponder];
}
-(void)setSubViews{
    UIImageView *backImg=[[UIImageView alloc] init];
    backImg.image=[UIImage imageNamed:@"mall_detail_back"];
    backImg.userInteractionEnabled=NO;
    UIButton *backBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    backBtn.backgroundColor=[UIColor clearColor];
    [self.view addSubview:backBtn];
    [backBtn addSubview:backImg];
    [backImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backBtn);
        make.left.mas_equalTo(12);
    }];
    
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *tipsImg=[[UIImageView alloc] init];
    tipsImg.image=[UIImage imageNamed:@"mall_detail_more"];
    tipsImg.userInteractionEnabled=NO;
    UIButton *tipsBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-100, 0, 100, 100)];
    tipsBtn.backgroundColor=[UIColor clearColor];
    [self.view addSubview:tipsBtn];
    [tipsBtn addSubview:tipsImg];
    [tipsImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(tipsBtn);
    }];
    [tipsBtn addTarget:self action:@selector(showTipsView) forControlEvents:UIControlEventTouchUpInside];
    self.tabView=[[GoodsDetailTabView alloc] init];
    self.tabView.delegate=self;
    [self.view addSubview:self.tabView];
}
-(void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showTipsView{
    if(self.tipsView.hidden){
        self.tipsView.hidden=NO;
    }else{
        self.tipsView.hidden=YES;
    }
}
-(void)getDetailInfo{
    if (self.goodsID.length) {
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        NSMutableDictionary *params = [@{@"id":self.goodsID} mutableCopy];
        if (!isEmptyString(self.toId)) {
            [params setObject:self.toId forKey:@"toid"];
        }
        [HTTPManager getGoodsDetailWithParams:params success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                self.detailInfo=[GoodsDetailInfo mj_objectWithKeyValues:dic[@"data"]];
                [self judgeGoodsType];
                 [self setSpecString];
                [self.tableView reloadData];
                self.tabView.isFavorite=self.detailInfo.isFavorite;
                //获取商家统计信息和详情
                [HTTPManager getGoodsStoreStatisticsInfoWithStoreId:self.detailInfo.supplyInfo.id success:^(NSDictionary *dic, resultObject *state) {
                    if (state.status) {
                        self.statisticsModel=[GoodsStoreStatisticsModel mj_objectWithKeyValues:dic[@"data"]];
                        [self.tableView reloadData];
                    }
                } fail:^(NSError *error) {
                }];
                //获取推荐商品
                [HTTPManager getGoodsDetailRecommendGoodsWithDic:@{@"supply_id":self.detailInfo.supplyInfo.id,@"sell_type":self.detailInfo.productInfo.sell_type} Success:^(NSDictionary *dic, resultObject *state) {
                    if (state.status) {
                        self.recommendArray=[NSMutableArray arrayWithArray:[RecommendProduct mj_objectWithKeyValues:dic[@"data"]].data];
                        [self.tableView reloadData];
                    }
                } fail:^(NSError *error) {
                    
                }];
                if (VUE_ON) {
                    [HTTPManager getGoodsDetailPictureInfoWithDic:@{@"product_id":self.goodsID} Success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        if (state.status) {
                            self.detailPictureDic=dic;
                            NSString *htmls = [NSString stringWithFormat:@"<style type=\"text/css\"> \n"
                                               "img {width:100%%; height:auto;}\n"
                                               "</style> \n"
                                               "%@",self.detailPictureDic[@"data"]];
                            
                            [self.webView loadHTMLString:htmls baseURL:[NSURL URLWithString:ImageBaseUrl]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool showToastWithView:self.view Text:NetFailure];
                    }];
                }else{
                    NSURLRequest *request=[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/mobile/product/description/id/%@/device/app",ImageBaseUrl,self.detailInfo.productInfo.product_id]]];
                    [self.webView loadRequest:request];
                }
            }else{
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }else{
        [XSTool showToastWithView:self.view Text:@"商品已下架"];
    }
    
}
//判断产品类型  其他的根据前面界面的传值
-(void)judgeGoodsType{
   //    promotion=  normal未促销   second秒杀    group团购   book预售
    switch ([self.detailInfo.productInfo.sell_type integerValue]) {
        case 1:
        {
            self.goodSellType=GoodSellNormal;//普通产品
            if ([self.detailInfo.promotion.promotion isEqualToString:@"second"]) {
                self.goodSellType=GoodSellSecondKill;
            }else if ([self.detailInfo.promotion.promotion isEqualToString:@"group"]) {
                self.goodSellType=GoodSellGroupBuy;
            }else if ([self.detailInfo.promotion.promotion isEqualToString:@"book"]) {
                self.goodSellType=GoodSellBook;
            }else if (!isEmptyString(self.toId)) {
                self.goodSellType = GoodSellFillPrice;
            }
        }
            break;
        case 2:
            self.goodSellType=GoodSellScore;//积分产品
            break;
        case 100:
            self.goodSellType=GoodSellWholesale;//批发产品
            break;
        default:
            break;
    }
    
    self.tabView.type=self.goodSellType;
}
//检测是否登录
-(BOOL)ifHaveLogined{
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        [self xs_pushViewController:nil];
        return NO;
    }
    return YES;
}
-(void)shareGoods{
    if(![self ifHaveLogined]){
        return;
    }
    [XSShareView creatShareView:self.detailInfo.productInfo.product_id viewController:self type:ShareTypeProduct];
}

-(void)setSpecString{
    UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if (self.goodSellType==GoodSellWholesale) {
        if (self.detailInfo.productInfo.spec.count>0) {
            NSMutableString *mStr=[NSMutableString stringWithString:@"选择："];
            for (SpecItem *item in self.detailInfo.productInfo.spec) {
                NSString *str=[NSString stringWithFormat:@"%@，",item.spec_name];
                [mStr appendString:str];
            }
            NSString *tempStr=[mStr substringToIndex:mStr.length-1];
            long length1=@"选择：".length;
            NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:tempStr];
            
            [attri addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(length1, tempStr.length-length1)];
            cell.textLabel.attributedText=attri;
            if (self.detailInfo.productInfo.spec.count) {
                for (SpecItem *item in self.detailInfo.productInfo.spec) {
                    if (item.spec_value.count>1) {
                        return;
                    }
                }
                NSMutableString *specNameStr=[NSMutableString stringWithString:@"已选择"];
                for (int i=0; i<self.detailInfo.productInfo.spec.count; i++) {
                    SpecItem *item=self.detailInfo.productInfo.spec[i];
                    Spec_valueItem *firstItem=item.spec_value.firstObject;
                    [specNameStr appendString:[NSString stringWithFormat:@"%@%@%@",@"\"",firstItem.spec_value_name,@"\""]];
                }
                cell.textLabel.text=specNameStr;
            }
        }else{
            cell.textLabel.text=@"选择:";
        }
    }else{
        cell.textLabel.attributedText=self.specView.showSpecStr;
    }

}
//规格选择完成  或者规格全部单选
-(BOOL)ifSpecChooseCompleted{
    return self.specView.specDic.allKeys.count==self.detailInfo.productInfo.spec.count;
}
-(void)showSpecView{
    if (self.goodSellType==GoodSellWholesale) {
        self.wholeSpecView.hidden=NO;
        self.wholeGrayView.hidden=NO;
    }else{
        self.grayBackView.hidden=NO;
        self.specView.hidden=NO;
        self.specView.isClickedBuyNowBtn=NO;
        self.isclickedAddToCart=NO;
    }
}
#pragma mark-TableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 2;
    }else if (section==1 || section==2 || section==4 || section==5){
        return 1;

    }else if (section==3){
        return (self.detailInfo.productEvaluation.count+1);
    }
    return self.recommendArray.count?((self.recommendArray.count-1)/2+1):0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 7;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return mainWidth;
        }else{
            return [self.heightDic[[NSString stringWithFormat:@"%li-%li",(long)indexPath.section,(long)indexPath.row]] floatValue];
        }
    }else if (indexPath.section==1){
        return [WholesaleCell cellHeight:self.detailInfo];
    }else if (indexPath.section==2){
        return 44;
    }else if(indexPath.section==3){
        if (indexPath.row==0) {
            return 49;
        }else{
            return  [self.heightDic[[NSString stringWithFormat:@"%li-%li",(long)indexPath.section,(long)indexPath.row]] floatValue];
        }
    }else if(indexPath.section==4){
        return 201;
    }else if(indexPath.section==5){
        return self.webView.frame.size.height;
    }else{
        return  78+(mainWidth-3)/2.0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==2 || section==3 || section==4){
        return 33;
    }
    if (section == 1 && !self.detailInfo.productInfo.wholesale_config_app.price) {
        return 0.1;
    }
    return 12;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            static NSString *cellId=@"scrollCell";
            GoodsDetailTopScrollCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
            if (!cell) {
                cell=[[GoodsDetailTopScrollCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                cell.currentVC=self;
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
//            cell.delegate = self;
            cell.imageList=self.detailInfo.productInfo.image_list;
            return cell;
        }else{
            static NSString *cellId=@"infoCell";
            GoodsDetailTopInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
            if (!cell) {
                cell=[[GoodsDetailTopInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.goodsDetailInfo=self.detailInfo;
            if (self.goodSellType == GoodSellGroupBuy && self.detailInfo.promotion) {
                cell.goodSellType = GoodSellGroupBuy;
                cell.groupBuyDetailInfo = self.detailInfo.promotion;
            } else if (self.goodSellType == GoodSellSecondKill && self.detailInfo.promotion) {
                cell.goodSellType = GoodSellSecondKill;
                cell.groupBuyDetailInfo = self.detailInfo.promotion;
            }
            cell.delegate=self;
            self.heightDic[[NSString stringWithFormat:@"%i-%i",indexPath.section,indexPath.row]]=@(cell.height);
            return cell;
        }
    }else if (indexPath.section==1){
        WholesaleCell *cell = [WholesaleCell createWholesaleCellWithTableVie:tableView];
        cell.goodDetailInfo = self.detailInfo;
        return cell;
    }else if (indexPath.section==2){
        static NSString *cellId=@"specCell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.font=[UIFont systemFontOfSize:15];
            cell.textLabel.textColor=[UIColor hexFloatColor:@"A1A1A1"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
        [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            [self showSpecView];
        }]];
        return cell;
    }else if (indexPath.section==3){
        if (indexPath.row==0) {
            static NSString *cellId=@"evaSumCell";
            GoodsDetailSumEvaluationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
            if (!cell) {
                cell=[[GoodsDetailSumEvaluationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.detailInfo=self.detailInfo;
            [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
                GoodsDetailEvaluateVC *evaVC=[[GoodsDetailEvaluateVC alloc] init];
                evaVC.goodsId=self.goodsID;
                evaVC.titleArray=@[[NSString stringWithFormat:@"全部(%@)",self.detailInfo.productEvaSum.sum],[NSString stringWithFormat:@"晒图(%@)",self.detailInfo.productEvaSum.img],[NSString stringWithFormat:@"%@(%@)",Localized(@"favourable"),self.detailInfo.productEvaSum.good],[NSString stringWithFormat:@"中评(%@)",self.detailInfo.productEvaSum.normal],[NSString stringWithFormat:@"差评(%@)",self.detailInfo.productEvaSum.bad]];
                [self.navigationController pushViewController:evaVC animated:YES];
            }]];
            return cell;
        }else{
            static NSString *cellId=@"evaCell";
            GoodsEvaluationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
            if (!cell) {
                cell=[[GoodsEvaluationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.contentView.backgroundColor=[UIColor whiteColor];
            }
            cell.currentVC=self;
            cell.evaItem=self.detailInfo.productEvaluation[indexPath.row-1];
            self.heightDic[[NSString stringWithFormat:@"%i-%i",indexPath.section,indexPath.row]]=@(cell.height);
            return cell;
        }
    }else if (indexPath.section==4){
        static NSString *cellId=@"sellerInfoCell";
        GoodsDetailSellerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[GoodsDetailSellerInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.delegate=self;
        cell.detailInfo=self.detailInfo;
        cell.statisticsModel=self.statisticsModel;
        return cell;
    }else if (indexPath.section==5){
        static NSString *cellId=@"webCell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.contentView addSubview:self.webView];
        }
        return cell;
    }else{
        static NSString *cellId=@"recommendCell";
        GoodsDetailRecommendGoodsCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[GoodsDetailRecommendGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.delegate=self;
        if((indexPath.row+1)*2-1>self.recommendArray.count-1){
            cell.recommendArray=@[self.recommendArray[(indexPath.row+1)*2-2]];
        }else{
            cell.recommendArray=@[self.recommendArray[(indexPath.row+1)*2-2],self.recommendArray[(indexPath.row+1)*2-1]];
        }
        return cell;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==2) {
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 33)];
        UIButton *btn=[UIButton new];
        [view addSubview:btn];
        [btn setImage:[UIImage imageNamed:@"mall_detail_flower"] forState:UIControlStateNormal];
        [btn setTitle:@"宝贝评价" forState:UIControlStateNormal];
        [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        UIView *line1=[UIView new];
        [view addSubview:line1];
        line1.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        UIView *line2=[UIView new];
        [view addSubview:line2];
        line2.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(view);
        }];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(btn.mas_left).mas_offset(-7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btn.mas_right).mas_offset(7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        return view;
    }else if(section==3){
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 33)];
        UIButton *btn=[UIButton new];
        [view addSubview:btn];
        [btn setImage:[UIImage imageNamed:@"mall_detail_photo"] forState:UIControlStateNormal];
        [btn setTitle:@"宝贝描述" forState:UIControlStateNormal];
        [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        UIView *line1=[UIView new];
        [view addSubview:line1];
        line1.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        UIView *line2=[UIView new];
        [view addSubview:line2];
        line2.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(view);
        }];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(btn.mas_left).mas_offset(-7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btn.mas_right).mas_offset(7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        return view;
    }else if(section==4){
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 33)];
        UIButton *btn=[UIButton new];
        [view addSubview:btn];
        [btn setImage:[UIImage imageNamed:@"mall_detail_thumb"] forState:UIControlStateNormal];
        [btn setTitle:@"掌柜推荐" forState:UIControlStateNormal];
        [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        UIView *line1=[UIView new];
        [view addSubview:line1];
        line1.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        UIView *line2=[UIView new];
        [view addSubview:line2];
        line2.backgroundColor=[UIColor hexFloatColor:@"D4D4D4"];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(view);
        }];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(btn.mas_left).mas_offset(-7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btn.mas_right).mas_offset(7.5);
            make.centerY.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        return view;
    }
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tipsView.hidden=YES;
}
-(void)hidtipsView{
    self.tipsView.hidden=YES;
}


#pragma mark-TabView Delegate
-(void)clickTabContact{
    if(![self ifHaveLogined]){
        return;
    }
    if ([self.detailInfo.supplyInfo.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        [XSTool showToastWithView:self.view Text:@"无法与自己聊天"];
        return;
    }
#ifdef ALIYM_AVALABLE
    
    YWPerson *person = [[YWPerson alloc] initWithPersonId:self.detailInfo.supplyInfo.user_id];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = self.detailInfo.supplyInfo.showname;
    chatVC.avatarUrl = self.detailInfo.supplyInfo.logo;
    chatVC.hidesBottomBarWhenPushed = YES;
    
    UUMessageProductShareModel *model = [UUMessageProductShareModel new];
    model.goodTitle = self.detailInfo.productInfo.product_name;
    model.goodPrice = [NSString stringWithFormat:@"¥%@",self.detailInfo.productInfo.sell_price];
    model.goodId = self.detailInfo.productInfo.product_id;
    model.goodICo = self.detailInfo.productInfo.image;
    chatVC.productJsonString = model.mj_JSONString;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.detailInfo.supplyInfo.user_id type:EMConversationTypeChat createIfNotExist:YES];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.title = self.detailInfo.supplyInfo.showname;
    chatVC.avatarUrl = self.detailInfo.supplyInfo.logo;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.detailInfo.supplyInfo.user_id title:self.detailInfo.supplyInfo.showname avatarURLPath:self.detailInfo.supplyInfo.logo];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    
    UUMessageProductShareModel *model = [UUMessageProductShareModel new];
    model.goodTitle = self.detailInfo.productInfo.product_name;
    model.goodPrice = [NSString stringWithFormat:@"¥%@",self.detailInfo.productInfo.sell_price];
    model.goodId = self.detailInfo.productInfo.product_id;
    model.goodICo = self.detailInfo.productInfo.image;
    chatVC.productJsonString = model.mj_JSONString;
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
}
-(void)clickTabCollect{
    if(![self ifHaveLogined]){
        return;
    }
    @weakify(self);
    if ([self.tabView.isFavorite integerValue]==0) {
        [HTTPManager addCollectionInfoWithFavType:@"0" favId:[NSString stringWithFormat:@"%@",self.detailInfo.productInfo.product_id] success:^(NSDictionary * _Nullable dic, resultObject* state) {
            @strongify(self);
            if (state.status) {
                self.tabView.isFavorite=@"1";
                [XSTool showToastWithView:self.view Text:@"收藏成功"];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }else{
        [HTTPManager deleteCollectionInfoIsOffline:NO WithId:self.detailInfo.productInfo.product_id success:^(NSDictionary * _Nullable dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                self.tabView.isFavorite=@"0";
                [XSTool showToastWithView:self.view Text:@"已取消收藏"];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }
}
-(void)clickTabAddToCart{
    if(![self ifHaveLogined]){
        return;
    }
    if (![self ifSpecChooseCompleted]) {
        self.grayBackView.hidden=NO;
        self.specView.hidden=NO;
        self.specView.isClickedBuyNowBtn=YES;
        self.isclickedAddToCart=YES;
    }else{
        [self addToCartWithNum:self.specView.goodsNum stock:self.specView.currentStockNum];
    }
}
-(void)clickTabBuyNow{
    if(![self ifHaveLogined]){
        return;
    }
    self.isClickedGroupBuy=NO;
    if(self.goodSellType==GoodSellWholesale){
        self.wholeSpecView.hidden=NO;
        self.wholeGrayView.hidden=NO;
        return;
    }
    if (![self ifSpecChooseCompleted] || self.goodSellType == GoodSellFillPrice) {
        self.grayBackView.hidden=NO;
        self.specView.hidden=NO;
        self.specView.isClickedBuyNowBtn=YES;
        self.isclickedAddToCart=NO;
    }else{
       [self buyNowWithNum:self.specView.goodsNum stock:self.specView.currentStockNum];
    }
}
-(void)clickTabGroupBuy{
    if(![self ifHaveLogined]){
        return;
    }
    self.isClickedGroupBuy=YES;
    if (![self ifSpecChooseCompleted]) {
        self.grayBackView.hidden=NO;
        self.specView.hidden=NO;
        self.specView.isClickedBuyNowBtn=YES;
        self.isclickedAddToCart=NO;
    }else{
        [self buyNowWithNum:self.specView.goodsNum stock:self.specView.currentStockNum];
    }
}
#pragma mark-TipsView Delegate
-(void)clickViewWithIndex:(NSInteger)index{
    self.tipsView.hidden=YES;
    if (index==0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (index==1){
        CartVC *cartVC=[[CartVC alloc] init];
        [self xs_pushViewController:cartVC];
    }else if(index==2){
        GoodsQRCodeViewController *qrVC=[[GoodsQRCodeViewController alloc] init];
        qrVC.detailInfo=self.detailInfo;
        [self.navigationController pushViewController:qrVC animated:YES];
    }else{
        [self shareGoods];
    }
}
#pragma mark-Cell Delegate
-(void)share{
    [self shareGoods];
}
-(void)clickStoreName{
    S_StoreInformation *infoVC=[[S_StoreInformation alloc] init];
    infoVC.storeInfor=self.detailInfo.supplyInfo.id;
    [self.navigationController pushViewController:infoVC animated:YES];
}
-(void)clickAllGoods{
    ProductModel *model=[ProductModel new];
    model.uid=self.detailInfo.supplyInfo.id;
    ProductListViewController *listVC=[[ProductListViewController alloc] init];
    listVC.model=model;
    [self.navigationController pushViewController:listVC animated:YES];
}
-(void)clickNewGoods{
    ProductModel *model=[ProductModel new];
    model.uid=self.detailInfo.supplyInfo.id;
    model.recommend=@"4";
    ProductListViewController *listVC=[[ProductListViewController alloc] init];
    listVC.model=model;
    [self.navigationController pushViewController:listVC animated:YES];
}
-(void)clickContactCustomer{
    if(![self ifHaveLogined]){
        return;
    }
    if ([self.detailInfo.supplyInfo.user_id isEqualToString:[UserInstance ShardInstnce].uid]) {
        [XSTool showToastWithView:self.view Text:@"无法与自己聊天"];
        return;
    }
#ifdef ALIYM_AVALABLE
    YWPerson *person = [[YWPerson alloc] initWithPersonId:self.detailInfo.supplyInfo.user_id];
    YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
    YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
    chatVC.title = self.detailInfo.supplyInfo.showname;
    chatVC.avatarUrl = self.detailInfo.supplyInfo.logo;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.detailInfo.supplyInfo.user_id type:EMConversationTypeChat createIfNotExist:YES];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.title = self.detailInfo.supplyInfo.showname;
    chatVC.avatarUrl = self.detailInfo.supplyInfo.logo;
    [self.navigationController pushViewController:chatVC animated:YES];
#else
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:self.detailInfo.supplyInfo.user_id title:self.detailInfo.supplyInfo.showname avatarURLPath:self.detailInfo.supplyInfo.logo];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    [self.navigationController pushViewController:chatVC animated:YES];
#endif
    
}
-(void)clickGoStore{
    S_StoreInformation *infoVC=[[S_StoreInformation alloc] init];
    infoVC.storeInfor=self.detailInfo.supplyInfo.id;
    [self.navigationController pushViewController:infoVC animated:YES];
}
-(void)didClickGoods:(RecommendProductDataItem *)item{
    GoodsDetailViewController *detailVC=[[GoodsDetailViewController alloc] init];
    detailVC.goodsID=item.product_id;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark-SpecView Delegate
-(void)clickCloseBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum{
    [self hideSpecView];
    [self setSpecString];
}
-(void)clickBuyNowBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum{
    if(![self ifHaveLogined]){
        return;
    }else{
        if ([self ifSpecChooseCompleted]) {
           [self buyNowWithNum:[NSString stringWithFormat:@"%lu",goodsNum] stock:[NSString stringWithFormat:@"%lu",stockNum]];
        [self setSpecString];
        }else{
            [XSTool showToastWithView:self.view Text:@"请完善规格"];
        }
    }
}
-(void)clickConfirmBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum{
    if(![self ifHaveLogined]){
        return;
    }else{
        if ([self ifSpecChooseCompleted]) {
            if (self.isclickedAddToCart) {
                //加入购物车
                  [self addToCartWithNum:[NSString stringWithFormat:@"%lu",goodsNum] stock:[NSString stringWithFormat:@"%lu",stockNum]];
            }else{//立即购买
                [self buyNowWithNum:[NSString stringWithFormat:@"%lu",goodsNum] stock:[NSString stringWithFormat:@"%lu",stockNum]];
            }
            [self setSpecString];
        }else{
            [XSTool showToastWithView:self.view Text:@"请完善规格"];
        }
    }
}
-(void)clickAddGoodsToShoppingCartBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum{
    [self hideSpecView];
    if(![self ifHaveLogined]){
        return;
    }else{
        if ([self ifSpecChooseCompleted]) {
            [self addToCartWithNum:[NSString stringWithFormat:@"%lu",goodsNum] stock:[NSString stringWithFormat:@"%lu",stockNum]];
            [self setSpecString];
        }else{
            [XSTool showToastWithView:self.view Text:@"请完善规格"];
        }
    }
}
#pragma mark-WholeSpecView Delegate
-(void)clickCloseBtnWithSpecDic:(NSMutableArray *)specDicArray num:(int)num{
    self.wholeGrayView.hidden=YES;
    self.wholeSpecView.hidden=YES;
    [self.wholeGrayView endEditing:YES];
}
-(void)clickBuyBtnWithSpecDic:(NSMutableArray *)specDicArray num:(int)num{
    if(![self ifHaveLogined]){
        [_wholeGrayView endEditing:YES];
        self.wholeGrayView.hidden=YES;
        self.wholeSpecView.hidden=YES;
        return;
    }
    if (num==0) {
        [XSTool showToastWithView:self.wholeSpecView Text:@"请先填写商品数量"];
        return;
    }
    if (num<[self.detailInfo.productInfo.wholesale_config_app.moq intValue]) {
        [XSTool showToastWithView:self.wholeSpecView Text:@"低于最小起批数量"];
        return;
    }
    self.wholeGrayView.hidden=YES;
    self.wholeSpecView.hidden=YES;
    if (specDicArray.count) {
        VerifyOrderViewController *verifyOrderController = [[VerifyOrderViewController alloc] init];
        verifyOrderController.goodSellType = GoodSellWholesale;
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:specDicArray];
        for (int i=0;i<tempArray.count;i++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:tempArray[i]];
            [tempArray removeObjectAtIndex:i];
            if ([dic.allKeys containsObject:@"price"]) {
                [dic removeObjectForKey:@"price"];
            }
            [tempArray insertObject:dic atIndex:i];
        }
        verifyOrderController.paramsDictionary = @{@"ext":tempArray,@"id":self.detailInfo.productInfo.product_id};
        verifyOrderController.addressId = @"";
        [self.navigationController pushViewController:verifyOrderController animated:YES];
        
    } else {
        //之前没有加入过
        NSArray *specsArray = @[[NSMutableDictionary dictionaryWithDictionary:@{@"ids":self.detailInfo.productInfo.product_ext_id,@"num":@(num)}]];
        VerifyOrderViewController *verifyOrderController = [[VerifyOrderViewController alloc] init];
        verifyOrderController.goodSellType = GoodSellWholesale;
        verifyOrderController.paramsDictionary = @{@"ext":specsArray,@"id":self.detailInfo.productInfo.product_id};
        verifyOrderController.addressId = @"";
        [self.navigationController pushViewController:verifyOrderController animated:YES];
    }
}

#pragma mark-UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //获取到webview的高度
    CGFloat height = [[self.webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue];
    self.webView.frame = CGRectMake(self.webView.frame.origin.x,self.webView.frame.origin.y, mainWidth, height+15);
    [self.tableView reloadData];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [XSTool showToastWithView:self.view Text:@"商品详情加载失败,请稍后重试"];
    [self.tableView reloadData];
}
@end
