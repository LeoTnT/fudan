//
//  ADListViewController.m
//  App3.0
//
//  Created by nilin on 2017/10/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ADListViewController.h"
#import "ADDetailWebViewController.h"
#import "TabMallModel.h"
#import "DefaultView.h"
#import "XSFormatterDate.h"
#import "VueWebViewController.h"

@interface ADListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *noticeArray;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, assign) NSUInteger limit;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面
@end

@implementation ADListViewController
#pragma mark - lazy lodding
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStylePlain];
        _tableView.showsVerticalScrollIndicator =NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.bounces = NO;
        _tableView.rowHeight = 44;
//        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self getInfo];
        }];
        MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [self getMoreInfo];
        }];
        [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
        [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
        [footer setTitle:@"" forState:MJRefreshStateIdle];
        
        //设置字体大小和文字颜色
        footer.stateLabel.font=[UIFont systemFontOfSize:14];
        footer.stateLabel.textColor=[UIColor darkGrayColor];
        _tableView.mj_footer=footer;
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = view;
        
    }
    return _tableView;
}

- (NSMutableArray *)noticeArray {
    if (!_noticeArray) {
        _noticeArray = [NSMutableArray array];
    }
    return _noticeArray;
}

- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = @"暂无公告";
    }
    return _defaultView;
}
#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.limit = 15;
    self.navigationItem.title=@"系统公告";
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationController.navigationBarHidden = NO;
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = BG_COLOR;
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    [self getInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getInfo {
    self.page = 1;
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getMallNoticeInfoWithPage:self.page limit:self.limit success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.noticeArray removeAllObjects];
            NSArray *tempArray = [NSArray arrayWithArray:dic[@"data"][@"data"]];
            if (tempArray.count) {
                self.defaultView.hidden = YES ;
                if (self.tableView.hidden) {
                    self.tableView.hidden = NO;
                }
                for (NSDictionary *tempDic in tempArray) {
                    TabMallNoticeItem *item=[TabMallNoticeItem mj_objectWithKeyValues:tempDic];
                    [self.noticeArray addObject:item];
                   
                }
                [self.tableView reloadData];
            } else {
                self.defaultView.hidden = NO;
                self.tableView.hidden = YES;
            }
           
        }else{
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
       [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)getMoreInfo {
    self.page++;
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getMallNoticeInfoWithPage:self.page limit:self.limit success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
       [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSArray *tempArray = [NSArray arrayWithArray:dic[@"data"][@"data"]];
            if (tempArray.count) {
                for (NSDictionary *tempDic in tempArray) {
                    TabMallNoticeItem *item=[TabMallNoticeItem mj_objectWithKeyValues:tempDic];
                    [self.noticeArray addObject:item];
                    
                }
                [self.tableView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
                self.page--;
            }
            
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        @strongify(self);
        self.page--;
        [self.tableView.mj_footer endRefreshing];
      [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"provinceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.noticeArray.count>indexPath.row) {
        TabMallNoticeItem *item = self.noticeArray[indexPath.row];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        UILabel *titleLabel = [UILabel new];
        [cell.contentView addSubview:titleLabel];
        UILabel *descLabel = [UILabel new];
        [cell.contentView addSubview:descLabel];
        titleLabel.font = [UIFont qsh_systemFontOfSize:16];
        descLabel.font = [UIFont qsh_systemFontOfSize:14];
        descLabel.textColor = mainGrayColor;
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(13);
            make.width.mas_equalTo(mainWidth/3*2);
        }];
        [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(titleLabel.mas_right).with.mas_offset(5);
            make.right.mas_equalTo(cell.contentView);
        }];
        titleLabel.text = item.title;
        descLabel.text = [XSFormatterDate yearDateWithTimeIntervalString:item.w_time];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TabMallNoticeItem *item = self.noticeArray[indexPath.row];
    if (VUE_ON) {
        VueWebViewController *vueVC=[[VueWebViewController alloc] init];
        vueVC.title= item.title.length?item.title:@"公告";
        vueVC.noticeId=item.adver_id;
        vueVC.vueType=VueTypeNotice;
        vueVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:vueVC animated:YES];
    } else {
        ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
        adWeb.urlStr=[NSString stringWithFormat:@"%@/mobile/notice/view/id/%@/device/app",ImageBaseUrl,item.adver_id];
        adWeb.navigationItem.title = item.title.length?item.title:@"公告";
        adWeb.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:adWeb animated:YES];
    }
    

}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.noticeArray.count;
}



@end
