//
//  GuideTopScrollCell.m
//  App3.0
//
//  Created by xinshang on 2017/6/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GuideTopScrollCell.h"
#import "XRCarouselView.h"
#define PICTURESCROLLVIEWHEIGHT 160*HScale
#define PAGECONTROLHEIGHT 50

@interface GuideTopScrollCell()<XRCarouselViewDelegate>

@property(nonatomic,strong)XRCarouselView *carouselView;
@end

@implementation GuideTopScrollCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
    }
    return self;
}
- (void)setUpUI
{
    self.carouselView = [[XRCarouselView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.frame.size.height)];
    _carouselView.height = self.cellHeight;
    //设置占位图片,须在设置图片数组之前设置,不设置则为默认占位图
    _carouselView.placeholderImage = [UIImage imageNamed:@"no_pic"];
    //用代理处理图片点击
    _carouselView.delegate = self;
    //设置每张图片的停留时间，默认值为5s，最少为2s
    _carouselView.time = 2;
    [self.contentView addSubview:_carouselView];
    [_carouselView setPageColor:[UIColor whiteColor] andCurrentPageColor:TAB_SELECTED_COLOR];
}
#pragma mark-刷新滚动图片
-(void)setItemArray:(NSArray *)itemArray{
    [self setUpUI];
    _itemArray=itemArray;
    NSMutableArray *imageUrlArray=[NSMutableArray array];
    if ([itemArray.firstObject isKindOfClass:[HomeImgsModel class]]) {
        for (HomeImgsModel *model in itemArray) {
            [imageUrlArray addObject:[NSString stringWithFormat:@"%@",model.image]];
        }
    }else if ([itemArray.firstObject isKindOfClass:[NSString class]]) {
        for (NSString *str in itemArray) {
            [imageUrlArray addObject:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,str]];
        }
    }
    
    //设置图片数组及图片描述文字
    self.carouselView.imageArray = imageUrlArray;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)carouselView:(XRCarouselView *)carouselView clickImageAtIndex:(NSInteger)index{
    [self.delegate clickImageWithIndex:index];
}


@end
