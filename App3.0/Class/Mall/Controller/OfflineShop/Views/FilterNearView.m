//
//  FilterNearView.m
//  App3.0
//
//  Created by xinshang on 2017/7/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FilterNearView.h"

#define HeadViewH 44

@interface FilterNearView ()<UITableViewDelegate,UITableViewDataSource>
{
    
    CGFloat leftHeight;
}
@property(nonatomic,strong) UITableView *leftTableView;
@property(nonatomic,strong) UITableView *rightTableView;
@property(nonatomic,strong) NSMutableArray *leftArray;
@property(nonatomic,strong) NSMutableArray *rightFirstArr;
@property(nonatomic,strong) NSMutableArray *rightOtherArr;
@property(nonatomic,strong) NSMutableArray *rightArray;

@property(nonatomic,assign) NSInteger leftIndex;
@property(nonatomic,assign) NSInteger rightIndex;


@property(nonatomic,strong) NSString *county_code;
@property(nonatomic,strong) NSString *town_code;
@property(nonatomic,strong) NSString *county_name;
@property(nonatomic,strong) NSString *town_name;
@property(nonatomic,strong) NSString *distance;
@property(nonatomic,strong) NSString *distanceName;



@end


@implementation FilterNearView
-(instancetype)initWithCity_code:(NSString*)city_code array:(NSArray*)arr
{
    
    self = [super initWithFrame:CGRectMake(0, HeadViewH, mainWidth, mainHeight-HeadViewH)];
    if (self) {
        self.city_code = city_code;
        self.rightArray = [NSMutableArray arrayWithArray:arr];
        [self setUpUI];
        
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
    if (!_leftIndex) {
        _leftIndex = 0;
    }
//    [self getAddressArea:self.city_code level:@"3"];
    [self getCountyByCity:self.city_code];
    
}
//获取某城市下的区县
- (void)getCountyByCity:(NSString *)cityCode{
    [HTTPManager OffGetCountyByCityWithcity_code:cityCode
                                         success:^(NSDictionary *dic, resultObject *state) {
                                             if (state.status) {
                                                 NSArray *arr = dic[@"data"];
                                                 [self.leftArray addObject:@{@"name":@"附近"}];
                                                 for (int x = 0; x <arr.count ; x ++) {
                                                     [self.leftArray addObject:arr[x]];
                                                 }
                                                 [self loadDataDistance];
                                                 
                                             }else{
                                                 [XSTool hideProgressHUDWithView:self];
                                                 if (dic[@"info"]) {
                                                     [XSTool showToastWithView:self Text:dic[@"info"]];
                                                 }
                                             }
                                             [self.leftTableView reloadData];
                                             [self.rightTableView reloadData];
                                         } fail:^(NSError *error) {
                                             
                                         }];
    
}


//获取某城市下的街道
- (void)GetTownByCounty:(NSString *)countyCode{
    
    [HTTPManager OffGetTownByCountyWithCounty_code:countyCode
                                           success:^(NSDictionary *dic, resultObject *state) {
                                               
                                               if (state.status) {
                                                   NSArray *arr = dic[@"data"];
                                                   
                                                   [self.rightOtherArr removeAllObjects];
                                                   [self.rightOtherArr addObject:@{@"name":Localized(@"alls")}];
                                                   for (int x = 0; x <arr.count ; x ++) {
                                                       [self.rightOtherArr addObject:arr[x]];
                                                   }
                                                   self.rightArray = self.rightOtherArr;
                                                   
                                               }else{
                                                   [XSTool hideProgressHUDWithView:self];
                                                   if (dic[@"info"]) {
                                                       [XSTool showToastWithView:self Text:dic[@"info"]];
                                                   }
                                               }
                                               [self.leftTableView reloadData];
                                               [self.rightTableView reloadData];
                                           } fail:^(NSError *error) {
                                               
                                           }];
    
}



- (void)getAddressArea:(NSString *)areaID level:(NSString *)level{
    if ([level isEqualToString:@"3"]) {
        if (self.leftArray.count > 0){ [self.leftArray removeAllObjects];   }
    }
    [XSTool showProgressHUDWithView:self];
    //    [HTTPManager getAreaChildList:areaID
    //                            level:level
    //                          Succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
    //
    //                              if (state.status) {
    //                                  NSArray *arr = dic[@"data"];
    //                                  if ([level isEqualToString:@"3"]) {
    //                                      [self.leftArray addObject:@{@"name":@"附近"}];
    //                                      for (int x = 0; x <arr.count ; x ++) {
    //                                          [self.leftArray addObject:arr[x]];
    //                                      }
    //                                      [self loadDataDistance];
    //                                  }else{
    //                                      [XSTool hideProgressHUDWithView:self];
    //
    //                                      [self.rightOtherArr removeAllObjects];
    //                                      [self.rightOtherArr addObject:@{@"name":Localized(@"alls")}];
    //                                      for (int x = 0; x <arr.count ; x ++) {
    //                                          [self.rightOtherArr addObject:arr[x]];
    //                                      }
    //                                      self.rightArray = self.rightOtherArr;
    //
    //                                  }
    //
    //                              }else{
    //                                  [XSTool hideProgressHUDWithView:self];
    //                                  if (dic[@"info"]) {
    //                                      [XSTool showToastWithView:self Text:dic[@"info"]];
    //                                  }
    //                              }
    //                              [self.leftTableView reloadData];
    //                              [self.rightTableView reloadData];
    //
    //                          } failure:^(NSError * _Nonnull error) {
    //                              [XSTool hideProgressHUDWithView:self];
    //                              [XSTool showToastWithView:self Text:NetFailure];
    //                          }];
}


- (void)loadDataDistance
{
    // 距离
    if (self.rightFirstArr.count <= 0) {
        //        __weak __typeof__(self) wSelf = self;
        //          [HTTPManager getSearchListSuccess:^(NSDictionary *dic, resultObject *state) {
        //            [XSTool hideProgressHUDWithView:nil];
        //              [self.rightFirstArr addObject:@{@"value":@"不限",@"name":@"不限"}];
        //            if ([dic[@"status"] integerValue]==StatusTypeSuccess) {
        //                NSArray *distanceArr = dic[@"data"][@"distance"][@"value"];
        //                for (int i =0; i<distanceArr.count; i++) {
        //                    [wSelf.rightFirstArr addObject:distanceArr[i]];
        //                }
        //                self.rightArray = self.rightFirstArr;
        //            }else{
        //                if (dic[@"info"]) {
        //                    [XSTool showToastWithView:self Text:dic[@"info"]];
        //                }
        //            }
        //            [wSelf.rightTableView reloadData];
        //
        //        } fail:^(NSError *error) {
        //            [XSTool hideProgressHUDWithView:self];
        //            [XSTool showToastWithView:self Text:NetFailure];
        //
        //        }];
        
        
        
        
        __weak __typeof__(self) wSelf = self;
        //        [XSTool showProgressHUDWithView:nil];
        [HTTPManager OffGetFileterConditionsSuccess:^(NSDictionary *dic, resultObject *state)
         {
             [XSTool hideProgressHUDWithView:nil];
             if (state.status) {
                 self.rightFirstArr = [NSMutableArray arrayWithArray:dic[@"data"][@"distance"]];
                 self.rightArray = self.rightFirstArr;
             }else{
                 [XSTool showToastWithView:nil Text:state.info];
                 
             }
             [wSelf.rightTableView reloadData];
             
             
         } fail:^(NSError *error) {
             [XSTool hideProgressHUDWithView:nil];
             [XSTool showToastWithView:nil Text:NetFailure];
             
         }];
        
    }
}




-(UITableView *)leftTableView{
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width*0.3, self.bounds.size.height-HeadViewH) style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.tableFooterView=[[UIView alloc]init];
        _leftTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _leftTableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        [self addSubview:_leftTableView];
        _leftTableView.estimatedRowHeight = 0;
        _leftTableView.estimatedSectionHeaderHeight = 0;
        _leftTableView.estimatedSectionFooterHeight = 0;
    }
#ifdef __IPHONE_11_0
    if ([_leftTableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _leftTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }    }
    
#endif
    return _leftTableView;
}
-(UITableView *)rightTableView{
    
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(self.bounds.size.width*0.3+1, 0, self.bounds.size.width*0.7,self.bounds.size.height-HeadViewH) style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tableFooterView=[[UIView alloc]init];
        _rightTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _rightTableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        
        [self addSubview:_rightTableView];
        _rightTableView.estimatedRowHeight = 0;
        _rightTableView.estimatedSectionHeaderHeight = 0;
        _rightTableView.estimatedSectionFooterHeight = 0;
    }
#ifdef __IPHONE_11_0
    if ([_rightTableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _rightTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }    }
    
#endif
    return _rightTableView;
    
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        return self.leftArray.count;
    }else{
        
        return self.rightArray.count;
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (tableView == _leftTableView) {
        static NSString *CellIdentifier = @"LeftCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.tintColor = mainColor;
        
        if (self.leftArray.count >0) {
            NSDictionary *dic = self.leftArray[indexPath.row];
            cell.textLabel.text = dic[@"name"];
            
            if (indexPath.row == _leftIndex) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.textLabel.textColor = mainColor;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.textLabel.textColor = [UIColor darkTextColor];
            }
        }
        
        
    }else{
        
        static NSString *CellIdentifier = @"RightCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.tintColor = mainColor;
        
        if (self.rightArray.count >0) {
            NSDictionary *dic = self.rightArray[indexPath.row];
            if (self.leftIndex == 0) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",dic[@"name"]];
            }else{
                cell.textLabel.text = [NSString stringWithFormat:@"%@",dic[@"name"]];
            }
            
            if (indexPath.row == _rightIndex) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.textLabel.textColor = mainColor;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.textLabel.textColor = [UIColor darkTextColor];
            }
        }
        
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _leftTableView) {
        self.leftIndex = indexPath.row;
        
        NSDictionary *dic = self.leftArray[indexPath.row];
        if (indexPath.row == 0) {
            self.county_code = nil;
            self.town_code = nil;
            self.county_name = nil;
            self.town_name = nil;
            self.distance = nil;
            self.distanceName = nil;
            self.rightArray = self.rightFirstArr;
            [self.rightTableView reloadData];
        }else{
            self.county_code = dic[@"code"];
            self.county_name = dic[@"name"];
            self.town_code = nil;
            self.town_name = nil;
            self.distance = nil;
            self.distanceName = nil;
            self.rightIndex =0;
//            [self getAddressArea:self.county_code level:@"4"];
            [self GetTownByCounty:self.county_code];
        }
        
        
    }else{
        self.rightIndex = indexPath.row;
        NSDictionary *dic = self.rightArray[indexPath.row];
        if (self.leftIndex==0) {
            self.town_code = nil;
            self.town_name = nil;
            self.distance = dic[@"name"];
            self.distanceName =  dic[@"value"];
            
        }else{
            self.town_code = dic[@"code"];
            self.town_name = dic[@"name"];
            self.distance = nil;
            self.distanceName = nil;
        }
    
        self.NearBlock(self.county_code, self.county_name, self.town_code, self.town_name, self.distance,self.distanceName);
        self.hidden = YES;
        
        
    }
    [self.leftTableView reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(NSMutableArray *)leftArray
{
    if (!_leftArray) {
        _leftArray = [NSMutableArray array];
    }
    return _leftArray;
}
-(NSMutableArray *)rightArray{
    if (!_rightArray) {
        _rightArray = [NSMutableArray array];
    }
    return _rightArray;
}
-(NSMutableArray *)rightFirstArr{
    if (!_rightFirstArr) {
        _rightFirstArr = [NSMutableArray array];
    }
    return _rightFirstArr;
}

-(NSMutableArray *)rightOtherArr{
    if (!_rightOtherArr) {
        _rightOtherArr = [NSMutableArray array];
    }
    return _rightOtherArr;
}

@end
