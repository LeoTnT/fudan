//
//  CustomPinAnnotationView.h
//  App3.0
//
//  Created by xinshang on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//


#import <MapKit/MapKit.h>
#import "MyAnnotationOffLine.h"

@interface CustomPinAnnotationView : MKAnnotationView


@property (nonatomic,strong) UILabel *label;
//@property (nonatomic,strong) UIImageView *imgView;

@end
