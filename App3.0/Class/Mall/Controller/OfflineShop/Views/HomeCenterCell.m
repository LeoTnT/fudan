//
//  HomeCenterCell.m
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeCenterCell.h"
#import "HomeCustomBtn.h"

@interface HomeCenterCell()
//{
//    __weak id<HomeCenterCellDelegate>delegate;
//}
//@property (nonatomic,strong) NSArray *listArr;
@end



#define space    8.0
#define width  (mainWidth-4*space)/3


@implementation HomeCenterCell

//@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        

    }
    return self;
}


#pragma mark-刷新界面
-(void)setImagArray:(NSArray *)imagArray
{
//    _walletAccountArray=walletAccountArray;
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            for (UIView *view1 in view.subviews) {
                if ([view1 isKindOfClass:[UILabel class]]) {
                    ((UILabel *)view1).text= @"";
                    [(UILabel *)view1 removeFromSuperview];

                }
                if ([view1 isKindOfClass:[UIImageView class]]) {
                    [(UIImageView *)view1 removeFromSuperview];
                }
            }
        }
    }
    CGFloat imageWidth = width*1.0;
    CGFloat imageHeight = imageWidth*0.8;
    CGFloat imgSpace = (width-imageWidth)/2;
    CGFloat height = imageWidth*0.8+imgSpace+50;
    NSMutableArray *tempArray=[NSMutableArray array];
    for (int i = 0; i < imagArray.count; i++) {
        HomeImgsModel *model = imagArray[i];

        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(space+(space+width)*(i%3),space+(int)(i/3)*height, width, height)];
        [tempArray addObject:btn];
        btn.tag=i+1;
        [self addSubview:btn];
        
       
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(imgSpace, (width-imageWidth)/2, imageWidth, imageHeight)];
        [btn addSubview:imgView];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(imgSpace, CGRectGetMaxY(imgView.frame)+5, width, 20)];
//        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor blackColor];
        title.font = [UIFont systemFontOfSize:15];
        [btn addSubview:title];
        
        UILabel *detaileTile = [[UILabel alloc] initWithFrame:CGRectMake(imgSpace, CGRectGetMaxY(title.frame), width, 20)];
        detaileTile.textColor = [UIColor grayColor];
//        detaileTile.textAlignment = NSTextAlignmentCenter;
        detaileTile.font = [UIFont systemFontOfSize:15];
        [btn addSubview:detaileTile];
        
        NSString *imgUrl = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,model.image];
        [imgView getImageWithUrlStr:imgUrl andDefaultImage:DefaultImage];
        
        title.text = model.name;
        detaileTile.text = model.subtitle;
    
    }
    self.btnArr=tempArray;
    self.cellHeight=((imagArray.count-1)/3+1)*height+5;

}

@end
