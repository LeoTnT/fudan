//
//  HomeItemLocationCell.h
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HomeItemLocationCellDelegate <NSObject>
-(void)relodLocalAction:(UIButton *)sender;
@end



@interface HomeItemLocationCell : UITableViewCell
@property(nonatomic,strong) UILabel *locaLb;//位置
@property(nonatomic,weak)id<HomeItemLocationCellDelegate> delegate;

@end
