//
//  HomeItemLocationCell.m
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeItemLocationCell.h"
@interface HomeItemLocationCell ()

@property(nonatomic,strong) UIImageView *locaImg;//位置
@property(nonatomic,strong) UIImageView *rightImg;//
@property(nonatomic,strong) UIButton *locaBtn;//定位

@property(nonatomic,assign)CGFloat height;//

@end
@implementation HomeItemLocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI{
    
    _locaImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_verify_address"]];
    [self addSubview:_locaImg];
    [_locaImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        make.centerY.mas_equalTo(self);
    }];
    
    _locaLb = [[UILabel alloc] init];
    _locaLb.text = @"我的位置:";
    _locaLb.font = [UIFont systemFontOfSize:15];
    [self addSubview:_locaLb];
    [_locaLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_locaImg.mas_right).offset(2);
        make.width.mas_equalTo(mainWidth-90);
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(44);
    }];
    
    _rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_verify_address"]];
    [self addSubview:_rightImg];
    [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-12);
        make.centerY.mas_equalTo(self);
    }];
    
    
    _locaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_locaBtn addTarget:self action:@selector(relocationAction:) forControlEvents:UIControlEventTouchUpInside ];
    [self addSubview:_locaBtn];
    [_locaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(44);

    }];
    
    
}


- (void)relocationAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(relodLocalAction:)]) {
        [self.delegate relodLocalAction:sender];
    }
}
@end
