//
//  CustomPinAnnotationView.m
//  App3.0
//
//  Created by xinshang on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//


#import "CustomPinAnnotationView.h"

@implementation CustomPinAnnotationView


- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //        在大头针旁边加一个label
//        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
//        self.imgView.image = [UIImage imageNamed:@"offLine_info"];
//        [self addSubview:self.imgView];
        
        
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 150, 20)];
        self.label.textColor = [UIColor redColor];
//        self.label.centerY = self.centerY;
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.font = [UIFont systemFontOfSize:20];
        [self addSubview:self.label];
        
    }
    return self;
    
}



@end
