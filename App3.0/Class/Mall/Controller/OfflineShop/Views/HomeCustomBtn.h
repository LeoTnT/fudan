//
//  HomeCustomBtn.h
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeImgsModel.h"


@protocol HomeCustomBtnDelegate <NSObject>
- (void)areaButtonClickWithIndex:(NSInteger)index;

@end
@interface HomeCustomBtn : UIButton

@property (nonatomic, weak) id<HomeCustomBtnDelegate>delegate;

@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) UILabel *titleLb;
@property (nonatomic,strong) UILabel *detailTitleLb;
@property (nonatomic,strong) UIImageView *imgView;


@property (nonatomic,assign) NSInteger index;
/*
 Model: @image:图片名 @title:名称 @fontSize:字体大小 @scale:图片比例 @imgHeight:图片高度 @isCenter:文字是否居住显示
 */
- (HomeCustomBtn *)initWithFrame:(CGRect)frame Model:(HomeImgsModel *)model Scale:(CGFloat)scale fontSize:(CGFloat)font imageHeight:(CGFloat)imgHeight TextAilgnment:(BOOL)isCenter;
- (void)setTitleColor:(UIColor *)color;

@end
