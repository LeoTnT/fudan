//
//  HomeTopScorllCell.m
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeTopScorllCell.h"
#import "homeModel.h"

@interface HomeTopScorllCell ()<UIScrollViewDelegate>

@property(nonatomic,strong) UIPageControl *pageCtrl;

@end
@implementation HomeTopScorllCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //菜单滚动视图
        self.menuScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0)];
        self.menuScrollView.pagingEnabled=YES;
        self.menuScrollView.showsHorizontalScrollIndicator=NO;
        [self.contentView addSubview:self.menuScrollView];
    }
    return self;
}

#pragma mark-设置菜单
-(void)setItemArray:(NSArray *)itemArray{
    _itemArray=itemArray;
    self.menuArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.menuScrollView.subviews) {
        [view removeFromSuperview];
    }
    for (UIControl *view in self.subviews) {
        if ([view isKindOfClass:[UIPageControl class]]) {
            [view removeFromSuperview];
        }
    }
    //页数
    if(itemArray.count){
        NSInteger num=(itemArray.count-1)/8+1;
        [self.menuScrollView setContentSize:CGSizeMake(mainWidth*num, 0)];
        if (num>1) {
            self.menuScrollView.scrollEnabled=YES;
        }else{
            self.menuScrollView.scrollEnabled=NO;
        }
        self.menuScrollView.delegate = self;

        UIView *tempView;
        CGFloat leftSpace=23,width=47,space=(mainWidth-47*4-2*23)/3.0;
        for (int i=0; i<itemArray.count; i++) {
            int line=i/4%2;
            int col=i%4;
            
            @autoreleasepool {
                UIView *menuView=[[UIView alloc] initWithFrame:CGRectMake(leftSpace+(col)*(width+space)+mainWidth*(i/8),(line+1)*20+65*line, width, 80)];
                [self.menuScrollView addSubview:menuView];
                menuView.tag=i;
                [self.menuArray addObject:menuView];
                UIImageView *menuImage=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, width, width)];
                menuImage.contentMode = UIViewContentModeScaleAspectFit;
                menuImage.userInteractionEnabled=YES;
                [menuView addSubview:menuImage];
                UILabel *menuLabel=[[UILabel alloc] initWithFrame:CGRectMake(-16.5, 50, 80, 80-width)];
                menuLabel.textColor=[UIColor hexFloatColor:@"111111"];
                menuLabel.font=[UIFont systemFontOfSize:12];
                menuLabel.textAlignment=NSTextAlignmentCenter;
                [menuView addSubview:menuLabel];
                tempView=menuView;
                if ([itemArray.firstObject isKindOfClass:[homeItemModel class]]) {
                    homeItemModel *item=[itemArray objectAtIndex:i];
                    [menuImage getImageWithUrlStr:item.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                    menuLabel.text=item.name.length?item.name:@"--";
                    
                }else if ([itemArray.firstObject isKindOfClass:[homeCategoryListModel class]])
                {
                    homeCategoryListModel *item = [itemArray objectAtIndex:i];
                    [menuImage getImageWithUrlStr:item.logo andDefaultImage:DefaultImage];
                    menuLabel.text=item.category_name.length?item.category_name:@"--";
                }
                
            }
        }
        
        if(self.itemArray.count>4){
            self.menuScrollView.frame=CGRectMake(0, 0, mainWidth, 193);
            self.height=193;
        }else{
            self.menuScrollView.frame=CGRectMake(0, 0, mainWidth, 100);
            self.height=97;
        }
        
        NSInteger countNum = 1;
        if (self.itemArray.count <=8) {
            return;
        }else{
            countNum = countNum +self.itemArray.count/8;
        }
        
        //创建UIPageControl
        _pageCtrl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.height, mainWidth, 20)];
        _pageCtrl.numberOfPages = countNum;//总的图片页数
        _pageCtrl.currentPage = 0; //当前页
        [_pageCtrl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
        
        //设置分页控制点颜色
        _pageCtrl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];//未选中的颜色
         _pageCtrl.currentPageIndicatorTintColor = mainColor;//选中时的颜色
        [self addSubview:_pageCtrl];  //将UIPageControl添加到主界面上。
        self.height= self.height+20;

        
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //更新UIPageControl的当前页
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.frame;
    [_pageCtrl setCurrentPage:offset.x / mainWidth];
    NSLog(@"%f",offset.x / bounds.size.width);
}
//然后是点击UIPageControl时的响应函数pageTurn
- (void)pageTurn:(UIPageControl*)sender
{
    //令UIScrollView做出相应的滑动显示
    CGSize viewSize = self.menuScrollView.frame.size;
    CGRect rect = CGRectMake(sender.currentPage * viewSize.width, 0, viewSize.width, viewSize.height);
    [self.menuScrollView scrollRectToVisible:rect animated:YES];
}

@end

