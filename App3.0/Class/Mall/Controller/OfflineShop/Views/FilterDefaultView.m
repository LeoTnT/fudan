//
//  FilterDefaultView.m
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FilterDefaultView.h"
#define HeadViewH 44

@interface FilterDefaultView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *sorterArr;

@property(nonatomic,assign) NSInteger selectIndex;

@end


@implementation FilterDefaultView

-(instancetype)initWithTitle:(NSString*)title array:(NSArray*)arr
{
    self = [super init];
    if (self) {
        self.titleName = title;
        self.sorterArr = [NSMutableArray arrayWithArray:arr];
        self.frame = CGRectMake(0, HeadViewH, mainWidth, mainHeight-HeadViewH);
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
    [self loadData];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-HeadViewH) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView=[[UIView alloc]init];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        [self addSubview:_tableView];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
#ifdef __IPHONE_11_0
    if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }    }
    
#endif
    return _tableView;
}

- (void)loadData{
    
    if (self.sorterArr.count >0) {
        [self setIndexSeleted];
        [self.tableView reloadData];
        return;
    }
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:nil];
    [HTTPManager OffGetFileterConditionsSuccess:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:nil];
         if (state.status) {
             self.sorterArr = [NSMutableArray arrayWithArray:dic[@"data"][@"sorter"]];
         }else{
             [XSTool showToastWithView:nil Text:state.info];

         }
         [self setIndexSeleted];
         [wSelf.tableView reloadData];

     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:nil];
         [XSTool showToastWithView:nil Text:NetFailure];
         
     }];
}

- (void)setIndexSeleted
{
    if (!_selectIndex) {
        for (int i = 0; i<self.sorterArr.count; i++) {
            NSDictionary *dic = self.sorterArr[i];
            NSString *name =[NSString stringWithFormat:@"%@",dic[@"name"]];
            if ([name isEqualToString:self.titleName]) {
                _selectIndex = i;
            }
        }
    }
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.sorterArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProfessionSelectCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.tintColor = mainColor;
    if (self.sorterArr.count >0) {
        NSDictionary *dic = self.sorterArr[indexPath.row];
       cell.textLabel.text =[NSString stringWithFormat:@"%@",dic[@"name"]];
        if (indexPath.row == _selectIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.textColor = mainColor;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.textColor = [UIColor darkTextColor];
        }
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectIndex = indexPath.row;
    [self.tableView reloadData];
    self.hidden = YES;
    NSDictionary *dic = self.sorterArr[indexPath.row];
    self.DefaultBlock([NSString stringWithFormat:@"%@",dic[@"name"]], [NSString stringWithFormat:@"%@",dic[@"value"]]);
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(NSMutableArray *)sorterArr
{
    if (!_sorterArr) {
        _sorterArr = [NSMutableArray array];
    }
    return _sorterArr;
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.sorterArr.count >0) {
        NSDictionary *dic = self.sorterArr[_selectIndex];
        self.DefaultBlock([NSString stringWithFormat:@"%@",dic[@"name"]], [NSString stringWithFormat:@"%@",dic[@"value"]]);

        self.hidden = YES;
    }else{
        self.DefaultBlock(nil, nil);
    }
    self.hidden = YES;
    
}

@end



