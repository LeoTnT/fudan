//
//  HomeCustomCell.h
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeModel.h"
@interface HomeCustomCell : UITableViewCell
@property (nonatomic,strong) homeModel * model;

@end
