//
//  HomeItemCenterCell.m
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeItemCenterCell.h"
#import "homeModel.h"

@interface HomeItemCenterCell ()
@end
@implementation HomeItemCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //菜单滚动视图
        self.menuScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0)];
        self.menuScrollView.pagingEnabled=YES;
        self.menuScrollView.showsHorizontalScrollIndicator=NO;
        [self.contentView addSubview:self.menuScrollView];
    }
    return self;
}

#pragma mark-设置菜单
-(void)setItemArray:(NSArray *)itemArray{
    _itemArray=itemArray;
    self.menuArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.menuScrollView.subviews) {
        [view removeFromSuperview];
    }
    //页数
    if(itemArray.count){
        NSInteger num=(itemArray.count-1)/4+1;
        [self.menuScrollView setContentSize:CGSizeMake(mainWidth*num, 0)];
        if (num>1) {
            self.menuScrollView.scrollEnabled=YES;
        }else{
            self.menuScrollView.scrollEnabled=NO;
        }
        UIView *tempView;
        CGFloat leftSpace=0,itemW=mainWidth/2;
        CGFloat space=0;
        CGFloat itemH= 80;
        CGFloat imageW= 53;
        for (int i=0; i<itemArray.count; i++) {
            int line=i/2%2;
            int col=i%2;
            @autoreleasepool {
                homeItemModel *item=[itemArray objectAtIndex:i];
                UIView *menuView=[[UIView alloc] initWithFrame:CGRectMake(leftSpace+(col)*(itemW+space)+mainWidth*(i/4),itemH*line, itemW, itemH)];
                [self.menuScrollView addSubview:menuView];
                menuView.tag=i;
                [self.menuArray addObject:menuView];
                
                UIImageView *menuImage=[[UIImageView alloc] initWithFrame:CGRectMake(itemW-imageW-10,(itemH-imageW)/2, imageW, imageW)];
                menuImage.contentMode = UIViewContentModeScaleAspectFit;
                [menuImage getImageWithUrlStr:item.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                menuImage.userInteractionEnabled=YES;
                [menuView addSubview:menuImage];
                
                UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 12, itemW/2+10,25)];
                nameLabel.text=item.name.length?item.name:@"--";
                nameLabel.textColor=[UIColor hexFloatColor:@"111111"];
                nameLabel.font=[UIFont boldSystemFontOfSize:15];
//                nameLabel.textAlignment=NSTextAlignmentCenter;
                [menuView addSubview:nameLabel];
                
                UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 12+30, itemW/2+10,25)];
                titleLabel.text=item.name.length?item.name:@"--";
                titleLabel.textColor=[UIColor hexFloatColor:@"111111"];
                titleLabel.font=[UIFont systemFontOfSize:15];
//                titleLabel.textAlignment=NSTextAlignmentCenter;
                [menuView addSubview:titleLabel];
                
                UIView *line1=[[UIView alloc] initWithFrame:CGRectMake(itemW-0.5, 0, 0.5,itemH)];
                line1.backgroundColor = [UIColor hexFloatColor:@"E8E8E8"];
                [menuView addSubview:line1];
                
                UIView *line2=[[UIView alloc] initWithFrame:CGRectMake(0,itemH-0.5,itemW,0.5)];
                line2.backgroundColor = [UIColor hexFloatColor:@"E8E8E8"];
                [menuView addSubview:line2];
                
                
                tempView=menuView;
            }
        }
        if(self.itemArray.count>2){
            self.menuScrollView.frame=CGRectMake(0, 0, mainWidth, itemH*2);
            self.height=itemH*2;
        }else{
            self.menuScrollView.frame=CGRectMake(0, 0, mainWidth, itemH);
            self.height=itemH;
        }
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end


