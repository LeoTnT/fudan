//
//  HomeCenterCell.h
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeImgsModel.h"

//
//@protocol HomeCenterCellDelegate <NSObject>
//- (void)homeCenterBtnClick:(NSInteger)index;
//@end

@interface HomeCenterCell : UITableViewCell
//@property(nonatomic, weak) id<HomeCenterCellDelegate>delegate;
@property (nonatomic,strong) NSArray * modelArr;
@property (nonatomic,assign) NSInteger cellHeight;
@property (nonatomic,strong) NSArray *imagArray;
@property (nonatomic,strong) NSArray *btnArr;


@end
