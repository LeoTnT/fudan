//
//  HomeCustomBtn.m
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeCustomBtn.h"


@interface HomeCustomBtn()
{
    __weak id<HomeCustomBtnDelegate>delegate;

}
@end

@implementation HomeCustomBtn

@synthesize delegate,index;
- (HomeCustomBtn *)initWithFrame:(CGRect)frame Model:(HomeImgsModel *)model Scale:(CGFloat)scale fontSize:(CGFloat)font imageHeight:(CGFloat)imgHeight TextAilgnment:(BOOL)isCenter
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat imageWidth = 47;
       _imgView = [[UIImageView alloc]initWithFrame:CGRectMake((frame.size.width-imageWidth)/2, 8, imageWidth, imageWidth)];

        [_imgView setImage:[UIImage imageNamed:model.image]];
        [self addSubview:_imgView];
        
        
        //title
        _titleLb = [[UILabel alloc]initWithFrame:CGRectMake(0,_imgView.frame.size.height+_imgView.frame.origin.y+8,frame.size.width , 20)];
        [_titleLb setText:model.name];
//        [_titleLabel setText:model[@"title"]];
        [_titleLb setFont:[UIFont systemFontOfSize:font]];
        if (isCenter) {
            _titleLb.textAlignment = NSTextAlignmentCenter;
        }
        //        [titleLabel setTextColor:[UIColor grayColor]];
        [self addSubview:_titleLb];
        
        //DetailTitle
        //title
        _detailTitleLb = [[UILabel alloc]initWithFrame:CGRectMake(0,_imgView.frame.size.height+_imgView.frame.origin.y+30,frame.size.width , 20)];
        [_detailTitleLb setText:model.subtitle];
//        [_detailTitleLb setText:model[@"detailTitle"]];
        [_detailTitleLb setFont:[UIFont systemFontOfSize:font]];
        if (isCenter) {
            _detailTitleLb.textAlignment = NSTextAlignmentCenter;
        }
        [_detailTitleLb setTextColor:[UIColor grayColor]];
        [self addSubview:_detailTitleLb];

        
        [self addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)btnAction {
    if (delegate) {
        [delegate areaButtonClickWithIndex:self.index];
    }
}

- (void)setTitleColor:(UIColor *)color
{
    [_titleLb setTextColor:color];
}
@end
