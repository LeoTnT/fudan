//
//  FilterAllView.h
//  App3.0
//
//  Created by xinshang on 2017/7/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeModel.h"

@interface FilterAllView : UIView

@property (nonatomic,strong) NSString * titleName;
//@property (nonatomic,copy) void(^AllBlock)(homeCategoryListModel *model);
@property (nonatomic,copy) void(^AllBlock)(NSString *ID,NSString *name);
-(instancetype)initWithTitle:(NSString*)title industryId:(NSString*)industryId;

@end
