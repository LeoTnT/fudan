//
//  FilterNearView.h
//  App3.0
//
//  Created by xinshang on 2017/7/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterNearView : UIView
@property (nonatomic,strong) NSString*city_code;

@property (nonatomic,copy) void(^NearBlock)(NSString *county_code,NSString *county_name,NSString *town_code,NSString *town_name,NSString *distance,NSString *distanceName);


-(instancetype)initWithCity_code:(NSString*)city_code array:(NSArray*)arr;

@end
