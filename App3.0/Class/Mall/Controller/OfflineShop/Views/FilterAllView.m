 //
//  FilterAllView.m
//  App3.0
//
//  Created by xinshang on 2017/7/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FilterAllView.h"

@interface FilterAllView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *allArray;

@property(nonatomic,assign) NSInteger selectIndex;
@property (nonatomic,strong) NSString * industryId;

@end

#define HeadViewH 44

@implementation FilterAllView

-(instancetype)initWithTitle:(NSString*)title industryId:(NSString*)industryId
{
    self = [super init];
    if (self) {
        self.titleName = title;
        self.industryId = industryId;
        self.frame = CGRectMake(0, HeadViewH, mainWidth, mainHeight-HeadViewH);
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
    [self loadData];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-HeadViewH-10) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView=[[UIView alloc]init];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        [self addSubview:_tableView];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
#ifdef __IPHONE_11_0
    if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }    }
    
#endif
    return _tableView;
}

- (void)loadData{
    
 
    if (isEmptyString(self.industryId)) {
     
        [self getAllIndustry];
    }else{
      
        
        [self getOtherIndustry];
    }

    
    

}
//获取全部行业
- (void)getAllIndustry{
    
//    if (self.allArray.count >0) {
//        if (!_selectIndex) {
//            for (int i = 0; i<self.allArray.count; i++) {
//                homeItemModel *model = self.allArray[i];
//                if ([model.name isEqualToString:self.titleName]) {
//                    _selectIndex = i;
//                }
//            }
//        }
//        [self.tableView reloadData];
//        return;
//    }
//
    //全部行业
    __weak __typeof__(self) wSelf = self;
    [HTTPManager OffSupplyIndustryListsSuccess:^(NSDictionary *dic, resultObject *state)
     {
         //         [wSelf tableViewEndRefreshing];
         if (state.status) {
             [self.allArray removeAllObjects];
             homeItemModel *model = [[homeItemModel alloc] init];
             model.ID = @"";
             model.name = Localized(@"alls");
             [self.allArray addObject:model];
             [self.allArray addObjectsFromArray:[homeItemModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
         }else{
             if (dic[@"info"]) {
                 [XSTool showToastWithView:nil Text:dic[@"info"]];
             }
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:nil];
         [XSTool showToastWithView:nil Text:NetFailure];
     }];
}
//获取指定行业下行业
- (void)getOtherIndustry
{
//    if (self.allArray.count >0) {
//        if (!_selectIndex) {
//            for (int i = 0; i<self.allArray.count; i++) {
//                homeCategoryListModel *model = self.allArray[i];
//                if ([model.category_name isEqualToString:self.titleName]) {
//                    _selectIndex = i;
//                }
//            }
//        }
//        [self.tableView reloadData];
//        return;
//    }
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:nil];
    
    [HTTPManager OffIndustrySubListsWithIndustry:self.industryId
                                         success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:nil];
         if (state.status) {
             [self.allArray removeAllObjects];
             homeCategoryListModel *model = [[homeCategoryListModel alloc] init];
             model.category_id = @"";
             model.category_name = Localized(@"alls");
             [self.allArray addObject:model];
             [self.allArray addObjectsFromArray:[homeCategoryListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
         }else{
             if (dic[@"info"]) {
                 [XSTool showToastWithView:nil Text:dic[@"info"]];
             }
         }
         if (!_selectIndex) {
             for (int i = 0; i<wSelf.allArray.count; i++) {
                 homeCategoryListModel *model = self.allArray[i];
                 if ([model.category_name isEqualToString:wSelf.titleName]) {
                     _selectIndex = i;
                 }
             }
         }
         
         [wSelf.tableView reloadData];
         
     }fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:nil];
         [XSTool showToastWithView:nil Text:NetFailure];
     }];
    
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.allArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProfessionSelectCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.tintColor = mainColor;
    if (self.allArray.count >0) {
        if (self.industryId.length <= 0) {//全部
            homeItemModel *model = self.allArray[indexPath.row];
            cell.textLabel.text = model.name;
        }else{
        
        homeCategoryListModel *model = self.allArray[indexPath.row];
        cell.textLabel.text = model.category_name;
       
        }
        
        
        if (indexPath.row == _selectIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.textColor = mainColor;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.textColor = [UIColor darkTextColor];
        }
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectIndex = indexPath.row;
    [self.tableView reloadData];
    self.hidden = YES;
    if (isEmptyString(self.industryId)) {
        homeItemModel *model = self.allArray[indexPath.row];
        self.AllBlock(model.ID, model.name);
    }else{
         homeCategoryListModel *model = self.allArray[indexPath.row];
        self.AllBlock(model.category_id, model.category_name);
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(NSMutableArray *)allArray
{
    if (!_allArray) {
        _allArray = [NSMutableArray array];
    }
    return _allArray;
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.allArray.count >0) {
        if (self.industryId) {
            homeCategoryListModel *model = self.allArray[0];
            self.AllBlock(model.category_id, model.category_name);
            self.hidden = YES;
        }else{
            homeItemModel *model = self.allArray[0];
            self.AllBlock(model.ID, model.name);
            self.hidden = YES;
        }
      
    }else{
        self.AllBlock(nil, nil);
    }
    self.hidden = YES;
    
}

@end

