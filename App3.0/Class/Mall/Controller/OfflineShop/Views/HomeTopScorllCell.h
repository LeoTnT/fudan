//
//  HomeTopScorllCell.h
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTopScorllCell : UITableViewCell
/**滚动式图下的类目数组*/
@property(nonatomic,strong)NSArray *itemArray;
/**高度*/
@property(nonatomic,assign)CGFloat height;
/**菜单滚动式图*/
@property(nonatomic,strong)UIScrollView *menuScrollView;
/**类目视图数组*/
@property(nonatomic,strong)NSMutableArray *menuArray;

///**滚动式图下的二级类目数组*/
//@property(nonatomic,strong)NSArray *categoryArray;


@end
