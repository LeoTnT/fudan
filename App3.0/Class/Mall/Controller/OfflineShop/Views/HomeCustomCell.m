//
//  HomeCustomCell.m
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HomeCustomCell.h"
#import "XSStarRateView.h"


@interface HomeCustomCell()
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *nameLb;

@property (nonatomic,strong) UILabel *numLb;
@property (nonatomic,strong) UILabel *categoryLb;
@property (nonatomic,strong) UILabel *locaLb;
@property (nonatomic,strong) XSStarRateView *starRateView;


@end
@implementation HomeCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}


-(void)setUpUI{
    
    CGFloat space = 10.0;
    CGFloat imageWidth = mainWidth/4;
    CGFloat imageHeight = imageWidth*0.8;
    CGFloat lbHeight = 20.0;
    //imageView
    _imgView = [[UIImageView alloc]init];
//    _imgView.image = DefaultImage;
    [self addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(8.0);
        make.top.mas_equalTo(space);
        make.width.mas_equalTo(imageWidth);
        make.height.mas_equalTo(imageHeight);
    }];
    
    //nameLb
    _nameLb = [[UILabel alloc] init];
    _nameLb.font = [UIFont systemFontOfSize:16];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_imgView.mas_right).offset(8);
        make.top.mas_equalTo(_imgView).offset(-1);
        make.width.mas_equalTo(mainWidth*3/4 - 70);
        make.height.mas_equalTo(lbHeight);
    }];
    
    //_categoryLb
    _categoryLb = [[UILabel alloc] init];
    _categoryLb.font = [UIFont systemFontOfSize:14];
    _categoryLb.textColor = COLOR_666666;
    [self addSubview:_categoryLb];
    [_categoryLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_imgView.mas_right).offset(8);
        make.bottom.mas_equalTo(_imgView).offset(-1);
        make.width.mas_equalTo(mainWidth*3/4 - 70);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    //_numLb
    _numLb = [[UILabel alloc] init];
    _numLb.font = [UIFont systemFontOfSize:15];
    _numLb.textAlignment = NSTextAlignmentRight;
    _numLb.textColor = COLOR_666666;
    [self addSubview:_numLb];
    [_numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-5);
        make.bottom.mas_equalTo(_imgView).offset(-1);
        make.width.mas_equalTo(mainWidth*3/4 - 70);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    _starRateView = [[XSStarRateView alloc] initWithFrame:CGRectMake(imageWidth+16, 10+imageHeight-42, 15*5, 15)];
    _starRateView.isAnimation = YES;
    _starRateView.rateStyle = WholeStar;
    _starRateView.tag = 1;
//    _starRateView.delegate = self;
    _starRateView.currentScore = 5;
    _starRateView.userInteractionEnabled = NO;
    [self addSubview:_starRateView];
    [_starRateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb);
        make.bottom.mas_equalTo(_numLb.mas_top).offset(-2);
        make.width.mas_equalTo(15*5);
        make.height.mas_equalTo(15);
    }];
    
    
    //locaLb
    _locaLb = [[UILabel alloc] init];
    _locaLb.font = [UIFont systemFontOfSize:15];
    _locaLb.textColor = [UIColor darkGrayColor];
    _locaLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_locaLb];
    [_locaLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_numLb);
        make.bottom.mas_equalTo(_numLb.mas_top).offset(-2);
        make.left.mas_equalTo(_starRateView.mas_right).offset(2);
        make.height.mas_equalTo(lbHeight);
    }];
    
  
    
    _nameLb.text = @"";
    _locaLb.text = @"";
    _numLb.text = @"";
    _imgView.image =DefaultImage;
    _categoryLb.text = @"";

}

-(void)setModel:(homeModel *)model
{
    _model = model;
    _nameLb.text = [NSString stringWithFormat:@"%@",model.name];
    _locaLb.text = [NSString stringWithFormat:@"%@ %@km",model.address,model.distance];
    _numLb.text = [NSString stringWithFormat:@"%@人消费",model.sell_num];
    [_imgView getImageWithUrlStr:[NSString stringWithFormat:@"%@",model.logo] andDefaultImage:DefaultImage];
    _categoryLb.text = [NSString stringWithFormat:@"%@",model.industry_name];
    _starRateView.currentScore = model.eva.intValue;

}

- (CGFloat)getContentWidthWithStr:(NSString*)str Font:(UIFont*)font
{
    CGFloat lb_height = 16;
    
    NSString *commentStr = [NSString stringWithFormat:@"%@",str];
    CGSize textSize = [commentStr boundingRectWithSize:CGSizeMake(MAXFLOAT, lb_height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    CGFloat width = 2;
    if (textSize.width <=0) {
        return width;
    }
    
    if (textSize.width > width) {
        width = textSize.width+width;
    }
    return width;
    
}


@end
