//
//  GuideTopScrollCell.h
//  App3.0
//
//  Created by xinshang on 2017/6/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeImgsModel.h"

@protocol GuideTopScrollCellDelegate <NSObject>
/**点击某个图片*/
-(void)clickImageWithIndex:(NSInteger)index;
@end


@interface GuideTopScrollCell : UITableViewCell
/**滚动式图数据*/

@property(nonatomic,assign) CGFloat cellHeight;

@property(nonatomic,strong)NSArray *itemArray;
@property(nonatomic,weak)id<GuideTopScrollCellDelegate> delegate;
@end
