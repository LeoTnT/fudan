//
//  FilterDefaultView.h
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterDefaultView : UIView

@property (nonatomic,strong) NSString * titleName;
-(instancetype)initWithTitle:(NSString*)title array:(NSArray*)arr;
@property (nonatomic,copy) void(^DefaultBlock)(NSString *name,NSString *value);

@end
