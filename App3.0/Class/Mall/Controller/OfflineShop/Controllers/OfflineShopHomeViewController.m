//
//  OfflineShopHomeViewController.m
//  App3.0
//
//  Created by xinshang on 2017/12/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OfflineShopHomeViewController.h"
#import "HomeCenterCell.h"
#import "HomeCustomCell.h"
#import "homeModel.h"
#import "LocateViewController.h"
#import "OffLineSearchVC.h"
#import "S_ChoueseCity.h"
#import "GuideTopScrollCell.h"
#import "FDLoginController.h"
#import "HomeTopScorllCell.h"
#import "OfflineShopHomeModeleVC.h"
#import "ADDetailWebViewController.h"
#import "OffLineWebViewController.h"

#define IS_PAD (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)

@interface OfflineShopHomeViewController ()<CLLocationManagerDelegate,GuideTopScrollCellDelegate> {
    UIButton *locaBtn;
    UILabel *tibLb;
    NSDictionary *cityList;
    
    // 定位经纬度
    CLLocationCoordinate2D coordinate;
}

@property (nonatomic,strong) NSMutableArray *collectArray;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, strong)CLGeocoder *geoCoder;
@property (nonatomic, strong)NSString *cityName;
@property (nonatomic, strong)NSString *city_code;
@property (nonatomic, strong)NSString *longitude;
@property (nonatomic, strong)NSString *latitude;
@property (nonatomic, strong)NSString *adress;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger centerCellH;

@property (nonatomic, strong) NSMutableArray *imgsArr;

@property (nonatomic,strong) NSMutableArray *centerImgArr;
@property (nonatomic, strong) NSArray *itemArr;

@property (nonatomic, assign) NSInteger topScorllCellH;
@end

@implementation OfflineShopHomeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (self.longitude.length <=0) {
        self.longitude = @"0.0";
        self.latitude = @"0.0";
    }
    [self setUpUI];
    
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 1;
        [self loadData];
        [wSelf getMerchantList];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf getMerchantList];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    if (self.cityName.length <=0 ) {
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            //定位不能用
            [XSTool showToastWithView:nil Text:@"请开启定位,为您提供更精准的服务!"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.cityName = @"南通";
                [self getAreaList:self.cityName];
                [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
                [self setImageToRightWithButton:locaBtn];
                
            });
            
        }else{
            //定位功能可用
            self.manager.delegate = self;
            [self.manager startUpdatingLocation];//定位位置
        }
    }else{
        [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
        [self setImageToRightWithButton:locaBtn];
        
    }
}

- (void)setUpUI {
    //地区
    if (!locaBtn) {
        
        CGFloat navHeight=64;
        UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, navHeight-STATUS_HEIGHT)];
        navView.backgroundColor=[UIColor whiteColor];
        CGFloat btnWidth=22,space = 10,topSpace=11;
        
        //返回按钮
        UIButton *backBtn=[[UIButton alloc] initWithFrame:CGRectMake(space, topSpace, btnWidth, btnWidth)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        
        //输入搜索
        UIView *searchField=[[UIView alloc] initWithFrame:CGRectMake((mainWidth-(mainWidth-100))/2.0, (navHeight-STATUS_HEIGHT-30)/2.0, mainWidth-100, 30)];
        searchField.backgroundColor=BG_COLOR;
        [navView addSubview:searchField];
        navView.userInteractionEnabled=YES;
        //添加手势搜索
        [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(serchButtonAction)]];
        self.navigationItem.titleView = searchField;
        //搜索图片
        UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(10, (30-20)/2.0, 20, 20)];
        searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
        searchImg.userInteractionEnabled=YES;
        [searchField addSubview:searchImg];
        //搜索文字
        UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+10,(30-20)/2.0 , 100, 20)];
        searchLabel.userInteractionEnabled=YES;
        searchLabel.text=@"搜索店铺";
        searchLabel.font=[UIFont systemFontOfSize:15];
        searchLabel.textColor=mainGrayColor;
        [searchField addSubview:searchLabel];
        searchField.layer.cornerRadius = 15;
        searchField.layer.masksToBounds = YES;
        
        //城市
        locaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [locaBtn setTitle:@"地区" forState:UIControlStateNormal];
        [locaBtn setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [locaBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        UIImage *image = [UIImage imageNamed:@"cart_gostore_arrow"];
        [locaBtn setImage:image forState:UIControlStateNormal];
        locaBtn.frame = CGRectMake(0, 0, 60, 40);
        [locaBtn addTarget:self action:@selector(locaAction:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:locaBtn];
        
        
        
    }
    if (self.cityName) {
        [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
    }
    [self setImageToRightWithButton:locaBtn];
}

- (void)backBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

//设置图片居右
- (void)setImageToRightWithButton:(UIButton *)btn {
    if (!btn) return;
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0){
        return 1;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return 1;
    }else{
        return 1+self.collectArray.count;
        //        return 10;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        static NSString *cellID=@"topScrollCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell=[[GuideTopScrollCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            ((GuideTopScrollCell *)cell).cellHeight = 150*HScale;
        }
        ((GuideTopScrollCell *)cell).layer.masksToBounds = YES;
        if (self.imgsArr.count >0) {
            ((GuideTopScrollCell *)cell).itemArray=self.imgsArr;
        }
        ((GuideTopScrollCell *)cell).delegate=self;
    }else if (indexPath.section == 1) {
        static NSString *topCellId=@"HomeTopScorllCell";
        cell=[tableView dequeueReusableCellWithIdentifier:topCellId];
        if (!cell) {
            cell=[[HomeTopScorllCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topCellId];
        }
        //        ((HomeTopScorllCell *)cell).delegate = self;
        if (self.itemArr.count > 0) {
            ((HomeTopScorllCell *)cell).itemArray = (NSArray *)self.itemArr;
        }
        for (UIView *view in ((HomeTopScorllCell *)cell).menuArray) {
            [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goMenuDetail:)]];
        }
        self.topScorllCellH = ((HomeTopScorllCell *)cell).height;
    }else if(indexPath.section == 2){
        
        static NSString *CellIdentifier = @"HomeCenterCell";
        cell = (HomeCenterCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[HomeCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.layer.masksToBounds = YES;
        
        HomeCenterCell *tCell = (HomeCenterCell *)cell;
        
        
        if (self.centerImgArr.count > 0) {
            tCell.imagArray = (NSArray *)self.centerImgArr;
            self.centerCellH = tCell.cellHeight;
            for (UIButton *btn in tCell.btnArr) {
                [btn addTarget:self action:@selector(homeCenterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
    }else {
        if (indexPath.row == 0) {
            static NSString *centerCellId=@"Cell20";
            cell=[tableView dequeueReusableCellWithIdentifier:centerCellId];
            if (!cell) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:centerCellId];
                cell.separatorInset = UIEdgeInsetsMake(0,-10, 0,0);
                
            }
            cell.layer.masksToBounds = YES;
            //                        if (self.collectArray.count >0) {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.textColor = Color(@"999999"); NSMutableAttributedString *hintString = [self setStringWithStr:@"———  猜你喜欢  ———" RangStr:@"  猜你喜欢  "];
            cell.textLabel.attributedText = hintString;
            //            }
        }else{
            static NSString *customCellId=@"HomeCustomCell";
            cell=[tableView dequeueReusableCellWithIdentifier:customCellId];
            if (!cell) {
                cell=[[HomeCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customCellId];
            }
            cell.layer.masksToBounds = YES;
            
            if (self.collectArray.count >0) {
                homeModel * model = self.collectArray[indexPath.row-1];
                ((HomeCustomCell *)cell).model = model;
            }
        }
    }
    return cell;
}

- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr {
    NSMutableAttributedString *hintString=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[hintString string]rangeOfString:rangStr];
    [hintString addAttribute:NSForegroundColorAttributeName value:Color(@"999999") range:range];
    [hintString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:range];
    return hintString;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.imgsArr.count >0) {
            return 150*HScale;
        }
        return 0;
    }else if (indexPath.section == 1) {
        return self.topScorllCellH;
    }else if (indexPath.section == 2) {
        if (self.centerImgArr.count > 0) {
            return  self.centerCellH;
        }
        return 0;
    }else {
        
        if (indexPath.row == 0) {
            //            if (self.collectArray.count >0) {
            return 45;
            //            }
            return 0;
        }
        return (mainWidth/4)*0.8+20;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 3) {
        if (indexPath.row != 0) {
            NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
            if (guid.length <= 0) {
                guid = @"app";
            }
            
            if (VUE_ON) {                
                homeModel * model = self.collectArray[indexPath.row-1];
                NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/offline/supplydetail/%@?device=%@",ImageBaseUrl,model.user_id,guid];
                OffLineWebViewController *vc=[[OffLineWebViewController alloc] init];
                vc.urlStr = urlStr;
                vc.urlType = WKWebViewURLOffLine;
                vc.navigationItem.title= model.name.length?model.name:@"";
                [self xs_pushViewController:vc];
            } else {
            }
        }
    }
}

//设置tableHeaderView的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (self.imgsArr.count<=0) {
            return 0.01;
        }
    }
    if (section == 2) {
        if (self.centerImgArr.count<=0) {
            return 0.01;
        }
    }
    return 10;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)serchButtonAction {
    [self.view endEditing:YES];
    
    homeParamModel *paramModel = [[homeParamModel alloc] init];
    paramModel.city_code = self.city_code;
    paramModel.cityName = self.cityName;
    paramModel.latitude = self.latitude;
    paramModel.longitude = self.longitude;
    paramModel.adress = self.adress;
    paramModel.item = [[homeItemModel alloc]init];

    //搜索
    UIViewController *tempVC;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[OffLineSearchVC class]]) {
            tempVC = (OffLineSearchVC *) controller;
        }
        
    }
    if (tempVC) {
        ((OffLineSearchVC *)tempVC).hidesBottomBarWhenPushed = YES;
        ((OffLineSearchVC *)tempVC).paramModel = paramModel;
        [self.navigationController popToViewController:tempVC animated:YES];
    }else{
        OffLineSearchVC *vc = [[OffLineSearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
        vc.hidesBottomBarWhenPushed = YES;
        vc.paramModel = paramModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ---------地区选择----------
- (void)locaAction:(id)sender {
    S_ChoueseCity *chouseCity =  [[S_ChoueseCity alloc] init];
    @weakify(self);
    [chouseCity setSelectedCountry:^(NSDictionary *dic,NSString *showTitle) {
        @strongify(self);
        NSLog(@"dic==%@,title == %@",dic,showTitle);
        [self reloadtableViewWithcityCode:dic[showTitle][@"code"] cityName:showTitle];
    }];
    
    
    [chouseCity setSelectedCity:^(NSDictionary *dic, NSString *title) {
        @strongify(self);
        NSLog(@"dic==%@,title == %@",dic,title);
        
        [self reloadtableViewWithcityCode:dic[@"code"] cityName:title];
        
    }];
    chouseCity.showAddress = locaBtn.titleLabel.text;
    [self.navigationController pushViewController:chouseCity animated:YES];
}

- (void)reloadtableViewWithcityCode:(NSString*)cityCode cityName:(NSString*)cityName {
    self.cityName = cityName;
    self.city_code = cityCode;
    [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
    [self setImageToRightWithButton:locaBtn];
    [self getMerchantList];
}

#pragma mark ---------定位----------
#pragma mark - CLLocationManagerDelegate
/**
 *  更新到位置之后调用
 *
 *  @param manager   位置管理者
 *  @param locations 位置数组
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"定位到了");
    [manager stopUpdatingLocation];//关闭定位服务
    if (locations.count <=0) {
        self.cityName = @"";
        dispatch_async(dispatch_get_main_queue(), ^{
            [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
            [self setImageToRightWithButton:locaBtn];
            
        });
        return;
    }
    CLLocation *loc = [locations objectAtIndex:0];
    
    coordinate = [WGS84TOGCJ02 transformFromWGSToGCJ:loc.coordinate];
    self.latitude =[NSString stringWithFormat:@"%f",coordinate.latitude];
    self.longitude =[NSString stringWithFormat:@"%f",coordinate.longitude];
    
    __block NSString *FormattedAddressLine;
    CLLocation *loction = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    @weakify(self);
    [self.geoCoder reverseGeocodeLocation:loction completionHandler:^(NSArray *placemarks, NSError *error) {
        @strongify(self);
        if (error) {NSLog(@" 反编码 - - ");return ;}
        for (CLPlacemark *placeMark in placemarks){
            NSDictionary *addressDic=placeMark.addressDictionary;
            NSArray *state=[addressDic objectForKey:@"FormattedAddressLines"];
            if (state.count >0) {
                FormattedAddressLine = state[0];
                
            }
        }
        if ([FormattedAddressLine containsString:@"市"]) {
            NSRange range = [FormattedAddressLine rangeOfString:@"市"];
            if (FormattedAddressLine.length > range.location+1) {
                self.adress = [FormattedAddressLine substringFromIndex:(range.location+1)];
            }
        }
        NSMutableString *localStr = [[NSMutableString alloc] init];
        CLPlacemark *palcemark;
        if (placemarks.count > 0) {
            palcemark = [placemarks firstObject];
            localStr = [[NSMutableString  alloc] initWithString:palcemark.locality];
        }else{
        }
        
        if (localStr.length > 0) {
            if ([localStr containsString:@"市"]) {
                NSRange range = [localStr rangeOfString:@"市"];
                [localStr deleteCharactersInRange:range];
            }
            self.cityName = localStr;
        }else{
            self.cityName = @"";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [locaBtn setTitle:self.cityName forState:UIControlStateNormal];
            [self setImageToRightWithButton:locaBtn];
        });
        if (palcemark) {
            [self getAreaList:palcemark.locality];
        }
    }];
}


- (void) getAreaList:(NSString *)locationCity {
    
    if (!cityList) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"cityList" ofType:@"plist"];
        cityList = [XSTool returnDictionaryWithDataPath:plistPath];
    }
    NSMutableString *lC = locationCity.mutableCopy;
    NSString *areaKey;
    NSString *areaID;
    if ([lC containsString:@"市"]) {
        NSRange range = [lC rangeOfString:@"市"];
        [lC deleteCharactersInRange:range];
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
        
    }else{
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
    }
    
    
    if (!areaID){
        self.city_code = nil;
    }else{
        self.city_code = areaID;
    }
    self.page = 1;
    [self getMerchantList];
}

#pragma mark ------------------数据加载
- (void)getMerchantList{
    
    [self.tableView.mj_footer resetNoMoreData];
    __weak __typeof__(self) wSelf = self;
    if (!self.cityName) {
        self.cityName = @"临沂";
    }
    NSDictionary *positionDic = @{@"longitude":self.longitude,@"latitude":self.latitude};
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager OffGuessYouLikeListsWithPage:[NSString stringWithFormat:@"%ld",self.page]
                                         city:self.cityName
                                user_position:positionDic
                                      success:^(NSDictionary *dic, resultObject *state) {
         [XSTool hideProgressHUDWithView:self.view];
         [wSelf tableViewEndRefreshing];
         if (state.status) {
             if (self.page == 1) {
                 [self.collectArray removeAllObjects];
             }
             NSLog(@"%@",dic);
             NSArray *dataArr = dic[@"data"][@"list"][@"data"];
             if (dataArr.count==0) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
             }else{
                 [wSelf.collectArray addObjectsFromArray:[homeModel mj_objectArrayWithKeyValuesArray:dataArr]];
             }
             [self.tableView reloadData];
             
         }else{
             Alert(state.info);
         }
     } fail:^(NSError *error) {
         [wSelf tableViewEndRefreshing];
         Alert(NetFailure);
     }];
}


- (void)loadData {
    //首页8个图标
    __weak __typeof__(self) wSelf = self;
    [HTTPManager OffSupplyIndustryListsSuccess:^(NSDictionary *dic, resultObject *state) {
         //         [wSelf tableViewEndRefreshing];
         if (state.status) {
             self.itemArr = [NSArray arrayWithArray:[homeItemModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
         }else{
             //             if (dic[@"info"]) {
             //                 Alert(state.info);
             //             }
         }
         [wSelf getOffHomeGetTopGgs];
         [wSelf getOffGetMiddleGgs];
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         //         [wSelf tableViewEndRefreshing];
         //         Alert(NetFailure);
     }];
}

//首页顶部轮播图
- (void)getOffHomeGetTopGgs {
    __weak __typeof__(self) wSelf = self;
    [HTTPManager OffHomeGetTopGgsSuccess:^(NSDictionary *dic, resultObject *state) {
         //         [wSelf tableViewEndRefreshing];
         if (state.status) {
             [wSelf.imgsArr  removeAllObjects];
             [wSelf.imgsArr addObjectsFromArray:[HomeImgsModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
         }else{
             //             if (dic[@"info"]) {
             //                 Alert(state.info);
             //             }
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         //         [wSelf tableViewEndRefreshing];
         //         Alert(NetFailure);
     }];
}

//首页广告位
- (void)getOffGetMiddleGgs {
    __weak __typeof__(self) wSelf = self;
    [HTTPManager OffGetMiddleGgsSuccess:^(NSDictionary *dic, resultObject *state) {
         //         [wSelf tableViewEndRefreshing];
         if (state.status) {
             [wSelf.centerImgArr  removeAllObjects];
             [wSelf.centerImgArr addObjectsFromArray:[HomeImgsModel mj_objectArrayWithKeyValuesArray: dic[@"data"]]];
         }else{
             //             if (dic[@"info"]) {
             //                 Alert(state.info);
             //             }
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         //         [wSelf tableViewEndRefreshing];
         //         Alert(NetFailure);
     }];
}

- (void)tableViewEndRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [XSTool hideProgressHUDWithView:self.view];
}

#pragma mark -------GuideTopScrollCellDelegate----
//顶部广告
- (void)clickImageWithIndex:(NSInteger)index {
    if (index <self.imgsArr.count) {
        HomeImgsModel *model = self.imgsArr[index];
        [self pushADWebVCWithurl:model];
    }
}

//中部广告
- (void)homeCenterBtnClick:(UIButton*)sender {
    if (sender.tag-1 <self.centerImgArr.count) {
        HomeImgsModel *model = self.centerImgArr[sender.tag-1];
        [self pushADWebVCWithurl:model];
    }
}

//行业
- (void)goMenuDetail:(UIGestureRecognizer *)tap {
    homeItemModel *item=[self.itemArr objectAtIndex:tap.view.tag];
    
    homeParamModel *paramModel = [[homeParamModel alloc] init];
    paramModel.city_code = self.city_code;
    paramModel.cityName = self.cityName;
    paramModel.latitude = self.latitude;
    paramModel.longitude = self.longitude;
    paramModel.adress = self.adress;
    paramModel.item = item;
    paramModel.keyWord = @"";
    
    OfflineShopHomeModeleVC *vc =[[OfflineShopHomeModeleVC alloc] init];
    vc.paramModel = paramModel;
    [self xs_pushViewController:vc];
}

- (void)pushADWebVCWithurl:(HomeImgsModel *)model {
    ADDetailWebViewController *vc=[[ADDetailWebViewController alloc] init];
    vc.urlStr = [NSString stringWithFormat:@"%@",model.url];
    vc.navigationItem.title= model.name.length?model.name:@"";
    [self xs_pushViewController:vc];
}

#pragma mark - 懒加载
- (CLLocationManager *)manager {
    if (!_manager) {
        _manager = [[CLLocationManager alloc] init];
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        [_manager requestWhenInUseAuthorization];
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            [_manager requestWhenInUseAuthorization];
        }
    }
    return _manager;
}

- (CLGeocoder *)geoCoder {
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (BOOL)checkIsLogin {
    // 获取本地存储的guid
    NSString *guid = [[NSUserDefaults standardUserDefaults] objectForKey:APPAUTH_KEY];
    
    if (guid.length <= 0) {
        FDLoginController *vc =[[FDLoginController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    return YES;
}

- (NSMutableArray *)imgsArr {
    if (!_imgsArr) {
        _imgsArr = [NSMutableArray array];
    }
    return _imgsArr;
}

- (NSMutableArray *)centerImgArr {
    if (!_centerImgArr) {
        _centerImgArr = [NSMutableArray array];
    }
    return _centerImgArr;
}

- (NSMutableArray *)collectArray {
    if (!_collectArray) {
        _collectArray = [NSMutableArray array];
    }
    return _collectArray;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _collectArray = nil;
    _manager = nil;
    _geoCoder = nil;
    _cityName = nil;
    _city_code = nil;
    _longitude = nil;
}

@end

