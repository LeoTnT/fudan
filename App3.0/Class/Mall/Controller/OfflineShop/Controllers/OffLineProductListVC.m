//
//  OffLineProductListVC.m
//  App3.0
//
//  Created by xinshang on 2017/12/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OffLineProductListVC.h"
#import "homeModel.h"
#import "LocateViewController.h"
#import "OffLineSearchVC.h"
#import "S_ChoueseCity.h"
#import "ScanViewController.h"
#import "LBXScanViewStyle.h"
#import<CoreText/CoreText.h>
#import "GuideTopScrollCell.h"
#import "FDLoginController.h"
#import "HomeTopScorllCell.h"
#import "OfflineShopHomeModeleVC.h"
#import "HomeItemCenterCell.h"
#import "HomeCustomCell.h"
#import "FilterAllView.h"
#import "FilterNearView.h"
#import "FilterDefaultView.h"
#import "OffLineMapViewController.h"
#import "ADDetailWebViewController.h"
#import "OffLineWebViewController.h"


#define HeadViewH 44*2+10

#define IS_PAD (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)

@interface OffLineProductListVC ()<CLLocationManagerDelegate,GuideTopScrollCellDelegate>
{
    UIButton *locaBtn;
    UILabel *tibLb;
    NSDictionary *cityList;
    
    // 定位经纬度
    CLLocationCoordinate2D coordinate;
    UIView *headView;
    FilterAllView *allView;
    FilterNearView *nearView;
    FilterDefaultView *otherView;
}
@property(nonatomic,strong) UILabel *locaLb;//位置
//@property(nonatomic,strong) UIButton *locaBtn;//定位

@property (nonatomic,strong) NSMutableArray *collectArray;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, strong)CLGeocoder *geoCoder;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL isAppear;
@property (nonatomic, assign) NSInteger centerCellH;

@property (nonatomic, strong) NSMutableArray *imgsArr;
@property (nonatomic,strong) NSMutableArray *centerImgArr;
@property (nonatomic, strong) LoginDataParser *parser;//用户信息
@property (nonatomic, strong) NSArray *indstry_ItemArr;


@property (nonatomic,strong) UIButton *allBtn;
@property (nonatomic,strong) UIButton *nearBtn;
@property (nonatomic,strong) UIButton *otherBtn;

@property (nonatomic, assign) NSInteger topScorllCellH;


@property (nonatomic,strong) NSArray *distanceArr;//附近距离
@property (nonatomic,strong) NSArray *sorterArr;//默认排序等


@property (nonatomic,strong) NSString *sort_name;//默认排序等
@property (nonatomic,strong) NSString *sort_value;//默认排序等

@property (nonatomic,strong) NSString *countryCode;//
@property (nonatomic,strong) NSString *townCode;//
@property (nonatomic,strong) NSString *distanceStr;//

@end

@implementation OffLineProductListVC
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //    [self.view addSubview:self.tableView];
    self.page = 1;
    if (self.isRefresh&&self.isAppear) {
        [self getMerchantList];
    }
    self.isAppear = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (self.paramModel.longitude.length <=0) {
        self.paramModel.longitude = @"0.0";
        self.paramModel.latitude = @"0.0";
    }
    if (!self.category_id) {
        self.category_id = @"0";
    }
    [self setUpUI];
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 1;
        [self loadData];
        [wSelf getMerchantList];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf getMerchantList];
    }];
    [self.tableView.mj_header beginRefreshing];
    //定位功能可用
    self.manager.delegate = self;
    
    if (self.paramModel.cityName.length <=0 ) {
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            //定位不能用
            [XSTool showToastWithView:nil Text:@"请开启定位,为您提供更精准的服务!"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.paramModel.cityName = @"临沂";
                [self getAreaList:self.paramModel.cityName];
               
                
            });
            
        }else{
            [self.manager startUpdatingLocation];//定位位置
        }
    }else{
      
    }
}


- (void)setUpUI
{
    //地区
    if (!locaBtn) {
        
        CGFloat navHeight=64;
        UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, navHeight-STATUS_HEIGHT)];
        navView.backgroundColor=[UIColor whiteColor];
        CGFloat btnWidth=22,space = 10,topSpace=11;
        
        //返回按钮
        UIButton *backBtn=[[UIButton alloc] initWithFrame:CGRectMake(space, topSpace, btnWidth, btnWidth)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        
        //输入搜索
        UIView *searchField=[[UIView alloc] initWithFrame:CGRectMake((mainWidth-(mainWidth-100))/2.0, (navHeight-STATUS_HEIGHT-30)/2.0, mainWidth-100, 30)];
        searchField.backgroundColor=BG_COLOR;
        [navView addSubview:searchField];
        navView.userInteractionEnabled=YES;
        //添加手势搜索
        [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(serchButtonAction)]];
        self.navigationItem.titleView = searchField;
        //搜索图片
        UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(10, (30-20)/2.0, 20, 20)];
        searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
        searchImg.userInteractionEnabled=YES;
        [searchField addSubview:searchImg];
        //搜索文字
        UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+10,(30-20)/2.0 , 100, 20)];
        searchLabel.userInteractionEnabled=YES;
        searchLabel.text=@"搜索店铺";
        searchLabel.font=[UIFont systemFontOfSize:15];
        searchLabel.textColor=mainGrayColor;
        [searchField addSubview:searchLabel];
        searchField.layer.cornerRadius = 15;
        searchField.layer.masksToBounds = YES;
        
        //附近商家
        locaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"offLine_map"];
        [locaBtn setImage:image forState:UIControlStateNormal];
        locaBtn.frame = CGRectMake(0, 0, 60, 40);
        [locaBtn addTarget:self action:@selector(locaAction:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:locaBtn];
    }
    
}

-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}





#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.collectArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
        static NSString *customCellId=@"HomeCustomCell";
        cell=[tableView dequeueReusableCellWithIdentifier:customCellId];
        if (!cell) {
            cell=[[HomeCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customCellId];
        }
        cell.layer.masksToBounds = YES;
        if (self.collectArray.count >0) {
            homeModel * model = self.collectArray[indexPath.row];
            ((HomeCustomCell *)cell).model = model;
        }
        
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return (mainWidth/4)*0.8+20;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (VUE_ON) {
            NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
            if (guid.length <= 0) {
                guid = @"app";
            }
            homeModel * model = self.collectArray[indexPath.row];
            NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/offline/supplydetail/%@?device=%@",ImageBaseUrl,model.user_id,guid];
            OffLineWebViewController *vc=[[OffLineWebViewController alloc] init];
            vc.urlStr = urlStr;
            vc.urlType = WKWebViewURLOffLine;
            vc.navigationItem.title= model.name.length?model.name:@"";
            [self xs_pushViewController:vc];
        } else {
            
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        if (!headView) {
            headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth,HeadViewH)];
            headView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [self creatHeadLocalView];
            [self creatHeadFilterView];
        }
        return headView;
        
    }else{
        UIView *view = [[UIView alloc] init];
        return view;
    }
    
    
}


//设置tableHeaderView的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 44*2+10;
    }
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 0) {
        if (self.centerImgArr.count<=0) {
            return 0.01;
        }
    }
    return 10;
}




- (void)serchButtonAction
{
    [self.view endEditing:YES];
    //搜索
    UIViewController *tempVC;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[OffLineSearchVC class]]) {
            tempVC = (OffLineSearchVC *) controller;
        }
        
    }
    if (tempVC) {
        ((OffLineSearchVC *)tempVC).hidesBottomBarWhenPushed = YES;
        ((OffLineSearchVC *)tempVC).paramModel = self.paramModel;
        [self.navigationController popToViewController:tempVC animated:YES];
    }else{
        OffLineSearchVC *vc = [[OffLineSearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
        vc.hidesBottomBarWhenPushed = YES;
        vc.paramModel = self.paramModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark ---------地区选择----------
- (void)locaAction:(id)sender
{
    OffLineMapViewController *vc =[[OffLineMapViewController alloc] init];
    vc.collectArray = self.collectArray;
    vc.titleStr = self.paramModel.item.name;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark ---------定位----------

#pragma mark - CLLocationManagerDelegate
/**
 *  更新到位置之后调用
 *
 *  @param manager   位置管理者
 *  @param locations 位置数组
 */
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"定位到了");
    [manager stopUpdatingLocation];//关闭定位服务
    if (locations.count <=0) {
        self.paramModel.cityName = @"临沂";
        return;
    }
    CLLocation *loc = [locations objectAtIndex:0];
    
    coordinate = [WGS84TOGCJ02 transformFromWGSToGCJ:loc.coordinate];
    self.paramModel.latitude =[NSString stringWithFormat:@"%f",coordinate.latitude];
    self.paramModel.longitude =[NSString stringWithFormat:@"%f",coordinate.longitude];
    
    __block NSString *FormattedAddressLine;
    CLLocation *loction = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    @weakify(self);
    [self.geoCoder reverseGeocodeLocation:loction completionHandler:^(NSArray *placemarks, NSError *error) {
        @strongify(self);
        if (error) {NSLog(@" 反编码 - - ");return ;}
        for (CLPlacemark *placeMark in placemarks){
            NSDictionary *addressDic=placeMark.addressDictionary;
            NSArray *state=[addressDic objectForKey:@"FormattedAddressLines"];
            if (state.count >0) {
                FormattedAddressLine = state[0];
            }
        }
        if ([FormattedAddressLine containsString:@"市"]) {
            NSRange range = [FormattedAddressLine rangeOfString:@"市"];
            if (FormattedAddressLine.length > range.location+1) {
                self.paramModel.adress = [FormattedAddressLine substringFromIndex:(range.location+1)];
                self.locaLb.text = [NSString stringWithFormat:@"我的位置:%@",self.paramModel.adress];
            }
        }
        
        NSMutableString *localStr = [[NSMutableString alloc] init];
        CLPlacemark *palcemark;
        if (placemarks.count > 0) {
            palcemark = [placemarks firstObject];
            localStr = [[NSMutableString  alloc] initWithString:palcemark.locality];
        }else{
        }
        
        if (localStr.length > 0) {
            if ([localStr containsString:@"市"]) {
                NSRange range = [localStr rangeOfString:@"市"];
                [localStr deleteCharactersInRange:range];
            }
            self.paramModel.cityName = localStr;
        }else{
            self.paramModel.cityName = @"临沂";
        }

        if (palcemark) {
            [self getAreaList:palcemark.locality];
        }
    }];
    
    
}



- (void) getAreaList:(NSString *)locationCity {
    
    if (!cityList) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"cityList" ofType:@"plist"];
        cityList = [XSTool returnDictionaryWithDataPath:plistPath];
    }
    NSMutableString *lC = locationCity.mutableCopy;
    NSString *areaKey;
    NSString *areaID;
    if ([lC containsString:@"市"]) {
        NSRange range = [lC rangeOfString:@"市"];
        [lC deleteCharactersInRange:range];
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
        
    }else{
        areaKey = [NSString stringWithFormat:@"name_%@",lC];
        areaID = cityList[areaKey][lC][@"code"];
    }
    
    
    if (!areaID){
        self.paramModel.city_code = nil;
    }else{
        self.paramModel.city_code = areaID;
    }
    self.page = 0;
    [self getMerchantList];
    
}


- (void)getMerchantList{
    
    if (self.page == 1) {
        [self.collectArray removeAllObjects];
    }
    if (!self.sort_value) {
        self.sort_value = @"0";
    }
    if (!self.countryCode) {
        self.countryCode = @"";
    }
    if (!self.townCode) {
        self.townCode = @"";
    }
    if (!self.distanceStr) {
        self.distanceStr = @"";
    }
    if (!self.paramModel.keyWord) {
        self.paramModel.keyWord = @"";
    }
    
    NSString *industry = self.paramModel.item.ID;
    if (self.paramModel.keyWord.length>0) {
        industry = @"";
    }

    [self.tableView.mj_footer resetNoMoreData];
    __weak __typeof__(self) wSelf = self;
    NSDictionary *positionDic = @{@"longitude":self.paramModel.longitude,@"latitude":self.paramModel.latitude};
    NSDictionary *filterDic = @{@"category":@{@"categoryid":self.category_id},
                                @"area":@{@"county":self.countryCode,@"town":self.townCode,@"distance":self.distanceStr,},
                                @"sorter":@{@"type":self.sort_value}};
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager OffSupplyListsWithPage:[NSString stringWithFormat:@"%ld",self.page]
                                   city:self.paramModel.cityName
                               industry:self.paramModel.item.ID
                                keyword:self.paramModel.keyWord
                                 filter:filterDic
                          user_position:positionDic
                                success:^(NSDictionary *dic, resultObject *state)
     {
         [wSelf tableViewEndRefreshing];
         [XSTool hideProgressHUDWithView:self.view];
         if (state.status) {
             NSLog(@"%@",dic);
             NSArray *dataArr = dic[@"data"][@"list"][@"data"];
             if (dataArr.count==0) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
             }else{
                 if (self.page == 1) {
                     [self.collectArray removeAllObjects];
                 }
                 [wSelf.collectArray addObjectsFromArray:[homeModel mj_objectArrayWithKeyValuesArray:dataArr]];
             }
             [self.tableView reloadData];
         }else{
             Alert(state.info);
         }
         if (self.paramModel.item.ID) {
             self.paramModel.keyWord = @"";
         }
     } fail:^(NSError *error) {
         [wSelf tableViewEndRefreshing];
         Alert(NetFailure);
         if (self.paramModel.item.ID) {
             self.paramModel.keyWord = @"";
         }
         
     }];
    
}

- (void)loadData{
//    [self getIndustrySubLists];
    [self getOffGetFileterConditions];
}
//行业按钮
- (void)getIndustrySubLists
{
    if (!self.paramModel.item.ID) {
        return;
    }
    __weak __typeof__(self) wSelf = self;
    //[XSTool showProgressHUDWithView:self.view];
    [HTTPManager OffIndustrySubListsWithIndustry:self.paramModel.item.ID
                                         success:^(NSDictionary *dic, resultObject *state)
     {
         //     [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             self.indstry_ItemArr = [NSArray arrayWithArray:[homeCategoryListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
         }else{
             //         if (dic[@"info"]) {
             //             Alert(dic[@"info"]);
             //         }
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         //     [XSTool hideProgressHUDWithView:wSelf.view];
         //     Alert(NetFailure);
     }];
}

//筛选数组
- (void)getOffGetFileterConditions
{
    if (self.distanceArr.count >0) {
        return;
    }
    __weak __typeof__(self) wSelf = self;
    [HTTPManager OffGetFileterConditionsSuccess:^(NSDictionary *dic, resultObject *state)
     {
         if (state.status) {
             wSelf.distanceArr = [NSArray arrayWithArray:dic[@"data"][@"distance"]];
             wSelf.sorterArr = [NSArray arrayWithArray:dic[@"data"][@"sorter"]];
         }else{
         }
         
     } fail:^(NSError *error) {
     }];
}

#pragma mark -------GuideTopScrollCellDelegate----
-(void)clickImageWithIndex:(NSInteger)index
{
    
}

#pragma mark --goCenterItemDetail
-(void)goMenuDetail:(UIGestureRecognizer *)tap{
    
}
-(void)goCenterItemDetail:(UIGestureRecognizer *)tap{
    
}
#pragma mark ---------重新定位
-(void)relodLocalAction:(UIButton *)sender
{
    _locaLb.text = @"我的位置: 定位中...";
    [self.manager startUpdatingLocation];//重新定位
}



#pragma mark ------------------ 筛选模块------------------
- (void)allBtnAction:(UIButton *)sender
{
    NSString *industryId = @"";
    if (self.paramModel.keyWord.length <= 0) {
        industryId =  self.paramModel.item.ID;
    }
    if (isEmptyString(self.paramModel.item.ID)) {
        self.paramModel.item = [[homeItemModel alloc]init];
    }
    
    [self setTableViewContentOff];
    [sender setTitleColor:mainColor forState:UIControlStateNormal];
    [sender setImage:[UIImage imageNamed:@"sort_up"] forState:UIControlStateNormal];
    if (!allView) {
        @weakify(self);

        allView = [[FilterAllView alloc] initWithTitle:Localized(@"alls") industryId:industryId];
        allView.AllBlock = ^(NSString *ID, NSString *name) {
            @strongify(self);
            if (isEmptyString(industryId)) {
                self.category_id = @"";
                if (ID.length>0) {
                    self.paramModel.item.ID = ID;
                }
            }else{
                self.category_id = ID;

            }
                [sender setTitle:name forState:UIControlStateNormal];
                [self setImageToRightWithButton:sender];
            self.page = 1;
            [self getMerchantList];
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            self.tableView.scrollEnabled =YES;
            [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
            
        };
        
      
        
        [self.view addSubview:allView];
    }else{
        if (allView.hidden == YES) {
            allView.hidden = NO;
        }else{
            allView.hidden = YES;
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            self.tableView.scrollEnabled =YES;
            [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
        }
        
        
    }
    [self hiddenOtherView];
    [self hiddenNearView];
    
}

//附近
- (void)nearBtnAction:(UIButton *)sender
{
    if (!self.paramModel.city_code) {
        Alert(@"获取信息失败!");
        return;
    }
    [self setTableViewContentOff];
    [sender setTitleColor:mainColor forState:UIControlStateNormal];
    [sender setImage:[UIImage imageNamed:@"sort_up"] forState:UIControlStateNormal];
    if (!nearView) {
        @weakify(self);
        nearView = [[FilterNearView alloc] initWithCity_code:self.paramModel.city_code  array:self.distanceArr];
        nearView.NearBlock = ^(NSString *county_code, NSString *county_name, NSString *town_code, NSString *town_name, NSString *distance,NSString *distanceName) {
            @strongify(self);
            
            self.countryCode = county_code;
            self.townCode = town_code;
            self.distanceStr = distanceName;
            
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            
            NSString *senderName = @"";
            if (distance.length>0) {
                senderName = distance;
                if (distanceName.integerValue == 0) {
                    senderName = @"附近";
                }
                
            }else if(town_name.length >0){
                if ([town_name isEqualToString:Localized(@"alls")]) {
                    senderName = county_name;
                }else{
                    senderName = town_name;
                }
            }
            self.page = 1;
            [self getMerchantList];
            
            [sender setTitle:senderName forState:UIControlStateNormal];
            [self setImageToRightWithButton:sender];
            self.tableView.scrollEnabled =YES;
            [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
            
        };
        [self.view addSubview:nearView];
    }else{
        if (nearView.hidden == YES) {
            nearView.hidden = NO;
        }else{
            nearView.hidden = YES;
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            self.tableView.scrollEnabled =YES;
            [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
        }
        
    }
    [self hiddenAllView];
    [self hiddenOtherView];
}

//默认排序
- (void)otherBtnAction:(UIButton *)sender
{
    [self setTableViewContentOff];
    
    __weak __typeof__(self) wSelf = self;
    [sender setTitleColor:mainColor forState:UIControlStateNormal];
    [sender setImage:[UIImage imageNamed:@"sort_up"] forState:UIControlStateNormal];
    if (!otherView) {
        otherView = [[FilterDefaultView alloc] initWithTitle:self.paramModel.item.name array:self.sorterArr];
        otherView.DefaultBlock = ^(NSString *name, NSString *value) {
            if (name.length > 1) {
                wSelf.sort_name = name;
                wSelf.sort_value = value;
                [sender setTitle:wSelf.sort_name forState:UIControlStateNormal];
                [wSelf setImageToRightWithButton:sender];
            }
            wSelf.page = 1;
            [wSelf getMerchantList];
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            wSelf.tableView.scrollEnabled =YES;
            [wSelf.tableView setContentOffset:CGPointMake(0,0) animated:YES];
        };
        [self.view addSubview:otherView];
    }else{
        if (otherView.hidden == YES) {
            otherView.hidden = NO;
        }else{
            otherView.hidden = YES;
            [sender setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [sender setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
            self.tableView.scrollEnabled =YES;
            [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
        }
    }
    [self hiddenAllView];
    [self hiddenNearView];
}

- (void)hiddenNearView
{
    if (nearView) {
        nearView.hidden = YES;
        [_nearBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_nearBtn setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
        
    }
}
- (void)hiddenAllView
{
    if (allView) {
        allView.hidden = YES;
        [_allBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_allBtn setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
        
    }
}

- (void)hiddenOtherView
{
    if (otherView) {
        otherView.hidden = YES;
        [_otherBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_otherBtn setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
        
    }
}

-(void)setTableViewContentOff
{
    CGFloat height = 0;
    [self.tableView setContentOffset:CGPointMake(0,height) animated:NO];
    self.tableView.scrollEnabled =NO; //设置tableview 不能滚动
}






//筛选
- (void)creatHeadFilterView
{
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 54, mainWidth, 43)];
    btnView.backgroundColor = [UIColor whiteColor];
    _allBtn = [self creatButtonWithTitle:Localized(@"alls") index:0];
    [_allBtn addTarget:self action:@selector(allBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [btnView addSubview:_allBtn];
    
    _nearBtn = [self creatButtonWithTitle:@"附近" index:1];
    [_nearBtn addTarget:self action:@selector(nearBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [btnView addSubview:_nearBtn];
    
    _otherBtn = [self creatButtonWithTitle:@"默认排序" index:2];
    [_otherBtn addTarget:self action:@selector(otherBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btnView addSubview:_otherBtn];
    [headView addSubview:btnView];
}

//我的位置
- (void)creatHeadLocalView
{
    UIView *localView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    localView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *_locaImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_verify_address"]];
    [localView addSubview:_locaImg];
    [_locaImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(localView).offset(10);
        make.centerY.mas_equalTo(localView);
    }];
    
    _locaLb = [[UILabel alloc] init];
    _locaLb.text = @"我的位置:";
    if (self.paramModel.adress.length >0) {
        _locaLb.text = [NSString stringWithFormat:@"我的位置:%@",self.paramModel.adress];
    }
    _locaLb.font = [UIFont systemFontOfSize:15];
    [localView addSubview:_locaLb];
    [_locaLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_locaImg.mas_right).offset(2);
        make.width.mas_equalTo(mainWidth-80);
        make.centerY.mas_equalTo(localView);
        make.height.mas_equalTo(44);
    }];
    
    UIImageView *_rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Local"]];
    [localView addSubview:_rightImg];
    [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(localView).offset(-12);
        make.centerY.mas_equalTo(localView);
    }];
    
    
    UIButton*reloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [reloadBtn addTarget:self action:@selector(relodLocalAction:) forControlEvents:UIControlEventTouchUpInside ];
    [localView addSubview:reloadBtn];
    [reloadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(localView.mas_right).offset(-5);
        make.centerY.mas_equalTo(localView.mas_centerY);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(44);
        
    }];
    
    [headView addSubview:localView];
}


#pragma mark - 懒加载
- (CLLocationManager *)manager
{
    if (!_manager) {
        _manager = [[CLLocationManager alloc] init];
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        [_manager requestWhenInUseAuthorization];
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            [_manager requestWhenInUseAuthorization];
        }
        
    }
    return _manager;
}

- (CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (BOOL)checkIsLogin{
    // 获取本地存储的guid
    NSString *guid = [[NSUserDefaults standardUserDefaults] objectForKey:APPAUTH_KEY];
    
    if (guid.length <= 0) {
        FDLoginController *vc =[[FDLoginController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    return YES;
}


-(NSMutableArray *)imgsArr{
    if (!_imgsArr) {
        _imgsArr = [NSMutableArray array];
    }
    return _imgsArr;
}
-(NSMutableArray *)centerImgArr{
    if (!_centerImgArr) {
        _centerImgArr = [NSMutableArray array];
    }
    return _centerImgArr;
}
-(NSMutableArray *)collectArray
{
    if (!_collectArray) {
        _collectArray = [NSMutableArray array];
    }
    return _collectArray;
}


-(NSArray *)distanceArr
{
    if (!_distanceArr) {
        _distanceArr = [NSArray array];
    }
    return _distanceArr;
}
-(NSArray *)sorterArr
{
    if (!_sorterArr) {
        _sorterArr = [NSArray array];
    }
    return _sorterArr;
}

- (void)tableViewEndRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [XSTool hideProgressHUDWithView:self.view];
}

//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}

- (UIButton *)creatButtonWithTitle:(NSString *)title index:(NSInteger)index{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat btnWidth = mainWidth/3;
    button.tag = index;
    button.frame = CGRectMake(btnWidth*index, 0, btnWidth, 43);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [button setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
    
    [self setImageToRightWithButton:button];
    return button;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}


@end




