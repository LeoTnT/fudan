//
//  OffLineProductListVC.h
//  App3.0
//
//  Created by xinshang on 2017/12/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeModel.h"


@interface OffLineProductListVC : XSBaseTableViewController

@property (nonatomic, strong) homeParamModel *paramModel;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic,strong) NSString *category_id;//

@end

