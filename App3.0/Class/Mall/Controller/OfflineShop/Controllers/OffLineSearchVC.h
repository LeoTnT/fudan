//
//  OffLineSearchVC.h
//  App3.0
//
//  Created by xinshang on 2017/12/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchBarView.h"
#import "homeModel.h"
@interface OffLineSearchVC : XSBaseTableViewController

@property (nonatomic ,copy) void (^ searchAction)(NSString *text);

- (instancetype)initWithSearchBarStyle:(SearchBarStyle)style;
@property (nonatomic, strong) homeParamModel *paramModel;

@end

