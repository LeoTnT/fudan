//
//  OfflineShopHomeModeleVC.h
//  App3.0
//
//  Created by xinshang on 2017/12/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "homeModel.h"
@interface OfflineShopHomeModeleVC : XSBaseTableViewController

@property (nonatomic, strong) homeParamModel *paramModel;
@property (nonatomic, strong) NSString *keyword;

@end
