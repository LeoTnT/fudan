//
//  OffLineMapViewController.m
//  App3.0
//
//  Created by xinshang on 2017/12/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "OffLineMapViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MyAnnotationOffLine.h"
#import "CustomPinAnnotationView.h"
#import "ADDetailWebViewController.h"
@interface OffLineMapViewController ()<MKMapViewDelegate>
{
    CLLocationManager *_locationManager;
    
    MKMapView *_mapView;
}

@property (nonatomic,retain) NSMutableArray *locationArray;

@end

@implementation OffLineMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBlackLeftBackBtn];
    self.title = self.titleStr;
    //    定位授权
    _locationManager = [[CLLocationManager alloc]init];
    [_locationManager startUpdatingLocation];//开启定位服务
    
    //    地图视图
    _mapView = [[MKMapView alloc]initWithFrame:self.view.frame];
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    [self.view addSubview:_mapView];

    [self loadData];
    
}

- (void)loadData{
    for (homeModel *model in self.collectArray) {
        MyAnnotationOffLine *MyAnnotationOffLineModel = [[MyAnnotationOffLine alloc]initWithAnnotationModelWithModel:model];
        
        [self.locationArray addObject:MyAnnotationOffLineModel];
    }
    
    //    核心代码
    [_mapView addAnnotations:self.locationArray];
    
}

#pragma mark ------ delegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    userLocation.title  =@"我的位置";
    [_locationManager stopUpdatingLocation];//关闭定位服务
    
    _mapView.centerCoordinate = userLocation.coordinate;
    
    [_mapView setRegion:MKCoordinateRegionMake(userLocation.coordinate, MKCoordinateSpanMake(0.5, 0.5)) animated:YES];
    
    
    
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    /*
     
     * 大头针分两种
     
     * 1. MKPinAnnotationView：他是系统自带的大头针，继承于MKAnnotationView，形状跟棒棒糖类似，可以设置糖的颜色，和显示的时候是否有动画效果
     
     * 2. MKAnnotationView：可以用指定的图片作为大头针的样式，但显示的时候没有动画效果，如果没有给图片的话会什么都不显示
     
     * 3. mapview有个代理方法，当大头针显示在试图上时会调用，可以实现这个方法来自定义大头针的动画效果，我下面写有可以参考一下
     
     * 4. 在这里我为了自定义大头针的样式，使用的是MKAnnotationView
     
     */
    
    //    判断是不是用户的大头针数据模型
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] init];
        //是否允许显示插入视图*********
        annotationView.canShowCallout = YES;
        
        return annotationView;
    }
    
    CustomPinAnnotationView *annoView = (CustomPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"otherAnnotationView"];
    if (annoView == nil) {
        annoView = [[CustomPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"otherAnnotationView"];
    }
    
    MyAnnotationOffLine *MyAnnotationOffLine = annotation;
    annoView.annotation = annotation;
    annoView.image = [UIImage imageNamed:@"offLine_local"];
    annoView.canShowCallout = YES;
    UIImage *image = [UIImage imageNamed:@"offLine_right"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = MyAnnotationOffLine.type.intValue;
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height); // don't use auto layout
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventPrimaryActionTriggered];
    annoView.rightCalloutAccessoryView = button;
    annoView.draggable = YES;
    
    return annoView;
    
    
}



-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    // 主要是这句
    
    [_mapView setCenterCoordinate:view.annotation.coordinate animated:YES];
    
}
//大头针显示在视图上时调用，在这里给大头针设置显示动画
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views{
    
    
    //    获得mapView的Frame
    CGRect visibleRect = [mapView annotationVisibleRect];
    
    for (MKAnnotationView *view in views) {
        
        CGRect endFrame = view.frame;
        CGRect startFrame = endFrame;
        startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
        view.frame = startFrame;
        [UIView beginAnimations:@"drop" context:NULL];
        [UIView setAnimationDuration:1];
        view.frame = endFrame;
        [UIView commitAnimations];
        
        
    }
    
    
}
#pragma mark -------------进入店铺-----------------
- (void)didTapButton:(UIButton*)sender{
    
    NSLog(@"id==%ld",(long)sender.tag);
    
    if (VUE_ON) {
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (guid.length <= 0) {
            guid = @"app";
        }
        homeModel * currentModel;
        for ( homeModel * model in self.collectArray) {
            if ( model.user_id.integerValue == sender.tag) {
                currentModel = model;
                NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/offline/supplydetail/%@?device=%@",ImageBaseUrl,currentModel.user_id,guid];
                ADDetailWebViewController *vc=[[ADDetailWebViewController alloc] init];
                vc.urlStr = urlStr;
                vc.urlType = WKWebViewURLNormal;
                vc.navigationItem.title= currentModel.name.length?currentModel.name:@"";
                [self xs_pushViewController:vc];
            }
        }
       
    } else {
        
    }
    
}


#pragma mark lazy load

- (NSMutableArray *)locationArray{
    
    if (_locationArray == nil) {
        
        _locationArray = [NSMutableArray new];
        
    }
    return _locationArray;
}

@end

