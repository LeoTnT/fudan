//
//  homeModel.h
//  App3.0
//
//  Created by xinshang on 2017/7/13.
//  Copyright © 2017年 mac. All rights reserved.
//
/*
 字段	类型及范围	说明
 id	integer	商家id
 user_id	string	商家用户id
 logo	string	店铺logo
 name	string	店铺名
 longitude	string	店铺经度
 latitude	string	店铺维度
 desc	string	店铺简介
 people_consumption	string	人均消费
 quick_percent_bonus	string	代付比例
 order_num	string	已售
 distance	double	距离(需要按距离排序+后台启用定位)
 */
#import <Foundation/Foundation.h>



@interface homeModel : NSObject


@property (nonatomic,strong) NSString *user_id;
@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *offline_approve_supply;
@property (nonatomic,strong) NSString *industry_id;
@property (nonatomic,strong) NSString *industry_name;

@property (nonatomic,strong) NSString *category_id;
@property (nonatomic,strong) NSString *domain;

@property (nonatomic,strong) NSString *logo;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *province;
@property (nonatomic,strong) NSString *city;

@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *county;
@property (nonatomic,strong) NSString *town;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *eva;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *sell_num;

@end

@interface homeCategoryListModel : NSObject
@property (nonatomic,strong) NSString *category_id;
@property (nonatomic,strong) NSString *category_name;
@property (nonatomic,strong) NSString *industry_id;
@property (nonatomic,strong) NSString *logo;
@end

@interface homeItemModel : NSObject

@property (nonatomic,strong) NSString *ID;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSArray *category_list;


@end



@interface homeParamModel : NSObject
@property (nonatomic, strong) homeItemModel *item;
@property (nonatomic, strong) NSArray *itemArr;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *city_code;

@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *adress;
@property (nonatomic, strong) NSString *keyWord;

@end;

/*
 "id":26,
 "name":"企业官网/新闻",
 "image":"",
 "category_list":[
 {
 "category_id":16,
 "category_name":"11222",
 "industry_id":26
 },
 {
 "category_id":15,
 "category_name":"11111112",
 "industry_id":26
 }
 ]
 */


/*
 
 {
 "status":1,
 "info":"",
 "data":[
 {
 "id":76,
 "user_id":1,
 "name":"官方商家",
 "logo":"/upload/common/888888/20170802/2f40f60190712951302c2ed3cac57654.jpg",
 "desc":"这是官方商家提供2",
 "people_consumption":"10.00",
 "quick_percent_bonus":"10.00",
 "longitude":"101.2",
 "latitude":"100.2",
 "order_num":2,
 "distance":"32.32"
 }
 ]
 }
 */
