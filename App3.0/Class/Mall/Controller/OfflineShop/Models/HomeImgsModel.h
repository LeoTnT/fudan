//
//  HomeImgsModel.h
//  App3.0
//
//  Created by xinshang on 2017/7/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeImgsModel : NSObject
@property (nonatomic,strong) NSString * ID;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * image;
@property (nonatomic,strong) NSString * industry_id;
@property (nonatomic,strong) NSString * url;

@property (nonatomic,strong) NSString * subtitle;
@property (nonatomic,strong) NSString * backcolor;
@property (nonatomic,strong) NSString * link_in;
@property (nonatomic,strong) NSString * link_objid;

@end
/*
 首页图标
 basic/industry/Navlist  首页7个图标
 传参
 无
 
 返回参数
 data": [
 {
 "id": 27,               行业id
 "name": "美食/餐厅",   行业名称
 "image": "",          行业图片
 },
 {
 "id": 46,
 "name": "租赁",
 "image": "",
 },
 

 */

/*
 "data":[
 {
 "name":"广告标题",
 "subtitle":"广告子标题",
 "url":"http://nwmvg.zj.dsceshi.cn/index.php?s=/Mobile/Stores/index",
 "image":"/upload/gugert/20170729/06542f93fbcddc36c1b5e82ad401d3e0.jpg",
 "backcolor":"",
 "link_in":"store",
 "link_objid":""
 },
 */
