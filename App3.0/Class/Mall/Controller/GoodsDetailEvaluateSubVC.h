//
//  GoodsDetailEvaluateSubVC.h
//  App3.0
//
//  Created by 孙亚男 on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsDetailEvaluateSubVC : XSBaseTableViewController
@property(nonatomic,copy)NSString *goodsId;
@property(nonatomic,copy)NSString *type;
@end
