//
//  GoodsDetailViewController.h
//  App3.0
//
//  Created by mac on 2017/4/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"

@interface GoodsDetailViewController : XSBaseTableViewController
@property(nonatomic,copy)NSString *goodsID;
@property(nonatomic,copy)NSString *toId;        // 补差价用
@end
