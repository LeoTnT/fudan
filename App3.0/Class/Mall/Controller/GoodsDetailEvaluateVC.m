//
//  GoodsDetailEvaluateVC.m
//  App3.0
//
//  Created by 孙亚男 on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailEvaluateVC.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "GoodsDetailEvaluateSubVC.h"

@interface GoodsDetailEvaluateVC ()<UIScrollViewDelegate>
@property(nonatomic,strong)HMSegmentedControl *segmentControl;
@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation GoodsDetailEvaluateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:@"" action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title=@"宝贝评价";
    [self addSubviews];
}
- (void)addSubviews{
    self.segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    self.segmentControl.sectionTitles =self.titleArray;
    self.segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName:mainColor,NSFontAttributeName:[UIFont systemFontOfSize:15]};
    self.segmentControl.selectedSegmentIndex = 0;
    self.segmentControl.selectionIndicatorHeight = 2;
    self.segmentControl.backgroundColor=[UIColor whiteColor];
    self.segmentControl.segmentWidthStyle=HMSegmentedControlSegmentWidthStyleFixed;
    self.segmentControl.selectionIndicatorColor =mainColor;
    self.segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    //        _segmentControl.borderType = HMSegmentedControlBorderTypeRight;
    self.segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor hexFloatColor:@"666666"],NSForegroundColorAttributeName,[UIFont systemFontOfSize:15],NSFontAttributeName ,nil];
    [self.segmentControl setTitleTextAttributes:dic];
    [self.segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.segmentControl];
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.segmentControl.frame), mainWidth, MAIN_VC_HEIGHT-CGRectGetMaxY(self.segmentControl.frame))];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.bounces = NO;
    self.scrollView.contentSize = CGSizeMake(mainWidth*self.titleArray.count, 0);
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.scrollView];
    [self addViewControllers];
}
- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

-(void)addViewControllers{
    for (int i=0; i<self.titleArray.count; i++) {
        GoodsDetailEvaluateSubVC *otherVC=[[GoodsDetailEvaluateSubVC alloc] init];
        switch (i) {
            case 0:
                otherVC.type=@"all";
                break;
            case 1:
                otherVC.type=@"img";
                break;
            case 2:
                otherVC.type=@"good";
                break;
            case 3:
                otherVC.type=@"normal";
                break;
            case 4:
                otherVC.type=@"bad";
                break;
            default:
                break;
        }
        otherVC.goodsId=self.goodsId;
        [self addChildViewController:otherVC];
        otherVC.view.frame=CGRectMake(mainWidth*i, 0, mainWidth,self.scrollView.frame.size.height);
        [self.scrollView addSubview:otherVC.view];
    }
}
#pragma mark-ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger page = scrollView.contentOffset.x / mainWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
