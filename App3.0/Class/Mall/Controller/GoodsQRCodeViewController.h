//
//  GoodsQRCodeViewController.h
//  App3.0
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"
@interface GoodsQRCodeViewController : XSBaseViewController
@property(nonatomic,strong)GoodsDetailInfo *detailInfo;
@end
