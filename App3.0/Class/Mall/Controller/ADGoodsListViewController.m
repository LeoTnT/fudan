//
//  ADGoodsListViewController.m
//  App3.0
//
//  Created by mac on 2017/4/24.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ADGoodsListViewController.h"
#import "TabMallModel.h"
#import "HotSaleTableViewCell.h"
#import "GoodsDetailViewController.h"
@interface ADGoodsListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *table;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)NSMutableArray *goodsArray;
@property(nonatomic,strong)NSMutableDictionary *paramDic;//参数字典
@end

@implementation ADGoodsListViewController
#pragma mark- LifeCircle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
    //获取数据
    [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
    [self getGoodsListInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Lazy Loading
-(UITableView *)table{
    if (!_table) {
        _table=[[UITableView alloc] initWithFrame:CGRectMake(0, navBarHeight+STATUS_HEIGHT, mainWidth, mainHeight-navBarHeight-STATUS_HEIGHT)];
        _table.delegate=self;
        _table.dataSource=self;
        _table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
        _table.separatorStyle=UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_table];
        [self setRefreshStyle];
    }
    return _table;
}
-(NSMutableArray *)goodsArray{
    if (!_goodsArray) {
        _goodsArray=[NSMutableArray array];
    }
    return _goodsArray;
}
-(NSMutableDictionary *)paramDic{
    if (!_paramDic) {
        _paramDic=[NSMutableDictionary dictionaryWithDictionary:self.paraDic];
    }
    return _paramDic;
}
#pragma mark-Private
-(void)setSubViews{
    self.automaticallyAdjustsScrollViewInsets=NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title=@"商品列表";
    self.view.backgroundColor=BG_COLOR;
}
-(void)setRefreshStyle{
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getGoodsListInfo];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:14];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    _table.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self getMoreGoods];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    _table.mj_footer=footer;
}
-(void)getMoreGoods{
    self.page++;
    [self.paramDic setValue:@(self.page) forKey:@"page"];
    @weakify(self);
    [HTTPManager getADGoodsListWithParaDic:self.paramDic Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.table.mj_footer endRefreshing];
        if (state.status) {
            NSArray *tempArray=dic[@"data"][@"data"];
            if(tempArray.count>0){
                for (NSDictionary *tempDic in tempArray) {
                    TabMallGoodsItem *item=[TabMallGoodsItem mj_objectWithKeyValues:tempDic];
                    [self.goodsArray addObject:item];
                }
                [self.table reloadData];
            }else{
                [XSTool showToastWithView:self.view Text:@"暂无更多商品"];
            }
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        self.page--;
        [self.table.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)getGoodsListInfo{
    self.page=1;
    [self.paramDic setValue:@(self.page) forKey:@"page"];
    @weakify(self);
    [HTTPManager getADGoodsListWithParaDic:self.paramDic Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.table.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.goodsArray removeAllObjects];
            NSArray *tempArray=dic[@"data"][@"data"];
            if (tempArray.count>0) {
                for (NSDictionary *tempDic in tempArray) {
                    TabMallGoodsItem *item=[TabMallGoodsItem mj_objectWithKeyValues:tempDic];
                    [self.goodsArray addObject:item];
                }
                [self.table reloadData];
            }else{
                [XSTool showToastWithView:self.view Text:@"暂无更多商品"];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.table.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)goGoodsDetail:(UITapGestureRecognizer *)tap{
    
    GoodsDetailViewController *detailVC=[[GoodsDetailViewController alloc] init];
    detailVC.goodsID=((HotSaleGoodsView *)tap.view).goodsItem.product_id;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark-TableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotSaleTableViewCell *cell;
    NSString *cellID=@"cellID";
    cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[HotSaleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if(indexPath.row*2+1>=self.goodsArray.count) {
        cell.goodsArray=@[self.goodsArray.lastObject];
        [cell.firstGoodsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goGoodsDetail:)]];
    }else{
        cell.goodsArray=@[self.goodsArray[indexPath.row*2],self.goodsArray[indexPath.row*2+1]];
        [cell.firstGoodsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goGoodsDetail:)]];
        [cell.secondGoodsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goGoodsDetail:)]];
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.goodsArray.count==0) {
        return 0;
    }else{
        return (self.goodsArray.count-1)/2+1;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

@end
