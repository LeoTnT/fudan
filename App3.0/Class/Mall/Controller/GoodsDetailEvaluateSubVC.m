//
//  GoodsDetailEvaluateSubVC.m
//  App3.0
//
//  Created by 孙亚男 on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsDetailEvaluateSubVC.h"
#import "GoodsDetailModel.h"
#import "DefaultView.h"
#import "GoodsEvaluationCell.h"

@interface GoodsDetailEvaluateSubVC ()
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)NSMutableArray *evaArray;
@property(nonatomic,strong)DefaultView*defaultView;
@property(nonatomic,strong)NSMutableDictionary *heightDic;
@end

@implementation GoodsDetailEvaluateSubVC
#pragma mark-Lazy Loading
-(NSMutableArray *)evaArray{
    if (!_evaArray) {
        _evaArray=[NSMutableArray array];
    }
    return _evaArray;
}
-(DefaultView*)defaultView{
    if (!_defaultView) {
        _defaultView=[[DefaultView alloc] initWithFrame:self.view.bounds];
        _defaultView.titleLabel.text=@"暂无相关评价";
        _defaultView.button.hidden=YES;
        _defaultView.baseButton.hidden=YES;
        [self.view addSubview:_defaultView];
    }
    
    return _defaultView;
}
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}

#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableViewStyle=UITableViewStylePlain;
    self.showRefreshHeader=YES;
    self.showRefreshFooter=YES;
    self.tableView.tableFooterView=[UIView new];
    @weakify(self);
    [self setRefreshHeader:^{
        @strongify(self);
        self.page=1;
        [self getNewEvaWithType:self.type];
    }];
    [self setRefreshFooter:^{
        @strongify(self);
        self.page++;
        [self getOldEvaWithType:self.type];
    }];
    [self.tableView.mj_header beginRefreshing];
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:@"" action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title=@"宝贝评价";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
-(void)getNewEvaWithType:(NSString *)type{
    self.page=1;
    NSDictionary *paraDic=@{@"pid":self.goodsId,@"type":type?type:@"all",@"page":@(self.page),@"limit":@(15)};
    @weakify(self);
    [HTTPManager getGoodsEvaluateWithParaDic:paraDic success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            [self.evaArray removeAllObjects];
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                ProductEvaluationItem *item=[ProductEvaluationItem mj_objectWithKeyValues:tempDic];
                [self.evaArray addObject:item];
            }
            if(self.evaArray.count==0){
                self.defaultView.hidden=NO;
                self.tableView.hidden=YES;
            }else{
                self.defaultView.hidden=YES;
                self.tableView.hidden=NO;
                //刷新表格
                [self.tableView reloadData];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)getOldEvaWithType:(NSString *)type{
    self.page++;
    NSDictionary *paraDic=@{@"pid":self.goodsId,@"type":type?type:@"all",@"page":@(self.page),@"limit":@(15)};
    @weakify(self);
    [HTTPManager getGoodsEvaluateWithParaDic:paraDic success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                ProductEvaluationItem *item=[ProductEvaluationItem mj_objectWithKeyValues:tempDic];
                [self.evaArray addObject:item];
            }
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            self.page--;
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        self.page--;
    }];
}
#pragma mark-TableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.evaArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"evaCell";
    GoodsEvaluationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[GoodsEvaluationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.currentVC=self;
    cell.evaItem=self.evaArray[indexPath.row];
    self.heightDic[[NSString stringWithFormat:@"%i-%i",indexPath.section,indexPath.row]]=@(cell.height);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  [self.heightDic[[NSString stringWithFormat:@"%i-%i",indexPath.section,indexPath.row]] floatValue];
}
@end
