//
//  GoodsQRCodeViewController.m
//  App3.0
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsQRCodeViewController.h"
#import "XSQR.h"

@interface GoodsQRCodeViewController ()

@end

@implementation GoodsQRCodeViewController
#pragma mark-Life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
-(void)setSubViews{
//    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title=@"商品二维码";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:@"mall_qrCode_point" htlImage:@"mall_qrCode_point" title:nil action:^{
        @strongify(self);
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"保存图片？" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *save=[UIAlertAction actionWithTitle:Localized(@"save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //保存图片
            @weakify(self);

       [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:self.detailInfo.productQrcode] options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
           @strongify(self);
           if (error) {
            [XSTool showToastWithView:self.view Text:@"图片保存失败"];
           }else{
            UIImageWriteToSavedPhotosAlbum(image, self, nil, NULL);
            [XSTool showToastWithView:self.view Text:@"图片保存成功"];
           }
      }];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:save];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    self.view.backgroundColor=BG_COLOR;
    //添加商品信息的view
    [self addGoodsInfoView];
}
-(void)addGoodsInfoView{
    CGFloat space=10;
    UIView *goodsInfoView=[[UIView alloc] initWithFrame:CGRectMake(space, navBarHeight*2, mainWidth-2*space, mainHeight*0.75)];
    goodsInfoView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:goodsInfoView];
    //商品图片
    UIImageView *goodsImg=[[UIImageView alloc] initWithFrame:CGRectMake(space, 3*space, (mainWidth-2*space)/4.5,mainHeight*0.072 )];
    [goodsImg getImageWithUrlStr:self.detailInfo.productInfo.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    [goodsInfoView addSubview:goodsImg];
    //商品名称
    UILabel *goodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(goodsImg.frame)+space, 2*space, mainWidth-2*space-CGRectGetMaxX(goodsImg.frame), 25)];
    goodsNameLabel.text=self.detailInfo.productInfo.product_name;
    goodsNameLabel.font=[UIFont systemFontOfSize:15];
    [goodsInfoView addSubview:goodsNameLabel];
    //价格
    UILabel *goodsPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(goodsImg.frame)+space, CGRectGetMaxY(goodsNameLabel.frame), mainWidth-2*space-CGRectGetMaxX(goodsImg.frame), 25)];
    goodsPriceLabel.textColor=[UIColor redColor];
    goodsPriceLabel.text=[NSString stringWithFormat:@"¥%@",self.detailInfo.productInfo.sell_price?self.detailInfo.productInfo.sell_price:@"0.00"];
    goodsPriceLabel.font=[UIFont systemFontOfSize:14];
    [goodsInfoView addSubview:goodsPriceLabel];
    //商家名称
    UILabel *supplyNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(goodsImg.frame)+space, CGRectGetMaxY(goodsPriceLabel.frame),mainWidth-2*space-CGRectGetMaxX(goodsImg.frame), 25)];
    supplyNameLabel.textColor=mainGrayColor;
    supplyNameLabel.font=[UIFont systemFontOfSize:14];
    supplyNameLabel.text=self.detailInfo.supplyInfo.name;
    [goodsInfoView addSubview:supplyNameLabel];
    //商品二维码片
    UIImageView *goodsQRImg=[[UIImageView alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(supplyNameLabel.frame)+space, mainWidth-4*space, mainWidth-4*space)];
    goodsQRImg.layer.borderColor=[UIColor colorWithRed:250/255.0 green:179/255.0 blue:82/255.0 alpha:1].CGColor;
    goodsQRImg.layer.borderWidth=10;
    goodsQRImg.image = [XSQR createQrImageWithContentString:self.detailInfo.productQrcode type:XSQRTypePRODUCT];
//    [goodsQRImg getImageWithUrlStr:self.detailInfo.productQrcode andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    [goodsInfoView addSubview:goodsQRImg];
    //惊喜
    UILabel *surpriseLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(goodsQRImg.frame), CGRectGetWidth(goodsInfoView.frame), 40)];
    surpriseLabel.font=[UIFont systemFontOfSize:12];
    surpriseLabel.textColor=mainGrayColor;
    surpriseLabel.textAlignment=NSTextAlignmentCenter;
    surpriseLabel.text=@"扫扫上面二维码，有更多惊喜哦";
    [goodsInfoView addSubview:surpriseLabel];
}
@end
