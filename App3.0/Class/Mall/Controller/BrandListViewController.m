//
//  BrandListViewController.m
//  App3.0
//
//  Created by mac on 2017/6/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BrandListViewController.h"
#import "DefaultView.h"
#import "TabMallModel.h"
#import "BrandListCell.h"
#import "ProductListViewController.h"

@interface BrandListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSMutableArray *brandArray;
@property(nonatomic,strong)DefaultView *defaultView;
@property(nonatomic,assign)int page;
@property(nonatomic,strong)UITextField *searchTextField;
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation BrandListViewController
#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
    [self setRefreshStyle];
    [self.tableView.mj_header beginRefreshing];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(52, (navBarHeight-30)/2.0, self.view.frame.size.width-52*2, 30)];
    self.searchTextField.font = [UIFont systemFontOfSize:15];
    self.searchTextField.backgroundColor = BG_COLOR;
    self.searchTextField.textColor=COLOR_999999;
    self.searchTextField.placeholder=@"搜索品牌";
    self.searchTextField.layer.masksToBounds = YES;
    self.searchTextField.layer.cornerRadius = 15;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchTextField.frame.size.height, self.searchTextField.frame.size.height)];
    UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_search"]];
    leftImg.frame = CGRectMake((30-15)/2.0, (30-15)/2.0, 15, 15);
    [leftView addSubview:leftImg];
    self.searchTextField.leftView=leftView;
    [self.navigationController.navigationBar addSubview:self.searchTextField];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            [view removeFromSuperview];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Lazy Loading
-(NSMutableArray *)brandArray{
    if (!_brandArray) {
        _brandArray=[NSMutableArray array];
    }
    return _brandArray;
}
#pragma mark-Private
-(void)setSubViews{
    self.automaticallyAdjustsScrollViewInsets=NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:@"" htlImage:@"" title:Localized(@"search") action:^{
       @strongify(self);
       [self searchBrands];
   }];
    self.defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
     self.defaultView.titleLabel.text = @"还没有品牌信息哦";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview: self.defaultView];
    self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-navBarHeight-STATUS_HEIGHT) style:UITableViewStylePlain];
    self.tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.tableFooterView=[UIView new];
    [self.view addSubview:_tableView];

}
-(void)setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewBrandInfo];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:14];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.tableView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldBrandInfo];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    self.tableView.mj_footer=footer;
}
-(void)searchBrands{
    [self.searchTextField resignFirstResponder];
    if (self.searchTextField.text.length) {
        [self.tableView.mj_header beginRefreshing];
        [self getNewBrandInfo];
    }
}
-(void)getNewBrandInfo{
    self.page=1;
    NSDictionary *paraDic=@{@"page":@(self.page),@"limit":@(15),@"keyword":self.searchTextField.text.length?self.searchTextField.text:@""};
    @weakify(self);
    [HTTPManager getBrandListParaDic:paraDic Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            [self.brandArray removeAllObjects];
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                Brand *brand=[Brand mj_objectWithKeyValues :tempDic];
                [self.brandArray addObject:brand];
            }
            if (self.brandArray.count==0) {
                self.tableView.hidden=YES;
                self.view.backgroundColor=[UIColor whiteColor];
            }else{
                self.tableView.hidden=NO;
                self.view.backgroundColor=BG_COLOR;
            }
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)getOldBrandInfo{
    self.page++;
    NSDictionary *paraDic=@{@"page":@(self.page),@"limit":@(15),@"keyword":self.searchTextField.text.length?self.searchTextField.text:@""};
    @weakify(self);
    [HTTPManager getBrandListParaDic:paraDic Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            NSArray *arr = dic[@"data"][@"data"];
            if (arr.count == 0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多品牌"];
            } else {
                for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                    Brand *brand=[Brand mj_objectWithKeyValues:tempDic];
                    [self.brandArray addObject:brand];
                }
                [self.tableView reloadData];
            }
            
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        self.page--;
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-TableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.brandArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"cellId";
    BrandListCell *cell;
    cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[BrandListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.brand=self.brandArray[indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductListViewController *pVC = [ProductListViewController new];
    
    ProductModel *model = [ProductModel new];
    model.brand_id = ((Brand *)self.brandArray[indexPath.row]).brand_id;
    pVC.model = model;
//    pVC.brand_id=((Brand *)self.brandArray[indexPath.row]).brand_id;
    [self.navigationController pushViewController:pVC animated:YES];
}
@end
