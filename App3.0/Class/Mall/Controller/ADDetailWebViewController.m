//
//  ADDetailWebViewController.m
//  App3.0
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ADDetailWebViewController.h"

@interface ADDetailWebViewController ()

@end

@implementation ADDetailWebViewController

#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUrlStr:(NSString *)urlStr {
    [super setUrlStr:urlStr];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
}

@end

