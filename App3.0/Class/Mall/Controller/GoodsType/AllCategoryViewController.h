//
//  AllCategoryViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsTypeModel.h"

@protocol AllCategoryDelegate <NSObject>

- (void)changeDataSourceWithCategoryId:(NSString *) CategoryId;

@end
@interface AllCategoryViewController : XSBaseTableViewController

@property (nonatomic, strong) NSMutableArray *typeArray;
@property (nonatomic, weak) id<AllCategoryDelegate> categoryDelegate;
@property (nonatomic, assign)  NSUInteger selectedIndex;
@property (nonatomic, copy) NSString *categoryId;
@end
