//
//  DetailCategoryViewController.h
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCategoryViewController : XSBaseTableViewController
@property (nonatomic, copy) NSString *categoryId;
@end
