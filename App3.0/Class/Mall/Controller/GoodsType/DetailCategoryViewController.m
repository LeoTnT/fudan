//
//  DetailCategoryViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DetailCategoryViewController.h"
#import "GoodsTypeModel.h"
#import "XSToastManager.h"
#import "GoodsTypeTopTableViewCell.h"
#import "TypeGoodsTableViewCell.h"
#import "MJRefresh.h"
#import "ContactModel.h"
#import "ChatViewController.h"
#import "UserInstance.h"
#import "GoodsTypeViewController.h"
#import "GoodsDetailViewController.h"
#import "ProductListViewController.h"
#import "DefaultView.h"

@interface DetailCategoryViewController ()<UITableViewDelegate,UITableViewDataSource,TypeGoodsDelegate,TypeTopViewDelegate>
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, assign) NSUInteger limit;
@property (nonatomic, strong) NSMutableArray *typeGoodsArray;
@property (nonatomic, strong) NSMutableArray *typeArray;
@property (nonatomic, strong) DefaultView *defaultView;
@end

@implementation DetailCategoryViewController
#pragma mark - lazy loadding
- (NSMutableArray *)typeGoodsArray {
    if (_typeGoodsArray==nil) {
        _typeGoodsArray = [NSMutableArray array];
    }
    return _typeGoodsArray;
}
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth-mainWidth*0.25, MAIN_VC_HEIGHT)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = @"暂时没有商品!";
    }
    return _defaultView;
}


- (NSMutableArray *)typeArray {
    if (_typeArray==nil) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}

#pragma mark - life circle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.limit = 8;
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)getBaseInfo {
    self.defaultView.hidden = YES;
    self.tableView.hidden = NO;
    self.page = 1;
    [self.typeGoodsArray removeAllObjects];
    [self.typeArray removeAllObjects];
    
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getCategoryWith:self.categoryId success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        NSUInteger showType = 0;
        if (state.status) {
            GoodsTypeParser *parser = [GoodsTypeParser mj_objectWithKeyValues:dic];
            [self.typeArray addObjectsFromArray:parser.data];
            if (self.typeArray.count) {
                [self.tableView reloadData];
            } else {
                showType = 1;
            }
            @weakify(self);
            [HTTPManager getCategoryGoodsWithPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] limit:[NSString stringWithFormat:@"%lu",(unsigned long)self.limit] cid:self.categoryId stype:@"1" success:^(NSDictionary * _Nullable dic, resultObject *state) {
                @strongify(self);
                NSUInteger displayType=0;
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    BaseTypeGoodsParser *parser = [BaseTypeGoodsParser mj_objectWithKeyValues:dic[@"data"]];
                    if (parser.data.count==0) {
                        
                        displayType=1;
                    } else {
                        [self.typeGoodsArray addObjectsFromArray:parser.data];
                        self.page++;
                        [self.tableView reloadData];
                    }
                    
                    if (showType==1&&displayType==1) {
                        self.tableView.hidden = YES;
                        self.defaultView.hidden = NO;
                    }
                }else {
                     [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)setCategoryId:(NSString *)categoryId {
    _categoryId = categoryId;
    [self getBaseInfo];
}

- (void)upwardPullInfo{
    //    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getCategoryGoodsWithPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] limit:[NSString stringWithFormat:@"%lu",self.limit] cid:self.categoryId stype:@"1" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
          [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            ((GoodsTypeViewController *) self.presentedViewController).view.userInteractionEnabled = YES;
            BaseTypeGoodsParser *parser = [BaseTypeGoodsParser mj_objectWithKeyValues:dic[@"data"]];
          
            if (parser.data.count) {
                [self.typeGoodsArray addObjectsFromArray:parser.data];
                
                [self.tableView reloadData];
                self.page++;
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
          [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)setSubviews{
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.bounces = NO;
    @weakify(self);
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self upwardPullInfo];
    }];
    
}

#pragma mark - TableviewDelegate
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.typeArray.count>0) {
        if (indexPath.row==0) {
            NSString *str = @"tCell";
            GoodsTypeTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
            if (cell==nil) {
                cell = [[GoodsTypeTopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.typeArray = self.typeArray;
            cell.typeView.delegate = self;
            return cell;
        }else {
            BaseTypeGoodsDataParser *dataParser = self.typeGoodsArray[indexPath.row-1];
            NSString *str = @"mCell";
            TypeGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
            if (cell==nil) {
                cell = [[TypeGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.typeGoodsDelegate = self;
            cell.dataParser = dataParser;
            return cell;
        }
        
    } else {
        BaseTypeGoodsDataParser *dataParser = self.typeGoodsArray[indexPath.row];
        NSString *str = @"mCell";
        TypeGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        if (cell==nil) {
            cell = [[TypeGoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.typeGoodsDelegate = self;
        cell.dataParser = dataParser;
        return cell;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.typeArray.count==0) {
        return self.typeGoodsArray.count;
    } else {
        
        return 1+self.typeGoodsArray.count;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0&&self.typeArray.count>0) {
        GoodsTypeTopTableViewCell *cell = (GoodsTypeTopTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.cellHeight;
    }
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass: [TypeGoodsTableViewCell class]]) {
        BaseTypeGoodsDataParser *dataParser = ((TypeGoodsTableViewCell *) cell).dataParser;
        GoodsDetailViewController *detailCpntroller = [[GoodsDetailViewController alloc] init];
        detailCpntroller.goodsID = dataParser.product_id;
        [self.navigationController pushViewController:detailCpntroller animated:YES];
    }
    
}

#pragma mark - TypeGoodsDelegate
- (void)chatSupplyWithId:(NSString *)supply_id {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        [self xs_pushViewController:nil];
        return;
    }
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoWithUid:supply_id success:^(NSDictionary * dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
            YWPerson *person = [[YWPerson alloc] initWithPersonId:parser.data.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [parser.data getName];
            chatVC.avatarUrl = parser.data.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:parser.data.uid type:EMConversationTypeChat createIfNotExist:YES];
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            NSString *title = parser.data.remark;
            if ([parser.data.remark isEqualToString:@""]||[parser.data.remark isEqualToString:@" "]||parser.data.remark.length==0) {
                title = parser.data.nickname;
            }
            chatVC.title = title;
            chatVC.avatarUrl = parser.data.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#else
            XMChatController *chatVC = [XMChatController new];
            SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:parser.data.uid title:[parser.data getName] avatarURLPath:parser.data.logo];
            chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
            [self.navigationController pushViewController:chatVC animated:YES];
#endif
            
        } else {
             [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        @strongify(self);
         [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

#pragma mark - TypeTopViewDelegate
- (void)lookProductListWithCategoryId:(NSString *)categoryId {
    ProductListViewController *productListController = [ProductListViewController new];
    productListController.style = PLCollectionStyleDefault;
//    productListController.cId = @([categoryId integerValue]);
    ProductModel *model = [ProductModel new];
    model.cid = categoryId;
    productListController.model = model;
    productListController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:productListController animated:YES];
}


@end
