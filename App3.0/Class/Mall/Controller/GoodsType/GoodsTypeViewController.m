//
//  GoodsTypeViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsTypeViewController.h"
#import "AllCategoryViewController.h"
#import "DetailCategoryViewController.h"
#import "GoodsTypeModel.h"
#import "XSToastManager.h"
#import "SearchVC.h"
#import "CartVC.h"

@interface GoodsTypeViewController ()<AllCategoryDelegate>
{
    UIView *_searchView;
}
@property (nonatomic,strong) NSMutableArray *typeArray;//类型数组
@property (nonatomic,strong) NSMutableArray *subclassArray;//类型数组
@property (nonatomic,strong) DetailCategoryViewController *detaiLVC;
@property (nonatomic,strong) AllCategoryViewController *allVC;
@property (nonatomic, assign) CGFloat leftWidth;

@end

@implementation GoodsTypeViewController
#pragma mark - lazy loadding
- (NSMutableArray *)typeArray {
    if (_typeArray==nil) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}

- (NSMutableArray *)subclassArray {
    if (_subclassArray==nil) {
        _subclassArray = [NSMutableArray array];
    }
    return _subclassArray;
}

#pragma mark - life circle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden=NO;
    _searchView=[[UIView alloc] initWithFrame:CGRectMake(50, 7, mainWidth-100, 30)];
    _searchView.backgroundColor=BG_COLOR;
    _searchView.layer.cornerRadius = 15;
    _searchView.layer.masksToBounds = YES;
    
    //输入搜索
    UIView *searchField=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_searchView.frame), CGRectGetHeight(_searchView.frame))];
    searchField.backgroundColor=BG_COLOR;
    [_searchView addSubview:searchField];
    _searchView.userInteractionEnabled=YES;
    
    //添加手势搜索
    [_searchView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toNewSearch:)]];
    
    //搜索图片
    UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(10, (30-15)/2.0, 15, 15)];
    searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
    searchImg.userInteractionEnabled=YES;
    [searchField addSubview:searchImg];
    
    //搜索文字
    UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(30-20)/2.0 , mainWidth-110-CGRectGetMaxX(searchImg.frame), 20)];
    searchLabel.userInteractionEnabled=YES;
    searchLabel.text=@"搜索宝贝";
    searchLabel.font=[UIFont systemFontOfSize:15];
    searchLabel.textColor=mainGrayColor;
    [searchField addSubview:searchLabel];
    //添加手势搜索
    [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchGoods)]];
    [_searchView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toNewSearch:)]];
    [self.navigationController.navigationBar addSubview:_searchView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_searchView removeFromSuperview];
    _searchView = nil;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.leftWidth = mainWidth*0.25;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self actionCustomRightBtnWithNrlImage:@"mall_cart_gray" htlImage:@"mall_cart_gray" title:nil action:^{
        @strongify(self);
        [_searchView removeFromSuperview];
        _searchView = nil;
        CartVC *cart = [[CartVC alloc] init];
        [self xs_pushViewController:cart];
//        [self.navigationController pushViewController:cart animated:YES];
    }];
    
    self.navRightBtn.frame = CGRectMake(mainWidth-26-NORMOL_SPACE*2, 2, 26, 26);
    [self getInfo];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)searchGoods {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
}

- (void)getInfo {
    @weakify(self);
    [HTTPManager getCategoryNameSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            [self.typeArray addObjectsFromArray:dic[@"data"]];
            NSLog(@"%@",self.categoryId);
            if (!self.categoryId) {
                self.categoryId = self.typeArray[0][@"category_id"];
              
            }
            [self setSubviews];
            [self addSubviews];

        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)toNewSearch:(UITapGestureRecognizer *)tap {
    NSLog(@"0000000");
}

- (void)setSubviews {
    [self.allVC removeFromParentViewController];
    [self.allVC.view removeFromSuperview];
    //左边类目展示vc
    self.allVC = [[AllCategoryViewController alloc] init];
    self.allVC.view.frame = CGRectMake(0, 0, self.leftWidth, self.view.frame.size.height);
    self.allVC.categoryId = self.categoryId;
    self.allVC.typeArray = self.typeArray;

    [self addChildViewController:self.allVC];

    [self.view addSubview:self.allVC.view];
    __weak typeof(self) wSelf= self;
    self.allVC.categoryDelegate = wSelf;
    
}

- (void)addSubviews{
    [self.detaiLVC removeFromParentViewController];
    [self.detaiLVC.view removeFromSuperview];
    //右边详细内容
    self.detaiLVC = [[DetailCategoryViewController alloc] init];
    self.detaiLVC.view.frame = CGRectMake(self.leftWidth, 0,mainWidth-self.leftWidth, self.view.frame.size.height);
    self.detaiLVC.categoryId = self.categoryId;
    [self addChildViewController:self.detaiLVC];
    [self.view addSubview:self.detaiLVC.view];
}

#pragma mark - AllCategoryDelegate
- (void)changeDataSourceWithCategoryId:(NSString *)CategoryId{
    [self.subclassArray removeAllObjects];
    for (UIViewController *VC in self.childViewControllers) {
        if ([VC isKindOfClass:[DetailCategoryViewController class]]) {
            VC.view.hidden = NO;
            ((DetailCategoryViewController *) VC).categoryId = CategoryId;
            [((DetailCategoryViewController *) VC).tableView reloadData];
        }
    }
}

@end
