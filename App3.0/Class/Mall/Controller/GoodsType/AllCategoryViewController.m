//
//  AllCategoryViewController.m
//  App3.0
//
//  Created by nilin on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AllCategoryViewController.h"
#import "GoodsTypeModel.h"
#import "XSToastManager.h"

@interface AllCategoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *line;

@end

@implementation AllCategoryViewController

- (NSMutableArray *)typeArray {
    if (!_typeArray) {
        _typeArray = [NSMutableArray array];
    }
    return _typeArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self getTypeInfo];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)getTypeInfo {
    @weakify(self);
    [HTTPManager getCategoryNameSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            if (self.typeArray.count==0) {
                [self.typeArray addObjectsFromArray:dic[@"data"]];
            }
            self.selectedIndex = 10000000;
            for (int i=0;i<self.typeArray.count;i++) {
                NSString *tempId = self.typeArray[i][@"category_id"];
                NSLog(@"tempID:%@,id%@",tempId,self.categoryId);
                if ([tempId integerValue]==[self.categoryId integerValue]) {
                    self.selectedIndex = i;
                    break;
                }
            }
            self.tableViewStyle = UITableViewStylePlain;
            self.tableView.bounces = NO;
            self.tableView.showsVerticalScrollIndicator = NO;
            self.tableView.showsHorizontalScrollIndicator = NO;
            self.tableView.backgroundColor = BG_COLOR;
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str = @"tyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setSeparatorInset:UIEdgeInsetsZero];
        cell.backgroundColor = BG_COLOR;
    }
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
    NSString *name = self.typeArray[indexPath.row][@"category_name"];//category_id
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 50)];
    label.lineBreakMode = NSLineBreakByCharWrapping;
    label.text = name;
    label.textAlignment = NSTextAlignmentCenter;
    [cell.contentView addSubview:label];
    label.font = [UIFont systemFontOfSize:15];
    if (mainWidth*0.25<90) {
        cell.textLabel.font = [UIFont systemFontOfSize:12];
    }
    UIColor *textColor = mainGrayColor;
    if (indexPath.row==self.selectedIndex) {
        textColor = mainColor;
    }
    label.textColor = textColor;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedIndex = indexPath.row;
    [self.tableView reloadData];
    NSString *category_id = self.typeArray[indexPath.row][@"category_id"];//category_id
    [self.categoryDelegate changeDataSourceWithCategoryId:category_id];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)viewDidLayoutSubviews {
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
