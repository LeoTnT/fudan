//
//  FDBaseCollectionReusableView.h
//  App3.0
//
//  Created by lichao on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDBaseCollectionReusableView : UICollectionReusableView

+ (void)registerNibHeaderViewlWithCollectionView:(UICollectionView *)collectionView;
+ (void)registerClassHeaderViewlWithCollectionView:(UICollectionView *)collectionView;

+ (void)registerNibFooterViewlWithCollectionView:(UICollectionView *)collectionView;
+ (void)registerClassFooterViewlWithCollectionView:(UICollectionView *)collectionView;

+ (NSString *)identifier;

@end
