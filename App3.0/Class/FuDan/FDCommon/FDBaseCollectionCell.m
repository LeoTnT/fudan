//
//  LCBaseCollectionVCell.h
//  yingliduo
//
//  Created by lichao on 2018/7/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCollectionCell.h"

@implementation FDBaseCollectionCell

+ (void)registerNibCellWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerNib:[UINib nibWithNibName:[self identifier] bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:[self identifier]];
}

+ (void)registerClassCellWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

+ (CGFloat)height {
    return 0;
}

+ (CGFloat)calculateCellHeightWithData:(id)data {
    return 0;
}

@end
