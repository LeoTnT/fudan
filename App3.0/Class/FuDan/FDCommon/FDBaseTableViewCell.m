//
//  LCBaseTableViewCell.h
//  yingliduo
//
//  Created by lichao on 2018/7/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"

@implementation FDBaseTableViewCell

+ (void)registerNibCellWithTableView:(UITableView *)tableView {
    [tableView registerNib:[UINib nibWithNibName:[self identifier] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[self identifier]];
}

+ (void)registerClassCellWithTableView:(UITableView *)tableView {
    [tableView registerClass:[self class] forCellReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

+ (CGFloat)height {
    return 0;
}

+ (CGFloat)calculateCellHeightWithData:(id)data {
    return 0;
}

@end
