//
//  LCBaseCustomView.m
//  yingliduo
//
//  Created by lichao on 2018/7/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

@implementation FDBaseCustomView

#pragma mark -- init
+ (instancetype)initView {
    return [[[NSBundle mainBundle] loadNibNamed:[self identifier] owner:nil options:nil] firstObject];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

+ (CGFloat)height {
    return 0;
}

@end
