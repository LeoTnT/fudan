//
//  FDBaseCollectionReusableView.m
//  App3.0
//
//  Created by lichao on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCollectionReusableView.h"

@implementation FDBaseCollectionReusableView

/** HeaderView */
+ (void)registerNibHeaderViewlWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerNib:[UINib nibWithNibName:[self identifier] bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[self identifier]];
}

+ (void)registerClassHeaderViewlWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerClass:[self class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[self identifier]];
}

/** FooterView */
+ (void)registerNibFooterViewlWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerNib:[UINib nibWithNibName:[self identifier] bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[self identifier]];
}

+ (void)registerClassFooterViewlWithCollectionView:(UICollectionView *)collectionView {
    [collectionView registerClass:[self class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

@end
