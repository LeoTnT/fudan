//
//  LCBaseCustomView.h
//  yingliduo
//
//  Created by lichao on 2018/7/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDBaseCustomView : UIView

+ (instancetype)initView;

+ (NSString *)identifier;

+ (CGFloat)height;

@end
