//
//  LCUITabBar.m
//
//  Created by lichao on 15/11/30.
//  Copyright © 2015年 lichao. All rights reserved.
//

#import "LCTabBar.h"
#import "UIView+LCExtension.h"

@interface LCTabBar()

/*发布按钮*/
//@property(nonatomic, weak) UIButton *publishBtn;

/**
 *  被点击的索引
 */
@property(nonatomic, assign) NSUInteger selectedIndex;

@end

@implementation LCTabBar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        //设置背景图片，@"barBackgound"是一张全通道全透明的图片
        [self setBackgroundImage:[UIImage imageNamed:@"fd_tabbar_clearBg"]];
        //边界线透明化
        self.shadowImage = [[UIImage alloc]init];
        
    }
    return self;
}

#pragma mark - <重写frame - 系统内部的方法>
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.frame;
    frame.size.height = kFDTabbarHeight;
    frame.origin.y -= kFDTabbarHeight-kTabbarHeight;
    self.frame = frame;
}

@end
