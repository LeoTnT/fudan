//
//  LCTabbarController.m
//
//  Created by lichao on 15/11/29.
//  Copyright © 2015年 lichao. All rights reserved.
//

#import "LCTabbarController.h"
#import "LCTabBar.h"
#import "LCNavigationController.h"

#import "FDLoginController.h"
#import "FDTabSecondController.h"

#import "FDTabFirstController.h"//去燥星球
#import "FDNoticeController.h"//星球公告
#import "FDMeController.h"//ME我

@interface LCTabbarController ()<UITabBarControllerDelegate>

@end

@implementation LCTabbarController

#pragma mark -- <初始化tabbar控制器>
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //设置UITabbarItem的属性
    [self setUpItemTitleTextAttributes];
    
    //添加所有子控制器
    [self setUpAllChildVc];
    
    //设置tabBar
    [self setUpTabBar];
    self.delegate = self;
}

/**
 设置tabbar
 */
- (void)setUpTabBar {
    [self setValue:[[LCTabBar alloc] init] forKey:@"TabBar"];
}


#pragma mark -- 设置UITabbarItem的属性
- (void)setUpItemTitleTextAttributes {
    //Normal状态下的文字属性
    NSMutableDictionary *NormalAttrs = [NSMutableDictionary dictionary];
    NormalAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    NormalAttrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    
    //设置选中状态的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    
    //利用Appearance对象统一设置文字
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:NormalAttrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}


#pragma mark -- 添加所有的子控制器
- (void)setUpAllChildVc {
    [self setUpOneChildVc:[[LCNavigationController alloc] initWithRootViewController:[[FDTabFirstController alloc] init]] image:@"Tab_dry_planet"selectedImage:@"Tab_dry_planet_select" title:@"去燥星球"];
    
    [self setUpOneChildVc:[[LCNavigationController alloc] initWithRootViewController:[[FDTabSecondController alloc] init]] image:@"Tab_planet_hole" selectedImage:@"Tab_planet_hole_select" title:@"星球燥洞"];
    
    [self setUpOneChildVc:[[LCNavigationController alloc] initWithRootViewController:[[FDNoticeController alloc] init]] image:@"Tab_planet_notice" selectedImage:@"Tab_planet_notice_select" title:@"星球公告"];
    
    [self setUpOneChildVc:[[LCNavigationController alloc] initWithRootViewController:[[FDMeController alloc] init]] image:@"Tab_mine" selectedImage:@"Tab_mine_select" title:@"ME我"];
}


#pragma mark -- 初始化一个子控制器
/**
 初始化一个子控制器

 @param childVc 需要传入的控制器
 @param image tabBar底部的按钮图片
 @param selectedImage tabBar底部的按钮选中状态下的图片
 @param title tabBar底部的标题
 */
- (void)setUpOneChildVc:(UIViewController *)childVc image:(NSString *)image selectedImage:(NSString *)selectedImage title:(NSString *)title {
    childVc.tabBarItem.title = title;
    
    UIImage *normalImage = [UIImage imageNamed:image];
    UIImage *selImage = [UIImage imageNamed:selectedImage];
    
    if (image.length) childVc.tabBarItem.image = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    if (image.length) childVc.tabBarItem.selectedImage = [selImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self addChildViewController:childVc];
}

#pragma mark -- UITabBarControllerDelegate

/**
 在此处控制某个tabBar子控制器是否需要登录
 */
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {

    //如果是第三个子控制器 则进行判断是否登录
    if ([tabBarController.viewControllers[1] isEqual:viewController] || [tabBarController.viewControllers[3] isEqual:viewController]) {
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];

        if (!guid && guid.length <= 0) {
            UIWindow *window= [UIApplication sharedApplication].keyWindow;
            FDLoginController *login = [[FDLoginController alloc] init];
            XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
            [window.rootViewController presentViewController:navi animated:YES completion:nil];
            return NO;
        }
    }
    
    return YES;
}

@end
