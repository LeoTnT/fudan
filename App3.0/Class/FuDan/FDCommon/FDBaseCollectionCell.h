//
//  FDBaseCollectionCell.H
//  fd
//
//  Created by lichao on 2018/7/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDBaseCollectionCell : UICollectionViewCell

/** 注册nib类型Cell */
+ (void)registerNibCellWithCollectionView:(UICollectionView *)collectionView;
/** 注册code类型Cell */
+ (void)registerClassCellWithCollectionView:(UICollectionView *)collectionView;
/** 获取与cell自身类名相同标识 */
+ (NSString *)identifier;
/** 重写height方法, 自定义cell的高度 */
+ (CGFloat)height;
/** 重写calculate方法, 通过数据计算高度 */
+ (CGFloat)calculateCellHeightWithData:(id)data;

@end
