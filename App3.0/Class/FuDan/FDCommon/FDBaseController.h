//
//  FDBaseController.h
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDBaseController : UIViewController

@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UILabel *navLabel;
@property (nonatomic, strong) UIButton *rightBtn;

/* 当前页面是否隐藏tabbar */
@property (nonatomic, assign) BOOL hidesBottomBarWhenPushed;

- (void)showBackgroudViewWithImageName:(NSString *)imageName;

- (void)setNavTitle:(NSString *)title textColor:(UIColor *)color;

- (void)setLeftBtnWithImageName:(NSString *)imageName block:(void(^)(void))btnBlock;

- (void)setRightBtnWithImageName:(NSString *)imageName block:(void(^)(void))btnBlock;

@end
