//
//  FDBaseController.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

@interface FDBaseController ()

@end

static char *btnAction;

@implementation FDBaseController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.hidesBottomBarWhenPushed) {
        self.tabBarController.tabBar.hidden = YES;
    }else {
        self.tabBarController.tabBar.hidden = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.hidesBottomBarWhenPushed) {
        self.tabBarController.tabBar.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
}

- (void)showBackgroudViewWithImageName:(NSString *)imageName {
    UIImageView *bgImageV = [[UIImageView alloc] init];

    if (imageName &&imageName.length) {
        bgImageV.image = [UIImage imageNamed:imageName];
    }else {
        bgImageV.image = [UIImage imageNamed:@"fd_blue_bg"];
    }
    bgImageV.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    [self.view addSubview:bgImageV];
}

- (void)setNavTitle:(NSString *)title textColor:(UIColor *)color {
    //title
    self.navLabel = [[UILabel alloc] init];
    self.navLabel.text = title;//默认不显示
    self.navLabel.textColor = color;
    self.navLabel.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:self.navLabel];
    [self.navLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(34+7);
        make.centerX.mas_equalTo(self.view);
        make.height.mas_equalTo(17);
    }];
}

- (void)setLeftBtnWithImageName:(NSString *)imageName block:(void(^)(void))btnBlock{
    if (imageName && imageName.length > 0) {
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.leftBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        objc_setAssociatedObject(self.leftBtn, &btnAction, btnBlock, OBJC_ASSOCIATION_COPY);
        [self.leftBtn addTarget:self action:@selector(actionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.leftBtn];
        [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(34);
            make.left.mas_equalTo(self.view).offset(18);
            make.size.mas_equalTo(CGSizeMake(31, 31));
        }];
    }
}

- (void)setRightBtnWithImageName:(NSString *)imageName block:(void(^)(void))btnBlock{
    if (imageName && imageName.length > 0) {
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.rightBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        objc_setAssociatedObject(self.rightBtn, &btnAction, btnBlock, OBJC_ASSOCIATION_COPY);
        [self.rightBtn addTarget:self action:@selector(actionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.rightBtn];
        [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(34);
            make.right.mas_equalTo(self.view).offset(-18);
            make.size.mas_equalTo(CGSizeMake(31, 31));
        }];
    }
}

#pragma mark -actionBtnClick
- (void)actionBtnClick:(UIButton *)btn {
    void (^btnBlock) (void) = objc_getAssociatedObject(btn, &btnAction);
    btnBlock();
}

@end
