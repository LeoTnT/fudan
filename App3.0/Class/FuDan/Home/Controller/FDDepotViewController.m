//
//  FDDepotViewController.m
//  App3.0
//
//  Created by lichao on 2018/10/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDDepotViewController.h"
#import "FDLoginController.h"
#import "FDDepotTopView.h"
#import "GoodsTypeViewController.h"
#import "GoodsDetailViewController.h"
#import "ProductListViewController.h"
#import "S_StoreInformation.h"

@interface FDDepotViewController ()<SDCycleScrollViewDelegate, FDDepotTopDelegate>

//头部滚动广告视图
@property (nonatomic, strong) SDCycleScrollView *bannerView;
/** 顶部广告数组 */
@property (nonatomic ,strong) NSMutableArray *bannerArr;

@end

@implementation FDDepotViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor hexFloatColor:@"C4E7FF"];

    [self setUpBannerView];
    [self setUpTableView];
    [self setUpCustomNav];
    [self requestBannerData];
}

/** 商城顶部广告 */
- (void)requestBannerData {
    
    RACSignal *signal1 = [XSHTTPManager rac_POSTURL:Url_GetMallADInfo params:@{@"flag_str":@"app_dry_index"}];
    [signal1 subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSMutableArray *imageUrlDataSource = [NSMutableArray array];
            NSArray *aaa = state.data[@"app_dry_index"];
            [aaa.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                TabMallADItem *item=[TabMallADItem mj_objectWithKeyValues:x];
                [arr addObject:item];
                [imageUrlDataSource addObject:item.image];
            }completed:^{
                self.bannerArr = arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.bannerView.imageURLStringsGroup = imageUrlDataSource;
                });
            }];
        }
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    self.fd_prefersNavigationBarHidden = YES;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(16, 27, 31, 31);
    [backBtn setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    backBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [backBtn addTarget:self action:@selector(navBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
}
    
- (void)setUpTableView {
    
    CGRect frame = self.view.frame;
    
    self.tableView.frame = frame;
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor hexFloatColor:@"C4E7FF"];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = self.bannerView;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 300;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    FDDepotTopView *topV = [FDDepotTopView initView];
    topV.delegate = self;
    topV.frame = CGRectMake(0, 0, mainWidth, 300);
    return topV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

- (void)navBackBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ===== FDDepotTopDelegate
//购物商城
- (void)goShopping {
    [self.navigationController pushViewController:[self getNameWithClass:@"FDMallController"] animated:YES];
}

//充值消费
- (void)goRecharge {
    [self fd_pushViewController:[self getNameWithClass:@"MobileRechargeVC"]];
}

//出行服务
- (void)goTrip {
    [self fd_pushViewController:[self getNameWithClass:@"TrainTicketHomeVC"]];
}

#pragma mark ===== SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    TabMallADItem *model = self.bannerArr[index];
    [self pushViewControllerWithType:model.link_in linkID:model.link_objid];
}

/** 根据类型跳转 */
- (void)pushViewControllerWithType:(NSString *)type linkID:(NSString *)linkID {
    if ([type isEqualToString:@"category"]) {//类目
        GoodsTypeViewController *controller = [[GoodsTypeViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    }else if ([type isEqualToString:@"product"] || type == nil || [type isEqualToString:@""]) {//商品详情页
        if (linkID.length) {
            GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
            goodsDetailVC.goodsID = linkID;
            [self.navigationController pushViewController:goodsDetailVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"商品不存在"];
        }
    }else if ([type isEqualToString:@"productlists"]) {//商品列表页
        // 商品列表
        NSMutableDictionary *paraDic=[NSMutableDictionary dictionary];
        NSArray *listArray = [linkID componentsSeparatedByString:@";"];
        for (NSString *listStr in listArray) {
            NSArray *tempArray = [listStr componentsSeparatedByString:@"="];
            [paraDic setValue:[tempArray lastObject] forKey:[tempArray firstObject]];
        }
        ProductListViewController *listVC = [[ProductListViewController alloc] init];
        listVC.model = [ProductModel mj_objectWithKeyValues:paraDic];
        [self.navigationController pushViewController:listVC animated:YES];
    }else if ([type isEqualToString:@"store"]) {//店铺
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"店铺不存在"];
        }
    }else if ([type isEqualToString:@"offline"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"FDOfflineStoreController"] animated:YES];
    }else if ([type isEqualToString:@"article"]) {//文章
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"文章不存在"];
        }
    }
}

#pragma mark ===== 功能
- (UIViewController *)getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

- (void)fd_pushViewController:(UIViewController *)vc {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        FDLoginController *loginVC = [[FDLoginController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:loginVC];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ===== 懒加载
/************ 顶部广告轮播 ************/
- (void)setUpBannerView {
    
    self.bannerView.frame = CGRectMake(0, 0, mainWidth, FontNum(280));
    self.bannerView.delegate = self;
    self.bannerView.placeholderImage = [UIImage imageNamed:@"no_pic"];
    self.bannerView.backgroundColor = [UIColor clearColor];
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.hidesForSinglePage = YES;
    self.bannerView.currentPageDotColor = [UIColor grayColor]; // 自定义分页控件小圆标颜色
    self.bannerView.pageDotColor = [UIColor whiteColor];
    self.bannerView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.bannerView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    
    self.bannerView.localizationImageNamesGroup = @[@"no_pic"];
    
    [self.view addSubview:self.bannerView];
}

- (SDCycleScrollView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[SDCycleScrollView alloc] init];
        [self.view addSubview:_bannerView];
    }
    return _bannerView;
}

@end
