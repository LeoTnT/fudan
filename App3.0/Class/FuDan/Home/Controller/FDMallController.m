//
//  FDMallController.m
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMallController.h"
#import "SearchVC.h"
#import "TabHomeMenuCell.h"
#import "TabMallCategoryCell.h"
#import "GussYoulikeCell.h"
#import "ADListViewController.h"
#import "FDMallAdHeaderView.h"
#import "XSShoppingWebViewController.h"//详情webView
#import "FDMallSpecialCell.h"
#import "FDMallGoodsHeaderView.h"
#import "XXCollectionViewCell.h"
#import "GoodsTypeViewController.h"
#import "ProductListViewController.h"
#import "GoodsDetailViewController.h"//商品
#import "S_StoreInformation.h"//店铺
#import "FDMallModel.h"

#import "FDLocationManager.h"//定位

@interface FDMallController ()<UICollectionViewDelegate, UICollectionViewDataSource, SDCycleScrollViewDelegate, TabHomeMenuCellDelegate, FDLocationDelegate>
@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) SDCycleScrollView *shopAdvier;// 顶部录播图

@property (nonatomic, strong) UICollectionView *collectionView;
/** 顶部广告数组 */
@property (nonatomic ,strong) NSMutableArray *adverDataSource;
/** 菜单数组 */
@property (nonatomic, strong) NSMutableArray *mallMenuArray;
/** 专题推荐 */
@property (nonatomic, strong) NSMutableArray *specialTopArr;
@property (nonatomic, strong) NSMutableArray *specialListArr;
/** 精选好物 */
@property (nonatomic, strong) NSMutableArray *goodsArr;

@property (nonatomic, strong) FDLocationManager *FDLocationManager;
/* 定位权限是否开启 */
@property (nonatomic, assign) BOOL isOpen;
/*纬度*/
@property (nonatomic, copy) NSString *lat;
/*经度*/
@property (nonatomic, copy) NSString *lng;

@end

@implementation FDMallController {
    CGFloat itemHeight;
    NSInteger page;
    BOOL isLoad;
    AdvertisementModel *adverItemModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    //获取定位信息
    [self.FDLocationManager beginUpdatingLocation];
    
    [self creatCollectionView];
    [self setRefreshStyle];
    [self setNavView];
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark ===== 网络请求
- (void)requestData {
    
    [self requestMallBannerAndMenuData];
    
    dispatch_group_t requesyGroup = dispatch_group_create();
    dispatch_queue_t requestQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_group_async(requesyGroup, requestQueue, ^{
        [self requestMallData];
        [self requestMallADInfo];
    });
}

/** 获取商城首页数据 */
- (void)requestMallData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetMallIndexData parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDMallModel *model = [FDMallModel mj_objectWithKeyValues:state.data];

            //专题推荐
            FDMallSpecialModel *specialModel = model.tuijian;
            weakSelf.specialTopArr = [NSMutableArray arrayWithArray:[FDMallSpecialTopModel mj_objectArrayWithKeyValuesArray:specialModel.advert]];
            weakSelf.specialListArr = [NSMutableArray arrayWithArray:[FDMallGoodsSubModel mj_objectArrayWithKeyValuesArray:specialModel.list]];
            
            //精选好物
            FDMallGoodsModel *goodsModel = model.jingxuan;
            weakSelf.goodsArr = [NSMutableArray arrayWithArray:[FDMallGoodsSubModel mj_objectArrayWithKeyValuesArray:goodsModel.list]];
            
            [weakSelf.collectionView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

/** 商城顶部广告 */
- (void)requestMallBannerAndMenuData {
    
    RACSignal *signal1 = [XSHTTPManager rac_POSTURL:Url_GetMallADInfo params:@{@"flag_str":@"mobile_index_slide"}];
    [signal1 subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSMutableArray *imageUrlDataSource = [NSMutableArray array];
            NSArray *aaa = state.data[@"mobile_index_slide"];
            [aaa.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                TabMallADItem *item=[TabMallADItem mj_objectWithKeyValues:x];
                [arr addObject:item];
                [imageUrlDataSource addObject:item.image];
            }completed:^{
                self.adverDataSource = arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.shopAdvier.imageURLStringsGroup = imageUrlDataSource;
                });
            }];
        }
    }];
    
    NSMutableArray *itemDataSource = [UserInstance ShardInstnce].mallMenuArray;
    
    if (itemDataSource.count <= 4) {
        itemHeight = 100;
    } else {
        if ([itemDataSource[4] isKindOfClass:[NSString class]]) {
            itemHeight = 100;
        } else {
            itemHeight = 202;
        }
    }
    
//    if (itemDataSource.count == 0 || !itemDataSource) {
    
        //商城首页菜单列表
        RACSignal *signal2 = [XSHTTPManager  rac_POSTURL:Url_GetMallMenuList params:@{@"type":@"mobile_index",@"limit":@(16)}];
        [signal2 subscribeNext:^(resultObject *state) {
            if (state.status) {
                NSMutableArray *arr = [NSMutableArray array];
                NSArray *data = state.data[@"data"];
                [data.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                    TabMallMenuItem *item=[TabMallMenuItem mj_objectWithKeyValues:x];
                    [arr addObject:item];
                }completed:^{
                    [UserInstance ShardInstnce].mallMenuArray = arr;
                    self.mallMenuArray = arr;
                    if (arr.count <= 4) {
                        itemHeight = 100;
                    } else {
                        itemHeight = 202;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isLoad = YES;
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                        FDHomeOneItem *cell = (FDHomeOneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
                        cell.isPlaceholderImage = NO;
                        cell.dataSource = arr;
                    });
                }];
            }
        }];
        
        [signal2 subscribeError:^(NSError * _Nullable error) {
            NSLog(@" 没数据 ");
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            FDHomeOneItem *cell = (FDHomeOneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
            cell.isPlaceholderImage = YES;
            isLoad = YES;
            [self endRefreshing];
        }];
        
//    }else{
//        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
//        FDHomeOneItem *cell = (FDHomeOneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
//        cell.dataSource = itemDataSource;
//    }
}

/** 广告数据(左右两个) */
- (void)requestMallADInfo {
    
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"flag_str":@"mobile_index_right_up,mobile_index_left_up"} success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSDictionary *itemDic = dic[@"data"];
            adverItemModel = [AdvertisementModel mj_objectWithKeyValues:itemDic];
            [self.collectionView reloadData];
        }
    } failure:^(NSError *error) {
        [self endRefreshing];
    }];
}

- (void)setNavView {
    CGFloat navHeight = kStatusBarAndNavigationBarHeight;
    self.navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, navHeight)];
    self.navBgView.backgroundColor = [UIColor whiteColor];
    self.navBgView.userInteractionEnabled = YES;
    [self.view addSubview:self.navBgView];
    //    self.navBgView.alpha = 0.01;
    
    //输入搜索
    UIView *searchField = [[UIView alloc] init];
    searchField.backgroundColor = BG_COLOR;
    searchField.layer.cornerRadius = 15;
    searchField.layer.masksToBounds = YES;
    [self.view addSubview:searchField];
    
    //添加手势搜索
    [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchGoods)]];
    [searchField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(33+12*2);
        make.right.mas_equalTo(self.view).offset(-51);
        make.top.mas_equalTo(self.view).offset(kStatusBarHeight+8);
        make.height.mas_equalTo(33);
    }];
    
    //搜索图片
    UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(12, (33-13)/2.0, 13, 13)];
    searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
    searchImg.userInteractionEnabled = YES;
    [searchField addSubview:searchImg];
    
    //搜索文字
    UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(33-20)/2.0 , 100, 20)];
    searchLabel.userInteractionEnabled = YES;
    searchLabel.text=@"搜索宝贝";
    searchLabel.font=[UIFont systemFontOfSize:12];
    searchLabel.textColor=mainGrayColor;
    [searchField addSubview:searchLabel];
    
    //购物车按钮
    UIButton *shopCartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shopCartBtn setImage:[UIImage imageNamed:@"mall_detail_shoppingCart"] forState:UIControlStateNormal];
    [shopCartBtn addTarget:self action:@selector(goShoppingCart) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shopCartBtn];
    [shopCartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.right.mas_equalTo(self.view).offset(-15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    //导航栏返回按钮
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"nav_back_h"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.left.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 2) {
        return self.goodsArr.count;
    }
    return 1;
}

//每一个分组的上左下右间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        return UIEdgeInsetsZero;
    }else if (section == 1) {
        return UIEdgeInsetsMake(10, 0, 0, 10);
    }else if (section == 2) {
        return UIEdgeInsetsMake(7, 12, 0, 12);
    }
    return UIEdgeInsetsMake(10, 0, 0, 0);
}

//定义每一个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {//导航栏菜单
        return CGSizeMake(mainWidth, itemHeight);
    }else if (indexPath.section == 1){//专题推荐
        return CGSizeMake(mainWidth, 302+55);
    }else if (indexPath.section == 2){//全球精选好物
        return CGSizeMake((mainWidth-24-7)/2, 246);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {//顶部轮播图
        return CGSizeMake(mainWidth, 200);
    }else if (section == 1) {//广告
        return CGSizeMake(mainWidth, 194);
    }else if (section == 2) {//全球精选好物
        return CGSizeMake(mainWidth, 55+10);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 0) {//首页顶部轮播图
            
            UICollectionReusableView *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FD_Section_One_Header" forIndexPath:indexPath];
            advertisement.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [advertisement addSubview:self.shopAdvier];
            advertisement.userInteractionEnabled = YES;
            return advertisement;
            
        }else if (indexPath.section == 1) {//广告
            
            FDMallAdHeaderView *adHeaderV = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[FDMallAdHeaderView identifier] forIndexPath:indexPath];
            adHeaderV.backgroundColor = [UIColor whiteColor];
            adHeaderV.advertisementModel = adverItemModel;

            //点击广告
            kFDWeakSelf;
            adHeaderV.adClickBlock = ^(BOOL isLeft) {
                if (isLeft) {//左边广告
                    TabMallADItem *leftModel = adverItemModel.mobile_index_left_up.firstObject;
                    [weakSelf pushViewControllerWithType:leftModel.link_in linkID:leftModel.link_objid];
                }else {
                    TabMallADItem *rightModel = adverItemModel.mobile_index_right_up.firstObject;
                    [weakSelf pushViewControllerWithType:rightModel.link_in linkID:rightModel.link_objid];
                }
            };
            
            return adHeaderV;
        }else if (indexPath.section == 2) {//全球精选好物
            
            FDMallGoodsHeaderView *adHeaderV = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[FDMallGoodsHeaderView identifier] forIndexPath:indexPath];
            adHeaderV.backgroundColor = [UIColor whiteColor];
            
            return adHeaderV;
        }
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        FDHomeOneItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDHomeOneItem identifier] forIndexPath:indexPath];
        cell.userInteractionEnabled = YES;
        cell.delegate = self;
        return cell;
        
    }else if (indexPath.section == 1) {//专题推荐
        FDMallSpecialCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDMallSpecialCell identifier] forIndexPath:indexPath];
        cell.topArr = self.specialTopArr;
        cell.listArr = self.specialListArr;
        
        kFDWeakSelf;
        //点击广告
        cell.topView.adBlock = ^{
            FDMallSpecialTopModel *topModel = weakSelf.specialTopArr.firstObject;
            [weakSelf pushViewControllerWithType:topModel.link_in linkID:topModel.link_objid];
        };
        //点击商品
        cell.item.listProductBlock = ^(FDMallGoodsSubModel *model) {
            [weakSelf pushViewControllerWithType:nil linkID:model.product_id];
        };
        
        return cell;
    }
    
    //全球精选好物cell
    XXCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"XXCollectionViewCell" forIndexPath:indexPath];
    cell.goodsItem = self.goodsArr[indexPath.row];
    cell.isFloorGood = NO;

    return cell;
}

#pragma mark =====  FDLocationDelegate (定位相关)
- (void)locationIsOpen:(BOOL)isOpen {
    self.isOpen = isOpen;
    
    if (self.isOpen) {//权限打开
        
    }else {//权限未打开
        
    }
}

- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark {
    
    self.lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    //获取定位, 加载数据
    //    [self loadGetUsers];
    
    if (placemark != nil) {
        //        NSString *myAddress;
        //        if (placemark.administrativeArea) {
        //            myAddress = [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.subLocality];
        //        }else {
        //            myAddress = [NSString stringWithFormat:@"%@%@", placemark.locality, placemark.subLocality];
        //        }
        
        //        NSLog(@"定位地址:%@", myAddress);
    }
}

#pragma mark ===== 点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        TabMallGoodsItem *model = self.goodsArr[indexPath.row];
        [self pushViewControllerWithType:nil linkID:model.product_id];
    }
}

//点击顶部广告
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    TabMallADItem *model = self.adverDataSource[index];
    [self pushViewControllerWithType:model.link_in linkID:model.link_objid];
}

//点击搜索框
- (void)searchGoods {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
    [self.navigationController pushViewController:search animated:YES];
}


- (void)backBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

//点击购物车
- (void) goShoppingCart {
    [self xs_pushViewController:[self getNameWithClass:@"CartVC"]];
}

/** 根据类型跳转 */
- (void)pushViewControllerWithType:(NSString *)type linkID:(NSString *)linkID {
    if ([type isEqualToString:@"category"]) {//类目
        GoodsTypeViewController *controller = [[GoodsTypeViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    }else if ([type isEqualToString:@"product"] || type == nil || [type isEqualToString:@""]) {//商品详情页
        if (linkID.length) {
            GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
            goodsDetailVC.goodsID = linkID;
            [self.navigationController pushViewController:goodsDetailVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"商品不存在"];
        }
    }else if ([type isEqualToString:@"productlists"]) {//商品列表页
        // 商品列表
        NSMutableDictionary *paraDic=[NSMutableDictionary dictionary];
        NSArray *listArray = [linkID componentsSeparatedByString:@";"];
        for (NSString *listStr in listArray) {
            NSArray *tempArray = [listStr componentsSeparatedByString:@"="];
            [paraDic setValue:[tempArray lastObject] forKey:[tempArray firstObject]];
        }
        ProductListViewController *listVC = [[ProductListViewController alloc] init];
        listVC.model = [ProductModel mj_objectWithKeyValues:paraDic];
        [self.navigationController pushViewController:listVC animated:YES];
    }else if ([type isEqualToString:@"store"]) {//店铺
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"店铺不存在"];
        }
    }else if ([type isEqualToString:@"offline"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"FDOfflineStoreController"] animated:YES];
    }else if ([type isEqualToString:@"article"]) {//文章
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"文章不存在"];
        }
    }
}

/** 顶部菜单点击事件  */
- (void)didSelectedMenuCell:(TabMallMenuItem *)model {
    
    // 如果url不为空并且是完整外链，则直接跳转网页
    if (!isEmptyString(model.url) && [model.url hasPrefix:@"http"]) {
        XSShoppingWebViewController * shoppingWebVC = [[XSShoppingWebViewController alloc] init];
        shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@", model.url];
        shoppingWebVC.titleString = model.name;
        [self.navigationController pushViewController:shoppingWebVC animated:YES];
        return;
    }
    
    if ([model.link_in isEqualToString:@"collect"]) {//商家众筹-会员众筹
        [self pushToWebViewControllerWithUrl:@"crowdfund/list" type:model.link_objid title:model.name];
    }else  if ([model.link_in isEqualToString:@"collectreal"]) {//实体众筹
        [self pushToWebViewControllerWithUrl:@"real" type:@"" title:model.name];
    }else  if ([model.link_in isEqualToString:@"rank"]) {//福宝等级
        [self.navigationController pushViewController:[self getNameWithClass:@"FDMallUpdateController"] animated:YES];
    }else  if ([model.link_in isEqualToString:@"wallet"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"WalletVC"] animated:YES];
    }else {
        [self pushViewControllerWithType:model.link_in linkID:model.link_objid];
    }
}

- (void)pushToWebViewControllerWithUrl:(NSString *)urlString type:(NSString *)type title:(NSString *)title{
    XSBaseWKWebViewController * shoppingWebVC = [[XSBaseWKWebViewController alloc] init];
    shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/%@?%@&longitude=%@&latitude=%@", ImageBaseUrl, urlString, type, self.lng, self.lat];
    shoppingWebVC.urlType = WKWebViewURLPresell;
    shoppingWebVC.navigationItem.title = title;
    [self xs_pushViewController:shoppingWebVC];
}

- (UIViewController * ) getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

#pragma mark ===== 懒加载
- (NSMutableArray *)specialTopArr {
    if (!_specialTopArr) {
        _specialTopArr = [NSMutableArray array];
    }
    return _specialTopArr;
}

- (NSMutableArray *)specialListArr {
    if (!_specialListArr) {
        _specialListArr = [NSMutableArray array];
    }
    return _specialListArr;
}

- (NSMutableArray *)goodsArr {
    if (!_goodsArr) {
        _goodsArr = [NSMutableArray array];
    }
    return _goodsArr;
}

- (void)creatCollectionView {
    
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 5;
    flowLayout.minimumInteritemSpacing = 2.5;
    _collectionView= [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, mainWidth, mainHeight-kTabbarHeight) collectionViewLayout:flowLayout];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FD_Section_One_Header"];
    
    [FDMallAdHeaderView registerNibHeaderViewlWithCollectionView:self.collectionView];
    
    [FDHomeOneItem registerClassCellWithCollectionView:self.collectionView];
    [FDMallSpecialCell registerClassCellWithCollectionView:self.collectionView];
    
    [FDMallGoodsHeaderView registerClassHeaderViewlWithCollectionView:self.collectionView];
    [_collectionView registerClass:[XXCollectionViewCell class] forCellWithReuseIdentifier:@"XXCollectionViewCell"];

    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.userInteractionEnabled = YES;
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:_collectionView];
}

- (void)setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.collectionView.mj_header=header;
//    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
//        [self getMoreGoodsListInfo];
//    }];
//    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
//    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
//    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
//    //设置字体大小和文字颜色
//    footer.stateLabel.font=[UIFont systemFontOfSize:14];
//    footer.stateLabel.textColor=[UIColor darkGrayColor];
//    self.collectionView.mj_footer=footer;
}

- (void)endRefreshing {
    
    [self.collectionView.mj_footer endRefreshing];
    [self.collectionView.mj_header endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark ===== 懒加载
- (FDLocationManager *)FDLocationManager {
    if (!_FDLocationManager) {
        _FDLocationManager = [[FDLocationManager alloc] init];
        _FDLocationManager.delegate = self;
    }
    return _FDLocationManager;
}

- (SDCycleScrollView *)shopAdvier {
    if (!_shopAdvier) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 198) delegate:self placeholderImage:nil];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        cycleScrollView.hidesForSinglePage = YES;
        cycleScrollView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
        cycleScrollView.pageDotColor = [UIColor whiteColor];
        cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        cycleScrollView.placeholderImage = [UIImage imageNamed:@"no_pic"];
        cycleScrollView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        _shopAdvier =  cycleScrollView;
        
    }
    return _shopAdvier;
}
@end
