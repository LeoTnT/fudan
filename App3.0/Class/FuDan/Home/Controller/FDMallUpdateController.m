//
//  FDMallUpdateController.m
//  App3.0
//
//  Created by lichao on 2018/9/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMallUpdateController.h"
#import "FDUpdateHeaderView.h"
#import "FDUpdateCell.h"

@interface FDMallUpdateController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) LoginDataParser *parser;//用户信息

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation FDMallUpdateController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = Localized(@"商城升级");
    self.dataArr = @[
                     @{@"title":@"消费积分", @"image":@"fd_mall_level"},
                     @{@"title":@"小区积分存量", @"image":@"fd_small_integral"},
                     @{@"title":@"总福宝积分存量", @"image":@"fd_total_integral"},
                     @{@"title":@"团队级别", @"image":@"fd_team_level"}
                     ];
    
    self.parser = [[UserInstance ShardInstnce] getUserInfo];
    
    FDUpdateHeaderView *headerV = [FDUpdateHeaderView initView];
    headerV.frame = CGRectMake(0, 0, mainWidth, 195);
    headerV.parser = self.parser;
    
    self.tableView.tableHeaderView = headerV;
    [FDUpdateCell registerNibCellWithTableView:self.tableView];
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 84;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDUpdateCell identifier]];
    if (cell == nil) {
        cell = [[FDUpdateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDUpdateCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
 
    NSDictionary *dic = self.dataArr[indexPath.section];
    cell.logo.image = [UIImage imageNamed:dic[@"image"]];
    cell.titleLabel.text = dic[@"title"];
    
    return cell;
}

#pragma mark ===== 懒加载
- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = self.view.bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
        
    }
    return _tableView;
}

@end
