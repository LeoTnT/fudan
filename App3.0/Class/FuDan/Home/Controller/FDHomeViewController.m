//
//  FDMallViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHomeViewController.h"
#import "SearchVC.h"
#import "TabMallMenuCell.h"
#import "TabMallCategoryCell.h"
#import "GussYoulikeCell.h"
#import "FDNoticeFooterView.h"
#import "ADListViewController.h"
#import "FDHomeAdHeaderView.h"
#import "XSShoppingWebViewController.h"//详情webView
#import "FDHotSellCell.h"
#import "FDActivityCell.h"
#import "FDHotPlayCell.h"
#import "FDHomeOtherCell.h"
#import "FDMallController.h"
#import "FDHomeModel.h"
#import "FDRepastController.h"//餐饮美食
#import "GoodsDetailViewController.h"//商品
#import "S_StoreInformation.h"//店铺
#import "FDVideoController.h"
#import "S_ChoueseCity.h"

@interface FDHomeViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, SDCycleScrollViewDelegate, TabMallMenuCellDelegate, FDHomeAdHeaderDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) SDCycleScrollView *shopAdvier;// 顶部录播图
@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) NSMutableArray *homeMenuArray;
@property (nonatomic, strong) NSMutableArray *topNoticeItemArray;

/** 首页顶部轮播图广告 */
@property (nonatomic ,strong) NSMutableArray *adverDataSource;
/** 热卖 */
@property (nonatomic, strong) NSMutableArray *hotTopArr;
@property (nonatomic, strong) NSMutableArray *hotListArr;
/** 活动 */
@property (nonatomic, strong) NSMutableArray *activityTopArr;
@property (nonatomic, strong) NSMutableArray *activityListArr;
/** 视频 */
@property (nonatomic, strong) NSMutableArray *videoListArr;
/** 底部列表 */
@property (nonatomic, strong) NSMutableArray *articleTopArr;
@property (nonatomic, strong) NSMutableArray *articleListArr;

@end

@implementation FDHomeViewController {
    CGFloat itemHeight;
    NSInteger page;
    BOOL isLoad;
    AdvertisementModel *adverItemModel;
    FDNoticeFooterView *noticeFooterView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    
    [self creatCollectionView];
    [self setRefreshStyle];
    [self setNavView];
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark ===== 网络请求
- (void)requestData {
    
    [self requestMallBannerAndMenuData];
    dispatch_group_t requesyGroup = dispatch_group_create();
    dispatch_queue_t requestQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_group_async(requesyGroup, requestQueue, ^{
        [self requestMallNoticeData];
        [self requestIndexData];
        [self requestMallADInfo];
    });
}

//首页接口
- (void)requestIndexData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetIndexData parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDHomeModel *model = [FDHomeModel mj_objectWithKeyValues:state.data];
            //热卖
            FDHomeHotsModel *hotModel = model.hots;
            weakSelf.hotTopArr = [NSMutableArray arrayWithArray:[FDHomeTopModel mj_objectArrayWithKeyValuesArray:hotModel.advert]];
            weakSelf.hotListArr = [NSMutableArray arrayWithArray:[FDHomeListModel mj_objectArrayWithKeyValuesArray:hotModel.list]];
            
            //活动
            FDHomeActivesModel *activeModel = model.actives;
            weakSelf.activityTopArr = [NSMutableArray arrayWithArray:[FDHomeTopModel mj_objectArrayWithKeyValuesArray:activeModel.advert]];
            weakSelf.activityListArr = [NSMutableArray arrayWithArray:[FDHomeActivesListModel mj_objectArrayWithKeyValuesArray:activeModel.list]];

            //视频
            FDHomeVideosModel *videoModel = model.videos;
            weakSelf.videoListArr = [NSMutableArray arrayWithArray:[FDVideoListModel mj_objectArrayWithKeyValuesArray:videoModel.list]];
            
            //底部列表
            FDHomeArticleModel *articleModel = model.article;
            weakSelf.articleTopArr = [NSMutableArray arrayWithArray:[FDHomeTopModel mj_objectArrayWithKeyValuesArray:articleModel.advert]];
            weakSelf.articleListArr = [NSMutableArray arrayWithArray:[FDHomeArticleListModel mj_objectArrayWithKeyValuesArray:articleModel.list]];

            [weakSelf.collectionView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        
        [weakSelf endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//商城首页广告
- (void)requestMallBannerAndMenuData {
    
    kFDWeakSelf;
    RACSignal *signal = [XSHTTPManager rac_POSTURL:Url_GetMallADInfo params:@{@"flag_str":@"mobile_index_slide"}];
    [signal subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSMutableArray *imageUrlDataSource = [NSMutableArray array];
            NSArray *aaa = state.data[@"mobile_index_slide"];
            [aaa.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                TabMallADItem *item=[TabMallADItem mj_objectWithKeyValues:x];
                [arr addObject:item];
                [imageUrlDataSource addObject:item.image];
            }completed:^{
                weakSelf.adverDataSource = arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.shopAdvier.imageURLStringsGroup = imageUrlDataSource;
                });
            }];
        }
    }];
    
    NSMutableArray *itemDataSource = [UserInstance ShardInstnce].homeMenuArray;
    
    if (itemDataSource.count <= 4) {
        itemHeight = 100;
    } else {
        if ([itemDataSource[4] isKindOfClass:[NSString class]]) {
            itemHeight = 100;
        } else {
            itemHeight = 202;
        }
    }
    
    if (itemDataSource.count == 0 || !itemDataSource) {
        
        //首页菜单列表
        RACSignal *signal = [XSHTTPManager  rac_POSTURL:Url_GetMallMenuList params:@{@"type":@"app_index",@"limit":@(16)}];
        [signal subscribeNext:^(resultObject *state) {
            if (state.status) {
                NSMutableArray *arr = [NSMutableArray array];
                NSArray *data = state.data[@"data"];
                [data.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                    TabMallMenuItem *item=[TabMallMenuItem mj_objectWithKeyValues:x];
                    [arr addObject:item];
                }completed:^{
                    [UserInstance ShardInstnce].homeMenuArray = arr;
                    weakSelf.homeMenuArray = arr;
                    if (arr.count <= 4) {
                        itemHeight = 100;
                    } else {
                        itemHeight = 202;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isLoad = YES;
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                        OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
                        cell.isPlaceholderImage = NO;
                        cell.dataSource = arr;
                    });
                }];
            }
        }];
        
        [signal subscribeError:^(NSError * _Nullable error) {
            NSLog(@" 没数据 ");
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
            cell.isPlaceholderImage = YES;
            isLoad = YES;
            [weakSelf endRefreshing];
            
        }];
        
    }else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        OneItem *cell = (OneItem *)[self.collectionView cellForItemAtIndexPath:indexPath];
        cell.dataSource = itemDataSource;
    }
}

/** 公告 数据 */
- (void)requestMallNoticeData {
    
    NSMutableArray *arr = [NSMutableArray array];
    NSMutableArray *arr1 = [NSMutableArray array];
    kFDWeakSelf;
    [HTTPManager getMallNoticeInfoWithSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            for (NSDictionary *tempDic in dic[@"data"][@"data"]) {
                TabMallNoticeItem *item = [TabMallNoticeItem mj_objectWithKeyValues:tempDic];
                [arr1 addObject:item];
                [arr addObject:item.title];
            }
            weakSelf.topNoticeItemArray = arr1;
            noticeFooterView.noticeV.arr = arr;
        }
        [weakSelf endRefreshing];
    } fail:^(NSError * _Nonnull error) {
        [weakSelf endRefreshing];
    }];
    
}

/** 公告专区 数据 */
- (void)requestMallADInfo {
    
    __weak typeof(self) weakSelf = self;
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"flag_str":@"mobile_index_left,mobile_index_topright,mobile_index_bottomright_left"} success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSDictionary *itemDic = dic[@"data"];
            adverItemModel = [AdvertisementModel mj_objectWithKeyValues:itemDic];
            [weakSelf.collectionView reloadData];
        }
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
    }];
}

- (void)setNavView {
    CGFloat navHeight = kStatusBarAndNavigationBarHeight;
    self.navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, navHeight)];
    self.navBgView.backgroundColor = [UIColor whiteColor];
    self.navBgView.userInteractionEnabled = YES;
    [self.view addSubview:self.navBgView];
//    self.navBgView.alpha = 0.01;
    
    //输入搜索
    UIView *searchField = [[UIView alloc] init];
    searchField.backgroundColor = BG_COLOR;
    searchField.layer.cornerRadius = 15;
    searchField.layer.masksToBounds = YES;
    [self.view addSubview:searchField];
    
    //添加手势搜索
    [searchField addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchGoods)]];
    [searchField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(33+12*2);
        make.right.mas_equalTo(self.view).offset(-69);
        make.top.mas_equalTo(self.view).offset(kStatusBarHeight+8);
        make.height.mas_equalTo(33);
    }];
    
    //搜索图片
    UIImageView *searchImg=[[UIImageView alloc] initWithFrame:CGRectMake(12, (33-13)/2.0, 13, 13)];
    searchImg.image=[UIImage imageNamed:@"mall_nav_search"];
    searchImg.userInteractionEnabled = YES;
    [searchField addSubview:searchImg];
    
    //搜索文字
    UILabel *searchLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchImg.frame)+6,(33-20)/2.0 , 100, 20)];
    searchLabel.userInteractionEnabled = YES;
    searchLabel.text=@"搜索宝贝";
    searchLabel.font=[UIFont systemFontOfSize:12];
    searchLabel.textColor=mainGrayColor;
    [searchField addSubview:searchLabel];
    
    //导航栏右按钮
    UIButton *areaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [areaBtn setImage:[UIImage imageNamed:@"cart_gostore_arrow"] forState:UIControlStateNormal];
    [areaBtn setTitle:Localized(@"地区") forState:UIControlStateNormal];
    [areaBtn setTitleColor:[UIColor hexFloatColor:@"666666"] forState:UIControlStateNormal];
    areaBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [areaBtn addTarget:self action:@selector(areaBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self setImageToRightWithButton:areaBtn];
    [self.view addSubview:areaBtn];
    [areaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.left.mas_equalTo(searchField.mas_right);
        make.right.mas_equalTo(self.view);
    }];
    
    
    UIImageView *mallLogo = [UIImageView new];
    [mallLogo getImageWithUrlStr:[AppConfigManager ShardInstnce].appConfig.logo_mobile andDefaultImage:[UIImage imageNamed:@"fuDan_logo"]];
    [self.view addSubview:mallLogo];
    [mallLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchField);
        make.left.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

//每一个分组的上左下右间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        return UIEdgeInsetsZero;
    }
    return UIEdgeInsetsMake(10, 0, 0, 0);
}

//定义每一个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {//导航栏菜单
        return CGSizeMake(mainWidth, itemHeight);
    }else if (indexPath.section == 1){//热卖推荐
        if (self.hotListArr.count > 0) {
            return CGSizeMake(mainWidth, 300+44);            
        }
        return CGSizeMake(mainWidth, 129+44);
    }else if (indexPath.section == 2){//参与活动
        if (self.activityListArr.count > 0) {
            return CGSizeMake(mainWidth, 355+44);
        }
        return CGSizeMake(mainWidth, 129+44);
    }else if (indexPath.section == 3){//热播推荐
        if (self.videoListArr.count > 0) {
            return CGSizeMake(mainWidth, 200+44);
        }
        return CGSizeMake(mainWidth, +44);
    }else if (indexPath.section == 4){//列表
        return CGSizeMake(mainWidth, 119*self.articleListArr.count+162);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {//顶部轮播图
        return CGSizeMake(mainWidth, 200);
    } else if (section == 1) {//公告专区
        return CGSizeMake(mainWidth, 230);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if (section == 0) return CGSizeMake(mainWidth, 50);
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 0) {//首页顶部轮播图
            UICollectionReusableView *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FD_Section_One_Header" forIndexPath:indexPath];
            advertisement.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [advertisement addSubview:self.shopAdvier];
            advertisement.userInteractionEnabled = YES;
            return advertisement;
            
        }else if (indexPath.section == 1) {//公告专区
            FDHomeAdHeaderView *advertisement = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FDHomeAdHeaderView" forIndexPath:indexPath];
            advertisement.backgroundColor = [UIColor whiteColor];
            
            advertisement.itemDataSource = adverItemModel;
            advertisement.sectionDelegate = self;
            return advertisement;
        }
    }
    if (kind == UICollectionElementKindSectionFooter) {//公告
        
        noticeFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FDNoticeFooterView" forIndexPath:indexPath];
        noticeFooterView.backgroundColor = [UIColor whiteColor];
        noticeFooterView.userInteractionEnabled = YES;
        //点击公告
        @weakify(self);
        noticeFooterView.noticeV.TapAction = ^{
            @strongify(self);
            [self goNoticeDetail];
        };
        
        return noticeFooterView;
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {//导航菜单
        OneItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[OneItem identifier] forIndexPath:indexPath];
        cell.userInteractionEnabled = YES;
        cell.delegate = self;
        return cell;
    }else if (indexPath.section == 1) {//热卖推送
        FDHotSellCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDHotSellCell identifier] forIndexPath:indexPath];
        
        cell.hotTopArr = self.hotTopArr;
        cell.hotListArr = self.hotListArr;
        
        kFDWeakSelf;
        //点击顶部广告
        cell.topView.adBlock = ^{
            FDHomeTopModel *topModel = weakSelf.hotTopArr.firstObject;
            [weakSelf pushViewControllerWithType:topModel.link_in linkID:topModel.link_objid];
        };
        //点击产品列表
        cell.item.listProductBlock = ^(FDHomeListModel *listModel) {
            [weakSelf pushViewControllerWithType:nil linkID:listModel.product_id];
        };
    
        return cell;
    }else if (indexPath.section == 2){//参与活动
        FDActivityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDActivityCell identifier] forIndexPath:indexPath];
        cell.actTopArr = self.activityTopArr;
        cell.actListArr = self.activityListArr;
        
        kFDWeakSelf;
        //点击更多
        cell.topView.moreBlock = ^{
            XSBaseWKWebViewController *zcListVC = [[XSBaseWKWebViewController alloc] init];
            NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
            if (guid.length <= 0) {
                guid = @"app";
            }
            zcListVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/crowdfund/list?type=supply&device=%@",ImageBaseUrl,guid];
            zcListVC.urlType = WKWebViewURLPresell;
            zcListVC.navigationItem.title = Localized(@"活动列表");
            [weakSelf xs_pushViewController:zcListVC];
        };
        //点击广告
        cell.activityAdBlock = ^{
            FDHomeTopModel *topModel = weakSelf.activityTopArr.firstObject;
            [weakSelf pushViewControllerWithType:topModel.link_in linkID:topModel.link_objid];
        };
        //点击列表
        cell.activityCellBlock = ^(FDHomeActivesListModel *model) {
            
            XSBaseWKWebViewController *zcDetailVC = [[XSBaseWKWebViewController alloc] init];
            zcDetailVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/crowdfund/detail/%@?type=real",ImageBaseUrl, model.product_id];
            zcDetailVC.urlType = WKWebViewURLPresell;
            zcDetailVC.navigationItem.title = Localized(@"活动详情");
            [weakSelf xs_pushViewController:zcDetailVC];
        };
        return cell;
    }else if (indexPath.section == 3) {//热播推荐
        FDHotPlayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDHotPlayCell identifier] forIndexPath:indexPath];
        cell.playListArr = self.videoListArr;
        
        kFDWeakSelf;
        //点击更多
        cell.topView.moreBlock = ^{
            [weakSelf.navigationController pushViewController:[self getNameWithClass:@"FDMediaVideoController"] animated:YES];
        };
        
        cell.videoBlock = ^(FDVideoListModel *videoModel) {
            FDVideoController *detailVC = [[FDVideoController alloc] init];
            detailVC.videoModel = videoModel;
            [weakSelf.navigationController pushViewController:detailVC animated:YES];
        };
        
        return cell;
    }else {//底部列表
        FDHomeOtherCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDHomeOtherCell identifier] forIndexPath:indexPath];
        cell.topArr = self.articleTopArr;
        cell.listArr = self.articleListArr;
        MJWeakSelf;
        //点击列表顶部广告
        cell.headerV.logoClickBlock = ^{
//            FDHomeTopModel *model = weakSelf.articleTopArr.firstObject;
//            XSBaseWKWebViewController *adWebVC = [[XSBaseWKWebViewController alloc] init];
//            adWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/article/detail/%@", ImageBaseUrl, model.link_objid];
//            adWebVC.urlType = WKWebViewURLNormal;
//            adWebVC.navigationItem.title = model.name;
//            [weakSelf xs_pushViewController:adWebVC];
        };
        //点击列表
        cell.articleBlock = ^(FDHomeArticleListModel *model) {
            XSBaseWKWebViewController *adWebVC = [[XSBaseWKWebViewController alloc] init];
            adWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/article/detail/%@", ImageBaseUrl, model.article_id];
            adWebVC.urlType = WKWebViewURLNormal;
            adWebVC.navigationItem.title = model.title;
            [weakSelf xs_pushViewController:adWebVC];
        };
        
        return cell;
    }
}

#pragma mark ===== 点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

//点击首页顶部广告
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    TabMallADItem *model = self.adverDataSource[index];
    [self pushViewControllerWithType:model.link_in linkID:model.link_objid];
}

//点击搜索框
- (void)searchGoods {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
    [self.navigationController pushViewController:search animated:YES];
}

//点击 公告
- (void)goNoticeDetail{
    
    if (self.topNoticeItemArray.count ==0) return;
    //公告列表
    ADListViewController *controller = [[ADListViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

//点击地区
- (void)areaBtnClick:(UIButton *)sender {
    S_ChoueseCity *chouseCity =  [[S_ChoueseCity alloc] init];
    [chouseCity setSelectedCountry:^(NSDictionary *dic,NSString *showTitle) {

        [sender setTitle:showTitle forState:UIControlStateNormal];
    }];
    
    
    [chouseCity setSelectedCity:^(NSDictionary *dic, NSString *title) {

        [sender setTitle:title forState:UIControlStateNormal];
    }];
    
    chouseCity.showAddress = sender.titleLabel.text;
    [self.navigationController pushViewController:chouseCity animated:YES];
}

/** 根据类型跳转 */
- (void)pushViewControllerWithType:(NSString *)type linkID:(NSString *)linkID {
    if ([type isEqualToString:@"product"] || type == nil || [type isEqualToString:@""]) {//商品详情页
        if (linkID.length) {
            GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
            goodsDetailVC.goodsID = linkID;
            [self.navigationController pushViewController:goodsDetailVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"商品不存在"];
        }
    }else if ([type isEqualToString:@"store"]) {//店铺
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"店铺不存在"];
        }
    }else if ([type isEqualToString:@"article"]) {//文章
        if (linkID.length) {
            S_StoreInformation *storeVC = [[S_StoreInformation alloc] init];
            storeVC.storeInfor = linkID;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else{
            [XSTool showToastWithView:self.view Text:@"文章不存在"];
        }
    }
}

#pragma mark ===== SectionHeaderDelegate
- (void)didSelectedItem:(UITapGestureRecognizer *)tap {
    
    NSInteger index = tap.view.tag;
    
    TabMallADItem *selectedItem;
    switch (index) {
        case 1:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_left[0];
            break;
        case 2:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_topright[0];
            break;
        case 3:
            selectedItem = (TabMallADItem *)adverItemModel.mobile_index_bottomright_left[0];
            break;
            
        default:
            break;
    }
    [self pushViewControllerWithType:selectedItem.link_in linkID:selectedItem.link_objid];
}

/** 顶部菜单点击事件  */
- (void)didSelectedMenuCell:(TabMallMenuItem *)model {
    // 如果url不为空并且是完整外链，则直接跳转网页
    if (!isEmptyString(model.url) && [model.url hasPrefix:@"http"]) {
        XSShoppingWebViewController * shoppingWebVC = [[XSShoppingWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@?device=%@",model.url, gui];
        shoppingWebVC.titleString = model.name;
        [self.navigationController pushViewController:shoppingWebVC animated:YES];
        return;
    }
    
    if ([model.name isEqualToString:@"商城"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"FDMallController"] animated:YES];
    }else if ([model.name isEqualToString:@"拼购"]) {
        // 拼团 对接Vue
        //        [self xs_pushViewController:[self getNameWithClass:@"GroupBuyListViewController"]];
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/groupbuy?device=%@",ImageBaseUrl,gui];
        adWeb.urlType = WKWebViewURLGroupBuy;
        adWeb.navigationItem.title = @"拼购";
        [self xs_pushViewController:adWeb];
    }else if ([model.name isEqualToString:@"充值缴费"]) {
        [self xs_pushViewController:[self getNameWithClass:@"MobileRechargeVC"]];
//        [self.navigationController pushViewController:[self getNameWithClass:@"MobileRechargeVC"] animated:YES];
    }else if ([model.name isEqualToString:@"火车/飞机票"]) {
        [self xs_pushViewController:[self getNameWithClass:@"TrainTicketHomeVC"]];
    }else if ([model.name isEqualToString:@"交友聊天"]) {
        self.tabBarController.selectedIndex = 3;
    }else if ([model.name isEqualToString:@"餐饮美食"]) {
        FDRepastController *vc =[[FDRepastController alloc] init];
        [self xs_pushViewController:vc];
    }else if ([model.name isEqualToString:@"媒体视频"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"FDMediaVideoController"] animated:YES];
    }else if ([model.name isEqualToString:@"游戏互动"]) {
        
    }
}

- (UIViewController *)getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

//设置图片居右
- (void)setImageToRightWithButton:(UIButton *)btn {
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}

#pragma mark ===== 懒加载
- (void)creatCollectionView {
    
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 5;
    flowLayout.minimumInteritemSpacing = 2.5;
    _collectionView= [[UICollectionView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight-kStatusBarAndNavigationBarHeight) collectionViewLayout:flowLayout];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FD_Section_One_Header"];
    [_collectionView registerClass:[FDNoticeFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FDNoticeFooterView"];

    [FDHomeAdHeaderView registerClassHeaderViewlWithCollectionView:self.collectionView];
    
    [OneItem registerClassCellWithCollectionView:self.collectionView];
    [FDHotSellCell registerClassCellWithCollectionView:self.collectionView];
    [FDActivityCell registerClassCellWithCollectionView:self.collectionView];
    [FDHotPlayCell registerClassCellWithCollectionView:self.collectionView];
    [FDHomeOtherCell registerClassCellWithCollectionView:self.collectionView];

    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.userInteractionEnabled = YES;
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:_collectionView];
}

- (void)setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.collectionView.mj_header=header;
//    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
        //        [self getMoreGoodsListInfo];
//    }];
//    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
//    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
//    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
//    //设置字体大小和文字颜色
//    footer.stateLabel.font=[UIFont systemFontOfSize:14];
//    footer.stateLabel.textColor=[UIColor darkGrayColor];
//    self.collectionView.mj_footer=footer;
}

- (void)endRefreshing {
    
    [self.collectionView.mj_footer endRefreshing];
    [self.collectionView.mj_header endRefreshing];
}

- (NSMutableArray *)hotTopArr {
    if (!_hotTopArr) {
        _hotTopArr = [NSMutableArray array];
    }
    return _hotTopArr;
}

- (NSMutableArray *)hotListArr {
    if (!_hotListArr) {
        _hotListArr = [NSMutableArray array];
    }
    return _hotListArr;
}

- (NSMutableArray *)activityTopArr {
    if (!_activityTopArr) {
        _activityTopArr = [NSMutableArray array];
    }
    return _activityTopArr;
}

- (NSMutableArray *)activityListArr {
    if (!_activityListArr) {
        _activityListArr = [NSMutableArray array];
    }
    return _activityListArr;
}

- (NSMutableArray *)videoListArr {
    if (!_videoListArr) {
        _videoListArr = [NSMutableArray array];
    }
    return _videoListArr;
}

- (NSMutableArray *)articleTopArr {
    if (!_articleTopArr) {
        _articleTopArr = [NSMutableArray array];
    }
    return _articleTopArr;
}

- (NSMutableArray *)articleListArr {
    if (!_articleListArr) {
        _articleListArr = [NSMutableArray array];
    }
    return _articleListArr;
}

- (SDCycleScrollView *)shopAdvier {
    if (!_shopAdvier) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 198) delegate:self placeholderImage:nil];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        cycleScrollView.hidesForSinglePage = YES;
        cycleScrollView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
        cycleScrollView.pageDotColor = [UIColor whiteColor];
        cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        cycleScrollView.placeholderImage = [UIImage imageNamed:@"no_pic"];
        cycleScrollView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
        _shopAdvier =  cycleScrollView;
        
    }
    return _shopAdvier;
}

@end

