//
//  FDTabFirstController.m
//  App3.0
//
//  Created by lichao on 2018/10/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDTabFirstController.h"
#import "FDLoginController.h"

#import "FDMediaVideoController.h"//星球接入
#import "OffLineWebViewController.h"//众筹订单
#import "FDDepotViewController.h"//补给站

#import "FDLocationManager.h"//定位

@interface FDTabFirstController ()<FDLocationDelegate>
/** 保存上一次点击的按钮 */
@property (nonatomic, weak) UIButton *lastButton;

@property (nonatomic, strong) FDLocationManager *FDLocationManager;
/* 定位权限是否开启 */
@property (nonatomic, assign) BOOL isOpen;
/*纬度*/
@property (nonatomic, copy) NSString *lat;
/*经度*/
@property (nonatomic, copy) NSString *lng;

@end

@implementation FDTabFirstController

//修改状态栏的前景色
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //view出现的时候状态栏前景颜色改为亮色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //view消失的时候状态栏前景颜色改为默认/黑色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    UIImageView *bgImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_firstvc_bg"]];
    bgImageV.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    [self.view addSubview:bgImageV];
    
    [self setUpUI];
    //获取定位信息
    [self.FDLocationManager beginUpdatingLocation];
}

#pragma mark ===== SetUpUI
- (void)setUpUI {
    
    NSArray *btnArr = @[Localized(@"星球燥洞"), Localized(@"星球接入"), Localized(@"星球大战"), Localized(@"创物志"), Localized(@"传送点"), Localized(@"星球补给站")];
    
    for (int i = 0; i<btnArr.count; i++) {
        UIButton *menuBtn = [self setCustomeButtonWithTitle:btnArr[i] tag:i];
        [self.view addSubview:menuBtn];
        
        CGFloat width = FontNum(98);
        CGFloat height = FontNum(31);
        CGFloat space = 3;
        if (i <= 2) {
            [menuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.view).offset(128+(height+space)*i);
                make.right.mas_equalTo(self.view).offset(-43);
                make.size.mas_equalTo(CGSizeMake(width, height));
            }];
        }else if (i > 2 && i <= 5) {
            CGFloat temp = abs(i-5);
            [menuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.view).offset(-(150+(height+space)*temp));
                make.left.mas_equalTo(self.view).offset(37);
                make.size.mas_equalTo(CGSizeMake(width, height));
            }];
        }
    }
}

#pragma mark ===== 点击事件
- (void)buttonClick:(UIButton *)sender {
//    sender.selected = !sender.selected;
//
//    if (self.lastButton) {
//        self.lastButton.selected = NO;
//    }
//    self.lastButton = sender;
    
    switch (sender.tag) {
        case 0: {//星球燥洞
            if (isEmptyString([UserInstance ShardInstnce].uid)) {
                FDLoginController *loginVC = [[FDLoginController alloc] init];
                XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:loginVC];
                [self presentViewController:navi animated:YES completion:nil];
            } else {
                self.tabBarController.selectedIndex = 1;
            }
        }
            break;
        case 1: {//星球接入->(媒体视频界面)
            FDMediaVideoController *mvVC = [[FDMediaVideoController alloc] init];
            [self fd_pushViewController:mvVC];
        }
            break;
        case 2: {//星球大战->小游戏，这个暂时不用管，后期做完后，做下跳转处理
            [XSTool showToastWithView:self.view Text:@"正在开发中.."];
        }
            break;
        case 3: {//创物志->(众筹模块)
            [self pushToWebViewControllerWithUrl:@"real" type:@"" title:Localized(@"实体众筹")];
        }
            break;
        case 4: {//传送点->众筹已完成的功能板块，可以看到店铺流水情况
            [self pushToWebViewControllerWithUrl:@"real/user" type:@"" title:Localized(@"实体众筹")];
        }
            break;
        case 5: {//星球补给站->费购物，生活缴费等界面显示
            FDDepotViewController *depotVC = [[FDDepotViewController alloc] init];
            [self.navigationController pushViewController:depotVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark =====  FDLocationDelegate (定位相关)
- (void)locationIsOpen:(BOOL)isOpen {
    self.isOpen = isOpen;
    
    if (self.isOpen) {//权限打开
        
    }else {//权限未打开
        
    }
}

- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark {
    
    self.lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    //获取定位, 加载数据
    //    [self loadGetUsers];
    
    if (placemark != nil) {
        //        NSString *myAddress;
        //        if (placemark.administrativeArea) {
        //            myAddress = [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.subLocality];
        //        }else {
        //            myAddress = [NSString stringWithFormat:@"%@%@", placemark.locality, placemark.subLocality];
        //        }
        
        //        NSLog(@"定位地址:%@", myAddress);
    }
}

- (void)pushToWebViewControllerWithUrl:(NSString *)urlString type:(NSString *)type title:(NSString *)title {
    XSBaseWKWebViewController * shoppingWebVC = [[XSBaseWKWebViewController alloc] init];
    shoppingWebVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/%@?%@&longitude=%@&latitude=%@", ImageBaseUrl, urlString, type, self.lng, self.lat];
    shoppingWebVC.urlType = WKWebViewURLPresell;
    shoppingWebVC.navigationItem.title = title;
    [self fd_pushViewController:shoppingWebVC];
}

- (void)fd_pushViewController:(UIViewController *)vc {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        FDLoginController *loginVC = [[FDLoginController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:loginVC];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ===== 懒加载
- (FDLocationManager *)FDLocationManager {
    if (!_FDLocationManager) {
        _FDLocationManager = [[FDLocationManager alloc] init];
        _FDLocationManager.delegate = self;
    }
    return _FDLocationManager;
}

#pragma mark ===== 功能
- (UIButton *)setCustomeButtonWithTitle:(NSString *)title tag:(NSInteger)tag{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"fd_bg_normal"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"fd_bg_selected"] forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor hexFloatColor:@"000000"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor hexFloatColor:@"FFFFFF"] forState:UIControlStateSelected];
    btn.titleLabel.font = [UIFont systemFontOfSize:FontNum(16)];
    btn.tag = tag;
    [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];

    return btn;
}

@end
