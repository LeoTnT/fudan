//
//  FDMallModel.h
//  App3.0
//
//  Created by lichao on 2018/9/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FDMallSpecialModel;
@class FDMallGoodsModel;
@interface FDMallModel : NSObject

@property (nonatomic, strong) FDMallSpecialModel *tuijian;
@property (nonatomic, strong) FDMallGoodsModel *jingxuan;

@end

/********** 专题推荐 **********/
@interface FDMallSpecialModel : NSObject
/** 广告位 */
@property (nonatomic, strong) NSArray *advert;
/** 商品列表 */
@property (nonatomic, strong) NSArray *list;

@end

@interface FDMallSpecialTopModel : NSObject

/** ... */
@property (nonatomic, strong) NSString *backcolor;
/** ... */
@property (nonatomic, strong) NSString *category_id;
/** 图片 */
@property (nonatomic, strong) NSString *image;
/** 跳转类型 */
@property (nonatomic, strong) NSString *link_in;
/** ID */
@property (nonatomic, strong) NSString *link_objid;
/** 广告名称 */
@property (nonatomic, strong) NSString *name;
/** 备注 */
@property (nonatomic, strong) NSString *remark;
/** 广告url */
@property (nonatomic, strong) NSString *url;

@end


/********** 精选好物 **********/
@interface FDMallGoodsModel : NSObject
/** 列表 */
@property (nonatomic, strong) NSArray *list;

@end

@interface FDMallGoodsSubModel : NSObject
/** 图片 */
@property (nonatomic, copy) NSString *image;
/** 图片2 */
@property (nonatomic, copy) NSString *image_thumb;
/** 观看数 */
@property (nonatomic, copy) NSString *look_num;
/** 市场价 */
@property (nonatomic, copy) NSString *market_price;
/** 商品ID */
@property (nonatomic, copy) NSString *product_id;
/** 商品名称 */
@property (nonatomic, copy) NSString *product_name;
/** 卖价 */
@property (nonatomic, copy) NSString *sell_price;
/** 销量 */
@property (nonatomic, copy) NSString *sell_num;
/** 月销量 */
@property (nonatomic, copy) NSString *sell_num_month;
/** 销售类型 */
@property (nonatomic, copy) NSString *sell_type;
/** 店铺名称 */
@property (nonatomic, copy) NSString *store_name;
/** userID */
@property (nonatomic, copy) NSString *user_id;

@end
