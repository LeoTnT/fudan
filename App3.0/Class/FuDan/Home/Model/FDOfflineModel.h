//
//  FDOfflineModel.h
//  App3.0
//
//  Created by lichao on 2018/9/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDOfflineModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *approve_supply;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *domain;
@property (nonatomic, copy) NSString *eva;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *isFav;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *username;

@end
