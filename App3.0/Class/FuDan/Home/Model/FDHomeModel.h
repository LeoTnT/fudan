//
//  FDHomeModel.h
//  AFD3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FDHomeActivesModel;
@class FDHomeHotsModel;
@class FDHomeVideosModel;
@class FDHomeArticleModel;
//首页模型
@interface FDHomeModel : NSObject

@property (nonatomic, strong) FDHomeActivesModel *actives;
@property (nonatomic, strong) FDHomeHotsModel *hots;
@property (nonatomic, strong) FDHomeVideosModel *videos;
@property (nonatomic, strong) FDHomeArticleModel *article;

@end

/********** 热卖 **********/
@interface FDHomeHotsModel : NSObject

/** 广告位 */
@property (nonatomic, strong) NSArray *advert;
/** 商品列表 */
@property (nonatomic, strong) NSArray *list;

@end

@interface FDHomeTopModel : NSObject

/** ... */
@property (nonatomic, strong) NSString *backcolor;
/** ... */
@property (nonatomic, strong) NSString *category_id;
/** 图片 */
@property (nonatomic, strong) NSString *image;
/** 跳转类型 */
@property (nonatomic, strong) NSString *link_in;
/** ID */
@property (nonatomic, strong) NSString *link_objid;
/** 广告名称 */
@property (nonatomic, strong) NSString *name;
/** 备注 */
@property (nonatomic, strong) NSString *remark;
/** 广告url */
@property (nonatomic, strong) NSString *url;

@end

@interface FDHomeListModel : NSObject

/** 图片 */
@property (nonatomic, strong) NSString *image;
/** 图片2 */
@property (nonatomic, strong) NSString *image_thumb;
/** 观看数 */
@property (nonatomic, strong) NSString *look_num;
/** 价格 */
@property (nonatomic, strong) NSString *market_price;
/** 产品id */
@property (nonatomic, strong) NSString *product_id;
/** 产品名称 */
@property (nonatomic, strong) NSString *product_name;
/** ... */
@property (nonatomic, strong) NSString *promotion;
/** ... */
@property (nonatomic, strong) NSString *score;
/** 卖出数量 */
@property (nonatomic, strong) NSString *sell_num;
/** 卖出数量/月 */
@property (nonatomic, strong) NSString *sell_num_month;
/** 卖出价格 */
@property (nonatomic, strong) NSString *sell_price;
/** 类型 */
@property (nonatomic, strong) NSString *sell_type;
/** ... */
@property (nonatomic, strong) NSString *store_name;
/** ... */
@property (nonatomic, strong) NSString *user_id;

@end

/********** 活动 **********/
@interface FDHomeActivesModel : NSObject
/** 广告位 */
@property (nonatomic, strong) NSArray *advert;
/** 商品列表 */
@property (nonatomic, strong) NSArray *list;

@end

@interface FDHomeActivesListModel : NSObject
/** 图片 */
@property (nonatomic, strong) NSString *image;
/** 产品名称 */
@property (nonatomic, strong) NSString *product_name;
/** 产品id */
@property (nonatomic, strong) NSString *product_id;
/** 进度 */
@property (nonatomic, strong) NSString *progress;
/** 价格 */
@property (nonatomic, strong) NSString *sell_price;

@end

/********** 视频 **********/
@interface FDHomeVideosModel : NSObject
/** 列表 */
@property (nonatomic, strong) NSArray *list;

@end

@interface FDVideoListModel : NSObject
/** 高度 */
@property (nonatomic, copy) NSString *img_height;
/** 宽度 */
@property (nonatomic, copy) NSString *img_width;

/** 内容 */
@property (nonatomic, copy) NSString *content;
/**  */
@property (nonatomic, copy) NSString *videoID;
/** logo */
@property (nonatomic, copy) NSString *logo;
/** 观看次数 */
@property (nonatomic, copy) NSString *look_num;
/** 昵称 */
@property (nonatomic, copy) NSString *nickname;
/** uid */
@property (nonatomic, copy) NSString *uid;
/** 用户编号 */
@property (nonatomic, copy) NSString *username;
/** 视频图片 */
@property (nonatomic, copy) NSString *video_image;
/** 视频链接 */
@property (nonatomic, copy) NSString *video_url;
/** 发布时间 */
@property (nonatomic, copy) NSString *w_time;
/** 赞 */
@property (nonatomic, copy) NSString *zan_num;

@end


//首页底部
@interface FDHomeArticleModel : NSObject

/** 广告位 */
@property (nonatomic, strong) NSArray *advert;
/** 商品列表 */
@property (nonatomic, strong) NSArray *list;

@end

//首页底部listModel
@interface FDHomeArticleListModel : NSObject

/** 文章id */
@property (nonatomic, strong) NSString *article_id;
/** 作者 */
@property (nonatomic, strong) NSString *author;
/** 分类id */
@property (nonatomic, strong) NSString *category_id;
/** 分类名称 */
@property (nonatomic, strong) NSString *category_name;
/** 内容 */
@property (nonatomic, strong) NSString *content;
/** 图片 */
@property (nonatomic, strong) NSString *image;
/**  */
@property (nonatomic, strong) NSString *is_show;
/** 观看数量 */
@property (nonatomic, strong) NSString *look_num;
/**  */
@property (nonatomic, strong) NSString *sort;
/**  */
@property (nonatomic, strong) NSString *src;
/** 子标题 */
@property (nonatomic, strong) NSString *subtitle;
/** 标题 */
@property (nonatomic, strong) NSString *title;
/**  */
@property (nonatomic, strong) NSString *u_time;
/** 时间 */
@property (nonatomic, strong) NSString *w_time;

@end
