//
//  FDHomeModel.m
//  FD3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHomeModel.h"

@implementation FDHomeHotsModel

@end

@implementation FDHomeTopModel

@end

@implementation FDHomeListModel

@end




@implementation FDHomeActivesModel

@end

@implementation FDHomeActivesListModel

@end


@implementation FDHomeVideosModel

@end

@implementation FDVideoListModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"videoID" : @"id"
             };
}
@end




@implementation FDHomeArticleModel

@end

@implementation FDHomeArticleListModel

@end

@implementation FDHomeModel

@end
