//
//  FDUpdateCell.h
//  App3.0
//
//  Created by lichao on 2018/9/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"

@interface FDUpdateCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIView *progressV;


@end
