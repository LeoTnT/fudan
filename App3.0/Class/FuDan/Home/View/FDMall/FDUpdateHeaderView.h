//
//  FDUpdateHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/9/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

@interface FDUpdateHeaderView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageV;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) LoginDataParser *parser;

@end
