//
//  FDMallGoodsHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMallGoodsHeaderView.h"

@implementation FDMallGoodsHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *title = [[UILabel alloc] init];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:17];
    title.textColor = [UIColor hexFloatColor:@"7A45E5"];;
    title.text = Localized(@"全球精选好物");
    [self addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(11+10);
        make.centerX.mas_equalTo(self).offset(5);
        make.height.mas_equalTo(17);
    }];
    
    UILabel *subTitle = [[UILabel alloc] init];
    subTitle.textAlignment = NSTextAlignmentCenter;
    subTitle.font=[UIFont systemFontOfSize:12];
    subTitle.textColor = [UIColor hexFloatColor:@"999999"];;
    subTitle.text = Localized(@"为你精挑细选");
    [self addSubview:subTitle];
    
    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(title.mas_bottom).offset(7);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(12);
    }];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_allGoods_logo"]];
    [self addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_left).offset(-3);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
}

@end
