//
//  PPMallListCell.m
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMallSpecialCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"

@interface FDMallSpecialCell()

@end

@implementation FDMallSpecialCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    
    _topView = [[FDSpecialTopHeaderView alloc] init];
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(55+123);
    }];
    
    _item = [[FDSpecialNewItemView alloc] initWithFrame:CGRectMake(0, 55+123, mainWidth, 179)];
    [self addSubview:_item];
}

- (void)setTopArr:(NSArray *)topArr {
    if (topArr.count > 0) {
        FDMallSpecialTopModel *topModel = topArr.firstObject;
        [self.topView.topImageV sd_setImageWithURL:[NSURL URLWithString:topModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (void)setListArr:(NSArray *)listArr {
    _listArr = listArr;
    _item.itemDataSource = listArr;
}

- (TabMallCategoryImgView *)getImageViewWithTag:(NSInteger)tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    return view;
}

- (UIView *)creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    return view;
}

- (UIView *)creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    return lineView;
}

@end

@implementation FDSpecialTopHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
 
    self.backgroundColor = [UIColor whiteColor];

    UILabel *title = [[UILabel alloc] init];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:17];
    title.textColor = mainColor;
    title.text = Localized(@"专题推荐");
    [self addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(11);
        make.centerX.mas_equalTo(self).offset(5);
        make.height.mas_equalTo(17);
    }];
    
    UILabel *subTitle = [[UILabel alloc] init];
    subTitle.textAlignment = NSTextAlignmentCenter;
    subTitle.font=[UIFont systemFontOfSize:12];
    subTitle.textColor = [UIColor hexFloatColor:@"999999"];;
    subTitle.text = Localized(@"总有你想不到的低价");
    [self addSubview:subTitle];
    
    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(title.mas_bottom).offset(7);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(12);
    }];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_special_logo"]];
    [self addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_left).offset(-3);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    self.topImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no_pic"]];
    self.topImageV.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topImageClick)];
    [self.topImageV addGestureRecognizer:tap];
    
    [self addSubview:self.topImageV];
    [self.topImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(55);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(123);
    }];
}

- (void)topImageClick {
    if (self.adBlock) {
        self.adBlock();
    }
}

@end


@implementation FDSpecialNewItemView
- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(113, self.mj_h);
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    [self.collectionView registerClass:[FDSpecialNewItem class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    FDSpecialNewItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.listProductBlock) {
        FDMallGoodsSubModel *model = self.itemDataSource[indexPath.row];
        self.listProductBlock(model);
    }
}

@end

@interface FDSpecialNewItem ()

@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *price;

@end

@implementation FDSpecialNewItem

- (void)setContentView {
    [super setContentView];
    [self setUpUI];
}

- (void)setModel:(FDMallGoodsSubModel *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.image_thumb] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.price.text = [NSString stringWithFormat:@"¥%@",model.sell_price];
    self.name.text = [NSString stringWithFormat:@"%@%@", model.sell_num, Localized(@"人已买")];
}

- (void)setUpUI {
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(10);
        make.height.mas_equalTo(113);
        make.width.mas_equalTo(113);
    }];
    
    self.price = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:14] titleColor:mainColor];
    [self addSubview:self.price];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(12);
        make.centerX.mas_equalTo(self);
        make.width.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(11);
    }];
    
    self.name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:11] titleColor:[UIColor hexFloatColor:@"999999"]];
    self.name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.price.mas_bottom).offset(7);
        make.centerX.mas_equalTo(self);
        make.width.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(12);
    }];
}


@end

