//
//  FDMallAdHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCollectionReusableView.h"
#import "TabMallModel.h"

typedef void(^adClickBlock)(BOOL isLeft);
@interface FDMallAdHeaderView : FDBaseCollectionReusableView

@property (weak, nonatomic) IBOutlet UIImageView *leftImageV;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageV;

@property (nonatomic, strong) AdvertisementModel *advertisementModel;
/* 点击广告(type=YES 左边广告/ type=NO 右边广告) */
@property (nonatomic, copy) adClickBlock adClickBlock;

@end
