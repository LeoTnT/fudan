//
//  FDOfflineStoreCell.m
//  App3.0
//
//  Created by lichao on 2018/9/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDOfflineStoreCell.h"

@implementation FDOfflineStoreCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setModel:(FDOfflineModel *)model {
    if (model) {
        [self.logo sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.storeNameLabel.text = model.name;
        self.phoneLabel.text = @"";
        self.addressLabel.text = model.address;
        self.distanceLabel.text = @"";
    }
}


@end
