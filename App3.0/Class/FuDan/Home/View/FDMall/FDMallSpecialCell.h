//
//  PPMallListCell.h
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDBaseCollectionCell.h"
#import "Shop_BaseView.h"
#import "FDMallModel.h"

typedef void(^adBlock)(void);
@interface FDSpecialTopHeaderView : UIView

@property (nonatomic, strong) UIImageView *topImageV;
@property (nonatomic, copy) adBlock adBlock;

@end


typedef void(^listProductBlock)(FDMallGoodsSubModel *subModel);
@interface FDSpecialNewItemView :QuickCollectionView

@property (nonatomic, strong) NSArray *itemDataSource;
@property (nonatomic, copy) listProductBlock listProductBlock;

@end;


//专题推荐
@interface FDMallSpecialCell : FDBaseCollectionCell

@property (nonatomic, strong) FDSpecialTopHeaderView *topView;
@property (nonatomic, strong) FDSpecialNewItemView *item;

@property (nonatomic, strong) NSArray *topArr;
@property (nonatomic, strong) NSArray *listArr;

@end


@interface FDSpecialNewItem :XSBaseCollectionCell

@property (nonatomic, strong) FDMallGoodsSubModel *model;

@end
