//
//  FDMallAdHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMallAdHeaderView.h"

@implementation FDMallAdHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];

    self.leftImageV.userInteractionEnabled = YES;
    self.rightImageV.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftAdImageClick)];
    [self.leftImageV addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightAdImageClick)];
    [self.rightImageV addGestureRecognizer:tap2];
}

- (void)setAdvertisementModel:(AdvertisementModel *)advertisementModel {
    if (advertisementModel) {
        TabMallADItem *leftModel = (TabMallADItem *)advertisementModel.mobile_index_left_up.firstObject;
        [self.leftImageV sd_setImageWithURL:[NSURL URLWithString:leftModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        
        TabMallADItem *rightModel = (TabMallADItem *)advertisementModel.mobile_index_right_up.firstObject;
        [self.rightImageV sd_setImageWithURL:[NSURL URLWithString:rightModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (void)leftAdImageClick {
    if (self.adClickBlock) {
        self.adClickBlock(YES);
    }
}

- (void)rightAdImageClick {
    if (self.adClickBlock) {
        self.adClickBlock(NO);
    }
}

@end
