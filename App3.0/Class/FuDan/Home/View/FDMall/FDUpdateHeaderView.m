//
//  FDUpdateHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/9/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDUpdateHeaderView.h"

@implementation FDUpdateHeaderView

- (void)setParser:(LoginDataParser *)parser {
    
    [self.logo sd_setImageWithURL:[NSURL URLWithString:parser.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.levelImageV.image = [UIImage imageNamed:@"fd_level_default"];
    self.nameLabel.text = parser.nickname.length > 0 ? parser.nickname : parser.username;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.logo.layer.cornerRadius = 68/2;
//    self.logo.layer.borderWidth = 4;
//    self.logo.layer.borderColor = [[UIColor hexFloatColor:@"FFFFFF"] colorWithAlphaComponent:0.12].CGColor;
    
    self.logo.layer.masksToBounds = YES;
}

@end
