//
//  FDOfflineStoreCell.h
//  App3.0
//
//  Created by lichao on 2018/9/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDOfflineModel.h"

@interface FDOfflineStoreCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (nonatomic, copy) FDOfflineModel *model;

@end
