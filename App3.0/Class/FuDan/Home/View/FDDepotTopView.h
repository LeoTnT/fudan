//
//  FDDepotTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

@protocol FDDepotTopDelegate <NSObject>
@optional
- (void)goShopping;
- (void)goRecharge;
- (void)goTrip;

@end

@interface FDDepotTopView : FDBaseCustomView

@property (nonatomic, weak) id<FDDepotTopDelegate>delegate;

@end
