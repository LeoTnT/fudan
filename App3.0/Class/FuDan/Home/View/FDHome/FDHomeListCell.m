//
//  FDHomeListCell.m
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHomeListCell.h"

@implementation FDHomeListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(FDHomeArticleListModel *)model {
    if (model) {
        [self.logo sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.contentLabel.text = model.title;
        self.timeLabel.text = model.w_time;
        self.lookNumLabel.text = model.look_num;
    }
}

+ (CGFloat)height {
    return 119;
}

@end
