//
//  FDHomeListCell.h
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDHomeModel.h"

@interface FDHomeListCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lookNumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logo;


@property (nonatomic, strong) FDHomeArticleListModel *model;

@end
