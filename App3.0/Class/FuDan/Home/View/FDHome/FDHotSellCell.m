//
//  PPMallListCell.m
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHotSellCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"

@interface FDHotSellCell()

@end

@implementation FDHotSellCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    
    _topView = [FDTopHeaderView new];
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(44+129);
    }];
    
    _item = [[FDNewItemView alloc] initWithFrame:CGRectMake(0, 44+129, mainWidth, 171)];
    [self addSubview:_item];
}

- (void)setHotTopArr:(NSArray *)hotTopArr {
    if (hotTopArr.count > 0) {
        FDHomeTopModel *topModel = hotTopArr.firstObject;
        [_topView.topImageV sd_setImageWithURL:[NSURL URLWithString:topModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (void)setHotListArr:(NSArray *)hotListArr {
    if (hotListArr.count > 0) {
        _item.itemDataSource = hotListArr;
    }
}

- (TabMallCategoryImgView *)getImageViewWithTag:(NSInteger)tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    return view;
}

- (UIView *)creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    return view;
}

- (UIView *)creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    return lineView;
}

@end

@implementation FDTopHeaderView 

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
 
    self.backgroundColor = [UIColor whiteColor];
    UIView *leftLine = [UIView new];
    [self addSubview:leftLine];
    
    UIView *rightLine = [UIView new];
    [self addSubview:rightLine];
    
    UIColor *color = [UIColor hexFloatColor:@"999999"];
    leftLine.backgroundColor = color;
    rightLine.backgroundColor = color;
    
    UILabel *title = [UILabel new];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor hexFloatColor:@"111111"];;
    title.text = Localized(@"热卖推送");
    [self addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(15);
    }];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_left).offset(-8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.left.mas_equalTo(title.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
    
    self.topImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_hot_sell_top"]];
    
    self.topImageV.userInteractionEnabled = YES;
    kFDWeakSelf;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (weakSelf.adBlock) {
            weakSelf.adBlock();
        }
    }];
    [self.topImageV addGestureRecognizer:tap];
    
    [self addSubview:self.topImageV];
    [self.topImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(44);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(129);
    }];
}

@end


@implementation FDNewItemView
- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(83, self.mj_h);
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    [self.collectionView registerClass:[FDNewItem class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    FDNewItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.listProductBlock) {
        FDHomeListModel *model = self.itemDataSource[indexPath.row];
        self.listProductBlock(model);
    }
}

@end

@interface FDNewItem ()

@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *price;

@end

@implementation FDNewItem

- (void)setContentView {
    [super setContentView];
    [self setUpUI];
}

- (void)setModel:(FDHomeListModel *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.name.text = model.product_name;
    self.price.text = [NSString stringWithFormat:@"¥%@",model.sell_price];
}

- (void)setUpUI {
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(15);
        make.height.mas_equalTo(82);
        make.width.mas_equalTo(82);
    }];
    
    self.name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:12] titleColor:[UIColor hexFloatColor:@"333333"]];
    self.name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(12.5);
        make.width.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(11);
    }];
    
    
    self.price = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:12] titleColor:[UIColor hexFloatColor:@"FF6600"]];
    [self addSubview:self.price];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.name.mas_bottom).offset(7.5);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(11);
    }];
}


@end

