//
//  PPMallListCell.m
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDTjStroreCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"

@interface FDTjStroreCell()

@end

@implementation FDTjStroreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatContentView];

    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    
    _topView = [FDPTjTopHeaderView new];
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(42);
    }];
    
    _item = [[FDTjCollectionView alloc] initWithFrame:CGRectMake(0, 42, mainWidth, 161)];
    kFDWeakSelf;
    _item.listBlock = ^(homeModel *model) {
        if (weakSelf.storeBlock) {
            weakSelf.storeBlock(model);
        }
    };
    
    [self addSubview:_item];
}

- (void)setStoresArr:(NSArray *)storesArr {
    if (storesArr.count > 0) {
        _item.itemDataSource = storesArr;
    }
}

- (TabMallCategoryImgView *)getImageViewWithTag:(NSInteger)tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    return view;
}

- (UIView *)creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    return view;
}

- (UIView *)creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    return lineView;
}

@end

@implementation FDPTjTopHeaderView {
    UIImageView *imageView;
    UILabel *title;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
 
    self.backgroundColor = [UIColor whiteColor];
    
    UILabel *title = [UILabel new];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor hexFloatColor:@"111111"];;
    title.text = Localized(@"推荐店铺");
    [self addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(13);
        make.height.mas_equalTo(16);
    }];
}

@end

@implementation FDTjCollectionView
- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(120, self.mj_h);
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    [self.collectionView registerClass:[FDTjItemCell class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    FDTjItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];

    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listBlock) {
        homeModel *model = self.itemDataSource[indexPath.row];
        self.listBlock(model);
    }
}

@end

@interface FDTjItemCell ()

@property (strong, nonatomic) UILabel *name;

@end

@implementation FDTjItemCell

- (void)setContentView {
    [super setContentView];
    [self setUpUI];
}

- (void)setModel:(homeModel *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.name.text = model.name;
}

- (void)setUpUI {
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self);
        make.height.mas_equalTo(120);
        make.width.mas_equalTo(120);
    }];

    self.name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:15] titleColor:[UIColor hexFloatColor:@"666666"]];
    self.name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(13);
        make.width.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(15);
    }];
}


@end

