//
//  TabMallADCell.h
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FDBaseCollectionReusableView.h"
#import "TabMallModel.h"
@class AdvertisementModel;
@protocol FDHomeAdHeaderDelegate <NSObject>

- (void)didSelectedItem:(UITapGestureRecognizer *)tap;

@end

@interface FDHomeAdHeaderView : FDBaseCollectionReusableView

@property (nonatomic ,weak)id <FDHomeAdHeaderDelegate> sectionDelegate;

@property(nonatomic,strong)UIImageView *leftImage;
@property(nonatomic,strong)UIImageView *rightTopImage;
@property(nonatomic,strong)UIImageView *rightBottomLeftImage;
@property(nonatomic,strong)UIImageView *rightBottomRightImage;

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic ,strong)AdvertisementModel *itemDataSource;
@end
