//
//  TabMallTopNoticeCell.h
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeAdView.h"

@interface FDNoticeFooterView : UICollectionReusableView

@property (nonatomic, strong) NoticeAdView *noticeV;

@end
