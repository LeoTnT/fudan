//
//  PPMallListCell.m
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHotPlayCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"

@interface FDHotPlayCell()

@end

@implementation FDHotPlayCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    
    _topView = [FDPlayTopHeaderView new];
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(44);
    }];
    
    _item = [[FDPlayNewItemView alloc] initWithFrame:CGRectMake(0, 44, mainWidth, 200)];
    kFDWeakSelf;
    _item.listBlock = ^(FDVideoListModel *videoModel) {
        if (weakSelf.videoBlock) {
            weakSelf.videoBlock(videoModel);
        }
    };
    
    [self addSubview:_item];
}

- (void)setPlayListArr:(NSArray *)playListArr {
    if (playListArr.count > 0) {
        _item.itemDataSource = playListArr;
    }
}

- (TabMallCategoryImgView *)getImageViewWithTag:(NSInteger)tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    return view;
}

- (UIView *)creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    return view;
}

- (UIView *)creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    return lineView;
}

@end

@implementation FDPlayTopHeaderView {
    UIImageView *imageView;
    UILabel *title;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
 
    self.backgroundColor = [UIColor whiteColor];
    UIView *leftLine = [UIView new];
    [self addSubview:leftLine];
    
    UIView *rightLine = [UIView new];
    [self addSubview:rightLine];
    
    UIColor *color = [UIColor hexFloatColor:@"999999"];
    leftLine.backgroundColor = color;
    rightLine.backgroundColor = color;
    
    UILabel *title = [UILabel new];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor hexFloatColor:@"111111"];;
    title.text = Localized(@"热播推荐");
    [self addSubview:title];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-50, 2, 40, 40)];
    [btn addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn setImage:[UIImage imageNamed:@"more_arrow"] forState:UIControlStateNormal];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.bounds.size.width-1, 0, btn.imageView.bounds.size.width+1)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.bounds.size.width+1, 0, -btn.titleLabel.bounds.size.width-1)];
    [self addSubview:btn];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(15);
    }];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_left).offset(-8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.left.mas_equalTo(title.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
}

- (void)moreAction {
    if (self.moreBlock) {
        self.moreBlock();
    }
}

@end

@implementation FDPlayNewItemView
- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(134, self.mj_h);
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    [self.collectionView registerClass:[FDPlayNewItem class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    FDPlayNewItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];
    kFDWeakSelf;
    cell.playBlock = ^{
        if (weakSelf.listBlock) {
            FDVideoListModel *model = weakSelf.itemDataSource[indexPath.row];
            weakSelf.listBlock(model);
        }
    };
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listBlock) {
        FDVideoListModel *model = self.itemDataSource[indexPath.row];
        self.listBlock(model);
    }
}

@end

@interface FDPlayNewItem ()

@property (strong, nonatomic) UILabel *name;

@end

@implementation FDPlayNewItem

- (void)setContentView {
    [super setContentView];
    [self setUpUI];
}

- (void)setModel:(FDVideoListModel *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.video_image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.name.text = model.content;
}

- (void)setUpUI {
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(15);
        make.height.mas_equalTo(134);
        make.width.mas_equalTo(134);
    }];
    
    UIButton *playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBtn setBackgroundImage:[UIImage imageNamed:@"fd_home_play"] forState:UIControlStateNormal];
    [playBtn addTarget:self action:@selector(playButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.headerImageView addSubview:playBtn];
    [playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self.headerImageView);
        make.size.mas_equalTo(CGSizeMake(36, 36));
    }];
    
    self.name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:12] titleColor:[UIColor hexFloatColor:@"333333"]];
    self.name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(17);
        make.width.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(13);
    }];
}

- (void)playButtonClick {
    if (self.playBlock) {
        self.playBlock();
    }
}

@end

