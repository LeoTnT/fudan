//
//  TabMallTopNoticeCell.m
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FDNoticeFooterView.h"

@interface FDNoticeFooterView ()

@end

@implementation FDNoticeFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    // 公告栏目
    self.noticeV = [[NoticeAdView alloc] init];
    self.noticeV.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.noticeV];
    self.noticeV.userInteractionEnabled = YES;
    
    [self.noticeV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(50);
    }];
}

@end
