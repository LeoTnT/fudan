//
//  FDHomeOtherCell.m
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHomeOtherCell.h"
#import "FDHomeListCell.h"

@interface FDHomeOtherCell ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArr;

@end

@implementation FDHomeOtherCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setTopArr:(NSArray *)topArr {
    if (topArr.count > 0) {
        FDHomeTopModel *topModel = topArr.firstObject;
        [self.headerV.logo sd_setImageWithURL:[NSURL URLWithString:topModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.headerV.contentLabel.text = topModel.name;
    }
}

- (void)setListArr:(NSArray *)listArr {
    if (listArr.count > 0) {
        self.dataArr = [NSMutableArray arrayWithArray:listArr];
        
        [FDHomeListCell registerNibCellWithTableView:self.tableView];
        self.tableView.tableHeaderView = self.headerV;
    }
}

- (void)setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    self.headerV = [FDHomeListHeaderView initView];
    self.headerV.frame = CGRectMake(0, 0, mainWidth, 162);
}

#pragma mark --<UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDHomeListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDHomeListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDHomeListCell identifier]];
    if (cell == nil) {
        cell = [[FDHomeListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    cell.model = self.dataArr[indexPath.section];
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.articleBlock) {
        self.articleBlock(self.dataArr[indexPath.section]);
    }
}

#pragma mark -- 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = CGRectMake(0, 0, mainWidth, 119*self.dataArr.count+162);
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self addSubview:_tableView];
    }
    return _tableView;
}

@end
