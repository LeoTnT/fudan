//
//  PPMallListCell.h
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDBaseCollectionCell.h"
#import "Shop_BaseView.h"
#import "FDHomeModel.h"

typedef void(^adBlock)(void);
@interface FDTopHeaderView : UIView

@property (nonatomic, strong) UIImageView *topImageV;

@property (nonatomic, copy) adBlock adBlock;

@end

typedef void(^listProductBlock)(FDHomeListModel *listModel);
@interface FDNewItemView :QuickCollectionView

@property (nonatomic, strong) NSArray *itemDataSource;
@property (nonatomic, copy) listProductBlock listProductBlock;

@end;


//热卖推荐
@interface FDHotSellCell : FDBaseCollectionCell

@property (nonatomic, strong) UIButton *categoryMoreButton;
@property (nonatomic, strong) FDTopHeaderView *topView;
@property (nonatomic, strong) FDNewItemView *item;
@property (nonatomic, strong) NSArray *hotTopArr;
@property (nonatomic, strong) NSArray *hotListArr;

@end



@interface FDNewItem :XSBaseCollectionCell

@property (nonatomic, strong) FDHomeListModel *model;

@end
