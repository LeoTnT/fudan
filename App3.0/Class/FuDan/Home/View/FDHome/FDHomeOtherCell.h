//
//  FDHomeOtherCell.h
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDBaseCollectionCell.h"
#import "FDHomeListHeaderView.h"
#import "FDHomeModel.h"

typedef void(^articleBlock)(FDHomeArticleListModel *model);
@interface FDHomeOtherCell : FDBaseCollectionCell

@property (nonatomic, strong) FDHomeListHeaderView *headerV;

@property (nonatomic, strong) NSArray *topArr;
@property (nonatomic, strong) NSArray *listArr;

@property (nonatomic, strong) articleBlock articleBlock;

@end
