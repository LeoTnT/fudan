//
//  FDHomeListHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHomeListHeaderView.h"

@implementation FDHomeListHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.logo.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topImageClick)];
    
    [self.logo addGestureRecognizer:tap];
}

- (void)topImageClick {
    if (self.logoClickBlock) {
        self.logoClickBlock();
    }
}

@end
