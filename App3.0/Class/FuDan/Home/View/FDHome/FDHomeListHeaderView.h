//
//  FDHomeListHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

typedef void(^logoClickBlock)(void);
@interface FDHomeListHeaderView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (nonatomic, copy) logoClickBlock logoClickBlock;

@end
