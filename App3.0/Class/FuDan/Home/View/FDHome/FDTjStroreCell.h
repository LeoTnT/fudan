//
//  PPMallListCell.h
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDBaseTableViewCell.h"
#import "Shop_BaseView.h"
#import "homeModel.h"

typedef void(^moreBlock)(void);
@interface FDPTjTopHeaderView : UIView

@property (nonatomic, copy) moreBlock moreBlock;

@end

typedef void(^listBlock)(homeModel *model);
@interface FDTjCollectionView :QuickCollectionView

@property (nonatomic, copy) listBlock listBlock;
@property (nonatomic, strong) NSArray *itemDataSource;

@end;


//推荐店铺
typedef void(^storeBlock)(homeModel *model);
@interface FDTjStroreCell : FDBaseTableViewCell

@property (nonatomic, strong) UIButton *categoryMoreButton;
@property (nonatomic, strong) FDPTjTopHeaderView *topView;
@property (nonatomic, strong) FDTjCollectionView *item;

@property (nonatomic, strong) NSArray *storesArr;

@property (nonatomic, copy) storeBlock storeBlock;

@end


typedef void(^playBlock)(void);
@interface FDTjItemCell :XSBaseCollectionCell

@property (nonatomic, strong) homeModel *model;
@property (nonatomic, copy) playBlock playBlock;


@end
