//
//  PPMallListCell.m
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDActivityCell.h"
#import "TabMallCategoryGoodsView.h"
#import "TabMallCategoryImgView.h"

@interface FDActivityCell()

@end

@implementation FDActivityCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
    self.layer.masksToBounds = YES;
    
    _topView = [[FDActivityTopHeaderView alloc] init];
    _topView.topImageV.userInteractionEnabled = YES;
    kFDWeakSelf;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (weakSelf.activityAdBlock) {
            weakSelf.activityAdBlock();
        }
    }];
    [_topView.topImageV addGestureRecognizer:tap];
    
    [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(44+129);
    }];
    
    _item = [[FDActivityNewItemView alloc] initWithFrame:CGRectMake(0, 44+129, mainWidth, 226)];
    _item.activityBlock = ^(FDHomeActivesListModel *model) {
        if (weakSelf.activityCellBlock) {
            weakSelf.activityCellBlock(model);
        }
    };
    
    [self addSubview:_item];
}

- (void)setActTopArr:(NSArray *)actTopArr {
    if (actTopArr.count > 0) {
        FDHomeTopModel *topModel = actTopArr.firstObject;
        [_topView.topImageV sd_setImageWithURL:[NSURL URLWithString:topModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (void)setActListArr:(NSArray *)actListArr {
    if (actListArr.count > 0) {
        _item.itemDataSource = actListArr;
    }
}

- (TabMallCategoryImgView *)getImageViewWithTag:(NSInteger)tag {
    
    TabMallCategoryImgView *view = [self viewWithTag:tag];
    return view;
}

- (UIView *)creatInforView:(CGRect )frame superView:(UIView *)superView {
    
    UIView *view =[[UIView alloc] initWithFrame:frame];
    [superView addSubview:view];
    return view;
}

- (UIView *)creatLineView:(CGRect)frame {
    UIView *lineView=[[UIView alloc] initWithFrame:frame];
    
    lineView.backgroundColor=mainGrayColor;
    lineView.alpha=0.5;
    [self addSubview:lineView];
    return lineView;
}

@end

@implementation FDActivityTopHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatContentView];
    }
    return self;
}

- (void)creatContentView {
 
    self.backgroundColor = [UIColor whiteColor];
    UIView *leftLine = [[UIView alloc] init];
    [self addSubview:leftLine];
    
    UIView *rightLine = [[UIView alloc] init];
    [self addSubview:rightLine];
    
    UIColor *color = [UIColor hexFloatColor:@"999999"];
    leftLine.backgroundColor = color;
    rightLine.backgroundColor = color;
    
    UILabel *title = [[UILabel alloc] init];
    title.textAlignment = NSTextAlignmentCenter;
    title.font=[UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor hexFloatColor:@"111111"];;
    title.text = Localized(@"参与活动");
    [self addSubview:title];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-50, 2, 40, 40)];
    [btn addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn setImage:[UIImage imageNamed:@"more_arrow"] forState:UIControlStateNormal];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.bounds.size.width-1, 0, btn.imageView.bounds.size.width+1)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.bounds.size.width+1, 0, -btn.titleLabel.bounds.size.width-1)];
    [self addSubview:btn];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(15);
    }];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.right.mas_equalTo(title.mas_left).offset(-8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.left.mas_equalTo(title.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(30, 1));
    }];
    
    self.topImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no_pic"]];
    [self addSubview:self.topImageV];
    [self.topImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(44);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(129);
    }];
}

- (void)moreAction {
    if (self.moreBlock) {
        self.moreBlock();
    }
}

@end


@implementation FDActivityNewItemView
- (void)setContentView {
    [super setContentView];
    
    self.cellSize = CGSizeMake(134, self.mj_h);
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 12, 0, 12);
    [self.collectionView registerClass:[FDActivityNewItem class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setItemDataSource:(NSArray *)itemDataSource {
    if (!itemDataSource) {
        return;
    }
    _itemDataSource = itemDataSource;
    [self.collectionView reloadData];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    FDActivityNewItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.itemDataSource[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.itemDataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.activityBlock) {
        FDHomeActivesListModel *model = self.itemDataSource[indexPath.row];
        self.activityBlock(model);
    }
}

@end

@interface FDActivityNewItem ()

@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UIView *progressV;
@property (strong, nonatomic) UILabel *price;

@end

@implementation FDActivityNewItem

- (void)setContentView {
    [super setContentView];
    [self setUpUI];
}

- (void)setModel:(FDHomeActivesListModel *)model {
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.name.text = model.product_name;
    self.price.text = [NSString stringWithFormat:@"¥%@",model.sell_price];
  
    if ([model.progress integerValue] < 100) {//根据认筹进度  显示进度条
        [self.progressV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo([model.progress integerValue]/134);
        }];
    }else {//>=100
        [self.progressV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(134);
        }];
    }
}

- (void)setUpUI {
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(10);
        make.height.mas_equalTo(134);
        make.width.mas_equalTo(134);
    }];
    
    self.name = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:13] titleColor:[UIColor hexFloatColor:@"333333"]];
    self.name.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.name];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.headerImageView);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).offset(12.5);
        make.height.mas_equalTo(11);
    }];
    
    //进度条灰色背景条
    UIView *progressBgV = [[UIView alloc] init];
    progressBgV.backgroundColor = [UIColor hexFloatColor:@"EEEEEE"];
    progressBgV.layer.cornerRadius = 3;
    progressBgV.layer.masksToBounds = YES;
    [self addSubview:progressBgV];
    [progressBgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.headerImageView);
        make.top.mas_equalTo(self.name.mas_bottom).offset(8);
        make.height.mas_equalTo(5);
    }];
    
    self.progressV = [[UIView alloc] init];
    self.progressV.backgroundColor = [UIColor hexFloatColor:@"FF8600"];
    self.progressV.layer.masksToBounds = YES;
    [progressBgV addSubview:self.progressV];
    [self.progressV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(progressBgV);
        make.width.mas_equalTo(10);
    }];
    
    self.price = [BaseUITool labelWithTitle:@" " textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"FF6600"]];
    [self addSubview:self.price];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(progressBgV.mas_bottom).offset(8);
        make.left.right.mas_equalTo(self.headerImageView);
        make.height.mas_equalTo(11);
    }];
}


@end

