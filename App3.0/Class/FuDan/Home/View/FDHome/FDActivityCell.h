//
//  PPMallListCell.h
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDBaseCollectionCell.h"
#import "Shop_BaseView.h"
#import "FDHomeModel.h"

typedef void(^moreBlock)(void);
@interface FDActivityTopHeaderView : UIView

@property (nonatomic, strong) UIImageView *topImageV;
@property (nonatomic, copy) moreBlock moreBlock;

@end

typedef void(^activityBlock)(FDHomeActivesListModel *Model);
@interface FDActivityNewItemView :QuickCollectionView

@property (nonatomic ,strong)NSArray *itemDataSource;
@property (nonatomic, copy) activityBlock activityBlock;

@end;


//参与活动
typedef void(^activityAdBlock)(void);
typedef void(^activityCellBlock)(FDHomeActivesListModel *model);
@interface FDActivityCell : FDBaseCollectionCell

@property (nonatomic, strong) UIButton *categoryMoreButton;
@property (nonatomic, strong) FDActivityTopHeaderView *topView;
@property (nonatomic, strong) FDActivityNewItemView *item;

@property (nonatomic, strong) NSArray *actTopArr;
@property (nonatomic, strong) NSArray *actListArr;

@property (nonatomic, copy) activityCellBlock activityCellBlock;
@property (nonatomic, copy) activityAdBlock activityAdBlock;

@end



@interface FDActivityNewItem :XSBaseCollectionCell

@property (nonatomic, strong) FDHomeActivesListModel *model;

@end
