//
//  PPMallListCell.h
//  App3.0
//
//  Created by lichao on 2018/8/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDBaseCollectionCell.h"
#import "Shop_BaseView.h"
#import "FDHomeModel.h"

typedef void(^moreBlock)(void);
@interface FDPlayTopHeaderView : UIView

@property (nonatomic, copy) moreBlock moreBlock;

@end

typedef void(^listBlock)(FDVideoListModel *videoModel);
@interface FDPlayNewItemView :QuickCollectionView

@property (nonatomic, copy) listBlock listBlock;
@property (nonatomic, strong) NSArray *itemDataSource;

@end;


//热播推荐
typedef void(^videoBlock)(FDVideoListModel *videoModel);
@interface FDHotPlayCell : FDBaseCollectionCell

@property (nonatomic, strong) UIButton *categoryMoreButton;
@property (nonatomic, strong) FDPlayTopHeaderView *topView;
@property (nonatomic, strong) FDPlayNewItemView *item;

@property (nonatomic, strong) NSArray *playListArr;

@property (nonatomic, copy) videoBlock videoBlock;

@end


typedef void(^playBlock)(void);
@interface FDPlayNewItem :XSBaseCollectionCell

@property (nonatomic, strong) FDVideoListModel *model;
@property (nonatomic, copy) playBlock playBlock;


@end
