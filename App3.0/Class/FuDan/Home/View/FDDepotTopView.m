//
//  FDDepotTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDDepotTopView.h"

@implementation FDDepotTopView


- (IBAction)mallBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goShopping)]) {
        [self.delegate goShopping];
    }
}

- (IBAction)rechargeBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goRecharge)]) {
        [self.delegate goRecharge];
    }
}

- (IBAction)tripBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goTrip)]) {
        [self.delegate goTrip];
    }
}

@end
