//
//  FDFindPswController.m
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFindPswController.h"
#import "FDRegisterTopView.h"
#import "FDRegisterCell.h"
#import "RSAEncryptor.h"
#import "FDResetPswController.h"

@interface FDFindPswController ()<UITableViewDelegate, UITableViewDataSource>
{
    BOOL _isTimer;// 是否在倒计时
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *menuArr;

//必须项
@property (nonatomic, weak) UITextField *phoneTextF;
@property (nonatomic, weak) UITextField *smsTextF;
@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation FDFindPswController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isTimer = NO;
    
    self.menuArr = @[@{@"image":@"fd_resgister_phone", @"name":@"请输入注册手机号码"},
                     @{@"image":@"fd_resgister_sms", @"name":@"输入短信验证码"},
                     @{@"image":@"", @"name":@"立即注册"},
                     ];
    
    [self showBackgroudViewWithImageName:@"fd_login_bg"];
    [self setUpTableView];
    [self setUpCustomNav];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:@"找回密码" textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDRegisterCell registerClassCellWithTableView:self.tableView];
    
    FDRegisterTopView *topV = [FDRegisterTopView initView];
    topV.logo.image = [UIImage imageNamed:@"fd_forget_top"];
    topV.frame = CGRectMake(0, 0, mainWidth, 150);
    self.tableView.tableHeaderView = topV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDRegisterCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section > 0) {
        return 16;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDRegisterCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDRegisterCell identifier]];
    if (cell == nil) {
        cell = [[FDRegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDRegisterCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = self.menuArr[indexPath.section];
    cell.logo.image = [UIImage imageNamed:dict[@"image"]];
    cell.inputTextF.placeholder = dict[@"name"];
    
    if (indexPath.section == 0) {//手机号
        self.phoneTextF = cell.inputTextF;
    }else if (indexPath.section == 1) {//短信验证码
        self.smsTextF = cell.inputTextF;
        
        UIButton *smsBtn = [self setButtonWithTitle:@"获取验证码" frame:CGRectMake((mainWidth-39*2)-97, (44-39)/2, 94, 39)];
        [smsBtn addTarget:self action:@selector(getSmsCode:) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:smsBtn];
        
    }else if (indexPath.section == self.menuArr.count-1) {//下一步
        
        cell.logo.hidden = YES;
        cell.inputTextF.hidden = YES;
        
        UIButton *nextBtn = [self setButtonWithTitle:@"下一步" frame:CGRectMake(0, 0, mainWidth-39*2, 44)];
        [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:nextBtn];
    }
    
    return cell;
}

#pragma mark ===== 点击事件
//点击下一步按钮
- (void)nextBtnClick {
    if(self.phoneTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"手机号不能为空"];
        return;
    }else {
        if (self.phoneTextF.text.length != 11) {
            [XSTool showToastWithView:self.view Text:@"手机号格式不正确"];
            return;
        }
    }
    if(self.smsTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入短信验证码"];
        return;
    }
    
    kFDWeakSelf;
    [HTTPManager checkSmsVerifyValidWithMobile:self.phoneTextF.text smsVerify:self.smsTextF.text success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 验证通过进入下一步
            FDResetPswController *resetVC = [[FDResetPswController alloc] init];
            resetVC.phone = weakSelf.phoneTextF.text;
            resetVC.smsCode = weakSelf.smsTextF.text;
            [weakSelf.navigationController pushViewController:resetVC animated:YES];
        } else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:weakSelf.view Text:error.description];
    }];
}

//获取验证码
- (void)getSmsCode:(UIButton *)sender {
    if(self.phoneTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"手机号不能为空"];
        return;
    }else {
        if (self.phoneTextF.text.length != 11) {
            [XSTool showToastWithView:self.view Text:@"手机号格式不正确"];
            return;
        }
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    [HTTPManager getSmsVerifyWithDic:@{@"mobile":self.phoneTextF.text} success:^(NSDictionary *dic, resultObject *state) {
        // 验证码发送成功
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"发送成功"];
            _isTimer = YES; // 设置倒计时状态为YES
            sender.enabled = NO; // 设置按钮为不可点击
            
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
            [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                //设置按钮的样式
                dispatch_source_cancel(_timer);
                [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                sender.enabled = YES; // 设置按钮可点击
                
                _isTimer = NO; // 倒计时状态为NO
            } otherAction:^(int time) {
                [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
            }];
            
        } else {
            if (state.info.length>0) {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            sender.enabled = YES; // 设置按钮为可点击
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
        sender.enabled = YES; // 设置按钮为可点击
    }];
}

#pragma mark ===== 功能
- (UIButton *)setButtonWithTitle:(NSString *)title frame:(CGRect)frame {
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = frame;
    [Btn setTitle:title forState:UIControlStateNormal];
    [Btn setTitleColor:[UIColor hexFloatColor:@"111111"] forState:UIControlStateNormal];
    Btn.titleLabel.font = [UIFont systemFontOfSize:14];
    Btn.backgroundColor = [UIColor hexFloatColor:@"FFE816"];
    Btn.layer.cornerRadius = frame.size.height/2;
    Btn.layer.masksToBounds = YES;
    return Btn;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
