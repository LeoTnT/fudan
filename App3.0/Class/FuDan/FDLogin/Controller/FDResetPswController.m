//
//  FDResetPswController.m
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDResetPswController.h"
#import "FDRegisterTopView.h"
#import "FDRegisterCell.h"
#import "RSAEncryptor.h"

@interface FDResetPswController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *menuArr;

//必须项
@property (nonatomic, weak) UITextField *pswTextF1;
@property (nonatomic, weak) UITextField *pswTextF2;

@end

@implementation FDResetPswController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuArr = @[@{@"image":@"fd_resgister_psw", @"name":@"请设置6位登录密码"},
                     @{@"image":@"fd_resgister_psw", @"name":@"请再次确认密码"},
                     @{@"image":@"", @"name":@"提交"},
                     ];
    
    [self showBackgroudViewWithImageName:@"fd_login_bg"];
    [self setUpTableView];
    [self setUpCustomNav];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:@"重置密码" textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDRegisterCell registerClassCellWithTableView:self.tableView];
    
    FDRegisterTopView *topV = [FDRegisterTopView initView];
    topV.logo.image = [UIImage imageNamed:@"fd_forget_top"];
    topV.frame = CGRectMake(0, 0, mainWidth, 150);
    self.tableView.tableHeaderView = topV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDRegisterCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section > 0) {
        return 16;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDRegisterCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDRegisterCell identifier]];
    if (cell == nil) {
        cell = [[FDRegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDRegisterCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = self.menuArr[indexPath.section];
    cell.logo.image = [UIImage imageNamed:dict[@"image"]];
    cell.inputTextF.placeholder = dict[@"name"];
    
    if (indexPath.section == 0) {//密码1
        self.pswTextF1 = cell.inputTextF;
        self.pswTextF1.secureTextEntry = YES;
    }else if (indexPath.section == 1) {//密码2
        self.pswTextF2 = cell.inputTextF;
        self.pswTextF2.secureTextEntry = YES;
    }else if (indexPath.section == self.menuArr.count-1) {//下一步
        
        cell.logo.hidden = YES;
        cell.inputTextF.hidden = YES;
        
        UIButton *nextBtn = [self setButtonWithTitle:@"提交" frame:CGRectMake(0, 0, mainWidth-39*2, 44)];
        [nextBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:nextBtn];
    }
    
    return cell;
}

#pragma mark ===== 点击事件
//点击按钮
- (void)submitBtnClick {
    if (self.pswTextF1.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入新密码"];
        return;
    }
    
    if (self.pswTextF2.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请再次输入新密码"];
        return;
    }else {
        if (![self.pswTextF1.text isEqualToString:self.pswTextF2.text]) {
            [XSTool showToastWithView:self.view Text:@"输入两次密码不一致"];
            return;
        }
    }
    
    [XSTool showProgressHUDWithView:self.view];
    NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:self.pswTextF1.text]];
    
    [HTTPManager resetLoginPwdWithMobile:self.phone pwdType:[NSString stringWithFormat:@"%d",PasswordLogin] smsVerify:self.smsCode  andNewPwd:encryptOldPassWord success:^(NSDictionary *dic, resultObject *state){
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            // 修改密码成功
            [XSTool showToastWithView:self.view Text:Localized(@"密码修改成功，请重新登录")];
            [self performSelector:@selector(popToLogin) withObject:nil afterDelay:2.0f];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"密码修改失败"];
    }];
}

- (void)popToLogin {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark ===== 功能
- (UIButton *)setButtonWithTitle:(NSString *)title frame:(CGRect)frame {
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = frame;
    [Btn setTitle:title forState:UIControlStateNormal];
    [Btn setTitleColor:[UIColor hexFloatColor:@"111111"] forState:UIControlStateNormal];
    Btn.titleLabel.font = [UIFont systemFontOfSize:14];
    Btn.backgroundColor = [UIColor hexFloatColor:@"FFE816"];
    Btn.layer.cornerRadius = frame.size.height/2;
    Btn.layer.masksToBounds = YES;
    return Btn;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


@end
