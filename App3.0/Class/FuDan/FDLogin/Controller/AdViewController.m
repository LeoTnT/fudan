//
//  AdViewController.m
//  App3.0
//
//  Created by lichao on 2018/6/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AdViewController.h"

@interface AdViewController ()

//计时器
@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation AdViewController

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.timer) dispatch_source_cancel(self.timer);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = BG_COLOR;
    UIImageView *bgImageV = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    if (mainHeight == 812) {
        bgImageV.image = [UIImage imageNamed:@"background_image_X"];
    }else {
        bgImageV.image = [UIImage imageNamed:@"background_image"];
    }
    
    [self.view addSubview:bgImageV];
    
    [self setUpUI];
}

#pragma mark ==================gif广告UI==================
- (void)setUpUI {
    
    /********* 动图数组 *********/
    UIImageView *imageV = [[UIImageView alloc] init];
    [self.view addSubview:imageV];
    
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view.mas_width);
    }];
    
    //gif
    imageV.image = [UIImage animatedImageWithImages:[self getImagesWithGif:@"fd_ad"] duration:2.5];
    [self startCountdown];
}

#pragma mark -- 事件处理
- (void)startCountdown {
    
    int totalTime = 3;
    
    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:self.timer walltime:totalTime stop:^{
        
        dispatch_source_cancel(self.timer);
        //倒计时结束切换根控制器
        if (self.gifFinishedBlock) {
            self.gifFinishedBlock();
        }
        
    } otherAction:^(int time) {
        
    }];
}

#pragma mark == 根据gif获取图片数组
- (NSArray *)getImagesWithGif:(NSString *)gifName {
    
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:gifName withExtension:@"gif"];
    
    CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef)fileUrl, NULL);
    size_t gifCount = CGImageSourceGetCount(gifSource);
    NSMutableArray *frames = [[NSMutableArray alloc]init];
    for (size_t i = 0; i< gifCount; i++) {
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
        UIImage *image = [UIImage imageWithCGImage:imageRef];
        [frames addObject:image];
        CGImageRelease(imageRef);
    }
    return frames;
}

@end
