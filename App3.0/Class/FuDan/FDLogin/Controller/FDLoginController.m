//
//  FDLoginController.m
//  App3.0
//
//  Created by lichao on 2018/10/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDLoginController.h"
#import "FDLoginTopView.h"
#import "FDRegisterCell.h"
#import "RSAEncryptor.h"
#import "FDFindPswController.h"

@interface FDLoginController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *menuArr;

//登录项
@property (nonatomic, weak) UITextField *phoneTextF;
@property (nonatomic, weak) UITextField *pswTextF;

@end

@implementation FDLoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuArr = @[@{@"image":@"fd_resgister_phone", @"name":@"账号"},
                     @{@"image":@"fd_resgister_psw", @"name":@"密码"},
                     @{@"image":@"", @"name":@"立即注册"}
                     ];
    
    [self showBackgroudViewWithImageName:@"fd_login_bg"];
    [self setUpTableView];
    [self setUpCustomNav];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDRegisterCell registerClassCellWithTableView:self.tableView];
    
    FDLoginTopView *topV = [FDLoginTopView initView];
    topV.frame = CGRectMake(0, 0, mainWidth, 407);
    self.tableView.tableHeaderView = topV;
}


#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDRegisterCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section > 0) {
        return 16;
    }
    return 0.01;}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == self.menuArr.count-1) {//立即注册
        return 43;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 43)];
    
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor hexFloatColor:@"FFE816"];
    [footerV addSubview:lineV];
    
    UILabel *forgotLabel = [UILabel new];
    forgotLabel.text = @"忘记密码?";
    forgotLabel.font = [UIFont systemFontOfSize:13];
    forgotLabel.textColor = [UIColor hexFloatColor:@"FFE816"];
    forgotLabel.userInteractionEnabled = YES;
    [forgotLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPassword)]];
    [footerV addSubview:forgotLabel];
    
    UILabel *registerLabel = [UILabel new];
    registerLabel.text = @"立即注册";
    registerLabel.font = SYSTEM_FONT(13);
    registerLabel.textColor = [UIColor hexFloatColor:@"FFE816"];
    registerLabel.userInteractionEnabled = YES;
    [registerLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushToRegisterController)]];
    [footerV addSubview:registerLabel];
    
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(footerV);
        make.size.mas_equalTo(CGSizeMake(1, 14));
    }];
    
    [forgotLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(lineV.mas_left).offset(-5);
        make.centerY.mas_equalTo(footerV);
    }];
    [registerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineV.mas_right).offset(5);
        make.centerY.mas_equalTo(forgotLabel);
    }];
    
    if (section == self.menuArr.count-1) {//立即注册
        return footerV;
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDRegisterCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDRegisterCell identifier]];
    if (cell == nil) {
        cell = [[FDRegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDRegisterCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = self.menuArr[indexPath.section];
    cell.logo.image = [UIImage imageNamed:dict[@"image"]];
    cell.inputTextF.placeholder = dict[@"name"];
    
    if (indexPath.section == 0) {//手机号
        self.phoneTextF = cell.inputTextF;
    }else if (indexPath.section == 1) {//登录密码
        self.pswTextF = cell.inputTextF;
        self.pswTextF.secureTextEntry = YES;
    }else if (indexPath.section == self.menuArr.count-1) {//立即注册
        
        cell.logo.hidden = YES;
        cell.inputTextF.hidden = YES;
        
        UIButton *loginBtn = [self setButtonWithTitle:@"登录" frame:CGRectMake(0, 0, mainWidth-39*2, 44)];
        [loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:loginBtn];
    }
    
    return cell;
}

#pragma mark ===== 点击事件
//点击登录
- (void)loginBtnClick {
    [XSTool showProgressHUDTOView:self.view withText:@"登录中..."];
    //密码加密
    NSString *encryptPassWord = [RSAEncryptor encryptString:self.pswTextF.text];
    
    [HTTPManager doLoginin:self.phoneTextF.text password:encryptPassWord email:@"" mobile:@"" google:@"" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];

        if (state.status) {
            NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
            NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];

            //登录环信
            [[EMClient sharedClient] loginWithUsername:parser.data.uid password:pwd completion:^(NSString *aUsername, EMError *aError) {
                if (!aError) {
                    NSLog(@"登录成功");
                    [XSTool hideProgressHUDWithView:self.view];
                    
                    // 将guid存到本地沙盒
                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                    [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                    //        [ud synchronize];
                    
                    //将用户信息存储到本地沙盒
                    LoginDataParser *dataPaser = parser.data;
                    NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                    [user setObject:userData forKey:USERINFO_LOGIN];
                    [user synchronize];
                    
                    // 存储登录账号信息
                    NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":encryptPassWord,@"avatar":dataPaser.logo};
                    [[UserInstance ShardInstnce] saveAccount:accountDic];
                    
                    // 实例化user数据
                    [[UserInstance ShardInstnce] setupUserInfo];
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    NSLog(@"登录失败 %@",aError.errorDescription);
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"环信登录失败:%i",aError.code]];
                }
            }];
           
        } else {
           [XSTool hideProgressHUDWithView:self.view];
           [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

//忘记密码
- (void)forgotPassword {
    [self.navigationController pushViewController:[self getNameWithClass:@"FDFindPswController"] animated:YES];
}
//立即注册
- (void)pushToRegisterController {
    [self.navigationController pushViewController:[self getNameWithClass:@"FDRegisterController"] animated:YES];
}

- (UIViewController *)getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

#pragma mark ===== 功能
- (UIButton *)setButtonWithTitle:(NSString *)title frame:(CGRect)frame {
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = frame;
    [Btn setTitle:title forState:UIControlStateNormal];
    [Btn setTitleColor:[UIColor hexFloatColor:@"111111"] forState:UIControlStateNormal];
    Btn.titleLabel.font = [UIFont systemFontOfSize:14];
    Btn.backgroundColor = [UIColor hexFloatColor:@"FFE816"];
    Btn.layer.cornerRadius = frame.size.height/2;
    Btn.layer.masksToBounds = YES;
    return Btn;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


@end
