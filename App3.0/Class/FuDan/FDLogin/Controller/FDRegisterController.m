//
//  FDRegisterController.m
//  App3.0
//
//  Created by lichao on 2018/10/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDRegisterController.h"
#import "FDRegisterTopView.h"
#import "FDRegisterCell.h"
#import "MQVerCodeImageView.h"
#import "RSAEncryptor.h"
#import "AgreementViewController.h"

@interface FDRegisterController ()<UITableViewDelegate, UITableViewDataSource>
{
    BOOL _isTimer;// 是否在倒计时
}
@property (nonatomic, strong) MQVerCodeImageView *codeImageView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *menuArr;

//注册项
@property (nonatomic, weak) UITextField *phoneTextF;
@property (nonatomic, weak) UITextField *pswTextF;
@property (nonatomic, weak) UITextField *imageCodeTextF;
@property (nonatomic, weak) UITextField *smsTextF;
@property (nonatomic, weak) UITextField *inviteTextF;

//当前的图形验证码
@property (nonatomic, copy) NSString *imageCodeStr;
@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation FDRegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isTimer = NO;

    self.menuArr = @[@{@"image":@"fd_resgister_phone", @"name":@"输入手机号"},
                     @{@"image":@"fd_resgister_psw", @"name":@"设置登录密码"},
                     @{@"image":@"fd_resgister_imageCode", @"name":@"输入图形验证码"},
                     @{@"image":@"fd_resgister_sms", @"name":@"输入短信验证码"},
                     @{@"image":@"fd_resgister_invite", @"name":@"邀请人电话号码"},
                     @{@"image":@"", @"name":@"立即注册"},
                     ];
    
    [self showBackgroudViewWithImageName:@"fd_login_bg"];
    [self setUpTableView];
    [self setUpCustomNav];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:@"注册" textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        //点击头像滑到左侧
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDRegisterCell registerClassCellWithTableView:self.tableView];
    
    FDRegisterTopView *topV = [FDRegisterTopView initView];
    topV.frame = CGRectMake(0, 0, mainWidth, 150);
    self.tableView.tableHeaderView = topV;
}


#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDRegisterCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section > 0) {
        return 16;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == self.menuArr.count-1) {//立即注册
        return 43;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 43)];
    
    UILabel *firstLabel = [UILabel new];
    firstLabel.text = @"注册即视为同意";
    firstLabel.font = [UIFont systemFontOfSize:13];
    firstLabel.textColor = [UIColor whiteColor];
    [footerV addSubview:firstLabel];
    
    UILabel *proLabel = [UILabel new];
    proLabel.text = [NSString stringWithFormat:@"《%@注册服务协议》",APP_NAME];
    proLabel.font = SYSTEM_FONT(13);
    proLabel.textColor = [UIColor hexFloatColor:@"FFE816"];
    proLabel.userInteractionEnabled = YES;
    [proLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookProtocol:)]];
    [footerV addSubview:proLabel];
    
    [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(footerV).offset(65);
        make.centerY.mas_equalTo(footerV);
    }];
    [proLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(firstLabel.mas_right).offset(2);
        make.centerY.mas_equalTo(firstLabel);
    }];
    
    if (section == self.menuArr.count-1) {//立即注册
        return footerV;
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDRegisterCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDRegisterCell identifier]];
    if (cell == nil) {
        cell = [[FDRegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDRegisterCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = self.menuArr[indexPath.section];
    cell.logo.image = [UIImage imageNamed:dict[@"image"]];
    cell.inputTextF.placeholder = dict[@"name"];
    
    if (indexPath.section == 0) {//手机号
        self.phoneTextF = cell.inputTextF;
    }else if (indexPath.section == 1) {//登录密码
        self.pswTextF = cell.inputTextF;
        self.pswTextF.secureTextEntry = YES;
    }else if (indexPath.section == 2) {//图形验证码
        self.imageCodeTextF = cell.inputTextF;
        
        if (!self.codeImageView) {
            self.codeImageView = [[MQVerCodeImageView alloc] initWithFrame:CGRectMake((mainWidth-39*2)-100, (44-34)/2, 82, 34)];
            
            //当前图形码的文字
            kFDWeakSelf;
            self.codeImageView.bolck = ^(NSString *imageCodeStr){//看情况是否需要
                weakSelf.imageCodeStr = imageCodeStr;
            };
            
            self.codeImageView.isRotation = YES;
            [self.codeImageView freshVerCode];
            
            //点击刷新
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
            [self.codeImageView addGestureRecognizer:tap];
            [cell.bgView addSubview:self.codeImageView];
        }
    }else if (indexPath.section == 3) {//短信验证码
        self.smsTextF = cell.inputTextF;

        UIButton *smsBtn = [self setButtonWithTitle:@"获取验证码" frame:CGRectMake((mainWidth-39*2)-97, (44-39)/2, 94, 39)];
        [smsBtn addTarget:self action:@selector(getSmsCode:) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:smsBtn];
        
    }else if (indexPath.section == 4) {//邀请人
        self.inviteTextF = cell.inputTextF;
        
    }else if (indexPath.section == self.menuArr.count-1) {//立即注册
        
        cell.logo.hidden = YES;
        cell.inputTextF.hidden = YES;
        
        UIButton *registerBtn = [self setButtonWithTitle:@"立即注册" frame:CGRectMake(0, 0, mainWidth-39*2, 44)];
        [registerBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cell.bgView addSubview:registerBtn];
    }
    
    return cell;
}

#pragma mark - 查看协议
- (void)lookProtocol:(UITapGestureRecognizer *)tap{
    AgreementViewController *controller = [[AgreementViewController alloc] initWithAgreementType:AgreementTypeReg];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark ===== 点击事件
//点击注册按钮
- (void)registerBtnClick {
    if(self.phoneTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"手机号不能为空"];
        return;
    }else {
        if (self.phoneTextF.text.length != 11) {
            [XSTool showToastWithView:self.view Text:@"手机号格式不正确"];
            return;
        }
    }
    if(self.pswTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"密码不能为空"];
        return;
    }
    if(self.imageCodeTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入图形验证码"];
        return;
    }
    if(self.smsTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入短信验证码"];
        return;
    }
    if(self.inviteTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入邀请人号码"];
        return;
    }
    
    NSString *encryptPassWord = [RSAEncryptor encryptString:self.pswTextF.text];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneTextF.text;//手机号
    params[@"passwd_one"] = encryptPassWord;//密码
    params[@"passwd_two"] = encryptPassWord;//密码
    params[@"verify_sms"] = self.smsTextF.text;//短信验证码
    params[@"intro"] = self.inviteTextF.text;//邀请人手机号
    
    [HTTPManager registerWithParams:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 注册成功
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showProgressHUDTOView:self.view withText:@"注册成功，登录中..."];
            
            //自动登录
            [HTTPManager doLoginin:dic[@"data"][@"username"] password:encryptPassWord success:^(NSDictionary * _Nullable dic, resultObject *state) {
                               
                LoginParser *parser = [LoginParser mj_objectWithKeyValues:dic];
                if (state.status) {
                    NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
                    NSString *pwd = [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,parser.data.uid]];
                 
                    //登录环信
                    [[EMClient sharedClient] loginWithUsername:parser.data.uid password:pwd completion:^(NSString *aUsername, EMError *aError) {
                        if (!aError) {
                            NSLog(@"登录成功");
                            [XSTool hideProgressHUDWithView:self.view];
                            
                            // 将guid存到本地沙盒
                            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                            [ud setObject:parser.data.guid forKey:APPAUTH_KEY];
                            //        [ud synchronize];
                            
                            //将用户信息存储到本地沙盒
                            LoginDataParser *dataPaser = parser.data;
                            NSLog(@"%@-%@",dataPaser.username,dataPaser.nickname);
                            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                            NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:dataPaser];
                            [user setObject:userData forKey:USERINFO_LOGIN];
                            [user synchronize];
                            
                            // 存储登录账号信息
                            NSDictionary *accountDic = @{@"account":isEmptyString(dataPaser.mobile)?dataPaser.username:dataPaser.mobile,@"password":encryptPassWord,@"avatar":dataPaser.logo};
                            [[UserInstance ShardInstnce] saveAccount:accountDic];
                            
                            // 实例化user数据
                            [[UserInstance ShardInstnce] setupUserInfo];
                            
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                        } else {
                            NSLog(@"登录失败 %@",aError.errorDescription);
                            [XSTool hideProgressHUDWithView:self.view];
                            [XSTool showToastWithView:self.view Text:[NSString stringWithFormat:@"环信登录失败:%i",aError.code]];
                        }
                    }];
                    
                }else {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
            }];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

//获取验证码
- (void)getSmsCode:(UIButton *)sender {
    if(self.imageCodeTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:@"请输入图形验证码"];
        return;
    }else {
        if (![[self.imageCodeTextF.text uppercaseString] isEqualToString:[self.imageCodeStr uppercaseString]]) {
            [XSTool showToastWithView:self.view Text:@"图形验证码错误"];
            [self tapClick:nil];
            return;
        }
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [HTTPManager regGetSmsVerify:self.phoneTextF.text success:^(NSDictionary *dic, resultObject *state) {
        // 验证码发送成功
        if (state.status) {
            [XSTool showToastWithView:self.view Text:@"发送成功"];
            _isTimer = YES; // 设置倒计时状态为YES
            sender.enabled = NO; // 设置按钮为不可点击
            
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
            [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                //设置按钮的样式
                dispatch_source_cancel(_timer);
                [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                sender.enabled = YES; // 设置按钮可点击
                
                _isTimer = NO; // 倒计时状态为NO
            } otherAction:^(int time) {
                [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
            }];
            
        } else {
            if (state.info.length>0) {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            sender.enabled = YES; // 设置按钮为可点击
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
        sender.enabled = YES; // 设置按钮为可点击
    }];
}

//刷新图形验证码
- (void)tapClick:(UITapGestureRecognizer*)tap {
    [self.codeImageView freshVerCode];
}

#pragma mark ===== 功能
- (UIButton *)setButtonWithTitle:(NSString *)title frame:(CGRect)frame {
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = frame;
    [Btn setTitle:title forState:UIControlStateNormal];
    [Btn setTitleColor:[UIColor hexFloatColor:@"111111"] forState:UIControlStateNormal];
    Btn.titleLabel.font = [UIFont systemFontOfSize:14];
    Btn.backgroundColor = [UIColor hexFloatColor:@"FFE816"];
    Btn.layer.cornerRadius = frame.size.height/2;
    Btn.layer.masksToBounds = YES;
    return Btn;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
