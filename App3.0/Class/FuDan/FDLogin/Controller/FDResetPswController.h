//
//  FDResetPswController.h
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

//重置密码
@interface FDResetPswController : FDBaseController

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *smsCode;

@end
