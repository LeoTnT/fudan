//
//  AdViewController.h
//  App3.0
//
//  Created by lichao on 2018/6/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

typedef void(^gifFinishedBlock)(void);
@interface AdViewController : XSBaseViewController

@property (nonatomic, strong) gifFinishedBlock gifFinishedBlock;

@end
