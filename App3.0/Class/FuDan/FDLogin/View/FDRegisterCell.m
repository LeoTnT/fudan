//
//  FDRegisterCell.m
//  App3.0
//
//  Created by lichao on 2018/10/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDRegisterCell.h"

@interface FDRegisterCell()

@end

@implementation FDRegisterCell

+ (CGFloat)height {
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.layer.cornerRadius = 44/2;
    self.bgView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.left.mas_equalTo(self).offset(39);
        make.right.mas_equalTo(self).offset(-39);
    }];
    
    //imageV
    self.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no_pic"]];
    [self.bgView addSubview:self.logo];
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView);
        make.left.mas_equalTo(self.bgView).offset(16);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    
    //输入框
    self.inputTextF = [[UITextField alloc] init];
    self.inputTextF.textColor = [UIColor hexFloatColor:@"242424"];
    self.inputTextF.font = [UIFont systemFontOfSize:16];
    [self.bgView addSubview:self.inputTextF];
    [self.inputTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView);
        make.left.mas_equalTo(self.logo.mas_right).offset(11);
        make.right.mas_equalTo(self.bgView).offset(-11);
        make.top.bottom.mas_equalTo(self.bgView);
    }];
}

@end
