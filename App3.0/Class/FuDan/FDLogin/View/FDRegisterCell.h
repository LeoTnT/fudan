//
//  FDRegisterCell.h
//  App3.0
//
//  Created by lichao on 2018/10/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"

@interface FDRegisterCell : FDBaseTableViewCell

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UITextField *inputTextF;

@end
