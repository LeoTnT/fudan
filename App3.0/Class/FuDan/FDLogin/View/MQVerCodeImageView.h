//
//  MQVerCodeImageView.h
//  App3.0
//
//  Created by xinshang on 2017/10/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MQCodeImageBlock)(NSString *codeStr);
@interface MQVerCodeImageView : UIView

@property (nonatomic, strong) NSString *imageCodeStr;
@property (nonatomic, assign) BOOL isRotation;
@property (nonatomic, copy) MQCodeImageBlock bolck;

-(void)freshVerCode;

@end
