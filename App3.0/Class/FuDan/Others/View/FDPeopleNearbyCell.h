//
//  FDPeopleNearbyCell.h
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDFindModel.h"

@interface FDPeopleNearbyCell : UITableViewCell

+ (void)registerClassCellWithTableView:(UITableView *)tableView;
+ (NSString *)identifier;

@property (nonatomic, strong) FDNearbyPeopleModel *model;

@end
