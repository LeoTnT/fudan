//
//  FDCartTicketCell.m
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDCartTicketCell.h"

@interface FDCartTicketCell ()

//背景view
@property (nonatomic, strong) UIView *bgView;
//价格单位
@property (nonatomic, strong) UILabel *pricelUnitL;
//价格
@property (nonatomic, strong) UILabel *pricelL;

//卡券名字
@property (nonatomic, strong) UILabel *nameL;
//地址
@property (nonatomic, strong) UILabel *addL;
//有效期限
@property (nonatomic, strong) UILabel *validityL;

//序号
@property (nonatomic, strong) UILabel *numberL;
//使用/过期提示图片
@property (nonatomic, strong) UIImageView *alertImageV;
//二维码
@property (nonatomic, strong) UIImageView *qrImageV;

@end

@implementation FDCartTicketCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setUpUI];
    }
    return self;
}

- (void)setListType:(ListType)listType {
    if (listType == ListTypeUnused) {//未使用
        self.alertImageV.hidden = YES;
        
        NSString *textStr = @"1234 5678 9123";
        self.numberL.text = textStr;

    }else {
        
        self.alertImageV.hidden = NO;

        NSString *textStr = @"1234 5678 9123";
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:textStr attributes:attribtDic];
        
        // 赋值
        self.numberL.attributedText = attribtStr;
        
        self.pricelUnitL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        self.pricelL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        self.nameL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        self.addL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        self.validityL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        self.numberL.textColor = [UIColor hexFloatColor:@"C0C0C0"];
        
        self.qrImageV.image = [UIImage imageNamed:@"fd_qrCode_gray"];
        
        if (listType == ListTypeUsed) {//已使用
            self.alertImageV.image = [UIImage imageNamed:@"fd_used_gray"];
        }else if (listType == ListTypeOverdue) {//已过期
            self.alertImageV.image = [UIImage imageNamed:@"fd_overdue_gray"];
        }
    }
}

- (void)setUpUI {
    //背景
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).offset(10);
        make.right.mas_equalTo(self.contentView).offset(-10);
    }];

    UIImageView *bgImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_cellBackgroundImage"]];
    [self.bgView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(self.bgView);
    }];
    
    //使用/过期提示
    self.alertImageV = [[UIImageView alloc] init];
    [self.bgView addSubview:self.alertImageV];
    [self.alertImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).offset(13);
        make.right.mas_equalTo(self.bgView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(66, 66));
    }];
    
    //单位
    self.pricelUnitL = [BaseUITool labelWithTitle:@"¥" textAlignment:NSTextAlignmentCenter font:SYSTEM_FONT(14) titleColor:mainColor];
    [self.bgView addSubview:self.pricelUnitL];
    [self.pricelUnitL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).offset(39);
        make.left.mas_equalTo(self.bgView).offset(17);
        make.width.mas_equalTo(FontNum(10));
    }];
    
    //价格
    self.pricelL = [BaseUITool labelWithTitle:@"128" textAlignment:NSTextAlignmentCenter font:SYSTEM_FONT(26) titleColor:mainColor];
    [self.bgView addSubview:self.pricelL];
    [self.pricelL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.pricelUnitL.mas_bottom).offset(2);
        make.left.mas_equalTo(self.pricelUnitL.mas_right).offset(0);
        make.width.mas_equalTo(FontNum(50));
    }];
 
    //卡券名
    self.nameL = [BaseUITool labelWithTitle:Localized(@"卡券名称") textAlignment:NSTextAlignmentLeft font:[UIFont boldSystemFontOfSize:18] titleColor:[UIColor hexFloatColor:@"111111"]];
    [self.bgView addSubview:self.nameL];
    [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).offset(20);
        make.left.mas_equalTo(self.pricelL.mas_right).offset(20);
        make.right.mas_equalTo(self.alertImageV.mas_left).offset(-10);
    }];
    
    //地址
    self.addL = [BaseUITool labelWithTitle:[NSString stringWithFormat:@"%@: %@", Localized(@"区块链地址"), @"F5hc24****454gu"] textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(12) titleColor:[UIColor hexFloatColor:@"999999"]];
    [self.bgView addSubview:self.addL];
    [self.addL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameL.mas_bottom).offset(8);
        make.left.mas_equalTo(self.nameL);
        make.right.mas_equalTo(self.nameL);
        make.height.mas_equalTo(13);
    }];
    
    //有效期限
    self.validityL = [BaseUITool labelWithTitle:[NSString stringWithFormat:@"%@ %@", Localized(@"有效期限"), @"2018.07.21-10.23"] textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(12) titleColor:[UIColor hexFloatColor:@"999999"]];
    [self.bgView addSubview:self.validityL];
    [self.validityL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addL.mas_bottom).offset(5);
        make.left.mas_equalTo(self.nameL);
        make.right.mas_equalTo(self.nameL);
        make.height.mas_equalTo(13);
    }];
    
    //二维码标志
    self.qrImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_qrCode"]];
    [self.bgView addSubview:self.qrImageV];
    [self.qrImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.bgView).offset(-13);
        make.right.mas_equalTo(self.bgView).offset(-13);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    //序号
    self.numberL = [BaseUITool labelWithTitle:@"1234 5678 9123" textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:16] titleColor:[UIColor hexFloatColor:@"111111"]];
    [self.bgView addSubview:self.numberL];
    [self.numberL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.bgView).offset(-16);
        make.left.mas_equalTo(self.bgView).offset(13);
        make.height.mas_equalTo(13);
    }];
}

#pragma mark === init
+ (void)registerClassCellWithTableView:(UITableView *)tableView {
    [tableView registerClass:[self class] forCellReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

@end
