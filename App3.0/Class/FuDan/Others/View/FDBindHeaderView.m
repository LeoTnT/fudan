
//
//  FDBindHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBindHeaderView.h"

@interface FDBindHeaderView ()

@property (nonatomic, strong) NSMutableArray *buttonArr;

@end

@implementation FDBindHeaderView

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setDataWithTitleArr:(NSArray *)titleArr {

    for (int i = 0; i < titleArr.count; i++) {
        
        CGFloat margin = 10;
        CGFloat width = (mainWidth - margin*5) / 4;
        CGFloat height = 36;
        CGFloat x = margin + (width+margin)*(i%4);
        CGFloat y = 17 + (height+margin)*(i/4);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, y, width, height);
        button.titleLabel.font = [UIFont systemFontOfSize:15];
        
        [button setTitle:titleArr[i] forState:UIControlStateNormal];
        [button setTitle:titleArr[i] forState:UIControlStateSelected];
        [button setTitleColor:mainColor forState:UIControlStateNormal];
        [button setTitleColor:[UIColor hexFloatColor:@"FFFFFF"] forState:UIControlStateSelected];
        
        [button setBackgroundColor:[UIColor hexFloatColor:@"DAEEFF"]];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        button.layer.borderWidth = 1;
        button.layer.borderColor = mainColor.CGColor;
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = YES;
        
        [self.buttonArr addObject:button];
        [self addSubview:button];
        
        if (i == (titleArr.count-1)) {
            self.frame = CGRectMake(0, 0, mainWidth, CGRectGetMaxY(button.frame)+margin);
        }
    }
}

- (void)buttonClick:(UIButton *)button {
    for (UIButton *tempBtn in self.buttonArr) {
        tempBtn.selected = NO;
        [tempBtn setBackgroundColor:[UIColor hexFloatColor:@"DAEEFF"]];
    }
    button.selected = !button.selected;
    [button setBackgroundColor:mainColor];
    
    if (self.buttonClickBlock) {
        self.buttonClickBlock(button.titleLabel.text);
    }
}

- (NSMutableArray *)buttonArr {
    if (!_buttonArr) {
        _buttonArr = [NSMutableArray array];
    }
    return _buttonArr;
}

@end
