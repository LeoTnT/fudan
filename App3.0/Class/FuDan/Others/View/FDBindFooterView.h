//
//  FDBindFooterView.h
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BindBlock)(void);
@interface FDBindFooterView : UIView

@property (nonatomic, strong) UIButton *confirmBtn;

@property (nonatomic, copy) BindBlock BindBlock;

@end
