//
//  FDShopCell.h
//  App3.0
//
//  Created by lichao on 2017/5/19.
//  Copyright © 2017年 lichao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDHomeModel.h"

typedef void(^playBlock)(void);
@interface FDVideoCell : UICollectionViewCell

@property (nonatomic, copy) playBlock playBlock;
/** 商品模型 */
@property (nonatomic, strong) FDVideoListModel *model;

+ (NSString *)identifier;

@end
