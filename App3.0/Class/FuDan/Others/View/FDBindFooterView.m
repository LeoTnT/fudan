//
//  FDBindFooterView.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBindFooterView.h"

@implementation FDBindFooterView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    
    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.confirmBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    
    [self.confirmBtn setTitle:Localized(@"确定") forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:[UIColor hexFloatColor:@"FFFFFF"] forState:UIControlStateNormal];
    
    [self.confirmBtn addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.confirmBtn setBackgroundColor:mainColor];
    
    self.confirmBtn.layer.cornerRadius = 4;
    self.confirmBtn.layer.masksToBounds = YES;
    [self addSubview:self.confirmBtn];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self).offset(25);
        make.right.mas_equalTo(self).offset(-25);
        make.height.mas_equalTo(50);
    }];
}

/* 按钮点击事件 */
- (void)buttonClick {
    if (self.BindBlock) {
        self.BindBlock();
    }
}

@end
