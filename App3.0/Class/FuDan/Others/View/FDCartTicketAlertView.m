//
//  FDCartTicketAlertView.m
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDCartTicketAlertView.h"

@interface FDCartTicketAlertView ()

//卡券名
@property (nonatomic, strong) UILabel *titleL;
//卡券金额
@property (nonatomic, strong) UILabel *moneyL;
//序号
@property (nonatomic, strong) UILabel *numberL;

//二维码
@property (nonatomic, strong) UIImageView *qrImageV;

@end

@implementation FDCartTicketAlertView

- (instancetype)init {
    if (self=[super init]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    self.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    
    UIView *bgV = [[UIView alloc] init];
    bgV.backgroundColor = [UIColor whiteColor];
    bgV.layer.cornerRadius = 10;
    bgV.layer.masksToBounds = YES;
    [self addSubview:bgV];
    [bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(FontNum(310), FontNum(383)));
    }];
    
    //标题
    self.titleL = [BaseUITool labelWithTitle:@"福多多卡券码" textAlignment:NSTextAlignmentCenter font:[UIFont boldSystemFontOfSize:24] titleColor:[UIColor hexFloatColor:@"FF8600"]];
    [bgV addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgV).offset(34);
        make.centerX.mas_equalTo(bgV);
        make.height.mas_equalTo(23);
    }];
    
    //卡券金额
    UIView *subV = [[UIView alloc] init];
    subV.backgroundColor = [UIColor hexFloatColor:@"FFEFD7"];
    subV.layer.cornerRadius = 5;
    subV.layer.masksToBounds = YES;
    [bgV addSubview:subV];
    [subV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleL.mas_bottom).offset(12);
        make.centerX.mas_equalTo(bgV);
        make.size.mas_equalTo(CGSizeMake(FontNum(120), FontNum(26)));
    }];
    
    self.moneyL = [BaseUITool labelWithTitle:[NSString stringWithFormat:@"%@ %@", Localized(@"卡券金额"), @"¥128"] textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"FF8600"]];
    [subV addSubview:self.moneyL];
    [self.moneyL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(subV);
        make.left.mas_equalTo(subV).offset(5);
        make.right.mas_equalTo(subV).offset(-5);
        make.height.mas_equalTo(14);
    }];
    
    //序号
    self.numberL = [BaseUITool labelWithTitle:@"1234 5678 9123" textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:22] titleColor:[UIColor hexFloatColor:@"111111"]];
    [bgV addSubview:self.numberL];
    [self.numberL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(subV.mas_bottom).offset(30);
        make.left.mas_equalTo(bgV).offset(15);
        make.right.mas_equalTo(bgV).offset(-15);
        make.height.mas_equalTo(17);
    }];
    
    //二维码
    self.qrImageV = [[UIImageView alloc] init];
    self.qrImageV.backgroundColor = [UIColor orangeColor];
    [bgV addSubview:self.qrImageV];
    [self.qrImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.numberL.mas_bottom).offset(19);
        make.centerX.mas_equalTo(bgV);
        make.size.mas_equalTo(CGSizeMake(FontNum(182), FontNum(182)));
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self removeFromSuperview];
}

@end
