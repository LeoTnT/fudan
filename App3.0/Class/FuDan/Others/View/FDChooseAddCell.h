//
//  FDChooseAddCell.h
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectedBlock)(UIButton *button);
@interface FDChooseAddCell : UITableViewCell

@property (nonatomic, copy) selectedBlock selectedBlock;

//选中按钮
@property (nonatomic, strong) UIButton *selectButton;

+ (void)registerClassCellWithTableView:(UITableView *)tableView;
+ (NSString *)identifier;

@end
