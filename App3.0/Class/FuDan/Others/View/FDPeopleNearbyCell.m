//
//  FDPeopleNearbyCell.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPeopleNearbyCell.h"
@interface FDPeopleNearbyCell ()

@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UILabel *disLabel;

@end

@implementation FDPeopleNearbyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpUI];
    }
    return self;
}

- (void)setModel:(FDNearbyPeopleModel *)model {
    if (model) {
        [self.logo sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.nameLabel.text = model.nickname;
        self.detailLabel.text = model.fandom;
        self.disLabel.text = model.distance;
    }
}

- (void)setUpUI {
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 5;
    bgView.layer.masksToBounds = YES;
    [self.contentView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).offset(12);
        make.right.mas_equalTo(self.contentView).offset(-12);
    }];
    
    self.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no_pic"]];
    [bgView addSubview:self.logo];
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bgView);
        make.left.mas_equalTo(bgView).offset(12);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    self.nameLabel = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(16) titleColor:Color(@"111111")];
    [bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView).offset(18);
        make.left.mas_equalTo(self.logo.mas_right).offset(13);
        make.height.mas_equalTo(16);
    }];
    
    self.detailLabel = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(13) titleColor:COLOR_999999];
    [bgView addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(11);
        make.left.mas_equalTo(self.logo.mas_right).offset(13);
        make.height.mas_equalTo(13);
    }];
    
    self.disLabel = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentRight font:SYSTEM_FONT(12) titleColor:COLOR_999999];
    [bgView addSubview:self.disLabel];
    [self.disLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView).offset(14);
        make.right.mas_equalTo(bgView).offset(-14);
        make.height.mas_equalTo(10);
    }];
}

#pragma mark === init
+ (void)registerClassCellWithTableView:(UITableView *)tableView {
    [tableView registerClass:[self class] forCellReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

@end
