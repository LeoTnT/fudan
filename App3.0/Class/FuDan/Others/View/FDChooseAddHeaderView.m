//
//  FDChooseAddHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChooseAddHeaderView.h"

@implementation FDChooseAddHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    //标题
    UILabel *titleL = [BaseUITool labelWithTitle:Localized(@"选择地址") textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:18] titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self);
        make.height.mas_equalTo(17);
    }];
    
    //关闭按钮
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"fd_add_close"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(self).offset(-16);
        make.size.mas_equalTo(CGSizeMake(14, 14));
    }];
    
    //下划线
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor hexFloatColor:@"E5E5E5"];
    [self addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

- (void)closeBtnClick {
    if (self.closeBlock) {
        self.closeBlock();
    }
}

@end
