//
//  FDChooseAddFooterView.m
//  App3.0
//
//  Created by lichao on 2018/8/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChooseAddFooterView.h"

@implementation FDChooseAddFooterView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor hexFloatColor:@"E5E5E5"];
    [self addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    //"添加地址"
    UILabel *addLabel = [BaseUITool labelWithTitle:Localized(@"添加地址") textAlignment:NSTextAlignmentCenter font:[UIFont systemFontOfSize:17] titleColor:mainColor];
    [self addSubview:addLabel];
    [addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.centerX.mas_equalTo(self).offset(10);
        make.height.mas_equalTo(16);
    }];
    
    //+图片
    UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_add"]];
    [self addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(addLabel.mas_left).offset(-3);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addClick)];
    [self addGestureRecognizer:tap];
}

- (void)addClick {
    if (self.addBlock) {
        self.addBlock();
    }
}

@end
