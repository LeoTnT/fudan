//
//  FDChooseAddressView.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChooseAddressView.h"
#import "FDChooseAddHeaderView.h"
#import "FDChooseAddFooterView.h"
#import "FDChooseAddCell.h"
#import "FDAddAddressController.h"

@interface FDChooseAddressView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, weak) UIButton *lastButton;
@property (nonatomic, weak) NSIndexPath *lastIndexPath;

@end

@implementation FDChooseAddressView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [FDChooseAddCell registerClassCellWithTableView:self.tableView];

    [self setUpHeaderView];
    [self setUpBottomView];
}

- (void)setUpHeaderView {
    CGRect frame = CGRectMake(0, mainHeight-450, mainWidth, 50);
    FDChooseAddHeaderView *headerV = [[FDChooseAddHeaderView alloc] initWithFrame:frame];
    headerV.backgroundColor = [UIColor whiteColor];
    //切任意圆角(只切上面两个)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_tableView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = headerV.bounds;
    maskLayer.path = maskPath.CGPath;
    headerV.layer.mask = maskLayer;
    
    //点击关闭按钮
    __weak typeof(self) weakSelf = self;
    headerV.closeBlock = ^{
        [weakSelf removeFromSuperview];
    };
    
    [self addSubview:headerV];
}

- (void)setUpBottomView {
    CGRect frame = CGRectMake(0, mainHeight-50, mainWidth, 50);
    FDChooseAddFooterView *footerV = [[FDChooseAddFooterView alloc] initWithFrame:frame];
    footerV.backgroundColor = [UIColor whiteColor];
    //点击添加地址
    __weak typeof(self) weakSelf = self;
    footerV.addBlock = ^{
        
        FDAddAddressController *addVC = [[FDAddAddressController alloc] init];
        [weakSelf.superController.navigationController pushViewController:addVC animated:YES];
        [weakSelf removeFromSuperview];
    };
    
    [self addSubview:footerV];
}

#pragma mark --<UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 73;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDChooseAddCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDChooseAddCell identifier]];
    if (cell == nil) {
        cell = [[FDChooseAddCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDChooseAddCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (self.lastIndexPath && [self.lastIndexPath isEqual:indexPath]) {
        cell.selectButton.selected = YES;
    }else {
        cell.selectButton.selected = NO;
    }
    
    //点击按钮
    __weak typeof(self) weakSelf = self;
    cell.selectedBlock = ^(UIButton *button) {
        
        button.selected = !button.selected;
        
        if (weakSelf.lastButton && ![weakSelf.lastButton isEqual:button]) {
            weakSelf.lastButton.selected = NO;
        }
        
        weakSelf.lastButton = nil;
        weakSelf.lastIndexPath = nil;
        weakSelf.lastButton = button;
        weakSelf.lastIndexPath = indexPath;
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDChooseAddCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectButton.selected = !cell.selectButton.selected;

    if (self.lastButton && ![self.lastButton isEqual:cell.selectButton]) {
        self.lastButton.selected = NO;
    }
    
    self.lastButton = nil;
    self.lastIndexPath = nil;
    self.lastButton = cell.selectButton;
    self.lastIndexPath = indexPath;
}

#pragma mark ===== 懒加载
- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = CGRectMake(0, mainHeight-400, mainWidth, 350);
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self addSubview:_tableView];
    }
    return _tableView;
}

@end
