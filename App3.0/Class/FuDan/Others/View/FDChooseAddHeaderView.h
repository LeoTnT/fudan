//
//  FDChooseAddHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^closeBlock)(void);
@interface FDChooseAddHeaderView : UIView

@property (nonatomic, copy) closeBlock closeBlock;

@end
