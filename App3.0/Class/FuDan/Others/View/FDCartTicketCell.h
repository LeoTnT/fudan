//
//  FDCartTicketCell.h
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ListType) {
    ListTypeUnused,
    ListTypeUsed,
    ListTypeOverdue
};
//卡券cell
@interface FDCartTicketCell : UITableViewCell

@property (nonatomic, assign) ListType listType;

+ (void)registerClassCellWithTableView:(UITableView *)tableView;
+ (NSString *)identifier;
    
@end
