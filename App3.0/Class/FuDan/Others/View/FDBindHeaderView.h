//
//  FDBindHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^buttonClickBlock)(NSString *name);
@interface FDBindHeaderView : UIView

@property (nonatomic, copy) buttonClickBlock buttonClickBlock;

- (void)setDataWithTitleArr:(NSArray *)titleArr;

@end
