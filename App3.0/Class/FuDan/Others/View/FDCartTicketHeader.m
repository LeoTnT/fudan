//
//  FDCartTicketHeader.m
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDCartTicketHeader.h"

@interface FDCartTicketHeader ()

//标题
@property (nonatomic, strong) UILabel *titleL;
//剩余金额
@property (nonatomic, strong) UILabel *moneyL;
//剩余张数
@property (nonatomic, strong) UILabel *numberL;

@end

@implementation FDCartTicketHeader

- (instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    //标题
    self.titleL = [BaseUITool labelWithTitle:Localized(@"区块链卡券剩余") textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(14) titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(13);
        make.height.mas_equalTo(14);
    }];
    
    //剩余张数
    self.numberL = [BaseUITool labelWithTitle:Localized(@"¥123") textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(14) titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:self.numberL];
    [self.numberL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-13);
        make.height.mas_equalTo(14);
    }];
    
    //剩余金额
    self.moneyL = [BaseUITool labelWithTitle:Localized(@"¥123") textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(14) titleColor:[UIColor hexFloatColor:@"666666"]];
    [self addSubview:self.moneyL];
    [self.moneyL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(self.numberL.mas_left);
        make.height.mas_equalTo(14);
    }];
}

@end
