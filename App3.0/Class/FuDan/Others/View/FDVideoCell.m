//
//  FDShopCell.h
//  App3.0
//
//  Created by lichao on 2017/5/19.
//  Copyright © 2017年 lichao. All rights reserved.
//

#import "FDVideoCell.h"
#import "UIImageView+WebCache.h"

@interface FDVideoCell()

//大图
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
//描述label
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
//logo
@property (weak, nonatomic) IBOutlet UIImageView *logo;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//播放按钮
@property (weak, nonatomic) IBOutlet UIButton *playBtn;

@end

@implementation FDVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imageView.layer.cornerRadius = 7;
    self.imageView.layer.masksToBounds = YES;
    
    self.logo.layer.cornerRadius = 27/2;
    self.logo.layer.masksToBounds = YES;
}

- (void)setModel:(FDVideoListModel *)model {
    if (model) {
        // 图片
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.video_image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.detailLabel.text = model.content;
        self.nameLabel.text = model.nickname;
        [self.logo sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (IBAction)playBtnClick:(id)sender {
    if (self.playBlock) {
        self.playBlock();
    }
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

@end
