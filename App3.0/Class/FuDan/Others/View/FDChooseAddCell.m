//
//  FDChooseAddCell.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChooseAddCell.h"

@interface FDChooseAddCell ()

//姓名
@property (nonatomic, strong) UILabel *nameLabel;
//地址
@property (nonatomic, strong) UILabel *addLabel;

@end

@implementation FDChooseAddCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    //选中按钮
    self.selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.selectButton setImage:[UIImage imageNamed:@"fd_unselected"] forState:UIControlStateNormal];
    [self.selectButton setImage:[UIImage imageNamed:@"fd_selected"] forState:UIControlStateSelected];
    [self.selectButton addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.selectButton];
    [self.selectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(20);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    
    //姓名
    self.nameLabel = [BaseUITool labelWithTitle:Localized(@"姓名") textAlignment:NSTextAlignmentLeft font:[UIFont boldSystemFontOfSize:16] titleColor:[UIColor hexFloatColor:@"111111"]];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).offset(19);
        make.left.mas_equalTo(self.selectButton.mas_right).offset(16);
        make.right.mas_equalTo(self.contentView).offset(-16);
        make.height.mas_equalTo(16);
    }];
    
    //地址
    self.addLabel = [BaseUITool labelWithTitle:Localized(@"钱包地址  Eds4f6sd4fcvx3d65dfsd99") textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"999999"]];
    [self.contentView addSubview:self.addLabel];
    [self.addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(8);
        make.left.mas_equalTo(self.nameLabel);
        make.right.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(15);
    }];
    
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor hexFloatColor:@"E5E5E5"];
    [self.contentView addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
        make.left.mas_equalTo(self.contentView).offset(20);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
}

- (void)selectBtnClick:(UIButton *)button {
    if (self.selectedBlock) {
        self.selectedBlock(button);
    }
}

#pragma mark === init
+ (void)registerClassCellWithTableView:(UITableView *)tableView {
    [tableView registerClass:[self class] forCellReuseIdentifier:[self identifier]];
}

+ (NSString *)identifier {
    return NSStringFromClass(self);
}

@end
