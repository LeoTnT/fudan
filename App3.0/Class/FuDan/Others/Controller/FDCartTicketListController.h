//
//  CartTicketListController.h
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "FDCartTicketCell.h"

//卡券列表
@interface FDCartTicketListController : XSBaseViewController

- (instancetype)initWithOrderType:(ListType)type;

@end
