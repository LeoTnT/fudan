//
//  FDAddAddressController.m
//  App3.0
//
//  Created by lichao on 2018/8/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDAddAddressController.h"
#import "FDBindFooterView.h"

@interface FDAddAddressController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

/* 钱包地址 */
@property (nonatomic, weak) UITextField *walletAddTextF;
/* 备注 */
@property (nonatomic, weak) UITextField *remarkTextF;

@end

@implementation FDAddAddressController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setUpUI];
}

#pragma mark ===== 网络请求
- (void)addRequest {
    NSDictionary *params = @{@"add":self.walletAddTextF.text, @"remark":self.remarkTextF.text};
    NSLog(@"==============%@", params);

}

#pragma mark ===== setUpUI
- (void)setUpUI {
    self.navigationItem.title = Localized(@"添加钱包地址");
    self.dataArr = @[Localized(@"钱包地址"), Localized(@"备注")];
    
    [self.view addSubview:self.tableView];
    
    FDBindFooterView *footerV = [[FDBindFooterView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44*2+50)];
    [footerV.confirmBtn setTitle:Localized(@"立即添加") forState:UIControlStateNormal];
    self.tableView.tableFooterView = footerV;
    //点击立即添加按钮
   __weak typeof(self) weakSelf = self;
    footerV.BindBlock = ^{
        [weakSelf addRequest];
    };
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 49;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"listCellID"];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.textLabel.text = self.dataArr[indexPath.row];
    cell.textLabel.textColor = [UIColor hexFloatColor:@"666666"];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    UITextField *textF = [self setCustomTextField];
    textF.userInteractionEnabled = YES;
    [cell.contentView addSubview:textF];
    [textF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView);
        make.right.mas_equalTo(cell.contentView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(cell.contentView.frame.size.width*2/3, 14));
    }];
    
    switch (indexPath.row) {
        case 0: {
            textF.placeholder = Localized(@"请输入钱包地址");
            self.walletAddTextF = textF;
        }
            break;
        case 1: {
            textF.placeholder = Localized(@"请输入备注信息");
            self.remarkTextF = textF;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

/* 自定义TextField */
- (UITextField *)setCustomTextField {
    UITextField *textF = [[UITextField alloc] init];
    textF.textColor = [UIColor hexFloatColor:@"111111"];
    textF.font = [UIFont systemFontOfSize:15];
    textF.textAlignment = NSTextAlignmentRight;
    return textF;
}

#pragma mark ===== 懒加载
- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = self.view.bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
    }
    return _tableView;
}

@end
