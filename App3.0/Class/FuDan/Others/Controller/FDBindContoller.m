//
//  FDBindContollerController.m
//  App3.0
//
//  Created by lichao on 2018/8/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBindContoller.h"
#import "FDBindHeaderView.h"
#import "FDBindFooterView.h"
#import "FDChooseAddressView.h"

@interface FDBindContoller ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

/* 提呗名称 */
@property (nonatomic, weak) UITextField *nameTextField;
/* 提呗数量 */
@property (nonatomic, weak) UITextField *inputTextField;
/* 提呗地址 */
@property (nonatomic, weak) UITextField *addTextField;
/* 手续费 */
@property (nonatomic, weak) UITextField *chargeTextField;
/* 短信验证码 */
@property (nonatomic, weak) UITextField *codeTextField;

@end

@implementation FDBindContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
    [self setUpTableView];
}

#pragma mark ===== setUpUI
- (void)setUpUI {
    self.navigationItem.title = Localized(@"绑定");
    self.dataArr = @[Localized(@"提呗名称"), Localized(@"提呗数量"), Localized(@"提呗地址"), Localized(@"手续费"), Localized(@"短信验证码"),];
}

- (void)setUpTableView {
    
    [self.view addSubview:self.tableView];

    FDBindHeaderView *headerV = [[FDBindHeaderView alloc] init];
    [headerV setDataWithTitleArr:@[Localized(@"福呗"), Localized(@"比特币"), Localized(@"以太坊"), Localized(@"呆马"), Localized(@"哈哈哈")]];
    self.tableView.tableHeaderView = headerV;
    //点击货币按钮
    headerV.buttonClickBlock = ^(NSString *name) {
        NSLog(@"==============%@", name);
    };
    

    FDBindFooterView *footerV = [[FDBindFooterView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44*2+50)];
    self.tableView.tableFooterView = footerV;
    //点击确定按钮
    footerV.BindBlock = ^{
        NSLog(@"==============");
    };
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 49;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"listCellID"];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.textLabel.text = self.dataArr[indexPath.row];
    cell.textLabel.textColor = [UIColor hexFloatColor:@"999999"];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    UITextField *textF = [self setCustomTextField];
    textF.userInteractionEnabled = YES;
    [cell.contentView addSubview:textF];
    [textF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView);
        make.right.mas_equalTo(cell.contentView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(cell.contentView.frame.size.width*2/3, 14));
    }];
    
    switch (indexPath.row) {
        case 0: {//名称label
            UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_tibeiName"]];
            [cell.contentView addSubview:logo];
            [logo mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(cell.contentView).offset(-10);
                make.size.mas_equalTo(CGSizeMake(20, 20));
            }];
            
            textF.text = Localized(@"提呗名称");
            textF.userInteractionEnabled = NO;
            [textF mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(logo.mas_left).offset(-10);
                make.size.mas_equalTo(CGSizeMake(cell.contentView.frame.size.width/2, 14));
            }];
            
            self.nameTextField = textF;
        }
            break;
        case 1: {//数量textField
            textF.placeholder = Localized(@"请输入");
            self.inputTextField = textF;
        }
            break;
        case 2: {//地址label
            textF.text = Localized(@"Hcj5654df9gfFD22565m2");
            textF.userInteractionEnabled = NO;
            
            //按钮点击
            UIButton *tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            tempBtn.backgroundColor = [UIColor clearColor];
            [tempBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:tempBtn];
            [tempBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.right.bottom.mas_equalTo(textF);
            }];

            self.addTextField = textF;
        }
            break;
        case 3: {//手续费label
            textF.text = Localized(@"0.00");
            textF.userInteractionEnabled = NO;
            self.chargeTextField = textF;
        }
            break;
        case 4: {//短信验证码textField
            UIButton *codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [codeBtn setTitleColor:mainColor forState:UIControlStateNormal];
            [codeBtn setTitle:Localized(@"获取验证码") forState:UIControlStateNormal];
            codeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
            [codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:codeBtn];
            [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(cell.contentView).offset(-10);
                make.height.mas_equalTo(14);
//                make.size.mas_equalTo(CGSizeMake(75, 14));
            }];
            
            UIView *lineV = [[UIView alloc] init];
            lineV.backgroundColor = [UIColor hexFloatColor:@"DCDCDC"];
            [cell.contentView addSubview:lineV];
            [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(codeBtn.mas_left).offset(-10);
                make.size.mas_equalTo(CGSizeMake(1, 24));
            }];
            
            textF.placeholder = Localized(@"短信验证码");
            [textF mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(lineV.mas_left).offset(-10);
                make.size.mas_equalTo(CGSizeMake(cell.contentView.frame.size.width/2, 14));
            }];
       
            self.codeTextField = textF;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)addBtnClick {
    [self.view endEditing:YES];
    
    FDChooseAddressView *chooseV = [[FDChooseAddressView alloc] init];
    chooseV.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    chooseV.superController = self;
    
    [[UIApplication sharedApplication].keyWindow addSubview:chooseV];
}


/* 自定义TextField */
- (UITextField *)setCustomTextField {
    UITextField *textF = [[UITextField alloc] init];
    textF.textColor = [UIColor hexFloatColor:@"C0C0C0"];
    textF.font = [UIFont systemFontOfSize:15];
    textF.textAlignment = NSTextAlignmentRight;
    return textF;
}

/* 点击获取验证码 */
- (void)codeBtnClick:(UIButton *)button {
    
}

#pragma mark ===== 懒加载
- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = self.view.bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
    }
    return _tableView;
}

@end
