//
//  CardTicketViewController.m
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDCardTicketController.h"
#import "FDCartTicketListController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "FD_PickerView.h"

@interface FDCardTicketController ()<SDCycleScrollViewDelegate, UIScrollViewDelegate>

//头部滚动广告视图
@property (nonatomic, strong) SDCycleScrollView *bannerView;

@property(weak, nonatomic) UIButton *menuButton;
//选择器
@property (nonatomic, strong) FD_PickerView *pickerView;
@property (nonatomic, assign) NSInteger currentSelectRow;

//
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) FDCartTicketListController *listVC1;
@property (nonatomic, strong) FDCartTicketListController *listVC2;
@property (nonatomic, strong) FDCartTicketListController *listVC3;

@property (nonatomic, strong) NSMutableArray *titleArr;

@end

@implementation FDCardTicketController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.titleArr = [NSMutableArray arrayWithObjects:@"全部", @"福满堂", @"福多多", @"福上福", @"福天下", nil];
    
    [self setUpUI];
}

#pragma mark ===== setUpUI
- (void)setUpUI {

    self.navigationItem.titleView = [self setUpNav];

    [self setUpBannerView];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    [self setUpSubViews];
}

/************ 自定义导航栏 ************/
- (UIButton *)setUpNav {
    
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(0, 0, 100, 30);
    [menuBtn setTitleColor:[UIColor hexFloatColor:@"111111"] forState:UIControlStateNormal];
    menuBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    
    NSString *name = self.titleArr.firstObject;
    [menuBtn setTitle:name forState:UIControlStateNormal];
    //箭头图片设置
    [menuBtn setImage:[UIImage imageNamed:@"fd_down"] forState:UIControlStateNormal];
    
    [self adjustEdgeInsetWithButton:menuBtn];
    
    [menuBtn addTarget:self action:@selector(showPickerView) forControlEvents:UIControlEventTouchUpInside];
    
    self.menuButton = menuBtn;
    return menuBtn;
}

//调整按钮
- (void)adjustEdgeInsetWithButton:(UIButton *)button {
    
    //计算文字的宽度和图片的宽度, 交互位置
    CGFloat imageW = button.currentImage.size.width;
    CGFloat titleW = [button.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:button.titleLabel.font, NSFontAttributeName,nil]].width;
    button.imageEdgeInsets = UIEdgeInsetsMake(0, titleW+imageW+10, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -(imageW+15), 0, 0);
    
}

/************ 顶部广告轮播 ************/
- (void)setUpBannerView {
    
    self.bannerView.frame = CGRectMake(0, 0, mainWidth, FontNum(150));
    
    self.bannerView.delegate = self;
    self.bannerView.placeholderImage = [UIImage imageNamed:@"no_pic"];
    self.bannerView.backgroundColor = [UIColor clearColor];
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.hidesForSinglePage = YES;
    self.bannerView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
    self.bannerView.pageDotColor = [UIColor whiteColor];
    self.bannerView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.bannerView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    
//    localizationImageNamesGroup
    self.bannerView.localizationImageNamesGroup = @[@"no_pic"];
//    self.bannerView.imageURLStringsGroup = @[];

    [self.view addSubview:self.bannerView];
}

- (void)setUpSubViews{
    //根据选项变化位置和内容
    [self addChildViewController:self.listVC1];
    [self.scrollView addSubview:self.listVC1.view];
    [self addChildViewController:self.listVC2];
    [self.scrollView addSubview:self.listVC2.view];
    [self addChildViewController:self.listVC3];
    [self.scrollView addSubview:self.listVC3.view];
}

//选择器
- (void)showPickerView {
    
    self.pickerView.dataArr = (NSArray *)self.titleArr;
    self.pickerView.selectRow = self.currentSelectRow;
    __weak typeof(self) weakSelf = self;
    self.pickerView.completionHandler = ^(NSInteger selectRow) {
        weakSelf.currentSelectRow = selectRow;

        NSString *name = weakSelf.titleArr[selectRow];
        [weakSelf.menuButton setTitle:name forState:UIControlStateNormal];
        //箭头图片设置
        [weakSelf.menuButton setImage:[UIImage imageNamed:@"fd_down"] forState:UIControlStateNormal];
        
        [weakSelf adjustEdgeInsetWithButton:weakSelf.menuButton];
        
        //刷新数据
        
    };
    
    [self.pickerView show];
}

#pragma mark ===== SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (cycleScrollView == self.bannerView) {
      
    }
}

#pragma mark ===== HMSegmentedControl
- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

#pragma mark ===== UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
}

#pragma mark ===== 懒加载
- (NSMutableArray *)titleArr {
    if (!_titleArr) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}

- (FD_PickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [FD_PickerView pickerView];
        //地址选择器默认3列, 设置为2列
        _pickerView.pickerRow = 1;
    }
    return _pickerView;
}

- (SDCycleScrollView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[SDCycleScrollView alloc] init];
        [self.view addSubview:_bannerView];
    }
    return _bannerView;
}

- (HMSegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bannerView.frame), mainWidth, 44)];
        _segmentControl.sectionTitles = @[Localized(@"未使用"), Localized(@"已使用"), Localized(@"已过期")];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : mainColor};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = mainColor;
        _segmentControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor hexFloatColor:@"999999"],NSForegroundColorAttributeName,[UIFont systemFontOfSize:15],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        CGFloat x = 0;
        CGFloat y = CGRectGetMaxY(self.segmentControl.frame);
        CGFloat w = mainWidth;
        CGFloat h = mainHeight - y;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(x, y, w, h)];
        _scrollView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    return _scrollView;
}

- (FDCartTicketListController *)listVC1 {
    if (!_listVC1) {
        _listVC1= [[FDCartTicketListController alloc] initWithOrderType:ListTypeUnused];
        _listVC1.view.frame = CGRectMake(mainWidth*0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _listVC1;
}

- (FDCartTicketListController *)listVC2 {
    if (!_listVC2) {
        _listVC2= [[FDCartTicketListController alloc] initWithOrderType:ListTypeUsed];
        _listVC2.view.frame = CGRectMake(mainWidth*1, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _listVC2;
}

- (FDCartTicketListController *)listVC3 {
    if (!_listVC3) {
        _listVC3= [[FDCartTicketListController alloc] initWithOrderType:ListTypeOverdue];
        _listVC3.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _listVC3;
}

@end
