//
//  CartTicketListController.m
//  App3.0
//
//  Created by lichao on 2018/8/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDCartTicketListController.h"
#import "FDCartTicketHeader.h"
#import "FDCartTicketAlertView.h"

@interface FDCartTicketListController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FDCartTicketAlertView *alertView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, assign) ListType listType;//类型

@end

@implementation FDCartTicketListController

- (instancetype)initWithOrderType:(ListType)type {
    self = [super init];
    if (self) {
        self.listType = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];

    [self setUpUI];
}

#pragma mark ===== setUpUI
- (void)setUpUI {
    
    switch (self.listType) {
        case ListTypeUnused: {//未使用
        }
            break;
            
        case ListTypeUsed: {//已使用

        }
            break;
            
        case ListTypeOverdue: {//已过期

        }
            break;
        default:
            break;
    }
    
    [FDCartTicketCell registerClassCellWithTableView:self.tableView];
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 134;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 42;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
 
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        FDCartTicketHeader *headerV = [[FDCartTicketHeader alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 42)];
        
        return headerV;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDCartTicketCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDCartTicketCell identifier]];
    if (cell == nil) {
        cell = [[FDCartTicketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDCartTicketCell identifier]];
    }
    
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.listType = self.listType;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [[UIApplication sharedApplication].keyWindow addSubview:self.alertView];
}

#pragma mark ===== 懒加载
- (FDCartTicketAlertView *)alertView {
    if (!_alertView) {
        _alertView = [[FDCartTicketAlertView alloc] init];
    }
    return _alertView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = self.view.bounds;
        if (mainHeight == 812) {//适配iPhoneX
            frame.size.height -= (150+44+88);
        } else {
            frame.size.height -= (150+44+64);
        }
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;

        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
