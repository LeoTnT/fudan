//
//  FDVideoModel.h
//  App3.0
//
//  Created by lichao on 2017/5/19.
//  Copyright © 2017年 lichao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDVideoModel : NSObject

/** 宽度  */
@property (nonatomic, assign) CGFloat w;
/** 高度  */
@property (nonatomic, assign) CGFloat h;
/** 图片  */
@property (nonatomic, copy) NSString *img;
/** 姓名  */
@property (nonatomic, copy) NSString *name;
/** 描述  */
@property (nonatomic, copy) NSString *detail;

@end
