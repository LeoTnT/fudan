//
//  FDMineViewController.m
//  App3.0
//
//  Created by lichao on 2018/8/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMeController.h"
#import "UserTopCell.h"
#import "UserFormCell.h"
#import "UserServiceCell.h"
#import "FormsVC.h"
#import "UIViewController+JASidePanel.h"
#import "CollectionViewController.h"
#import "CollectionShopsViewController.h"
#import "HistoryViewController.h"
#import "PersonViewController.h"
#import "SetupViewController.h"
#import "UserModel.h"
#import "RefundWebViewController.h"
#import "OffLineWebViewController.h"

#import "FDCardTicketController.h"
#import "FDBindContoller.h"

@interface FDMeController ()<UITableViewDelegate, UITableViewDataSource, UserTopCellDelegate, UserFormCellDelegate, UserServiceCellDelegate>
{
    UIView *_qrBgView;
    LoginDataParser *_parser;//用户信息
    NSMutableArray *_numberAry;//各类订单数量
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *signInBtn;
@property (nonatomic, strong) UserParser *userInfo;
@property (strong, nonatomic) NSArray *dataArray;   // 功能按键

@end

@implementation FDMeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    _numberAry = [NSMutableArray array];
    self.navigationItem.title = @"我";
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _parser = [[UserInstance ShardInstnce] getUserInfo];
  
    self.dataArray = @[@[@{@"image":@"fd_myWallet",@"title":Localized(@"我的钱包")},
                         @{@"image":@"fd_zhongchu",@"title":Localized(@"众筹订单")},
                         @{@"image":@"fd_fubei",@"title":Localized(@"福呗")},
                         @{@"image":@"fd_fudai",@"title":Localized(@"收款记录")},
                         @{@"image":@"fd_awardDetail",@"title":Localized(@"奖金明细")}
                         ],
                       @[@{@"image":@"fd_setting",@"title":Localized(@"设置")},
                         ]
                       ];
    
    [self.view addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getOrderNum];
}

- (void)getOrderNum {
    _parser = [[UserInstance ShardInstnce] getUserInfo];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [HTTPManager getOrderStatusCountSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        
        NSArray *numArray = dic[@"data"];
        
        if (numArray.count>0) {
            _numberAry = [NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%@",[numArray firstObject] ],[NSString stringWithFormat:@"%@",numArray[1]],[NSString stringWithFormat:@"%@",numArray[2]],[NSString stringWithFormat:@"%@",numArray[3]], nil];
            NSLog(@"%@",_numberAry);
            NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2+self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }if (section == 1) {
        return 2;
    }
    NSArray *tempArr = self.dataArray[section-2];
    return tempArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 180+99+10;
    }else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            return 80;
        }else {
            return 44;
        }
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section >0) {
        return 12;
    }
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"userTopCell";
        UserTopCell *cell = (UserTopCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            cell = [[UserTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        cell.parser = _parser;
        
        [cell setTapAction:^(NSInteger index){
            
            if (index == 0) {//我的收藏
                CollectionViewController *collectController = [[CollectionViewController alloc] init];
                //            collectController.type = index;
                [self.navigationController pushViewController:collectController animated:YES];
            } else if (index == 1) {//我的足迹
                HistoryViewController *hisVC = [[HistoryViewController alloc] init];
                [self.navigationController pushViewController:hisVC animated:YES];
            } else {//购物车
                [self.navigationController pushViewController:[self getNameWithClass:@"CartVC"] animated:YES];
            }
        }];
        return cell;
    }else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            static NSString *CellIdentifier = @"userFormCell0";
            UserFormCell *cell = (UserFormCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UserFormCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.delegate = self;
            }
            cell.numberArray = _numberAry;
            return cell;
        }else if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"userFormCell1";
            UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
                cell.textLabel.text = Localized(@"商城订单");
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.detailTextLabel.text = @"查看全部已购宝贝";
                cell.detailTextLabel.textColor = COLOR_999999;
                cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
                
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 43.5, mainWidth-12, 0.5)];
                lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
                [cell addSubview:lineView];
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            return cell;
        }
        
    }

    static NSString *CellIdentifier = @"userAppCell1_1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 43.5, mainWidth-12, 0.5)];
        lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
        [cell addSubview:lineView];
    }
    
    NSDictionary *dic = self.dataArray[indexPath.section-2][indexPath.row];
    cell.imageView.image = [UIImage imageNamed:dic[@"image"]];
    cell.textLabel.text = dic[@"title"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row == 0) {
        FormsVC *formVC = [[FormsVC alloc] initWithFormType:FormTypeAll];
        formVC.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:formVC animated:YES];
    } else {
        [self appAreaClick:indexPath];
    }
}

#pragma mark - 点击事件
- (void)formAreaClick:(NSInteger)index {
    NSInteger newIndex = index;
    if (index == 4) {
        
        RefundWebViewController *adWeb=[[RefundWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (VUE_ON) {
            adWeb.urlStr =
            [NSString stringWithFormat:@"%@/wap/#/order/refund/list?device=%@",ImageBaseUrl,gui];
            adWeb.urlType =  WKWebViewURLRefundList;
        } else {
            //        adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
        }
        
        adWeb.navigationItem.title = @"退款/退货详情";
        [self.navigationController pushViewController:adWeb animated:YES];
    } else {
        
        FormsVC *formVC = [[FormsVC alloc] initWithFormType:(FormType)(newIndex+1)];
        formVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:formVC animated:YES];
    }
    
}

- (UIViewController * )getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

- (void)appAreaClick:(NSIndexPath *)indexPath {
    if (indexPath.section < 2) return;
    
    NSDictionary *dic = self.dataArray[indexPath.section-2][indexPath.row];
    NSString *titleStr = [NSString stringWithFormat:@"%@",dic[@"title"]];
    if ([titleStr isEqualToString:Localized(@"我的钱包")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"WalletVC"] animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"众筹订单")]) {
        NSString *urlStr = @"wap/#/crowdfund/order/list";
        [self pushToWebViewControllerWithUrl:urlStr title:Localized(@"众筹订单")];
    }else if ([titleStr isEqualToString:Localized(@"福呗")]) {
        FDBindContoller *bindVC = [[FDBindContoller alloc] init];
        [self.navigationController pushViewController:bindVC animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"收款记录")]) {
        NSString *urlStr = @"wap/#/forms";
        [self pushToWebViewControllerWithUrl:urlStr title:Localized(@"收款记录")];
    }else if ([titleStr isEqualToString:Localized(@"奖金明细")]) {
        
    }else if ([titleStr isEqualToString:Localized(@"设置")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"SetupViewController"] animated:YES];
    }
}

- (void)pushToWebViewControllerWithUrl:(NSString *)urlString title:(NSString *)title{
    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
    if (guid.length <= 0) {
        guid = @"app";
    }
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@?device=%@", ImageBaseUrl, urlString, guid];
    OffLineWebViewController *vc=[[OffLineWebViewController alloc] init];
    vc.urlStr = urlStr;
    vc.urlType = WKWebViewURLOffLine;
    vc.navigationItem.title = title;
    [self xs_pushViewController:vc];
}

#pragma mark - User Delegate
- (void)topAvatarClick {//个人资料
    PersonViewController *personVC = [[PersonViewController alloc] init];
    personVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:personVC animated:YES];
}

- (void)topRightNavClick{//个人资料
    PersonViewController *personVC = [[PersonViewController alloc] init];
    personVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:personVC animated:YES];
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-kFDTabbarHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            if (@available(iOS 11.0, *)) {
                _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            }
        }
#endif
    }
    return _tableView;
}

@end
