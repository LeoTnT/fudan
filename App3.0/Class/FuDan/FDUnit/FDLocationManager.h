//
//  FDLocationManager.h
//  LcTools
//
//  Created by 李超 on 2017/12/15.
//  Copyright © 2017年 lichao. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol FDLocationDelegate;

@interface FDLocationManager : NSObject

@property (nonatomic, weak) id<FDLocationDelegate> delegate;

//开始定位
- (void)beginUpdatingLocation;

@end

@protocol FDLocationDelegate <NSObject>

/**
 代理方法, 获取是否开启权限
 */
- (void)locationIsOpen:(BOOL)isOpen;

/**
 代理方法, 获取经纬度以及地方名

 @param location 获取经纬度
 @param placemark 获取地名
 */
- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark;

@end
