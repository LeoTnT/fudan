//
//  FDLocationManager.m
//  LcTools
//
//  Created by 李超 on 2017/12/15.
//  Copyright © 2017年 lichao. All rights reserved.
//

#import "FDLocationManager.h"

@interface FDLocationManager () <CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager *locationManager;

@property(nonatomic, assign) BOOL isFirstLocation;


@end

@implementation FDLocationManager

//开启定位服务
- (void)beginUpdatingLocation {
    
    //定位前先确定权限
    [self requestLocationAuthorization];
}

//开始定位
- (void)startLocation {
    [self.locationManager startUpdatingLocation];
}


//获取定位权限
- (void)requestLocationAuthorization {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusNotDetermined) {//用户未作出选择
        
        //判断info.plist文件知否配置相关配置
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }else if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }else {
//            NSLog(@"未配置定位相关配置文件");
        }
        
        self.isFirstLocation = YES;
        
        //用户未选择, 未获取权限
        if ([self.delegate respondsToSelector:@selector(locationIsOpen:)]) {
            [self.delegate locationIsOpen:NO];
        }
        
    }else if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)){//用户允许
        
        self.isFirstLocation = NO;
        
        //用户允许权限
        if ([self.delegate respondsToSelector:@selector(locationIsOpen:)]) {
            [self.delegate locationIsOpen:YES];
        }
        
        //开始定位
        [self startLocation];
    }else {//kCLAuthorizationStatusDenied 用户拒绝
        //定位不能用
//        NSLog(@"用户拒绝使用定位");
        
        self.isFirstLocation = NO;
        
        //用户拒绝
        if ([self.delegate respondsToSelector:@selector(locationIsOpen:)]) {
            [self.delegate locationIsOpen:NO];
        }
    }
}

#pragma mark -- locationDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    //获取新的位置
    CLLocation *newLocation = locations.lastObject;
    
    //经纬度
//    CLLocationDegrees longitude = newLocation.coordinate.longitude;
//    CLLocationDegrees latitude = newLocation.coordinate.latitude;
    
    //根据经纬度反向地理编译出地址信息
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    kFDWeakSelf;
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        if (placemarks.count > 0) {
            CLPlacemark *placemark = placemarks.firstObject;
            
            //存储位置信息
            //placemark.country;             国家
            //placemark.administrativeArea;  省份
            //placemark.locality;            城市
            //placemark.subLocality;         县区
            
            if ([weakSelf.delegate respondsToSelector:@selector(locationDidEndUpdatingLocation:placemark:)]) {
                [weakSelf.delegate locationDidEndUpdatingLocation:newLocation placemark:placemark];
            }
        }else {
            
            if ([weakSelf.delegate respondsToSelector:@selector(locationDidEndUpdatingLocation:placemark:)]) {
                [weakSelf.delegate locationDidEndUpdatingLocation:newLocation placemark:nil];
            }
        }
    
    }];
    
    [manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status != kCLAuthorizationStatusNotDetermined) {//用户做出了选择
        if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {//用户已经允许
            
            if (self.isFirstLocation) {
                [self startLocation];
            }
            
            //用户已经允许
            if ([self.delegate respondsToSelector:@selector(locationIsOpen:)]) {
                [self.delegate locationIsOpen:YES];
            }
        }
        if (status == kCLAuthorizationStatusDenied) {//用户已拒绝
           
//            NSLog(@"用户已拒绝,请前往设置打开定位");
            //用户已经拒绝
            if ([self.delegate respondsToSelector:@selector(locationIsOpen:)]) {
                [self.delegate locationIsOpen:NO];
            }
        }
    }
}

#pragma mark -- 懒加载
- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
        //设置定位精度
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //设置过滤器为无
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        
    }
    return _locationManager;
}


@end
