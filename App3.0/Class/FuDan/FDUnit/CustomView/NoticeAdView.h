//
//  NoticeAdCell.h
//  App3.0
//
//  Created by lichao on 2018/7/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeAdView : UITableViewCell

/**公告*/
@property (nonatomic,strong) UIView *noticeView;
/**公告标题图片*/
@property (nonatomic,strong) UIImageView *noticeImgView;

@property (nonatomic ,strong) NSArray *arr;

@property (nonatomic ,copy)void (^TapAction)(void);

@end
