//
//  HeadLineView.h
//  App3.0
//
//  Created by mac on 2017/12/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDHeadLineView : UIButton
@property (nonatomic,copy) void (^clickBlock)(NSInteger index);//第几个数据被点击

//数组内部数据需要是GBTopLineViewModel类型
- (void)setVerticalShowDataArr:(NSMutableArray *)dataArr;

//停止定时器(界面消失前必须要停止定时器否则内存泄漏)
- (void)stopTimer;
@end

@interface ZYJHeadLineModel : NSObject
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *title;
@end


