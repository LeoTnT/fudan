//
//  HeadLineView.m
//  App3.0
//
//  Created by mac on 2017/12/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FDHeadLineView.h"

@interface FDHeadLineView(){
    NSTimer *_timer;     //定时器
    int count;
    int flag; //标识当前是哪个view显示(currentView/hidenView)
    NSMutableArray *_dataArr;
}
@property (nonatomic,strong) UIView *currentView;   //当前显示的view
@property (nonatomic,strong) UIView *hidenView;     //底部藏起的view
@property (nonatomic,strong) UILabel *currentLabel;
@property (nonatomic,strong) UIButton *currentBtn;
@property (nonatomic,strong) UIButton *hidenBtn;
@property (nonatomic,strong) UILabel *hidenLabel;
@end

@implementation FDHeadLineView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    count = 0;
    flag = 0;
    
    self.layer.masksToBounds = YES;
    
    [self createCurrentView];
    [self createHidenView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dealTap:)];
    [self addGestureRecognizer:tap];
    //改进
    UILongPressGestureRecognizer*longPress=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(dealLongPress:)];
    [self addGestureRecognizer:longPress];
}

- (void)setVerticalShowDataArr:(NSMutableArray *)dataArr {
    count = 0;
    _dataArr = dataArr;
    NSLog(@"dataArr-->%@",dataArr);
    ZYJHeadLineModel *model = _dataArr[count];
    self.currentLabel.text = model.title;
    if (dataArr.count>1) {
        //创建定时器
        [self createTimer];
    } else  {
        if (dataArr.count==1) {
        }
    }
}

- (void)dealLongPress:(UILongPressGestureRecognizer*)longPress {
    
    if(longPress.state==UIGestureRecognizerStateEnded){
        
        _timer.fireDate=[NSDate distantPast];
        
        
    }
    if(longPress.state==UIGestureRecognizerStateBegan){
        
        _timer.fireDate=[NSDate distantFuture];
    }
}

- (void)dealTap:(UITapGestureRecognizer *)tap {
    self.clickBlock(count);
}

- (void)createTimer {
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dealTimer) userInfo:nil repeats:YES];
}

#pragma mark - 跑马灯操作
- (void)dealTimer {
    if (!_dataArr || _dataArr.count == 0) {
        return;
    }
    count++;
    if (count == _dataArr.count) {
        count = 0;
    }
    
    if (flag == 1) {
        ZYJHeadLineModel *currentModel = _dataArr[count];
        self.currentLabel.text = currentModel.title;
    }
    
    if (flag == 0) {
        ZYJHeadLineModel *hienModel = _dataArr[count];
        self.hidenLabel.text = hienModel.title;
    }
    
    
    if (flag == 0) {
        [UIView animateWithDuration:0.5 animations:^{
            self.currentView.frame = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            flag = 1;
            self.currentView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height);
        }];
        [UIView animateWithDuration:0.5 animations:^{
            self.hidenView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }else{
        
        [UIView animateWithDuration:0.5 animations:^{
            self.hidenView.frame = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            flag = 0;
            self.hidenView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.width);
        }];
        [UIView animateWithDuration:0.5 animations:^{
            self.currentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void)createCurrentView {
    ZYJHeadLineModel *model = _dataArr[count];
    
    self.currentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:self.currentView];
    
    //此处是类型按钮(不需要点击)
    self.currentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.currentBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [self.currentBtn setTitle:Localized(@"热卖") forState:UIControlStateNormal];
    self.currentBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    self.currentBtn.frame = CGRectMake(0, 17, 29, 15);
    self.currentBtn.layer.borderWidth = 1;
    self.currentBtn.layer.borderColor = mainColor.CGColor;
    self.currentBtn.layer.cornerRadius = 3;
    self.currentBtn.layer.masksToBounds = YES;
    [self.currentView addSubview:self.currentBtn];
    
    //内容标题
    self.currentLabel = [[UILabel alloc]initWithFrame:CGRectMake(29+6, 15, self.frame.size.width, 20)];
    self.currentLabel.text = model.title;
    self.currentLabel.textAlignment = NSTextAlignmentLeft;
    self.currentLabel.textColor = [UIColor hexFloatColor:@"333333"];
    self.currentLabel.font = [UIFont systemFontOfSize:14];
    [self.currentView addSubview:self.currentLabel];
}

- (void)createHidenView {
    self.hidenView = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    [self addSubview:self.hidenView];
    
    //此处是类型按钮(不需要点击)
    self.hidenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.hidenBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [self.hidenBtn setTitle:Localized(@"热卖") forState:UIControlStateNormal];
    self.hidenBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    self.hidenBtn.frame = CGRectMake(0, 17, 29, 15);
    self.hidenBtn.layer.borderWidth = 1;
    self.hidenBtn.layer.borderColor = mainColor.CGColor;
    self.hidenBtn.layer.cornerRadius = 3;
    self.hidenBtn.layer.masksToBounds = YES;
    [self.hidenView addSubview:self.hidenBtn];
    
    //内容标题
    self.hidenLabel = [[UILabel alloc]initWithFrame:CGRectMake(29+6, 15, self.frame.size.width, 20)];
    self.hidenLabel.text = @"";
    self.hidenLabel.textAlignment = NSTextAlignmentLeft;
    self.hidenLabel.textColor = [UIColor hexFloatColor:@"333333"];
    self.hidenLabel.font = [UIFont systemFontOfSize:14];
    [self.hidenView addSubview:self.hidenLabel];
}

#pragma mark - 停止定时器
- (void)stopTimer {
    //停止定时器
    //在invalidate之前最好先用isValid先判断是否还在线程中：
    if ([_timer isValid] == YES) {
        [_timer invalidate];
        _timer = nil;
    }
}

@end

@implementation ZYJHeadLineModel

@end
