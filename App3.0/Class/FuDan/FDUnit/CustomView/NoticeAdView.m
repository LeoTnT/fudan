//
//  NoticeAdCell.m
//  App3.0
//
//  Created by lichao on 2018/7/31.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "NoticeAdView.h"
#import "FDHeadLineView.h"

@interface NoticeAdView ()

@property (nonatomic, strong) FDHeadLineView *headLineView;

@end

@implementation NoticeAdView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self creatContentView];
    }
    return self;
}

- (void)setArr:(NSArray *)arr {
    if (arr && arr.count > 0) {
        _arr = arr;
        NSMutableArray *dataArr = [NSMutableArray array];
        for (int i = 0; i < arr.count; i++) {
            ZYJHeadLineModel *model = [[ZYJHeadLineModel alloc]init];
            //        model.type = @"";
            model.title = arr[i];
            [dataArr addObject:model];
        }
        if (dataArr.count == 0) {
            return;
        }
        [self.headLineView setVerticalShowDataArr:dataArr];
    }
}

- (void) creatContentView {
    
    //评评头条
    self.userInteractionEnabled = YES;
    self.noticeImgView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 15, 42, 20)];
    self.noticeImgView.image = [UIImage imageNamed:@"mall_new_notice"];
    [self addSubview:self.noticeImgView];
    
    UIView *lineV = [[UIView alloc] init];
    lineV.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
    lineV.frame = CGRectMake(42+12+15, 15, 1, 20);
    [self addSubview:lineV];
    
    //头条内容
    self.headLineView = [[FDHeadLineView alloc] initWithFrame:CGRectMake(42+12+15*2, 0, mainWidth-(100+20), 50)];
    [self addSubview:self.headLineView];
    self.headLineView.userInteractionEnabled = NO;
    [self.headLineView setClickBlock:^(NSInteger index) {
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        if (self.TapAction) {
            self.TapAction();
        }
    }];
    [self addGestureRecognizer:tap];
    
}

@end
