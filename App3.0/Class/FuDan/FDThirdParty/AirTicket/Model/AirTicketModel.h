//
//  AirTicketModel.h
//  App3.0
//
//  Created by xinshang on 2018/3/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirTicketModel : NSObject

@property (nonatomic,copy) NSString *dpt;// 出发机场三字码
@property (nonatomic,copy) NSString *arr;//  到达机场三字码
@property (nonatomic,copy) NSString *dptAirport;// 出发机场
@property (nonatomic,copy) NSString *dptTerminal;//  出发航站楼
@property (nonatomic,copy) NSString *arrAirport;//  到达机场
@property (nonatomic,copy) NSString *arrTerminal;//  到达航站楼
@property (nonatomic,copy) NSString *dptTime;//  出发时间
@property (nonatomic,copy) NSString *arrTime;//  到达时间
@property (nonatomic,copy) NSString *carrier;//  航司
@property (nonatomic,copy) NSString *flightNum;//  航班号
@property (nonatomic,copy) NSString *cabin;//  舱位
@property (nonatomic,copy) NSString *distance;//  航程
@property (nonatomic,copy) NSString *flightTimes;//  飞行时间
@property (nonatomic,copy) NSString *arf;//  机建
@property (nonatomic,copy) NSString *tof;//  燃油
@property (nonatomic,copy) NSString *planetype;//  机型
@property (nonatomic,copy) NSString *flightTypeFullName;//  机型全名
@property (nonatomic,assign) BOOL codeShare;// true表示共享,false表示非共享
@property (nonatomic,copy) NSString *actFlightNum;//  主飞航班
@property (nonatomic,assign) BOOL top;//  true表示经停,false表示无经停
@property (nonatomic,copy) NSString *stopsNum;//  经停次数
@property (nonatomic,copy) NSString *stopCityCode;//  经停城市三字码，如果有多个经停按英文逗号分隔
@property (nonatomic,copy) NSString *stopCityName;//  经停城市名称，如果有多个经停按英文逗号分隔
@property (nonatomic,copy) NSString *stopAirportCode;//  经停机场三字码，如果有多个经停按英文逗号分隔
@property (nonatomic,copy) NSString *stopAirportName;//  经停机场名称，如果有多个经停按英文逗号分隔
@property (nonatomic,copy) NSString *stopAirportFullName;//  经停机场全称，如果有多个经停按英文逗号分隔
@property (nonatomic,copy) NSString *barePrice;//  销售价
@property (nonatomic,copy) NSString *discount;//  折扣
@property (nonatomic,copy) NSString *tag;//  最低价Tag
@property (nonatomic,copy) NSString *bfPrice;//  头等商务最低包装价
@property (nonatomic,copy) NSString *bfBarePrice;//  头等商务最低裸票价bfTag 头等商务最低价Tag
@property (nonatomic,copy) NSString *bfTag;//  头等商务最低价Tag

@end
/*
 　　{
 　　　　　　"dpt":"PEK",
 　　　　　　"arr":"SHA",
 　　　　　　"dptAirport":"首都机场",
 　　　　　　"dptTerminal":"T3",
 　　　　　　"arrAirport":"虹桥机场",
 　　　　　　"arrTerminal":"T2",
 　　　　　　"dptTime":"13:30",
 　　　　　　"arrTime":"15:35",
 　　　　　　"carrier":"CA",
 　　　　　　"flightNum":"CA1517",
 　　　　　　"cabin":"U",
 　　　　　　"distance":"1178",
 　　　　　　"flightTimes":"2小时5分钟",
 　　　　　　"arf":50,
 　　　　　　"tof":0,
 　　　　　　"planetype":"773",
 　　　　　　"flightTypeFullName":"波音777(大)",
 　　　　　　"codeShare":false,
 　　　　　　"actFlightNum":"CA1517",
 　　　　　　"stop":false,
 　　　　　　"stopsNum":0,
 　　　　　　"stopCityCode":"",
 　　　　　　"stopCityName":"",
 　　　　　　"stopAirportCode":"",
 　　　　　　"stopAirportName":"",
 　　　　　　"stopAirportFullName":"",
 　　　　　　"barePrice":1026,
 　　　　　　"tag":"OPL4",
 　　　　　　"bfTag":"",
 　　　　　　"bfPrice":0,
 　　　　　　"bfBarePrice":0,
 　　　　　　"price":0,
 　　　　　　"discount":8.3,
 　　　　　　"meal":true,
 　　　　　　"minVppr":0,
 　　　　　　"flightQuotePrices":null
 　　　　},
 */
