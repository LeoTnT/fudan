//
//  AirTicketListCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketListCell.h"


@interface AirTicketListCell()

@property (nonatomic, strong) UIImageView *imgView;//
@property (nonatomic, strong) UIImageView *arrowImg;//
@property (nonatomic, strong) UILabel *from_timeLb;//出发站时间
@property (nonatomic, strong) UILabel *to_timeLb;//到达站时间

@property (nonatomic, strong) UILabel *priceLb;//价格
@property (nonatomic, strong) UILabel *typeLb;//票类型
@property (nonatomic, strong) UILabel *statusLb;//经济舱
@property (nonatomic, strong) UILabel *start_timeLb;//出发时间

@property (nonatomic,strong) NSDictionary * carrierDic;//航司

@property (nonatomic, strong) UILabel *from_stationLb;//出发站
@property (nonatomic, strong) UILabel *to_stationLb;//到达站

@property (nonatomic, strong) UIButton *orderBtn;//预定
@end

@implementation AirTicketListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =BG_COLOR;
    CGFloat space = 10.0;
    
    CGFloat bgViewH = 90.0;
    CGFloat labelW = (mainWidth-10)/2;
    CGFloat labelH = 25.0;
    CGFloat imgW = 40.0;
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = Color(@"FFFFFF");
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 2.5;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.bottom.mas_equalTo(self);
    }];
    
    
  
    
    
    //_from_timeLb
    _from_timeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:17] Radius:0];
    [bgView addSubview:_from_timeLb];
    [_from_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    //_arrowImg
    _arrowImg = [[UIImageView alloc] init];
    _arrowImg.image = [UIImage imageNamed:@"Air_list_arrow"];
    [bgView addSubview:_arrowImg];
    [_arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_timeLb.mas_right);
        make.centerY.mas_equalTo(_from_timeLb);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    //_to_timeLb
    _to_timeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:17] Radius:0];
    _to_timeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_to_timeLb];
    [_to_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_arrowImg.mas_right);
        make.top.mas_equalTo(_from_timeLb);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    
    
    
    //_from_stationLb
    _from_stationLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_from_stationLb];
    [_from_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_timeLb);
        make.top.mas_equalTo(_from_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    //_to_stationLb
    _to_stationLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_to_stationLb];
    [_to_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_to_timeLb);
        make.top.mas_equalTo(_from_stationLb);
        make.height.mas_equalTo(labelH);
    }];
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:17] Radius:0];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_from_timeLb);
        make.width.mas_equalTo(labelW*1.25);
        make.height.mas_equalTo(labelH);
    }];
    
    //_statusLb
    _statusLb = [self getLabelWithTextColor:Color(@"1A3C57") Font:[UIFont systemFontOfSize:14] Radius:0];
    _statusLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_statusLb];
    [_statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_priceLb);
        make.top.mas_equalTo(_from_stationLb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_imgView
    _imgView = [[UIImageView alloc] init];
    _imgView.image = [UIImage imageNamed:@"Air_order_img"];
    [bgView addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_stationLb);
        make.top.mas_equalTo(_from_stationLb.mas_bottom).offset(space/2);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    //_typeLb
    _typeLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_typeLb];
    [_typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_imgView.mas_right).offset(space/3);
        make.centerY.mas_equalTo(_imgView);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    
   
    
    _from_timeLb.text = @"13:10";
    _to_timeLb.text = @"15:20";
    
    _from_stationLb.text = @"上海虹桥";
    _to_stationLb.text = @"北京南";
    _typeLb.text = @"中国东方航空MU5552";
    _priceLb.text = @" 560";
    _statusLb.text = @"";
    //    _start_timeLb.text = @"出发时间:2018-03-20 17:42";
    
}

-(void)setModel:(AirTicketModel *)model
{
    _model = model;
    
        _from_timeLb.text = [NSString stringWithFormat:@"%@",model.dptTime];
        _to_timeLb.text = [NSString stringWithFormat:@"%@",model.arrTime];
    _from_stationLb.text = [NSString stringWithFormat:@"%@",model.dptAirport];
    _to_stationLb.text = [NSString stringWithFormat:@"%@",model.arrAirport];
        _priceLb.text = [NSString stringWithFormat:@" %@",model.barePrice];
//        _start_timeLb.text = [NSString stringWithFormat:@"出发时间:%@",model.train_date];
    
    _typeLb.text = [NSString stringWithFormat:@"%@%@    %@",[self getCarrierStr:model.carrier],model.flightNum,model.flightTypeFullName];
   
}

- (NSString *)getCarrierStr:(NSString *)str
{
    NSString *carrier = @"";
    if (!isEmptyString(str)) {
        carrier = [self.carrierDic objectForKey:str];
    }
    return carrier;

}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

-(NSDictionary *)carrierDic
{
    if (!_carrierDic) {
        _carrierDic =@{@"CA":@"国际航空",
                       @"CZ":@"南方航空",
                       @"PN":@"西部航空",
                       @"MU":@"东方航空",
                       @"MF":@"厦门航空",
                       @"SC":@"山东航空",
                       @"FM":@"上海航空",
                       @"ZH":@"深圳航空",
                       @"X2":@"新华航空",
                       @"JR":@"幸福航空",
                       @"3Q":@"云南航空",
                       @"UQ":@"新疆航空",
                       @"3U":@"四川航空",
                       @"Z2":@"中原航空",
                       @"WU":@"武汉航空",
                       @"G4":@"贵州航空",
                       @"HU":@"海南航空",
                       @"GP":@"通用航空",
                       @"3W":@"南京航空",
                       @"ZJ":@"浙江航空",
                       @"G8":@"长城航空",
                       @"FJ":@"福建航空",
                       @"9H":@"长安航空",
                       @"GJ":@"浙江长龙航空",
                       @"JD":@"首都航空",
                       @"HO":@"上海吉祥航空",
                       @"9C":@"上海春秋航空",
                       @"GS":@"天津航空",
                       @"KN":@"联合航空",
                       @"G5":@"华夏航空"};
    }
    return _carrierDic;
}


@end



