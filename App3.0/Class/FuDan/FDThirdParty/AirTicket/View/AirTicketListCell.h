//
//  AirTicketListCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AirTicketModel.h"

@interface AirTicketListCell : UITableViewCell
@property (nonatomic,strong) AirTicketModel * model;

@end
