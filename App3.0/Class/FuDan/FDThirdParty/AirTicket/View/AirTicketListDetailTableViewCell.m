//
//  AirTicketListDetailTableViewCell.m
//  App3.0
//
//  Created by mac on 2018/9/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketListDetailTableViewCell.h"

@interface AirTicketListDetailTableViewCell()
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *descLabel1;
@property (nonatomic, strong) UILabel *descLabel2;
@property (nonatomic, strong) UIButton *prepareButton;
@end

@implementation AirTicketListDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = BG_COLOR;
        UIView *backView = [UIView new];
        backView.backgroundColor = [UIColor whiteColor];
        backView.layer.cornerRadius = 5;
        backView.layer.masksToBounds = YES;
        [self.contentView addSubview:backView];
        
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(8);
            make.top.mas_equalTo(self.contentView);
            make.right.mas_equalTo(self.contentView).offset(-8);
            make.bottom.mas_equalTo(self.contentView).offset(-8);
        }];
        
        
        self.priceLabel = [BaseUITool labelWithTitle:@"￥820" textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:20] titleColor:Color(@"F78F15")];
        [backView addSubview:self.priceLabel];
        
        self.typeLabel = [BaseUITool labelWithTitle:@"商务舱" textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:14] titleColor:Color(@"1A3C57")];
        [backView addSubview:self.typeLabel];
        
        self.descLabel1 = [BaseUITool labelWithTitle:@"商务舱|实时出票|提前改期免费 >" textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:12] titleColor:COLOR_999999];
        [backView addSubview:self.descLabel1];
        
        self.descLabel2 = [BaseUITool labelWithTitle:@"托运行李额20KG" textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:12] titleColor:Color(@"1A3C57")];
        [backView addSubview:self.descLabel2];
        
        self.prepareButton = [BaseUITool buttonWithTitle:@"预定" titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:15] superView:backView];
        self.prepareButton.backgroundColor = Color(@"F78F15");
        self.prepareButton.layer.cornerRadius = 4;
        self.prepareButton.layer.masksToBounds = YES;
        
        UILabel *line = [UILabel new];
        line.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:line];
       
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.top.mas_equalTo(17);
             make.right.mas_equalTo(self.typeLabel.mas_left).offset(-5);
        }];
        
        [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.priceLabel);
            make.right.mas_equalTo(self.prepareButton.mas_left).offset(-15);
        }];
        
        [self.prepareButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(45, 45));
            make.centerY.mas_equalTo(backView);
            make.right.mas_equalTo(backView).offset(-10);
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(backView);
            make.height.mas_equalTo(1);
            make.left.mas_equalTo(self.priceLabel);
            make.right.mas_equalTo(self.prepareButton.mas_left).offset(-40);
        }];
        
        [self.descLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(line.mas_bottom).offset(10);
            make.left.mas_equalTo(self.priceLabel);
            make.right.mas_equalTo(self.prepareButton.mas_left).offset(-10);
        }];
        
        [self.descLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(backView).offset(-10);
            make.left.mas_equalTo(self.priceLabel);
            make.right.mas_equalTo(self.prepareButton.mas_left).offset(-10);
        }];
        
    }
    
    return self;
}

@end
