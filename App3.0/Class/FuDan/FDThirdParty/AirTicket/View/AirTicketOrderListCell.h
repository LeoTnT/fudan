//
//  AirTicketOrderListCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AirTicketOrderModel.h"
@interface AirTicketOrderListCell : UITableViewCell
@property (nonatomic,strong) AirTicketOrderModel * model;

@end
