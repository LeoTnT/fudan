//
//  AirTicketListDetailTimeCell.m
//  App3.0
//
//  Created by xinshang on 2018/4/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketListDetailTimeCell.h"

@interface AirTicketListDetailTimeCell()

@property (nonatomic, strong) UIImageView *imgView;//
@property (nonatomic, strong) UILabel *from_timeLb;//出发站时间
@property (nonatomic, strong) UILabel *to_timeLb;//到达站时间
@property (nonatomic, strong) UIImageView *arrowImg;//


@property (nonatomic, strong) UILabel *from_stationLb;//出发站
@property (nonatomic, strong) UILabel *to_stationLb;//到达站

@property (nonatomic, strong) UILabel *descLb;//准点率 67%|有餐饮 |中型机

@property (nonatomic, strong) UILabel *typeLb;//票类型中联航KN5922 3月10日
@property (nonatomic,strong) NSDictionary * carrierDic;//航司

@end

@implementation AirTicketListDetailTimeCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =BG_COLOR;
    CGFloat space = 10.0;
    
    CGFloat bgViewH = 120.0;
    CGFloat labelW = (mainWidth-10)/2;
    CGFloat labelH = 19;
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = Color(@"FFFFFF");
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 3.5;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-space);
        make.bottom.mas_equalTo(self);
    }];
    
    
    //_imgView
    _imgView = [[UIImageView alloc] init];
    _imgView.image = [UIImage imageNamed:@"Air_order_img"];
    [bgView addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    //_typeLb
    _typeLb = [self getLabelWithTextColor:Color(@"333333") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_typeLb];
    [_typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(space*2);
        make.centerY.mas_equalTo(_imgView);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    //_from_timeLb
    _from_timeLb = [self getLabelWithTextColor:Color(@"333333") Font:[UIFont systemFontOfSize:20] Radius:0];
    _from_timeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_from_timeLb];
   
    CGFloat imgW = 40.0;

    //_arrowImg
    _arrowImg = [[UIImageView alloc] init];
    _arrowImg.image = [UIImage imageNamed:@"Air_list_arrow"];
    [bgView addSubview:_arrowImg];
    [_arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_from_timeLb.mas_right);
        make.centerY.centerX.mas_equalTo(bgView);
//        make.width.mas_equalTo(imgW*1.3);
//        make.height.mas_equalTo(imgW*1.3);
    }];
    [_from_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_typeLb);
        make.bottom.mas_equalTo(_arrowImg.mas_centerY);
        make.right.mas_equalTo(_arrowImg.mas_left).offset(-space/2);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    
    //_to_timeLb
    _to_timeLb = [self getLabelWithTextColor:Color(@"333333") Font:[UIFont systemFontOfSize:20] Radius:0];
    _to_timeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_to_timeLb];
    [_to_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_arrowImg.mas_right).offset(space/2);
        make.top.mas_equalTo(_from_timeLb);
        make.right.mas_equalTo(bgView).offset(-space*2);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    
    
    
    //_from_stationLb
    _from_stationLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    _from_stationLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_from_stationLb];
    [_from_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_from_timeLb);
        make.top.mas_equalTo(_from_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    //_to_stationLb
    _to_stationLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    _to_stationLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_to_stationLb];
    [_to_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_to_timeLb);
        make.top.mas_equalTo(_from_stationLb);
        make.height.mas_equalTo(labelH);
    }];
    
  

    //_descLb
    _descLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    _descLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_descLb];
    [_descLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.bottom.mas_equalTo(bgView).offset(-10);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    
    
    _from_timeLb.text = @"13:10";
    _to_timeLb.text = @"15:20";
    
    _from_stationLb.text = @"上海虹桥";
    _to_stationLb.text = @"北京南";
    _typeLb.text = @"中国东方航空MU5552";
    _descLb.text = @"准点率67% | 有餐饮 | 中型机";
    
}

-(void)setModel:(AirTicketModel *)model
{
    _model = model;
    
    _from_timeLb.text = [NSString stringWithFormat:@"%@",model.dptTime];
    _to_timeLb.text = [NSString stringWithFormat:@"%@",model.arrTime];
    _from_stationLb.text = [NSString stringWithFormat:@"%@",model.dptAirport];
    _to_stationLb.text = [NSString stringWithFormat:@"%@",model.arrAirport];
//    _start_timeLb.text = [NSString stringWithFormat:@"出发时间:%@",model.train_date];
    
    _typeLb.text = [NSString stringWithFormat:@"%@%@    %@",[self getCarrierStr:model.carrier],model.flightNum,model.flightTypeFullName];
    
//    _descLb.text = [NSString stringWithFormat:@"准点率67% | 有餐饮 | 中型机",model.barePrice];

    
}

- (NSString *)getCarrierStr:(NSString *)str
{
    NSString *carrier = @"";
    if (!isEmptyString(str)) {
        carrier = [self.carrierDic objectForKey:str];
    }
    return carrier;
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

-(NSDictionary *)carrierDic
{
    if (!_carrierDic) {
        _carrierDic =@{@"CA":@"国际航空",
                       @"CZ":@"南方航空",
                       @"PN":@"西部航空",
                       @"MU":@"东方航空",
                       @"MF":@"厦门航空",
                       @"SC":@"山东航空",
                       @"FM":@"上海航空",
                       @"ZH":@"深圳航空",
                       @"X2":@"新华航空",
                       @"JR":@"幸福航空",
                       @"3Q":@"云南航空",
                       @"UQ":@"新疆航空",
                       @"3U":@"四川航空",
                       @"Z2":@"中原航空",
                       @"WU":@"武汉航空",
                       @"G4":@"贵州航空",
                       @"HU":@"海南航空",
                       @"GP":@"通用航空",
                       @"3W":@"南京航空",
                       @"ZJ":@"浙江航空",
                       @"G8":@"长城航空",
                       @"FJ":@"福建航空",
                       @"9H":@"长安航空",
                       @"GJ":@"浙江长龙航空",
                       @"JD":@"首都航空",
                       @"HO":@"上海吉祥航空",
                       @"9C":@"上海春秋航空",
                       @"GS":@"天津航空",
                       @"KN":@"联合航空",
                       @"G5":@"华夏航空"};
    }
    return _carrierDic;
}


@end




