//
//  AirTicketListDetailheadView.m
//  App3.0
//
//  Created by xinshang on 2018/3/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketListDetailheadView.h"

@interface AirTicketListDetailheadView()
@property (nonatomic, strong) UIButton *backBtn;//返回
@property (nonatomic, strong) UILabel *from_station_namelb; /*出发车站名*/
@property (nonatomic, strong) UILabel *to_station_namelb; /*到达车站名*/
@end


@implementation AirTicketListDetailheadView


-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = mainColor;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 44;
    CGFloat labelW = 90.0;
    CGFloat labelH = 30.0;
    CGFloat imgW = 42.0;
    
    
    //_backBtn
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[UIImage imageNamed:@"Train_back1"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_backBtn];
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*1.1);
        make.top.mas_equalTo(space*1.5);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    
    UIImageView *arrowImg = [[UIImageView alloc] init];
    arrowImg.image = [UIImage imageNamed:@"Train_arrow"];
    [self addSubview:arrowImg];
    [arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(_backBtn);
        make.width.mas_equalTo(imgW*0.8);
        make.height.mas_equalTo(imgW*0.8);
    }];
    
    
    self.from_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:16] Radius:0];
    [self addSubview:self.from_station_namelb];
    [self.from_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImg.mas_left).offset(-2.5);
        make.top.mas_equalTo(arrowImg);
        make.height.mas_equalTo(imgW);
    }];
    
    //_endLb
    self.to_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:16] Radius:0];
    [self addSubview:self.to_station_namelb];
    [self.to_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(arrowImg.mas_right).offset(2.5);
        make.top.mas_equalTo(arrowImg);
        make.height.mas_equalTo(imgW);
    }];
    
   
}
#pragma mark --btnAction----

- (void)backBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(backBtnClick:)]) {
        [self.delegate backBtnClick:sender];
    }
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

-(void)setFrom_station:(NSString *)from_station {
    _from_station = from_station;
    if (isEmptyString(from_station)) {
        from_station = @"--";
    }
    self.from_station_namelb.text = [NSString stringWithFormat:@"%@",from_station];
}

-(void)setTo_station:(NSString *)to_station {
    _to_station = to_station;
    if (isEmptyString(to_station)) {
        to_station = @"--";
    }
    self.to_station_namelb.text = [NSString stringWithFormat:@"%@",to_station];
}

@end
