//
//  AirTicketSelectedVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@interface AirTicketSelectedVC : XSBaseTableViewController

@property (nonatomic,copy) void(^selCityBlock)(TrainCityModels *model);
@property (nonatomic, strong) NSArray *allArr;

@end
