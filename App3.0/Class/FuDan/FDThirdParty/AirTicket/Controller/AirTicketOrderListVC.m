//
//  AirTicketOrderListVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketOrderListVC.h"
#import "AirTicketOrderListCell.h"
#import "TrainOrderDetialVC.h"

@interface AirTicketOrderListVC ()
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面

@end

@implementation AirTicketOrderListVC
- (instancetype)initWithTrainOrder:(TrainOrderType)type
{
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self SetUpUI];
}



- (void)SetUpUI
{
    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    __weak __typeof__(self) wSelf = self;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 1;
        [wSelf getOrderList];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf getOrderList];
    }];
    [self.tableView.mj_header beginRefreshing];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    
    
    
}

- (NSString *)getStatusStr
{
    NSString *status = @"";
    switch (self.type) {
        case TrainOrderAll:
            status = @"";
            break;
        case TrainOrderWaitPay:
            status = @"2";
            break;
        case TrainOrderFinish:
            status = @"4";
            break;
        default:
            break;
    }
    return status;
}
//订单记录查询
- (void)getOrderList
{
    
    
    [self tableViewEndRefreshing];
//    if (self.page == 1) {
//        [self.listArr removeAllObjects];
//    }
//    [self.tableView.mj_footer resetNoMoreData];
//    __weak __typeof__(self) wSelf = self;
//    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager train_QueryAllOrderWithPage:[NSString stringWithFormat:@"%ld",self.page]
//                                      status:[self getStatusStr]
//                                     success:^(NSDictionary *dic, resultObject *state)
//     {
//         [self tableViewEndRefreshing];
//         if (state.status) {
//             NSArray *array = dic[@"data"][@"data"];
//             
//             if (array.count >0) {
//                 [self.listArr addObjectsFromArray:[TrainOrderModel mj_objectArrayWithKeyValuesArray:array]];
//             }else{
//                 if (self.page >0) {
//                     [self.tableView.mj_footer endRefreshingWithNoMoreData];
//                 }
//             }
//             
//             if (self.listArr.count >0) {
//                 [self hideDefaultView];
//             }else {
//                 [self showDefaultView];
//             }
//         }else{
//             Alert(state.info);
//             [self showDefaultView];
//         }
//         [wSelf.tableView reloadData];
//     } fail:^(NSError *error) {
//         [self tableViewEndRefreshing];
//         self.defaultView.hidden = NO;
//         self.tableView.hidden = YES;
//         Alert(NetFailure);
//         
//     }];
    
}
#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return self.listArr.count;
    return 5;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AirTicketOrderListCell";
    AirTicketOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AirTicketOrderListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
    }
    if (self.listArr.count >0) {
        cell.model = self.listArr[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TrainOrderDetialVC *vc =[[TrainOrderDetialVC alloc] init];
    if (self.listArr.count >0) {
        vc.model = self.listArr[indexPath.row];
    }
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = BG_COLOR;
    return view;
}
-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}


- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - lazy loadding
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = Localized(@"暂无更多记录");
        
    }
    return _defaultView;
}

- (void)showDefaultView
{
    self.defaultView.hidden = NO;
    self.tableView.hidden = YES;
}
- (void)hideDefaultView
{
    self.defaultView.hidden = YES ;
    if (self.tableView.hidden) {
        self.tableView.hidden = NO;
    }
}


@end

