//
//  AirTicketOrderListVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainOrderModel.h"

@interface AirTicketOrderListVC : XSBaseTableViewController
@property (nonatomic,assign) TrainOrderType type;
- (instancetype)initWithTrainOrder:(TrainOrderType)type;
- (void)getOrderList;
@end
