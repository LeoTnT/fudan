//
//  AirTicketHomeVC.m
//  App3.0
//
//  Created by 李超 on 2018/3/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketHomeVC.h"
#import "FSCalenderSelectedViewController.h"
#import "AirTicketSelectedVC.h"
#import "AirTicketListVC.h"
#import "AirTicketOrderVC.h"

@interface AirTicketHomeVC ()<SDCycleScrollViewDelegate>

@property (nonatomic,strong) UIButton * backBtn;//返回按钮
@property (nonatomic,strong) UIButton * orderBtn;//订单
@property (nonatomic, strong) SDCycleScrollView *adAdvier;// 顶部录播图
@property (nonatomic, strong) UIButton *selectedBtn;// 选中的button

@property (nonatomic, strong) NSMutableArray *buttonArr;//
@property (nonatomic, strong) UIView *lineView;//

@property (nonatomic,strong) UIView * checkView;//背景视图 ||
@property (nonatomic,strong) UITextField * startTF;//出发站
@property (nonatomic,strong) UITextField * endTF;//到达站
@property (nonatomic,strong) UIButton * exchangeBtn;//交换
@property (nonatomic,strong) UIButton * dateBtn1;//日期button 左边
@property (nonatomic,strong) UIButton * dateBtn2;//日期button 右边
@property (nonatomic,strong) UILabel * dateLb1;//日期左边
@property (nonatomic,strong) UILabel * dateLb2;//日期右边

@property(weak, nonatomic) UIView *childBgV;
@property(weak, nonatomic) UIView *babyBgV;

@property (nonatomic,strong) UIButton * childBtn;//儿童
@property (nonatomic,strong) UIButton * babyBtn;//婴儿

@property (nonatomic,strong) UIButton * checkBtn;//查询

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic,copy) NSDate * selDate1;//选中的日期
@property (nonatomic,copy) NSDate * selDate2;//选中的日期2
@property (nonatomic,strong) TrainCityModels * selStartModel;//选中出发站
@property (nonatomic,strong) TrainCityModels * selEndModel;//选中到达站

@property(strong, nonatomic) NSString *hasChild;//是否携带儿童
@property(strong, nonatomic) NSString *hasBaby;//是否携带婴儿

@property (nonatomic,strong) UIView * bgView1;//
//按钮背景图片
@property(strong, nonatomic) UIView *bgV;
//底部背景图片
@property(strong, nonatomic) UIView *bottomV;

@end

@implementation AirTicketHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"飞机票";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
//    [self setUpBtnTitle];
    
    //默认设置
    self.hasChild = @"不携带儿童";
    self.hasBaby = @"携带婴儿";
}

#pragma mark -- setUpUI
- (void)setUpUI{
    
    CGFloat space = 10.0;
    CGFloat lbH = 35.0;
    CGFloat imgW = 35.0;
    CGFloat bgH = 248.0;
    CGFloat bgview1H = 55.0;
    
    //轮播图
    [self.view addSubview:self.adAdvier];
//        self.adAdvier.imageURLStringsGroup = @[@"img1",@"img2"];
    self.adAdvier.localizationImageNamesGroup = @[@"A1_bg1",@"D1_bgImg"];

    //返回按钮
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backBtn setImage:[UIImage imageNamed:@"Train_back"] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:self.backBtn];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(space*3);
        make.left.mas_equalTo(space*1.2);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    //订单按钮
    self.orderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.orderBtn setImage:[UIImage imageNamed:@"Train_order"] forState:UIControlStateNormal];
    [self.orderBtn addTarget:self action:@selector(orderBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:self.orderBtn];
    [self.orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.right.mas_equalTo(-space*1.2);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    //中间圆角背景view
    _checkView = [[UIView alloc]init];
    _checkView.backgroundColor = [UIColor whiteColor];
    _checkView.layer.masksToBounds = YES;
    _checkView.layer.cornerRadius = 5;
    [self.view addSubview:_checkView];
    [_checkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*1.3);
        make.right.mas_equalTo(-space*1.3);
        make.top.mas_equalTo( self.adAdvier.mas_bottom).offset(-45);
        make.height.mas_equalTo(bgH+44);
    }];
    
    //标题背景view
    _bgV = [[UIView alloc]init];
    _bgV.backgroundColor = [UIColor whiteColor];
    [_checkView addSubview:_bgV];
    [_bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(5);
        make.height.mas_equalTo(10);
    }];
    
    //下半部分背景view
    _bottomV = [[UIView alloc] init];
    _bottomV.backgroundColor = [UIColor whiteColor];
    [_checkView addSubview:_bottomV];
    [_bottomV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_bgV.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(bgH);
    }];
    
    //出发地/目的地  背景view
    _bgView1 = [[UIView alloc]init];
    
    _bgView1.backgroundColor = [UIColor whiteColor];

    [_bottomV addSubview:_bgView1];
    [_bgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(bgview1H);
    }];
    
    
    //_startTF
    _startTF = [[UITextField alloc] init];
    _startTF.textAlignment = NSTextAlignmentLeft;
    _startTF.placeholder = @"出发站";
    _startTF.font = [UIFont systemFontOfSize:17];
    _startTF.enabled = NO;
    [_bgView1 addSubview:_startTF];
    [_startTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH);
    }];

    
    //_endTF
    _endTF = [[UITextField alloc] init];
    _endTF.placeholder = @"到达站";
    _endTF.enabled = NO;
    _endTF.textAlignment = NSTextAlignmentRight;
    _endTF.font = [UIFont systemFontOfSize:17];
    [_bgView1 addSubview:_endTF];
    [_endTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH);
    }];
    
    // 出发站
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startBtn.tag = 101;
    [startBtn addTarget:self action:@selector(endOrStartBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:startBtn];
    [startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH*1.2);
    }];
    
    // 到达站
    UIButton *endtBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    endtBtn.tag = 102;
    [endtBtn addTarget:self action:@selector(endOrStartBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:endtBtn];
    [endtBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH*1.2);
    }];
    
    
    // 交换按钮
    self.exchangeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.exchangeBtn setImage:[UIImage imageNamed:@"Train_exchange"] forState:UIControlStateNormal];
    [self.exchangeBtn addTarget:self action:@selector(exchangeBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:self.exchangeBtn];
    [self.exchangeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(_bgView1);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(35);
    }];
    //线1
    UIView *_line1 = [UIView new];
    [_bottomV addSubview:_line1];
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_bgView1.mas_bottom).offset(-0.8);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(0.8);
        
    }];
    _line1.backgroundColor = BG_COLOR;
    
    
    //_dateLb_左边
    _dateLb1 = [[UILabel alloc] init];
    _dateLb1.font = [UIFont systemFontOfSize:17];
    [_bottomV addSubview:_dateLb1];
    [_dateLb1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_line1).offset(space);
        make.width.mas_equalTo((mainWidth-24)/2);
        make.height.mas_equalTo(lbH);
    }];
    //日期button_左边
    self.dateBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dateBtn1.tag = 2331;
    [self.dateBtn1 addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bottomV addSubview:self.dateBtn1];
    [self.dateBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_line1).offset(space);
        make.width.mas_equalTo((mainWidth-24)/2);
        make.height.mas_equalTo(lbH);
    }];
    
    //_dateLb_右边
    _dateLb2 = [[UILabel alloc] init];
    _dateLb2.textAlignment = NSTextAlignmentRight;
    _dateLb2.font = [UIFont systemFontOfSize:17];
    [_bottomV addSubview:_dateLb2];
    [_dateLb2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_line1).offset(space);
        make.right.mas_equalTo(self.checkView).offset(-space);
        make.width.mas_equalTo((mainWidth-24)/2);
        make.height.mas_equalTo(lbH);
    }];
    
    //日期button_右边
    self.dateBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dateBtn2.tag = 2332;
    [self.dateBtn2 addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bottomV addSubview:self.dateBtn2];
    [self.dateBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_line1).offset(space);
        make.right.mas_equalTo(self.checkView).offset(-space);
        make.width.mas_equalTo((mainWidth-24)/2);
        make.height.mas_equalTo(lbH);
    }];
    
    _dateLb2.hidden = YES;
    _dateBtn2.hidden = YES;
    
    //线1
    UIView *_line2 = [UIView new];
    [_bottomV addSubview:_line2];
    [_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_dateLb1.mas_bottom).offset(4);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(0.8);
        
    }];
    _line2.backgroundColor = BG_COLOR;
    
    //bgView2
    UIView *bgView2 = [[UIView alloc]init];
    [_bottomV addSubview:bgView2];
    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_line2.mas_bottom);
        make.height.mas_equalTo(bgview1H);
    }];
    /********************** (携带儿童) **********************/
    //按钮背景图
    UIView *v1 = [[UIView alloc] init];
    v1.backgroundColor = [UIColor whiteColor];
    
    self.childBgV = v1;
    [bgView2 addSubview:v1];
    [v1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(bgView2);
    }];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(childBtnClick)];
    [v1 addGestureRecognizer:tap1];
    
    // 携带儿童
    self.childBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.childBtn setImage:[UIImage imageNamed:@"A3_noselected"] forState:UIControlStateNormal];
    [self.childBtn setImage:[UIImage imageNamed:@"A3_selected"] forState:UIControlStateSelected];
    [self.childBtn addTarget:self action:@selector(childBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [v1 addSubview:self.childBtn];
    [self.childBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v1);
        make.centerY.mas_equalTo(v1);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = @"携带儿童";
    label1.font = [UIFont systemFontOfSize:15];
    label1.textColor = [UIColor hexFloatColor:@"111111"];
    [v1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(v1).offset(10);
        make.left.equalTo(self.childBtn.mas_right).offset(5);
        make.right.equalTo(v1);
        make.height.mas_equalTo(15);
    }];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = @"2-12岁";
    label2.font = [UIFont systemFontOfSize:13];
    label2.textColor = [UIColor hexFloatColor:@"717171"];
    [v1 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(v1).offset(-10);
        make.left.equalTo(self.childBtn.mas_right).offset(5);
        make.right.equalTo(v1);
        make.height.mas_equalTo(15);
    }];
    
    /********************** (携带婴儿) **********************/
    //按钮背景图
    UIView *v2 = [[UIView alloc] init];
    v2.backgroundColor = [UIColor whiteColor];
    self.babyBgV = v2;
    [bgView2 addSubview:v2];
    [v2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v1.mas_right);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(bgView2);
    }];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(babyBtnClick)];
    [v2 addGestureRecognizer:tap2];
    
    // 携带婴儿
    self.babyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.babyBtn setImage:[UIImage imageNamed:@"A3_noselected"] forState:UIControlStateNormal];
    [self.babyBtn setImage:[UIImage imageNamed:@"A3_selected"] forState:UIControlStateSelected];
    [self.babyBtn addTarget:self action:@selector(babyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [v2 addSubview:self.babyBtn];
    [self.babyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v2);
        make.centerY.mas_equalTo(v2);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.text = @"携带婴儿";
    label3.font = [UIFont systemFontOfSize:15];
    label3.textColor = [UIColor hexFloatColor:@"111111"];
    [v2 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(v2).offset(10);
        make.left.equalTo(self.babyBtn.mas_right).offset(5);
        make.right.equalTo(v2);
        make.height.mas_equalTo(15);
    }];

    UILabel *label4 = [[UILabel alloc] init];
    label4.text = @"14天-2岁";
    label4.font = [UIFont systemFontOfSize:13];
    label4.textColor = [UIColor hexFloatColor:@"717171"];
    [v2 addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(v2).offset(-10);
        make.left.equalTo(self.babyBtn.mas_right).offset(5);
        make.right.equalTo(v2);
        make.height.mas_equalTo(15);
    }];
    
    
    // 查询按钮
    self.checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkBtn setTitle:@"开始搜索" forState:UIControlStateNormal];
    [self.checkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.checkBtn addTarget:self action:@selector(checkBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    
    self.checkBtn.layer.masksToBounds = YES;
    self.checkBtn.layer.cornerRadius = 4;
    self.checkBtn.backgroundColor = mainColor;
    [_bottomV addSubview:self.checkBtn];
    [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView2.mas_bottom).offset(space*1.4);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(bgview1H*0.9);
    }];
    
    //默认显示日期
    [self setDateLbTextWithDate:self.selDate1 button:self.dateBtn1];
    [self setDateLbTextWithDate:self.selDate2 button:self.dateBtn2];
}

/** 添加标题按钮 */
- (void)setUpBtnTitle {
    
    NSArray *btnArr = @[@"单程", @"往返"];
    
    CGFloat btnW = (mainWidth-24)/btnArr.count;
    CGFloat btnH = 39;
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    
    for (int i = 0; i < btnArr.count; i++) {
        //添加按钮
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = [UIFont systemFontOfSize:19];
        [btn setTitleColor:[UIColor hexFloatColor:@"1A3C57"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor hexFloatColor:@"6F7588"] forState:UIControlStateSelected];
        [btn setTitle:btnArr[i] forState:UIControlStateNormal];
        
        //给按钮绑定tag
        btn.tag = i;
        
        //设置按钮位置
        btnX = i * btnW;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i == 0) {
            
            //添加下划线
            UIView *lineView = [[UIView alloc] init];
            lineView.backgroundColor = [UIColor hexFloatColor:@"1A3C57"];
            lineView.frame = CGRectMake(btnX, CGRectGetMaxY(btn.frame), 30, 2);
            lineView.xs_centerX = btn.xs_centerX;
            
            self.lineView = lineView;
            [self.bgV addSubview:lineView];
            
            //默认点击第一个
            [self btnClick:btn];
        }
        
        [self.buttonArr addObject:btn];
        [self.bgV addSubview:btn];
    }
}

#pragma mark -- 标题按钮点击事件
- (void)btnClick:(UIButton *)button {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.lineView.xs_centerX = button.xs_centerX;
    }];
    
    self.selectedBtn.selected = !self.selectedBtn.selected;
    button.selected = !button.selected;
    if (button.tag == 0) {//单程
        
        self.childBgV.hidden = NO;
        self.babyBgV.hidden = NO;
        self.dateLb2.hidden = YES;
        self.dateBtn2.hidden = YES;
        
        self.bottomV.hidden = NO;
        
    }else if (button.tag == 1) {//往返
        
        self.childBgV.hidden = NO;
        self.babyBgV.hidden = NO;
        self.dateLb2.hidden = NO;
        self.dateBtn2.hidden = NO;
        
        self.bottomV.hidden = NO;
    }
    
    self.selectedBtn = button;
}

#pragma mark -- 婴儿/儿童点击事件
- (void)childBtnClick {
    
    self.childBtn.selected = !self.childBtn.selected;
   
    if (self.childBtn.selected) {//选中状态
        self.hasChild = @"携带儿童";
    }else {
        self.hasChild = @"不携带儿童";
    }
}

- (void)babyBtnClick {
    
    self.babyBtn.selected = !self.babyBtn.selected;

    if (self.babyBtn.selected) {//选中状态
        self.hasBaby = @"携带婴儿";
    }else {
        self.hasBaby = @"不携带婴儿";
    }
}

#pragma mark -- 导航栏点击事件
//订单
- (void)orderBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    AirTicketOrderVC *vc =[[AirTicketOrderVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
//返回
- (void)backBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-----ButtonAction
//开始搜索
- (void)checkBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSLog(@"出发站:%@, 终点站:%@, 出发日期:%@, 返程日期:%@, %@, %@", [NSString stringWithFormat:@"%@",self.selStartModel.name], [NSString stringWithFormat:@"%@",self.selEndModel.name], self.selDate1, self.selDate2, self.hasChild, self.hasBaby);
    
    
        self.dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate1];
        if (isEmptyString(dateStr)) {
            Alert(@"请选择日期");
            return;
        }
            if (isEmptyString(self.selStartModel.code)) {
                Alert(@"请选择出发站");
                return;
            }
            if (isEmptyString(self.selEndModel.code)) {
                Alert(@"请选择到达站");
                return;
            }
    
        AirTicketListVC *vc =[[AirTicketListVC alloc] init];
        vc.selStartModel = self.selStartModel;
        vc.selEndModel = self.selEndModel;
        vc.from_station = [NSString stringWithFormat:@"%@",self.selStartModel.name];
        vc.to_station = [NSString stringWithFormat:@"%@",self.selEndModel.name];
        vc.selDate = self.selDate1;
        [self.navigationController pushViewController:vc animated:YES];
    
}




//设置日期显示
- (void)setDateLbTextWithDate:(NSDate *)date button:(UIButton *)button
{
    NSString *dateStr = [XSTool dateStrStringFromDate:date];
    NSString *weekStr = [XSTool weekdayStringFromDate:date];
    NSString *textStr = [NSString stringWithFormat:@"%@ %@",dateStr,weekStr];
    NSMutableAttributedString *attributeStr = [self setStringWithStr:textStr RangStr:[NSString stringWithFormat:@"%@",weekStr]];
    
    if (button.tag == 2331) {//左边日期
        
        _dateLb1.attributedText=attributeStr;
    }else if (button.tag == 2332){//右边日期
        
        _dateLb2.attributedText=attributeStr;
    }
}
- (void)dateBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    FSCalenderSelectedViewController *vc =[[FSCalenderSelectedViewController alloc] init];
    @weakify(self);
    vc.dateBlock = ^(NSDate *date) {
        @strongify(self);
        if (date) {
            if (sender.tag == 2331) {
                
                self.selDate1 = date;
            }else if (sender.tag == 2332) {
                
                self.selDate2 = date;
            }
        }
        
        [self setDateLbTextWithDate:date button:sender];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
}

//出发站 或 到达站
- (void)endOrStartBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    AirTicketSelectedVC *vc =[[AirTicketSelectedVC alloc] init];
    @weakify(self);
    vc.selCityBlock = ^(TrainCityModels *model) {
        @strongify(self);
        if (sender.tag == 101) {//出发站
            self.startTF.text = model.name;
            self.selStartModel = model;
        }else{
            self.endTF.text = model.name;
            self.selEndModel = model;
            
        }
    };
    
    [self.navigationController pushViewController:vc animated:YES];
}

// 交换按钮点击
- (void)exchangeBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (isEmptyString(_startTF.text)) {
        _startTF.text = @"";
    }
    if (isEmptyString(_endTF.text)) {
        _endTF.text = @"";
    }
    
    NSString *changeText = _startTF.text;
    _startTF.text = _endTF.text;
    _endTF.text = changeText;
}


-(SDCycleScrollView *)adAdvier {
    if (!_adAdvier) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 240) delegate:self placeholderImage:nil];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        cycleScrollView.hidesForSinglePage = YES;
        //        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
        cycleScrollView.tag = 100;
        cycleScrollView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
        cycleScrollView.pageDotColor = [UIColor whiteColor];
        cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        cycleScrollView.placeholderImage = [UIImage imageNamed:@"no_pic"];
        cycleScrollView.backgroundColor = BG_COLOR;
        
        _adAdvier =  cycleScrollView;
        
    }
    return _adAdvier;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    //    [self goDifferentADDetail:adverItemModel.app_middle_banner[index]];
}

//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self.view endEditing:YES];
}

- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:range];
    
    return attributeStr;
}


#pragma mark -- 懒加载
- (NSDate *)selDate1
{
    if (!_selDate1) {
        _selDate1 = [NSDate date];
    }
    return _selDate1;
}

- (NSDate *)selDate2
{
    if (!_selDate2) {
        _selDate2 = [NSDate date];
    }
    return _selDate2;
}


- (NSDateFormatter *)dateFormatter{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return _dateFormatter;
    
}

- (NSMutableArray *)buttonArr {
    if (_buttonArr) {
        _buttonArr = [NSMutableArray array];
    }
    return _buttonArr;
}

@end
