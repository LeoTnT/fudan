//
//  AirTicketListDetailVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "AirTicketListDetailVC.h"
#import "UIColor+hexColor.h"
#import "AirTicketListDetailheadView.h"
#import "AirTicketListDetailTimeCell.h"
#import "AirTicketListDetailTableViewCell.h"

@interface AirTicketListDetailVC ()<AirTicketListDetailheadViewDelegate>

@property (nonatomic, strong) AirTicketListDetailheadView *air_headView;//头部界面
@property (nonatomic, strong) NSMutableArray *listArr;

@end

@implementation AirTicketListDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)setUpUI {
  
  
    //头部train_headView
    [self.view addSubview:self.air_headView];
    self.air_headView.delegate = self;
    [self.air_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(64);
    }];
    
    self.air_headView.from_station = self.from_station;
    self.air_headView.to_station = self.to_station;
    
//    UIView *layerBgView = [UIView new];
//    [layerBgView setFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
//    layerBgView.layer.masksToBounds = YES;
//    [layerBgView.layer addSublayer:[UIColor setGradualChangingColor:layerBgView fromColor:@"1A3C57" toColor:@"828282"]];
//    [self.view addSubview:layerBgView];
//
//    [layerBgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(0);
//        make.top.mas_equalTo(self.air_headView.mas_bottom).offset(12);
//        make.width.mas_equalTo(mainWidth);
//        make.bottom.mas_equalTo(0);
//    }];
    
    
    self.view.backgroundColor = BG_COLOR;

   
    [self.view addSubview:self.tableView];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.air_headView.mas_bottom).offset(12);
        make.width.mas_equalTo(mainWidth);
        make.bottom.mas_equalTo(0);
    }];


}




#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return self.listArr.count;
    if (section == 0) {
        return 1;
    }
    return 3;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    
    if (indexPath.section == 0 ) {
        static NSString *CellIdentifier = @"AirTicketListDetailTimeCell";
        AirTicketListDetailTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AirTicketListDetailTimeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
//            cell.delegate = self;
//            if (self.listArr) {
                cell.model = self.selModel;
//                cell.tDic = self.listArr[indexPath.row];
//            }
        return cell;

    }else{
        static NSString *CellIdentifier = @"UITableViewCell";
        AirTicketListDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AirTicketListDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 135;
    }
    return 105;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

#pragma mark ---AirTicketListDetailheadViewDelegate
//返回
-(void)backBtnClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)airticketQueryPrice
{
    
}


-(AirTicketListDetailheadView *)air_headView
{
    if (!_air_headView) {
        _air_headView = [[AirTicketListDetailheadView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 64)];
    }
    return _air_headView;
    
}
@end
