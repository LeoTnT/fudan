//
//  AirTicketListDetailVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AirTicketModel.h"
@interface AirTicketListDetailVC : XSBaseTableViewController
@property (nonatomic,strong) AirTicketModel *selModel;

@property (nonatomic,copy) NSString * from_station;//出发站
@property (nonatomic,copy) NSString * to_station;//到达站

@end
