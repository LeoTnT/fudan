//
//  NoticeViewController.m
//  App3.0
//
//  Created by xinshang on 2018/4/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "NoticeViewController.h"
#import "HTTPManager+ThirdParty.h"

@interface NoticeViewController ()
@property (nonatomic, strong) UILabel *noticeLb;//
@property (nonatomic, strong) UIButton *closeBtn;//
@property (nonatomic, copy) NSString *noticeStr;//
@property (nonatomic, strong) UIScrollView *scroview;
@end

@implementation NoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    [self loadData];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)setUpUI{
    CGFloat space = 10;

    self.scroview = [[UIScrollView alloc] init];
    [self.view addSubview:self.scroview];
    [self.scroview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(40);
        make.width.mas_equalTo(mainWidth-space*2);
        make.bottom.mas_equalTo(-44);
    }];
    
    [self.scroview setContentSize:CGSizeMake(mainWidth-space*2, mainHeight-84)];
    
    //_kmLb
    _noticeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:14] Radius:0];
    _noticeLb.numberOfLines = 0;
    _noticeLb.textAlignment = NSTextAlignmentLeft;
    [self.scroview addSubview:_noticeLb];
    [_noticeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth-space*2);
        make.height.mas_equalTo(mainHeight-100);
    }];
  
    self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeBtn setImage:[UIImage imageNamed:@"user_cart_close"] forState:UIControlStateNormal];
    [self.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: self.closeBtn];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    self.noticeStr = @"购票说明\n 我司通过铁路客运机构官方网站或授权代售点为客户提供火车票预订和增值服务。\n因通过铁路客运机构出票，我司无法承诺百分之百出票成功，我司预先收取您的票款，如果出票失败，退款会原渠道返回您的支付账户\n\n取票说明\n发车前凭预订时使用的证件原件和电子取票号，可在全国任意火车站窗口、自动售取票机或客票代售点取票。\n\n退票说明\n在线退票时间：6:00-23:00。 退票条件：未取纸质票，且离发车时间大于35分钟。其他不能在线退票的情况需在发车前至火车站窗口办理。(配送票若需退票，可在收到车票后到火车站窗口办理，直接退还现金）";

}



- (void)loadData{
    if (!self.type) {
        Alert(@"获取数据失败");
        return;
    }
    CGFloat space = 10;
     __weak __typeof__(self) wSelf = self;
     [XSTool showProgressHUDWithView:self.view];
     [HTTPManager train_GetNoticeWithType:self.type
                                  success:^(NSDictionary *dic, resultObject *state)
      {
        [XSTool hideProgressHUDWithView:wSelf.view];
        if (state.status) {
           NSString *dataStr = [NSString stringWithFormat:@"%@",dic[@"data"]];
            self.noticeLb.text = self.noticeStr;
            if (!isEmptyString(dic[@"data"])) {
                [self setNoticeLbWithStr:dataStr];
            }else{
                [self setNoticeLbWithStr:self.noticeStr];
            }
            
            
        }else{
            [self setNoticeLbWithStr:self.noticeStr];
        }
    } fail:^(NSError *error) {
        [self setNoticeLbWithStr:self.noticeStr];
        [XSTool hideProgressHUDWithView:wSelf.view];
        Alert(NetFailure);
    }];
    
}

- (void)setNoticeLbWithStr:(NSString *)notice{
    CGFloat space = 10;
    self.noticeLb.text = notice;
    CGSize titleSize = [self.noticeStr boundingRectWithSize:CGSizeMake(mainWidth-space*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size;
    if (titleSize.height > mainHeight-84) {
        [self.scroview setContentSize:CGSizeMake(mainWidth-space*2,titleSize.height)];
       
    }
    [self.noticeLb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth-space*2);
        make.height.mas_equalTo(titleSize.height);
    }];
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    //    label.layer.masksToBounds = YES;
    //    label.layer.cornerRadius = radius;
    return label;
}

- (void)closeBtnAction:(UIButton *)sender
{
    NSLog(@"button  Clicked!!! ");
    [self.navigationController popViewControllerAnimated:YES];
}
@end
