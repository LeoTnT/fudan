//
//  TrainListVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@interface TrainListVC : XSBaseTableViewController
@property (nonatomic,copy) NSString * from_station;//出发站
@property (nonatomic,copy) NSString * to_station;//到达站

@property (nonatomic,assign) BOOL isSelectedG;//勾选高铁

@property (nonatomic,copy) NSDate * selDate;//选中的日期

@property (nonatomic,strong) TrainCityModels * selStartModel;//选中出发站
@property (nonatomic,strong) TrainCityModels * selEndModel;//选中到达站

@end
