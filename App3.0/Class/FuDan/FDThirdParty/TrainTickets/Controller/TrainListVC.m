//
//  TrainListVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListVC.h"
#import "TrainListCell.h"
#import "TrainListHeadView.h"
#import "TrainListFootView.h"
#import "FSCalenderSelectedViewController.h"
#import "TrainFilterView.h"
#import "TrainListDetailVC.h"
#import "HTTPManager+ThirdParty.h"

@interface TrainListVC ()<TrainListHeadViewDelegate,TrainListFootViewDelegate,TrainFilterViewDelegate>
@property (nonatomic, strong) UILabel *headView;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面
@property (nonatomic, strong) TrainListHeadView *train_headView;//头部界面
@property (nonatomic, strong) TrainListFootView *train_footView;//底部界面

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) TrainFilterView *filterView;


@property (strong, nonatomic) NSArray *time_filterArr;//筛选-时间
@property (strong, nonatomic) NSArray *type_filterArr;//筛选-车型
@property (strong, nonatomic) NSArray *station_filterArr;//筛选-车站

@property (strong, nonatomic) NSArray *allArr;//筛选用的全部数组

@property (strong, nonatomic) NSArray *fromArr;//出发站全部数组
@property (strong, nonatomic) NSArray *arriveArr;//到达站全部数组



@property (assign, nonatomic) NSInteger timeIndex;//时间筛选 0未选择 1最早出发 2最晚出发 3耗时最短
@property (assign, nonatomic) NSInteger typeIndex;//车型筛选 0未选择 1全部车型 2高铁动车 3特快直达
@property (assign, nonatomic) NSInteger stationIndex;//车站筛选 0未选择 1已选
@property (nonatomic, copy) NSString *sel_from_station;//出发站
@property (nonatomic, copy) NSString *sel_to_station;//到达站

@property (assign, nonatomic) NSInteger ticketIndex;//有票筛选 0未选择 1已选

@property (assign, nonatomic) NSInteger block_Index;
@property (nonatomic, copy) NSString *block_from_station;
@property (nonatomic, copy) NSString *block_to_station;
@end

@implementation TrainListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self SetUpUI];
}


-(void)setIsSelectedG:(BOOL)isSelectedG
{
    _isSelectedG = isSelectedG;
    if (self.isSelectedG) {
        self.typeIndex = 2;
    }else{
        self.typeIndex = 0;

    }
    
}
- (void)SetUpUI
{
    //头部视图
    self.train_headView = [[TrainListHeadView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 110)];
    self.train_headView.delegate = self;
    self.train_headView.from_station = self.from_station;
    self.train_headView.to_station = self.to_station;
    self.train_headView.selDate = self.selDate;
    [self.view addSubview:self.train_headView];
    [self.train_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(110);
    }];
    
    
    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    //    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.train_headView.mas_bottom);
        make.width.mas_equalTo(mainWidth);
        make.bottom.mas_equalTo(-44);
    }];
    
    __weak __typeof__(self) wSelf = self;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = NO;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //        wSelf.page = 1;
        [wSelf queryOrder];
    }];
    
    //    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
    //        wSelf.page++;
    //        [wSelf queryOrder];
    //    }];
    [self.tableView.mj_header beginRefreshing];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    
    //底部界面
    self.train_footView = [[TrainListFootView alloc] initWithFrame:CGRectMake(0, mainWidth-44, mainWidth, 44)];
    self.train_footView.delegate = self;
    [self.view addSubview:self.train_footView];
    [self.train_footView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(44);
    }];
}

//充值记录查询
- (void)queryOrder
{
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate];
    if (isEmptyString(dateStr)) {
        Alert(@"请选择日期");
        return;
    }
            if (isEmptyString(self.selStartModel.code)) {
                Alert(@"请选择出发站");
                return;
            }
            if (isEmptyString(self.selEndModel.code)) {
                Alert(@"请选择到达站");
                return;
            }
    
        NSDictionary *param = @{@"train_date":dateStr,
                                    @"from_station":self.selStartModel.code,
                                    @"to_station":self.selEndModel.code,
                                    };

//    NSDictionary *param = @{@"train_date":dateStr,
//                            @"from_station":@"BDP",
//                            @"to_station":@"BJP",
//                            };
//    
    
    [self.listArr removeAllObjects];
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager  train_QueryTicketsWithDic:param
                                    success:^(NSDictionary *dic, resultObject *state)
     {
         [self tableViewEndRefreshing];
         if (state.status) {
             NSArray *array = dic[@"data"][@"list"];
             if (array.count >0) {
                 [self.listArr addObjectsFromArray:[TrainInfoModels mj_objectArrayWithKeyValuesArray:array]];
                 self.allArr = self.listArr;
                 [self fiterTypeSortArray];
             }
             if (self.listArr.count >0) {
                 //                 [self hideDefaultView];
             }else {
                 //                 [self showDefaultView];
             }
         }else{
             Alert(state.info);
             //             [self showDefaultView];
         }
         [wSelf.tableView reloadData];
     } fail:^(NSError *error) {
         [self tableViewEndRefreshing];
         //         self.defaultView.hidden = NO;
         //         self.tableView.hidden = YES;
         Alert(NetFailure);
     }];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TrainListCell";
    TrainListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TrainListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
    }
    
    if (self.listArr) {
        cell.model = self.listArr[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    TrainListDetailVC *vc =[[TrainListDetailVC alloc] init];
    vc.selDate = self.selDate;
    
    if (self.listArr.count >indexPath.row) {
        TrainInfoModels *model = self.listArr[indexPath.row];
        vc.selModel = model;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = BG_COLOR;
    return view;
}

#pragma mark ---TrainListHeadViewDelegate
//返回
-(void)backBtnClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//前一天
-(void)frontBtnClick:(UIButton *)sender
{
    NSDate *frontDate = [XSTool dateStringFromDate:self.selDate year:0 month:0 day:-1];
    self.selDate = frontDate;
    self.train_headView.selDate = self.selDate;
    [self queryOrder];
}
//后一天
-(void)behindBtnClick:(UIButton *)sender
{
    NSDate *behindDate = [XSTool dateStringFromDate:self.selDate year:0 month:0 day:1];
    self.selDate = behindDate;
    self.train_headView.selDate = self.selDate;
    [self queryOrder];
}
//日历
-(void)dateBtnClick:(UIButton *)sender
{
    
    FSCalenderSelectedViewController *vc =[[FSCalenderSelectedViewController alloc] init];
    @weakify(self);
    vc.dateBlock = ^(NSDate *date) {
        @strongify(self);
        if (date) {
            self.selDate = date;
        }
        self.train_headView.selDate = self.selDate;
        [self queryOrder];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ------------TrainListFootViewDelegate--------------
//筛选
-(void)filterBtnClick:(UIButton *)sender
{
    switch (sender.tag) {
        case 100://时间
        {
            [self showConfirmView:self.time_filterArr filterIndex:0];
        }
            break;
        case 101://车型
        {
            [self showConfirmView:self.type_filterArr filterIndex:1];
        }
            break;
        case 102://车站
        {
            [self showConfirmView:self.station_filterArr filterIndex:2];
        }
            break;
        case 103://有票优先
        {
            self.ticketIndex = sender.selected ? 1: 0;
            [self fiterTypeSortArray];
        }
            break;
        default:
            break;
    }
    
}




-(TrainFilterView *)filterView
{
    if (!_filterView) {
        _filterView = [[TrainFilterView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _filterView.delegate = self;
        
        @weakify(self);
       _filterView.filterBlock = ^(NSDictionary *dict, NSInteger index,NSString *from_station,NSString *to_station) {
            @strongify(self);
           self.block_Index = index;
           self.block_from_station = from_station;
           self.block_to_station = to_station;
        };
        
        
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        [window.rootViewController.view addSubview:_filterView];
    }
    
    return _filterView;
}
-(void)cutomBtnClick:(UIButton *)sender
{
    [self hideConfirmView];
    
    switch (sender.tag) {
        case 1://取消
        {
        }
            break;
        case 2://重置
        {
            if (self.filterView.filterIndex == 0) {//时间
                self.timeIndex = 0;
            }else if (self.filterView.filterIndex == 1) {//车型
                self.typeIndex = 0;
            }else if (self.filterView.filterIndex == 2) {//车站筛选
                self.stationIndex = 0;
                self.sel_from_station = @"";
                self.sel_to_station = @"";
            }
        }
            break;
        case 3://确定
        {
            if (self.filterView.filterIndex == 0) {//时间
                self.timeIndex = self.block_Index+1;
            }else if (self.filterView.filterIndex == 1) {//车型
                self.typeIndex = self.block_Index+1;
            }else if (self.filterView.filterIndex == 2) {//车站筛选
                self.stationIndex = 1;
                self.sel_from_station = self.block_from_station;
                self.sel_to_station = self.block_to_station;
            }
        }
            break;
        default:
            break;
    }
            [self fiterTypeSortArray];
}


- (void)showConfirmView:(NSArray*)array filterIndex:(NSInteger)index
{
    self.filterView.filterIndex = index;
    self.filterView.listArr = array;
    self.filterView.hidden = NO;
    
}
- (void)hideConfirmView
{
    self.filterView.hidden = YES;
}


#pragma Mark ------懒加载------

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}

-(NSArray *)allArr{
    if (!_allArr) {
        _allArr = [NSArray array];
    }
    return _allArr;
    
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - lazy loadding
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 104, mainWidth, mainHeight-110)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = Localized(@"暂无更多记录");
        
    }
    return _defaultView;
}

- (void)showDefaultView
{
    self.defaultView.hidden = NO;
    self.tableView.hidden = YES;
}
- (void)hideDefaultView
{
    self.defaultView.hidden = YES ;
    if (self.tableView.hidden) {
        self.tableView.hidden = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(NSDateFormatter *)dateFormatter{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return _dateFormatter;
    
}
-(NSArray *)time_filterArr
{
    if (!_time_filterArr) {
        _time_filterArr =@[@{@"title":@"时间",
                             @"type":@"1",
                             @"data":@[@"最早出发",@"最晚出发",@"耗时最短"]
                             }];
    }
    return _time_filterArr;
}

-(NSArray *)type_filterArr
{
    if (!_type_filterArr) {
        _type_filterArr =@[@{@"title":@"车型",
                             @"type":@"2",
                             @"data":@[@"全部车型",@"高铁动车",@"特快直达"]
                             }];
    }
    return _type_filterArr;
}

-(NSArray *)station_filterArr
{
    if (!_station_filterArr) {
        NSMutableArray *mFromArr = [NSMutableArray array];
        NSMutableArray *mToArr = [NSMutableArray array];
        for (TrainInfoModels *model in self.allArr) {
            [mFromArr addObject:model.from_station_name];
            [mToArr addObject:model.to_station_name];
        }
        
        self.fromArr = [mFromArr valueForKeyPath:@"@distinctUnionOfObjects.self"];
        self.arriveArr = [mToArr valueForKeyPath:@"@distinctUnionOfObjects.self"];
        
        _station_filterArr =@[@{@"title":@"出发站",
                                @"type":@"3",
                                @"data":self.fromArr
                                },
                              @{@"title":@"到达站",
                                @"type":@"3",
                                @"data":self.arriveArr
                                }];
    }
    return _station_filterArr;
}

#pragma mark ---筛选排序
- (void)fiterTypeSortArray
{
    NSArray *sortArr = [NSArray array];
    
    //有票筛选
    if (self.ticketIndex == 1) {
        sortArr = [self getHasTicketNumArr];
    }else{
        sortArr = self.allArr;
    }
    
    //时间筛选
    if (self.timeIndex>0) {
        NSArray *timeArr = [sortArr sortedArrayUsingComparator:
                            ^(TrainInfoModels *obj1, TrainInfoModels* obj2){
                                if (obj1.start_time.length <5 &&obj2.start_time.length <5) {
                                    return(NSComparisonResult)NSOrderedSame;
                                }
                                
                                NSString *hour1 =  [obj1.start_time substringToIndex:2];//截取掉下标2之前的字符串
                                NSString *mintune1 = [obj1.start_time substringFromIndex:obj1.start_time.length- 2];//截取后两位
                                
                                NSString *hour2 =  [obj2.start_time substringToIndex:2];//截取掉下标2之前的字符串
                                NSString *mintune2 = [obj2.start_time substringFromIndex:obj2.start_time.length- 2];//截取后两位
                                
                                if (self.timeIndex == 1) {//最早出发
                                    if(hour1.intValue < hour2.intValue) {
                                        return(NSComparisonResult)NSOrderedAscending;
                                    }else if(hour1.intValue == hour2.intValue){
                                        if(mintune1.intValue < mintune2.intValue) {
                                            return(NSComparisonResult)NSOrderedAscending;
                                        }else {
                                            return(NSComparisonResult)NSOrderedDescending;
                                        }
                                    }else {
                                        return(NSComparisonResult)NSOrderedDescending;
                                    }
                                }else if (self.timeIndex == 2) {//最晚出发
                                    if(hour1.intValue > hour2.intValue) {
                                        return(NSComparisonResult)NSOrderedAscending;
                                    }else if(hour1.intValue == hour2.intValue){
                                        if(mintune1.intValue > mintune2.intValue) {
                                            return(NSComparisonResult)NSOrderedAscending;
                                        }else {
                                            return(NSComparisonResult)NSOrderedDescending;
                                        }
                                    }else {
                                        return(NSComparisonResult)NSOrderedDescending;
                                    }
                                }else {//耗时最短
                                    if(obj1.run_time_minute.doubleValue < obj2.run_time_minute.doubleValue) {
                                        return(NSComparisonResult)NSOrderedAscending;
                                    }else {
                                        return(NSComparisonResult)NSOrderedDescending;
                                    }
                                }
                                
                            }];
        sortArr = timeArr;
    }
    
    
    //车型筛选 全部车型 2高铁动车 3特快直达
    if (self.typeIndex>1) {
        NSMutableArray *mTypeArr = [NSMutableArray array];
        for (TrainInfoModels *tModel in sortArr) {
            if ([tModel.train_type isEqualToString:@"G"] ||
                [tModel.train_type isEqualToString:@"C"] ||
                [tModel.train_type isEqualToString:@"D"]) {//高铁动车
                if (self.typeIndex == 2) {
                    [mTypeArr addObject:tModel];
                }
            }else{
                if (self.typeIndex == 3) {//特快直达
                    [mTypeArr addObject:tModel];
                }
            }
        }
        
        sortArr = (NSArray*)mTypeArr;
    }
    
    
    //车站筛选
    if (self.stationIndex == 1) {
        //
        if (!isEmptyString(self.sel_from_station)) {//出发站
            NSMutableArray *mfromArr = [NSMutableArray array];
            for (TrainInfoModels *sModel in sortArr) {
                if ([sModel.from_station_name isEqualToString:self.sel_from_station]) {
                    [mfromArr addObject:sModel];
                }
            }
            sortArr = (NSArray*)mfromArr;
        }
        if (!isEmptyString(self.sel_to_station)) {//到达站
            NSMutableArray *mToArr = [NSMutableArray array];
            for (TrainInfoModels *tModel in sortArr) {
                if ([tModel.to_station_name isEqualToString:self.sel_to_station]) {
                    [mToArr addObject:tModel];
                }
            }
            sortArr = (NSArray*)mToArr;
        }
    }
    
    self.listArr = [NSMutableArray arrayWithArray:sortArr];
    [self.tableView reloadData];
}

//有票优先
- (NSArray *)getHasTicketNumArr
{
    //二等座;一等座;特等座;商务座;软座;动卧;硬座;硬卧;软卧;无座;高级软卧;其他
    
    NSMutableArray *tArr = [NSMutableArray array];
    for (TrainInfoModels *model in self.allArr) {
        if (model.edz_num.doubleValue >0||
            model.ydz_num.doubleValue >0||
            model.tdz_num.doubleValue >0||
            model.swz_num.doubleValue >0||
            model.rz_num.doubleValue >0||
            model.dw_num.doubleValue >0||
            model.yz_num.doubleValue >0||
            model.yw_num.doubleValue >0||
            model.rw_num.doubleValue >0||
            model.wz_num.doubleValue >0||
            model.gjrw_num.doubleValue >0||
            model.qtxb_num.doubleValue >0) {
            [tArr addObject:model];
        }
    }
    return (NSArray*)tArr;
}



@end

