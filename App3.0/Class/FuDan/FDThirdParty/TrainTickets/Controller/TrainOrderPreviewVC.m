//
//  TrainOrderPreviewVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderPreviewVC.h"
#import "TrainOrderPreviewPassengerCell.h"
#import "TrainOrderPreviewAddOrEditCell.h"
#import "TrainOrderPreviewContactCell.h"
#import "MobilePhoneContactVC.h"
#import "TrainPassengerListVC.h"
#import "TrainOrderDetialVC.h"
#import "NoticeViewController.h"
#import "PasswordAlertView.h"
#import "ModifyOldPayViewController.h"
#import "MissPayPswViewController.h"
#import "UserModel.h"
#import "TrainOrderVC.h"
#import "HTTPManager+ThirdParty.h"
#import "RSAEncryptor.h"

@interface TrainOrderPreviewVC ()<TrainListDetailHeadViewDelegate,TrainOrderPreviewContactCellDelegate,TrainOrderPreviewAddOrEditCellDelegate,TrainOrderPreviewPassengerCellDelegate>
@property (nonatomic, strong) TrainListDetailHeadView *train_headView;//头部界面
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSArray *allArr;//筛选用的全部数组
//@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) NSString *mobileStr;

@property (nonatomic, strong) NSArray *selPassageArr;

@property (nonatomic, strong) UIView *footView;
@property (nonatomic, strong) UILabel *priceLb;//价格
@property (nonatomic, strong) UIButton *commitBtn;//提交订单
@property (nonatomic, strong) NSDictionary *zwcodeDic;//座位名称对应的字典
@property (nonatomic, strong) NSString *price;//价格
@property (nonatomic, strong) NSString *prop;//比例
@property (nonatomic, strong) NSString *jh_free;//聚合手续费
@property (nonatomic, strong) NSString *free;//平台手续费
@property(nonatomic,strong) PasswordAlertView *pswView;
@property(nonatomic,strong) NSString *payPassWord;
@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation TrainOrderPreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self SetUpUI];
}

- (void)SetUpUI
{
    
    //头部train_headView
    [self.view addSubview:self.train_headView];
    self.train_headView.dateView.hidden = YES;
    self.train_headView.ZW_View.hidden = NO;
    self.train_headView.delegate = self;
    [self.train_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(215);
    }];
    
    
    self.train_headView.selDate = self.selDate;
    self.train_headView.model= self.selModel;
    self.train_headView.zw_lb.text = self.nameStr;
    self.train_headView.price_lb.text = self.priceStr;
    [self.view addSubview:self.train_headView];

    
    //底部__footView
    self.footView = [[UIView alloc] init];
    self.footView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.footView];
    [self.footView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(50);
    }];
    
    //_priceLb
    _priceLb =  [[UILabel alloc] init];
    _priceLb.textColor = Color(@"F78F15");
    _priceLb.font = [UIFont boldSystemFontOfSize:17];
    [self.footView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(30);
    }];
    _priceLb.text = @" 0.0";
    //_commitBtn
    _commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _commitBtn.backgroundColor = Color(@"F78F15");
    _commitBtn.layer.masksToBounds = YES;
    _commitBtn.layer.cornerRadius = 4;
    [_commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_commitBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
     [_commitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [_commitBtn addTarget:self action:@selector(commitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.footView addSubview:_commitBtn];
    
    [_commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(30);
    }];
    
    
    
    
    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    //    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.train_headView.mas_bottom).offset(12);
        make.width.mas_equalTo(mainWidth);
        make.bottom.mas_equalTo(self.footView.mas_top);
    }];
    
//    __weak __typeof__(self) wSelf = self;
    //    self.showRefreshHeader = YES;
    //    self.showRefreshFooter = NO;
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //        wSelf.page = 1;
//        [wSelf queryOrder];
//    }];
    
    //    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
    //        wSelf.page++;
    //        [wSelf queryOrder];
    //    }];
//    [self.tableView.mj_header beginRefreshing];
    
    
    
}




#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.selPassageArr.count;
    }else if (section == 1)
    {
        return 1;
    }else if (section == 2)
    {
        return 2;
    }
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"TrainOrderPreviewPassengerCell";
        TrainOrderPreviewPassengerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[TrainOrderPreviewPassengerCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cell.delegate = self;
        cell.delBtn.tag = indexPath.row;
        if (self.selPassageArr.count >0) {
            cell.model = self.selPassageArr[indexPath.row];
        }
        return cell;
    }else if (indexPath.section == 1) {
        static NSString *CellIdentifier = @"TrainOrderPreviewAddOrEditCell";
        TrainOrderPreviewAddOrEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[TrainOrderPreviewAddOrEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cell.delegate = self;
        if (self.selPassageArr.count >0) {

            NSInteger type = 2;
            for (TrainPassengerModel *model in self.selPassageArr) {
                if (model.piaotype.intValue == 1
                    ||model.piaotype.intValue == 4) {//成人票||残军票
                     type = 1;
                }
            }
            cell.type = type;
        }else{
            cell.type = 0;
        }
        return cell;
    }else {
        if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"TrainOrderPreviewContactCell";
            TrainOrderPreviewContactCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[TrainOrderPreviewContactCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            [cell.mobileTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            cell.delegate = self;
            if (!isEmptyString(self.mobileStr)) {
                cell.mobileTF.text = self.mobileStr;
            }
            return cell;
        }else{
        static NSString *CellIdentifier = @"UITableViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = COLOR_666666;
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            
        }
            cell.textLabel.text = @"预订须知";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 63;
    }
    return 44;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    if (indexPath.section == 2) {
        if (indexPath.row == 1) {
            NoticeViewController *vc =[[NoticeViewController alloc] init];
            vc.type = @"train_submit";
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return 10;
    }
    return CGFLOAT_MIN;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = BG_COLOR;
    
    return footerView;
    
}

#pragma mark ---TrainOrderPreviewPassengerCellDelegate
//删除
-(void)delBtnClick:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (sender.tag >=self.selPassageArr.count) {
        return;
    }
    
    NSMutableArray *mArr = [NSMutableArray arrayWithArray:self.selPassageArr];
    [mArr removeObjectAtIndex:sender.tag];
    self.selPassageArr = mArr;
    [self.tableView reloadData];
    
}
//编辑或添加
-(void)customBtnClick:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (self.selPassageArr.count >=5) {
        Alert(@"最多添加5位乘客");
        return;
    }
    if (sender.tag == 3) {//添加儿童
        if (self.selPassageArr.count <=0) {
            return;
        }
        self.selPassageArr = [self getNewChildenArray];
        [self.tableView reloadData];
        [self getPassengers];

    }else{
        TrainPassengerListVC *vc =[[TrainPassengerListVC alloc] init];
        @weakify(self);
        vc.selPassengerBlock = ^(NSArray *passengerArr) {
            @strongify(self);
            self.selPassageArr = [NSArray array];
            self.selPassageArr = passengerArr;
            [self.tableView reloadData];
            [self getPassengers];
        };
        vc.selPassengerArr = self.selPassageArr;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
   
}
- (NSArray *)getNewChildenArray
{
     NSMutableArray *mArr = [NSMutableArray arrayWithArray:self.selPassageArr];
    //新建一个模型
    TrainPassengerModel *addModel = [[TrainPassengerModel alloc] init];
    TrainPassengerModel *firstModel = self.selPassageArr.firstObject;
    addModel.piaotype = @"2";
    addModel.piaotypename = @"儿童票";
    addModel.passengersename = firstModel.passengersename;
    addModel.uid = firstModel.uid;
    addModel.passporttypeseid = firstModel.passporttypeseid;
    addModel.passporttypeseidname = firstModel.passporttypeseidname;
    addModel.passportseno = firstModel.passportseno;
    
    [mArr addObject:addModel];
    return (NSArray *)mArr;
}
//联系人
-(void)contactBtnClick:(UIButton *)sender
{
    [self.view endEditing:YES];

    MobilePhoneContactVC *vc =[[MobilePhoneContactVC alloc] init];
    @weakify(self);
    vc.mobileBlock = ^(NSString *mobileNum) {
        @strongify(self);
        if (mobileNum.length == 11) {
            //            mobileNum = [NSString stringWithFormat:@"%@ %@ %@",[mobileNum substringWithRange:NSMakeRange(0, 2)],[mobileNum substringWithRange:NSMakeRange(3, 6)],[mobileNum substringWithRange:NSMakeRange(7, 10)]];
        }
        self.mobileStr = mobileNum;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    };
    
    [self.navigationController pushViewController:vc animated:YES];

    
}

//1.验证输入信息
-(void)commitBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    
    //发车日期，如：2015-07-01（务必按照此格式）
    NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate];
    if (isEmptyString(dateStr)) {
        Alert(@"请选择日期");
        return;
    }
    if (!self.selModel) {
        Alert(@"获取订单信息失败");
        return;
    }
    if (!self.mobileStr) {
        Alert(@"请填写通知人手机号");
        return;
    }
    /* "zwcode":如：1。表示座位编码，其中
        F:动卧(新增),
        9:商务座,
        P:特等座,
        M:一等座,
        O（大写字母O，不是数字0）:二等座,
        6:高级软卧,
        4:软卧,
        3:硬卧,
        2:软座,
        1:硬座。
     */
    [self showPSWAlert];
    

}
//输入支付密码
- (void)showPSWAlert
{
    if ([self.payPassWord isEqualToString:@""]) {
        //未设置支付密码
        //弹出警告框
        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"提示") message:Localized(@"您还没有设置支付密码，是否要设置支付密码？") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"取消") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"确认") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //跳转到设置支付密码界面
            [self pushChangePayView];
            
        }];
        [alertControl addAction:cancelAction];
        [alertControl addAction:okAction];
        [self presentViewController:alertControl animated:YES completion:nil];
        return;
    }
    
    
    self.pswView = [[PasswordAlertView alloc] initPasswordView];
    @weakify(self);
    self.pswView.passWordText = ^(NSString *text) {
        @strongify(self);
        NSLog(@"text=%@",text);
        if (text.length == 6) {
            [self submitWithPwd:text];
            [self.pswView removeFromSuperview];
            
        }
    };
//    self.pswView.forgetBlock = ^{
//        @strongify(self);
//        //跳转到设置支付密码界面
//        [self forgetBtnAction];
//        [self.pswView removeFromSuperview];
//        
//    };
    [self.pswView passwordShow];
    
}

//3.提交订单
- (void)submitWithPwd:(NSString *)pwd
{
    //    [{"passengerid":1,"passengersename":"张三","piaotype":"1","piaotypename":"成人票","passporttypeseid":"1","passporttypeseidname":"二代身份证","passportseno":"420205199207231234","price":"763.5","zwcode":"M","zwname":"一等座"}]
    
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    //发车日期，如：2015-07-01（务必按照此格式）
    NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate];

    NSDate *end_date = [XSTool dateStringFromDate:self.selDate year:0 month:0 day:self.selModel.arrive_days.intValue ];//到达日期
    NSString *end_dateStr = [self.dateFormatter stringFromDate:end_date];
    
    
    NSString *start_time = [NSString stringWithFormat:@"%@ %@:00",dateStr,self.selModel.start_time];
    NSString *arrive_time = [NSString stringWithFormat:@"%@ %@:00",end_dateStr,self.selModel.arrive_time];
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_SubmitWithTrain_date:dateStr
                          from_station_code:self.selModel.from_station_code
                            to_station_code:self.selModel.to_station_code
                         is_accept_standing:@"yes"
                                      checi:self.selModel.train_code
                                 start_time:start_time
                                arrive_time:arrive_time
                          from_station_name:self.selModel.from_station_name
                            to_station_name:self.selModel.to_station_name
                                orderamount:self.priceStr
                                    runtime:self.selModel.run_time
                                      phone:self.mobileStr
                                    pay_pwd:[RSAEncryptor encryptString:pwd]
                                 passengers:[self getPassengers]
                                    success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             NSString *orderid = [NSString stringWithFormat:@"%@",dic[@"data"][@"oid"]];
             if (!isEmptyString(orderid)) {
                     //为避免聚合处理延迟  进入详情页，再支付
                     TrainOrderDetialVC *vc =[[TrainOrderDetialVC alloc] init];
                     vc.isWait = NO;
                     vc.orderid = orderid;
                     [self.navigationController pushViewController:vc animated:YES];
             }
             
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } failure:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}






//4.预支付
- (void)payTrainOrderWithPwd:(NSString *)pwd Orderid:(NSString*)orderid
{
    NSString *encryptPassWord = [RSAEncryptor encryptString:pwd];
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_PayWithOrderid:orderid
                              pay_pwd:encryptPassWord
                              success:^(NSDictionary *dic, resultObject *state) {
                                  [XSTool hideProgressHUDWithView:wSelf.view];
                                  if (state.status) {
//                                      Alert(state.info);
//                                      [wSelf getOrderInfo:wSelf.model.orderid];
                                      
                                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                                          TrainOrderDetialVC *vc =[[TrainOrderDetialVC alloc] init];
//                                          vc.isWait = NO;
//                                          vc.orderid = orderid;
//                                          [self.navigationController pushViewController:vc animated:YES];
                                          
                                          [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:@"提示" message:@"订单支付成功,等待出票中,请稍后查询出票结果?" CallBackBlock:^(NSInteger btnIndex) {
                                              if (btnIndex==1) {
                                                  TrainOrderVC  * vc = [[TrainOrderVC  alloc] init];
                                                  [self.navigationController pushViewController:vc animated:YES];
                                              }
                                          } cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
                                          
                                      
                                      
                                      });
                                      
                                  }else{
                                      Alert(state.info);
                                  }
                              } fail:^(NSError *error) {
                                  [XSTool hideProgressHUDWithView:wSelf.view];
                                  Alert(NetFailure);
                              }];
}



//跳转到设置支付密码界面
- (void)pushChangePayView
{
    ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
    [self.navigationController pushViewController:oldPayViewController animated:YES];
    
}


#pragma mark - 忘记密码
-(void)forgetBtnAction{
    
    MissPayPswViewController *missVC = [[MissPayPswViewController alloc] init];
    [self.navigationController pushViewController:missVC animated:YES];
    
}


//获取乘客信息数组(接口使用)
- (NSArray *)getPassengers
{
    
    CGFloat totalPrice = 0;
    NSMutableArray *mPassengerArr = [NSMutableArray array];
    NSMutableDictionary *tDic = [NSMutableDictionary dictionary];
    for (int i=0; i<self.selPassageArr.count; i++) {
        TrainPassengerModel *model = self.selPassageArr[i];
        int passengerid = i+1;
        [tDic setObject:@(passengerid) forKey:@"passengerid"];
        [tDic setObject:model.passengersename forKey:@"passengersename"];
        [tDic setObject:model.piaotype forKey:@"piaotype"];
        [tDic setObject:model.piaotypename forKey:@"piaotypename"];
        [tDic setObject:model.passporttypeseid forKey:@"passporttypeseid"];
        [tDic setObject:model.passporttypeseidname forKey:@"passporttypeseidname"];
        [tDic setObject:model.passportseno forKey:@"passportseno"];
        

        [tDic setObject:self.priceStr forKey:@"price"];//价格
        
        if (model.piaotype.intValue == 2) {//儿童票价格
            totalPrice = totalPrice+self.priceStr.doubleValue/2.0f;
        }else{
            totalPrice = totalPrice+self.priceStr.doubleValue;
            
        }
        if ([self.nameStr isEqualToString:@"无座"]) {
            NSString * zwname = @"硬座";
            if (self.typeArr.count>0) {
                NSDictionary *firtDic = self.typeArr.firstObject;
                if (![firtDic.allValues.firstObject isEqualToString:@"无座"]) {
                    zwname = firtDic.allValues.firstObject;
                    [tDic setObject:[self getWZ_codeWithName:zwname] forKey:@"zwcode" ];//座位编码
                    [tDic setObject:zwname forKey:@"zwname"];//座位名称
                }else{
                    if (self.typeArr.count >1) {
                        NSDictionary *secondDic = self.typeArr[1];
                        zwname = secondDic.allValues.firstObject;
                        [tDic setObject:[self getWZ_codeWithName:zwname] forKey:@"zwcode" ];//座位编码
                        [tDic setObject:zwname forKey:@"zwname"];//座位名称
                    }
                    
                }
            }
           

        }else{
            [tDic setObject:self.nameStr forKey:@"zwname"];//座位名称
            [tDic setObject:[self getWZ_codeWithName:self.nameStr] forKey:@"zwcode" ];//座位编码

        }
        
        [mPassengerArr addObject:tDic];
        
    }
    if (!isEmptyString(self.jh_free)) {
        totalPrice = self.priceStr.floatValue +self.jh_free.floatValue + self.free.floatValue;
    }
    self.priceLb.text = [NSString stringWithFormat:@"%.2f",totalPrice];

    if (self.price.doubleValue >0 && self.prop.doubleValue >0 ) {
        //     * @description 获取NMC比例 [money/prop/price = 要支付的nmc数量] 除法

        CGFloat nmcPrice = totalPrice/self.prop.doubleValue/self.price.doubleValue;
//        self.priceLb.text = [NSString stringWithFormat:@"%.2f(%.2fNMC)",totalPrice,nmcPrice];
        
        NSString *textStr = [NSString stringWithFormat:@"%.2f(%.2fNMC)",totalPrice,nmcPrice];
        NSMutableAttributedString *attributeStr = [self setStringWithStr:textStr RangStr:[NSString stringWithFormat:@"(%.2fNMC)",nmcPrice]];
        self.priceLb.attributedText=attributeStr;
    }
    return mPassengerArr;
}


- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:range];
    return attributeStr;
    
}

- (NSString *)getWZ_codeWithName:(NSString *)zw_name
{
  
    NSString *zwcode = @"";
    
//    if ([zw_name isEqualToString:@"无座"]) {
////        if ([self.selModel.train_type isEqualToString:@"G"]||
////            [self.selModel.train_type isEqualToString:@"D"]||
////            [self.selModel.train_type isEqualToString:@"C"]) {
////            zwcode = @"O";
////        }else{
////            zwcode = @"1";
////
////        }
//
//        if (<#condition#>) {
//            <#statements#>
//        }
//        return zwcode;
//    }
    
    NSArray *allKeys = self.zwcodeDic.allKeys;
    if ([allKeys containsObject:zw_name]) {
        zwcode = [self.zwcodeDic objectForKey:zw_name];
    }
    return zwcode;
    
}

#pragma mark ---TrainListDetialHeadViewDelegate
//返回
-(void)backBtnClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(NSDateFormatter *)dateFormatter{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return _dateFormatter;
    
}

#pragma Mark ------懒加载------

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSArray *)typeArr{
    if (!_typeArr) {
        _typeArr = [NSArray array];
    }
    return _typeArr;
    
}

-(NSArray *)selPassageArr{
    if (!_selPassageArr) {
        _selPassageArr = [NSArray array];
    }
    return _selPassageArr;
    
}
-(NSArray *)allArr{
    if (!_allArr) {
        _allArr = [NSArray array];
    }
    return _allArr;
    
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
-(TrainListDetailHeadView *)train_headView
{
    if (!_train_headView) {
        _train_headView = [[TrainListDetailHeadView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 215)];
    }
    return _train_headView;
    
}
-(NSDictionary *)zwcodeDic
{
    if (!_zwcodeDic) {
        _zwcodeDic =  @{@"动卧":@"F",
                        @"商务座":@"9",
                        @"特等座":@"P",
                        @"一等座":@"M",
                        @"二等座":@"O",
                        @"高级软卧":@"6",
                        @"软卧":@"4",
                        @"硬卧":@"3",
                        @"软座":@"2",
                        @"硬座":@"1",
                        @"无座":@""};
    }
    return _zwcodeDic;
}
-(void)textFieldDidChange :(UITextField *)textField{
    self.mobileStr = textField.text;
}

@end
