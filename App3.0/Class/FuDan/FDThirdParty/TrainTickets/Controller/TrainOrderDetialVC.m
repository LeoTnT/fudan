//
//  TrainOrderDetialVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderDetialVC.h"
#import "TrainOrderDetialHeadView.h"
#import "TrainOrderDetialPassengerCell.h"
#import "PasswordAlertView.h"
#import "ModifyOldPayViewController.h"
#import "MissPayPswViewController.h"
#import "UserModel.h"
#import "TrainOrdeNoteCell.h"
#import "HTTPManager+ThirdParty.h"
#import "RSAEncryptor.h"

@interface TrainOrderDetialVC ()
{
    BOOL _isTimer;// 是否在倒计时
    
}
@property (nonatomic, strong) TrainOrderDetialHeadView *train_headView;//头部界面
@property (nonatomic, strong) UIView *footView;
@property (nonatomic, strong) UILabel *priceLb;//价格
@property (nonatomic, strong) UIButton *commitBtn;//提交订单
@property(nonatomic,strong) PasswordAlertView *pswView;
@property(nonatomic,strong) NSString *payPassWord;
@property (nonatomic ,strong) dispatch_source_t timer;
@end

@implementation TrainOrderDetialVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUserInformation];
}

- (void)getUserInformation {
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord = parser.pay_password;
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // 启动倒计时管理
    [kCountDownManager2 start];
    
    //    滕州  枣庄
    
    if (self.isWait) {//请稍后待处理
        
        
        _isTimer = YES; // 设置倒计时状态为YES
        _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:30 stop:^{
            //设置按钮的样式
            dispatch_source_cancel(_timer);
            
            [XSTool hideProgressHUDWithView:self.view];
            _isTimer = NO; // 倒计时状态为NO
            [self SetUpUI];
            if (!isEmptyString(self.orderid)) {
                [self getOrderInfo:self.orderid];
            }
        } otherAction:^(int time) {
            NSString *timeStr = [NSString stringWithFormat:@"正在处理请稍后(%.2d)...", time];
            [XSTool showToastWithView:self.view Text:timeStr];
            
        }];
        
    }else{
        [self SetUpUI];
        
        if (!isEmptyString(self.orderid)) {
            [self getOrderInfo:self.orderid];
        }
    }
    
}

- (TrainOrderDetialHeadView *)train_headView {
    if (!_train_headView) {
        _train_headView = [[TrainOrderDetialHeadView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 180)];
    }
    return _train_headView;
    
}

- (void)SetUpUI {
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height-50);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    //头部train_headView
    self.tableView.tableHeaderView = self.train_headView;
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(mainHeight-64-50);
        //        make.bottom.mas_equalTo(self.footView.mas_top);
    }];
    __weak __typeof__(self) wSelf = self;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = NO;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //            wSelf.page = 1;
        [wSelf getOrderInfo:self.orderid];
    }];
    
    //    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
    //        wSelf.page++;
    //        [wSelf queryOrder];
    //    }];
    //    [self.tableView.mj_header beginRefreshing];
    
    
    if (self.model.from_station_code) {
        self.train_headView.model = self.model;
    }
    
    
    //底部__footView
    self.footView = [[UIView alloc] init];
    self.footView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.footView];
    [self.footView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(50);
    }];
    
    //_priceLb
    _priceLb =  [[UILabel alloc] init];
    _priceLb.textColor = Color(@"F78F15");
    _priceLb.font = [UIFont boldSystemFontOfSize:17];
    [self.footView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(30);
    }];
    _priceLb.text = @" 0.0";
    //_commitBtn
    _commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _commitBtn.backgroundColor = Color(@"F78F15");
    _commitBtn.layer.masksToBounds = YES;
    _commitBtn.layer.cornerRadius = 4;
    [_commitBtn setBackgroundImage:[UIImage xl_imageWithColor:Color(@"F78F15") size:CGSizeMake(100, 30)] forState:UIControlStateNormal];
    [_commitBtn setBackgroundImage:[UIImage xl_imageWithColor:[UIColor grayColor] size:CGSizeMake(100, 30)] forState:UIControlStateDisabled];
    
    [_commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_commitBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [_commitBtn setTitle:@"去支付" forState:UIControlStateNormal];
    [_commitBtn addTarget:self action:@selector(commitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.footView addSubview:_commitBtn];
    
    [_commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(30);
    }];
}

- (TrainOrderModel *)model {
    if (!_model) {
        _model =  [[TrainOrderModel alloc] init];
    }
    return _model;
}

//单个订单查询
- (void)getOrderInfo:(NSString *)orderid {
    //    [self tableViewEndRefreshing];
    //    if (self.page == 1) {
    //        [self.listArr removeAllObjects];
    //    }
    [self.tableView.mj_footer resetNoMoreData];
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_QueryOrderStatusWithOrderid:orderid
                                           success:^(NSDictionary *dic, resultObject *state)
     {
         [self tableViewEndRefreshing];
         if (state.status) {
             wSelf.model = [TrainOrderModel mj_objectWithKeyValues:dic[@"data"]];
             wSelf.train_headView.model = wSelf.model;
             wSelf.priceLb.text = [NSString stringWithFormat:@"%@",wSelf.model.orderamount];
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
     } fail:^(NSError *error) {
         [self tableViewEndRefreshing];
         Alert(NetFailure);
         
     }];
}

//比较两个日期大小
- (int)compareDate:(NSString*)startDate withDate:(NSString*)endDate {
    
    int comparisonResult;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] init];
    date1 = [formatter dateFromString:startDate];
    date2 = [formatter dateFromString:endDate];
    NSComparisonResult result = [date1 compare:date2];
    NSLog(@"result==%ld",(long)result);
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending:
            comparisonResult = 1;
            break;
            //date02比date01小
        case NSOrderedDescending:
            comparisonResult = -1;
            break;
            //date02=date01
        case NSOrderedSame:
            comparisonResult = 0;
            break;
        default:
            NSLog(@"erorr dates %@, %@", date1, date2);
            break;
    }
    return comparisonResult;
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.model.status.intValue == 4) {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
        
        
        int comparisonResult = [self compareDate:currentDateStr withDate:self.model.start_time];
        if(comparisonResult >= 0){
            //endDate 小
            return 2;
        }
        return 1;
    }
    if ([self.model.status integerValue]==2) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (self.model.passengers.count>0) {
            return self.model.passengers.count+1;
        }
        return self.model.passengers.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (indexPath.row == self.model.passengers.count&&indexPath.row >0) {
            static NSString *CellIdentifier = @"TrainOrdeNoteCell";
            TrainOrdeNoteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[TrainOrdeNoteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.noteLb.numberOfLines = 0;
            cell.noteLb.backgroundColor = [UIColor whiteColor];
            cell.noteLb.text = [NSString stringWithFormat:@"备注:%@",self.model.msg];
            return cell;
            
        }else{
            
            static NSString *CellIdentifier = @"TrainOrderDetialPassengerCell";
            TrainOrderDetialPassengerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[TrainOrderDetialPassengerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.textLabel.font = [UIFont systemFontOfSize:15];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
                
            }
            cell.status = self.model.status;
            if (self.model.passengers.count >0) {
                cell.model = self.model.passengers[indexPath.row];
            }
            if (self.model.status.intValue == 2) {
                self.commitBtn.enabled = YES;
                self.footView.hidden = NO;
            }else{
                self.commitBtn.enabled = NO;
                self.footView.hidden = YES;
                
            }
            return cell;
        }
        
    } else {
        
        static NSString *CellIdentifier = @"UITableViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = COLOR_666666;
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            
        }
        cell.backgroundColor = [UIColor clearColor];
        UIButton *cancelBtn = [self creatcancelBtn];
        [cell addSubview:cancelBtn];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == self.model.passengers.count&&indexPath.row >0) {
            NSString *commentStr = [NSString stringWithFormat:@"备注:%@",self.model.msg];
            
            CGSize textSize = [commentStr boundingRectWithSize:CGSizeMake(mainWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size;
            
            return textSize.height+20;
        }
        return 100;
    }
    if (self.model.status.intValue == 4 || self.model.status.intValue == 2) {
        return 40;
    }
    return CGFLOAT_MIN;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return CGFLOAT_MIN;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        if (self.model.status.intValue != 2) {
            return CGFLOAT_MIN;
        }
        return 10;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = BG_COLOR;
    
    return footerView;
    
}

//去支付
- (void)commitBtnAction:(UIButton *)sender {
    if (isEmptyString(self.model.orderid)) {
        Alert(@"获取订单信息失败");
        return;
    }
    if ([self.payPassWord isEqualToString:@""]) {
        //未设置支付密码
        //弹出警告框
        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"提示") message:Localized(@"您还没有设置支付密码，是否要设置支付密码？") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"取消") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"确认") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //跳转到设置支付密码界面
            [self pushChangePayView];
            
            
        }];
        [alertControl addAction:cancelAction];
        [alertControl addAction:okAction];
        [self presentViewController:alertControl animated:YES completion:nil];
        return;
    }
    
    
    self.pswView = [[PasswordAlertView alloc] initPasswordView];
    @weakify(self);
    self.pswView.passWordText = ^(NSString *text) {
        @strongify(self);
        NSLog(@"text=%@",text);
        if (text.length == 6) {
            [self payTrainOrderWithPwd:text];
            [self.pswView removeFromSuperview];
            
        }
    };
    //    self.pswView.forgetBlock = ^{
    //        @strongify(self);
    //        //跳转到设置支付密码界面
    //        [self forgetBtnAction];
    //        [self.pswView removeFromSuperview];
    //
    //    };
    [self.pswView passwordShow];
    
}

//跳转到设置支付密码界面
- (void)pushChangePayView {
    ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
    [self.navigationController pushViewController:oldPayViewController animated:YES];
}

#pragma mark - 忘记密码
- (void)forgetBtnAction {
    
    MissPayPswViewController *missVC = [[MissPayPswViewController alloc] init];
    [self.navigationController pushViewController:missVC animated:YES];
}
- (void)payTrainOrderWithPwd:(NSString *)pwd {
    NSString *encryptPassWord = [RSAEncryptor encryptString:pwd];
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_PayWithOrderid:self.model.user_orderid
                              pay_pwd:encryptPassWord
                              success:^(NSDictionary *dic, resultObject *state) {
                                  [XSTool hideProgressHUDWithView:wSelf.view];
                                  if (state.status) {
                                      Alert(state.info);
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTrainNotification" object:nil];
                                      [wSelf getOrderInfo:self.orderid];
                                      
                                  }else{
                                      Alert(state.info);
                                  }
                              } fail:^(NSError *error) {
                                  [XSTool hideProgressHUDWithView:wSelf.view];
                                  Alert(NetFailure);
                              }];
}

- (UIButton *)creatcancelBtn {
    CGFloat space = 18;
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(space,10, mainWidth - space*2, 50);
    if([self.model.status integerValue]==4){
         [Btn setTitle:Localized(@"退票") forState:UIControlStateNormal];
        [Btn addTarget:self action:@selector(refundBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }else{
         [Btn setTitle:@"取消订单" forState:UIControlStateNormal];
        [Btn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
   
    [Btn setBackgroundColor:Color(@"FFFFFF")];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 5;
    [Btn setTitleColor:mainColor forState:UIControlStateNormal];
    
    return Btn;
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
#pragma mark ----- 取消
- (void)cancelBtnAction:(UIButton*)sender
{

    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_train_CancelOrderWithOrderid:wSelf.model.ID
                                            success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             Alert(state.info);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTrainNotification" object:nil];
             [wSelf getOrderInfo:wSelf.orderid];
         }else{
             Alert(state.info);
         }

     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}

#pragma mark ----- 退票

- (void)refundBtnAction:(UIButton*)sender {
    if (!self.model.orderid) {
        return;
    }
    //拼接乘客
    NSMutableString *tempStr=[[NSMutableString alloc] init];
    for (TrainPassengersModel *model in self.model.passengers) {
        [tempStr appendString:model.passengerid];
        [tempStr appendString:@","];
    }
    [tempStr deleteCharactersInRange:NSMakeRange(tempStr.length-1, 1)];
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_RefundWithOrderid:self.model.ID passengers:tempStr
                                 success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             Alert(state.info);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTrainNotification" object:nil];
             [wSelf getOrderInfo:wSelf.orderid];
         }else{
             Alert(state.info);
         }
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}

@end
