//
//  TrainOrderVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderVC.h"
#import "TrainOrderListVC.h"
#import <HMSegmentedControl/HMSegmentedControl.h>

@interface TrainOrderVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) HMSegmentedControl *segmentControl;

@property (nonatomic, strong) TrainOrderListVC *allListVC;
@property (nonatomic, strong) TrainOrderListVC *waitPayListVC;
@property (nonatomic, strong) TrainOrderListVC *finishListVC;

@property (nonatomic, assign) CGFloat segmentHeight;

@end

@implementation TrainOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.segmentHeight = 44;
    self.title = @"我的订单";
    self.view.backgroundColor = BG_COLOR;
    [self setUpUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTrainOrder) name:@"refreshTrainNotification" object:nil];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];


}
- (void)refreshTrainOrder {
        [self.allListVC getOrderList];
        [self.waitPayListVC getOrderList];
        [self.finishListVC getOrderList];
    
}

- (void)setUpUI {

    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = mainColor;
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    [self setSubViews];
    
}





- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    for (UIViewController*vc in self.childViewControllers) {
        [vc.view endEditing:YES];
    }
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
    if (segmentedControl.selectedSegmentIndex == 0) {
        [self.allListVC getOrderList];
    }else  if (segmentedControl.selectedSegmentIndex == 1) {
        [self.waitPayListVC getOrderList];
    }else  if (segmentedControl.selectedSegmentIndex == 2) {
        [self.finishListVC getOrderList];
    }
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    for (UIViewController*vc in self.childViewControllers) {
        [vc.view endEditing:YES];
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
    
    if (page == 0) {
        [self.allListVC getOrderList];
    }else if (page == 1) {
        [self.waitPayListVC getOrderList];
    }else if (page == 2) {
        [self.finishListVC getOrderList];
    }
    
}

#pragma mark - self cycle

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        self.segmentHeight = 44;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.segmentHeight, mainWidth, mainHeight-self.segmentHeight)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*3, mainHeight-150);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        self.segmentHeight = 44;
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.segmentHeight)];
        _segmentControl.backgroundColor = Color(@"FFFFFF");
        _segmentControl.sectionTitles = @[Localized(@"全部"), Localized(@"待支付"), Localized(@"已成功")];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor hexFloatColor:@"1A3C57"]};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = [UIColor hexFloatColor:@"1A3C57"];
        _segmentControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:Color(@"666666"),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:15],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}


-(void) setSubViews{
    //根据选项变化位置和内容
  
    [self addChildViewController:self.allListVC];
    [self.scrollView addSubview:self.allListVC.view];
    
    [self addChildViewController:self.waitPayListVC];
    [self.scrollView addSubview:self.waitPayListVC.view];
    
    [self addChildViewController:self.finishListVC];
    [self.scrollView addSubview:self.finishListVC.view];
    
   
}

#pragma mark --懒加载


-(TrainOrderListVC *)allListVC
{
    if (!_allListVC) {
        _allListVC= [[TrainOrderListVC alloc] initWithTrainOrder:TrainOrderAll];
        _allListVC.view.frame = CGRectMake(mainWidth*0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _allListVC;
    
}

-(TrainOrderListVC *)waitPayListVC
{
    if (!_waitPayListVC) {
        _waitPayListVC= [[TrainOrderListVC alloc] initWithTrainOrder:TrainOrderWaitPay];
        _waitPayListVC.view.frame = CGRectMake(mainWidth*1, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _waitPayListVC;
    
}
-(TrainOrderListVC *)finishListVC
{
    if (!_finishListVC) {
        _finishListVC= [[TrainOrderListVC alloc] initWithTrainOrder:TrainOrderFinish];
        _finishListVC.view.frame = CGRectMake(mainWidth*2, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _finishListVC;
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTrainNotification" object:self];
}
@end
