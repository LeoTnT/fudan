//
//  TrainAddPassengerVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainAddPassengerVC.h"
#import "TrainPassporttyTypeSelectVC.h"
#import "HTTPManager+ThirdParty.h"

@interface TrainAddPassengerVC ()
@property (nonatomic,strong) UITextField *nameTF;
@property (nonatomic,strong) UIButton *normalBtn;//成人票
@property (nonatomic,strong) UIButton *childenBtn;//儿童票
@property (nonatomic,strong) UIButton *otherBtn;//残军票

@property (nonatomic,strong) UITextField *typeTF;
@property (nonatomic,strong) UITextField *cardNumTF;
@property (nonatomic,copy) NSString *passporttypeseid;
@property (nonatomic,copy) NSString *passporttypeseidname;

@property (nonatomic,assign) NSInteger selTypeIndex;//乘客类型选择
@property (nonatomic,strong) NSArray *PassengerArr;
@property (strong, nonatomic) NSMutableArray *btnArr;//乘客类型btn数组


@end

@implementation TrainAddPassengerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (isEmptyString(self.passporttypeseidname)) {
        
        if (self.model) {

            self.passporttypeseidname = self.model.passporttypeseidname;
            self.passporttypeseid = self.model.passporttypeseid;
        }else{
            self.passporttypeseidname = @"二代身份证";
            self.passporttypeseid = @"1";
        }
    }
    [self setUpUI];
    
}


#pragma mark - 设置子视图
-(void)setUpUI{
    
    if (self.model) {
        self.title = @"编辑乘客";
    }else{
        self.title = @"添加乘客";
    }
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    
    self.tableView.backgroundColor = BG_COLOR;
    [self.tableView reloadData];
}


#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }
    
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"UITableViewCell"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //            [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
            cell.textLabel.textColor = COLOR_666666;
            cell.textLabel.font = [UIFont systemFontOfSize:14.5];
        }
        NSArray *titleArr = @[@"乘客姓名",@"乘客类型",@"证件类型",@"证件号码"];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",titleArr[indexPath.row]];
        
        if (indexPath.row == 0) {
            CGRect tFrame =   CGRectMake(85, 0, mainWidth - 95, 44);
            if (!self.nameTF) {
                self.nameTF = [self creatTextFieldWithPlaceTilte:@"必填" WithFrame:tFrame];
                
                [cell addSubview:self.nameTF];
                if (self.model) {
                    self.nameTF.text = self.model.passengersename;
                }
            }
            
        }else if (indexPath.row == 1) {
            if (!self.btnArr) {
                self.btnArr = [NSMutableArray array];
                for (int i = 0; i<self.PassengerArr.count; i++) {
                    NSDictionary *dict = self.PassengerArr[i];
                    NSString *title =dict[@"piaotypename"];
                    
                    CGRect tFframe = CGRectMake(85 + 65*i, 11, 55, 22);
                    UIButton *btn = [self creatButtonWithTilte:title WithFrame:tFframe];
                    btn.tag = 100+i;
                    [cell addSubview:btn];
                    [self.btnArr addObject:btn];
                    
                    if (self.model) {
                        if (self.model.piaotype.intValue == 1 && i == 0) {//成人票
                                self.selTypeIndex = 0;
                                btn.selected = YES;
                                btn.layer.borderColor = mainColor.CGColor;
                        }else  if (self.model.piaotype.intValue == 2 && i == 1) {//儿童票
                            self.selTypeIndex = 1;
                            btn.selected = YES;
                            btn.layer.borderColor = mainColor.CGColor;
                        }else  if (self.model.piaotype.intValue == 4 && i == 2) {//残军票
                            self.selTypeIndex = 2;
                            btn.selected = YES;
                            btn.layer.borderColor = mainColor.CGColor;
                        }
                    }else{
                        if (i == 0) {
                            self.selTypeIndex = 0;
                            btn.selected = YES;
                            btn.layer.borderColor = mainColor.CGColor;
                            
                        }
                    }
                }
            }
            
        }else if (indexPath.row == 2) {
            
            if (!self.typeTF) {
                CGRect tFrame =   CGRectMake(85, 0, mainWidth-100, 44);
                self.typeTF = [self creatTextFieldWithPlaceTilte:@"" WithFrame:tFrame];
                //                self.typeTF.text =@"身份证";
                self.typeTF.enabled = NO;
                [cell addSubview:self.typeTF];
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (!isEmptyString(self.passporttypeseidname)) {
                self.typeTF.text = self.passporttypeseidname;
            }
            
        }else if (indexPath.row == 3) {
            
            CGRect tFrame =   CGRectMake(85, 0, mainWidth - 95, 44);
            if (!self.cardNumTF) {
                self.cardNumTF = [self creatTextFieldWithPlaceTilte:@"必填" WithFrame:tFrame];
                
                [cell addSubview:self.cardNumTF];
                if (self.model) {
                    self.cardNumTF.text = self.model.passportseno;
                }
            }
        }
        return cell;
        
    }else
    {
        static NSString *CellIdentifier = @"CellRegisterBtn";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
        }
        cell.backgroundColor = BG_COLOR;
        UIButton *registerBtn = [self creatFinishBtn];
        [cell addSubview:registerBtn];
        return cell;
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return 44;
    }
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            TrainPassporttyTypeSelectVC *vc =[[TrainPassporttyTypeSelectVC alloc] init];
            @weakify(self);
            vc.selBlock = ^(NSString *passporttypeseidname, NSString *passporttypeseid) {
                @strongify(self);
                self.passporttypeseid = passporttypeseid;
                self.passporttypeseidname = passporttypeseidname;
                if (passporttypeseidname) {
                    self.typeTF.text = passporttypeseidname;
                }
//                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //    if (section == 0) {
    //        return 10;
    //    }
    return CGFLOAT_MIN;
}




#pragma mark --BtnAction
//完成
- (void)finishBtnAction:(UIButton*)sender
{
    if (isEmptyString(self.nameTF.text)) {
        Alert(@"请输入乘客姓名");
        return;
    }

    if (isEmptyString(self.cardNumTF.text)) {
        Alert(@"请输入证件号码");
        return;
    }
    
    if (self.model) {//编辑乘客
        [self editPassenger];
    }else{//增加乘客
        [self AddPassenger];
    }
    


    
}

- (void)editPassenger
{
    NSDictionary *tDic = self.PassengerArr[self.selTypeIndex];
    //编辑乘客
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_EditPassengerWithID:self.model.ID
                           passengersename:self.nameTF.text
                                  piaotype:tDic[@"piaotype"]
                              piaotypename:tDic[@"piaotypename"]
                          passporttypeseid:self.passporttypeseid
                      passporttypeseidname:self.passporttypeseidname
                              passportseno:self.cardNumTF.text
                                   success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             Alert(state.info);
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self.navigationController popViewControllerAnimated:YES];
             });
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}
- (void)AddPassenger
{
    NSDictionary *tDic = self.PassengerArr[self.selTypeIndex];

    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_AddPassengerWithPassengersename:self.nameTF.text
                                              piaotype:tDic[@"piaotype"]
                                          piaotypename:tDic[@"piaotypename"]
                                      passporttypeseid:self.passporttypeseid
                                  passporttypeseidname:self.passporttypeseidname
                                          passportseno:self.cardNumTF.text
                                               success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             Alert(state.info);
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self.navigationController popViewControllerAnimated:YES];
             });
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}

//乘客类型选择
- (void)customBtnAction:(UIButton*)sender
{
    for (UIButton*btn in self.btnArr) {
        if (sender == btn) {
            sender.selected = YES;
            self.selTypeIndex = sender.tag-100;
            sender.layer.borderColor = mainColor.CGColor;
            
        }else{
            btn.selected = NO;
            btn.layer.borderColor = [UIColor grayColor].CGColor;
            
        }
    }
    
}





- (UIButton *)creatFinishBtn
{
    CGFloat space = 18;
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(space,22, mainWidth - space*2, 50);
    [Btn setTitle:Localized(@"完成") forState:UIControlStateNormal];
    [Btn setBackgroundColor:mainColor];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 5;
    [Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(finishBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    return Btn;
}

- (UIButton *)creatButtonWithTilte:(NSString*)title WithFrame:(CGRect)frame
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    customBtn.frame = frame;
    customBtn.layer.masksToBounds = YES;
    customBtn.selected = NO;
    customBtn.layer.borderColor = [UIColor grayColor].CGColor;
    customBtn.layer.borderWidth = 1;
    customBtn.layer.cornerRadius = 1.5;
    [customBtn setTitleColor:mainColor forState:UIControlStateSelected];
    [customBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    customBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [customBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [customBtn setTitle:title forState:UIControlStateNormal];
    return customBtn;
}

- (UITextField *)creatTextFieldWithPlaceTilte:(NSString*)title WithFrame:(CGRect)frame
{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.textAlignment = NSTextAlignmentLeft;
    textField.placeholder = Localized(title);
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:15];
    textField.delegate = self;
    //    textField.adjustsFontSizeToFitWidth=YES;
    UILabel *placeholderLabel = [textField valueForKeyPath:@"_placeholderLabel"];
    placeholderLabel.numberOfLines = 0;
    return textField;
}

//     1 :成人票,2 :儿童票,4 :残军票  没有学生票只有这三种
-(NSArray *)PassengerArr
{
    if (!_PassengerArr) {
        _PassengerArr =  @[@{@"piaotype":@"1",@"piaotypename":@"成人票"},
                           @{@"piaotype":@"2",@"piaotypename":@"儿童票"},
                           @{@"piaotype":@"4",@"piaotypename":@"残军票"}];
    }
    return _PassengerArr;
}
@end
