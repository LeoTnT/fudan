//
//  TrainOrderDetialVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainOrderModel.h"

@interface TrainOrderDetialVC : XSBaseTableViewController
@property (nonatomic,strong) TrainOrderModel * model;
@property (nonatomic,strong) NSString * orderid;
@property (nonatomic,assign) BOOL isWait;

@end
