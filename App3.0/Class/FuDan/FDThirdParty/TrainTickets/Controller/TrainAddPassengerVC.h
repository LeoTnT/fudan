//
//  TrainAddPassengerVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"
@interface TrainAddPassengerVC : XSBaseTableViewController
@property (nonatomic,strong) TrainPassengerModel *model;

@end
