//
//  TrainPassengerListVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainPassengerListVC.h"
#import "TrainPassengerListCell.h"
#import "TrainAddPassengerVC.h"
#import "TrainAddPassengerVC.h"
#import "HTTPManager+ThirdParty.h"

@interface TrainPassengerListVC ()<TrainPassengerListCellDelegate>

@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, assign) BOOL isSureBtn;
@property (strong, nonatomic) NSMutableArray *selectIndexs; //多选选中的行
@end

@implementation TrainPassengerListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self SetUpUI];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self GetPassenger];

}

- (void)SetUpUI
{
    self.isSureBtn = NO;
    self.title = @"选择乘客";
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStyleGrouped;
    //    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(mainWidth);
        make.bottom.mas_equalTo(0);
    }];
    
        __weak __typeof__(self) wSelf = self;
        self.showRefreshHeader = YES;
        self.showRefreshFooter = NO;
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            wSelf.page = 1;
            [wSelf GetPassenger];
        }];
    
//        self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            wSelf.page++;
//            [wSelf queryOrder];
//        }];
//        [self.tableView.mj_header beginRefreshing];
    
    
    
}
- (void)creatRightSureBtn
{
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"确定" action:^{
        if (self.listArr.count >0) {
            NSMutableArray *mArr = [NSMutableArray array];
            for (NSIndexPath *indexPath in self.selectIndexs) {
                TrainPassengerModel *model = self.listArr[indexPath.row];
                [mArr addObject:model];
            }
            if (self.selPassengerBlock) {
                self.selPassengerBlock((NSArray*)mArr);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)GetPassenger
{
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_GetPassengerSuccess:^(NSDictionary *dic, resultObject *state)
     {
         [wSelf tableViewEndRefreshing];
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             self.listArr = [NSMutableArray arrayWithArray:[TrainPassengerModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"data"]]];
             if (self.listArr.count >0 && !self.isSureBtn) {
                 [self creatRightSureBtn];
             }
             
             for (int i = 0; i<self.listArr.count; i++) {
                 TrainPassengerModel*tModel = self.listArr[i];
                 for (int j = 0; j<self.selPassengerArr.count; j++) {
                     TrainPassengerModel*selModel = self.selPassengerArr[j];
                     if (tModel.ID == selModel.ID  && tModel.piaotype == selModel.piaotype) {
                         NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
                         [wSelf.selectIndexs addObject:indexPath]; //添加索引数据到数组
                     }
                 }
             }
             
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [wSelf tableViewEndRefreshing];
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return self.listArr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"UITableViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        if (!self.addBtn) {
            self.addBtn = [self creatAddBtn];
            [cell addSubview:self.addBtn];
        }
        
        return cell;
    }else{
        static NSString *CellIdentifier = @"TrainPassengerListCell";
        TrainPassengerListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[TrainPassengerListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cell.checkBtn.tag = indexPath.row;
        cell.delegate = self;
        if (self.listArr.count >0) {
            cell.model = self.listArr[indexPath.row];
            cell.checkBtn.selected = NO;
            for (NSIndexPath *index in self.selectIndexs) {
                if (indexPath == index) {
                    cell.checkBtn.selected = YES; //切换为选中
                }
            }
        }
        

   
        return cell;
    }
    
}


// 只要实现了这个方法，左滑出现Delete按钮的功能就有了
// 点击了“左滑出现的Delete按钮”会调用这个方法
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

   
    TrainPassengerModel *delModel = self.listArr[indexPath.row];
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_DelPassengerWithID:delModel.ID
                                  success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             
             //删除选中的数据
             for (int i = 0; i<self.listArr.count; i++) {
                 TrainPassengerModel*tModel = self.listArr[i];
                 if (delModel.ID == tModel.ID  && delModel.piaotype == tModel.piaotype) {
                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
                     if ([self.selectIndexs containsObject:indexPath]) {
                         [self.selectIndexs removeObject:indexPath]; //数据移除
                     }
                 }
             }
             
             // 删除模型
             [self.listArr removeObjectAtIndex:indexPath.row];
             // 刷新
             [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
             
             [wSelf GetPassenger];
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
   
}
// 修改Delete按钮文字为“删除”
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

-(void)deleteActionWithID:(NSString *)ID{
    
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 44;
    }
    return 63;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
         TrainPassengerModel *passengerModel = self.listArr[indexPath.row];
        TrainAddPassengerVC *vc =[[TrainAddPassengerVC alloc] init];
        vc.model = passengerModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
   

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }else{
        return CGFLOAT_MIN;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = BG_COLOR;
    
    return footerView;
    
}

#pragma mark ---TrainPassengerListCellDelegate
//选择
-(void)checkBtnClick:(UIButton *)sender
{
//    sender.selected = !sender.selected;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:1];
    if (sender.selected) { //如果为选中状态
       sender.selected = NO; //切换为未选中
        [self.selectIndexs removeObject:indexPath]; //数据移除
    }else { //未选中
       sender.selected = YES; //切换为选中
        [self.selectIndexs addObject:indexPath]; //添加索引数据到数组
    }
    
    
    
}
//新增乘客
-(void)addBtnAction:(UIButton *)sender
{
    TrainAddPassengerVC *vc =[[TrainAddPassengerVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (UIButton *)creatAddBtn
{
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(0,0, mainWidth, 44);
    [Btn setTitle:Localized(@" 新增乘客") forState:UIControlStateNormal];
    [Btn setImage:[UIImage imageNamed:@"Train_add"] forState:UIControlStateNormal];
    [Btn setTitleColor:mainColor forState:UIControlStateNormal];
    [Btn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [Btn addTarget:self action:@selector(addBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    return Btn;
}

#pragma Mark ------懒加载------

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}
-(NSMutableArray *)selectIndexs
{
    if (!_selectIndexs) {
        _selectIndexs = [NSMutableArray array];
    }
    return _selectIndexs;
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}


@end

