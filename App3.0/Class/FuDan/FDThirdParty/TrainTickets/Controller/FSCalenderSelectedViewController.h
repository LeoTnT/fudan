//
//  FSCalenderSelectedViewController.h
//  App3.0
//
//  Created by xinshang on 2018/3/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FSCalenderSelectedViewController : XSBaseViewController

@property (nonatomic,copy) void(^dateBlock)(NSDate *date);

@end

NS_ASSUME_NONNULL_BEGIN

@interface LunarFormatter : NSObject

- (NSString *)stringFromDate:(NSDate *)date;

@end

NS_ASSUME_NONNULL_END
