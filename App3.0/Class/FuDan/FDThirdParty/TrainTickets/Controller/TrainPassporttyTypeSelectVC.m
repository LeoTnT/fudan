//
//  TrainPassporttyTypeSelectVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainPassporttyTypeSelectVC.h"

@interface TrainPassporttyTypeSelectVC ()
@property (nonatomic, strong) NSArray *listArr;
@property (nonatomic, assign) NSInteger selIndex;

@end

@implementation TrainPassporttyTypeSelectVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self SetUpUI];
//    1:二代身份证,2:一代身份证,C:港澳通行证,B:护照,G:台湾通行证
}



- (void)SetUpUI
{
    if (!self.selIndex) {
        self.selIndex = -1;
    }
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.title = @"选择证件类型";
    self.listArr = @[@{@"passporttypeseid":@"1",@"passporttypeseidname":@"二代身份证"},
                     @{@"passporttypeseid":@"2",@"passporttypeseidname":@"一代身份证"},
                     @{@"passporttypeseid":@"C",@"passporttypeseidname":@"港澳通行证"},
                     @{@"passporttypeseid":@"B",@"passporttypeseidname":@"护照"},
                     @{@"passporttypeseid":@"G",@"passporttypeseidname":@"台湾通行证"}];
    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
    //        self.edgesForExtendedLayout = UIRectEdgeNone;
    //    }
    //    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(0);
    //        make.top.mas_equalTo(0);
    //        make.width.mas_equalTo(mainWidth);
    //        make.bottom.mas_equalTo(0);
    //    }];
    [self.tableView reloadData];
  
    
}


#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return self.listArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = Color(@"111111");
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
    }
    
        if (self.listArr.count >0) {
            NSDictionary *dict = self.listArr[indexPath.row];
            cell.textLabel.text = dict[@"passporttypeseidname"];
            
            if (self.selIndex == indexPath.row) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
   
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listArr.count >0) {
        NSDictionary *dict = self.listArr[indexPath.row];
        self.selIndex = indexPath.row;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        if (self.selBlock) {
            self.selBlock(dict[@"passporttypeseidname"], dict[@"passporttypeseid"]);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = BG_COLOR;
    return view;
}





@end

