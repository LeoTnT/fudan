//
//  TrainListDetailVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"
@interface TrainListDetailVC : XSBaseTableViewController

@property (nonatomic,copy) NSDate * selDate;//选中的日期
@property (nonatomic,strong) TrainInfoModels *selModel;
@end
