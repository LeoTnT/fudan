//
//  TrainOrderPreviewVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainListDetailHeadView.h"

@interface TrainOrderPreviewVC : XSBaseTableViewController
@property (nonatomic,strong) NSDate * selDate;//选中的日期
@property (nonatomic,strong) TrainInfoModels *selModel;
@property (nonatomic,strong) NSArray *typeArr;

@property (nonatomic,copy) NSString * nameStr;//选中的票类型
@property (nonatomic,copy) NSString * priceStr;//选中的票价格

@end
