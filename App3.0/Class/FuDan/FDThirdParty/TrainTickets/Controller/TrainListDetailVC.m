//
//  TrainListDetailVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListDetailVC.h"
#import "TrainListDetailHeadView.h"
#import "FSCalenderSelectedViewController.h"
#import "TrainListDetailCell.h"
#import "TrainOrderPreviewVC.h"
#import "HTTPManager+ThirdParty.h"

@interface TrainListDetailVC ()<TrainListDetailHeadViewDelegate,TrainListDetailCellDelegate>
@property (nonatomic, strong) TrainListDetailHeadView *train_headView;//头部界面
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSArray *allArr;//筛选用的全部数组
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) NSArray *typeArr;


@end

@implementation TrainListDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self SetUpUI];
}


//充值记录查询
- (void)queryOrder
{
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate];
    if (isEmptyString(dateStr)) {
        Alert(@"请选择日期");
        return;
    }
    if (isEmptyString(self.selModel.from_station_name)) {
        Alert(@"请选择出发站");
        return;
    }
    if (isEmptyString(self.selModel.to_station_name)) {
        Alert(@"请选择到达站");
        return;
    }

    NSDictionary *param = @{@"train_date":dateStr,
                            @"from_station":self.selModel.from_station_code,
                            @"to_station":self.selModel.to_station_code
                            };
    
    [self.listArr removeAllObjects];
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager  train_QueryTicketsWithDic:param
                                    success:^(NSDictionary *dic, resultObject *state)
     {
         @strongify(self);

         [self tableViewEndRefreshing];
         if (state.status) {
             NSArray *array = dic[@"data"][@"list"];
             if (array.count >0) {
                 self.allArr = [NSArray arrayWithArray:[TrainInfoModels mj_objectArrayWithKeyValuesArray:array]];
             }
             TrainInfoModels *tSelModel;
             for (TrainInfoModels*tModel in self.allArr) {
                 if (tModel.train_code == self.selModel.train_code) {
                     tSelModel = tModel;
                 }
             }
             if (tSelModel) {
                 self.selModel = tSelModel;
                 [self setDataReloadTablewView];
             }
             
         }else{
             Alert(state.info);
         }
         [self.tableView reloadData];
     } fail:^(NSError *error) {
         [self tableViewEndRefreshing];
         Alert(NetFailure);
     }];
}

- (void)SetUpUI
{
    
    //头部train_headView
    [self.view addSubview:self.train_headView];
    self.train_headView.dateView.hidden = NO;
    self.train_headView.ZW_View.hidden = YES;
    self.train_headView.delegate = self;
    [self.train_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(215);
    }];
    
    self.train_headView.selDate = self.selDate;
    self.train_headView.model= self.selModel;

    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    //    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.train_headView.mas_bottom).offset(12);
        make.width.mas_equalTo(mainWidth);
        make.bottom.mas_equalTo(0);
    }];
    
//    __weak __typeof__(self) wSelf = self;
//    self.showRefreshHeader = YES;
//    self.showRefreshFooter = NO;
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //        wSelf.page = 1;
//        [wSelf queryOrder];
//    }];
    
    //    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
    //        wSelf.page++;
    //        [wSelf queryOrder];
    //    }];
//    [self.tableView.mj_header beginRefreshing];
    

    
}




#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TrainListDetailCell";
    TrainListDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TrainListDetailCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    cell.delegate = self;
    if (self.listArr) {
        cell.model = self.selModel;
        cell.tDic = self.listArr[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

#pragma mark ---TrainListDetailCellDelegate
-(void)orderBtnClickNameStr:(NSString *)nameStr priceStr:(NSString *)priceStr;
{
    TrainOrderPreviewVC *vc =[[TrainOrderPreviewVC alloc] init];
    vc.typeArr = self.listArr;
    vc.nameStr = nameStr;
    vc.priceStr = priceStr;
    vc.selDate = self.selDate;
    vc.selModel = self.selModel;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark ---TrainListDetialHeadViewDelegate
//返回
-(void)backBtnClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//前一天
-(void)frontBtnClick:(UIButton *)sender
{
    NSDate *frontDate = [XSTool dateStringFromDate:self.selDate year:0 month:0 day:-1];
    self.selDate = frontDate;
    self.train_headView.selDate = self.selDate;
    [self queryOrder];
}
//后一天
-(void)behindBtnClick:(UIButton *)sender
{
    NSDate *behindDate = [XSTool dateStringFromDate:self.selDate year:0 month:0 day:1];
    self.selDate = behindDate;
    self.train_headView.selDate = self.selDate;
    [self queryOrder];
}
//日历
-(void)dateBtnClick:(UIButton *)sender
{
    
    FSCalenderSelectedViewController *vc =[[FSCalenderSelectedViewController alloc] init];
    @weakify(self);
    vc.dateBlock = ^(NSDate *date) {
        @strongify(self);
        if (date) {
            self.selDate = date;
        }
        self.train_headView.selDate = self.selDate;
        [self queryOrder];
    };
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(NSDateFormatter *)dateFormatter{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return _dateFormatter;
    
}

#pragma Mark ------懒加载------

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}

-(NSArray *)allArr{
    if (!_allArr) {
        _allArr = [NSArray array];
    }
    return _allArr;
    
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

-(void)setSelModel:(TrainInfoModels *)selModel
{
    _selModel = selModel;
    
    [self setDataReloadTablewView];

}

- (void)setDataReloadTablewView
{
    if (!self.selModel) {
        [self.listArr removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    NSArray *Properties = [self.selModel getAllProperties];
    NSDictionary *PropertDic = [self.selModel propertieschangeToDic];
    
    // 可变数组--获取座位类型数组
    NSMutableArray *mArr = [NSMutableArray array];
    for (NSDictionary *tDic in self.typeArr) {
        for (NSString *str2 in Properties) {
            NSString *str1 =tDic.allKeys.firstObject;
            if ([str1 isEqualToString:str2]) {
                NSString *numStr = [PropertDic objectForKey:str1];
                if (![numStr isEqualToString:@"--"]) {
                    [mArr addObject:tDic];
                }
            }
        }
    }
    if ( self.train_headView) {
        self.train_headView.selDate = self.selDate;
        self.train_headView.model = self.selModel;
    }
    self.listArr = mArr;
    [self.tableView reloadData];

}


-(NSArray *)typeArr
{
    if (!_typeArr) {
        _typeArr = @[@{@"edz_num":@"二等座"},
                     @{@"ydz_num":@"一等座"},
                     @{@"tdz_num":@"特等座"},
                     @{@"swz_num":@"商务座"},
                     @{@"rz_num":@"软座"},
                     @{@"dw_num":@"动卧"},
                     @{@"yz_num":@"硬座"},
                     @{@"yw_num":@"硬卧"},
                     @{@"rw_num":@"软卧"},
                     @{@"wz_num":@"无座"},
                     @{@"gjrw_num":@"高级软卧"},
                     @{@"qtxb_num":@"其他"}
                     ];
    }
    return _typeArr;
}
-(TrainListDetailHeadView *)train_headView
{
    if (!_train_headView) {
        _train_headView = [[TrainListDetailHeadView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 215)];
    }
    return _train_headView;
    
}

@end
