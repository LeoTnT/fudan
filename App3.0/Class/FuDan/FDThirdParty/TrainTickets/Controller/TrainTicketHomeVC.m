//
//  TrainTicketHomeVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainTicketHomeVC.h"
#import "FSCalenderSelectedViewController.h"
#import "TrainCitySelectedVC.h"
#import "TrainListVC.h"
#import "TrainOrderVC.h"
#import "AirTicketListVC.h"
#import "AirTicketSelectedVC.h"

@interface TrainTicketHomeVC ()<SDCycleScrollViewDelegate>
@property (nonatomic,strong) UIButton * backBtn;//返回按钮
@property (nonatomic,strong) UIButton * orderBtn;//订单
@property (nonatomic, strong) SDCycleScrollView *adAdvier;// 顶部录播图

@property (nonatomic,strong) UIView * checkView;//背景视图 ||
@property (nonatomic,strong) UITextField * startTF;//出发站
@property (nonatomic,strong) UITextField * endTF;//到达站
@property (nonatomic,strong) UIButton * exchangeBtn;//交换
@property (nonatomic,strong) UIButton * dateBtn;//日期
@property (nonatomic,strong) UILabel * dateLb;//日期

@property (nonatomic,strong) UIButton * studuteBtn;//学生
@property (nonatomic,strong) UIButton * otherBtn;//高铁动车

@property (nonatomic,strong) UIButton * checkBtn;//查询

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic,copy) NSDate * selDate;//选中的日期
@property (nonatomic,strong) TrainCityModels * selStartModel;//选中出发站
@property (nonatomic,strong) TrainCityModels * selEndModel;//选中到达站

@property (nonatomic,strong) UIView * bgView1;//


@property (nonatomic,assign) BOOL isSelectedG;//勾选高铁

@property (nonatomic,strong) UIImageView * changeBgView;//火车飞机切换背景
@property (nonatomic,strong) UIButton * trainBtn;//火车
@property (nonatomic,strong) UIButton * airBtn;//飞机

@property(strong, nonatomic) UIView *trainBg;
@property(strong, nonatomic) UIView *airBg;
@property(strong, nonatomic) UIView *childBgV;
@property(strong, nonatomic) UIView *babyBgV;
@property (nonatomic,strong) UIButton * childBtn;//儿童
@property (nonatomic,strong) UIButton * babyBtn;//婴儿
@property(strong, nonatomic) NSString *hasChild;//是否携带儿童
@property(strong, nonatomic) NSString *hasBaby;//是否携带婴儿
@end

@implementation TrainTicketHomeVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"火车票";
    [self setUpUI];
    [self getAdInfo];

}
//火车票广告位 app_train_index
- (void)getAdInfo{
    RACSignal *signal = [XSHTTPManager rac_POSTURL:Url_GetMallADInfo params:@{@"flag_str":@"app_train_index"}];
    [signal subscribeNext:^(resultObject *state) {
        if (state.status) {
            NSMutableArray *arr = [NSMutableArray array];
            NSMutableArray *imageUrlDataSource = [NSMutableArray array];
            //            NSArray *aaa = state.data[@"mobile_index_slide"];
            NSArray *aaa = state.data[@"app_train_index"];
            [aaa.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                TabMallADItem *item=[TabMallADItem mj_objectWithKeyValues:x];
                [arr addObject:item];
                [imageUrlDataSource addObject:item.image];
            }completed:^{
//                self.adAdvier = arr;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.adAdvier.imageURLStringsGroup = imageUrlDataSource;
                });
                
            }];
        }
        
        
    }];
}

-(NSDate *)selDate
{
    if (!_selDate) {
        _selDate = [NSDate date];
    }
    return _selDate;
}
- (void)setUpUI{
    self.isSelectedG = NO;
    //头部视图
    CGFloat space = 10.0;
    CGFloat lbH = 35.0;
    CGFloat imgW = 35.0;
    CGFloat bgH = 325.0;
    CGFloat bgview1H = 55.0;
    
    self.view.backgroundColor = BG_COLOR;
    //轮播图
    [self.view addSubview:self.adAdvier];
//    self.adAdvier.imageURLStringsGroup = @[@"img1",@"img2"];
//    self.adAdvier.localizationImageNamesGroup = @[@"A1_bg1",@"D1_bgImg"];
    
    //返回按钮
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backBtn setImage:[UIImage imageNamed:@"ticket_back"] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:self.backBtn];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(space*3);
        make.left.mas_equalTo(space*1.2);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    //订单按钮
    self.orderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.orderBtn setImage:[UIImage imageNamed:@"ticket_order"] forState:UIControlStateNormal];
    [self.orderBtn addTarget:self action:@selector(orderBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:self.orderBtn];
    [self.orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.right.mas_equalTo(-space*1.2);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    
    //_checkView
    _checkView = [[UIView alloc]init];
    _checkView.backgroundColor = [UIColor whiteColor];
    _checkView.layer.masksToBounds = YES;
    _checkView.layer.cornerRadius = 5;
    [self.view addSubview:_checkView];
    [_checkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*1.3);
        make.right.mas_equalTo(-space*1.3);
        make.top.mas_equalTo( self.adAdvier.mas_bottom).offset(-45);
        make.height.mas_equalTo(bgH);
    }];
    
    _changeBgView = [UIImageView new];
    _changeBgView.userInteractionEnabled = YES;
    _changeBgView.image = [UIImage imageNamed:@"ticket_change_bg"];
    [_checkView addSubview:_changeBgView];
    [_changeBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(55);
    }];
    
    _trainBtn = [BaseUITool buttonWithTitle:@"火车票" titleColor:[UIColor hexFloatColor:@"ffffff" alpha:0.7] font:SYSTEM_FONT(18) superView:_changeBgView];
    [_trainBtn setImage:[UIImage imageNamed:@"ticket_change_arrow"] forState:UIControlStateSelected];
    [_trainBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_trainBtn setImageEdgeInsets:UIEdgeInsetsMake(50, 0, 0, -_trainBtn.titleLabel.intrinsicContentSize.width-5)];
    [_trainBtn addTarget:self action:@selector(changeTicket:) forControlEvents:UIControlEventTouchUpInside];
    [_trainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.size.mas_equalTo(CGSizeMake(120, 55));
        make.centerY.mas_equalTo(_changeBgView);
    }];
    _trainBtn.selected = YES;
    
    _airBtn = [BaseUITool buttonWithTitle:@"飞机票" titleColor:[UIColor hexFloatColor:@"ffffff" alpha:0.7] font:SYSTEM_FONT(18) superView:_changeBgView];
    [_airBtn setImage:[UIImage imageNamed:@"ticket_change_arrow"] forState:UIControlStateSelected];
    [_airBtn setImageEdgeInsets:UIEdgeInsetsMake(50, 0, 0, -_airBtn.titleLabel.intrinsicContentSize.width-5)];
    [_airBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_airBtn addTarget:self action:@selector(changeTicket:) forControlEvents:UIControlEventTouchUpInside];
    [_airBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-40);
        make.size.mas_equalTo(_trainBtn);
        make.centerY.mas_equalTo(_changeBgView);
    }];
    
    //bgView1
    _bgView1 = [[UIView alloc]init];
    [_checkView addSubview:_bgView1];
    [_bgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_changeBgView.mas_bottom).offset(12);
        make.height.mas_equalTo(bgview1H);
    }];
    
    
    //_startTF
    _startTF = [[UITextField alloc] init];
    _startTF.textAlignment = NSTextAlignmentLeft;
    _startTF.placeholder = @"出发站";
    _startTF.font = [UIFont systemFontOfSize:17];
    _startTF.enabled = NO;
    [_bgView1 addSubview:_startTF];
    [_startTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH);
    }];
    
    
    //_endTF
    _endTF = [[UITextField alloc] init];
    _endTF.placeholder = @"到达站";
    _endTF.enabled = NO;
    _endTF.textAlignment = NSTextAlignmentRight;
    _endTF.font = [UIFont systemFontOfSize:17];
    [_bgView1 addSubview:_endTF];
    [_endTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH);
    }];
    
    // 出发站
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startBtn.tag = 101;
    [startBtn addTarget:self action:@selector(endOrStartBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:startBtn];
    [startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH*1.2);
    }];
    
    // 到达站
    UIButton *endtBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    endtBtn.tag = 102;
    [endtBtn addTarget:self action:@selector(endOrStartBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:endtBtn];
    [endtBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(_bgView1);
        make.width.mas_equalTo(mainWidth/3);
        make.height.mas_equalTo(lbH*1.2);
    }];
    
    
    
    // 交换按钮
    self.exchangeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.exchangeBtn setImage:[UIImage imageNamed:@"ticket_exchange"] forState:UIControlStateNormal];
    [self.exchangeBtn addTarget:self action:@selector(exchangeBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView1 addSubview:self.exchangeBtn];
    [self.exchangeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(_bgView1);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(35);
    }];
    //线1
    UIView *_line1 = [UIView new];
    [_checkView addSubview:_line1];
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_bgView1.mas_bottom).offset(-0.8);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(0.8);
        
    }];
    _line1.backgroundColor = BG_COLOR;
    
    
    //_dateLb
    _dateLb = [[UILabel alloc] init];
    _dateLb.font = [UIFont systemFontOfSize:17];
    [_checkView addSubview:_dateLb];
    [_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_line1).offset(space);
        make.width.mas_equalTo(mainWidth-40);
        make.height.mas_equalTo(lbH);
    }];
    
    
    
    //
    self.dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dateBtn addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [_checkView addSubview:self.dateBtn];
    [self.dateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_line1).offset(space);
        make.width.mas_equalTo(mainWidth-40);
        make.height.mas_equalTo(lbH);
    }];
    
    
    
    //线1
    UIView *_line2 = [UIView new];
    [_checkView addSubview:_line2];
    [_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_dateLb.mas_bottom).offset(4);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(0.8);
        
    }];
    _line2.backgroundColor = BG_COLOR;
    
    
    
    
    //bgView2
    UIView *bgView2 = [[UIView alloc]init];
    [_checkView addSubview:bgView2];
    self.trainBg = bgView2;
    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_line2.mas_bottom);
        make.height.mas_equalTo(bgview1H);
    }];
    
    // 学生票
    self.studuteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.studuteBtn setImage:[UIImage imageNamed:@"ticket_select"] forState:UIControlStateNormal];
    [self.studuteBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    [self.studuteBtn setTitle:@"学生票" forState:UIControlStateNormal];
    [self.studuteBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [self setImageToRightWithButton:self.studuteBtn];
    [bgView2 addSubview:self.studuteBtn];
    [self.studuteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(35);
    }];
    self.studuteBtn.hidden = YES;
    
    // 高铁动车
    self.otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.otherBtn setImage:[UIImage imageNamed:@"ticket_select"] forState:UIControlStateSelected];
    [self.otherBtn setImage:[UIImage imageNamed:@"ticket_noselect"] forState:UIControlStateNormal];
    [self.otherBtn setTitle:@"高铁动车" forState:UIControlStateNormal];
    [self.otherBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
    [self.otherBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [self setImageToRightWithButton:self.otherBtn];
    [self.otherBtn addTarget:self action:@selector(otherBtnAction:) forControlEvents:UIControlEventTouchUpInside ];

    [bgView2 addSubview:self.otherBtn];
    [self.otherBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(35);
    }];
    
    //bgView2
    UIView *bgView3 = [[UIView alloc] init];
    [_checkView addSubview:bgView3];
    self.airBg = bgView3;
    bgView3.hidden = YES;
    [bgView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_line2.mas_bottom);
        make.height.mas_equalTo(bgview1H);
    }];
    /********************** (携带儿童) **********************/
    //按钮背景图
    UIView *v1 = [[UIView alloc] init];
    v1.backgroundColor = [UIColor whiteColor];
    
    self.childBgV = v1;
    [bgView3 addSubview:v1];
    [v1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(bgView2);
    }];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(childBtnClick)];
    [v1 addGestureRecognizer:tap1];
    
    // 携带儿童
    self.childBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.childBtn setImage:[UIImage imageNamed:@"ticket_noselect"] forState:UIControlStateNormal];
    [self.childBtn setImage:[UIImage imageNamed:@"ticket_select"] forState:UIControlStateSelected];
    [self.childBtn addTarget:self action:@selector(childBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [v1 addSubview:self.childBtn];
    [self.childBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v1);
        make.centerY.mas_equalTo(v1);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = @"携带儿童";
    label1.font = [UIFont systemFontOfSize:15];
    label1.textColor = [UIColor hexFloatColor:@"111111"];
    [v1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(v1).offset(10);
        make.left.equalTo(self.childBtn.mas_right).offset(5);
        make.right.equalTo(v1);
        make.height.mas_equalTo(15);
    }];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = @"2-12岁";
    label2.font = [UIFont systemFontOfSize:13];
    label2.textColor = [UIColor hexFloatColor:@"717171"];
    [v1 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(v1).offset(-10);
        make.left.equalTo(self.childBtn.mas_right).offset(5);
        make.right.equalTo(v1);
        make.height.mas_equalTo(15);
    }];
    
    /********************** (携带婴儿) **********************/
    //按钮背景图
    UIView *v2 = [[UIView alloc] init];
    v2.backgroundColor = [UIColor whiteColor];
    self.babyBgV = v2;
    [bgView3 addSubview:v2];
    [v2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v1.mas_right);
        make.centerY.mas_equalTo(bgView2);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(bgView2);
    }];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(babyBtnClick)];
    [v2 addGestureRecognizer:tap2];
    
    // 携带婴儿
    self.babyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.babyBtn setImage:[UIImage imageNamed:@"ticket_noselect"] forState:UIControlStateNormal];
    [self.babyBtn setImage:[UIImage imageNamed:@"ticket_select"] forState:UIControlStateSelected];
    [self.babyBtn addTarget:self action:@selector(babyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [v2 addSubview:self.babyBtn];
    [self.babyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v2);
        make.centerY.mas_equalTo(v2);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.text = @"携带婴儿";
    label3.font = [UIFont systemFontOfSize:15];
    label3.textColor = [UIColor hexFloatColor:@"111111"];
    [v2 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(v2).offset(10);
        make.left.equalTo(self.babyBtn.mas_right).offset(5);
        make.right.equalTo(v2);
        make.height.mas_equalTo(15);
    }];
    
    UILabel *label4 = [[UILabel alloc] init];
    label4.text = @"14天-2岁";
    label4.font = [UIFont systemFontOfSize:13];
    label4.textColor = [UIColor hexFloatColor:@"717171"];
    [v2 addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(v2).offset(-10);
        make.left.equalTo(self.babyBtn.mas_right).offset(5);
        make.right.equalTo(v2);
        make.height.mas_equalTo(15);
    }];
    
    // 查询按钮
    self.checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkBtn setTitle:@"查询" forState:UIControlStateNormal];
    [self.checkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.checkBtn addTarget:self action:@selector(checkBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    
    self.checkBtn.layer.masksToBounds = YES;
    self.checkBtn.layer.cornerRadius = 4;
    self.checkBtn.backgroundColor = mainColor;
    [_checkView addSubview:self.checkBtn];
    [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView2.mas_bottom).offset(space*1.4);
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(bgview1H*0.9);
    }];
    
    [self setDateLbTextWithDate:self.selDate];
}

- (void)changeTicket:(UIButton *)sender {
    if ([sender isEqual:self.trainBtn]) {
        self.trainBtn.selected = YES;
        self.airBtn.selected = NO;
        self.trainBg.hidden = NO;
        self.airBg.hidden = YES;
    } else {
        self.trainBtn.selected = NO;
        self.airBtn.selected = YES;
        self.trainBg.hidden = YES;
        self.airBg.hidden = NO;
    }
}

//设置日期显示
- (void)setDateLbTextWithDate:(NSDate *)date
{
    NSString *dateStr = [XSTool dateStrStringFromDate:date];
    NSString *weekStr = [XSTool weekdayStringFromDate:date];
    NSString *textStr = [NSString stringWithFormat:@"%@ %@",dateStr,weekStr];
    NSMutableAttributedString *attributeStr = [self setStringWithStr:textStr RangStr:[NSString stringWithFormat:@"%@",weekStr]];
    _dateLb.attributedText=attributeStr;
}
- (void)dateBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    
    
    FSCalenderSelectedViewController *vc =[[FSCalenderSelectedViewController alloc] init];
    @weakify(self);
    vc.dateBlock = ^(NSDate *date) {
        @strongify(self);
        if (date) {
            self.selDate = date;
        }
        [self setDateLbTextWithDate:date];
        
    };
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
}

#pragma mark -- 婴儿/儿童点击事件
- (void)childBtnClick {
    
    self.childBtn.selected = !self.childBtn.selected;
    
    if (self.childBtn.selected) {//选中状态
        self.hasChild = @"携带儿童";
    }else {
        self.hasChild = @"不携带儿童";
    }
}

- (void)babyBtnClick {
    
    self.babyBtn.selected = !self.babyBtn.selected;
    
    if (self.babyBtn.selected) {//选中状态
        self.hasBaby = @"携带婴儿";
    }else {
        self.hasBaby = @"不携带婴儿";
    }
}

#pragma mark-----ButtonAction
//查询
- (void)checkBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [self.dateFormatter stringFromDate:self.selDate];
    if (isEmptyString(dateStr)) {
        Alert(@"请选择日期");
        return;
    }
    if (isEmptyString(self.selStartModel.code)) {
        Alert(@"请选择出发站");
        return;
    }
    if (isEmptyString(self.selEndModel.code)) {
        Alert(@"请选择到达站");
        return;
    }

    if (self.trainBtn.selected) {
        TrainListVC *vc =[[TrainListVC alloc] init];
        vc.isSelectedG = self.isSelectedG;
        vc.selStartModel = self.selStartModel;
        vc.selEndModel = self.selEndModel;
        vc.from_station = [NSString stringWithFormat:@"%@",self.selStartModel.name];
        vc.to_station = [NSString stringWithFormat:@"%@",self.selEndModel.name];
        vc.selDate = self.selDate;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        AirTicketListVC *vc =[[AirTicketListVC alloc] init];
        vc.selStartModel = self.selStartModel;
        vc.selEndModel = self.selEndModel;
        vc.from_station = [NSString stringWithFormat:@"%@",self.selStartModel.name];
        vc.to_station = [NSString stringWithFormat:@"%@",self.selEndModel.name];
        vc.selDate = self.selDate;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
//订单
- (void)orderBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    TrainOrderVC *vc =[[TrainOrderVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
//返回
- (void)backBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


//出发站 或 到达站
- (void)endOrStartBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (self.trainBtn.selected) {
        TrainCitySelectedVC *vc =[[TrainCitySelectedVC alloc] init];
        @weakify(self);
        vc.selCityBlock = ^(TrainCityModels *model) {
            @strongify(self);
            if (sender.tag == 101) {//出发站
                self.startTF.text = model.name;
                self.selStartModel = model;
            }else{
                self.endTF.text = model.name;
                self.selEndModel = model;
                
            }
        };
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        AirTicketSelectedVC *vc =[[AirTicketSelectedVC alloc] init];
        @weakify(self);
        vc.selCityBlock = ^(TrainCityModels *model) {
            @strongify(self);
            if (sender.tag == 101) {//出发站
                self.startTF.text = model.name;
                self.selStartModel = model;
            }else{
                self.endTF.text = model.name;
                self.selEndModel = model;
                
            }
        };
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}
// 交换
- (void)exchangeBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];

    if (isEmptyString(_startTF.text)) {
        _startTF.text = @"";
    }
    if (isEmptyString(_endTF.text)) {
        _endTF.text = @"";
    }
    
    NSString *changeText = _startTF.text;
    _startTF.text = _endTF.text;
    _endTF.text = changeText;
}


//高铁动车
- (void)otherBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
    
    self.isSelectedG = sender.selected;
}

-(SDCycleScrollView *)adAdvier {
    if (!_adAdvier) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 240) delegate:self placeholderImage:nil];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        cycleScrollView.hidesForSinglePage = YES;
        //        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
        cycleScrollView.tag = 100;
        cycleScrollView.currentPageDotColor = mainColor; // 自定义分页控件小圆标颜色
        cycleScrollView.pageDotColor = [UIColor whiteColor];
        cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        cycleScrollView.placeholderImage = [UIImage imageNamed:@"ticket_bg"];
        cycleScrollView.backgroundColor = BG_COLOR;
        
        _adAdvier =  cycleScrollView;
        
    }
    return _adAdvier;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    //    [self goDifferentADDetail:adverItemModel.app_middle_banner[index]];
    
    
    
}



//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:range];
    return attributeStr;
    
}


//根据日期得到周几
- (NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"Sunday", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六",nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [calendar setTimeZone: timeZone];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    return [weekdays objectAtIndex:theComponents.weekday];
    
}

-(NSDateFormatter *)dateFormatter{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return _dateFormatter;
    
}
@end
