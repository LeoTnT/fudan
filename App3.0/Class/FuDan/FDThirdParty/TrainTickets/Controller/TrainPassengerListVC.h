//
//  TrainPassengerListVC.h
//  App3.0
//
//  Created by xinshang on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainPassengerListVC : XSBaseTableViewController
@property (nonatomic,copy) void(^selPassengerBlock)(NSArray *passengerArr);
@property (nonatomic,strong) NSArray *selPassengerArr;
@end
