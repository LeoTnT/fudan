//
//  TrainCitySelectedVC.m
//  App3.0
//
//  Created by xinshang on 2018/3/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainCitySelectedVC.h"
#import "NSString+PinYin.h"
#import "HTTPManager+ThirdParty.h"

@interface TrainCitySelectedVC ()<UISearchBarDelegate>

@property (nonatomic, strong) UILabel *headView;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面

@end

@implementation TrainCitySelectedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"城市选择";
  
    [self SetUpUI];
    [self getCityCode];
}


- (void)SetUpUI
{
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
//     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.showRefreshHeader = YES;
    self.showRefreshFooter = NO;
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 1;
        wSelf.isRefresh = YES;
        [wSelf getCityCode];
        
    }];
    
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        wSelf.page++;
////        [wSelf queryOrder];
//    }];
//    [self.tableView.mj_header beginRefreshing];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;
    
    
    UISearchBar  *mSearchBar = [[UISearchBar alloc] init];
    mSearchBar.delegate = self;
    mSearchBar.placeholder = @"搜索";
    [mSearchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mSearchBar sizeToFit];
    self.tableView.tableHeaderView=mSearchBar;
    
}


//获取城市列表
- (void)getCityCode
{
    if (!self.isRefresh) {
        if ([UserInstance ShardInstnce].cityArr) {
            self.allArr = [UserInstance ShardInstnce].cityArr;
        }
        
        if (self.allArr.count>0) {
            
            [self tableViewEndRefreshing];
            [self initDataWithSource:self.allArr];
            return;
        }
    }
   
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager train_GetCityCodeWithStationName:@""
                                              all:@"0"
                                          refresh:@"1"
                                          success:^(NSDictionary *dic, resultObject *state)
     {
        [wSelf tableViewEndRefreshing];
         if (state.status) {
             wSelf.allArr = [NSArray arrayWithArray:[TrainCityModels mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
             [UserInstance ShardInstnce].cityArr = self.allArr;
             [wSelf initDataWithSource:wSelf.allArr];
         }else{
             Alert(state.info);
         }
         wSelf.isRefresh = NO;
     } fail:^(NSError *error) {
         [wSelf tableViewEndRefreshing];
         Alert(NetFailure);
     }];
}


- (void)initDataWithSource:(NSArray *)allArray
{
    NSMutableArray *mArr = [NSMutableArray array];
    for (TrainCityModels *model in allArray) {
        [mArr addObject:model.name];
    }
    
    //索引数组
    NSArray  *indexArray= [((NSArray*)mArr) arrayWithPinYinFirstLetterFormat];
    self.listArr =[NSMutableArray arrayWithArray:indexArray];
    
    [self.tableView reloadData];
}

#pragma mark--- UITableViewDataSource and UITableViewDelegate Methods---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.listArr count];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if(section == 0)
//    {
//        return 1;
//    }else{
        NSDictionary *dict = self.listArr[section];
        NSMutableArray *array = dict[@"content"];
        return [array count];
//    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    if (self.listArr) {
        NSDictionary *dict = self.listArr[indexPath.section];
        NSMutableArray *array = dict[@"content"];
        NSString *cityName = array[indexPath.row];
        TrainCityModels *selModel = [[TrainCityModels alloc] init];
       
        for (TrainCityModels*model in self.allArr) {
            if ([model.name isEqualToString:cityName]) {
                selModel = model;
            }
        }
        self.selCityBlock(selModel);
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
    }

    
    
    NSDictionary *dict = self.listArr[indexPath.section];
    NSMutableArray *array = dict[@"content"];
    cell.textLabel.text = array[indexPath.row];
    cell.textLabel.textColor = COLOR_999999;
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = COLOR_999999;
    titleLabel.font = [UIFont systemFontOfSize:14];
    
    NSString *title = self.listArr[section][@"firstLetter"];
    titleLabel.text=title;
    [myView addSubview:titleLabel];
    
    return myView;
}


#pragma mark---tableView索引相关设置----
//添加TableView头视图标题
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *dict = self.listArr[section];
    NSString *title = dict[@"firstLetter"];
    return title;
}


//添加索引栏标题数组
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *resultArray =[NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (NSDictionary *dict in self.listArr) {
        NSString *title = dict[@"firstLetter"];
        [resultArray addObject:title];
    }
    return resultArray;
}


//点击索引栏标题时执行
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    //这里是为了指定索引index对应的是哪个section的，默认的话直接返回index就好。其他需要定制的就针对性处理
    if ([title isEqualToString:UITableViewIndexSearch])
    {
        [tableView setContentOffset:CGPointZero animated:NO];//tabview移至顶部
        return NSNotFound;
    }
    else
    {
        return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index] - 1; // -1 添加了搜索标识
    }
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"开始输入搜索内容");
    if (searchBar.text.length >0) {
        NSMutableArray *mArr = [NSMutableArray array];
        NSString *searchText = searchBar.text;
        for (TrainCityModels *model in self.allArr) {
            if ([model.name containsString:searchText] || [[model.name getFirstLetter] containsString:[searchText uppercaseString]] ) {
                [mArr addObject:model];
            }
        }
        [self initDataWithSource:((NSArray *)mArr)];
    }else{
        [self initDataWithSource:self.allArr];

    }
    
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    if (searchBar.text.length >0) {
//        [searchBar setShowsCancelButton:YES animated:YES]; // 动画显示取消按钮
        NSMutableArray *mArr = [NSMutableArray array];
        
        NSString *searchText = searchBar.text;
        for (TrainCityModels *model in self.allArr) {
            if ([model.name containsString:searchText] || [[model.name getFirstLetter] containsString:[searchText uppercaseString]] ) {
                [mArr addObject:model];
            }
        }
        [self initDataWithSource:((NSArray *)mArr)];
    }else{
        [self initDataWithSource:self.allArr];
        
    }
}


-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - lazy loadding
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = Localized(@"暂无更多记录");
        
    }
    return _defaultView;
}

- (void)showDefaultView
{
    self.defaultView.hidden = NO;
    self.tableView.hidden = YES;
}
- (void)hideDefaultView
{
    self.defaultView.hidden = YES ;
    if (self.tableView.hidden) {
        self.tableView.hidden = NO;
    }
}
-(NSArray *)allArr
{
    if (!_allArr) {
        _allArr = [NSArray array];
    }
    return _allArr;
    
}
@end

