//
//  TrainOrderModel.h
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TrainOrderType)
{
    TrainOrderAll,
    TrainOrderWaitPay,
    TrainOrderFinish
};

@interface TrainPassengersModel : NSObject

@property (nonatomic,copy) NSString *passengerid;//1,
@property (nonatomic,copy) NSString *passengersename;//张三",
@property (nonatomic,copy) NSString *piaotype;//1",
@property (nonatomic,copy) NSString *piaotypename;//成人票",
@property (nonatomic,copy) NSString *passporttypeseid;//1",
@property (nonatomic,copy) NSString *passporttypeseidname;//二代身份证",
@property (nonatomic,copy) NSString *passportseno;//420205199207231234",
@property (nonatomic,copy) NSString *price;//763.5",
@property (nonatomic,copy) NSString *zwcode;//M",
@property (nonatomic,copy) NSString *zwname;//一等座"
@property (nonatomic,copy) NSString *ticket_no;//E686622869106015C",
@property (nonatomic,copy) NSString *cxin;//06车厢,15C座",
@property (nonatomic,copy) NSString *reason;//0

@end
@interface TrainOrderModel : NSObject

@property (nonatomic,copy) NSString *uid;//6,
@property (nonatomic,copy) NSString *ID;//1,
@property (nonatomic,copy) NSString *user_orderid;//"T1803131353136170294",
@property (nonatomic,copy) NSString *orderid;//"",
@property (nonatomic,copy) NSString *train_date;//"2018-04-01",
@property (nonatomic,copy) NSString *is_accept_standing;//"yes",
@property (nonatomic,copy) NSString *choose_seats;//"",
@property (nonatomic,copy) NSString *from_station_code;//"VNP",
@property (nonatomic,copy) NSString *to_station_code;//"OHH",
@property (nonatomic,copy) NSString *checi;//"G101",
@property (nonatomic,strong) NSArray *passengers;//"[{\"passengerid\":1,\"passengersename\":\"张三\",\"piaotype\":\"1\",\"piaotypename\":\"成人票\",\"passporttypeseid\":\"1\",\"passporttypeseidname\":\"二代身份证\",\"passportseno\":\"420205199207231234\",\"price\":\"763.5\",\"zwcode\":\"M\",\"zwname\":\"一等座\"},{\"passengerid\":2,\"passengersename\":\"李四\",\"piaotype\":\"1\",\"piaotypename\":\"成人票\",\"passporttypeseid\":\"1\",\"passporttypeseidname\":\"二代身份证\",\"passportseno\":\"420205199207231234\",\"price\":\"763.5\",\"zwcode\":\"M\",\"zwname\":\"一等座\"}]",
@property (nonatomic,copy) NSString *status;//0,
@property (nonatomic,copy) NSString *result;//"",
@property (nonatomic,copy) NSString *msg;//"",
@property (nonatomic,copy) NSString *ordernumber;//null,
@property (nonatomic,copy) NSString *submit_time;//null,
@property (nonatomic,copy) NSString *deal_time;//null,
@property (nonatomic,copy) NSString *cancel_time;//null,
@property (nonatomic,copy) NSString *pay_time;//null,
@property (nonatomic,copy) NSString *finished_time;//"0000-00-00 00:00:00",
@property (nonatomic,copy) NSString *refund_time;//null,
@property (nonatomic,copy) NSString *juhe_refund_time;//null,
@property (nonatomic,copy) NSString *start_time;//null,
@property (nonatomic,copy) NSString *arrive_time;//null,
@property (nonatomic,copy) NSString *runtime;//null,
@property (nonatomic,copy) NSString *from_station_name;//null,
@property (nonatomic,copy) NSString *to_station_name;//null,
@property (nonatomic,copy) NSString *refund_money;//null,
@property (nonatomic,copy) NSString *orderamount;//null@end
@property (nonatomic,copy) NSString *nmc_refund_status;//null,
@property (nonatomic,copy) NSString *price_total;//null
@property (nonatomic,copy) NSString *is_query;//null
@property (nonatomic,copy) NSString *over_time;//null
@property (nonatomic,copy) NSString *autotime;//null

@end
