//
//  TrainTicketsModels.h
//  App3.0
//
//  Created by xinshang on 2018/3/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrainTicketsModels : NSObject

@end

@interface TrainPassengerModel : NSObject

@property (nonatomic,copy) NSString *ID;//id":2,
@property (nonatomic,copy) NSString *uid;//30,
@property (nonatomic,copy) NSString *passengersename;//123",
@property (nonatomic,copy) NSString *piaotype;//1",
@property (nonatomic,copy) NSString *piaotypename;//成人票",
@property (nonatomic,copy) NSString *passporttypeseid;//1",
@property (nonatomic,copy) NSString *passporttypeseidname;//身份证",
@property (nonatomic,copy) NSString *passportseno;//370481199111022297"
@property (nonatomic,copy) NSString *username ;//= 888888;
@end



@interface TrainCityModels : NSObject
@property (nonatomic,copy) NSString * name;
@property (nonatomic,copy) NSString * code;
@end

@interface TrainInfoModels : NSObject

@property (nonatomic,copy) NSString *sale_date_time;//1230", /*车票开售时间*/
@property (nonatomic,copy) NSString *can_buy_now;//Y",   /*当前是否可以接受预定*/
@property (nonatomic,copy) NSString *arrive_days;//0",  /*列车从出发站到达目的站的运行天数 0:当日到达1,: 次日到达,2:三日到达,3:四日到达,依此类推*/
@property (nonatomic,copy) NSString *train_start_date;//20150711", /*列车从始发站出发的日期*/
@property (nonatomic,copy) NSString *train_code;//G101", /*车次*/
@property (nonatomic,copy) NSString *access_byidcard;//1", /*是否可凭二代身份证直接进出站*/
@property (nonatomic,copy) NSString *train_no;//240000G1010A", /*列车号*/
@property (nonatomic,copy) NSString *train_type;//G", /*列车类型*/
@property (nonatomic,copy) NSString *from_station_code;//VNP", /*出发车站简码*/
@property (nonatomic,copy) NSString *from_station_name;//北京南", /*出发车站名*/
@property (nonatomic,copy) NSString *to_station_code;//AOH", /*到达车站简码*/
@property (nonatomic,copy) NSString *to_station_name;//上海虹桥", /*到达车站名*/
@property (nonatomic,copy) NSString *start_station_name;//北京南", /*始发站名*/
@property (nonatomic,copy) NSString *end_station_name;//上海虹桥", /*终到站名*/
@property (nonatomic,copy) NSString *start_time;//07:00", /*当前站出发时刻*/
@property (nonatomic,copy) NSString *arrive_time;//12:37", /*到达时刻*/
@property (nonatomic,copy) NSString *run_time;//05:37", /*历时（出发站到目的站）*/
@property (nonatomic,copy) NSString *run_time_minute;//337", /*历时分钟合计*/
@property (nonatomic,copy) NSString *gjrw_num;//--", /*高级软卧余票数量*/
@property (nonatomic,copy) NSString *gjrws_price;//0, /* 高级软卧（上）票价*/
@property (nonatomic,copy) NSString *gjrw_price;// 0, /*高级软卧票价*/
@property (nonatomic,copy) NSString *qtxb_num;//--", /*其他席别余票数量*/
@property (nonatomic,copy) NSString *qtxb_price;// 0, /*其他席别对应的票价*/
@property (nonatomic,copy) NSString *rw_num;//--", /*软卧余票数量*/
@property (nonatomic,copy) NSString *rw_price;// 0, /*软卧（上）票价*/
@property (nonatomic,copy) NSString *rwx_price;//10, /*软卧(下)票价*/
@property (nonatomic,copy) NSString *rz_num;//--", /*软座的余票数量*/
@property (nonatomic,copy) NSString *rz_price;// 0, /*软座的票价*/
@property (nonatomic,copy) NSString *swz_num;//15", /*商务座余票数量*/
@property (nonatomic,copy) NSString *swz_price;// 1748, /*商务座票价*/
@property (nonatomic,copy) NSString *tdz_num;//--", /*特等座的余票数量*/
@property (nonatomic,copy) NSString *tdz_price;// 0,  /*特等座票价*/
@property (nonatomic,copy) NSString *wz_num;//--", /*无座的余票数量*/
@property (nonatomic,copy) NSString *wz_price;// 0, /*无座票价*/
@property (nonatomic,copy) NSString *dw_num;//"8",/*动卧的余票数量*/
@property (nonatomic,copy) NSString *dw_price;//"",/*动卧(上)票价[12306新增]*/
@property (nonatomic,copy) NSString *dwx_price;//"",/*动卧(下)票价[12306新增]*/
@property (nonatomic,copy) NSString *yw_num;//--", /*硬卧的余票数量*/
@property (nonatomic,copy) NSString *yw_price;// 0, /*硬卧(上)票价（因为硬卧上中下铺价格不同，这个价格一般是均价），请务必阅读 http://code.juhe.cn/docs/201 中第15条*/
@property (nonatomic,copy) NSString * ywz_price;//90,/*硬卧(中)票价*/
@property (nonatomic,copy) NSString *ywx_price;//86.5,/*硬卧(下)票价*/
@property (nonatomic,copy) NSString *yz_num;//--", /*硬座的余票数量*/
@property (nonatomic,copy) NSString *yz_price;// 0, /*硬座票价*/
@property (nonatomic,copy) NSString *edz_num;//900", /*二等座的余票数量*/
@property (nonatomic,copy) NSString *edz_price;// 553, /*二等座票价*/
@property (nonatomic,copy) NSString *ydz_num;//54", /*一等座余票数量*/
@property (nonatomic,copy) NSString *ydz_price;// 933, /*一等座票价*/
@property (nonatomic,copy) NSString *distance;//0, /*里程数*/


//获取对象的所有属性
- (NSArray *)getAllProperties;

//Model 到字典
- (NSDictionary *)propertieschangeToDic;

@end


