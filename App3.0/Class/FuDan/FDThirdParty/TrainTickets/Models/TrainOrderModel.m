//
//  TrainOrderModel.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderModel.h"


@implementation TrainPassengersModel

@end

@implementation TrainOrderModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"passengers":@"TrainPassengersModel"
             };
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"ID":@"id"};
}
@end
