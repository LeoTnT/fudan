//
//  TrainListDetailCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListDetailCell.h"



@interface TrainListDetailCell()

@property (nonatomic, strong) UILabel *nameLb;//名称-票

@property (nonatomic, strong) UILabel *priceLb;//价格
@property (nonatomic, strong) UILabel *ticket_numLb;//票数量



@property (nonatomic, strong) UIButton *orderBtn;//预定
@end

@implementation TrainListDetailCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =Color(@"FFFFFF") ;
    CGFloat space = 10.0;
    
    CGFloat bgViewH = 44.0;
    CGFloat labelW = (mainWidth-20)/4;
    CGFloat labelH = 30.0;
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = Color(@"FFFFFF");
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(space);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(bgViewH);
    }];
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:15] Radius:0];
    [bgView addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(bgView);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:15] Radius:0];
    [bgView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb.mas_right);
        make.centerY.mas_equalTo(bgView);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_ticket_numLb
    _ticket_numLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:15] Radius:0];
    [bgView addSubview:_ticket_numLb];
    [_ticket_numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_priceLb.mas_right);
        make.centerY.mas_equalTo(bgView);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_orderBtn
    _orderBtn = [self getCustomBtnWithTitle:@"预订"];
    [_orderBtn setBackgroundImage:[UIImage xl_imageWithColor:Color(@"F78F15") size:CGSizeMake(65, labelH)] forState:UIControlStateNormal];
    [_orderBtn setBackgroundImage:[UIImage xl_imageWithColor:[UIColor grayColor] size:CGSizeMake(65, labelH)] forState:UIControlStateDisabled];
    _orderBtn.layer.masksToBounds = YES;
    _orderBtn.layer.cornerRadius = 3;
    [bgView addSubview:_orderBtn];
    [_orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(bgView);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(labelH*0.8);
    }];
    
    
    //line
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = BG_COLOR;
    [bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb);
        make.bottom.mas_equalTo(bgView);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(0.85);
    }];
    
    _nameLb.text = @"二等座";
    _priceLb.text = @"939";
    _ticket_numLb.text = @"有票";

}


-(void)setTDic:(NSDictionary *)tDic
{
    _tDic = tDic;
    
//    NSArray *Properties = [self.model getAllProperties];
    NSDictionary *PropertDic = [self.model propertieschangeToDic];
    
    NSString * key = tDic.allKeys.firstObject;;
    NSString * nameStr = [tDic objectForKey:key];//model的坐票名称
    NSString * numStr = [PropertDic objectForKey:key];//model的余票数量
    _ticket_numLb.textColor = Color(@"111111");

    _nameLb.text = nameStr;
    if (numStr.intValue >0) {
        if (numStr.intValue <20) {
            _ticket_numLb.text = [NSString stringWithFormat:@"仅剩%d张",numStr.intValue];
        }else
        {
            _ticket_numLb.text = @"有票";
        }
        _orderBtn.enabled = YES;
    }else
    {
        _ticket_numLb.text = @"无票";
        _ticket_numLb.textColor = Color(@"CCCCCC");
        _orderBtn.enabled = NO;
    }
    
    NSString *priceStr = @"";
    if ([key isEqualToString:@"edz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.edz_price];
    }else if ([key isEqualToString:@"ydz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.ydz_price];
    }else if ([key isEqualToString:@"tdz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.tdz_price];
    }else if ([key isEqualToString:@"swz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.swz_price];
    }else if ([key isEqualToString:@"rz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.rz_price];
    }else if ([key isEqualToString:@"dw_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.dw_price];
    }else if ([key isEqualToString:@"yz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.yz_price];
    }else if ([key isEqualToString:@"yw_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.yw_price];
    }else if ([key isEqualToString:@"rw_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.rw_price];
    }else if ([key isEqualToString:@"wz_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.wz_price];
    }else if ([key isEqualToString:@"gjrw_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.gjrw_price];
    }else if ([key isEqualToString:@"qtxb_num"]) {
        priceStr = [NSString stringWithFormat:@"%@",self.model.qtxb_price];
    }
    _priceLb.text = [NSString stringWithFormat:@"%@",priceStr];
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}
- (UIButton *)getCustomBtnWithTitle:(NSString*)title
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setTitle:Localized(title) forState:UIControlStateNormal];
    [customBtn setTitleColor:Color(@"FFFFFF") forState:UIControlStateNormal];
    [customBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [customBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return customBtn;
}
-(void)customBtnAction:(UIButton *)sender
{
    
    NSString *priceStr = _priceLb.text;
    if ([self.delegate respondsToSelector:@selector(orderBtnClickNameStr:priceStr:)]) {
        [self.delegate orderBtnClickNameStr:_nameLb.text priceStr:priceStr];
    }
    
}

@end
