//
//  TrainListDetailCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"
@protocol TrainListDetailCellDelegate <NSObject>
-(void)orderBtnClickNameStr:(NSString *)nameStr priceStr:(NSString *)priceStr;
@end

@interface TrainListDetailCell : UITableViewCell

@property(nonatomic,weak)id<TrainListDetailCellDelegate> delegate;
@property (nonatomic,strong) TrainInfoModels * model;
@property (nonatomic,strong) NSDictionary  * tDic;


@end




