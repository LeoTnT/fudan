//
//  TrainOrderDetialPassengerCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainOrderModel.h"
@interface TrainOrderDetialPassengerCell : UITableViewCell
@property (nonatomic,strong) TrainPassengersModel * model;
@property (nonatomic,strong) NSString * status;

@end
