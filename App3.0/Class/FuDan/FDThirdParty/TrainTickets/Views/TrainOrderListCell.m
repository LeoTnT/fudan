//
//  TrainOrderListCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderListCell.h"


@interface TrainOrderListCell()

@property (nonatomic, strong) UIImageView *imgView;//
@property (nonatomic, strong) UIImageView *arrowImg;//
@property (nonatomic, strong) UILabel *from_stationLb;//出发站
@property (nonatomic, strong) UILabel *to_stationLb;//到达站

@property (nonatomic, strong) UILabel *priceLb;//价格
@property (nonatomic, strong) UILabel *typeLb;//票类型
@property (nonatomic, strong) UILabel *statusLb;//付款状态
@property (nonatomic, strong) UILabel *start_timeLb;//出发时间



@property (nonatomic, strong) UIButton *orderBtn;//预定
@end

@implementation TrainOrderListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =BG_COLOR;
    CGFloat space = 10.0;
    
    CGFloat bgViewH = 88.0;
    CGFloat labelW = (mainWidth-10)/2;
    CGFloat labelH = 25.0;
    CGFloat imgW = 20.0;

    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = Color(@"FFFFFF");
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 2.5;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_imgView
    _imgView = [[UIImageView alloc] init];
    _imgView.image = [UIImage imageNamed:@"Train_order_img"];
    [bgView addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    //_from_stationLb
    _from_stationLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:17] Radius:0];
    [bgView addSubview:_from_stationLb];
    [_from_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_imgView.mas_right);
        make.centerY.mas_equalTo(_imgView);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    //_arrowImg
    _arrowImg = [[UIImageView alloc] init];
    _arrowImg.image = [UIImage imageNamed:@"Train_order_arrow"];
    [bgView addSubview:_arrowImg];
    [_arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_stationLb.mas_right);
        make.centerY.mas_equalTo(_from_stationLb);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    //_to_stationLb
    _to_stationLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:17] Radius:0];
    [bgView addSubview:_to_stationLb];
    [_to_stationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_arrowImg.mas_right);
        make.top.mas_equalTo(_from_stationLb);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(labelH*1.25);
    }];
    
    
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:17] Radius:0];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_from_stationLb);
        make.width.mas_equalTo(labelW*1.25);
        make.height.mas_equalTo(labelH);
    }];
    
    //_statusLb
    _statusLb = [self getLabelWithTextColor:Color(@"1A3C57") Font:[UIFont systemFontOfSize:14] Radius:0];
    _statusLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_statusLb];
    [_statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_priceLb);
        make.top.mas_equalTo(_priceLb.mas_bottom);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_typeLb
    _typeLb = [self getLabelWithTextColor:Color(@"6F7588") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_typeLb];
    [_typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_stationLb);
        make.top.mas_equalTo(_statusLb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_start_timeLb
    _start_timeLb = [self getLabelWithTextColor:Color(@"6F7588") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_start_timeLb];
    [_start_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_stationLb);
        make.top.mas_equalTo(_typeLb.mas_bottom);
        make.width.mas_equalTo(mainWidth-50);
        make.height.mas_equalTo(labelH);
    }];
    

    
    _from_stationLb.text = @" ";
    _to_stationLb.text = @" ";
    _typeLb.text = @" ";
    _priceLb.text = @"";
    _statusLb.text = @" ";
    _start_timeLb.text = @" ";

}

-(void)setModel:(TrainOrderModel *)model
{
    _model = model;
    
    _from_stationLb.text = [NSString stringWithFormat:@"%@",model.from_station_name];
    _to_stationLb.text = [NSString stringWithFormat:@"%@",model.to_station_name];
    _typeLb.text = [NSString stringWithFormat:@"%@",model.checi];
    
    if (model.orderamount.floatValue >0) {
        _priceLb.text = [NSString stringWithFormat:@" %@",model.orderamount];
    }
    
    _start_timeLb.text = [NSString stringWithFormat:@"出发时间:%@",model.train_date];
    
    /*订单的所有的可能的状态为 status
     0：刚提交，待处理；处理完将变成1或2；
     1：失败／失效／取消的订单；
     2：占座成功待支付（此时可取消订单，超时不支付将失效）；
     3：支付成功待出票；
     4：出票成功；
     5：出票失败；关于出票失败的问题，请阅读 http://code.juhe.cn/docs/201 中第33条
     6：正在处理线上退票请求；请阅读 http://code.juhe.cn/docs/201 中第16、17、18条
     7：有乘客退票（改签）成功（status保存的是最后一次操作该订单后的状态，先有乘客退票失败，
     然后有乘客退票成功，那么status为7）；
     8：有乘客退票失败（status保存的是最后一次操作该订单后的状态，先有乘客退票成功，
     然后有乘客退票失败，那么status为8）；
     */
    NSString *statusStr = @"";
    switch (model.status.intValue) {
        case 0:
              statusStr = @"待处理";
            break;
        case 1:
            statusStr = @"已失效";///已取消
            break;
        case 2:
            statusStr = @"待支付";
            break;
        case 3:
            statusStr = @"待出票";
            break;
        case 4:
            statusStr = @"出票成功";
            break;
        case 5:
            statusStr = @"出票失败";
            break;
        case 6:
            statusStr = @"正在处理线上退票请求";
            break;
        case 7:
//            statusStr = @"退票(改签)成功";
            statusStr = @"已退票";
            break;
        case 8:
            statusStr = @"退票失败";
            break;
            
        default:
            break;
    }
    _statusLb.text = statusStr;

}


- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}



@end

