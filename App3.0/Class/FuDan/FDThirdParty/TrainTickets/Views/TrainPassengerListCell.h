//
//  TrainPassengerListCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@protocol TrainPassengerListCellDelegate <NSObject>
-(void)checkBtnClick:(UIButton *)sender;
@end


@interface TrainPassengerListCell : UITableViewCell

@property(nonatomic, weak) id<TrainPassengerListCellDelegate> delegate;
@property (nonatomic,strong) TrainPassengerModel * model;

@property (nonatomic, strong) UIButton *checkBtn;//选择框

@end
