//
//  TrainListDetailHeadView.h
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@protocol TrainListDetailHeadViewDelegate <NSObject>
-(void)backBtnClick:(UIButton *)sender;
@optional
-(void)frontBtnClick:(UIButton *)sender;
-(void)behindBtnClick:(UIButton *)sender;
-(void)dateBtnClick:(UIButton *)sender;

@end

@interface TrainListDetailHeadView : UIView
@property(nonatomic,weak)id<TrainListDetailHeadViewDelegate> delegate;

@property (nonatomic,copy) NSDate * selDate;//选中的日期

@property (nonatomic, strong) UIView *dateView;//日期视图

@property (nonatomic, strong) UIView *ZW_View;//等座视图
@property (nonatomic, strong) UILabel *zw_lb; /*二等座*/
@property (nonatomic, strong) UILabel *price_lb; /*票价*/

@property (nonatomic,strong) TrainInfoModels *model;

@end
