//
//  TrainOrderPreviewPassengerCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@protocol TrainOrderPreviewPassengerCellDelegate <NSObject>
-(void)delBtnClick:(UIButton *)sender;
@end


@interface TrainOrderPreviewPassengerCell : UITableViewCell

@property(nonatomic, weak) id<TrainOrderPreviewPassengerCellDelegate> delegate;
@property (nonatomic,strong) TrainPassengerModel * model;
@property (nonatomic, strong) UIButton *delBtn;//删除

@end
