//
//  TrainListFootView.m
//  App3.0
//
//  Created by xinshang on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListFootView.h"
#import "UIButton+ImageTitleSpacing.h"

@interface TrainListFootView()
@property (nonatomic, strong) NSMutableArray *btnArr; /*按钮数组*/
@property (nonatomic, strong) UIButton *selBtn; /*选中的按钮*/

@end

@implementation TrainListFootView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = [UIColor whiteColor];
    CGFloat space = 10.0;

    CGFloat btnW = mainWidth/4;
    
    NSArray *titleArr = @[@"时间",@"全部车型",@"车站筛选",@"有票优先"];
    
    for (int i =0; i<titleArr.count; i++) {
        //cutomBtn
       UIButton *cutomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cutomBtn.tag = 100+i;
        NSString *normalImg = [NSString stringWithFormat:@"Train_type_%d",i];
        NSString *selImg = [NSString stringWithFormat:@"Train_type_sel_%d",i];
        [cutomBtn setImage:[UIImage imageNamed:normalImg] forState:UIControlStateNormal];
       
        
        [cutomBtn setTitleColor:Color(@"6F7588") forState:UIControlStateNormal];
        
        [cutomBtn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
        [cutomBtn setTitle:titleArr[i] forState:UIControlStateNormal];
        [cutomBtn setTitle:titleArr[i] forState:UIControlStateSelected];
        
        if (i == titleArr.count-1) {
            [cutomBtn setTitleColor:mainColor forState:UIControlStateSelected];
            [cutomBtn setImage:[UIImage imageNamed:selImg] forState:UIControlStateSelected];
        }

        [cutomBtn addTarget:self action:@selector(cutomBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cutomBtn];
        cutomBtn.frame = CGRectMake(btnW*i, 0, btnW, 44);
        [self.btnArr addObject:cutomBtn];
        
        [cutomBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop
                                        imageTitleSpace:space/3];

    }
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = BG_COLOR;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(0.7);
    }];
    
}

-(void)initButton:(UIButton*)btn{
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;//使图片和文字水平居中显示
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(btn.imageView.frame.size.height ,-btn.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,0.0, -btn.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
}

#pragma mark --btnAction----
- (void)cutomBtnAction:(UIButton*)sender
{
    for (UIButton *btn in self.btnArr) {
        if (btn == sender) {
            sender.selected = !sender.selected;
        }else{
            btn.selected = NO;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(filterBtnClick:)]) {
        [self.delegate filterBtnClick:sender];
    }
    
}

-(NSMutableArray *)btnArr
{
    if (!_btnArr) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
    
}
@end
