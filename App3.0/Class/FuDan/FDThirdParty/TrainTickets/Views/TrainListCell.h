//
//  TrainListCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@interface TrainListCell : UITableViewCell

@property (nonatomic,strong) TrainInfoModels * model;

@end
