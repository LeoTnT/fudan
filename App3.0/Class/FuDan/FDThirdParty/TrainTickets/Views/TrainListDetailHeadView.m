//
//  TrainListDetailHeadView.m
//  App3.0
//
//  Created by xinshang on 2018/3/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListDetailHeadView.h"


@interface TrainListDetailHeadView()
@property (nonatomic, strong) UIButton *backBtn;//返回

@property (nonatomic, strong) UILabel *start_timeLb;//出发时刻
@property (nonatomic, strong) UILabel *arrive_timeLb;//到达时刻
@property (nonatomic, strong) UILabel *from_station_namelb; /*出发车站名*/
@property (nonatomic, strong) UILabel *to_station_namelb; /*到达车站名*/


@property (nonatomic, strong) UILabel *run_timeLb;//历时
@property (nonatomic, strong) UILabel *train_codeLb;//车次
@property (nonatomic, strong) UILabel *timePlayLb;//时课表

@property (nonatomic, strong) UILabel *from_dateLb;//出发日期
@property (nonatomic, strong) UILabel *to_dateLb;//到达日期

@property (nonatomic, strong) UILabel *titleLb;//


@property (nonatomic, strong) UIButton *frontBtn;//前一天
@property (nonatomic, strong) UIButton *behindBtn;//后一天
@property (nonatomic, strong) UIView *centerView;
@property (nonatomic, strong) UILabel *datelb; /*日期*/




//@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation TrainListDetailHeadView


-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor = mainColor;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 172.0;
    CGFloat labelW = (mainWidth-20)/3;
    CGFloat labelH = 28.0;
    CGFloat dateViewH = 43.0;
    CGFloat imgW = 42.0;
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = mainColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(bgViewH);
    }];
    
    //_backBtn
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[UIImage imageNamed:@"Train_back1"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_backBtn];
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space/3);
        make.top.mas_equalTo(space*2.5);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    //_titleLb
    _titleLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:17] Radius:0];
    _titleLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_titleLb];
    [_titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(_backBtn);
        make.width.mas_equalTo(labelW*1.5);
        make.height.mas_equalTo(labelH);
    }];
    
    //左边
    //_from_station_namelb
    _from_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_from_station_namelb];
    [_from_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_backBtn.mas_bottom).offset(space);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_start_timeLb
    _start_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont boldSystemFontOfSize:16] Radius:0];
    [bgView addSubview:_start_timeLb];
    [_start_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_station_namelb);
        make.top.mas_equalTo(_from_station_namelb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    //_from_dateLb
    _from_dateLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_from_dateLb];
    [_from_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_station_namelb);
        make.top.mas_equalTo(_start_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
  
    //中间
    //_train_codeLb
    _train_codeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:15] Radius:0];
    _train_codeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_train_codeLb];
    [_train_codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(_from_station_namelb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_run_timeLb
    _run_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    _run_timeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_run_timeLb];
    [_run_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.centerY.mas_equalTo(_from_dateLb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_timePlayLb
    _timePlayLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:13] Radius:0];
    _timePlayLb.textAlignment = NSTextAlignmentCenter;
    _timePlayLb.layer.masksToBounds = YES;
    _timePlayLb.layer.cornerRadius = 2;
    _timePlayLb.layer.borderWidth = 1;
    _timePlayLb.layer.borderColor = Color(@"FFFFFF").CGColor;
    [bgView addSubview:_timePlayLb];
    [_timePlayLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.centerY.mas_equalTo(_start_timeLb);
        make.width.mas_equalTo(68);
        make.height.mas_equalTo(labelH*0.8);
    }];
    
    UIView *leftLine = [[UIView alloc] init];
    leftLine.backgroundColor = Color(@"FFFFFF");
    [bgView addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_timePlayLb.mas_left);
        make.centerY.mas_equalTo(_timePlayLb);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(1);
    }];
    
    UIView *rightLine = [[UIView alloc] init];
    rightLine.backgroundColor = Color(@"FFFFFF");
    [bgView addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_timePlayLb.mas_right);
        make.centerY.mas_equalTo(_timePlayLb);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(1);
    }];
   
    
    //右边
    //_to_station_namelb
    _to_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    _to_station_namelb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_to_station_namelb];
    [_to_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_from_station_namelb);
        make.height.mas_equalTo(labelH);
    }];
    
    //_arrive_timeLb
    _arrive_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont boldSystemFontOfSize:16] Radius:0];
    _arrive_timeLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_arrive_timeLb];
    [_arrive_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_to_station_namelb);
        make.top.mas_equalTo(_to_station_namelb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    //_to_dateLb
    _to_dateLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_to_dateLb];
    [_to_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_to_station_namelb);
        make.top.mas_equalTo(_arrive_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
   
    
    //日期视图
    _dateView = [[UIView alloc] init];
    _dateView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_dateView];
    [_dateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(bgView.mas_bottom);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(dateViewH);
    }];
    
    CGFloat btnW = 80;
    //_frontBtn
    _frontBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_frontBtn setImage:[UIImage imageNamed:@"Train_front_1"] forState:UIControlStateNormal];
    [_frontBtn setTitle:@"前一天" forState:UIControlStateNormal];
    [_frontBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [_frontBtn setTitleColor:mainGrayColor forState:UIControlStateDisabled];
    [_frontBtn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
    [_frontBtn addTarget:self action:@selector(frontBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_dateView addSubview:_frontBtn];
    [_frontBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*0.2);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_behindBtn
    _behindBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_behindBtn setImage:[UIImage imageNamed:@"Train_behind_1"] forState:UIControlStateNormal];
    [_behindBtn setTitle:@"后一天" forState:UIControlStateNormal];
    [_behindBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [_behindBtn setTitleColor:mainGrayColor forState:UIControlStateDisabled];
    [_behindBtn addTarget:self action:@selector(behindBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_behindBtn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
    [self setImageToRightWithButton:_behindBtn];
    [_dateView addSubview:_behindBtn];
    [_behindBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space*0.2);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_datelb
    _datelb = [self getLabelWithTextColor:mainColor Font:[UIFont systemFontOfSize:13] Radius:0];
    _datelb.textAlignment = NSTextAlignmentCenter;
    [_dateView addSubview:_datelb];
    [_datelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_dateView);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(labelW*1.1);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    
    UIImageView *dateImg = [[UIImageView alloc] init];
    dateImg.image = [UIImage imageNamed:@"Train_date_1"];
    [_dateView addSubview:dateImg];
    [dateImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_datelb.mas_left);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(imgW/3);
        make.height.mas_equalTo(imgW/3);
    }];
    
    UIImageView *downImg = [[UIImageView alloc] init];
    downImg.image = [UIImage imageNamed:@"Train_down_1"];
    [_dateView addSubview:downImg];
    [downImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_datelb.mas_right);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(imgW/3.5);
        make.height.mas_equalTo(imgW/3.5);
    }];
    
    //dateBtn
    UIButton *dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dateBtn addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_dateView addSubview:dateBtn];
    [dateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_dateView);
        make.centerY.mas_equalTo(_dateView);
        make.width.mas_equalTo(btnW*1.35);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //二等座视图
    _ZW_View = [[UIView alloc] init];
    _ZW_View.backgroundColor = [UIColor whiteColor];
    [self addSubview:_ZW_View];
    [_ZW_View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(bgView.mas_bottom);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(dateViewH);
    }];
    //_zw_lb
    _zw_lb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:14] Radius:0];
    [_ZW_View addSubview:_zw_lb];
    [_zw_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_ZW_View);
        make.width.mas_equalTo(85);
        make.height.mas_equalTo(labelH);
    }];
    
    //_price_lb
    _price_lb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:15] Radius:0];
    [_ZW_View addSubview:_price_lb];
    [_price_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_zw_lb.mas_right).offset(-10);
        make.centerY.mas_equalTo(_ZW_View);
        make.width.mas_equalTo(mainWidth/2);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    _titleLb.text = @"火车票详情";
    _start_timeLb.text = @"06:30";
    _arrive_timeLb.text = @"12:18";
    
    _train_codeLb.text = @"G120";
    
    _from_station_namelb.text = @"上海虹桥";
    _to_station_namelb.text = @"北京南";
    
    _run_timeLb.text = @"5小时24分钟";
    _timePlayLb.text = @"时刻表";
    
    _ZW_View.hidden = YES;
}
/*
 G-高铁
 D-动车
 C-城际动车
 Z-直达列车
 T-特快
 K-快速列车
 
 1—5开头-普快列车
 6—7开头-普客列车
 8开头-通勤列车
 */

-(void)setModel:(TrainInfoModels *)model
{
    _model = model;

  
    
    
    if (model) {
        _start_timeLb.text = [NSString stringWithFormat:@"%@",model.start_time];//出发时刻
        _arrive_timeLb.text = [NSString stringWithFormat:@"%@",model.arrive_time];//到达时刻
        _train_codeLb.text = [NSString stringWithFormat:@"%@",model.train_code];//车次
        _from_station_namelb.text = [NSString stringWithFormat:@"%@",model.from_station_name];//出发车站名
        _to_station_namelb.text = [NSString stringWithFormat:@"%@",model.to_station_name];//到达车站名
        
        /*历时*/
        NSString *run_time_minute = [NSString stringWithFormat:@"%@",model.run_time_minute];
        NSInteger hour = run_time_minute.integerValue/60;
        NSInteger minute = run_time_minute.integerValue%60;
        
        NSString *run_time = [NSString stringWithFormat:@"%@分",model.run_time_minute];
        if (hour >0) {
            run_time = [NSString stringWithFormat:@"%ld小时%ld分",hour,minute];
        }
        _run_timeLb.text = run_time;
    }
    
    if (self.selDate) {
        NSDate *start_date = self.selDate;
        NSDate *end_date = [XSTool dateStringFromDate:start_date year:0 month:0 day:model.arrive_days.intValue ];//到达日期
        
        NSString *startDateStr = [XSTool dateStrStringFromDate:start_date];
        NSString *startWeekStr = [XSTool weekdayStringFromDate:start_date];
        
        NSString *endDateStr = [XSTool dateStrStringFromDate:end_date];
        NSString *endWeekStr = [XSTool weekdayStringFromDate:end_date];
        _from_dateLb.text = [NSString stringWithFormat:@"%@ %@",startDateStr,startWeekStr];
        _to_dateLb.text = [NSString stringWithFormat:@"%@ %@",endDateStr,endWeekStr];
    }
    

   
}




#pragma mark - 点击返回
#pragma mark --btnAction----

- (void)backBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(backBtnClick:)]) {
        [self.delegate backBtnClick:sender];
    }
    
}
- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}
#pragma mark ---日期
//前一天
- (void)frontBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(frontBtnClick:)]) {
        [self.delegate frontBtnClick:sender];
    }
    
}

- (void)behindBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(behindBtnClick:)]) {
        [self.delegate behindBtnClick:sender];
    }
    
}
//日历
- (void)dateBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(dateBtnClick:)]) {
        [self.delegate dateBtnClick:sender];
    }
    
}

-(void)setSelDate:(NSDate *)selDate
{
    _selDate = selDate;
    NSString *dateStr = [XSTool dateStrStringFromDate:selDate];
    NSString *weekStr = [XSTool weekdayStringFromDate:selDate];
    _datelb.text = [NSString stringWithFormat:@"%@ %@",dateStr,weekStr];
    
    NSInteger index = [self compareOneDay:selDate withAnotherDay:[NSDate date]];
    if (index <=0) {
        _frontBtn.enabled = NO;
        [_frontBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    }else{
        _frontBtn.enabled = YES;
        [_frontBtn setTitleColor:mainColor forState:UIControlStateNormal];
    }
    
    
    
}

#pragma mark -
- (NSInteger)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //NSLog(@"Date1  is in the future");
        return 1;
    }
    else if (result == NSOrderedAscending){
        //NSLog(@"Date1 is in the past");
        return -1;
    }
    //NSLog(@"Both dates are the same");
    return 0;
    
}
//字符串转日期格式
- (NSDate *)stringToDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
}


@end
