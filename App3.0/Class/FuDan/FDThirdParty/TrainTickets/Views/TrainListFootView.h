//
//  TrainListFootView.h
//  App3.0
//
//  Created by xinshang on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TrainListFootViewDelegate <NSObject>

-(void)filterBtnClick:(UIButton *)sender;

@end


@interface TrainListFootView : UIView
@property(nonatomic,weak)id<TrainListFootViewDelegate> delegate;

@end
