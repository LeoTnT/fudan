//
//  TrainOrderPreviewContactCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TrainOrderPreviewContactCellDelegate <NSObject>
-(void)contactBtnClick:(UIButton *)sender;
@end


@interface TrainOrderPreviewContactCell : UITableViewCell
@property(nonatomic, weak) id<TrainOrderPreviewContactCellDelegate> delegate;
@property (nonatomic, strong) UITextField *mobileTF;//手机号

@end
