//
//  TrainOrderPreviewContactCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderPreviewContactCell.h"




@interface TrainOrderPreviewContactCell()

@property (nonatomic, strong) UILabel *nameLb;//联系手机



@property (nonatomic, strong) UIButton *contactBtn;//联系人
@end

@implementation TrainOrderPreviewContactCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =Color(@"FFFFFF") ;
    CGFloat space = 10.0;
    
    CGFloat labelW = 85;
    CGFloat labelH = 35.0;
    
    
   
    //_nameLb
    _nameLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:15] Radius:0];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*1.5);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
 

   
    
    //_contactBtn
    _contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_contactBtn setImage:[UIImage imageNamed:@"Train_contact"] forState:UIControlStateNormal];
    [_contactBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_contactBtn];
    [_contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(labelH);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //
    _mobileTF = [[UITextField alloc] init];
    _mobileTF.font = [UIFont systemFontOfSize:15];
    [self addSubview:_mobileTF];
    [_mobileTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb.mas_right);
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(_contactBtn.mas_left);
        make.height.mas_equalTo(labelH);
    }];
    
    _mobileTF.placeholder = Localized(@"通知出票信息");
    _nameLb.text = Localized(@"联系手机");
    
}




    - (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
    {
        UILabel *label = [[UILabel alloc] init];
        label.textColor = color;
        //    label.textAlignment = NSTextAlignmentCenter;
        label.font = font;
        label.layer.masksToBounds = YES;
        label.layer.cornerRadius = radius;
        return label;
    }
-(void)customBtnAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(contactBtnClick:)]) {
        [self.delegate contactBtnClick:sender];
    }
    
}

@end
