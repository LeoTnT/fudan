//
//  TrainListCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListCell.h"



@interface TrainListCell()

@property (nonatomic, strong) UILabel *start_timeLb;//出发时刻
@property (nonatomic, strong) UILabel *arrive_timeLb;//到达时刻
@property (nonatomic, strong) UILabel *from_station_namelb; /*出发车站名*/
@property (nonatomic, strong) UILabel *to_station_namelb; /*到达车站名*/
@property (nonatomic, strong) UILabel *priceLb;//价格

@property (nonatomic, strong) UILabel *run_timeLb;//历时
@property (nonatomic, strong) UILabel *train_codeLb;//车次

@property (nonatomic, strong) UILabel *fristLb;//硬座//一等座
@property (nonatomic, strong) UILabel *sencondLb;//硬卧//二等座
@property (nonatomic, strong) UILabel *otherLb;//软卧//商务座
@property (nonatomic, strong) UILabel *wz_Lb;//无座


@property (nonatomic, strong) UIImageView *access_byidcardImg;///*是否可凭二代身份证直接进出站*/

@property (nonatomic, strong) UIView *bgView;//背景视图


@property (nonatomic, strong) UIView *leftView;//小圆点
@property (nonatomic, strong) UIView *rightView;//小圆点
//@property (nonatomic, strong) NSDictionary *typeDict;
@property (nonatomic, strong) NSArray *typeArr;


//@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation TrainListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor = BG_COLOR;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 109.0;
    CGFloat labelW = (mainWidth-20)/4;
    CGFloat labelH = 30.0;
    
    
    _bgView = [[UIView alloc] init];
    _bgView.backgroundColor = Color(@"FFFFFF");
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(space);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(bgViewH);
    }];
    
    //_start_timeLb
    _start_timeLb = [self getLabelWithTextColor:Color(@"1A3C57") Font:[UIFont systemFontOfSize:16] Radius:0];
    [_bgView addSubview:_start_timeLb];
    [_start_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space*1.2);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_from_station_namelb
    _from_station_namelb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:15] Radius:0];
    [_bgView addSubview:_from_station_namelb];
    [_from_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_start_timeLb);
        make.top.mas_equalTo(_start_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_run_timeLb
    _run_timeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:13] Radius:0];
    _run_timeLb.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_run_timeLb];
    [_run_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_start_timeLb.mas_right);
        make.top.mas_equalTo(_start_timeLb).offset(5);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH*0.8);
    }];
    
    
    CGFloat lineW = 6;
    //直线
    UIView *tline = [[UIView alloc] init];
    tline.backgroundColor = Color(@"CCCCCC");
    [_bgView addSubview:tline];
    [tline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_run_timeLb);
        make.top.mas_equalTo(_run_timeLb.mas_bottom).offset(0.5);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(1);
    }];
    
    //小圆点--左边
    UIView *tleftView = [[UIView alloc] init];
    tleftView.backgroundColor = Color(@"CCCCCC");
    tleftView.layer.masksToBounds = YES;
    tleftView.layer.cornerRadius = lineW/2;
    [_bgView addSubview:tleftView];
    [tleftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(tline.mas_left);
        make.centerY.mas_equalTo(tline);
        make.width.mas_equalTo(lineW);
        make.height.mas_equalTo(lineW);
    }];
    
    //小圆点--左边-实心
    _leftView = [[UIView alloc] init];
    _leftView.backgroundColor = Color(@"FFFFFF");
    _leftView.layer.masksToBounds = YES;
    _leftView.layer.cornerRadius = 1.7;
    [tleftView addSubview:_leftView];
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(tleftView);
        make.width.mas_equalTo(3.4);
        make.height.mas_equalTo(3.4);
    }];
    
    
    //小圆点--右边
    UIView *trightView = [[UIView alloc] init];
    trightView.backgroundColor = Color(@"CCCCCC");
    trightView.layer.masksToBounds = YES;
    trightView.layer.cornerRadius = lineW/2;
    [_bgView addSubview:trightView];
    [trightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tline.mas_right);
        make.centerY.mas_equalTo(tline);
        make.width.mas_equalTo(lineW);
        make.height.mas_equalTo(lineW);
    }];
    
    //小圆点--右边-实心
    _rightView = [[UIView alloc] init];
    _rightView.backgroundColor = Color(@"FFFFFF");
    _rightView.layer.masksToBounds = YES;
    _rightView.layer.cornerRadius = 1.7;
    [trightView addSubview:_rightView];
    [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(tleftView);
        make.width.mas_equalTo(3.4);
        make.height.mas_equalTo(3.4);
    }];
    
    
    
    
    //_train_codeLb
    _train_codeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:13] Radius:0];
    _train_codeLb.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_train_codeLb];
    [_train_codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_run_timeLb);
        make.top.mas_equalTo(_run_timeLb.mas_bottom).offset(2);
        make.width.mas_equalTo(_run_timeLb.mas_width);
        make.height.mas_equalTo(_run_timeLb.mas_height);
    }];
    
    //_access_byidcardImg
    _access_byidcardImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Train_acce_card"]];
    [_bgView addSubview:_access_byidcardImg];
    [_access_byidcardImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_run_timeLb).offset(labelW/2+10);
        make.centerY.mas_equalTo(_train_codeLb);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:16] Radius:0];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [_bgView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_start_timeLb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH*1.5);
    }];
    
    
    
    //_arrive_timeLb
    _arrive_timeLb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:15] Radius:0];
    _arrive_timeLb.textAlignment = NSTextAlignmentRight;
    [_bgView addSubview:_arrive_timeLb];
    [_arrive_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_priceLb.mas_left).offset(-space*0.5);
        make.top.mas_equalTo(_start_timeLb);
        make.height.mas_equalTo(labelH);
    }];
    
    //_to_station_namelb
    _to_station_namelb = [self getLabelWithTextColor:Color(@"111111") Font:[UIFont systemFontOfSize:15] Radius:0];
    _to_station_namelb.textAlignment = NSTextAlignmentRight;
    [_bgView addSubview:_to_station_namelb];
    [_to_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_arrive_timeLb);
        make.top.mas_equalTo(_from_station_namelb);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    //_fristLb
    _fristLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:13] Radius:0];
    _fristLb.textAlignment = NSTextAlignmentLeft;
    [_bgView addSubview:_fristLb];
    [_fristLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_station_namelb);
        make.top.mas_equalTo(_from_station_namelb.mas_bottom).offset(2);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_sencondLb
    _sencondLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:13] Radius:0];
    _sencondLb.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_sencondLb];
    [_sencondLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_run_timeLb);
        make.top.mas_equalTo(_fristLb);
        make.width.mas_equalTo(_run_timeLb.mas_width);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_otherLb
    _otherLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:13] Radius:0];
    _otherLb.textAlignment = NSTextAlignmentRight;
    [_bgView addSubview:_otherLb];
    [_otherLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_to_station_namelb);
        make.top.mas_equalTo(_fristLb);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    //_wz_Lb
    _wz_Lb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:13] Radius:0];
    _wz_Lb.textAlignment = NSTextAlignmentRight;
    [_bgView addSubview:_wz_Lb];
    [_wz_Lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_priceLb);
        make.top.mas_equalTo(_fristLb);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    
    
    
    
    _start_timeLb.text = @"06:30";
    _arrive_timeLb.text = @"12:35";
    _priceLb.text = @"550";
    _train_codeLb.text = @"G120";
    
    _from_station_namelb.text = @"上海虹桥";
    _to_station_namelb.text = @"北京南";
    
    _fristLb.text = @"二等座:有票";
    _sencondLb.text = @"一等座:有票";
    _otherLb.text = @"商务座:有票";
    _wz_Lb.text = @"";
    
    _run_timeLb.text = @"5小时38分钟";
    
    _wz_Lb.hidden = YES;
}
/*
 G-高铁
 D-动车
 C-城际动车
 Z-直达列车
 T-特快
 K-快速列车
 
 1—5开头-普快列车
 6—7开头-普客列车
 8开头-通勤列车
 */

-(void)setModel:(TrainInfoModels *)model
{
    _model = model;
    _start_timeLb.text = [NSString stringWithFormat:@"%@",model.start_time];//出发时刻
    _arrive_timeLb.text = [NSString stringWithFormat:@"%@",model.arrive_time];//到达时刻
    _train_codeLb.text = [NSString stringWithFormat:@"%@",model.train_code];//车次
    _from_station_namelb.text = [NSString stringWithFormat:@"%@",model.from_station_name];//出发车站名
    _to_station_namelb.text = [NSString stringWithFormat:@"%@",model.to_station_name];//到达车站名
    
    /*历时*/
    NSString *run_time_minute = [NSString stringWithFormat:@"%@",model.run_time_minute];
    NSInteger hour = run_time_minute.integerValue/60;
    NSInteger minute = run_time_minute.integerValue%60;
    
    NSString *run_time = [NSString stringWithFormat:@"%@分",model.run_time_minute];
    if (hour >0) {
        run_time = [NSString stringWithFormat:@"%ld小时%ld分",hour,minute];
    }
    _run_timeLb.text = run_time;
    
    
    /*是否可凭二代身份证直接进出站*/
    CGFloat labelW = (mainWidth-20)/4;
    if (model.access_byidcard.intValue == 1) {
        _access_byidcardImg.hidden = NO;
        [_train_codeLb mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_run_timeLb).offset(-10);
            make.top.mas_equalTo(_run_timeLb.mas_bottom).offset(2);
            make.width.mas_equalTo(_run_timeLb.mas_width);
            make.height.mas_equalTo(_run_timeLb.mas_height);
        }];
        [_access_byidcardImg mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_run_timeLb).offset(labelW/2+10);
            make.centerY.mas_equalTo(_train_codeLb);
            make.width.mas_equalTo(20);
            make.height.mas_equalTo(20);
        }];
        
    }else{
        _access_byidcardImg.hidden =YES;
        [_train_codeLb mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_run_timeLb);
            make.top.mas_equalTo(_run_timeLb.mas_bottom).offset(2);
            make.width.mas_equalTo(_run_timeLb.mas_width);
            make.height.mas_equalTo(_run_timeLb.mas_height);
        }];
        
    }
    
    /*是否是起始站*/
    
    if ([model.from_station_name isEqualToString:model.start_station_name]) {
        self.leftView.hidden = YES;
    }else{
        self.leftView.hidden = NO;
    }
    /*是否是终点站*/
    if ([model.to_station_name isEqualToString:model.end_station_name]) {
        self.rightView.hidden = YES;
    }else{
        self.rightView.hidden = NO;
    }
    
    
    _wz_Lb.hidden = YES;
    UIColor *normal_color = Color(@"666666");
    
    _fristLb.textColor =
    _sencondLb.textColor =
    _otherLb.textColor =
    _wz_Lb.textColor = normal_color;
    
    _fristLb.hidden =
    _sencondLb.hidden =
    _otherLb.hidden =
    _wz_Lb.hidden = YES;

//    UIColor *wp_color = Color(@"CCCCCC");
//    /*要显示的票的类型*/
//    if ([model.train_type isEqualToString:@"G"]||
//        [model.train_type isEqualToString:@"C"]) {// G-高铁 C-城际动车
//
//        tPrice = [NSString stringWithFormat:@"%@",model.edz_price];//二等座价格
//
//        if (model.edz_num.intValue) {
//            _fristLb.text = @"二等座:有票";
//        }else{
//            _fristLb.text = @"二等座:无票";
//            _fristLb.textColor = wp_color;
//        }
//        if (model.ydz_num.intValue) {
//            _sencondLb.text = @"一等座:有票";
//        }else{
//            _sencondLb.text = @"一等座:无票";
//            _sencondLb.textColor = wp_color;
//        }
//        if (model.swz_num.intValue) {
//            _otherLb.text = @"商务座:有票";
//        }else{
//            _otherLb.text = @"商务座:无票";
//            _otherLb.textColor = wp_color;
//        }
//
//    }else if ([model.train_type isEqualToString:@"D"]) {// D-动车
//        tPrice = [NSString stringWithFormat:@"%@",model.edz_price];//二等座价格
//        if (model.edz_num.intValue) {
//            _fristLb.text = @"二等座:有票";
//        }else{
//            _fristLb.text = @"二等座:无票";
//            _fristLb.textColor = wp_color;
//        }
//        if (model.ydz_num.intValue) {
//            _sencondLb.text = @"一等座:有票";
//        }else{
//            _sencondLb.text = @"一等座:无票";
//            _sencondLb.textColor = wp_color;
//        }
//        if (model.wz_num.intValue) {
//            _otherLb.text = @"无座:有票";
//        }else{
//            _otherLb.text = @"无座:无票";
//            _otherLb.textColor = wp_color;
//        }
//    }else{// 普通
//        tPrice = [NSString stringWithFormat:@"%@",model.yz_price];//硬座价格
//        _wz_Lb.hidden = NO;
//        if (model.yz_num.intValue) {
//            _fristLb.text = @"硬座:有票";
//        }else{
//            _fristLb.text = @"硬座:无票";
//            _fristLb.textColor = wp_color;
//        }
//        if (model.yw_num.intValue) {
//            _sencondLb.text = @"硬卧:有票";
//        }else{
//            _sencondLb.text = @"硬卧:无票";
//            _sencondLb.textColor = wp_color;
//        }
//        if (model.rw_num.intValue) {
//            _otherLb.text = @"软卧:有票";
//        }else{
//            _otherLb.text = @"软卧:无票";
//            _otherLb.textColor = wp_color;
//        }
//        if (model.wz_num.intValue) {
//            _wz_Lb.text = @"无座:有票";
//        }else{
//            _wz_Lb.text = @"无座:无票";
//            _wz_Lb.textColor = wp_color;
//        }
//    }
//
    /*要显示的票价*/
    
    NSString *tPrice = [NSString stringWithFormat:@"%@",model.edz_price];
    if (model.wz_price.doubleValue >0) {
        tPrice =[NSString stringWithFormat:@"%@",model.wz_price];
    }else if (model.yz_price.doubleValue >0) {
        tPrice =[NSString stringWithFormat:@"%@",model.yz_price];
    }else if (model.edz_price.doubleValue >0) {
        tPrice =[NSString stringWithFormat:@"%@",model.edz_price];
    }
    _priceLb.text = [NSString stringWithFormat:@"%@",tPrice];//车票显示价格
    
    
    NSArray *Properties = [model getAllProperties];
    NSDictionary *PropertDic = [model propertieschangeToDic];
    
    // 可变数组
    NSMutableArray *mArr = [NSMutableArray array];
    for (NSDictionary *tDic in self.typeArr) {
     for (NSString *str2 in Properties) {
         NSString *str1 =tDic.allKeys.firstObject;
            if ([str1 isEqualToString:str2]) {
                NSString *numStr = [PropertDic objectForKey:str1];
                if (![numStr isEqualToString:@"--"]) {
                    [mArr addObject:tDic];
                }
            }
        }
    }
    
    
//    显示座位和余票
    for (int i = 0; i < mArr.count; i++)
    {
        
        NSDictionary *tDic = mArr[i];
        NSString * key = tDic.allKeys.firstObject;;
        NSString * nameStr = [tDic objectForKey:key];//model的坐票名称
        NSString * numStr = [PropertDic objectForKey:key];//model的余票数量

        switch (i) {
            case 0:
                {
                    [self setLb:_fristLb nameStr:nameStr numStr:numStr];
                }
                break;
            case 1:
            {
                [self setLb:_sencondLb nameStr:nameStr numStr:numStr];
            }
                break;
            case 2:
            {
                [self setLb:_otherLb nameStr:nameStr numStr:numStr];
            }
                break;
            case 3:
            {
                [self setLb:_wz_Lb nameStr:nameStr numStr:numStr];
            }
                break;
            default:
                break;
        }
        

    }
    
    
    
    
}


- (void)setLb:(UILabel*)label nameStr:(NSString *)nameStr numStr:(NSString *)numStr
{
    label.hidden = NO;
    if (numStr.intValue) {
        label.text = [NSString stringWithFormat:@"%@:%@",nameStr,Localized(@"有票")];
        if (numStr.intValue <20) {
            label.text = [NSString stringWithFormat:@"%@:%@%@",nameStr,numStr,Localized(@"张")];
        }
    }else{
        label.text = [NSString stringWithFormat:@"%@:%@",nameStr,Localized(@"无票")];
        label.textColor = Color(@"CCCCCC");
    }
}





#pragma mark - 点击返回
//
//- (void)backBtnAction
//{
//    if ([self.delegate respondsToSelector:@selector(backBtnClick)]) {
//        [self.delegate backBtnClick];
//    }
//}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    
    return label;
}

-(NSArray *)typeArr
{
    if (!_typeArr) {
        _typeArr = @[@{@"edz_num":@"二等座"},
                     @{@"ydz_num":@"一等座"},
                     @{@"tdz_num":@"特等座"},
                     @{@"swz_num":@"商务座"},
                     @{@"rz_num":@"软座"},
                     @{@"dw_num":@"动卧"},
                     @{@"yz_num":@"硬座"},
                     @{@"yw_num":@"硬卧"},
                     @{@"rw_num":@"软卧"},
                     @{@"wz_num":@"无座"},
                     @{@"gjrw_num":@"高级软卧"},
                     @{@"qtxb_num":@"其他"}
                     ];
    }
    return _typeArr;
}

//"edz_num": "900", /*二等座的余票数量*/
//"ydz_num": "54", /*一等座余票数量*/
//"tdz_num": "--", /*特等座的余票数量*/
//"swz_num": "15", /*商务座余票数量*/
//"rz_num": "--", /*软座的余票数量*/
//"dw_num":"8",/*动卧的余票数量*/
//"yz_num": "--", /*硬座的余票数量*/
//"yw_num": "--", /*硬卧的余票数量*/
//"rw_num": "--", /*软卧余票数量*/
//"wz_num": "--", /*无座的余票数量*/
// "gjrw_num": "--", /*高级软卧余票数量*/
//"qtxb_num": "--", /*其他席别余票数量*/
//获取对象的所有属性


@end


