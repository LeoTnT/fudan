//
//  TrainOrdeNoteCell.m
//  App3.0
//
//  Created by mac on 2018/7/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrdeNoteCell.h"
@interface TrainOrdeNoteCell()

@end
@implementation TrainOrdeNoteCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =BG_COLOR;
    CGFloat space = 10.0;
    _noteLb.backgroundColor = [UIColor whiteColor];
    //_noteLb
    _noteLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:15] Radius:0];
    [self addSubview:_noteLb];
    [_noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-space);
    }];
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}


@end
