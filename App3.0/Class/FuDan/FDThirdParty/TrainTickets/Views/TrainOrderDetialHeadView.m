//
//  TrainOrderDetialHeadView.m
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderDetialHeadView.h"

@interface TrainOrderDetialHeadView()

@property (nonatomic, strong) UILabel *start_timeLb;//出发时刻
@property (nonatomic, strong) UILabel *arrive_timeLb;//到达时刻
@property (nonatomic, strong) UILabel *from_station_namelb; /*出发车站名*/
@property (nonatomic, strong) UILabel *to_station_namelb; /*到达车站名*/


@property (nonatomic, strong) UILabel *run_timeLb;//历时
@property (nonatomic, strong) UILabel *train_codeLb;//车次
@property (nonatomic, strong) UILabel *timePlayLb;//时课表

@property (nonatomic, strong) UILabel *from_dateLb;//出发日期
@property (nonatomic, strong) UILabel *to_dateLb;//到达日期

@property (nonatomic, strong) UILabel *headLb;//
@property (nonatomic, strong) UIView *headView;//

@property (nonatomic, strong) UIView *bottomView;//


//@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation TrainOrderDetialHeadView


-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUpUI];
        // 监听通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification2) name:kCountDownNotification2 object:nil];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor = BG_COLOR;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 134.0;
    CGFloat labelW = (mainWidth-20)/3;
    CGFloat labelH = 28.0;
    CGFloat dateViewH = 43.0;
    CGFloat imgW = 42.0;
    
    _headView = [[UIView alloc] init];
    _headView.backgroundColor = Color(@"FFF8DF");
    [self addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(36);
    }];
    //_headLb
    _headLb = [self getLabelWithTextColor:mainColor Font:[UIFont systemFontOfSize:15] Radius:0];
    _headLb.numberOfLines = 0;
    [_headView addSubview:_headLb];
    [_headLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = mainColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_headView.mas_bottom).offset(space);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //左边
    //_from_station_namelb
    _from_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_from_station_namelb];
    [_from_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space*1.25);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_start_timeLb
    _start_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont boldSystemFontOfSize:16] Radius:0];
    [bgView addSubview:_start_timeLb];
    [_start_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_station_namelb);
        make.top.mas_equalTo(_from_station_namelb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    //_from_dateLb
    _from_dateLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_from_dateLb];
    [_from_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_from_station_namelb);
        make.top.mas_equalTo(_start_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    //中间
    //_train_codeLb
    _train_codeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:15] Radius:0];
    _train_codeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_train_codeLb];
    [_train_codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.top.mas_equalTo(_from_station_namelb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_run_timeLb
    _run_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    _run_timeLb.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:_run_timeLb];
    [_run_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.centerY.mas_equalTo(_from_dateLb);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_timePlayLb
    _timePlayLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:13] Radius:0];
    _timePlayLb.textAlignment = NSTextAlignmentCenter;
    _timePlayLb.layer.masksToBounds = YES;
    _timePlayLb.layer.cornerRadius = 2;
    _timePlayLb.layer.borderWidth = 1;
    _timePlayLb.layer.borderColor = Color(@"FFFFFF").CGColor;
    [bgView addSubview:_timePlayLb];
    [_timePlayLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bgView);
        make.centerY.mas_equalTo(_start_timeLb);
        make.width.mas_equalTo(68);
        make.height.mas_equalTo(labelH*0.8);
    }];
    
    UIView *leftLine = [[UIView alloc] init];
    leftLine.backgroundColor = Color(@"FFFFFF");
    [bgView addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_timePlayLb.mas_left);
        make.centerY.mas_equalTo(_timePlayLb);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(1);
    }];
    
    UIView *rightLine = [[UIView alloc] init];
    rightLine.backgroundColor = Color(@"FFFFFF");
    [bgView addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_timePlayLb.mas_right);
        make.centerY.mas_equalTo(_timePlayLb);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(1);
    }];
    
    
    //右边
    //_to_station_namelb
    _to_station_namelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    _to_station_namelb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_to_station_namelb];
    [_to_station_namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_from_station_namelb);
        make.height.mas_equalTo(labelH);
    }];
    
    //_arrive_timeLb
    _arrive_timeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont boldSystemFontOfSize:16] Radius:0];
    _arrive_timeLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_arrive_timeLb];
    [_arrive_timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_to_station_namelb);
        make.top.mas_equalTo(_to_station_namelb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    //_to_dateLb
    _to_dateLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:14] Radius:0];
    [bgView addSubview:_to_dateLb];
    [_to_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_to_station_namelb);
        make.top.mas_equalTo(_arrive_timeLb.mas_bottom);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    _start_timeLb.text = @"00:00";
    _arrive_timeLb.text = @"00:00";
    
    _train_codeLb.text = @"";
    
    _from_station_namelb.text = @"";
    _to_station_namelb.text = @"";
    
    _run_timeLb.text = @"";
    _timePlayLb.text = @"时刻表";
    _headLb.text = @"";
    
}
/*
 G-高铁
 D-动车
 C-城际动车
 Z-直达列车
 T-特快
 K-快速列车
 
 1—5开头-普快列车
 6—7开头-普客列车
 8开头-通勤列车
 */

-(void)setModel:(TrainOrderModel *)model
{
    _model = model;
    
    
    
    
        _train_codeLb.text = [NSString stringWithFormat:@"%@",model.checi];//车次
        _from_station_namelb.text = [NSString stringWithFormat:@"%@",model.from_station_name];//出发车站名
        _to_station_namelb.text = [NSString stringWithFormat:@"%@",model.to_station_name];//到达车站名
        
        /*历时*/
    if (model.runtime.length>0) {//只有出票成功有历时
//
//        NSString *run_time_minute = [NSString stringWithFormat:@"%@",model.runtime];
//        NSInteger hour = run_time_minute.integerValue/60;
//        NSInteger minute = run_time_minute.integerValue%60;
//
//        NSString *run_time = [NSString stringWithFormat:@"%@分",model.runtime];
//        if (hour >0) {
//            run_time = [NSString stringWithFormat:@"%ld小时%ld分",hour,minute];
//        }
        _run_timeLb.text = model.runtime;
    }

        if (model.status.intValue == 2) {
            _headView.hidden = NO;
        }else{
            _headView.hidden = YES;

        }
  
    if (model.start_time.length >6) {
        
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
        NSDate *start_date =[dateFormat dateFromString:model.start_time];
        NSDate *end_date =[dateFormat dateFromString:model.arrive_time];
        if (!start_date||!end_date ) {
            return;
        }
        
        NSString *start_dateStr = [XSTool dateStrStringFromDate:start_date];
        NSString *start_weekStr = [XSTool weekdayStringFromDate:start_date];
        NSString *end_dateStr = [XSTool dateStrStringFromDate:end_date];
        NSString *end_weekStr = [XSTool weekdayStringFromDate:end_date];
        _from_dateLb.text = [NSString stringWithFormat:@"%@ %@",start_dateStr,start_weekStr];
        _to_dateLb.text = [NSString stringWithFormat:@"%@ %@",end_weekStr,end_dateStr];
        
        
        NSDate *start_date1 = [self changeToDateWithDateStr:model.start_time];
        NSDate *arrive_date1 = [self changeToDateWithDateStr:model.arrive_time];
        _start_timeLb.text = [NSString stringWithFormat:@"%@",[self changeToDateStrWithDate:start_date1]];//出发时刻
        _arrive_timeLb.text = [NSString stringWithFormat:@"%@",[self changeToDateStrWithDate:arrive_date1]];//到达时刻
    }
    
    
    
}

#pragma mark - 倒计时通知回调
- (void)countDownNotification2 {
    /// 判断是否需要倒计时 -- 可能有的cell不需要倒计时,根据真实需求来进行判断
    if ([self.model.status intValue]==2) {
        if (self.model.autotime.intValue <= 0) {
            _headLb.text = @"订单超时";
            return;
        }
        //    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
        //
        //    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
        //
        NSInteger timeCount = self.model.autotime.integerValue;
        //    NSLog(@"%@",[XSFormatterDate dateWithTimeIntervalString:self.model.expire_time]);
        /// 计算倒计时
        NSInteger countDown = timeCount - kCountDownManager2.timeInterval;
        /// 当倒计时到了进行回调
        if (countDown <= 0) {
            _headLb.text = @"订单超时";
            return;
        }
        /// 重新赋值
        
        
        NSString *title = [NSString stringWithFormat:@"%@:%02zd%@ %02zd%@ %02zd%@,%@",Localized(@"剩余时间") ,countDown/3600, Localized(@"时"),(countDown/60)%60,Localized(@"分"), countDown%60,Localized(@"秒"),Localized(@"超时订单自动取消")];
        
        //    if (self.model.role.intValue == 1&&self.model.orderStatus .intValue == 3) {
        //        if (self.isCharge) {
        //            title = [NSString stringWithFormat:@"%@:%02zd%@ %02zd%@ %02zd%@",Localized(@"金融家确认收款剩余时间") ,countDown/3600, Localized(@"时"),(countDown/60)%60,Localized(@"分"), countDown%60,Localized(@"秒")];
        //        }
        //    }
        
        _headLb.text =title;
    }
    
    
}






//1、字符串转换为日期
- (NSDate *)changeToDateWithDateStr:(NSString *)dateStr
{
   
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    
    NSDate *date =[dateFormat dateFromString:dateStr];
    return date;
}


//2、日期转换为字符串
- (NSString *)changeToDateStrWithDate:(NSDate *)date
{

    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"HH:mm"];//设定时间格式,这里可以设置成自己需要的格式
    NSString *currentDateStr = [dateFormat stringFromDate:date];
    
    return currentDateStr;
}
- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}



#pragma mark -
- (NSInteger)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //NSLog(@"Date1  is in the future");
        return 1;
    }
    else if (result == NSOrderedAscending){
        //NSLog(@"Date1 is in the past");
        return -1;
    }
    //NSLog(@"Both dates are the same");
    return 0;
    
}
//字符串转日期格式
- (NSDate *)stringToDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
}


@end

