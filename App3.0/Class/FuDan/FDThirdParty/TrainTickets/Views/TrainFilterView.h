//
//  TrainFilterView.h
//  App3.0
//
//  Created by xinshang on 2018/3/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TrainFilterViewDelegate <NSObject>
-(void)cutomBtnClick:(UIButton *)sender;
@end

@interface TrainFilterView : UIView

@property(nonatomic,weak)id<TrainFilterViewDelegate> delegate;
@property (nonatomic, strong) NSArray *listArr; /*数组*/
@property (nonatomic,copy) void(^filterBlock)(NSDictionary *dict,NSInteger index,NSString *from_station,NSString *to_station);
@property (nonatomic,assign)  NSInteger filterIndex;
@end
