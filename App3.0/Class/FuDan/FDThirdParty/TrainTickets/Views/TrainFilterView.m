//
//  TrainFilterView.m
//  App3.0
//
//  Created by xinshang on 2018/3/16.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainFilterView.h"
@interface TrainFilterView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *leftTable;
@property (nonatomic,strong) UITableView *rightTable;
//@property (nonatomic,strong) NSArray *array;
@property (nonatomic,assign) BOOL isSelected;

@property (nonatomic, strong) UIView *headView; /*头部按钮视图*/

@property (nonatomic, strong) UIButton *cancelBtn; /*取消按钮*/
@property (nonatomic, strong) UIButton *sureBtn; /*确定按钮*/
@property (nonatomic, strong) UIButton *resetBtn; /*重置按钮*/
@property (nonatomic, assign) NSInteger  leftSelIndex;
@property (nonatomic, assign) NSInteger  rightSelIndex;

@property (nonatomic, strong) NSString *from_station;
@property (nonatomic, strong) NSString *to_station;
@end
static NSString *const resueIdleft = @"leftCell";
static NSString *const resueIdright = @"rightCell";

@implementation TrainFilterView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.listArr= [NSArray array];
        [self setUpUI];
    }
    return self;
}
-(void)setListArr:(NSArray *)listArr
{
    _listArr = listArr;
    _rightSelIndex = -1;
    _leftSelIndex = 0;
    if (self.filterIndex == 2) {
        self.to_station = @"";
        self.from_station = @"";
    }
    [self.leftTable reloadData];
    [self.rightTable reloadData];

}


- (void)setUpUI
{
    self.backgroundColor = [UIColor colorWithWhite:0.35 alpha:0.4];


    CGFloat space = 10.0;
    CGFloat btnW = 55;
    CGFloat headViewH = 44.0;
    CGFloat bgViewH = 44.0+200;

    //bgView
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(bgViewH);
    }];
    
    //self.headView
    self.headView = [[UIView alloc] init];
    self.headView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:self.headView];
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(headViewH);
    }];
    
    
    //_cancelBtn
    _cancelBtn = [self getCustomBtnWithTitle:@"取消"];
    _cancelBtn.tag = 1;
    [_headView addSubview:_cancelBtn];
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(_headView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(headViewH);
    }];
    
    
    //_resetBtn
    _resetBtn = [self getCustomBtnWithTitle:@"重置"];
    _resetBtn.tag = 2;
    [_headView addSubview:_resetBtn];
    [_resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_headView);
        make.centerY.mas_equalTo(_headView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(headViewH);
    }];
    
    
    //_sureBtn
    _sureBtn = [self getCustomBtnWithTitle:@"确定"];
    [_sureBtn setTitleColor:mainColor forState:UIControlStateNormal];
    _sureBtn.tag = 3;
    [_headView addSubview:_sureBtn];
    [_sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.centerY.mas_equalTo(_headView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(headViewH);
    }];
    
    
    self.leftTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.frame.size.width / 4, self.frame.size.height - 20) style:UITableViewStylePlain];
    self.leftTable.delegate = self;
    self.leftTable.dataSource = self;
    [self.leftTable registerClass:[UITableViewCell class] forCellReuseIdentifier:resueIdleft];
    [self.leftTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    self.leftTable.tableFooterView = [[UIView alloc] init];
    self.leftTable.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:self.leftTable];
    
    [self.leftTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(_headView.mas_bottom);
        make.width.mas_equalTo(mainWidth/3);
        make.bottom.mas_equalTo(0);
    }];
    
    self.rightTable = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.size.width / 4, 20, self.frame.size.width / 4 * 3, self.frame.size.height - 20) style:UITableViewStylePlain];
    self.rightTable.dataSource = self;
    self.rightTable.delegate = self;
    self.rightTable.tableFooterView = [[UIView alloc] init];
    self.rightTable.backgroundColor = [UIColor whiteColor];
    [self.rightTable registerClass:[UITableViewCell class] forCellReuseIdentifier:resueIdright];
    [bgView addSubview:self.rightTable];
    [self.rightTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_headView.mas_bottom);
        make.width.mas_equalTo(mainWidth/3*2);
        make.bottom.mas_equalTo(0);
    }];

    
    
}



#pragma mark ~~~~~~~~~~ TableViewDataSource ~~~~~~~~~~
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.leftTable) {
        return self.listArr.count;
    }else{
        if (self.listArr.count == 0) {
            return 0;
        }
    // 获取选中leftTable那一行的数组
        NSDictionary *tDic = self.listArr[self.leftSelIndex];
        NSArray *arr = [tDic valueForKey:@"data"];
    return arr.count;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    if (tableView == self.leftTable) {
        cell = [tableView dequeueReusableCellWithIdentifier:resueIdleft];
        NSDictionary *tDic = self.listArr[indexPath.row];
        cell.textLabel.text = [tDic valueForKey:@"title"];
        return cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:resueIdright];
        // 获取选中leftTable那一行的数组
        NSDictionary *tDic = self.listArr[self.leftSelIndex];

        NSArray *arr = [tDic valueForKey:@"data"];
        cell.textLabel.text = arr[indexPath.row];
        if (self.rightSelIndex == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;

        }
        return cell;
    }
}

#pragma mark ~~~~~~~~~~ TableViewDelegate ~~~~~~~~~~
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView == self.leftTable) {
        self.leftSelIndex = indexPath.row;
        _rightSelIndex = -1;

        [self.rightTable reloadData];
    }else{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];

        self.rightSelIndex = indexPath.row;
        
        if (self.filterIndex == 2) {
            // 获取选中leftTable那一行的数组
            NSDictionary *tDic = self.listArr[self.leftSelIndex];
            NSArray *arr = [tDic valueForKey:@"data"];
            
            if (self.leftSelIndex == 0) {
                self.from_station = arr[indexPath.row];
            }else{
                self.to_station = arr[indexPath.row];
            }
        }
       
        [self.rightTable reloadData];
    }
}

// 头部视图将要显示时调用
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (tableView == self.rightTable) {
        // 判断，如果是左边点击触发的滚动，这不执行下面代码
        if (self.isSelected) {
            return;
        }
        // 获取可见视图的第一个row
        NSInteger currentSection = [[[self.rightTable indexPathsForVisibleRows] firstObject] section];
        NSIndexPath *index = [NSIndexPath indexPathForRow:currentSection inSection:0];
        // 点击左边对应区块
        [self.leftTable selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
}

// 开始拖动赋值没有点击
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // 当右边视图将要开始拖动时，则认为没有被点击了。
    self.isSelected = NO;
}



- (UIButton *)getCustomBtnWithTitle:(NSString*)title
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setTitle:Localized(title) forState:UIControlStateNormal];
    [customBtn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
    [customBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [customBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return customBtn;
}
-(void)customBtnAction:(UIButton *)sender
{
    NSDictionary *tDic = self.listArr[self.leftSelIndex];
    if (self.filterBlock) {
        self.filterBlock(tDic,self.rightSelIndex,self.from_station,self.to_station);
        
    }
    if ([self.delegate respondsToSelector:@selector(cutomBtnClick:)]) {
        [self.delegate cutomBtnClick:sender];
    }
    
}

@end
