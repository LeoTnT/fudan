//
//  TrainOrderDetialPassengerCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderDetialPassengerCell.h"

@interface TrainOrderDetialPassengerCell()

@property (nonatomic, strong) UILabel *nameLb;//姓名
@property (nonatomic, strong) UILabel *priceLb;//二等座 553
@property (nonatomic, strong) UILabel *card_numLb; /*证件号码*/
@property (nonatomic, strong) UILabel *train_numLb; /*04车厢09A号*/


@property (nonatomic, strong) UILabel *statusLb;//待支付
@end

@implementation TrainOrderDetialPassengerCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor = BG_COLOR;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 100.0;
    CGFloat labelW = (mainWidth-20)/2;
    CGFloat labelH = 28.0;
    
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-space);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_card_numLb
    _card_numLb = [self getLabelWithTextColor:COLOR_111111 Font:[UIFont systemFontOfSize:15] Radius:0];
    [bgView addSubview:_card_numLb];
    [_card_numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(bgView);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:COLOR_111111 Font:[UIFont systemFontOfSize:15] Radius:0];
    [bgView addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.bottom.mas_equalTo(_card_numLb.mas_top).offset(space/5);
        make.height.mas_equalTo(labelH);
    }];
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:COLOR_111111 Font:[UIFont systemFontOfSize:15] Radius:0];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_nameLb);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
   
    
    //_train_numLb
    _train_numLb = [self getLabelWithTextColor:COLOR_111111 Font:[UIFont systemFontOfSize:15] Radius:0];
    _train_numLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_train_numLb];
    [_train_numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_card_numLb);
        make.height.mas_equalTo(labelH);
    }];
    
    //_statusLb
    _statusLb = [self getLabelWithTextColor:Color(@"F78F15") Font:[UIFont boldSystemFontOfSize:15] Radius:0];
    [bgView addSubview:_statusLb];
    [_statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb);
        make.top.mas_equalTo(_card_numLb.mas_bottom).offset(space/5);
        make.height.mas_equalTo(labelH);
    }];
    
    
    _nameLb.text = @"***";
    _priceLb.text = @" 0.00";
    _card_numLb.text = @"- -";
    _train_numLb.text = @"";
    _statusLb.text = @"";

    
}
-(void)setModel:(TrainPassengersModel *)model
{
    _model = model;
    
    _nameLb.text = [NSString stringWithFormat:@"%@",model.passengersename];
    
    if (!isEmptyString(model.passportseno)) {
        if (model.passportseno.length >10) {
            NSString *passportseno =[NSString stringWithFormat:@"%@",model.passportseno];
            NSString *str1 = [passportseno substringToIndex:6];//截取前6位
            NSString *str2 = [passportseno substringFromIndex:passportseno.length-4];//截取后4位
            _card_numLb.text = [NSString stringWithFormat:@"%@******%@",str1,str2];
        }else{
            _card_numLb.text = [NSString stringWithFormat:@"%@",model.passportseno];
        }
    }
    
    if (!isEmptyString(model.cxin)) {
        _train_numLb.text = [NSString stringWithFormat:@"%@",model.cxin];

    }
    
//    _priceLb.text = [NSString stringWithFormat:@"%@",model.price];
    NSString *priceStr = [NSString stringWithFormat:@"%@",model.price];
    NSString *textStr = [NSString stringWithFormat:@"%@ %@",model.zwname,priceStr];
    NSMutableAttributedString *attributeStr = [self setStringWithStr:textStr RangStr:priceStr];
    _priceLb.attributedText=attributeStr;
    
   
}

- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:Color(@"F78F15") range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:range];
    return attributeStr;
    
}

-(void)setStatus:(NSString *)status
{
    _status = status;
    NSString *statusStr = @"";
    switch (status.intValue) {
        case 0:
            statusStr = @"待处理";
            break;
        case 1:
            statusStr = @"已失效";///已取消
            break;
        case 2:
            statusStr = @"待支付";
            break;
        case 3:
            statusStr = @"待出票";
            break;
        case 4:
            statusStr = @"出票成功";
            break;
        case 5:
            statusStr = @"出票失败";
            break;
        case 6:
            statusStr = @"正在处理线上退票请求";
            break;
        case 7:
            statusStr = @"退票(改签)成功";
            break;
        case 8:
            statusStr = @"退票失败";
            break;
            
        default:
            break;
    }
    _statusLb.text = statusStr;

    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

@end
