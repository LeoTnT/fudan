//
//  TrainOrderPreviewAddOrEditCell.h
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TrainOrderPreviewAddOrEditCellDelegate <NSObject>
-(void)customBtnClick:(UIButton *)sender;
@end

@interface TrainOrderPreviewAddOrEditCell : UITableViewCell

@property(nonatomic,weak)id<TrainOrderPreviewAddOrEditCellDelegate> delegate;

@property (nonatomic, assign) NSInteger type;//0(没有乘客)添加乘客 1含有成人票 2只有儿童票
@end
