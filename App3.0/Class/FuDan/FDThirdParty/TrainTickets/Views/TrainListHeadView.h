//
//  TrainListHeadView.h
//  App3.0
//
//  Created by xinshang on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainTicketsModels.h"

@protocol TrainListHeadViewDelegate <NSObject>
-(void)backBtnClick:(UIButton *)sender;
-(void)frontBtnClick:(UIButton *)sender;
-(void)behindBtnClick:(UIButton *)sender;
-(void)dateBtnClick:(UIButton *)sender;

@end
@interface TrainListHeadView : UIView

@property(nonatomic,weak)id<TrainListHeadViewDelegate> delegate;
@property (nonatomic,copy) NSString * from_station;
@property (nonatomic,copy) NSString * to_station;

@property (nonatomic,copy) NSDate * selDate;//选中的日期

@end
