//
//  TrainPassengerListCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainPassengerListCell.h"

@interface TrainPassengerListCell()

@property (nonatomic, strong) UILabel *nameLb;//乘客姓名
@property (nonatomic, strong) UILabel *typeLb;//车票类型
@property (nonatomic, strong) UILabel *cardLb;//身份证号


@end

@implementation TrainPassengerListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =Color(@"FFFFFF") ;
    CGFloat space = 10.0;
    
    CGFloat labelW = 66;
    CGFloat labelH = 35.0;
    
    //_checkBtn
    _checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_checkBtn setImage:[UIImage imageNamed:@"Train_noselect"] forState:UIControlStateNormal];
    [_checkBtn setImage:[UIImage imageNamed:@"Train_select"] forState:UIControlStateSelected];
    [self addSubview:_checkBtn];
    [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(labelH);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:16] Radius:0];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_checkBtn.mas_right);
        make.centerY.mas_equalTo(self).offset(-12);
        make.width.mas_equalTo(labelW);
        make.height.mas_equalTo(labelH);
    }];
    
    //_typeLb
    _typeLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:12.5] Radius:0];
    _typeLb.textAlignment = NSTextAlignmentCenter;
    //    [_typeLb sizeToFit];
    _typeLb.backgroundColor = Color(@"8C9DAB");
    _typeLb.layer.cornerRadius = 2.5;
    [self addSubview:_typeLb];
    [_typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb.mas_right);
        make.centerY.mas_equalTo(_nameLb);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(18);
    }];
    
    //_cardLb
    _cardLb = [self getLabelWithTextColor:Color(@"666666") Font:[UIFont systemFontOfSize:15] Radius:0];
    [self addSubview:_cardLb];
    [_cardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_checkBtn.mas_right);
        make.centerY.mas_equalTo(self).offset(12);
        make.width.mas_equalTo(mainWidth-80);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //checkSelBtn
   UIButton *checkSelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkSelBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:checkSelBtn];
    [checkSelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(labelH*2);
        make.height.mas_equalTo(labelH*2);
    }];
    
    
    
    
    
    _nameLb.text = Localized(@"***");
    _typeLb.text = Localized(@"成人票");
    _cardLb.text = Localized(@"身份证 ");
    
}

-(void)setModel:(TrainPassengerModel *)model
{
    _model = model;
    CGFloat labelH = 35.0;

    _nameLb.text = [NSString stringWithFormat:@"%@",model.passengersename];
    _typeLb.text = [NSString stringWithFormat:@"%@",model.piaotypename];
    _cardLb.text = [NSString stringWithFormat:@"%@ %@",model.passporttypeseidname,model.passportseno];
    
    
    NSString *nameStr = [NSString stringWithFormat:@"%@",model.passengersename];
    CGSize titleSize = [nameStr boundingRectWithSize:CGSizeMake(MAXFLOAT, labelH) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]} context:nil].size;
    [_nameLb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_checkBtn.mas_right);
        make.centerY.mas_equalTo(self).offset(-12);
        make.width.mas_equalTo(titleSize.width+10);
        make.height.mas_equalTo(labelH);
    }];
    [_typeLb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb.mas_right);
        make.centerY.mas_equalTo(_nameLb);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(18);
    }];
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}
-(void)customBtnAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(checkBtnClick:)]) {
        [self.delegate checkBtnClick:self.checkBtn];
    }

}

@end

