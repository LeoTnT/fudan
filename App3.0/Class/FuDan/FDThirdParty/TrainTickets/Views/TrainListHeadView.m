//
//  TrainListHeadView.m
//  App3.0
//
//  Created by xinshang on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainListHeadView.h"

@interface TrainListHeadView()

@property (nonatomic, strong) UIButton *backBtn;//返回
@property (nonatomic, strong) UILabel *startLb;//出发站
@property (nonatomic, strong) UILabel *endLb;//到达站
//@property (nonatomic, strong) UIImageView *arrowImg;//箭头

@property (nonatomic, strong) UIButton *frontBtn;//前一天
@property (nonatomic, strong) UIButton *behindBtn;//后一天
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *centerView;
@property (nonatomic, strong) UILabel *datelb; /*日期*/



//@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation TrainListHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI
{
    self.backgroundColor = mainColor;
    CGFloat space = 10.0;
    
    
    CGFloat bgViewH = 44;
    CGFloat labelW = 90.0;
    CGFloat labelH = 30.0;
    CGFloat imgW = 42.0;

    
    //_backBtn
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[UIImage imageNamed:@"Train_back1"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_backBtn];
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*1.1);
        make.top.mas_equalTo(space*1.5);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    
    
    UIImageView *arrowImg = [[UIImageView alloc] init];
    arrowImg.image = [UIImage imageNamed:@"Train_arrow"];
    [self addSubview:arrowImg];
    [arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(_backBtn);
        make.width.mas_equalTo(imgW*0.8);
        make.height.mas_equalTo(imgW*0.8);
    }];
    
    //_startLb
    _startLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:16] Radius:0];
    [self addSubview:_startLb];
    [_startLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImg.mas_left).offset(-2.5);
        make.top.mas_equalTo(arrowImg);
        make.height.mas_equalTo(imgW);
    }];
    
    //_endLb
    _endLb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:16] Radius:0];
    [self addSubview:_endLb];
    [_endLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(arrowImg.mas_right).offset(2.5);
        make.top.mas_equalTo(arrowImg);
        make.height.mas_equalTo(imgW);
    }];
    
    
    _bgView = [[UIView alloc] init];
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(_backBtn.mas_bottom).offset(space*0.7);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(bgViewH);
    }];
    
    CGFloat btnW = 80;
    //_frontBtn
    _frontBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_frontBtn setImage:[UIImage imageNamed:@"Train_front"] forState:UIControlStateNormal];
    [_frontBtn setTitle:@"前一天" forState:UIControlStateNormal];
    [_frontBtn setTitleColor:Color(@"FFFFFF") forState:UIControlStateNormal];
    [_frontBtn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
     [_frontBtn addTarget:self action:@selector(frontBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:_frontBtn];
    [_frontBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*0.2);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_behindBtn
    _behindBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_behindBtn setImage:[UIImage imageNamed:@"Train_behind"] forState:UIControlStateNormal];
    [_behindBtn setTitle:@"后一天" forState:UIControlStateNormal];
    [_behindBtn addTarget:self action:@selector(behindBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_behindBtn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
    [self setImageToRightWithButton:_behindBtn];
    [_bgView addSubview:_behindBtn];
    [_behindBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space*0.2);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(btnW);
        make.height.mas_equalTo(bgViewH);
    }];
    
    
    //_datelb
    _datelb = [self getLabelWithTextColor:Color(@"FFFFFF") Font:[UIFont systemFontOfSize:13] Radius:0];
    _datelb.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_datelb];
    [_datelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_bgView);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(labelW*1.1);
        make.height.mas_equalTo(labelH);
    }];
    
   
    
    
    UIImageView *dateImg = [[UIImageView alloc] init];
    dateImg.image = [UIImage imageNamed:@"Train_date"];
    [_bgView addSubview:dateImg];
    [dateImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_datelb.mas_left);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(imgW/3);
        make.height.mas_equalTo(imgW/3);
    }];
    
    UIImageView *downImg = [[UIImageView alloc] init];
    downImg.image = [UIImage imageNamed:@"Train_down"];
    [_bgView addSubview:downImg];
    [downImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_datelb.mas_right);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(imgW/3.5);
        make.height.mas_equalTo(imgW/3.5);
    }];
    
    //dateBtn
    UIButton *dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dateBtn addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:dateBtn];
    [dateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_bgView);
        make.centerY.mas_equalTo(_bgView);
        make.width.mas_equalTo(btnW*1.35);
        make.height.mas_equalTo(bgViewH);
    }];

}
#pragma mark --btnAction----

- (void)backBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(backBtnClick:)]) {
        [self.delegate backBtnClick:sender];
    }
    
}
//前一天
- (void)frontBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(frontBtnClick:)]) {
        [self.delegate frontBtnClick:sender];
    }
    
}

- (void)behindBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(behindBtnClick:)]) {
        [self.delegate behindBtnClick:sender];
    }
    
}

//日历
- (void)dateBtnAction:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(dateBtnClick:)]) {
        [self.delegate dateBtnClick:sender];
    }
    
}




- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

//设置图片居右
-(void)setImageToRightWithButton:(UIButton *)btn
{
    if (!btn) {  return;}
    
    NSDictionary *attribute = @{NSFontAttributeName:btn.titleLabel.font};
    //获取文本的宽度
    CGFloat btnWidth = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(0, 24)
                                                         options:
                        NSStringDrawingTruncatesLastVisibleLine |
                        NSStringDrawingUsesLineFragmentOrigin |
                        NSStringDrawingUsesFontLeading
                                                      attributes:attribute
                                                         context:nil].size.width+3;
    
    
    //通过调节文本和图片的内边距到达目的
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, btnWidth, 0, -btnWidth);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.image.size.width, 0, btn.imageView.image.size.width)];
}

-(void)setFrom_station:(NSString *)from_station
{
    _from_station = from_station;
    if (isEmptyString(from_station)) {
        from_station = @"--";
    }
    _startLb.text = [NSString stringWithFormat:@"%@",from_station];
}
-(void)setTo_station:(NSString *)to_station
{
    _to_station = to_station;
    if (isEmptyString(to_station)) {
        to_station = @"--";
    }
    _endLb.text = [NSString stringWithFormat:@"%@",to_station];
    
}


-(void)setSelDate:(NSDate *)selDate
{
    _selDate = selDate;
    NSString *dateStr = [XSTool dateStrStringFromDate:selDate];
    NSString *weekStr = [XSTool weekdayStringFromDate:selDate];
    _datelb.text = [NSString stringWithFormat:@"%@ %@",dateStr,weekStr];
   
    NSInteger index = [self compareOneDay:selDate withAnotherDay:[NSDate date]];
    if (index <=0) {
        _frontBtn.enabled = NO;
        [_frontBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    }else{
        _frontBtn.enabled = YES;
        [_frontBtn setTitleColor:Color(@"FFFFFF") forState:UIControlStateNormal];
    }
}

#pragma mark -
- (NSInteger)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //NSLog(@"Date1  is in the future");
        return 1;
    }
    else if (result == NSOrderedAscending){
        //NSLog(@"Date1 is in the past");
        return -1;
    }
    //NSLog(@"Both dates are the same");
    return 0;
    
}

@end
