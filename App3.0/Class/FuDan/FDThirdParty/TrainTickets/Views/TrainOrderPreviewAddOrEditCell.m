//
//  TrainOrderPreviewAddOrEditCell.m
//  App3.0
//
//  Created by xinshang on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TrainOrderPreviewAddOrEditCell.h"




@interface TrainOrderPreviewAddOrEditCell()
@property (nonatomic, strong) UIButton *addBtn;//添加联系人
@property (nonatomic, strong) UIButton *editBtn;//添加/编辑乘客
@property (nonatomic, strong) UIButton *childenBtn;//添加儿童
@end

@implementation TrainOrderPreviewAddOrEditCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.backgroundColor =Color(@"FFFFFF") ;
//    CGFloat space = 10.0;
//
//    CGFloat labelW = 85;
//    CGFloat labelH = 35.0;
//
    
    
    
    
    //_addBtn
    _addBtn = [self getCustomBtnWithTitle:@"添加乘客"];
    _addBtn.tag = 1;
    [_addBtn setImage:[UIImage imageNamed:@"Train_add"] forState:UIControlStateNormal];
    [self addSubview:_addBtn];
    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.width.mas_equalTo(mainWidth);
        make.top.mas_equalTo(self);
        make.bottom.mas_equalTo(self);
    }];
    
    
    //_editBtn
    _editBtn = [self getCustomBtnWithTitle:@"添加/编辑乘客"];
    _editBtn.tag = 2;
    [self addSubview:_editBtn];
    [_editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.width.mas_equalTo(mainWidth/2);
        make.top.mas_equalTo(self);
        make.bottom.mas_equalTo(self);
    }];
    
    //_childenBtn
    _childenBtn = [self getCustomBtnWithTitle:@"添加儿童"];
    _childenBtn.tag = 3;
    [self addSubview:_childenBtn];
    [_childenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self);
        make.width.mas_equalTo(mainWidth/2);
        make.top.mas_equalTo(self);
        make.bottom.mas_equalTo(self);
    }];
    _addBtn.hidden = YES;

}
//0(没有乘客)添加乘客 1含有成人票 2只有儿童票
-(void)setType:(NSInteger)type
{
    [_editBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.width.mas_equalTo(mainWidth/2);
        make.top.mas_equalTo(self);
        make.bottom.mas_equalTo(self);
    }];
    
    switch (type) {
        case 0:
        {
            _addBtn.hidden = NO;
            _editBtn.hidden = YES;
            _childenBtn.hidden = YES;
        }
            break;
        case 1:
        {
            _addBtn.hidden = YES;
            _editBtn.hidden = NO;
            _childenBtn.hidden = NO;
        }
            break;
        case 2:
        {
            _addBtn.hidden = YES;
            _editBtn.hidden = NO;
            _childenBtn.hidden = YES;
            
            [_editBtn mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self);
                make.width.mas_equalTo(mainWidth);
                make.top.mas_equalTo(self);
                make.bottom.mas_equalTo(self);
            }];
        }
            break;
            
        default:
            break;
    }
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

- (UIButton *)getCustomBtnWithTitle:(NSString*)title
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setTitle:Localized(title) forState:UIControlStateNormal];
    [customBtn setTitleColor:mainColor forState:UIControlStateNormal];
    [customBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [customBtn addTarget:self action:@selector(customBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return customBtn;
}

-(void)customBtnAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(customBtnClick:)]) {
        [self.delegate customBtnClick:sender];
    }
    
}

@end

