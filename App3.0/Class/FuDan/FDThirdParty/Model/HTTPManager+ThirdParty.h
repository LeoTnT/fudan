//
//  HTTPManager+ThirdParty.h
//  App3.0
//
//  Created by 沈浩 on 2018/9/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSApi.h"

//////////////////////话费充值//////////////////////
//查询所有话费面值价格
#define Url_PhoneProductList            @"/api/v1/user/mobilerecharge/PhoneProductList"

//创建充值订单
#define Url_CreatePhoneOrder           @"/api/v1/user/mobilerecharge/CreatePhoneOrder"

//检测手机号码是否能充值 - 查询商品信息
#define Url_TelCheckQuery           @"/api/v1/user/mobilerecharge/TelCheckQuery"

//充值记录查询
#define Url_PhoneOrderList            @"/api/v1/user/mobilerecharge/PhoneOrderList"

//////////////////////火车票//////////////////////
// 站点简码查询,一般不会变，请做好缓存
#define Nevo_train_GetCityCodeUrl            @"/api/v1/user/train/GetCityCode"

// 获取火车票规则 协议
#define Nevo_train_GetNoticeUrl              @"/api/v1/user/train/GetNotice"

// 添加乘车人
#define Nevo_train_AddPassengerUrl           @"/api/v1/user/train/PassengerAdd"

//FD 编辑乘车人
#define Nevo_train_EditPassengerUrl           @"/api/v1/user/train/PassengerEdit"

//FD 删除乘车人
#define Nevo_train_DelPassengerUrl            @"/api/v1/user/train/PassengerDel"

// 获取乘车人(乘客信息列表[所有])
#define Nevo_train_GetPassengerUrl            @"/api/v1/user/train/PassengerList"

// 单个订单详情查询
#define Nevo_train_QueryOrderStatusUrl        @"/api/v1/user/train/QueryOrder"

// 查询用户所有的订单
#define Nevo_train_QueryAllOrderUrl           @"/api/v1/user/train/QueryOrders"

// 余票查询
#define Nevo_train_QueryTicketsUrl            @"/api/v1/user/train/QueryTickets"

// 取消待支付的订单
#define Nevo_train_CancelOrderUrl             @"/api/v1/user/train/TrainCancel"

// 支付-请求出票
#define Nevo_train_PayUrl                     @"/api/v1/user/train/TrainPay"

// 线上退票
#define Nevo_train_RefundUrl                  @"/api/v1/user/train/TrainRefund"

//FD 火车票订单发送短信
#define Nevo_train_SendMsg                    @"/api/v1/user/train/TrainSendMsg"

// 提交订单（未支付）
#define Nevo_train_SubmitUrl                  @"/api/v1/user/train/TrainSubmit"


//////////////////////飞机票//////////////////////

//机场查询
#define Nevo_air_QueryAirportUrl            @"/api/v1/user/airticket/QueryAirport"

//航班查询
#define Nevo_air_FlightsUrl            @"/api/v1/user/airticket/Flights"

//报价查询
#define Nevo_air_QueryPriceUrl            @"/api/v1/user/airticket/QueryPrice"

@interface HTTPManager (ThirdParty)

//////////////////////话费充值//////////////////////
//获取手机号充值列表
+ (void)TelCheckResWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//创建充值订单
+ (void)TelCreatePhoneOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//检测手机号码是否能充值 - 查询商品信息
+ (void)TelCheckQueryWithParams:(NSDictionary *)params
                               success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//充值记录查询
+ (void)TelQueryOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/******************************* 火车票 ***************************/
//1 站点简码查询,一般不会变，请做好缓存
/*
 stationName => ''; 站点名，如苏州、苏州北，不需要加“站”字
 all => '0'  如果需要全部站点简码，请将此参数设为1
 refresh => '0'  是否刷新缓存:默认0->不刷新|1->刷新
 */
+ (void)train_GetCityCodeWithStationName:(NSString *)stationName
                                     all:(NSString *)all
                                 refresh:(NSString *)refresh
                                 success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//2  余票查询
/*
 train_date=> '2018-04-01'; 发车日期，如：2015-07-01（务必按照此格式）
 from_station=> 'BJP'  出发站简码，如：BJP
 to_station=> 'SZH'  到达站简码，如：SZH
 */
+ (void)train_QueryTicketsWithDic:(NSDictionary*)param
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail;

//3 添加乘车人
/*
 passengersename => ‘张三’ ; // 乘车人姓名
 piaotype = > '1';  //1 :成人票,2 :儿童票,4 :残军票  没有学生票只有这三种
 piaotypename => '成人票'； //填写上面对面的名称
 passporttypeseid => '1'; //1:二代身份证,2:一代身份证,C:港澳通行证,B:护照,G:台湾通行证
 passporttypeseidname => '二代身份证' // 证件类型名称 上面的
 passportseno => '420205199207231234' //乘客证件号码
 */
+ (void)train_AddPassengerWithPassengersename:(NSString *)passengersename
                                     piaotype:(NSString *)piaotype
                                 piaotypename:(NSString *)piaotypename
                             passporttypeseid:(NSString *)passporttypeseid
                         passporttypeseidname:(NSString *)passporttypeseidname
                                 passportseno:(NSString *)passportseno
                                      success:(XSAPIClientSuccess)success
                                         fail:(XSAPIClientFailure)fail;

//4 获取乘车人
+ (void)train_GetPassengerSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 传参：
 train_date  发车日期，如：2015-07-01（务必按照此格式）
 
 from_station_code 出发站简码，如：BJP
 
 to_station_code 到达站简码，如：SZH
 
 is_accept_standing 是否接受无座（最低座次无票时自动尝试抢无座票），传值yes则接受，传值no则不接受，默认yes
 
 //choose_seats 需要选的座位，如：1A2B2C，详情请查阅 https://code.juhe.cn/docs/201第43条 选座去掉不要了
 
 checi 车次，如：G7027，请注意：出发站、到达站信息必须属实，例如G101车次不经过北京（经过北京南），出发站信息中不能填北京
 
 passengers 乘车人信息
 */

+(void)train_SubmitWithTrain_date:(NSString *)train_date
                from_station_code:(NSString *)from_station_code
                  to_station_code:(NSString *)to_station_code
               is_accept_standing:(NSString *)is_accept_standing
                            checi:(NSString *)checi
                       start_time:(NSString *)start_time
                      arrive_time:(NSString *)arrive_time
                from_station_name:(NSString *)from_station_name
                  to_station_name:(NSString *)to_station_name
                      orderamount:(NSString *)orderamount
                          runtime:(NSString *)runtime
                            phone:(NSString *)phone
                          pay_pwd:(NSString *)pay_pwd
                       passengers:(NSArray *)passengers
                          success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

//6 单个订单查询
/*
 orderid=>''; 订单号 orderid
 */
+ (void)train_QueryOrderStatusWithOrderid:(NSString *)orderid
                                  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**
 * @description 修改乘车人
 * @param string $id 乘客id 必须
 * @param string $passengersename 乘车人姓名
 * @param string $piaotype 1 :成人票,2 :儿童票,4 :残军票
 * @param string $piaotypename 票种名称
 * @param string $passporttypeseid 1:二代身份证,2:一代身份证,C:港澳通行证,B:护照,G:台湾
 * @param string $passporttypeseidname 证件类型名称
 * @param string $passportseno 如：420205199207231234。乘客证件号码
 * @return bool
 * @return_format status
 */

+ (void)train_EditPassengerWithID:(NSString *)ID
                  passengersename:(NSString *)passengersename
                         piaotype:(NSString *)piaotype
                     piaotypename:(NSString *)piaotypename
                 passporttypeseid:(NSString *)passporttypeseid
             passporttypeseidname:(NSString *)passporttypeseidname
                     passportseno:(NSString *)passportseno
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail;

/**
 * @description 删除乘车人
 * @param string $id 乘客id
 * @return bool
 * @return_format status
 */
+ (void)train_DelPassengerWithID:(NSString *)ID
                         success:(XSAPIClientSuccess)success
                            fail:(XSAPIClientFailure)fail;

//7 查询用户所有的订单
+ (void)train_QueryAllOrderWithPage:(NSString *)page
                             status:(NSString *)status
                            success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//8 取消待支付的订单
+ (void)train_train_CancelOrderWithOrderid:(NSString *)orderid
                                   success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//9 支付订单
//价格显示是每张票加2元手续费，退票不退这两块手续费的！ 价格后台取数据库里的价格
+ (void)train_PayWithOrderid:(NSString *)orderid
                     pay_pwd:(NSString *)pay_pwd
                     success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//9 申请线上退票
+ (void)train_RefundWithOrderid:(NSString *)orderid passengers:(NSString *)passengers  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//10 票务说明
//@param string $type 参数类型 train_submit 预订 train_refund 退票
+ (void)train_GetNoticeWithType:(NSString *)type
                        success:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail;

//获取附近人 暂时无分页
+ (void)nearbyGetUsersWithLat:(NSString *)lat
                          lng:(NSString *)lng
                      success:(XSAPIClientSuccess)success
                         fail:(XSAPIClientFailure)fail;

//实时同步更新位置信息 [登录用户更换地址实时更新定位]
+ (void)nearbyUpdateInfoWithLat:(NSString *)lat
                            lng:(NSString *)lng
                           name:(NSString *)name
                        success:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail;



//////////////////////飞机票//////////////////////
//机场查询
// 1国内 只有国内 默认
+ (void)air_QueryAirportWithWhere:(NSString *)where
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail;
//航班查询
+ (void)air_FlightsWithDpt:(NSString *)dpt
                       arr:(NSString *)arr
                      date:(NSString *)date
                   success:(XSAPIClientSuccess)success
                      fail:(XSAPIClientFailure)fail;


//报价查询

+ (void)air_QueryPriceWithDpt:(NSString *)dpt
                          arr:(NSString *)arr
                         date:(NSString *)date
                    flightNum:(NSString *)flightNum
                      success:(XSAPIClientSuccess)success
                         fail:(XSAPIClientFailure)fail;
@end
