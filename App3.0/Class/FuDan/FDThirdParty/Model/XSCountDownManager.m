//
//  XSCountDownManager.m
//  App3.0
//
//  Created by xinshang on 2017/9/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSCountDownManager.h"


@interface XSCountDownManager ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation XSCountDownManager

+ (instancetype)manager {
    static XSCountDownManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[XSCountDownManager alloc]init];
    });
    return manager;
}
+ (instancetype)manager1 {
    static XSCountDownManager *manager1 = nil;
    static dispatch_once_t onceToken1;
    dispatch_once(&onceToken1, ^{
        manager1 = [[XSCountDownManager alloc]init];
    });
    return manager1;
}
+ (instancetype)manager2 {
    static XSCountDownManager *manager2 = nil;
    static dispatch_once_t onceToken2;
    dispatch_once(&onceToken2, ^{
        manager2 = [[XSCountDownManager alloc]init];
    });
    return manager2;
}
- (void)start {
    // 启动定时器
    [self timer];
}

- (void)reload {
    // 刷新只要让时间差为0即可
    _timeInterval = 0;
}

- (void)invalidate {
    [self.timer invalidate];
    self.timer = nil;
    self.timeInterval = 0;
}

- (void)timerAction {
    // 时间差+1
    self.timeInterval ++;
    // 发出通知--可以将时间差传递出去,或者直接通知类属性取
    [[NSNotificationCenter defaultCenter] postNotificationName:kCountDownNotification object:nil userInfo:@{@"TimeInterval" : @(self.timeInterval)}];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCountDownNotification1 object:nil userInfo:@{@"TimeInterval" : @(self.timeInterval)}];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCountDownNotification2 object:nil userInfo:@{@"TimeInterval" : @(self.timeInterval)}];

}

- (NSTimer *)timer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}

@end
