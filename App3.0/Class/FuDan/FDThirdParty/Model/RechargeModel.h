//
//  RechargeModel.h
//  App3.0
//
//  Created by xinshang on 2017/10/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeModel : NSObject
@property (nonatomic,strong) NSString * cardid;
@property (nonatomic,strong) NSString * inprice;
@property (nonatomic,strong) NSString * nmc;
@property (nonatomic,strong) NSString * game_area;
@property (nonatomic,strong) NSString * price;
@property (nonatomic,strong) NSString * toperator;//0移动1联通2电信
@end

@interface RechargeResultModel : NSObject
@property (nonatomic,strong) NSString * cardid;
@property (nonatomic,strong) NSString * cardnum;
@property (nonatomic,strong) NSString * ordercash;
@property (nonatomic,strong) NSString * cardname;
@property (nonatomic,strong) NSString * sporder_id;
@property (nonatomic,strong) NSString * uorderid;
@property (nonatomic,strong) NSString * game_userid;
@property (nonatomic,strong) NSString * game_state;
@end


@interface RechargeListModel : NSObject
@property (nonatomic,strong) NSString * username;
@property (nonatomic,strong) NSString * m_order_id;
@property (nonatomic,strong) NSString * phoneno;
@property (nonatomic,strong) NSString * money;
@property (nonatomic,strong) NSString * r_money;
@property (nonatomic,strong) NSString * w_time;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * toperator;//0移动1联通2电信
@end
