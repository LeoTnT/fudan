//
//  HTTPManager+ThirdParty.m
//  App3.0
//
//  Created by 沈浩 on 2018/9/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HTTPManager+ThirdParty.h"

@implementation HTTPManager (ThirdParty)
//////////////////////话费充值//////////////////////
//获取手机号充值列表
+ (void)TelCheckResWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_PhoneProductList parameters:params success:success failure:fail];
}

//创建充值订单
/*
 phoneno    string    必填    手机号码
 cardid    float    -    商品查询记录ID    TelCheckQuery返回
 pay_pwd    float    必填    支付密码
 */
+ (void)TelCreatePhoneOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_CreatePhoneOrder parameters:params success:success failure:fail];
}

// 检测手机号码是否能充值 - 查询商品信息
+ (void)TelCheckQueryWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_TelCheckQuery parameters:params success:success failure:fail];
}

//充值记录查询
+ (void)TelQueryOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_PhoneOrderList parameters:params success:success failure:fail];
}


//////////////////////火车票//////////////////////
//1 站点简码查询,一般不会变，请做好缓存
/*
 stationName => ''; 站点名，如苏州、苏州北，不需要加“站”字
 all => '0'  如果需要全部站点简码，请将此参数设为1
 refresh => '0'  是否刷新缓存:默认0->不刷新|1->刷新
 */
+ (void)train_GetCityCodeWithStationName:(NSString *)stationName
                                     all:(NSString *)all
                                 refresh:(NSString *)refresh
                                 success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"stationName":stationName,
                            @"all":all,
                            @"refresh":refresh,
                            };
    
    [XSHTTPManager post:Nevo_train_GetCityCodeUrl parameters:param success:success failure:fail];
}

//2  余票查询
/*
 train_date=> '2018-04-01'; 发车日期，如：2015-07-01（务必按照此格式）
 from_station=> 'BJP'  出发站简码，如：BJP
 to_station=> 'SZH'  到达站简码，如：SZH
 */
+ (void)train_QueryTicketsWithDic:(NSDictionary*)param
                          success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *tParam = param;
    [XSHTTPManager post:Nevo_train_QueryTicketsUrl parameters:tParam success:success failure:fail];
}


//3 添加乘车人
/*
 passengersename => ‘张三’ ; // 乘车人姓名
 piaotype = > '1';  //1 :成人票,2 :儿童票,4 :残军票  没有学生票只有这三种
 piaotypename => '成人票'； //填写上面对面的名称
 passporttypeseid => '1'; //1:二代身份证,2:一代身份证,C:港澳通行证,B:护照,G:台湾通行证
 passporttypeseidname => '二代身份证' // 证件类型名称 上面的
 passportseno => '420205199207231234' //乘客证件号码
 */
+ (void)train_AddPassengerWithPassengersename:(NSString *)passengersename
                                     piaotype:(NSString *)piaotype
                                 piaotypename:(NSString *)piaotypename
                             passporttypeseid:(NSString *)passporttypeseid
                         passporttypeseidname:(NSString *)passporttypeseidname
                                 passportseno:(NSString *)passportseno
                                      success:(XSAPIClientSuccess)success
                                         fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"passengersename":passengersename,
                            @"piaotype":piaotype,
                            @"piaotypename":piaotypename,
                            @"passporttypeseid":passporttypeseid,
                            @"passporttypeseidname":passporttypeseidname,
                            @"passportseno":passportseno
                            };
    [XSHTTPManager post:Nevo_train_AddPassengerUrl parameters:param success:success failure:fail];
}

//4 获取乘车人
+ (void)train_GetPassengerSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = nil;
    
    [XSHTTPManager post:Nevo_train_GetPassengerUrl parameters:param success:success failure:fail];
}


/*
 传参：
 train_date  发车日期，如：2015-07-01（务必按照此格式）
 
 from_station_code 出发站简码，如：BJP
 
 to_station_code 到达站简码，如：SZH
 
 is_accept_standing 是否接受无座（最低座次无票时自动尝试抢无座票），传值yes则接受，传值no则不接受，默认yes
 
 //choose_seats 需要选的座位，如：1A2B2C，详情请查阅 https://code.juhe.cn/docs/201第43条 选座去掉不要了
 
 checi 车次，如：G7027，请注意：出发站、到达站信息必须属实，例如G101车次不经过北京（经过北京南），出发站信息中不能填北京
 
 passengers 乘车人信息
 */

+(void)train_SubmitWithTrain_date:(NSString *)train_date
                from_station_code:(NSString *)from_station_code
                  to_station_code:(NSString *)to_station_code
               is_accept_standing:(NSString *)is_accept_standing
                            checi:(NSString *)checi
                       start_time:(NSString *)start_time
                      arrive_time:(NSString *)arrive_time
                from_station_name:(NSString *)from_station_name
                  to_station_name:(NSString *)to_station_name
                      orderamount:(NSString *)orderamount
                          runtime:(NSString *)runtime
                            phone:(NSString *)phone
                          pay_pwd:(NSString *)pay_pwd
                       passengers:(NSArray *)passengers
                          success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passengers
                                                       options:kNilOptions
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    NSDictionary *param = @{
                            @"train_date":train_date,
                            @"from_station_code":from_station_code,
                            @"to_station_code":to_station_code,
                            @"is_accept_standing":is_accept_standing,
                            @"checi":checi,
                            @"start_time":start_time,
                            @"arrive_time":arrive_time,
                            @"from_station_name":from_station_name,
                            @"to_station_name":to_station_name,
                            @"orderamount":orderamount,
                            @"runtime":runtime,
                            @"pay_pwd":pay_pwd,
                            @"passengers":jsonString,
                            @"phone":phone
                            };
    [XSHTTPManager post:Nevo_train_SubmitUrl parameters:param success:success failure:failure];
}



//6 单个订单查询
/*
 orderid=>''; 订单号 orderid
 */
+ (void)train_QueryOrderStatusWithOrderid:(NSString *)orderid
                                  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"oid":orderid};
    [XSHTTPManager post:Nevo_train_QueryOrderStatusUrl parameters:param success:success failure:fail];
}
//7 查询用户所有的订单
+ (void)train_QueryAllOrderWithPage:(NSString *)page
                             status:(NSString *)status
                            success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSMutableDictionary *param =  [@{@"page":page} mutableCopy];
    if (!isEmptyString(status)) {
        [param setObject:status forKey:@"status"];
    }
    
    [XSHTTPManager post:Nevo_train_QueryAllOrderUrl parameters:param success:success failure:fail];
}

//8 取消待支付的订单
+ (void)train_train_CancelOrderWithOrderid:(NSString *)orderid
                                   success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"oid":orderid};
    [XSHTTPManager post:Nevo_train_CancelOrderUrl parameters:param success:success failure:fail];
}
//9 支付订单
+ (void)train_PayWithOrderid:(NSString *)orderid
                     pay_pwd:(NSString *)pay_pwd
                     success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"orderid":orderid,
                            @"pay_pwd":pay_pwd
                            };
    [XSHTTPManager post:Nevo_train_PayUrl parameters:param success:success failure:fail];
}
//9 申请线上退票
+ (void)train_RefundWithOrderid:(NSString *)orderid passengers:(NSString *)passengers
                        success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    //    NSError *error = nil;
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tickets
    //                                                       options:kNilOptions
    //                                                         error:&error];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData
    //                                                 encoding:NSUTF8StringEncoding];
    NSDictionary *param = @{@"oid":orderid,
                                                        @"passengers":passengers
                            };
    [XSHTTPManager post:Nevo_train_RefundUrl parameters:param success:success failure:fail];
}
// 新加 编辑乘车人
+ (void)train_EditPassengerWithID:(NSString *)ID
                  passengersename:(NSString *)passengersename
                         piaotype:(NSString *)piaotype
                     piaotypename:(NSString *)piaotypename
                 passporttypeseid:(NSString *)passporttypeseid
             passporttypeseidname:(NSString *)passporttypeseidname
                     passportseno:(NSString *)passportseno
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":ID,
                            @"passengersename":passengersename,
                            @"piaotype":piaotype,
                            @"piaotypename":piaotypename,
                            @"passporttypeseidname":passporttypeseidname,
                            @"passportseno":passportseno,
                            @"passporttypeseid":passporttypeseid
                            };
    [XSHTTPManager post:Nevo_train_EditPassengerUrl parameters:param success:success failure:fail];
}

//新加 删除乘车人
+ (void)train_DelPassengerWithID:(NSString *)ID
                         success:(XSAPIClientSuccess)success
                            fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":ID };
    [XSHTTPManager post:Nevo_train_DelPassengerUrl parameters:param success:success failure:fail];
}


//10 票务说明
//@param string $type 参数类型 train_submit 预订 train_refund 退票
+ (void)train_GetNoticeWithType:(NSString *)type
                        success:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"type":type };
    [XSHTTPManager post:Nevo_train_GetNoticeUrl parameters:param success:success failure:fail];
}

//////////////////////飞机票//////////////////////
//机场查询
// 1国内 只有国内 默认
+ (void)air_QueryAirportWithWhere:(NSString *)where
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"where":where };
    [XSHTTPManager post:Nevo_air_QueryAirportUrl parameters:param success:success failure:fail];
}
//航班查询
+ (void)air_FlightsWithDpt:(NSString *)dpt
                       arr:(NSString *)arr
                      date:(NSString *)date
                   success:(XSAPIClientSuccess)success
                      fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"dpt":dpt,
                            @"arr":arr,
                            @"date":date,
                            @"ex_track":@"youxuan",
                            };
    [XSHTTPManager post:Nevo_air_FlightsUrl parameters:param success:success failure:fail];
}
//报价查询

+ (void)air_QueryPriceWithDpt:(NSString *)dpt
                          arr:(NSString *)arr
                         date:(NSString *)date
                    flightNum:(NSString *)flightNum
                      success:(XSAPIClientSuccess)success
                         fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"dpt":dpt,
                            @"arr":arr,
                            @"date":date,
                            @"flightNum":@"flightNum",
                            @"ex_track":@"youxuan",
                            
                            };
    [XSHTTPManager post:Nevo_air_QueryPriceUrl parameters:param success:success failure:fail];
}
@end
