//
//  XSCountDownManager.h
//  App3.0
//
//  Created by xinshang on 2017/9/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 使用单例
#define kCountDownManager [XSCountDownManager manager]
#define kCountDownManager1 [XSCountDownManager manager1]
#define kCountDownManager2 [XSCountDownManager manager2]
/// 倒计时的通知
#define kCountDownNotification @"CountDownNotification"
#define kCountDownNotification1 @"CountDownNotification1"
#define kCountDownNotification2 @"CountDownNotification2"

@interface XSCountDownManager : NSObject

/// 时间差(单位:秒)
@property (nonatomic, assign) NSInteger timeInterval;

/// 使用单例
+ (instancetype)manager;
+ (instancetype)manager1;
+ (instancetype)manager2;
/// 开始倒计时
- (void)start;
/// 刷新倒计时
- (void)reload;
/// 停止倒计时
- (void)invalidate;

@end
