//
//  MobileRechargeVC.m
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobileRechargeVC.h"
#import "MobileRechargeTopCell.h"
#import "MobileRechargeView.h"
#import "RechagreListVC.h"
#import "MobilePhoneContactVC.h"
#import "RechargeModel.h"
//#import "FinancierAlertView.h"
#import "UserModel.h"
#import "ModifyOldPayViewController.h"
#import "PasswordAlertView.h"
#import "MissPayPswViewController.h"
#import "MobileRechargeAppCatargeCell.h"
#import "HTTPManager+ThirdParty.h"
#import "RSAEncryptor.h"

//#import "TrainTicketHomeVC.h"
//#import "AirTicketHomeVC.h"
//#import "RedPacketGamesVC.h"


#define Space 10.0
#define BtnWidth  (mainWidth-Space*4)/3
#define BtnHeight BtnWidth *0.6

@interface MobileRechargeVC ()<MobileRechargeViewDelegate,MobileRechargeAppCatargeCellDelegate,UITextFieldDelegate>
{
    BOOL _isTimer;// 是否在倒计时
    
}
@property (nonatomic, strong) UITextField *mobileTF;
//@property(nonatomic,strong) FinancierAlertView *alertView;
@property (nonatomic, strong) UILabel *tipLb;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) MobileRechargeView *selView;

@property (nonatomic, strong) RechargeResultModel *resultModel;
@property (nonatomic, strong) RechargeModel *selModel;
@property (nonatomic, strong) UIButton *chargeBtn;
@property (nonatomic, assign) BOOL isEnable;
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic ,strong) dispatch_source_t timer;
@property (nonatomic ,strong) NSTimer *onTimer;
@property(nonatomic,strong) NSString *payPassWord;
@property(nonatomic,strong) PasswordAlertView *pswView;

@end

@implementation MobileRechargeVC
NSInteger idex;//定义全局变量

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getUserInformation];
    
}

- (void)getUserInformation {
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord = parser.pay_password;
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isEnable = NO;
    self.title = @"手机充值";
    @weakify(self);
    [self actionCustomRightBtnWithNrlImage:@"" htlImage:nil title:@"充值记录" action:^{
        @strongify(self);
        RechagreListVC *vc =[[RechagreListVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        
    }];
//    [self.navRightBtn setTitleColor:mainColor forState:UIControlStateNormal];
    
    [self SetUpUI];
    [self getTelCheckRes];
}

- (void)SetUpUI
{
    _isTimer = NO;

    [self.listArr  removeAllObjects];
    for (int n = 0; n < 9; n++) {
        RechargeModel*model = [[RechargeModel alloc] init];
        if (n == 0) {
            model.price = @"1";
            model.inprice = @"1.06";
            model.nmc = @"1.06";
        }
        if (n == 1) {
            model.price = @"5";
            model.inprice = @"5.08";
            model.nmc = @"5.08";

        }
        if (n == 2) {
            model.price = @"10";
            model.inprice = @"10.08";
            model.nmc = @"10.08";

        }
        if (n == 3) {
            model.price = @"20";
            model.inprice = @"20.3";
            model.nmc = @"20.3";

        }
        if (n == 4) {
            model.price = @"30";
            model.inprice = @"29.89";
            model.nmc = @"29.89";

        }
        if (n == 5) {
            model.price = @"50";
            model.inprice = @"49.81";
            model.nmc = @"49.81";

        }
        if (n == 6) {
            model.price = @"100";
            model.inprice = @"99.63";
            model.nmc = @"99.63";

        }
        if (n == 7) {
            model.price = @"200";
            model.inprice = @"199.26";
            model.nmc = @"199.26";

        }
        if (n == 8) {
            model.price = @"300";
            model.inprice = @"298.89";
            model.nmc = @"298.89";

        }
//        [self.listArr addObject:model];
    }

    self.view.backgroundColor = [UIColor whiteColor];
 
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
}



- (void)onTimerAction {
    [self getTelCheckRes];
}


- (void)getTelCheckRes
{
    NSString *mobileStr =[self.mobileTF.text stringByReplacingOccurrencesOfString:@" "withString:@""];
    NSLog(@"手机号:%@",mobileStr);
    if (!mobileStr) {
        if ([UserInstance ShardInstnce].mobile) {
            mobileStr = [UserInstance ShardInstnce].mobile;
        } else {
            mobileStr = @"";
        }
    }
    
    __weak __typeof__(self) wSelf = self;
//    if (!self.isFirst) {
        [XSTool showProgressHUDWithView:self.view];
//    }
    [HTTPManager TelCheckResWithParams:@{@"phoneno":mobileStr}
                               success:^(NSDictionary *dic, resultObject *state)
     {
         wSelf.isFirst = YES;
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             NSArray *array = dic[@"data"];
             if (array.count >0) {
                 [self.listArr removeAllObjects];
                 self.listArr = [NSMutableArray arrayWithArray:[RechargeModel mj_objectArrayWithKeyValuesArray:array]];
                 
                 self.isEnable = YES;
                 self.tipLb.hidden = NO;
                 self.tipLb.textColor =[ UIColor grayColor];
                 RechargeModel *model = self.listArr.firstObject;
                 self.tipLb.text = [NSString stringWithFormat:@"%@",model.game_area];
                 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
                 [wSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             }
         }else{
             self.isEnable = NO;
                 Alert(state.info);
             if ([state.info isEqualToString:@"对不起，此手机号暂不支持充值"]) {
                 self.tipLb.hidden = NO;
                 self.tipLb.text = @"暂不支持此号码充值";
                 self.tipLb.textColor = [UIColor redColor];
             }
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}



#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return 1;
    }
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        static NSString *CellIdentifier = @"MobileRechargeTopCell";
        MobileRechargeAppCatargeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[MobileRechargeAppCatargeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
        }
        cell.delegate = self;
        return cell;
    } else {
    
  if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"MobileRechargeTopCell";
        MobileRechargeTopCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[MobileRechargeTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
        }
        if (!self.mobileTF) {
            self.mobileTF = cell.mobileTF;
            if ([UserInstance ShardInstnce].mobile) {
                self.mobileTF.text = [UserInstance ShardInstnce].mobile;
            }
        }
        
        if (!self.tipLb) {
            self.tipLb = cell.tipLb;
        }
        self.mobileTF.keyboardType = UIKeyboardTypeNumberPad;
        self.mobileTF.delegate = self;
        [cell.rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
        [self.mobileTF addTarget:self action:@selector(textFieldDidEditing:) forControlEvents:UIControlEventEditingChanged];
        return cell;
    }else if (indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];

        }
        for (UIView *view in cell.subviews) {
            if ([view isKindOfClass:[MobileRechargeView class]]) {
                [view removeFromSuperview];
            }
        }
        
        for (int j = 0; j < self.listArr.count; j++) {
            CGRect frame =  CGRectMake(Space+j%3*(Space +BtnWidth), Space+j/3*(Space +BtnHeight), BtnWidth, BtnHeight);
            RechargeModel *model = self.listArr[j];
            NSString *title1 = [NSString stringWithFormat:@"%@元",model.price];
            NSString *title2 = [NSString stringWithFormat:@"售价:%@元",model.inprice];
            MobileRechargeView *view = [[MobileRechargeView alloc] initWithFrame:frame Title1:title1 title2:title2  enable:self.isEnable];
            view.index = j;
            view.delegate = self;
            [cell addSubview:view];
            if (j== 0) {
//                view.isSelect = YES;
//                self.selView = view;
             }
            
        }
        
        
        

        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell3";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
        }
        if (!self.chargeBtn) {
            self.chargeBtn = [self creatChargeBtn];
            [cell addSubview:self.chargeBtn];
        }
        self.chargeBtn.enabled = self.isEnable;
        if (!self.isEnable) {
            self.chargeBtn.backgroundColor = [UIColor grayColor];
        }else{
            self.chargeBtn.backgroundColor = mainColor;
        }
        return cell;
    
    }
    }
}






-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return 181;
    }else{
        if (indexPath.row == 0) {
            return 135;
        }else if (indexPath.row==1){
            return ((self.listArr.count-1)/3+1)*(Space +BtnHeight);
        }
        return 80;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, mainWidth-24, 40)];
    [view addSubview:headLabel];
    headLabel.backgroundColor = [UIColor whiteColor];
    if (section == 0) {
        headLabel.text = @"充话费";
    } else {
        headLabel.text = @"更多缴费";
    }
    
    headLabel.font = [UIFont systemFontOfSize:15];
    headLabel.textColor = COLOR_111111;
    return view;
   
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 40;
    }
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }
    return 0.01;
}

#pragma mark ----- 充值
- (void)chargeBtnAction:(UIButton*)sender
{
    [self.view endEditing:YES];
    if (self.mobileTF.text.length < 1) {
        Alert(@"请输入手机号");
        return;
    }
    
    if (!_selView) {
        Alert(@"请选择充值金额");
        return;
    }
    if ([self.payPassWord isEqualToString:@""]) {
        //未设置支付密码
        //弹出警告框
        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"提示") message:Localized(@"您还没有设置支付密码，是否要设置支付密码？") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"取消") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"确认") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            //跳转到设置支付密码界面
            [self pushChangePayView];

            
        }];
        [alertControl addAction:cancelAction];
        [alertControl addAction:okAction];
        [self presentViewController:alertControl animated:YES completion:nil];
        return;
    }
    

    self.selModel = self.listArr[_selView.index];

    self.pswView = [[PasswordAlertView alloc] initPasswordView];
    @weakify(self);
    self.pswView.passWordText = ^(NSString *text) {
        @strongify(self);
        NSLog(@"text=%@",text);
        if (text.length == 6) {
            [self ActiveRequestWithPwd:text];
            [self.pswView removeFromSuperview];
            
        }
    };
//    self.pswView.forgetBlock = ^{
//        @strongify(self);
//        //跳转到设置支付密码界面
//        [self forgetBtnAction];
//        [self.pswView removeFromSuperview];
//
//    };
    [self.pswView passwordShow];

  
}





//跳转到设置支付密码界面
- (void)pushChangePayView
{
    ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
    [self.navigationController pushViewController:oldPayViewController animated:YES];
    
}


#pragma mark - 忘记密码
-(void)forgetBtnAction{
    MissPayPswViewController *missVC = [[MissPayPswViewController alloc] init];
    [self.navigationController pushViewController:missVC animated:YES];
    
}
//
//
//#pragma mark - 点击代理
////验证码
//-(void)codeBtnSelected:(UIButton *)sender
//{
//    if ([UserInstance ShardInstnce].mobile.length<=0) {
//        Alert(@"获取手机号失败");
//        return;
//    }
//
//    sender.enabled = NO; // 设置按钮为不可点击
//    [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
//    // 获取验证码
//    [HTTPManager getSmsVerify:[UserInstance ShardInstnce].mobile success:^(NSDictionary *dic, resultObject *state) {
//        // 验证码发送成功
//        if (state.status) {
//            [XSTool showToastWithView:self.alertView Text:Localized(@"发送成功")];
//            _isTimer = YES; // 设置倒计时状态为YES
//            sender.enabled = NO; // 设置按钮为不可点击
//
//            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
//            [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
//                //设置按钮的样式
//                dispatch_source_cancel(_timer);
//                [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
//                sender.enabled = YES; // 设置按钮可点击
//
//                _isTimer = NO; // 倒计时状态为NO
//            } otherAction:^(int time) {
//                [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
//            }];
//
//        } else {
//            if (state.info.length>0) {
//                [XSTool showToastWithView:self.alertView Text:state.info];
//            }
//            sender.enabled = YES; // 设置按钮为可点击
//        }
//    } fail:^(NSError * _Nonnull error) {
//
//        [XSTool showToastWithView:self.alertView Text:Localized(@"验证码发送失败")];
//        sender.enabled = YES; // 设置按钮为可点击
//    }];
//}
//
//-(void)confirmBtnCileck:(NSString *)codeText psw:(NSString *)psw
//{
//    [self ActiveRequestWithCodeText:codeText pwd:psw];
//}

//创建充值订单
- (void)ActiveRequestWithPwd:(NSString *)pwd
{
    
    NSString *encryptPassWord = [RSAEncryptor encryptString:pwd];
    NSString *mobileStr =[self.mobileTF.text stringByReplacingOccurrencesOfString:@" "withString:@""];
//    NSLog(@"手机号:%@",mobileStr);
//
//    NSString *verify_mobile = [NSString stringWithFormat:@"%@",[UserInstance ShardInstnce].mobile];
//    NSString *area_code = [NSString stringWithFormat:@"%@",[UserInstance ShardInstnce].area_code];
//
//    if (isEmptyString(verify_mobile)) {
//        Alert(@"获取手机号失败");
//        return;
//    }
//    if (isEmptyString(area_code)) {
//        area_code = @"86";
//    }
    if (!self.selModel) {
        Alert(@"获取数据失败");
        return;
    }
    
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager TelCheckQueryWithParams:@{@"phoneno":mobileStr, @"cardnum":self.selModel.price} success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.resultModel = [RechargeResultModel mj_objectWithKeyValues:dic[@"data"][@"result"]];
        } else {
            
        }
    } fail:^(NSError *error) {
        
    }];
    
    //创建充值订单
    [HTTPManager TelCreatePhoneOrderWithParams:@{@"phoneno":mobileStr, @"cardid":@"", @"pay_pwd":encryptPassWord} success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:self.view];
         if (state.status) {
             Alert(state.info);
        }else{
             self.isEnable = NO;
             Alert(state.info);
             
         }
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:self.view];
         Alert(NetFailure);
     }];
}












//订单状态查询
- (void)CheckOrderStatus
{
//    if (self.resultModel.uorderid.length <=0) {
//        Alert(@"获取订单信息有误");
//        return;
//    }
//    __weak __typeof__(self) wSelf = self;
//    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager TelCheckOrderStatusWithOrderid:self.resultModel.uorderid
//                                       success:^(NSDictionary *dic, resultObject *state)
//     {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         if (state.status) {
//             if (state.info) {
//                 Alert(state.info);
//             }
//         }else{
//             Alert(state.info);
//
//         }
//     } fail:^(NSError *error) {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         Alert(NetFailure);
//     }];
}





//通讯录
- (void)rightBtnAction:(UIButton*)sender
{
    MobilePhoneContactVC *vc =[[MobilePhoneContactVC alloc] init];
    vc.mobileBlock = ^(NSString *mobileNum) {
        if (mobileNum.length == 11) {
//            mobileNum = [NSString stringWithFormat:@"%@ %@ %@",[mobileNum substringWithRange:NSMakeRange(0, 2)],[mobileNum substringWithRange:NSMakeRange(3, 6)],[mobileNum substringWithRange:NSMakeRange(7, 10)]];
        }
        self.mobileTF.text = mobileNum;
        [self getTelCheckRes];
    };
    [self.navigationController pushViewController:vc animated:YES];

}


- (UIButton *)creatChargeBtn
{
    CGFloat space = 18;
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(space,30, mainWidth - space*2, 50);
    [Btn setTitle:Localized(@"充值") forState:UIControlStateNormal];
    [Btn setBackgroundColor:mainColor];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 5;
    [Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(chargeBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    return Btn;
}
-(void)textFieldDidEditing:(UITextField *)textField{
    if (textField == self.mobileTF) {
        self.isEnable = NO;
        if (textField.text.length == 11) {
         [textField resignFirstResponder];
        }
//        if (textField.text.length >idex) {
//            if (textField.text.length == 4 || textField.text.length == 9 ) {//输入
//                NSMutableString * str = [[NSMutableString alloc ] initWithString:textField.text];
//                [str insertString:@" " atIndex:(textField.text.length-1)];
//                textField.text = str;
//            }if (textField.text.length >= 13 ) {//输入完成
//                textField.text = [textField.text substringToIndex:13];
//                [textField resignFirstResponder];
//
//            }
//            idex = textField.text.length;
//
//        }else if (textField.text.length < idex){//删除
//            if (textField.text.length == 4 || textField.text.length == 9) {
//                textField.text = [NSString stringWithFormat:@"%@",textField.text];
//                textField.text = [textField.text substringToIndex:(textField.text.length-1)];
//            }
//            idex = textField.text.length;
//        }
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}



-(void)selectBtnAction:(MobileRechargeView *)sender;
{
    [self.view endEditing:YES];
    if (!self.isEnable) {
        return;
    }
    NSLog(@"%ld  Clicked!!! ",sender.index);
    if (!_selView) {
        sender.isSelect = YES;
        _selView = sender;
    }else{
        _selView.isSelect = NO;
        sender.isSelect = YES;
        _selView = sender;
        
    }
//    NSDictionary *dic = self.listArr[sender.index];
//   NSString *title = [NSString stringWithFormat:@"%@",dic[@"title"]];
//    NSString *price = [NSString stringWithFormat:@"%@",dic[@"price"]];

}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.isEnable = NO;
    self.tipLb.hidden = YES;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length >0) {
        self.isEnable = YES;
        [self getTelCheckRes];

    }
  
    
}
-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self.onTimer invalidate];
    
}

- (void)appAreaClick:(NSInteger)index
{
    [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:@"暂未开放" message:nil CallBackBlock:^(NSInteger btnIndex) {
        
    } cancelButtonTitle:@"确定" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
}

@end
