//
//  MobilePhoneContactVC.m
//  App3.0
//
//  Created by xinshang on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobilePhoneContactVC.h"
#import <Contacts/Contacts.h>
#import "NSString+PinYin.h"
#import <MessageUI/MessageUI.h>
#import "MobilePhoneContactCell.h"

@interface MobilePhoneContactVC ()<UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    BOOL _checkFinished;
}

@property (strong, nonatomic) NSMutableArray *sortArray;
@property (strong, nonatomic) NSMutableArray *contactArr;   // 存放所有联系人
@property (strong, nonatomic) NSMutableArray *UpArr;   // 存放所有联系人(上传)
//@property (strong, nonatomic) NSMutableArray *tRegisterArr;      // 存放已注册的联系人(已邀请)
//@property (strong, nonatomic) NSMutableArray *others_register;      // 存放已注册的联系人(他人邀请)
@property (strong, nonatomic) NSMutableArray *indexs;
//@property (strong, nonatomic) UILabel *showLabel;
@property (nonatomic, assign) NSInteger already_num;
@property (nonatomic, assign) NSInteger tj_num;

@end

@implementation MobilePhoneContactVC


- (NSMutableArray *)sortArray {
    if (!_sortArray) {
        _sortArray = [NSMutableArray array];
    }
    return _sortArray;
}
- (NSMutableArray *)UpArr {
    if (!_UpArr) {
        _UpArr = [NSMutableArray array];
    }
    return _UpArr;
}
- (NSMutableArray *)contactArr {
    if (!_contactArr) {
        _contactArr = [NSMutableArray array];
    }
    return _contactArr;
}

//- (NSMutableArray *)tRegisterArr {
//    if (!_tRegisterArr) {
//        _tRegisterArr = [NSMutableArray array];
//    }
//    return _tRegisterArr;
//}
//- (NSMutableArray *)others_register {
//    if (!_others_register) {
//        _others_register = [NSMutableArray array];
//    }
//    return _others_register;
//}

- (NSMutableArray *)indexs {
    if (!_indexs) {
        _indexs = [NSMutableArray array];
    }
    return _indexs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self checkContact];
//    }];
    
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [self checkContact];
//    }];
//    [self.tableView.mj_header beginRefreshing];

}

- (void)setUpUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    _checkFinished = NO;
//
//    _showLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
//    _showLabel.backgroundColor = mainColor;
//    _showLabel.textAlignment = NSTextAlignmentCenter;
//    _showLabel.textColor = [UIColor whiteColor];
//    _showLabel.text = @"已邀请0人,还可邀请0人";
//    [self.view addSubview:_showLabel];
//
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.tabBarController.edgesForExtendedLayout = UIRectEdgeNone;//防止tableview错位
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"通讯录";    
    
}
-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-0) style:UITableViewStylePlain];
        self.tabBarController.edgesForExtendedLayout = UIRectEdgeNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
       _tableView.tableFooterView=[[UIView alloc]init];
        //    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        [self.view addSubview:_tableView];

        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
#ifdef __IPHONE_11_0
    if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }    }
    
#endif
        return _tableView;
    
}

- (void)checkContact
{
    [self.UpArr removeAllObjects];
    [self.contactArr removeAllObjects];
//    [self.tRegisterArr removeAllObjects];
    [self.sortArray removeAllObjects];
    [XSTool showToastWithView:self.view Text:@"获取中..."];
    __weak typeof(self) wSelf = self;
    // 判断授权状态
    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusNotDetermined) {
        
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                NSLog(@"授权成功");
                [wSelf loadDataWithStore:store];
            }
            else {
                NSLog(@"授权失败");
            }
        }];
    }
    else if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized) {
        //获取联系人仓库
        CNContactStore *store = [[CNContactStore alloc] init];
        
        [self loadDataWithStore:store];
    }
    
}

-(void)loadDataWithStore:(CNContactStore *)store
{
    
    //创建联系人信息的请求对象
    NSArray *keys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactOrganizationNameKey,CNContactImageDataKey];
    
    //根据请求key，创建请求对象
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
    
    //发送请求
    [store enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionary];
        // 获取姓名
        NSString *givenName = contact.givenName;
        NSString *familyName = contact.familyName;
        NSString *orgName = contact.organizationName;
        NSData *image = contact.imageData;
        NSLog(@"%@-%@",givenName,familyName);
        NSString *name = [NSString stringWithFormat:@"%@%@%@",familyName,givenName,orgName];
        [dic setValue:name forKey:@"name"];
        //获取电话
        NSMutableArray *phoneNumArr = [NSMutableArray array];
        NSArray *phoneArray = contact.phoneNumbers;
        for (CNLabeledValue *labelValue in phoneArray) {
            CNPhoneNumber *number = labelValue.value;
            NSLog(@"%@--%@",number.stringValue,labelValue.label);
            [phoneNumArr addObject:number.stringValue];
            
        }
        [dic setValue:phoneNumArr forKey:@"phone"];
        [self.UpArr addObject:dic];
        
        [dic1 setValue:name forKey:@"name"];
        [dic1 setValue:phoneNumArr forKey:@"phone"];
        [dic1 setValue:image forKey:@"image"];
//        [dic1 setValue:@"0" forKey:@"istRegister"];
//        [dic1 setValue:@"0" forKey:@"isOtherRegister"];
        [self.contactArr addObject:dic1];
        
        
    }];
    _checkFinished = YES;
    [self sortContactList:self.contactArr];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [XSTool hideProgressHUDWithView:self.view];

    });

    [self.tableView reloadData];

}

- (void)sortContactList:(NSArray *)contacts {
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSDictionary *dic in contacts) {
        [tempArr addObject:dic[@"name"]];
        
    }
    NSArray *resultArr = [tempArr arrayWithPinYinFirstLetterFormat];
    [self.sortArray removeAllObjects];

    // 对比字符串找出对应contactmodel来替代字符串
    for (int i = 0; i < resultArr.count; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:resultArr[i][@"firstLetter"] forKey:@"firstLetter"];
        NSMutableArray *arr = [NSMutableArray array];
        for (NSString *string in resultArr[i][@"content"]) {
            for (NSDictionary *dic in contacts) {
                if ([string isEqualToString:dic[@"name"]]) {
                    [arr addObject:dic];
                    break;
                }
            }
            [dic setObject:arr forKey:@"content"];
        }
        [self.sortArray addObject:dic];
    }
    
    [self.indexs removeAllObjects];
    for (NSDictionary *dic in resultArr) {
        [self.indexs addObject:dic[@"firstLetter"]];
    }
}

//- (void)attentionAcion:(UIButton *)sender
//{
//    UITableViewCell *cell = (UITableViewCell *)sender.superview;
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
//    DeviceFansDetailParser *parser = self.tRegisterArr[indexPath.row];
//    __weak typeof(self) wSelf = self;
//    [XSTool showProgressHUDTOView:self.view withText:nil];
//    [HTTPManager attentionFansWithUid:parser.uid success:^(NSDictionary * dic, resultObject *state) {
//        [XSTool showToastWithView:self.view Text:state.info];
//        if (state.status) {
//            [wSelf checkContact];
//        }
//        [XSTool hideProgressHUDWithView:self.view];
//    } fail:^(NSError * _Nonnull error) {
//        [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
//        [XSTool hideProgressHUDWithView:self.view];
//    }];
//}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sortArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *contentArr = [self.sortArray[section] objectForKey:@"content"];
    return contentArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //自定义Header标题
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = BG_COLOR;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor = mainGrayColor;
    titleLabel.font = [UIFont systemFontOfSize:12];
    
    NSString *title = self.sortArray[section][@"firstLetter"];
    titleLabel.text=title;
    [myView  addSubview:titleLabel];
    
    return myView;
}

//返回索引数组
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.indexs;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSInteger count = 0;
    for (NSString *character in self.indexs) {
        if ([[character uppercaseString] hasPrefix:title]) {
            return count;
        }
        count++;
    }
    return 0;
}

//返回每个索引的内容
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.indexs objectAtIndex:section];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    NSArray *contentArr = [self.sortArray[indexPath.section] objectForKey:@"content"];
//    NSDictionary *dic = contentArr[indexPath.row];
//    if (dic && [dic valueForKey:@"phone"]) {
//        [self sendMessage:@"紫京城" withPhoneNum:[dic valueForKey:@"phone"]];
//    } else {
//        [XSTool showToastWithView:self.view Text:@"暂不支持此功能"];
//    }
    NSArray *contentArr = [self.sortArray[indexPath.section] objectForKey:@"content"];
    NSDictionary *tDic = contentArr[indexPath.row];
    NSString *phoneString = @"";
    if (tDic && [tDic valueForKey:@"phone"]) {
        NSArray * phoneNum = [tDic valueForKey:@"phone"];
        phoneString = phoneNum[0];
    }
    if ([phoneString containsString:@"-"]) {
        phoneString=[phoneString stringByReplacingOccurrencesOfString:@"-"withString:@""];
    }
    if ([phoneString containsString:@"+86"] ||[phoneString containsString:@"+ 86"]) {
        phoneString=[phoneString stringByReplacingOccurrencesOfString:@"+86"withString:@""];
    }
    if ([phoneString containsString:@","]) {
        phoneString=[phoneString stringByReplacingOccurrencesOfString:@","withString:@""];
    }
    if ([phoneString containsString:@"("] && [phoneString containsString:@")"] ) {
        NSRange startRange = [phoneString rangeOfString:@"("];
        NSRange endRange = [phoneString rangeOfString:@")"];
        NSRange range = NSMakeRange(startRange.location + startRange.length, endRange.location - startRange.location - startRange.length);
        phoneString = [phoneString substringWithRange:range];
    }
    if ([phoneString containsString:@" "]) {
        phoneString=[phoneString stringByReplacingOccurrencesOfString:@" "withString:@""];
    }
    if ([phoneString containsString:@" "]) {
        phoneString=[phoneString stringByReplacingOccurrencesOfString:@" "withString:@""];
    }
    phoneString = [phoneString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (self.mobileBlock) {
        self.mobileBlock(phoneString);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *CellIdentifier = @"chatCell1";
    //    UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    //    if(cell == nil)
    //    {
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //
    //        NSArray *contentArr = [self.sortArray[indexPath.section] objectForKey:@"content"];
    //        NSDictionary *dic = contentArr[indexPath.row];
    //
    //        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, 100, 30)];
    //        name.text = [dic valueForKey:@"name"];
    //        name.font = [UIFont systemFontOfSize:16];
    //        [cell addSubview:name];
    //        [cell.imageView getImageWithUrlStr:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,[dic valueForKey:@"avatar"]] andDefaultImage:AvatarDefaultImage];
    //
    //
    //        //        UILabel *phone = [[UILabel alloc] initWithFrame:CGRectMake(130, 7, mainWidth-230, 30)];
    //        //        NSArray *phoneArr = [dic valueForKey:@"phone"];
    //        //        phone.text = phoneArr[0];
    //        //        phone.font = [UIFont systemFontOfSize:16];
    //        //        [cell addSubview:phone];
    //
    //        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    //        cell.detailTextLabel.text = @"短信邀请";
    //    } else {
    //        for (UIView *view in cell.subviews) {
    //            if ([view isKindOfClass:[UILabel class]]) {
    //                [view removeFromSuperview];
    //            }
    //        }
    //        NSArray *contentArr = [self.sortArray[indexPath.section] objectForKey:@"content"];
    //        NSDictionary *dic = contentArr[indexPath.row];
    //
    //        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, 100, 30)];
    //        name.text = [dic valueForKey:@"name"];
    //        name.font = [UIFont systemFontOfSize:16];
    //        [cell addSubview:name];
    //        [cell.imageView setImage:[UIImage imageWithData:[dic valueForKey:@"image"]]];
    ////        [cell.imageView getImageWithUrlStr:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,[dic valueForKey:@"avatar"]] andDefaultImage:AvatarDefaultImage];
    //
    //
    //        //        UILabel *phone = [[UILabel alloc] initWithFrame:CGRectMake(110, 7, mainWidth-230, 30)];
    //        //        NSArray *phoneArr = [dic valueForKey:@"phone"];
    //        //        phone.text = phoneArr[0];
    //        //        phone.font = [UIFont systemFontOfSize:16];
    //        //        [cell addSubview:phone];
    //    }
    static NSString *CellIdentifier = @"MobilePhoneContactCell";
    MobilePhoneContactCell *cell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[MobilePhoneContactCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
//    cell.showBtn.indexPath = indexPath;
//    [cell.showBtn addTarget:self action:@selector(showBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSArray *contentArr = [self.sortArray[indexPath.section] objectForKey:@"content"];
    NSDictionary *dic = contentArr[indexPath.row];
    cell.dic = dic;
    
    return cell;
}

//-(void)sendMessage:(NSString *)message withPhoneNum:(NSArray *)phoneNum
//{
//    if ([MFMessageComposeViewController canSendText]) {
//        //实例化MFMessageComposeViewController,并设置委托
//        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
//        
//        messageController.delegate = self;
//        messageController.messageComposeDelegate = self;
//        
//        //设置短信内容
//        messageController.body = message;
//        
//        //设置发送给谁
//        NSString *phoneString = phoneNum[0];
//        messageController.recipients = @[phoneString];
//        NSLog(@"%@----%@",messageController.body,messageController.recipients);
//        //推到发送试图控制器
//        [self presentViewController:messageController animated:YES completion:^{
//            
//        }];
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
//                                                        message:@"该设备不支持短信功能"
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"确定", nil];
//        [alert show];
//    }
//    
//    
//}
//
//
////发送短信后回调的方法
//-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
//{
//    NSString *tipContent;
//    
//    switch (result) {
//        case MessageComposeResultCancelled:
//            tipContent = @"发送短信已取消";
//            break;
//            
//        case MessageComposeResultFailed:
//            tipContent = @"发送短信失败";
//            break;
//            
//        case MessageComposeResultSent:
//            tipContent = @"发送成功";
//            break;
//            
//        default:
//            break;
//    }
//    
//    UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:@"提示" message:tipContent delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
//    [alterView show];
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//


//- (void)showBtnClick:(HomeCustomBtn*)sender
//{
//    NSIndexPath *indexPath = sender.indexPath;
//

//    __weak __typeof__(self) wSelf = self;
//    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager mobileRegisterWithMobile:phoneString
//                                    intro:[UserInstance ShardInstnce].userName
//                                  Success:^(NSDictionary *dic, resultObject *state)
//     {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         if ([dic[@"status"] integerValue]==StatusTypeSuccess) {
//             Alert(@"邀请成功");
//             [tDic setValue:@"1" forKey:@"istRegister"];
//
////             [sender setTitle:@"已邀请" forState:UIControlStateNormal];
////             [sender setTitleColor:mainColor forState:UIControlStateNormal];
////             sender.enabled = NO;
////             sender.backgroundColor = [UIColor whiteColor];
//             self.already_num = self.already_num+1;
//             self.tj_num = self.tj_num-1;
//             _showLabel.text = [NSString stringWithFormat:@"已邀请%ld人,还可邀请%ld人",(long)self.already_num,self.tj_num];
//
//             [wSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//
//         }else{
//             if (dic[@"info"]) {
//                 Alert(dic[@"info"]);
//             }
//         }
//
//     } fail:^(NSError *error) {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         Alert(NetFailure);
//     }];

//}


- (void)tableViewEndRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
@end
