//
//  MobilePhoneContactVC.h
//  App3.0
//
//  Created by xinshang on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface MobilePhoneContactVC : XSBaseViewController
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,copy) void(^mobileBlock)(NSString *mobileNum);

@end
