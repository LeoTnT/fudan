//
//  RechagreListVC.m
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RechagreListVC.h"
#import "RechagreListCell.h"
#import "HTTPManager+ThirdParty.h"

@interface RechagreListVC ()
@property (nonatomic, strong) UILabel *headView;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) DefaultView *defaultView;//默认界面

@end

@implementation RechagreListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"充值记录";
    
    [self SetUpUI];
}


- (void)SetUpUI
{
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height);
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 1;
        [wSelf queryOrder];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf queryOrder];
    }];
    [self.tableView.mj_header beginRefreshing];
    [self.view addSubview:self.defaultView];
    self.defaultView.hidden = YES;

}

//充值记录查询
- (void)queryOrder
{
    if (self.page == 1) {
        [self.listArr removeAllObjects];
    }
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [self.tableView.mj_footer resetNoMoreData];
    [HTTPManager TelQueryOrderWithParams:@{@"page":@(self.page)}
                                success:^(NSDictionary *dic, resultObject *state)
     {
         [self tableViewEndRefreshing];
         if (state.status) {
             NSArray *array = dic[@"data"][@"data"];
             NSString *hasmore = [NSString stringWithFormat:@"%@",dic[@"data"][@"has_more"]];
             if (hasmore.intValue == 0&&self.page >1) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
                 [wSelf.tableView reloadData];
                 return ;
             }
             
             if (array.count >0) {
                 [self.listArr addObjectsFromArray:[RechargeListModel mj_objectArrayWithKeyValuesArray:array]];
             }else{
                 if (self.page >1) {
                     [self.tableView.mj_footer endRefreshingWithNoMoreData];
                 }
             }
             
             if (self.listArr.count >0) {
                 [self hideDefaultView];
             }else {
                 [self showDefaultView];
             }
         }else{
             Alert(state.info);
             [self showDefaultView];
         }
         [wSelf.tableView reloadData];
     } fail:^(NSError *error) {
         [self tableViewEndRefreshing];
         self.defaultView.hidden = NO;
         self.tableView.hidden = YES;
         Alert(NetFailure);
     }];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RechagreListCell";
    RechagreListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RechagreListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
    }

    if (self.listArr) {
        cell.model = self.listArr[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 14;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    _headView = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, mainWidth-10, 40)];
//    _headView.font = [UIFont systemFontOfSize:15];
//    _headView.backgroundColor = [UIColor groupTableViewBackgroundColor];
//    
//    if (section == 0) {
//        _headView.text = @"  本月";
//    }else{
//        _headView.text = @"  9月";
//    }
//    return _headView;
//    
//}


-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(NSMutableArray *)listArr{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
    
}
- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - lazy loadding
- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = Localized(@"暂无更多记录");
        
    }
    return _defaultView;
}

- (void)showDefaultView
{
    self.defaultView.hidden = NO;
    self.tableView.hidden = YES;
}
- (void)hideDefaultView
{
    self.defaultView.hidden = YES ;
    if (self.tableView.hidden) {
        self.tableView.hidden = NO;
    }
}
@end
