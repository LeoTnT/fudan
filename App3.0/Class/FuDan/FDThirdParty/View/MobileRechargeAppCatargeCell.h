//
//  MobileRechargeAppCatargeCell.h
//  App3.0
//
//  Created by xinshang on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MobileRechargeAppCatargeCellDelegate <NSObject>
- (void)appAreaClick:(NSInteger)index;
@end

@interface MobileRechargeAppCatargeCell : UITableViewCell
@property(nonatomic, weak) id<MobileRechargeAppCatargeCellDelegate>delegate;
@end
