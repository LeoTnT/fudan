//
//  MobileRechargeTopCell.h
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MobileRechargeTopCell : UITableViewCell

@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) UITextField *mobileTF;
@property (nonatomic, strong) UILabel *tipLb;

@end
