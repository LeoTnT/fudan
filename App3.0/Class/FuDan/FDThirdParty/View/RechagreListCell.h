//
//  RechagreListCell.h
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RechargeModel.h"

@interface RechagreListCell : UITableViewCell
@property (nonatomic, strong) UIImageView *titleImg;
@property (nonatomic, strong) RechargeListModel *model;

@end
