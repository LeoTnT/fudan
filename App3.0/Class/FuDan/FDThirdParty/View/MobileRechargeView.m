//
//  MobileRechargeView.m
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobileRechargeView.h"

@interface MobileRechargeView()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;

@end

#define Space 10.0
#define BtnWidth  (mainWidth-Space*4)/3
#define BtnHeight BtnWidth *0.6

@implementation MobileRechargeView

-(instancetype)initWithFrame:(CGRect)frame Title1:(NSString *)title1 title2:(NSString *)title2 enable:(BOOL)isEnable
{
    self = [super initWithFrame:frame];
    if (self) {
        //        self.frame = CGRectMake(0, 0, BtnWidth, BtnHeight);
        [self setUpUIWithTitle1:title1 title2:title2 enable:(BOOL)isEnable];
    }
    return self;
}

- (void)setUpUIWithTitle1:(NSString *)title1 title2:(NSString *)title2 enable:(BOOL)isEnable
{
    _bgView = [[UIView alloc] init];
    _bgView.layer.masksToBounds = YES;
    _bgView.layer.borderColor = mainColor.CGColor;
    _bgView.layer.borderWidth = 1;
    _bgView.layer.cornerRadius = 3;
    [self addSubview:_bgView];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
        make.width.mas_equalTo(BtnWidth);
        make.height.mas_equalTo(BtnHeight);
    }];
    
    
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button addTarget:self action:@selector(vipBtnSelect) forControlEvents:UIControlEventTouchUpInside ];
    [_bgView addSubview:_button];
    
    [_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(_bgView);
        make.width.mas_equalTo(BtnWidth);
        make.height.mas_equalTo(BtnHeight);
    }];
    
    
    
    CGFloat lbHeight = 25;
    //_label1
    _label1 = [[UILabel alloc] init];
    _label1.text = [NSString stringWithFormat:@"%@",title1];
    _label1.textColor = mainColor;
    _label1.font = [UIFont systemFontOfSize:17];
    _label1.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_label1];
    
    [_label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_bgView);
        make.centerY.mas_equalTo(_bgView.mas_centerY).offset(-13);
        make.width.mas_equalTo(BtnWidth);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    //_label2
    _label2 = [[UILabel alloc] init];
    _label2.text = title2;
    _label2.textColor = mainColor;
    _label2.font = [UIFont systemFontOfSize:12];
    if (mainWidth <=320) {
        _label2.font = [UIFont systemFontOfSize:9];
    }
    _label2.textAlignment = NSTextAlignmentCenter;
    [_bgView addSubview:_label2];
    [_label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_bgView);
        make.centerY.mas_equalTo(_bgView.mas_centerY).offset(10);
        make.width.mas_equalTo(BtnWidth);
        make.height.mas_equalTo(lbHeight);
    }];
    
    if (!isEnable) {
        _label1.textColor = [UIColor grayColor];
        _label2.textColor = [UIColor grayColor];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.borderColor = [UIColor grayColor].CGColor;

    }
    
}
- (void)vipBtnSelect
{
    if ([self.delegate respondsToSelector:@selector(selectBtnAction:)]) {
        [self.delegate selectBtnAction:self];
    }
}
-(void)setIsSelect:(BOOL)isSelect
{
    _isSelect = isSelect;
    if (isSelect) {
        _label1.textColor = [UIColor whiteColor];
        _label2.textColor = [UIColor whiteColor];
        _bgView.backgroundColor = mainColor;
    }else{
        _label1.textColor = mainColor;
        _label2.textColor = mainColor;
        _bgView.backgroundColor = [UIColor whiteColor];
    }
}

@end

