//
//  MobileRechargeAppCatargeCell.m
//  App3.0
//
//  Created by xinshang on 2017/12/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobileRechargeAppCatargeCell.h"
#import "AreaButton.h"

@interface MobileRechargeAppCatargeCell()
{
    __weak id<MobileRechargeAppCatargeCellDelegate>delegate;
}
@end

@implementation MobileRechargeAppCatargeCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat width = mainWidth/4;
        CGFloat height = 90;
        
        NSArray *arr = @[@{@"image":@"F1_01",@"title":@"电影票"},@{@"image":@"F1_02",@"title":@"生活续费"},@{@"image":@"F1_03",@"title":@"冲流量"},@{@"image":@"F1_04",@"title":@"水电费"},@{@"image":@"F1_05",@"title":@"加油卡"}];
        
        
        for (int i = 0; i < arr.count; i++) {
            AreaButton *btn = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%4), (int)(i/4)*height, width, height) Model:arr[i] Scale:1.0/3.0 fontSize:14];
            btn.index = i;
            [btn addTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
//            if (i%4 == 0 && i != 0) {
//                UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, height*(int)(i/4), mainWidth, 0.5)];
//                hLine.backgroundColor = LINE_COLOR;
//                [self addSubview:hLine];
//            }
        }
        
    }
    return self;
}

- (void)areaAction:(AreaButton *)pSender
{
    if ([delegate respondsToSelector:@selector(appAreaClick:)]) {
        [delegate appAreaClick:pSender.index];
    }
}
@end

