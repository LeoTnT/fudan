//
//  MobileRechargeTopCell.m
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobileRechargeTopCell.h"


@interface MobileRechargeTopCell()


@end

#define space 10
@implementation MobileRechargeTopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
        
    }
    return self;
}


-(void)setUpUI{
    
    CGFloat labelH = 25.0;
    
    _mobileTF = [[UITextField alloc] init];
    _mobileTF.font = [UIFont systemFontOfSize:22];
    [self addSubview:_mobileTF];
    
    [_mobileTF mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space*2);
        make.width.mas_equalTo(mainWidth-80);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_tipLb
    _tipLb = [self getLabelWithTextColor:[UIColor grayColor] Font:[UIFont systemFontOfSize:12]];
    [self addSubview:_tipLb];
    
    [_tipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(_mobileTF.mas_bottom);
        make.width.mas_equalTo(mainWidth-80);
        make.height.mas_equalTo(20);
    }];
    
  
    
    //_rightBtn
    _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:_rightBtn];
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(space);
        make.width.mas_equalTo(labelH*2);
        make.height.mas_equalTo(labelH*2);
    }];
   
    
    //line
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(_tipLb.mas_bottom).offset(space/2);
        make.width.mas_equalTo(mainWidth);
        make.height.mas_equalTo(0.55);
    }];
    
    //titleLb
    UILabel *titleLb = [self getLabelWithTextColor:[UIColor blackColor] Font:[UIFont systemFontOfSize:16]];
    [self addSubview:titleLb];
    
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(line.mas_bottom).offset(space*2);
        make.width.mas_equalTo(mainWidth-80);
        make.height.mas_equalTo(labelH);
    }];
    
    
    
    
    _tipLb.text = Localized(@"默认号码(山东联通)");
    _mobileTF.placeholder = Localized(@"请输入手机号码");
    [_mobileTF setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];

    titleLb.text = Localized(@"充话费");
    [_rightBtn setImage:[UIImage imageNamed:@"A6_user"] forState:UIControlStateNormal];
}



- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    return label;
}

-(BOOL) pushSignIn:(NSString *)str{
    /**
     * 移动号段正则表达式
     */
    NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
    /**
     * 联通号段正则表达式
     */
    NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
    /**
     * 电信号段正则表达式
     */
    NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
    BOOL isMatch1 = [pred1 evaluateWithObject:str];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
    BOOL isMatch2 = [pred2 evaluateWithObject:str];
    NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
    BOOL isMatch3 = [pred3 evaluateWithObject:str];
    
    if (isMatch1 || isMatch2 || isMatch3) {
        return YES;
    }else{
        return NO;
    }
}


@end
