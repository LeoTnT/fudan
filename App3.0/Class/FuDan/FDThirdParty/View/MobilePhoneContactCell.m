//
//  MobilePhoneContactCell.m
//  App3.0
//
//  Created by xinshang on 2017/8/8.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "MobilePhoneContactCell.h"

@interface MobilePhoneContactCell()
@property (nonatomic,strong) UIImageView *userImg;
@property (nonatomic,strong) UILabel *nameLb;

@end

@implementation MobilePhoneContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    CGFloat space = 10.0;
    CGFloat width = (mainWidth-60)/2;
    CGFloat height = 30;
    
    CGFloat imgWidth = 35;
    
    //_userImg
    _userImg = [[UIImageView alloc]init];
    _userImg.image = avatarDefaultImg;
    _userImg.layer.masksToBounds = YES;
    _userImg.layer.cornerRadius = imgWidth/2;
    [self addSubview:_userImg];
    [_userImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7.5);
        make.left.mas_equalTo(space);
        make.width.mas_equalTo(imgWidth);
        make.height.mas_equalTo(imgWidth);
    }];
    
    
    //_nameLb
    _nameLb = [[UILabel alloc] init];
    _nameLb.font = [UIFont systemFontOfSize:16];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(space);
        make.left.mas_equalTo(_userImg.mas_right).offset(space/3);
        make.width.mas_equalTo(mainWidth-70);
        make.height.mas_equalTo(height);
    }];
    
//    //_showLb
//    _showLb = [[UILabel alloc] init];
//    _showLb.font = [UIFont systemFontOfSize:16];
//    _showLb.textAlignment = NSTextAlignmentCenter;
//    [self addSubview:_showLb];
//    [_showLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(space);
//        make.right.mas_equalTo(-space*1.8);
//        make.width.mas_equalTo(65);
//        make.height.mas_equalTo(height);
//    }];
//
    
//    //_showBtn
//    _showBtn = [HomeCustomBtn buttonWithType:UIButtonTypeCustom];
//    _showBtn.titleLabel.font = [UIFont systemFontOfSize:15];
//    _showBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    _showBtn.enabled = NO;
//    [self addSubview:_showBtn];
//    [_showBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(space);
//        make.right.mas_equalTo(-space*1.8);
//        make.width.mas_equalTo(75);
//        make.height.mas_equalTo(height);
//    }];
}

-(void)setDic:(NSDictionary *)dic
{
    _dic = dic;
    if ([dic valueForKey:@"image"]) {
        [_userImg setImage:[UIImage imageWithData:[dic valueForKey:@"image"]]];
        
    }else{
        [_userImg setImage:avatarDefaultImg];
    }
    if ([dic valueForKey:@"name"]) {
        _nameLb.text = [dic valueForKey:@"name"];
    }
    
    
    
    NSString *istRegister = [dic valueForKey:@"istRegister"];
    NSString *isOtherRegister = [dic valueForKey:@"isOtherRegister"];
//    if (istRegister.intValue >0) {
////        _showBtn.titleLabel.text = @"已邀请";
//        [_showBtn setTitle:@"已邀请" forState:UIControlStateNormal];
//        [_showBtn setTitleColor:mainColor forState:UIControlStateNormal];
//        _showBtn.enabled = NO;
//        _showBtn.backgroundColor = [UIColor whiteColor];
//    }else if (isOtherRegister.intValue >0)
//    {
//        _showBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//        [_showBtn setTitle:@"已被他人邀请" forState:UIControlStateNormal];
//        [_showBtn setTitleColor:mainColor forState:UIControlStateNormal];
//        _showBtn.backgroundColor = [UIColor whiteColor];
//
//        _showBtn.enabled = NO;
//    }else{
//        [_showBtn setTitle:@"邀请" forState:UIControlStateNormal];
//        [_showBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _showBtn.backgroundColor = mainColor;
//        _showBtn.layer.masksToBounds = YES;
//        _showBtn.layer.cornerRadius =3;
//        _showBtn.enabled = YES;
//    }
}



@end
