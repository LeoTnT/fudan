//
//  RechagreListCell.m
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RechagreListCell.h"
#import "XSFormatterDate.h"
@interface RechagreListCell()


@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UILabel *detailLb;
@property (nonatomic, strong) UILabel *priceLb;





@end

#define space 10
@implementation RechagreListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
        
    }
    return self;
}


-(void)setUpUI{
    
    CGFloat labelH = 20.0;
    CGFloat ImgH = 44.0;
    CGFloat leftImgH = 18.0;
    
    //_titleImg
    _titleImg = [[UIImageView alloc]init];
    [self addSubview:_titleImg];
    [_titleImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(space);
        make.top.mas_equalTo(space*1.2);
        make.width.mas_equalTo(ImgH);
        make.height.mas_equalTo(ImgH);
    }];
    
    
    //_titleLb
    _titleLb = [self getLabelWithTextColor:[UIColor blackColor] Font:[UIFont systemFontOfSize:15.5]];
    [self addSubview:_titleLb];
    
    [_titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_titleImg.mas_right).offset(space/2);
        make.top.mas_equalTo(_titleImg).offset(space/3);
        make.width.mas_equalTo(mainWidth-ImgH-65);
        make.height.mas_equalTo(labelH);
    }];
    
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:[UIColor blackColor] Font:[UIFont boldSystemFontOfSize:20]];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_priceLb];
    
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_titleLb);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(labelH);
    }];
    
    //_detailLb
    _detailLb = [self getLabelWithTextColor:[UIColor grayColor] Font:[UIFont systemFontOfSize:13]];
    [self addSubview:_detailLb];
    
    [_detailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_titleLb);
        make.top.mas_equalTo(_titleLb.mas_bottom).offset(space/2.5);
        make.width.mas_equalTo(mainWidth-ImgH*3);
        make.height.mas_equalTo(labelH);
    }];
    
   
    
    _titleLb.text = @"";
    _detailLb.text = @"";
    _priceLb.text = @"";
    _titleImg.image = [UIImage imageNamed:@"A6_liantong"];

    
    //    NSMutableAttributedString *hintString = [self setStringWithStr:@"300GBP" RangStr:@"GBP"];
    //    _priceLb.attributedText=hintString;
    //    _titleImg.image = [UIImage imageNamed:@"A1_bg1"];
    
}
-(void)setModel:(RechargeListModel *)model
{
    _model = model;
   _titleLb.text =  [NSString stringWithFormat:@"%@%@CNY--%@",Localized(@"充值"),model.money,model.phoneno];
    _detailLb.text = [XSFormatterDate dateWithTimeIntervalString:model.w_time];//w_time
    _priceLb.text = [NSString stringWithFormat:@"%@",model.r_money];
    
    
    if (model.toperator.intValue ==  0) {//移动
        self.titleImg.image = [UIImage imageNamed:@"A6_yidong"];
    }else if (model.toperator.intValue ==  1) {//联通
        self.titleImg.image = [UIImage imageNamed:@"A6_liantong"];
    }else if (model.toperator.intValue ==  2) {//电信
        self.titleImg.image = [UIImage imageNamed:@"A6_dianxin"];
    }else {//未知
        self.titleImg.image = [UIImage imageNamed:@"A6_liantong"];
    }
}



- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *hintString=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[hintString string]rangeOfString:rangStr];
    [hintString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:255/255 green:79/255 blue:79/255 alpha:1] range:range];
    [hintString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
    return hintString;
    
}


- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    return label;
}

@end


