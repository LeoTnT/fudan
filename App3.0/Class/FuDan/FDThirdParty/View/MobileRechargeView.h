//
//  MobileRechargeView.h
//  App3.0
//
//  Created by xinshang on 2017/10/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MobileRechargeView;
@protocol MobileRechargeViewDelegate <NSObject>
-(void)selectBtnAction:(MobileRechargeView *)sender;
@end



@interface MobileRechargeView : UIView

@property (nonatomic,assign) NSInteger index;
@property (nonatomic,assign) BOOL isSelect;
@property(nonatomic,weak)id<MobileRechargeViewDelegate> delegate;
-(instancetype)initWithFrame:(CGRect)frame Title1:(NSString *)title1 title2:(NSString *)title2  enable:(BOOL)isEnable;


@end
