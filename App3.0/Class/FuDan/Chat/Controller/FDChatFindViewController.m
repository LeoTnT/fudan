//
//  FDChatFindViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChatFindViewController.h"
#import "SearchVC.h"

@interface FDChatFindViewController ()
@property(nonatomic, strong)UIButton *searchBtn;
@end

@implementation FDChatFindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.searchBtn];
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(48, 0, 0, 0));
    }];
    
    self.showRefreshHeader = YES;
    @weakify(self);
    [self setRefreshHeader:^{
        @strongify(self);
        
        [self.tableView.mj_header endRefreshing];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchAction {
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleLine];
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [UIImage imageNamed:@"chat_find_circle"];
    cell.textLabel.text = @"朋友圈";
    cell.textLabel.textColor = Color(@"111111");
    cell.textLabel.font = SYSTEM_FONT(16);
    cell.detailTextLabel.text = @"可查看朋友圈";
    cell.detailTextLabel.textColor = COLOR_999999;
    cell.detailTextLabel.font = SYSTEM_FONT(12);
    return cell;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 8, mainWidth-24, 32)];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitle:Localized(@"搜索联系人/群组") forState:UIControlStateNormal];
        [_searchBtn setTitleColor:COLOR_999999 forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_searchBtn setImage:[UIImage imageNamed:@"chat_search"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
        _searchBtn.layer.masksToBounds = YES;
        _searchBtn.layer.cornerRadius = 5;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -2.5, 0, 0)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2.5, 0, 0)];
    }
    return _searchBtn;
}
@end
