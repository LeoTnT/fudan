//
//  FYChatViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDChatViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "TabChatVC.h"
#import "FDChatFindViewController.h"
#import "FriendsVC.h"
#import "AddFriendsViewController.h"

@interface FDChatViewController () <SGPageTitleViewDelegate, SGPageContentViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;
@property (nonatomic, strong) NSArray *titleArr;
@end

@implementation FDChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"聊天";
    self.navLeftBtn.hidden = YES;
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubviews {
    _titleArr = @[@"消息", @"发现", @"通讯录", @"邀请"];
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = COLOR_666666;
    configure.titleSelectedColor = mainColor;
    configure.titleFont = SYSTEM_FONT(14);
    configure.indicatorColor = mainColor;
//    configure.indicatorCornerRadius = 2;
//    configure.indicatorToBottomDistance = 5;
//    configure.indicatorDynamicWidth = 18;
    configure.indicatorStyle = SGIndicatorStyleDefault;
    configure.bottomSeparatorColor = LINE_COLOR;
    
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44) delegate:self titleNames:_titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_pageTitleView];
    
    NSMutableArray * childArr = [NSMutableArray array];
    [_titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            TabChatVC *oneVC = [[TabChatVC alloc] init];
            [childArr addObject:oneVC];
        } else if (idx == 1) {
            FDChatFindViewController *oneVC = [[FDChatFindViewController alloc] init];
            [childArr addObject:oneVC];
        } else if (idx == 2) {
            FriendsVC *oneVC = [[FriendsVC alloc] init];
            [childArr addObject:oneVC];
        } else {
            AddFriendsViewController *oneVC = [[AddFriendsViewController alloc] init];
            [childArr addObject:oneVC];
        }
        
        
    }];
    
    CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame) - kTabbarHeight - kStatusBarAndNavigationBarHeight;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)setContentPage:(NSInteger)page {
    [self.pageContentView setPageContentViewCurrentIndex:page];
}
@end
