//
//  FDMediaVideoController.m
//  App3.0
//
//  Created by lichao on 2018/8/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMediaVideoController.h"
#import "FDVideoController.h"
#import "FDWaterFallLayout.h"
#import "FDVideoModel.h"
#import "FDHomeModel.h"
#import "FDVideoCell.h"
#import "FDPostingController.h"

@interface FDMediaVideoController ()<UITextFieldDelegate, UICollectionViewDataSource, FDWaterFallLayoutDeleaget>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, weak) UIView *backV;
@property (nonatomic, weak) UITextField *textField;
@property (nonatomic, weak) UIImageView *searchImageV;

@property (nonatomic, assign) NSInteger page;

@end

@implementation FDMediaVideoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
    [self.collectionView.mj_header beginRefreshing];
}

/* 加载新数据 */
- (void)loadData{
    self.page = 1;
    [self loadMoreData];
}

/* 加载更多 */
- (void)loadMoreData{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"is_my"] = @"0";//是否自己发布的(1是)
    params[@"page"] = @(self.page);
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetVideoLists parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArr = [FDVideoListModel mj_objectArrayWithKeyValuesArray:state.data[@"data"]];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
                weakSelf.dataArr = [NSMutableArray arrayWithArray:tempArr];
            }else {
                [weakSelf.dataArr addObjectsFromArray:tempArr];
            }
            if (tempArr.count > 0) {
                weakSelf.page ++;
            }

            [weakSelf.collectionView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)setUpUI{
    
    self.title = Localized(@"媒体视频");
    self.view.backgroundColor = [UIColor whiteColor];
    kFDWeakSelf;
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"发布") action:^{
        FDPostingController *postVC = [[FDPostingController alloc] init];
        [weakSelf.navigationController pushViewController:postVC animated:YES];
    }];
    
    //注册cell
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([FDVideoCell class]) bundle:nil] forCellWithReuseIdentifier:[FDVideoCell identifier]];
    //顶部搜索输入框
    [self setSearchNavView];
}

/************ 搜索界面 ************/
- (void)setSearchNavView {
    
    UIView *backV = [[UIView alloc] init];
    backV.frame = CGRectMake(6, 10, mainWidth-6*2, 39);
    backV.backgroundColor = [UIColor hexFloatColor:@"EFEFEF"];
    backV.layer.cornerRadius = backV.frame.size.height/2;
    backV.layer.masksToBounds = YES;
    self.backV = backV;
    [self.view addSubview:backV];
    
    //搜索输入框
    UITextField *textField = [[UITextField alloc] init];
    textField.font = [UIFont systemFontOfSize:13];
    textField.textColor = [UIColor hexFloatColor:@"999999"];
    textField.backgroundColor = [UIColor hexFloatColor:@"EFEFEF"];
    textField.placeholder = Localized(@"大家都在搜：星巴克网红咖啡");
    textField.textAlignment = NSTextAlignmentCenter;
    textField.delegate = self;
    textField.returnKeyType = UIReturnKeySearch;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [textField addTarget:self action:@selector(textFieldDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
        [textField addTarget:self action:@selector(textFieldDidEnd:) forControlEvents:UIControlEventEditingDidEnd];

    self.textField = textField;
    [backV addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(backV);
    }];
    
    //图片
    UIImageView *searchImageV = [[UIImageView alloc] init];
    searchImageV.image = [UIImage imageNamed:@"fd_search"];
    self.searchImageV = searchImageV;
    [backV addSubview:searchImageV];
    [searchImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backV);
        make.right.mas_equalTo(textField.mas_left).offset(-4);
        make.size.mas_equalTo(CGSizeMake(13, 13));
    }];
}

//点击空白处, 隐藏键盘
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.textField resignFirstResponder];
}

#pragma mark - <UITextFieldDelegate>
- (void)textFieldDidBegin:(UITextField*)textField {
    textField.textAlignment = NSTextAlignmentLeft;
    [textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.backV);
        make.left.mas_equalTo(self.backV).offset(20+13+4*2);
        make.right.mas_equalTo(self.backV).offset(-10);
    }];
}

- (void)textFieldDidEnd:(UITextField*)textField {
//    textField.textAlignment = NSTextAlignmentCenter;
//    [textField mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.centerY.mas_equalTo(self.backV);
//    }];
}

- (void)textFieldDidChange:(UITextField*)textField {
 
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    //隐藏键盘
    [textField resignFirstResponder];
    //进行搜索
    [self.collectionView.mj_header beginRefreshing];
    
    return YES;
}

#pragma mark  - <FDWaterFallLayoutDeleaget>
- (CGFloat)waterFallLayout:(FDWaterFallLayout *)waterFallLayout heightForItemAtIndexPath:(NSUInteger)indexPath itemWidth:(CGFloat)itemWidth{
    
    FDVideoListModel *model = self.dataArr[indexPath];
    CGFloat height = 0.0;
    if ([model.img_width integerValue] > 0 && [model.img_height integerValue] > 0) {
        height = (itemWidth / [model.img_width integerValue]) * [model.img_height integerValue];
    }else {
        height = itemWidth;//默认高度
    }
    return height+87;//视频的高度+标题高度+姓名高度 以及间距
}

- (CGFloat)rowMarginInWaterFallLayout:(FDWaterFallLayout *)waterFallLayout{
    
    return 15;
}

- (NSUInteger)columnCountInWaterFallLayout:(FDWaterFallLayout *)waterFallLayout{
    
    return 2;
}

#pragma mark UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    self.collectionView.mj_footer.hidden = self.dataArr.count == 0;
    return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FDVideoCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDVideoCell identifier] forIndexPath:indexPath];
   
    FDVideoListModel *model = self.dataArr[indexPath.item];
    cell.model = model;
    
    //点击cell的播放按钮
    kFDWeakSelf;
    cell.playBlock = ^{
        FDVideoController *detailVC = [[FDVideoController alloc] init];
        detailVC.videoModel = model;
        [weakSelf.navigationController pushViewController:detailVC animated:YES];
    };
    return cell;
}

- (void)endRefreshing {
    
    [self.collectionView.mj_footer endRefreshing];
    [self.collectionView.mj_header endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - 懒加载
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        // 创建布局
        FDWaterFallLayout * waterFallLayout = [[FDWaterFallLayout alloc]init];
        waterFallLayout.delegate = self;
        
        CGRect frame = self.view.bounds;
        frame.origin.y += 39+10;
        frame.size.height -= 39+10;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:frame collectionViewLayout:waterFallLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        
        kFDWeakSelf;
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf loadData];
        }];
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

@end
