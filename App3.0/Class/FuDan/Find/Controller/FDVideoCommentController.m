//
//  FDVideoCommentController.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoCommentController.h"
#import "FDVideoCommentCell.h"
#import "FDVideoSubCommentCell.h"
#import "FDFindModel.h"

@interface FDVideoCommentController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, assign) NSInteger page;
/** video视频=YES | replay回复=NO */
@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, weak) UITextField *textField;
//回复当前section
@property (nonatomic, strong) NSIndexPath *currentIndexPath;

@end

@implementation FDVideoCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.isVideo = YES;//默认回复视频
    
    [self setUpUI];
}

- (void)setVideoModel:(FDVideoListModel *)videoModel {
    _videoModel = videoModel;
    if (videoModel) {
        [self loadData];
    }
}

#pragma mark ===== 网络请求
- (void)loadData {
    self.page = 1;
    [self loadMoreData];
}
- (void)loadMoreData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"vid"] = self.videoModel.videoID;//视频id
    params[@"page"] = @(self.page);

    kFDWeakSelf;
    [XSHTTPManager post:FD_GetVideoReplayList parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            
            NSArray *tempArr = [NSMutableArray arrayWithArray:[FDVideoCommentModel mj_objectArrayWithKeyValuesArray:state.data[@"data"]]];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
                weakSelf.dataArr = [NSMutableArray arrayWithArray:tempArr];
            }else {
                [weakSelf.dataArr addObjectsFromArray:tempArr];
            }
            
            if (tempArr.count > 0) {
                weakSelf.page ++;
                if (tempArr.count < 10) {
                    weakSelf.tableView.mj_footer.hidden = YES;
                }else {
                    weakSelf.tableView.mj_footer.hidden = NO;
                }
            }else {
                weakSelf.tableView.mj_footer.hidden = YES;
            }
            
            [weakSelf.tableView reloadData];
            
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        
        [weakSelf endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//点赞接口
- (void)niceRequestWithRid:(NSString *)rid {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"vid"] = self.videoModel.videoID;//视频id
    params[@"rid"] = rid;//回复id
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_ThumbUpUrl parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            //点赞成功 更新数据
            [weakSelf loadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//评论接口
- (void)commentRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"type"] = self.isVideo ? @"video" : @"replay";//video视频|replay回复
    params[@"vid"] = self.videoModel.videoID;//视频ID

    if (!self.isVideo) {
        FDVideoCommentModel *model = self.dataArr[self.currentIndexPath.section];
        params[@"rid"] = model.ID;//回复ID
    }
    params[@"content"] = self.textField.text;//回复内容

    [XSTool showProgressHUDWithView:self.view];
    kFDWeakSelf;
    [XSHTTPManager post:FD_VideoReplayUrl parameters:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        if (state.status) {
            //评论成功 更新数据
            [weakSelf loadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpUI {
    [FDVideoCommentCell registerNibCellWithTableView:self.tableView];
    [FDVideoSubCommentCell registerNibCellWithTableView:self.tableView];
    
    //输入框
    UIView *bgV = [[UIView alloc] init];
    bgV.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgV];
    [bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(46);
    }];
    
    //输入框
    UIView *inputV = [[UIView alloc] init];
    inputV.backgroundColor = [UIColor hexFloatColor:@"F4F4F4"];
    inputV.layer.cornerRadius = 17;
    inputV.layer.masksToBounds = YES;
    [bgV addSubview:inputV];
    [inputV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(bgV);
        make.left.mas_equalTo(18);
        make.right.mas_equalTo(-18);
        make.height.mas_equalTo(34);
    }];
    
    //搜索输入框
    UITextField *textField = [[UITextField alloc] init];
    textField.font = [UIFont systemFontOfSize:14];
    textField.textColor = [UIColor hexFloatColor:@"C0C0C0"];
    textField.backgroundColor = [UIColor hexFloatColor:@"EFEFEF"];
    textField.placeholder = Localized(@"写评论...");
    textField.textAlignment = NSTextAlignmentLeft;
    textField.returnKeyType = UIReturnKeySend;
    textField.delegate = self;
    self.textField = textField;
    [inputV addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(inputV);
        make.left.mas_equalTo(17);
        make.right.mas_equalTo(-17);
        make.height.mas_equalTo(34);
    }];
}


#pragma mark ===== UITextFieldDelegate
//点击发送
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [self commentRequest];
    [textField endEditing:YES];
    return YES;
}

//编辑完成
- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.isVideo = YES;
    self.currentIndexPath = nil;
}

#pragma mark ===== UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    FDVideoCommentModel *model = self.dataArr[section];
    NSArray *tempArr = model.list;
    return tempArr.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {//视频评论cell
        FDVideoCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDVideoCommentCell identifier]];
        if (cell == nil) {
            cell = [[FDVideoCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDVideoCommentCell identifier]];
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;

        FDVideoCommentModel *model = self.dataArr[indexPath.section];
        cell.model = model;

        kFDWeakSelf;
        //点击点赞按钮
        cell.niceBlock = ^(UIButton *niceBtn) {
            [weakSelf niceRequestWithRid:model.ID];
        };

        //点击回复按钮
        cell.commentBlock = ^{
            weakSelf.isVideo = NO;
            weakSelf.currentIndexPath = indexPath;
            [weakSelf.textField becomeFirstResponder];
        };

        return cell;
    }else {
        //回复cell
        FDVideoSubCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDVideoSubCommentCell identifier]];
        if (cell == nil) {
            cell = [[FDVideoSubCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDVideoSubCommentCell identifier]];
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        FDVideoCommentModel *model = self.dataArr[indexPath.section];
        NSArray *tempArr = [FDVideoCommentModel mj_objectArrayWithKeyValuesArray:model.list];
        FDVideoCommentModel *tempModel = tempArr[indexPath.row-1];
        cell.model = tempModel;
        
        kFDWeakSelf;
        //点击点赞按钮
        cell.niceBlock = ^(UIButton *niceBtn) {
            [weakSelf niceRequestWithRid:tempModel.ID];
        };
        
        //点击回复按钮
        cell.commentBlock = ^{
            weakSelf.isVideo = NO;
            weakSelf.currentIndexPath = indexPath;
            [weakSelf.textField becomeFirstResponder];
        };
        
        return cell;
    }
}

- (void)endRefreshing {
    
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}

#pragma mark ==== 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        CGRect frame = self.view.bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 104;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        kFDWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf loadData];
        }];
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        _tableView.mj_footer.hidden = YES;
        
        [self.view addSubview:_tableView];
        
    }
    return _tableView;
}

@end
