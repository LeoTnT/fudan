//
//  FDVideoDetailController.m
//  App3.0
//
//  Created by lichao on 2018/8/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoController.h"
#import "FDVideoDetailTopView.h"
#import "FDVideoDetailBottomView.h"
#import "FDFindModel.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "FDVideoDetailController.h"
#import "FDVideoCommentController.h"

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface FDVideoController ()<UIScrollViewDelegate>

//视频播放器
@property (nonatomic, strong) AVPlayerViewController *playerVC;
@property (nonatomic, weak) FDVideoDetailBottomView *bottomView;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) FDVideoDetailController *listVC1;
@property (nonatomic, strong) FDVideoCommentController *listVC2;

@end

@implementation FDVideoController

- (void)viewDidLoad {
    [super viewDidLoad];

    //视频第一帧作为背景图
    UIImageView *bgImageV = [[UIImageView alloc] initWithFrame:self.view.frame];
    [bgImageV sd_setImageWithURL:[NSURL URLWithString:self.videoModel.video_image]];
    [self.view addSubview:bgImageV];
    [self blurImageImplementWithSuperView:self.view];

    [self setUpPlayerVC];
    [self setUpNav];
    [self setUpBottomView];
    [self setUpSegmentController];
    [self loadData];
    
}

#pragma mark ==== 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"vid"] = self.videoModel.videoID;//视频id
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetVideoDetail parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDVideoDetailModel *detailModel = [FDVideoDetailModel mj_objectWithKeyValues:state.data];
            //底部
            weakSelf.bottomView.detailModel = detailModel;
            //详情数据更新
            weakSelf.listVC1.detailV.model = detailModel;
            //评论页面
            weakSelf.listVC2.videoModel = weakSelf.videoModel;

        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//点赞接口
- (void)niceRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"vid"] = self.videoModel.videoID;//视频id
//    params[@"rid"] = @"0";//回复id

    kFDWeakSelf;
    [XSHTTPManager post:FD_ThumbUpUrl parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            //点赞成功 更新数据
            [weakSelf loadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//增加播放次数
- (void)requestWatchVideo {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"vid"] = self.videoModel.videoID;//视频id
    
    [XSHTTPManager post:FD_AddWatchVideo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        
        NSLog(@"===播放增加一次");
    } failure:^(NSError *error) {

    }];
}

#pragma mark ==== setUpUI
- (void)setUpPlayerVC {
    self.playerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.videoModel.video_url]];
    [self.playerVC.player play];
    //播放一次  增加一次
    [self requestWatchVideo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(runLoopTheMovie) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)setUpNav {
    self.fd_prefersNavigationBarHidden = YES;
    
    FDVideoDetailTopView *topV = [FDVideoDetailTopView initView];
    topV.frame = CGRectMake(0, 0, mainWidth, 72);
    topV.backgroundColor = [UIColor clearColor];
    topV.model = self.videoModel;
    kFDWeakSelf;
    topV.backBlock = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.view addSubview:topV];
}

- (void)setUpBottomView {
    FDVideoDetailBottomView *bottomV = [FDVideoDetailBottomView initView];
    bottomV.frame = CGRectMake(0, mainHeight-102, mainWidth, 102);
    bottomV.backgroundColor = [UIColor clearColor];
    kFDWeakSelf;
    //底部按钮点击事件
    bottomV.menuBlock = ^(DetailMenuType menuType) {
        
        if (menuType == MenuBlockTypeNice) {//点赞
            [weakSelf niceRequest];
        }else {//详情/评论
            [weakSelf showBottomViewWithType:menuType isShow:YES];
        }
    };
    
    self.bottomView = bottomV;
    [self.view addSubview:bottomV];
}

- (void)setUpSegmentController {
    //底部背景图
    self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, mainHeight, mainWidth, 380)];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bgView];
    
    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(0, 43, mainWidth, 1)];
    lineV.backgroundColor = [UIColor hexFloatColor:@"E5E5E5"];
    [self.bgView addSubview:lineV];
    //关闭按钮
    UIImageView *closeImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_video_close"]];
    closeImageV.frame = CGRectMake(mainWidth-14-12, 16, 12, 12);
    [self.bgView addSubview:closeImageV];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(mainWidth-35, 6, 30, 30);
    [closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:closeBtn];
    //
    [self.bgView addSubview:self.segmentControl];
    [self.bgView addSubview:self.scrollView];
    
    //根据选项变化位置和内容
    [self addChildViewController:self.listVC1];
    [self.scrollView addSubview:self.listVC1.view];
    [self addChildViewController:self.listVC2];
    [self.scrollView addSubview:self.listVC2.view];
}

//点击关闭按钮
- (void)closeBtnClick {
    [self showBottomViewWithType:0 isShow:NO];
}

//播放完成
- (void)runLoopTheMovie {
    NSLog(@"=======播放完成");
}

//显示/隐藏 详情/评论页
- (void)showBottomViewWithType:(DetailMenuType)type isShow:(BOOL)isShow {
    if (isShow) {
        
        if (type == MenuBlockTypeDetail) {
            self.segmentControl.selectedSegmentIndex = 0;
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }else if (type == MenuBlockTypeComment) {
            self.segmentControl.selectedSegmentIndex = 1;
            [self.scrollView setContentOffset:CGPointMake(mainWidth, 0) animated:YES];
        }
        
        [UIView animateWithDuration:0.3 animations:^{
            //更新播放器的frame
            self.playerVC.videoGravity = AVLayerVideoGravityResizeAspect;
            self.playerVC.view.frame = CGRectMake(0, 72, mainWidth, 200);
            //更新底部视图的frame
            self.bgView.frame = CGRectMake(0, 287, mainWidth, 380);
        }];
        
    }else {
        
        [UIView animateWithDuration:0.3 animations:^{
            //更新播放器的frame
            self.playerVC.videoGravity = AVLayerVideoGravityResize;
            self.playerVC.view.frame = self.view.frame;
            //更新底部视图的frame
            self.bgView.frame = CGRectMake(0, mainHeight, mainWidth, 380);
        }];
    }
}

#pragma mark ===== HMSegmentedControl
- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
}

#pragma mark ===== UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
}

// iOS8 使用系统自带的处理方式
- (void)blurImageImplementWithSuperView:(UIView *)superView {
    UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *view = [[UIVisualEffectView alloc] initWithEffect:beffect];
    view.frame = superView.bounds;
    [superView addSubview:view];
}

#pragma mark ==== 懒加载
- (AVPlayerViewController *)playerVC {
    if (!_playerVC) {
        _playerVC = [[AVPlayerViewController alloc] init];
        _playerVC.showsPlaybackControls = NO;
        _playerVC.videoGravity = AVLayerVideoGravityResize;
        _playerVC.view.frame = self.view.frame;
        _playerVC.view.backgroundColor = [UIColor clearColor];
        _playerVC.videoGravity = AVLayerVideoGravityResize;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            NSLog(@"12312312");
        }];
        
        [_playerVC.view addGestureRecognizer:tap];
        
        [self.view addSubview:_playerVC.view];
        [self addChildViewController:_playerVC];
    }
    return _playerVC;
}

- (FDVideoDetailController *)listVC1 {
    if (!_listVC1) {
        _listVC1= [[FDVideoDetailController alloc] init];
        _listVC1.view.frame = CGRectMake(mainWidth*0, 0, mainWidth, self.scrollView.frame.size.height);
        _listVC1.detailV.frame = CGRectMake(mainWidth*0, 0, mainWidth, self.scrollView.frame.size.height);
    }
    return _listVC1;
}

- (FDVideoCommentController *)listVC2 {
    if (!_listVC2) {
        _listVC2= [[FDVideoCommentController alloc] init];
        _listVC2.view.frame = CGRectMake(mainWidth*1, 0, mainWidth, self.scrollView.frame.size.height);
        _listVC2.tableView.frame = CGRectMake(mainWidth*0, 0, mainWidth, self.scrollView.frame.size.height-46);
    }
    return _listVC2;
}

- (HMSegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth/2, 43)];
//        _segmentControl.type = HMSegmentedControlTypeTextImages;
//        _segmentControl.sectionImages = @[[UIImage imageNamed:@"chat_fans"], [UIImage imageNamed:@"chat_fans"]];
        _segmentControl.sectionTitles = @[Localized(@"详情"), Localized(@"评论")];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor hexFloatColor:@"FF8600"]};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = [UIColor hexFloatColor:@"FF8600"];
        _segmentControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor hexFloatColor:@"999999"],NSForegroundColorAttributeName,[UIFont systemFontOfSize:15], NSFontAttributeName, nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        CGFloat x = 0;
        CGFloat y = CGRectGetMaxY(self.segmentControl.frame)+1;
        CGFloat w = mainWidth;
        CGFloat h = 380-44;
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(x, y, w, h)];
        _scrollView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth, 0);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.scrollEnabled = NO;
//        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
