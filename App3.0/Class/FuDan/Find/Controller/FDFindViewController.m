//
//  FDFindViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFindViewController.h"
#import "FDPeopleNearbyViewController.h"
#import "FDMediaVideoController.h"
#import "FDFindCell.h"

@interface FDFindViewController () <SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *bannerView;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation FDFindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"发现";
    [self actionCustomLeftBtnWithNrlImage:nil htlImage:nil title:nil action:^{
        
    }];
    
    self.dataArray = @[@[
                           @{@"image":@"find_near_friend",@"title":@"附近的人",@"detail":@"可查看附近的人"},
                           @{@"image":@"find_near_store",@"title":@"附近的商家",@"detail":@"可查看附近的商家"}
                       ],
                       @[
                           @{@"image":@"find_new_actions",@"title":@"最新动态",@"detail":@"可查看会员最新动态"},
                           @{@"image":@"find_video",@"title":@"媒体视频",@"detail":@"查看最新的媒体视频"}
                           ]];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    [self setSubviews];
}

- (void)setSubviews {
    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, 160) delegate:self placeholderImage:nil];
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.hidesForSinglePage = YES;
    self.bannerView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    self.bannerView.pageDotColor = [UIColor whiteColor];
    self.bannerView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.bannerView.placeholderImage = [UIImage imageNamed:@"no_pic"];
    self.bannerView.backgroundColor = [UIColor hexFloatColor:@"f4f4f4"];
    
    self.tableView.tableHeaderView = self.bannerView;
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = self.dataArray[section];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FDFindCell *cell = [FDFindCell createFindCellWithTableView:tableView];
    NSArray *arr = self.dataArray[indexPath.section];
    NSDictionary *dic = arr[indexPath.row];
    cell.dataDic = dic;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {//附近的人
            FDPeopleNearbyViewController *nbVC = [[FDPeopleNearbyViewController alloc] init];
            [self.navigationController pushViewController:nbVC animated:YES];
        }else if (indexPath.row == 1) {//附近的商家
            
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {//最新动态
            
        }else if (indexPath.row == 1) {//媒体视频
            FDMediaVideoController *mvVC = [[FDMediaVideoController alloc] init];
            [self.navigationController pushViewController:mvVC animated:YES];
        }
    }
}

@end
