//
//  FDVideoCommentController.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "FDHomeModel.h"

@interface FDVideoCommentController : XSBaseViewController

@property (nonatomic, strong) FDVideoListModel *videoModel;

@property (nonatomic, strong) UITableView *tableView;

@end
