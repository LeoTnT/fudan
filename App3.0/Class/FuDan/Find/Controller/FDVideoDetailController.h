//
//  FDVideoDetailController.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "FDVideoDetailView.h"

@interface FDVideoDetailController : XSBaseViewController

@property (nonatomic, strong) FDVideoDetailView *detailV;

@end
