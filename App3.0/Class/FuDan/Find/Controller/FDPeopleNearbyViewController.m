//
//  FDPeopleNearbyViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPeopleNearbyViewController.h"
#import "FDPeopleNearbyCell.h"
#import "FDLocationManager.h"
#import "FDFindModel.h"

@interface FDPeopleNearbyViewController ()<FDLocationDelegate>

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) FDLocationManager *FDLocationManager;
/* 定位权限是否开启 */
@property (nonatomic, assign) BOOL isOpen;
/*纬度*/
@property (nonatomic, copy) NSString *lat;
/*经度*/
@property (nonatomic, copy) NSString *lng;

@property (nonatomic, assign) NSInteger page;

@end

@implementation FDPeopleNearbyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    [self setUpUI];
    //获取定位信息
    [self.FDLocationManager beginUpdatingLocation];
}

/* 加载新数据 */
- (void)loadData{
    self.page = 1;
    [self loadMoreData];
}

/* 加载更多 */
- (void)loadMoreData{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = @(self.page);
    params[@"lat"] = self.lat;//经度
    params[@"lng"] = self.lng;//纬度
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetNearbyUsers parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArr = [FDNearbyPeopleModel mj_objectArrayWithKeyValuesArray:state.data];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
                weakSelf.dataArr = [NSMutableArray arrayWithArray:tempArr];
            }else {
                [weakSelf.dataArr addObjectsFromArray:tempArr];
            }
            if (tempArr.count > 0) {
                weakSelf.page ++;
            }
            
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf endRefreshing];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)setUpUI {
    
    self.navigationItem.title = Localized(@"附近的人");

    [FDPeopleNearbyCell registerClassCellWithTableView:self.tableView];
    self.tableViewStyle = UITableViewStyleGrouped;
    kFDWeakSelf;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    self.tableView.mj_footer.hidden = YES;
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 74;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FDPeopleNearbyCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDPeopleNearbyCell identifier]];
    if (cell == nil) {
        cell = [[FDPeopleNearbyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDPeopleNearbyCell identifier]];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.model = self.dataArr[indexPath.section];
    
    return cell;
}

- (void)endRefreshing {
    
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}

#pragma mark =====  FDLocationDelegate (定位相关)
- (void)locationIsOpen:(BOOL)isOpen {
    self.isOpen = isOpen;
    
    if (self.isOpen) {//权限打开
        
    }else {//权限未打开
        
    }
}

- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark {
    
//    NSLog(@"%f-------%f", location.coordinate.longitude, location.coordinate.latitude);
    
    self.lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    //获取定位, 加载数据
    [self.tableView.mj_header beginRefreshing];
    
    if (placemark != nil) {
        //        NSString *myAddress;
        //        if (placemark.administrativeArea) {
        //            myAddress = [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.subLocality];
        //        }else {
        //            myAddress = [NSString stringWithFormat:@"%@%@", placemark.locality, placemark.subLocality];
        //        }
        
        //        NSLog(@"定位地址:%@", myAddress);
    }
}

#pragma mark ==== 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (FDLocationManager *)FDLocationManager {
    if (!_FDLocationManager) {
        _FDLocationManager = [[FDLocationManager alloc] init];
        _FDLocationManager.delegate = self;
    }
    return _FDLocationManager;
}

@end
