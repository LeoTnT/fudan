//
//  FDVideoDetailController.h
//  App3.0
//
//  Created by lichao on 2018/8/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "FDHomeModel.h"

//视频详情页面
@interface FDVideoController : XSBaseViewController

@property (nonatomic, strong) FDVideoListModel *videoModel;

@end
