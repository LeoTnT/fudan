//
//  FDVideoDetailController.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoDetailController.h"

@interface FDVideoDetailController ()

@end

@implementation FDVideoDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
}

- (void)setUpUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.detailV = [FDVideoDetailView initView];
    self.detailV.frame = self.view.bounds;
    [self.view addSubview:self.detailV];
}

@end
