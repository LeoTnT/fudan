//
//  FDPostingController.m
//  App3.0
//
//  Created by lichao on 2018/9/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPostingController.h"
#import "FDPostingView.h"
#import "VideoHandleModel.h"

@interface FDPostingController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, VideoHandleModelDelegate>

@property (nonatomic, strong) FDPostingView *postingView;
/**视频控制器*/
@property (nonatomic, strong) UIImagePickerController *videoPickerVC;
/**视频缩略图*/
@property (nonatomic,strong) UIImage *image;
/**本地路径*/
@property (nonatomic,strong) NSURL *url;
/**视频数据*/
@property (nonatomic,strong) NSData *videoData;

@end

@implementation FDPostingController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = Localized(@"发布视频");
    self.view.backgroundColor = [UIColor hexFloatColor:@"FFFFFF"];
    kFDWeakSelf;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back_h" htlImage:nil title:nil action:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"发布") action:^{
        [weakSelf uploadVideo];
    }];
    
    [self setUpUI];
}

#pragma mark ===== 网络请求
- (void)pushRequestWithVideoUrl:(NSString *)videoUrl imageUrl:(NSString *)imageUrl{
    
    if (self.postingView.inputTextView.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请输入内容")];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"video_image"] = imageUrl;//视频封面链接
    params[@"video_url"] = videoUrl;//视频链接
    params[@"content"] = self.postingView.inputTextView.text;//内容
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_InsertVideo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            [XSTool showToastWithView:weakSelf.view Text:Localized(@"发布成功")];
            /*延迟执行时间*/
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            });
            
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }

    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//上传视频
- (void)uploadVideo {
    if (self.videoData.length <= 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请选择视频")];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"video";
    params[@"formname"] = @"file";

    kFDWeakSelf;
    [XSTool showProgressHUDTOView:self.view withText:Localized(@"发布中..")];
    [HTTPManager upLoadDataIsVideo:YES WithDic:params andData:self.videoData WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        if(state){
            //上传成功发布视频
            NSString *imageURL = [NSString stringWithFormat:@"%@", dic[@"data"][@"image"]];
            NSString *videoURL = [NSString stringWithFormat:@"%@", dic[@"data"][@"video"]];

            [weakSelf pushRequestWithVideoUrl:videoURL imageUrl:imageURL];
        }else{
            [XSTool showToastWithView:weakSelf.view Text:dic[@"info"]];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpUI {
    self.postingView = [FDPostingView initView];
    [self.view addSubview:self.postingView];
    [self.postingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(195);
    }];

    kFDWeakSelf;
    self.postingView.addBlock = ^(UIButton *addButton) {
        [weakSelf addVideo];
    };
}

#pragma mark ===== 视频相关
//** 拍摄视频 */
- (void)addVideo {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.videoPickerVC=[[UIImagePickerController alloc] init];
    //数据源
    self.videoPickerVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    self.videoPickerVC.mediaTypes=@[(NSString *)kUTTypeMovie];
    //展示拍照控板
    self.videoPickerVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.videoPickerVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModeVideo;
    //后置摄像头
    self.videoPickerVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.videoPickerVC.delegate=self;
    //视频最大时间
    self.videoPickerVC.videoMaximumDuration=10;
    self.videoPickerVC.videoQuality=UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:self.videoPickerVC animated:YES completion:nil];
}

#pragma mark ===== VideoHandleModelDelegate
- (void)sendVideoDataWithDic:(NSDictionary *)videoDic {

    self.image = videoDic[@"image"];
    self.url = videoDic[@"url"];
    self.videoData = videoDic[@"videoData"];
    kFDWeakSelf;
    [self.videoPickerVC dismissViewControllerAnimated:YES completion:^{
        [weakSelf.postingView.addButton setImage:self.image forState:UIControlStateNormal];
    }];
}

#pragma mark ===== UIImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    if([picker isEqual:self.videoPickerVC]){
        //视频路径
        NSURL *url=[info objectForKey:@"UIImagePickerControllerMediaURL"];
        VideoHandleModel *model=[[VideoHandleModel alloc]init];
        model.delegate=self;
        [model getVideoFirstImageAndDataAndUrlWithVideoUrl:url];
    }
}

@end
