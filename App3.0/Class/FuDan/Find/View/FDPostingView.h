//
//  FDPostingView.h
//  App3.0
//
//  Created by lichao on 2018/9/6.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

typedef void(^addBlock)(UIButton *addButton);
@interface FDPostingView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (nonatomic, copy) addBlock addBlock;

@end
