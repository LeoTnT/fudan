//
//  FDPostingView.m
//  App3.0
//
//  Created by lichao on 2018/9/6.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPostingView.h"

@interface FDPostingView ()<UITextViewDelegate>

@end

@implementation FDPostingView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.inputTextView.delegate = self;

}

- (IBAction)addBtnClick:(id)sender {
    if (self.addBlock) {
        self.addBlock(self.addButton);
    }
}

#pragma mark ===== UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 0) {
        self.placeholderLabel.hidden = YES;
    }else {//
        self.placeholderLabel.hidden = NO;
    }
}


@end
