//
//  FDVideDetailTopView.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDHomeModel.h"

typedef void(^backBlock)(void);
@interface FDVideoDetailTopView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (nonatomic, copy) backBlock backBlock;
@property (nonatomic, strong) FDVideoListModel *model;

@end
