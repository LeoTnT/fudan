//
//  FDFindCell.m
//  App3.0
//
//  Created by 沈浩 on 2018/8/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFindCell.h"

@interface FDFindCell()
@property (nonatomic, strong) UIImageView *cellImageView;
@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic, strong) UILabel *cellDetail;
@end

@implementation FDFindCell

+ (instancetype)createFindCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"FDFindCell";
    FDFindCell *cell = (FDFindCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[FDFindCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    self.cellImageView = [UIImageView new];
    [self.contentView addSubview:self.cellImageView];
    [self.cellImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(36, 36));
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(self);
    }];
    
    self.cellTitle = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(16) titleColor:COLOR_111111];
    [self.contentView addSubview:self.cellTitle];
    [self.cellTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cellImageView.mas_right).offset(9);
        make.bottom.mas_equalTo(self.cellImageView.mas_centerY).offset(-2);
    }];
    
    self.cellDetail = [BaseUITool labelWithTitle:@"" textAlignment:NSTextAlignmentLeft font:SYSTEM_FONT(12) titleColor:COLOR_999999];
    [self.contentView addSubview:self.cellDetail];
    [self.cellDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cellTitle);
        make.top.mas_equalTo(self.cellImageView.mas_centerY).offset(2);
    }];
    
    UIImageView *arrow = [UIImageView new];
    arrow.image = [UIImage imageNamed:@"cell_arrow"];
    [self.contentView addSubview:arrow];
    [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(self);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = BG_COLOR;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.contentView.mas_bottom);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
}

- (void)setDataDic:(NSDictionary *)dataDic {
    _dataDic = dataDic;
    
    self.cellImageView.image = [UIImage imageNamed:dataDic[@"image"]];
    self.cellTitle.text = dataDic[@"title"];
    self.cellDetail.text = dataDic[@"detail"];
}
@end
