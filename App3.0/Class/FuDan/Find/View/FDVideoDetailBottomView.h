//
//  FDVideoDetailBottomView.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDFindModel.h"

typedef NS_ENUM(NSUInteger, DetailMenuType) {
    MenuBlockTypeDetail,
    MenuBlockTypeComment,
    MenuBlockTypeNice,
};

typedef void(^menuBlock)(DetailMenuType menuType);
@interface FDVideoDetailBottomView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UIButton *niceBtn;

@property (nonatomic, strong) FDVideoDetailModel *detailModel;
@property (nonatomic, strong) menuBlock menuBlock;

@end
