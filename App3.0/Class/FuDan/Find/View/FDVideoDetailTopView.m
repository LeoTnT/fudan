//
//  FDVideDetailTopView.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoDetailTopView.h"
#import "XSFormatterDate.h"

@implementation FDVideoDetailTopView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.logo.layer.cornerRadius = 36/2;
    self.logo.layer.masksToBounds = YES;
}

- (void)setModel:(FDVideoListModel *)model {
    [self.logo sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.nameLabel.text = model.nickname;
    
    self.detailLabel.text = [NSString stringWithFormat:@"%@ · %@%@", [XSFormatterDate fdDateWithTimeIntervalString:model.w_time], model.look_num, Localized(@"次播放")];
}

- (IBAction)backBtnClick:(id)sender {
    if (self.backBlock) {
        self.backBlock();
    }
}

@end
