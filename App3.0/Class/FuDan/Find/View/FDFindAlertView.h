//
//  FDFindAlertView.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

typedef NS_ENUM(NSUInteger, TabFindType) {
    TabFindTypeNearbyPeople,
    TabFindTypeNews,
    TabFindTypeNearbyShops,
    TabFindTypezVideo
};

typedef void(^tabFindBlock)(TabFindType type);
@interface FDFindAlertView : FDBaseCustomView

@property (nonatomic, strong) tabFindBlock tabFindBlock;

@end
