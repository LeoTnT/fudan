//
//  FDVideoDetailBottomView.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoDetailBottomView.h"

@implementation FDVideoDetailBottomView

- (void)setDetailModel:(FDVideoDetailModel *)detailModel {
    if (detailModel) {
        self.contentLabel.text = detailModel.content;
        [self.commentBtn setTitle:[NSString stringWithFormat:@"%@·%@", Localized(@"评论"), detailModel.ping_num] forState:UIControlStateNormal];
        [self.niceBtn setTitle:[NSString stringWithFormat:@"%@·%@", Localized(@"赞"), detailModel.zan_num] forState:UIControlStateNormal];
        [self.niceBtn setTitle:[NSString stringWithFormat:@"%@·%@", Localized(@"赞"), detailModel.zan_num] forState:UIControlStateSelected];
        //是否是本人点赞
        self.niceBtn.selected = [detailModel.is_zan integerValue];
    }
}

- (IBAction)detailBtnClick:(id)sender {
    if (self.menuBlock) {
        self.menuBlock(MenuBlockTypeDetail);
    }
}

- (IBAction)commentBtnClick:(id)sender {
    if (self.menuBlock) {
        self.menuBlock(MenuBlockTypeComment);
    }
}

- (IBAction)niceBtnClick:(id)sender {
    if (self.menuBlock) {
        self.menuBlock(MenuBlockTypeNice);
    }
}

@end
