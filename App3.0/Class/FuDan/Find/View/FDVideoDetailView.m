//
//  FDVideoDetailView.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoDetailView.h"
#import "XSFormatterDate.h"

@implementation FDVideoDetailView

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setModel:(FDVideoDetailModel *)model {
    
    self.videoDetailLabel.text = model.content;
    self.videoTimeLabel.text = [XSFormatterDate fdDateWithTimeIntervalString:model.w_time];
    self.videoNiceNumLabel.text = [NSString stringWithFormat:@"%@次赞", model.zan_num];
}

@end
