//
//  FDFindAlertView.m
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFindAlertView.h"

@implementation FDFindAlertView

//关闭按钮
- (IBAction)closeBtnClick:(id)sender {
    [self removeFromSuperview];
}

//附近的人
- (IBAction)nearbyPeopleBtnClick:(id)sender {
    if (self.tabFindBlock) {
        [self removeFromSuperview];
        self.tabFindBlock(TabFindTypeNearbyPeople);
    }
}
//最新动态
- (IBAction)newsBtnClick:(id)sender {
    if (self.tabFindBlock) {
        [self removeFromSuperview];
        self.tabFindBlock(TabFindTypeNews);
    }
}
//附近商店
- (IBAction)nearbyShopsBtnClick:(id)sender {
    if (self.tabFindBlock) {
        [self removeFromSuperview];
        self.tabFindBlock(TabFindTypeNearbyShops);
    }
}
//媒体视频
- (IBAction)videoBtnClick:(id)sender {
    if (self.tabFindBlock) {
        [self removeFromSuperview];
        self.tabFindBlock(TabFindTypezVideo);
    }
}

@end
