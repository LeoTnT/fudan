//
//  FDVideoDetailView.h
//  App3.0
//
//  Created by lichao on 2018/9/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDFindModel.h"

@interface FDVideoDetailView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UILabel *videoDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoNiceNumLabel;

@property (nonatomic, strong) FDVideoDetailModel *model;

@end
