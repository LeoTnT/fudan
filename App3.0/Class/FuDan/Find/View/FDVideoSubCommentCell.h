//
//  FDVideoCommentCell.h
//  App3.0
//
//  Created by lichao on 2018/9/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDFindModel.h"

typedef void(^niceBlock)(UIButton *niceBtn);
typedef void(^commentBlock)(void);
@interface FDVideoSubCommentCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *niceButton;

@property (nonatomic, strong) FDVideoCommentModel *model;

@property (nonatomic, copy) niceBlock niceBlock;
@property (nonatomic, copy) commentBlock commentBlock;

@end
