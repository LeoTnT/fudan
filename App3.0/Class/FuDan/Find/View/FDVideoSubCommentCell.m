//
//  FDVideoCommentCell.m
//  App3.0
//
//  Created by lichao on 2018/9/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDVideoSubCommentCell.h"
#import "XSFormatterDate.h"

@implementation FDVideoSubCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.logo.layer.cornerRadius = 32/2;
    self.logo.layer.masksToBounds = YES;
}

- (void)setModel:(FDVideoCommentModel *)model {
    [self.logo sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    self.nameLabel.text = model.nickname;
    self.contentLabel.text = model.content;
    self.timeLabel.text = [XSFormatterDate fdDateWithTimeIntervalString:model.w_time];
    [self.niceButton setTitle:model.zan_num forState:UIControlStateNormal];
    [self.niceButton setTitle:model.zan_num forState:UIControlStateSelected];
    //是否是本人点赞
    self.niceButton.selected = [model.is_zan integerValue];
}

- (IBAction)replyBtnClick:(id)sender {
    if (self.commentBlock) {
        self.commentBlock();
    }
}

- (IBAction)niceBtnClick:(id)sender {
    if (self.niceBlock) {
        self.niceBlock(self.niceButton);
    }
}


@end
