//
//  PPFindModel.h
//  App3.0
//
//  Created by lichao on 2018/9/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDFindModel : NSObject

@end

@interface FDNearbyPeopleModel : NSObject

/* 用户id */
@property (nonatomic, copy) NSString *uid;
/* 用户编号 */
@property (nonatomic, copy) NSString *username;
/* 昵称 */
@property (nonatomic, copy) NSString *nickname;
/* 距离 */
@property (nonatomic, copy) NSString *distance;
/* 最新一条说说[文字] */
@property (nonatomic, copy) NSString *fandom;
/* 头像 */
@property (nonatomic, copy) NSString *logo;

@end

@interface FDVideoDetailModel : NSObject

/* 时间 */
@property (nonatomic, copy) NSString *w_time;
/* 内容 */
@property (nonatomic, copy) NSString *content;
/* 赞数 */
@property (nonatomic, copy) NSString *zan_num;
/* 评论数 */
@property (nonatomic, copy) NSString *ping_num;
/* 是否已经点赞 */
@property (nonatomic, copy) NSString *is_zan;

@end

@interface FDVideoCommentModel : NSObject

/* 内容 */
@property (nonatomic, copy) NSString *content;
/*  */
@property (nonatomic, copy) NSString *hid;
/*  */
@property (nonatomic, copy) NSString *ID;
/* 是否已经点赞[登录状态] */
@property (nonatomic, copy) NSString *is_zan;
/* 列表 */
@property (nonatomic, copy) NSArray *list;
/* 用户头像[评论] */
@property (nonatomic, copy) NSString *logo;
/* 用户昵称[评论] */
@property (nonatomic, copy) NSString *nickname;
/* 评论/回复ID */
@property (nonatomic, copy) NSString *rid;
/* 用户头像[被评论] */
@property (nonatomic, copy) NSString *tologo;
/* 用户昵称[被评论] */
@property (nonatomic, copy) NSString *tonickname;
/* 用户ID[被评论] */
@property (nonatomic, copy) NSString *touid;
/* 用户编号[评论] */
@property (nonatomic, copy) NSString *tousername;
/* 用户ID[评论] */
@property (nonatomic, copy) NSString *uid;
/* 用户编号[评论] */
@property (nonatomic, copy) NSString *username;
/* 视频ID */
@property (nonatomic, copy) NSString *vid;
/* 时间 */
@property (nonatomic, copy) NSString *w_time;
/* 点赞数 */
@property (nonatomic, copy) NSString *zan_num;

@end
