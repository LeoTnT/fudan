//
//  FDNoticeModel.h
//  App3.0
//
//  Created by lichao on 2018/11/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDNoticeModel : NSObject

/** 顶部广告 */
@property (nonatomic, strong) NSArray *nav_top;
/** 中间广告 */
@property (nonatomic, strong) NSArray *nav_mid;
/** 底部广告 */
@property (nonatomic, strong) NSArray *nav_bom;

/** 上面文章 */
@property (nonatomic, strong) NSArray *art_top;
/** 下面文章 */
@property (nonatomic, strong) NSArray *art_bom;

@end

@interface FDNoticeAdModel : NSObject

/** 名称 */
@property (nonatomic, copy) NSString *name;
/** 图片 */
@property (nonatomic, copy) NSString *image;
/** 创建时间 */
@property (nonatomic, copy) NSString *link_in;
/** 创建时间 */
@property (nonatomic, copy) NSString *link_objid;
/** url */
@property (nonatomic, copy) NSString *url;

@end

@interface FDNoticeArtModel : NSObject

/** 文章ID */
@property (nonatomic, copy) NSString *article_id;
/** 标题 */
@property (nonatomic, copy) NSString *title;
/** 短标题 */
@property (nonatomic, copy) NSString *subtitle;
/** 分类ID */
@property (nonatomic, copy) NSString *category_id;
/** 分类名称 */
@property (nonatomic, copy) NSString *category_name;
/** 图片 */
@property (nonatomic, copy) NSString *image;
/** 作者 */
@property (nonatomic, copy) NSString *author;
/** 内容 */
@property (nonatomic, copy) NSString *content;
/** 查看数 */
@property (nonatomic, copy) NSString *look_num;
/** 创建时间 */
@property (nonatomic, copy) NSString *w_time;

@end
