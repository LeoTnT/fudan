//
//  FDNoticeModel.m
//  App3.0
//
//  Created by lichao on 2018/11/5.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDNoticeModel.h"

@implementation FDNoticeModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"nav_top" : @"FDNoticeAdModel",
             @"nav_mid" : @"FDNoticeAdModel",
             @"nav_bom" : @"FDNoticeAdModel",
             @"art_top" : @"FDNoticeArtModel",
             @"art_bom" : @"FDNoticeArtModel",
             };
}
@end

@implementation FDNoticeAdModel

@end

@implementation FDNoticeArtModel

@end
