//
//  FDNoticeListCell.h
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDNoticeModel.h"

@interface FDNoticeListCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *timeAndDesL;
@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (nonatomic, strong) FDNoticeArtModel *model;

@end
