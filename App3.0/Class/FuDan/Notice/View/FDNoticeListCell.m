//
//  FDNoticeListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDNoticeListCell.h"
#import "XSFormatterDate.h"

@implementation FDNoticeListCell

- (void)setModel:(FDNoticeArtModel *)model {
    if (model) {
        self.titleL.text = model.title;
        self.timeAndDesL.text = [NSString stringWithFormat:@"%@·%@", [XSFormatterDate fdDateWithTimeIntervalString:model.w_time], model.author];
        [self.logo sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


@end
