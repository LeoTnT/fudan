//
//  FDNoticeController.m
//  App3.0
//
//  Created by lichao on 2018/10/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDNoticeController.h"
#import "FDNoticeHeaderView.h"
#import "FDNoticeListCell.h"
#import "FDNoticeModel.h"
#import "FDLoginController.h"

@interface FDNoticeController ()<SDCycleScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>
//头部滚动广告视图
@property (nonatomic, strong) SDCycleScrollView *bannerView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) FDNoticeModel *noticeModel;

@property (nonatomic, strong) NSMutableArray *adDataArr;
@property (nonatomic, strong) NSMutableArray *dataArr1;
@property (nonatomic, strong) NSMutableArray *dataArr2;

@end

@implementation FDNoticeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.fd_prefersNavigationBarHidden = YES;
    self.navigationItem.title = Localized(@"星球公告");
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpBannerView];
    [self setUpTableView];
    
    [self beginRefreshTableView];
}

#pragma mark ===== 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetNotice parameters:params success:^(NSDictionary *dic, resultObject *state) {
        [weakSelf endRefreshTableView];
        if (state.status) {
            weakSelf.noticeModel = [FDNoticeModel mj_objectWithKeyValues:state.data];
            
            weakSelf.adDataArr = [NSMutableArray arrayWithArray:weakSelf.noticeModel.nav_top];
            NSMutableArray *titles = [NSMutableArray array];
            NSMutableArray *images = [NSMutableArray array];
            for (FDNoticeAdModel *adModel in weakSelf.adDataArr) {
                [titles addObject:adModel.name];
                [images addObject:adModel.image];
            }
            weakSelf.bannerView.localizationImageNamesGroup = images;
            weakSelf.bannerView.titlesGroup = titles;
            
            weakSelf.dataArr1 = [NSMutableArray arrayWithArray:weakSelf.noticeModel.art_top];
            weakSelf.dataArr2 = [NSMutableArray arrayWithArray:weakSelf.noticeModel.art_bom];
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
/************ 顶部广告轮播 ************/
- (void)setUpBannerView {
    
    self.bannerView.frame = CGRectMake(0, 0, mainWidth, FontNum(200));
    
    self.bannerView.delegate = self;
    self.bannerView.placeholderImage = [UIImage imageNamed:@"no_pic"];
    self.bannerView.backgroundColor = [UIColor clearColor];
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.hidesForSinglePage = YES;
    self.bannerView.currentPageDotColor = [UIColor grayColor]; // 自定义分页控件小圆标颜色
    self.bannerView.pageDotColor = [UIColor whiteColor];
    self.bannerView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.bannerView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDNoticeListCell registerNibCellWithTableView:self.tableView];
    self.tableView.tableHeaderView = self.bannerView;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.dataArr1.count;
    }
    return self.dataArr2.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 107;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [FDNoticeHeaderView height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    FDNoticeHeaderView *headerV = [FDNoticeHeaderView initView];
    headerV.frame = CGRectMake(0, 0, mainWidth, 134);
    if (section == 0) {
        FDNoticeAdModel *adModel = self.noticeModel.nav_mid.firstObject;
        [headerV.adImageV sd_setImageWithURL:[NSURL URLWithString:adModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }else {
        FDNoticeAdModel *adModel = self.noticeModel.nav_bom.firstObject;
        [headerV.adImageV sd_setImageWithURL:[NSURL URLWithString:adModel.image] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    }
    
    return headerV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDNoticeListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDNoticeListCell identifier]];
    if (cell == nil) {
        cell = [[FDNoticeListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDNoticeListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        cell.model = self.dataArr1[indexPath.row];
    }else {
        cell.model = self.dataArr2[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FDNoticeArtModel *model;
    if (indexPath.section == 0) {
        model = self.dataArr1[indexPath.row];
    }else {
        model = self.dataArr2[indexPath.row];
    }
    
    XSBaseWKWebViewController *detailVC = [[XSBaseWKWebViewController alloc] init];
    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
    if (guid.length <= 0) {
        guid = @"app";
    }
    detailVC.urlStr = [NSString stringWithFormat:@"%@/wap/#/article2/detail/%@?device=%@", ImageBaseUrl, model.article_id, guid];
    detailVC.urlType = WKWebViewURLNormal;
    detailVC.navigationItem.title = model.title;
    [self fd_pushViewController:detailVC];
}

- (void)fd_pushViewController:(UIViewController *)vc {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        FDLoginController *loginVC = [[FDLoginController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:loginVC];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ===== SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (cycleScrollView == self.bannerView) {
        
    }
}

- (void)navBackBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, mainHeight-kStatusBarAndNavigationBarHeight-kFDTabbarHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        //下拉刷新
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf loadData];
        }];
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (SDCycleScrollView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[SDCycleScrollView alloc] init];
        [self.view addSubview:_bannerView];
    }
    return _bannerView;
}

@end
