//
//  FDForumModel.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDForumModel : NSObject

@end

//头像列表
@interface FDIconModel : NSObject

@property (nonatomic, copy) NSString *ID;
/** 编号 */
@property (nonatomic, copy) NSString *img;
/** 昵称 */
@property (nonatomic, copy) NSString *is_default;

@end

//用户信息
@interface FDUserInfoModel : NSObject

@property (nonatomic, copy) NSString *ID;
/** 编号 */
@property (nonatomic, copy) NSString *username;
/** 昵称 */
@property (nonatomic, copy) NSString *nickname;
/** 头像 */
@property (nonatomic, copy) NSString *dry_logo;
/** 实体众筹等级 */
@property (nonatomic, copy) NSString *collect_rank;
/** 什么币[待定] */
@property (nonatomic, copy) NSString *coin;

@end

//
@interface FDUserNumModel : NSObject
/** 我燥 */
@property (nonatomic, copy) NSString *pnum;
/** 他燥 */
@property (nonatomic, copy) NSString *snum;

@end

