//
//  FDFriendModel.h
//  App3.0
//
//  Created by lichao on 2018/11/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDFriendModel : NSObject

/** ID */
@property (nonatomic, copy) NSString *ID;
/** 标题 */
@property (nonatomic, copy) NSString *title;
/** uid */
@property (nonatomic, copy) NSString *uid;
/** 用户编号 */
@property (nonatomic, copy) NSString *username;
/** 昵称 */
@property (nonatomic, copy) NSString *nickname;
/** 燥头像 */
@property (nonatomic, copy) NSString *dry_logo;
/** 总头像 */
@property (nonatomic, copy) NSString *logo;
/** 燥事时间 */
@property (nonatomic, copy) NSString *w_time;

@end
