//
//  FDProblemModel.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDProblemModel.h"

@implementation FDProblemModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"ID" : @"id"
             };
}
@end

@implementation FDProblemListModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"ID" : @"id"
             };
}
@end

@implementation FDProblemInfoModel

@end

@implementation FDProblemInfoTopModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"ID" : @"id"
             };
}
@end

@implementation FDProblemInfoReplyModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"data" : @"FDProblemReplyListModel"
             };
}
@end


@implementation FDProblemReplyListModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"reply" : @"FDProblemReplyListModel"
             };
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"ID" : @"id"
             };
}
@end
