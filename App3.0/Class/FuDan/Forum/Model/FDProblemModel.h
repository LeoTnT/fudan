//
//  FDProblemModel.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

//标签/球
@interface FDProblemModel : NSObject

/** ID */
@property (nonatomic, copy) NSString *ID;
/** 用户ID */
@property (nonatomic, copy) NSString *uid;
/** 主题 */
@property (nonatomic, copy) NSString *title;
/** 内容 */
@property (nonatomic, copy) NSString *content;
/** 短主题 */
@property (nonatomic, copy) NSString *title_short;


@end

//我燥/他燥列表
@interface FDProblemListModel : NSObject

@property (nonatomic, copy) NSString *ID;
/** 用户ID */
@property (nonatomic, copy) NSString *uid;
/** 主题 */
@property (nonatomic, copy) NSString *title;
/** 内容 */
@property (nonatomic, copy) NSString *content;
/** 评论数 */
@property (nonatomic, copy) NSString *num;
/** 时间 */
@property (nonatomic, copy) NSString *w_time;

@property (nonatomic, copy) NSString *is_show;

@end

//我燥/他燥详情页
@class FDProblemInfoTopModel;
@class FDProblemInfoReplyModel;
@interface FDProblemInfoModel : NSObject

@property (nonatomic, strong) FDProblemInfoTopModel *message;
/** 用户ID */
@property (nonatomic, strong) FDProblemInfoReplyModel *reply;

@end

@interface FDProblemInfoTopModel : NSObject

/**  */
@property (nonatomic, copy) NSString *ID;
/** 用户ID */
@property (nonatomic, copy) NSString *uid;
/** 标题 */
@property (nonatomic, copy) NSString *title;
/** 内容 */
@property (nonatomic, copy) NSString *content;
/** 时间 */
@property (nonatomic, copy) NSString *w_time;
/**  */
@property (nonatomic, copy) NSString *num;

@end

@interface FDProblemInfoReplyModel : NSObject

/**  */
@property (nonatomic, copy) NSString *current_page;
/**  */
@property (nonatomic, copy) NSString *per_page;
/**  */
@property (nonatomic, copy) NSString *total;

//评论数组
@property (nonatomic, strong) NSArray *data;

@end

@interface FDProblemReplyListModel : NSObject

//回复数组
@property (nonatomic, strong) NSArray *reply;

/**  */
@property (nonatomic, copy) NSString *ID;
/** 关联燥事ID */
@property (nonatomic, copy) NSString *mid;
/** 用户ID */
@property (nonatomic, copy) NSString *uid;
/** 回复ID */
@property (nonatomic, copy) NSString *rid;
/** 被评论人ID */
@property (nonatomic, copy) NSString *touid;
/** 评论内容 */
@property (nonatomic, copy) NSString *content;
/** 时间 */
@property (nonatomic, copy) NSString *w_time;
/** 评论用户编号 */
@property (nonatomic, copy) NSString *username;
/** 评论昵称 */
@property (nonatomic, copy) NSString *nickname;
/** logo */
@property (nonatomic, copy) NSString *logo;
/** 被评论用户编号 */
@property (nonatomic, copy) NSString *tousername;
/** 被评论昵称 */
@property (nonatomic, copy) NSString *tonickname;
/** 被评论头像 */
@property (nonatomic, copy) NSString *tologo;

@end
