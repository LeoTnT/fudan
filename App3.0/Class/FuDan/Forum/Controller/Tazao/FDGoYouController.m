//
//  FDGoYouController.m
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDGoYouController.h"
#import "FDGoTopView.h"
#import "FDProblemModel.h"

@interface FDGoYouController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) FDGoTopView *topV;
@property (nonatomic, strong) UITableView *tableView;


@end

@implementation FDGoYouController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = mainColor;
    
    [self setUpTableView];
    [self setUpCustomNav];
    [self loadData];
}

#pragma mark ===== 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mid"] = self.mid;//燥事ID
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemHelp parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDProblemInfoTopModel *topModel = [FDProblemInfoTopModel mj_objectWithKeyValues:state.data];
            weakSelf.topV.model = topModel;
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {

        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)replyMessage {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mid"] = self.mid;//燥事ID
    params[@"rid"] = @"0";//燥事ID
    params[@"content"] = self.topV.inputTextF.text;//回复内容
    
    kFDWeakSelf;
    [XSTool showProgressHUDWithView:self.view];
    [XSHTTPManager post:FD_GetReply parameters:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        if (state.status) {
            [XSTool showToastWithView:weakSelf.view Text:@"发布成功"];
            /*延迟执行时间*/
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:Localized(@"乱七八燥") textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    FDGoTopView *topV = [FDGoTopView initView];
    topV.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    
    kFDWeakSelf;
    //点击-"走你"
    topV.pushBlock = ^{
        [weakSelf replyMessage];
    };
    
    self.topV = topV;
    self.tableView.tableHeaderView = topV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = mainColor;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
