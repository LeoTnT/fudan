//
//  FDSphereController.m
//  App3.0
//
//  Created by lichao on 2018/10/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDSphereController.h"
#import "FDGoYouController.h"//走你界面
#import "MFSideMenu.h"
#import "DBSphereView.h"
#import "FDProblemModel.h"

@interface FDSphereController ()

@property (nonatomic, strong) DBSphereView *sphereView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation FDSphereController

//修改状态栏的前景色
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //view出现的时候状态栏前景颜色改为亮色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //view消失的时候状态栏前景颜色改为默认/黑色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showBackgroudViewWithImageName:@"fd_blue_bg"];
    [self setHidesBottomBarWhenPushed:YES];
    [self setUpCustomNav];
    
    [self loadData];
}

#pragma mark ===== 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"order"] = @"rand";//随机rand|热门num
    params[@"is_short"] = @"1";//默认0|1(是否短标题)
    params[@"limit"] = @"36";//数据个数
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemMore parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
           
            if (weakSelf.sphereView) {
                [weakSelf.sphereView removeFromSuperview];
            }
            if (weakSelf.dataArr.count > 0) {
                [weakSelf.dataArr removeAllObjects];
            }
            
            weakSelf.dataArr = [NSMutableArray arrayWithArray:[FDProblemModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            
            [weakSelf setUpSphereView];
            [weakSelf setUpBottomButton];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_nav_menu" block:^{
        //点击头像滑到左侧
        [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    }];
}

/** 立体标签球 */
- (void)setUpSphereView {
    
    //0-红色, 1-白色, 2-黄色, 3-白色
    NSArray *sphereColors = @[[UIColor hexFloatColor:@"F03C68"], [UIColor hexFloatColor:@"FFFFFF"], [UIColor hexFloatColor:@"FFE816"]];
    
    self.sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(30, kStatusBarAndNavigationBarHeight+68, mainWidth-30*2, mainWidth)];
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < self.dataArr.count; i ++) {
        
        FDProblemModel *model = self.dataArr[i];
        //背景
        UIView *bgV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 60)];
        bgV.backgroundColor = [UIColor clearColor];
        bgV.tag = i;
        //标题
        UILabel *titleL = [XSUITool creatLabelWithFrame:CGRectMake(0, 0, 100, 13) titleColor:[UIColor hexFloatColor:@"FFFFFF"] titleFont:14 backgroundColor:[UIColor clearColor] textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"%@", model.title]];
        [bgV addSubview:titleL];
        //圆球
        UIView *spereV = [[UIView alloc] initWithFrame:CGRectMake((100-23)/2, 13+4, 23, 23)];
        //设置随机背景色
        int randNum = [self getRandomNumber:0 to:2];
        UIColor *color = sphereColors[randNum];
        spereV.backgroundColor = color;
        spereV.layer.cornerRadius = 23/2;
        spereV.layer.masksToBounds = YES;
        [bgV addSubview:spereV];
        //描述
        UILabel *desL = [XSUITool creatLabelWithFrame:CGRectMake(0, 13+4+23+3, 100, 10) titleColor:[UIColor hexFloatColor:@"FFFFFF"] titleFont:12 backgroundColor:[UIColor clearColor] textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"%@", model.title_short]];
        [bgV addSubview:desL];
        
        [array addObject:bgV];
        [self.sphereView addSubview:bgV];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgVClickWithBgView:)];
        [bgV addGestureRecognizer:tap];
    }
    [self.sphereView setCloudTags:array];
    self.sphereView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.sphereView];
}

- (void)setUpBottomButton {
    //换一批按钮
    UIButton *changeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    changeBtn.frame = CGRectMake((mainWidth-95)/2, CGRectGetMaxY(self.sphereView.frame)+40, 95, 38);
    [changeBtn setTitle:Localized(@"换一批") forState:UIControlStateNormal];
    changeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    changeBtn.layer.borderColor = [UIColor hexFloatColor:@"00E0FF"].CGColor;
    changeBtn.layer.borderWidth = 2;
    changeBtn.layer.cornerRadius = 38/2;
    changeBtn.layer.masksToBounds = YES;
    [changeBtn addTarget:self action:@selector(changeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:changeBtn];
    
    //提示语
    UILabel *alertL = [XSUITool creatLabelWithFrame:CGRectMake((mainWidth-100)/2, CGRectGetMaxY(changeBtn.frame)+15, 100, 12) titleColor:[UIColor hexFloatColor:@"FFFFFF"] titleFont:12 backgroundColor:[UIColor clearColor] textAlignment:NSTextAlignmentCenter title:Localized(@"选择一个燥事")];
    [self.view addSubview:alertL];
}

#pragma mark ===== 点击事件
//点击标签
- (void)bgVClickWithBgView:(id)sender {
    UITapGestureRecognizer *tap = sender;
    
    FDProblemModel *model = self.dataArr[tap.view.tag];
    FDGoYouController *goyouVC = [[FDGoYouController alloc] init];
    goyouVC.mid = model.ID;
    [self.navigationController pushViewController:goyouVC animated:YES];
}

//点击换一批
- (void)changeBtnClick {
    for (UIView *subV in self.view.subviews) {
        if ([subV isKindOfClass:[DBSphereView class]]) {
            [subV removeFromSuperview];
        }
    }
    [self loadData];
}

#pragma mark ===== 功能
/** 在固定范围内获取随机数 */
- (int)getRandomNumber:(int)from to:(int)to {
    int tooo = to-from+1;
    int x = arc4random() % tooo;
    return (int)(from + from+x);
}

#pragma mark ===== 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

@end
