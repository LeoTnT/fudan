//
//  FDGoYouController.h
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

//乱七八糟(救燥扶伤) - 走你界面
@interface FDGoYouController : FDBaseController

/** 燥事ID */
@property (nonatomic, copy) NSString *mid;

@end
