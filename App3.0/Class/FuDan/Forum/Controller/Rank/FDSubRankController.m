//
//  FDSubRankController.m
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDSubRankController.h"
#import "FDRankTopView.h"
#import "FDRankListCell.h"
#import "FDPersonController.h"

@interface FDSubRankController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation FDSubRankController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = mainColor;
    [self setHidesBottomBarWhenPushed:YES];
    [self setUpTableView];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [FDRankListCell registerNibCellWithTableView:self.tableView];
    
    FDRankTopView *topV = [FDRankTopView initView];
    topV.frame = CGRectMake(0, 0, mainWidth, 230);
    kFDWeakSelf;
    topV.iconBlock = ^(NSString *userID) {
        FDPersonController *personVC =[[FDPersonController alloc] init];
        [weakSelf.navigationController pushViewController:personVC animated:YES];
    };
    
    self.tableView.tableHeaderView = topV;
}


#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDRankListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDRankListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDRankListCell identifier]];
    if (cell == nil) {
        cell = [[FDRankListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDRankListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    cell.rankLabel.text = [NSString stringWithFormat:@"%ld", indexPath.section+4];
    
    if (indexPath.section == 0) {//第一个
        [self setCornerInView:cell.bgView rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    }else if (indexPath.section == 4) {//最后一个
        [self setCornerInView:cell.bgView rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    }
    
    //点击头像
    kFDWeakSelf;
    cell.listIconBlock = ^{
        FDPersonController *personVC =[[FDPersonController alloc] init];
        [weakSelf.navigationController pushViewController:personVC animated:YES];
    };
    
    return cell;
}

- (void)setCornerInView:(UIView *)view rectCorner:(UIRectCorner)rectCorner{
    //切任意圆角
    CGRect frame = view.bounds;
    //
    frame.origin.y -=1;
    frame.size.height +=3;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:frame byRoundingCorners:rectCorner cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = frame;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-34-8-kStatusBarAndNavigationBarHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = mainColor;

        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
