//
//  FDPersonController.m
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPersonController.h"
#import "MFSideMenu.h"
#import "FDPersonTopView.h"
#import "FDMineListCell.h"
#import "FDForumModel.h"
#import "FDProblemModel.h"
#import "FDGoYouController.h"

@interface FDPersonController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FDPersonTopView *topV;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation FDPersonController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [self setUpTableView];
    [self setUpCustomNav];
    
    [self getProblemOther];
}

#pragma mark ===== 网络请求
- (void)getProblemOther {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"uid"] = self.uid;
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemOther parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDUserInfoModel *userInfo = [FDUserInfoModel mj_objectWithKeyValues:state.data[@"user_info"]];
            weakSelf.dataArr = [NSMutableArray arrayWithArray:[FDProblemListModel mj_objectArrayWithKeyValuesArray:state.data[@"drys"][@"data"]]];
            weakSelf.topV.userInfoModel = userInfo;
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDMineListCell registerNibCellWithTableView:self.tableView];
    
    self.topV = [FDPersonTopView initView];
    self.topV.frame = CGRectMake(0, 0, mainWidth, 310);
    self.tableView.tableHeaderView = self.topV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDMineListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDMineListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDMineListCell identifier]];
    if (cell == nil) {
        cell = [[FDMineListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDMineListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.listModel = self.dataArr[indexPath.section];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FDProblemListModel *model = self.dataArr[indexPath.section];
    FDGoYouController *goyouVC = [[FDGoYouController alloc] init];
    goyouVC.mid = model.ID;
    [self.navigationController pushViewController:goyouVC animated:YES];
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, kFDTabbarHeight, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
