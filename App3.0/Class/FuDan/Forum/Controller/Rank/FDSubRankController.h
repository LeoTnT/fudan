//
//  FDSubRankController.h
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//


#import "FDBaseController.h"

typedef NS_ENUM(NSUInteger, FDRankType) {
    FDRankTypeDay,
    FDRankTypeWeek,
    FDRankTypeTotal,
};

@interface FDSubRankController : FDBaseController
/** 列表类型 */
@property (nonatomic, assign) FDRankType type;

@end
