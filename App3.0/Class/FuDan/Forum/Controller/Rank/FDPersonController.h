//
//  FDPersonController.h
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

//个人主页
@interface FDPersonController : FDBaseController

@property (nonatomic, copy) NSString *uid;

@end
