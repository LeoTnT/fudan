//
//  LeftViewController.h
//  LCSideTabbar_Demo
//
//  Created by lichao on 16/4/8.
//  Copyright © 2016年 lichao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftController : UIViewController

- (void)getUserInfo;

- (void)getDryMessageCount;

@end
