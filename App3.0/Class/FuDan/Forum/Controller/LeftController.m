//
//  LeftViewController.m
//  LCSideTabbar_Demo
//
//  Created by lichao on 16/4/8.
//  Copyright © 2016年 lichao. All rights reserved.
//

#import "LeftController.h"
#import "FDLeftTopView.h"
#import "MFSideMenu.h"
#import "FDTabSecondController.h"//我燥
#import "FDSphereController.h"//他燥
#import "FDFriendController.h"//寻找燥友
#import "FDForumController.h"//燥事区
#import "FDRankController.h"//排行榜
#import "FDMineController.h"//我的
#import "FDBackController.h"//回去
#import "FDForumModel.h"
#import "FDIconChangeController.h"

@interface LeftController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) FDLeftTopView *topV;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *menuArr;
@property (nonatomic, strong) FDUserNumModel *numModel;

@end

@implementation LeftController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.menuArr = @[@{@"image":@"fd_wozao", @"name":@"我燥"},
                     @{@"image":@"fd_tazao", @"name":@"他燥"},
                     @{@"image":@"fd_find_zaoyou", @"name":@"寻找燥友"},
                     @{@"image":@"fd_zaoshiqu", @"name":@"燥事区"},
                     @{@"image":@"fd_zaoshen_rank", @"name":@"燥神排行榜"},
                     @{@"image":@"fd_wode", @"name":@"我的"},
                     @{@"image":@"fd_goback", @"name":@"回去"},
                     ];
                     
    [self setUpTableView];
//    [self getUserInfo];
//    [self getDryMessageCount];
}

#pragma mark ===== 网络请求
- (void)getUserInfo {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetUserInfo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDUserInfoModel *userInfo = [FDUserInfoModel mj_objectWithKeyValues:state.data];
            weakSelf.topV.userInfoModel = userInfo;
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)getDryMessageCount {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetDryMessageCount parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.numModel = [FDUserNumModel mj_objectWithKeyValues:state.data];
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    FDLeftTopView *topV = [FDLeftTopView initView];
    topV.frame = CGRectMake(0, 0, FontNum(213), 276);
    kFDWeakSelf;
    //点击头像
    topV.iconBlock = ^{
        FDIconChangeController *iconVC = [[FDIconChangeController alloc] init];
        iconVC.isLeft = YES;
        //设置成功回调
        iconVC.setIconBlock = ^{
            [weakSelf getUserInfo];
        };
        [weakSelf pushToMenuWithController:iconVC];
    };
    self.topV = topV;
    self.tableView.tableHeaderView = topV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor hexFloatColor:@"242424"];
    }

    
    NSDictionary *dic = self.menuArr[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:dic[@"image"]];
    cell.textLabel.text = dic[@"name"];
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {//我燥
        UILabel *badgeL = [self showBadgeLabelWithNumber:self.numModel.snum];
        [cell.contentView addSubview:badgeL];
    }else if (indexPath.row == 1) {//他燥
        UILabel *badgeL = [self showBadgeLabelWithNumber:self.numModel.pnum];
        [cell.contentView addSubview:badgeL];
    }
    
    return cell;
}

- (UILabel *)showBadgeLabelWithNumber:(NSString *)numString {
    if ([numString integerValue] <= 0) {
        return nil;
    }
    
    UILabel *badgeLabel = [[UILabel alloc] init];
    badgeLabel.backgroundColor = [UIColor hexFloatColor:@"F03C68"];
    badgeLabel.font = [UIFont systemFontOfSize:12];
    badgeLabel.textColor = [UIColor whiteColor];
    badgeLabel.textAlignment = NSTextAlignmentCenter;
    badgeLabel.layer.cornerRadius = 18/2;
    badgeLabel.layer.masksToBounds = YES;
   
    CGFloat width;
    //小红点显示
    if ([numString integerValue] > 99) {
        width = 25;
        badgeLabel.text = @"99+";
    }else {
        width = 18;
        badgeLabel.text = numString;
    }
    badgeLabel.frame = CGRectMake(FontNum(213)-25-18, (51-18)/2, width, 18);
    
    return badgeLabel;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {//我燥
        FDTabSecondController *wzVC = [[FDTabSecondController alloc] init];
        [self pushToMenuWithController:wzVC];
    }else if (indexPath.row == 1) {//他燥
        FDSphereController *tzVC = [[FDSphereController alloc] init];
        [self pushToMenuWithController:tzVC];
    }else if (indexPath.row == 2) {//寻找燥友
        FDFriendController *findVC = [[FDFriendController alloc] init];
        [self pushToMenuWithController:findVC];
    }else if (indexPath.row == 3) {//燥事区
        FDForumController *zsVC = [[FDForumController alloc] init];
        [self pushToMenuWithController:zsVC];
    }else if (indexPath.row == 4) {//燥神排行榜
        FDRankController *rankVC = [[FDRankController alloc] init];
        [self pushToMenuWithController:rankVC];
    }else if (indexPath.row == 5) {//我的
        FDMineController *mineVC = [[FDMineController alloc] init];
        [self pushToMenuWithController:mineVC];
    }else if (indexPath.row == 6) {//回去
        FDBackController *backVC = [[FDBackController alloc] init];
        [self pushToMenuWithController:backVC];
    }
}

//点击菜单跳转
- (void)pushToMenuWithController:(id)controller {
    
    UITabBarController *tabBarController = self.menuContainerViewController.centerViewController;
    UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
    NSArray *controllers = [NSArray arrayWithObject:controller];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, FontNum(213), mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, kFDTabbarHeight, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


@end
