//
//  FDTabSecondController.m
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDTabSecondController.h"
#import "MFSideMenu.h"
#import "FDMyDryTopView.h"
#import "FDHotForumView.h"
#import "FDTagsFrame.h"

#import "FDForumModel.h"
#import "FDProblemModel.h"
#import "FDGoYouController.h"
#import "FDMineController.h"
#import "FDSphereController.h"//他燥
#import "FDIconChangeController.h"

@interface FDTabSecondController ()<UITableViewDelegate, UITableViewDataSource> 

@property (nonatomic, strong) FDMyDryTopView *headerV;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) FDTagsFrame *tagsFrame;
//标签model数组
@property (nonatomic, strong) NSMutableArray *dataArr;
//标签标题数组
@property (nonatomic, strong) NSMutableArray *tagsArr;

@end

@implementation FDTabSecondController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUserInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    [self setHidesBottomBarWhenPushed:NO];
    [self setUpTableView];
    [self setUpCustomNav];
    
    [self loadData];
}

#pragma mark ===== 网络请求
//发布燥事
- (void)pushMessage {
    
    if (self.headerV.titleTextF.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请输入燥事主题!")];
        return;
    }
    if (self.headerV.contentTextV.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请输入燥事内容!")];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"title"] = self.headerV.titleTextF.text;
    params[@"content"] = self.headerV.contentTextV.text;
    
    kFDWeakSelf;
    [XSTool showProgressHUDWithView:self.view];
    [XSHTTPManager post:FD_GetRelease parameters:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        if (state.status) {
            [XSTool showToastWithView:weakSelf.view Text:Localized(@"发布成功")];
            /*延迟执行时间*/
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                FDMineController *mineVC = [[FDMineController alloc] init];
                [weakSelf.navigationController pushViewController:mineVC animated:YES];
            });
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//个人信息
- (void)getUserInfo {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetUserInfo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDUserInfoModel *userInfo = [FDUserInfoModel mj_objectWithKeyValues:state.data];
            weakSelf.headerV.userInfo = userInfo;
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//加载标签
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"order"] = @"num";//随机rand|热门num
    params[@"is_short"] = @"1";//默认0|1(是否短标题)
    params[@"limit"] = @"9";//数据个数
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemMore parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            weakSelf.dataArr = [NSMutableArray arrayWithArray:[FDProblemModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            
            weakSelf.tagsArr = [NSMutableArray array];
            for (FDProblemModel *model in weakSelf.dataArr) {
                NSString *title = model.title_short;
                [weakSelf.tagsArr addObject:title];
            }
            
            weakSelf.tagsFrame = [[FDTagsFrame alloc] init];
            weakSelf.tagsFrame.tagsArray = weakSelf.tagsArr;
          
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_nav_menu" block:^{
        if (weakSelf.isPresent) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }else {
            [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        }
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    FDMyDryTopView *headerV = [FDMyDryTopView initView];
    headerV.frame = CGRectMake(0, 0, mainWidth, 598);
    kFDWeakSelf;
    //点击头像
    headerV.iconBlock = ^{
        FDIconChangeController *iconVC = [[FDIconChangeController alloc] init];
        //设置成功回调
        iconVC.setIconBlock = ^{
            [weakSelf getUserInfo];
        };
        [weakSelf.navigationController pushViewController:iconVC animated:YES];
    };
    //点击走你
    headerV.pushBlock = ^{
        [weakSelf pushMessage];
    };
    
    self.headerV = headerV;
    self.tableView.tableHeaderView = headerV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.tagsFrame.tagsHeight+87;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    FDHotForumView *topV = [[FDHotForumView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.tagsFrame.tagsHeight)];
    topV.tagsFrame = self.tagsFrame;
    topV.tagsArr = self.tagsArr;
    
    kFDWeakSelf;
    //点击更多
    topV.moreBtnBlock = ^{
        FDSphereController *tzVC = [[FDSphereController alloc] init];
        [weakSelf.navigationController pushViewController:tzVC animated:YES];
    };
    //点击标签
    topV.hotBtnBlock = ^(UIButton *button) {
        FDProblemModel *model = weakSelf.dataArr[button.tag];
        FDGoYouController *goyouVC = [[FDGoYouController alloc] init];
        goyouVC.mid = model.ID;
        [self.navigationController pushViewController:goyouVC animated:YES];
    };
    
    return topV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-kFDTabbarHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


@end
