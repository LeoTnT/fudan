//
//  FDBackController.m
//  App3.0
//
//  Created by lichao on 2018/10/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBackController.h"
#import "MFSideMenu.h"
#import "LCTabbarController.h"
#import "LeftController.h"

@interface FDBackController ()

@end

@implementation FDBackController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = mainColor;
    [self setHidesBottomBarWhenPushed:YES];
    [self setUpCustomNav];
    [self setUpUI];
}

- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        //点击返回
        [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    }];
}

- (void)setUpUI {
    UIImageView *bgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_back_bg"]];
    [self.view addSubview:bgV];
    CGFloat wh = mainWidth-25*2;
    [bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(89);
        make.centerX.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(wh, wh));
    }];
    
    UIImageView *titleImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fd_back_title"]];
    [self.view addSubview:titleImageV];
    [titleImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgV).offset(69);
        make.left.mas_equalTo(bgV).offset(77);
        make.right.mas_equalTo(bgV).offset(-62);
        make.height.mas_equalTo(66);
    }];
    
    UIButton *goButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [goButton setTitle:Localized(@"去燥星球") forState:UIControlStateNormal];
    [goButton setTitleColor:mainColor forState:UIControlStateNormal];
    [goButton setBackgroundColor:[UIColor whiteColor]];
    goButton.layer.cornerRadius = 49/2;
    goButton.layer.masksToBounds = YES;
    [goButton addTarget:self action:@selector(goButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:goButton];
    [goButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgV.mas_bottom).offset(21);
        make.centerX.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(180, 49));
    }];
}

#pragma mark ===== 点击按钮
- (void)goButtonClick {
    UIWindow *window= [UIApplication sharedApplication].keyWindow;
    LCTabbarController *TabVC = [[LCTabbarController alloc] init];
    LeftController *leftVC = [[LeftController alloc] init];
    MFSideMenuContainerViewController *containerVC = [MFSideMenuContainerViewController containerWithCenterViewController:TabVC leftMenuViewController:leftVC rightMenuViewController:nil];
    containerVC.menuSlideAnimationEnabled = NO;
    [containerVC setLeftMenuWidth:FontNum(213)];//设置左滑视图宽度
    [containerVC setPanMode:MFSideMenuPanModeSideMenu];
    window.rootViewController = containerVC;
}

@end
