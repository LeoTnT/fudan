//
//  FDGoYouController.m
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDWayController.h"
#import "FDWayTopView.h"
#import "FDWayCommentCell.h"
#import "FDWaySubCommentCell.h"
#import "FDWayFooterView.h"
#import "FDProblemModel.h"
#import "FDPersonController.h"

@interface FDWayController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, weak) FDWayTopView *topV;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL isOpen;

@property (nonatomic, strong) FDProblemInfoModel *infoModel;
@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) FDProblemReplyListModel *replyModel;//回复model

@property (nonatomic, weak) UIView *inputBgView;//输入框背景
@property (nonatomic, weak) UITextField *textField;//回复输入框

@end

@implementation FDWayController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = mainColor;
    
    self.isOpen = YES;
    
    [self setUpTableView];
    [self setUpCustomNav];
    [self setUpInputV];
    //默认隐藏输入框
    self.inputBgView.hidden = YES;
    
    [self beginRefreshTableView];
}

#pragma mark ===== 网络请求
- (void)loadData {
    self.page = 1;
    [self loadMoreData];
}

- (void)loadMoreData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mid"] = self.mid;//燥事ID
    params[@"type"] = self.type;//区分我燥wo|他燥ta
    params[@"page"] = @(self.page);
//    params[@"is_get"] = @"1";//是否只获取良方|默认1
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemInfo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            weakSelf.infoModel = [FDProblemInfoModel mj_objectWithKeyValues:state.data];
            weakSelf.topV.topModel = weakSelf.infoModel.message;
            weakSelf.dataArr = [NSMutableArray arrayWithArray:weakSelf.infoModel.reply.data];
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshTableView];
    } failure:^(NSError *error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//回复消息
- (void)replyMessage {
    if (self.textField.text.length <= 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请输入回复内容!")];
        return;
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mid"] = self.replyModel.mid;//燥事ID
    params[@"rid"] = self.replyModel.ID;//回复ID|评论时默认0
    params[@"content"] = self.textField.text;//回复内容
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetReply parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            weakSelf.textField.text = @"";
            [XSTool showToastWithView:weakSelf.view Text:@"发布成功"];
            [weakSelf loadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:self.navTitle textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDWayCommentCell registerNibCellWithTableView:self.tableView];
    [FDWaySubCommentCell registerNibCellWithTableView:self.tableView];
    
    FDWayTopView *topV = [FDWayTopView initView];
    kFDWeakSelf;
    topV.checkBlock = ^(UIImageView *imageV) {
        weakSelf.isOpen = !weakSelf.isOpen;
        imageV.image = weakSelf.isOpen ? [UIImage imageNamed:@"fd_list_up"] : [UIImage imageNamed:@"fd_list_down"];
        [weakSelf.tableView reloadData];
    };
    topV.frame = CGRectMake(0, 0, mainWidth, 318);
    self.topV = topV;
    self.tableView.tableHeaderView = topV;
}

- (void)setUpInputV {
    //输入框
    UIView *bgV = [[UIView alloc] init];
    bgV.backgroundColor = [UIColor whiteColor];
    self.inputBgView = bgV;
    [self.view addSubview:bgV];
    [bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(46);
    }];
    
    //输入框
    UIView *inputV = [[UIView alloc] init];
    inputV.backgroundColor = [UIColor hexFloatColor:@"F4F4F4"];
    inputV.layer.cornerRadius = 17;
    inputV.layer.masksToBounds = YES;
    [bgV addSubview:inputV];
    [inputV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(bgV);
        make.left.mas_equalTo(18);
        make.right.mas_equalTo(-18);
        make.height.mas_equalTo(34);
    }];
    
    //搜索输入框
    UITextField *textField = [[UITextField alloc] init];
    textField.font = [UIFont systemFontOfSize:14];
    textField.textColor = [UIColor hexFloatColor:@"C0C0C0"];
    textField.backgroundColor = [UIColor hexFloatColor:@"EFEFEF"];
    textField.placeholder = Localized(@"写评论...");
    textField.textAlignment = NSTextAlignmentLeft;
    textField.returnKeyType = UIReturnKeySend;
    textField.delegate = self;
    self.textField = textField;
    [inputV addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(inputV);
        make.left.mas_equalTo(17);
        make.right.mas_equalTo(-17);
        make.height.mas_equalTo(34);
    }];
}

#pragma mark ===== UITextFieldDelegate
//点击发送
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    //回复接口 == 暂做测试
    [self replyMessage];
    self.inputBgView.hidden = YES;
    [textField endEditing:YES];
    return YES;
}

//编辑完成
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField endEditing:YES];
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isOpen) {
        return self.dataArr.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    FDProblemReplyListModel *model = self.dataArr[section];
    NSArray *commentArr = model.reply;
    return commentArr.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 54;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    FDWayFooterView *footerV = [FDWayFooterView initView];
    footerV.frame = CGRectMake(0, 0, mainWidth, 54);
    
    kFDWeakSelf;
    //点击回复
    footerV.replyBlock = ^{
        weakSelf.inputBgView.hidden = NO;
        weakSelf.replyModel = nil;
        weakSelf.replyModel = self.dataArr[section];
        [weakSelf.textField becomeFirstResponder];
    };
    
    return footerV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        FDWayCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDWayCommentCell identifier]];
        if (cell == nil) {
            cell = [[FDWayCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDWayCommentCell identifier]];
        }
        //点击不变色
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        FDProblemReplyListModel *model = self.dataArr[indexPath.section];
        cell.listmodel = model;
        kFDWeakSelf;
        //点击头像
        cell.iconBlock = ^{
            FDPersonController *personVC =[[FDPersonController alloc] init];
            personVC.uid = model.uid;
            [weakSelf.navigationController pushViewController:personVC animated:YES];
        };
        //点击谈天说地
        cell.talkBlock = ^{
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:model.uid type:EMConversationTypeChat createIfNotExist:YES];
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = model.nickname;
            chatVC.avatarUrl = model.logo;
            [self.navigationController pushViewController:chatVC animated:YES];
        };
        
        return cell;
    }else {
        FDWaySubCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDWaySubCommentCell identifier]];
        if (cell == nil) {
            cell = [[FDWaySubCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDWaySubCommentCell identifier]];
        }
        //点击不变色
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        FDProblemReplyListModel *model = self.dataArr[indexPath.section];
        FDProblemReplyListModel *subModel = model.reply[indexPath.row-1];
        cell.subModel = subModel;
        
        if (indexPath.row == 1) {
            cell.topConstraint.constant = 12;
        }else {
            cell.topConstraint.constant = 5;
        }
        
        if (indexPath.row == model.reply.count) {//最后一个回复
            cell.bottomConstraint.constant = 12;
        }else {
            cell.bottomConstraint.constant = 0;
        }
     
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= 1) {//点击回复评论
        //获取该条回复的model
        FDProblemReplyListModel *model = self.dataArr[indexPath.section];
        FDProblemReplyListModel *subModel = model.reply[indexPath.row-1];
        
        self.inputBgView.hidden = NO;
        self.replyModel = nil;
        self.replyModel = subModel;;
        [self.textField becomeFirstResponder];
    }
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        
        //下拉刷新
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf loadData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 200;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
