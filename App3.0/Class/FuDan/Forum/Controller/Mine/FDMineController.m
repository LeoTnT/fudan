//
//  FDMineController.m
//  App3.0
//
//  Created by lichao on 2018/10/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMineController.h"
#import "MFSideMenu.h"
#import "FDMineTopView.h"
#import "FDMineListCell.h"
#import "FDProblemModel.h"

#import "FDWayController.h"
#import "FDIconChangeController.h"

@interface FDMineController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) FDMineTopView *headerV;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *woDataArr;
@property (nonatomic, strong) NSMutableArray *taDataArr;

@property (nonatomic, assign) NSInteger page;
//我燥/他燥
@property (nonatomic, assign) BOOL isMe;

@end

@implementation FDMineController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setHidesBottomBarWhenPushed:YES];

    [self setUpTableView];
    [self setUpCustomNav];
    
    self.isMe = YES;
    [self beginRefreshTableView];
}

#pragma mark ===== 网络请求
- (void)getUserInfo {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetUserInfo parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            FDUserInfoModel *userInfo = [FDUserInfoModel mj_objectWithKeyValues:state.data];
            weakSelf.headerV.userInfoModel = userInfo;
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

- (void)loadData {
    self.page = 1;
    [self loadMoreData];
}

- (void)loadMoreData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = @(self.page);
    params[@"type"] = self.isMe ? @"wo" : @"ta";//区分我燥wo|他燥ta
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemList parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArr = state.data[@"data"];
            if (weakSelf.page == 1) {
                if (weakSelf.isMe) {
                    weakSelf.woDataArr = [NSMutableArray arrayWithArray:[FDProblemListModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }else {
                    weakSelf.taDataArr = [NSMutableArray arrayWithArray:[FDProblemListModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }
            }else {
                if (weakSelf.isMe) {
                    [weakSelf.woDataArr addObjectsFromArray:[FDProblemListModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }else {
                    [weakSelf.taDataArr addObjectsFromArray:[FDProblemListModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }
            }
            [weakSelf.tableView reloadData];
            if (tempArr.count > 0) {
                weakSelf.page++;
            }
      
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshTableView];
    } failure:^(NSError *error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setLeftBtnWithImageName:@"fd_nav_menu" block:^{
        //点击头像滑到左侧
        [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [FDMineListCell registerNibCellWithTableView:self.tableView];
    
    FDMineTopView *headerV = [FDMineTopView initView];
    headerV.frame = CGRectMake(0, 0, mainWidth, 360);
    kFDWeakSelf;
    //点击头像
    headerV.iconBlock = ^{
        FDIconChangeController *iconVC = [[FDIconChangeController alloc] init];
        //设置成功回调
        iconVC.setIconBlock = ^{
            [weakSelf getUserInfo];
        };
        [weakSelf.navigationController pushViewController:iconVC animated:YES];
    };
    
    //点击 我燥/他燥
    headerV.menuBlock = ^(BOOL isMe) {
        weakSelf.isMe = isMe;
        if (isMe) {//我燥
            if (weakSelf.woDataArr.count > 0) {
                [weakSelf.tableView reloadData];
            }else {
                [weakSelf loadData];
            }
        }else{//我燥
            if (weakSelf.taDataArr.count > 0) {
                [weakSelf.tableView reloadData];
            }else {
                [weakSelf loadData];
            }
        }
    };
    self.headerV = headerV;
    self.tableView.tableHeaderView = headerV;
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isMe) {
        return self.woDataArr.count;
    }
    return self.taDataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDMineListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDMineListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDMineListCell identifier]];
    if (cell == nil) {
        cell = [[FDMineListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDMineListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    if (self.isMe) {
        FDProblemListModel *model = self.woDataArr[indexPath.section];
        cell.listModel = model;
    }else {
        FDProblemListModel *model = self.taDataArr[indexPath.section];
        cell.listModel = model;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FDWayController *wayVC = [[FDWayController alloc] init];
    if (self.isMe) {
        FDProblemListModel *model = self.woDataArr[indexPath.section];
        wayVC.mid = model.ID;
        wayVC.navTitle = Localized(@"我的乱七八燥");
        wayVC.type = @"wo";
    }else {
        FDProblemListModel *model = self.taDataArr[indexPath.section];
        wayVC.mid = model.ID;
        wayVC.navTitle = Localized(@"我的去燥良方");
        wayVC.type = @"ta";
    }
    [self.navigationController pushViewController:wayVC animated:YES];
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (NSMutableArray *)woDataArr {
    if (!_woDataArr) {
        _woDataArr = [NSMutableArray array];
    }
    return _woDataArr;
}

- (NSMutableArray *)taDataArr {
    if (!_taDataArr) {
        _taDataArr = [NSMutableArray array];
    }
    return _taDataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        //下拉刷新
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf getUserInfo];
            [weakSelf loadData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
