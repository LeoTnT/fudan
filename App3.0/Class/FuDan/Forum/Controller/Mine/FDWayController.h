//
//  FDGoYouController.h
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

//我的-乱七八燥
@interface FDWayController : FDBaseController

/** 标题 */
@property (nonatomic, copy) NSString *navTitle;
/** 燥事ID */
@property (nonatomic, copy) NSString *mid;
/** 区分我燥wo|他燥ta */
@property (nonatomic, copy) NSString *type;

@end
