
//
//  FDIconChangeController.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDIconChangeController.h"
#import "FDIconCell.h"
#import "FDIconFooterView.h"
#import "FDForumModel.h"
#import "MFSideMenu.h"

@interface FDIconChangeController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(strong, nonatomic) UICollectionViewFlowLayout *layout;
@property(strong, nonatomic) UICollectionView *collectionView;
@property(strong, nonatomic) NSMutableArray *dataArr;
//当前选中的
@property (nonatomic, strong) NSIndexPath *selectIndexPath;

@end

@implementation FDIconChangeController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = mainColor;
    [self setHidesBottomBarWhenPushed:YES];
    [FDIconCell registerNibCellWithCollectionView:self.collectionView];
    [FDIconFooterView registerNibFooterViewlWithCollectionView:self.collectionView];
    
    [self setUpCustomNav];
    [self loadData];
}

#pragma mark ===== 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetUserImgGet parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            weakSelf.dataArr = [FDIconModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [weakSelf.collectionView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//设置头像
- (void)submitUserImg {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    FDIconModel *model = self.dataArr[self.selectIndexPath.item];
    params[@"id"] = model.ID;
    
    kFDWeakSelf;
    [XSTool showProgressHUDWithView:self.view];
    [XSHTTPManager post:FD_GetUserImgSet parameters:params success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        if (state.status) {
            [XSTool showToastWithView:weakSelf.view Text:Localized(@"设置成功")];
            if (self.setIconBlock) {
                self.setIconBlock();
            }
            /*延迟执行时间*/
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                if (weakSelf.isLeft) {
                    [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
                }else {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            });
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool hideProgressHUDWithView:weakSelf.view];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== UI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:Localized(@"修改头像") textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        if (weakSelf.isLeft) {
            [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        }else {
            [weakSelf.navigationController popViewControllerAnimated:YES];            
        }
    }];
}

#pragma mark ===== <UICollectionViewDelegate>
//section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//item的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}

//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (mainWidth-28*2-22*2) / 3;
    CGFloat height = width+12;
    
    return CGSizeMake(width, height);
}

//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 15;
}

//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(mainWidth, 49);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        FDIconFooterView *footerV = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[FDIconFooterView identifier] forIndexPath:indexPath];
        kFDWeakSelf;
        //点击"设为头像"
        footerV.submitBlock = ^{
            [weakSelf submitUserImg];
        };
        return footerV;
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FDIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FDIconCell identifier] forIndexPath:indexPath];
    
    FDIconModel *model = self.dataArr[indexPath.item];
    cell.iconModel = model;
    if ([model.is_default integerValue] == 1) {//选中当前的头像
        self.selectIndexPath = nil;
        self.selectIndexPath = indexPath;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectIndexPath) {
        //取消上一个头像的选中
        FDIconCell *cell = (FDIconCell *)[collectionView cellForItemAtIndexPath:self.selectIndexPath];
        cell.isDefaultImageV.hidden = YES;
        
        self.selectIndexPath = nil;
    }
    self.selectIndexPath = indexPath;
    
    //选中当前头像
    FDIconCell *cell = (FDIconCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.isDefaultImageV.hidden = NO;
}

#pragma mark -- 懒加载
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        //创建UI布局
        self.layout = [[UICollectionViewFlowLayout alloc] init];
        self.layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        CGRect frame = self.view.bounds;
        frame.origin.y += kStatusBarAndNavigationBarHeight+18;
        frame.size.height -= (kStatusBarAndNavigationBarHeight+18);
        
        self.layout.sectionInset = UIEdgeInsetsMake(37, 28, 15, 28);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:self.layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.scrollEnabled = YES;
        _collectionView.alwaysBounceVertical = YES;//一直允许滑动
        _collectionView.showsVerticalScrollIndicator = NO;//关闭滑动指示条
        
        //切任意圆角(只切上面两个)
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_collectionView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = _collectionView.bounds;
        maskLayer.path = maskPath.CGPath;
        _collectionView.layer.mask = maskLayer;
        
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

@end
