//
//  FDIconChangeController.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

typedef void(^setIconBlock)(void);
//修改头像
@interface FDIconChangeController : FDBaseController

@property (nonatomic, assign) BOOL isLeft;

@property (nonatomic, copy) setIconBlock setIconBlock;

@end
