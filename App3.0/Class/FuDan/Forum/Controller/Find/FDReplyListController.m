//
//  FDSubFriendController.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDReplyListController.h"
#import "FDReplyListCell.h"
#import "FDBaseLeftView.h"
#import "FDPersonController.h"

#import "FDCustomTableView.h"//列表
#import "ContactListSelectViewController.h"
#import "ChatViewController.h"

@interface FDReplyListController ()<UITableViewDelegate, UITableViewDataSource, FDLeftViewDelegate>

@property (nonatomic, strong) UIView *networkStateView;

@property (nonatomic, weak) FDBaseLeftView *leftV;
//上一个选中的按钮
@property (nonatomic, strong) UIButton *lastButton;

@property (nonatomic, strong) FDCustomTableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArr1;//距离
@property (nonatomic, strong) NSMutableArray *dataArr2;//邀请
@property (nonatomic, strong) NSMutableArray *dataArr3;//建群

@property (nonatomic, assign) NSInteger page;
/** 1-2-3 */
@property (nonatomic, copy) NSString *orderType;

@end

@implementation FDReplyListController

- (void)isConnect:(BOOL)isConnect{
    if (!isConnect) {
        self.tableView.tableHeaderView = self.networkStateView;
    }else {
        self.tableView.tableHeaderView = nil;
    }
}

- (void)networkChanged:(EMConnectionState)connectionState {
    if (connectionState == EMConnectionDisconnected) {
        self.tableView.tableHeaderView = self.networkStateView;
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        if (window) {
            
            [MBProgressHUD showMessage:@"网络已断开,请检查您的网络" view:window];
        }
    }
    else{
        self.tableView.tableHeaderView = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置chatHelper
    [ChatHelper shareHelper].replyVC = self;
    
    [self setHidesBottomBarWhenPushed:YES];
    [self showBackgroudViewWithImageName:@"fd_blue_bg"];
    
    [self setUpTableView];
    [self setUpLeftView];
}

#pragma mark ===== 网络请求
// 刷新消息列表
- (void)refreshConversationLists {
//    unreadCount = 0;
    [self.dataArr1 removeAllObjects];
    [self.dataArr2 removeAllObjects];
    [self.dataArr3 removeAllObjects];

    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSArray* sorted = [conversations sortedArrayUsingComparator:
                       ^(EMConversation *obj1, EMConversation* obj2){
                           EMMessage *message1 = [obj1 latestMessage];
                           EMMessage *message2 = [obj2 latestMessage];
                           if(message1.timestamp > message2.timestamp) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    for (EMConversation *conversation in sorted) {
        if (conversation.latestMessage == nil) {
            continue;
        }
        
        // 累加消息未读数
//        unreadCount += conversation.unreadMessagesCount;
        
        NSLog(@"conversation:%@-%d-%d",conversation.conversationId,conversation.unreadMessagesCount,conversation.type);
        ConversationModel *model = [[ConversationModel alloc] initWithConversation:conversation];
        
        if (conversation.type == EMConversationTypeChat) {//单聊
            NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:conversation.conversationId];
            if (contacts != nil && contacts.count > 0) {
                // 本地数据库存在
                ContactDataParser *contact = contacts[0];
                model.title = [contact getName];
                model.avatarURLPath = contact.avatar;
                if ([model.relation integerValue] == 1) {
                    [self.dataArr2 addObject:model];
                }else {//非好友会话列表
                    [self.dataArr3 addObject:model];
                }
            } else {
                // 本地数据库不存在
                [HTTPManager getUserInfoWithUid:conversation.conversationId success:^(NSDictionary * dic, resultObject *state) {
                    UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
                    if (state.status) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                            
                            // 会话对象是机器人特殊处理
                            if ([conversation.conversationId isEqualToString:@"robot"]) {
                                cdParser.username = Localized(@"系统消息");
                                cdParser.remark = Localized(@"系统消息");
                                cdParser.nickname = Localized(@"系统消息");
                                model.title = Localized(@"系统消息");
                                
                            } else {
                                cdParser.username = parser.data.username;
                                cdParser.remark = parser.data.remark;
                                cdParser.nickname = parser.data.nickname;
                                model.title = [parser.data getName];
                            }
                            cdParser.uid = conversation.conversationId;
                            cdParser.avatar = parser.data.logo;
                            cdParser.relation = parser.data.relation;
                            cdParser.mobile = parser.data.mobile;
                            
                            
                            model.avatarURLPath = parser.data.logo;
                            
                            // 防止同一时间刷新两次列表造成数据重复添加
                            NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                            if (contactT.count == 0) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if ([parser.data.relation integerValue] == 1) {//好友关系
                                        [self.dataArr2 addObject:model];
                                    }else {//非好友会话列表
                                        [self.dataArr3 addObject:model];
                                    }
                                    [self.tableView reloadData];
                                });
                            }
                            
                            // 添加数据库操作放在最后
                            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                        });
                        
                    }
                } fail:^(NSError * _Nonnull error) {
                    
                }];
            }
            
        } else if (conversation.type == EMConversationTypeGroupChat) {//群聊
            NSArray *groupDBArray = [[DBHandler sharedInstance] getGroupByGroupId:conversation.conversationId];
            if (groupDBArray != nil && groupDBArray.count > 0) {
                GroupDataModel *parser = groupDBArray[0];
                model.title = parser.name;
                model.avatarURLPath = parser.avatar;
                [self.dataArr1 addObject:model];
            } else {
                [HTTPManager fetchGroupInfoWithGroupId:conversation.conversationId getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
                    GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
                    if (state.status) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                            model.title = parser.data.name;
                            model.avatarURLPath = parser.data.avatar;
                            
                            // 防止同一时间刷新两次列表造成数据重复添加
                            NSArray *groupT = [[DBHandler sharedInstance] getGroupByGroupId:parser.data.gid];
                            if (groupT.count == 0) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.dataArr1 addObject:model];
                                    [self.tableView reloadData];
                                });
                            }
                            
                            // 添加数据库操作放在最后
                            [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
                        });
                        
                    }
                } fail:^(NSError * _Nonnull error) {
                    
                }];
            }
        }
    }
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [FDReplyListCell registerNibCellWithTableView:self.tableView];
}

- (void)setUpLeftView {
    FDBaseLeftView *leftV = [FDBaseLeftView initView];
    
    leftV.backgroundColor = [UIColor clearColor];
    leftV.delegate = self;
    self.leftV = leftV;
    //匹配列表
    [self setMenuButtonWithTitle1:@"群聊" title2:@"好友" title3:@"其他"];
    
    /****************** 默认选中第一个 ******************/
    leftV.firstButton.selected = YES;
    
    if (self.lastButton && self.lastButton != leftV.firstButton) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = leftV.firstButton;
    self.orderType = @"1";
    [self refreshConversationLists];
    /****************** 默认选中 ******************/
    
    [self.view addSubview:leftV];
    [leftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tableView);
        make.bottom.mas_equalTo(self.tableView);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(96);
    }];
    [self.view bringSubviewToFront:leftV];
}

#pragma mark ===== <FDLeftViewDelegate>
- (void)menuButtonClick:(UIButton *)button {
    if (self.lastButton != button) {
        button.selected = !button.selected;
    }
    
    if (self.lastButton && self.lastButton != button) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = button;
    
    switch (button.tag) {
        case 1001: {//群聊
            self.orderType = @"1";
        }
            break;
        case 1002: {//好友
            self.orderType = @"2";
        }
            break;
        case 1003: {//其他
            self.orderType = @"3";
        }
            break;
    }
    [self.tableView reloadData];
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.orderType integerValue] == 1) {//群聊
        return self.dataArr1.count;
    }else if ([self.orderType integerValue] == 2) {//好友
        return self.dataArr2.count;
    }else {//其他
        return self.dataArr3.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [FDReplyListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDReplyListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDReplyListCell identifier]];
    if (cell == nil) {
        cell = [[FDReplyListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDReplyListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    if ([self.orderType integerValue] == 1) {//群聊
        cell.conversationModel = self.dataArr1[indexPath.section];
    }else if ([self.orderType integerValue] == 2){//好友
        cell.conversationModel = self.dataArr2[indexPath.section];
    }else {//其他
        cell.conversationModel = self.dataArr3[indexPath.section];
    }
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ConversationModel *model;
    if ([self.orderType integerValue] == 1) {//群聊
        model = self.dataArr1[indexPath.section];
    }else if ([self.orderType integerValue] == 2) {//好友
        model = self.dataArr2[indexPath.section];
    }else {//其他
        model = self.dataArr3[indexPath.section];
    }
    
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:model.conversation];
    chatVC.title = model.title;
    chatVC.avatarUrl = model.avatarURLPath;
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark ===== 功能
- (NSArray *)sortedArray:(NSArray *)array {
    NSArray* sorted = [array sortedArrayUsingComparator:^NSComparisonResult(GroupDataModel *obj1, GroupDataModel *obj2) {
        if (NSOrderedDescending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (NSOrderedAscending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return sorted;
}

//自定义button
- (void)setMenuButtonWithTitle1:(NSString *)title1 title2:(NSString *)title2 title3:(NSString *)title3 {
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateNormal];
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateSelected];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateNormal];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateSelected];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateNormal];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateSelected];
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (NSMutableArray *)dataArr1 {
    if (!_dataArr1) {
        _dataArr1 = [NSMutableArray array];
    }
    return _dataArr1;
}

- (NSMutableArray *)dataArr2 {
    if (!_dataArr2) {
        _dataArr2 = [NSMutableArray array];
    }
    return _dataArr2;
}

- (NSMutableArray *)dataArr3 {
    if (!_dataArr3) {
        _dataArr3 = [NSMutableArray array];
    }
    return _dataArr3;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[FDCustomTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.opaque = NO;
        _tableView.rowHeight = 63;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(29);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_equalTo(63*7);
        }];
    }
    return _tableView;
}

- (UIView *)networkStateView {
    if (_networkStateView == nil) {
        _networkStateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
        _networkStateView.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:199 / 255.0 blue:199 / 255.0 alpha:0.5];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (_networkStateView.frame.size.height - 20) / 2, 20, 20)];
        imageView.image = [UIImage imageNamed:@"messageSendFail"];
        [_networkStateView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 5, 0, _networkStateView.frame.size.width - (CGRectGetMaxX(imageView.frame) + 15), _networkStateView.frame.size.height)];
        label.font = [UIFont systemFontOfSize:15.0];
        label.textColor = [UIColor grayColor];
        label.backgroundColor = [UIColor clearColor];
        label.text = @"网络连接失败，请检查网络设置";
        [_networkStateView addSubview:label];
    }
    
    return _networkStateView;
}

@end
