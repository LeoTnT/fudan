//
//  FDSubFriendController.h
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

@interface FDReplyListController : FDBaseController

- (void)refreshConversationLists;
- (void)isConnect:(BOOL)isConnect;
- (void)networkChanged:(EMConnectionState)connectionState;

@end
