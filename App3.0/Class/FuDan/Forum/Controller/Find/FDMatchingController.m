//
//  FDSubFriendController.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMatchingController.h"
#import "FDFriendListCell.h"
#import "FDBaseLeftView.h"
#import "FDFriendModel.h"
#import "FDFindModel.h"

#import "FDCustomTableView.h"//列表
#import "FDLocationManager.h"//定位
#import "ChatViewController.h"//聊天界面

@interface FDMatchingController ()<UITableViewDelegate, UITableViewDataSource, FDLeftViewDelegate, FDLocationDelegate>

@property (nonatomic, weak) FDBaseLeftView *leftV;
//上一个选中的按钮
@property (nonatomic, strong) UIButton *lastButton;

@property (nonatomic, strong) FDCustomTableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArr1;
@property (nonatomic, strong) NSMutableArray *dataArr2;
@property (nonatomic, strong) NSMutableArray *dataArr3;

@property (nonatomic, assign) NSInteger page;
/** 列表类型w_time最新|num热门 */
@property (nonatomic, copy) NSString *orderType;

@property (nonatomic, strong) FDLocationManager *FDLocationManager;
/* 定位权限是否开启 */
@property (nonatomic, assign) BOOL isOpen;
/*纬度*/
@property (nonatomic, copy) NSString *lat;
/*经度*/
@property (nonatomic, copy) NSString *lng;

@end

@implementation FDMatchingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setHidesBottomBarWhenPushed:YES];
    [self showBackgroudViewWithImageName:@"fd_blue_bg"];

    [self setUpTableView];
    [self setUpLeftView];

    //获取定位信息
    [self.FDLocationManager beginUpdatingLocation];
}

#pragma mark ===== 网络请求
/* 加载新数据 */
- (void)loadGetUsers{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"lat"] = self.lat;//经度
    params[@"lng"] = self.lng;//纬度
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetNearbyUsers parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArr = [FDNearbyPeopleModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [weakSelf.dataArr1 removeAllObjects];
            weakSelf.dataArr1 = [NSMutableArray arrayWithArray:tempArr];
            
            if (tempArr.count > 0) {
                weakSelf.page ++;
            }
            
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//加载列表数据
- (void)loadData {
    self.page = 1;
    [self loadMoreData];
}

- (void)loadMoreData {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"order"] = self.orderType;//w_time最新|num热门
    params[@"page"] = @(self.page);
//    params[@"limit"] = @"7";
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemUser parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSArray *tempArr = state.data[@"data"];
            if ([weakSelf.orderType isEqualToString:@"w_time"]) {
                if (weakSelf.page == 1) {
                    weakSelf.dataArr2 = [NSMutableArray arrayWithArray:[FDFriendModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }else {
                    [weakSelf.dataArr2 addObjectsFromArray:[FDFriendModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }
                [weakSelf.tableView reloadData];
                if (tempArr.count > 0) {
                    weakSelf.page++;
                }
            }else {
                if (weakSelf.page == 1) {
                    weakSelf.dataArr3 = [NSMutableArray arrayWithArray:[FDFriendModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }else {
                    [weakSelf.dataArr3 addObjectsFromArray:[FDFriendModel mj_objectArrayWithKeyValuesArray:tempArr]];
                }
                [weakSelf.tableView reloadData];
                if (tempArr.count > 0) {
                    weakSelf.page++;
                }
            }
           
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshTableView];
    } failure:^(NSError *error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [FDFriendListCell registerNibCellWithTableView:self.tableView];
}

- (void)setUpLeftView {
    FDBaseLeftView *leftV = [FDBaseLeftView initView];

    leftV.backgroundColor = [UIColor clearColor];
    leftV.delegate = self;
    self.leftV = leftV;
    //匹配列表
    [self setMenuButtonWithTitle1:@"距离" title2:@"最新" title3:@"热门"];

    /****************** 默认选中第一个 ******************/
    leftV.firstButton.selected = YES;
    
    if (self.lastButton && self.lastButton != leftV.firstButton) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = leftV.firstButton;

    self.orderType = @"";
    self.tableView.mj_footer.hidden = YES;
    self.tableView.mj_header.hidden = YES;
    /****************** 默认选中 ******************/

    [self.view addSubview:leftV];
    [leftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tableView);
        make.bottom.mas_equalTo(self.tableView);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(96);
    }];
    [self.view bringSubviewToFront:leftV];
}

#pragma mark ===== <FDLeftViewDelegate>
- (void)menuButtonClick:(UIButton *)button {
    if (self.lastButton != button) {
        button.selected = !button.selected;
    }
    
    if (self.lastButton && self.lastButton != button) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = button;

    self.tableView.mj_footer.hidden = NO;
    self.tableView.mj_header.hidden = NO;
    switch (button.tag) {
        case 1001: {//距离
            self.orderType = @"";
            self.tableView.mj_footer.hidden = YES;
            self.tableView.mj_header.hidden = YES;
            if (self.dataArr1.count > 0) {
                [self.tableView reloadData];
            }else {
                [self loadGetUsers];
            }
        }
            break;
        case 1002: {//最新
            self.orderType = @"w_time";
            //加载最新数据
            if (self.dataArr2.count > 0) {
                [self.tableView reloadData];
            }else {
                [self loadData];
            }
        }
            break;
        case 1003: {//热门
           self.orderType = @"num";
            if (self.dataArr3.count > 0) {
                [self.tableView reloadData];
            }else {
                [self loadData];
            }
        }
            break;
    }
}

#pragma mark =====  FDLocationDelegate (定位相关)
- (void)locationIsOpen:(BOOL)isOpen {
    self.isOpen = isOpen;
    
    if (self.isOpen) {//权限打开
        
    }else {//权限未打开
        
    }
}

- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark {
        
    self.lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    //获取定位, 加载数据
    [self loadGetUsers];
    
    if (placemark != nil) {
        //        NSString *myAddress;
        //        if (placemark.administrativeArea) {
        //            myAddress = [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.subLocality];
        //        }else {
        //            myAddress = [NSString stringWithFormat:@"%@%@", placemark.locality, placemark.subLocality];
        //        }
        
        //        NSLog(@"定位地址:%@", myAddress);
    }
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.orderType isEqualToString:@"w_time"]) {//最新
        return self.dataArr2.count;
    }else if ([self.orderType isEqualToString:@"num"]) {//热门
        return self.dataArr3.count;
    }else {//距离
        return self.dataArr1.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [FDFriendListCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FDFriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDFriendListCell identifier]];
    if (cell == nil) {
        cell = [[FDFriendListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDFriendListCell identifier]];
    }
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

    if ([self.orderType isEqualToString:@"w_time"]) {//最新
        FDFriendModel *model = self.dataArr2[indexPath.section];
        cell.model = model;
    }else if ([self.orderType isEqualToString:@"num"]) {//最新
        FDFriendModel *model = self.dataArr3[indexPath.section];
        cell.model = model;
    }else {//距离
        FDNearbyPeopleModel *model = self.dataArr1[indexPath.section];
        cell.nearbyModel = model;
    }
  
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *uid;
    NSString *title;
    NSString *logo;
    if ([self.orderType isEqualToString:@"w_time"]) {//最新
        FDFriendModel *model = self.dataArr2[indexPath.section];
        uid = model.uid;
        title = model.nickname;
        logo = model.logo;
    }else if ([self.orderType isEqualToString:@"num"]) {//热门
        FDFriendModel *model = self.dataArr3[indexPath.section];
        uid = model.uid;
        title = model.nickname;
        logo = model.logo;
    }else {//距离
        FDNearbyPeopleModel *model = self.dataArr1[indexPath.section];
        uid = model.uid;
        title = model.nickname;
        logo = model.logo;
    }
    
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:uid type:EMConversationTypeChat createIfNotExist:YES];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
    chatVC.title = title;
    chatVC.avatarUrl = logo;
    
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark ===== 功能
//自定义button
- (void)setMenuButtonWithTitle1:(NSString *)title1 title2:(NSString *)title2 title3:(NSString *)title3 {
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateNormal];
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateSelected];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateNormal];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateSelected];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateNormal];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateSelected];
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (FDLocationManager *)FDLocationManager {
    if (!_FDLocationManager) {
        _FDLocationManager = [[FDLocationManager alloc] init];
        _FDLocationManager.delegate = self;
    }
    return _FDLocationManager;
}


- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[FDCustomTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.opaque = NO;
        _tableView.rowHeight = 63;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        
        //下拉刷新
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf loadData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(29);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_equalTo(63*7);
        }];
    }
    return _tableView;
}

@end
