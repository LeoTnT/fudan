//
//  FDFriendController.h
//  App3.0
//
//  Created by lichao on 2018/10/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseController.h"

//寻找燥友
@interface FDFriendController : FDBaseController

@property (nonatomic, assign) BOOL isPresent;

@end
