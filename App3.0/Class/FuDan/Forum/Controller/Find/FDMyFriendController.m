//
//  FDSubFriendController.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMyFriendController.h"
#import "FDFriendListCell.h"
#import "FDInviteCell.h"
#import "FDBaseLeftView.h"
#import "FDPersonController.h"
#import "FDFriendModel.h"
#import "FDFindModel.h"

#import "FDCustomTableView.h"//列表
#import "FDLocationManager.h"//定位
#import "ContactListSelectViewController.h"

@interface FDMyFriendController ()<UITableViewDelegate, UITableViewDataSource, FDLeftViewDelegate, FDLocationDelegate>

@property (nonatomic, weak) FDBaseLeftView *leftV;
//上一个选中的按钮
@property (nonatomic, strong) UIButton *lastButton;

@property (nonatomic, strong) FDCustomTableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArr1;//距离
@property (nonatomic, strong) NSMutableArray *dataArr2;//邀请
@property (nonatomic, strong) NSMutableArray *dataArr3;//建群

@property (nonatomic, assign) NSInteger page;
/** 1-2-3 */
@property (nonatomic, copy) NSString *orderType;

@property (nonatomic, strong) FDLocationManager *FDLocationManager;
/* 定位权限是否开启 */
@property (nonatomic, assign) BOOL isOpen;
/*纬度*/
@property (nonatomic, copy) NSString *lat;
/*经度*/
@property (nonatomic, copy) NSString *lng;

@end

@implementation FDMyFriendController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setHidesBottomBarWhenPushed:YES];
    [self showBackgroudViewWithImageName:@"fd_blue_bg"];
    
    [self setUpTableView];
    [self setUpLeftView];
    
    [self getRelationsData];
    [self getMyGroupList];
}

//下拉刷新
- (void)refreshData {
    if ([self.orderType integerValue] == 1) {//距离
        [self getRelationsData];
    }else if ([self.orderType integerValue] == 2) {//邀请
        
    }else {//建群
        [self getMyGroupList];
    }
}

//刷新好友列表
- (void)reloadData {
    if (self.dataArr1.count > 0) {
        [self.dataArr1 removeAllObjects];
    }
    // 从本地数据库拿数据
    NSMutableArray *dbArr = [[DBHandler sharedInstance] getAllContact];
    NSMutableArray *myFriends = [NSMutableArray array];
    for (ContactDataParser *parser in dbArr) {
//        if ([parser.relation integerValue] == ContactRelationFriend) {
            [myFriends addObject:parser];
//        }
    }
    if (myFriends.count > 0) {
        self.dataArr1 = [NSMutableArray arrayWithArray:myFriends];
        [self.tableView reloadData];
    }
}

//刷新群组列表
- (void)reloadGroupListData {
    if (self.dataArr3.count > 0) {
        [self.dataArr3 removeAllObjects];
    }
    
    // 从本地数据库拿数据
    NSArray *dbArr = [[DBHandler sharedInstance] getAllGroup];
    if (dbArr.count > 0) {
        self.dataArr3 = [NSMutableArray arrayWithArray:dbArr];
    }
    [self.tableView reloadData];
}

#pragma mark ===== 网络请求
//我的好友列表 (所有0|好友1|关注的2|粉丝3)
- (void)getRelationsData {
    kFDWeakSelf;
    [HTTPManager getRelationListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            ContactParser *dataParser = [ContactParser mj_objectWithKeyValues:dic];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSMutableArray *tempArr = [NSMutableArray array];
                NSMutableArray *allContacts = [[DBHandler sharedInstance] getAllContact];
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:allContacts];
                for (ContactDataParser *parser in dataParser.data) {
                    [tempArr addObject:parser];
                    
                    // 加入数据库
                    [[DBHandler sharedInstance] addOrUpdateContact:parser];
                    
                    NSLog(@"%@",parser.nickname);
                    // 更新群组成员数据库
                    NickNameDataParser *nndP = [NickNameDataParser new];
                    nndP.logo = parser.avatar;
                    nndP.nickname = parser.nickname;
                    nndP.uid = parser.uid;
                    [[DBHandler sharedInstance] addOrUpdateGroupMember:nndP];
                    
                    // 数据库排除现有联系人，剩下的就是将要删除的
                    for (ContactDataParser *oldParser in allContacts) {
                        if ([oldParser.uid isEqualToString:parser.uid]) {
                            [deleteArr removeObject:oldParser];
                        }
                    }
                }
                
                // 数据库删除不存在的联系人
                for (ContactDataParser *delParser in deleteArr) {
                    if ([delParser.relation integerValue] > 0) {
                        [[DBHandler sharedInstance] deleteContactByUid:delParser.uid];
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self reloadData];
                });
            });
            
        } else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshTableView];
    } fail:^(NSError *error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

//获取我的群组列表
- (void)getMyGroupList {
    // 使用新接口，从环信获取群组列表放在后台操作
    kFDWeakSelf;
    [HTTPManager getMyGroupListWithSuccess:^(NSDictionary * dic, resultObject *state) {
        if (state.status) {
            GroupModel *dataModel = [GroupModel mj_objectWithKeyValues:dic];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSMutableArray *tempArr = [NSMutableArray array];
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:weakSelf.dataArr3];
                for (GroupDataModel *model in dataModel.data) {
                    [tempArr addObject:model];
                    // 加入数据库
                    [[DBHandler sharedInstance] addOrUpdateGroup:model];
                    
                    // 数据库排除现有群组，剩下的就是将要删除的
                    for (GroupDataModel *oldParser in weakSelf.dataArr3) {
                        if ([oldParser.gid isEqualToString:model.gid]) {
                            [deleteArr removeObject:oldParser];
                        }
                    }
                }
                
                // 数据库删除不存在的群组
                for (GroupDataModel *delParser in deleteArr) {
                    [[DBHandler sharedInstance] deleteGroupByGroupId:delParser.gid];
                }
                
                [weakSelf.dataArr3 removeAllObjects];
                [weakSelf.dataArr3 addObjectsFromArray:tempArr];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            });
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
        [weakSelf endRefreshTableView];
    } fail:^(NSError * _Nonnull error) {
        [weakSelf endRefreshTableView];
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [FDFriendListCell registerNibCellWithTableView:self.tableView];
    [FDInviteCell registerNibCellWithTableView:self.tableView];
}

- (void)setUpLeftView {
    FDBaseLeftView *leftV = [FDBaseLeftView initView];
    
    leftV.backgroundColor = [UIColor clearColor];
    leftV.delegate = self;
    self.leftV = leftV;
    //匹配列表
    [self setMenuButtonWithTitle1:@"距离" title2:@"邀请" title3:@"建群"];
    
    /****************** 默认选中第一个 ******************/
    leftV.firstButton.selected = YES;
    
    if (self.lastButton && self.lastButton != leftV.firstButton) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = leftV.firstButton;
    self.orderType = @"1";
    [self reloadData];
    /****************** 默认选中 ******************/
    
    [self.view addSubview:leftV];
    [leftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tableView);
        make.bottom.mas_equalTo(self.tableView);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(96);
    }];
    [self.view bringSubviewToFront:leftV];
}

#pragma mark ===== <FDLeftViewDelegate>
- (void)menuButtonClick:(UIButton *)button {
    if (self.lastButton != button) {
        button.selected = !button.selected;
    }
    
    if (self.lastButton && self.lastButton != button) {
        self.lastButton.selected = NO;
        self.lastButton = nil;
    }
    self.lastButton = button;
    
    self.tableView.mj_footer.hidden = NO;
    self.tableView.mj_header.hidden = NO;
    
    switch (button.tag) {
        case 1001: {//距离
            self.orderType = @"1";
            [self reloadData];
        }
            break;
        case 1002: {//邀请
            self.tableView.mj_footer.hidden = YES;
            self.tableView.mj_header.hidden = YES;
            self.orderType = @"2";
            //加载最新数据
            self.dataArr2 = [NSMutableArray arrayWithArray:@[
                                                            @{},
                                                            @{@"image":@"fd_invite_wx", @"name":@"邀请微信好友", @"des":@"邀请或添加你的微信好友"},
                                                            @{@"image":@"fd_invite_qq", @"name":@"邀请QQ好友", @"des":@"邀请或添加你的QQ好友"},
                                                            @{@"image":@"fd_invite_weibo", @"name":@"邀请微博好友", @"des":@"邀请或添加你的微博好友"},
                                                            @{@"image":@"fd_invite_contact", @"name":@"邀请通讯录好友", @"des":@"邀请或添加你的通讯录好友"},
                                                            ]];
            [self.tableView reloadData];
        }
            break;
        case 1003: {//建群
            self.orderType = @"3";
            //加载最新数据
            [self reloadGroupListData];
        }
            break;
    }
}

#pragma mark =====  FDLocationDelegate (定位相关)
- (void)locationIsOpen:(BOOL)isOpen {
    self.isOpen = isOpen;
    
    if (self.isOpen) {//权限打开
        
    }else {//权限未打开
        
    }
}

- (void)locationDidEndUpdatingLocation:(CLLocation *)location placemark:(CLPlacemark *)placemark {
    
    self.lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    //获取定位, 加载数据
//    [self loadGetUsers];
    
    if (placemark != nil) {
        //        NSString *myAddress;
        //        if (placemark.administrativeArea) {
        //            myAddress = [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.subLocality];
        //        }else {
        //            myAddress = [NSString stringWithFormat:@"%@%@", placemark.locality, placemark.subLocality];
        //        }
        
        //        NSLog(@"定位地址:%@", myAddress);
    }
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.orderType integerValue] == 1) {//好友
        return self.dataArr1.count;
    }else if ([self.orderType integerValue] == 2) {//邀请
        return self.dataArr2.count;
    }else {//建群
        return self.dataArr3.count+1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([self.orderType integerValue] == 2) {//邀请
//        return [FDInviteCell height];
//    }
//    return [FDFriendListCell height];
    return [FDInviteCell height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.orderType integerValue] == 1) {//我的燥友- 距离
        FDFriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDFriendListCell identifier]];
        if (cell == nil) {
            cell = [[FDFriendListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDFriendListCell identifier]];
        }
        //点击不变色
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        ContactDataParser *parser = self.dataArr1[indexPath.section];
        cell.parser = parser;
        
        return cell;
    }else {
        FDInviteCell *cell = [tableView dequeueReusableCellWithIdentifier:[FDInviteCell identifier]];
        if (cell == nil) {
            cell = [[FDInviteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FDInviteCell identifier]];
        }
        //点击不变色
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    
        if ([self.orderType integerValue] == 2) {//邀请
            cell.inviteDict = self.dataArr2[indexPath.section];
        }else {
            if (indexPath.section == 0) {//新建群组
                [cell.icon setImage:[UIImage imageNamed:@"fd_new_group"] forState:UIControlStateNormal];
                cell.nameLabel.text = @"新建群聊";
                cell.desLabel.text = @"和好友建群,一起来燥";
            }else {//群组列表
                GroupDataModel *model = self.dataArr3[indexPath.section-1];
                cell.groupModel = model;
            }
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    if ([self.orderType integerValue] == 1) {//好友
        ContactDataParser *parser = self.dataArr1[indexPath.section];

        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:parser.uid type:EMConversationTypeChat createIfNotExist:YES];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
        chatVC.title = parser.nickname;
        chatVC.avatarUrl = parser.avatar;
        [self.navigationController pushViewController:chatVC animated:YES];
        
    }else if ([self.orderType integerValue] == 2) {//邀请
        
    }else {//建群
        if (indexPath.section == 0) {//新建群聊
            // 创建群聊
            ContactListSelectViewController *clsVC = [[ContactListSelectViewController alloc] init];
            clsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:clsVC animated:YES];
        }else {
            GroupDataModel *groupModel = self.dataArr3[indexPath.section-1];
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:groupModel.gid type:EMConversationTypeGroupChat createIfNotExist:YES];
            if ([ChatHelper shareHelper].transVC) {
                //生成Message
                NSString *from = [[EMClient sharedClient] currentUsername];
                EMMessage *emMessgae = [ChatHelper shareHelper].transVC.message;
                EMMessage *message = [[EMMessage alloc] initWithConversationID:conversation.conversationId from:from to:conversation.conversationId body:emMessgae.body ext:emMessgae.ext];
                message.chatType = (EMChatType)conversation.type;
                [[EMClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
                    
                } completion:^(EMMessage *message, EMError *error) {
                    if (!error) {
                        [XSTool showToastWithView:self.view Text:@"发送成功"];
                    }else{
                        [XSTool showToastWithView:self.view Text:error.description];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSUInteger index = [self.navigationController.viewControllers indexOfObject:self.parentViewController];
                        [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:YES];
                    });
                }];
                return;
            }
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = groupModel.name;
            chatVC.avatarUrl = groupModel.avatar;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
            
        }
    }
}

#pragma mark ===== 功能
- (NSArray *)sortedArray:(NSArray *)array {
    NSArray* sorted = [array sortedArrayUsingComparator:^NSComparisonResult(GroupDataModel *obj1, GroupDataModel *obj2) {
        if (NSOrderedDescending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (NSOrderedAscending==[obj1.name compare:obj2.name])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return sorted;
}

//自定义button
- (void)setMenuButtonWithTitle1:(NSString *)title1 title2:(NSString *)title2 title3:(NSString *)title3 {
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateNormal];
    [self.leftV.firstButton setTitle:Localized(title1) forState:UIControlStateSelected];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateNormal];
    [self.leftV.secondButton setTitle:Localized(title2) forState:UIControlStateSelected];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateNormal];
    [self.leftV.thirdButton setTitle:Localized(title3) forState:UIControlStateSelected];
}

#pragma mark ===== mjRefresh
- (void)beginRefreshTableView {
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshTableView {
    // 头部控件结束刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark ===== 懒加载
- (FDLocationManager *)FDLocationManager {
    if (!_FDLocationManager) {
        _FDLocationManager = [[FDLocationManager alloc] init];
        _FDLocationManager.delegate = self;
    }
    return _FDLocationManager;
}


- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[FDCustomTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.opaque = NO;
        _tableView.rowHeight = 74;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        //下拉刷新
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf refreshData];
        }];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(29);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_equalTo(74*6);
        }];
    }
    return _tableView;
}

@end
