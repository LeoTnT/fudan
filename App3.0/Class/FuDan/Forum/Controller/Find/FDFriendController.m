//
//  FDFriendController.m
//  App3.0
//
//  Created by lichao on 2018/10/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFriendController.h"
#import "MFSideMenu.h"
#import "FDMatchingController.h"
#import "FDMyFriendController.h"
#import "FDReplyListController.h"
#import "AddFriendsViewController.h"

@interface FDFriendController ()

@property(nonatomic, weak) UIView *titleView;
@property(nonatomic, weak) UIView *lineView;

@property(nonatomic, weak) UIScrollView *contentScrollView;
//默认选中那个控制器
@property(nonatomic, assign) NSInteger selectedTag;
//当前选中的button
@property(nonatomic, weak) UIButton *selectedBtn;
//标题按钮数组
@property(nonatomic, strong) NSMutableArray *buttonArr;

@end

@implementation FDFriendController

//修改状态栏的前景色
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //view出现的时候状态栏前景颜色改为亮色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //view消失的时候状态栏前景颜色改为默认/黑色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showBackgroudViewWithImageName:@"fd_blue_bg"];
    [self setHidesBottomBarWhenPushed:YES];

    [self setUpCustomNav];
    [self setUpUI];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:Localized(@"寻找燥友") textColor:[UIColor hexFloatColor:@"FFFFFF"]];
    
    [self setLeftBtnWithImageName:@"fd_back" block:^{
        //点击头像滑到左侧
        if (weakSelf.isPresent) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }else {
            [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        }
    }];
    
    [self setRightBtnWithImageName:@"fd_find_chat" block:^{
        // 添加朋友
        AddFriendsViewController *addVC = [[AddFriendsViewController alloc] init];
        [weakSelf.navigationController pushViewController:addVC animated:YES];
    }];
}

- (void)setUpUI {
    
    //1.添加标题滚动视图
    [self setUpTitleView];
    //2.添加内容滚动视图
    [self setUpContentScrollView];
    //3.添加所有的子控制器
    [self setUpAllChildViewController];
    //4.添加标题按钮
    [self setUpBtnTitle];
    //5.添加下划线
//    [self addUnderLine];
    
    
    //取消额外滚动区域
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //设置内容滚动视图区域
    self.contentScrollView.contentSize = CGSizeMake(self.childViewControllers.count * mainWidth, 0);
    //设置整页翻页
    self.contentScrollView.pagingEnabled = YES;
    //水平滚动指示器不显示
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //禁止滚动
    self.contentScrollView.scrollEnabled = NO;
    
    //滚动完成的时候   设置代理
    //    self.contentScrollView.delegate = self;
}

#pragma mark -- SetUp
//1.添加标题视图
- (void)setUpTitleView {
    UIView *titleView = [[UIScrollView alloc] init];
    titleView.backgroundColor = [UIColor clearColor];
    
    titleView.frame = CGRectMake(0, FontNum(kStatusBarAndNavigationBarHeight)+FontNum(8), mainWidth, FontNum(34));
    
    self.titleView = titleView;
    [self.view addSubview:titleView];
}

//2.添加内容滚动视图
- (void)setUpContentScrollView {
    UIScrollView *contentScrollView = [[UIScrollView alloc] init];
    //计算尺寸
    CGFloat Y = CGRectGetMaxY(self.titleView.frame);
    contentScrollView.frame = CGRectMake(0, Y, mainWidth, mainHeight - Y);
    contentScrollView.backgroundColor = [UIColor lightGrayColor];
    
    self.contentScrollView = contentScrollView;
    [self.view addSubview:contentScrollView];
}

//3.添加所有的子控制器
- (void)setUpAllChildViewController {
    
    // 控制器1
    FDMatchingController *matVC = [[FDMatchingController alloc] init];
    matVC.title = Localized(@"匹配列表");
    [self addChildViewController:matVC];

    // 控制器2
    FDMyFriendController *myVC = [[FDMyFriendController alloc] init];
    myVC.title = Localized(@"我的燥友");
    [self addChildViewController:myVC];
    
    // 控制器2
    FDReplyListController *listVC = [[FDReplyListController alloc] init];
    listVC.title = Localized(@"回复列表");
    [self addChildViewController:listVC];
}

//4.添加标题按钮
- (void)setUpBtnTitle {
    
    //遍历所有的子控制器
    NSUInteger count = self.childViewControllers.count; //取出子控制器的数量
    
    CGFloat btnW = FontNum(100);
    CGFloat btnH = FontNum(34);
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat margin = (mainWidth-FontNum(100)*3)/4;
   
    for (int i = 0; i<count; i++) {
        
        UIViewController *vc = self.childViewControllers[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:[UIColor clearColor]];

        [btn setTitle:vc.title forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitleColor:[UIColor hexFloatColor:@"FFFFFF"] forState:UIControlStateNormal];
        
        btn.layer.cornerRadius = btnH/2;
        btn.layer.masksToBounds = YES;
        
        //给按钮绑定tag
        btn.tag = i;
        
        //设置按钮位置
        btnX = i * (btnW + margin) + margin;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        //监听按钮点击
        if (i == self.selectedTag) {
            [self btnClick:btn];
        }
        
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        //保存到数组
        [self.buttonArr addObject:btn];
        [self.titleView addSubview:btn];
    }
}

//5.添加下划线
- (void)addUnderLine {
    UIButton *selectBtn = self.buttonArr[self.selectedTag];
    //添加下划线
    UIView *lineView = [[UIView alloc] init];
    
    [self.titleView addSubview:lineView];
    
    lineView.backgroundColor = [selectBtn titleColorForState:UIControlStateSelected];
    lineView.xs_height = 2;
    lineView.xs_y = self.titleView.xs_height - lineView.xs_height;
    
    //默认点击了第一个按钮
    selectBtn.selected = YES;
    
    //让按钮根据内部文字的大小算出自己的大小
    [selectBtn.titleLabel sizeToFit];
    
    lineView.xs_width = selectBtn.titleLabel.xs_width;
    lineView.xs_centerX = selectBtn.xs_centerX;
    
    
    self.lineView = lineView;
}

/*** 监听按钮点击方法 ***/
- (void)btnClick:(UIButton *)btn {
    [self selectButton:btn];
    
    //切换界面
    NSInteger i = btn.tag;
    CGFloat x = i * mainWidth;
    
    [self showVc:x];
    self.contentScrollView.contentOffset = CGPointMake(x, 0);
}

/*** 选中按钮 ***/
- (void)selectButton:(UIButton *)btn {
    //还原上一个选中按钮
    [self.selectedBtn setBackgroundColor:[UIColor clearColor]];
    [btn setBackgroundColor:mainColor];
    //    /** 移动下划线 */
    //    [UIView animateWithDuration:0.2 animations:^{
    //        self.lineView.lb_centerX = btn.lb_centerX;
    //    }];
//    self.lineView.xs_width = btn.titleLabel.xs_width;
//    self.lineView.xs_centerX = btn.xs_centerX;
    
    self.selectedBtn = btn;
}

/*** 显示控制器的方法 ***/
- (void)showVc:(CGFloat)x {
    //取出对应控制器的view添加内容到滚动视图上去
    int i = x / mainWidth;
    UIViewController *showVC = self.childViewControllers[i];
    
    //判断当前控制器有没有加载完成
    if (showVC.isViewLoaded) return;
    
    //计算内容视图的frame
    showVC.view.frame = CGRectMake(x, 0, mainWidth, self.contentScrollView.bounds.size.height);
    
    [self.contentScrollView addSubview:showVC.view];
}

#pragma mark ===== 功能
- (void)setButtonWithTitle:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor hexFloatColor:@"FFFFFF"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:button];
}

#pragma mark -- 懒加载
- (NSMutableArray *)buttonArr {
    if (_buttonArr == nil) {
        _buttonArr = [NSMutableArray array];
    }
    return _buttonArr;
}

@end
