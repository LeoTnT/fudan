//
//  FDForumController.m
//  App3.0
//
//  Created by lichao on 2018/10/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDForumController.h"
#import "MFSideMenu.h"
#import "FDHotForumView.h"
#import "FDTagsFrame.h"
#import "FDProblemModel.h"
#import "FDGoYouController.h"
#import "FDSphereController.h"

@interface FDForumController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) FDTagsFrame *tagsFrame;
//数组
@property (nonatomic, strong) NSArray *dataArr;
//标签model数组
@property (nonatomic, strong) NSMutableArray *tagsModelArr;
//标签标题数组
@property (nonatomic, strong) NSMutableArray *tagsArr;

@end

@implementation FDForumController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setHidesBottomBarWhenPushed:YES];

    self.dataArr = @[@"全部",@"哈哈哈哈哈哈",@"的点点滴滴多多",@"他啦啦啦啦啦啦",@"发哈几个"];
    
    [self setUpTableView];
    [self setUpCustomNav];
    [self loadData];
}

#pragma mark ===== 网络请求
//加载标签
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"order"] = @"num";//随机rand|热门num
    params[@"is_short"] = @"1";//默认0|1(是否短标题)
    params[@"limit"] = @"9";//数据个数
    
    kFDWeakSelf;
    [XSHTTPManager post:FD_GetProblemMore parameters:params success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            weakSelf.tagsModelArr = [NSMutableArray arrayWithArray:[FDProblemModel mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            
            weakSelf.tagsArr = [NSMutableArray array];
            for (FDProblemModel *model in weakSelf.tagsModelArr) {
                NSString *title = model.title_short;
                [weakSelf.tagsArr addObject:title];
            }
            
            weakSelf.tagsFrame = [[FDTagsFrame alloc] init];
            weakSelf.tagsFrame.tagsArray = weakSelf.tagsArr;
            
            [weakSelf.tableView reloadData];
        }else {
            [XSTool showToastWithView:weakSelf.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [XSTool showToastWithView:weakSelf.view Text:NetFailure];
    }];
}

#pragma mark ===== setUpUI
- (void)setUpCustomNav {
    kFDWeakSelf;
    [self setNavTitle:Localized(@"燥事区") textColor:[UIColor hexFloatColor:@"242424"]];

    [self setLeftBtnWithImageName:@"fd_nav_menu_gray" block:^{
        //点击头像滑到左侧
        if (weakSelf.isPresent) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }else {
            [weakSelf.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        }
    }];
}

- (void)setUpTableView {
    //适配iOS11
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

#pragma mark ===== <UITableViewDelegate, UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return self.tagsFrame.tagsHeight+87;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    FDHotForumView *footerV = [[FDHotForumView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.tagsFrame.tagsHeight)];
    footerV.tagsFrame = self.tagsFrame;
    footerV.tagsArr = self.tagsArr;
    kFDWeakSelf;
    //点击更多
    footerV.moreBtnBlock = ^{
        FDSphereController *tzVC = [[FDSphereController alloc] init];
        [weakSelf.navigationController pushViewController:tzVC animated:YES];
    };
    
    //点击标签
    footerV.hotBtnBlock = ^(UIButton *button) {
        
        FDProblemModel *model = weakSelf.tagsModelArr[button.tag];
        FDGoYouController *goyouVC = [[FDGoYouController alloc] init];
        goyouVC.mid = model.ID;
        [weakSelf.navigationController pushViewController:goyouVC animated:YES];
    };
    
    return footerV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, mainWidth-60, 60)];
    bgView.backgroundColor = mainColor;
    [cell.contentView addSubview:bgView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, (60-17)/2, bgView.frame.size.width-20, 14)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:titleLabel];

    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(0, 59, bgView.frame.size.width, 0.5)];
    lineV.backgroundColor = [[UIColor hexFloatColor:@"FFFFFF"] colorWithAlphaComponent:0.18];
    [bgView addSubview:lineV];
    //点击不变色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == 0) {//第一个
        [self setCornerInView:bgView rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    }else if (indexPath.row == self.dataArr.count-1) {//最后一个
        [self setCornerInView:bgView rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    }
    
    titleLabel.text = @"测试文字";
    
    return cell;
}

- (void)setCornerInView:(UIView *)view rectCorner:(UIRectCorner)rectCorner{
    //切任意圆角
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:rectCorner cornerRadii:CGSizeMake(20, 20)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

- (void)leftItemClick {
    //点击头像滑到左侧
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (void)rightItemClick {
    
}

#pragma mark ===== 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.contentInset = UIEdgeInsetsMake(kStatusBarAndNavigationBarHeight+30, 0, kTabbarSafeBottomMargin, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        
        //解决iOS11 刷新tableview闪动问题
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
