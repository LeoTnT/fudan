//
//  FDMyDryTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDForumModel.h"

typedef void(^iconBlock)(void);
typedef void(^pushBlock)(void);
@interface FDMyDryTopView : FDBaseCustomView

//顶部头像背景
@property (weak, nonatomic) IBOutlet UIImageView *iconBgImageV;
//头像
@property (weak, nonatomic) IBOutlet UIImageView *iconImageV;
//昵称
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//积分
@property (weak, nonatomic) IBOutlet UIView *rankBgV;
@property (weak, nonatomic) IBOutlet UIButton *rankLabel;
//ID
@property (weak, nonatomic) IBOutlet UIButton *numLabel;

//主题背景view
@property (weak, nonatomic) IBOutlet UIView *titleTextVBgView;

//主题TextField
@property (weak, nonatomic) IBOutlet UITextField *titleTextF;

//内容背景view
@property (weak, nonatomic) IBOutlet UIView *textBgView;
//占位文字label
@property (weak, nonatomic) IBOutlet UILabel *palceholderLabel;
//输入内容TextView
@property (weak, nonatomic) IBOutlet UITextView *contentTextV;

@property (weak, nonatomic) IBOutlet UILabel *remainLabel;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (nonatomic, strong) FDUserInfoModel *userInfo;

@property (nonatomic, copy) iconBlock iconBlock;
@property (nonatomic, copy) pushBlock pushBlock;


@end
