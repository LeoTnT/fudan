//
//  FDMineListCell.h
//  App3.0
//
//  Created by lichao on 2018/10/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDProblemModel.h"

@interface FDMineListCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) FDProblemListModel *listModel;

@end
