//
//  FDMyDryTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDForumModel.h"

typedef void(^iconBlock)(void);
typedef void(^menuBlock)(BOOL isMe);
@interface FDMineTopView : FDBaseCustomView

//顶部头像背景
@property (weak, nonatomic) IBOutlet UIImageView *iconBgImageV;
//头像
@property (weak, nonatomic) IBOutlet UIImageView *iconImageV;
//昵称
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//积分

@property (weak, nonatomic) IBOutlet UIView *rankBgV;
@property (weak, nonatomic) IBOutlet UIButton *rankLabel;
//ID
@property (weak, nonatomic) IBOutlet UIButton *numLabel;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *meButton;
@property (weak, nonatomic) IBOutlet UIButton *heButton;

@property (nonatomic, copy) iconBlock iconBlock;
@property (nonatomic, copy) menuBlock menuBlock;

@property (nonatomic, strong) FDUserInfoModel *userInfoModel;

@end
