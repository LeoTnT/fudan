//
//  FDGoTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDProblemModel.h"

typedef void(^checkBlock)(UIImageView *imageV);
@interface FDWayTopView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *contentL;

@property (weak, nonatomic) IBOutlet UIView *ButtonView;
@property (weak, nonatomic) IBOutlet UIImageView *upOrDownImageV;

@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numWidthConstraint;


@property (nonatomic, copy) checkBlock checkBlock;

@property (nonatomic, strong) FDProblemInfoTopModel *topModel;

@end
