//
//  FDIconFooterView.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCollectionReusableView.h"

typedef void(^submitBlock)(void);
@interface FDIconFooterView : FDBaseCollectionReusableView

@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (nonatomic, copy) submitBlock submitBlock;

@end
