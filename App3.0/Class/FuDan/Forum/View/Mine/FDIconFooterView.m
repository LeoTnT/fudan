//
//  FDIconFooterView.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDIconFooterView.h"

@implementation FDIconFooterView

- (void)awakeFromNib {
    [super awakeFromNib];

    self.submitButton.layer.cornerRadius = 49/2;
    self.submitButton.layer.masksToBounds = YES;
}

- (IBAction)submitBtnClick:(id)sender {
    if (self.submitBlock) {
        self.submitBlock();
    }
}

@end
