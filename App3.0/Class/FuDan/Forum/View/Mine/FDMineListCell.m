//
//  FDMineListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMineListCell.h"
#import "XSFormatterDate.h"

@implementation FDMineListCell

- (void)setListModel:(FDProblemListModel *)listModel {
    if (listModel) {
        self.titleLabel.text = listModel.title;
        self.contentLabel.text = listModel.content;
        self.timeLabel.text = [XSFormatterDate dateWithTimeIntervalString:listModel.w_time];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (CGFloat)height {
    return 139;
}

@end
