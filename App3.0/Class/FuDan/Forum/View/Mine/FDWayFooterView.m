//
//  FDWayFooterView.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDWayFooterView.h"

@implementation FDWayFooterView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.replyButton.layer.borderWidth = 1;
    self.replyButton.layer.borderColor = [UIColor hexFloatColor:@"C0C0C0"].CGColor;
    self.replyButton.layer.cornerRadius = 24/2;
    self.replyButton.layer.masksToBounds = YES;
}

- (IBAction)replyBtnClick:(id)sender {
    if (self.replyBlock) {
        self.replyBlock();
    }
}

@end
