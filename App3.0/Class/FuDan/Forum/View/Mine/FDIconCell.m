//
//  FDIconCell.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDIconCell.h"

@implementation FDIconCell

- (void)setIconModel:(FDIconModel *)iconModel {
    [self.icon sd_setImageWithURL:[NSURL URLWithString:iconModel.img] placeholderImage:[UIImage imageNamed:@"no_pic"]];
    //选中头像
    self.isDefaultImageV.hidden = ![iconModel.is_default integerValue];
}

@end
