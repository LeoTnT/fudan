//
//  FDWaySubCommentCell.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDWaySubCommentCell.h"

@implementation FDWaySubCommentCell

- (void)setSubModel:(FDProblemReplyListModel *)subModel {
    if (subModel) {
        NSString *contentStr = [NSString stringWithFormat:@"%@ 回复 %@: %@", subModel.nickname, subModel.tonickname, subModel.content];
        
        // 创建Attributed
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:contentStr];
        // 需要改变的区间
        NSRange range = NSMakeRange(0, subModel.nickname.length);
        // 改变颜色
        [attr addAttribute:NSForegroundColorAttributeName value:mainColor range:range];
        // 改变字体大小及类型
        //        [attr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-BoldOblique" size:27] range:range];
        // 为label添加Attributed
        [self.commentLabel setAttributedText:attr];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.bgView.layer.cornerRadius = 3;
    self.bgView.layer.masksToBounds = YES;
}

@end
