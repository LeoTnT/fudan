//
//  FDWayCommentCell.m
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDWayCommentCell.h"
#import "XSFormatterDate.h"

@implementation FDWayCommentCell

- (void)setListmodel:(FDProblemReplyListModel *)listmodel {
    if (listmodel) {
        [self.iconButton sd_setImageWithURL:[NSURL URLWithString:listmodel.logo] forState:UIControlStateNormal];
        self.nameLabel.text = listmodel.nickname;
        self.timeLabel.text = [XSFormatterDate dateWithTimeIntervalString:listmodel.w_time];
        self.contentLabel.text = listmodel.content;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconButton.layer.cornerRadius = 35/2;
    self.iconButton.layer.masksToBounds = YES;
    
    self.talkButton.layer.cornerRadius = 29/2;
    self.talkButton.layer.masksToBounds = YES;
}

//点击头像
- (IBAction)iconClick:(id)sender {
    if (self.iconBlock) {
        self.iconBlock();
    }
}
//点击谈天说地
- (IBAction)talkBtnClick:(id)sender {
    if (self.talkBlock) {
        self.talkBlock();
    }
}

@end
