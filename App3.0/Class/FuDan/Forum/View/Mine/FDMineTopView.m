//
//  FDMyDryTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMineTopView.h"

@interface FDMineTopView ()

@end

@implementation FDMineTopView

- (void)setUserInfoModel:(FDUserInfoModel *)userInfoModel {
    if (userInfoModel) {
        [self.iconBgImageV sd_setImageWithURL:[NSURL URLWithString:userInfoModel.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        [self.iconImageV sd_setImageWithURL:[NSURL URLWithString:userInfoModel.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        
        //昵称
        self.nameLabel.text = userInfoModel.nickname.length > 0 ? userInfoModel.nickname : userInfoModel.username;
        //级别
        [self.rankLabel setTitle:userInfoModel.collect_rank forState:UIControlStateNormal];
        //用户编号
        [self.numLabel setTitle:userInfoModel.username forState:UIControlStateNormal];
    }
}

+ (CGFloat)height {
    return 360;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //头像
    self.iconImageV.layer.cornerRadius = 92/2;
    self.iconImageV.layer.masksToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconClick)];
    self.iconImageV.userInteractionEnabled = YES;
    [self.iconImageV addGestureRecognizer:tap];
    
    //
    self.bgView.layer.borderWidth = 1;
    self.bgView.layer.borderColor = mainColor.CGColor;
    self.bgView.layer.cornerRadius = 40/2;
    self.bgView.layer.masksToBounds = YES;
    
    self.rankBgV.layer.cornerRadius = 16/2;
    self.rankBgV.layer.masksToBounds = YES;
    
    //默认选中 我燥
    self.meButton.selected = YES;
}

- (void)iconClick {
    if (self.iconBlock) {
        self.iconBlock();
    }
}

- (IBAction)meBtnClick:(id)sender {
    UIButton *meBtn = (UIButton *)sender;
    if (!meBtn.selected) {
        meBtn.selected = YES;
        if (self.menuBlock) {
            self.menuBlock(YES);
        }
        
        if (self.heButton.selected) {
            self.heButton.selected = NO;
        }
    }
}

- (IBAction)heBtnClick:(id)sender {
    UIButton *heBtn = (UIButton *)sender;
    if (!heBtn.selected) {
        heBtn.selected = YES;
        if (self.menuBlock) {
            self.menuBlock(NO);
        }
        
        if (self.meButton.selected) {
            self.meButton.selected = NO;
        }
    }
}


@end
