//
//  FDWayCommentCell.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDProblemModel.h"

typedef void(^iconBlock)(void);
typedef void(^talkBlock)(void);
@interface FDWayCommentCell : FDBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *iconButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *talkButton;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (nonatomic, strong) FDProblemReplyListModel *listmodel;

@property (nonatomic, copy) iconBlock iconBlock;
@property (nonatomic, copy) talkBlock talkBlock;

@end
