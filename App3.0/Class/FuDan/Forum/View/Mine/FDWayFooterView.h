//
//  FDWayFooterView.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

typedef void(^replyBlock)(void);
@interface FDWayFooterView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIButton *replyButton;

@property (nonatomic, copy) replyBlock replyBlock;

@end
