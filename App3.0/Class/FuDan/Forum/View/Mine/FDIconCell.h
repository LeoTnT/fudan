//
//  FDIconCell.h
//  App3.0
//
//  Created by lichao on 2018/10/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCollectionCell.h"
#import "FDForumModel.h"

@interface FDIconCell : FDBaseCollectionCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UIImageView *isDefaultImageV;

@property (nonatomic, strong) FDIconModel *iconModel;

@end
