
//
//  FDGoTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDWayTopView.h"

@interface FDWayTopView()<UITextViewDelegate>

@end

@implementation FDWayTopView

- (void)setTopModel:(FDProblemInfoTopModel *)topModel {
    if (topModel) {
        self.titleL.text = topModel.title;
        self.contentL.text = topModel.content;
        if ([topModel.num integerValue] > 0) {
            self.numLabel.hidden = NO;
            
            NSString *bageNum = topModel.num;
            //小红点显示
            if ([bageNum integerValue] > 99) {
                self.numWidthConstraint.constant = 25;
                self.numLabel.text = @"99+";
            }else {
                self.numWidthConstraint.constant = 18;
                self.numLabel.text = bageNum;
            }
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    //bottom
    self.ButtonView.layer.cornerRadius = 44/2;
    self.ButtonView.layer.masksToBounds = YES;
    
    self.numLabel.layer.cornerRadius = 18/2;
    self.numLabel.layer.masksToBounds = YES;
    self.numLabel.hidden = YES;
    
    //点击事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBtnClick)];
    self.ButtonView.userInteractionEnabled = YES;
    [self.ButtonView addGestureRecognizer:tap];
}

- (void)checkBtnClick {
    if (self.checkBlock) {
        self.checkBlock(self.upOrDownImageV);
    }
}

@end
