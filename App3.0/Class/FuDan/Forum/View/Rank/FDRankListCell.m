//
//  FDRankListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDRankListCell.h"

@implementation FDRankListCell

+ (CGFloat)height {
    return 83;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.icon.layer.cornerRadius = 49/2;
    self.icon.layer.masksToBounds = YES;
    
    //点击
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconClick)];
    self.icon.userInteractionEnabled = YES;
    [self.icon addGestureRecognizer:tap];
}

- (void)iconClick {
    if (self.listIconBlock) {
        self.listIconBlock();
    }
}

@end
