//
//  FDRankListCell.h
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"

typedef void(^listIconBlock)(void);
@interface FDRankListCell : FDBaseTableViewCell

//背景图
@property (weak, nonatomic) IBOutlet UIView *bgView;
//排名
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
//头像
@property (weak, nonatomic) IBOutlet UIImageView *icon;
//头像_logo
@property (weak, nonatomic) IBOutlet UIImageView *gbLabel;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//
@property (weak, nonatomic) IBOutlet UIButton *numBtn;

@property (nonatomic, copy) listIconBlock listIconBlock;

@end
