//
//  FDRankTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDRankTopView.h"

@implementation FDRankTopView

+ (CGFloat)height {
    return 230;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.logoImageV1.layer.borderColor = [UIColor hexFloatColor:@"F2D308"].CGColor;
    self.logoImageV1.layer.borderWidth = 3;
    self.logoImageV1.layer.cornerRadius = 100/2;
    self.logoImageV1.layer.masksToBounds = YES;

    self.logoImageV2.layer.borderColor = [UIColor hexFloatColor:@"40B0BC"].CGColor;
    self.logoImageV2.layer.borderWidth = 3;
    self.logoImageV2.layer.cornerRadius = 60/2;
    self.logoImageV2.layer.masksToBounds = YES;
    
    self.logoImageV3.layer.borderColor = [UIColor hexFloatColor:@"F13C68"].CGColor;
    self.logoImageV3.layer.borderWidth = 3;
    self.logoImageV3.layer.cornerRadius = 60/2;
    self.logoImageV3.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(top1IconClick)];
    self.logoImageV1.userInteractionEnabled = YES;
    [self.logoImageV1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(top2IconClick)];
    self.logoImageV2.userInteractionEnabled = YES;
    [self.logoImageV2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(top3IconClick)];
    self.logoImageV3.userInteractionEnabled = YES;
    [self.logoImageV3 addGestureRecognizer:tap3];
}

- (void)top1IconClick {
    if (self.iconBlock) {
        self.iconBlock(@"1");
    }
}

- (void)top2IconClick {
    if (self.iconBlock) {
        self.iconBlock(@"2");
    }
}

- (void)top3IconClick {
    if (self.iconBlock) {
        self.iconBlock(@"3");
    }
}

@end
