//
//  FDRankTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

typedef void(^iconBlock)(NSString *userID);
@interface FDRankTopView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *logoImageV1;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageV2;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageV3;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel1;
@property (weak, nonatomic) IBOutlet UIButton *numBtn1;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel2;
@property (weak, nonatomic) IBOutlet UIButton *numBtn2;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel3;
@property (weak, nonatomic) IBOutlet UIButton *numBtn3;

@property (nonatomic, copy) iconBlock iconBlock;

@end
