//
//  FDBaseView.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseLeftView.h"

@implementation FDBaseLeftView

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (CGFloat)height {
    return 393;
}

- (IBAction)firstBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(menuButtonClick:)]) {
        [self.delegate menuButtonClick:sender];
    }
}

- (IBAction)secondBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(menuButtonClick:)]) {
        [self.delegate menuButtonClick:sender];
    }
}

- (IBAction)thirdBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(menuButtonClick:)]) {
        [self.delegate menuButtonClick:sender];
    }
}

@end
