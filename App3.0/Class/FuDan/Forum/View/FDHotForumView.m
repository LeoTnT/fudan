//
//  FDMyDryHeaderView.m
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDHotForumView.h"

@interface FDHotForumView ()

@property (nonatomic, strong) UIView *lineV;

@end

@implementation FDHotForumView

- (instancetype)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    //标题
    UILabel *titleL = [[UILabel alloc] init];
    titleL.textColor = [UIColor hexFloatColor:@"242424"];
    titleL.font = [UIFont systemFontOfSize:18];
    titleL.text = Localized(@"#热门燥事区");
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(44);
        make.left.mas_equalTo(self).offset(30);
        make.height.mas_equalTo(17);
    }];
    
    //更多按钮
    UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreBtn setImage:[UIImage imageNamed:@"fd_more"] forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(41);
        make.right.mas_equalTo(self).offset(-30);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    //分割线
    self.lineV = [[UIView alloc] init];
    self.lineV.backgroundColor = [UIColor hexFloatColor:@"E5E5E5"];
    [self addSubview:self.lineV];
    [self.lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleL.mas_bottom).offset(10);
        make.left.mas_equalTo(self).offset(30);
        make.right.mas_equalTo(self).offset(-30);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)setTagsArr:(NSArray *)tagsArr {
    
    //标签页
    UIView *bgV = [[UIView alloc] init];
    bgV.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgV];
    [bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.lineV.mas_bottom).offset(15);
        make.left.mas_equalTo(self);
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(self.tagsFrame.tagsHeight);
    }];
    
    //0-蓝色, 1-红色, 2-黄色, 3-白色
    NSArray *bgColorArr = @[mainColor, [UIColor hexFloatColor:@"F03C68"], [UIColor hexFloatColor:@"FFE816"], [UIColor hexFloatColor:@"FFFFFF"]];
    
    NSArray *titleColorArr = @[[UIColor hexFloatColor:@"F03C68"], mainColor, [UIColor hexFloatColor:@"8E1636"]];

    for (NSInteger i=0; i< tagsArr.count; i++) {
        
        UIButton *tagsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //标题
        NSString *title = tagsArr[i];
//        if (title.length > 5) {
//            title = [title substringToIndex:5];
//            title = [NSString stringWithFormat:@"%@...", title];
//        }
        [tagsBtn setTitle:title forState:UIControlStateNormal];
    
        //设置随机背景色
        int randNum1 = [self getRandomNumber:0 to:3];
        UIColor *bgColor = bgColorArr[randNum1];
        [tagsBtn setBackgroundColor:bgColor];
        tagsBtn.layer.borderColor = bgColor.CGColor;

        //设置字体颜色
        if (randNum1 == 0 || randNum1 == 1) {
            [tagsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else if (randNum1 == 2) {
            [tagsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }else {//白色背景的  字体颜色,边框颜色--随机
            int randNum2 = [self getRandomNumber:0 to:2];
            UIColor *titleColor = titleColorArr[randNum2];
            [tagsBtn setTitleColor:titleColor forState:UIControlStateNormal];
            tagsBtn.layer.borderColor = titleColor.CGColor;
        }

        tagsBtn.layer.borderWidth = 1;
        tagsBtn.layer.cornerRadius = 4;
        tagsBtn.layer.masksToBounds = YES;
        
        tagsBtn.titleLabel.font = TagsTitleFont;
        tagsBtn.frame = CGRectFromString(self.tagsFrame.tagsFrames[i]);
        tagsBtn.layer.cornerRadius = tagsBtn.frame.size.height/2;
        tagsBtn.layer.masksToBounds = YES;
        
        tagsBtn.tag = i;
        [tagsBtn addTarget:self action:@selector(hotBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [bgV addSubview:tagsBtn];
    }
}

- (void)hotBtnClick:(UIButton *)sender {
    if (self.hotBtnBlock) {
        self.hotBtnBlock(sender);
    }
}

- (void)moreBtnClick {
    if (self.moreBtnBlock) {
        self.moreBtnBlock();
    }
}

/** 在固定范围内获取随机数 */
- (int)getRandomNumber:(int)from to:(int)to {
    int tooo = to-from+1;
    int x = arc4random() % tooo;
    return (int)(from + from+x);
}

@end
