//
//  FDPersonTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDPersonTopView.h"

@implementation FDPersonTopView

- (void)setUserInfoModel:(FDUserInfoModel *)userInfoModel {
    if (userInfoModel) {
        [self.iconBgImageV sd_setImageWithURL:[NSURL URLWithString:userInfoModel.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:userInfoModel.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        
        self.nameLabel.text = userInfoModel.nickname;
        [self.rankLabel setTitle:userInfoModel.collect_rank forState:UIControlStateNormal];
        [self.numLabel setTitle:userInfoModel.username forState:UIControlStateNormal];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //头像
    self.icon.layer.cornerRadius = 92/2;
    self.icon.layer.masksToBounds = YES;
}

@end
