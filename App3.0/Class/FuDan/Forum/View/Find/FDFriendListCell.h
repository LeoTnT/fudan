//
//  FDFriendListCell.h
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"
#import "FDFindModel.h"
#import "FDFriendModel.h"

typedef void(^iconBlock)(void);
@interface FDFriendListCell : FDBaseTableViewCell

//头像
@property (weak, nonatomic) IBOutlet UIButton *icon;
//logo
@property (weak, nonatomic) IBOutlet UIImageView *gbLogo;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//状态
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
//积分
@property (weak, nonatomic) IBOutlet UIButton *numBtn;
//距离
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (nonatomic, copy) iconBlock iconBlock;
//附近的人
@property (nonatomic, strong) FDNearbyPeopleModel *nearbyModel;
//好友
@property (nonatomic, strong) FDFriendModel *model;
//我的燥友model
@property (nonatomic, strong) ContactDataParser *parser;


@end
