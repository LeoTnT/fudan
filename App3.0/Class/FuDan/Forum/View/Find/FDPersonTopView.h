//
//  FDPersonTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/27.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDForumModel.h"

@interface FDPersonTopView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *iconBgImageV;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *rankLabel;
@property (weak, nonatomic) IBOutlet UIButton *numLabel;

@property (nonatomic, strong) FDUserInfoModel *userInfoModel;

@end
