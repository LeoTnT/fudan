//
//  FDFriendListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDFriendListCell.h"

@implementation FDFriendListCell

- (void)setParser:(ContactDataParser *)parser {
    if (parser) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:parser.avatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"fd_default_icon"]];
        self.nameLabel.text = parser.nickname;
        self.desLabel.text = parser.remark;
        self.distanceLabel.text = @"0";
        [self.numBtn setTitle:parser.username forState:UIControlStateNormal];
    }
}

- (void)setNearbyModel:(FDNearbyPeopleModel *)nearbyModel {
    if (nearbyModel) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:nearbyModel.logo] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"fd_default_icon"]];
        self.nameLabel.text = nearbyModel.nickname;
        self.desLabel.text = nearbyModel.fandom;
        self.distanceLabel.text = nearbyModel.distance;
        [self.numBtn setTitle:nearbyModel.username forState:UIControlStateNormal];
    }
}

- (void)setModel:(FDFriendModel *)model {
    if (model) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:model.logo] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"fd_default_icon"]];
        self.nameLabel.text = model.nickname;
//        self.desLabel.text = model.title;
        self.distanceLabel.text = @"0";
        [self.numBtn setTitle:model.username forState:UIControlStateNormal];
    }
}

+ (CGFloat)height {
    return 63;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.icon.layer.cornerRadius = 50/2;
    self.icon.layer.masksToBounds = YES;
}

- (IBAction)iconBtnClick:(id)sender {
    if (self.iconBlock) {
        self.iconBlock();
    }
}

@end
