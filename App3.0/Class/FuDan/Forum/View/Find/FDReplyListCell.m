//
//  FDFriendListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDReplyListCell.h"

@implementation FDReplyListCell

- (void)setConversationModel:(ConversationModel *)conversationModel {
    if (conversationModel) {
        self.titleLabel.text = conversationModel.title;
        [self.iconBtn sd_setImageWithURL:[NSURL URLWithString:conversationModel.avatarURLPath] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"fd_default_icon"]];
        
        EMConversation *conversation = conversationModel.conversation;
        if (conversation.unreadMessagesCount > 0) {
            if (conversation.unreadMessagesCount <= 99) {
                self.badgeNumLabel.text = [NSString stringWithFormat:@"%d",conversation.unreadMessagesCount];
            } else {
                self.badgeNumLabel.text = @"99+";
            }
            
            self.badgeNumLabel.hidden = NO;
        } else {
            self.badgeNumLabel.hidden = YES;
        }
        
        EMMessage *latestMes = conversation.latestMessage;
        self.lastConversationLabel.text = [[ChatHelper shareHelper] latestMessageTitleWithMessage:latestMes];
        
        // 显示最后会话时间
        NSString *str = [self timeWithTimeIntervalString:[NSString stringWithFormat:@"%lld",latestMes.timestamp]];
        NSString *subString = [str substringWithRange:NSMakeRange(0, 19)];
        NSDate *lastDate = [NSDate dateFromString:subString withFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"%@",lastDate);
        NSString *dateStr;  //年月日
        NSString *hour;     //时
        
        if ([lastDate year] == [[NSDate date] year]) {
            NSInteger days = [NSDate daysOffsetBetweenStartDate:lastDate endDate:[NSDate date]];
            NSLog(@"days === %li",(long)days);
            if (days <= 2) {
                dateStr = [lastDate stringYearMonthDayCompareToday];
                if (isEmptyString(dateStr)) {
                    hour = [NSString stringWithFormat:@"%02d",(int)[lastDate hour]];
                    self.timeLabel.text = [NSString stringWithFormat:@"%@:%02d",hour,(int)[lastDate minute]];
                } else {
                    self.timeLabel.text = [NSString stringWithFormat:@"%@",dateStr];
                }
            }else{
                dateStr = [lastDate stringMonthDay];
                self.timeLabel.text = [NSString stringWithFormat:@"%@",dateStr];
            }
        }else{
            dateStr = [lastDate stringYearMonthDay];
            self.timeLabel.text = [NSString stringWithFormat:@"%@",dateStr];
        }
    }
}

+ (CGFloat)height {
    return 63;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.iconBtn.layer.cornerRadius = 50/2;
    self.iconBtn.layer.masksToBounds = YES;
    
    self.badgeNumLabel.layer.cornerRadius = 18/2;
    self.badgeNumLabel.layer.masksToBounds = YES;
}

- (NSString *)timeWithTimeIntervalString:(NSString *)timeString {
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]/ 1000.0];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

@end
