//
//  FDFriendListCell.m
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDInviteCell.h"

@implementation FDInviteCell

- (void)setGroupModel:(GroupDataModel *)groupModel {
    if (groupModel) {
        [self.icon  sd_setImageWithURL:[NSURL URLWithString:groupModel.avatar] forState:UIControlStateNormal];
        self.nameLabel.text = groupModel.name;
        self.desLabel.text = [NSString stringWithFormat:@"群成员%@人", groupModel.member_count];
    }
}

- (void)setInviteDict:(NSDictionary *)inviteDict {
    if (inviteDict) {
        [self.icon setImage:[UIImage imageNamed:inviteDict[@"image"]] forState:UIControlStateNormal];
        self.nameLabel.text = inviteDict[@"name"];
        self.desLabel.text = inviteDict[@"des"];
    }
}

+ (CGFloat)height {
    return 74;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.icon.layer.cornerRadius = 50/2;
    self.icon.layer.masksToBounds = YES;
}

- (IBAction)iconBtnClick:(id)sender {
  
}

@end
