//
//  FDFriendListCell.h
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseTableViewCell.h"

@interface FDInviteCell : FDBaseTableViewCell

//头像
@property (weak, nonatomic) IBOutlet UIButton *icon;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//状态
@property (weak, nonatomic) IBOutlet UILabel *desLabel;

//邀请
@property (nonatomic, strong) NSDictionary *inviteDict;
//我的群组
@property (nonatomic, strong) GroupDataModel *groupModel;


@end
