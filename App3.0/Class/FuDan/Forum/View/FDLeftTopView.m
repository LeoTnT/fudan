//
//  FDLeftTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDLeftTopView.h"

@implementation FDLeftTopView

- (void)setUserInfoModel:(FDUserInfoModel *)userInfoModel {
    if (userInfoModel) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:userInfoModel.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.nameLabel.text = userInfoModel.nickname.length > 0 ? userInfoModel.nickname : userInfoModel.username;
        [self.rankBtn setTitle:userInfoModel.collect_rank forState:UIControlStateNormal];
        self.desLabel.text = @"暂无";
        [self.numLabel setTitle:userInfoModel.username forState:UIControlStateNormal];
    }
}

+ (CGFloat)height {
    return 276;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //头像
    self.icon.layer.cornerRadius = 92/2;
    self.icon.layer.masksToBounds = YES;
    
    self.rankBgV.layer.cornerRadius = 16/2;
    self.rankBgV.layer.masksToBounds = YES;
    
    //头像添加点击事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconClick)];
    self.icon.userInteractionEnabled = YES;
    [self.icon addGestureRecognizer:tap];
}

- (void)iconClick {
    if (self.iconBlock) {
        self.iconBlock();
    }
}

@end
