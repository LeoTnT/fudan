//
//  FDMyDryTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDMyDryTopView.h"

@interface FDMyDryTopView ()<UITextViewDelegate>

@end

@implementation FDMyDryTopView

- (void)setUserInfo:(FDUserInfoModel *)userInfo {
    if (userInfo) {
        [self.iconBgImageV sd_setImageWithURL:[NSURL URLWithString:userInfo.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        [self.iconImageV sd_setImageWithURL:[NSURL URLWithString:userInfo.dry_logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        //昵称
        self.nameLabel.text = userInfo.nickname.length > 0 ? userInfo.nickname : userInfo.username;
        //积分
        [self.rankLabel setTitle:userInfo.collect_rank forState:UIControlStateNormal];
        //id
        [self.numLabel setTitle:userInfo.username forState:UIControlStateNormal];
    }
}

+ (CGFloat)height {
    return 598;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //头像
    self.iconImageV.layer.cornerRadius = 92/2;
    self.iconImageV.layer.masksToBounds = YES;
    
    //rank
    self.rankBgV.layer.cornerRadius = 16/2;
    self.rankBgV.layer.masksToBounds = YES;
    
    //头像添加点击事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconBtnClick)];
    self.iconImageV.userInteractionEnabled = YES;
    [self.iconImageV addGestureRecognizer:tap];
    
    //主题
    self.titleTextVBgView.layer.cornerRadius = 40/2;
    self.titleTextVBgView.layer.masksToBounds = YES;
    
    //修改textField占位文字属性
    [self.titleTextF setValue:[UIColor hexFloatColor:@"C0C0C0"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.titleTextF setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    
    //内容
    self.textBgView.layer.cornerRadius = 10;
    self.textBgView.layer.masksToBounds = YES;
    self.contentTextV.delegate = self;
    
    //走你
    self.sureBtn.layer.cornerRadius = 49/2;
    self.sureBtn.layer.masksToBounds = YES;
}

- (void)iconBtnClick {
    if (self.iconBlock) {
        self.iconBlock();
    }
}

- (IBAction)sureBtnClick:(id)sender {
    if (self.pushBlock) {
        self.pushBlock();
    }
}

#pragma mark ==== <UITextViewDelegate>
- (void)textViewDidChange:(UITextView *)textView {
    self.palceholderLabel.hidden = textView.text.length > 0 ? YES : NO;
    //剩余输入字数
    if (textView.text.length <= 200) {
        NSInteger remainNum = 200 - textView.text.length;
        self.remainLabel.text = [NSString stringWithFormat:@"%ld/200", (long)remainNum];
    }else {
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:200];
        textView.text = [textView.text substringToIndex:range.location];
    }
}

@end
