//
//  FDBaseView.h
//  App3.0
//
//  Created by lichao on 2018/10/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"

@protocol FDLeftViewDelegate <NSObject>
@optional
- (void)menuButtonClick:(UIButton *)button;

@end

@interface FDBaseLeftView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdButton;

@property (nonatomic, weak) id<FDLeftViewDelegate>delegate;

@end
