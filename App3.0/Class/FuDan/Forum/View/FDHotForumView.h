//
//  FDMyDryHeaderView.h
//  App3.0
//
//  Created by lichao on 2018/10/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDTagsFrame.h"

typedef void(^moreBtnBlock)(void);
typedef void(^hotBtnBlock)(UIButton *button);
@interface FDHotForumView : UIView

@property (nonatomic, strong) FDTagsFrame *tagsFrame;
@property (nonatomic, strong) NSArray *tagsArr;

@property (nonatomic, copy) hotBtnBlock hotBtnBlock;
@property (nonatomic, copy) moreBtnBlock moreBtnBlock;

@end
