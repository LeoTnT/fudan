//
//  FDLeftTopView.h
//  App3.0
//
//  Created by lichao on 2018/10/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDBaseCustomView.h"
#import "FDForumModel.h"

typedef void(^iconBlock)(void);
@interface FDLeftTopView : FDBaseCustomView

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIView *rankBgV;
@property (weak, nonatomic) IBOutlet UIButton *rankBtn;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIButton *numLabel;

@property (nonatomic, copy) iconBlock iconBlock;
@property (nonatomic, strong) FDUserInfoModel *userInfoModel;

@end
