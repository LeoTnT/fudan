
//
//  FDGoTopView.m
//  App3.0
//
//  Created by lichao on 2018/10/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "FDGoTopView.h"

@interface FDGoTopView()<UITextViewDelegate>

@end

@implementation FDGoTopView

- (void)setModel:(FDProblemInfoTopModel *)model {
    if (model) {
        self.titleLabel.text = model.title;
        self.contentLabel.text = model.content;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    //白色背景
    [self setCornerInView:self.bgView rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
    //灰色背景
    self.inputBgView.layer.cornerRadius = 10;
    self.inputBgView.layer.masksToBounds = YES;
    self.inputTextF.delegate = self;
    
    //走你
    self.goButton.layer.cornerRadius = 49/2;
    self.goButton.layer.masksToBounds = YES;
}

- (IBAction)goBtnClick:(id)sender {
    if (self.pushBlock) {
        self.pushBlock();
    }
}

#pragma mark ==== <UITextViewDelegate>
- (void)textViewDidChange:(UITextView *)textView {
    self.placeholderLabel.hidden = textView.text.length > 0 ? YES : NO;
    //剩余输入字数
    if (textView.text.length <= 200) {
        NSInteger remainNum = 200 - textView.text.length;
        self.remainLabel.text = [NSString stringWithFormat:@"%ld/200", (long)remainNum];
    }else {
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:200];
        textView.text = [textView.text substringToIndex:range.location];
    }
}


- (void)setCornerInView:(UIView *)view rectCorner:(UIRectCorner)rectCorner{
    //切任意圆角
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:rectCorner cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

@end
