//
//  BindCountTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/6/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BindCountTableViewCell.h"
#import "XSCustomButton.h"

@interface BindCountTableViewCell ()
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UIButton *bindButton;
@end
@implementation BindCountTableViewCell
static CGFloat CellHeight = 60;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE*2, NORMOL_SPACE, NORMOL_SPACE*4, NORMOL_SPACE*4)];
        [self.contentView addSubview:self.logo];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.logo.frame)+NORMOL_SPACE, CGRectGetMinY(self.logo.frame), mainWidth-CGRectGetMaxX(self.logo.frame)-2*NORMOL_SPACE, NORMOL_SPACE*2)];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.titleLabel];
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.logo.frame)+NORMOL_SPACE, CGRectGetMaxY(self.titleLabel.frame), mainWidth-CGRectGetMaxX(self.logo.frame)-2*NORMOL_SPACE-CellHeight, NORMOL_SPACE*2)];
        self.descriptionLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:self.descriptionLabel];
        self.bindButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-NORMOL_SPACE*2-CellHeight, CGRectGetMaxY(self.titleLabel.frame)-NORMOL_SPACE, CellHeight, NORMOL_SPACE*3)];
        self.bindButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.bindButton setTitle:Localized(@"bind_btn_text") forState:UIControlStateNormal];
        [self.bindButton setTitle:@"取消绑定" forState:UIControlStateSelected];
        [self.bindButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bindButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
        self.bindButton.layer.cornerRadius = 5;
        self.bindButton.layer.masksToBounds = YES;
        self.bindButton.backgroundColor = mainColor;
        [self.contentView addSubview:self.bindButton];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CellHeight-1, mainWidth, 0.5)];
        lineView.backgroundColor = LINE_COLOR;
        [self.contentView addSubview: lineView];
    }
    return self;
}

- (void)bindAction {
    self.bindButton.selected = !self.bindButton.selected;
    if (self.bindButton.selected) {
        self.bindButton.backgroundColor = LINE_COLOR;
       
        if ([self.delegate respondsToSelector:@selector(bindSocialWithType:)]) {
            [self.delegate bindSocialWithType:self.bindCountParser.type];
        }
//            [self.bindButton setTitle:@"取消绑定" forState:UIControlStateSelected];
        
    } else {
//        [self.bindButton setTitle:Localized(@"change_mobile_btn_text") forState:UIControlStateNormal];
        self.bindButton.backgroundColor = mainColor;
        if ([self.delegate respondsToSelector:@selector(cancelBindWithType:)]) {
            [self.delegate cancelBindWithType:self.bindCountParser.type];
        }
        
    }
}

- (void)changeMobileAction{
     self.bindButton.selected = !self.bindButton.selected;
    if ([self.delegate respondsToSelector:@selector(changeSocialWithType:)]) {
        [self.delegate changeSocialWithType:self.bindCountParser.type];
    }
}

- (void)setBindCountParser:(BindCountParser *)bindCountParser {
    _bindCountParser = bindCountParser;
    self.titleLabel.text = _bindCountParser.name;
    
    if ([_bindCountParser.type isEqualToString:@"weixin"]) {
        self.logo.image = [UIImage imageNamed:@"user_binding_wechat"];
    } else if ([_bindCountParser.type isEqualToString:@"qq"]) {
        self.logo.image = [UIImage imageNamed:@"user_bind_qq"];
    } else {
        self.logo.image = [UIImage imageNamed:@"user_binding_phone"];
    }
    if ([_bindCountParser.type isEqualToString:@"mobile"]) {
        self.bindButton.backgroundColor = LINE_COLOR;
        [self.bindButton addTarget:self action:@selector(changeMobileAction) forControlEvents:UIControlEventTouchUpInside|UIControlStateSelected];
        [self.bindButton setTitle:Localized(@"change_mobile_btn_text") forState:UIControlStateSelected];
        [self.bindButton setTitle:Localized(@"change_mobile_btn_text") forState:UIControlStateNormal];
    } else {
        [self.bindButton addTarget:self action:@selector(bindAction) forControlEvents:UIControlEventTouchUpInside];
    }

    if ([_bindCountParser.status integerValue]==1) {
        self.bindButton.selected = YES;
        self.bindButton.backgroundColor = LINE_COLOR;
        NSLog( @"%@",_bindCountParser.type);
        self.descriptionLabel.text = [NSString stringWithFormat:@"当前绑定账号:%@",_bindCountParser.target];
        
    } else {
        self.bindButton.selected = NO;
        self.bindButton.backgroundColor = mainColor;
        self.descriptionLabel.text = _bindCountParser.memo;
    }
}
@end
