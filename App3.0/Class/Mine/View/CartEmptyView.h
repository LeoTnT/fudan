//
//  CartEmptyView.h
//  App3.0
//
//  Created by mac on 17/3/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartEmptyViewDelegate <NSObject>
- (void)goBuyClick;
- (void)goCollectClick;
@end

@interface CartEmptyView : UIView
@property (nonatomic, weak) id<CartEmptyViewDelegate>delegate;
@end
