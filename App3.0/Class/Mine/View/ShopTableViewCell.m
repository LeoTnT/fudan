//
//  ShopTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ShopTableViewCell.h"
#import "UIImage+XSWebImage.h"



@interface ShopTableViewCell()
{
    UIImageView *_logo;
    UILabel *_title;
    UIView *_starView;//星级
    UIButton *_enjoyBtn;
    UIButton *_deleteBtn;
    
}
@end
@implementation ShopTableViewCell
static CGFloat IMG_SIZE = 60;
static CGFloat BTN_SIZE = 40;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    self.contentView.backgroundColor = BG_COLOR;
    //图片
    _logo = [[UIImageView alloc] initWithFrame:CGRectMake(NORMOL_SPACE, NORMOL_SPACE*2, IMG_SIZE+2*NORMOL_SPACE, IMG_SIZE)];
    _logo.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_logo];
    _title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_logo.frame)+NORMOL_SPACE, CGRectGetMinY(_logo.frame), mainWidth-CGRectGetWidth(_logo.frame)-3*NORMOL_SPACE, NORMOL_SPACE*2)];
    _title.font = [UIFont systemFontOfSize:20];
    [self.contentView addSubview:_title];
    _starView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_title.frame), CGRectGetMaxY(_title.frame)+NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
    //    _starView.backgroundColor = [UIColor yellowColor];
    for (int i=0; i<5; i++) {
        @autoreleasepool {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i*(NORMOL_SPACE+NORMOL_SPACE), 0, NORMOL_SPACE*2, NORMOL_SPACE*2)];
            [_starView addSubview:button];
            [button setImage:[UIImage imageNamed:@"user_collect_unstar"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"user_collect_star"] forState:UIControlStateSelected];
        }
        
    }
    [self.contentView addSubview:_starView];
    _enjoyBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-BTN_SIZE*2-2*NORMOL_SPACE, CGRectGetMinY(_starView.frame), BTN_SIZE, BTN_SIZE)];
    //    [_enjoyBtn addTarget:self action:@selector(toEnjoy:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toEnjoy:)];
    _enjoyBtn.userInteractionEnabled = YES;
    [_enjoyBtn addGestureRecognizer:tap];
    [_enjoyBtn setImage:[UIImage imageNamed:@"user_collect_share"] forState:UIControlStateNormal];
    [self.contentView addSubview:_enjoyBtn];
    _enjoyBtn.hidden = YES;
    _deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_enjoyBtn.frame)+NORMOL_SPACE, CGRectGetMinY(_enjoyBtn.frame), BTN_SIZE, BTN_SIZE)];
    //    _deleteBtn.backgroundColor = mainGrayColor;
    [_deleteBtn setImage:[UIImage imageNamed:@"user_collect_heart"] forState:UIControlStateNormal];
    //    [_deleteBtn addTarget:self action:@selector(toDelete:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *deleteTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toDelete:)];
    _deleteBtn.userInteractionEnabled = YES;
    [_deleteBtn addGestureRecognizer:deleteTap];
    [self.contentView addSubview:_deleteBtn];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_logo.frame)+NORMOL_SPACE*2, mainWidth, 0.5)];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self.contentView addSubview:line];
}

- (void)toEnjoy:(UITapGestureRecognizer *)sender {
    //     if ([self.shopDelegate respondsToSelector:@selector(clickToEnjoyShopWithId:)]) {
    //    [self.shopDelegate clickToEnjoyShopWithId:self.dataParser.user_id];
    //     }
    NSLog(@"enjoy");
    
}

- (void)toDelete:(UITapGestureRecognizer *)sender {
    NSLog(@"delete");
    if ([self.shopDelegate respondsToSelector:@selector(clickToCancelCollectWithId:)]) {
        [self.shopDelegate clickToCancelCollectWithId:self.dataParser.user_id];
    }
}

- (void)setDataParser:(ShopDataParser *)dataParser {
    _dataParser = dataParser;
    [_logo getImageWithUrlStr:_dataParser.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    _title.text = _dataParser.name;
    if ([_dataParser.eva integerValue]>0) {
        NSArray *ary = _starView.subviews;
        for (int i=0; i<[_dataParser.eva integerValue]; i++) {
            if ([[ary objectAtIndex:i] isKindOfClass:[UIButton class]]) {
                UIButton *btn = [ary objectAtIndex:i];
                btn.selected = YES;
            }
        }
    }
    
}

@end
