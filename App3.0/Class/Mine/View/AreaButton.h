//
//  AreaButton.h
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AreaButtonDelegate <NSObject>
- (void)areaButtonClickWithIndex:(NSInteger)index;
@end
@interface AreaButton : UIButton
@property (nonatomic, weak) id<AreaButtonDelegate>delegate;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, copy) NSString *bageNumber;
/*
 Model: @image:图片名 @title:名称 @fontSize:字体大小 @scale:图片比例
 */
- (AreaButton *)initWithFrame:(CGRect)frame Model:(NSDictionary *)model Scale:(CGFloat)scale fontSize:(CGFloat)font;
- (void)setTitleColor:(UIColor *)color;
@end
