//
//  GoodsTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectModel.h"
#import "XSFormatterDate.h"
#import "XSCustomButton.h"
@protocol CollectDelegate <NSObject>
@optional
/**移除收藏商品*/
- (void)deleteCollectionWithId:(NSString * )product_id;
@optional
/**立即购买*/
- (void)clickToGoodsDetailPage:(NSString *)product_id goodsName:(NSString *)goodsName;

@end

@interface GoodsTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UITextView *descTextView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) XSCustomButton *deleteCollectionBtn;
@property (nonatomic, strong) XSCustomButton *buyBtn;
@property (nonatomic, strong) UIButton *enjoyBtn;
@property (nonatomic, strong) CollectParser *parser;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id<CollectDelegate> collectDelegate;

/**标识*/
+ (NSString *)idString;
@end

