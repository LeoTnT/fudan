//
//  UserServiceCell.m
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserServiceCell.h"
#import "AreaButton.h"

@interface UserServiceCell()
{
    __weak id<UserServiceCellDelegate>delegate;
}
@end

@implementation UserServiceCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat width = mainWidth/4;
        CGFloat height = 90;
        
        UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 1)];
        hLine.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:hLine];
        
        NSArray *arr = @[@{@"image":@"user_list_thh",@"title":@"泰合会"},@{@"image":@"user_list_fruit",@"title":@"切水果"},@{@"image":@"user_list_recharge",@"title":@"充值中心"}];
        
        for (int i = 0; i < arr.count; i++) {
            AreaButton *btn = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%4), (int)(i/4)*height, width, height) Model:arr[i] Scale:1.0/3.0 fontSize:14];
            btn.index = i;
             [btn addTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
        }
        
    }
    return self;
}

- (void)areaAction:(AreaButton *)pSender
{
    if ([delegate respondsToSelector:@selector(serviceAreaClick:)]) {
        [delegate serviceAreaClick:pSender.index];
    }
}
@end
