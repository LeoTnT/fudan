//
//  BankTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardModel.h"

@protocol BankDelegate <NSObject>
@optional
/**设为默认*/
- (void)bankCardToDefault:(BankCardDataParser *)bankParser;
@optional
/**删除*/
- (void)bankCardToDelete:(BankCardDataParser *)bankParser;
@end

@interface BankTableViewCell : UITableViewCell
@property (nonatomic, strong) BankCardDataParser *parser;
@property (nonatomic, assign) BOOL isShow;//是否显示带删除的区域
@property (nonatomic, weak) id<BankDelegate> bankDelegate;
@property (nonatomic, strong) UIView *backView;
@end
