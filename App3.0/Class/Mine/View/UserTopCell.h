//
//  UserTopCell.h
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginModel.h"
#import "UserModel.h"
@protocol UserTopCellDelegate <NSObject>
@optional
- (void)topAvatarClick;
- (void)topRightNavClick;

@end

@interface UserTopCell : UITableViewCell

@property (nonatomic, weak) id<UserTopCellDelegate>delegate;
@property (nonatomic, strong) LoginDataParser *parser;

@property (nonatomic, copy) void(^TapAction)(NSInteger index);

@end
