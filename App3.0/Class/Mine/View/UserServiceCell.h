//
//  UserServiceCell.h
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol UserServiceCellDelegate <NSObject>
@optional
- (void)serviceAreaClick:(NSInteger)index;
@end
@interface UserServiceCell : UITableViewCell
@property(nonatomic, weak) id<UserServiceCellDelegate>delegate;
@end
