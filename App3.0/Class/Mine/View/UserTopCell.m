//
//  UserTopCell.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserTopCell.h"
#import "UIImage+XSWebImage.h"
#import "ChatHelper.h"
#import "AreaButton.h"
#import "CountShowView.h"

@interface UserTopCell()<AreaButtonDelegate>
{
    __weak id<UserTopCellDelegate>delegate;
}
@property (nonatomic, strong) UIImageView *userAvatar;
@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *userNo;
@property (nonatomic, strong) NSMutableArray *footerViewArray;
@property (nonatomic, strong) UIImageView *redPoint;

@end

@implementation UserTopCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //背景图
        UIImageView *bgImageV = [UIImageView new];
        [bgImageV setImage:[UIImage imageNamed:@"fd_top_bg"]];
        [self.contentView addSubview:bgImageV];
        [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self);
            make.height.mas_equalTo(FontNum(170));
        }];
        
        //编辑资料
        UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [editBtn setTitle:Localized(@"编辑资料") forState:UIControlStateNormal];
        editBtn.titleLabel.font = [UIFont systemFontOfSize:11];
        [editBtn setImage:[UIImage imageNamed:Localized(@"fd_edit_left")] forState:UIControlStateNormal];
        [editBtn setBackgroundImage:[UIImage imageNamed:@"fd_edit_bg"] forState:UIControlStateNormal];
        [editBtn setTitleEdgeInsets:UIEdgeInsetsMake(0,5,0,0)];
        [editBtn addTarget:self action:@selector(rightNavAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:editBtn];
        [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).offset(49);
            make.right.mas_equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(75, 27));
        }];
        
        //白色背景
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 10;
        //        bgView.layer.masksToBounds = YES;
        //设置阴影
        bgView.layer.shadowColor = [UIColor hexFloatColor:@"9C9C9C"].CGColor;
        bgView.layer.shadowOpacity = 0.24;//设置阴影的透明度
        bgView.layer.shadowOffset = CGSizeMake(1, 2);//设置阴影的偏移量
        bgView.layer.shadowRadius = 8;//设置阴影的圆角
        
        [self.contentView addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).offset(15);
            make.right.mas_equalTo(self.contentView).offset(-15);
            make.height.mas_equalTo(FontNum(180));
            make.bottom.mas_equalTo(self.contentView).offset(-10);
        }];
        //头像
        _userAvatar = [UIImageView new];
        _userAvatar.layer.cornerRadius = 68/2;
        _userAvatar.layer.masksToBounds = YES;
        [self.contentView addSubview:_userAvatar];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatar:)];
        _userAvatar.userInteractionEnabled = YES;
        [_userAvatar addGestureRecognizer:tap];
        [_userAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.contentView);
            make.centerY.mas_equalTo(bgView.mas_top);
            make.size.mas_equalTo(CGSizeMake(68, 68));
        }];
        
        _userName = [[UILabel alloc] init];
        _userName.textColor = [UIColor hexFloatColor:@"111111"];
        _userName.textAlignment = NSTextAlignmentCenter;
        _userName.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:_userName];
        [_userName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.top.mas_equalTo(_userAvatar.mas_bottom).offset(11);
        }];
        
        _userNo = [[UILabel alloc] init];
        _userNo.textColor = [UIColor hexFloatColor:@"999999"];
        _userNo.textAlignment = NSTextAlignmentCenter;
        _userNo.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_userNo];
        
        [_userNo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.top.mas_equalTo(_userName.mas_bottom).offset(14);
        }];
        
#ifdef APP_SHOW_JYS
        
#else
        
        self.footerViewArray = [NSMutableArray array];
        NSArray *buttonArr = @[@{@"image":@"fd_mycollect",@"title":Localized(@"我的收藏")},@{@"image":@"fd_myfooter",@"title":Localized(@"我的足迹")},@{@"image":@"fd_cart",@"title":Localized(@"购物车")}];
        
        for (int i = 0; i < buttonArr.count; i++) {
            
            CGFloat width = (mainWidth-15*2)/buttonArr.count;
            AreaButton *areaBtn = [[AreaButton alloc] initWithFrame:CGRectMake(width*i, 180-80, width, 70) Model:buttonArr[i] Scale:0 fontSize:12];
            [areaBtn setTitleColor:[UIColor hexFloatColor:@"666666"]];
            areaBtn.index = i;
            areaBtn.delegate = self;
            [bgView addSubview:areaBtn];
        }
#endif
    }
    return self;
}
#pragma mark - 点击头像
- (void)tapAvatar:(UITapGestureRecognizer *)tap {
    [self.delegate topAvatarClick];
}

- (void)setParser:(LoginDataParser *)parser {
    _parser = parser;
    _userName.text = [NSString stringWithFormat:@"%@",_parser.nickname];
    _userNo.text = [NSString stringWithFormat:@"团队业绩 %@", _parser.username];
    [_userAvatar getImageWithUrlStr:_parser.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    if ([ChatHelper shareHelper].badgeNumber > 0) {
        self.redPoint.hidden = NO;
    } else {
        self.redPoint.hidden = YES;
    }
}

- (void)rightNavAction {
    if (delegate) {
        [delegate topRightNavClick];
    }
}

- (void)areaButtonClickWithIndex:(NSInteger)index {
    if (self.TapAction) {
        self.TapAction(index);
    }
}

@end
