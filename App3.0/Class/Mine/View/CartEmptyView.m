//
//  CartEmptyView.m
//  App3.0
//
//  Created by mac on 17/3/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartEmptyView.h"

@interface CartEmptyView()
{
    __weak id<CartEmptyViewDelegate>delegate;
}
@end

@implementation CartEmptyView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth/2-80, 150, 160, 160)];
        imgView .image = [UIImage imageNamed:@"empty_logo"];
        [self addSubview:imgView];
        UILabel *des = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(imgView.frame), mainWidth-20, 20)];
        des.text = @"购物车里还没有添加商品哦";
        des.font = [UIFont systemFontOfSize:14];
        des.textAlignment = NSTextAlignmentCenter;
        [self addSubview:des];
        
        UIButton *goBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(des.frame)+NORMOL_SPACE, mainWidth/2-40, 40)];
        [goBtn setTitle:Localized(@"去逛逛") forState:UIControlStateNormal];
        [goBtn setBackgroundColor:mainColor];
        goBtn.layer.masksToBounds = YES;
        goBtn.layer.cornerRadius = 3;
        [goBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [goBtn addTarget:self action:@selector(goAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:goBtn];
        
        UIButton *collect = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth/2+10, CGRectGetMaxY(des.frame)+NORMOL_SPACE, mainWidth/2-40, 40)];
        [collect setTitle:@"我的收藏" forState:UIControlStateNormal];
        [collect setBackgroundColor:[UIColor whiteColor]];
        collect.layer.masksToBounds = YES;
        collect.layer.cornerRadius = 3;
        collect.layer.borderWidth = 1;
        collect.layer.borderColor = [mainColor CGColor];
        [collect setTitleColor:mainColor forState:UIControlStateNormal];
        [collect addTarget:self action:@selector(collectAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:collect];
    }
    return self;
}

- (void)goAction
{
    if (delegate) {
        [delegate goBuyClick];
    }
}

- (void)collectAction
{
    if (delegate) {
        [delegate goCollectClick];
    }
}
@end
