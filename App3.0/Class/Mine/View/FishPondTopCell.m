//
//  FishPondTopCell.m
//  App3.0
//
//  Created by mac on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FishPondTopCell.h"
@implementation FishPondTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //蓝色背景
        UIView *blueView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 220)];
        blueView.backgroundColor=TAB_SELECTED_COLOR;
        [self.contentView addSubview:blueView];
        //头像
        self.headImg=[[UIImageView alloc] initWithFrame:CGRectMake((mainWidth-100)/2.0, 30, 100, 100)];
        self.headImg.image=[UIImage imageNamed:@"user_fans_avatar"];
        self.headImg.layer.cornerRadius=100/2.0;
        self.headImg.layer.masksToBounds=YES;
        [self.contentView addSubview:self.headImg];
        //昵称
        self.nickNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.headImg.frame)+10 , mainWidth, 20)];
        self.nickNameLabel.text=Localized(@"person_info_nick");
        self.nickNameLabel.textAlignment=NSTextAlignmentCenter;
        self.nickNameLabel.textColor=[UIColor whiteColor];
        [self.contentView addSubview:self.nickNameLabel];
        //鱼数
        self.fishNumLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.nickNameLabel.frame)+10 , mainWidth, 20)];
        self.fishNumLabel.text=@"共收到3条鱼";
        [self.contentView addSubview:self.fishNumLabel];
        self.fishNumLabel.textAlignment=NSTextAlignmentCenter;
        self.fishNumLabel.textColor=[UIColor whiteColor];
        //价值
        UILabel *priceLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(self.fishNumLabel.frame)+50 , mainWidth, 20)];
        priceLabel.text=@"价值";
        [self.contentView addSubview:priceLabel];
        priceLabel.textColor=mainGrayColor;
        priceLabel.textAlignment=NSTextAlignmentCenter;
        self.moneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(priceLabel.frame)+10 , mainWidth, 50)];
        self.moneyLabel.text=@"¥0";
        self.moneyLabel.textColor=[UIColor blackColor];
        self.moneyLabel.font=[UIFont systemFontOfSize:30];
        self.moneyLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:self.moneyLabel];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
