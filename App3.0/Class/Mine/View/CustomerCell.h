//
//  CustomerCell.h
//  App3.0
//
//  Created by mac on 2018/5/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface CustomerCell : UITableViewCell

@property (strong, nonatomic) CustomerModel *customerModel;
@property (nonatomic ,copy)void (^ callAction)(CustomerModel *customerModel);
@property (nonatomic ,copy)void (^ chatAction)(CustomerModel *customerModel);
+ (instancetype)createCustomerCellWithTableView:(UITableView *)tableView;
@end
