//
//  CustomerCell.m
//  App3.0
//
//  Created by mac on 2018/5/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "CustomerCell.h"

@interface CustomerCell ()
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *nickNameLabel;
@end

@implementation CustomerCell
+ (instancetype)createCustomerCellWithTableView:(UITableView *)tableView {
    static NSString *identifier = @"groupBuyCell";
    CustomerCell *cell = (CustomerCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[CustomerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.avatar = [[UIImageView alloc] init];
        self.avatar.image = [UIImage imageNamed:@"user_fans_avatar"];
        self.avatar.layer.masksToBounds = YES;
        self.avatar.layer.cornerRadius = 5;
        [self.contentView addSubview:self.avatar];
        
        self.nickNameLabel = [[UILabel alloc] init];
        self.nickNameLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:self.nickNameLabel];
        
        UILabel *descript = [[UILabel alloc] init];
        descript.text = @"我是您的专属客服，点击按钮向我提问哦！";
        descript.textColor = COLOR_999999;
        descript.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:descript];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = LINE_COLOR_NORMAL;
        [self.contentView addSubview:lineView];
        
        UIButton *callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [callButton setTitle:@"拨打电话" forState:UIControlStateNormal];
        [callButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        callButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [callButton setBackgroundImage:[UIImage xl_imageWithColor:mainColor size:CGSizeZero] forState:UIControlStateNormal];
        [callButton setBackgroundImage:[UIImage xl_imageWithColor:HighLightColor_Main size:CGSizeZero] forState:UIControlStateHighlighted];
        callButton.layer.masksToBounds = YES;
        callButton.layer.cornerRadius = 3;
        [callButton addTarget:self action:@selector(callClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:callButton];
        
        UIButton *chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [chatButton setTitle:@"发送消息" forState:UIControlStateNormal];
        [chatButton setTitleColor:mainColor forState:UIControlStateNormal];
        chatButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [chatButton setBackgroundImage:[UIImage xl_imageWithColor:[UIColor whiteColor] size:CGSizeZero] forState:UIControlStateNormal];
        [chatButton setBackgroundImage:[UIImage xl_imageWithColor:BG_COLOR size:CGSizeZero] forState:UIControlStateHighlighted];
        chatButton.layer.masksToBounds = YES;
        chatButton.layer.cornerRadius = 3;
        [chatButton addTarget:self action:@selector(chatClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:chatButton];
        
        [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.top.mas_equalTo(self).offset(12);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.avatar.mas_right).offset(8);
            make.top.mas_equalTo(self.avatar);
            make.right.mas_lessThanOrEqualTo(self).offset(-8);
        }];
        [descript mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.avatar.mas_right).offset(8);
            make.bottom.mas_equalTo(self.avatar);
            make.right.mas_lessThanOrEqualTo(self).offset(-8);
        }];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self);
            make.top.mas_equalTo(self.avatar.mas_bottom).offset(12);
            make.height.mas_equalTo(0.5f);
        }];
        [callButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.avatar);
            make.right.mas_equalTo(self.mas_centerX).offset(-12);
            make.top.mas_equalTo(lineView.mas_bottom).offset(10);
            make.height.mas_equalTo(40);
        }];
        [chatButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_centerX).offset(12);
            make.right.mas_equalTo(self).offset(-12);
            make.top.mas_equalTo(lineView.mas_bottom).offset(10);
            make.height.mas_equalTo(40);
        }];
    }
    return self;
}

- (void)setCustomerModel:(CustomerModel *)customerModel {
    _customerModel = customerModel;
    [self.avatar getImageWithUrlStr:self.customerModel.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.nickNameLabel.text = self.customerModel.nickname;
}

- (void)callClick {
    if (!self.customerModel.mobile) {
        return;
    }
    if (self.callAction) {
        self.callAction(self.customerModel);
    }
}

- (void)chatClick {
    if (self.chatAction) {
        self.chatAction(self.customerModel);
    }
}
@end
