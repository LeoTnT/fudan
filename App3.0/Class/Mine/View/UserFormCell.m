//
//  UserFormCell.m
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserFormCell.h"
#import "AreaButton.h"

@interface UserFormCell()<AreaButtonDelegate>
{
    __weak id<UserFormCellDelegate>delegate;
}
@property (nonatomic, strong) NSMutableArray *areaBtnArray;
@end

@implementation UserFormCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.backgroundColor = mainColor;
        NSArray *arr = @[@{@"image":@"fd_wait_pay",@"title":@"待付款"},@{@"image":@"fd_wait_send",@"title":@"待发货"},@{@"image":@"fd_wait_receive",@"title":@"待收货"},@{@"image":@"fd_wait_evaluate",@"title":@"待评价"}];
        
        CGFloat width = mainWidth/arr.count;
        self.areaBtnArray = [NSMutableArray array];
        for (int i = 0; i < arr.count; i++) {
            AreaButton *areaBtn = [[AreaButton alloc] initWithFrame:CGRectMake(width*i, 0, width, 80) Model:arr[i] Scale:0 fontSize:12];
            [areaBtn setTitleColor:[UIColor hexFloatColor:@"666666"]];
            areaBtn.index = i;
            areaBtn.delegate = self;
            [self.contentView addSubview:areaBtn];
            [self.areaBtnArray addObject:areaBtn];
        }
        
    }
    return self;
}
-(void)setNumberArray:(NSMutableArray *)numberArray{
    _numberArray = numberArray;
    for (int i= 0; i < numberArray.count; i++) {
        AreaButton *areaBtn = self.areaBtnArray[i];
        areaBtn.bageNumber = numberArray[i];
    }
    
}
- (void)areaButtonClickWithIndex:(NSInteger)index
{
    if (delegate) {
        [delegate formAreaClick:index];
    }
}

@end
