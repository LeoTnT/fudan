//
//  HistoryTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "UIImage+XSWebImage.h"


@interface HistoryTableViewCell()
{
//    UILabel *self.dateLabel;
//    UIView *self.topView;//头部view
    UIImageView *_logo;
    UILabel *_title;
    UILabel *_shopName;
    UILabel *_price;
    UILabel *_marketPrice;
    UIButton *_lookBtn;
    
    UIImageView *_tintImage;
    CGFloat _imageSize;
    CGFloat _buttonSize;
    CGFloat _circleButton;
    UILabel *line;
    
}

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIView *topView;//头部view
@end

@implementation HistoryTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle =UITableViewCellSelectionStyleNone;
        _imageSize = 80;
        _buttonSize = 30;
        _circleButton = 20;
        [self setSubviews];
        
    }
    return self;
}

-(void)lookShopPage {
    if ([self.historyDelegate respondsToSelector:@selector(tolookShopPageWithSupplyId:)]) {
        [self.historyDelegate tolookShopPageWithSupplyId:self.dataParser.supply_id];
    }
    
}
- (void)lookGoodsPage {
    if ([self.historyDelegate respondsToSelector:@selector(tolookGoodsPageWithGoodsId:)]) {
        [self.historyDelegate tolookGoodsPageWithGoodsId:self.dataParser.data_id];
    }
}
- (void)setSubviews {
    self.backgroundColor = BG_COLOR;
    self.topView =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 32.5)];
    [self.contentView addSubview:self.topView];

    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, CGRectGetWidth(self.topView.frame)-12*2, CGRectGetHeight(self.topView.frame))];
    self.dateLabel.font = [UIFont systemFontOfSize:14];
    self.dateLabel.textColor = COLOR_666666;
    self.dateBtn = [[UIButton alloc] initWithFrame:CGRectMake(13, 7.5, 19, 19)];
    [self.dateBtn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [self.dateBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    [self.dateBtn addTarget:self action:@selector(clickDateAction:) forControlEvents:UIControlEventTouchUpInside];
      self.dateBtn.hidden = YES;
    [self.topView addSubview:self.dateBtn];
    
    self.dateLabel.text = @"dsgfdgdfgsdfg";
    
    [self.topView addSubview:self.dateLabel];
  

    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topView.frame), mainWidth+2*13+19,105)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.selectedBtn = [[UIButton alloc] initWithFrame:CGRectMake(13, 43, 19, 19)];
    [self.selectedBtn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [self.selectedBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    [self.selectedBtn addTarget:self action:@selector(clickSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.selectedBtn];

    //图片
    _logo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, _imageSize, _imageSize)];
    _logo.contentMode = UIViewContentModeScaleToFill;
    [self.bottomView addSubview:_logo];
    _tintImage = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, NORMOL_SPACE*3, NORMOL_SPACE*3)];
    [self.bottomView addSubview:_tintImage];
    _title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_logo.frame)+11.5, CGRectGetMinY(_logo.frame), mainWidth-CGRectGetMaxX(_logo.frame)-11.5-16.5, 36.5)];
    _title.contentMode = UIViewContentModeTop;
    _title.font = [UIFont systemFontOfSize:16];
    _title.lineBreakMode = NSLineBreakByCharWrapping;
    _title.numberOfLines = 0;
    [self.bottomView addSubview:_title];

    _shopName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_title.frame), CGRectGetMaxY(_title.frame)+7, CGRectGetWidth(_title.frame), 12.5)];
    _shopName.textColor = COLOR_999999;
    _shopName.font = [UIFont systemFontOfSize:13];
    [self.bottomView addSubview:_shopName];

    _price = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_title.frame), CGRectGetMaxY(_shopName.frame)+9, CGRectGetWidth(self.frame)-_imageSize-NORMOL_SPACE*2, 12)];

    //现价，市场价
    _price.textColor = [UIColor redColor];
    _price.font = [UIFont systemFontOfSize:12];
    [self.bottomView addSubview:_price];
    _marketPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_price.frame), CGRectGetMaxY(_shopName.frame)+9, CGRectGetWidth(self.frame)-_imageSize-NORMOL_SPACE*2, 12)];
    _marketPrice.textColor = LINE_COLOR_NORMAL;
    _marketPrice.font = [UIFont systemFontOfSize:12];
    [self.bottomView addSubview:_marketPrice];

    _lookBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-13-66, 67, 66, 26)];
    _lookBtn.backgroundColor = [UIColor whiteColor];
    _lookBtn.layer.cornerRadius = 12;
    _lookBtn.layer.borderColor = [UIColor hexFloatColor:@"3F8EF7"].CGColor;
    _lookBtn.layer.borderWidth = 1;
    _lookBtn.layer.masksToBounds = YES;
    _lookBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [_lookBtn setTitle:@"再去看看" forState:UIControlStateNormal];
    [_lookBtn setTitleColor:[UIColor hexFloatColor:@"3F8EF7"] forState:UIControlStateNormal];
    [self.bottomView addSubview:_lookBtn];
    line = [[UILabel alloc] initWithFrame:CGRectMake(12, 104.5, mainWidth-12, 0.5)];
    [self.bottomView addSubview:line];
    line.backgroundColor = LINE_COLOR_NORMAL;
    [self.contentView addSubview:self.bottomView];

    self.cellHeight = CGRectGetMaxY(self.bottomView.frame);
    
}
- (void)clickTopView:(UITapGestureRecognizer *) tap {
    [self clickDateAction:self.dateBtn];
}

- (void)clickBottomView:(UITapGestureRecognizer *) tap {
    [self clickSelectedAction:self.selectedBtn];
    
}

- (void)clickDateAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        if ([self.historyDelegate respondsToSelector:@selector(deleteHistoryInfoWithDate:)]) {
            [self.historyDelegate deleteHistoryInfoWithDate:self.dataParser.w_time];
        }
    } else {
        if ([self.historyDelegate respondsToSelector:@selector(cancelHistoryInfoWithDate:)]) {
            [self.historyDelegate cancelHistoryInfoWithDate:self.dataParser.w_time];
        }
    }
    
}

- (void)clickSelectedAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        if ([self.historyDelegate respondsToSelector:@selector(deleteHistoryInfoWithStr:andType:)]) {
            [self.historyDelegate deleteHistoryInfoWithStr:self.dataParser.ID andType:self.dataParser.type];
        }
    }else{
        if ([self.historyDelegate respondsToSelector:@selector(cancelDeleteHistoryInfoWithStr:andType:)]) {
            [self.historyDelegate cancelDeleteHistoryInfoWithStr:self.dataParser.ID andType:self.dataParser.type];
        }
    }
    
}

- (void)setDataParser:(HistoryDataParser *)dataParser {
    _dataParser = dataParser;
    [_lookBtn removeTarget:self action:@selector(lookShopPage) forControlEvents:UIControlEventTouchUpInside];
    [_lookBtn removeTarget:self action:@selector(lookGoodsPage) forControlEvents:UIControlEventTouchUpInside];
    if (_dataParser.product_name) {
        [_logo getImageWithUrlStr:_dataParser.product_image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        _title.text = _dataParser.product_name;
        _shopName.text = _dataParser.product_supply_name;
        NSString *str3 = [NSString stringWithFormat:@"¥%.2f",[_dataParser.product_price floatValue]];
        _price.lineBreakMode = NSLineBreakByTruncatingTail;
        CGSize size3 = CGSizeMake(300, 20);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16],NSFontAttributeName,nil];
        CGSize  actualsize =[str3 boundingRectWithSize:size3 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic context:nil].size;
        NSMutableAttributedString *attStr1 = [[NSMutableAttributedString alloc] initWithString:str3];
        [attStr1 addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:16]
                        range:NSMakeRange(1, str3.length-1)];
        _price.attributedText = attStr1;
        _price.frame =CGRectMake(CGRectGetMinX(_title.frame), CGRectGetMaxY(_shopName.frame)+9, actualsize.width, 12);

        NSString *str2 = [NSString stringWithFormat:@"¥%.2f",[_dataParser.product_market_price floatValue]];
        UIFont *tFont2 = [UIFont systemFontOfSize:13];
        _marketPrice.font = tFont2;
        _marketPrice.lineBreakMode = NSLineBreakByTruncatingTail;
        // 文字中划线效果
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:str2 attributes:attribtDic];
        _marketPrice.attributedText =  attribtStr;
        CGSize size2 = CGSizeMake(300, 20);
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:tFont2,NSFontAttributeName,nil];
        CGSize  actualsize2 =[str2 boundingRectWithSize:size2 options:NSStringDrawingUsesLineFragmentOrigin  attributes:dic2 context:nil].size;
        _marketPrice.frame = CGRectMake(CGRectGetMaxX(_price.frame)+5, CGRectGetMaxY(_shopName.frame)+9, actualsize2.width, CGRectGetHeight(_price.frame));
        [_lookBtn addTarget:self action:@selector(lookGoodsPage) forControlEvents:UIControlEventTouchUpInside];
        _tintImage.image = [UIImage imageNamed:@"user_history_goods"];
    } else {
        [_logo getImageWithUrlStr:_dataParser.supply_logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        _title.text = _dataParser.supply_name;
        _price.hidden = YES;
        _marketPrice.hidden = YES;
        _shopName.hidden = YES;
        [_lookBtn addTarget:self action:@selector(lookShopPage) forControlEvents:UIControlEventTouchUpInside];
        _tintImage.image = [UIImage imageNamed:@"user_history_shop"];
    }

    if (self.isShow) {
        self.dateLabel.text = _dataParser.w_time;
        self.topView.hidden = NO;
        self.bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), mainWidth+2*13+19, 105);


        UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTopView:)];
        self.topView.userInteractionEnabled = YES;
        [self.topView addGestureRecognizer:taps];

    } else {
        self.topView.hidden = YES;
        self.bottomView.frame = CGRectMake(0, 0, mainWidth+2*13+19, 105);

    }
    self.selectedBtn.frame = CGRectMake(13, CGRectGetMaxY(self.bottomView.frame)-43-19, 19, 19);
    self.selectedBtn.selected = NO;
    self.dateBtn.hidden = YES;
    self.dateLabel.frame = CGRectMake(12, 0, mainWidth-12*2, 32.5);
    if (_isMoveShow) {
        CGRect frame = self.bottomView.frame;
        frame.origin.x+=13+19+13;
        self.bottomView.frame = frame;
        CGRect frame2 = self.dateLabel.frame;
        frame2.origin.x+=13+19+12.5;
        self.dateLabel.frame = frame2;
        self.dateBtn.hidden = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBottomView:)];
        self.bottomView.userInteractionEnabled = YES;
        [self.bottomView addGestureRecognizer:tap];

    } else {
        if (!isEmptyString(_dataParser.product_name)) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookGoodsPage)];
            self.bottomView.userInteractionEnabled = YES;
            [self.bottomView addGestureRecognizer:tap];
        } else {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookShopPage)];
            self.bottomView.userInteractionEnabled = YES;
            [self.bottomView addGestureRecognizer:tap];
        }
    }
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.cellHeight = CGRectGetMaxY(self.bottomView.frame);
}


@end
