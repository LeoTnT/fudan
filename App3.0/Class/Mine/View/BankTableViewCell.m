//
//  BankTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BankTableViewCell.h"
#import "UIImage+XSWebImage.h"

@interface BankTableViewCell(){
    UIImageView *_imgView;
    UILabel *_bankNameLabel;
    UILabel *_nameLabel;
    UILabel *_bankNumber;
    UIImageView *_img;//默认图标
    UIView *_hiddenView;
}
@end
@implementation BankTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifie {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifie];
    if (self) {
        [self setSubviews];
    }
    return self;
    
}

- (void)setIsShow:(BOOL)isShow {
    _isShow = isShow;
    _hiddenView.hidden = !_isShow;
}

- (void)setParser:(BankCardDataParser *)parser {
    _parser = parser;
    [_imgView getImageWithUrlStr:_parser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    _bankNameLabel.text = _parser.name;
    _nameLabel.text = _parser.bankname;
    if (![_parser.name isEqualToString:Localized(@"wechat")]&&(![_parser.name isEqualToString:Localized(@"alipay")])) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i=3; i<_parser.bankcard.length; i+=4) {
            [tempArray addObject:@"****"];
        }
        [tempArray removeLastObject];
        if (_parser.bankcard.length >4) {
            [tempArray addObject:[_parser.bankcard substringFromIndex:_parser.bankcard.length-4]];
        }else{
            [tempArray addObject:_parser.bankcard];
        }
        _bankNumber.text = [tempArray componentsJoinedByString:@" "];
        
    } else {
        _bankNumber.text = _parser.bankcard;
    }
    
    
    _img.hidden = ![_parser.is_default  integerValue];
}

- (void)setSubviews {
    CGFloat imageSize = 37,cellHeight = 110.5+12;
    UIColor *color = [UIColor whiteColor];
   self.backView = [[UIView alloc] initWithFrame:CGRectMake(12, 11,mainWidth-11-13 ,cellHeight-12 )];
    self.backView.layer.cornerRadius = 4;
    self.backView.layer.masksToBounds = YES;
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 14.5, imageSize, imageSize)];
    _imgView.contentMode = UIViewContentModeScaleAspectFit;
    [_imgView getImageWithUrlStr:nil andDefaultImage:[UIImage imageNamed:@"chat_single"]];
    [self.backView addSubview:_imgView];
    _bankNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+5, 16.5, mainWidth-CGRectGetWidth(_imgView.frame)-16-5-9.5-19, CGRectGetHeight(_imgView.frame)/2)];
    _bankNameLabel.font = [UIFont systemFontOfSize:15];
    _bankNameLabel.textColor = color;
    [self.backView addSubview:_bankNameLabel];
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_bankNameLabel.frame), CGRectGetMaxY(_bankNameLabel.frame), CGRectGetWidth(_bankNameLabel.frame)+19, CGRectGetHeight(_bankNameLabel.frame))];
    _nameLabel.font = [UIFont systemFontOfSize:12];
    _nameLabel.textColor = color;
    [self.backView addSubview:_nameLabel];
    _img= [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.backView.frame)-9.5-19, 7.5, 19, 19)];
    _img.image = [UIImage imageNamed:@"user_card_ok"];
    [self.backView addSubview:_img];
    _bankNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_bankNameLabel.frame), CGRectGetHeight(self.backView.frame)-21.5-30, CGRectGetWidth(_nameLabel.frame), 30)];
    _bankNumber.font = [UIFont systemFontOfSize:22];
    _bankNumber.textColor = color;
    
    [self.backView addSubview:_bankNumber];
    
    //设为默认，删除view
    _hiddenView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetHeight(self.backView.frame)-NORMOL_SPACE*4, CGRectGetWidth(self.backView.frame), NORMOL_SPACE*4)];
    _hiddenView.hidden = self.isShow;
    UIColor *hColor = [UIColor blackColor];
    _hiddenView.backgroundColor = [hColor colorWithAlphaComponent:0.5];
    UIButton *defaultBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_hiddenView.frame)/2, CGRectGetHeight(_hiddenView.frame))];
    defaultBtn.backgroundColor = [UIColor clearColor];
    [defaultBtn addTarget:self action:@selector(toDefaultBankAction:) forControlEvents:UIControlEventTouchUpInside];
    [defaultBtn setTitle:@"设置为默认账号" forState:UIControlStateNormal];
    [_hiddenView addSubview:defaultBtn];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(defaultBtn.frame), 0, 1, CGRectGetHeight(_hiddenView.frame))];
    line.backgroundColor = [UIColor whiteColor];
    [_hiddenView addSubview:line];
    UIButton *deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(defaultBtn.frame)+1, 0, CGRectGetWidth(_hiddenView.frame)/2-1, CGRectGetHeight(_hiddenView.frame))];
    deleteBtn.backgroundColor = [UIColor clearColor];
    [deleteBtn addTarget:self action:@selector(todeleteBankAction:) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [_hiddenView addSubview:deleteBtn];
    [_hiddenView addSubview:defaultBtn];
    [self.backView addSubview:_hiddenView];
    [self.contentView addSubview:self.backView];
}

- (void)toDefaultBankAction:(UIButton *)sender {
    if ([self.bankDelegate respondsToSelector:@selector(bankCardToDefault:)]) {
        [self.bankDelegate bankCardToDefault:self.parser];
    }
}

- (void)todeleteBankAction:(UIButton *)sender {
    if ([self.bankDelegate respondsToSelector:@selector(bankCardToDelete:)]) {
        [self.bankDelegate bankCardToDelete:self.parser];
    }
}
@end
