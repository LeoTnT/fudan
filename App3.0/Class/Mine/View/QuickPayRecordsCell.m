//
//  QuickPayRecordsCell.m
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "QuickPayRecordsCell.h"
#import "QuickPayRecordsModel.h"
#import "XSFormatterDate.h"

@interface QuickPayRecordsCell()
/** 店铺logo */
@property (nonatomic, strong) UIImageView * logoImageView;
/** 消费店铺 */
@property (nonatomic, strong) UILabel * titleLabel;
/** 买单时间 */
@property (nonatomic, strong) UILabel * timeLabel;
/** 消费金额 */
@property (nonatomic, strong) UILabel * moneyLabel;
/** 实付金额 */
@property (nonatomic, strong) UILabel * payMoneyLabel;
/** 省下金额 */
@property (nonatomic, strong) UILabel * saveLabel;
/** 支付编号 */
@property (nonatomic, strong) UILabel * payNoLabel;


@end

@implementation QuickPayRecordsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView * bg1View = [[UIView alloc] init];
        bg1View.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:bg1View];
        [bg1View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self);
            make.height.mas_equalTo(157);
        }];
        
        UIView * bg2View = [[UIView alloc] init];
        bg2View.backgroundColor = BG_COLOR;
        [self.contentView addSubview:bg2View];
        [bg2View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(bg1View);
            make.top.mas_equalTo(bg1View.mas_bottom);
            make.height.mas_equalTo(10);
        }];
        
        _logoImageView = [UIImageView new];
        _logoImageView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_logoImageView];
        
        [_logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(14);
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.size.mas_equalTo(CGSizeMake(16, 16));
        }];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.textColor = [UIColor hexFloatColor:@"171717"];
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_logoImageView.mas_right).offset(6);
            make.centerY.mas_equalTo(_logoImageView);
            make.width.mas_lessThanOrEqualTo(160);
        }];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = [UIColor hexFloatColor:@"999999"];
        [self.contentView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-12);
            make.centerY.mas_equalTo(_logoImageView);
            make.width.mas_lessThanOrEqualTo(160);
        }];
        
        UIView * line1V = [UIView new];
        line1V.backgroundColor = [UIColor hexFloatColor:@"e6e6e6"];
        [self.contentView addSubview:line1V];
        
        [line1V mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.right.mas_equalTo(self).offset(-12);
            make.top.mas_equalTo(self).offset(43.5f);
            make.height.mas_equalTo(0.5f);
        }];
        
        UIView * zhan1View = [[UIView alloc] init];
        zhan1View.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:zhan1View];
        [zhan1View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(54.5);
            make.size.mas_equalTo(CGSizeMake(57, 48));
            make.centerX.mas_equalTo(self.mas_centerX).multipliedBy(0.5f);
        }];
    
        _moneyLabel = [[UILabel alloc] init];
        _moneyLabel.font = [UIFont systemFontOfSize:24];
        _moneyLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:_moneyLabel];
        
        [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(zhan1View);
//            make.height.mas_equalTo(28);
        }];

        //¥
        UILabel * symbol1Lab = [[UILabel alloc] init];
        symbol1Lab.font = [UIFont systemFontOfSize:14];
        symbol1Lab.textColor = [UIColor blackColor];
        symbol1Lab.text = @"¥";
        [self.contentView addSubview:symbol1Lab];
        
        [symbol1Lab mas_makeConstraints:^(MASConstraintMaker *make){
            make.bottom.mas_equalTo(_moneyLabel.mas_bottom).offset(-2);
            make.right.mas_equalTo(_moneyLabel.mas_left).offset(-2);
        }];
        
        //消费金额
        UILabel * consumeLab = [[UILabel alloc] init];
        consumeLab.font = [UIFont systemFontOfSize:12];
        consumeLab.textColor = [UIColor hexFloatColor:@"999999"];
        consumeLab.text = @"消费金额";
        [self.contentView addSubview:consumeLab];
        
        [consumeLab mas_makeConstraints:^(MASConstraintMaker *make){
//            make.centerX.mas_equalTo(self.centerX).multipliedBy(0.5f);
//            make.top.mas_equalTo(_moneyLabel.bottom).offset(6);
            make.right.mas_equalTo(_moneyLabel);
            make.bottom.mas_equalTo(zhan1View);
            make.centerX.mas_equalTo(zhan1View);
        }];
        
        UIView * zhan2View = [[UIView alloc] init];
        zhan2View.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:zhan2View];
        [zhan2View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(54.5);
            make.size.mas_equalTo(CGSizeMake(57, 48));
            make.centerX.mas_equalTo(self.mas_centerX).multipliedBy(1.5f);
        }];
        
        _payMoneyLabel = [[UILabel alloc] init];
        _payMoneyLabel.font = [UIFont systemFontOfSize:24];
        _payMoneyLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:_payMoneyLabel];
        
        [_payMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(zhan2View);
//            make.top.mas_equalTo(_moneyLabel);
//            make.height.mas_equalTo(30);
        }];
        
        //¥
        UILabel * symbol2Lab = [[UILabel alloc] init];
        symbol2Lab.font = [UIFont systemFontOfSize:14];
        symbol2Lab.textColor = [UIColor blackColor];
        symbol2Lab.text = @"¥";
        [self.contentView addSubview:symbol2Lab];
        
        [symbol2Lab mas_makeConstraints:^(MASConstraintMaker *make){
            make.bottom.mas_equalTo(_payMoneyLabel.mas_bottom).offset(-2);
            make.right.mas_equalTo(_payMoneyLabel.mas_left).offset(-2);
        }];
        
        //实付金额
        UILabel * consume2Lab = [[UILabel alloc] init];
        consume2Lab.font = [UIFont systemFontOfSize:12];
        consume2Lab.textColor = [UIColor hexFloatColor:@"999999"];
        consume2Lab.text = @"实付金额";
        [self.contentView addSubview:consume2Lab];
        
        [consume2Lab mas_makeConstraints:^(MASConstraintMaker *make){
            make.right.mas_equalTo(_payMoneyLabel);
            make.bottom.mas_equalTo(zhan2View);
            make.centerX.mas_equalTo(zhan2View);
        }];
        
        //省多少
        UIView * saveView = [[UIView alloc] init];
        saveView.backgroundColor = [UIColor hexFloatColor:@"3DB443"];
        [self.contentView addSubview:saveView];
        [saveView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_payMoneyLabel.mas_right).offset(5);
            make.height.mas_equalTo(16);
            make.centerY.mas_equalTo(_payMoneyLabel);
        }];
        
        _saveLabel = [[UILabel alloc] init];
        _saveLabel.font = [UIFont systemFontOfSize:12];
        _saveLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview:_saveLabel];
        
        [_saveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.centerX.mas_equalTo(saveView);
            make.width.mas_equalTo(saveView).offset(-6);
            //            make.top.mas_equalTo(_moneyLabel);
            //            make.height.mas_equalTo(30);
        }];
        
        UIView * line2V = [UIView new];
        line2V.backgroundColor = [UIColor hexFloatColor:@"e6e6e6"];
        [self.contentView addSubview:line2V];
        
        [line2V mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.right.mas_equalTo(self).offset(-12);
            make.top.mas_equalTo(self.mas_bottom).offset(-54);
            make.height.mas_equalTo(0.5f);
        }];
        
        //支付编号
        UILabel * payNumLab = [[UILabel alloc] init];
        payNumLab.font = [UIFont systemFontOfSize:13];
        payNumLab.textColor = [UIColor hexFloatColor:@"999999"];
        payNumLab.text = @"支付编号";
        [self.contentView addSubview:payNumLab];
        
        [payNumLab mas_makeConstraints:^(MASConstraintMaker *make){
            make.left.mas_equalTo(self).offset(12);
            make.bottom.mas_equalTo(self.mas_bottom).offset(-25);
        }];
        
        _payNoLabel = [[UILabel alloc] init];
        _payNoLabel.font = [UIFont systemFontOfSize:13];
        _payNoLabel.textColor = [UIColor hexFloatColor:@"171717"];
        [self.contentView addSubview:_payNoLabel];
        
        [_payNoLabel mas_makeConstraints:^(MASConstraintMaker *make){
            make.left.mas_equalTo(payNumLab.mas_right).offset(6);
            make.centerY.mas_equalTo(payNumLab);
        }];
        
    }
    return self;
}

-(void)setDataToCellWithModel:(QuickPayRecordsModel *)model
{
    [self.logoImageView getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.titleLabel.text = model.name;
    self.timeLabel.text = [XSFormatterDate dateWithTimeIntervalString:model.pay_time];
    self.moneyLabel.text = model.price_total;
    self.payMoneyLabel.text = model.price_pay;
    self.payNoLabel.text = model.pay_no;
    self.saveLabel.text = [NSString stringWithFormat:@"省%@元",model.price_discount];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
