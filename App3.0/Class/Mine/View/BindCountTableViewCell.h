//
//  BindCountTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/6/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BindCountModel.h"
@protocol BindCountDelegate<NSObject>

- (void)bindSocialWithType:(NSString *) type;
- (void)cancelBindWithType:(NSString *) type;
- (void)changeSocialWithType:(NSString *) type;
@end
@interface BindCountTableViewCell : UITableViewCell

@property (nonatomic,strong) BindCountParser *bindCountParser;
@property (nonatomic,weak) id<BindCountDelegate> delegate;

@end
