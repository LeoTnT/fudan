//
//  BankCardTypeView.m
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BankCardTypeView.h"


@interface BankCardTypeView ()<UIScrollViewDelegate>
{
    CardTypeDataParser *_dataParser;//选中的类型
    BusinessGoodsSellTypeParser *_sellTypeParser;//选中的类型
    OrderCouponDataDetailParser *_couponParser;//选中的类型
    NSDictionary *_selectedType;
    
}
@property(nonatomic,assign) CGFloat height;
@property (nonatomic, assign) CGFloat celHeight;
@property (nonatomic, assign) CGFloat topSpace;
@end

@implementation BankCardTypeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.celHeight = 50;
        self.topSpace = 150;
        self.height = 39;
    }
    return self;
}

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.backgroundColor  = [UIColor whiteColor];
        //        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = YES;
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}
-(void)setTypeArray:(NSMutableArray *)typeArray{
    _typeArray = typeArray;
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    if (self.viewType==AlertTypeBankCard) {
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        UILabel *please = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2,self.topSpace-self.celHeight , mainWidth-self.celHeight, self.celHeight)];
        please.backgroundColor = mainColor;
        please.text = Localized(@"choose");
        please.textAlignment = NSTextAlignmentCenter;
        please.font = [UIFont boldSystemFontOfSize:20];
        please.textColor = [UIColor whiteColor];
        UITapGestureRecognizer *tempTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(temp:)] ;
        please.userInteractionEnabled = YES;
        [please addGestureRecognizer:tempTap];
        [self addSubview:please];
        self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, CGRectGetHeight(self.frame)-self.topSpace-2*NORMOL_SPACE);
        
        [self addSubview:self.scrollView];
        for (UIView *view in self.scrollView.subviews) {
            [view removeFromSuperview];
        }
        //类型view
        for (int i=0;i<_typeArray.count; i++) {
            
            UIView *cView = [[UIView alloc] initWithFrame:CGRectMake(0, i*(self.celHeight+1), CGRectGetWidth(self.scrollView.frame), self.celHeight+1)];
            cView.backgroundColor = [UIColor whiteColor];
            //标题，按钮
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2, 0, CGRectGetWidth(cView.frame)/3*2, self.celHeight)];
            CardTypeDataParser *parser = [_typeArray objectAtIndex:i];
            title.text = parser.name;
            [cView addSubview:title];
            UIButton *selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(cView.frame)-self.celHeight,NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
            [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_no"] forState:UIControlStateNormal];
            [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_yes"] forState:UIControlStateSelected];
            selectBtn.tag = 999+i;
            [selectBtn addTarget:self action:@selector(selectedAction:) forControlEvents:UIControlEventTouchUpInside];
            [cView addSubview:selectBtn];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedView:)] ;
            cView.userInteractionEnabled = YES;
            [cView addGestureRecognizer:tap];
            if ([self.selectedId isEqual:parser.ID]) {
                selectBtn.selected = YES;
            }
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.celHeight, CGRectGetWidth(cView.frame), 1)];
            line.backgroundColor = LINE_COLOR;
            [cView addSubview:line];
            [self.scrollView addSubview:cView];
            //        [self addSubview:line];
        }
        if (_typeArray.count*(self.celHeight+1)<=CGRectGetHeight(self.scrollView.frame)) {
            self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, _typeArray.count*(self.celHeight+1));
        }
        self.scrollView.contentSize = CGSizeMake(0, _typeArray.count*(self.celHeight+1));
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenView:)] ;
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
        
    } else if (self.viewType==AlertTypeBankOrderRejectReason){
        self.backgroundColor = BG_COLOR;
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        self.scrollView.frame = CGRectMake(0,0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
        [self addSubview:self.scrollView];
        for (UIView *view in self.scrollView.subviews) {
            [view removeFromSuperview];
        }
        //退款原因
        for (int i=0;i<_typeArray.count; i++) {
            self.height = 40;
            UIView *cView = [[UIView alloc] initWithFrame:CGRectMake(0, i*self.height, CGRectGetWidth(self.scrollView.frame), self.height)];
            cView .tag = 555+i;
            //标题，按钮
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, 0, CGRectGetWidth(cView.frame)-NORMOL_SPACE*4, self.height)];
            NSDictionary *data = [_typeArray objectAtIndex:i];
            title.text = data.allValues.firstObject;
            [cView addSubview:title];
            [self.scrollView addSubview:cView];
            
        }
        if (_typeArray.count*self.height<=CGRectGetHeight(self.scrollView.frame)) {
            self.scrollView.frame = CGRectMake(0,0 , CGRectGetWidth(self.frame), _typeArray.count*self.height);
        }
        self.scrollView.contentSize = CGSizeMake(0, _typeArray.count*self.height);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenReasonView:)] ;
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
    } else if (self.viewType==AlertTypeSellType) {
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        UILabel *please = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2,self.topSpace-self.celHeight , mainWidth-self.celHeight, self.celHeight)];
        please.backgroundColor = mainColor;
        please.text = Localized(@"choose");
        please.textAlignment = NSTextAlignmentCenter;
        please.font = [UIFont boldSystemFontOfSize:20];
        please.textColor = [UIColor whiteColor];
        UITapGestureRecognizer *tempTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(temp2:)] ;
        please.userInteractionEnabled = YES;
        [please addGestureRecognizer:tempTap];
        [self addSubview:please];
        self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, CGRectGetHeight(self.frame)-self.topSpace-2*NORMOL_SPACE);
        
        [self addSubview:self.scrollView];
        for (UIView *view in self.scrollView.subviews) {
            [view removeFromSuperview];
        }
        //类型view
        for (int i=0;i<_typeArray.count; i++) {
            
            UIView *cView = [[UIView alloc] initWithFrame:CGRectMake(0, i*(self.celHeight+1), CGRectGetWidth(self.scrollView.frame), self.celHeight+1)];
            cView.backgroundColor = [UIColor whiteColor];
            //标题，按钮
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2, 0, CGRectGetWidth(cView.frame)/3*2, self.celHeight)];
            BusinessGoodsSellTypeParser *parser = [_typeArray objectAtIndex:i];
            title.text = parser.name;
            [cView addSubview:title];
            UIButton *selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(cView.frame)-self.celHeight,NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
            [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_no"] forState:UIControlStateNormal];
            [selectBtn setImage:[UIImage imageNamed:@"user_wallet_check_yes"] forState:UIControlStateSelected];
            selectBtn.tag = 999+i;
            [selectBtn addTarget:self action:@selector(selectedAction2:) forControlEvents:UIControlEventTouchUpInside];
            [cView addSubview:selectBtn];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedView2:)] ;
            cView.userInteractionEnabled = YES;
            [cView addGestureRecognizer:tap];
            if ([self.selectedId isEqual:parser.ID]) {
                selectBtn.selected = YES;
            }
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.celHeight, CGRectGetWidth(cView.frame), 1)];
            line.backgroundColor = LINE_COLOR;
            [cView addSubview:line];
            [self.scrollView addSubview:cView];
            
        }
        if (_typeArray.count*(self.celHeight+1)<=CGRectGetHeight(self.scrollView.frame)) {
            self.scrollView.frame = CGRectMake(CGRectGetMinX(please.frame),CGRectGetMaxY(please.frame) , mainWidth-self.celHeight, _typeArray.count*(self.celHeight+1));
        }
        self.scrollView.contentSize = CGSizeMake(0, _typeArray.count*(self.celHeight+1));
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenView2:)] ;
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
    } else if (self.viewType==AlertTypeCouponType) {
        //抵扣券
        self.celHeight = 44;
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.65];
        
        CGFloat sHeight = mainHeight-44-20;
        self.scrollView.frame = CGRectMake(0,20 , mainWidth, sHeight);
        
        [self addSubview:self.scrollView];
        for (UIView *view in self.scrollView.subviews) {
            [view removeFromSuperview];
        }
        
        //类型view
        for (int i=0;i<_typeArray.count; i++) {
            
            UIView *cView = [[UIView alloc] initWithFrame:CGRectMake(0, i*(self.celHeight+1), CGRectGetWidth(self.scrollView.frame), self.celHeight+1)];
            cView.backgroundColor = [UIColor whiteColor];
            //标题，按钮
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.celHeight/2, 0, CGRectGetWidth(cView.frame)/3*2, self.celHeight)];
            title.textColor = COLOR_999999;
            title.font = [UIFont systemFontOfSize:13];
            OrderCouponDataDetailParser *parser = [_typeArray objectAtIndex:i];
            NSString *titleString = @"";
            if ([parser.ID isEqualToString:@"no_use"]) {
                titleString = [NSString stringWithFormat:@"%@ %@",parser.limit_amount,parser.limit_amount_str];
            } else {
                titleString = [NSString stringWithFormat:@"%@元 %@",parser.limit_amount,parser.limit_amount_str];
            }
            
            
            NSMutableAttributedString *attStr1 = [[NSMutableAttributedString alloc] initWithString:titleString];
            [attStr1 addAttribute:NSForegroundColorAttributeName
                            value:[UIColor hexFloatColor:@"111111"]
                            range:NSMakeRange(0, parser.limit_amount.length+1)];
            [attStr1 addAttribute:NSFontAttributeName
                            value:[UIFont systemFontOfSize:15]
                            range:NSMakeRange(0, parser.limit_amount.length+1)];
            title.attributedText = attStr1;
            
            [cView addSubview:title];
            
            
            UIButton *selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(cView.frame)-self.celHeight,NORMOL_SPACE, NORMOL_SPACE*2, NORMOL_SPACE*2)];
            [selectBtn setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
            [selectBtn setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
            selectBtn.tag = 999+i;
            [selectBtn addTarget:self action:@selector(selectedCouponAction:) forControlEvents:UIControlEventTouchUpInside];
            [cView addSubview:selectBtn];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedCouponView:)] ;
            cView.userInteractionEnabled = YES;
            [cView addGestureRecognizer:tap];
            if ([self.selectedId isEqual:parser.ID]) {
                selectBtn.selected = YES;
            }
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.celHeight, CGRectGetWidth(cView.frame), 1)];
            line.backgroundColor = LINE_COLOR_NORMAL;
            [cView addSubview:line];
            [self.scrollView addSubview:cView];
        }
        CGFloat couponHeight = _typeArray.count*(self.celHeight+1);
        UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(12, couponHeight+13, mainWidth-12*2, 44)];
        [okButton setBackgroundColor:mainColor];
        [okButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        [okButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateSelected];
        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        okButton.titleLabel.font = [UIFont systemFontOfSize:16];
        okButton.layer.cornerRadius = 4;
        okButton.layer.masksToBounds = YES;
        [self.scrollView addSubview:okButton];
        [okButton addTarget:self action:@selector(okCouponAction:) forControlEvents:UIControlEventTouchUpInside];
        if (couponHeight+13+13+44<=CGRectGetHeight(self.scrollView.frame)) {
            self.scrollView.frame = CGRectMake(0,mainHeight-couponHeight-13-13-44, mainWidth,couponHeight+13+13+44 );
        }
        self.scrollView.contentSize = CGSizeMake(0, _typeArray.count*(self.celHeight+1)+13+13+44);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenCouponView:)] ;
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
        UILabel *please = [[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMinY(self.scrollView.frame)-44, mainWidth, self.celHeight)];
        please.backgroundColor = [UIColor hexFloatColor:@"EAEAEA"];
        please.text = @"选择抵扣券";
        please.textAlignment = NSTextAlignmentCenter;
        please.font = [UIFont boldSystemFontOfSize:18];
        please.textColor = [UIColor hexFloatColor:@"111111"];
        UITapGestureRecognizer *tempTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(temp:)] ;
        please.userInteractionEnabled = YES;
        [please addGestureRecognizer:tempTap];
        [self addSubview:please];
    }
}



- (void)okCouponAction:(UIButton *) sender {
    
    //抵扣券确定
    if (_couponParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCouponView:coupon:)]) {
            [self.typeDelegate hiddenCouponView:_couponParser.ID coupon:_couponParser.limit_amount];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCouponView:coupon:)]) {
            [self.typeDelegate hiddenCouponView:nil coupon:nil];
        }
    }
    
}
-(void)hiddenCouponView:(UITapGestureRecognizer *) tap{
    //    if (_couponParser) {
    //        if ([self.typeDelegate respondsToSelector:@selector(hiddenCouponView:coupon:)]) {
    //            [self.typeDelegate hiddenCouponView:_couponParser.ID coupon:_couponParser.limit_amount];
    //        }
    //    } else {
    if ([self.typeDelegate respondsToSelector:@selector(hiddenCouponView)]) {
        [self.typeDelegate hiddenCouponView];
    }
    //    }
    
}

- (void)selectedCouponView:(UITapGestureRecognizer *) tap {
    for (UIView *view in tap.view.subviews) {
        if (view.tag>=999) {
            [self selectedCouponAction:(UIButton *) view];
            break;
        }
    }
    
}
-(void)selectedCouponAction:(UIButton *) sender{
    for (int i=999; i<999+_typeArray.count; i++) {
        
        UIButton *btn = [self viewWithTag:i];
        if (btn.tag==sender.tag) {
            btn.selected = !btn.selected;
            if (btn.selected) {
                if (self.viewType==AlertTypeCouponType) {
                    _couponParser = _typeArray[sender.tag-999];
                }
            } else {
                
                _couponParser = nil;
                
            }
        } else {
            if (btn.selected) {
                btn.selected= !btn.selected;
            }
        }
    }
    
}



-(void)temp:(UITapGestureRecognizer *) tap {
    NSLog(@"");
}
-(void)hiddenView:(UITapGestureRecognizer *) tap{
    if (_dataParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:_dataParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:nil];
        }
    }
    
    NSLog(@"消失");
}
-(void)hiddenReasonView:(UITapGestureRecognizer *) tap{
    CGPoint point = [tap locationInView:self];
    NSUInteger index = (point.y)/(self.height+1);
    _selectedType = _typeArray[index];
    
    if (_selectedType) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenReasonView:)]) {
            [self.typeDelegate hiddenReasonView:_selectedType];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenReasonView:)]) {
            [self.typeDelegate hiddenReasonView:nil];
        }
    }
    
    NSLog(@"消失");
}

- (void)selectedView:(UITapGestureRecognizer *) tap {
    for (UIView *view in tap.view.subviews) {
        if (view.tag>=999) {
            [self selectedAction:(UIButton *) view];
            break;
        }
    }
    
}
-(void)selectedAction:(UIButton *) sender{
    //    sender.selected = !sender.selected;
    for (int i=999; i<999+_typeArray.count; i++) {
        
        UIButton *btn = [self viewWithTag:i];
        if (btn.tag==sender.tag) {
            btn.selected = !btn.selected;
        } else {
            if (btn.selected) {
                btn.selected= !btn.selected;
            }
        }
    }
    if (self.viewType==AlertTypeBankCard) {
        _dataParser = _typeArray[sender.tag-999];
    }
    if (_dataParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:_dataParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenCardView:)]) {
            [self.typeDelegate hiddenCardView:nil];
        }
    }
    
    
    
}
-(void)temp2:(UITapGestureRecognizer *) tap {
    if (_sellTypeParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:_sellTypeParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:nil];
        }
    }
}
- (void)selectedView2:(UITapGestureRecognizer *) tap {
    for (UIView *view in tap.view.subviews) {
        if (view.tag>=999) {
            [self selectedAction2:(UIButton *) view];
            break;
        }
    }
    
}
-(void)selectedAction2:(UIButton *) sender{
    //    sender.selected = !sender.selected;
    for (int i=999; i<999+_typeArray.count; i++) {
        
        UIButton *btn = [self viewWithTag:i];
        if (btn.tag==sender.tag) {
            btn.selected = !btn.selected;
        } else {
            if (btn.selected) {
                btn.selected= !btn.selected;
            }
        }
    }
    if (self.viewType==AlertTypeSellType) {
        _sellTypeParser = _typeArray[sender.tag-999];
    }
    if (_sellTypeParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:_sellTypeParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:nil];
        }
    }
}
-(void)hiddenView2:(UITapGestureRecognizer *) tap{
    if (_sellTypeParser) {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:_sellTypeParser];
        }
    } else {
        if ([self.typeDelegate respondsToSelector:@selector(hiddenSellTypeView:)]) {
            [self.typeDelegate hiddenSellTypeView:nil];
        }
    }
}
@end
