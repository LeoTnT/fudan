//
//  HistoryTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryModel.h"

@protocol HistoryDelegate <NSObject>
@optional
/**删除历史信息*/
- (void)deleteHistoryInfoWithStr: (NSString *) idsStr andType: (NSString *) type;
//取消删除历史信息
- (void)cancelDeleteHistoryInfoWithStr: (NSString *) idsStr andType: (NSString *) type;
@optional
/**查看商品详情*/
- (void)tolookGoodsPageWithGoodsId: (NSString *) goodsId;
@optional
/**查看店铺首页*/
- (void)tolookShopPageWithSupplyId: (NSString *) supplyId;

/**根据日期删除历史信息*/
- (void)deleteHistoryInfoWithDate:(NSString *) dateString;
/**根据日期取消删除历史信息*/
- (void)cancelHistoryInfoWithDate:(NSString *) dateString;
@end

@interface HistoryTableViewCell : UITableViewCell
@property (nonatomic, strong) HistoryDataParser *dataParser;
@property (nonatomic, assign) CGFloat cellHeight;//表格高度
@property (nonatomic, assign) BOOL isShow;//是否显示日期label
@property (nonatomic, strong) UIButton *selectedBtn;//圆圈
@property (nonatomic, strong) UIButton *dateBtn;//日期批量
@property (nonatomic, strong) UIView *bottomView;//底部view
@property (nonatomic, assign) BOOL isMoveShow;//删除按钮是否可见
@property (nonatomic, weak) id<HistoryDelegate> historyDelegate;
- (void)clickSelectedAction:(UIButton *)sender;
@end
