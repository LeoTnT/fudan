//
//  UserApplicationCell.h
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserApplicationCellDelegate <NSObject>
- (void)appAreaClick:(NSInteger)index;
@end

@interface UserApplicationCell : UITableViewCell
@property(nonatomic, weak) id<UserApplicationCellDelegate>delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataArray:(NSArray *)dataArray;
@end
