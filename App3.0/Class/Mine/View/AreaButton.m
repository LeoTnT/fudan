//
//  AreaButton.m
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AreaButton.h"

@interface AreaButton()
{
    __weak id<AreaButtonDelegate>delegate;
}
@property (nonatomic, strong) UILabel *bageLabel;
@end
@implementation AreaButton
@synthesize delegate,index;
- (AreaButton *)initWithFrame:(CGRect)frame Model:(NSDictionary *)model Scale:(CGFloat)scale fontSize:(CGFloat)font
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setImage:[UIImage imageNamed:model[@"image"]]];
        [self addSubview:imageView];
        
        //title
        self.title = [[UILabel alloc] init];
        [self.title setText:model[@"title"]];
        [self.title setFont:[UIFont systemFontOfSize:font]];
        self.title.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.title];
        
        self.bageLabel = [[UILabel alloc] init];
        self.bageLabel.layer.masksToBounds = YES;
        self.bageLabel.layer.borderWidth = 1;
        self.bageLabel.layer.borderColor = [UIColor redColor].CGColor;
        self.bageLabel.layer.cornerRadius = 9;
        self.bageLabel.font = [UIFont systemFontOfSize:12];
        self.bageLabel.textAlignment = NSTextAlignmentCenter;
        self.bageLabel.textColor = [UIColor redColor];
        self.bageLabel.backgroundColor = [UIColor whiteColor];
        self.bageLabel.hidden = YES;
        [imageView addSubview:self.bageLabel];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.centerY.mas_equalTo(self.mas_centerY).offset(-10);
        }];
        
        [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.top.mas_equalTo(imageView.mas_bottom).offset(10);
        }];
        
        [self.bageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(imageView.mas_right);
            make.centerY.mas_equalTo(imageView.mas_top).offset(2);
            make.width.height.mas_greaterThanOrEqualTo(18);
        }];
        
        [self addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)btnAction {
    if (delegate) {
        [delegate areaButtonClickWithIndex:self.index];
    }
}

- (void)setTitleColor:(UIColor *)color
{
    [self.title setTextColor:color];
}

- (void)setBageNumber:(NSString *)bageNumber {
    _bageNumber = bageNumber;
    if ([bageNumber integerValue] <= 0) {
        self.bageLabel.hidden = YES;
    } else {
        self.bageLabel.hidden = NO;
        if ([bageNumber integerValue] > 99) {
            self.bageLabel.text = @"99";
        } else {
            self.bageLabel.text = bageNumber;
        }
    }
}
@end
