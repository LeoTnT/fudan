//
//  ShopTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"
@protocol ShopDelegate<NSObject>
@optional
-(void)clickToEnjoyShopWithId:(NSString *) favId;
@optional
-(void)clickToCancelCollectWithId:(NSString *) favId;
@end

@interface ShopTableViewCell : UITableViewCell
@property(nonatomic,strong) ShopDataParser *dataParser;
@property(nonatomic,weak) id<ShopDelegate> shopDelegate;
@end
