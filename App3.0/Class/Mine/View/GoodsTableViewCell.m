//
//  GoodsTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsTableViewCell.h"
#import "XSCustomButton.h"
#import "UIImage+XSWebImage.h"

@implementation GoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //子视图
        [self setSubViews];
    }
    return self;
}
#pragma mark - 添加子视图
- (void)setSubViews {
    CGFloat imageSize = 80,buttonHeight = 20,buttonWidth = 60,descHeight = 40,priceHeight = 20;
    UIView *spaceView = [[UIView alloc] init];
    spaceView.backgroundColor = BG_COLOR;
    self.contentView.userInteractionEnabled = YES;
    [self.contentView addSubview:spaceView];
    
    //顶部 店铺名  时间
    UIView *topView = [[UIView alloc] init];
    [self.contentView addSubview:topView];
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.textColor=LINE_COLOR;
    [topView addSubview:self.titleLabel];
    [topView addSubview:self.timeLabel];
    
    //分割线
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = LINE_COLOR_NORMAL;
    [self.contentView addSubview:lineView];
    
    //下部 图片 描述 价格 移除收藏按钮 立即购买按钮 分享按钮
    self.imgView = [[UIImageView alloc] init];
    self.descTextView = [[UITextView alloc] init];
    self.descTextView.userInteractionEnabled = NO;
    self.descTextView.showsVerticalScrollIndicator = NO;
    self.descTextView.showsHorizontalScrollIndicator = NO;
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = [UIFont systemFontOfSize:20];
    self.priceLabel.textColor = mainColor;
    self.deleteCollectionBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.imgView.frame)+NORMOL_SPACE, CGRectGetMaxY(self.imgView.frame)-buttonHeight, buttonWidth, buttonHeight) title:@"移除收藏" titleColor:[UIColor grayColor] fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:LINE_COLOR highBackgroundColor:[UIColor whiteColor]];
    [self.deleteCollectionBtn setBorderWith:1 borderColor:[LINE_COLOR CGColor] cornerRadius:5];
    [self.deleteCollectionBtn addTarget:self action:@selector(deleteCollectionAction:) forControlEvents:UIControlEventTouchUpInside];
    self.deleteCollectionBtn.userInteractionEnabled = YES;
    
    self.buyBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.deleteCollectionBtn.frame)+NORMOL_SPACE, CGRectGetMinY(self.deleteCollectionBtn.frame), buttonWidth, buttonHeight) title:@"立即购买" titleColor:mainColor fontSize:12 backgroundColor:[UIColor whiteColor] higTitleColor:mainColor highBackgroundColor:mainColor];
    [self.buyBtn setBorderWith:1 borderColor:[mainColor CGColor] cornerRadius:5];
    [self.buyBtn addTarget:self action:@selector(buyCollectGoodsAction:) forControlEvents:UIControlEventTouchUpInside];
    self.enjoyBtn = [[UIButton alloc] init];
    [self.enjoyBtn addTarget:self action:@selector(enjoyCollectGoodsAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.enjoyBtn setBackgroundImage:[UIImage imageNamed:@"user_collect_share"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.imgView];
    [self.contentView addSubview:self.descTextView];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.deleteCollectionBtn];
    [self.contentView addSubview:self.buyBtn];
    //    [self.contentView addSubview:self.enjoyBtn];
    
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(spaceView.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(40);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_lessThanOrEqualTo((mainWidth-3*10)/2);
        make.right.mas_equalTo(topView).with.mas_offset(-10);
        make.top.bottom.mas_equalTo(topView);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topView).with.mas_offset(10);
        make.right.mas_equalTo(self.timeLabel.mas_left).with.mas_offset(-10);
        make.top.bottom.mas_equalTo(topView);
    }];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_bottom);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).with.mas_offset(10);
        make.left.mas_equalTo(10);
        make.width.height.mas_equalTo(imageSize);
    }];
    [self.descTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imgView.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(topView.mas_bottom).with.mas_offset(10);
        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
        make.height.mas_equalTo(descHeight);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.descTextView);
        make.top.mas_equalTo(self.descTextView.mas_bottom);
        make.height.mas_equalTo(priceHeight);
    }];
    [self.deleteCollectionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imgView.mas_right).with.mas_offset(10);
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(buttonHeight);
        make.top.mas_equalTo(self.imgView.mas_bottom).with.mas_offset(-buttonHeight/2);
    }];
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.deleteCollectionBtn.mas_right).with.mas_offset(10);
        make.width.top.height.mas_equalTo(self.deleteCollectionBtn);
    }];
    //    [self.enjoyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.right.mas_equalTo(self.contentView).with.mas_offset(-10);
    //        make.width.height.mas_equalTo(buttonHeight);
    //        make.top.mas_equalTo(self.deleteCollectionBtn);
    //    }];
}

- (void)deleteCollectionAction:(UIButton *)sender {
    if ([self.collectDelegate respondsToSelector:@selector(deleteCollectionWithId:)]) {
        [self.collectDelegate deleteCollectionWithId:self.parser.product_id];
    }
}

- (void)buyCollectGoodsAction:(UIButton *)sender {
    if ([self.collectDelegate respondsToSelector:@selector(clickToGoodsDetailPage:goodsName:)]) {
        [self.collectDelegate clickToGoodsDetailPage:self.parser.product_id goodsName:self.parser.product_name];
    }
}

- (void)enjoyCollectGoodsAction:(UIButton *)sender{
    
    
}

#pragma mark - 根据model，设置内容
- (void)setParser:(CollectParser *)parser {
    _parser = parser;
    //设置内容
    self.titleLabel.text = _parser.name;
    
    //时间戳转为时间
    self.timeLabel.text = [XSFormatterDate dateWithTimeIntervalString:_parser.w_time];
    [self.imgView getImageWithUrlStr:_parser.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.imgView.contentMode = UIViewContentModeScaleAspectFit;
    self.descTextView.text = _parser.product_name;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",_parser.sell_price];
    self.cellHeight = CGRectGetMaxY(self.deleteCollectionBtn.frame)+NORMOL_SPACE;
}
+(NSString *)idString{
    
    return @"goodsCell";
    
}
@end

