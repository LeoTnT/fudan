//
//  CountShowView.h
//  App3.0
//
//  Created by mac on 2017/9/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountShowView : UIView
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@end
