//
//  BankCardDefaultView.m
//  App3.0
//
//  Created by nilin on 2017/5/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BankCardDefaultView.h"

@implementation BankCardDefaultView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat space = 60,imageSize = 80,width = 100;
        self.defaultImageView = [[UIImageView alloc] initWithFrame:CGRectMake(mainWidth/2-imageSize/2, space*2, imageSize, imageSize)];
        self.defaultImageView.image = [UIImage imageNamed:@"user_auth"];
        [self addSubview:self.defaultImageView];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(NORMOL_SPACE, space/4+CGRectGetMaxY(self.defaultImageView.frame), mainWidth-NORMOL_SPACE*2, space/2)];
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;;
        self.titleLabel.numberOfLines = 0;
        [self addSubview:self.titleLabel];
        self.descLabel = [[UILabel alloc] initWithFrame:CGRectMake(space/3, CGRectGetMaxY(self.titleLabel.frame), mainWidth-space/3*2, space/3)];
        self.descLabel.numberOfLines = 0;
        self.descLabel.textAlignment = NSTextAlignmentCenter;
        self.descLabel.textColor = mainGrayColor;
        self.descLabel.font = [UIFont systemFontOfSize:14];
        self.descLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:self.descLabel];
        self.vertifyButton = [[XSCustomButton alloc] initWithFrame:CGRectMake(mainWidth/2-width/2, CGRectGetMaxY(self.descLabel.frame)+space/3, width, imageSize/2) title:Localized(@"user_approve_suggest_no_btn") titleColor:[UIColor whiteColor] fontSize:17 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [self.vertifyButton setBorderWith:0 borderColor:nil cornerRadius:5];
        [self.vertifyButton addTarget:self action:@selector(vertifyAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.vertifyButton];
    }
    return self;
}

- (void)vertifyAction {
    if ([self.delegate respondsToSelector:@selector(toVertify)]) {
        [self.delegate toVertify];
    }
}

@end
