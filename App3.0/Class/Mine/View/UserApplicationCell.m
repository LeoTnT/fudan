//
//  UserApplicationCell.m
//  App3.0
//
//  Created by mac on 17/2/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserApplicationCell.h"
#import "AreaButton.h"

@interface UserApplicationCell()
{
    __weak id<UserApplicationCellDelegate>delegate;
}
@property (strong, nonatomic) NSArray *dataArray;
@end

@implementation UserApplicationCell
@synthesize delegate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataArray:(NSArray *)dataArray
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat width = mainWidth/3;
        CGFloat height = 100;
        
        self.dataArray = [NSArray arrayWithArray:dataArray];
        NSInteger row = ceil(self.dataArray.count/3.0);
        
        for (int i = 0; i < self.dataArray.count; i++) {
            AreaButton *btn = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%3), (int)(i/3)*height, width, height) Model:self.dataArray[i] Scale:1.0/3.0 fontSize:14];
            btn.index = i;
            [btn addTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            if (i%3 == 0 && i != 0) {
                UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, height*(int)(i/3)-0.5, mainWidth, 0.5)];
                hLine.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
                [self addSubview:hLine];
            }
        }
        for (int i = 1; i <= 2; i++) {
            UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(width*i-0.5, 0, 0.5, height*row)];
            hLine.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
            [self addSubview:hLine];
        }
        
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
}

- (void)areaAction:(AreaButton *)pSender
{
    if ([delegate respondsToSelector:@selector(appAreaClick:)]) {
        [delegate appAreaClick:pSender.index];
    }
}

@end
