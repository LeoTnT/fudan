//
//  AddressTableViewCell.h
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiverAddressModel.h"

typedef NS_ENUM (NSInteger, AddressButtonType) {
    AddressButtonTypeForAddress, 
    AddressButtonTypeForOrder
};

@protocol AddressProtocol <NSObject>

@optional
/**默认地址*/
- (void)defaultAddressWithId:(NSString *)addressId;
@optional
/**编辑地址*/
- (void)editAddressWithAddress:(AddressParser *)address;
@optional
/**删除地址*/
- (void)deleteAddressWithId:(NSString *)addressId andSender:(UIButton *)sender;
@optional
/**联系卖家*/
- (void)linkSeller;
@end

@interface AddressTableViewCell : UITableViewCell
@property (nonatomic, strong) AddressParser *address;
@property (nonatomic, weak) id<AddressProtocol> addressDelegate;
//@property (nonatomic, assign) AddressViewType addressViewType;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, copy) NSString *receiver;//收货人
@property (nonatomic, copy) NSString *tel;//电话
@property (nonatomic, copy) NSString *detailAddress;//收货地址
@property (nonatomic, strong)  UIButton *defaultBtn;
@property (nonatomic, strong) UIButton *editBtn;
@property (nonatomic, strong) UIButton *deleteBtn;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, assign) BOOL isShowDefaultButton;
@property (nonatomic, strong) UIColor *fontColor;
@property (nonatomic, assign) AddressButtonType addressButtonType;
@end
