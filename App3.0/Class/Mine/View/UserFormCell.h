//
//  UserFormCell.h
//  App3.0
//
//  Created by mac on 17/2/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserFormCellDelegate <NSObject>
- (void)formAreaClick:(NSInteger)index;
@end
@interface UserFormCell : UITableViewCell
@property(nonatomic, weak) id<UserFormCellDelegate>delegate;
@property(nonatomic,assign) NSMutableArray *numberArray;//各种订单数目
@end
