//
//  BankCardDefaultView.h
//  App3.0
//
//  Created by nilin on 2017/5/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSCustomButton.h"
@protocol BankCardDefaultViewDelegate<NSObject>
@optional
- (void)toVertify;
@end

@interface BankCardDefaultView : UIView
@property (nonatomic, strong)  UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIImageView *defaultImageView;
@property (nonatomic, strong)  XSCustomButton *vertifyButton;
@property (nonatomic, weak) id<BankCardDefaultViewDelegate> delegate;
@end
