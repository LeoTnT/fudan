//
//  AddressTableViewCell.m
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddressTableViewCell.h"


@implementation AddressTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = BG_COLOR;
    }
    return self;
}

- (void)setAddress:(AddressParser *)address {
    _address = address;
    self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 12, mainWidth, 200)];
    self.bgView.backgroundColor = [UIColor whiteColor];
    //收货人，手机号
    CGFloat baseWidth = (CGRectGetWidth(self.bgView.frame)-13.5-12.5-10)/3;
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(13.5 , 15, baseWidth*2, 0)];
    nameLabel.text =  [_address name];
    nameLabel.textColor = self.fontColor?self.fontColor:[UIColor blackColor];
    nameLabel.numberOfLines = 0;
    nameLabel.font = [UIFont systemFontOfSize:15];
    nameLabel.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size = [nameLabel sizeThatFits:CGSizeMake(nameLabel.frame.size.width, MAXFLOAT)];
    if (size.height<30) {
        size.height = 30;
    }
    nameLabel.frame = CGRectMake(13.5 , 15, size.width, size.height);
    [self.bgView addSubview:nameLabel];
    UILabel *phoneLabel = [[UILabel alloc] init];
    phoneLabel.text = [_address mobile];
    phoneLabel.textColor = self.fontColor?self.fontColor:[UIColor blackColor];
    phoneLabel.font = [UIFont systemFontOfSize:15];
    phoneLabel.numberOfLines = 0;
    phoneLabel.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size3 = [phoneLabel sizeThatFits:CGSizeMake(phoneLabel.frame.size.width, MAXFLOAT)];
    phoneLabel.frame = CGRectMake(CGRectGetWidth(self.bgView.frame)-12.5-size3.width, CGRectGetMinY(nameLabel.frame), size3.width, CGRectGetHeight(nameLabel.frame));
    [self.bgView addSubview:phoneLabel];
    
    
    //地址
    UILabel *addressLabel = [[UILabel alloc] init];
    addressLabel.text = [NSString stringWithFormat:@"%@-%@-%@-%@-%@",[_address.province name],[_address.city name],[_address.county name],[_address.town name],[_address detail]];
    addressLabel.numberOfLines = 0;
    addressLabel.font = [UIFont systemFontOfSize:13];
    addressLabel.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size2 = [addressLabel sizeThatFits:CGSizeMake(addressLabel.frame.size.width, MAXFLOAT)];
    if (size2.height<30) {
        size2.height = 30;
    }
    addressLabel.textColor = self.fontColor?self.fontColor:COLOR_666666;
    addressLabel.frame = CGRectMake(CGRectGetMinX(nameLabel.frame), CGRectGetMaxY(nameLabel.frame)+10.5, CGRectGetWidth(self.bgView.frame)-11-11.5, size2.height);
    [self.bgView addSubview:addressLabel];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(nameLabel.frame), CGRectGetMaxY(addressLabel.frame)+16, CGRectGetWidth(addressLabel.frame), 0.5)];
    lineView.backgroundColor = LINE_COLOR_NORMAL;
    [self.bgView addSubview:lineView];
    
    //按钮
    CGFloat width = 150;
    self.defaultBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(nameLabel.frame)+5, CGRectGetMaxY(lineView.frame)+14.5, width, NORMOL_SPACE*2)];
    [self.defaultBtn setTitle:@"设为默认地址" forState:UIControlStateNormal];
    
    //向左对齐
    self.defaultBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.defaultBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.defaultBtn setTitleColor:self.fontColor?self.fontColor:COLOR_666666 forState:UIControlStateNormal];
    [self.defaultBtn setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
    [self.defaultBtn setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
    if ( [_address.is_default integerValue]==1) {
        self.defaultBtn.selected = YES;
        self.defaultBtn.userInteractionEnabled = NO;
    } else {
        self.defaultBtn.userInteractionEnabled = YES;
        [self.defaultBtn addTarget:self action:@selector(defaultAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.defaultBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    [self.bgView addSubview:self.defaultBtn];
    CGFloat smallWidth = 50;
    self.editBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12*2-2*smallWidth, CGRectGetMinY(self.defaultBtn.frame),smallWidth, CGRectGetHeight(self.defaultBtn.frame))];
    self.editBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.editBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [ self.editBtn setTitle:Localized(@"edit") forState:UIControlStateNormal];
    [ self.editBtn setImage:[UIImage imageNamed:@"user_site_edit_gray"] forState:UIControlStateNormal];
    [ self.editBtn setTitleColor:self.fontColor?self.fontColor:COLOR_666666 forState:UIControlStateNormal];
    [ self.editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.editBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -6.5, 0, 0)];
    [self.bgView addSubview: self.editBtn];
    self.deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-12-smallWidth, CGRectGetMinY(self.editBtn.frame), smallWidth, CGRectGetHeight(self.editBtn.frame))];
    self.deleteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.deleteBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteBtn setTitleColor:self.fontColor?self.fontColor:COLOR_666666                                            forState:UIControlStateNormal];
    [self.deleteBtn setImage:[UIImage imageNamed:@"user_site_delete_gray"] forState:UIControlStateNormal];
    [self.deleteBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.deleteBtn];
    [self.contentView addSubview:self.bgView];
    [self.deleteBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -6.5, 0, 0)];
    
    self.bgView.frame = CGRectMake(0, 12, mainWidth, CGRectGetMaxY(self.defaultBtn.frame)+11.5);
    self.cellHeight = CGRectGetHeight(self.bgView.frame)+11.5;
    NSLog(@"%f",self.cellHeight);
    
}

- (void)defaultAction:(UIButton *)sender {
    if ([self.addressDelegate respondsToSelector:@selector(defaultAddressWithId:)]) {
        [self.addressDelegate defaultAddressWithId:self.address.ID];
    }
}

- (void)editAction:(UIButton *)sender {
    if ([self.addressDelegate respondsToSelector:@selector(editAddressWithAddress:)]) {
        [self.addressDelegate editAddressWithAddress:self.address];
    }
}

- (void)deleteAction:(UIButton *)sender {
    if ([self.addressDelegate respondsToSelector:@selector(deleteAddressWithId:andSender:)]) {
        [self.addressDelegate deleteAddressWithId:self.address.ID andSender:sender];
    }
}

- (void)clickToSeller:(UIButton *)sender {
    if ([self.addressDelegate respondsToSelector:@selector(linkSeller)]) {
        [self.addressDelegate linkSeller];
    }
}

@end
