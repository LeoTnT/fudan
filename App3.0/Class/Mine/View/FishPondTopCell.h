//
//  FishPondTopCell.h
//  App3.0
//
//  Created by mac on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FishPondTopCell : UITableViewCell
/**头像*/
@property(nonatomic,strong)UIImageView *headImg;
/**昵称*/
@property(nonatomic,strong)UILabel *nickNameLabel;
/**鱼的数量*/
@property(nonatomic,strong)UILabel *fishNumLabel;
/**价值金额*/
@property(nonatomic,strong)UILabel *moneyLabel;
@end
