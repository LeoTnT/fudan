//
//  QuickPayRecordsCell.h
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QuickPayRecordsModel;

@interface QuickPayRecordsCell : UITableViewCell

-(void)setDataToCellWithModel:(QuickPayRecordsModel *)model;

@end
