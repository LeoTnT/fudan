//
//  BankCardTypeView.h
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardModel.h"
#import "BusinessModel.h"
#import "OrderFormModel.h"
@protocol CardTypeDelegate<NSObject>

@optional
-(void) hiddenCardView:(CardTypeDataParser *) dataParser;
-(void) hiddenSellTypeView:(BusinessGoodsSellTypeParser *) selectedParser;
@optional
-(void) hiddenReasonView:(NSDictionary *) selectedType;
-(void) hiddenCouponView:(NSString *) couponId  coupon:(NSString *) coupon;
-(void) hiddenCouponView;
@end

typedef NS_ENUM(NSInteger, AlertViewType) {
    AlertTypeBankCard,
    AlertTypeBankOrderRejectReason,
    AlertTypeSellType,
    AlertTypeCouponType,//抵扣券类型
};

@interface BankCardTypeView : UIView
@property (nonatomic, strong) NSMutableArray *typeArray;
@property (nonatomic, copy) NSString *selectedId;
@property (nonatomic, weak) id<CardTypeDelegate> typeDelegate;
@property (nonatomic, assign) AlertViewType viewType;//弹出视图类型
@property (nonatomic, strong) UIScrollView *scrollView;
@end
