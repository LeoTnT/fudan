//
//  CountShowView.m
//  App3.0
//
//  Created by mac on 2017/9/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CountShowView.h"

@implementation CountShowView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.countLabel = [UILabel new];
        self.countLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.countLabel];
        
        self.nameLabel = [UILabel new];
        self.countLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.nameLabel];
        
        [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(self).offset(15);
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.bottom.mas_equalTo(self).offset(-15);
        }];
    }
    return self;
}
@end
