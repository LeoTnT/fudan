//
//  ShopViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ShopViewController.h"
#import "DefaultView.h"
#import "ShopModel.h"
#import "ShopTableViewCell.h"
#import "MJRefresh.h"
#import "S_StoreInformation.h"
#import "S_StoreCollectionView.h"

@interface ShopViewController ()<DefaultViewDelegate,ShopDelegate>
@property (nonatomic, strong) DefaultView *defaultView;
@property (nonatomic, strong) NSMutableArray *shopArray;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, copy) NSString *limitSize;
@end

@implementation ShopViewController
#pragma mark - lazy loadding
- (NSMutableArray *)shopArray {
    if (!_shopArray) {
        _shopArray = [NSMutableArray array];
    }
    return _shopArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.limitSize = @"6";
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self pullDownNewInfo];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self upwardPullInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [XSTool hideProgressHUDWithView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void)pullDownNewInfo {
  
    [XSTool showProgressHUDWithView:self.view];
    self.page = 1;
    @weakify(self);
    [HTTPManager getShopCarInfoWithFavType:@"1" page:[NSString stringWithFormat:@"%lu",self.page] limit:self.limitSize type:self.collectShopType==CollectShopTypeOnline?@"online":@"offline" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.shopArray  removeAllObjects];
            self.shopArray  = [NSMutableArray arrayWithArray:[ShopDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"data"]]];
            [self setSubViews];
            self.page++;
        }else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
         [self.tableView.mj_header endRefreshing];
         [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)upwardPullInfo {
    [self.tableView.mj_footer resetNoMoreData];
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getShopCarInfoWithFavType:@"1" page:[NSString stringWithFormat:@"%lu",self.page] limit:self.limitSize type:self.collectShopType==CollectShopTypeOnline?@"online":@"offline" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSArray *array = dic[@"data"][@"data"];
            if (array.count>0) {
                [self.shopArray addObjectsFromArray:[ShopDataParser mj_objectArrayWithKeyValuesArray:array]];
                [self.tableView reloadData];
            }
            
            //页数加一
            self.page++;
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
          [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)addDefaultView {
    self.defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.view.frame))];
    self.defaultView.defaultDelegete = self;
    self.defaultView.defaultViewType = DefaultViewTypeNormal;
    self.defaultView.titleLabel.text = @"您还没有收藏过店铺哦";
    [self.defaultView.button setTitle:@"去看看" forState:UIControlStateNormal];
    [self.view addSubview:self.defaultView];
}

- (void)setSubViews {
    if (self.shopArray.count==0) {
        
        //默认界面
        [self addDefaultView];
        
    } else {
        [self.tableView reloadData];
    }
}

- (void)clickToCancelCollectWithId:(NSString *)favId {
    //弹出警告框
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"确认取消收藏？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager deleteShopCarInfoWithFavId:favId success:^(NSDictionary * _Nullable dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"成功取消收藏"];
                if (self.shopArray.count<=1) {
                    
                    //默认界面
                    [self addDefaultView];
                    [self.shopArray removeAllObjects];
                    [self.tableView removeFromSuperview];
                } else {
                    for (ShopDataParser *parser in self.shopArray) {
                        if ([parser.user_id integerValue]==[favId integerValue]) {
                            [self.shopArray removeObject:parser];
                            break;
                        }
                    }
                    [self.tableView reloadData];
                }
            } else {
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];

    }];
    [alertControl addAction:cancelAction];
    [alertControl addAction:okAction];
    [self presentViewController:alertControl animated:YES completion:nil];
    
}

#pragma mark - ShopDelegate
- (void)buttonClicktoMall {
    S_StoreCollectionView *shop = [S_StoreCollectionView new];
    [self.navigationController pushViewController:shop animated:YES];
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str = @"shopCell";
    ShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell==nil) {
        cell = [[ShopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.dataParser = self.shopArray[indexPath.row];
    cell.shopDelegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    S_StoreInformation *shop = [S_StoreInformation new];
    ShopDataParser *shopParser = self.shopArray[indexPath.row];
    shop.storeInfor =  shopParser.user_id;
    [self.navigationController pushViewController:shop animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.shopArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

@end
