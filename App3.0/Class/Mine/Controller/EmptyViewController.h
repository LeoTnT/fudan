//
//  EmptyViewController.h
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface EmptyViewController : XSBaseViewController
@property(nonatomic, copy) NSString *title;
@end
