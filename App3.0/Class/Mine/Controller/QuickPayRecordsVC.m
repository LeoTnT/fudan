//
//  QuickPayRecordsVC.m
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "QuickPayRecordsVC.h"
#import "QuickPayRecordsCell.h"
#import "QuickPayRecordsModel.h"

@interface QuickPayRecordsVC ()<UITableViewDelegate,UITableViewDataSource>

/**数据源*/
@property (nonatomic, strong) NSMutableArray * dataArray;
/**您的土豪朋友福旦已累计替您支付%@元*/
@property (nonatomic, strong) UILabel * theTitleLabel;
/** 头视图 */
@property (nonatomic, strong) UIView * secsionHview;
/** 无数据视图 */
@property (nonatomic, strong) DefaultView *emptyView;
/** 分页 */
@property (nonatomic, assign) NSUInteger page;

@end

@implementation QuickPayRecordsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"快速买单记录";
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setUpUI];
}

#pragma mark 设置UI
- (void)setUpUI
{
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    
    @weakify(self);
    [self setRefreshHeader:^{
        @strongify(self);
        self.page = 1;
        [self.dataArray removeAllObjects];
        [self requestData];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    [self setRefreshFooter:^{
        @strongify(self);
        self.page++;
        [self requestData];
    }];
    
    // 默认页
    self.emptyView = [[DefaultView alloc] initWithFrame:self.view.bounds];
    self.emptyView.button.hidden = YES;
    self.emptyView.titleLabel.text = @"暂无快速买单记录";
    self.emptyView.hidden = YES;
    [self.view addSubview:self.emptyView];
    
    UIView * secsionHeaderview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    secsionHeaderview.backgroundColor = BG_COLOR;
    
    self.theTitleLabel = [[UILabel alloc] init];
    self.theTitleLabel.textColor = [UIColor hexFloatColor:@"666666"];
    self.theTitleLabel.font = [UIFont systemFontOfSize:12];
    [secsionHeaderview addSubview:self.theTitleLabel];
    
    [self.theTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(secsionHeaderview);
    }];
    
    self.secsionHview = secsionHeaderview;
    //    self.tableView.tableHeaderView = secsionHeaderview;
}

-(void)endRefresh
{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

-(void) requestData {
    NSString * pageStr = [NSString stringWithFormat:@"%lu",(unsigned long)self.page];
    [XSTool showProgressHUDWithView:self.view];
    
    @weakify(self);
    [HTTPManager requestQuickOrderListData:nil page:pageStr success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self endRefresh];
        
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            NSDictionary * listDict = dataDict[@"list"];
            
            [self setTitleString:[NSString stringWithFormat:@"您的土豪朋友福旦已累计替您支付%@元",dataDict[@"pay_sum"]]];
            
            NSArray * dataArr = [QuickPayRecordsModel mj_objectArrayWithKeyValuesArray:listDict[@"data"]];
            
            if (dataArr.count < [listDict[@"per_page"] integerValue]) {
                self.tableView.mj_footer.hidden = YES;
            } else {
                self.tableView.mj_footer.hidden = NO;
            }
            
            [self.dataArray addObjectsFromArray:dataArr];
            
            if (self.dataArray.count) {
                self.emptyView.hidden = YES;
                self.tableView.backgroundColor=BG_COLOR;
            } else {
                self.emptyView.hidden = NO;
                self.view.backgroundColor=[UIColor whiteColor];
            }
            
            [self.tableView reloadData];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self endRefresh];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

#pragma mark tableView delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellID = @"QuickPayRecordsCellID";
    QuickPayRecordsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[QuickPayRecordsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataArray.count > 0) {
        QuickPayRecordsModel * model = self.dataArray[indexPath.row];
        [cell setDataToCellWithModel:model];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 167;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.secsionHview;
}

#pragma mark 设置头视图文字
- (void)setTitleString:(NSString *)title {
    self.theTitleLabel.text = title;
    NSRange range1 = [self.theTitleLabel.text rangeOfString:@"朋友"];
    NSRange range2 = [self.theTitleLabel.text rangeOfString:@"已累计"];
    NSRange range3 = [self.theTitleLabel.text rangeOfString:@"支付"];
    NSRange range4 = [self.theTitleLabel.text rangeOfString:@"元"];
    NSMutableAttributedString *att1 = [[NSMutableAttributedString alloc] initWithString:self.theTitleLabel.text];
    [att1 addAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"6995f4"]} range:NSMakeRange(range1.location+2, range2.location-range1.location-2)];
    [att1 addAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"6995f4"]} range:NSMakeRange(range3.location+2, range4.location-range3.location-2)];
    self.theTitleLabel.attributedText = att1;
}



@end
