//
//  CustomerViewController.m
//  App3.0
//
//  Created by mac on 2017/9/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CustomerViewController.h"
#import "UserModel.h"
#import "CustomerCell.h"

@interface CustomerViewController ()
@property (strong, nonatomic) NSMutableArray *dataArray;
@end

@implementation CustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"我的客服");
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    
    self.dataArray = [NSMutableArray array];
    [self requestData];
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:nil action:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData {
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getCustomerServicesWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.dataArray = [CustomerModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomerCell *cell = [CustomerCell createCustomerCellWithTableView:tableView];
    CustomerModel *model = self.dataArray[indexPath.section];
    cell.customerModel = model;
    [cell setCallAction:^(CustomerModel *customerModel) {
        NSString *telStr = [NSString stringWithFormat:@"tel://%@",customerModel.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telStr]];
    }];
    [cell setChatAction:^(CustomerModel *customerModel) {
#ifdef ALIYM_AVALABLE
        YWPerson *person = [[YWPerson alloc] initWithPersonId:self.customerModel.uid];
        YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
        YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
        chatVC.title = self.customerModel.nickname;
        chatVC.avatarUrl = self.customerModel.logo;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:customerModel.uid type:EMConversationTypeChat createIfNotExist:YES];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
        chatVC.title = customerModel.nickname;
        chatVC.avatarUrl = customerModel.logo;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
#else
        XMChatController *chatVC = [XMChatController new];
        SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:customerModel.uid title:customerModel.nickname avatarURLPath:customerModel.logo];
        chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
        [self.navigationController pushViewController:chatVC animated:YES];
#endif
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
@end
