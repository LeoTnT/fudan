//
//  HistoryViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HistoryViewController.h"
#import "DefaultView.h"
#import "AppDelegate.h"
#import "HistoryModel.h"
#import "HistoryTableViewCell.h"
#import "TabMallVC.h"
#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "RSAEncryptor.h"

@interface HistoryViewController ()<DefaultViewDelegate,HistoryDelegate>
@property (nonatomic, strong) NSMutableArray *historyArray;
@property (nonatomic, strong) NSMutableDictionary *cellHeightDictionary;
@property (nonatomic, strong) NSMutableArray *deleteGoodsArray;//需要删除的商品记录
@property (nonatomic, strong) NSMutableArray *deleteShopArray;//删除店铺
@property (nonatomic, strong) UIView *bottomView;//底部区域
@property (nonatomic, strong) UIButton *allSelectedBtn;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, assign) BOOL isShowMove;//是否显示删除圆圈
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, assign) NSUInteger limit;
@property (nonatomic, strong) NSMutableArray *allCellArray;//所有的cell
@property (nonatomic, strong) NSMutableArray *selectedDateArray;//所有的cell
@property (nonatomic, strong) UILabel *allSelectedLabel;//全选
@end

@implementation HistoryViewController

#pragma mark - lazy loadding
-(NSMutableArray *)selectedDateArray {
    if (!_selectedDateArray) {
        _selectedDateArray = [NSMutableArray array];
    }
    return _selectedDateArray;
}


- (NSMutableArray *)historyArray {
    if (!_historyArray) {
        _historyArray = [NSMutableArray array];
    }
    return _historyArray;
}

- (NSMutableArray *)allCellArray {
    if (!_allCellArray) {
        _allCellArray = [NSMutableArray array];
    }
    return _allCellArray;
}

- (NSMutableArray *)deleteShopArray {
    if (!_deleteShopArray) {
        _deleteShopArray = [NSMutableArray array];
    }
    return _deleteShopArray;
}

- (NSMutableArray *)deleteGoodsArray {
    if (!_deleteGoodsArray) {
        _deleteGoodsArray = [NSMutableArray array];
    }
    return _deleteGoodsArray;
}

- (NSMutableDictionary *)cellHeightDictionary {
    if (!_cellHeightDictionary) {
        _cellHeightDictionary = [NSMutableDictionary dictionary];
    }
    return _cellHeightDictionary;
}

#pragma mark - life circle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.limit=10;
    self.page=1;
    self.automaticallyAdjustsScrollViewInsets =NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.isShowMove = NO;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self pullDownNewInfo];
    }];
    
    self.tableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self upwardPullInfo];
    }];
    [self setSubViews];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - private
- (void)setSubViews {
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = Localized(@"历史");
    self.navigationController.navigationBarHidden = NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.navRightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.navRightBtn];
    self.navRightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    self.navRightBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [self.navRightBtn setTitle:@"删除" forState:UIControlStateNormal];
     [self.navRightBtn setTitle:Localized(@"完成") forState:UIControlStateSelected];
    [self.navRightBtn setTitleColor:[UIColor hexFloatColor:@"333333"] forState:UIControlStateNormal];
    [self.navRightBtn setTitleColor:[UIColor hexFloatColor:@"333333"] forState:UIControlStateSelected];
    [self.navRightBtn addTarget:self action:@selector(showdeleteButtons:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)pullDownNewInfo {
    
    [XSTool showProgressHUDTOView:self.view withText:@"查询中"];
    self.page = 1;
    @weakify(self);
    [HTTPManager getHistoryListsWithPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:[NSString stringWithFormat:@"%lu",(unsigned long)self.limit] success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            [self.historyArray removeAllObjects];
            //            self.isShowMove = NO;
            [self.historyArray  addObjectsFromArray:[HistoryDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"][@"data"]]];
            NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
            [self addSubviews];
            
            //页数加一
            self.page++;
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)upwardPullInfo {
    [XSTool showProgressHUDTOView:self.view withText:@"查询中"];
    @weakify(self);
    [HTTPManager getHistoryListsWithPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:[NSString stringWithFormat:@"%lu",(unsigned long)self.limit] success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            //            self.isShowMove = NO;
            
            //不可点击时
            if(self.navRightBtn.enabled==NO){
                //                [self cancelSelectedAction:nil];
            }
            NSArray *array = dic[@"data"][@"list"][@"data"];
            if (array.count>0) {
                [self.historyArray addObjectsFromArray:[HistoryDataParser mj_objectArrayWithKeyValuesArray:array]];
                [self.tableView reloadData];
                
                //页数加一
                self.page++;
            } else {
                [XSTool showToastWithView:self.view Text:@"没有更多数据了！"];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)addSubviews {
    if (self.historyArray.count==0) {
        self.navRightBtn.hidden = YES;
        DefaultView * defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        defaultView.defaultDelegete = self;
        defaultView.defaultViewType = DefaultViewTypeNormal;
        defaultView.titleLabel.text = @"还没有浏览历史哦";
        self.view.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:defaultView];
        
    } else {
        [self.tableView reloadData];
        self.navRightBtn.hidden = NO;
        for (UIView *view in self.view.subviews) {
            if (view.tag==1000) {
                for (UIView *subView in view.subviews) {
                    [subView removeFromSuperview];
                }
                [view removeFromSuperview];
            }
        }
        CGFloat bottomHeight = 47;
        
        if (self.isShowMove) {
            self.navRightBtn.enabled=NO;
            //右边屏幕之外
            self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame)-bottomHeight, mainWidth, bottomHeight)];
        }else{
            self.navRightBtn.enabled=YES;
            self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(mainWidth, CGRectGetMaxY(self.tableView.frame)-bottomHeight, mainWidth, bottomHeight)];
        }
        self.bottomView.backgroundColor  = [UIColor whiteColor];
        [self.view addSubview:self.bottomView];
        self.bottomView.tag=1000;
        CGFloat width = mainWidth/3;
        self.allSelectedBtn = [[UIButton alloc] initWithFrame:CGRectMake(13, bottomHeight/2-19/2, 19, 19)];
        [self.allSelectedBtn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
        [self.allSelectedBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
        [self.allSelectedBtn addTarget:self action:@selector(selectAllAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:self.allSelectedBtn];
        self.allSelectedLabel = [[UILabel alloc] initWithFrame:CGRectMake( CGRectGetMaxX(self.allSelectedBtn.frame)+13.5, 0,width-CGRectGetMaxX(self.allSelectedBtn.frame)-13.5, bottomHeight)];
        self.allSelectedLabel.text = @"全选";
        self.allSelectedLabel.textColor = [UIColor blackColor];
        self.allSelectedLabel.font = [UIFont systemFontOfSize:15];
        [self.bottomView addSubview:self.allSelectedLabel];
        self.cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.allSelectedLabel.frame), CGRectGetMinY(self.allSelectedLabel.frame), width, CGRectGetHeight(self.allSelectedLabel.frame))];
        [self.cancelButton setTitle:Localized(@"cancel_btn") forState:UIControlStateNormal];
        [self.cancelButton setTitleColor:[UIColor hexFloatColor:@"666666"] forState:UIControlStateNormal] ;
        self.cancelButton.backgroundColor = [UIColor hexFloatColor:@"E7E7E7"];
        [self.cancelButton addTarget:self action:@selector(cancelSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
        self.cancelButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.bottomView addSubview:self.cancelButton];
        self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.cancelButton.frame), CGRectGetMinY(self.allSelectedLabel.frame), width, CGRectGetHeight(self.allSelectedLabel.frame))];
        [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        self.deleteButton.titleLabel.textColor = [UIColor whiteColor];
        self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:16];
        self.deleteButton.backgroundColor = [UIColor hexFloatColor:@"3F8EF7"];
        [self.deleteButton addTarget:self action:@selector(deleteFewAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:self.deleteButton];
        
    }
}

- (void)selectAllAction:(UIButton *) sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        for (int i=0; i<self.historyArray.count; i++) {
            HistoryDataParser *dataParser = self.historyArray[i];
            if (![self.deleteGoodsArray containsObject:dataParser.ID]) {
                [self.deleteGoodsArray addObject:dataParser.ID];
            }
            if (![self.selectedDateArray containsObject:dataParser.w_time]) {
                [self.selectedDateArray addObject:dataParser.w_time];
            }
        }
    } else {
        [self.deleteGoodsArray removeAllObjects];
        [self.selectedDateArray removeAllObjects];
    }
    
    [self cellsForTableView:self.tableView selected:sender.selected];
}

- (void )cellsForTableView:(UITableView *)tableView selected:(BOOL )isSelected {
    [self.tableView reloadData];
}

- (void)cancelSelectedAction:(UIButton *)sender {
    [self showdeleteButtons:self.navRightBtn];
}


- (void)deleteFewAction:(UIButton *)sender {
   
    //删除商品和店铺
    if (self.deleteGoodsArray.count==0) {
        [XSTool showToastWithView:self.view Text:@"请选择要删除的商品或店铺！"];
    } else {
         [self showdeleteButtons:self.navRightBtn];
        self.isShowMove=NO;
        NSMutableString *mGoodsStr = [NSMutableString string];
        //    [self.deleteGoodsArray addObjectsFromArray:self.deleteShopArray];
        for (int i=0; i<self.deleteGoodsArray.count; i++) {
            [mGoodsStr appendString:self.deleteGoodsArray[i]];
        }
        [XSTool showProgressHUDTOView:self.view withText:@"删除中"];
        @weakify(self);
        [HTTPManager deleteHistoryInfoWithIdsString:mGoodsStr success:^(NSDictionary * _Nullable dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                
                //                [self cancelSelectedAction:nil];
                [self.deleteGoodsArray removeAllObjects];
                [self.selectedDateArray removeAllObjects];
                //刷新界面
                [self pullDownNewInfo];
                
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    }
    
}

- (void)showdeleteButtons:(UIButton *)sender {
    sender.selected = !sender.selected;
   
    if (sender.selected) {
        self.allSelectedBtn.selected = NO;
        self.isShowMove = YES;
        [self.tableView reloadData];
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.bottomView.frame;
            frame.origin.x-=mainWidth;
            self.bottomView.frame = frame;
        } completion:^(BOOL finished) {
        }];
    } else {
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.bottomView.frame;
            frame.origin.x+=mainWidth;
            self.bottomView.frame = frame;
        } completion:^(BOOL finished) {
            self.isShowMove = NO;
            [self.deleteShopArray removeAllObjects];
            [self.deleteGoodsArray removeAllObjects];
            [self.selectedDateArray removeAllObjects];

            [self cellsForTableView:self.tableView selected:NO];
        }];
        
    }
   
}

#pragma mark - tableView Delegate

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = @"historyCell";
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[HistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
    }
      HistoryDataParser *dataParser = self.historyArray[indexPath.row];
    if (indexPath.row==0) {
        cell.isShow = YES;
        cell.dateBtn.selected = NO;
        if ([self.selectedDateArray containsObject:dataParser.w_time]) {
            cell.dateBtn.selected = YES;
        }
    }else {
        HistoryDataParser *tempParser = self.historyArray[indexPath.row-1];
      
        if ([dataParser.w_time isEqualToString:tempParser.w_time]) {
            cell.isShow = NO;
        }else{
            cell.isShow = YES;
            cell.dateBtn.selected = NO;
            if ([self.selectedDateArray containsObject:dataParser.w_time]) {
                cell.dateBtn.selected = YES;
            }
        }
    }
    cell.isMoveShow = self.isShowMove;
    cell.historyDelegate = self;
    cell.dataParser = dataParser;

    [self.cellHeightDictionary setObject:@(cell.cellHeight) forKey:@(indexPath.row)];
    
    //将cell放进数组
    [self.allCellArray addObject:cell];
    //判断之前有没有选过
    for (NSString *parseId in self.deleteGoodsArray) {
        if ( cell.dataParser &&[cell.dataParser.ID integerValue]==[parseId integerValue]) {
            cell.selectedBtn.selected=YES;
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.historyArray.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [[self.cellHeightDictionary objectForKey:@(indexPath.row)] floatValue];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y> 0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryDataParser *historyParser =  self.historyArray[indexPath.row];
    if (!self.isShowMove) {
        if (historyParser.supply_id) {
            S_StoreInformation *storeController = [S_StoreInformation new];
            storeController.storeInfor = historyParser.supply_id;
            [self.navigationController pushViewController:storeController animated:YES];
            
        } else {
            GoodsDetailViewController *goodsController = [GoodsDetailViewController new];
            goodsController.goodsID = historyParser.data_id;
            [self.navigationController pushViewController:goodsController animated:YES];
        }

    }
}

#pragma mark -HistoryDelegate
-(void)deleteHistoryInfoWithDate:(NSString *)dateString {
    for (int i=0; i<self.historyArray.count; i++) {
         HistoryDataParser *dataParser = self.historyArray[i];
        if ([dataParser.w_time isEqualToString:dateString]) {
            if (![self.deleteGoodsArray containsObject:dataParser.ID]) {
                  [self.deleteGoodsArray addObject:[NSString stringWithFormat:@"%@",dataParser.ID]];
            }
           
        }
    }
    if (![self.selectedDateArray containsObject:dateString]) {
          [self.selectedDateArray addObject:dateString];
    }
    [self cellsForTableView:self.tableView selected:YES];
}

-(void)cancelHistoryInfoWithDate:(NSString *)dateString {
    for (int i=0; i<self.historyArray.count; i++) {
        HistoryDataParser *dataParser = self.historyArray[i];
        if ([dataParser.w_time isEqualToString:dateString]) {
            [self.deleteGoodsArray removeObject:[NSString stringWithFormat:@"%@",dataParser.ID]];
        }
    }
     [self.selectedDateArray removeObject:dateString];
    [self cellsForTableView:self.tableView selected:NO];
}
- (void)deleteHistoryInfoWithStr:(NSString *)idsStr andType:(NSString *)type {
    if (![self.deleteGoodsArray containsObject:idsStr]) {
         [self.deleteGoodsArray addObject:[NSString stringWithFormat:@"%@",idsStr]];
    }

   
}
-(void)cancelDeleteHistoryInfoWithStr:(NSString *)idsStr andType:(NSString *)type{
    [self.deleteGoodsArray removeObject:[NSString stringWithFormat:@"%@",idsStr]];

}
- (void)tolookGoodsPageWithGoodsId:(NSString *)goodsId {
    GoodsDetailViewController *goodsController = [GoodsDetailViewController new];
    goodsController.goodsID = goodsId;
    [self.navigationController pushViewController:goodsController animated:YES];
    
}

- (void)tolookShopPageWithSupplyId:(NSString *)supplyId {
    S_StoreInformation *storeController = [S_StoreInformation new];
    storeController.storeInfor = supplyId;
    [self.navigationController pushViewController:storeController animated:YES];
}

#pragma mark - DefaultViewDelegate
- (void)buttonClicktoMall {
    self.tabBarController.selectedIndex = 2;
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
