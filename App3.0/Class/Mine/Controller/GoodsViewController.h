//
//  GoodsViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,CollectGoodsType) {
    CollectGoodsTypeOnline,
    CollectGoodsTypeOffline
};

@interface GoodsViewController : XSBaseTableViewController

@property (nonatomic, assign) CollectGoodsType collectGoodsType;

@end
