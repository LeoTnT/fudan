//
//  ShopViewController.h
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,CollectShopType) {
    CollectShopTypeOnline,
    CollectShopTypeOffline
};
@interface ShopViewController : XSBaseTableViewController
@property (nonatomic, assign) CollectShopType collectShopType;
@end
