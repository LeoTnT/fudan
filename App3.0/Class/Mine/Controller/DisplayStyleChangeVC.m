//
//  DisplayStyleChangeVC.m
//  App3.0
//
//  Created by mac on 2017/11/30.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "DisplayStyleChangeVC.h"

@interface DisplayStyleChangeVC ()

@end

@implementation DisplayStyleChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubviews {
    UIButton *areaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [areaBtn setTitle:@"九宫格" forState:UIControlStateNormal];
    [areaBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [areaBtn setTitleColor:mainColor forState:UIControlStateSelected];
    areaBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [areaBtn setImage:[UIImage imageNamed:@"display_area_normal"] forState:UIControlStateNormal];
    [areaBtn setImage:[UIImage imageNamed:@"display_area_select"] forState:UIControlStateSelected];
    [areaBtn addTarget:self action:@selector(areaClick) forControlEvents:UIControlEventTouchUpInside];
    [areaBtn setImageEdgeInsets:UIEdgeInsetsMake(-areaBtn.titleLabel.intrinsicContentSize.height-6, 0, 0, -areaBtn.titleLabel.intrinsicContentSize.width)];
    [areaBtn setTitleEdgeInsets:UIEdgeInsetsMake(areaBtn.currentImage.size.height+6, -areaBtn.currentImage.size.width, 0, 0)];
    [self.view addSubview:areaBtn];
    [areaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(40);
        make.left.mas_equalTo(self.view);
    }];
    
    UIButton *rowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rowBtn setTitle:@"横向列表" forState:UIControlStateNormal];
    [rowBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rowBtn setTitleColor:mainColor forState:UIControlStateSelected];
    rowBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [rowBtn setImage:[UIImage imageNamed:@"display_row_normal"] forState:UIControlStateNormal];
    [rowBtn setImage:[UIImage imageNamed:@"display_row_select"] forState:UIControlStateSelected];
    [rowBtn addTarget:self action:@selector(rowClick) forControlEvents:UIControlEventTouchUpInside];
    [rowBtn setImageEdgeInsets:UIEdgeInsetsMake(-rowBtn.titleLabel.intrinsicContentSize.height-6, 0, 0, -rowBtn.titleLabel.intrinsicContentSize.width)];
    [rowBtn setTitleEdgeInsets:UIEdgeInsetsMake(rowBtn.currentImage.size.height+6, -rowBtn.currentImage.size.width, 0, 0)];
    [self.view addSubview:rowBtn];
    [rowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(40);
        make.right.mas_equalTo(self.view);
    }];
    
    if ([UserInstance ShardInstnce].displayStyle == 0) {
        areaBtn.selected = YES;
    } else {
        rowBtn.selected = YES;
    }
}

- (void)areaClick {
    if (self.TapAction) {
        self.TapAction(0);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rowClick {
    if (self.TapAction) {
        self.TapAction(1);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
