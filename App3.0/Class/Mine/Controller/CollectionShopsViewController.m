//
//  CollectionShopsViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/1/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "CollectionShopsViewController.h"
#import "ShopViewController.h"
@interface CollectionShopsViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UISegmentedControl * segmentControl;

@end

@implementation CollectionShopsViewController

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, mainWidth, MAIN_VC_HEIGHT-50)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*2, MAIN_VC_HEIGHT-50);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        //        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (UISegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"线上店铺",@"线下店铺"]];
        _segmentControl.layer.cornerRadius = 4;
        _segmentControl.clipsToBounds = YES;
        
        [_segmentControl addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
        
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(13)];
        attrs[NSForegroundColorAttributeName] = XSYCOLOR(0x1C6AE7);
        
        NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
        selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
        selectedAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
        
        [_segmentControl setTitleTextAttributes:attrs forState:UIControlStateNormal];
        [_segmentControl setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
        
        _segmentControl.tintColor = XSYCOLOR(0x1C6AE7);//XSYCOLOR(0x334659);
        _segmentControl.backgroundColor = [UIColor clearColor];
        _segmentControl.selectedSegmentIndex = 0;
        
    }
    return _segmentControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    //设置子视图
    [self addSubViews];
}

#pragma mark - private
- (void)addSubViews {
    self.navigationItem.title = @"收藏店铺";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    [self.view addSubview:self.segmentControl];
    [self.segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).offset(6);
        make.size.mas_equalTo(CGSizeMake(FontNum(220), 30));
    }];
    
    //    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.segmentHeight-0.5, mainWidth, 0.5)];
    //    for (int i=1; i<2; i++) {
    //        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(mainWidth/2*i, 10, 1, self.segmentHeight-20)];
    //        line1.backgroundColor = LINE_COLOR;
    //        [self.view addSubview:line1];
    //
    //    }
    //    lineView.backgroundColor = BG_COLOR;
    //    [self.view addSubview:lineView];
    ShopViewController *shop1VC = [[ShopViewController alloc] init];
    [self addChildViewController:shop1VC];
    shop1VC.collectShopType = CollectShopTypeOnline;
    shop1VC.view.frame = CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
    [self.scrollView addSubview:shop1VC.view];
    ShopViewController *shop2VC = [[ShopViewController alloc] init];
    shop2VC.collectShopType = CollectShopTypeOffline;;
    [self addChildViewController:shop2VC];
    shop2VC.view.frame = CGRectMake(mainWidth, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
    [self.scrollView addSubview:shop2VC.view];
    
}

-(void)segmentClick:(UISegmentedControl *)segment
{
    XSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segment.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segment.selectedSegmentIndex, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page];
    //    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
}

//- (void)setType:(NSInteger)type {
//    _type = type;
//    self.segmentControl.selectedSegmentIndex = type;
//    [self.scrollView setContentOffset:CGPointMake(mainWidth*type, 0) animated:YES];
//}

@end
