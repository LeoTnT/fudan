//
//  GoodsViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "GoodsViewController.h"
#import "GoodsTableViewCell.h"
#import "DefaultView.h"
#import "CollectModel.h"
#import "MJRefresh.h"
#import "TabMallVC.h"
#import "GoodsDetailViewController.h"
#import "OfflineShopHomeViewController.h"

@interface GoodsViewController ()<DefaultViewDelegate,CollectDelegate>

@property(nonatomic,strong) DefaultView *defaultView;//默认界面
@property(nonatomic,strong) NSMutableArray *goodsArray;//商品信息数组
@property(nonatomic,assign) NSUInteger page;
@property (nonatomic, copy) NSString *limitSize;

@end

@implementation GoodsViewController

#pragma mark - lazy loadding
- (NSMutableArray *)goodsArray {
    if (!_goodsArray) {
        _goodsArray = [NSMutableArray array];
    }
    return _goodsArray;
}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.limitSize = @"6";
    self.view.backgroundColor = [UIColor whiteColor];
    self.page = 1;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self pullDownNewInfo];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self upwardPullInfo];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [XSTool hideProgressHUDWithView:self.view];
    
}

#pragma mark - private
- (void)pullDownNewInfo {
    
    self.page = 1;
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getCollectionInfoWithFavType:@"0" andPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:self.limitSize type:self.collectGoodsType==CollectGoodsTypeOnline?@"online":@"offline" success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSArray *array = dic[@"data"][@"data"];
            [self.goodsArray removeAllObjects];
            if (array.count>0) {
                [self.goodsArray addObjectsFromArray:[CollectParser mj_objectArrayWithKeyValuesArray:array]];
            }
            [self addSubViews];
        }else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        self.page++;
        
        
    } failure:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)upwardPullInfo {
    [self.tableView.mj_footer resetNoMoreData];
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getCollectionInfoWithFavType:@"0" andPage:[NSString stringWithFormat:@"%lu",(unsigned long)self.page] andLimit:self.limitSize type:self.collectGoodsType==CollectGoodsTypeOnline?@"online":@"offline" success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        
        if (state.status) {
            NSArray *array = dic[@"data"][@"data"];
            if (array>0) {
                [self.goodsArray addObjectsFromArray:[CollectParser mj_objectArrayWithKeyValuesArray:array]];
                [self.tableView reloadData];
                self.page++;
            }
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}

- (void)addSubViews {
    if (self.goodsArray.count==0) {
        [self addDefaultView];
        
    } else {
        [self.tableView reloadData];
        
    }
}

- (void)addDefaultView {
    self.defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame),CGRectGetHeight(self.view.frame))];
    self.defaultView.defaultViewType = DefaultViewTypeNormal;
    self.defaultView.defaultDelegete = self;
    [self.view addSubview:self.defaultView];
}

- (void)viewDidLayoutSubviews {
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - DefaultViewDelegate
- (void)buttonClicktoMall {
    //线上到商城首页,线下到线下店铺
    if (self.collectGoodsType==CollectGoodsTypeOnline) {
        [self.tabBarController setSelectedIndex:2];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        //退出当前tab
        [self.navigationController  pushViewController:[OfflineShopHomeViewController new] animated:YES];
    }

}

- (void)deleteCollectionWithId:(NSString *)product_id {
    //弹出警告框
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"确认取消收藏？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [HTTPManager deleteCollectionInfoIsOffline:self.collectGoodsType==CollectGoodsTypeOffline?YES:NO WithId:product_id success:^(NSDictionary * _Nullable dic, resultObject *state) {
            if (state.status) {
                [XSTool showToastWithView:self.view Text:@"成功取消收藏"];
                if (self.goodsArray.count<=1) {
                    
                    [self.tableView removeFromSuperview];
                    [self addDefaultView];
                } else {
                    for (CollectParser *collect in self.goodsArray) {
                        if ([collect.product_id  isEqualToString: product_id]) {
                            [self.goodsArray removeObject:collect];
                            [self.tableView reloadData];
                            break;
                        }
                    }
                    
                }
            }else{
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } failure:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
        
    }];
    [alertControl addAction:cancelAction];
    [alertControl addAction:okAction];
    [self presentViewController:alertControl animated:YES completion:nil];
    
}

- (void)clickToGoodsDetailPage:(NSString *)product_id goodsName:(NSString *)goodsName {
    //判断线上还是线下
    if (self.collectGoodsType==CollectGoodsTypeOnline) {
        GoodsDetailViewController *goodsCopntroller = [GoodsDetailViewController new];
        goodsCopntroller.goodsID = product_id;
        [self.navigationController pushViewController:goodsCopntroller animated:YES];
    }else{
        //        http://baoman205-zgl.dsceshi.cn/wap/#/offline/productdetail/187864da26a711e881a2005056bcde5f
        XSBaseWKWebViewController *vc=[XSBaseWKWebViewController new];
        vc.urlStr=[NSString stringWithFormat:@"%@/wap/#/offline/productdetail/%@?device=%@",ImageBaseUrl,product_id,[XSTool getStrUseKey:APPAUTH_KEY]];
        vc.urlType=WKWebViewURLOfflineGoodsDetail;
        vc.navigationItem.title=goodsName;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CollectParser *parser = self.goodsArray[indexPath.row];
    //判断线上还是线下
    if (self.collectGoodsType==CollectGoodsTypeOnline) {
        GoodsDetailViewController *goodsCopntroller = [GoodsDetailViewController new];
        goodsCopntroller.goodsID = parser.product_id;
        [self.navigationController pushViewController:goodsCopntroller animated:YES];
    }else{
        XSBaseWKWebViewController *vc=[XSBaseWKWebViewController new];
        vc.urlStr=[NSString stringWithFormat:@"%@/wap/#/offline/productdetail/%@?device=%@",ImageBaseUrl,parser.product_id,[XSTool getStrUseKey:APPAUTH_KEY]];
        vc.urlType=WKWebViewURLOfflineGoodsDetail;
        vc.navigationItem.title=parser.product_name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr = [NSString stringWithFormat:@"goodCell%ld",(long)indexPath.row];
    GoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[GoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[GoodsTableViewCell idString]];
    }
    CollectParser *parser = self.goodsArray[indexPath.row];
    cell.parser = parser;
    cell.collectDelegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.goodsArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return   170;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
