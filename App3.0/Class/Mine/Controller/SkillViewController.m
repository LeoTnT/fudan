//
//  SkillViewController.m
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SkillViewController.h"

@interface SkillViewController ()

@end

@implementation SkillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置子视图
    [self addSubViews];
}
#pragma mark - 设置子视图
-(void)addSubViews{
    //设置导航条
    self.navigationItem.title=Localized(@"我的技能");
    self.navigationController.navigationBarHidden = NO;
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"edit") action:^{
        [self developing];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //当前已经点亮技能标签
    UILabel *haveClicked=[[UILabel alloc] initWithFrame:CGRectMake(10, navBarHeight+STATUS_HEIGHT+10, mainWidth, 15)];
    haveClicked.text=@"当前已点亮的技能";
    haveClicked.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:haveClicked];
    self.view.backgroundColor=BG_COLOR;
    //技能所在的视图
    UIView *skillView=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(haveClicked.frame), mainWidth, 200)];
    skillView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:skillView];
    //技能按钮
    NSArray *skillArray=@[@"贴壁纸",@"电路改造",@"防水",@"木地板安装",@"贴壁纸贴壁纸贴壁纸"];
    //创建技能按钮
    for (int i=0; i<skillArray.count; i++) {
        UIButton *btn=[[UIButton alloc] init];
        [btn setTitle:[skillArray objectAtIndex:i] forState:UIControlStateNormal];
        [btn setTitleColor:mainGrayColor forState:UIControlStateNormal];
        //设置尺寸
        CGFloat height=40;
        CGFloat width=(mainWidth-4*SKILL_BTN_SPACE)/3.0;
        //位置
        //行数
        int line=i/3;
        //列数
        int column=i%3;
        btn.frame=CGRectMake((column+1)*SKILL_BTN_SPACE+column*width, (line+1)*SKILL_BTN_SPACE+line*height, width, height);
        //设置边框
        btn.layer.cornerRadius=3;
        btn.layer.borderColor=LINE_COLOR.CGColor;
        btn.layer.borderWidth=1;
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        [skillView addSubview:btn];
        [btn addTarget:self action:@selector(developing) forControlEvents:UIControlEventTouchUpInside];
    }
    //点亮技能按钮
        XSCustomButton *clickBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10,120,mainWidth-20, 50) title:@"点亮新技能" titleColor:[UIColor whiteColor] fontSize:16 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [clickBtn setBorderWith:1 borderColor:[mainColor CGColor] cornerRadius:5];
        [clickBtn addTarget:self action:@selector(developing) forControlEvents:UIControlEventTouchUpInside];
        [skillView addSubview:clickBtn];
        skillView.bounds=CGRectMake(0, 0, mainWidth, CGRectGetMaxY(clickBtn.frame)+10);
}
#pragma mark-新功能开发中
-(void)developing{
     [XSTool showToastWithView:self.view Text:@"功能正在开发中，请谅解。"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
