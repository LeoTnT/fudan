//
//  CollectionViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CollectionViewController.h"
//#import <HMSegmentedControl.h>
#import "GoodsViewController.h"
//#import "ShopViewController.h"

@interface CollectionViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
//@property (nonatomic, strong) HMSegmentedControl *segmentControl;
/** segment */
@property (nonatomic, strong) UISegmentedControl * segmentControl;
//@property (nonatomic, assign) CGFloat segmentHeight;

@end

@implementation CollectionViewController
#pragma mark - lazy lodding
- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, mainWidth, MAIN_VC_HEIGHT-50)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*2, MAIN_VC_HEIGHT-50);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
//        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (UISegmentedControl *)segmentControl {
    if (_segmentControl == nil) {
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"线上商品",@"线下商品"]];
        _segmentControl.layer.cornerRadius = 4;
        _segmentControl.clipsToBounds = YES;
        
        [_segmentControl addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
        
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(13)];
        attrs[NSForegroundColorAttributeName] = XSYCOLOR(0x1C6AE7);
        
        NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
        selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
        selectedAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
        
        [_segmentControl setTitleTextAttributes:attrs forState:UIControlStateNormal];
        [_segmentControl setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];

        _segmentControl.tintColor = XSYCOLOR(0x1C6AE7);//XSYCOLOR(0x334659);
        _segmentControl.backgroundColor = [UIColor clearColor];
        _segmentControl.selectedSegmentIndex = 0;
        
    }
    return _segmentControl;
}

//- (HMSegmentedControl *)segmentControl {
//    if (_segmentControl == nil) {
//        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 50)];
//        _segmentControl.sectionTitles = @[@"商品",@"店铺"];
//        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR};
//        _segmentControl.selectedSegmentIndex = 0;
//        _segmentControl.selectionIndicatorHeight = 2;
//        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
//        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
//        //        _segmentControl.borderType = HMSegmentedControlBorderTypeRight;
//        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:16],NSFontAttributeName ,nil];
//        [_segmentControl setTitleTextAttributes:dic];
//        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
//    }
//    return _segmentControl;
//}

#pragma mark - life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
//    self.segmentHeight = 50;
    
    //设置子视图
    [self addSubViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - private
- (void)addSubViews {
    self.navigationItem.title = Localized(@"收藏商品");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];

    self.view.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    [self.view addSubview:self.segmentControl];
    [self.segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).offset(6);
        make.size.mas_equalTo(CGSizeMake(FontNum(220), 30));
    }];
    
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.segmentHeight-0.5, mainWidth, 0.5)];
//    for (int i=1; i<2; i++) {
//        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(mainWidth/2*i, 10, 1, self.segmentHeight-20)];
//        line1.backgroundColor = LINE_COLOR;
//        [self.view addSubview:line1];
//
//    }
//    lineView.backgroundColor = BG_COLOR;
//    [self.view addSubview:lineView];
    GoodsViewController *goods1VC = [[GoodsViewController alloc] init];
    goods1VC.collectGoodsType = CollectGoodsTypeOnline;
    [self addChildViewController:goods1VC];
    goods1VC.view.frame = CGRectMake(0, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
    [self.scrollView addSubview:goods1VC.view];
    GoodsViewController *goods2VC = [[GoodsViewController alloc] init];
    goods2VC.collectGoodsType = CollectGoodsTypeOffline;
    [self addChildViewController:goods2VC];
    goods2VC.view.frame = CGRectMake(mainWidth, 0, mainWidth, CGRectGetHeight(self.scrollView.frame));
    [self.scrollView addSubview:goods2VC.view];
    
}

-(void)segmentClick:(UISegmentedControl *)segment
{
    XSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segment.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segment.selectedSegmentIndex, 0) animated:YES];
}

//- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
//    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
//    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
//
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page];
//    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
}

//- (void)setType:(NSInteger)type {
//    _type = type;
//    self.segmentControl.selectedSegmentIndex = type;
//    [self.scrollView setContentOffset:CGPointMake(mainWidth*type, 0) animated:YES];
//}

@end
