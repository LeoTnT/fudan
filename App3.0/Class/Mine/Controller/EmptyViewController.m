//
//  EmptyViewController.m
//  App3.0
//
//  Created by mac on 2017/7/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "EmptyViewController.h"
#import "DefaultView.h"

@interface EmptyViewController ()
@property (nonatomic, strong) DefaultView *defaultView;//默认界面
@end

@implementation EmptyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = self.title;
    [self.view addSubview:self.defaultView];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (DefaultView *)defaultView {
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.button.hidden = YES;
        _defaultView.titleLabel.text = @"空空如也";
    }
    return _defaultView;
}
@end
