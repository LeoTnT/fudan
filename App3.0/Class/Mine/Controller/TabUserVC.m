//
//  TabUserVC.m
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TabUserVC.h"
#import "UserTopCell.h"
#import "UserFormCell.h"
#import "UserApplicationCell.h"
#import "UserServiceCell.h"
#import "FormsVC.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "WalletVC.h"
#import "CartVC.h"
#import "InstallViewController.h"
#import "MySkillViewController.h"
#import "FansCircleViewController.h"
#import "CollectionViewController.h"
#import "CollectionShopsViewController.h"
#import "HistoryViewController.h"
#import "ReportFormsViewController.h"
#import "UserInstance.h"
#import "PersonViewController.h"
#import "OrderFormModel.h"
#import "UserQRView.h"
#import "BusinessMainViewController.h"
#import "TaiHeHuiViewController.h"
#import "EmptyViewController.h"
#import "TabSkillVC.h"
#import "SkillPublishViewController.h"
#import "SetupViewController.h"
#import "ADDetailWebViewController.h"
#import "UserModel.h"
#import "UserQRViewController.h"
#import "QRCodeViewController.h"
#import "DisplayStyleChangeVC.h"
/** 快速买单记录 */
#import "QuickPayRecordsVC.h"
/** 新登录注册页 */
#import "UserLoginViewController.h"
#import "RefundWebViewController.h"
#import "OffLineWebViewController.h"
#import "ApplyTypeListViewController.h"

#import "FDCardTicketController.h"

@interface TabUserVC () <UITableViewDelegate, UITableViewDataSource, UserTopCellDelegate, UserFormCellDelegate, UserApplicationCellDelegate, UserServiceCellDelegate>
{
    UIView *_qrBgView;
    UserQRView *_userQrView;
    LoginDataParser *_parser;//用户信息
    NSMutableArray *_numberAry;//各类订单数量
    
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *signInBtn;
@property (nonatomic, strong) UserParser *userInfo;
@property (strong, nonatomic) NSArray *dataArray;   // 功能按键
@end

@implementation TabUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    _numberAry = [NSMutableArray array];

    self.navigationItem.title = @"我";
    self.automaticallyAdjustsScrollViewInsets = NO;
    _parser = [[UserInstance ShardInstnce] getUserInfo];
//    self.dataArray = @[@{@"image":@"user_list_wallet",@"title":@"钱包"},@{@"image":@"user_list_cart",@"title":@"购物车"},@{@"image":@"user_list_form",@"title":Localized(@"报表")},@{@"image":@"user_list_popularize",@"title":Localized(@"me_item_spread")},@{@"image":@"user_list_display",@"title":Localized(@"展示面板")},@{@"image":@"user_list_setting",@"title":Localized(@"me_item_setting")},@{@"image":@"user_list_customer",@"title":Localized(@"客服")},@{@"image":@"user_list_merchants",@"title":@"商家版"},@{@"image":@"user_list_fancircle",@"title":@"粉丝圈"},@{@"image":@"user_list_history",@"title":Localized(@"历史")},@{@"image":@"user_list_fastBuy",@"title":Localized(@"快速买单")}];
//
    
     self.dataArray = @[@{@"image":@"user_list_wallet",@"title":@"钱包"},
                        @{@"image":@"user_list_cart",@"title":@"购物车"},
                        @{@"image":@"user_list_fancircle",@"title":@"粉丝圈"},
                        @{@"image":@"user_list_merchants",@"title":@"商家版"},
                        @{@"image":@"user_list_form",@"title":Localized(@"报表")},
                        @{@"image":@"user_list_myskill",@"title":Localized(@"我的技能")},
                        @{@"image":@"user_list_skill",@"title":Localized(@"技能交换")},
                        @{@"image":@"user_list_popularize",@"title":Localized(@"分享APP")},
                        @{@"image":@"user_list_setting",@"title":Localized(@"me_item_setting")},
                        @{@"image":@"user_list_crowd",@"title":Localized(@"众筹")},
                        @{@"image":@"user_list_fastBuy",@"title":Localized(@"快速买单")},
                        @{@"image":@"user_list_coupon",@"title":Localized(@"优惠券")},
                        @{@"image":@"user_list_display",@"title":Localized(@"展示面板")},
                         @{@"image":@"user_list_customer",@"title":Localized(@"客服")},
                        @{@"image":@"user_list_help",@"title":Localized(@"help")},@{@"image":@"user_list_help",@"title":@"C2C"}];
    
    
    [self.view addSubview:self.tableView];
    // 添加二维码弹出框
//    UIButton *QRBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [QRBtn setBackgroundImage:[UIImage imageNamed:@"user_top_qr"] forState:UIControlStateNormal];
//    [QRBtn addTarget:self action:@selector(topQRClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:QRBtn];
//    [QRBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.view).offset(12);
//        make.top.mas_equalTo(self.view).offset(30);
//    }];
    
//    _qrBgView = [[UIView alloc] initWithFrame:self.view.bounds];
//    _qrBgView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//    _qrBgView.hidden = YES;
//    [[[UIApplication sharedApplication] keyWindow] addSubview:_qrBgView];
//    _userQrView = [[UserQRView alloc] initWithFrame:CGRectMake(mainWidth*0.1, mainHeight+mainWidth+100, mainWidth*0.8, mainWidth*0.8)];
//    _userQrView.hidden = YES;
//
//    [[[UIApplication sharedApplication] keyWindow] addSubview:_userQrView];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(qrBgTap)];
//    [_qrBgView addGestureRecognizer:tap];
    
    if ([AppConfigManager ShardInstnce].appConfig.is_enable_sign) {
        // 签到
        [self createSignedView];
    }
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    // 隐藏nav bar
//    self.navigationController.navigationBarHidden = YES;
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 允许侧滑
//    self.sidePanelController.allowRightSwipe = YES;
    [self getInfo];
   
}



-(void)getInfo{
    _parser = [[UserInstance ShardInstnce] getUserInfo];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [HTTPManager getOrderStatusCountSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        
        NSArray *numArray = dic[@"data"];

        if (numArray.count>0) {
            _numberAry = [NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%@",[numArray firstObject] ],[NSString stringWithFormat:@"%@",numArray[1]],[NSString stringWithFormat:@"%@",numArray[2]],[NSString stringWithFormat:@"%@",numArray[3]],@"", nil];
            NSLog(@"%@",_numberAry);
            NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
            [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    } failure:^(NSError * _Nonnull error) {

    }];
    
    [HTTPManager getUserInfoSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.userInfo = [UserParser mj_objectWithKeyValues:dic];
            [UserInstance ShardInstnce].userInfo = self.userInfo.data;
            [UserInstance ShardInstnce].trueName = self.userInfo.data.truename;
            if ([AppConfigManager ShardInstnce].appConfig.is_enable_sign) {
                if (!self.userInfo.data.isSign) {
                    [self showSignView];
                } else {
                    if (self.signInBtn) {
                        [self hideSignView];
                    }
                    
                }
            }
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // 禁止侧滑
    self.sidePanelController.allowRightSwipe = NO;
//    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)qrBgTap
//{
//    _qrBgView.hidden = YES;
//    [UIImageView animateWithDuration:0.3 animations:^{
//        _userQrView.frame = CGRectMake(mainWidth*0.1, mainHeight+mainWidth+100, mainWidth*0.8, mainWidth*0.8);
//    } completion:^(BOOL finished) {
//        _userQrView.hidden = YES;
//    }];
//}

- (void)createSignedView {
    // 签到
    UIImage *signImage = [UIImage imageNamed:@"user_signin"];
    CGFloat width = CGImageGetWidth(signImage.CGImage)/2;
    CGFloat height = CGImageGetHeight(signImage.CGImage)/2;
    self.signInBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.signInBtn.frame = CGRectMake(mainWidth, mainHeight-80-height, width, height);
    [self.signInBtn setImage:[UIImage imageNamed:@"user_signin"] forState:UIControlStateNormal];
    [self.signInBtn addTarget:self action:@selector(signInAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.signInBtn];
    self.signInBtn.hidden = YES;
}

- (void)showSignView {
    self.signInBtn.hidden = NO;
    self.signInBtn.enabled = NO;
    
    UIImage *signImage = [UIImage imageNamed:@"user_signin"];
    CGFloat width = CGImageGetWidth(signImage.CGImage)/2;
    CGFloat height = CGImageGetHeight(signImage.CGImage)/2;
    [UIView animateWithDuration:0.2 animations:^{
        self.signInBtn.frame = CGRectMake(mainWidth-width, mainHeight-80-height, width, height);
    } completion:^(BOOL finished) {
        self.signInBtn.enabled = YES;
    }];
}

- (void)hideSignView {
    self.signInBtn.enabled = NO;
    UIImage *signImage = [UIImage imageNamed:@"user_signin"];
    CGFloat width = CGImageGetWidth(signImage.CGImage)/2;
    CGFloat height = CGImageGetHeight(signImage.CGImage)/2;
    [UIView animateWithDuration:0.2 animations:^{
        self.signInBtn.frame = CGRectMake(mainWidth, mainHeight-80-height, width, height);
    } completion:^(BOOL finished) {
        [self.signInBtn removeFromSuperview];
        self.signInBtn = nil;
    }];
}

- (void)signInAction {
    ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
    NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
    if (VUE_ON) {
        adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/user/sign?device=%@",ImageBaseUrl,gui];
    } else {
        adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
    }
    
    adWeb.navigationItem.title = @"签到";
    [self.navigationController pushViewController:adWeb animated:YES];
}

#pragma mark - User Delegate
- (void)topAvatarClick
{
    PersonViewController *personVC = [[PersonViewController alloc] init];
    personVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:personVC animated:YES];
    
}

- (void)topQRClick
{
//    _qrBgView.hidden = NO;
//    _userQrView.hidden = NO;
//    [UIImageView animateWithDuration:0.3 animations:^{
//        _userQrView.frame = CGRectMake(mainWidth*0.1, (mainHeight-mainWidth)/2, mainWidth*0.8, mainWidth*0.8);
//    }];
    UserQRViewController *qrVC = [[UserQRViewController alloc] init];
    [self.navigationController pushViewController:qrVC animated:YES];
    
//    QRCodeViewController *qrVC = [[QRCodeViewController alloc] init];
//    qrVC.is_shop = self.userInfo.data.is_shop;
//    qrVC.qrImageString = self.userInfo.data.qrcode;
//    [self.navigationController pushViewController:qrVC animated:YES];
}

- (void)topRightNavClick
{
//    self.tabBarController.selectedIndex = 0;
    /********** 测试 **********/
    FDCardTicketController *cartTicketVC = [[FDCardTicketController alloc] init];
    cartTicketVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:cartTicketVC animated:YES];
}

- (void)formAreaClick:(NSInteger)index
{
    NSInteger newIndex = index;
    if (index == 4) {
        
        RefundWebViewController *adWeb=[[RefundWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (VUE_ON) {
            adWeb.urlStr =
            [NSString stringWithFormat:@"%@/wap/#/order/refund/list?device=%@",ImageBaseUrl,gui];
            adWeb.urlType =  WKWebViewURLRefundList;
        } else {
            //        adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
        }
        
        adWeb.navigationItem.title = @"退款/退货详情";
        [self.navigationController pushViewController:adWeb animated:YES];
    } else {
        
        FormsVC *formVC = [[FormsVC alloc] initWithFormType:(FormType)(newIndex+1)];
        formVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:formVC animated:YES];
    }
   
}

- (UIViewController * )getNameWithClass:(NSString *)className {
    Class cla = NSClassFromString(className);
    return [cla new];
}

- (void)appAreaClick:(NSInteger)index
{
    NSDictionary *dic = self.dataArray[index];
    NSString *titleStr = [NSString stringWithFormat:@"%@",dic[@"title"]];
    
    if ([titleStr isEqualToString:@"钱包"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"WalletVC"] animated:YES];
    }else if ([titleStr isEqualToString:@"购物车"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"CartVC"] animated:YES];

    }else if ([titleStr isEqualToString:@"粉丝圈"]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"FansCircleViewController"] animated:YES];

    }else if ([titleStr isEqualToString:@"商家版"]) {
        if ([self.userInfo.data.is_shop isEqualToString:@"offline"]) {
            [XSTool showToastWithView:self.view Text:@"您的账号不支持此功能！"];
            return;
        }
        
        if ([self.userInfo.data.is_shop isEqualToString:@"online"]) {
            //线上店铺
            BusinessMainViewController *businessController = [[BusinessMainViewController alloc] init];
            [XSTool showProgressHUDWithView:self.view];
            @weakify(self);
            [HTTPManager getSupplyRegisterEnterInfoSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    BusinessRegisterParser *registerParser = [BusinessRegisterParser mj_objectWithKeyValues:dic[@"data"]];
                    if ([registerParser.disabled integerValue]==1) {
                        [XSTool showToastWithView:self.view Text:@"您的商家权限已被禁用，请联系平台管理员！"];
                    } else {
                        businessController.businessParser = registerParser;
                        businessController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:businessController animated:YES];
                    }
                    
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } fail:^(NSError * _Nonnull error) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        }
        
        //未申请，或者申请失败
        if (isEmptyString(self.userInfo.data.is_shop)) {
            ApplyTypeListViewController *controller = [[ApplyTypeListViewController alloc]  init];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
       
    }else if ([titleStr isEqualToString:Localized(@"报表")]) {
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (VUE_ON) {
            adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/forms?device=%@",ImageBaseUrl,gui];
        } else {
            adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/baobiao/index/device/%@",ImageBaseUrl,gui];
        }
        adWeb.urlType = WKWebViewURLNormal;
        adWeb.navigationItem.title = Localized(@"报表");
        [self.navigationController pushViewController:adWeb animated:YES];

    }else if ([titleStr isEqualToString:Localized(@"收藏")]) {
        CollectionViewController *collect = [[CollectionViewController alloc] init];
        collect.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:collect animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"我的技能")]) {
        MySkillViewController *skill=[[MySkillViewController alloc]init];
        skill.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:skill animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"技能交换")]) {
        TabSkillVC *tabSkill=[[TabSkillVC alloc] init];
        tabSkill.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:tabSkill animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"历史")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"HistoryViewController"] animated:YES];

    }else if ([titleStr isEqualToString:Localized(@"分享APP")]) {
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (VUE_ON) {
            adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/download/share?device=%@",ImageBaseUrl,gui];
        } else {
            adWeb.urlStr = [NSString stringWithFormat:@"%@/mobile/user/mysharelink/device/%@",ImageBaseUrl,gui];
        }
        adWeb.urlType = WKWebViewURLShare;
        adWeb.navigationItem.title = Localized(@"分享APP");
        [self.navigationController pushViewController:adWeb animated:YES];
    }else if ([titleStr isEqualToString:Localized(@"me_item_setting")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"SetupViewController"] animated:YES];

    }else if ([titleStr isEqualToString:Localized(@"展示面板")]) {
        DisplayStyleChangeVC *vc = [[DisplayStyleChangeVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        [vc setTapAction:^(NSInteger index) {
            [UserInstance ShardInstnce].displayStyle = index;
            [self.tableView reloadData];
        }];
    }else if ([titleStr isEqualToString:Localized(@"快速买单")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"QuickPayRecordsVC"] animated:YES];

    }else if ([titleStr isEqualToString:Localized(@"优惠券")]) {//线下商家
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (guid.length <= 0) {
            guid = @"app";
        }
        NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/offline/coupons?device=%@",ImageBaseUrl,guid];
        OffLineWebViewController *vc=[[OffLineWebViewController alloc] init];
        vc.urlStr = urlStr;
        vc.urlType = WKWebViewURLOffLine;
        vc.navigationItem.title= Localized(@"我的兑换券");
        [self xs_pushViewController:vc];
    }else if ([titleStr isEqualToString:Localized(@"众筹")]) {
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (guid.length <= 0) {
            guid = @"app";
        }
        NSString *urlStr = [NSString stringWithFormat:@"%@/wap/#/crowdfund/order/list?device=%@",ImageBaseUrl,guid];
        OffLineWebViewController *vc=[[OffLineWebViewController alloc] init];
        vc.urlStr = urlStr;
        vc.urlType = WKWebViewURLOffLine;
        vc.navigationItem.title= Localized(@"众筹");
        [self xs_pushViewController:vc];
    }else if ([titleStr isEqualToString:Localized(@"客服")]) {
        [self.navigationController pushViewController:[self getNameWithClass:@"CustomerViewController"] animated:YES];

    }else if ([titleStr isEqualToString:Localized(@"help")]) {
        XSBaseWKWebViewController *adWeb = [[XSBaseWKWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        if (VUE_ON) {
//            、、http://masterv3.xsy.dsceshi.cn/wap/#/help
            adWeb.urlStr = [NSString stringWithFormat:@"%@/wap/#/help?device=%@",ImageBaseUrl,gui];
        }
       
        adWeb.urlType = WKWebViewURLNormal;
        adWeb.navigationItem.title = Localized(@"help");
        [self.navigationController pushViewController:adWeb animated:YES];
        
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 2;
    }
    if (section == 2) {
        if ([UserInstance ShardInstnce].displayStyle == 0) {
            return 1;
        } else {
            return self.dataArray.count;
        }
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 218+70;
    }
    else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            return 80;
        }
        else {
            return 44;
        }
    }
    else if (indexPath.section == 2) {
        if ([UserInstance ShardInstnce].displayStyle == 0) {
            NSInteger row = ceil(self.dataArray.count/3.0);
            return row*100;
        } else {
            return 44;
        }
    }

    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 12;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        FormsVC *formVC = [[FormsVC alloc] initWithFormType:FormTypeAll];
        formVC.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:formVC animated:YES];
    } else if (indexPath.section == 2) {
        [self appAreaClick:indexPath.row];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            static NSString *CellIdentifier = @"userFormCell0";
            UserFormCell *cell = (UserFormCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UserFormCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.delegate = self;                
            }
            cell.numberArray = _numberAry;
            return cell;
        }
        else if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"userFormCell1";
            UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
                cell.textLabel.text = Localized(@"personal_my_orders");
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.detailTextLabel.text = @"查看全部已购宝贝";
                cell.detailTextLabel.textColor = COLOR_999999;
                cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
                
//                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, mainWidth, 0.5)];
//                lineView.backgroundColor = LINE_COLOR;
//                [cell addSubview:lineView];
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            return cell;
        }
        
    }
    else if (indexPath.section == 2) {
        if ([UserInstance ShardInstnce].displayStyle == 0) {
            static NSString *CellIdentifier = @"userAppCell1";
            UserApplicationCell *cell = (UserApplicationCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UserApplicationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier dataArray:self.dataArray];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.delegate = self;
            }
            return cell;
        } else {
            static NSString *CellIdentifier = @"userAppCell1_1";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(12, 43.5, mainWidth-12, 0.5)];
                lineView.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
                [cell addSubview:lineView];
            }
            
            NSDictionary *dic =self.dataArray[indexPath.row];
            cell.imageView.image = [UIImage imageNamed:dic[@"image"]];
            cell.textLabel.text = dic[@"title"];
            return cell;
        }
        
    }
    else if (indexPath.section == 3) {
        static NSString *CellIdentifier = @"userServiceCell1";
        UserServiceCell *cell = (UserServiceCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UserServiceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        return cell;
    }
    static NSString *CellIdentifier = @"userTopCell";
    UserTopCell *cell = (UserTopCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UserTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    cell.parser = _parser;
    if (self.userInfo) {
//        cell.userInfo = self.userInfo.data;
    }

    [cell setTapAction:^(NSInteger index){
        if (index == 0) {
            CollectionViewController *collectController = [[CollectionViewController alloc] init];
//            collectController.type = index;
            [self.navigationController pushViewController:collectController animated:YES];
        } else if (index == 1) {
            CollectionShopsViewController *collectShopController = [[CollectionShopsViewController alloc] init];
//            collectShopController.type = index;
            [self.navigationController pushViewController:collectShopController animated:YES];
        } else {
            HistoryViewController *hisVC = [[HistoryViewController alloc] init];
            [self.navigationController pushViewController:hisVC animated:YES];
        }
    }];
    return cell;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-kTabbarHeight) style:UITableViewStyleGrouped];
//        _tableView.backgroundColor = mainColor;
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        
#ifdef __IPHONE_11_0
        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            if (@available(iOS 11.0, *)) {
                _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            }
        }
#endif
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, -mainHeight, mainWidth, mainHeight)];
        bgView.backgroundColor = [UIColor hexFloatColor:@"369ff2"];
        [_tableView addSubview:bgView];
    }
    return _tableView;
}

@end
