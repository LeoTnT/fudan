//
//  CollectModel.h
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectParser : NSObject
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *w_time;
@end

@interface CollectModel : NSObject

@end
