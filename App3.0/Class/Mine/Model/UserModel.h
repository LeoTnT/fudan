//
//  UserModel.h
//  App3.0
//
//  Created by nilin on 2017/3/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserDataParser : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *approve_mobile;
@property (nonatomic, copy) NSString *approve_user;//实名认证 -1未提交 0已提交 1通过 2拒绝
@property (nonatomic, copy) NSString *approve_supply;// 店铺是否认证 (1是/0否/-1未提交/2拒绝)
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *rank;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *pay_password;//支付密码 (未设置为空/设置为******)
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *logo_fandom_bg;
@property (nonatomic,copy) NSString *card_no;//身份证号

@property (nonatomic, copy) NSString *img_card;
@property (nonatomic, copy) NSString *comment;
@property (nonatomic, assign) unsigned int isSign;

@property (nonatomic, copy) NSString *rank_name;    // 等级
@property (nonatomic, copy) NSString *fav_product_num;  // 收藏商品
@property (nonatomic, copy) NSString *fav_supply_num;   // 收藏店铺
@property (nonatomic, copy) NSString *history_num;      // 浏览记录
@property (nonatomic, copy) NSString *qrcode_type;      // 收款码类型(空为个人|online线上|offline线下)
@property (nonatomic, copy) NSString *is_shop;      // 是否店铺(空为个人|online线上|offline线下)
@property (nonatomic, copy) NSString *qrcode;      // 收款码图片
@property (nonatomic, copy) NSString *is_google_verify;
@end

@interface UserParser : NSObject

@property (nonatomic, retain) UserDataParser *data;
@end

@interface CustomerModel : NSObject
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, assign) unsigned int rank;
@end

@interface UserModel : NSObject

@end
