//
//  UserInstance.h
//  App3.0
//
//  Created by mac on 17/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginModel.h"
#import "SetupModel.h"
#import "UserModel.h"

@interface UserInstance : NSObject
/**用户编号（新）*/
@property(nonatomic,readonly)NSString *uid;
/**用户编号*/
@property(nonatomic,readonly)NSString *userName;
/**用户昵称*/
@property(nonatomic,readonly)NSString *nickName;
/**用户真实姓名*/
@property(nonatomic,copy)NSString *trueName;
/**用户头像*/
@property(nonatomic,readonly)NSString *avatarImgUrl;
/**用户手机号*/
@property(nonatomic,readonly)NSString *mobile;
/**性别*/
@property(nonatomic,readonly)NSString *sex;

@property (assign, nonatomic) NSInteger displayStyle;

@property (nonatomic ,copy)NSString *guid;

@property (strong, nonatomic) UserDataParser *userInfo;

@property (nonatomic ,strong) NSArray *cityArr;
@property (nonatomic ,strong) NSArray *airArr;

/** 所以交易对信息 */
@property (nonatomic, copy) NSArray * coinDatasArray;

/*
 交易所相关
 */
@property (nonatomic ,assign) BOOL isWhite;

/** 涨的颜色 */
@property (nonatomic, strong) UIColor * roseColor;
/** 涨的颜色 */
@property (nonatomic, copy) NSString * roseColorString;
/** 跌的颜色 */
@property (nonatomic, strong) UIColor * fellColor;
/** 跌的颜色 */
@property (nonatomic, copy) NSString * fellColorString;
/********/

// 首页顶部菜单数组
@property (nonatomic ,strong) NSMutableArray *homeMenuArray;
// 商城顶部菜单数组
@property (nonatomic ,strong) NSMutableArray *mallMenuArray;


- (NSString *)showName;

+(UserInstance*)ShardInstnce;
- (void)logout;
- (LoginDataParser *) getUserInfo;
- (void)setupUserInfo;

- (void)saveAccount:(NSDictionary *)account;
- (void)deleteAccount:(NSDictionary *)account;
- (NSArray *)getAccounts;

// 获取聊天设置
- (ChatSetModel *)getChatSet;
- (void)setChatSet:(ChatSetModel *)model;
- (void)getChatSetFromServer;
- (void)setChatSetToServer:(ChatSetModel *)model;
@end
