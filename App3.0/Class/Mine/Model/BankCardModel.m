//
//  BankCardModel.m
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BankCardModel.h"

@implementation CardTypeDataParser

@synthesize ID,name,image;

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation BankCardDataParser
@synthesize bank_id,ID,is_default,name,bankname,bankaddress,bankcard,bankuser,image;
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end


@implementation BankCardModel

@end
