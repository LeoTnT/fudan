//
//  UserInstance.m
//  App3.0
//
//  Created by mac on 17/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserInstance.h"
#import "LoginModel.h"

static UserInstance* user = nil;

@interface UserInstance ()
{
    __block NSString* uid;
    __block NSString* userName;
    __block NSString* nickName;
    __block NSString* trueName;
    __block NSString* avatarImgUrl;
    __block NSString* mobile;
    __block NSString* sex;
}

@end
@implementation UserInstance
@synthesize uid,userName,nickName,trueName,avatarImgUrl,mobile,sex;
+(UserInstance*)ShardInstnce
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[UserInstance alloc] init];
    });
    return user;
}

- (NSString *)showName {
    return  isEmptyString(self.nickName) ? self.userName:self.nickName;
}


- (void)setupUserInfo
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:USERINFO_LOGIN];
    LoginDataParser *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (userInfo.username != nil && ![userInfo.username isEqualToString:@""]) {
        uid = userInfo.uid;
        userName = userInfo.username;
        nickName = userInfo.nickname;
        trueName = userInfo.truename;
        avatarImgUrl = userInfo.logo;
        mobile = userInfo.mobile;
        sex = userInfo.sex;
        self.guid = userInfo.guid;
    }
    
}

-(LoginDataParser *)getUserInfo{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [user objectForKey:USERINFO_LOGIN];
    LoginDataParser *loginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    return loginInfo;
}

- (void)logout
{
#ifdef ALIYM_AVALABLE
    [[SPKitExample sharedInstance] exampleLogout];
#elif defined EMIM_AVALABLE
    [[EMClient sharedClient] logout:YES];
#else
    
#endif
    
    uid = nil;
    userName = nil;
    nickName = nil;
    trueName = nil;
    avatarImgUrl = nil;
    mobile = nil;
    sex = nil;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:USERINFO_LOGIN];
    [userDefault removeObjectForKey:APPAUTH_KEY];
    [userDefault synchronize];
}

- (void)saveAccount:(NSDictionary *)account {
    NSArray *listArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
    NSMutableArray *mutCopyArr = [NSMutableArray arrayWithArray:listArr];
    for (NSDictionary *dic in listArr) {
        if ([dic[@"account"] isEqualToString:account[@"account"]]) {
            [mutCopyArr removeObject:dic];
            break;
        }
    }
    NSLog(@"%@",mutCopyArr);
    [mutCopyArr insertObject:account atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:mutCopyArr forKey:ACCOUNT_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)deleteAccount:(NSDictionary *)account {
    NSArray *listArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
    NSMutableArray *mutCopyArr = [NSMutableArray arrayWithArray:listArr];
    for (NSDictionary *dic in listArr) {
        if ([dic[@"account"] isEqualToString:account[@"account"]]) {
            [mutCopyArr removeObject:dic];
            break;
        }
    }
    NSLog(@"%@",mutCopyArr);
    [[NSUserDefaults standardUserDefaults] setObject:mutCopyArr forKey:ACCOUNT_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getAccounts {
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST]);
    return [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
}

// 聊天设置
- (void)setChatSet:(ChatSetModel *)model
{
    
    
//    BOOL isVibrate = NO;
//    if (model.is_voice == 1) {
//        isVibrate = YES;
//        if (model.is_vibrate == 0) {
//            isVibrate = NO;
//        }else{
//            isVibrate = YES;
//
//        }
//        [[ChatHelper shareHelper] createSystemSoundWithName:@"message" soundType:@"wav" vibrate:isVibrate];
//    }else{
//        isVibrate = NO;
//        [[ChatHelper shareHelper] createSystemSoundWithName:nil soundType:@"wav" vibrate:isVibrate];
//
//    }
    NSDictionary *dic = model.mj_keyValues;
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:USER_CHATSET];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
}

-(ChatSetModel *)getChatSet {
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:USER_CHATSET];
    ChatSetModel *chatSet = [ChatSetModel mj_objectWithKeyValues:dic];
    if (!chatSet) {
        chatSet = [ChatSetModel new];
        chatSet.is_rec_msg = 1;
        chatSet.is_show_msg = 1;
        chatSet.is_voice = 1;
        chatSet.is_vibrate = 1;
        chatSet.is_not_disturb = 0;
        chatSet.is_receiver_voice = 1;
    }
    return chatSet;
}

- (void)getChatSetFromServer {
    @weakify(self);
    [HTTPManager getMyChatSetWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        if (state.status) {
            ChatSetModel *model = [ChatSetModel mj_objectWithKeyValues:dic[@"data"]];
            [self setChatSet:model];
        }
    } fail:^(NSError *error) {
        
    }];
}

- (void)setChatSetToServer:(ChatSetModel *)model {
    NSDictionary *dic = model.mj_keyValues;
    [HTTPManager setMyChatSetWithParams:dic success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSLog(@"上传成功");
        }
    } fail:^(NSError *error) {
        
    }];
}

// 展示面板风格
- (void)setDisplayStyle:(NSInteger)displayStyle {
    [[NSUserDefaults standardUserDefaults] setInteger:displayStyle forKey:@"displayStyle"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger)displayStyle {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"displayStyle"];
}
@end
