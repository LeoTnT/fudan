//
//  User.m
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "User.h"

@implementation User
#pragma mark- 将字典转为用户对象
+(instancetype)getUserModelWithDic:(NSDictionary *)dic{
    User *user=[[User alloc] init];
    [user setValuesForKeysWithDictionary:dic];
    return user;
}
#pragma mark-将字典数组转为用户对象数组
+(NSArray *)getUserModelsWithDics:(NSArray *)dicArray{
    NSMutableArray *tempArray=[NSMutableArray array];
    for (NSDictionary *dic in dicArray) {
        User *user=[User getUserModelWithDic:dic];
        [tempArray addObject:user];
    }
    return tempArray;
}
#pragma mark-测试方法
-(instancetype)userWithImg:(NSString *)image andNickName:(NSString *)name{
    User *user=[[User alloc] init];
    user.headImg=image;
    user.nickName=name;
    return user;
}
#pragma mark-防止字典与对象不匹配
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end
