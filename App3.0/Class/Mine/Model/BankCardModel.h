//
//  BankCardModel.h
//  App3.0
//
//  Created by nilin on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardTypeDataParser : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;

@end


@interface BankCardDataParser : NSObject
@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *bankname;
@property (nonatomic, copy) NSString *bankaddress;
@property (nonatomic, copy) NSString *bankcard;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *bankuser;
@property (nonatomic, copy) NSString *image;

@end


@interface BankCardModel : NSObject

@end
