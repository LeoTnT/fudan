//
//  ReceiverAddressModel.h
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressAreaParser : NSObject
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@end
@interface AddressParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *zipcode;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, strong) AddressAreaParser *province;
@property (nonatomic, strong) AddressAreaParser *city;
@property (nonatomic, strong) AddressAreaParser *county;
@property (nonatomic, strong) AddressAreaParser *town;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *area;
@end

@interface AppointAddressParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *province_code;
@property (nonatomic, copy) NSString *city_code;
@property (nonatomic, copy) NSString *county_code;
@property (nonatomic, copy) NSString *town_code;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *w_time;
@end
@interface ReceiverAddressModel : NSObject

@end
