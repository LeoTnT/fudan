//
//  QuickPayRecordsModel.h
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPManager+Mine.h"

@interface QuickPayRecordsModel : NSObject

/** 买单数组 */
@property (nonatomic, strong) NSArray * data;

/** 订单id */
@property (nonatomic, copy) NSString * ID;

/** 订单表名 */
@property (nonatomic, copy) NSString * tname;

/** 订单编号 */
@property (nonatomic, copy) NSString * pay_no;

/** 订单状态(0未支付|1已支付) */
@property (nonatomic, copy) NSString * status;

/**订单类型(1付给个人|2付给商家) */
@property (nonatomic, copy) NSString * type;

/** 对方名称 */
@property (nonatomic, copy) NSString * name;

/** 对方用户编号 */
@property (nonatomic, copy) NSString * supply_name;

/** 对方用户Logo */
@property (nonatomic, copy) NSString * logo;

/** 消费总额 */
@property (nonatomic, copy) NSString * price_total;

/** 支付金额 */
@property (nonatomic, copy) NSString * price_pay;

/** 优惠金额 */
@property (nonatomic, copy) NSString * price_discount;

/** 支付时间 */
@property (nonatomic, copy) NSString * pay_time;



@end
