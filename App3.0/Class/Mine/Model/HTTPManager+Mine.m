//
//  HTTPManager+Mine.m
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "HTTPManager+Mine.h"

@implementation HTTPManager (Mine)

+(void)requestQuickOrderListData:(NSString *)payType page:(NSString *)page success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary * parameters;
    if (payType != nil) {
        parameters = @{@"page": page , @"pay":payType};
    } else {
        parameters = @{@"page": page};
    }
    [XSHTTPManager post:Url_Buyer_GetQuickOrderList parameters:parameters success:success failure:fail];
}

+(void)requestVerificationData:(NSDictionary *)parameters success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_verification_GetIDCardInfo parameters:parameters success:success failure:fail];
}

+(void)requestAccountBindingData:(NSDictionary *)parameters success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_AccountBinding_GetSocialBindList parameters:parameters success:success failure:fail];
}

+(void)bindEmailRequestWithPassword:(NSString *)pwd Email:(NSString *)email success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary * parameters = @{@"pwd":pwd, @"email":email};
    [XSHTTPManager post:Url_AccountBinding_EmailBind parameters:parameters success:success failure:fail];
}

+(void)getAccountBindingInfoWithType:(NSString *)type code:(NSString *)code openid:(NSString *)openid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:type forKey:@"type"];
    if (code) {
        [dictionary setObject:code forKey:@"code"];
    }
    if (openid) {
        [dictionary setObject:openid forKey:@"openid"];
    }
    [XSHTTPManager post:Url_AccountBinding_GetBindOrSocialInfo parameters:dictionary success:success failure:fail];
}

+(void)toBindAccountWithIscreate:(NSString *)iscreate type:(NSString *)type openid:(NSString *)openid unionid:(NSString *)unionid token:(NSString *)token name:(NSString *)name pass:(NSString *)pass verify:(NSString *)verify nickname:(NSString *)nickname logo:(NSString *)logo sex:(NSString *)sex intro:(NSString *)intro success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"iscreate"] = iscreate;
    dict[@"type"] = type;
    dict[@"openid"] = openid;
    dict[@"unionid"] = unionid;
    if (token) {
        dict[@"token"] = token;
    }
    if ([iscreate integerValue] != 2) {
        dict[@"name"] = name;
        dict[@"pass"] = pass;
        if ([iscreate integerValue] == 1) {
            dict[@"verify"] = verify;
        }
    }
    dict[@"nickname"] = nickname;
    dict[@"logo"] = logo;
    dict[@"sex"] = sex;
    if (intro) {
        dict[@"intro"] = intro;
    }
    
    [XSHTTPManager post:Url_AccountBinding_BindUserBySocial parameters:dict success:success failure:fail];
}

+(void)unbindTheAccountWithType:(NSString *)type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary * dict = @{@"type":type};
    [XSHTTPManager post:Url_AccountBinding_UnbindUser parameters:dict success:success failure:fail];
}

@end
