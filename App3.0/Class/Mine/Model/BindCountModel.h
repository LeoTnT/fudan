//
//  BindCountModel.h
//  App3.0
//
//  Created by nilin on 2017/6/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface  BindCountParser: NSObject
@property (nonatomic, copy) NSString *memo;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *target;
@property (nonatomic, copy) NSString *status;
@end

@interface BindCountModel : NSObject

@end
