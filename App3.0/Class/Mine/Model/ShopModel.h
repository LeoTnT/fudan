//
//  ShopCarModel.h
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ShopCarInfoUrl @"/user/favorite/GetFavoriteList/"
#define DeleteShopCarUrl  @"/user/favorite/Delete/favType/1"
@interface ShopDataParser : NSObject

@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *eva;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *username;


@end

@interface ShopModel : NSObject

@end
