//
//  HistoryModel.h
//  App3.0
//
//  Created by nilin on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface HistoryDataParser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *data_id;
@property (nonatomic, copy) NSString *w_time;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_image;
@property (nonatomic, copy) NSString *product_price;
@property (nonatomic, copy) NSString *product_supply_name;
@property (nonatomic, copy) NSString *supply_id;
@property (nonatomic, copy) NSString *supply_name;
@property (nonatomic, copy) NSString *product_market_price;
//@property (nonatomic, copy) NSString *sell_price;
@property (nonatomic, copy) NSString *supply_logo;
@end



@interface HistoryModel : NSObject



@end
