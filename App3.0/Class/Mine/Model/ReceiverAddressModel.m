//
//  ReceiverAddressModel.m
//  App3.0
//
//  Created by nilin on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReceiverAddressModel.h"
@implementation AddressAreaParser
@synthesize code,name;

@end

@implementation AddressParser

@synthesize ID;
@synthesize is_default;
@synthesize user_id;
@synthesize name;
@synthesize mobile;
@synthesize zipcode;
@synthesize w_time;
@synthesize province;
@synthesize city;
@synthesize county;
@synthesize town;
@synthesize detail;
@synthesize area;
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}

@end

@implementation AppointAddressParser

@synthesize ID,is_default,user_id,name,mobile,province,province_code,city,city_code,county,county_code,town,town_code,description,w_time;
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end
@implementation ReceiverAddressModel
@end
