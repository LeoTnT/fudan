//
//  HTTPManager+Mine.h
//  App3.0
//
//  Created by admin on 2017/12/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"
// 快速买单记录
#define Url_Buyer_GetQuickOrderList      @"/api/v1/buyer/order/GetQuickOrderList"

// 获取实名认证信息
#define Url_verification_GetIDCardInfo      @"/api/v1/user/approve/GetApproveInfo"

// 获取实名认证信息
#define Url_verification_GetIDCardInfomation      @"/api/v1/user/approve/GetApproveInfo"

// 获取账户绑定信息
#define Url_AccountBinding_GetSocialBindList      @"/api/v1/user/login/GetSocialBindList"

// 绑定邮箱
#define Url_AccountBinding_EmailBind      @"/api/v1/user/login/EmailBind"

// 获取绑定信息（qq 微信是否可绑）
#define Url_AccountBinding_GetBindOrSocialInfo      @"/api/v1/user/login/GetBindOrSocialInfo"

//绑定到用户
#define Url_AccountBinding_BindUserBySocial      @"/api/v1/user/login/BindUserBySocial"

//解绑第三方登录
#define Url_AccountBinding_UnbindUser      @"/api/v1/user/login/UnbindUser"

@interface HTTPManager (Mine)

/**
 快速买单列表
 payType    买单类型(1付款列表|2收款列表)
 */
+ (void)requestQuickOrderListData:(nullable NSString *)payType page:(nonnull NSString *)page success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

#pragma mark 我的--》设置--》账号与安全--》实名认证
/**
 实名认证信息
 */
+ (void)requestVerificationData:(nullable NSDictionary *)parameters success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

#pragma mark 我的--》设置--》账号与安全--》账户绑定
/**
 账户绑定信息
 */
+ (void)requestAccountBindingData:(nullable NSDictionary *)parameters success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

/**
 绑定邮箱
 pwd    登录密码
 email  邮箱地址
 */
+ (void)bindEmailRequestWithPassword:(nonnull NSString *)pwd Email:(nonnull NSString *)email success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

/**
 获取绑定信息或互联信息
 type   第三方标识(weixin微信|qq腾讯QQ)
 code   第三方的code
 openid 第三方的openid
 */
+ (void)getAccountBindingInfoWithType:(nonnull NSString *)type code:(nullable NSString *)code openid:(nullable NSString *)openid success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

/**
 绑定账号（微信||QQ）
 iscreate   操作(1新建账号|0绑定原账号|2绑定当前账号)
 type       互联类型(weixin微信|qq腾讯QQ)
 openid     互联openid
 unionid    互联unionid
 token      互联token
 name       手机号(新建必须)|用户编号 (绑定当前账号时不用|其它必须)
 pass       用户密码
 verify     短信验证码   （新建账号必须）
 nickname   昵称
 logo       头像
 sex        性别(0男|1女)
 intro      推荐人
 */
+ (void)toBindAccountWithIscreate:(nonnull NSString *)iscreate type:(nonnull NSString *)type openid:(nonnull NSString *)openid unionid:(nonnull NSString *)unionid token:(nullable NSString *)token name:(nullable NSString *)name pass:(nullable NSString *)pass verify:(nullable NSString *)verify nickname:(nonnull NSString *)nickname logo:(nonnull NSString *)logo sex:(nonnull NSString *)sex intro:(nullable NSString *)intro success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;

/**
 解除绑定账号（QQ||微信）
 type   第三方类型(qq|weixin)
 */
+ (void)unbindTheAccountWithType:(nonnull NSString *)type success:(XSAPIClientSuccess _Nonnull )success fail:(XSAPIClientFailure _Nonnull)fail;


@end
