//
//  User.h
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
/**用户编号*/
@property(nonatomic,copy)NSString *userName;
/**用户昵称*/
@property(nonatomic,copy)NSString *nickName;
/**用户头像*/
@property(nonatomic,copy)NSString *headImg;
/**密码*/
@property(nonatomic,copy)NSString *password;
/**
将字典转为用户对象
 */
+(instancetype)getUserModelWithDic:(NSDictionary *)dic;
/**
将字典数组转为用户对象数组
 */
+(NSArray *)getUserModelsWithDics:(NSArray *)dicArray;
@end
