//
//  VersionUpdateModel.h
//  App3.0
//
//  Created by syn on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface VersionUpdateDataModel :NSObject
@property(nonatomic,strong)NSNumber *client_download_status;
@property(nonatomic,copy)NSString *client_file;
@property(nonatomic,strong)NSNumber *client_force_update;
@property(nonatomic,copy)NSString *client_tips;
@property(nonatomic,copy)NSString *client_version;
@property(nonatomic,copy)NSString *client_url;
@end

@interface VersionUpdateModel : NSObject
@property(nonatomic,strong)VersionUpdateDataModel *data;
@end
