//
//  RemindVersionUpdateView.h
//  App3.0
//
//  Created by syn on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VersionUpdateModel.h"
@protocol RemindVersionUpdateViewDelegate <NSObject>
-(void)closeUpdateView;
@end
@interface RemindVersionUpdateView : UIView
@property(nonatomic,weak) id<RemindVersionUpdateViewDelegate> delegate;
-(instancetype)initWithInfo:(VersionUpdateDataModel *)model;
@end
