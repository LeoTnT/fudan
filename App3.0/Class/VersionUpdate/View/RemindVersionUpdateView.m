//
//  RemindVersionUpdateView.m
//  App3.0
//
//  Created by syn on 2017/8/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RemindVersionUpdateView.h"

@interface RemindVersionUpdateView()
@property(nonatomic,strong)UIScrollView *scroll;
@end
@implementation RemindVersionUpdateView
-(instancetype)initWithInfo:(VersionUpdateDataModel *)model{
    if (self=[super init]) {
        self.frame=CGRectMake(0, 0, mainWidth, mainHeight);
        UIColor *bColor = [UIColor blackColor];
        self.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        UIView *whiteView=[[UIView alloc] init];
        whiteView.layer.masksToBounds=YES;
        whiteView.layer.cornerRadius=20;
        whiteView.backgroundColor=[UIColor whiteColor];
        [self addSubview:whiteView];
        //上方图片
        UIImageView *topImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"update_top_logo"]];
        [whiteView addSubview:topImage];
        //分割线
        UIView *line1=[[UIView alloc] init];
        line1.backgroundColor=mainGrayColor;
        line1.alpha=0.3;
        [whiteView addSubview:line1];
        //标题
        UILabel *tipsLabel=[[UILabel alloc] init];
        tipsLabel.text=@"一次重要的更新";
        tipsLabel.textAlignment=NSTextAlignmentCenter;
        tipsLabel.textColor=TAB_SELECTED_COLOR;
        tipsLabel.font=[UIFont systemFontOfSize:25];
        [whiteView addSubview:tipsLabel];
        //滚动图
        self.scroll=[[UIScrollView alloc] init];
        self.scroll.showsVerticalScrollIndicator=NO;
        [whiteView addSubview:self.scroll];
        UIView *line2=[[UIView alloc] init];
        line2.backgroundColor=mainGrayColor;
        line2.alpha=0.3;
        [whiteView addSubview:line2];
        //马上更新
        UILabel *quickLabel=[[UILabel alloc] init];
        quickLabel.textAlignment=NSTextAlignmentCenter;
        quickLabel.font=[UIFont systemFontOfSize:20];
        quickLabel.textColor=TAB_SELECTED_COLOR;
        quickLabel.text=@"马上更新";
        quickLabel.userInteractionEnabled=YES;
        [quickLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.client_url]];
        }]];
        [whiteView addSubview:quickLabel];
        [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(50);
            make.right.mas_equalTo(self).offset(-50);
            make.centerY.mas_equalTo(self);
            make.height.mas_equalTo(250);
        }];
        [topImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(whiteView);
            //            make.height.mas_equalTo(50);
        }];
        [tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(whiteView);
            make.top.mas_equalTo(topImage.mas_bottom).offset(5);
        }];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(whiteView);
            make.top.mas_equalTo(topImage.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
        [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(whiteView);
            make.top.mas_equalTo(tipsLabel.mas_bottom).offset(10);
            make.bottom.mas_equalTo(line2.mas_top);
        }];
        // 设置scrollView的子视图，即过渡视图contentSize，并设置其约束
        UIView *excessView = [[UIView alloc]init];
        [self.scroll addSubview:excessView];
        [excessView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.and.right.equalTo(self.scroll).with.insets(UIEdgeInsetsZero);
            make.width.equalTo(self.scroll);
        }];
        NSArray *tipsArray=[model.client_tips componentsSeparatedByString:@"#"];
        UIView *lastView =nil;
        for (NSInteger index =0; index <tipsArray.count; index++)
        {
            if ([tipsArray[index] length] > 0) {
                UILabel *label = [[UILabel alloc]init];
                label.textColor=[UIColor colorWithRed:199/255.0 green:199/255.0 blue:199/255.0 alpha:1];
                label.textAlignment =NSTextAlignmentCenter;
                label.text =tipsArray[index];
                label.font=[UIFont systemFontOfSize:15];
                //添加到父视图，并设置过渡视图中子视图的约束
                [excessView addSubview:label];
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.and.right.equalTo(excessView);
                    make.height.mas_equalTo(25);
                    if (lastView)
                    {
                        make.top.mas_equalTo(lastView.mas_bottom);
                    }
                    else
                    {
                        make.top.mas_equalTo(0);
                    }
                }];
                
                lastView = label;
            }
            
        }
        
        // 设置过渡视图的底边距（此设置将影响到scrollView的contentSize）
        [excessView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(lastView.mas_bottom);
        }];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(whiteView);
            make.bottom.mas_equalTo(quickLabel.mas_top);
            make.height.mas_equalTo(0.5);
        }];
        [quickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self);
            make.bottom.mas_equalTo(whiteView);
            make.height.mas_equalTo(40);
        }];
        
        //关闭按钮
        UIButton *closeBtn=[[UIButton alloc] init];
        [closeBtn setBackgroundImage:[UIImage imageNamed:@"chat_close"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(hideRemindView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeBtn];
        if ([model.client_force_update integerValue]) {
            closeBtn.hidden=YES;
        }else{
            closeBtn.hidden=NO;
        }
        [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(whiteView.mas_bottom).offset(20);
        }];
    }
    return self;
}
-(void)hideRemindView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeUpdateView)]) {
        [self.delegate closeUpdateView];
    }
}
@end
