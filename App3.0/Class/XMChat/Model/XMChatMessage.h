//
//  XMChatMessage.h
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GDataXML_HTML/GDataXMLNode.h>


typedef enum : NSUInteger {
    XMPPMessageDirectionSend ,
    XMPPMessageDirectionReceive
} XMPPMessageDirection;

typedef enum : NSUInteger {
    XMPPMessageStatusSuccessed = 0,       /*! \~chinese 发送成功 \~english Successed */
    XMPPMessageStatusFailed,          /*! \~chinese 发送失败 \~english Failed */
    XMPPMessageStatusUnRead,          /*! \~chinese 未读 \~english Failed */
    XMPPMessageStatusRead            /*! \~chinese 已读 \~english Failed */
} XMPPMessageSendState;


typedef enum : NSUInteger {
    XMPPMessageTypeText       = 0 , // 文字
    XMPPMessageTypePicture    = 1 , // 图片
    XMPPMessageTypeVoice      = 2 , // 语音
    XMPPMessageTypeLocation   = 3 , // 位置
    XMPPMessageTypeRedPacket  = 4 , // 红包
    XMPPMessageTypeTransfer   = 5 , // 转账
    XMPPMessageTypeShare      = 6 , // 分享
    XMPPMessageTypeFile       = 7 , // 文件
    XMPPMessageTypeVideo      = 8 , // 视频
    XMPPMessageTypeSystemPush      = 9,   // 系统消息推送消息
    XMPPMessageTypeCall       = 10   // 语音视频通话
} XMPPMessageType;


@interface XMPPBaseMessage :NSObject
/**
 发送状态
 */
@property (nonatomic ,assign)XMPPMessageSendState xmppSendState;

/**
 消息类型
 */
@property (nonatomic ,assign)XMPPMessageType xmppMessageType;


+ (void) xmppUpimage:(NSData *)imageData progress:(void (^)(NSProgress *))uploadProgrees
           completed:(void (^)(NSError *, NSString *))completedBlock;

+ (void) xmppUpVoice:(NSData *)imageData  completed:(void (^)(NSError *, NSString *))completedBlock;

+ (void) xmppUpFile:(NSData *)fileData  completed:(void (^)(NSError *, NSString *))completedBlock;


/**
 多文件上传
 
 @param videoTuple image,data,time,size
 @param completedBlock completedBlock description
 */
+ (void) xmppUpVideo:(RACTuple *)videoData completed:(void (^)(NSError *, NSDictionary *))completedBlock;

@end



@interface XMChatMessage : XMPPBaseMessage

@property (nonatomic ,copy)NSString *xmMessageId;

@property (nonatomic ,copy)NSString *xmlFrom;

@property (nonatomic ,copy)NSString *xmlTo;

@property (nonatomic ,copy)NSString *XMLBody;


// 语音时间
@property (nonatomic ,copy)NSString *strVoiceTime;

@property (nonatomic ,strong)NSURL *voiceUrl;

/**
 判断消息收发人
 */
@property (nonatomic ,assign)XMPPMessageDirection direction;

- (instancetype)initWithMessage:(XMPPMessage *)messageXML ;

@end



