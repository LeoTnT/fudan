//
//  XMChatMessage.m
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMChatMessage.h"
/*
 <message xmlns="jabber:client" type="chat"
 to="11@localhost.localdomain" from="12@localhost.localdomain/1204607085-tigase-11" id="F4BCA9A1-8B9E-4BC1-87B0-FD946830824F">
 <body>21</body>
 <message>message</message>
 <request xmlns="urn:xmpp:receipts"></request>
 
 </message>
 
 <message xmlns="jabber:client" to="12@localhost.localdomain/TigaseMessenger" type="chat" from="11@localhost.localdomain/TigaseMessenger">
 
 <body>http://192.168.0.184:8088/assist/uploadFolder/20180203/20180223151547085.jpg</body><message>image</message>
 
 <request xmlns="jabber:x:oob">
 
 </request>
 
 
 </message>
 */

@implementation XMChatMessage

- (instancetype)initWithMessage:(XMPPMessage *)message {
    if (self = [super init]) {
        NSString *displayName = [[message from] user];
        
        self.xmlFrom = isEmptyString(displayName ) ? @"" : displayName;
        self.xmlTo = [[message to] user];
        self.direction = [self.xmlTo isEqualToString:[XMPPManager sharedManager].stream.myJID.user] ? XMPPMessageDirectionReceive:XMPPMessageDirectionSend;
        NSXMLElement *request = [message elementForName:@"x"];
        
        if ([message hasReceiptResponse]) {
            self.xmppMessageType = XMPPMessageTypeText;
            self.XMLBody = message.body;
        }else if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ){
            for (NSXMLElement *element in message.children) {
                NSString *type = [element.children.firstObject stringValue];
                NSString *time = [element.children.lastObject stringValue];
                if ([type isEqualToString:@"voice"]) {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *path = [paths objectAtIndex:0];
                    NSString *filName = [message.body componentsSeparatedByString:@"/"].lastObject;
                    NSString *filePath = [NSString stringWithFormat:@"%@/%@",path,filName];
                    self.strVoiceTime = time;
                    self.xmppMessageType = XMPPMessageTypeVoice;
                    self.voiceUrl = [NSURL URLWithString:filePath];
                }else if ([type isEqualToString:@"image"]){
                    self.xmppMessageType = XMPPMessageTypePicture;
                    self.XMLBody = message.body;
                    
                }
                
            }
        }
    }
    return self;
}


@end



@implementation XMPPBaseMessage

+ (void) xmppUpimage:(NSData *)imageData progress:(void (^)(NSProgress *))uploadProgrees completed:(void (^)(NSError *, NSString *))completedBlock {
    
    [XMPPBaseMessage xmppUpData:imageData xmppMessageType:XMPPMessageTypePicture progress:uploadProgrees completed:completedBlock];
    
}


+ (void) xmppUpVoice:(NSData *)imageData  completed:(void (^)(NSError *, NSString *))completedBlock {
    
    [XMPPBaseMessage xmppUpData:imageData xmppMessageType:XMPPMessageTypeVoice progress:nil completed:completedBlock];
    
}

+ (void) xmppUpFile:(NSData *)fileData  completed:(void (^)(NSError *, NSString *))completedBlock {
    
    [XMPPBaseMessage xmppUpData:fileData xmppMessageType:XMPPMessageTypeFile progress:nil completed:completedBlock];
    
}

+ (void) xmppUpVideo:(RACTuple *)videoData completed:(void (^)(NSError *, NSDictionary*))completedBlock {
    
    [XMPPBaseMessage xmppRACTuple:videoData  completed:completedBlock];
    
}



+ (void) xmppRACTuple:(RACTuple *)videoTuple completed:(void(^)(NSError *,NSDictionary *))completedBlock {
    
    AFHTTPSessionManager *manager = [XSHTTPManager sharedClient];
 
    NSData *videoData = videoTuple.second;
    UIImage *iii = videoTuple.first;
    NSData *imageData = UIImageJPEGRepresentation(iii, 0);
    NSArray *arr = @[videoData,imageData];
    [manager POST:XMUP_PATH_MULTFILEUPLOAD parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
          for (NSInteger x = 0; x < arr.count; x ++) {
             if (x == 0) {
                [formData appendPartWithFileData:arr[x] name:@"file" fileName:XMPP_VIDEO_FILE mimeType:@"video/mp4"];
            }else{
                [formData appendPartWithFileData:arr[x] name:@"file" fileName:@"123.jpg" mimeType:@"image/png"];
            }
         }
 
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:(NSJSONReadingMutableContainers) error:&error];
        NSLog(@"dic  =%@",dic);
        if (error) {
            completedBlock(error,nil);
        }else{
            completedBlock(nil,dic);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (error) {
            completedBlock(error,nil);
        }
    }];
}

+ (void) xmppUpData:(NSData *)imageData xmppMessageType:(XMPPMessageType)messageType
                 progress:(void (^)(NSProgress *))uploadProgrees
          completed:(void(^)(NSError *,NSString *))completedBlock {
    
    AFHTTPSessionManager *manager = [XSHTTPManager sharedClient];
//    if (isEmptyString(XMUP_PATH)) {
//        [XSTool saveStr:@"http://47.75.81.58:8089//assist/up/fileUpload" forKey:XMUP_PATH_KEY];
//    }
      [manager POST:XMUP_PATH parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSString *mimeType,*fileName;
        if (messageType == XMPPMessageTypePicture) {
            mimeType = @"image/png";
            fileName = @"123.jpg";
        }else if (messageType == XMPPMessageTypeVoice){
            mimeType = @"application/octet-stream";
            fileName = kRecordAudioFile;
        }else if (messageType == XMPPMessageTypeFile){
            mimeType = @"application/octet-stream";
            fileName = [XMPPManager sharedManager].fileName;
        }else if (messageType == XMPPMessageTypeFile){
            mimeType = @"application/octet-stream";
            fileName = XMPP_VIDEO_FILE;
        }
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:mimeType];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:(NSJSONReadingMutableContainers) error:&error];
        NSLog(@"dic  =%@",dic);
        if (error) {
            completedBlock(error,@"发送失败");
        }else{
            completedBlock(nil,dic[@"data"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (error) {
            completedBlock(error,nil);
        }
    }];
    
}




@end

