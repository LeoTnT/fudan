//
//  XMGroupChatController.h
//  App3.0
//
//  Created by apple on 2018/2/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "BaseChatController.h"

@interface XMGroupChatController : BaseChatController



- (instancetype)initWithRoomJID:(ConversationModel *)xmppRoomManager ;

@end
