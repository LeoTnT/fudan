
//
//  XMChatController.m
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMChatController.h"
#import "WalletAccountViewController.h"
#import "OrderDetailViewController.h"
#import "ADDetailWebViewController.h"
#import "GoodsDetailViewController.h"
#import "UUAVAudioPlayer.h"
#import "CLPlayerView.h"
@interface XMChatController ()<NSFetchedResultsControllerDelegate,UUMessageCellDelegate>

@property (nonatomic ,strong)NSFetchedResultsController *resultController;
@property (nonatomic ,strong)NSMutableArray *messageDataList;
@property (nonatomic ,strong)NSMutableArray *updataCount;
@property (nonatomic ,strong)NSMutableArray *insetCount;
@property (nonatomic,assign) BOOL isBurnChat;
 
@property(nonatomic,strong)CLPlayerView *playerView;
@end

@implementation XMChatController
{
    BOOL canScrollBottom;
    UserInfoDataParser *_userInfo;
    
}

- (NSMutableArray *)insetCount {
    if (!_insetCount) {
        _insetCount = [NSMutableArray array];
    }
    return _insetCount;
}
- (NSMutableArray *)updataCount {
    if (!_updataCount) {
        _updataCount = [NSMutableArray array];
    }
    return _updataCount;
}

- (NSString *)toString {
    
    if (isEmptyString(self.conversationModel.bareJID.user)) {
        
        if ([self.conversationModel.conversation containsString:@"@"]) {
            return [self.conversationModel.conversation componentsSeparatedByString:@"@"].firstObject;
        }
        return self.conversationModel.conversation;
    }
    return self.conversationModel.bareJID.user;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.playerView) {
        [self.playerView pausePlay];
        [self.playerView endPlay:^{
            [self.playerView destroyPlayer];
        }];
        self.playerView=nil;
    }
 
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [NSMutableArray array];
    canScrollBottom = YES;

    @weakify(self);
    self.messageDataList = [NSMutableArray array];
    self.isBurnChat = NO;
    [self setHeaderRefresh];
    self.page = 1;
    [[XMPPManager sharedManager].stream addDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
    [self.resultController performFetch:nil];
 
    [self willReloadTableViewWitPage:self.page resultController:self.resultController completed:^(NSInteger index) {
        @strongify(self);
        [self getMessageModel:index];
    }];

    
    self.getVoiceBlock = ^(NSString *serviceURl, NSString *voiceTime) {
        @strongify(self);
         [XMPPSignal sendVoice:serviceURl time:voiceTime to:self.conversationModel.conversation complete:nil];
    };
    
    NSString *_uid = [self toString];
 
    if (isEmptyString(_uid)) return;
    
    if (![_uid isEqualToString:@"robot"]) {
        [self actionCustomRightBtnWithNrlImage:@"chat_single" htlImage:nil title:nil action:^{
            @strongify(self);
             [self pushPersonaAction:_uid];
        }];
    }
    

    [HTTPManager getUserInfoWithUid:_uid success:^(NSDictionary * dic, resultObject *state) {
        
        if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            _userInfo = parser.data;
            
            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
            cdParser.uid = _userInfo.uid;
            cdParser.username = _userInfo.username;
            cdParser.nickname = _userInfo.nickname;
            cdParser.avatar = _userInfo.logo;
            cdParser.relation = _userInfo.relation;
            cdParser.mobile = _userInfo.mobile;
            cdParser.remark = _userInfo.remark;
            self.conversationModel.relation = _userInfo.relation;
            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            [[ChatHelper shareHelper].friendVC reloadContacts];
            
            self.title = [self getNameWithParser:cdParser];
         }
    } fail:^(NSError * _Nonnull error) {
        
    }];
 
    [XMPPSignal sendReceiveMessageID:_uid];
    
}
- (void) pushPersonaAction:(NSString *)uid {
    PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:uid];
    @weakify(self);
    [perVC setGetRemar:^(NSString *remark) {
        @strongify(self);
        self.title = remark;
    }];
    [self.navigationController pushViewController:perVC animated:YES];
}
- (NSString *)getNameWithParser:(ContactDataParser *)parser {
    if (parser.remark.length > 0) {
        return parser.remark;
    }else if (parser.nickname.length > 0) {
        return parser.nickname;
    }else if (parser.username.length > 0) {
        return parser.username;
    }else if (parser.uid.length > 0) {
        return parser.uid;
    }
    return parser.nickname;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    
    if ([message isChatMessage] ) {
        
        
        if ([message.messageExtension isEqualToString:VOICE_CHAT_REQUEST]) {// 请求
            
            [self.backView removeFromSuperview];
            self.backView = nil;
        }
        
         if (![message.from.user isEqualToString:self.conversationModel.bareJID.user]) return ;
         [self requestMessage:message];
        if ([message isMessageWithBody]) {
            [self showAlertWithRequestNotice:message];
        }
    }
}



/**
 已读回执
 */
- (void) requestMessage:(XMPPMessage *)message {
    
    if ([message hasReceiptResponse]) {
        NSString *messageID = message.receiptResponseID;
        [XMPPSignal updateXMPPMessageArchiving:self.conversationModel.bareJID.user messageID:nil];
    }
    
    if ([message hasReceiptRequest]) {
        XMPPMessage *msg = [message generateReceiptResponse];
        [[XMPPManager sharedManager].stream sendElement:msg];
    }
    
}

/*
 <message xmlns="jabber:client" from="7@localhost.localdomain/TigaseMessenger" to="8@localhost.localdomain/TigaseMessenger" id="047D4BD5-B3E7-4BAD-8D33-F6454BC9AE05" type="chat">
 <body>(null)请求焚毁聊天</body>
 <x xmlns="jabber:x:oob">
 <ext>{"MSG_TYPE":"CHAT_DESTROY"}</ext>
 </x>
 </message>
 
  请求焚毁聊天，  已同意焚毁，  已拒绝焚毁
 */
- (void) showAlertWithRequestNotice:(XMPPMessage *)message {
    
    NSXMLElement *request = [message elementForName:@"x"];
    if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ){
        NSString *jsonStr = [request elementForName:@"ext"].stringValue;
        NSDictionary *dic = jsonStr.mj_JSONObject;
        NSString *type = dic[MSG_TYPE];
        if (isEmptyString(type))return;
        NSInteger state = 0;
        if ([type isEqualToString:CHAT_DESTROY]) {
            state = 0;
             [self showAlertBody:message.body];
          }else if([type isEqualToString:CHAT_DESTROY_AGREE]){
            state = 1;
              [self burnChatList];
        }else if([type isEqualToString:CHAT_DESTROY_DISAGREE]){
            state = 2;
        }else{
            
        }
 
    }
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.isBurn) {
         NSString *body = [NSString stringWithFormat:@"%@请求焚烧聊天",self.conversationModel.title];
         [self showAlertBody:body];
    }
}

- (void) showAlertBody:(NSString *)body {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];
        [self.chatKeyBoard keyboardDown];
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:body message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self makeSureBurn];
        }];
        [alert addAction:action1];
        UIAlertAction *action2=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self makeCancelBurn];
        }];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
    });
    

}


- (void) burnChatList {

    
     [XMPPSignal deletedChatHistoryWithUserID:self.conversationModel.conversation];
    [XMPPSignal deletedContactListWith:self.conversationModel.conversation];
    [XMPPSignal MR_updateXMPPMessageArchiving_ContactIsBurn:self.conversationModel.conversation];
 
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.messageDataList removeAllObjects];
        [self showEncryptMessage];
        [self.tableView reloadData];
    });
    
    
}

- (void) sendBurnAfterReading {
 
    NSString *name = [UserInstance ShardInstnce].nickName;
    NSString *text = [NSString stringWithFormat:@"%@请求焚毁聊天",name];
    [self burnMessageWithBody:text type:CHAT_DESTROY];
}


- (void) makeSureBurn {
 
    [XMPPSignal deletedChatHistoryWithUserID:self.conversationModel.conversation];
    NSString *name = [UserInstance ShardInstnce].nickName;
    NSString *text = [NSString stringWithFormat:@"%@已同意焚毁聊天",name];
    [self burnMessageWithBody:text type:CHAT_DESTROY_AGREE];
    [XMPPSignal MR_updateXMPPMessageArchiving_ContactIsBurn:self.conversationModel.conversation];
    [self.messageDataList removeAllObjects];
    [self.resultController performFetch:nil];
    [self.tableView reloadData];
}

- (void) makeCancelBurn {
 
    NSString *name = [UserInstance ShardInstnce].nickName;
    NSString *text = [NSString stringWithFormat:@"%@已拒绝焚毁聊天",name];
    [self burnMessageWithBody:text type:CHAT_DESTROY_DISAGREE];
     [XMPPSignal MR_updateXMPPMessageArchiving_ContactIsBurn:self.conversationModel.conversation];
}


- (void) burnMessageWithBody:(NSString *)body type:(NSString *)type{
    NSDictionary *exDic =@{MSG_TYPE:type};
    NSString *name = [UserInstance ShardInstnce].userInfo.nickname;
    [XMPPSignal sendMessage:body to:self.conversationModel.conversation ext:exDic complete:^(XMPPMessageSendState state) {
        NSLog(@"state = %ld",(long)state);
        
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   [XMPPSignal MR_updateXMPPMessageArchiving_Contact:self.conversationModel.conversation];
    [[XMPPManager sharedManager].stream removeDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
    if (self.isBurnChat) {
        [XMPPSignal deletedChatHistoryWithUserID:self.conversationModel.conversation];
        [XMPPSignal deletedContactListWith:self.conversationModel.conversation];
        [XMPPSignal MR_updateXMPPMessageArchiving_ContactIsBurn:self.conversationModel.conversation];
    }
    
}





- (void)firstChatTips {
//    NSString *from = [[EMClient sharedClient] currentUsername];
//    NSLog(@"from：%@ to %@",from,_chatterName);
//    EMTextMessageBody *body = [[EMTextMessageBod/y alloc] initWithText:String_SafeNotice];
    //生成Message
//    EMMessage *message = [[EMMessage alloc] initWithConversationID:_chatterName from:from to:_chatterName body:body ext:nil];
//    message.chatType = (EMChatType)_conversation.type;// 设置消息类型
//    message.ext = @{MSG_TYPE:MESSAGE_SAFE_NOTCIE};
//    // 更新消息列表
//    NSDictionary *dic = @{@"emMessage":message,
//                          @"messageId":message.messageId,
//                          @"type": @(UUMessageTypeText),
//                          @"from": @(UUMessageFromMe),
//                          @"strName":from,
//                          @"strNotice":[self noticeStringConvertByMessage:message],
//                          @"strIcon":_mineAvatar,
//                          @"chatType":@(_typeChat)};
    
    
    
//    [self dealTheFunctionData:dic];
    //    [[EMClient sharedClient].chatManager importMessages:@[message] completion:^(EMError *aError) {
    //        if (!aError) {
    //            NSLog(@"导入消息成功");
    //            _conversation.ext = @{@"firstChat":@(NO)};
    //        }
    //    }];
//    [self.conversation insertMessage:message error:nil];
}

- (void) getMessageModel:(NSInteger)index {
    @weakify(self);
    
    NSMutableArray *arr = [NSMutableArray array];
       [self.coreDataDataSource.rac_sequence.signal subscribeNext:^(XMPPMessageArchiving_Message_CoreDataObject *object) {
           
           @strongify(self);
           
           UUMessageFrame *messageModel =object.uuFrame;
           NSString *avatar = object.isOutgoing?[UserInstance ShardInstnce].avatarImgUrl:self.conversationModel.avatarURLPath;
           messageModel.message.strIcon = avatar;
           [arr addObject:messageModel];
           
           if (messageModel.message.type == UUMessageTypePicture) {
               
               NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:messageModel.message.pictureURL]];
               
               if (!imageData) return ;
               PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
               NSDate *date = [[XSTool shareYMDHMS] dateFromString:messageModel.message.strTime];
               pbImage.imageId = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
               pbImage.imageTime = [date timeIntervalSince1970];
               [self.photoArray addObject:pbImage];
           }
           
    } completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.page == 1) {
                [self.messageDataList addObjectsFromArray:arr];
                 // 进入会话获取历史消息完成
                if (!isEmptyString(self.productJsonString)) {
 
                    
                    UUMessage *message = [UUMessage new];
                    message.type = UUMessageTypeProduct;
                    message.shareJosnString = self.productJsonString;
                    UUMessageFrame *frame = [UUMessageFrame new];
                    [frame setMessage:message];
                    [self.messageDataList addObject:frame];
                    [self.tableView reloadData];
                    [self tableViewScrollToBottom];
                }
                [self showEncryptMessage];

 
                
            }else{
                
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, index)];
                [self.messageDataList insertObjects:arr atIndexes:set];

            }
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
            if (self.page == 1) {
                [self tableViewScrollToBottom];
            }
            
        });
    }];
    
}



/**
 显示加密标题
 */
- (void) showEncryptMessage {
    
    if (self.messageDataList.count ==0 &&isEmptyString(self.productJsonString)) {
        
        UUMessageFrame *frame = [UUMessageFrame new];
        UUMessage *message = [UUMessage new];
        frame.showNotice = YES;
        message.extType = MESSAGE_SAFE_NOTCIE;
        message.strNotice = @"此对话中的信息和通话已进行端到端的加密。";
        frame.message = message;
        [self.messageDataList insertObject:frame atIndex:0];
        
    }
}


- (void)popAction {
    [BaseUITool navigationControllersIsContainViewController:@"PersonalViewController" viewController:^(BOOL isContainVC, UIViewController *getViewController, NSString *containName) {
        if (isContainVC) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
 

- (void)loadRefreshData {
    self.page ++;

    [self willReloadTableViewWitPage:self.page resultController:self.resultController completed:^(NSInteger index){
        [self getMessageModel:index];
    }];
    
 
}

#pragma mark - MapLocationViewController Delegate

- (void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address {
    
    NSString *strLat = [NSString stringWithFormat:@"%f",coordinate.latitude];
    NSString *strLon = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
    [XMPPSignal sendLocation:address latitude:strLat longitude:strLon to:self.conversationModel.conversation complete:^(XMPPMessageSendState state) {
        
    }];
    
}


#pragma mark tableview delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    if (point.y+scrollView.bounds.size.height < scrollView.contentSize.height-200) {
        // 如果在浏览之前的聊天内容，收到新消息不让跳到最底部
        canScrollBottom = NO;
    } else {
        canScrollBottom = YES;
    }
    if (self.playerView) {
        [self.playerView destroyPlayer];
    }
}

- (NSFetchedResultsController *)resultController {
    
    if (!_resultController) {
        XMPPMessageArchivingCoreDataStorage *messageCDS = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        
        NSManagedObjectContext *context = messageCDS.mainThreadManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:messageCDS.messageEntityName];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr=%@ AND bareJidStr=%@",getJID([XMPPManager sharedManager].stream.myJID.user),getJID(self.conversationModel.conversation)];
        request.predicate = predicate;
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
        request.sortDescriptors = @[sortDescriptor];
        _resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        _resultController.delegate = self;
 
    }
    return _resultController;
}



- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    XMPPMessageArchiving_Message_CoreDataObject *object = anObject;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
        {
 
            UUMessageFrame *messageModel = object.uuFrame;
            NSString *avatar = object.isOutgoing?[UserInstance ShardInstnce].avatarImgUrl:self.conversationModel.avatarURLPath;
            messageModel.message.strIcon = avatar;
            
            
            [self.messageDataList addObject:messageModel];
            if (object.isOutgoing) [self.updataCount addObject:@(self.messageDataList.count - 1)];
            
            
            [tableView reloadData];
            [self tableViewScrollToBottom];
            NSLog(@" NSFetchedResultsChangeInsert- -  - - - - - - - - - - - - -- - -%@ ",object.messageStatus);
            if (messageModel.message.type == UUMessageTypePicture) {
                NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:messageModel.message.pictureURL]];
                if (!imageData) return;
                PhotoBrowserImage *pbImage = [[PhotoBrowserImage alloc] initWithData:imageData];
                NSDate *date = [[XSTool shareYMDHMS] dateFromString:messageModel.message.strTime];
                pbImage.imageId = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
                pbImage.imageTime = [date timeIntervalSince1970];
                [self.photoArray addObject:pbImage];
            }
        }
            break;
            
        case NSFetchedResultsChangeDelete:
            //            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            
            break;
            
        case NSFetchedResultsChangeUpdate:{
            
            XMPP_LOG(@"  - -  %ld",indexPath.row);
            //           UUMessageFrame *oldMessageModel = self.messageDataList[indexPath.row];
//            oldMessageModel.message.status = [object.messageStatus integerValue];
            [self.updataCount enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSInteger count = [obj integerValue];
                UUMessageFrame *oldMessageModel = self.messageDataList[count];
                oldMessageModel.message.status = [object.messageStatus integerValue];
                NSIndexPath *x_indexPath = [NSIndexPath indexPathForRow:count inSection:0];
                UUMessageCell *cell = [tableView cellForRowAtIndexPath:x_indexPath];
                [cell changeMessageState:4];
            }];
            [self.updataCount removeAllObjects];

 
        }
            break;
            
        case NSFetchedResultsChangeMove:
            //            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            //            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)tableViewScrollToBottom
{
    if (self.messageDataList.count==0)
        return;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageDataList.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messageDataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UUMessageFrame *messageModel = self.messageDataList[indexPath.row];
    
    return messageModel.cellHeight;
 
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"chatCell";
    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
    }
    
    UUMessageFrame *messageModel = self.messageDataList[indexPath.row];
    messageModel.message.relation = self.conversationModel.relation;
    [cell setMessageFrame:messageModel];
    
    return cell;
}

#pragma mark -- ChatKeyBoardDataSource
- (NSArray<MoreItem *> *)chatKeyBoardMorePanelItems
{
    MoreItem *item1 = [MoreItem moreItemWithPicName:@"sharemore_photo" highLightPicName:nil itemName:Localized(@"拍照")];
    MoreItem *item2 = [MoreItem moreItemWithPicName:@"sharemore_pic" highLightPicName:nil itemName:@"图片"];
    MoreItem *item3 = [MoreItem moreItemWithPicName:@"sharemore_fs" highLightPicName:nil itemName:@"焚烧聊天"];
    MoreItem *item4 = [MoreItem moreItemWithPicName:@"sharemore_location" highLightPicName:nil itemName:Localized(@"位置")];
//    MoreItem *item6 = [MoreItem moreItemWithPicName:@"sharemore_file" highLightPicName:nil itemName:@"文件"];

    MoreItem *item5 = [MoreItem moreItemWithPicName:@"sharemore_redpacket" highLightPicName:nil itemName:Localized(@"红包")];
    MoreItem *item6 = [MoreItem moreItemWithPicName:@"sharemore_transfer" highLightPicName:nil itemName:Localized(@"转账")];

    MoreItem *item7 = [MoreItem moreItemWithPicName:@"sharemore_voice" highLightPicName:nil itemName:@"语音"];
    MoreItem *item8 = [MoreItem moreItemWithPicName:@"sharemore_video" highLightPicName:nil itemName:@"视频"];
    self.moreItems = @[item1, item2];
    
    return self.moreItems;
}


- (void) tapTransform {
    ChatTransferViewController *transVC = [[ChatTransferViewController alloc] init];
    NickNameDataParser *parser = [NickNameDataParser new];
    parser.logo = self.avatarUrl;
    parser.nickname = self.title;
    parser.uid = self.conversationModel.conversation;
    
    transVC.userInfo = parser;
    transVC.conversation = self.conversationModel;
    [self.navigationController pushViewController:transVC animated:YES];
    
}

- (void)transferClick:(UUMessageCell *)cell {
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getTransferInfoWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            TransferDetailModel *model = [TransferDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转转账详情
            ChatTransferDetailViewController *trans = [[ChatTransferDetailViewController alloc] init];
            trans.model = model;
            trans.conversation = self.conversationModel.conversation;
            trans.transferId = rpId;
            trans.resendMessage = cell.messageFrame.message;
            [self.navigationController pushViewController:trans animated:YES];
        } else {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)cellContentDelete:(UUMessageCell *)cell
{
    NSLog(@"删除");
    
    UUMessage *message = cell.messageFrame.message;
    NSString *messageID = message.messageId;
    [XMPPSignal xmpp_deletedMessageWith:messageID contactID:self.conversationModel.conversation];
    if ([self.messageDataList containsObject:cell.messageFrame]) {
    [self.messageDataList removeObject:cell.messageFrame];
    }
    
    [self.tableView reloadData];
}

- (void)chatKeyBoardSendText:(NSString *)text {
    
    @weakify(self);
    [XMPPSignal sendMessage:text to:self.conversationModel.conversation complete:^(XMPPMessageSendState state) {
        @strongify(self);
//         NSMutableDictionary *dic = [@{
//                                      @"strContent": text,
//                                      @"type": @(UUMessageTypeText),
//                                      @"from":@(XMPPMessageDirectionSend),
//                                      @"strName":[UserInstance ShardInstnce].uid,
//                                      @"status":@"",
//                                      @"strIcon":[UserInstance ShardInstnce].avatarImgUrl,
//                                      @"chatType":@(0)} mutableCopy];
//        
//        // 增加最新消息
//        [self.chatModel addMessageItems:dic];
//        [self.tableView reloadData];
//        [self tableViewScrollToBottom];
    }];
 
}

- (void)chatWithVoice:(BOOL)isVoice {
    [self.chatKeyBoard keyboardDown];
}

/*
 NSString *displayName = [[self.message from] user];
 uuMessage.strName = displayName;
 NSDateFormatter* formatter = [XSTool shareYMDHMS];
 uuMessage.strTime = [formatter stringFromDate:self.timestamp];
 uuMessage.from = !self.isOutgoing;
 uuMessage.chatType = UUChatTypeChat;
 uuMessage.relation = [ChatHelper isfriendWithUid:self.message.xm_messageUser];
 
 uuMessage.status = [self.messageStatus integerValue];
 uuMessage.type = XMPPMessageTypePicture;
 uuMessage.pictureURL =  self.message.body;
 
 */
- (void)sendImageWithData:(UIImage *)selectedImage imageData:(NSData *)imageData{

    NSData *selectedImageData = imageData ? imageData :UIImageJPEGRepresentation(selectedImage, 0);
    if (!selectedImageData) {
        return;
    }
    

    
    __block NSProgress *_progress;
    [XMPPBaseMessage xmppUpimage:selectedImageData progress:^(NSProgress * progress) {
         _progress = progress;
        NSLog(@"  localizedDescription --- %@, localizedAdditionalDescription --- %@", progress.localizedDescription, progress.localizedAdditionalDescription);
 
    }  completed:^(NSError *error,NSString *url) {
            if (error) {
                NSLog(@"error  =%@",error.localizedDescription);
                [MBProgressHUD showMessage:!isEmptyString(url) ?url:@"发送失败" view:self.view hideTime:1.5 doSomeThing:nil];
  
            }else{
                if (!isEmptyString(url)) {
                    [XMPPSignal sendImage:url to:self.conversationModel.conversation complete:^(XMPPMessageSendState state) {
                        
                        if (state == XMPPMessageStatusSuccessed) {
                            
                            
                        }else{
                            
                            UUMessageFrame *uumessageFrame = [UUMessageFrame new];
                            UUMessage *uuMessage = [UUMessage new];
                            uuMessage.strName= [UserInstance ShardInstnce].uid;
                            NSDateFormatter* formatter = [XSTool shareYMDHMS];
                            uuMessage.strTime = [formatter stringFromDate:[NSDate date]];
                            uuMessage.from = 0;
                            uuMessage.chatType = UUChatTypeChat;
                            uuMessage.status = UUMessageStatusFailed;
                            uuMessage.picture = selectedImage;
                            uumessageFrame.message = uuMessage;
                            uuMessage.showDateLabel = YES;
                            uuMessage.type =  UUMessageTypePicture;
                            ChatModel *model = [ChatModel new];
                            uumessageFrame = [model messageWithUUmessage:uuMessage refreshTime:YES];
                            
                            
                            [self.messageDataList addObject:uumessageFrame];
                            [self.insetCount addObject:self.messageDataList.lastObject];
                            [self.tableView reloadData];

                        }
//                        _progress
                        
                    }];
                }else{
                   
                    [MBProgressHUD showMessage:@"发送失败" view:self.view];
                }
                
            }
            
        }];
    
    
}



/**
 
 @param dataTuple image,data,time,size
 */
- (void)sendVideoWitdURL:(RACTuple *)dataTuple {
    
    NSString *time = dataTuple.third;
    NSString *size = dataTuple.fourth;
    [XMPPBaseMessage xmppUpVideo:dataTuple completed:^(NSError *error, NSDictionary *dic) {
        
        if (error) {
            NSLog(@"error  =%@",error.localizedDescription);
 
            [MBProgressHUD showMessage:@"发送失败" view:self.view hideTime:1.5 doSomeThing:nil];
 
        }else{
            if (dic) {
                NSArray *arr = dic[@"data"];
               __block NSString *movString;
                __block NSString *imageString;
                __block NSString *nameString;
                [arr.rac_sequence.signal subscribeNext:^(NSString *x) {
                    if ([x containsString:@"."]) {
                        NSString *ss = [x componentsSeparatedByString:@"."].lastObject;
                        if ([ss isEqualToString:@"jpg"]) {
                            imageString = x;
                        }else if ([ss isEqualToString:@"mov"]){
                            movString = x;
        
                           nameString = [x componentsSeparatedByString:@"/"].lastObject;
                        }
                    }
                }completed:^{
                    [XMPPSignal sendWithVideo:movString time:time to:self.conversationModel.conversation image:imageString size:size name:nameString
                                     complete:^(XMPPMessageSendState state) {
                        
                        NSLog(@" - - - - - - - - - %ld",(long)state);
                        
                    }];
                }];
                

            }else{
                [MBProgressHUD showMessage:@"发送失败" view:self.view];
            }
            
        }
        
    }];
    
}


#pragma mark - UUMessage delegate

- (void)productShareClick:(NSString *)productId {
    GoodsDetailViewController *vc = [GoodsDetailViewController new];
    vc.goodsID = productId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendProductLinkClick:(NSString *)jsonString {
    NSDictionary *extDic = @{MSG_TYPE:MESSAGE_PRODUCT_TYPE,MESSAGE_SHARE_DATA:jsonString};
    UUMessageProductShareModel *model = [UUMessageProductShareModel mj_objectWithKeyValues:jsonString];
    [XMPPSignal sendMessage:model.goodTitle to:self.conversationModel.conversation ext:extDic complete:^(XMPPMessageSendState state) {
        
    }];
    
}

//- (void)cellContentUrlDidClick:(NSString *)url;
- (void)headImageDidClick:(UUMessageCell *)cell {
    
    if (cell.messageFrame.message.from == UUMessageFromOther) {
        PersonalViewController *personalVC = [[PersonalViewController alloc] initWithUid:self.conversationModel.bareJID.user];
        [self.navigationController pushViewController:personalVC animated:YES];
    }


}

- (void)cellContentDidClick:(UUMessageCell *)cell imageView:(UIImageView *)contentImageView
{
    NSArray* sorted = [self.photoArray sortedArrayUsingComparator:
                       ^(PhotoBrowserImage *obj1, PhotoBrowserImage* obj2){
                           if(obj1.imageTime < obj2.imageTime) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    [self.photoArray removeAllObjects];
    [self.photoArray addObjectsFromArray:sorted];
    NSInteger index = 0;
    NSDate *date = [[XSTool shareYMDHMS] dateFromString:cell.messageFrame.message.strTime];
    NSString *imageId = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    for (PhotoBrowserImage *image1 in self.photoArray) {
        
        if ([image1.imageId isEqualToString:imageId]) {
            index = [self.photoArray indexOfObject:image1];
            break;
        }
    }
    self.photoBrowser = [XLPhotoBrowser showPhotoBrowserWithImages:self.photoArray currentImageIndex:index pageControlHidden:YES];
    if (!self.photoBrowser) {
        [XSTool showToastWithView:self.view Text:@"图片获取失败"];
        return;
    }
    self.photoBrowser.delegate = self;
    [self.photoBrowser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    self.photoBrowser.isCusctomAdd = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.tabBarController.view addSubview:self.photoBrowser];
    
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}

-(void)photoBrowserDismiss{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)cellContentFileDidClick:(UUMessageCell *)cell {
    UUMessage *message = cell.messageFrame.message;
    if (![message.fileUrl containsString:@"/"]) {
        return;
    }
    
   NSString *fiel = [message.fileUrl componentsSeparatedByString:@"/"].lastObject;
    BOOL isExist = [XMPPSignal isFileExist:fiel];
    NSLog(@"isExist  =%@",isExist?@"YES":@"NO");
    
    if (isExist) {
        NSURL *fileUrl = [NSURL fileURLWithPath:message.fileUrl];
        if (fileUrl != nil) {
            if (self.fileInteractionController == nil) {
                //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                self.fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
                self.fileInteractionController.delegate = self;
            } else {
                self.fileInteractionController.URL = fileUrl;
            }
            [self.fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"文件打开失败"];
        }
        
        return;
    }
    NSURL *url = [NSURL URLWithString:message.fileUrl];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSString *cachePath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
         NSString *fileName=[cachePath stringByAppendingPathComponent:response.suggestedFilename];
         return [NSURL fileURLWithPath:fileName];

    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        XMPP_LOG(@" _ _ _ _ _File downloaded to: %@", filePath);
//        [XMPPSignal deletedFileWith:response.suggestedFilename];

        if (filePath != nil) {
            if (self.fileInteractionController == nil) {
                //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
                self.fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:filePath];
                self.fileInteractionController.delegate = self;
            } else {
                self.fileInteractionController.URL = filePath;
            }
            [self.fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:@"文件打开失败"];
        }
     }];
    [downloadTask resume];
    
    
}



//- (void)locationDidClickLatitude:(double)latitude longitude:(double)longitue adddress:(NSString *)adddress;
- (void)voiceDidClick:(UUMessageCell *)cell {
//    [cell.btnContent setGCDTime:cell.messageFrame.message.strVoiceTime.integerValue];
    
    [self setGCDTime:cell.messageFrame.message.strVoiceTime.integerValue tap:cell];
    cell.btnContent.backImageView.backgroundColor =[UIColor redColor];

}

- (void)setGCDTime:(NSInteger)time tap:(UUMessageCell *)cell{
    
//    cell.userInteractionEnabled = NO;
//    cell.btnContent.mScrubber.maximumValue = time;
// 
//   __block int _time = (int)time;
//      dispatch_source_t _timer;
//    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
//    [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:_time+1 stop:^{
//        //设置按钮的样式
//        dispatch_source_cancel(_timer);
//         cell.userInteractionEnabled = YES;
//        cell.btnContent.mScrubber.maximumValue = 0;
// 
//    } otherAction:^(int time) {
//         cell.btnContent.mScrubber.value = [UUAVAudioPlayer sharedInstance].player.currentTime;
//        if (time == 0) {
//            cell.userInteractionEnabled = YES;
//        }
//    }];
    
}


#pragma mark - - - - - - videoDidClick
- (void)videoDidClick:(UUMessageCell *)cell {
 
    //之前的停止播放
    if (self.playerView) {
        [self.playerView destroyPlayer];
        [self.playerView removeFromSuperview];
    }
 
    
    NSURL *videoUrl = [NSURL URLWithString:cell.messageFrame.message.strContent];
    self.playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    [[UIApplication sharedApplication].keyWindow addSubview:self.playerView];
    //视频地址
    self.playerView.url =videoUrl;
    self.playerView.autoFullScreen=YES;
    //播放
    [self.playerView playVideo];
    //播放完成回调
    [self.playerView endPlay:^{
        //销毁播放器
        [self.playerView destroyPlayer];
    }];
    
    
    
}
//- (void)cellContentLongPress:(UUMessageCell *)cell;r
//- (void)cellContentDelete:(UUMessageCell *)cell;
//- (void)cellContentTranspond:(UUMessageCell *)cell;
//- (void)cellContentCopy:(UUMessageCell *)cell;
//- (void)cellContentRecall:(UUMessageCell *)cell;
//- (void)cellContentDetector:(UUMessageCell *)cell;
//- (void)sendFailTap:(UUMessageCell *)cell;
//- (void)redPacketClick:(UUMessageCell *)cell;
//- (void)transferClick:(UUMessageCell *)cell;
//- (void)shareContentClick:(NSString *)link;
//- (void)callContentDidClick:(UUMessageCell *)cell;
- (void)systemNoticeContentDidClick:(UUMessageSystemNoticeModel *)sysNoticeModel {
//    NSString *link_url = sysNoticeModel.link_url;
//    if (![link_url containsString:ImageBaseUrl]) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link_url]];
//        return;
//    }
    NSString *paramStr = sysNoticeModel.link_app;
    NSArray *arr = [paramStr componentsSeparatedByString:@"/"];
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"wallet"]) {
        WalletAccountViewController *vc=[[WalletAccountViewController alloc] init];
        vc.walletType = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"order"]) {
        OrderDetailViewController *vc=[[OrderDetailViewController alloc] init];
        vc.orderId = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"chat"]) {
        // 客服
        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"CustomerViewController"] animated:YES];
    } else if ([qrtype isEqualToString:@"userapprove"]) {
        // 实名认证
        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
    } else if ([qrtype isEqualToString:@"supplyapprove"]) {
        // 委托商认证
        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"MerchantApplicationController"] animated:YES];
    } else {
        ADDetailWebViewController *adWeb=[[ADDetailWebViewController alloc] init];
        NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
        adWeb.urlStr = [NSString stringWithFormat:@"%@&device=%@",sysNoticeModel.link_url,gui];
        adWeb.title = sysNoticeModel.link_title;
        [self.navigationController pushViewController:adWeb animated:YES];
    }
    
    
}

- (void)noticeContentDidClick:(UUMessageCell *)cell {
    NSString *_uid = self.conversationModel.bareJID.user;
 

    NSNumber *relation = [ChatHelper isfriendWithUid:_uid];

    if ([relation isEqualToNumber:@1]) return;

    
      [HTTPManager attentionFansWithUid:_uid success:^(NSDictionary * dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
             [XMPPAddFriendsManager addBuddyWithJid:_uid];
            [self getUserInfroWithID:_uid withCell:cell];
            NSString *message = [NSString stringWithFormat:@"%@%@",[UserInstance ShardInstnce].nickName,Localized(@"加你为好友,开始聊天吧")];
            NSDictionary *extDic = @{MSG_TYPE:MESSAGE_FOCUS_TYPE};
            self.conversationModel.relation = @1;
//            UUMessageFrame *frame = cell.messageFrame;
//            NSString *sss = frame.message.strNotice;
//            self.mes
//            cell.labelNotice.text = sss;
            [XMPPSignal sendMessage:message to:_uid ext:extDic complete:nil];
             // 刷新联系人列表
            [[ChatHelper shareHelper].friendVC getRelationsData];
            
          } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:TOAST_DO_FAIL];
        [XSTool hideProgressHUDWithView:self.view];
    }];
    
    


    
}


- (void) getUserInfroWithID:(NSString *)uid withCell:(UUMessageCell *)cell{
 
    
    [HTTPManager getUserInfoWithUid:uid success:^(NSDictionary * dic, resultObject *state) {
         if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            _userInfo = parser.data;
             ContactDataParser *cdParser = [[ContactDataParser alloc] init];
            cdParser.uid = _userInfo.uid;
            cdParser.username = _userInfo.username;
            cdParser.nickname = _userInfo.nickname;
            cdParser.avatar = _userInfo.logo;
            cdParser.relation = _userInfo.relation;
             cdParser.remark = _userInfo.remark;
             cdParser.mobile = _userInfo.mobile;
            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            [[ChatHelper shareHelper].friendVC reloadContacts];

//            NSString *ccc = @"点击请确认。";
//            if ([sss containsString:ccc]) {
//                NSString *sssss = [sss substringWithRange:NSMakeRange(0, sss.length - ccc.length)];
//                cell.labelNotice.text = @"";
//                cell.labelNotice.text = [NSString stringWithFormat:@"%@快来聊天吧。",sssss];
//            }
 
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}

 

//- (void)systemNoticeContentDidClick:(UUMessageSystemNoticeModel *)sysNoticeModel;

@end
