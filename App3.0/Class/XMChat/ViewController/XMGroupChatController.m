//
//  XMGroupChatController.m
//  App3.0
//
//  Created by apple on 2018/2/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMGroupChatController.h"
#import "GroupInfoViewController.h"
#import "InviteDetailViewController.h"
@interface XMGroupChatController ()

@property (nonatomic ,strong)NSFetchedResultsController *resultController;


@property (nonatomic ,strong)NSMutableArray *messageDataList;

@end

@implementation XMGroupChatController
{
    
    BOOL isDataFromCD;
    BOOL canScrollBottom;
    void (^getUserImage)(NSString *logo);
}


- (NSString *)roomID {
    
    if (isEmptyString(self.conversationModel.bareJID.user)) {
        if ([self.conversationModel.conversation containsString:@"@"]) {
            return [self.conversationModel.conversation componentsSeparatedByString:@"@"].firstObject;
        }
        return self.conversationModel.conversation;
    }
    return self.conversationModel.bareJID.user;
}

- (instancetype)initWithRoomJID:(ConversationModel *)conversion {
    if (self = [super init]) {
        
        self.xmppRoomManager = conversion.xmppRoomManager;
        self.xmppRoom =conversion.xmppRoomManager.xmppRoom;
        self.title =conversion.title;
        self.dataSource = [NSMutableArray array];
        self.conversationModel = conversion;
        self.page = 1;
    }
    return self;
}

-(NSMutableArray *)messageDataList {
    if (!_messageDataList) {
        _messageDataList = [NSMutableArray array];
    }
    return _messageDataList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //    [[DBHandler sharedInstance] getMemberByMemberId:@""];
    canScrollBottom = YES;
    [self setHeaderRefresh];
    
    [self fetchObjectWithPage];
    
    @weakify(self);
    [self actionCustomRightBtnWithNrlImage:@"chat_group" htlImage:nil title:nil action:^{
        @strongify(self);
        [self showRoomInfor];
    }];
    self.navigationItem.title = self.conversationModel.title;
    
    self.getVoiceBlock = ^(NSString *serviceURl, NSString *voiceTime) {
        @strongify(self);
        XMPP_RoomVoiceMessage *message = [XMPP_RoomVoiceMessage new];
        message.type = @"voice";
        message.body = serviceURl;
        message.strVoiceTime = voiceTime;
        NSString *messageString = message.mj_JSONString;
        [self.xmppRoom sendMessageWithBody:messageString];
    };
    
    
    [self checkIsJoinGroup];
    [[XMPPManager sharedManager].deletedMessageSubject subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        RACTupleUnpack(NSString *type,XMPPRoom *xmppRoom) = x;
        if ([type isEqualToString:@"deleteRoom"]) {
            [self checkIsJoinGroup];
        }
    }];
    
    
}

- (void) checkIsJoinGroup {
    @weakify(self);
    
    
    [XMPPRoomModel checkIsJoinGroup:[self roomID] finash:^(BOOL isJoin ,GroupDataModel *groupModel) {
        @strongify(self);
        if (!isEmptyString(groupModel.name)) {
            self.title = groupModel.name;
        }
        if (!isJoin) {
            [self leaveRoomAction];
        }
    }];
    
    
}

- (void) leaveRoomAction {
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"群信息错误或已被管理员解散，是否删除会话？", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [XMPPSignal MR_deletedContactID:self.conversationModel.conversation];
        [XMPPSignal deleteGroupHistoryWithRoomID:self.conversationModel.conversation];
        [XMPPSignal deletedContactListWith:self.conversationModel.conversation];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void) fetchObjectWithPage {
    if (self.page == 0) {
        self.resultController.fetchRequest.fetchLimit = 2;
        [self.resultController.fetchRequest setFetchOffset:0];
    }else{
        self.resultController.fetchRequest.fetchLimit = 20;
        [self.resultController.fetchRequest setFetchOffset:self.messageDataList.count];
    }
    [self.resultController performFetch:nil];
    NSMutableArray *temArr = [NSMutableArray array];
    NSMutableArray *newArr = [NSMutableArray arrayWithArray:self.resultController.fetchedObjects];
    NSArray *arr = [[newArr reverseObjectEnumerator]allObjects];
    
    [arr.rac_sequence.signal subscribeNext:^(XMPPRoomMessageCoreDataStorageObject *object) {
        UUMessageFrame *messageModel = [self.chatModel messageFrameWith:object refreshTime:YES];
        messageModel.message.remoteTimestamp = object.localTimestamp;
        if (object.isFromMe) {
            messageModel.message.strIcon = [UserInstance ShardInstnce].avatarImgUrl;
        }else{
            if (isEmptyString(messageModel.message.strIcon)) {
                [self cellImageWithUserID:object.message.from.xm_roomUserID messageModel:messageModel.message];
            }
        }
        [temArr addObject:messageModel];
    } completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self fetchCompleted:temArr];
        });
        
    }];
    
}


- (void) fetchCompleted:(NSMutableArray *)temArr {
    if (self.page == 0) {
        [self.messageDataList addObjectsFromArray:temArr];
        [self showEncryptMessage];
    }else{
        if (temArr.count ==0 && self.page >0)  self.page --;
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, temArr.count)];
        [self.messageDataList insertObjects:temArr atIndexes:set];
    }
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    [self tableViewScrollToBottom];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [XMPPSignal MR_updateXMPPMessageArchiving_Contact:self.conversationModel.conversation];
    
}

- (void) showRoomInfor{
    @weakify(self);
    GroupInfoViewController *groupVC = [[GroupInfoViewController alloc] initWithGroupId:self.xmppRoomManager.xmppRoom.roomJID.user];
    groupVC.roomManager = self.xmppRoomManager;
    [groupVC setRoomInfor:^(NSString *title) {
        @strongify(self);
        self.title = title;
    }];
    [self.navigationController pushViewController:groupVC animated:YES];
}


/**
 显示加密标题
 */
- (void) showEncryptMessage {
    
    if (self.messageDataList.count ==0) {
        
        UUMessageFrame *frame = [UUMessageFrame new];
        UUMessage *message = [UUMessage new];
        frame.showNotice = YES;
        message.extType = MESSAGE_SAFE_NOTCIE;
        message.ext = @{MSG_TYPE:MESSAGE_SAFE_NOTCIE};
        message.strNotice = NSLocalizedString(@"此对话中的信息和通话已进行端到端的加密。", nil);
        frame.message = message;
        [self.messageDataList insertObject:frame atIndex:0];
        
    }
}



- (void)chatKeyBoardSendText:(NSString *)text {
    XMPP_RoomMessage *message = [XMPP_RoomMessage new];
    message.type = @"txt";
    message.body = text;
    NSString *messageString = message.mj_JSONString;
    [self.xmppRoom sendMessageWithBody:messageString];
    
}


- (void)headImageDidClick:(UUMessageCell *)cell {
    
    NSString *ID = cell.messageFrame.message.strId;
    PersonalViewController *personalVC = [[PersonalViewController alloc] initWithUid:ID];
    [self.navigationController pushViewController:personalVC animated:YES];
    
}

- (void)sendImageWithData:(UIImage *)selectedImage imageData:(NSData *)imageData{
    
    NSData *selectedImageData = UIImageJPEGRepresentation(selectedImage, 0);
    [XMPPBaseMessage xmppUpimage:selectedImageData progress:^(NSProgress * progress) {
        
    } completed:^(NSError *error,NSString *url) {
        if (error) {
            NSLog(@"error  =%@",error.localizedDescription);
            [MBProgressHUD showMessage:!isEmptyString(url) ?url:NSLocalizedString(@"发送失败", nil) view:self.view hideTime:1.5 doSomeThing:nil];
            
        }else{
            
            XMPP_RoomMessage *message = [XMPP_RoomMessage new];
            message.type = @"image";
            message.body = url;
            NSString *messageString = message.mj_JSONString;
            [self.xmppRoom sendMessageWithBody:messageString];
            
        }
    }];
    
    
}


- (void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address {
    
    
    XMPP_RoomLocationMessage *message = [XMPP_RoomLocationMessage new];
    message.type = @"location";
    message.body = address;
    message.latitude = coordinate.latitude;
    message.longitude = coordinate.longitude;
    NSString *messageString = message.mj_JSONString;
    [self.xmppRoom sendMessageWithBody:messageString];
}




- (void)popAction {
    [BaseUITool navigationControllersIsContainViewController:@"GroupSetTitleViewController" viewController:^(BOOL isContainVC, UIViewController *getViewController, NSString *containName) {
        if (isContainVC) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}


- (void)loadRefreshData {
    self.page ++;
    [self fetchObjectWithPage];
    
}

- (NSFetchedResultsController *)resultController {
    if (!_resultController) {
        XMPPRoomCoreDataStorage *manager = [XMPPRoomCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = manager.mainThreadManagedObjectContext;
        NSFetchRequest *request = [self requestWithMessaegPage];
        _resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        _resultController.delegate = self;
        
    }
    return _resultController;
}


- (NSFetchRequest *) requestWithMessaegPage {
    XMPPRoomCoreDataStorage *manager = [XMPPRoomCoreDataStorage sharedInstance];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:manager.messageEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomJIDStr == %@",self.xmppRoom.roomJID.full];
    [request setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"localTimestamp" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    return request;
}



- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
        {
            XMPPRoomMessageCoreDataStorageObject *object = anObject;
            UUMessageFrame *messageModel = [self.chatModel messageFrameWith:object refreshTime:YES];
            if (object.isFromMe) {
                messageModel.message.strIcon = [UserInstance ShardInstnce].avatarImgUrl;
            }else{
                if (isEmptyString(messageModel.message.strIcon)) {
                    [self cellImageWithUserID:object.message.from.xm_roomUserID messageModel:messageModel.message];
                }
            }
            [self.messageDataList addObject:messageModel];
            [self.tableView reloadData];
            [self tableViewScrollToBottom];
            
        }
            break;
            
        case NSFetchedResultsChangeDelete:
            //            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
            //                    atIndexPath:indexPath];
            //            [self.tableView reloadData];
            break;
            
        case NSFetchedResultsChangeMove:
            //            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            //            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
            //                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.messageDataList.count;
}



- (void)cellContentDelete:(UUMessageCell *)cell
{
    
    UUMessage *message = cell.messageFrame.message;
    NSString *messageID = message.messageId;
    [XMPPSignal xmpp_deletedMessageWith:messageID contactID:self.conversationModel.conversation];
    if ([self.messageDataList containsObject:cell.messageFrame]) {
        [self.messageDataList removeObject:cell.messageFrame];
    }
    
    [self.tableView reloadData];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UUMessageFrame *messageModel = self.messageDataList[indexPath.row];
    return messageModel.cellHeight;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"chatCell";
    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
    }
    if (self.messageDataList.count < indexPath.row) {
        return nil;
    }
    UUMessageFrame *messageModel = self.messageDataList[indexPath.row];
    [cell setMessageFrame:messageModel];
    cell.statusImageView.hidden = YES;
    return cell;
}


- (void) cellImageWithUserID:(NSString *)userID messageModel:(UUMessage *)message {
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:userID];
    if (contacts != nil && contacts.count > 0) {
        ContactDataParser *contact = contacts[0];
        message.strIcon = contact.avatar;
        message.strName = [contact getName];
        
    }else{
        [HTTPManager getUserInfoWithUid:userID success:^(NSDictionary * dic, resultObject *state) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            
            if (state.status) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    ContactDataParser *cdParser = [[ContactDataParser alloc] init];
                    
                    // 会话对象是机器人特殊处理
                    if ([userID isEqualToString:@"robot"]) {
                        cdParser.username = NSLocalizedString(@"系统消息", nil);
                        cdParser.remark = NSLocalizedString(@"系统消息", nil);
                        cdParser.nickname = NSLocalizedString(@"系统消息", nil);
                        
                        
                    } else {
                        cdParser.username = parser.data.username;
                        cdParser.remark = parser.data.remark;
                        cdParser.nickname = parser.data.nickname;
                        //                        model.title = [parser.data getName];
                    }
                    cdParser.uid = userID;
                    cdParser.avatar = parser.data.logo;
                    cdParser.relation = parser.data.relation;
                    cdParser.mobile = parser.data.mobile;
                    cdParser.avatar = parser.data.logo;
                    message.strIcon = parser.data.logo;
                    message.strName = [parser.data getName];
                    // 防止同一时间刷新两次列表造成数据重复添加
                    NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                    // 添加数据库操作放在最后
                    [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
                });
            }
        } fail:^(NSError * _Nonnull error) {
            
        }];
    }
}



#pragma mark -- ChatKeyBoardDataSource
- (NSArray<MoreItem *> *)chatKeyBoardMorePanelItems
{
    MoreItem *item1 = [MoreItem moreItemWithPicName:@"sharemore_photo" highLightPicName:nil itemName:NSLocalizedString(@"拍照", nil)];
    MoreItem *item2 = [MoreItem moreItemWithPicName:@"sharemore_pic" highLightPicName:nil itemName:NSLocalizedString(@"图片", nil)];
    
    MoreItem *item3 = [MoreItem moreItemWithPicName:@"sharemore_location" highLightPicName:nil itemName:NSLocalizedString(@"位置", nil)];
    MoreItem *item4 = [MoreItem moreItemWithPicName:@"sharemore_redpacket" highLightPicName:nil itemName:NSLocalizedString(@"红包", nil)];
    MoreItem *item6 = [MoreItem moreItemWithPicName:@"sharemore_file" highLightPicName:nil itemName:NSLocalizedString(@"文件", nil)];
    //    MoreItem *item5 = [MoreItem moreItemWithPicName:@"sharemore_transfer" highLightPicName:nil itemName:NSLocalizedString(@"转账", nil)];
    //    MoreItem *item7 = [MoreItem moreItemWithPicName:@"sharemore_PersonInfor" highLightPicName:nil itemName:NSLocalizedString(@"个人资料", nil)];
    //        MoreItem *item7 = [MoreItem moreItemWithPicName:@"sharemore_voice" highLightPicName:nil itemName:NSLocalizedString(@"语音", nil)];
    //    MoreItem *item8 = [MoreItem moreItemWithPicName:@"sharemore_video" highLightPicName:nil itemName:NSLocalizedString(@"视频", nil)];
    self.moreItems = @[item1, item2,item3,item4,item6];
    
    return self.moreItems;
}

- (void)sendDocument:(RACTuple *)documentString {
    XMPP_RoomFileMessage *message = [XMPP_RoomFileMessage new];
    message.type = @"file";
    message.body = documentString.first;
    message.fileSize = documentString.third;
    message.displayName = documentString.second;
    NSString *messageString = message.mj_JSONString;
    [self.xmppRoom sendMessageWithBody:messageString];
    
}

//- (void) sendDocument:(NSString *)documentString {
//

//}

- (void)noticeContentDidClick:(UUMessageCell *)cell {
    
    NSString *mesType = cell.messageFrame.message.ext[MSG_TYPE];
    NSString *messageID = cell.messageFrame.message.messageId;
    if (isEmptyString(cell.messageFrame.message.body)) return;
    if (cell.messageFrame.message.ext && [mesType isEqualToString:GROUP_INVITE_CONFIRM]) {
        return;
    }
    
    
    XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:cell.messageFrame.message.body];
    if (![roomMessage isRoomOwn]) {
        return;
    }
    
    InviteDetailViewController *vc = [[InviteDetailViewController alloc] init];
    vc.invite_id = cell.messageFrame.message.ext[GROUP_INVITE_ID];
    vc.message = cell.messageFrame.message;
    vc.xmppRoom = self.conversationModel.xmppRoomManager.xmppRoom;
    vc.messageID = messageID;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

#pragma mark tableview delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    if (point.y+scrollView.bounds.size.height < scrollView.contentSize.height-200) {
        // 如果在浏览之前的聊天内容，收到新消息不让跳到最底部
        canScrollBottom = NO;
    } else {
        canScrollBottom = YES;
    }
    
}

- (void)tableViewScrollToBottom
{
    if (self.messageDataList.count==0)
        return;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageDataList.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}


@end
