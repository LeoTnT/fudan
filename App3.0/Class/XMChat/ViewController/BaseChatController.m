//
//  BaseChatController.m
//  App3.0
//
//  Created by apple on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "BaseChatController.h"
#import "PersonalViewController.h"
#import <Reachability/Reachability.h>
#import <Photos/PHAsset.h>
#import <Photos/Photos.h>
#import "TransferAccountViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "HVideoViewController.h"

@interface BaseChatController ()




@end

@implementation BaseChatController
{
    
    BOOL canScrollBottom;
    // 录音
    BOOL isbeginVoiceRecord;
    Mp3Recorder *MP3;
    NSInteger playTime;
    NSTimer *playTimer;
    NSTimer *recordLevelTimer;
    
    NSInteger limitCount;
    
    BOOL isVoice;
}
- (ChatKeyBoard *)chatKeyBoard
{
    if (_chatKeyBoard == nil) {
        _chatKeyBoard = [ChatKeyBoard keyBoardWithNavgationBarTranslucent:NO];
        _chatKeyBoard.delegate = self;
        _chatKeyBoard.dataSource = self;
        _chatKeyBoard.keyBoardStyle = KeyBoardStyleChat;
        _chatKeyBoard.placeHolder = @"请输入消息";
        @weakify(self);
        //        chatKeyBoardMorePanelItems
//        [_chatKeyBoard.chatToolBar setToolbarBtnClick:^(UIButton *sender) {
//            @strongify(self);
//            [self showActionSheet];
//        }];
    }
    return _chatKeyBoard;
}

- (NSMutableArray *)photoArray {
    if (!_photoArray) {
        _photoArray = [NSMutableArray array];
    }
    return _photoArray;
}



- (void)setConversationModel:(ConversationModel *)conversationModel {
    _conversationModel = conversationModel;
    self.title = conversationModel.title;
 
    
}

- (void) popAction {
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = BG_COLOR;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    limitCount = 20;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50+self.iphonx_bottom, 0));
    }];
    [[IQKeyboardManager sharedManager].disabledDistanceHandlingClasses addObject:[self class]];
    [[[IQKeyboardManager sharedManager] disabledToolbarClasses] addObject:[self class]];
    self.coreDataDataSource = [NSMutableArray array];
    self.chatKeyBoard.associateTableView = self.tableView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.chatKeyBoard];
    //    [self setHeaderRefresh];
    @weakify(self);
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        [self.chatKeyBoard keyboardDown];
    }]];
    self.navigationController.interactivePopGestureRecognizer.delaysTouchesBegan=NO;
    canScrollBottom = YES;
    
    [ChatHelper shareHelper].xmchatVC = self;
    
    [self loadBaseViewsAndData];
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self popAction];
    }];

    
    if ([self.conversationModel.bareJID.user isEqualToString:@"robot"]) {
        // 不是机器人
        self.chatKeyBoard.hidden = YES;
        self.chatKeyBoard.associateTableView = nil;
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }  
    

//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xm_bgImage"]];
//    self.tableView.backgroundColor= [UIColor clearColor];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

//- (void) setMessageStatus:(XMPPMessage *)message {
//
//    if ([message hasReceiptResponse]) {
//          [self.chatModel.dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            UUMessageFrame *model = self.chatModel.dataSource[idx];
//            model.message.status = UUMessageStatusRead;
//        }];
//        [self.tableView reloadData];
//
//    }
//    if ([message hasReceiptRequest]) {
//         [XMPPSignal generateReceiptResponse:message];
//    }
//
//}


- (void) setupRefesh {
    
    [self.tableView.mj_footer endRefreshing];
    
}

- (void)loadBaseViewsAndData
{
    self.chatModel = [[ChatModel alloc]init];
    self.chatModel.isGroupChat = NO;
}

/**
 点击 + 弹出 框
 */
- (void) showActionSheet {
    [self.chatKeyBoard keyboardDown];
    NSArray *arr = [self chatKeyBoardMorePanelItems];
    NSMutableArray *list = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoreItem *item = arr[idx];
        Item *model = [Item new];
        model.title =item.itemName;
        model.icon = item.itemPicName;
        [list addObject:model];
    }];
    
    [[[PicAndTextActionSheet alloc] initWithList:list title:nil selected:^(NSInteger index) {
        [self chatKeyBoard:self.chatKeyBoard didSelectMorePanelItemIndex:index];
    }] showInView:nil];
    
}

- (void) willReloadTableViewWitPage:(NSInteger )page resultController:(NSFetchedResultsController *)resultController completed:(void(^)(NSInteger index))completed {
 
       limitCount = 20;
 
    [self.coreDataDataSource removeAllObjects];
     NSInteger index;
    NSInteger allData = resultController.fetchedObjects.count;
    NSArray *temp = [resultController.fetchedObjects reverseObjectEnumerator].allObjects;
    NSRange raaa ;
    if (page*limitCount-allData >= limitCount) {
        completed(0);
        return;
    }
    if (page * limitCount <= allData) {
        raaa = NSMakeRange((page -1)*limitCount, limitCount);
        index = limitCount;
    } else {
        raaa = NSMakeRange((page -1)*limitCount, limitCount-page*limitCount+allData);
        index = limitCount-page*limitCount+allData;
    }
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           raaa];
    NSArray *resultArray = [temp  objectsAtIndexes:indexes];
    resultArray = [resultArray reverseObjectEnumerator].allObjects;
    [self.coreDataDataSource addObjectsFromArray:resultArray];
    completed(index);
    
    [self tableViewScrollToBottom];
}



#pragma mark tableview delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point=scrollView.contentOffset;
    if (point.y+scrollView.bounds.size.height < scrollView.contentSize.height-200) {
        // 如果在浏览之前的聊天内容，收到新消息不让跳到最底部
        canScrollBottom = NO;
    } else {
        canScrollBottom = YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [UIView new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 45)];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return nil;
    //    static NSString *identifier = @"chatCell";
    //    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    //    if (cell == nil) {
    //        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    //     }
    //    [cell setMessageFrame:self.chatModel.dataSource[indexPath.row]];
    //
    //    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.chatKeyBoard keyboardDown];
}

- (void)locationDidClickLatitude:(double)latitude longitude:(double)longitue adddress:(NSString *)adddress
{
    
    S_BaiduMap *locationController = [S_BaiduMap new];
    locationController.showType = BMKShowMap;
    locationController.delegate = self;
    CLLocationCoordinate2D cc = {latitude,longitue} ;
    locationController.locationCoordinate = cc;
    locationController.pathShowAddress = adddress;
    [self.navigationController pushViewController:locationController animated:YES];
}




- (void)redPacketClick:(UUMessageCell *)cell {
    // 服务器拉取红包数据
    NSString *rpId = cell.messageFrame.message.rpId;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            
            if (self.conversationModel.chatType == UUChatTypeChat) {
                if (model.is_can_rec) {
                    // 我可以领（收红包）
                    //                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversationModel;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            } else {
                if (model.is_can_rec || (!model.is_can_rec && !model.is_received)) {
                    // 我可以领（群红包）或者 我已经不可领并且我还没领
                    //                    [[ChatHelper shareHelper] createSystemSoundWithName:@"money" soundType:@"mp3" vibrate:NO];
                    self.openRPView = [OpenRedPacketView creatViewWithRedpacketId:rpId model:model viewController:self];
                } else {
                    // 跳转红包详情
                    RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                    rpd.model = model;
                    rpd.conversation = self.conversationModel;
                    [self.navigationController pushViewController:rpd animated:YES];
                }
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
    
}

- (void)openRedPacketWithId:(NSString *)rpId {
    [XSTool showProgressHUDWithView:self.view];
    if (self.conversationModel.chatType == UUChatTypeChat) {
        [HTTPManager getRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId};
                        
                        [XMPPSignal sendMessage:text to:self.conversationModel.bareJID.full ext:extDic complete:^(XMPPMessageSendState state) {
                            
                        }];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversationModel;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    } else {
        [HTTPManager getGroupRedPacketWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                // 领取红包成功
                
                // 刷新用户钱包
                [HTTPManager updateWalletWithSuccess:^(NSDictionary *dic, resultObject *state) {
                    
                } fail:^(NSError *error) {
                    
                }];
                
                // 获取红包详情
                [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [self.openRPView dismiss];
                    if (state.status) {
                        RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
                        
                        // 发送领取通知消息
                        NSString *text = [NSString stringWithFormat:@"领取了%@的红包",model.nickname];
                        NSDictionary *extDic = @{MSG_TYPE:READ_PACKET_BACK,CUSTOM_MSG_ID:rpId,MESSAGE_PACK_FROM:model.uid};
                        
                        XMPP_RoomMessage *message = [XMPP_RoomMessage new];
                        message.type = @"txt";
                        message.body = text;
                        message.ext = extDic.mj_JSONString;
                        NSString *messageString = message.mj_JSONString;
                        //    NSString *messageString = [message.mj_JSONString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                        [self.conversationModel.xmppRoomManager.xmppRoom sendMessageWithBody:messageString];
                        
                        // 跳转红包详情
                        RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
                        rpd.model = model;
                        rpd.conversation = self.conversationModel;
                        [self.navigationController pushViewController:rpd animated:YES];
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } fail:^(NSError *error) {
                    [self.openRPView dismiss];
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
                }];
            } else {
                [self.openRPView dismiss];
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:state.info];
            }
            
        } fail:^(NSError *error) {
            [self.openRPView dismiss];
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }
    
}

- (void)detailRedPacketWithId:(NSString *)rpId {
    [HTTPManager getRedPacketDetailWithId:rpId success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.openRPView dismiss];
        if (state.status) {
            RedPacketDetailModel *model = [RedPacketDetailModel mj_objectWithKeyValues:dic[@"data"]];
            // 跳转红包详情
            RedPacketDetailViewController *rpd = [[RedPacketDetailViewController alloc] init];
            rpd.model = model;
            rpd.conversation = self.conversationModel;
            [self.navigationController pushViewController:rpd animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [self.openRPView dismiss];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)cellContentFileDidClick:(UUMessageCell *)cell {

    NSURL *fileUrl = [NSURL fileURLWithPath:cell.messageFrame.message.fileUrl];
    if (fileUrl != nil) {
        if (_fileInteractionController == nil) {
            //            _fileInteractionController = [[UIDocumentInteractionController alloc] init];
            _fileInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
            _fileInteractionController.delegate = self;
        } else {
            _fileInteractionController.URL = fileUrl;
        }
        [_fileInteractionController presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
    } else {
        [XSTool showToastWithView:self.view Text:@"文件打开失败"];
    }
    
}
#pragma mark - UIDocumentInteractionControllerDelegate
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller {
    return self.view.frame;
}

#pragma mark -- ChatKeyBoardDataSource
- (NSArray<MoreItem *> *)chatKeyBoardMorePanelItems
{
 
    return nil;
}
- (NSArray<ChatToolBarItem *> *)chatKeyBoardToolbarItems
{
    ChatToolBarItem *item1 = [ChatToolBarItem barItemWithKind:kBarItemFace normal:@"face" high:@"face_HL" select:@"keyboard"];
    ChatToolBarItem *item2 = [ChatToolBarItem barItemWithKind:kBarItemVoice normal:@"voice" high:@"voice_HL" select:@"keyboard"];
    ChatToolBarItem *item3 = [ChatToolBarItem barItemWithKind:kBarItemMore normal:@"more_ios" high:@"more_ios_HL" select:nil];
    ChatToolBarItem *item4 = [ChatToolBarItem barItemWithKind:kBarItemSwitchBar normal:@"switchDown" high:nil select:nil];
    return @[item1, item2, item3, item4];
}

- (NSArray<FaceThemeModel *> *)chatKeyBoardFacePanelSubjectItems
{
    return [FaceSourceManager loadFaceSource];
}




- (void)chatKeyBoard:(ChatKeyBoard *)chatKeyBoard didSelectMorePanelItemIndex:(NSInteger)index
{
    NSLog(@"Select %ld",(long)index);
    MoreItem *item = self.moreItems[index];
 
    if ([item.itemName isEqualToString:Localized(@"拍照")]) {
        
        
        if (self.conversationModel.chatType == UUChatTypeChatRoom) {
            
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.delegate = self;
                [self presentViewController:picker animated:YES completion:nil];
            }
        }else{
            
            HVideoViewController *ctrl = [[NSBundle mainBundle] loadNibNamed:@"HVideoViewController" owner:nil options:nil].lastObject;
            ctrl.HSeconds = 10;//设置可录制最长时间
            ctrl.takeBlock = ^(RACTuple *item) {
                
                if (item.count ==1) {
                    [self sendImageWithData:item.first imageData:nil];
                }else if (item.count == 4){
                    
                    [self sendVideoWitdURL:item];
                    
                }
 
            };
            [self presentViewController:ctrl animated:YES completion:nil];
 
        }
        
     } else if ([item.itemName isEqualToString:@"图片"]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }else if ([item.itemName isEqualToString:Localized(@"person_info")]){
        PersonalViewController *personalVC = [[PersonalViewController alloc] initWithUid:self.conversationModel.bareJID.user];
        
        [self.navigationController pushViewController:personalVC animated:YES];
    }else if ([item.itemName isEqualToString:Localized(@"红包")]) {
        RedPacketSendViewController *redPacketVC = [[RedPacketSendViewController alloc] init];
        redPacketVC.conversation = self.conversationModel;
        [self.navigationController pushViewController:redPacketVC animated:YES];
    } else if ([item.itemName isEqualToString:Localized(@"转账")]) {
        [self tapTransform];
        
    }else if ([item.itemName isEqualToString:Localized(@"位置")]) {
        S_BaiduMap *locationController = [S_BaiduMap new];
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        [self.navigationController pushViewController:locationController animated:YES];
    }else if ([item.itemName isEqualToString:Localized(@"文件")]) {
        NSArray *documentTypes = @[@"public.content",@"public.text",@"public.source-code",@"public.image",@"public.audiovisual-content",@"com.adobe.pdf",@"com.apple.keynote.key",@"com.microsoft.word.doc",@"com.microsoft.excel.xls",@"com.microsoft.powerpoint.ppt"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
        docPicker.delegate = self;
        docPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:docPicker animated:YES completion:nil];
    }else if([item.itemName isEqualToString:@"焚烧聊天"]){
        [self sendBurnAfterReading];
        
    }else if ([item.itemName isEqualToString:@"语音"]) {
        
 
        isVoice = YES;
 
//        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
//        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_AUDIO];
        [self chatWithVoice:YES];
        
    } else if ([item.itemName isEqualToString:@"视频"]) {
        isVoice = NO;
 
        [self chatWithVoice:NO];
        
//        NSString *callName = [NSString stringWithFormat:@"xsy%@",_chatterName];
//        [[TILChatCallManager sharedManager] makeCallWithUsername:callName type:TILCALL_TYPE_VIDEO];
    }
}


- (void) tapTransform {
    
}

- (void) chatWithVoice:(BOOL) isVoice {
    
}


- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    BOOL fileUrlAuthozied = [url startAccessingSecurityScopedResource];
    if (fileUrlAuthozied) {
        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
        NSError *error;
        
        [fileCoordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL * _Nonnull newURL) {
            NSData *data = [NSData dataWithContentsOfURL:newURL];
            
            // 构造文件消息
            NSString *displayName = [[url path] lastPathComponent];
            [XMPPManager sharedManager].fileName = displayName;
            NSString *size = [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile];
            
            [XMPPBaseMessage xmppUpFile:data completed:^(NSError *error, NSString *url1) {
                if (error) {
                    NSLog(@"error  =%@",error.localizedDescription);
                    [MBProgressHUD showMessage:!isEmptyString(url1) ?url1:@"发送失败" view:self.view hideTime:1.5 doSomeThing:nil];

                }else{
                    [XMPPSignal sendFileUrl:url1 displayName:displayName fileSize:size to:self.conversationModel.bareJID.full complete:^(XMPPMessageSendState state) {
                        
                    }];
                    
                }
            }];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [url stopAccessingSecurityScopedResource];
    } else {
        // Error handling
    }
}



#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage]?:info[UIImagePickerControllerOriginalImage];
    
    
    [self sendImageWithData:selectedImage imageData:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) sendImageWithData:(UIImage *)selectedImage  imageData:(NSData *)imageData{
    
}

- (void) sendImageWithData:(NSData *)imageData {
    
}

- (void) sendVideoWitdURL:(RACTuple *)videoTuple {
    
}

- (void)tableViewScrollToBottom {
 
    

}

- (void) setHeaderRefresh {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshData)];
    [header setTitle:Localized(@"下拉加载最新数据") forState:MJRefreshStatePulling];
    self.tableView.mj_header = header;
}


- (void)cellContentCopy:(UUMessageCell *)cell
{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = cell.messageFrame.message.strContent;
}



- (void)cellContentTranspond:(UUMessageCell *)cell
{
    NSLog(@"转发");
    UUMessage *message = cell.messageFrame.message;
    MessageType type = message.type;
    if (type == UUMessageTypeText) {
        TranspondListViewController *trVC = [[TranspondListViewController alloc] init];
        XM_TransModel *transModel = [[XM_TransModel alloc] initDefaultShareText:message.strContent ext:nil];
        trVC.transModel = transModel;
        [self.navigationController pushViewController:trVC animated:YES];
    }else{
        [MBProgressHUD showMessage:@"暂不支持转发此消息" view:self.view];
    }
    
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)chatKeyBoardTextViewDidBeginEditing:(UITextView *)textView
{
    [self.menuController setMenuItems:@[]];
}

- (void)chatKeyBoardTextViewDidChange:(UITextView *)textView
{
    
    
}

 


#pragma mark - 录音状态

- (void)startRecording {
    [self setAudioSessionWithCategory:AVAudioSessionCategoryRecord];
    if (_audioRecorder) {
        [_audioRecorder stop];
        _audioRecorder.delegate = nil;
        _audioRecorder = nil;
    }
    if (![self.audioRecorder isRecording]) {
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder record];
        recordLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(levelTimerCallback:) userInfo:nil repeats:YES];
    }
    playTime = 0;
    playTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countVoiceTime) userInfo:nil repeats:YES];
    [UUProgressHUD show];
}

/* 该方法确实会随环境音量变化而变化，但具体分贝值是否准确暂时没有研究 */
- (void)levelTimerCallback:(NSTimer *)timer {
    [self.audioRecorder updateMeters];
    
    float   level;                // The linear 0.0 .. 1.0 value we need.
    float   minDecibels = -80.0f; // Or use -60dB, which I measured in a silent room.
    float   decibels    = [self.audioRecorder averagePowerForChannel:0];
    
    if (decibels < minDecibels)
    {
        level = 0.0f;
    }
    else if (decibels >= 0.0f)
    {
        level = 1.0f;
    }
    else
    {
        float   root            = 2.0f;
        float   minAmp          = powf(10.0f, 0.05f * minDecibels);
        float   inverseAmpRange = 1.0f / (1.0f - minAmp);
        float   amp             = powf(10.0f, 0.05f * decibels);
        float   adjAmp          = (amp - minAmp) * inverseAmpRange;
        
        level = powf(adjAmp, 1.0f / root);
    }
    
    /* level 范围[0 ~ 1], 转为[0 ~6] 之间 */
    dispatch_async(dispatch_get_main_queue(), ^{
        [UUProgressHUD changeRecordLevel:level*7+1];
    });
}

/**
 *  语音状态
 */
- (void)chatKeyBoardDidStartRecording:(ChatKeyBoard *)chatKeyBoard
{
    playTime = 0;
    [[ChatHelper shareHelper] createSystemSoundWithName:@"talkroom_begin" soundType:@"mp3" vibrate:YES];
    [self performSelector:@selector(startRecording) withObject:nil afterDelay:0.5];
}

- (void)chatKeyBoardDidCancelRecording:(ChatKeyBoard *)chatKeyBoard
{
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    if (playTimer) {
        [self.audioRecorder stop];
        [self.audioRecorder deleteRecording];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    }
    [UUProgressHUD dismissWithError:Localized(@"has_been_cancelled")];
}

- (void)chatKeyBoardDidFinishRecoding:(ChatKeyBoard *)chatKeyBoard
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRecording) object:nil];//可以取消成功。
    
    //此处需要恢复设置回放标志，否则会导致其它播放声音也会变小
    [self setAudioSessionWithCategory:AVAudioSessionCategoryPlayback];
    
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        [recordLevelTimer invalidate];
    } else {
        return;
    }
    if (playTime == 0) {
        [UUProgressHUD dismissWithError:Localized(@"时间过短")];
        return;
    }
    [UUProgressHUD dismissWithSuccess:Localized(@"录音成功")];
    NSLog(@"%@--%@",[self getSavePath],[self getSavePath].path);
    NSString *voiceTime = [NSString stringWithFormat:@"%d",(int)playTime];
    
    NSURL *url =  [self getSavePath];
    NSData *voiceData = [NSData dataWithContentsOfURL:url];
    [XMPPBaseMessage xmppUpVoice:voiceData completed:^(NSError *error,NSString *serviceURl) {
        
        NSLog(@"error  =%@",error.localizedDescription);
        if (isEmptyString(serviceURl)) return ;
        
        [self sendSignalChatMessage:serviceURl time:voiceTime];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        if ([manager fileExistsAtPath:url.path]) {
            NSString *sss = [serviceURl componentsSeparatedByString:@"/"].lastObject;
            NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            NSString *newPath =[urlStr stringByAppendingPathComponent:sss];
             NSError *error;
            BOOL iii = [manager createFileAtPath:newPath contents:voiceData attributes:nil];
            if (error) {
                XMPP_LOG(@"error = = = %@",error.localizedDescription)
            }
            NSLog(@"iii =%@",iii?@"YES":@"NO");
        }
        
        
    }];
    
}




- (void) sendSignalChatMessage:(NSString *)serviceURl time:(NSString *)voiceTime {
    
    if (self.getVoiceBlock) {
        self.getVoiceBlock(serviceURl, voiceTime);
    }
    
}

- (void)countVoiceTime
{
    playTime ++;
    if (playTime>=60) {
        [self chatKeyBoardDidFinishRecoding:nil];
        [UUProgressHUD dismissWithSuccess:nil];
     }
}

- (void)endRecordVoice:(UIButton *)button
{
    if (playTimer) {
        [self.audioRecorder stop];
        [playTimer invalidate];
        playTimer = nil;
        playTime = 0;
        
        
        
        //        //缓冲消失时间 (最好有block回调消失完成)
        //        self.btnVoiceRecord.enabled = NO;
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //            self.btnVoiceRecord.enabled = YES;
        //        });
    }
}

/**
 *  设置音频会话
 */
-(void)setAudioSessionWithCategory:(NSString *)category {
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    //设置为录音状态
    [audioSession setCategory:category error:nil];
    [audioSession setActive:YES error:nil];
}

/**
 *  取得录音文件保存路径
 *
 *  @return 录音文件路径
 */
-(NSURL *)getSavePath {
    //    NSString *name = [NSString stringWithFormat:@"recorder"];
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
    NSLog(@"file path:%@",urlStr);
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}
/**
 *  取得录音文件设置
 *
 *  @return 录音设置
 */
-(NSDictionary *)getAudioSetting{
    NSMutableDictionary *dicM=[NSMutableDictionary dictionary];
    //设置录音格式
    [dicM setObject:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
    //设置录音采样率，8000是电话采样率，对于一般录音已经够了
    [dicM setObject:@(8000) forKey:AVSampleRateKey];
    //设置通道,这里采用单声道
    [dicM setObject:@(1) forKey:AVNumberOfChannelsKey];
    //每个采样点位数,分为8、16、24、32
    [dicM setObject:@(8) forKey:AVLinearPCMBitDepthKey];
    //是否使用浮点数采样
    [dicM setObject:@(YES) forKey:AVLinearPCMIsFloatKey];
    //录音的质量
    [dicM setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    //....其他设置等
    return dicM;
}

- (AVAudioRecorder *)audioRecorder
{
    if (_audioRecorder == nil) {
        //创建录音文件保存路径
        NSURL *url=[self getSavePath];
        //创建录音格式设置
        NSDictionary *setting=[self getAudioSetting];
        NSError *error;
        _audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate = self;
        if (error) {
            NSLog(@"创建录音机对象时发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}

- (AVAudioPlayer *)audioPlayer
{
    if (!_audioPlayer) {
        NSURL *url = [self getSavePath];
        NSError *error;
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        _audioPlayer.numberOfLoops = 0;
        [_audioPlayer prepareToPlay];
        if (error) {
            NSLog(@"创建播放器过程中发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioPlayer;
}



@end

