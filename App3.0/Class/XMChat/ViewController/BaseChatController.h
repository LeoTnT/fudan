//
//  BaseChatController.h
//  App3.0
//
//  Created by apple on 2018/2/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTablViewController.h"

#import "FaceSourceManager.h"

#import "ChatKeyBoard.h"
#import <AVFoundation/AVFoundation.h>
#import "S_BaiduMap.h"
#import "XLPhotoBrowser.h"
// 红包
#import "OpenRedPacketView.h"
#import "RedPacketSendViewController.h"
#import "RedPacketDetailViewController.h"

// 转账
#import "ChatTransferViewController.h"
#import "ChatTransferDetailViewController.h"

#import "ChatModel.h"
// 气泡
#import "UUMessageCell.h"
#import "UUMessageFrame.h"
#import "UUMessage.h"

#import "ActionItemCell.h"
#import "PicAndTextActionSheet.h"

#import "Mp3Recorder.h"
#import "UUProgressHUD.h"


#import "ConversationModel.h"
#import "PersonalViewController.h"

@interface BaseChatController : XSBaseTablViewController
<ChatKeyBoardDelegate,ChatKeyBoardDataSource,UUMessageCellDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVAudioRecorderDelegate,MapLocationViewDelegate, XLPhotoBrowserDelegate, XLPhotoBrowserDatasource, UIDocumentPickerDelegate, UIDocumentInteractionControllerDelegate,NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) ChatKeyBoard *chatKeyBoard;
@property (nonatomic, strong) ChatModel *chatModel;
@property (nonatomic, strong) AVAudioRecorder *audioRecorder;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;//音频播放器，用于播放录音文件
@property (nonatomic, strong) UIMenuController *menuController;
@property (nonatomic, strong) NSIndexPath *menuIndexPath; //选中消息菜单索引
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) XLPhotoBrowser *photoBrowser;
@property (nonatomic, strong) OpenRedPacketView *openRPView;
@property (nonatomic, strong) NSTimer *recordingTimer;
@property (nonatomic, strong) UIDocumentInteractionController *fileInteractionController; // 文件预览实例
@property (nonatomic, strong) NSArray *moreItems;

@property (nonatomic, copy) NSString *avatarUrl;


/**
 弹出选择时间
 */
@property (nonatomic ,strong)UIView *backView;

/**
 获取声音 数据
 */
@property (nonatomic ,copy)void (^getVoiceBlock)(NSString *url,NSString *time);

/*  群聊 */

@property (nonatomic ,strong)XMPPRoomManager *xmppRoomManager;

@property (nonatomic ,strong)XMPPRoom *xmppRoom;
 

/**
 存储 message 对象 数组
 */
@property (nonatomic ,strong)NSMutableArray *dataSource;


/**
 从 coreData 数组中 取数据
 */
@property (nonatomic ,strong)NSMutableArray *coreDataDataSource;

@property (nonatomic ,strong)ConversationModel *conversationModel;

- (void)tableViewScrollToBottom;

- (void)openRedPacketWithId:(NSString *)rpId;

- (void)detailRedPacketWithId:(NSString *)rpId;


- (void) tapTransform;

/**
 长按事件弹出菜单
 */
- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(MessageType)messageType;


/**
 发送图片
 
 @param selectedImage selectedImage description
 */
- (void) sendImageWithData:(UIImage *)selectedImage  imageData:(NSData *)imageData;



/**
  视频
  @param videoTuple  (image,data,time,size);
 */
- (void) sendVideoWitdURL:(RACTuple *)videoTuple ;



/**
 单聊
 */
- (void) chatWithVoice:(BOOL) isVoice;

- (NSURL *) getSavePath;


/**
 阅后即焚
 */
- (void) sendBurnAfterReading;

/**
 tableView 分页 展示
 
 @param page page description
 @param completed completed descriptio
 */
- (void) willReloadTableViewWitPage:(NSInteger )page resultController:(NSFetchedResultsController *)resultController completed:(void(^)(NSInteger index))completed;

- (void) popAction;
@end

