//
//  XMChatController.h
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChatController.h"
@interface XMChatController : BaseChatController

/*
 商品详情进入会话显示商品
 */
@property (nonatomic, copy) NSString *productJsonString;
@property (nonatomic, copy) NSString *productMessageId;

@property (nonatomic,assign) BOOL isBurn;
@end
