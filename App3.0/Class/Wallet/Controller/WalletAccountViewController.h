//
//  WalletAccountViewController.h
//  App3.0
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletAccountViewController : XSBaseViewController
/**钱包类型*/
@property(nonatomic,copy)NSString *walletType;
/**标题*/
@property(nonatomic,copy)NSString *navTitle;
@end
