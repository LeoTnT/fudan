//
//  ReportFormsViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReportFormsViewController.h"

#define topHeight 180
#define leftSpace 10
#define bottomSpace 80
#define imgSize 30
#define baseHeight 80
#define baseLabelWidth 80
#define bottomHeight 280
@interface ReportFormsViewController ()
{
    UILabel *_yesterdayMoney;//昨天收益
    UILabel *_servicesMoney;//咨询服务费用
    NSMutableArray *_statisticsArray;//昨日收益+统计概览的值数组 共6个
    NSArray *_imgImageArray;//收益图片数组
    NSArray *_titleArray;//收益标题数组

}
@end

@implementation ReportFormsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //默认是0
    _statisticsArray = [NSMutableArray arrayWithArray:@[@(0),@(0),@(0),@"0元",@"0元",@"0元"]];
    _imgImageArray = @[@"user_cart_totalsell",@"user_cart_totalorder",@"user_cart_totaloearn",@"user_cart_todayearn",@"user_cart_planearn"];
    _titleArray = @[@"累计销售",@"累计订单数",@"累计收益",@"今日收益",@"今日预计收益"];
    
    [self getInfo];
  
    [self setSubViews];
}

#pragma mark - 网络获取数据
-(void)getInfo{


}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//   self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = Localized(@"报表");
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];

}
-(void)setSubViews{
    
    //顶部view
    UIView *top_View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, topHeight)];
    top_View.backgroundColor = mainColor;
    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, leftSpace*4, mainWidth, leftSpace*2)];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.font = [UIFont systemFontOfSize:16];
    topLabel.text = @"昨日收益(元)";
    topLabel.textColor = [UIColor whiteColor];
    [top_View addSubview:topLabel];
    
    //昨日收益
    _yesterdayMoney = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(topLabel.frame)+2*leftSpace, mainWidth, leftSpace*3)];
    _yesterdayMoney.font = [UIFont systemFontOfSize:35];
    _yesterdayMoney.textAlignment = NSTextAlignmentCenter;
    _yesterdayMoney.text = [NSString stringWithFormat:@"%@",[_statisticsArray objectAtIndex:0]];
    _yesterdayMoney.textColor = [UIColor whiteColor];
    [top_View addSubview:_yesterdayMoney];
    
    //咨询费view
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_yesterdayMoney.frame)+3*leftSpace, mainWidth, 4*leftSpace)];
    bottomView.backgroundColor = HighLightColor_Main;
    
    _servicesMoney = [[UILabel alloc] initWithFrame:CGRectMake(2*leftSpace, leftSpace, mainWidth/3, 2*leftSpace)];
    _servicesMoney.text = @"未领取咨询服务费 ¥0";
    _servicesMoney.font = [UIFont systemFontOfSize:11];
    _servicesMoney.textColor = [UIColor whiteColor];
    [bottomView addSubview:_servicesMoney];
    
    //领取咨询费的按钮
    UIButton *servicesBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-3*leftSpace-4*leftSpace, CGRectGetMinY(_servicesMoney.frame), 4*leftSpace, CGRectGetHeight(_servicesMoney.frame))];
    [servicesBtn setTitle:@"领取" forState:UIControlStateNormal];
    [servicesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [servicesBtn setBackgroundColor:LINE_COLOR];
    servicesBtn.layer.cornerRadius = 5;
    servicesBtn.layer.masksToBounds = YES;
    servicesBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    [bottomView addSubview:servicesBtn];
    [top_View addSubview:bottomView];
    [self.view addSubview:top_View];
    
    //下部view
//    CGFloat bottomHeight = mainHeight-leftSpace-bottomSpace-CGRectGetMaxY(top_View.frame);
    UIView *bottom_View = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(top_View.frame)+leftSpace, mainWidth, bottomHeight)];
    bottom_View.backgroundColor = [UIColor whiteColor];
    UILabel *statistic = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace, 2*leftSpace, mainWidth, 2*leftSpace)];
    statistic.text = @"统计概览";
    statistic.font = [UIFont systemFontOfSize:14];
    [bottom_View addSubview:statistic];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(statistic.frame), mainWidth, 1)];
    lineView.backgroundColor = LINE_COLOR;
    [bottom_View addSubview:lineView];
    //五个收益
    for (int i=0; i<5; i++) {
        NSUInteger row = i%2;
        NSUInteger col = i/2;
        CGFloat width = mainWidth/2;
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(width*row, col*baseHeight, width, baseHeight)];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(lineView.frame)+leftSpace, imgSize, imgSize)];
        imgView.image = [UIImage imageNamed:[_imgImageArray objectAtIndex:i]];
        [leftView addSubview:imgView];
        UILabel  *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)+leftSpace, CGRectGetMinY(imgView.frame), baseLabelWidth, leftSpace)];
        titleLabel.text = [_titleArray objectAtIndex:i];
        titleLabel.textColor = HighLightColor_Gray;
        titleLabel.font = [UIFont systemFontOfSize:13];
        [leftView addSubview:titleLabel];
        //具体收益  设置tag值
        UILabel *monyLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame), CGRectGetWidth(titleLabel.frame), 3*leftSpace)];
        monyLabel.textColor = [UIColor blackColor];
        monyLabel.text = [NSString stringWithFormat:@"%@",[_statisticsArray objectAtIndex:i+1]];
        monyLabel.font = [UIFont systemFontOfSize:18];
        if (i>1) {
            //富文本改变样式
            // 创建Attributed
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:monyLabel.text];
            NSUInteger secondLoc = [[noteStr string] rangeOfString:@"元"].location;
            // 需要改变的区间
            NSRange range = NSMakeRange(secondLoc,1 );
            // 改变字体大小及类型
            [noteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:range];
            // 为label添加Attributed  
            [monyLabel setAttributedText:noteStr];
        }
        
        [leftView addSubview:monyLabel];
        [bottom_View addSubview:leftView];
    }
    
    [self.view addSubview:bottom_View];


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
