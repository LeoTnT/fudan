//
//  WithdrawViewController.m
//  App3.0
//
//  Created by mac on 2017/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WithdrawViewController.h"
#import "UserModel.h"
#import "BindBankCardViewController.h"
#import "RealNameVerificationViewController.h"
#import "UserModel.h"
#import "AddBankCardViewController.h"
#import "BankTableViewCell.h"
#import "MJRefresh.h"
#import "WalletModel.h"
#import "BankCardModel.h"
#import "WithDrawRecordViewController.h"
#import "BankCardViewController.h"
#import "RSAEncryptor.h"
#import "PersonViewController.h"
#import "ModifyOldPayViewController.h"
#import "LoginModel.h"
#import "BankCardDefaultView.h"
#import "ContainFieldTableViewCell.h"
#import "GrayBackView.h"

@interface WithdrawViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,BankCardDefaultViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UILabel *mostMoneyLabel,*chargeLabel,*realAndChargeLabel;//提示最多提现金额  手续费
@property(nonatomic,strong)UIButton *allWithDrawBtn;//全部提现按钮
@property(nonatomic,strong)UITextField *moneyField;//提现金额
@property(nonatomic,strong)UITableView *walletTypeTable;
@property(nonatomic,copy)NSString *walletTypeStr;//提现钱包
@property(nonatomic,strong)WithDrawRule *withDrawRule;//提现规则
@property(nonatomic,strong)WithDrawWalletType *walletType;//当前钱包类型
@property(nonatomic,assign)float mostWithDrawMoney;//最大提现金额
@property(nonatomic,assign)float leastWithDrawMoney;//最小提现金额
@property(nonatomic,strong)NSMutableArray *accountArray;//钱包类型数组
@property(nonatomic,strong)NSArray *bankCardArray;//银行卡数组
@property (nonatomic,strong) XSCustomButton *commitBtn;
@property (nonatomic, copy) NSString *payPassWord;
@property (assign, nonatomic) BOOL isTimer;
@property(nonatomic,strong)BankCardDefaultView *defaultView;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descString;
@property(nonatomic,strong)UITextField *walletTypeField;
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)NSArray *placeHoldArray;
@property(nonatomic,strong)UITextField *realMoneyField;
@property(nonatomic,strong)UITextField *bankField,*chargeField;
@end

@implementation WithdrawViewController

#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self setNavBar];
    self.titleArray=@[@"提现钱包",@"提现金额",Localized(@"sjdz"),@"提现管理费",Localized(@"银行类型")];
    self.placeHoldArray=@[@"请选择提现的钱包",@"请输入提现金额",@"提现金额-提现管理费",@"提现管理费",@"请选择银行卡"];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.commitBtn];
    //获取提现信息
    [self getWithDrawWallet];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getInfo];
    //注册通知
    if (self.parser) {
        [self setBankCard];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.scrollEnabled=NO;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
-(XSCustomButton *)commitBtn{
    if (!_commitBtn) {
        //提交按钮
        _commitBtn= [[XSCustomButton alloc] initWithFrame:CGRectMake(10, 460, mainWidth-20, 50) title:Localized(@"bug_submit_do") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [_commitBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
        [_commitBtn addTarget:self action:@selector(commitWithDraw) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_commitBtn];
    }
    return _commitBtn;
}
-(UITableView *)walletTypeTable{
    if (!_walletTypeTable) {
        if(self.accountArray.count<=3){
            _walletTypeTable=[[UITableView alloc] initWithFrame:CGRectMake(mainWidth-mainWidth/1.5, 50, mainWidth/1.5, 40*self.accountArray.count)];
        }else{
            _walletTypeTable=[[UITableView alloc] initWithFrame:CGRectMake(mainWidth-mainWidth/1.5, 50, mainWidth/1.5, 40*3)];
        }
        _walletTypeTable.dataSource=self;
        _walletTypeTable.delegate=self;
        _walletTypeTable.hidden=YES;
        [self.view addSubview:_walletTypeTable];
    }
    return _walletTypeTable;
}
-(NSMutableArray *)accountArray{
    if (!_accountArray) {
        _accountArray=[NSMutableArray array];
    }
    return _accountArray;
}

#pragma mark-Private
-(void)getWithDrawWallet{
    [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
    @weakify(self);
    [HTTPManager getWithDrawWalletWithType:@"takecash" Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.accountArray removeAllObjects];
            for (NSDictionary *wDic in dic[@"data"]) {
                WithDrawWalletType *type=[WithDrawWalletType mj_objectWithKeyValues:wDic];
                [self.accountArray addObject:type];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}

-(void)setBankCard{
    self.bankField.text=[NSString stringWithFormat:@"%@(尾号%@)",self.parser.bankname,self.parser.bankcard.length>=4?[self.parser.bankcard substringFromIndex:self.parser.bankcard.length-4]:self.parser.bankcard];
}
//手续费提示
-(void)setChargeTips{
    if (self.withDrawRule) {
        //手续费来源
        if ([self.withDrawRule.set.taxtop intValue]>0 &&[self.withDrawRule.set.taxlow intValue]>0 &&([self.withDrawRule.set.taxlow floatValue]==[self.withDrawRule.set.taxtop floatValue])) {
            self.chargeLabel.text=[NSString stringWithFormat:@"说明:提现管理费=%@",self.withDrawRule.set.taxtop];
        }else if ([self.withDrawRule.set.taxtop intValue]>0 &&[self.withDrawRule.set.taxlow intValue]>0) {
            self.chargeLabel.text=[NSString stringWithFormat:@"说明:提现管理费=提现金额*%@%@(最低%@元,最高%@元)",self.withDrawRule.set.tax,@"%",self.withDrawRule.set.taxlow,self.withDrawRule.set.taxtop];
        }else if ([self.withDrawRule.set.taxlow intValue]>0){
            self.chargeLabel.text=[NSString stringWithFormat:@"说明:提现管理费=提现金额*%@%@(最低%@元)",self.withDrawRule.set.tax,@"%",self.withDrawRule.set.taxlow];
        }else{
             self.chargeLabel.text=[NSString stringWithFormat:@"说明:提现管理费=提现金额*%@%@",self.withDrawRule.set.tax,@"%"];
        }
    }
    if ([self.withDrawRule.set.taxtype integerValue]==0) {
        self.chargeLabel.text=[NSString stringWithFormat:@"%@提现管理费另收取",self.chargeLabel.text];
    }
}
//最高提现金额提示
-(void)setMostMoneyLabel{
    //如果maxnum为空  则没有限制
    if ([self.withDrawRule.set.maxnum integerValue]<=0) {
        if ([self.withDrawRule.set.taxtype intValue]==1) {
          self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue];
        }else{
             self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue]/(1+([self.withDrawRule.set.tax floatValue]/100));
        }
    }else{
        //计算最多可提现金额
        if ([self.withDrawRule.set.maxnum floatValue]>=[self.withDrawRule.wallet floatValue]) {
            if ([self.withDrawRule.set.taxtype intValue]==1){
                 self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue];
            }else{
                  self.mostWithDrawMoney=[self.withDrawRule.wallet floatValue]/(1+([self.withDrawRule.set.tax floatValue]/100));
            }
        }else{
              if ([self.withDrawRule.set.taxtype intValue]==1){
                   self.mostWithDrawMoney=[self.withDrawRule.set.maxnum floatValue];
              }else{
                   self.mostWithDrawMoney=[self.withDrawRule.set.maxnum floatValue]/(1+([self.withDrawRule.set.tax floatValue]/100));
              }
        }
    }
    if([self.withDrawRule.set.minnum integerValue]<=0){
        self.leastWithDrawMoney=0;
    }else{
           if ([self.withDrawRule.set.taxtype intValue]==1){
               self.leastWithDrawMoney=[self.withDrawRule.set.minnum floatValue];
           }else{
                  self.leastWithDrawMoney=[self.withDrawRule.set.minnum floatValue]/(1+([self.withDrawRule.set.tax floatValue]/100));
           }
    }
    long int length1=[@"  最多可提现金额：" length];
    long int length2=[NSString stringWithFormat:@"%.2f",self.mostWithDrawMoney].length;
    NSMutableAttributedString *attri=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  最多可提现金额：%.2f",self.mostWithDrawMoney]];
    [attri addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(length1, length2)];
    self.mostMoneyLabel.attributedText=attri;
}
-(void)setDefaultBankCard{
    for (BankCardDataParser *parser in self.bankCardArray) {
        //设置默认银行卡
        if ([parser.is_default integerValue]==1) {
            self.parser=parser;
            self.bankField.text=[NSString stringWithFormat:@"%@(尾号%@)",self.parser.bankname,self.parser.bankcard.length>=4?[self.parser.bankcard substringFromIndex:self.parser.bankcard.length-4]:self.parser.bankcard];
            break;
        }
    }
}

-(void)toVertify{
    RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)setNavBar{
    self.view.backgroundColor=BG_COLOR;
//    self.navigationController.navigationBarHidden =NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"提现记录" action:^{
         @strongify(self);
        WithDrawRecordViewController*vc=[[WithDrawRecordViewController alloc] init];
        vc.wallet_type=@"money";
        [self.navigationController pushViewController:vc animated:YES];
    }];
    self.navigationItem.title=Localized(@"账户提现");
}

-(void)calculatePoundage{
    //判断金额
    if ([self.moneyField.text floatValue]>self.mostWithDrawMoney) {
        [XSTool showToastWithView:self.view Text:@"余额不足"];
        return ;
    }
    if ([self.moneyField.text floatValue]<self.leastWithDrawMoney) {
        [XSTool showToastWithView:self.view Text:@"低于最低提现金额"];
        return;
    }
    //计算手续费
    float poundAge=[self.moneyField.text floatValue]*[self.withDrawRule.set.tax floatValue]/100;//手续费
    if (poundAge<=[self.withDrawRule.set.taxlow floatValue]) {//手续费与最低手续费比较
        poundAge=[self.withDrawRule.set.taxlow floatValue];
    }
    if (poundAge>[self.withDrawRule.set.taxtop floatValue]) {//手续费与最高手续费比较  为0没有限制
        if([self.withDrawRule.set.taxtop integerValue]!=0){
            poundAge=[self.withDrawRule.set.taxtop floatValue];
        }
    }
    if ([self.withDrawRule.set.taxtype intValue]==1) {//提现扣除
        self.realMoneyField.text=[NSString stringWithFormat:@"%.2f",[self.moneyField.text floatValue]-poundAge];
    }else{
         self.realMoneyField.text=[NSString stringWithFormat:@"%.2f",[self.moneyField.text floatValue]];
    }
    self.chargeField.text=[NSString stringWithFormat:@"%.2f",poundAge];
    if ([self.withDrawRule.set.taxtype intValue]==1) {
        self.realAndChargeLabel.text=[NSString stringWithFormat:@"实收%.2f元,含提现管理费%.2f元",[self.moneyField.text floatValue],poundAge];
    }else{
        self.realAndChargeLabel.text=[NSString stringWithFormat:@"实收%.2f元,含提现管理费%.2f元",[self.moneyField.text floatValue]+poundAge,poundAge];
    }
   
}

-(void)commitWithDraw{
    [self.view endEditing:YES];
    //    判断提现信息
    if (self.walletTypeStr.length&&self.moneyField.text.length&&self.parser.bankcard.length) {
        //未设置支付密码
        if ([self.payPassWord isEqualToString:@""]) {
            //弹出警告框
            UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"您还没有设置支付密码，是否要设置支付密码？" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //跳转到设置支付密码界面
                ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
                [self.navigationController pushViewController:oldPayViewController animated:YES];
            }];
            [alertControl addAction:cancelAction];
            [alertControl addAction:okAction];
            [self presentViewController:alertControl animated:YES completion:nil];
        } else {
            //弹出密码输入框
            UIAlertController *pwdAlertVC=[UIAlertController alertControllerWithTitle:Localized(@"input_pay_pwd") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *conAction=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UITextField *pwdField=pwdAlertVC.textFields.firstObject;
                if (!pwdField.text.length) {
                    [XSTool showToastWithView:self.view Text:Localized(@"input_pay_pwd")];
                }else{
                        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                        NSString *tempString = [[NSString alloc]initWithString:[pwdField.text stringByTrimmingCharactersInSet:whiteSpace]];
                        NSString *encryptPassWord = [RSAEncryptor encryptString:tempString];
                        [XSTool showProgressHUDTOView:self.view withText:nil];
                        NSDictionary *dic=@{@"wallet_type":self.walletTypeStr,@"number":self.moneyField.text ,@"bankcardid":self.parser.ID,@"pass2":encryptPassWord};
                        //发送请求
                        @weakify(self);
                    [HTTPManager commitWithDrawWithParamDic:dic Success:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"提现成功"];
                            //进入记录页面
                            WithDrawRecordViewController *record=[[WithDrawRecordViewController alloc] init];
                            record.wallet_type=self.walletType.wallet_flag;
                            [self.navigationController pushViewController:record animated:YES];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }
                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                }
            }];
            UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
            [pwdAlertVC addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.secureTextEntry = YES;
             }];
            [pwdAlertVC addAction:conAction];
            [pwdAlertVC addAction:cancelAction];
            [self presentViewController:pwdAlertVC animated:YES completion:nil];
        }
    } else{
        [XSTool showToastWithView:self.view Text:@"请完善提现信息"];
    }
}

-(void)hideKeyBoard{
    [self.view endEditing:YES];
}
-(void)showBankCards{
    BankCardViewController *bank=[[BankCardViewController alloc] init];
    [self.navigationController pushViewController:bank animated:YES];
}
#pragma mark-TableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    static NSString *cellId=@"cellId";
    static NSString *typeCellId=@"typeCellId";
    if ([tableView isEqual:self.tableView]) {
        if (indexPath.section==0) {
            cell=[[ContainFieldTableViewCell alloc] initWithId:cellId TextLabelText:self.titleArray[indexPath.row] fontSize:17 WithFieldFrame:CGRectMake(100, 0, mainWidth-100-10, 50) Placehold:self.placeHoldArray[indexPath.row] textColor:[UIColor blackColor]  fontSize:13 KeyBoardType:UIKeyboardTypeDecimalPad textAlignMent:NSTextAlignmentRight];
            if (indexPath.row==0) {
                self.walletTypeField=((ContainFieldTableViewCell *)cell).inputField;
                self.walletTypeField.userInteractionEnabled=NO;
                [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showWalletType)]];
            }
            if (indexPath.row==1) {
                self.moneyField=((ContainFieldTableViewCell *)cell).inputField;
                [self.moneyField addTarget:self action:@selector(calculatePoundage) forControlEvents:UIControlEventEditingChanged];
            }
            if (indexPath.row==2) {
                self.realMoneyField=((ContainFieldTableViewCell *)cell).inputField;
                self.realMoneyField.userInteractionEnabled=NO;
            }
            if (indexPath.row==3) {
                self.chargeField=((ContainFieldTableViewCell *)cell).inputField;
                self.chargeField.userInteractionEnabled=NO;
            }
        }else{
            cell=[[ContainFieldTableViewCell alloc] initWithId:cellId TextLabelText:self.titleArray[4] fontSize:17 WithFieldFrame:CGRectMake(mainWidth/2.0, 0, mainWidth/2.0-10, 50) Placehold:self.placeHoldArray[4] textColor:[UIColor blackColor]  fontSize:13 KeyBoardType:UIKeyboardTypeDecimalPad textAlignMent:NSTextAlignmentRight];
            self.bankField=((ContainFieldTableViewCell *)cell).inputField;
            self.bankField.userInteractionEnabled=NO;
            [cell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showBankCards)]];
        }
    }else{
        cell=[tableView dequeueReusableCellWithIdentifier:typeCellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:typeCellId];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        }
        cell.textLabel.text=((WithDrawWalletType *)[self.accountArray objectAtIndex:indexPath.row]).wallet_name;
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        if (section==0) {
            return 4;
        }
        return 1;
    }else{
        return self.accountArray.count;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([tableView isEqual:self.tableView]) {
        return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        if (section==0) {
            return 50;
        }else if (section==1){
            return 70;
        }
        return 0.1;
    }
    return 0.1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        if (section==0) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 50)];
            //添加可提现金额提醒
            self.mostMoneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 50)];
            self.mostMoneyLabel.text=@"  最多可提现金额：";
            self.mostMoneyLabel.font=[UIFont systemFontOfSize:15];
            [view addSubview:self.mostMoneyLabel];
            return view;
        }else{
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 70)];
            self.realAndChargeLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 0, mainWidth-12*2, 30)];
            self.realAndChargeLabel.text=@"实收0.00元,含提现管理费0.00元";
            self.realAndChargeLabel.font=[UIFont systemFontOfSize:15];
            self.realAndChargeLabel.textAlignment=NSTextAlignmentCenter;
            [view addSubview:self.realAndChargeLabel];
            //添加可提现金额提醒
            self.chargeLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 30, mainWidth-12*2, 40)];
            self.chargeLabel.numberOfLines=0;
            self.chargeLabel.text=@"说明:提现管理费=提现金额*0%";
            self.chargeLabel.font=[UIFont systemFontOfSize:15];
            self.chargeLabel.textAlignment=NSTextAlignmentCenter;
            [view addSubview:self.chargeLabel];
            return view;
        }
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {
        return 50;
    }
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.walletTypeTable]) {
        WithDrawWalletType *type=[self.accountArray objectAtIndex:indexPath.row];
        self.walletTypeStr=type.wallet_flag;
        self.walletTypeField.text=type.wallet_name;
        //清空提现金额和手续费
        self.moneyField.text=@"";
        self.realMoneyField.placeholder=@"提现金额-提现管理费";
        self.realMoneyField.text=@"";
        self.chargeField.text=@"";
        self.walletType=[self.accountArray objectAtIndex:indexPath.row];
        self.walletTypeTable.hidden=YES;
        [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
        //获取提现规则
        @weakify(self);
        [HTTPManager getWithDrawRuleWithWalletType:self.walletTypeStr Success:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                WithDrawRule *rule=[WithDrawRule mj_objectWithKeyValues:dic[@"data"]];
                self.withDrawRule=rule;
                [self setMostMoneyLabel];
                [self setChargeTips];
                //设置银行卡数组
                self.bankCardArray=[NSMutableArray arrayWithArray:[BankCardDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"bank"]]];
                //设置默认银行卡
                [self setDefaultBankCard];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }else{
        if (indexPath.section==1) {
            if ([self.view isFirstResponder]) {
                [self.view endEditing:YES];
            }else{
                [self showBankCards];
            }
        }else{
            [self.view endEditing:YES];
        }
    }
}

-(void)showWalletType{
    if (self.walletTypeTable.hidden) {
        self.walletTypeTable.hidden=NO;
    }else{
        self.walletTypeTable.hidden=YES;
    }
}

-(void)allWithDraw{
    self.moneyField.text=[NSString stringWithFormat:@"%.2f",self.mostWithDrawMoney];
    //计算手续费
    [self calculatePoundage];
}

-(void)setUnIdentificationSubviews{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[BankCardDefaultView class]]) {
            [view removeFromSuperview];
        }
    }
    self.defaultView = [[BankCardDefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT)];
    self.defaultView.titleLabel.text = self.titleString;
    self.defaultView.descLabel.text = self.descString;
    self.defaultView.delegate = self;
    [self.view addSubview:self.defaultView];
}
-(void)vertifyAction:(UIButton *)btn{
    RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
}

-(void)getInfo{
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
         @strongify(self);
        if(state){
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord=parser.pay_password;
            }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
         @strongify(self);
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [self.view endEditing:YES];
}
@end
