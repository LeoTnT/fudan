//
//  S_PayTypeController.h
//  App3.0
//
//  Created by apple on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletModel.h"
@interface S_PayTypeController : XSBaseViewController

@property(nonatomic ,strong)NSMutableArray *modelArr;

@property (nonatomic ,copy)void(^S_BackSelected)(WalletPayTypeModel *);
@end

