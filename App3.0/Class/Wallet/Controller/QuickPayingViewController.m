//
//  QuickPayingViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "QuickPayingViewController.h"
#import "MyAllowanceViewController.h"


#define SPACE 10
#define CELL_BHIEGHT 60
@interface QuickPayingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSMutableArray *_shopArray;//商家数组
    float _payMoney;//土豪朋友支付金额
}
@end


@implementation QuickPayingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _shopArray = [NSMutableArray array];
    _payMoney = 0;
    [self getInfo];
    [self setSubviews];
    
}

#pragma mark - 获取信息
-(void)getInfo{

}

-(void)setSubviews{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.showsVerticalScrollIndicator =NO;
    _tableView.sectionHeaderHeight = SPACE;
    _tableView.sectionFooterHeight = 0.1;
    
    [self.view addSubview:_tableView];

}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = @"normalCell";
    NSString *bgStr = @"bgCell";
    
    if (indexPath.section==0) {
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idStr];
        }
        cell.textLabel.text =[NSString stringWithFormat:@"您的土豪朋友已累计替您支付%.2f元",_payMoney] ;
        cell.textLabel.font = [UIFont systemFontOfSize:13];
        cell.textLabel.textColor = mainGrayColor;
        return cell;

    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bgStr];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:bgStr];
        }
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,mainWidth, CELL_BHIEGHT)];
        UIButton *officialBtn = [[UIButton alloc] initWithFrame:CGRectMake(SPACE, SPACE, 10*SPACE, SPACE*2)];
        officialBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        officialBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [officialBtn  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        officialBtn.imageView.frame = CGRectMake(0, 0, SPACE, SPACE);
        [officialBtn setTitle:@"官方商家" forState:UIControlStateNormal];
        [officialBtn setImage:[UIImage imageNamed:@"user_wallet_store"] forState:UIControlStateNormal];
        officialBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(officialBtn.imageView.frame), 0, 0);
        [bgView addSubview:officialBtn];
        [cell.contentView addSubview:bgView];
        return cell;

    
    
    }
   
   
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return _shopArray.count+1;
    }

}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0.1;
    }else{
    return tableView.sectionHeaderHeight;
    
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = @"快速买单记录";
    self.view.backgroundColor = BG_COLOR;
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"我的补贴" action:^{
//        MyAllowanceViewController *allowance = [[MyAllowanceViewController alloc] init];
//        [self.navigationController pushViewController:allowance animated:YES];
        
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
