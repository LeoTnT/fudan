//
//  RechargeViewController.m
//  App3.0
//
//  Created by mac on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RechargeViewController.h"
#import "AppDelegate.h"
#import "S_PayTypeController.h"
#import "RechargeRecordVC.h"
#import "OrderPayViewController.h"
#import "UPPaymentControl.h"
#import "WalletModel.h"
#import "AgreementViewController.h"

@interface RechargeViewController ()<WXApiManagerDelegate>
{
    
    /**提交按钮*/
    UIButton *commitBtn;
    /**展示验证是否成功的文字*/
    UILabel *textLabel;
    /**滑块*/
    RechargeSlider *slider;
    /**金额输入框*/
    UITextField *amountField;
    /**同意协议按钮*/
    UIButton *tipsBtn;
    /**钱包类型表格*/
    UITableView *walletTypeTable;
    
    /**展示支付方式的标签*/
    UILabel *wayLabel;
    /**微信支付的蓝色标记*/
    UIImageView *weixinImageView;
    /**支付宝支付的蓝色标记*/
    UIImageView *zhifubaoImageView;
    /**覆盖变灰的背景*/
    UIView *grayView;
    /**输入框之前的值*/
    NSString *tempTextFeildStr;
}
/**钱包类型字符串数组*/
@property(nonatomic,strong) NSMutableArray *wallet_typeStrArray;

@property(nonatomic,strong) NSMutableArray *payList;

@property (nonatomic, copy) NSString *tradeNumber;//第三方支付时返回的订单编号

@property (nonatomic ,strong) UILabel *rechargeType;    // 充值类型
@property (nonatomic ,strong) UILabel *rechargeWay;     // 种植方式

@property (nonatomic ,copy)NSString *payType;

@end

@implementation RechargeViewController{
    
    
    
    NSDictionary *tempDic;
    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:@"" htlImage:@"" title:@"充值记录" action:^{
        @strongify(self);
        RechargeRecordVC *recordVC=[[RechargeRecordVC alloc] init];
        recordVC.walletType=@"money";
        [self.navigationController pushViewController:recordVC animated:YES];
    }];
//    self.navigationController.navigationBarHidden =NO;
    // 设置子视图
    [self setSubViews];
    self.view.backgroundColor = BG_COLOR;
    //取消导航对滚动式图的影响
    self.edgesForExtendedLayout=UIRectEdgeNone;
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(pushPaySuccessList) name:PAY_SUCCESS_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(toastPayFail) name:PAY_FAIL_LIST object:nil];
    [self getRechargeRule];
}

- (void)toastPayFail {
    [XSTool showToastWithView:self.view Text:@"支付失败"];
}
- (void) pushPaySuccessList {
    NSUInteger returnPage = 0;
    UIViewController *returnController;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[OrderPayViewController class]]) {
            returnController = (OrderPayViewController *)controller;
            returnPage = 1;
            break;
        }
    }
    if (returnPage==1) {
          ((OrderPayViewController *)returnController).isRecharge = YES;
        [self.navigationController popToViewController:returnController animated:YES];
    } else {
        __block NSString *tp;
        [tempDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            
            if ([tempDic[key] isEqualToString:self.rechargeType.text]) {
                
                tp = key;
                return ;
            }
        }];
        RechargeRecordVC *recordVC=[[RechargeRecordVC alloc] init];
        recordVC.walletType=tp;
        [self.navigationController pushViewController:recordVC animated:YES];
    }
}




-(NSMutableArray *)wallet_typeStrArray{
    if (!_wallet_typeStrArray) {
        _wallet_typeStrArray=[NSMutableArray array];
    }
    return _wallet_typeStrArray;
}
#pragma mark-设置子视图
-(void)setSubViews{

    self.navigationItem.title=Localized(@"账户充值");
    NSArray *arr = @[@"充值钱包",Localized(@"充值金额"),Localized(@"充值方式")];
    
    CGFloat leftMargin = 12;
    UILabel *middleLabe,*topLabel,*lasL;
    UILabel *lastLabel;
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 12, mainWidth, 44*3)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    for (NSInteger x = 0; x <3; x ++) {
        
        @autoreleasepool {
            
            UILabel *label = [UILabel new];
            label.textAlignment = NSTextAlignmentLeft;
            label.text = arr[x];
            label.textColor = COLOR_666666;
            label.font = [UIFont systemFontOfSize:14];
            label.frame = lastLabel ?CGRectMake(leftMargin, CGRectGetMaxY(lastLabel.frame), CGRectGetWidth(lastLabel.frame), CGRectGetHeight(lastLabel.frame)): CGRectMake(leftMargin, 0, 150, 44);
            [bgView addSubview:label];
            lastLabel = label;
            
            if (x < 2) {
                UILabel *lineL = [UILabel new];
                lineL.backgroundColor = LINE_COLOR_NORMAL;
                lineL.frame = CGRectMake(leftMargin, CGRectGetMaxY(lastLabel.frame)-0.5, mainWidth-leftMargin, 0.5);
                [bgView addSubview:lineL];
            }
            
            
            switch (x) {
                case 0:topLabel = label;break;
                case 1:middleLabe = label;break;
                case 2:lasL = label;break;
                    
                default:
                    break;
            }
            
            
        }
    }
    
    // 充值钱包类型
    self.rechargeType = [self creatLabel:@"请选择充值钱包类型"];
    [bgView addSubview:self.rechargeType];
    self.rechargeType.frame = CGRectMake(CGRectGetMaxX(topLabel.frame), CGRectGetMinY(topLabel.frame), mainWidth-CGRectGetWidth(topLabel.frame)-2*leftMargin, CGRectGetHeight(topLabel.frame));
    @weakify(self);
    [self.self.rechargeType addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        [self showWalletTypeTable];
    }]];
    
    // 充值金额
    amountField = [UITextField new];
    amountField.placeholder = @"请输入充值金额";
    amountField.textAlignment = NSTextAlignmentRight;
    amountField.keyboardType = UIKeyboardTypeDecimalPad;
    amountField.textColor = COLOR_999999;
    amountField.font = [UIFont systemFontOfSize:14];
    amountField.delegate = self;
    [amountField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [bgView addSubview:amountField];
    amountField.frame = CGRectMake(CGRectGetMaxX(middleLabe.frame), CGRectGetMinY(middleLabe.frame), mainWidth-CGRectGetWidth(middleLabe.frame)-2*leftMargin, CGRectGetHeight(middleLabe.frame));
    
    // 充值方式
    self.rechargeWay = [self creatLabel:@"请选择充值方式"];
    [bgView addSubview:self.rechargeWay];
    self.rechargeWay.frame = CGRectMake(CGRectGetMinX(amountField.frame), CGRectGetMinY(lastLabel.frame), CGRectGetWidth(amountField.frame), CGRectGetHeight(lastLabel.frame));

    S_PayTypeController *payVC =  [S_PayTypeController new];
    [self.rechargeWay addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        payVC.modelArr = self.payList;
        [self.navigationController pushViewController:payVC animated:YES];
    }]];

    [payVC setS_BackSelected:^(WalletPayTypeModel *model) {
        @strongify(self);
        self.rechargeWay.text = model.name;
        self.payType = model.key;
        
        [self changeSliderState];
    }];
    
    // 滑块验证
    slider=[[RechargeSlider alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(bgView.frame)+12, mainWidth-24, 40)];
    slider.continuous = YES;
    slider.maximumValue=100;
    slider.minimumValue=0;
    slider.backgroundColor=[UIColor clearColor];
    
    [slider setMinimumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bg"] forState:UIControlStateHighlighted];
    [slider setMaximumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateHighlighted];
    UIImage *sliderImg=[UIImage imageNamed:@"user_wallet_slider"];
    [slider addTarget:self action:@selector(sliderMove:) forControlEvents:UIControlEventTouchUpInside];
    [slider setThumbImage:sliderImg forState:UIControlStateNormal];
    [self.view addSubview:slider];
    
    textLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(bgView.frame)+12, mainWidth-24, 40)];
    textLabel.layer.cornerRadius=5;
    textLabel.layer.masksToBounds=YES;
    [self.view addSubview:textLabel];
    textLabel.backgroundColor=[UIColor clearColor];
    textLabel.text=@"向右拖动滑块以验证";
    textLabel.textColor=[UIColor whiteColor];
    textLabel.font=[UIFont systemFontOfSize:14];
    textLabel.textAlignment=NSTextAlignmentCenter;
    textLabel.userInteractionEnabled=NO;
    
    //选择或者不选择按钮
    tipsBtn=[[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(slider.frame)+10, 20, 20)];
    [tipsBtn setBackgroundImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    [tipsBtn setBackgroundImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
    [tipsBtn addTarget:self action:@selector(select:) forControlEvents:UIControlEventTouchUpInside];
    tipsBtn.selected=YES;
    [self.view addSubview:tipsBtn];
    
    //协议
    UILabel *tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tipsBtn.frame)+10, CGRectGetMinY(tipsBtn.frame), 200, 20)];
    tipsLabel.textColor=COLOR_999999;
    tipsLabel.userInteractionEnabled=YES;
    tipsLabel.font=[UIFont systemFontOfSize:15];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@"我同意《网银充值服务协议》"];
    [attributedStr addAttribute:NSForegroundColorAttributeName  value:mainColor range:NSMakeRange(3, 10)];
    tipsLabel.attributedText=attributedStr;
    [tipsLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        AgreementViewController *agreeVC=[[AgreementViewController alloc] initWithAgreementType:AgreementTypeRecharge];
        [self.navigationController pushViewController:agreeVC animated:YES];
    }]];
    [self.view addSubview:tipsLabel];

    //提交按钮
    commitBtn= [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(tipsLabel.frame)+48, mainWidth-40, 44)];
    commitBtn.layer.cornerRadius=5;
    [commitBtn setTitle:Localized(@"bug_submit_do") forState:UIControlStateNormal];
    [commitBtn setBackgroundColor:mainColor];
    [commitBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:commitBtn];
    
    
    walletTypeTable=[[UITableView alloc] init];
    walletTypeTable.delegate=self;
    walletTypeTable.dataSource=self;
    walletTypeTable.scrollEnabled=NO;
    walletTypeTable.backgroundColor = [UIColor groupTableViewBackgroundColor];
    walletTypeTable.frame=CGRectMake(mainWidth/2.0, 10, mainWidth/2.0, 0);
    [self.view addSubview:walletTypeTable];
    walletTypeTable.hidden=YES;
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    walletTypeTable.hidden = YES;
    [self.view endEditing:YES];
}
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}
#pragma mark-滑动滑块
-(void)sliderMove:(UISlider *)tempSlider{
    //注意这里要加UIControlStateHightlighted的状态，否则当拖动滑块时滑块将变成原生的控件
    if (tempSlider.value>80) {
        tempSlider.value=100;
        [slider setThumbImage:[UIImage imageNamed:@"user_wallet_slider_success"] forState:UIControlStateNormal];
    }else{
        tempSlider.value=0;
        [slider setThumbImage:[UIImage imageNamed:@"user_wallet_slider"] forState:UIControlStateNormal];
        textLabel.text=@"向右拖动滑块以验证";
        [slider setMinimumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
    }
    if (tempSlider.value==100) {
        textLabel.text=Localized(@"验证成功");
        [slider setMinimumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bg"] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bg"] forState:UIControlStateNormal];
    }
}
#pragma mark-提交
-(void)click:(UIButton *)button{
    if (amountField.text.length == 0) {
        [XSTool showToastWithView:self.view Text:@"请输入充值金额"];
        return;
    }
    if ([self.rechargeType.text isEqualToString:@"请选择充值钱包类型"]) {
        [XSTool showToastWithView:self.view Text:@"请选择充值钱包类型"];
        return;
    }
    if ([self.rechargeWay.text isEqualToString:@"请选择充值方式"]) {
        [XSTool showToastWithView:self.view Text:@"请选择支付方式"];
        return;
    }
    if (slider.value < 100) {
        [XSTool showToastWithView:self.view Text:@"拖动滑块以验证"];
        return;
    }
    
    __block NSString *tp;
    [tempDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        if ([tempDic[key] isEqualToString:self.rechargeType.text]) {
            
            tp = key;
            return ;
        }
    }];
    if (amountField.text.length == 0) return;
    if (self.rechargeType.text.length == 0) return;
    if (tp.length ==0) return;
    
    NSDictionary *dic =@{@"number":amountField.text,
                         @"type":_payType,
                         @"wallet_type":tp};
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager rechargeSubmissionParams:dic succrss:^(NSDictionary * _Nullable dic, resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status == YES){
            NSDictionary *data = dic[@"data"];
            if ([data.allKeys containsObject:@"pay_link"]) {
                //网页支付
                NSString *payLink = [data objectForKey:@"pay_link"];
                XSBaseWKWebViewController *controller = [[XSBaseWKWebViewController alloc] init];
                //                        controller.navigationItem.titl e = self.payType;
                controller.urlStr = payLink;
                controller.urlType = WKWebViewURLNormal;
                [self.navigationController pushViewController:controller animated:YES];
                
            } else {
                 [self payAction:dic[@"data"] type:_payType];
            }
           
        }else{
             [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
       [MBProgressHUD hideHUDForView:self.view animated:YES];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
    
    
}


- (void)popToLastViewController {
    //查看支付状态
    [HTTPManager lookPayResultStatusWithTradeNumber:self.tradeNumber success:^(NSDictionary * _Nullable dic, resultObject *state) {
        NSLog(@"-----%@",dic);
        if (state.status) {
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        [self performSelector:@selector(popViewController) withObject:self afterDelay:1];
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void) popViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doUPPay:(NSDictionary *)dictionary {
    //当获得的tn不为空时，调用支付接口
    NSString *tn = dictionary[@"tn"];
    if (tn != nil && tn.length > 0)
    {
        [[UPPaymentControl defaultControl]
         startPay:tn
         fromScheme:@"xsyUPPay"
         mode:@"00"
         viewController:self];
    }
    
}

- (void)payAction:(NSDictionary *)dic type:(NSString *)type {
    
    self.tradeNumber = dic[@"out_trade_no"];
    
    if ([type isEqualToString:@"UnionPayApp"]) {
        [self doUPPay:dic];
    } else if ([type isEqualToString:@"WeiXinApp"]) {
        [self weChatPay:dic];
    } else if ([type isEqualToString:@"ZhiFuBaoApp"]) {
        [self doAlipayPay:dic];
    }
    

//    [sender isEqualToString:@"WeiXinApp"] ? [self weChatPay:dic]:[self doAlipayPay:dic];
    
    
}

- (void)weChatPay:(NSDictionary *)dic {
    
    if (!([WXApi isWXAppInstalled]&&[WXApi isWXAppSupportApi])) {
        [XSTool showToastWithView:self.view Text:@"尚未安装微信客户端"];
        return;
    }
    payRequestModel *reqModel = [payRequestModel mj_objectWithKeyValues:dic];
    PayReq *request = [[PayReq alloc] init];
    request.openID = reqModel.openID;
    request.partnerId = reqModel.partnerId;
    request.prepayId = reqModel.prepayId;
    request.nonceStr = reqModel.nonceStr;
    request.timeStamp = reqModel.timeStamp;
    request.package = reqModel.package;
    request.sign = reqModel.sign;
    [WXApi sendReq:request];
    
}

- (void)doAlipayPay:(NSDictionary *)dic {
    [[AlipaySDK defaultService]payOrder:dic[@"pay_str"] fromScheme:AliPayScheme callback:^(NSDictionary *resultDic) {
        NSString *title=[NSString stringWithFormat:@"%@",resultDic[@"resultStatus"]];
        
        if ([title isEqualToString:@"9000"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
            return;
        }else if ([title isEqualToString:@"6001"]){
            title = @"您中途取消了订单";
        }else if ([title isEqualToString:@"4000"]){
            title = @"您的订单支付失败";
        }else if ([title isEqualToString:@"6002"]){
            title = @"网络连接出错";
        }else{
            title = @"未知错误";
        }
        [XSTool showToastWithView:self.view Text:title];
    }];

    
    
    
}



-(void)select:(UIButton *)button{
    button.selected=!button.selected;
}

#pragma mark-行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.wallet_typeStrArray.count;
    
}

#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    static NSString *cellID=@"walletTypeCellID";
    cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.text=[self.wallet_typeStrArray objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor=BG_COLOR;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
    
}


#pragma mark-点击cell
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self hideWalletTypeTable];
    
    self.rechargeType.text=[self.wallet_typeStrArray objectAtIndex:indexPath.row];
    
    
}
#pragma mark-隐藏表格
-(void)hideWalletTypeTable{
    walletTypeTable.frame=CGRectMake(mainWidth/2.0, 10, mainWidth/2.0, 0);
    walletTypeTable.hidden=YES;
}
#pragma mark-展示钱包类型表格
-(void)showWalletTypeTable{
    if (walletTypeTable.hidden) {
        [UIView animateWithDuration:.25 animations:^{
            
            walletTypeTable.frame=CGRectMake(mainWidth/2.0, 10, mainWidth/2.0, self.wallet_typeStrArray.count*40);
            walletTypeTable.hidden=NO;
        }];
    }else{
        
        walletTypeTable.frame=CGRectMake(mainWidth/2.0, 10, mainWidth/2.0, 0);
        
        walletTypeTable.hidden=YES;
    }
}
#pragma mark-行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
    
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    
    
    [self changeSliderState];
}

- (void)changeSliderState {
    slider.value=0;
    textLabel.text=@"向右拖动滑块以验证";
    [slider setMinimumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[UIImage imageNamed:@"user_wallet_slider_bgR"] forState:UIControlStateNormal];
}


- (UILabel *)creatLabel:(NSString *)string{
    UILabel *label = [UILabel new];
    label.userInteractionEnabled = YES;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentRight;
    label.text = string;
    label.textColor = COLOR_999999;
    return label;
    
}

//
-(void)getRechargeRule {
    
    [HTTPManager getRechargeRule:^(NSDictionary * _Nullable dic, resultObject *state) {
        
        
        if (state.status) {

            tempDic=[[dic valueForKey:@"data"] valueForKey:@"wallet"];
            
            
            if ([self.wallet_typeStrArray count]>0) {
                
                [self.wallet_typeStrArray removeAllObjects];
            }
            
            NSArray *arr = dic[@"data"][@"payList"];
            NSMutableArray *dataSource = [NSMutableArray array];
            [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                WalletPayTypeModel *model = [WalletPayTypeModel mj_objectWithKeyValues:arr[idx]];
                if (!isEmptyString(model.logo)) {
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.logo]];
                    model.logoImage = [UIImage imageWithData:data];
                }
                [dataSource addObject:model];
            }];
            self.payList = dataSource;
            
            
            [self.wallet_typeStrArray addObjectsFromArray:tempDic.allValues];
            
            
            [walletTypeTable reloadData];
        }else{
            
            
            [MBProgressHUD showMessage:dic[@"info"] view:self.view];
            [XSTool showToastWithView:self.view Text:@"尚未安装微信客户端"];

        }
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD showMessage:NetFailure view:self.view];
        
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    
    
}



- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}
@end

@implementation RechargeSlider
//-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [super touchesEnded:touches withEvent:event];
//
//    if (_touchEnd) {
//        _touchEnd();
//    }
//}

@end
