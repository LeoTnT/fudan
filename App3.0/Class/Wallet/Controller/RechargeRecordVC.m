//
//  RechargeRecordVC.m
//  App3.0
//
//  Created by 孙亚男 on 2018/2/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "RechargeRecordVC.h"
#import "WalletModel.h"
#import "DefaultView.h"
#import "RechargeRecordCell.h"

@interface RechargeRecordVC ()
@property(nonatomic,assign)int page;
@property(nonatomic,strong)NSMutableArray *dataArray;
@property(nonatomic,strong)UIView *topView;
@property(nonatomic,strong)DefaultView *emptyView;
@end

@implementation RechargeRecordVC
#pragma mark-Lazy Loading
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray=[NSMutableArray array];
    }
    return _dataArray;
}
-(DefaultView *)emptyView{
    if (!_emptyView) {
        _emptyView=[[DefaultView alloc] initWithFrame:self.view.bounds];
        _emptyView.titleLabel.text=@"暂无充值记录";
        _emptyView.button.hidden=YES;
        _emptyView.hidden=YES;
        [self.view addSubview:_emptyView];
    }
    return _emptyView;
}
#pragma mark-life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:@"" action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title=@"充值记录";
    self.tableViewStyle=UITableViewStyleGrouped;
    self.showRefreshHeader=YES;
    self.showRefreshFooter=YES;
    [self setRefreshHeader:^{
        @strongify(self);
        [self getNewData];
    }];
    [self setRefreshFooter:^{
        @strongify(self);
        [self getOldData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
-(void)getNewData{
    self.page=1;
    @weakify(self);
    [HTTPManager getRechargeRecordWithDic:@{@"page":@(self.page)} WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        if (state.status) {
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:[RechargeRecordModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"][@"data"]]];
            if (self.dataArray.count) {
                self.emptyView.hidden=YES;
                self.tableView.hidden=NO;
                 [self.tableView reloadData];
            }else{
                self.emptyView.hidden=NO;
                self.tableView.hidden=YES;
            }
        }else{
             [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
-(void)getOldData{
    self.page++;
    @weakify(self);
    [HTTPManager getRechargeRecordWithDic:@{@"page":@(self.page)} WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        if (state.status) {
            [self.dataArray addObjectsFromArray:[RechargeRecordModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"][@"data"]]];
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:state.info];
            self.page--;
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:NetFailure];
        self.page--;
    }];
}
#pragma mark-TableView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 44;
    }else{
        return 0.01;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 12;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"idstr";
    RechargeRecordCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[RechargeRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.model=self.dataArray[indexPath.section];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
        self.topView.backgroundColor=BG_COLOR;
        for (int i=0; i<3; i++) {
            @autoreleasepool {
                CGFloat width = 10*7;
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(14+(width+22)*i, (44-16)/2.0, width, 16)];
                [btn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:12];
                if (i==0) {
                    [btn setTitle:@"正在审核" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review"] forState:UIControlStateNormal];
                    
                }else if(i==1){
                    [btn setTitle:@"审核成功" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review_success"] forState:UIControlStateNormal];
                    
                }else{
                    [btn setTitle:@"审核失败" forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:@"user_review_fail"] forState:UIControlStateNormal];
                }
                btn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(btn.imageView.frame), 0, 0);
                [self.topView addSubview:btn];
            }
        }
        return self.topView;
    }else{
        return [UIView new];
    }
}
@end
