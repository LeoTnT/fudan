//
//  WalletVC.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletVC.h"
#import "WalletTopCell.h"
#import "WalletAccountCell.h"
#import "WalletAppCell.h"
#import "RechargeViewController.h"
#import "BindBankCardViewController.h"
#import "ModifyOldPayViewController.h"
#import "BindBankCardViewController.h"
#import "FinancialRecordViewController.h"
#import "TransferAccountViewController.h"
#import "QuickPayingViewController.h"
#import "FishPondViewController.h"
#import "WalletModel.h"
#import "WalletAccountViewController.h"
#import "WithdrawViewController.h"
#import "RealNameVerificationViewController.h"
#import "UserModel.h"
#import "QRCodeViewController.h"

@interface WalletVC ()<UITableViewDelegate, UITableViewDataSource>
/**购物、积分等类别*/
@property(nonatomic,strong)NSMutableArray *accountArray;
/**购物、积分等高度*/
@property(nonatomic,assign)CGFloat accountCellHeight;
/**存放高度的字典*/
@property(nonatomic,strong) NSMutableDictionary *heightDict;
/**转账类型字典*/
@property(nonatomic,strong)NSDictionary *tradeType;
@property (nonatomic, assign) NSInteger apptove;//审核状态，实名认证

@end

@implementation WalletVC
-(NSMutableDictionary *)heightDict{
    if (_heightDict==nil) {
        _heightDict = [NSMutableDictionary dictionary];
    }
    return _heightDict;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden =YES;
    [self getAccountArray];
}
//-(void) viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden =NO;
//}
-(NSMutableArray *)accountArray{
    if (!_accountArray) {
        _accountArray=[NSMutableArray array];
    }
    return _accountArray;
}

-(void)getAccountArray{
    //获取现金、购物券等类目数组
    @weakify(self);
    [HTTPManager getWalletInfoWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.accountArray removeAllObjects];
            NSDictionary *userWallet=[NSDictionary dictionaryWithDictionary:dic[@"data"][@"userWallet"]];
            NSDictionary *walletConf=[NSDictionary dictionaryWithDictionary:dic[@"data"][@"walletConf"]];
            self.tradeType=walletConf;
            for (int i=0; i<walletConf.allKeys.count; i++) {
                for (int j=0; j<userWallet.allKeys.count; j++) {
                    if ([[walletConf.allKeys objectAtIndex:i] isEqualToString:[userWallet.allKeys objectAtIndex:j]]) {
                        WalletAccount *account=[[WalletAccount alloc] init];
                        account.englishName=[walletConf.allKeys objectAtIndex:i];
                        account.chineseName=[walletConf valueForKey:[walletConf.allKeys objectAtIndex:i]];
                        account.numStr=[userWallet valueForKey:[userWallet.allKeys objectAtIndex:j]];
                        [self.accountArray addObject:account];
                    }
                }
            }
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    // Do any additional setup after loading the view.
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    [self setSubViews];
    [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setSubViews{
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.bounces = NO;
    
    CGFloat height = IS_iPhoneX?45:25;
    
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(10, height, 25, 25)];
    [back setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView addSubview:back];
    [self.tableView reloadData];
}
- (void)backAction
{
//    self.navigationController.navigationBarHidden =NO;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 180;
    }
    else if (indexPath.section == 1) {
        return [[self.heightDict valueForKey:[NSString stringWithFormat:@"%lu",indexPath.section]] floatValue];
    }
    else  {
        return 200;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1 || section == 2 || section == 3) {
        return 10;
    }
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        static NSString *CellIdentifier = @"walletAccountCell";
        WalletAccountCell *cell = (WalletAccountCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[WalletAccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.walletAccountArray=self.accountArray;
        [self.heightDict setValue:@(cell.height) forKey:[NSString stringWithFormat:@"%lu",indexPath.section ]];
        for (UIButton *btn in cell.btnArray) {
            [btn addTarget:self action:@selector(getAccountDetail:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
    }
    else if (indexPath.section == 2) {
        static NSString *CellIdentifier = @"walletAppCell";
        WalletAppCell *cell = (WalletAppCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[WalletAppCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setAreaBtnTapAction:^(NSInteger index) {
                switch (index) {
                    case 0:
                    {
                        RechargeViewController *recharge=[[RechargeViewController alloc] init];
                        [self.navigationController pushViewController:recharge animated:YES];
                    }
                        break;
                    case 1:
                    {
                        [self checkUserApprove_userWithIndexRow:index];
                        
                    }
                        break;
                    case 2:
                    {
                        ModifyOldPayViewController *pay=[[ModifyOldPayViewController alloc] init];
                        [self.navigationController pushViewController:pay animated:YES];
                        
                    }
                        break;
                    case 3:
                    {
                        [self checkUserApprove_userWithIndexRow:index];
                        
                        
                    }
                        break;
                    case 4:
                    {
                        [self checkUserApprove_userWithIndexRow:index];
                        
                    }
                        break;
                    case 5:
                    {
                        QRCodeViewController *qrVC = [[QRCodeViewController alloc] init];
                        qrVC.is_shop = [UserInstance ShardInstnce].userInfo.qrcode_type;
                        qrVC.qrImageString = [UserInstance ShardInstnce].userInfo.qrcode;
                        [self.navigationController pushViewController:qrVC animated:YES];
                        
                    }
                        break;
                    default:
                        break;
                }
            }];
        }
        
        return cell;
    }
    static NSString *CellIdentifier = @"walletTopCell";
    WalletTopCell *cell = (WalletTopCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[WalletTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    for (int i=0; i<self.accountArray.count; i++) {
        WalletAccount *account=[self.accountArray objectAtIndex:i];
        if ([account.englishName isEqualToString:@"money"]) {
            cell.moneyStr=account.numStr;
        }
    }
    return cell;
}

#pragma mark-获取详情
-(void)getAccountDetail:(UIButton *)btn{
    WalletAccount *account=[self.accountArray objectAtIndex:btn.tag-1];
    WalletAccountViewController *vc=[[WalletAccountViewController alloc] init];
    vc.walletType=account.englishName;
    vc.navTitle=[NSString stringWithFormat:@"%@记录",account.chineseName];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"去认证" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
        [self.navigationController pushViewController:vertifyVC animated:YES];
    }];

    UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:action];
    [alertVC addAction:cancelAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)checkUserApprove_userWithIndexRow:(NSInteger)index{
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if([dic[@"status"] isEqual:@1]){
            UserParser *parser = [UserParser mj_objectWithKeyValues:dic];
            self.apptove = [parser.data.approve_user integerValue];
            if (self.apptove==-1) {
                //未提交实名认证
                [self showAlertWithMessage:@"抱歉,您未进行实名认证,暂无法使用该操作"];
            }else if (self.apptove==0){
                //已提交审核中
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:Localized(@"实名认证正在审核中,暂无法使用该操作") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
                [alertVC addAction:action];
                [alertVC addAction:cancelAct];
                [self presentViewController:alertVC animated:YES completion:nil];
                
            }else if (self.apptove==1){
                //审核通过
                [self pushViewControllerWithIndex:index];
                return;
            }else{
                //审核失败
                [self showAlertWithMessage:@"抱歉,您的实名认证审核未通过,暂无法使用该操作"];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}

- (void)pushViewControllerWithIndex:(NSInteger)index{
    if (index==1) {
        BindBankCardViewController *bank=[[BindBankCardViewController alloc] init];
        [self.navigationController pushViewController:bank animated:YES];
    }
    if (index == 3) {
        WithdrawViewController *with=[[WithdrawViewController alloc] init];
        [self.navigationController pushViewController:with animated:YES];
        
    }else if (index == 4){
        TransferAccountViewController *record=[[TransferAccountViewController alloc] init];
        record.tradeType=self.tradeType;
        [self.navigationController pushViewController:record animated:YES];
    }
    
}

@end
