//
//  TransferRecordViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TransferRecordViewController.h"
#import "WalletModel.h"
#import "TransferAccountRecordCell.h"
#import "DefaultView.h"

@interface TransferRecordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *recordArray;
@property(nonatomic,assign)int page;
@property(nonatomic,strong)DefaultView *defaultView;
@property(nonatomic,strong)UIView *topView;
@end

@implementation TransferRecordViewController

-(DefaultView *)defaultView{
    if (!_defaultView) {
        _defaultView = [[DefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _defaultView.titleLabel.text = @"还没有转账记录哦";
        _defaultView.button.hidden=YES;
        _defaultView.baseButton.hidden=YES;
        self.view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_defaultView];
    }
    return _defaultView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    _recordArray=[NSMutableArray array];
    [self setSubviews];
    [self getNewRecord];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)setSubviews{
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = @"转账记录";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
    self.topView.backgroundColor=BG_COLOR;
    for (int i=0; i<3; i++) {
        @autoreleasepool {
            CGFloat width = 10*7;
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(14+(width+22)*i, (44-16)/2.0, width, 16)];
            [btn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:12];
            if (i==0) {
                [btn setTitle:@"正在审核" forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"user_review"] forState:UIControlStateNormal];
                
            }else if(i==1){
                [btn setTitle:@"审核成功" forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"user_review_success"] forState:UIControlStateNormal];
                
            }else{
                [btn setTitle:@"审核失败" forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"user_review_fail"] forState:UIControlStateNormal];
            }
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, -CGRectGetWidth(btn.imageView.frame), 0, 0);
            [self.topView addSubview:btn];
        }
    }
    //表格
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, MAIN_VC_HEIGHT) style:UITableViewStyleGrouped];
    [self.view addSubview:_tableView];
    _tableView.dataSource=self;
    _tableView.delegate=self;
    _tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    _tableView.tableHeaderView=self.topView;
    _tableView.hidden=YES;
    //设置刷新样式
    [self setRefreshStyle];
}
#pragma mark-设置刷新样式
-(void)setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewRecord];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:14];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    _tableView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldRecord];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    _tableView.mj_footer=footer;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark-获取新的网络数据
-(void)getNewRecord{
    self.page=1;
    @weakify(self);
    [HTTPManager getNewTransferAccountRecordWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [_tableView.mj_header endRefreshing];
        if (state.status) {
            TransferAccountRecordTotal *total=[TransferAccountRecordTotal mj_objectWithKeyValues:dic];
            [self.recordArray removeAllObjects];
            [self.recordArray addObjectsFromArray:total.data.list.data];
            if (self.recordArray.count>0) {
                [_tableView reloadData];
                _tableView.hidden=NO;
                self.defaultView.hidden=YES;
            }else{
                self.defaultView.hidden=NO;
                _tableView.hidden=YES;
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [_tableView.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-获取旧的数据
-(void)getOldRecord{
    self.page++;
    @weakify(self);
    [HTTPManager getOldTransferAccountRecordWithPage:@(self.page) Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [_tableView.mj_footer endRefreshing];
        if (state.status) {
             TransferAccountRecordTotal *total=[TransferAccountRecordTotal mj_objectWithKeyValues:dic];
             [self.recordArray addObjectsFromArray:total.data.list.data];
            if (total.data.list.data.count>0) {
                [_tableView reloadData];
            }else{
                [XSTool showToastWithView:self.view Text:@"暂无更多记录"];
            }
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } fail:^(NSError *error) {
        @strongify(self);
        [_tableView.mj_footer endRefreshing];
        self.page--;
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"cellId";
    TransferAccountRecordCell *cell;
    cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[TransferAccountRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.record=[self.recordArray objectAtIndex:indexPath.section];
    return cell;
}
#pragma mark-行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _recordArray.count;
}
#pragma mark-设置高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TransferAccountRecord *record=_recordArray[indexPath.section];
    if (record.c_remark.length) {
        return 232;
    }else{
        return 200;
    }
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
