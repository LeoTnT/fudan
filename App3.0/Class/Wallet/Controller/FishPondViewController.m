//
//  FishPondViewController.m
//  App3.0
//
//  Created by mac on 2017/3/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FishPondViewController.h"
#import "FishPondTopCell.h"
@interface FishPondViewController ()
{
    /**表格*/
    UITableView *table;
}
@end

@implementation FishPondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //取消导航对滚动式图的影响
    self.edgesForExtendedLayout=UIRectEdgeNone;
    //设置子视图
    [self setSubViews];
}
#pragma mark-设置子视图
-(void)setSubViews{
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title=@"收到的鱼";
    //表格  位置不是太对
    table=[[UITableView alloc] initWithFrame:CGRectMake(0, -navBarHeight, mainWidth, mainHeight) style:UITableViewStyleGrouped];
    table.delegate=self;
    table.dataSource=self;
    [self.view addSubview:table];
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return 0;
    }
}
#pragma mark-设置行数
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (indexPath.row==0) {
        static NSString *topCellID=@"top";
        cell=[tableView dequeueReusableCellWithIdentifier:topCellID];
        if (!cell) {
            cell=[[FishPondTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topCellID];
        }
    }
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
