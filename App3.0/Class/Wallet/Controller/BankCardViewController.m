//
//  BankCardViewController.m
//  App3.0
//
//  Created by mac on 2017/4/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "BankCardViewController.h"
#import "AddBankCardViewController.h"
#import "BankTableViewCell.h"
#import "WithdrawViewController.h"

@interface BankCardViewController ()
@property(nonatomic,strong)NSMutableArray *bankCardArray;//银行卡数组
@property(nonatomic,strong)UITableView *bankCardTable;//银行卡表格
@end

@implementation BankCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
}
#pragma mark-设置子视图
-(void)setSubViews{
    self.view.backgroundColor=BG_COLOR;

    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"添加") action:^{
        @strongify(self);
        AddBankCardViewController *add=[[AddBankCardViewController alloc] init];
        add.isAdd=YES;
        [self.navigationController pushViewController:add animated:YES];
    }];
    self.navigationItem.title=Localized(@"选择银行卡");
    //创建表格
    _bankCardTable=[[UITableView alloc] initWithFrame:CGRectMake(0,0, mainWidth,mainHeight)style:UITableViewStylePlain];
    _bankCardTable.hidden=YES;
    [self.view addSubview:_bankCardTable];
    _bankCardTable.delegate=self;
    _bankCardTable.dataSource=self;
   _bankCardTable.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden =NO;
    //获取银行卡信息
    [self getBankCardInfo];
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bankCardArray.count;
}
#pragma mark-设置行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = @"bankCell";
    BankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idStr];
    if (cell==nil) {
        cell = [[BankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
//    cell.bankDelegate = self;
    if (indexPath.row%3==0) {
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"45B157"];
    } else if (indexPath.row%3==1) {
        
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"4C9DC1"];
    } else {
        
        cell.backView.backgroundColor = [UIColor hexFloatColor:@"eb8d4f"];
    }
    cell.parser = self.bankCardArray[indexPath.row];
    
    
    cell.isShow = NO;
    return cell;

}
#pragma mark-获取银行卡信息
-(void)getBankCardInfo{
    [XSTool showToastWithView:self.view Text:@"正在获取数据"];
    @weakify(self);
    [HTTPManager getBindBankCardInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
           self.bankCardArray = [NSMutableArray arrayWithArray:[BankCardDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"][@"list"]]];
            if (self.bankCardArray.count==0) {
                self.bankCardTable.hidden=YES;
                [XSTool showToastWithView:self.view Text:@"暂时没有绑定银行卡信息"];
            }else{
                self.bankCardTable.hidden=NO;
                
                [self.bankCardTable reloadData];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}
#pragma mark-点击某行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BankCardDataParser *parser=[_bankCardArray objectAtIndex:indexPath.row];
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[WithdrawViewController class]]) {
            ((WithdrawViewController *)vc).parser=parser;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
