//
//  S_PayTypeController.m
//  App3.0
//
//  Created by apple on 2017/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "S_PayTypeController.h"
#import "S_BankCell.h"

@interface S_PayTypeController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)NSMutableArray *nameArr,*dataArray;

//@property (nonatomic ,strong)NSMutableDictionary *allDic;
@end



@implementation S_PayTypeController
{
    NSArray *imageDataSource;
}

static NSString *cellIdentifier = @"S_PayTypeController";
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView=[UIView new];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


- (NSMutableArray *)nameArr{
    if (!_nameArr) {
        _nameArr = [NSMutableArray array];
    }
    return _nameArr;
}
-(void)setModelArr:(NSMutableArray *)modelArr {
    
    _modelArr = modelArr;
    if (_modelArr == modelArr) return;
    
//    NSLog(@"_modelArr =%@",modelArr);
    
    
    
//    NSLog(@"key =%@",aa);
//    if (self.nameArr) {
//        [self.nameArr removeAllObjects];
//    }
//
// [aa enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//    NSString *ke = [NSString stringWithFormat:@"%@",aa[idx]];
//         NSDictionary *ssss = modelArr[ke];
//     NSMutableDictionary *allDic = [NSMutableDictionary dictionary];
//     [allDic setDictionary:ssss];
//     [allDic setValue:ke forKey:@"payName"];
//     [self.nameArr addObject:allDic];
//
//     
// }];
//    
    [self.tableView reloadData];
    
    NSLog(@"self.nameArr cc%lu",(unsigned long)self.nameArr.count);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageDataSource = @[@"user_wallet_wxpay",@"user_wallet_alipay",@"user_wallet_uppay"];
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    _dataArray = [NSMutableArray array];
    for (int i = 0; i<10; i++) {
        S_BankModel *model = [[S_BankModel alloc] init];
        model.selectStatus = NO; //全部初始为未选中状态
        [_dataArray addObject:model];
    }
    self.navigationItem.title = Localized(@"recharge_wallet_pay_select");
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = [UIView new];

}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.modelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    S_BankCell *cell = [S_BankCell cellWithTableView:tableView];
    
    WalletPayTypeModel *model = self.modelArr[indexPath.row];
    
    cell.textLabel.text =model.name;
    cell.detailTextLabel.text = model.memo;
    NSString *imageName;
    if ([cell.textLabel.text isEqualToString:@"微信app支付"]) {
        imageName = imageDataSource[0];
        cell.imageView.image = [UIImage imageNamed:imageName];
    }else if ([cell.textLabel.text isEqualToString:@"支付宝app支付"]){
        imageName = imageDataSource[1];
        cell.imageView.image = [UIImage imageNamed:imageName];
    }else if ([cell.textLabel.text containsString:@"银联"]){
        imageName = imageDataSource[2];
        
    }else{
    
        cell.imageView.image = model.logoImage;
        
    }
    
    if (!isEmptyString(imageName)) cell.imageView.image = [UIImage imageNamed:imageName];
    
//    S_BankModel *model = _dataArray[indexPath.row];
//    
//    [cell setContentWithModel:model]; //默认不显示控件
    
//    cell.selectBtn.tag = 1000+indexPath.row;
//    [cell.selectBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)selectAction:(UIButton *)selectBtn {
    NSInteger index = selectBtn.tag-1000;
    //改变数据源
//    for (int i = 0; i<_dataArray.count; i++) {
//        S_BankModel *model = _dataArray[i];
//        if (i==index) {
//            model.selectStatus = YES;
//        } else {
//            model.selectStatus = NO;
//        }
//    }
//    
//    [self selectAction:btn];
    
    if (_S_BackSelected) {
        
        _S_BackSelected(self.modelArr[index]);
        
    }
    [self.navigationController popViewControllerAnimated:YES];
    //刷新tableView
//    [_tableView reloadData];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UIButton *btn = [self.view viewWithTag:1000+indexPath.row];
//    [self selectAction:btn];
    
    if (_S_BackSelected) {
        
        _S_BackSelected(self.modelArr[indexPath.row]);
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


@end
