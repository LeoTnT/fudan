//
//  WalletAccountViewController.m
//  App3.0
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletAccountViewController.h"
#import "WalletModel.h"
#import "WalletAccountRecordCell.h"
#import "MJRefresh.h"
#import "WalletVC.h"
#import "DefaultView.h"
#import "GrayBackView.h"

@interface WalletAccountViewController ()<UITableViewDelegate,UITableViewDataSource,GrayBackViewDelegate>
/**数据源*/
@property(nonatomic,strong)NSMutableArray *dataArray;
/**表格*/
@property(nonatomic,strong)UITableView *table;
/**收入支出类型字典*/
@property(nonatomic,strong)NSDictionary *tradeTypeDic;
/**数据的页数*/
@property(nonatomic,assign)int page;
/**高度字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
/**选择交易类型表格*/
@property(nonatomic,strong)UITableView *tradeView;
/**灰色背景*/
@property(nonatomic,strong)GrayBackView *grayView;
/**交易类型数组*/
@property(nonatomic,strong)NSMutableArray *tradeTypeArray;
/**交易类型ID*/
@property(nonatomic,strong)TradeType *tradeType;
@property (strong, nonatomic) DefaultView *emptyView;
@end

@implementation WalletAccountViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self setSubViews];
    [self setTableRefreshStyle];
    [self.table.mj_header beginRefreshing];
}

-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}


-(GrayBackView *)grayView{
    if (!_grayView) {
        _grayView=[[GrayBackView alloc] init];
        _grayView.hidden=YES;
        _grayView.delegate=self;
        UILabel *chooseLabel=[[UILabel alloc] initWithFrame:CGRectMake((mainWidth-mainWidth/1.5)/2.0, (mainHeight-200)/2.0, mainWidth/1.5, 40)];
        chooseLabel.backgroundColor=TAB_SELECTED_COLOR;
        chooseLabel.text=Localized(@"choose");
        chooseLabel.textColor=[UIColor whiteColor];
        chooseLabel.textAlignment=NSTextAlignmentCenter;
        [_grayView addSubview:chooseLabel];
        _tradeView=[[UITableView alloc] initWithFrame:CGRectMake((mainWidth-mainWidth/1.5)/2.0, (mainHeight-200)/2.0+40, mainWidth/1.5, 160)];
        _tradeView.delegate=self;
        _tradeView.dataSource=self;
        [_tradeView flashScrollIndicators];
        _tradeView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
        [_grayView addSubview:_tradeView];
    }
    return _grayView;
}
-(NSMutableArray *)tradeTypeArray{
    if (!_tradeTypeArray) {
        _tradeTypeArray=[NSMutableArray array];
    }
    return _tradeTypeArray;
}
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray=[NSMutableArray array];
    }
    return _dataArray;
}
#pragma mark-获取新的数据
-(void)getNewData{
    self.page=1;
    NSDictionary *paraDic=@{@"wallet_type":self.walletType?self.walletType:@"money",@"page":@(self.page),@"limit":@"20"};
    if(self.tradeType){
        self.navigationItem.title=[NSString stringWithFormat:@"%@记录",self.tradeType.name];
        paraDic=@{@"wallet_type":self.walletType?self.walletType:@"money",@"page":@(self.page),@"limit":@20,@"leixing":self.tradeType.key};
    }else{
        self.navigationItem.title=self.navTitle;
    }
    //获取新数据
    @weakify(self);
    [HTTPManager getAccountDetailWithParamDic:paraDic WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.table.mj_header endRefreshing];
        if (state.status) {
            WalletAccountClass *account=[WalletAccountClass mj_objectWithKeyValues:dic];
            if (!self.tradeType) {
                NSDictionary *tradTypeDic=account.data.trade_type;
                NSArray *keys=tradTypeDic.allKeys;
                [self.tradeTypeArray removeAllObjects];
                for (NSString *key in keys) {
                    TradeType *type=[TradeType mj_objectWithKeyValues:@{@"key":key,@"name":tradTypeDic[key]}];
                    [self.tradeTypeArray addObject:type];
                }
            }
            if(account.data.list.data.count==0){
                self.view.backgroundColor=[UIColor whiteColor];
                self.emptyView.hidden = NO;
            }else{
                self.emptyView.hidden = YES;
                [self.dataArray removeAllObjects];
                [self.dataArray addObjectsFromArray:account.data.list.data];
                self.tradeTypeDic=account.data.trade_type;
                //刷新表格
                [self.table reloadData];
                self.view.backgroundColor=BG_COLOR;
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.table.mj_header endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-设置子视图
-(void)setSubViews{
    //    self.navigationController.navigationBarHidden =NO;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        
        for (UIViewController *vc in self.navigationController.viewControllers) {
            
            if ([vc isKindOfClass:[NSClassFromString(@"GrabEedEnvelopeController") class]] ||[vc isKindOfClass:[NSClassFromString(@"RechargeViewController") class]]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                return ;
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"筛选" action:^{
        @strongify(self);
        [self alertChooseTypeView];
    }];
    self.navigationItem.title=self.navTitle;
    self.table=[[UITableView alloc] initWithFrame:CGRectZero];
    self.table.delegate=self;
    self.table.dataSource=self;
    //    self.table.hidden=YES;
    [self.view addSubview:self.table];
    [self.table mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    self.view.backgroundColor=BG_COLOR;
    self.table.backgroundColor=[UIColor whiteColor];
    self.table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.tradeType=nil;
    // 默认页
    self.emptyView = [[DefaultView alloc] initWithFrame:self.view.bounds];
    self.emptyView.button.hidden = YES;
    self.emptyView.titleLabel.text = @"暂无更多记录";
    self.emptyView.hidden=YES;
    [self.view addSubview:self.emptyView];
}

#pragma mark-下拉刷新、上拉加载
-(void)setTableRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:14];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.table.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldData];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    self.table.mj_footer=footer;
}
#pragma mark-获取旧的数据
-(void)getOldData{
    self.page++;
    NSDictionary *paraDic=@{@"wallet_type":self.walletType?self.walletType:@"money",@"page":@(self.page),@"limit":@20};
    if(self.tradeType){
        paraDic=@{@"wallet_type":self.walletType?self.walletType:@"money",@"page":@(self.page),@"limit":@20,@"leixing":self.tradeType.key};
    }
    //获取新数据
    @weakify(self);
    [HTTPManager getAccountDetailWithParamDic:paraDic WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.table.mj_footer endRefreshing];
        if(state.status){
            WalletAccountClass *account=[WalletAccountClass mj_objectWithKeyValues:dic];
            if (account.data.list.data.count==0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多记录"];
            }else{
                [self.dataArray addObjectsFromArray:account.data.list.data];
                self.tradeTypeDic=account.data.trade_type;
                //刷新表格
                [self.table reloadData];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            self.page--;
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.table.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        self.page--;
    }];
}
#pragma mark-表格的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.table]) {
        return self.dataArray.count;
    }else{
        return self.tradeTypeArray.count+1;
    }
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if ([tableView isEqual:self.table]) {
        static NSString *cellId=@"tableCellId";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[WalletAccountRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        ((WalletAccountRecordCell *)cell).record=self.dataArray[indexPath.row];
        ((WalletAccountRecordCell *)cell).tradeType=self.tradeTypeDic;
        self.heightDic[@(indexPath.row)]=@(((WalletAccountRecordCell *)cell).height);
    }else{
        static NSString *cellId=@"tradeCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        if(indexPath.row==0){
            cell.textLabel.text=@"全部记录";
        }else{
            cell.textLabel.text=[self.tradeTypeArray[indexPath.row-1] name];
        }
        [cell.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refreshData:)]];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark-行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.table]&&self.heightDic[@(indexPath.row)]) {
        return [self.heightDic[@(indexPath.row)] floatValue];
    }else  if([tableView isEqual:self.table]){
        return 100;
    }
    return 40;
}
-(void)refreshData:(UITapGestureRecognizer *)tap{
    self.grayView.hidden=YES;
    UITableViewCell *cell=(UITableViewCell *)tap.view.superview;
    NSIndexPath *indexPath=[self.tradeView indexPathForCell:cell];
    if (indexPath.row==0) {
        self.tradeType=nil;
    }else{
        self.tradeType=self.tradeTypeArray[indexPath.row-1];
    }
    //重新刷新表格
    [self.table.mj_header beginRefreshing];
    [self getNewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)alertChooseTypeView{
    //保证只请求一次
    if(self.tradeTypeArray.count){
        self.grayView.hidden=NO;
        [self.tradeView reloadData];
    }else{
        self.grayView.hidden=YES;
    }
}
#pragma mark-GrayBackViewDelegate
-(void)grayViewClicked{
    self.grayView.hidden=YES;
}
@end
