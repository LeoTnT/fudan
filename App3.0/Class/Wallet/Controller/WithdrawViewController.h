//
//  WithdrawViewController.h
//  App3.0
//
//  Created by mac on 2017/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardModel.h"
@interface WithdrawViewController : XSBaseViewController
@property(nonatomic,strong)BankCardDataParser *parser;//银行卡信息
-(void)getInfo;
@end
