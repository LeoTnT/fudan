//
//  TransferAccountViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TransferAccountViewController.h"
#import "XSCustomButton.h"
#import "TransferRecordViewController.h"
#import "WalletModel.h"
#import "UserModel.h"
#import "RealNameVerificationViewController.h"
#import "RSAEncryptor.h"
#import "PersonViewController.h"
#import "ModifyOldPayViewController.h"
#import "BankCardDefaultView.h"
#import "ContainFieldTableViewCell.h"


@interface TransferAccountViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,BankCardDefaultViewDelegate>
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)UITextField *shiftAccount;//转入账号
@property(nonatomic,strong)UITextField *changeMoney;//转账金额
@property(nonatomic,strong)UITextField *handlingCharge;//手续费
@property(nonatomic,strong)UITextView *remark;//备注
@property(nonatomic,strong)UILabel *tintLabel;//提示可以输入的字数
@property(nonatomic,strong)UITableView *transferMethodTable;//转账方式
@property(nonatomic,strong) UITableView *transferTypeTable;//转账类型
@property(nonatomic,strong)UITextField *methodField;//转账方式标签
@property(nonatomic,strong)UITextField *typeField;//转账类型标签
@property(nonatomic,strong) NSMutableArray *methodStrArray;//转账类型数组
@property(nonatomic,strong)NSMutableArray *rulesArray;//转账规则数组
@property(nonatomic,strong)NSMutableArray *typeStrArray;//转账类型字符串数组
@property(nonatomic,strong)TransferAccountRule *rule;//当前对应的转账规则
@property(nonatomic,strong)NSDictionary *walletDic;//当前钱包
@property(nonatomic,copy)NSString *walletBalance;//当前余额
@property(nonatomic,assign)NSUInteger apptove;//实名认证类型
@property(nonatomic,strong)XSCustomButton *vertifyBtn;//实名认证按钮
@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descString;
@property (nonatomic, copy) NSString *payPassWord;
@property(nonatomic,strong)BankCardDefaultView *defaultView;
@property(nonatomic,strong)XSCustomButton *submitBtn;
@property(nonatomic,strong)NSArray *placeHoldArray;
@property(nonatomic,assign)float transferleastMoney;
@property(nonatomic,assign)float transferMostMoney;
@end

@implementation TransferAccountViewController
#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.submitBtn];
    //获取转账规则
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getTransferRulesWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            [self.methodStrArray removeAllObjects];
            TransferAccountTotal *total=[TransferAccountTotal mj_objectWithKeyValues:dic];
            self.rulesArray=[NSMutableArray array];;
            self.walletDic=dic[@"data"][@"wallet"];
            //获取方式字符串
            for (TransferAccountRule *tempRule in total.data.set) {
                if ([self.tradeType.allKeys containsObject:tempRule.bank] && [self.tradeType.allKeys containsObject:tempRule.tobank]) {
                    NSMutableString *str=[NSMutableString string];
                    for (NSString *englishName in self.tradeType.allKeys) {
                        if ([tempRule.bank isEqualToString:englishName]) {
                            [str appendString:[NSString stringWithFormat:@"%@转",[self.tradeType valueForKey:tempRule.bank]]];
                        }
                    }
                    for(NSString *englishName in self.tradeType.allKeys) {
                        if ([tempRule.tobank isEqualToString:englishName]) {
                            [str appendString:[self.tradeType valueForKey:tempRule.tobank]];
                        }
                    }
                    [self.methodStrArray addObject:str];
                    [self.rulesArray addObject:tempRule];
                }
            }
            
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    [self getInfo];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Lazy Loading
-(NSArray *)titleArray{
    if (!_titleArray) {
        _titleArray = @[@"转账方式",@"转账类型",@"转入账号",@"转账金额",Localized(@"sxf"),Localized(@"note")];
    }
    return _titleArray;
}
-(NSArray *)placeHoldArray{
    if (!_placeHoldArray) {
        _placeHoldArray=@[@"转账方式",@"转账类型",@"收款账号(转给自己可以忽略)",@"可用余额0.00元",@"费用=转账金额*0.00%"];
    }
    return _placeHoldArray;
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 45*7+180) style:UITableViewStyleGrouped];
        _tableView.scrollEnabled=NO;
        _tableView.backgroundColor = BG_COLOR;
        _tableView.sectionHeaderHeight = 10;
        _tableView.sectionFooterHeight = 0.1;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
-(XSCustomButton *)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [[XSCustomButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.tableView.frame)-50, mainWidth-2*10, 50) title:Localized(@"bug_submit_do") titleColor:[UIColor whiteColor] fontSize:20 backgroundColor:mainColor higTitleColor:[UIColor whiteColor] highBackgroundColor:HighLightColor_Main];
        [_submitBtn setBorderWith:0 borderColor:[mainColor CGColor] cornerRadius:5];
        [_submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_submitBtn];
    }
    return _submitBtn;
}
-(UITableView *)transferMethodTable{
    if (!_transferMethodTable) {
        if (self.methodStrArray.count<=5) {
              _transferMethodTable=[[UITableView alloc] initWithFrame:CGRectMake(mainWidth-mainWidth/1.5, 10, mainWidth/1.5, 45*self.methodStrArray.count)];
        }else{
         _transferMethodTable=[[UITableView alloc] initWithFrame:CGRectMake(mainWidth-mainWidth/1.5, 10, mainWidth/1.5, 45*5)];
        }
        _transferMethodTable.hidden=YES;
        _transferMethodTable.dataSource=self;
        _transferMethodTable.delegate=self;
        _transferMethodTable.showsVerticalScrollIndicator=NO;
        [self.view addSubview:_transferMethodTable];
    }
    return _transferMethodTable;
}

-(NSMutableArray *)methodStrArray{
    if (!_methodStrArray) {
        _methodStrArray=[NSMutableArray array];
    }
    return _methodStrArray;
}
-(NSMutableArray *)typeStrArray{
    if (!_typeStrArray) {
        _typeStrArray=[NSMutableArray array];
    }
    return _typeStrArray;
}
-(UITableView *)transferTypeTable{
    if (!_transferTypeTable) {
        _transferTypeTable=[[UITableView alloc] initWithFrame:CGRectMake(mainWidth-mainWidth/1.5, 10+45, mainWidth/1.5, 0)];
        _transferTypeTable.hidden=YES;
        _transferTypeTable.dataSource=self;
        _transferTypeTable.delegate=self;
        _transferTypeTable.showsVerticalScrollIndicator=NO;
        [self.view addSubview:_transferTypeTable];
    }
    return _transferTypeTable;
}
#pragma mark-Private
-(void)setNavBar{
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = Localized(@"账户转账");
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:@"转账记录" action:^{
         @strongify(self);
        //转账记录vc
        TransferRecordViewController *recordVC = [[TransferRecordViewController alloc] init];
        [self.navigationController pushViewController:recordVC animated:YES];
    }];
  }
-(void)keyboardWillShow:(NSNotification *)notification
{
    if([self.remark isFirstResponder] && mainHeight<600){
        float tableMaxHeight=45*7+180;
        [UIView animateWithDuration:0.25 animations:^{
            self.tableView.frame=CGRectMake(0, -45*5, mainWidth, 45*7+180);
            self.submitBtn.frame=CGRectMake(10, tableMaxHeight-50-45*5-20, mainWidth-2*10, 50);
        }];
    }
}
-(void)keyboardDidShow{
    if([self.remark isFirstResponder] && mainHeight<600){
         float tableMaxHeight=45*7+180;
        [UIView animateWithDuration:0.25 animations:^{
            self.tableView.frame=CGRectMake(0, -45*5, mainWidth, 45*7+180);
            self.submitBtn.frame=CGRectMake(10, tableMaxHeight-50-45*5-20, mainWidth-2*10, 50);
        }];
    }
}
-(void)keyboardWillHidden:(NSNotification *)notification
{
    [UIView animateWithDuration:0.25 animations:^{
        self.tableView.frame=CGRectMake(0, 0, mainWidth, 45*7+180);
        self.submitBtn.frame=CGRectMake(10, CGRectGetMaxY(self.tableView.frame)-50, mainWidth-2*10, 50);
    }];
}

-(void)getInfo{
    @weakify(self);
    [HTTPManager getUserInfoSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        if(state){
            UserDataParser *parser = [UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            self.payPassWord=parser.pay_password;
            }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } failure:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}

-(void)setUnIdentificationSubviews{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[BankCardDefaultView class]]) {
            [view removeFromSuperview];
        }
    }
    self.defaultView = [[BankCardDefaultView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    self.defaultView.titleLabel.text = self.titleString;
    self.defaultView.descLabel.text = self.descString;
    self.defaultView.delegate = self;
    [self.view addSubview:self.defaultView];
}
-(void)vertifyAction:(UIButton *)btn{
    RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)submitAction:(UIButton *) sender{
    [self.view endEditing:YES];
    BOOL isPerfect=NO;
    NSString *typeStr=self.typeField.text;
    if (typeStr.length<=0) {
        isPerfect=NO;
    } else if ([typeStr isEqualToString:@"转给自己"]) {
        if (self.methodField.text.length&&self.changeMoney.text.length){
            isPerfect=YES;
        }
    }else{
        if (self.methodField.text.length&&self.shiftAccount.text.length&&self.changeMoney.text.length){
            isPerfect=YES;
        }
    }
    if (isPerfect) {
        [self.view endEditing:YES];
        //未设置支付密码
        if (!self.payPassWord.length) {
            //弹出警告框
            UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"您还没有设置支付密码，是否要设置支付密码？" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //跳转到设置支付密码界面
                ModifyOldPayViewController *oldPayViewController = [[ModifyOldPayViewController alloc] init];
                [self.navigationController pushViewController:oldPayViewController animated:YES];
                
            }];
            [alertControl addAction:cancelAction];
            [alertControl addAction:okAction];
            [self presentViewController:alertControl animated:YES completion:nil];
        } else {
            //弹出密码输入框
            UIAlertController *pwdAlertVC=[UIAlertController alertControllerWithTitle:Localized(@"input_pay_pwd") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *conAction=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UITextField *pwdField=pwdAlertVC.textFields.firstObject;
                if (!pwdField.text.length) {
                    [XSTool showToastWithView:self.view Text:Localized(@"input_pay_pwd")];
                }else{
                    NSString *encryptOldPassWord = [RSAEncryptor encryptString:pwdField.text];
                    NSDictionary *dic;
                    if ([typeStr isEqualToString:@"转给自己"]) {
                        dic=@{@"number":[NSNumber numberWithDouble:[self.changeMoney.text doubleValue]],@"pass2":encryptOldPassWord,@"transfer_set":self.rule.id,@"transfer_to":@1,@"remark":self.remark.text};
                    }else{
                        dic=@{@"number":[NSNumber numberWithDouble:[self.changeMoney.text doubleValue]],@"pass2":encryptOldPassWord,@"transfer_set":self.rule.id,@"transfer_to":@2,@"new_userid":self.shiftAccount.text,@"remark":self.remark.text};
                    }
                    //提交
                    [XSTool showProgressHUDTOView:self.view withText:nil];
                    @weakify(self);
                    [HTTPManager commitTransAccountWithParamDic:dic WithSuccess:^(NSDictionary *dic, resultObject *state) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        if (state.status) {
                            [XSTool showToastWithView:self.view Text:@"转账成功"];
                            TransferRecordViewController *recordVC = [[TransferRecordViewController alloc] init];
                            [self.navigationController pushViewController:recordVC animated:YES];
                        }else{
                            [XSTool showToastWithView:self.view Text:dic[@"info"]];
                        }

                    } fail:^(NSError *error) {
                        @strongify(self);
                        [XSTool hideProgressHUDWithView:self.view];
                        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                    }];
                }
            }];
            UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
            [pwdAlertVC addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                textField.secureTextEntry = YES;
            }];
            [pwdAlertVC addAction:conAction];
            [pwdAlertVC addAction:cancelAction];
            [self presentViewController:pwdAlertVC animated:YES completion:nil];
        }
    }else{
        [XSTool showToastWithView:self.view Text:@"请完善转账信息"];
    }
}
#pragma mark - TableViewDelegate
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static  NSString *methodIdStr=@"methodCellStr";
    static NSString *typeIdStr=@"typeCellStr";
    static NSString *idStr1=@"cellStr1";
    UITableViewCell *cell;
    if ([tableView isEqual:_transferMethodTable]){
        cell= [tableView dequeueReusableCellWithIdentifier:methodIdStr];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:methodIdStr];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        }
        cell.textLabel.text=[_methodStrArray objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
    }else if([tableView isEqual:_transferTypeTable]){
        cell= [tableView dequeueReusableCellWithIdentifier:typeIdStr];
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:typeIdStr];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        }
        cell.textLabel.text=[_typeStrArray objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
    }else{
        if (indexPath.row<5) {
            cell=[[ContainFieldTableViewCell alloc] initWithId:idStr1 TextLabelText:self.titleArray[indexPath.row] fontSize:17 WithFieldFrame:CGRectMake(10*9, 0, mainWidth-10*10, 45) Placehold:self.placeHoldArray[indexPath.row] textColor:[UIColor blackColor] fontSize:13 KeyBoardType:UIKeyboardTypeDecimalPad textAlignMent:NSTextAlignmentRight];
        
        if(indexPath.row==0){
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMethodTable)];
            [cell addGestureRecognizer:tap];
            self.methodField=((ContainFieldTableViewCell *)cell).inputField;
            self.methodField.userInteractionEnabled=NO;
        }
        if (indexPath.row==1) {
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTypeTable)];
            [cell addGestureRecognizer:tap];
            self.typeField=((ContainFieldTableViewCell *)cell).inputField;
            self.typeField.userInteractionEnabled=NO;
        }
        if (indexPath.row==2) {
            self.shiftAccount=((ContainFieldTableViewCell *)cell).inputField;
            self.shiftAccount.keyboardType=UIKeyboardTypeDefault;
        }
        if (indexPath.row==3) {
            self.changeMoney=((ContainFieldTableViewCell *)cell).inputField;
            [self.changeMoney addTarget:self action:@selector(calculateCharge) forControlEvents:UIControlEventEditingChanged];
        }
        if (indexPath.row==4) {
            self.handlingCharge=((ContainFieldTableViewCell *)cell).inputField;
            self.handlingCharge.userInteractionEnabled=NO;
        }
    }else{
            cell=[tableView dequeueReusableCellWithIdentifier:@"remark"];
            if (!cell) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reamrk"];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,mainWidth, 180)];
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(2*10, 10, 10*4, 10*2)];
                label.text = self.titleArray[indexPath.row];
                label.font = [UIFont systemFontOfSize:15];
                [bgView addSubview:label];
                self.remark = [[UITextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label.frame)+10, 10, CGRectGetWidth(bgView.frame)-CGRectGetMaxX(label.frame)-10*3, 45*3)];
                self.remark.delegate = self;
                self.remark.tintColor = mainGrayColor;
                self.remark.returnKeyType=UIReturnKeyDone;
                [bgView addSubview:_remark];
                self.tintLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.remark.frame), CGRectGetWidth(bgView.frame)-10, 10*2)];
                self.tintLabel.text = @"还可以输入50个字";
                self.tintLabel.textColor = LINE_COLOR;
                self.tintLabel.textAlignment = NSTextAlignmentRight;
                self.tintLabel.font = [UIFont systemFontOfSize:12];
                [bgView addSubview:self.tintLabel];
                [cell.contentView addSubview:bgView];
            }
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:_transferMethodTable]) {
        self.transferMethodTable.hidden=YES;
        self.methodField.text=[self.transferMethodTable cellForRowAtIndexPath:indexPath].textLabel.text;
        self.rule=[self.rulesArray objectAtIndex:indexPath.row];
        [self.typeStrArray removeAllObjects];
        if ([self.rule.tome isEqual:@1]) {
            [self.typeStrArray addObject:@"转给自己"];
        }
        if ([self.rule.toyou isEqual:@1]) {
            [self.typeStrArray addObject:@"转给朋友"];
        }
        self.transferTypeTable.frame=CGRectMake(mainWidth-mainWidth/1.5, 10+45,mainWidth/1.5, self.typeStrArray.count*45+10);
        [self.transferTypeTable reloadData];
        //清空金额和手续费
        self.changeMoney.text=@"";
        self.handlingCharge.text=@"";
        self.typeField.text=@"";
        self.shiftAccount.text=@"";
        //计算余额
        [self calculateBalance];
    }else if([tableView isEqual:_transferTypeTable]){
        self.transferTypeTable.hidden=YES;
       self.typeField.text=[_typeStrArray objectAtIndex:indexPath.row];
    }else if([tableView isEqual:self.tableView]){
        [self.view endEditing:YES];
    }
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:_transferMethodTable]||[tableView isEqual:_transferTypeTable]) {
        return 45;
    }else{
        if (indexPath.row<5) {
            return 45;
        }else{
            return 180;
        }
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        return 6;
    }else if([tableView isEqual:_transferMethodTable]){
        return self.methodStrArray.count;
    }else{
        return self.typeStrArray.count;
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1;
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.25 animations:^{
        self.tableView.frame=CGRectMake(0, 0, mainWidth, 45*7+180);
        self.submitBtn.frame=CGRectMake(10, CGRectGetMaxY(self.tableView.frame)-50, mainWidth-2*10, 50);
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [UIView animateWithDuration:0.25 animations:^{
        self.tableView.frame=CGRectMake(0, 0, mainWidth, 45*7+180);
        self.submitBtn.frame=CGRectMake(10, CGRectGetMaxY(self.tableView.frame)-50, mainWidth-2*10, 50);
    }];
    return  [self.view endEditing:YES];
}
-(void)showMethodTable{
    if (self.transferMethodTable.hidden) {
        if (!self.transferTypeTable.hidden) {
            self.transferTypeTable.hidden=YES;
        }
        [self.transferMethodTable reloadData];
        self.transferMethodTable.hidden=NO;
    }else{
        self.transferMethodTable.hidden=YES;
    }
}
-(void)showTypeTable{
    if(!self.methodField.text.length) {
        [XSTool showToastWithView:self.view Text:@"请先选择转账方式"];
        return;
    }
    if (self.transferTypeTable.hidden) {
        if (!self.transferMethodTable.hidden) {
            self.transferMethodTable.hidden=YES;
        }
         [self.transferTypeTable reloadData];
        self.transferTypeTable.hidden=NO;
    }else{
        self.transferTypeTable.hidden=YES;
    }
}
-(BOOL)judgeTransferMoney{
    if ([self.changeMoney.text floatValue]<self.transferleastMoney) {
        //输入的转账金额大于余额
        [XSTool showToastWithView:self.view Text:@"金额低于最低转账金额"];
        return NO;
    }
    if ([self.changeMoney.text floatValue]>self.transferMostMoney) {
        //输入的转账金额大于余额
        [XSTool showToastWithView:self.view Text:@"余额不足"];
        return NO;
    }
    return YES;
    
}
-(void)calculateCharge{
    if([self judgeTransferMoney]){
        float money=[self.changeMoney.text floatValue];
        float change=money*[_rule.tax floatValue]/100;
        if (change<=[_rule.taxlow floatValue]) {//手续费与最低手续费比较
            change=[_rule.taxlow floatValue];
        }
        if (change>[_rule.taxtop floatValue]) {//手续费与最高手续费比较  为0没有限制
            if([_rule.taxtop integerValue]!=0){
                change=[_rule.taxtop floatValue];
            }
        }
        //展示手续费
        self.handlingCharge.text=[NSString stringWithFormat:@"%.2f",change];
    }
}
-(void)calculateBalance{
    for (NSString *str in _walletDic.allKeys) {
        if ([_rule.bank isEqualToString:str]) {
            self.changeMoney.placeholder=[NSString stringWithFormat:@"可用余额 %@元",_walletDic[str]];
            _walletBalance=_walletDic[str];
            if ([_rule.maxnum integerValue]<=0) {
                self.transferMostMoney=[_walletDic[str] floatValue];
            }else{
                //计算最多可提现金额
                if ([_rule.maxnum floatValue]>=[_walletDic[str] floatValue]) {
                    self.transferMostMoney=[_walletDic[str] floatValue];
                }else{
                    self.transferMostMoney=[_rule.maxnum floatValue];
                }
            }
            if([_rule.minnum integerValue]<=0){
                self.transferleastMoney=0;
            }else{
                self.transferleastMoney=[_rule.minnum floatValue];
            }
            //显示手续费的比例
            self.handlingCharge.placeholder=[NSString stringWithFormat:@"费用 = 转账的金额 * %.2f%%",[_rule.tax floatValue]];
        }
    }
}
-(void)toVertify{
    RealNameVerificationViewController *vertifyVC = [[RealNameVerificationViewController alloc] init];
    [self.navigationController pushViewController:vertifyVC animated:YES];
}

#pragma mark -TextViewDelegate
-(void) textViewDidChange:(UITextView*)textView
{
    if([textView.text length] == 0){
        self.tintLabel.text = @"还可以输入50个字";
    }else{
        self.tintLabel.text = @"";//这里给空
    }
    //计算剩余字数   不需要的也可不写
    NSString *nsTextCotent = textView.text;
    NSUInteger existTextNum = [nsTextCotent length];
    NSUInteger remainTextNum = 50 - existTextNum;
   self.tintLabel.text = [NSString stringWithFormat:@"还可以输入%lu个字",remainTextNum];
}

-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        [textView becomeFirstResponder];
        [textView resignFirstResponder];
        return NO;
    }
    if (range.location >= 50)
    {
        return  NO;
    }else
    {
        return YES;
    }
}
@end
