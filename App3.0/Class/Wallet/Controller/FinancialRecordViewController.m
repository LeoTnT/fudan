//
//  FinancialRecordViewController.m
//  App3.0
//
//  Created by nilin on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FinancialRecordViewController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>

#define SEGMENT_HEIGHT 50
@interface FinancialRecordViewController ()<UIScrollViewDelegate>
{
    //    NSArray *_titleArr;
}
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentControl;

@end

@implementation FinancialRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setSubviews];
}

-(void)setSubviews{

    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.segmentControl];
    [self.view addSubview:self.scrollView];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT-0.5, mainWidth, 0.5)];
    lineView.backgroundColor = BG_COLOR;
    [self.view addSubview:lineView];
    
    
}
- (void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    [self.scrollView setContentOffset:CGPointMake(mainWidth*segmentedControl.selectedSegmentIndex, 0) animated:YES];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    
}
#pragma mark - self cycle

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SEGMENT_HEIGHT, mainWidth, mainHeight-SEGMENT_HEIGHT)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(mainWidth*3, mainHeight-200);
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}

- (HMSegmentedControl *)segmentControl
{
    if (_segmentControl == nil) {
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, mainWidth, SEGMENT_HEIGHT)];
        _segmentControl.sectionTitles = @[@"所有记录",@"余额收入",@"余额支出"];
        _segmentControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TAB_SELECTED_COLOR};
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.selectionIndicatorHeight = 2;
        _segmentControl.selectionIndicatorColor = TAB_SELECTED_COLOR;
        _segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        //        _segmentControl.borderType = HMSegmentedControlBorderTypeRight;
        _segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:16],NSFontAttributeName ,nil];
        [_segmentControl setTitleTextAttributes:dic];
        [_segmentControl addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBarHidden =NO;
    self.navigationItem.title = @"财务记录";
    self.view.backgroundColor = BG_COLOR;
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
