//
//  WalletModel.m
//  App3.0
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletModel.h"
#import "BankCardModel.h"

@implementation WalletAccount
-(instancetype)initWithEnglishName:(NSString *)englishName andChineseName:(NSString *)chineseName andNum:(NSString *)num{
    WalletAccount *count=[[WalletAccount alloc] init];
    count.englishName=englishName;
    count.chineseName=chineseName;
    count.numStr=num;
    return count;
}
@end

@implementation WalletAccountRecord

@end

@implementation WalletAccountList
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"WalletAccountRecord"
             };
}
@end

@implementation WalletAccountData

@end

@implementation WalletAccountClass

@end

@implementation TransferAccountRule

@end

@implementation Wallet

@end

@implementation TransferAccountData
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"set":@"TransferAccountRule"
             };
}
@end

@implementation TransferAccountTotal

@end

@implementation  TransferAccountRecord

@end

@implementation TransferAccountRecordList
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"TransferAccountRecord"
             };
}
@end

@implementation  TransferAccountRecordData

@end

@implementation TransferAccountRecordTotal

@end

@implementation WithDrawRule
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"bank":@"BankCardDataParser"
             };
}
@end
@implementation WithDrawWalletTypeSet

@end

@implementation WithDrawRecord

@end

@implementation WithDrawList
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"WithDrawRecord"
             };
}

@end

@implementation WithDrawWalletType

@end

@implementation TradeType

@end

@implementation RechargeRecordModel
@end

@implementation WalletModel

@end
@implementation WalletPayTypeModel



@end
