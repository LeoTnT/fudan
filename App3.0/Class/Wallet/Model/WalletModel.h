//
//  WalletModel.h
//  App3.0
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WalletAccount : NSObject
/**英语名称*/
@property(nonatomic,copy)NSString *englishName;
/**中文*/
@property(nonatomic,copy)NSString *chineseName;
/**数值*/
@property(nonatomic,copy)NSString *numStr;
-(instancetype)initWithEnglishName:(NSString *)englishName andChineseName:(NSString *)chineseName andNum:(NSString *)num;
@end

@interface WalletAccountRecord:NSObject
@property(nonatomic,strong)NSNumber *admin_id;
@property(nonatomic,strong)NSNumber *id;
@property(nonatomic,copy)NSString *number;
@property(nonatomic,copy)NSString *remain;
@property(nonatomic,copy)NSString *remark;
@property(nonatomic,copy)NSString *table;
@property(nonatomic,strong)NSNumber *table_id;
@property(nonatomic,strong)NSNumber *type;
@property(nonatomic,strong)NSNumber *username;
@property(nonatomic,strong)NSNumber *w_time;
@end

@interface WalletAccountList :NSObject
@property(nonatomic,strong)NSArray *data;
@property(nonatomic,strong)NSNumber *current_page;
@property(nonatomic,strong)NSNumber *per_page;
@property(nonatomic,strong)NSNumber *total;
@end

@interface WalletAccountData:NSObject
@property(nonatomic,strong)WalletAccountList *list;
@property(nonatomic,strong)NSNumber *scale;
@property(nonatomic,copy)NSString *totalMoney;
@property(nonatomic,strong)NSDictionary *trade_type;
@end

@interface WalletAccountClass : NSObject
@property(nonatomic,copy)NSString *info;
@property(nonatomic,strong)NSNumber *status;
@property(nonatomic,strong)WalletAccountData *data;
@end

@interface TransferAccountRule : NSObject
@property(nonatomic,strong)NSNumber *id;
@property(nonatomic,copy)NSString *bank;
@property(nonatomic,copy)NSString *tobank;
@property(nonatomic,strong)NSNumber *tome;
@property(nonatomic,strong)NSNumber *toyou;
@property(nonatomic,strong)NSNumber *taxtype;
@property(nonatomic,copy)NSString *tax;
@property(nonatomic,copy)NSString *taxtop;
@property(nonatomic,copy)NSString *taxlow;
@property(nonatomic,strong)NSNumber *exchange;
@property(nonatomic,copy)NSString *maxnum;
@property(nonatomic,copy)NSString *minnum;
@property(nonatomic,strong)NSNumber *intnum;
@property(nonatomic,strong)NSNumber *w_time;
@property(nonatomic,strong)NSNumber *u_time;
@property(nonatomic,strong)NSNumber *disabled;
@end

@interface Wallet : NSObject
@property(nonatomic,strong)NSNumber *id;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *money;
@property(nonatomic,copy)NSString *score;
@property(nonatomic,copy)NSString *coupon;
@property(nonatomic,copy)NSString *cash;
@property(nonatomic,copy)NSString *testmoney;
@property(nonatomic,copy)NSString *test;
@end

@interface  TransferAccountData: NSObject
@property(nonatomic,strong)NSArray *set;
@property(nonatomic,strong)Wallet *wallet;
@end

@interface TransferAccountTotal : NSObject
@property(nonatomic,copy)NSString *info;
@property(nonatomic,strong)NSNumber *status;
@property(nonatomic,strong)TransferAccountData *data;
@end

@interface TransferAccountRecord : NSObject
@property(nonatomic,copy)NSString *bank;
@property(nonatomic,copy)NSString *c_remark;
@property(nonatomic,copy)NSString *c_time;
@property(nonatomic,copy)NSString *exchange;
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *number;
@property(nonatomic,copy)NSString *poundage;
@property(nonatomic,copy)NSString *remark;
@property(nonatomic,copy)NSString *status;
@property(nonatomic,copy)NSString *taxtype;
@property(nonatomic,copy)NSString *to_bank;
@property(nonatomic,copy)NSString *to_number;
@property(nonatomic,copy)NSString *to_username;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *w_time;
@end

@interface TransferAccountRecordList:NSObject
@property(nonatomic,strong)NSNumber *current_page;
@property(nonatomic,strong)NSArray *data;
@property(nonatomic,strong)NSNumber *per_page;
@property(nonatomic,strong)NSNumber *total;
@end

@interface  TransferAccountRecordData: NSObject
@property(nonatomic,strong)TransferAccountRecordList *list;
@end

@interface TransferAccountRecordTotal:NSObject
@property(nonatomic,strong)NSNumber *status;
@property(nonatomic,strong)TransferAccountRecordData *data;
@property(nonatomic,copy)NSString *info;
@end

@interface WithDrawWalletTypeSet : NSObject
@property(nonatomic,copy)NSString *intnum;
@property(nonatomic,copy)NSString *maxnum;
@property(nonatomic,copy)NSString *minnum;
@property(nonatomic,copy)NSString *nocheck;
@property(nonatomic,copy)NSString *noday;
@property(nonatomic,copy)NSString *pass2;
@property(nonatomic,copy)NSString *tax;
@property(nonatomic,copy)NSString *taxlow;
@property(nonatomic,copy)NSString *taxtop;
@property(nonatomic,copy)NSString *taxtype;
@end

@interface WithDrawRule : NSObject
@property(nonatomic,strong)NSArray *bank;
@property(nonatomic,strong)WithDrawWalletTypeSet *set;
@property(nonatomic,copy)NSString *wallet;
@property(nonatomic,copy)NSString *wallet_type;
@end

@interface WithDrawRecord : NSObject
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *bankname;
@property(nonatomic,copy)NSString *bankaddress;
@property(nonatomic,copy)NSString *bankcard;
@property(nonatomic,copy)NSString *bankuser;
@property(nonatomic,copy)NSString *c_remark;
@property(nonatomic,copy)NSString *c_time;
@property(nonatomic,strong)NSNumber *number;
@property(nonatomic,strong)NSNumber *poundage;
@property(nonatomic,strong)NSNumber *receive;
@property(nonatomic,copy)NSString *remark;
@property(nonatomic,strong)NSNumber *status;
@property(nonatomic,strong)NSNumber *taxtype;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,strong)NSNumber *w_time;
@property(nonatomic,copy)NSString *wallet_type;
@property(nonatomic,copy)NSString *wallet_name;
@end

@interface WithDrawList : NSObject
@property(nonatomic,strong)NSNumber *current_page;
@property(nonatomic,strong)NSArray *data;
@property(nonatomic,strong)NSNumber *per_page;
@property(nonatomic,strong)NSNumber *total;
@end

@interface WithDrawWalletType : NSObject
@property(nonatomic,copy)NSString *wallet_flag;
@property(nonatomic,copy)NSString *wallet_name;
@end

@interface TradeType : NSObject
@property(nonatomic,copy)NSString *key;
@property(nonatomic,copy)NSString *name;
@end

@interface RechargeRecordModel:NSObject
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *memo;
@property (nonatomic ,copy)NSString *money;
@property (nonatomic ,copy)NSString *pay_no;
@property (nonatomic ,copy)NSString *pay_type;
@property (nonatomic ,copy)NSString *payment;
@property (nonatomic ,copy)NSString *payment_class;
@property (nonatomic ,copy)NSString *status;
@property (nonatomic ,copy)NSString *type;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *username;
@property (nonatomic ,copy)NSString *w_time;

@end
@interface WalletModel : NSObject

@end

@interface WalletPayTypeModel : NSObject

@property (nonatomic ,copy)NSString *key;
@property (nonatomic ,copy)NSString *logo;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *memo;
@property (nonatomic ,strong)UIImage *logoImage;
@end
