//
//  WalletAccountRecordCell.h
//  App3.0
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletModel.h"
@interface WalletAccountRecordCell : UITableViewCell
@property(nonatomic,strong) WalletAccountRecord *record;
@property(nonatomic,strong)NSDictionary *tradeType;
@property(nonatomic,assign)CGFloat height;
@end
