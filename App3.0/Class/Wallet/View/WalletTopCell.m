//
//  WalletTopCell.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletTopCell.h"

@interface WalletTopCell()
{
    UILabel *_account;
}
@end

@implementation WalletTopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = mainColor;
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, mainWidth-20, 20)];
        title.text = Localized(@"我的账户（元）");
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.font = [UIFont systemFontOfSize:14];
        [self addSubview:title];
        
        _account = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, mainWidth-20, 40)];
        _account.text = @"0.00";
        _account.textColor = [UIColor whiteColor];
        _account.textAlignment = NSTextAlignmentCenter;
        _account.font = [UIFont boldSystemFontOfSize:30];
        [self addSubview:_account];
    }
    return self;
}
-(void)setMoneyStr:(NSString *)moneyStr{
    _moneyStr=moneyStr;
    _account.text=moneyStr;
}
@end
