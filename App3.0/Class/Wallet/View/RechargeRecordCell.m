//
//  RechargeRecordCell.m
//  App3.0
//
//  Created by 孙亚男 on 2018/2/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "RechargeRecordCell.h"
#import "XSFormatterDate.h"

@interface RechargeRecordCell()
@property(nonatomic,strong)UILabel *userNameLabel,*timeLabel,*rechargeTypeLabel,*numLabel,*typeLabel,*remarkLabel;
@property(nonatomic,strong)UIImageView *statusImg;

@end

@implementation RechargeRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.statusImg=[UIImageView new];
        self.statusImg.image=[UIImage imageNamed:@"no_pic"];
        [self.contentView addSubview:self.statusImg];
        self.userNameLabel=[XSTool createLabelWithText:Localized(@"用户编号") Font:[UIFont systemFontOfSize:14] textColor:[UIColor hexFloatColor:@"111111"] lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.userNameLabel];
        self.timeLabel=[XSTool createLabelWithText:@"" Font:[UIFont systemFontOfSize:12] textColor:COLOR_999999 lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.timeLabel];
        UIView *line1=[UIView new];
        [self.contentView addSubview:line1];
        line1.backgroundColor=[UIColor hexFloatColor:@"E6E6E6"];
        self.rechargeTypeLabel=[XSTool createLabelWithText:Localized(@"充值方式") Font:[UIFont systemFontOfSize:14] textColor:COLOR_666666 lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.rechargeTypeLabel];
        self.numLabel=[XSTool createLabelWithText:Localized(@"充值金额") Font:[UIFont systemFontOfSize:14] textColor:COLOR_666666 lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.numLabel];
        self.typeLabel=[XSTool createLabelWithText:Localized(@"类型") Font:[UIFont systemFontOfSize:14] textColor:COLOR_666666 lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.typeLabel];
        self.remarkLabel=[XSTool createLabelWithText:[NSString stringWithFormat:@"%@:",Localized(@"note")] Font:[UIFont systemFontOfSize:14] textColor:COLOR_999999 lines:1 textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.remarkLabel];
        UIView *line2=[UIView new];
        [self.contentView addSubview:line2];
        line2.backgroundColor=[UIColor hexFloatColor:@"E6E6E6"];
        [self.statusImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(14);
            make.size.mas_equalTo(CGSizeMake(14, 14));
        }];
        [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.statusImg.mas_right).mas_offset(4);
            make.centerY.mas_equalTo(self.statusImg);
        }];
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-12);
            make.centerY.mas_equalTo(self.statusImg);
        }];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.top.mas_equalTo(44);
            make.height.mas_equalTo(1);
        }];
        [self.rechargeTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(line1.mas_bottom).mas_offset(17);
        }];
        [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-12);
            make.centerY.mas_equalTo(self.rechargeTypeLabel);
        }];
        [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(self.rechargeTypeLabel.mas_bottom).mas_offset(13);
        }];
        [line2  mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.top.mas_equalTo(self.typeLabel.mas_bottom).mas_offset(13);
            make.height.mas_equalTo(1);
        }];
        [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(line2.mas_bottom).mas_offset(17);
        }];
    }
    return self;
}
-(void)setModel:(RechargeRecordModel *)model{
    _model=model;
    //0 审核中 1 审核通过 2 审核未通过
    if ([model.status integerValue]==0) {//审核中
        self.statusImg.image=[UIImage imageNamed:@"user_review"];
    }else if ([model.status integerValue]==1){//通过
        self.statusImg.image=[UIImage imageNamed:@"user_review_success"];
    }else{//未通过
        self.statusImg.image=[UIImage imageNamed:@"user_review_fail"];
    }
    self.userNameLabel.text=[NSString stringWithFormat:@"%@: %@",Localized(@"用户编号"),model.username];
    self.timeLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",model.w_time]];
//充值方式
    NSMutableAttributedString *attStr1=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",Localized(@"充值方式"),model.payment]];
    [attStr1 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"111111"] range:NSMakeRange(5, attStr1.length-5)];
    self.rechargeTypeLabel.attributedText=attStr1;
 //充值金额
    NSMutableAttributedString *attStr2=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",Localized(@"充值金额"),model.money]];
    [attStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"111111"] range:NSMakeRange(5, attStr2.length-5)];
    self.numLabel.attributedText=attStr2;
    //类型
    NSMutableAttributedString *attStr3=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",Localized(@"类型"),model.type]];
    [attStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"111111"] range:NSMakeRange(3, attStr3.length-3)];
    self.typeLabel.attributedText=attStr3;
    self.remarkLabel.text=[NSString stringWithFormat:@"%@:%@",Localized(@"note"),model.memo];
}
@end
