//
//  ContainFieldTableViewCell.m
//  App3.0
//
//  Created by syn on 2017/6/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ContainFieldTableViewCell.h"


@implementation TextFieldInCell

+(UITextField *)initWithFrame:(CGRect)frame Placehold:(NSString *)placeHold textColor:(UIColor *)textColor fontSize:(NSUInteger)size KeyBoardType:(UIKeyboardType)keyBoardType textAlignMent:(NSTextAlignment)textAlignMent{
    UITextField *field=[[UITextField alloc] initWithFrame:frame];
    field.placeholder=placeHold;
    field.textColor=textColor;
    field.font=[UIFont systemFontOfSize:size];
    field.keyboardType=keyBoardType;
    field.textAlignment=textAlignMent;
    return field;
}

@end

@implementation ContainFieldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifie{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifie]) {
        
    }
    return self;
}

-(ContainFieldTableViewCell *)initWithId:(NSString *)idStr TextLabelText:(NSString *)text fontSize:(NSUInteger)size WithFieldFrame:(CGRect)frame Placehold:(NSString *)placeHold textColor:(UIColor *)textColor fontSize:(NSUInteger)fieldSize KeyBoardType:(UIKeyboardType)keyBoardType textAlignMent:(NSTextAlignment)fieldTextAlignMent{
    ContainFieldTableViewCell *cell=[[ContainFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idStr];
    cell.textLabel.text=text;
    cell.textLabel.font=[UIFont systemFontOfSize:size];
    cell.inputField=[TextFieldInCell initWithFrame:frame Placehold:placeHold textColor:textColor fontSize:fieldSize KeyBoardType:keyBoardType textAlignMent:fieldTextAlignMent];
    [cell.contentView addSubview:cell.inputField];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
