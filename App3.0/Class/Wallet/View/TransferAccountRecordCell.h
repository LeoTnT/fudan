//
//  TransferAccountRecordCell.h
//  App3.0
//
//  Created by mac on 2017/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletModel.h"
@interface TransferAccountRecordCell : UITableViewCell
@property(nonatomic,strong)TransferAccountRecord *record;
@end
