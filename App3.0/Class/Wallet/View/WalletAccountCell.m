//
//  WalletAccountCell.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletAccountCell.h"
#import "WalletModel.h"
@implementation WalletAccountCell
#pragma mark-刷新界面
-(void)setWalletAccountArray:(NSArray *)walletAccountArray{
    _walletAccountArray=walletAccountArray;
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            for (UIView *view1 in view.subviews) {
                if ([view1 isKindOfClass:[UILabel class]]) {
                    ((UILabel *)view1).text=@"";
                }
            }
        }
    }
    CGFloat width = mainWidth/3;
    NSMutableArray *tempArray=[NSMutableArray array];
    for (int i = 0; i < walletAccountArray.count; i++) {
        WalletAccount *account=[walletAccountArray objectAtIndex:i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(width*(i%3), 70*(int)(i/3), width, 70)];
        [tempArray addObject:btn];
        btn.tag=i+1;
        [self addSubview:btn];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, width, 20)];
        title.text =account.chineseName;
        title.textAlignment = NSTextAlignmentCenter;
        title.font = [UIFont systemFontOfSize:14];
        [btn addSubview:title];
        
        UILabel *balance = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, width, 20)];
        balance.text =account.numStr;
        balance.textColor = [UIColor redColor];
        balance.textAlignment = NSTextAlignmentCenter;
        balance.font = [UIFont systemFontOfSize:14];
        [btn addSubview:balance];
    }
    self.btnArray=tempArray;
    if (walletAccountArray.count) {
        self.height=((walletAccountArray.count-1)/3+1)*70;
        //垂直分割线
        NSUInteger vLineNum=((walletAccountArray.count-1)/3+1)*2;
        CGFloat vLineX=width*2;
        for (int i = 0; i <vLineNum; i++) {
            if (vLineX==width*2) {
                vLineX=width;
            }else{
                vLineX=width*2;
            }
            CGFloat vLineY=70*(i/2);
            UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(vLineX-0.5, vLineY, 0.5, 70)];
            vLine.backgroundColor = LINE_COLOR_NORMAL;
            [self addSubview:vLine];
        }
        //水平分割线
        NSUInteger hLineNum=(walletAccountArray.count+2)/3;
        for (int i=0; i<hLineNum; i++) {
            UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, 70*(i+1)-0.5, mainWidth, 0.5)];
            hLine.backgroundColor = LINE_COLOR_NORMAL;
            [self addSubview:hLine];
        }
    }else{
        self.height=0;
    }
   }
@end
