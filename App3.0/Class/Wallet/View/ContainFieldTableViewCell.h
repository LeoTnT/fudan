//
//  ContainFieldTableViewCell.h
//  App3.0
//
//  Created by syn on 2017/6/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldInCell : UITextField
+(UITextField *)initWithFrame:(CGRect)frame Placehold:(NSString *)placeHold textColor:(UIColor *)textColor fontSize:(NSUInteger)size KeyBoardType:(UIKeyboardType)keyBoardType textAlignMent:(NSTextAlignment)textAlignMent;
@end

@interface ContainFieldTableViewCell : UITableViewCell
@property(nonatomic,strong)UITextField *inputField;
-(ContainFieldTableViewCell *)initWithId:(NSString *)idStr TextLabelText:(NSString *)text fontSize:(NSUInteger)size WithFieldFrame:(CGRect)frame Placehold:(NSString *)placeHold textColor:(UIColor *)textColor fontSize:(NSUInteger)fieldSize KeyBoardType:(UIKeyboardType)keyBoardType textAlignMent:(NSTextAlignment)fieldTextAlignMent;
@end

