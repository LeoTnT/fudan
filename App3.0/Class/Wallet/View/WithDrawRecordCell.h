//
//  WithDrawRecordCell.h
//  App3.0
//
//  Created by 孙亚男 on 2017/11/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletModel.h"

@interface WithDrawRecordCell : UITableViewCell
@property(nonatomic,strong)UIButton *cancelBtn;
@property(nonatomic,strong)WithDrawRecord *record;
@end
