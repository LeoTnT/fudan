//
//  WithDrawRecordCell.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WithDrawRecordCell.h"
#import "XSFormatterDate.h"

@interface WithDrawRecordCell()
@property(nonatomic,strong)UIImageView *logoImage;
@property(nonatomic,strong)UILabel *bankNameLabel,*bankNumLabel,*timeLabel,*sendLabel,*receiveLabel,*typeLabel,*taxLabel,*remarkLabel,*serviceLabel;
@property(nonatomic,strong)UIView *line1,*line2;
@end

@implementation WithDrawRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor=[UIColor whiteColor];
        self.logoImage=[UIImageView new];
        [self.contentView addSubview:self.logoImage];
        self.bankNameLabel=[UILabel new];
        [self.contentView addSubview:self.bankNameLabel];
        self.bankNameLabel.font=[UIFont systemFontOfSize:14];
        self.bankNameLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.bankNumLabel=[UILabel new];
        [self.contentView addSubview:self.bankNumLabel];
        self.bankNumLabel.font=[UIFont systemFontOfSize:13];
        self.bankNumLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.serviceLabel=[UILabel new];
        [self.contentView addSubview:self.serviceLabel];
        self.serviceLabel.font=[UIFont systemFontOfSize:14];
        self.serviceLabel.textColor=[UIColor hexFloatColor:@"FC4C61"];
        self.line1=[UIView new];
        [self.contentView addSubview:self.line1];
        self.line1.backgroundColor=BG_COLOR;
        self.timeLabel=[UILabel new];
        [self.contentView addSubview:self.timeLabel];
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        self.timeLabel.textColor=COLOR_999999;
        self.sendLabel=[UILabel new];
        [self.contentView addSubview:self.sendLabel];
        self.sendLabel.font=[UIFont systemFontOfSize:14];
        self.sendLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.receiveLabel=[UILabel new];
        [self.contentView addSubview:self.receiveLabel];
        self.receiveLabel.font=[UIFont systemFontOfSize:14];
        self.receiveLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.typeLabel=[UILabel new];
        [self.contentView addSubview:self.typeLabel];
        self.typeLabel.font=[UIFont systemFontOfSize:14];
        self.typeLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.taxLabel=[UILabel new];
        [self.contentView addSubview:self.taxLabel];
        self.taxLabel.font=[UIFont systemFontOfSize:14];
        self.taxLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.line2=[UIView new];
        [self.contentView addSubview: self.line2];
         self.line2.backgroundColor=BG_COLOR;
        self.remarkLabel=[UILabel new];
        [self.contentView addSubview:self.remarkLabel];
        self.remarkLabel.font=[UIFont systemFontOfSize:14];
        self.remarkLabel.textColor=COLOR_999999;
        self.cancelBtn=[UIButton new];
        [self.contentView addSubview:self.cancelBtn];
        [self.cancelBtn setBackgroundColor:mainColor];
        [self.cancelBtn setTitle:@"撤销" forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.cancelBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        self.cancelBtn.layer.cornerRadius=2;
        self.cancelBtn.layer.masksToBounds=YES;
           }
    return self;
}
-(void)setRecord:(WithDrawRecord *)record{
    _record=record;
    if ([record.status isEqual:@0]) {//审核中
        self.logoImage.image=[UIImage imageNamed:@"user_review"];
    }else if ([record.status isEqual:@1]){//通过
        self.logoImage.image=[UIImage imageNamed:@"user_review_success"];
    }else{//未通过
        self.logoImage.image=[UIImage imageNamed:@"user_review_fail"];
    }
    self.bankNameLabel.text=record.bankname;
    if (record.bankcard.length>=4) {
        self.bankNumLabel.text=[NSString stringWithFormat:@"(%@****%@)",Localized(@"尾号"),[record.bankcard substringWithRange:NSMakeRange(record.bankcard.length-4, 4)]];
    }else{
        self.bankNumLabel.text=[NSString stringWithFormat:@"(%@****%@)",Localized(@"尾号"),record.bankcard];
    }
    self.serviceLabel.text=record.c_remark;
    self.timeLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",record.w_time]];
   [self frontStr:Localized(@"转出金额") nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"转出金额"),record.number] label:self.sendLabel];
    
    [self frontStr:[NSString stringWithFormat:@"%@ ",Localized(@"sjdz")] nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"sjdz"),record.receive] label:self.receiveLabel];
    [self frontStr:Localized(@"类型") nowStr:[NSString stringWithFormat:@"%@ %@",Localized(@"类型"),record.wallet_name] label:self.typeLabel];
    [self frontStr:Localized(@"sxf") nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"sxf"),record.poundage] label:self.taxLabel];
    self.remarkLabel.text=[NSString stringWithFormat:@"%@:%@",Localized(@"note"),record.remark.length?record.remark:Localized(@"note")];
    [self.logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(14);
        make.width.height.mas_equalTo(16);
    }];
    [self.bankNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(33);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.bankNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bankNameLabel.mas_right);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.serviceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.left.mas_equalTo(self.logoImage);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.bankNameLabel.mas_bottom).mas_offset(18.5);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-12);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.sendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        if (record.c_remark.length) {
            make.top.mas_equalTo(self.serviceLabel.mas_bottom).mas_offset(32);
        }else{
             make.top.mas_equalTo(self.bankNameLabel.mas_bottom).mas_offset(32);
        }
    }];
    [self.receiveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-12);
        make.top.mas_equalTo(self.sendLabel);
    }];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.sendLabel.mas_bottom).mas_offset(12.5);
    }];
    [self.taxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-12);
        make.centerY.mas_equalTo(self.typeLabel);
    }];
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.typeLabel.mas_bottom).mas_offset(30);
    }];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-12);
        make.top.mas_equalTo(self.remarkLabel.mas_bottom).mas_offset(11.5);
        make.size.mas_equalTo(CGSizeMake(74, 27));
    }];
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        if (record.c_remark.length) {
          make.top.mas_equalTo(self.serviceLabel.mas_bottom).mas_offset(14.5);
        }else{
            make.top.mas_equalTo(self.bankNameLabel.mas_bottom).mas_offset(14.5);
        }
        make.height.mas_equalTo(0.5);
    }];
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.mas_equalTo(self.line1);
        make.top.mas_equalTo(self.typeLabel.mas_bottom).mas_offset(13.5);
    }];

}
//给标签改变文字颜色
-(void)frontStr:(NSString *)frontStr nowStr:(NSString *)nowStr label:(UILabel *)changeLabel{
    NSInteger length1=frontStr.length;
    NSMutableAttributedString *attStr=[[NSMutableAttributedString alloc] initWithString:nowStr];
    [attStr addAttribute:NSForegroundColorAttributeName value:COLOR_666666 range:NSMakeRange(0, length1)];
    changeLabel.attributedText=attStr;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
