//
//  WalletAccountCell.h
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletAccountCell : UITableViewCell
/**积分、现金等数组*/
@property(nonatomic,strong)NSArray *walletAccountArray;
/**高度*/
@property(nonatomic,assign)CGFloat height;
/**按钮数组*/
@property(nonatomic,strong)NSArray *btnArray;

@end
