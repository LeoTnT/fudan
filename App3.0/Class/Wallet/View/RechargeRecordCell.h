//
//  RechargeRecordCell.h
//  App3.0
//
//  Created by 孙亚男 on 2018/2/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletModel.h"

@interface RechargeRecordCell : UITableViewCell
@property(nonatomic,strong)RechargeRecordModel *model;
@end
