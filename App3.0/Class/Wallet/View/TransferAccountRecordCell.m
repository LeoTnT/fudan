//
//  WithDrawRecordCell.m
//  App3.0
//
//  Created by 孙亚男 on 2017/11/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TransferAccountRecordCell.h"
#import "XSFormatterDate.h"

@interface TransferAccountRecordCell()
@property(nonatomic,strong)UIImageView *logoImage,*arrowImage;
@property(nonatomic,strong)UILabel *fromUserLabel,*toUserLabel,*timeLabel,*sendTypeLabel,*sendNumLabel,*receiveTypeLabel,*receiveNumLabel,*taxLabel,*remarkLabel,*serviceLabel;
@property(nonatomic,strong)UIView *line1,*line2;
@end

@implementation TransferAccountRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor=[UIColor whiteColor];
        self.logoImage=[UIImageView new];
        [self.contentView addSubview:self.logoImage];
        self.arrowImage=[UIImageView new];
        self.arrowImage.image=[UIImage imageNamed:@"transfer_record_arrow"];
        [self.contentView addSubview:self.arrowImage];
        self.fromUserLabel=[UILabel new];
        [self.contentView addSubview:self.fromUserLabel];
        self.fromUserLabel.font=[UIFont systemFontOfSize:14];
        self.fromUserLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.toUserLabel=[UILabel new];
        [self.contentView addSubview:self.toUserLabel];
        self.toUserLabel.font=[UIFont systemFontOfSize:14];
        self.toUserLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.line1=[UIView new];
        [self.contentView addSubview:self.line1];
        self.line1.backgroundColor=BG_COLOR;
        self.timeLabel=[UILabel new];
        [self.contentView addSubview:self.timeLabel];
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        self.timeLabel.textColor=COLOR_999999;
        self.serviceLabel=[UILabel new];
        [self.contentView addSubview:self.serviceLabel];
        self.serviceLabel.font=[UIFont systemFontOfSize:14];
        self.serviceLabel.textColor=[UIColor hexFloatColor:@"FC4C61"];
        self.sendTypeLabel=[UILabel new];
        [self.contentView addSubview:self.sendTypeLabel];
        self.sendTypeLabel.font=[UIFont systemFontOfSize:14];
        self.sendTypeLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.sendNumLabel=[UILabel new];
        [self.contentView addSubview:self.sendNumLabel];
        self.sendNumLabel.font=[UIFont systemFontOfSize:14];
        self.sendNumLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.receiveTypeLabel=[UILabel new];
        [self.contentView addSubview:self.receiveTypeLabel];
        self.receiveTypeLabel.font=[UIFont systemFontOfSize:14];
        self.receiveTypeLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.receiveNumLabel=[UILabel new];
        [self.contentView addSubview:self.receiveNumLabel];
        self.receiveNumLabel.font=[UIFont systemFontOfSize:14];
        self.receiveNumLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.taxLabel=[UILabel new];
        [self.contentView addSubview:self.taxLabel];
        self.taxLabel.font=[UIFont systemFontOfSize:14];
        self.taxLabel.textColor=[UIColor hexFloatColor:@"111111"];
        self.line2=[UIView new];
        [self.contentView addSubview:self.line2];
        self.line2.backgroundColor=BG_COLOR;
        self.remarkLabel=[UILabel new];
        [self.contentView addSubview:self.remarkLabel];
        self.remarkLabel.font=[UIFont systemFontOfSize:14];
        self.remarkLabel.textColor=COLOR_999999;
           }
    return self;
}
-(void)setRecord:(TransferAccountRecord *)record{
    _record=record;
    if ([record.status integerValue]==0) {//审核中
        self.logoImage.image=[UIImage imageNamed:@"user_review"];
    }else if ([record.status integerValue]==1){//通过
        self.logoImage.image=[UIImage imageNamed:@"user_review_success"];
    }else{//未通过
        self.logoImage.image=[UIImage imageNamed:@"user_review_fail"];
    }
    self.fromUserLabel.text=[NSString stringWithFormat:@"用户:%@",record.username];
     self.toUserLabel.text=[NSString stringWithFormat:@"用户:%@",record.to_username];
    self.timeLabel.text=[XSFormatterDate dateAccurateToDayWithTimeIntervalString:[NSString stringWithFormat:@"%@",record.w_time]];
    [self frontStr:Localized(@"转出类型") nowStr:[NSString stringWithFormat:@"%@ %@",Localized(@"转出类型"),record.bank] label:self.sendTypeLabel];
     [self frontStr:Localized(@"转出金额") nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"转出金额"),record.number] label:self.sendNumLabel];
    [self frontStr:Localized(@"转入类型") nowStr:[NSString stringWithFormat:@"%@ %@",Localized(@"转入类型"),record.to_bank] label:self.receiveTypeLabel];
    [self frontStr:Localized(@"转入金额") nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"转入金额"),record.to_number] label:self.receiveNumLabel];
    [self frontStr:Localized(@"sxf") nowStr:[NSString stringWithFormat:@"%@ ¥%@",Localized(@"sxf"),record.poundage] label:self.taxLabel];
    self.remarkLabel.text=[NSString stringWithFormat:@"%@:%@",Localized(@"note"),record.remark.length?record.remark:Localized(@"暂无")];
    self.serviceLabel.text=record.c_remark;
    [self.logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(14);
        make.width.height.mas_equalTo(16);
    }];
    [self.fromUserLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(33);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fromUserLabel.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.toUserLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.arrowImage.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-10);
        make.centerY.mas_equalTo(self.logoImage);
    }];
    [self.serviceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logoImage);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.fromUserLabel.mas_bottom).mas_offset(18.5);
    }];
      [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        if (record.c_remark.length) {
            make.top.mas_equalTo(self.serviceLabel.mas_bottom).mas_offset(14.5);
        }else{
            make.top.mas_equalTo(self.fromUserLabel.mas_bottom).mas_offset(14.5);
        }
        make.height.mas_equalTo(0.5);
    }];
    [self.sendTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.line1.mas_bottom).mas_offset(16.5);
    }];
    [self.sendNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.sendTypeLabel);
    }];
    [self.receiveTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.sendTypeLabel.mas_bottom).mas_offset(12.5);
    }];
    [self.receiveNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.sendTypeLabel.mas_bottom).mas_offset(12.5);
    }];
    [self.taxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.receiveTypeLabel.mas_bottom).mas_offset(12.5);
    }];
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.taxLabel.mas_bottom).mas_offset(34);
    }];
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.mas_equalTo(self.line1);
        make.top.mas_equalTo(self.taxLabel.mas_bottom).mas_offset(17);
    }];

}
//给标签改变文字颜色
-(void)frontStr:(NSString *)frontStr nowStr:(NSString *)nowStr label:(UILabel *)changeLabel{
    NSInteger length1=frontStr.length;
    NSMutableAttributedString *attStr=[[NSMutableAttributedString alloc] initWithString:nowStr];
    [attStr addAttribute:NSForegroundColorAttributeName value:COLOR_666666 range:NSMakeRange(0, length1)];
    changeLabel.attributedText=attStr;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
