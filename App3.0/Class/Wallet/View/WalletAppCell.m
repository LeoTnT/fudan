//
//  WalletAppCell.m
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "WalletAppCell.h"
#import "AreaButton.h"
#import "RechargeViewController.h"
@implementation WalletAppCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    NSArray *arr = @[@{@"image":@"user_wallet_recharge",@"title":Localized(@"账户充值")},@{@"image":@"user_wallet_bindingcard",@"title":Localized(@"bind_bank_title")},@{@"image":@"user_wallet_resetpwd",@"title":Localized(@"密码重置")},@{@"image":@"user_wallet_withdraw",@"title":Localized(@"账户提现")},@{@"image":@"user_wallet_transfer",@"title":Localized(@"账户转账")},@{@"image":@"user_qr",@"title":Localized(@"收款码")}];
        CGFloat width = mainWidth/3;
        CGFloat height = 100;
        NSInteger row = ceil(arr.count/3.0);
        
        for (int i = 0; i < arr.count; i++) {
            AreaButton *btn = [[AreaButton alloc] initWithFrame:CGRectMake(width*(i%3), (int)(i/3)*height, width, height) Model:arr[i] Scale:1.0/3.0 fontSize:14];
            //记录按钮的下标
            btn.index=i;
            [btn addTarget:self action:@selector(areaAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            
            if (i%3 == 0 && i != 0) {
                UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, height*(int)(i/3)-0.5, mainWidth, 0.5)];
                hLine.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
                [self addSubview:hLine];
            }
        }
        for (int i = 1; i <= 2; i++) {
            UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(width*i-0.5, 0, 0.5, height*row)];
            hLine.backgroundColor = [UIColor hexFloatColor:@"E6E6E6"];
            [self addSubview:hLine];
        }
    }
    return self;
}

- (void)areaAction:(AreaButton *)pSender
{
    if (self.areaBtnTapAction) {
        self.areaBtnTapAction(pSender.index);
    }
}
@end
