//
//  WalletAppCell.h
//  App3.0
//
//  Created by mac on 17/3/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletAppCell : UITableViewCell
@property (nonatomic ,copy)void (^ areaBtnTapAction)(NSInteger index);
@end
