//
//  JYSCurrencyTradingViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradingViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "JYSDetailBuyViewController.h"//买入
#import "JYSDetailSoldViewController.h"//卖出
#import "JYSCurrentCommissionedViewController.h"//当前委托
#import "JYSHistoryDealViewController.h"//历史交易
#import "JYSTradingDropDownView.h"
#import "JYSCurrencySearchViewController.h"//搜索界面
#import "SRWebSocketTool.h"
#import "JYSCurrencyTradingModel.h"
#import "LoginViewController.h"

@interface JYSCurrencyTradingViewController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>

/** titleButton */
@property (nonatomic, strong) UIButton * titleButton;
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

/** 市场的常用信息(数量价格的小数精度等） */
@property (nonatomic, strong) JYSSymbolBInfoModel * bInfoModel;

/** 下拉选择菜单 */
@property (nonatomic, strong) JYSTradingDropDownView * dropDownView;

/** 下拉菜单左侧选中index */
@property (nonatomic, copy) NSString * menuLeftSelectedString;
/** 下拉菜单右侧选中index */
@property (nonatomic, assign) NSUInteger menuRightSelectedIndex;

///** 菜单数据数组 */
//@property (nonatomic, strong) NSArray * topMenus;

/** 菜单数据字典 */
@property (nonatomic, copy) NSDictionary * topMenuDict;

/** 卖-数组 */
@property (nonatomic, strong) NSMutableArray * asksArray;
/** 买-数组 */
@property (nonatomic, strong) NSMutableArray * bidsArray;

/** 币名 */
@property (nonatomic, copy) NSString * symbolString;

//@property (nonatomic, strong) NSDictionary * selCoinDic;//选中的币种字典
@property (nonatomic, strong) JYSCurrencyTradingModel * selectCoinModel;//选中的币种模型

/** 小数点位数数组 */
@property (nonatomic, copy) NSArray * decimalPointArray;

@property (nonatomic, copy) NSString * closePrice;//当前价
@property (nonatomic, copy) NSString * selTitle;//


///** 币名 */
//@property (nonatomic, strong) NSString * coinName;

/** 深度 */
@property (nonatomic, copy) NSString * deepString;
@property (nonatomic, copy) NSString * deepSelStr;

/** 买入界面 */
@property (nonatomic, strong) JYSDetailBuyViewController * buyVC;

/** 卖出界面 */
@property (nonatomic, strong) JYSDetailSoldViewController * soldVC;

///** symbol */
//@property (nonatomic, copy) NSString * symbolString;
/** 当前委托 */
@property (nonatomic, strong) JYSCurrentCommissionedViewController *entrustVC;

/** 历史记录 */
@property (nonatomic, strong) JYSHistoryDealViewController * historyVC;

@end

@implementation JYSCurrencyTradingViewController

- (JYSDetailBuyViewController *)buyVC {
    if (_buyVC == nil) {
        _buyVC = [[JYSDetailBuyViewController alloc] init];
    }
    return _buyVC;
}

- (JYSDetailSoldViewController *)soldVC {
    if (_soldVC == nil) {
        _soldVC = [[JYSDetailSoldViewController alloc] init];
    }
    return _soldVC;
}

- (JYSCurrentCommissionedViewController *)entrustVC {
    if (_entrustVC == nil) {
        _entrustVC = [[JYSCurrentCommissionedViewController alloc] init];
    }
    return _entrustVC;
}

- (JYSHistoryDealViewController *)historyVC {
    if (_historyVC == nil) {
        _historyVC = [[JYSHistoryDealViewController alloc] init];
    }
    return _historyVC;
}

- (NSMutableArray *)asksArray {
    if (_asksArray == nil) {
        _asksArray = [[NSMutableArray alloc] init];
    }
    return _asksArray;
}

- (NSMutableArray *)bidsArray {
    if (_bidsArray == nil) {
        _bidsArray = [[NSMutableArray alloc] init];
    }
    return _bidsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.translucent = NO;
    
    self.symbolString = @"gpacny";
    self.decimalPointArray = @[@"0",@"1",@"2",@"3",@"4",@"5"];
    
    
    
    self.deepString = @"2";
    self.deepSelStr = @"4";
    
    self.menuLeftSelectedString = @"";
    self.menuRightSelectedIndex = 0;
    [self requestMenuData];
    
    //    __weak typeof(self) weakSelf = self;
    //    [self actionCustomLeftBtnWithNrlImage:@"jys_back" htlImage:nil title:nil action:^{
    //        [[SRWebSocketTool sharedSRWebSocketTool] closeWebSocket];
    //        self.tabBarController.navigationController.navigationBar.translucent = YES;
    //        [weakSelf.tabBarController.navigationController popViewControllerAnimated:YES];
    //    }];
    
    [self setupNavigationBar];
    //    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithImage:@"jys_search" highImage:@"jys_search_2" target:self action:@selector(rightBarButtonItemClicked)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithImage:@"contact_search" highImage:@"contact_search" target:self action:@selector(rightBarButtonItemClicked)];
    
    [self setUpUI];
    [self getWebSocketMessage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInfo) name:@"TradingUpdateInfo" object:nil];
    
}

- (void)getWebSocketMessage{
    [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
        NSArray * dictAllKeys = [dic allKeys];
        if ([dictAllKeys containsObject:@"rep"] || [dictAllKeys containsObject:@"ch"]) {
            //            XSLog(@"key =%@   =%@  =%@ ",key,message,dic);
            
            NSString * typeString;
            if ([dictAllKeys containsObject:@"rep"]) {
                typeString = dic[@"rep"];
            } else {
                typeString = dic[@"ch"];
            }
            NSLog(@"typeString=>%@",typeString);
            if (isEmptyString([self getDeepTypeStr])||isEmptyString(typeString)) {
                return ;
            }
            if ([typeString hasSuffix:[self getDeepTypeStr]]) {
                //            if ([typeString containsString:@".depth.step"]) {
                [self.asksArray removeAllObjects];
                [self.bidsArray removeAllObjects];
                if ([dictAllKeys containsObject:@"tick"]) {
                    NSDictionary * tickDict = dic[@"tick"];
                    NSArray * tickDictKeys = [tickDict allKeys];
                    if ([tickDictKeys containsObject:@"asks"]) {
                        NSArray * asksA = tickDict[@"asks"];
                        if ([asksA isKindOfClass:[NSArray class]]) {
                            [self.asksArray addObjectsFromArray:tickDict[@"asks"]];
                        }
                    }
                    if ([tickDictKeys containsObject:@"bids"]) {
                        NSArray * bidsA = tickDict[@"bids"];
                        if ([bidsA isKindOfClass:[NSArray class]]) {
                            [self.bidsArray addObjectsFromArray:tickDict[@"bids"]];
                        }
                    }
                    
                    [self.buyVC setBuyArray:self.bidsArray soldArray:self.asksArray depStr:self.deepSelStr];
                    [self.soldVC setBuyArray:self.bidsArray soldArray:self.asksArray depStr:self.deepSelStr];
                    
                }
            }
            if (isEmptyString(typeString)||isEmptyString([self getDetailTypeStr])) {
                return ;
            }
            
            if ([typeString hasSuffix:[self getDetailTypeStr]]) {
                if ([dictAllKeys containsObject:@"tick"]) {
                    NSDictionary * tickDict = dic[@"tick"];
                    NSArray * tickDictKeys = [tickDict allKeys];
                    if ([tickDictKeys containsObject:@"close"]) {
                        self.closePrice = [NSString stringWithFormat:@"%@",tickDict[@"close"]];
                    }
                }
                [self.buyVC setBuyClosePrice:self.closePrice];
                [self.soldVC setSoldClosePrice:self.closePrice];
            }
        }
    }];
    
}

- (void)rightBarButtonItemClicked {
    
    //搜索🔍
    JYSCurrencySearchViewController * searchVC = [[JYSCurrencySearchViewController alloc] init];
    __weak __typeof__(self) weakSelf = self;
    searchVC.searchBlock = ^(JYSCurrencyTradingModel *tradingModel) {
        
        [weakSelf unSunInfo];
        
        weakSelf.menuLeftSelectedString = tradingModel.ucoin;
        NSArray * rightMenus = weakSelf.topMenuDict[weakSelf.menuLeftSelectedString];
        //            weakSelf.menuRightSelectedIndex = [rightMenus indexOfObject:currencyModel.gcoin];
        for (NSDictionary *tDic in rightMenus) {
            NSString * gcoin = tDic[@"gcoin"];
            
            if ([gcoin isEqualToString:tradingModel.gcoin]) {
                weakSelf.menuRightSelectedIndex = [rightMenus indexOfObject:tDic];
            }
        }
        if (weakSelf.topMenuDict.count) {
            [weakSelf.dropDownView setTheTopMenuData:weakSelf.topMenuDict leftSelectString:weakSelf.menuLeftSelectedString rightSelectIndex:weakSelf.menuRightSelectedIndex];
        }
        
        weakSelf.selTitle = [NSString stringWithFormat:@"%@/%@",tradingModel.gcoin,tradingModel.ucoin];
        [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:[weakSelf.symbolString stringByAppendingString:@".depth.step"] key:@"unsub" deep:weakSelf.deepString];
        [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:[weakSelf.symbolString stringByAppendingString:@".depth.step"] key:@"req" deep:weakSelf.deepString];
        
        NSString *symbolStr = [NSString stringWithFormat:@"%@%@",tradingModel.gcoin,tradingModel.ucoin];
        weakSelf.symbolString = [symbolStr lowercaseString];
        weakSelf.selectCoinModel = tradingModel;
        weakSelf.deepString = @"2";
        weakSelf.deepSelStr = @"4";
        [weakSelf updateInfo];
        
        //    searchVC.searchBlock = ^(NSDictionary *searchDic) {
        //        if (searchDic.count) {
        //            [weakSelf unSunInfo];
        //            weakSelf.selTitle = [NSString stringWithFormat:@"%@/%@",searchDic[@"gcoin"],searchDic[@"ucoin"]];
        //            [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:[weakSelf.symbolString stringByAppendingString:@".depth.step"] key:@"unsub" deep:weakSelf.deepString];
        //            [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:[weakSelf.symbolString stringByAppendingString:@".depth.step"] key:@"req" deep:weakSelf.deepString];
        //
        //            NSString *symbolStr = [NSString stringWithFormat:@"%@%@",searchDic[@"gcoin"],searchDic[@"ucoin"]];
        //            weakSelf.symbolString = [symbolStr lowercaseString];
        //            weakSelf.selCoinDic = searchDic;
        //            weakSelf.deepString = @"2";
        //            weakSelf.deepSelStr = @"4";
        //
        //            [weakSelf updateInfo];
        //        }
    };
    [self.navigationController pushViewController:searchVC animated:YES];
}
- (void)unSunInfo{
    if (isEmptyString(self.symbolString)) {
        return;
    }
    NSString * requestprice =[self.symbolString stringByAppendingString:@".detail"];
    NSString * requestString =[self.symbolString stringByAppendingString:@".depth.step"];
    
    // 请求市场最新价格之类的
    // 请求市场最新深度之类的
    [[SRWebSocketTool sharedSRWebSocketTool] sendMessage:requestprice key:@"unsub" deep:nil];
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:requestString key:@"unsub" deep:self.deepString];
}
static CGFloat const space = 5.f;
- (void)setupNavigationBar {
    self.titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.titleButton.frame = CGRectMake(0, 0, FontNum(180), FontNum(20));
    
    [self.titleButton addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = self.titleButton;
}

- (void)setTitleBtnData:(NSString *)title {
    [XSUITool setButton:self.titleButton titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil image:[UIImage imageNamed:@"mall_down"] backgroundImage:nil forState:UIControlStateNormal title:title];
    
    CGFloat labelWidth = self.titleButton.titleLabel.intrinsicContentSize.width; //注意不能直接使用titleLabel.frame.size.width,原因为有时候获取到0值
    CGFloat imageWidth = self.titleButton.imageView.frame.size.width;
    [XSUITool setButton:self.titleButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space) titleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0,  imageWidth+space)];
    self.titleButton.titleLabel.font = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isEmptyString(self.symbolString)||!self.selectCoinModel) {
        return;
    }
    [self updateInfo];
}
- (void)setUpUI {
    self.view.backgroundColor = BG_COLOR;
    NSArray *titleArr = @[Localized(@"buy"), Localized(@"selltwo"), Localized(@"orders_tab_ing"),Localized(@"entrusthis")];
//    NSArray *titleArr = @[@"买入", @"卖出", @"当前委托",@"历史成交"];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleFont = [UIFont systemFontOfSize:FontNum(14) weight:UIFontWeightSemibold];
    configure.titleColor = COLOR_999999;
    configure.titleSelectedColor = XSYCOLOR(0x232426);
    configure.indicatorColor = JYSMainSelelctColor;
    configure.indicatorDynamicWidth = FontNum(2);
    /// pageTitleView
    // 这里的 - 10 是为了让 SGPageTitleView 超出父视图，给用户一种效果体验
    
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) delegate:self titleNames:titleArr configure:configure];
    //    _pageTitleView.backgroundColor = [UIColor clearColor];
    // 对 navigationItem.titleView 的包装，为的是 让View 占据整个视图宽度
    [self.view addSubview:self.pageTitleView];
    
    NSUInteger navHeight;
    NSUInteger tabBarHeight;
    if (SCREEN_HEIGHT == 812) {
        navHeight = 88;
        tabBarHeight = 83;
    } else {
        navHeight = 64;
        tabBarHeight = 49;
    }
    
    NSArray *childArr = @[self.buyVC, self.soldVC, self.entrustVC, self.historyVC];
    
    __weak __typeof__(self) wSelf = self;
    self.buyVC.refreshBlock = ^(BOOL isRefresh) {
        [wSelf updateInfo];
    };
    self.soldVC.refreshBlock = ^(BOOL isRefresh) {
        [wSelf updateInfo];
    };
    
    __weak typeof(self) weakSelf = self;
    [[UserSingle sharedUserSingle].siginleSubject subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString * type ,id response) = x;
        if ([response isKindOfClass:[NSString class]]) {
            
            NSString * responseStr = response;
            [weakSelf getDeepInfoWithString:responseStr];
            
        } else if ([response isKindOfClass:[JYSCurrencyTradingModel class]]) {
            JYSCurrencyTradingModel * currencyModel = response;
            XSLog(@"%@---%@",type,currencyModel.symbol);
            //取消上一个订阅
            [self unSunInfo];
            
            weakSelf.symbolString = currencyModel.symbol;
            weakSelf.menuLeftSelectedString = currencyModel.ucoin;
            NSArray * rightMenus = weakSelf.topMenuDict[weakSelf.menuLeftSelectedString];
            //            weakSelf.menuRightSelectedIndex = [rightMenus indexOfObject:currencyModel.gcoin];
            for (NSDictionary *tDic in rightMenus) {
                NSString * gcoin = tDic[@"gcoin"];
                
                if ([gcoin isEqualToString:currencyModel.gcoin]) {
                    weakSelf.menuRightSelectedIndex = [rightMenus indexOfObject:tDic];
                    if (rightMenus.count > weakSelf.menuRightSelectedIndex) {
                        NSDictionary * rightMenuDict = rightMenus[weakSelf.menuRightSelectedIndex];
                        JYSCurrencyTradingModel * tempModel = [JYSCurrencyTradingModel mj_objectWithKeyValues:rightMenuDict];
                        self.selectCoinModel = tempModel;
                    }
                }
            }
            
            //            [weakSelf setTitleBtnData:[NSString stringWithFormat:@"%@/%@",currencyModel.gcoin,currencyModel.ucoin]];
            
            if (self.topMenuDict.count) {
                [self.dropDownView setTheTopMenuData:self.topMenuDict leftSelectString:self.menuLeftSelectedString rightSelectIndex:self.menuRightSelectedIndex];
            }
            
            self.selTitle = [NSString stringWithFormat:@"%@/%@",currencyModel.gcoin,currencyModel.ucoin];
            if ([type isEqualToString:@"buy"]) {
                //                weakSelf.pageTitleView.selectedIndex = 0;
                [weakSelf.pageTitleView setResetSelectedIndex:0];
            } else {
                //                weakSelf.pageTitleView.selectedIndex = 1;
                [weakSelf.pageTitleView setResetSelectedIndex:1];
            }
            
            [self updateInfo];
            
        }
    }];
    
    /// pageContentView
    CGFloat contentViewHeight;
    if (self.navigationController.childViewControllers.count >1) {
        contentViewHeight = self.view.frame.size.height - 44-navHeight;
    } else {
        contentViewHeight = self.view.frame.size.height - 44-navHeight-tabBarHeight;
    }
    
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
    
    self.dropDownView = [[JYSTradingDropDownView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT, 1)];
    [self.view addSubview:self.dropDownView];
    [self.dropDownView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}
//获取深度信息
-(void)getDeepInfoWithString:(NSString*)responseStr{
    [self.view endEditing:YES];
    self.deepSelStr = responseStr;
    switch (responseStr.intValue) {
        case 2:
            self.deepString = @"4";
            break;
        case 3:
            self.deepString = @"3";
            break;
        case 4:
            self.deepString = @"2";
            break;
        case 5:
            self.deepString = @"1";
            break;
        default:
            break;
    }
    //    XSLog(@"%@---%@",type,responseStr);
    [self requestData];
}


//-(void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//
//    [self.dropDownView initialHideMenuView];
//}

- (void)requestMenuData {
    
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSPullDownMenuURL params:nil HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.topMenuDict = dic[@"data"];
            if ([self.topMenuDict isKindOfClass:[NSDictionary class]] && self.topMenuDict.count) {
                
                NSArray * allKeys = [self.topMenuDict allKeys];
                self.menuLeftSelectedString = allKeys[0];
                NSArray * rightMenus = self.topMenuDict[self.menuLeftSelectedString];
                
                NSDictionary * rightMenuDict = rightMenus[0];
                JYSCurrencyTradingModel * tempModel = [JYSCurrencyTradingModel mj_objectWithKeyValues:rightMenuDict];
                self.selectCoinModel = tempModel;
                //                self.selCoinDic = rightMenuDict;
                NSString *gcoin = [NSString stringWithFormat:@"%@",rightMenuDict[@"gcoin"]];
                NSString *ucoin = [NSString stringWithFormat:@"%@",rightMenuDict[@"ucoin"]];
                if (!isEmptyString(gcoin)&& !isEmptyString(ucoin)) {
                    self.selTitle = [NSString stringWithFormat:@"%@/%@",gcoin,ucoin];
                }
                self.symbolString = [NSString stringWithFormat:@"%@",rightMenuDict[@"symbol"]];
                [self updateInfo];
                
                __weak typeof(self) weakSelf = self;
                [self.dropDownView setTopMenuData:self.topMenuDict leftSelectString:self.menuLeftSelectedString rightSelectIndex:self.menuRightSelectedIndex dropDownBlock:^(NSDictionary *rightDict, NSString *leftSelectedString, NSUInteger rightSelectedIndex) {
                    //取消上一次订阅
                    [weakSelf unSunInfo];
                    
                    weakSelf.deepString = @"2";
                    weakSelf.deepSelStr = @"4";
                    weakSelf.menuLeftSelectedString = leftSelectedString;
                    weakSelf.menuRightSelectedIndex = rightSelectedIndex;
                    
                    JYSCurrencyTradingModel * tempModel = [JYSCurrencyTradingModel mj_objectWithKeyValues:rightDict];
                    
                    if (rightDict.count) {
                        NSString *gcoin = [NSString stringWithFormat:@"%@",rightDict[@"gcoin"]];
                        NSString *ucoin = [NSString stringWithFormat:@"%@",rightDict[@"ucoin"]];
                        if (!isEmptyString(gcoin)&& !isEmptyString(ucoin)) {
                            weakSelf.selTitle = [NSString stringWithFormat:@"%@/%@",gcoin,ucoin];
                        }
                        
                        weakSelf.symbolString = [NSString stringWithFormat:@"%@",rightDict[@"symbol"]];
                        //                        weakSelf.selCoinDic = rightDict;
                        weakSelf.selectCoinModel = tempModel;
                        
                        [weakSelf updateInfo];
                    }
                    weakSelf.titleButton.selected = NO;
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    [UIView animateWithDuration:0.25 animations:^{
                        strongSelf.titleButton.imageView.transform=CGAffineTransformRotate(strongSelf.titleButton.imageView.transform, M_PI);
                    }];
                }];
            }
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}


//更新页面信息
- (void)updateInfo
{
    //设置title
    if (!isEmptyString(self.selTitle)) {
        [self setTitleBtnData:self.selTitle];
    }
    //请求市场最新价格 数量,设置折线图
    [self requestData];
    
    //买入/卖出余额更新
    [self setBuyOrSoldRemain];
    
    //更新当前委托
    [self.entrustVC setSymbolStringData:self.symbolString];
    
    //更新历史成交
    [self.historyVC setSymbolStringData:self.symbolString];
    
    //查询市场的常用信息(数量价格的小数精度等）
    [self getSymbolBInfoURL];
}

- (void)getSymbolBInfoURL{
    [self.view endEditing:YES];
    if (self.symbolString) {
        NSDictionary *param = @{
                                @"symbol":self.symbolString
                                };
        __weak __typeof__(self) wSelf = self;
        [JYSAFNetworking getOrPostWithType:POST withUrl:JYSSymbolBInfoURL params:param HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
            if (state) {
                NSDictionary *dicData = dic[@"data"];
                if ([dicData isKindOfClass:[NSDictionary class]]) {
                    wSelf.bInfoModel = [JYSSymbolBInfoModel mj_objectWithKeyValues:dicData];
                    [self updateDemoInfo];
                    wSelf.buyVC.bInfoModel = wSelf.bInfoModel;
                    wSelf.soldVC.bInfoModel = wSelf.bInfoModel;
                }
            }
        } fail:^(NSError *error) {
            
        } showHUD:YES];
    }
}
- (void)updateDemoInfo
{
    //获取小数位数
    NSInteger precision = self.bInfoModel.price_precision.integerValue < self.bInfoModel.amount_precision.integerValue ? self.bInfoModel.price_precision.integerValue:self.bInfoModel.amount_precision.integerValue;
    
    //    if (precision >=8) {
    //        self.decimalPointArray = @[@"2",@"3",@"4",@"5",@"8"];
    //    }else if (precision >=5) {
    //        self.decimalPointArray = @[@"2",@"3",@"4",@"5"];
    //    }else
    
    if (precision >=4) {
        self.decimalPointArray = @[@"2",@"3",@"4"];
    }else if (precision >=3) {
        self.decimalPointArray = @[@"2",@"3"];
    }else{
        self.decimalPointArray = @[@"2"];
    }
    self.buyVC.decimalPlacesArray = self.decimalPointArray;
    self.soldVC.decimalPlacesArray = self.decimalPointArray;
}
- (void)setBuyOrSoldRemain
{
    //获取页面余额信息
    //    self.buyVC.selCoinDic = self.selCoinDic;
    self.buyVC.selCoinModel = self.selectCoinModel;
    //    self.soldVC.selCoinDic = self.selCoinDic;
    self.soldVC.selCoinModel = self.selectCoinModel;
}


- (void)requestData {
    NSString * requestprice =[self.symbolString stringByAppendingString:@".detail"];
    
    // 请求市场最新价格之类的
    [[SRWebSocketTool sharedSRWebSocketTool] sendMessage:requestprice key:@"req" deep:nil];
    // 请求市场最新价格之类的
    [[SRWebSocketTool sharedSRWebSocketTool] sendMessage:requestprice key:@"sub" deep:nil];
    
    NSString * requestString =[self.symbolString stringByAppendingString:@".depth.step"];
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:requestString key:@"req" deep:self.deepString  isTradeView:YES];
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:requestString key:@"sub" deep:self.deepString isTradeView:YES];
    
    
    NSString* parameterString = [requestString stringByAppendingString:self.deepString];
    //    NSString* parameterPrice = requestprice;
    
    XSLog(@"%@",parameterString);
    self.closePrice = @"";
    [self.buyVC setBuyClosePrice:self.closePrice];
    [self.soldVC setSoldClosePrice:self.closePrice];
    
    [self.buyVC setBuyArray:[NSArray array] soldArray:[NSArray array] depStr:self.deepSelStr];
    [self.soldVC setBuyArray:[NSArray array] soldArray:[NSArray array] depStr:self.deepSelStr];
    
    
    
    
}

- (void)titleBtnClick:(UIButton *)button {
    [self.view endEditing:YES];
    
    if (!self.selTitle) {
        [self requestMenuData];
        return;
    }
    
    self.titleButton.selected = !self.titleButton.selected;
    [UIView animateWithDuration:0.25 animations:^{
        self.titleButton.imageView.transform=CGAffineTransformRotate(self.titleButton.imageView.transform, M_PI);
    }];
    
    if (self.titleButton.selected) {
        [self.dropDownView showView];
    } else {
        [self.dropDownView hideView];
    }
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}


- (BOOL)checkIsLogin{
    // 获取本地存储的guid
    NSString *guid = [[NSUserDefaults standardUserDefaults] objectForKey:APPAUTH_KEY];
    
    if (guid.length <= 0) {
        LoginViewController *vc =[[LoginViewController alloc] init];
        [self.navigationController presentViewController:vc animated:YES completion:nil];
        return NO;
    }
    return YES;
}


- (NSString *)getDeepTypeStr{
    NSString *deepTypeString =@"";
    NSString *message = [self.symbolString stringByAppendingString:@".depth.step"];
    if (self.deepString.length) {
        deepTypeString = [NSString stringWithFormat:@"market.%@%@",message,self.deepString];
    }else{
        deepTypeString = [NSString stringWithFormat:@"market.%@",message];
    }
    return deepTypeString;
    
}

- (NSString *)getDetailTypeStr{
    NSString *detailTypeString = [self.symbolString stringByAppendingString:@".detail"];
    return detailTypeString;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end

