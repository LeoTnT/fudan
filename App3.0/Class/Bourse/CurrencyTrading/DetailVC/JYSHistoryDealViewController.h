//
//  JYSHistoryDealViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface JYSHistoryDealViewController : XSBaseTableViewController

- (void)setSymbolStringData:(NSString *)symbol;

@end
