//
//  JYSEntrustOrdersModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSEntrustOrdersModel : NSObject

@property (nonatomic, copy) NSString * name;

/** id */
@property (nonatomic, copy) NSString * ID;
/**  //'0准备提交1提交2部分成交3部分成交并撤销4完全成交5撤销',0，1，2可撤单 */
@property (nonatomic, copy) NSString * type;
/** '0买 1卖出', */
@property (nonatomic, copy) NSString * status;
/** 纳秒 */
@property (nonatomic, copy) NSString * created_time;
/** 委托数量 */
@property (nonatomic, copy) NSString * amount;
/** 委托价格 */
@property (nonatomic, copy) NSString * price;
/** 已成交数量 */
@property (nonatomic, copy) NSString * field_amount;

/**  */
@property (nonatomic, copy) NSString * gcoin;

/**  */
@property (nonatomic, copy) NSString * ucoin;

@end
