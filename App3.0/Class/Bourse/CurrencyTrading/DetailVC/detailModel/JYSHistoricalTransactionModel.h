//
//  JYSHistoricalTransactionModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSHistoricalTransactionModel : NSObject

/** id */
@property (nonatomic, copy) NSString * ID;
/** 纳秒时间，除以10亿为时间戳 */
@property (nonatomic, copy) NSString * create_time;
/** '0买 1卖出', */
@property (nonatomic, copy) NSString * type;
/** 纳秒 */
//@property (nonatomic, copy) NSString * created_time;
/** 委托数量 */
//@property (nonatomic, copy) NSString * amount;
/** 委托价格 */
@property (nonatomic, copy) NSString * price;
/** 成交量 */
@property (nonatomic, copy) NSString * filled_amount;

/**  */
//@property (nonatomic, copy) NSString * direction;

/**  */
@property (nonatomic, copy) NSString * gcoin;

/**  */
@property (nonatomic, copy) NSString * ucoin;

@end
