//
//  JYSHistoryDealViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSHistoryDealViewController.h"
#import "JYSHistoryDealCell.h"
#import "JYSHistoricalTransactionModel.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
@interface JYSHistoryDealViewController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 页码 */
@property (nonatomic, assign) NSUInteger page;
/** 数据源数组 */
@property (nonatomic, strong) NSMutableArray * dataArray;

/** 市场名称 */
@property (nonatomic, copy) NSString * symbolString;

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation JYSHistoryDealViewController

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"entrusthis");
    
    self.page = 1;
    
    [self setUpUI];
    
    [self downRefresh];
    [self upRefresh];
}

- (void)setSymbolStringData:(NSString *)symbol {
    self.symbolString = symbol;
    
    [self requestHistryList];
}

static NSString * const cellID = @"JYSHistoryDealCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSHistoryDealCell class] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //    self.tableView.tableHeaderView = self.topSortingView;
}

#pragma mark 下拉刷新
- (void)downRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshHeader:^{
        weakSelf.page = 1;
        [weakSelf requestHistryList];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)upRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshFooter:^{
        weakSelf.page++;
        [weakSelf requestHistryList];
    }];
    self.tableView.mj_footer.hidden = YES;
}

-(void)endRefresh
{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestHistryList {
    self.shouldShowEmptyView = YES;
    
    NSString * pageString = [NSString stringWithFormat:@"%ld",self.page];
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    if (self.symbolString.length) {
        parameters[@"symbol"] = self.symbolString;
    }
    parameters[@"page"] = pageString;
    parameters[@"state"] = @"0";
    parameters[@"limit"] = @"8";
    
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSHistoricalTransactionURL params:parameters HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        [self endRefresh];
        if (state.status) {
            if (self.page == 1) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary * dataDict = dic[@"data"];
            NSArray * dataArr = dataDict[@"data"];
            if (dataArr.count < [dataDict[@"per_page"] integerValue]) {
                self.tableView.mj_footer.hidden = YES;
            } else {
                self.tableView.mj_footer.hidden = NO;
            }
            
            NSArray * datas = [JYSHistoricalTransactionModel mj_objectArrayWithKeyValuesArray:dataArr];
            
            [self.dataArray addObjectsFromArray:datas];
            
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } fail:^(NSError *error) {
        [self endRefresh];
    } showHUD:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSHistoryDealCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataArray.count) {
        JYSHistoricalTransactionModel * model = self.dataArray[indexPath.row];
        [cell setHistoricalDataWithModel:model];
    }
//    [cell setDataWithStyle:indexPath.row%2];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(93);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}
//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"暂无历史成交信息");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}



@end
