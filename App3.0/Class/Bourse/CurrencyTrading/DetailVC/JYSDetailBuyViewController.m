//
//  JYSDetailBuyViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSDetailBuyViewController.h"
#import "JYSTradingBuyOrSoldView.h"
#import "SRWebSocketTool.h"
#import "JYSCurrencyTradingModel.h"

@interface JYSDetailBuyViewController ()<JYSTradingBuyOrSoldViewDelegate>

/** 买入卖出View */
@property (nonatomic, strong) JYSTradingBuyOrSoldView * buyOrSoldView;

/** 买数组 */
@property (nonatomic, strong) NSArray * buyDatasArray;
/** 卖数组 */
@property (nonatomic, strong) NSArray * soldDatasArray;


@property (nonatomic, copy) NSString * ucoin;//买
@property (nonatomic, copy) NSString * gcoin;


@property (nonatomic, copy) NSString * coin_id;
@property (nonatomic, copy) NSString * number;
@property (nonatomic, copy) NSString * locked;

@property (nonatomic, assign) NSInteger depInter;
@end

@implementation JYSDetailBuyViewController


- (JYSTradingBuyOrSoldView *)buyOrSoldView {
    if (_buyOrSoldView == nil) {
        _buyOrSoldView = [[JYSTradingBuyOrSoldView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FontNum(520)) themeStyle:YES];
        _buyOrSoldView.delegate = self;
    }
    return _buyOrSoldView;
}

- (void)setBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray depStr:(NSString*)depStr{
    self.buyDatasArray = buyArr;
    self.soldDatasArray = soldArray;
    self.depInter = [depStr integerValue];
    
    [self.buyOrSoldView setBuyArray:buyArr soldArray:soldArray depStr:depStr];
}
- (void)setBuyClosePrice:(NSString*)price
{
    if (price.floatValue<=0) {
        self.buyOrSoldView.closePrice = @"- -";
        return;
    }
    self.buyOrSoldView.closePrice = [NSString stringWithFormat:@"%.6f",[price doubleValue]];
    
    self.buyOrSoldView.closePrice = [self.buyOrSoldView.closePrice substringWithRange:NSMakeRange(0, self.buyOrSoldView.closePrice.length-6+self.depInter)];
}
-(void)setDecimalPlacesArray:(NSArray *)decimalPlacesArray
{
    _decimalPlacesArray = decimalPlacesArray;
    [self.buyOrSoldView setTheDecimalPlacesDataArray:self.decimalPlacesArray];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    self.decimalPlacesArray = @[@"3位小数点",@"4位小数点",@"5位小数点",@"6位小数点"];
    
    [self setUpUI];
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [self.buyOrSoldView setTheDecimalPlacesDataArray:self.decimalPlacesArray];
    //    });
    
    //    [self.buyOrSoldView setTheDecimalPlacesDataArray:self.decimalPlacesArray];
    
    [self.buyOrSoldView.buySoldButton addTarget:self action:@selector(buySoldButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    __weak __typeof__(self) wSelf = self;
    self.buyOrSoldView.floatBlock = ^(CGFloat percent, BOOL idSelect) {
        //    }
        //    self.buyOrSoldView.floatBlock = ^(CGFloat percent) {
        if (wSelf.number.floatValue>0&&!isEmptyString(self.buyOrSoldView.limitSheetTF.text)&&percent>0) {
            //单价
            CGFloat price = wSelf.buyOrSoldView.limitSheetTF.text.doubleValue;
            
            //可购买数量
            CGFloat avilable_amount = (wSelf.number.doubleValue*percent)/price;
            
            wSelf.buyOrSoldView.valuationTF.text = [NSString stringWithFormat:@"%.8f", avilable_amount];
            //根据后台返回的小数位数显示
            if (wSelf.buyOrSoldView.valuationTF.text.length >wSelf.buyOrSoldView.valuationTF.text.length-8+wSelf.bInfoModel.amount_precision.intValue) {
                wSelf.buyOrSoldView.valuationTF.text = [wSelf.buyOrSoldView.valuationTF.text substringWithRange:NSMakeRange(0, wSelf.buyOrSoldView.valuationTF.text.length-8+wSelf.bInfoModel.amount_precision.intValue)];
                
            }
//            wSelf.buyOrSoldView.dealAmountTF.text = [NSString stringWithFormat:@"%.6f",wSelf.number.doubleValue*percent];
            wSelf.buyOrSoldView.dealAmountTF.text = [NSString stringWithFormat:@"%.6f",wSelf.buyOrSoldView.valuationTF.text.doubleValue*price];

            //
            wSelf.buyOrSoldView.dealAmountTF.text = [wSelf.buyOrSoldView.dealAmountTF.text substringWithRange:NSMakeRange(0, wSelf.buyOrSoldView.dealAmountTF.text.length-6+wSelf.depInter)];
        }else{
            wSelf.buyOrSoldView.valuationTF.text =[NSString stringWithFormat:@""];
            wSelf.buyOrSoldView.dealAmountTF.text =[NSString stringWithFormat:@""];
            
        }
        
        //        if (!isEmptyString(self.buyOrSoldView.limitSheetTF.text)) {
        //            //成交金额
        //            CGFloat dealPrice = self.buyOrSoldView.limitSheetTF.text.floatValue*self.buyOrSoldView.valuationTF.text.floatValue;
        //            if (dealPrice >0) {
        //                wSelf.buyOrSoldView.dealAmountTF.text = [NSString stringWithFormat:@"%.6f",dealPrice];
        //            }else{
        //                wSelf.buyOrSoldView.dealAmountTF.text =[NSString stringWithFormat:@"0"];
        //            }
        //
        //        }else{
        //            wSelf.buyOrSoldView.dealAmountTF.text =[NSString stringWithFormat:@""];
        //        }
    };
    
}
-(void)setBInfoModel:(JYSSymbolBInfoModel *)bInfoModel
{
    _bInfoModel = bInfoModel;
    self.buyOrSoldView.bInfoModel = bInfoModel;
    
    self.buyOrSoldView.limitSheetTF.text = @"";
    self.buyOrSoldView.valuationTF.text = @"";
    self.buyOrSoldView.dealAmountTF.text = @"";
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.buyOrSoldView.limitSheetTF.text = @"";
    self.buyOrSoldView.valuationTF.text = @"";
    self.buyOrSoldView.dealAmountTF.text = @"";
}
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    self.tableView.tableHeaderView.xs_height = FontNum(580);
    self.tableView.tableHeaderView = self.buyOrSoldView;
}

- (void)isChooseDecimalPlacesOpen:(BOOL)isOpen {
    //    float headerHeight;
    //    if (isOpen) {
    ////        headerHeight = FontNum(580)+210;
    //        headerHeight = FontNum(580);
    //    } else {
    //        headerHeight = FontNum(580);
    //    }
    //    _buyOrSoldView.frame = CGRectMake(0, 0, SCREEN_WIDTH, headerHeight);
    //    self.tableView.tableHeaderView.xs_height = headerHeight;
    //
    //    CGPoint offset = self.tableView.contentOffset;
    //    if (isOpen) {
    //        offset.y = 0;
    ////        offset.y = 210;
    //        [self.tableView setContentOffset:offset animated:YES];
    //    }
    //
    //    [self.tableView reloadData];
}

-(void)setSelCoinModel:(JYSCurrencyTradingModel *)selCoinModel {
    _selCoinModel = selCoinModel;
    self.ucoin = selCoinModel.ucoin;
    self.gcoin = selCoinModel.gcoin;
    //[NSString stringWithFormat:@"%@",self.selCoinDic[@"gcoin"]];
    self.buyOrSoldView.valuationTF.text = @"";
    self.buyOrSoldView.limitSheetTF.text = @"";
    
    //    self.buyOrSoldView.valuationTF.placeholder = [NSString stringWithFormat:@"数量[%@]",[self.ucoin uppercaseString]];
    self.buyOrSoldView.valuationTF.placeholder = [NSString stringWithFormat:Localized(@"exchange_volume")];
    
    self.buyOrSoldView.dealAmountTF.placeholder = [NSString stringWithFormat:@"%@[%@]",Localized(@"buy_sell_num_hint"),[self.ucoin uppercaseString]];
    
    
    
    [self getCoinRemain];
}
//-(void)setSelCoinDic:(NSDictionary *)selCoinDic{
//
//    _selCoinDic = selCoinDic;
//    self.ucoin = [NSString stringWithFormat:@"%@",self.selCoinDic[@"ucoin"]];
//    self.gcoin = [NSString stringWithFormat:@"%@",self.selCoinDic[@"gcoin"]];
//    self.buyOrSoldView.valuationTF.text = @"";
//    self.buyOrSoldView.limitSheetTF.text = @"";
//
//    //    self.buyOrSoldView.valuationTF.placeholder = [NSString stringWithFormat:@"数量[%@]",[self.ucoin uppercaseString]];
//    self.buyOrSoldView.valuationTF.placeholder = [NSString stringWithFormat:Localized(@"exchange_volume")];
//
//    self.buyOrSoldView.dealAmountTF.placeholder = [NSString stringWithFormat:@"成交金额[%@]",[self.ucoin uppercaseString]];
//
//
//
//    [self getCoinRemain];
//
//}



//买入的话查询 ucoin 余额 ，卖出查询gcoin 余额
- (void)getCoinRemain{
    [self.view endEditing:YES];
    if (self.selCoinModel) {
        //        NSString *currency = [NSString stringWithFormat:@"%@",self.selCoinDic[@"ucoin"]];
        NSDictionary *param = @{
                                @"currency":self.selCoinModel.ucoin
                                };
        __weak __typeof__(self) wSelf = self;
        
        [JYSAFNetworking getOrPostWithType:POST withUrl:JYSTradeWalletRemainURL params:param HUDShowView:self.view HUDAnimated:NO success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                NSDictionary *dicData = dic[@"data"];
                if ([dicData isKindOfClass:[NSDictionary class]]) {
                    wSelf.coin_id = [NSString stringWithFormat:@"%@",dicData[@"coin_id"]];
                    wSelf.number = [NSString stringWithFormat:@"%@",dicData[@"number"]];
                    wSelf.locked = [NSString stringWithFormat:@"%@",dicData[@"locked"]];
                   
                    wSelf.buyOrSoldView.canUseLabel.text = [NSString stringWithFormat:@"%@%@",wSelf.number,[wSelf.ucoin uppercaseString]];
                    if (wSelf.number.floatValue <=0) {
                        wSelf.buyOrSoldView.canUseLabel.text = [NSString stringWithFormat:@"%@%@",@"0",[wSelf.ucoin uppercaseString]];
                    }
                }
            }else{
                Alert(state.info);
                wSelf.buyOrSoldView.canUseLabel.text = [NSString stringWithFormat:@"%@%@",@"0",[wSelf.ucoin uppercaseString]];
                
            }
        } fail:^(NSError *error) {
            
        } showHUD:NO];
    }
    
}


- (BOOL)checkTextField
{
    if (!self.selCoinModel) {
        Alert(@"获取币种信息失败!");
        return NO;
    }
    if (isEmptyString(self.buyOrSoldView.limitSheetTF.text)) {
        Alert(Localized(@"trade_prince_input_hint"));
        return NO;
    }
    
    if (isEmptyString(self.buyOrSoldView.valuationTF.text)) {
        Alert(Localized(@"input_buy_sell_num_hint"));
        return NO;
    }
    if (self.buyOrSoldView.valuationTF.text&&self.bInfoModel) {
        //        if (self.buyOrSoldView.valuationTF.text.floatValue <self.bInfoModel.buy_min.floatValue) {
        //            NSString *minStr = [NSString stringWithFormat:@"%@%@",Localized(@"最小买入数量"),self.bInfoModel.buy_min];
        //            Alert(minStr);
        //            return NO;
        //        }
        //        if (self.buyOrSoldView.valuationTF.text.floatValue >self.bInfoModel.buy_min.floatValue) {
        //            NSString *maxStr = [NSString stringWithFormat:@"%@%@",Localized(@"最大买入数量"),self.bInfoModel.buy_max];
        //            Alert(maxStr);
        //            return NO;
        //        }
    }
    return YES;
}

/*参数
 string symbol //货币对名称  btccnys
 string amount  // 交易数量  必填   整数类型
 string price   //委托价格  必填
 */
//为了你的账户安全，建议你进行身份认证
//买入
- (void)buySoldButtonClick {
    [self.view endEditing:YES];
    if (![self checkTextField]) { return; }
    // 获取本地存储的guid
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *certKey = [ud objectForKey:APPCERTAUTH_KEY];
    //    NSLog(@"guid = %@",guid);
    if (!certKey) {
        certKey = @"0";
    }
    if (self.bInfoModel.user_approve.intValue != 2) {//0未认证，1审核中，2已认证
        if (self.bInfoModel.switch_user_approve.intValue>0) {
        
            [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"请先进行实名认证") message:nil CallBackBlock:^(NSInteger btnIndex) {
                    [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
            } cancelButtonTitle:nil destructiveButtonTitle:Localized(@"去认证") otherButtonTitles:nil];
            return;
        }else{
            if (self.bInfoModel.user_approve.intValue != 2&&(certKey.intValue == 0)) {
                [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"为了你的账户安全，建议你进行身份认证") message:nil CallBackBlock:^(NSInteger btnIndex) {
                    if (btnIndex == 1) {
                        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
                    }else if (btnIndex == 0) {
                        // 将guid存到本地沙盒
                        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                        [ud setObject:@"1" forKey:APPCERTAUTH_KEY];
                        [ud synchronize];
                    }
                } cancelButtonTitle:Localized(@"不再提醒") destructiveButtonTitle:Localized(@"去认证") otherButtonTitles:nil];
                return;
            }
        }
    }
    [self tradeBuyRequst];
}
- (void)tradeBuyRequst
{
    
    if (self.selCoinModel) {
        //        NSString *symbol = [NSString stringWithFormat:@"%@",self.selCoinDic[@"symbol"]];
        NSDictionary *param = @{
                                @"symbol":self.selCoinModel.symbol,
                                @"amount":self.buyOrSoldView.valuationTF.text,
                                @"price":self.buyOrSoldView.limitSheetTF.text
                                };
        __weak __typeof__(self) wSelf = self;
        
        [JYSAFNetworking getOrPostWithType:POST withUrl:JYSTradeBuyURL params:param HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
            if (state.status) {
                self.buyOrSoldView.limitSheetTF.text = @"";
                NSString *alertText = Localized(@"user_approve_success");
                if (state.info) {
                    alertText = state.info;
                }
                [XSTool showToastWithView:wSelf.view Text:alertText];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (wSelf.refreshBlock) {
                        wSelf.refreshBlock(YES);
                    }
                });
            }else{
                [XSTool showToastWithView:wSelf.view Text:state.info];
            }
        } fail:^(NSError *error) {
            
        } showHUD:YES];
    }
}


-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end

