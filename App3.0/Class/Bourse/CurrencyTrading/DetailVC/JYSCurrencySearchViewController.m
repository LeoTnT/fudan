//
//  JYSCurrencySearchViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencySearchViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "JYSCurrencyTradingModel.h"
#import "NSPredicate+Search.h"

@interface JYSCurrencySearchViewController ()<UITextFieldDelegate>

//@property (strong, nonatomic) UISearchController *searchController;
//
@property (strong, nonatomic) NSMutableArray *searchList; // 保存搜索数据

/** 搜索框 */
@property (nonatomic, strong) UITextField * searchTF;

@end

@implementation JYSCurrencySearchViewController

//- (UISearchController *)searchController {
//    if (_searchController == nil) {
//        // 传入 nil 默认在原视图展示
//        _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
//        // 成为代理
//        _searchController.searchBar.enablesReturnKeyAutomatically = NO;
//        _searchController.searchResultsUpdater = self;
//        _searchController.hidesNavigationBarDuringPresentation = YES;
//        [_searchController.searchBar sizeToFit];
//        // 搜索时是否出现遮罩
//        _searchController.dimsBackgroundDuringPresentation = NO;
//        //去除黑线
//        [_searchController.searchBar setBackgroundImage:[UIImage imageNamed:@"line_g"]];
//        // 设置搜索框内部textField的背景图
//        //[_searchController.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//        // 设置搜索框背景图
//        //  [_searchController.searchBar setBackgroundImage:[UIImage imageNamed:@"search"]];
//        // 设置搜索框内放大镜图片
//        [_searchController.searchBar setImage:[UIImage imageNamed:@"jys_search_2"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
//        // 设置搜索框内按钮文字颜色，以及搜索光标颜色。
//        _searchController.searchBar.tintColor= XSYCOLOR(0x6A8EBD);
//        // 设置搜索框背景颜色
//        _searchController.searchBar.barTintColor = [UIColor whiteColor];
//        _searchController.searchBar.placeholder = @"搜索币种";
//    }
//    return _searchController;
//}

- (NSMutableArray *)searchList {
    if (!_searchList) {
        _searchList = [NSMutableArray array];
    }
    return _searchList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavigationBar];
    
    [self setUpUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;

    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    //TODO: 页面appear 禁用
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
    
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}
- (void)setUpNavigationBar {
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"jys_search_2" highImage:@"jys_search_2" target:nil action:nil];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:XSYCOLOR(0x6A8EBD) titleFont:[UIFont systemFontOfSize:16] target:self action:@selector(cancelButtonClicked) titleString:Localized(@"cancel_btn")];
    
    self.searchTF = [XSUITool creatTextFieldWithFrame:CGRectZero backgroundColor:nil font:FontNum(14) textAlignment:NSTextAlignmentLeft delegate:self placeholderColor:XSYCOLOR(0xD1D5DD) textColor:JYSMainTextColor placeholder:Localized(@"seach_hiht") text:nil];

    self.searchTF.delegate = self;
    self.searchTF.frame = CGRectMake(0, 0, FontNum(290), 30);
    self.searchTF.returnKeyType = UIReturnKeySearch;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textVauleChanged:)name:UITextFieldTextDidChangeNotification object:self.searchTF];
    
    self.navigationItem.titleView = self.searchTF;
    
}

- (void)textVauleChanged:(NSNotification *)notification {
    UITextField * textField = notification.object;
    
    if (textField == self.searchTF) {
        
        if ([UserInstance ShardInstnce].coinDatasArray.count) {
            
            JYSCurrencyTradingModel * model = [UserInstance ShardInstnce].coinDatasArray[0];
            XSLog(@"%@",model.symbol);
            
//            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"symbol CONTAINS[cd] %@",self.searchTF.text]; //predicate只能是对象
//
//            NSArray * filteredArray = [[UserInstance ShardInstnce].coinDatasArray filteredArrayUsingPredicate:predicate];
            
            [self.searchList removeAllObjects];
            
            NSPredicate *predicate = [NSPredicate predicateWithSearch:textField.text searchTerm:@"symbol"];
            
            NSArray * dataArr = [[UserInstance ShardInstnce].coinDatasArray filteredArrayUsingPredicate:predicate];
            [self.searchList addObjectsFromArray:dataArr];
            [self.tableView reloadData];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self requestMenuData:textField.text];
    
    return YES;
}


- (void)requestMenuData:(NSString *)searchText {
    [self.view endEditing:YES];
    [self.searchTF resignFirstResponder];
    
    if (isEmptyString(searchText)) {
        searchText = @"";
    }
    NSDictionary *param = @{
                            @"search":searchText
                            };
    __weak __typeof__(self) wSelf = self;
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSTradeSearchURL params:param HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if ([dic[@"status"] integerValue] == 1) {
            NSArray *dataArray = dic[@"data"];
            if ([dataArray isKindOfClass:[NSArray class]]) {
                [wSelf.searchList removeAllObjects];
                NSArray * datas = [JYSCurrencyTradingModel mj_objectArrayWithKeyValuesArray:dataArray];
                [wSelf.searchList addObjectsFromArray:datas];
                [wSelf.tableView reloadData];
            }
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}

- (void)cancelButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpUI {
    
    self.navigationController.navigationBar.hidden = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = BG_COLOR;
    
    self.tableViewStyle = UITableViewStylePlain;
    //    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    //分别表示距离上边界，左边界，下边界，右边界的位移
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kStatusBarAndNavigationBarHeight, 0, -kStatusBarAndNavigationBarHeight, 0));
    }];
    
    
    //    self.tableView.tableHeaderView = self.searchController.searchBar;
}

//-(void)viewDidLayoutSubviews {
//    if(self.searchController.active) {
//        [self.tableView setFrame:CGRectMake(0, 20, mainWidth, mainHeight -20)];
//    }else {
//        self.tableView.frame =self.view.bounds;
//    }
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"ID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:FontNum(15) weight:UIFontWeightMedium];
    cell.textLabel.textColor = JYSMainTextColor;
    if (self.searchList.count) {
        JYSCurrencyTradingModel * model = self.searchList[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@/%@",[model.gcoin uppercaseString],[model.ucoin uppercaseString]];
        
//        // 返回搜索后的数组
//        NSDictionary *dict = self.searchList[indexPath.row];
//        if ([dict isKindOfClass:[NSDictionary class]]) {
//            NSString *gcoin = [NSString stringWithFormat:@"%@",dict[@"gcoin"]];
//            NSString *ucoin = [NSString stringWithFormat:@"%@",dict[@"ucoin"]];
//
//            cell.textLabel.text = [NSString stringWithFormat:@"%@/%@",[gcoin uppercaseString],[ucoin uppercaseString]];
//        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.searchTF resignFirstResponder];
    if (self.searchList.count) {
        // 返回搜索后的数组
//        NSDictionary *dict = self.searchList[indexPath.row];
        JYSCurrencyTradingModel * model = self.searchList[indexPath.row];
        if (self.searchBlock) {
            self.searchBlock(model);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.searchTF];
}

@end

