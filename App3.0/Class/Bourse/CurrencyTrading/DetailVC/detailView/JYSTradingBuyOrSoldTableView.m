//
//  JYSTradingBuyOrSoldTableView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSTradingBuyOrSoldTableView.h"

@interface JYSTradingBuyOrSoldTableView ()<UITableViewDelegate,UITableViewDataSource>

/** trading tableView */
@property (nonatomic, strong) UITableView * tradingTableView;

/** dataArray */
@property (nonatomic, strong) NSArray * dataArray;
@property (nonatomic, strong) NSString * depStr;

@end

@implementation JYSTradingBuyOrSoldTableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

static NSString * const cellID = @"JYSTradingBuyOrSoldCellID";
- (void) setUpUI {
    self.tradingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.xs_width, self.xs_height) style:UITableViewStylePlain];
    self.tradingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tradingTableView.backgroundColor = [UIColor whiteColor];
    self.tradingTableView.showsVerticalScrollIndicator = NO;
    [self.tradingTableView registerClass:[JYSTradingBuyOrSoldCell class] forCellReuseIdentifier:cellID];
    self.tradingTableView.delegate = self;
    self.tradingTableView.dataSource = self;
    [self addSubview:self.tradingTableView];
    [self.tradingTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)setDataWithArray:(NSArray *)arr maxFloat:(CGFloat)maxFloat depStr:(NSString*)depStr{
    self.dataArray = arr;
    
    //    NSArray *dataArr = [self getMovementsArray:self.dataArray];
    //    self.maxFloat = [self getMaxValueForAry:dataArr];
    //
    self.maxFloat = maxFloat;
    self.depStr = depStr;
    NSLog(@"depStr==%@",depStr);
    [self.tradingTableView reloadData];
    
    
}

- (NSArray *)getMovementsArray:(NSArray *)dataArr {
    NSMutableArray * movementsArray = [NSMutableArray array];
    for (NSInteger i = 0; i < dataArr.count; i++) {
        NSArray * solddatas = dataArr[i];
        NSString * countString = [NSString stringWithFormat:@"%@",solddatas[1]];
        [movementsArray addObject:countString];
    }
    
    return movementsArray;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSTradingBuyOrSoldCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //        [cell setCellDataWithArray:self.dataArray[indexPath.row]];
    
    CGFloat cellPercent = 0;
    NSString *priceStr = @"";
    NSString *countStr = @"";
    if (self.dataArray.count) {
        if (self.isBuyView) {
            if (indexPath.row <self.dataArray.count) {
                
                NSArray *cellArray = self.dataArray[indexPath.row];
                priceStr = [NSString stringWithFormat:@"%@",cellArray[0]];
                countStr = [NSString stringWithFormat:@"%@",cellArray[1]];
                if (self.maxFloat) {
                    cellPercent = countStr.floatValue/self.maxFloat;
                }
            }
            
            
        }else{
            
            if ((5-indexPath.row-1) <self.dataArray.count) {
                
                NSArray *cellArray = self.dataArray[(5-indexPath.row-1)];
                priceStr = [NSString stringWithFormat:@"%@",cellArray[0]];
                countStr = [NSString stringWithFormat:@"%@",cellArray[1]];
                if (self.maxFloat) {
                    cellPercent = countStr.floatValue/self.maxFloat;
                }
            }
            
        }
        
        
        
    }
    cell.countLabel.text = countStr;
    cell.percent = cellPercent;
    [cell setThemeColorWithStyle:self.isBuyView];
    cell.priceLabel.text = priceStr;
    if (!isEmptyString(priceStr)) {
        switch (self.depStr.intValue) {
            case 2:
                cell.priceLabel.text = [NSString stringWithFormat:@"%.2f",priceStr.doubleValue];
                break;
            case 3:
                cell.priceLabel.text = [NSString stringWithFormat:@"%.3f",priceStr.doubleValue];
                break;
            case 4:
                cell.priceLabel.text = [NSString stringWithFormat:@"%.4f",priceStr.doubleValue];
                break;
            case 5:
                cell.priceLabel.text = [NSString stringWithFormat:@"%.5f",priceStr.doubleValue];
                break;
            case 8:
                cell.priceLabel.text = [NSString stringWithFormat:@"%.8f",priceStr.doubleValue];
                break;
                //        case 3:
                //            cell.priceLabel.text = [NSString stringWithFormat:@"%.3f",priceStr.doubleValue];
                //            break;
                
            default:
                break;
        }
    }else{
        cell.priceLabel.text = @"- -";
    }
    
    if (!isEmptyString(countStr)) {
        switch (self.depStr.intValue) {
            case 2:
                cell.countLabel.text = [NSString stringWithFormat:@"%.2f",countStr.doubleValue];
                break;
            case 3:
                cell.countLabel.text = [NSString stringWithFormat:@"%.3f",countStr.doubleValue];
                break;
            case 4:
                cell.countLabel.text = [NSString stringWithFormat:@"%.4f",countStr.doubleValue];
                break;
            case 5:
                cell.countLabel.text = [NSString stringWithFormat:@"%.5f",countStr.doubleValue];
                break;
            case 8:
                cell.countLabel.text = [NSString stringWithFormat:@"%.8f",countStr.doubleValue];
                break;
                //        case 3:
                //            cell.priceLabel.text = [NSString stringWithFormat:@"%.3f",priceStr.doubleValue];
                //            break;
                
            default:
                break;
        }
    }else{
        cell.countLabel.text = @"- -";
    }
    
    
    
    return cell;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(33);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *priceStr = @"";
    NSString *volume = @"";
    
    if (self.isBuyView) {
        if (indexPath.row <self.dataArray.count) {
            NSArray *cellArray = self.dataArray[indexPath.row];
            priceStr = [NSString stringWithFormat:@"%@",cellArray[0]];
            volume = [NSString stringWithFormat:@"%@",cellArray[1]];
            
        }
    }else{
        if ((5-indexPath.row-1) <self.dataArray.count) {
            NSArray *cellArray = self.dataArray[(5-indexPath.row-1)];
            priceStr = [NSString stringWithFormat:@"%@",cellArray[0]];
            volume = [NSString stringWithFormat:@"%@",cellArray[1]];
        }
    }
    
    NSIndexPath *tIndexPath = [tableView indexPathForSelectedRow];
    
    JYSTradingBuyOrSoldCell * cell = (JYSTradingBuyOrSoldCell *)[tableView cellForRowAtIndexPath:tIndexPath];
    
//NSLog(@"priceLabel=%@,priceLabel=%@",cell.priceLabel.text,cell.countLabel.text);
//    if (!isEmptyString(cell.priceLabel.text) || [cell.priceLabel.text isEqualToString:@"- -"]) {
//        self.priceBlock(cell.priceLabel.text,cell.countLabel.text);
//    }
    if ([cell.priceLabel.text doubleValue] > 0) {
        self.priceBlock(cell.priceLabel.text,cell.countLabel.text);
    }
}


-(CGFloat)getMaxValueForAry:(NSArray *)ary{
    if (ary.count <=0) {
        return 0;
        
    }
    CGFloat max = [ary[0] doubleValue];
    for (int i=0; i<ary.count; i++) {
        if (max < [ary[i] doubleValue]) {
            max = [ary[i] doubleValue];
        }
    }
    return max;
}
@end

@interface JYSTradingBuyOrSoldCell ()



@end


@implementation JYSTradingBuyOrSoldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setThemeColorWithStyle:(BOOL)isBuyView {
    self.themeColor = isBuyView?[UserInstance ShardInstnce].roseColor:[UserInstance ShardInstnce].fellColor;
    
    self.themeColor_a = isBuyView?[UIColor hexFloatColor:[UserInstance ShardInstnce].roseColorString alpha:0.1]:[UIColor hexFloatColor:[UserInstance ShardInstnce].fellColorString alpha:0.1];
    
    self.priceLabel.textColor = self.themeColor;
    self.bgView.backgroundColor = self.themeColor_a;
    
    //    double bgWidth = (double)arc4random() / 0x100000000 * self.contentView.xs_width;
    double bgWidth = self.percent * self.contentView.xs_width;
    
    [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(bgWidth);
    }];
}

- (void)setUpUI {
    self.bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:nil];
    [self.contentView addSubview:self.bgView];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(FontNum(21));
        make.width.mas_equalTo(CGFLOAT_MIN);
    }];
    
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:self.themeColor titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.074966"];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(0);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.countLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.586"];
    [self.contentView addSubview:self.countLabel];
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(0);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
}

- (void)setCellDataWithArray:(NSArray *)cellArray {
    if (cellArray.count >1) {
        self.priceLabel.text = [NSString stringWithFormat:@"%@",cellArray[0]];
        self.countLabel.text = [NSString stringWithFormat:@"%@",cellArray[1]];
    }
}




@end

