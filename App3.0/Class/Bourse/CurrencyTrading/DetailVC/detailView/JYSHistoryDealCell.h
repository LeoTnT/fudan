//
//  JYSHistoryDealCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSHistoricalTransactionModel;
@interface JYSHistoryDealCell : UITableViewCell

- (void)setHistoricalDataWithModel:(JYSHistoricalTransactionModel *)model;

@end
