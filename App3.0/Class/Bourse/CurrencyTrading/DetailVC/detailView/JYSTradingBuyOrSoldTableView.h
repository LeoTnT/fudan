//
//  JYSTradingBuyOrSoldTableView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSTradingBuyOrSoldTableView : UIView

/** 买入or卖出界面 */
@property (nonatomic, assign) BOOL isBuyView;
@property (nonatomic, assign) CGFloat maxFloat;
@property (nonatomic,copy) void(^priceBlock)(NSString *price,NSString *volume);

- (void)setDataWithArray:(NSArray *)arr maxFloat:(CGFloat)maxFloat depStr:(NSString*)depStr;

@end


@interface JYSTradingBuyOrSoldCell : UITableViewCell

/** price */
@property (nonatomic, strong) UILabel * priceLabel;
/** 数量 */
@property (nonatomic, strong) UILabel * countLabel;
/** 背景图 */
@property (nonatomic, strong) UIView * bgView;

/** 主题颜色 */
@property (nonatomic, strong) UIColor * themeColor;
/** 主题颜色(透明属性) */
@property (nonatomic, strong) UIColor * themeColor_a;
- (void)setCellDataWithArray:(NSArray *)cellArray;

- (void)setThemeColorWithStyle:(BOOL)isBuyView;


@property (nonatomic, assign) CGFloat percent;


@end

