//
//  JYSTradingBuyOrSoldView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYSSymbolBInfoModel.h"

@protocol JYSTradingBuyOrSoldViewDelegate <NSObject>

- (void)isChooseDecimalPlacesOpen:(BOOL)isOpen;

@end

@interface JYSTradingBuyOrSoldView : UIView

/** 代理 */
@property (nonatomic, weak) id<JYSTradingBuyOrSoldViewDelegate>delegate;

/** 可用 */
@property (nonatomic, strong) UILabel * canUseLabel;

/** 价格 */
@property (nonatomic, copy) NSString * closePrice;



/** 限价单TextField */
@property (nonatomic, strong) UITextField * limitSheetTF;
/**数量 */
@property (nonatomic, strong) UITextField * valuationTF;
/** 选中按钮 */
@property (nonatomic, strong) UIButton * selectPercentileBtn;
/** 成交金额 */
@property (nonatomic, strong) UITextField * dealAmountTF;

/** 买入||卖出按钮 */
@property (nonatomic, strong) UIButton * buySoldButton;



/** 市场的常用信息(数量价格的小数精度等） */
@property (nonatomic, strong) JYSSymbolBInfoModel * bInfoModel;

@property (nonatomic,copy) void(^floatBlock)(CGFloat percent, BOOL idSelect);





/**
 * isbuy yes->买 no->卖
 */
- (instancetype)initWithFrame:(CGRect)frame themeStyle:(BOOL)isBuy;

- (void)setBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray depStr:(NSString*)depStr;

- (void)setTheDecimalPlacesDataArray:(NSArray *)decimalPlaces;

- (void)hideDecimalPlaces;

@end

