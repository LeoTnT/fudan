//
//  JYSCommissionedCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCommissionedCell.h"
#import "JYSEntrustOrdersModel.h"
#import "XSFormatterDate.h"

@interface JYSCommissionedCell ()

/** 买入或卖出 */
@property (nonatomic, strong) UILabel * buyLab;

/** 币名 */
@property (nonatomic, strong) UILabel * coinNameLabel;
/** 数量 */
@property (nonatomic, strong) UILabel * amountLabel;
/** 价格 */
@property (nonatomic, strong) UILabel * priceLabel;
/** 时间 */
@property (nonatomic, strong) UILabel * timeLabel;
/** 撤单按钮 */
@property (nonatomic, strong) UIButton * cancelButton;

@end

@implementation JYSCommissionedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.backgroundColor = ;
    
    UIView * bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 15));
    }];
    
    UIImageView * bgImageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"jys_cell_bgImage"]];
    [self.contentView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-2, 2, -5, 2));
    }];
    
    self.buyLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:[UserInstance ShardInstnce].roseColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"buy")];
    [self.contentView addSubview:self.buyLab];
    [self.buyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(13);
        make.baseline.mas_equalTo(bgView.mas_centerY).multipliedBy(0.6);
    }];
    
    UILabel * amountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"exchange_volume")];
    [self.contentView addSubview:amountLab];
    [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyLab);
        make.centerY.mas_equalTo(bgView);
    }];
    
    UILabel * priceLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"price_exchange")];
    [self.contentView addSubview:priceLab];
    [priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyLab);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.35);
    }];
    
    self.coinNameLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"ETH/BTC"];
    [self.contentView addSubview:self.coinNameLabel];
    [self.coinNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyLab.mas_right).offset(10);
        make.centerY.mas_equalTo(self.buyLab);
    }];
    
    self.amountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.62/200.00"];
    [self.contentView addSubview:self.amountLabel];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(amountLab.mas_right);
        make.centerY.mas_equalTo(amountLab);
    }];
    
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"5269.700522"];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coinNameLabel);
        make.centerY.mas_equalTo(priceLab);
    }];
    
    self.timeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2018-03-24 16:12"];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView.mas_right).offset(-12);
        make.centerY.mas_equalTo(self.buyLab);
    }];
    
    self.cancelButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(cancelButtonClick) titleColor:[UIColor whiteColor] titleFont:FontNum(13) backgroundColor:JYSMainSelelctColor image:nil backgroundImage:nil title:Localized(@"order_cancel_btn")];
    
//    [self.cancelButton setBackgroundColor:XSYCOLOR(0x6A8EBD)];
//    self.cancelButton.userInteractionEnabled = NO;
    [self.contentView addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView.mas_right).offset(-12);
        make.bottom.mas_equalTo(priceLab.mas_baseline);
        make.size.mas_equalTo(CGSizeMake(FontNum(60), FontNum(27)));
    }];
}

- (void)setOrderDataWithModel:(JYSEntrustOrdersModel *)model {
    self.coinNameLabel.text = [NSString stringWithFormat:@"%@/%@",model.gcoin,model.ucoin];
    if ([model.type integerValue] == 1) {
        self.buyLab.text = Localized(@"selltwo");
        self.buyLab.textColor = [UserInstance ShardInstnce].fellColor;
    } else {
        self.buyLab.text = Localized(@"buy");
        self.buyLab.textColor = [UserInstance ShardInstnce].roseColor;
    }

    self.amountLabel.text = [NSString stringWithFormat:@"%@/%@",model.field_amount,model.amount];
    self.priceLabel.text = model.price;
//    self.timeLabel.text = model.created_time;
    
    XSLog(@"%@",model.created_time);
    
    NSString * timeString =[NSString stringWithFormat:@"%ld",[model.created_time integerValue]/1000000000];
    
    self.timeLabel.text = [XSFormatterDate timeWithStyle:XSFormatterDateStyleDefault timeFormat:nil intervalString:timeString];
    
    if ([model.status integerValue] == 0 || [model.status integerValue] == 1 || [model.status integerValue] == 2) {
        [self.cancelButton setBackgroundColor:JYSMainSelelctColor];
        self.cancelButton.userInteractionEnabled = YES;
        [self.cancelButton setTitle:Localized(@"order_cancel_btn") forState:UIControlStateNormal];
//        self.cancelButton.hidden = NO;
    } else {
//        self.cancelButton.hidden = YES;
        [self.cancelButton setBackgroundColor:XSYCOLOR(0x6A8EBD)];
        self.cancelButton.userInteractionEnabled = NO;
        if ([model.status integerValue] == 3 || [model.status integerValue] == 5) {
            [self.cancelButton setTitle:Localized(@"已撤单") forState:UIControlStateNormal];
        } else if ([model.status integerValue] == 4) {
            [self.cancelButton setTitle:Localized(@"orders_tab_over") forState:UIControlStateNormal];
        }
    }
}

- (void)cancelButtonClick {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

@end
