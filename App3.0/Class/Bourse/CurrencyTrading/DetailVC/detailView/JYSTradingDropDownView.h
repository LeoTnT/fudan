//
//  JYSTradingDropDownView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^JYSTradingDropDownBlock)(NSDictionary * rightDict,NSString * leftSelectedString,NSUInteger rightSelectedIndex);

@interface JYSTradingDropDownView : UIView

- (void)setTopMenuData:(NSDictionary *)topMenuDatas leftSelectString:(NSString *)leftSelectString rightSelectIndex:(NSUInteger)rightSelectIndex dropDownBlock:(JYSTradingDropDownBlock)block;

- (void)setTheTopMenuData:(NSDictionary *)topMenuDatas leftSelectString:(NSString *)leftSelectString rightSelectIndex:(NSUInteger)rightSelectIndex;

-(void)showView;

-(void)hideView;

- (void)initialHideMenuView;

@end

@interface JYSTradingDropDownTableViewCell : UITableViewCell

- (void)setCoinDataWithTitile:(NSString *)title;


@end

@interface JYSTradingDropDownCollectionCell : UICollectionViewCell

//- (void)setCoinDataWithTitile:(NSString *)title;

- (void)setCoinDataWithDict:(NSDictionary *)dict;

- (void)setCellBeSelected:(BOOL)selected;

@end

