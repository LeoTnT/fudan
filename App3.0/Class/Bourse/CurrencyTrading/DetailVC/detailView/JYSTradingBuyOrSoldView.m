//
//  JYSTradingBuyOrSoldView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSTradingBuyOrSoldView.h"
#import "KKChartView.h"
#import "JYSTradingBuyOrSoldTableView.h"

@interface JYSTradingBuyOrSoldView ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>



/** 价格 */
@property (nonatomic, strong) UILabel * priceLabel;

/** 买入走势 */
@property (nonatomic, strong) KKChartView * buyMovementsView;
/** 卖出走势 */
@property (nonatomic, strong) KKChartView * soldMovementsView;

/** 几位小数点 */
@property (nonatomic, strong) UILabel * decimalPlacesLabel;
/** 几位小数点 */
@property (nonatomic, strong) UIButton * decimalPlacesButton;

/** 选择小数点位数 */
@property (nonatomic, strong) UITableView * decimalPlacesTableView;
/** 小数点位数数组 */
@property (nonatomic, strong) NSArray * decimalPlacesArray;

/** 买or卖 */
@property (nonatomic, assign) BOOL isBuyView;

/** 主题颜色 */
@property (nonatomic, strong) UIColor * themeColor;

/** buy数组 */
@property (nonatomic, strong) NSArray * buyDatasArr;
/** 卖数组 */
@property (nonatomic, strong) NSArray * soldDatasArr;

@property (nonatomic, strong) UIImageView * scaleImageView;
@property (nonatomic, strong) UILabel*scaleLabel;

/** buyOrSoldTableView1 */
@property (nonatomic, strong) JYSTradingBuyOrSoldTableView * buyOrSoldTableView1;
/** buyOrSoldTableView2 */
@property (nonatomic, strong) JYSTradingBuyOrSoldTableView * buyOrSoldTableView2;

@property (nonatomic, assign) NSInteger depInter;
@end

@implementation JYSTradingBuyOrSoldView

- (JYSTradingBuyOrSoldTableView *)buyOrSoldTableView1 {
    if (_buyOrSoldTableView1 == nil) {
        _buyOrSoldTableView1 = [[JYSTradingBuyOrSoldTableView alloc] init];
    }
    return _buyOrSoldTableView1;
}

- (JYSTradingBuyOrSoldTableView *)buyOrSoldTableView2 {
    if (_buyOrSoldTableView2 == nil) {
        _buyOrSoldTableView2 = [[JYSTradingBuyOrSoldTableView alloc] init];
    }
    return _buyOrSoldTableView2;
}

- (instancetype)initWithFrame:(CGRect)frame themeStyle:(BOOL)isBuy {
    self = [super initWithFrame:frame];
    if (self) {
        self.isBuyView = isBuy;
        
        self.themeColor = self.isBuyView ? XSYCOLOR(0xF95453):XSYCOLOR(0x00BF84);
        [self setUpUI];
    }
    return self;
}

static NSUInteger const rowHeight = 44;//行高
static NSUInteger const maxRows = 5;//最多一屏显示行数
- (void) setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    UILabel * limitSheetLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(15) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"only_one_price")];
    [self addSubview:limitSheetLab];
    [limitSheetLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(15);
        make.top.mas_equalTo(self).offset(12);
    }];
    
    self.limitSheetTF = [XSUITool creatTextFieldWithFrame:CGRectZero backgroundColor:nil font:FontNum(16) textAlignment:NSTextAlignmentCenter delegate:nil placeholderColor:XSYCOLOR(0xCDCDCD) textColor:JYSMainTextColor placeholder:Localized(@"price_exchange") text:nil];
    [XSUITool setTextField:self.limitSheetTF cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [XSUITool setTextField:self.limitSheetTF returnKeyType:UIReturnKeyNext keyboardType:UIKeyboardTypeDecimalPad secureTextEntry:NO clearButtonMode:UITextFieldViewModeNever];
    //    self.limitSheetTF.font = [UIFont systemFontOfSize:FontNum(16) weight:UIFontWeightSemibold];
    [self addSubview:self.limitSheetTF];
    [self.limitSheetTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(limitSheetLab);
        make.top.mas_equalTo(limitSheetLab.mas_baseline).offset(12);
        make.size.mas_equalTo(CGSizeMake(FontNum(162), FontNum(36)));
    }];
    
    //估值
    //    UILabel * valuationLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"估值 --"];
    UILabel * valuationLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@""];
    [self addSubview:valuationLab];
    [valuationLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.limitSheetTF);
        make.top.mas_equalTo(self.limitSheetTF.mas_bottom).offset(8);
    }];
    
    self.valuationTF = [XSUITool creatTextFieldWithFrame:CGRectZero backgroundColor:nil font:FontNum(16) textAlignment:NSTextAlignmentCenter delegate:nil placeholderColor:XSYCOLOR(0xCDCDCD) textColor:JYSMainTextColor placeholder:Localized(@"exchange_volume") text:nil];
    [XSUITool setTextField:self.valuationTF cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [XSUITool setTextField:self.valuationTF returnKeyType:UIReturnKeyNext keyboardType:UIKeyboardTypeDecimalPad secureTextEntry:NO clearButtonMode:UITextFieldViewModeNever];
    //    self.valuationTF.font = [UIFont systemFontOfSize:FontNum(16) weight:UIFontWeightSemibold];
    [self addSubview:self.valuationTF];
    [self.valuationTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(limitSheetLab);
        make.top.mas_equalTo(valuationLab.mas_baseline).offset(12);
        make.size.mas_equalTo(CGSizeMake(FontNum(162), FontNum(36)));
    }];
    
    UIButton * percentileButton1 = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(percentileButtonClicked:) titleColor:COLOR_999999 titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:XSYCOLOR(0xE8EBEE)] title:@"25%"];
    percentileButton1.tag = 101;
    [XSUITool setButton:percentileButton1 titleColor:[UIColor whiteColor] titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:self.themeColor] forState:UIControlStateSelected title:@"25%"];
    percentileButton1.selected = NO;
    //    self.selectPercentileBtn = percentileButton1;
    [self addSubview:percentileButton1];
    
    UIButton * percentileButton2 = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(percentileButtonClicked:) titleColor:COLOR_999999 titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:XSYCOLOR(0xE8EBEE)] title:@"50%"];
    percentileButton2.tag = 102;
    [XSUITool setButton:percentileButton2 titleColor:[UIColor whiteColor] titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:self.themeColor] forState:UIControlStateSelected title:@"50%"];
    [self addSubview:percentileButton2];
    
    UIButton * percentileButton3 = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(percentileButtonClicked:) titleColor:COLOR_999999 titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:XSYCOLOR(0xE8EBEE)] title:@"75%"];
    percentileButton3.tag = 103;
    [XSUITool setButton:percentileButton3 titleColor:[UIColor whiteColor] titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:self.themeColor] forState:UIControlStateSelected title:@"75%"];
    [self addSubview:percentileButton3];
    
    UIButton * percentileButton4 = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(percentileButtonClicked:) titleColor:COLOR_999999 titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:XSYCOLOR(0xE8EBEE)] title:@"100%"];
    percentileButton4.tag = 104;
    [XSUITool setButton:percentileButton4 titleColor:[UIColor whiteColor] titleFont:FontNum(10) backgroundColor:nil image:nil backgroundImage:[UIImage yy_imageWithColor:self.themeColor] forState:UIControlStateSelected title:@"100%"];
    [self addSubview:percentileButton4];
    
    [percentileButton1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.valuationTF);
        make.top.mas_equalTo(self.valuationTF.mas_bottom).offset(14);
        make.height.mas_equalTo(FontNum(22));
    }];
    
    [percentileButton2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(percentileButton1.mas_right).offset(6);
        make.size.centerY.mas_equalTo(percentileButton1);
    }];
    
    [percentileButton3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(percentileButton2.mas_right).offset(6);
        make.size.centerY.mas_equalTo(percentileButton1);
    }];
    
    [percentileButton4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(percentileButton3.mas_right).offset(6);
        make.size.centerY.mas_equalTo(percentileButton1);
        make.right.mas_equalTo(self.valuationTF);
    }];
    
    self.dealAmountTF = [XSUITool creatTextFieldWithFrame:CGRectZero backgroundColor:nil font:FontNum(16) textAlignment:NSTextAlignmentCenter delegate:nil placeholderColor:XSYCOLOR(0xCDCDCD) textColor:JYSMainTextColor placeholder:Localized(@"buy_sell_num_hint") text:nil];
    self.dealAmountTF .enabled = NO;
    [XSUITool setTextField:self.dealAmountTF cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [XSUITool setTextField:self.dealAmountTF returnKeyType:UIReturnKeyNext keyboardType:UIKeyboardTypeDecimalPad secureTextEntry:NO clearButtonMode:UITextFieldViewModeNever];
    //    self.dealAmountTF.font = [UIFont systemFontOfSize:FontNum(16) weight:UIFontWeightSemibold];
    [self addSubview:self.dealAmountTF];
    [self.dealAmountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(limitSheetLab);
        make.top.mas_equalTo(percentileButton1.mas_bottom).offset(12);
        make.size.mas_equalTo(CGSizeMake(FontNum(162), FontNum(36)));
    }];
    
    UILabel * canUseLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"can_use")];
    [self addSubview:canUseLab];
    [canUseLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(limitSheetLab);
        make.top.mas_equalTo(self.dealAmountTF.mas_bottom).offset(14);
    }];
    
    self.canUseLabel =  [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentRight title:@"0"];
    [self addSubview:self.canUseLabel];
    [self.canUseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.left.mas_equalTo(canUseLab.mas_right).offset(40);
        make.right.mas_equalTo(self.limitSheetTF);
        make.centerY.mas_equalTo(canUseLab);
        make.left.mas_equalTo(canUseLab.mas_right).with.mas_offset(5);
    }];
    
    _buySoldButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:nil action:nil titleColor:[UIColor whiteColor] titleFont:FontNum(16) backgroundColor:self.themeColor image:nil backgroundImage:nil title:self.isBuyView ?Localized(@"buy"):Localized(@"selltwo")];
    
    [self addSubview:_buySoldButton];
    [_buySoldButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.dealAmountTF);
        make.top.mas_equalTo(canUseLab.mas_baseline).offset(20);
        make.height.mas_equalTo(FontNum(40));
    }];
    
    
    
    //
    //    UILabel * horizontalLineLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:self.themeColor titleFont:FontNum(10) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@""];
    //    [self addSubview:horizontalLineLab];
    //    [horizontalLineLab mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(self.priceLabel.mas_right).offset(100);
    //        make.centerY.mas_equalTo(self.priceLabel);
    //    }];
    
    UILabel * priceLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"price_exchange")];
    [self addSubview:priceLab];
    [priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_centerX).offset(12);
        make.baseline.mas_equalTo(self.limitSheetTF.mas_top);
    }];
    
    UILabel * countLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"exchange_volume")];
    [self addSubview:countLab];
    [countLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-16);
        make.baseline.mas_equalTo(self.limitSheetTF.mas_top);
    }];
    
    //买入或者卖出按钮---买入
    UIView *redView = [[UIView alloc] init];
    redView.backgroundColor = XSYCOLOR(0xF95453);
    [XSUITool setView:redView cornerRadius:FontNum(2) borderWidth:0 borderColor:nil];
    [self addSubview:redView];
    [redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_buySoldButton.mas_bottom).offset(15);
        make.left.mas_equalTo(_buySoldButton);
        make.width.height.mas_equalTo(FontNum(10));
    }];
    UILabel*_tbuyLb = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(11) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"buyone")];
    [self addSubview:_tbuyLb];
    [_tbuyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(redView);
        make.left.mas_equalTo(redView.mas_right).offset(5);
    }];
    
    UIView *greenView = [[UIView alloc] init];
    greenView.backgroundColor = XSYCOLOR(0x00BF84);
    [XSUITool setView:greenView cornerRadius:FontNum(2) borderWidth:0 borderColor:nil];
    [self addSubview:greenView];
    [greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.width.height.mas_equalTo(redView);
        make.left.mas_equalTo(_tbuyLb.mas_right).offset(15);
    }];
    UILabel*_tsaleLb = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(11) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"sell")];
    [self addSubview:_tsaleLb];
    [_tsaleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(redView);
        make.left.mas_equalTo(greenView.mas_right).offset(5);
    }];
    
    _scaleImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:XSYCOLOR(0xDDDDDD) image:nil];
    [self addSubview:_scaleImageView];
    [_scaleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(redView);
        make.size.mas_equalTo(CGSizeMake(FontNum(11), 0));
        //        make.size.mas_equalTo(CGSizeMake(FontNum(11), 1));
    }];
    
    _scaleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(10) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@""];
    [self addSubview:_scaleLabel];
    [_scaleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(greenView.mas_bottom).offset(10);
        make.left.mas_equalTo(_scaleImageView.mas_right).offset(5);
        make.baseline.mas_equalTo(_scaleImageView);
    }];
    
    
    
    
    
    [self addSubview:self.buyOrSoldTableView1];
    self.buyOrSoldTableView1.isBuyView = NO;
    [self.buyOrSoldTableView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_centerX).offset(12);
        make.right.mas_equalTo(self).offset(-16);
        make.top.mas_equalTo(self.limitSheetTF).offset(10);
        make.height.mas_equalTo(FontNum(200));
    }];
    
    
    [self addSubview:self.buyOrSoldTableView2];
    self.buyOrSoldTableView2.isBuyView = YES;
    [self.buyOrSoldTableView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_centerX).offset(12);
        make.right.mas_equalTo(self).offset(-16);
        make.top.mas_equalTo(_buySoldButton).offset(10);
        make.height.mas_equalTo(FontNum(200));
    }];
    
    
    //选择地址
    self.decimalPlacesLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"0 %@",Localized(@"decimal_D")]];
    [self addSubview:self.decimalPlacesLabel];
    
    self.decimalPlacesButton = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(chooseDecimalPlaces)];
    [self addSubview:self.decimalPlacesButton];
    [self.decimalPlacesButton setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
    [XSUITool setButton:self.decimalPlacesButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentRight contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 12) titleEdgeInsets:UIEdgeInsetsZero];
    [XSUITool setButton:self.decimalPlacesButton cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [self.decimalPlacesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.bottom.mas_equalTo(_scaleLabel.mas_bottom).offset(FontNum(160));
        make.left.mas_equalTo(self.buyOrSoldTableView2);
        make.size.mas_equalTo(CGSizeMake(FontNum(172), FontNum(30)));
    }];
    
    [self.decimalPlacesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.decimalPlacesButton).offset(12);
        make.centerY.mas_equalTo(self.decimalPlacesButton);
    }];
    
    self.decimalPlacesTableView = [[UITableView alloc] initWithFrame:CGRectMake(self.decimalPlacesButton.xs_x, self.decimalPlacesButton.xs_bottom, self.decimalPlacesButton.xs_width, 44*5) style:UITableViewStylePlain];
    self.decimalPlacesTableView.showsVerticalScrollIndicator = NO;
    self.decimalPlacesTableView.bounces = YES;
    self.decimalPlacesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.decimalPlacesTableView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.decimalPlacesTableView];
    self.decimalPlacesTableView.delegate = self;
    self.decimalPlacesTableView.dataSource = self;
    
    
    [self.limitSheetTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.valuationTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.limitSheetTF.delegate = self;
    self.valuationTF.delegate = self;
    
    __weak __typeof__(self) wSelf = self;
    self.buyOrSoldTableView1.priceBlock = ^(NSString *price,NSString *volume) {
        wSelf.limitSheetTF.text = price;
//        wSelf.valuationTF.text = volume;
//        [wSelf changDealAmoutText];
        
        
    };
    self.buyOrSoldTableView2.priceBlock = ^(NSString *price,NSString *volume) {
        wSelf.limitSheetTF.text = price;
//        wSelf.valuationTF.text = volume;
        
//        [wSelf changDealAmoutText];
        
    };
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:self.themeColor titleFont:FontNum(17) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"- -"];
    self.priceLabel.font = [UIFont systemFontOfSize:FontNum(15) weight:UIFontWeightSemibold];
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainWidth/2);
        make.top.mas_equalTo(self.canUseLabel).offset(5);
        make.width.mas_equalTo(mainWidth/2);
        make.height.mas_equalTo(25);
    }];
    
}

- (void)changDealAmoutText{
    if (!isEmptyString(self.limitSheetTF.text)&&!isEmptyString(self.valuationTF.text)) {
        //成交金额
        CGFloat dealPrice = self.limitSheetTF.text.floatValue*self.valuationTF.text.floatValue;
        if (dealPrice >0) {
            self.dealAmountTF.text = [NSString stringWithFormat:@"%.6f",dealPrice];
        }else{
            self.dealAmountTF.text =[NSString stringWithFormat:@"0.0"];
        }
        
    }else{
        self.dealAmountTF.text = @"";
    }
}
-(void)setClosePrice:(NSString *)closePrice
{
    _closePrice = closePrice;
    self.priceLabel.text = closePrice;
    
    if (self.buyMovementsView) {
        self.buyMovementsView.currentFloat = self.closePrice.floatValue;
    }
    if (self.soldMovementsView) {
        self.soldMovementsView.currentFloat = self.closePrice.floatValue;
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self touchesTheScreen];
}

- (void)percentileButtonClicked:(UIButton *)button {
    
    CGFloat percent = 0.0;
    switch (button.tag) {
        case 101:
            percent = 0.25;
            break;
        case 102:
            percent = 0.5;
            break;
        case 103:
            percent = 0.75;
            break;
        case 104:
            percent = 1.0;
            break;
        default:
            break;
    }
    
    if (!self.selectPercentileBtn) {
        button.selected =YES;
        self.selectPercentileBtn = button;
    }else{
        if (self.selectPercentileBtn != button) {
            button.selected = YES;
            self.selectPercentileBtn.selected = NO;
            self.selectPercentileBtn = button;
        }else{
            button.selected = !button.selected;
            if (!button.selected) {
                percent = 0.0;
            }
            self.selectPercentileBtn = button;
            
        }
    }
    if (self.floatBlock) {
        self.floatBlock(percent, self.selectPercentileBtn.selected);
    }
    //    if (self.selectPercentileBtn != button) {
    //        self.selectPercentileBtn.selected = NO;
    //        self.selectPercentileBtn = button;
    //        self.selectPercentileBtn.selected = YES;
    //    }
    
}

//- (void)buySoldButtonClick {
//
//}

#pragma mark 选择小数点位数
- (void)chooseDecimalPlaces {
    if (self.decimalPlacesArray.count) {
        self.decimalPlacesButton.selected = !self.decimalPlacesButton.selected;
        [UIView animateWithDuration:0.25 animations:^{
            self.decimalPlacesButton.imageView.transform=CGAffineTransformRotate(self.decimalPlacesButton.imageView.transform, M_PI);
        }];
        
        if (self.decimalPlacesButton.selected) {
            [self showDecimalPlacesView];
        } else {
            [self hideDecimalPlacesView];
        }
    }
}

- (void)setTheDecimalPlacesDataArray:(NSArray *)decimalPlaces {
    self.decimalPlacesArray = decimalPlaces;
    
    self.decimalPlacesLabel.text = [NSString stringWithFormat:@"%@ %@",decimalPlaces.lastObject,Localized(@"decimal_D")];
    
    self.decimalPlacesTableView.frame = CGRectMake(self.decimalPlacesButton.xs_x, self.decimalPlacesButton.xs_bottom, self.decimalPlacesButton.xs_width, CGFLOAT_MIN);
    self.decimalPlacesTableView.alpha = 0;
    self.decimalPlacesTableView.hidden = YES;
    [self.decimalPlacesTableView reloadData];
}

- (void)showDecimalPlacesView {
    if ([self.delegate respondsToSelector:@selector(isChooseDecimalPlacesOpen:)]) {
        [self.delegate isChooseDecimalPlacesOpen:YES];
    }
    
    self.decimalPlacesTableView.alpha = 1;
    self.decimalPlacesTableView.hidden = NO;
    NSUInteger count = self.decimalPlacesArray.count;
    if (count > maxRows) {
        count = maxRows;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.decimalPlacesTableView.frame = CGRectMake(self.decimalPlacesButton.xs_x, self.decimalPlacesButton.xs_bottom- count * rowHeight- FontNum(30), self.decimalPlacesButton.xs_width, count * rowHeight);
    }];
}

- (void)hideDecimalPlacesView {
    
    if ([self.delegate respondsToSelector:@selector(isChooseDecimalPlacesOpen:)]) {
        [self.delegate isChooseDecimalPlacesOpen:NO];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.decimalPlacesTableView.frame = CGRectMake(self.decimalPlacesButton.xs_x, self.decimalPlacesButton.xs_bottom, self.decimalPlacesButton.xs_width, CGFLOAT_MIN);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.decimalPlacesTableView.alpha = 0;
        } completion:^(BOOL finished) {
            self.decimalPlacesTableView.hidden = YES;
        }];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.decimalPlacesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellID = @"cellID";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:FontNum(12)];
    if (self.decimalPlacesArray.count) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",self.decimalPlacesArray[indexPath.row],Localized(@"decimal_D")];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.decimalPlacesLabel.text = [NSString stringWithFormat:@"%@ %@",self.decimalPlacesArray[indexPath.row],Localized(@"decimal_D")];
    //    self.addressLabel.textColor = JYSMainTextColor;
    [self touchesTheScreen];
    
    [[UserSingle sharedUserSingle].siginleSubject sendNext:RACTuplePack(@"ppp",self.decimalPlacesArray[indexPath.row])];
}

- (void)hideDecimalPlaces {
    [self touchesTheScreen];
}
- (void)touchesTheScreen {
    [self endEditing:YES];
    
    
    [self hideDecimalPlacesView];
    if (self.decimalPlacesButton.selected) {
        [UIView animateWithDuration:0.25 animations:^{
            self.decimalPlacesButton.imageView.transform=CGAffineTransformRotate(self.decimalPlacesButton.imageView.transform, M_PI);
        }];
    }
    self.decimalPlacesButton.selected = NO;
}


-(void)setBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray  depStr:(NSString*)depStr{
    //    XSLog(@"%@\n-------------------------\n%@",buyArr,soldArray);
    
    [self creatLineView];
    self.buyDatasArr = buyArr;
    self.soldDatasArr= soldArray;
    self.depInter = [depStr integerValue];
    
    if (self.closePrice) {
        self.buyMovementsView.currentFloat = self.closePrice.floatValue;
        self.soldMovementsView.currentFloat = self.closePrice.floatValue;
    }
    
    //设置深度信息
    [self setRightTableBuyArray:buyArr soldArray:soldArray depStr:depStr];
    
    
    NSArray *buyDataArr = [self getMovementsArray1];
    NSArray *soldDataArr = [self getMovementsArray2];
    NSMutableArray *mData1 =[NSMutableArray array];
    NSMutableArray *mData2 =[NSMutableArray array];
    CGFloat numStr = 0.0;
    for (NSString *countStr1 in buyDataArr) {
        
        numStr = numStr +countStr1.doubleValue;
        [mData1 addObject:[NSString stringWithFormat:@"%f",numStr]];
    }
    numStr = 0.0;
    for (NSString *countStr2 in soldDataArr) {
        
        numStr = numStr +countStr2.doubleValue;
        [mData2 addObject:[NSString stringWithFormat:@"%f",numStr]];
    }
    
    
    
    
    CGFloat max1 = [self getMaxValueForAry:mData1];
    CGFloat max2 = [self getMaxValueForAry:mData2];
    CGFloat maxFloat = max1 >max2 ?max1:max2;
    
    self.buyMovementsView.maxFloat = maxFloat;
    self.soldMovementsView.maxFloat = maxFloat;
    
    
    
    [self setDataArray1:mData1];
    [self setDataArray2:mData2];
    
    
}


- (void)setRightTableBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray depStr:(NSString*)depStr
{
    NSArray *buyDataArr = [self getMovementsArray1];
    NSArray *soldDataArr = [self getMovementsArray2];
    
    
    CGFloat max1 = [self getMaxValueForAry:buyDataArr];
    CGFloat max2 = [self getMaxValueForAry:soldDataArr];
    //获取最大的买入或者卖出数量
    CGFloat maxFloat = max1 >max2 ?max1:max2;
    
    [self.buyOrSoldTableView1 setDataWithArray:soldArray maxFloat:maxFloat depStr:depStr];
    [self.buyOrSoldTableView2 setDataWithArray:buyArr maxFloat:maxFloat depStr:depStr];
}

-(void)setDataArray1:(NSArray *)dataArray1
{
    //    _dataArray1 =dataArray1;
    if (dataArray1.count == 0) {
        dataArray1 = @[@"0"];
    }
    NSMutableArray *mArr = [NSMutableArray array];
    NSString *text = @"0";
    for (int i =0; i<5; i++) {
        if (i>=dataArray1.count) {
            text = dataArray1.lastObject;
        }else{
            text = dataArray1[i];
        }
        [mArr addObject:text];
    }
    self.buyMovementsView.index = 1;
    self.buyMovementsView.dataAry = (NSArray*)(NSArray*)[self sortArrayWithArray:mArr isUp:NO];
    self.buyMovementsView.isShowGradient = YES;
    
}
-(void)setDataArray2:(NSArray *)dataArray2
{
    //    _dataArray2 =dataArray2;
    
    if (dataArray2.count == 0) {
        dataArray2 = @[@"0"];
    }
    NSMutableArray *mArr = [NSMutableArray array];
    NSString *text = @"0";
    for (int i =0; i<5; i++) {
        if (i>=dataArray2.count) {
            text = @"0";
        }else{
            text = dataArray2[i];
        }
        [mArr addObject:text];
    }
    
    self.soldMovementsView.index = 2;
    self.soldMovementsView.dataAry = (NSArray*)[self sortArrayWithArray:mArr isUp:YES];
    self.soldMovementsView.isShowGradient = YES;
    
}


-(CGFloat)getMaxValueForAry:(NSArray *)ary{
    if (ary.count <=0) {
        return 0;
        
    }
    CGFloat max = [ary[0] doubleValue];
    for (int i=0; i<ary.count; i++) {
        if (max < [ary[i] doubleValue]) {
            max = [ary[i] doubleValue];
        }
    }
    return max;
}


- (NSArray *)getMovementsArray1 {
    NSMutableArray * movementsArray1 = [NSMutableArray array];
    NSInteger countNum =  self.buyDatasArr.count;
    if (self.buyDatasArr.count>5) {
        countNum = 5;
    }
    
    for (NSInteger i = 0; i <countNum; i++) {
        NSArray * buydatas = self.buyDatasArr[i];
        NSString * count1String = [NSString stringWithFormat:@"%@",buydatas[1]];
        [movementsArray1 addObject:count1String];
    }
    
    return movementsArray1;
}

- (NSArray *)getMovementsArray2 {
    NSMutableArray * movementsArray2 = [NSMutableArray array];
    NSInteger countNum =  self.soldDatasArr.count;
    if (self.soldDatasArr.count>5) {
        countNum = 5;
    }
    
    for (NSInteger i = 0; i < countNum; i++) {
        NSArray * solddatas = self.soldDatasArr[i];
        NSString * count2String = [NSString stringWithFormat:@"%@",solddatas[1]];
        [movementsArray2 addObject:count2String];
    }
    
    return movementsArray2;
}

- (NSArray *)sortArrayWithArray:(NSMutableArray*)sortArray isUp:(BOOL)isUp {
    
    [sortArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2)
     {
         //此处的规则含义为：若前一元素比后一元素小，则返回降序（即后一元素在前，为从大到小排列）
         if ([obj1 doubleValue] < [obj2 doubleValue])
         {
             if (isUp) {
                 return NSOrderedAscending;
             }else{
                 return NSOrderedDescending;
             }
         }else
         {
             if (isUp) {
                 return NSOrderedDescending;
             }else{
                 return NSOrderedAscending;
             }
         }
     }];
    
    return sortArray;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    NSLog(@"%@====%@",textField.text,_moneyTextField.text);
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location==NSNotFound) {
        isHaveDian=NO;
    }
    if ([string length]>0)
    {
        unichar single=[string characterAtIndex:0];//当前输入的字符
        if ((single >='0' && single<='9') || single=='.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length]==0){
                if(single == '.'){
                    //                    [self alertView:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
                //                if (single == '0') {
                ////                    [self alertView:@"亲，第一个数字不能为0"];
                //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                //                    return NO;
                //
                //                }
            }
            if (single=='.')
            {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian=YES;
                    return YES;
                }else
                {
                    //                    [self alertView:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            else
            {
                if (isHaveDian)//存在小数点
                {
                    NSInteger precision = 8;
                    if (textField == self.limitSheetTF) {//价格小数位数
                        if (self.bInfoModel.price_precision) {
                            precision = self.bInfoModel.price_precision.integerValue;
                        }
                        //判断小数点的位数
                        NSRange ran=[textField.text rangeOfString:@"."];
                        NSUInteger tt=range.location-ran.location;
                        if (tt <= precision){
                            return YES;
                        }else{
                            //                        [self alertView:@"亲，您最多输入两位小数"];
                            return NO;
                        }
                    }else if(textField == self.valuationTF) {//数量小数位数
                        if (self.bInfoModel.amount_precision) {
                            precision = self.bInfoModel.amount_precision.integerValue;
                        }
                        //判断小数点的位数
                        NSRange ran=[textField.text rangeOfString:@"."];
                        NSUInteger tt=range.location-ran.location;
                        if (tt <= precision){
                            return YES;
                        }else{
                            //                        [self alertView:@"亲，您最多输入两位小数"];
                            return NO;
                        }
                        
                    }
                    return YES;
                }else
                {
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            //            [self alertView:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
    
}

- (void)textFieldDidChange:(id) sender {
    //    UITextField *_field = (UITextField *)sender;
    if (!isEmptyString(self.limitSheetTF.text)&&!isEmptyString(self.valuationTF.text)) {
        //成交金额
        CGFloat dealPrice = self.limitSheetTF.text.floatValue*self.valuationTF.text.floatValue;
        if (dealPrice >0) {
            self.dealAmountTF.text = [NSString stringWithFormat:@"%.6f",dealPrice];
            self.dealAmountTF.text = [self.dealAmountTF.text substringWithRange:NSMakeRange(0, self.dealAmountTF.text.length-6+self.depInter)];
        }else{
            self.dealAmountTF.text =[NSString stringWithFormat:@"0.0"];
        }
        
    }else{
        self.dealAmountTF.text = @"";
    }
}



- (void)creatLineView
{
    CGFloat KViewW = (mainWidth/2-8*2)/2;
    
    for (UIView *view in self.subviews) {
        if ( [view isKindOfClass:[KKChartView class]]) {
            [view removeFromSuperview];
        }
    }
    
    
    
    //折线图
    self.buyMovementsView = [[KKChartView alloc]initWithFrame:CGRectMake(15, _scaleImageView.xs_bottom+10, FontNum(80), FontNum(150))];
    self.buyMovementsView.coordinateColor = [UIColor clearColor];
    self.buyMovementsView.lineWidth = 1;
    //    self.buyMovementsView.lineColor = XSYCOLOR(0xFF504B);
    self.buyMovementsView.lineColor = [UIColor redColor];
    
    self.buyMovementsView.isShowGradient = YES;
    self.buyMovementsView.gradientColor = @"FF504B";
    
    self.buyMovementsView.backgroundColor = BG_COLOR;
    [self addSubview:self.buyMovementsView];
    [self.buyMovementsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_scaleLabel.mas_bottom).offset(9);
        make.left.mas_equalTo(15);
        make.size.mas_equalTo(CGSizeMake(FontNum(80), FontNum(150)));
    }];
    
    
    
    
    
    //折线图
    self.soldMovementsView = [[KKChartView alloc]initWithFrame:CGRectMake(15, _scaleImageView.xs_bottom+10, FontNum(80), FontNum(150))];
    self.soldMovementsView.coordinateColor = [UIColor clearColor];
    self.soldMovementsView.lineWidth = 1;
    //    self.soldMovementsView.lineColor = XSYCOLOR(0x00BF84);;
    self.soldMovementsView.lineColor = [UIColor greenColor];
    
    self.soldMovementsView.isShowGradient = YES;
    self.soldMovementsView.gradientColor = @"00BF84";
    
    self.soldMovementsView.backgroundColor = BG_COLOR;
    [self addSubview:self.soldMovementsView];
    [self.soldMovementsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.mas_equalTo(self.buyMovementsView);
        make.left.mas_equalTo(self.buyMovementsView.mas_right);
    }];
    
    //    self.buyMovementsView.currentFloat = 0.0;
    //    self.soldMovementsView.currentFloat = 0.0;
    //    self.buyMovementsView.maxFloat = 1.0;
    //    self.soldMovementsView.maxFloat = 1.5;
    
    self.buyMovementsView.index = 1;
    
    //    self.buyMovementsView.dataAry = [self getMovementsArray1];
    self.buyMovementsView.isShowGradient = YES;
    
    self.soldMovementsView.index = 2;
    //    self.soldMovementsView.dataAry = [self getMovementsArray2];//@[@"0.01",@"0.15",@"0.3",@"0.55",@"0.9",@"1.1",@"1.5"];
    self.soldMovementsView.isShowGradient = YES;
    
}




@end

