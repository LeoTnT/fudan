//
//  JYSHistoryDealCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSHistoryDealCell.h"
#import "JYSHistoricalTransactionModel.h"
#import "XSFormatterDate.h"

@interface JYSHistoryDealCell ()

/** 状态按钮 买or卖 */
@property (nonatomic, strong) UIButton * typeBtn;
/** 币种 */
@property (nonatomic, strong) UILabel * coinTypeLabel;
/** 委托数量 */
@property (nonatomic, strong) UILabel * entrustAmountLabel;
/** 价格 */
@property (nonatomic, strong) UILabel * priceLabel;
/** 时间 */
@property (nonatomic, strong) UILabel * timeLabel;

@end

@implementation JYSHistoryDealCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.backgroundColor = XSYCOLOR(0xf1f1f1);
    
    UIView * bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 15));
    }];
    
    UIImageView * bgImageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"jys_cell_bgImage"]];
    [self.contentView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-2, 2, -5, 2));
    }];
    
    NSString * rosebgImageNameString;
    NSString * fellbgImageNameString;
    
    if ([[UserInstance ShardInstnce].roseColorString isEqualToString:JYSRedString]) {
        rosebgImageNameString = @"entrustmentRecords_tag_buy";
        fellbgImageNameString = @"entrustmentRecords_tag_sell";
    } else {
        rosebgImageNameString = @"entrustmentRecords_tag_sell";
        fellbgImageNameString = @"entrustmentRecords_tag_buy";
    }
    
    self.typeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [XSUITool setButton:self.typeBtn titleColor:[UIColor whiteColor] titleFont:12 backgroundColor:nil image:nil backgroundImage:[UIImage imageNamed:rosebgImageNameString] forState:UIControlStateNormal title:Localized(@"buyone")];
    [XSUITool setButton:self.typeBtn titleColor:[UIColor whiteColor] titleFont:12 backgroundColor:nil image:nil backgroundImage:[UIImage imageNamed:fellbgImageNameString] forState:UIControlStateSelected title:Localized(@"sell")];
    self.typeBtn.userInteractionEnabled = NO;
    [XSUITool setButton:self.typeBtn contentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft contentVerticalAlignment:UIControlContentVerticalAlignmentTop imageEdgeInsets:UIEdgeInsetsZero titleEdgeInsets:UIEdgeInsetsMake(2, 2, 0, 0)];
    
    [self.contentView addSubview:self.typeBtn];
    [self.typeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(FontNum(31), FontNum(31)));
    }];
    
    UILabel * coinTypeLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"orders_bType_title")];
    [self.contentView addSubview:coinTypeLab];
    [coinTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.baseline.mas_equalTo(bgView.mas_centerY);
    }];
    
    UILabel * entrustAmountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"order_weituo_count")];
    [self.contentView addSubview:entrustAmountLab];
    [entrustAmountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(coinTypeLab);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.35);
    }];
    
    UILabel * priceLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"orders_price_title")];
    [self.contentView addSubview:priceLab];
    [priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView.mas_centerX).multipliedBy(1.2);
        make.top.mas_equalTo(coinTypeLab);
//        make.width.mas_equalTo(FontNum(30));
    }];
    
    self.coinTypeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"ETH/BTC"];
    [self.contentView addSubview:self.coinTypeLabel];
    [self.coinTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(coinTypeLab.mas_right).offset(10);
        make.centerY.mas_equalTo(coinTypeLab);
    }];
    
    self.entrustAmountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"80.210"];
    [self.contentView addSubview:self.entrustAmountLabel];
    [self.entrustAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(entrustAmountLab.mas_right).offset(10);
        make.centerY.mas_equalTo(entrustAmountLab);
    }];
    
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.19000000"];
    self.priceLabel.adjustsFontSizeToFitWidth = YES;
    self.priceLabel.minimumScaleFactor = 0.8;
    
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(priceLab.mas_right).offset(8);
        make.centerY.mas_equalTo(priceLab);
        make.right.mas_equalTo(bgView.mas_right).offset(-8);
    }];
    
    
    self.timeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2018-03-24 16:12"];
    [self.contentView addSubview:self.timeLabel];
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    self.timeLabel.minimumScaleFactor = 0.8;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(priceLab);
        make.centerY.mas_equalTo(entrustAmountLab);
        make.right.mas_equalTo(bgView.mas_right).offset(-8);
    }];
    
}

- (void)setHistoricalDataWithModel:(JYSHistoricalTransactionModel *)model {
    self.typeBtn.selected = [model.type integerValue];
    self.coinTypeLabel.text = [NSString stringWithFormat:@"%@/%@",model.gcoin,model.ucoin];
    self.priceLabel.text = model.price;
    self.entrustAmountLabel.text = model.filled_amount;
    self.timeLabel.text = [XSFormatterDate timeWithTimeIntervalString:model.create_time];
}


@end
