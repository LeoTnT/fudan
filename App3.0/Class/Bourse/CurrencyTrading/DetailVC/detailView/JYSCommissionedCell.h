//
//  JYSCommissionedCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSEntrustOrdersModel;

typedef void(^cancellationsBlock)(void);

@interface JYSCommissionedCell : UITableViewCell

- (void)setOrderDataWithModel:(JYSEntrustOrdersModel *)model;

/** 撤单事件 */
@property (nonatomic, copy) cancellationsBlock cancelBlock;

@end
