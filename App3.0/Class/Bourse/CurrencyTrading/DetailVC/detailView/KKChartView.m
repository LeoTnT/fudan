//
//  KKChartView.m
//  KKChartView
//
//  Created by lk on 2017/5/9.
//  Copyright © 2017年 lukai. All rights reserved.
//


#define KKWidth self.frame.size.width
#define KKHeight self.frame.size.height
#define KKTop 10
#define KKRight 0.01


#import "KKChartView.h"
#import "UIColor+expanded.h"
#import <objc/runtime.h>
@interface KKChartView ()
@property(nonatomic, strong)UILabel *titilLable;
@end

@implementation KKChartView
{
    NSMutableArray *_pointAry;
    CAShapeLayer *layer;
    CGPoint _endPoint;
    CALayer *baseLayer;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _pointAry = [NSMutableArray array];
        self.marginX = 0;
        self.marginY = 10;
        _coordinateColor = [UIColor redColor];
        _lineWidth = 0.5;
        _gradientColor = @"0xf38b10";
        _lineColor = [UIColor yellowColor];
    }
    return self;
}

-(void)kk_drawRect{
    
//    [self.layer removeAllAnimations];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(_marginX, KKTop)];
    [path addLineToPoint:CGPointMake(_marginX, KKHeight - _marginY)];
    [path moveToPoint:CGPointMake(_marginX, KKHeight - _marginY)];
    [path addLineToPoint:CGPointMake(KKWidth - KKRight, KKHeight - _marginY)];
    

    if (layer) {
        [layer removeFromSuperlayer];
    }
    layer = [CAShapeLayer layer];
    layer.path = path.CGPath;
    layer.lineJoin = kCALineJoinRound;
    layer.lineCap = kCALineCapRound;
    layer.lineWidth = _lineWidth;
    layer.strokeColor = _coordinateColor.CGColor;
    [self.layer addSublayer:layer];
    
}

-(void)kk_addLeftBezierPoint{

    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointFromString(_pointAry.firstObject)];
        
    for (int i=0; i<_pointAry.count; i++) {

        if (i != 0) {
            CGPoint nowPoint = CGPointFromString(_pointAry[i]);
            
            int tIndex = i-1;
            if (tIndex <=  0) {
                tIndex = 0;
            }
            CGPoint oldPoint = CGPointFromString(_pointAry[tIndex]);
            [path addCurveToPoint:nowPoint controlPoint1:CGPointMake((nowPoint.x+oldPoint.x)/2, oldPoint.y) controlPoint2:CGPointMake((nowPoint.x+oldPoint.x)/2, nowPoint.y)];
            
        }
    }
    
    CAShapeLayer *lineLay = [CAShapeLayer new];
    lineLay.path = path.CGPath;
    lineLay.lineWidth = _lineWidth;
    lineLay.strokeColor = _lineColor.CGColor;
    lineLay.lineJoin = kCALineJoinRound;
    lineLay.lineCap = kCALineCapRound;
    lineLay.fillColor = [UIColor clearColor].CGColor;
    [self.layer addSublayer:lineLay];
    
}

-(void)kk_addGradientLayer{
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointFromString(_pointAry.firstObject)];
    
    
    for (int i=0; i<_pointAry.count; i++) {
        
        if (i != 0) {
            CGPoint nowPoint = CGPointFromString(_pointAry[i]);
            int tIndex = i-1;
            if (tIndex <= 0) {
                tIndex = 0;
            }
            CGPoint oldPoint = CGPointFromString(_pointAry[tIndex]);
            [path addCurveToPoint:nowPoint controlPoint1:CGPointMake((nowPoint.x+oldPoint.x)/2, oldPoint.y) controlPoint2:CGPointMake((nowPoint.x+oldPoint.x)/2, nowPoint.y)];
            
            if (i == _pointAry.count-1) {
                _endPoint = nowPoint;
                [path moveToPoint:nowPoint];//添加连线
                
            }
        }
    }
    
    CGPoint p1 = CGPointFromString(_pointAry.firstObject);
    [path addLineToPoint:CGPointMake(_endPoint.x, KKHeight - _marginY)];
    [path addLineToPoint:CGPointMake(_marginX, KKHeight - _marginY)];
    [path addLineToPoint:CGPointMake(_marginX, p1.y)];
    [path addLineToPoint:CGPointMake(p1.x, p1.y)];
    
    CAShapeLayer *shadeLayer = [CAShapeLayer new];
    shadeLayer.path = path.CGPath;
    shadeLayer.lineJoin = kCALineJoinRound;
    shadeLayer.lineCap = kCALineCapRound;
    shadeLayer.strokeColor = [UIColor clearColor].CGColor;
//    shadeLayer.fillColor = [UIColor yellowColor].CGColor;
//    [self.layer addSublayer:shadeLayer];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(5, 0, 0, KKHeight  - KKTop);
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(0, 1);
    gradientLayer.cornerRadius = 5;
    gradientLayer.masksToBounds = YES;
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:_gradientColor andAlpha:0.6].CGColor,(__bridge id)[UIColor colorWithHexString:_gradientColor andAlpha:0.01].CGColor];
    gradientLayer.locations = @[@(0.5f)];
    
    if (baseLayer) {
        [baseLayer removeFromSuperlayer];
    }//需要删除原来的
    
    baseLayer = [CALayer layer];
    [baseLayer addSublayer:gradientLayer];
    [baseLayer setMask:shadeLayer];
    [self.layer addSublayer:baseLayer];
    
    
    CABasicAnimation *anmio = [CABasicAnimation animation];
    anmio.keyPath = @"bounds";
    anmio.duration = 0.01f;
    anmio.toValue = [NSValue valueWithCGRect:CGRectMake(5, 0, 2*_endPoint.x, self.bounds.size.height - KKTop)];
    anmio.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anmio.fillMode = kCAFillModeForwards;
    anmio.autoreverses = NO;
    anmio.removedOnCompletion = NO;
    [gradientLayer addAnimation:anmio forKey:@"bounds"];
}


-(void)setLineColor:(UIColor *)lineColor{
    _lineColor = lineColor;
    [self kk_addLeftBezierPoint];
}
-(void)setLineWidth:(NSInteger)lineWidth{
    _lineWidth = lineWidth;
    [self kk_drawRect];
}
-(void)setCoordinateColor:(UIColor *)coordinateColor{
    _coordinateColor = coordinateColor;
    [self kk_drawRect];
}

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry;
//    [self.layer removeFromSuperlayer];

    
    if (!dataAry || dataAry.count<1) return;
    CGFloat max = [self getMaxValueForAry:dataAry];
    if (self.maxFloat) {
        max =self.maxFloat;
    }
    for (int i=0; i<dataAry.count; i++) {
        CGFloat value = [dataAry[i] doubleValue];
        CGFloat x =_marginX + (KKWidth - _marginX - KKRight)/dataAry.count*(i);
        CGFloat y = KKHeight-_marginY-(KKHeight - _marginY - KKTop)/max*value;
        CGPoint point = CGPointMake(x, y);
        [_pointAry addObject:NSStringFromCGPoint(point)];

    }
    [self kk_drawRect];
    [self kk_addLeftBezierPoint];
    
    if (self.index == 1) {
        CGFloat perY = (KKHeight - _marginY - KKTop)/4;
        CGFloat value = max/4;
        
        //纵坐标
        for (int i =0; i<4; i++) {
            UILabel *leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(3, i*perY,65, 20)];
            leftLabel.font = [UIFont systemFontOfSize:10.0f];
            leftLabel.textColor = COLOR_666666;
//            leftLabel.textAlignment = NSTextAlignmentRight;
            leftLabel.text = [NSString stringWithFormat:@"%.2f",value*(4-i)];
            [self addSubview:leftLabel];
            
            //横坐标呢
            if (i == 0) {
                //底下的价格
                CGFloat botommlbW = 150;
                UILabel *bottomLb = [[UILabel alloc]initWithFrame:CGRectMake(KKWidth-botommlbW/2-_marginX-5,KKHeight, botommlbW, 20)];
                bottomLb.font = [UIFont systemFontOfSize:10.0f];
                bottomLb.textColor = COLOR_666666;
                bottomLb.textAlignment = NSTextAlignmentCenter;
                bottomLb.text = [NSString stringWithFormat:@"%.3f %.3f",self.currentFloat,self.currentFloat];
                [self addSubview:bottomLb];
                
                
                //横线
                UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0,KKHeight, KKWidth*2-10, 1)];
                line1.backgroundColor = [UIColor grayColor];
                [self addSubview:line1];
                
                //竖线
                UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0,0,1, KKHeight)];
                line2.backgroundColor = [UIColor grayColor];
                [self addSubview:line2];
//
//                UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(KKWidth*2,0,1, KKHeight)];
//                line3.backgroundColor = [UIColor grayColor];
//                [self addSubview:line3];

            }
        }
    }
  
    
}
-(void)setIsShowGradient:(BOOL)isShowGradient{
    
    _isShowGradient = isShowGradient;
    
    if (isShowGradient == YES) {
        [self kk_addGradientLayer];
    }

}
-(void)setGradientColor:(NSString *)gradientColor{
    
    _gradientColor = gradientColor;
    [self kk_addGradientLayer];
}

-(void)showLable:(UIButton *)sender{
    NSInteger i = sender.tag;
    
    if (_titilLable) {
        [_titilLable removeFromSuperview];
        _titilLable = nil;
    }
    
    [self addSubview:self.titilLable];
    self.titilLable.text = _dataAry[i];
    [_titilLable sizeToFit];
    _titilLable.center = CGPointMake(sender.center.x, sender.center.y - _titilLable.frame.size.height);
}

-(CGFloat)getMaxValueForAry:(NSArray *)ary{
    
    if (ary.count <=0) {
        return 0;
    }
    CGFloat max = [ary[0] doubleValue];
    for (int i=0; i<ary.count; i++) {
        if (max < [ary[i] doubleValue]) {
            max = [ary[i] doubleValue];
        }
    }
    return max;
}

-(UILabel *)titilLable{
    
    if (_titilLable == nil) {
        _titilLable = [UILabel new];
        _titilLable.font = [UIFont systemFontOfSize:10.0f];
        _titilLable.textColor = _lineColor;
    }
    return _titilLable;
}

@end
