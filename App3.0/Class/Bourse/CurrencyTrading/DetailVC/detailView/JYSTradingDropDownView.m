//
//  JYSTradingDropDownView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/4.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSTradingDropDownView.h"

@interface JYSTradingDropDownView ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,copy)JYSTradingDropDownBlock dropDownBlock;
@property (nonatomic ,strong)UICollectionViewFlowLayout *flowLayout;
/** 下拉菜单tableview */
@property (nonatomic, strong) UITableView * dropDownTableView;
/** collectionView */
@property (nonatomic, strong) UICollectionView * dropDownCollectionView;
/** titlesArray */
@property (nonatomic, copy) NSDictionary * topMenuDatasDict;
/** 右边数组 */
@property (nonatomic, copy) NSArray * rightMenusArray;

/** 选中行 */
@property (nonatomic, copy) NSString * leftMenuSelectedString;
@property (nonatomic, assign) NSInteger rightMenuSelectedIndex;

/** 左边数组 */
@property (nonatomic, copy) NSArray * leftKeysArray;
/** 左选中行 */
@property (nonatomic, assign) NSInteger leftKeysSelectedIndex;


@end

@implementation JYSTradingDropDownView


- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout= [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.minimumInteritemSpacing = 1;
        _flowLayout.minimumLineSpacing = 1;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _flowLayout;
}

- (UICollectionView *)dropDownCollectionView {
    if (!_dropDownCollectionView) {
        _dropDownCollectionView= [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.flowLayout];
        _dropDownCollectionView.dataSource = self;
        _dropDownCollectionView.delegate = self;
        _dropDownCollectionView.backgroundColor = [UIColor whiteColor];
        _dropDownCollectionView.showsHorizontalScrollIndicator = NO;
        _dropDownCollectionView.showsVerticalScrollIndicator = NO;
        [self addSubview:_dropDownCollectionView];
    }
    return _dropDownCollectionView;
}

- (UITableView *)dropDownTableView {
    if (_dropDownTableView == nil) {
        _dropDownTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _dropDownTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _dropDownTableView.showsVerticalScrollIndicator = NO;
        _dropDownTableView.backgroundColor = BG_COLOR;
        _dropDownTableView.delegate = self;
        _dropDownTableView.dataSource = self;
    }
    return _dropDownTableView;
}

static NSUInteger const rowHeight = 80;//行高
//static NSUInteger const maxRows = 4;//最多一屏显示行数

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

static NSString * const tableViewCellID = @"JYSTradingDropDownTableViewCellID";
static NSString * const collectionCellID = @"JYSTradingDropDownCollectionCellID";
-(void)setUpUI {
    self.backgroundColor = XSYCOLOR_a(0x111111, 0.6);
    
    [self addSubview:self.dropDownTableView];
    
    [self.dropDownTableView registerClass:[JYSTradingDropDownTableViewCell class] forCellReuseIdentifier:tableViewCellID];
    [self.dropDownTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self);
        make.width.mas_equalTo(FontNum(120));
        make.height.mas_equalTo(FontNum(320));
    }];
    
    [self addSubview:self.dropDownCollectionView];
    
    [self.dropDownCollectionView registerClass:[JYSTradingDropDownCollectionCell class] forCellWithReuseIdentifier:collectionCellID];
    [self.dropDownCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self);
        make.left.mas_equalTo(self.dropDownTableView.mas_right);
        make.height.mas_equalTo(self.dropDownTableView);
    }];
    
    self.hidden = YES;
}

- (void)setTopMenuData:(NSDictionary *)topMenuDatas leftSelectString:(NSString *)leftSelectString rightSelectIndex:(NSUInteger)rightSelectIndex dropDownBlock:(JYSTradingDropDownBlock)block {
    [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self);
        make.width.mas_equalTo(FontNum(120));
        make.height.mas_equalTo(CGFLOAT_MIN);
    }];
    [self.dropDownCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self);
        make.left.mas_equalTo(self.dropDownTableView.mas_right);
        make.height.mas_equalTo(self.dropDownTableView);
    }];
    self.alpha = 0;
    self.hidden = YES;
    
    self.topMenuDatasDict = topMenuDatas;
    self.leftMenuSelectedString = leftSelectString;
    self.rightMenuSelectedIndex = rightSelectIndex;
    self.dropDownBlock = block;
    self.leftKeysArray = [topMenuDatas allKeys];
    self.leftKeysSelectedIndex = [self.leftKeysArray indexOfObject:self.leftMenuSelectedString];
    
    [self.dropDownTableView reloadData];
    
    [self.dropDownTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.leftKeysSelectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    self.rightMenusArray = self.topMenuDatasDict[self.leftMenuSelectedString];
    
    [self.dropDownCollectionView reloadData];
    
    [self.dropDownCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:self.rightMenuSelectedIndex inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
}


- (void)setTheTopMenuData:(NSDictionary *)topMenuDatas leftSelectString:(NSString *)leftSelectString rightSelectIndex:(NSUInteger)rightSelectIndex {
    self.topMenuDatasDict = topMenuDatas;
    self.leftMenuSelectedString = leftSelectString;
    self.rightMenuSelectedIndex = rightSelectIndex;
    self.leftKeysArray = [topMenuDatas allKeys];
    self.leftKeysSelectedIndex = [self.leftKeysArray indexOfObject:self.leftMenuSelectedString];
    
    [self.dropDownTableView reloadData];
    
    [self.dropDownTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.leftKeysSelectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    self.rightMenusArray = self.topMenuDatasDict[self.leftMenuSelectedString];
    
    [self.dropDownCollectionView reloadData];
    
    [self.dropDownCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:self.rightMenuSelectedIndex inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
}

-(void)showView {
    self.alpha = 1;
    self.hidden = NO;
    if (!self.topMenuDatasDict ||!self.leftKeysArray) {
        [XSTool showToastWithView:self Text:@"获取数据失败!"];
        return;
    }
    //    NSUInteger count = self.topMenuDatasArray.count/3+1;
    //    if (count > maxRows) {
    //        count = maxRows;
    //    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self);
            make.width.mas_equalTo(FontNum(120));
            make.height.mas_equalTo(FontNum(320));
        }];
        [self.dropDownCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.right.mas_equalTo(self);
            make.left.mas_equalTo(self.dropDownTableView.mas_right);
            make.height.mas_equalTo(self.dropDownTableView);
        }];
        [self layoutIfNeeded];
    }];
}

-(void)hideView {
    [UIView animateWithDuration:0.3 animations:^{
        [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self);
            make.width.mas_equalTo(FontNum(120));
            make.height.mas_equalTo(CGFLOAT_MIN);
        }];
        [self.dropDownCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.right.mas_equalTo(self);
            make.left.mas_equalTo(self.dropDownTableView.mas_right);
            make.height.mas_equalTo(self.dropDownTableView);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
        }];
        [self layoutIfNeeded];
    }];
}

- (void)initialHideMenuView {
    [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self);
        make.width.mas_equalTo(FontNum(120));
        make.height.mas_equalTo(CGFLOAT_MIN);
    }];
    [self.dropDownCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self);
        make.left.mas_equalTo(self.dropDownTableView.mas_right);
        make.height.mas_equalTo(self.dropDownTableView);
    }];
    self.alpha = 0;
    self.hidden = YES;
}

#pragma mark tableViewDataSouse
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.leftKeysArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSTradingDropDownTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.leftKeysArray.count) {
        //        NSDictionary * topMenuDatasDict = ;
        [cell setCoinDataWithTitile:self.leftKeysArray[indexPath.row]];
    }
    //    XSLog(@"%ld",self.leftMenuSelectedIndex);
    
    //    NSIndexPath * selectIndex = [NSIndexPath indexPathForRow:self.leftMenuSelectedIndex inSection:0];
    //    JYSTradingDropDownTableViewCell * selectCell = (JYSTradingDropDownTableViewCell *)[tableView cellForRowAtIndexPath:selectIndex];
    //    [selectCell setSelected:YES animated:NO];
    
    //    if (indexPath.row == self.selectedIndex) {
    //        [cell setSelected:YES animated:NO];
    //    } else {
    //        [cell setSelected:NO animated:NO];
    //    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(44);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.leftKeysSelectedIndex != indexPath.row) {
        //        NSIndexPath * selectIndex = [NSIndexPath indexPathForRow:self.leftMenuSelectedIndex inSection:0];
        //        JYSTradingDropDownTableViewCell * cell = (JYSTradingDropDownTableViewCell *)[tableView cellForRowAtIndexPath:selectIndex];
        //        [cell setSelected:NO animated:YES];
        //
        //        JYSTradingDropDownTableViewCell * clickedCell = (JYSTradingDropDownTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        //        [clickedCell setSelected:YES animated:YES];
        self.leftKeysSelectedIndex = indexPath.row;
        
        self.leftMenuSelectedString = self.leftKeysArray[self.leftKeysSelectedIndex];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.dropDownTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] animated:NO scrollPosition:UITableViewScrollPositionNone];
        
        self.rightMenusArray = self.topMenuDatasDict[self.leftMenuSelectedString];
        self.rightMenuSelectedIndex = -1;
        [self.dropDownCollectionView reloadData];
        //        if (self.dropDownBlock) {
        //            NSString * titleString = self.topMenuDatasArray[indexPath.row];
        //            self.dropDownBlock(titleString, self.selectedIndex);
        //        }
        //        [self hideView];
    }
}

#pragma mark UICollectionViewDataSource
// 指定Section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

// 指定section中的collectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.rightMenusArray.count;
}

// 配置section中的collectionViewCell的显示
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JYSTradingDropDownCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellID forIndexPath:indexPath];
    if (self.rightMenusArray.count) {
        //        YLHomePageBannerModel * model = self.firstSectionDatas[indexPath.row];
        //        [cell setDataWithFirstSectionModel:model];
        [cell setCoinDataWithDict:self.rightMenusArray[indexPath.row]];
    }
    
    //    [cell setCellBeSelected:NO];
    //
    //    NSIndexPath * selectIndex = [NSIndexPath indexPathForRow:self.rightMenuSelectedIndex inSection:0];
    //    JYSTradingDropDownCollectionCell * selectCell = (JYSTradingDropDownCollectionCell *)[collectionView cellForItemAtIndexPath:selectIndex];
    //    [selectCell setCellBeSelected:YES];
    
    return cell;
}

#pragma mark UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((SCREEN_WIDTH-FontNum(120))/3-1, FontNum(rowHeight));
}

// 设置整个组的缩进量是多少
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
// 设置最小行间距，也就是前一行与后一行的中间最小间隔
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

// 设置最小列间距，也就是左行与右一行的中间最小间隔
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

// 设置section头视图的参考大小，与tableheaderview类似
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

// 设置section尾视图的参考大小，与tablefooterview类似
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.rightMenuSelectedIndex != indexPath.row) {
        //        NSIndexPath * selectIndex = [NSIndexPath indexPathForRow:self.rightMenuSelectedIndex inSection:0];
        //        JYSTradingDropDownCollectionCell * cell = (JYSTradingDropDownCollectionCell *)[collectionView cellForItemAtIndexPath:selectIndex];
        //        [cell setCellBeSelected:NO];
        //
        //        JYSTradingDropDownCollectionCell * clickedCell = (JYSTradingDropDownCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        //        [clickedCell setCellBeSelected:YES];
        
        self.rightMenuSelectedIndex = indexPath.row;
        
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        [self.dropDownCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        
        if (self.dropDownBlock) {
            NSDictionary * rightDic = self.rightMenusArray[indexPath.row];
            self.dropDownBlock(rightDic, self.leftMenuSelectedString, self.rightMenuSelectedIndex);
        }
        [self hideView];
    } else {
        [self touchScreen];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self touchScreen];
}

- (void)touchScreen {
    if (self.dropDownBlock) {
        NSDictionary * rightDict;
        
        if (self.rightMenuSelectedIndex >= 0) {
            rightDict = self.rightMenusArray[self.rightMenuSelectedIndex];
        } else {
            rightDict = @{};
        }
        self.dropDownBlock(rightDict, self.leftMenuSelectedString,self.rightMenuSelectedIndex);
    }
    [self hideView];
}

@end

@interface JYSTradingDropDownTableViewCell ()

/** title */
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation JYSTradingDropDownTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self makeUI];
    }
    return self;
}
- (void)makeUI {
    self.titleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"- -"];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.contentView);
    }];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel.textColor = JYSMainSelelctColor;
    } else {
        self.backgroundColor = BG_COLOR;
        self.titleLabel.textColor = JYSMainTextColor;
    }
}

- (void)setCoinDataWithTitile:(NSString *)title {
    self.titleLabel.text = title;
}

@end

@interface JYSTradingDropDownCollectionCell ()

/** 分类图片 */
@property (nonatomic, strong) UIImageView * imageV;
/** title */
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation JYSTradingDropDownCollectionCell

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self makeUI];
    }
    return self;
}

- (void)makeUI {
    self.imageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"no_pic"]];
    [self.contentView addSubview:self.imageV];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView.mas_centerY).offset(0);
        make.size.mas_equalTo(CGSizeMake(FontNum(20), FontNum(20)));
    }];
    
    self.titleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:nil];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView.mas_centerY).offset(6);
        make.width.mas_equalTo(SCREEN_WIDTH/3-5);
    }];
}

//- (void)setCoinDataWithTitile:(NSString *)title {
//    [self.imageV setImage:[UIImage imageNamed:@"no_pic"]];
//    self.titleLabel.text = title;
//}

- (void)setCoinDataWithDict:(NSDictionary *)dict {
    [self.imageV getImageWithUrlStr:dict[@"logo"] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.titleLabel.text = dict[@"gcoin"];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        [self.contentView setBackgroundColor:XSYCOLOR(0xF1F1F1)];
        self.titleLabel.textColor = JYSMainSelelctColor;
    } else {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        self.titleLabel.textColor = JYSMainTextColor;
    }
}

- (void)setCellBeSelected:(BOOL)selected {
    if (selected) {
        [self.contentView setBackgroundColor:XSYCOLOR(0xF1F1F1)];
        self.titleLabel.textColor = JYSMainSelelctColor;
    } else {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        self.titleLabel.textColor = JYSMainTextColor;
    }
}

@end



