//
//  JYSCurrentCommissionedViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrentCommissionedViewController.h"
#import "JYSCommissionedCell.h"
#import "JYSEntrustOrdersModel.h"//委托订单模型
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
@interface JYSCurrentCommissionedViewController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 页码 */
@property (nonatomic, assign) NSUInteger page;
/** 数据源数组 */
@property (nonatomic, strong) NSMutableArray * dataArray;

/** 市场名称 */
@property (nonatomic, copy) NSString * symbolString;

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation JYSCurrentCommissionedViewController

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
    
    [self setUpUI];
    
    [self downRefresh];
    [self upRefresh];
}

- (void)setSymbolStringData:(NSString *)symbol {
    self.symbolString = symbol;
    
    [self requestOrderList];
}

static NSString * const cellID = @"JYSCommissionedCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSCommissionedCell class] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //    self.tableView.tableHeaderView = self.topSortingView;
}

#pragma mark 下拉刷新
- (void)downRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshHeader:^{
        weakSelf.page = 1;
        [weakSelf requestOrderList];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)upRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshFooter:^{
        weakSelf.page++;
        [weakSelf requestOrderList];
    }];
    self.tableView.mj_footer.hidden = YES;
}

-(void)endRefresh
{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestOrderList {
    self.shouldShowEmptyView = YES;
    
    NSString * pageString = [NSString stringWithFormat:@"%ld",self.page];
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    if (self.symbolString.length) {
        parameters[@"symbol"] = self.symbolString;
    }
    parameters[@"page"] = pageString;
    parameters[@"state"] = @"1";
    parameters[@"limit"] = @"8";
    
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSEntrustOrderURL params:parameters HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        [self endRefresh];
        if (state.status) {
            if (self.page == 1) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary * dataDict = dic[@"data"];
            NSArray * dataArr = dataDict[@"data"];
            if (dataArr.count < [dataDict[@"per_page"] integerValue]) {
                self.tableView.mj_footer.hidden = YES;
            } else {
                self.tableView.mj_footer.hidden = NO;
            }
            
            NSArray * datas = [JYSEntrustOrdersModel mj_objectArrayWithKeyValuesArray:dataArr];
            
            [self.dataArray addObjectsFromArray:datas];
            
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        [self endRefresh];
    } showHUD:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSCommissionedCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataArray.count) {
        JYSEntrustOrdersModel * model = self.dataArray[indexPath.row];
        [cell setOrderDataWithModel:model];
        
        __weak typeof(self) weakSelf = self;
        cell.cancelBlock = ^{
            UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"您确定要执行撤单操作？") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:Localized(@"material_dialog_default_title") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [weakSelf cancellationsRequest:indexPath.row model:model];
            }];
            [alertControl addAction:cancelAction];
            [alertControl addAction:okAction];
            [weakSelf presentViewController:alertControl animated:YES completion:nil];
        };
    }
    
    return cell;
}

#pragma mark 撤单
- (void)cancellationsRequest:(NSUInteger)index model:(JYSEntrustOrdersModel *)model {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSCancellationsURL params:@{@"id":model.ID} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        NSString * infoStr = dic[@"info"];
        
        if (state.status) {
            if (!isEmptyString(infoStr)) {
                [XSTool showToastWithView:self.view Text:infoStr];
            } else {
                [XSTool showToastWithView:self.view Text:Localized(@"give_up_success")];
            }
            
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:index inSection:0];
            [self.dataArray removeObjectAtIndex:indexPath.row];
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView endUpdates];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 发送刷新通知
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TradingUpdateInfo" object:nil];
            });
            
        } else {
            if (!isEmptyString(infoStr)) {
                [XSTool showToastWithView:self.view Text:infoStr];
            }
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(111);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}
//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"no_data_hint");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


@end
