//
//  JYSCurrencySearchViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"
@class JYSCurrencyTradingModel;

@interface JYSCurrencySearchViewController : XSBaseTableViewController

/** 货币数据 */
@property (nonatomic, copy) NSArray * currencyDatas;
//@property (nonatomic,copy) void(^searchBlock)(NSDictionary *searchDic);

@property (nonatomic,copy) void(^searchBlock)(JYSCurrencyTradingModel *tradingModel);

@end
