//
//  JYSDetailSoldViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"
#import "JYSSymbolBInfoModel.h"
@class JYSCurrencyTradingModel;
@interface JYSDetailSoldViewController : XSBaseTableViewController

/** 小数点位数数组 */
@property (nonatomic, strong) NSArray * decimalPlacesArray;

/** 市场的常用信息(数量价格的小数精度等） */
@property (nonatomic, strong) JYSSymbolBInfoModel * bInfoModel;

//选中的币种字典
//@property (nonatomic, strong) NSDictionary * selCoinDic;
/**  */
@property (nonatomic, strong) JYSCurrencyTradingModel * selCoinModel;


//刷新
@property (nonatomic,copy) void(^refreshBlock)(BOOL isRefresh);


//设置深度
//- (void)setBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray;
- (void)setBuyArray:(NSArray *)buyArr soldArray:(NSArray *)soldArray depStr:(NSString*)depStr;

//设置当前价
- (void)setSoldClosePrice:(NSString*)price;

@end

