//
//  JYSCurrencyModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSCurrencyMenuModel : NSObject

/** id */
@property (nonatomic, copy) NSString * ID;
/** logo */
@property (nonatomic, copy) NSString * logo;
/**  */
@property (nonatomic, copy) NSString * opening;
/**  */
@property (nonatomic, copy) NSString * trade_status;
/**  */
@property (nonatomic, copy) NSString * symbol;
/**  */
@property (nonatomic, copy) NSString * gcoin;
/**  */
@property (nonatomic, copy) NSString * ucoin;

@end
