//
//  JYSSymbolBInfoModel.h
//  App3.0
//
//  Created by xinshang on 2018/5/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSSymbolBInfoModel : NSObject

@property (nonatomic,copy) NSString *ID;//124",
@property (nonatomic,copy) NSString *symbol;//ethcny",
@property (nonatomic,copy) NSString *opening; //0不可交易，1可交易
@property (nonatomic,copy) NSString *price_precision;//4", //价格小数位数
@property (nonatomic,copy) NSString *amount_precision;//4", //数量小数位数
@property (nonatomic,copy) NSString *buy_min;//5", //最小买入数量
@property (nonatomic,copy) NSString *buy_max;//50", //最大买入数量
@property (nonatomic,copy) NSString *sell_min;//5", //最小卖出数量
@property (nonatomic,copy) NSString *sell_max;//50" //最大卖出数量

@property (nonatomic,copy) NSString *user_approve;//0未认证，1审核中，2已认证
@property (nonatomic,copy) NSString *switch_user_approve;//0关闭，1开启

@end
