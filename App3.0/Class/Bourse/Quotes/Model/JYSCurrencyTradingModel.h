//
//  JYSCurrencyTradingModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSCurrencyTradingModel : NSObject

/**  */
@property (nonatomic, copy) NSString * ID;
/**  */
@property (nonatomic, copy) NSString * chg;
/**  */
@property (nonatomic, copy) NSString * close;
/**  */
@property (nonatomic, copy) NSString * symbol;
/**  */
@property (nonatomic, copy) NSString * amount;
/**  */
@property (nonatomic, copy) NSString * count;

/**  */
@property (nonatomic, copy) NSString * logo;

/**  */
@property (nonatomic, copy) NSString * high;
/**  */
@property (nonatomic, copy) NSString * change;
/**  */
@property (nonatomic, copy) NSString * low;
/**  */
@property (nonatomic, copy) NSString * open;
/**  */
@property (nonatomic, copy) NSString * vol;
/** 交易区coin */
@property (nonatomic, copy) NSString * ucoin;

/** id */
@property (nonatomic, copy) NSString * gcoin;

/**  */
@property (nonatomic, copy) NSString * bsecurrency;
/**  */
@property (nonatomic, copy) NSString * quotecurrency;

@end

@interface JYSCurrencyAnnouncementModel : NSObject

/** 名 */
@property (nonatomic, copy) NSString * username;
/**  */
@property (nonatomic, copy) NSString * type;
/**  */
@property (nonatomic, copy) NSString * number;
/**  */
@property (nonatomic, copy) NSString * remark;
/**  */
@property (nonatomic, copy) NSString * coin_name;
/**  */
@property (nonatomic, copy) NSString * intro;
/**  */
@property (nonatomic, copy) NSString * w_time;
/**  */
@property (nonatomic, copy) NSString * type_name;

@end
