//
//  JYSTradeCoinModel.h
//  App3.0
//
//  Created by xinshang on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSTradeCoinModel : NSObject

@property(nonatomic,copy)NSString *ID;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *number;
@property(nonatomic,copy)NSString *total;
@property(nonatomic,copy)NSString *release_time;
@property(nonatomic,copy)NSString *raise_price;
@property(nonatomic,copy)NSString *white_paper;
@property(nonatomic,copy)NSString *Website;
@property(nonatomic,copy)NSString *coin_search;
@property(nonatomic,copy)NSString *intro;


@end
