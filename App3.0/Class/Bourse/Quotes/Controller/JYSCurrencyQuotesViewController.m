//
//  JYSCurrencyQuotesViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyQuotesViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "SRWebSocketTool.h"
#import "JYSCurrencyTradingModel.h"
#import "JYSCurrencyTradingListViewController.h"
#import "JYSKLineViewController.h"
#import "JYSCurrencyTradingAnnouncementView.h"
#import "JYSCurrencyTradingModel.h"
#import "JYSCurrencySearchViewController.h"

@interface JYSCurrencyQuotesViewController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>

@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

/** 上面标题 */
@property (nonatomic, strong) NSMutableArray * titlesArray;
/** 所有列表数据 */
@property (nonatomic, strong) NSMutableArray * allListDatasArray;
/** 视图控制器存放数组 */
@property (nonatomic, strong) NSMutableArray * childArray;

/** 滚动公告视图 */
@property (nonatomic, strong) JYSCurrencyTradingAnnouncementView * announcementView;
/** 公告数组 */
@property (nonatomic, strong) NSMutableArray * announceArray;

/** 搜索数据 */
@property (nonatomic, strong) JYSCurrencyTradingModel * currencyTradingModel;

@end

@implementation JYSCurrencyQuotesViewController

- (NSMutableArray *)announceArray {
    if (_announceArray == nil) {
        _announceArray = [[NSMutableArray alloc] init];
    }
    return _announceArray;
}

- (JYSCurrencyTradingAnnouncementView *)announcementView {
    if (_announcementView == nil) {
        _announcementView = [[JYSCurrencyTradingAnnouncementView alloc] init];
        _announcementView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(44));
    }
    return _announcementView;
}

- (NSMutableArray *)childArray {
    if (_childArray == nil) {
        _childArray = [[NSMutableArray alloc] init];
    }
    return _childArray;
}

- (NSMutableArray *)allListDatasArray {
    if (_allListDatasArray == nil) {
        _allListDatasArray = [[NSMutableArray alloc] init];
    }
    return _allListDatasArray;
}

- (NSMutableArray *)titlesArray {
    if (_titlesArray == nil) {
        _titlesArray = [[NSMutableArray alloc] init];
    }
    return _titlesArray;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getSymbolRate];
    [self checkCurrentNetworkStatus];
    
    [self requestAnnouncementData];
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:@"overview" key:@"sub" deep:nil];
    //    [sr]
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:@"overview" key:@"unsub" deep:nil];
}

//- (void)rightBarButtonItemClicked {
//    JYSKLineViewController * klineVC = [[JYSKLineViewController alloc] init];
//    [self.navigationController pushViewController:klineVC animated:YES];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithImage:@"contact_search" highImage:@"contact_search" target:self action:@selector(rightBarButtonItemClicked)];
    
    [self requestData];
    
    //    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:[UIColor blackColor] titleFont:[UIFont systemFontOfSize:FontNum(15)] target:self action:@selector(rightBarButtonItemClicked) titleString:@"查看"];
    
    [[SRWebSocketTool sharedSRWebSocketTool] configWebSocketService];
    
    
    [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
        
        XSLog(@"key =%@   =%@",key,message);
        //        [self.allListDatasArray removeAllObjects];
        
        NSArray * dictAllKeys = [dic allKeys];
        if ([dictAllKeys containsObject:@"data"]) {
            NSArray * allDatasArr = dic[@"data"];
            if ([dic[@"ch"] isEqualToString:@"market.overview"]) {
                [self.allListDatasArray removeAllObjects];
                
                NSArray * dataArray = [JYSCurrencyTradingModel mj_objectArrayWithKeyValuesArray:allDatasArr];
                
                [self.allListDatasArray addObjectsFromArray:dataArray];
                
                [UserInstance ShardInstnce].coinDatasArray = self.allListDatasArray;
            }
        }
        
        if (self.childArray.count && self.allListDatasArray.count) {
            for (NSInteger j = 0; j < self.childArray.count; j++) {
                JYSCurrencyTradingListViewController * listVC = self.childArray[j];
                [listVC setListData:self.allListDatasArray titleString:self.titlesArray[j]];
            }
        }
        
        if (self.titlesArray.count == 0) {
            NSArray * dicJsonAllKeys = [dic allKeys];
            if ([dicJsonAllKeys containsObject:@"ch"]) {
                if ([dic[@"ch"] isEqualToString:@"market.overview"]) {
                    NSArray * titleDataArray = dic[@"data"];
                    //                    XSLog(@"%@",titleDataArray);
                    if ([titleDataArray isKindOfClass:[NSArray class]]) {
                        for (NSInteger i = 0; i < titleDataArray.count; i++) {
                            NSDictionary * titleDataDict = titleDataArray[i];
                            NSString * coinString = titleDataDict[@"ucoin"];
                            if (![self.titlesArray containsObject:coinString]) {
                                [self.titlesArray addObject:coinString];
                            }
                        }
                    }
                    XSLog(@"%@",self.titlesArray);
                    [self setupNavigationBar:self.titlesArray];
                }
            }
        }
    }];
}

- (void)rightBarButtonItemClicked {
    //搜索🔍
    JYSCurrencySearchViewController * searchVC = [[JYSCurrencySearchViewController alloc] init];
    //    __weak __typeof__(self) weakSelf = self;
    @weakify(self);
    searchVC.searchBlock = ^(JYSCurrencyTradingModel *tradingModel) {
        XSLog(@"%@",tradingModel.symbol);
        @strongify(self);
        self.currencyTradingModel = tradingModel;
        [self performSelector:@selector(pushToKlineVC) withObject:nil afterDelay:0.1f];
    };
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)pushToKlineVC {
    JYSKLineViewController * klineVC = [[JYSKLineViewController alloc] init];
    klineVC.currencyModel = self.currencyTradingModel;
    [self.navigationController pushViewController:klineVC animated:YES];
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSCurrencyTitlesURL params:nil HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        XSLog(@"%@",dic);
        if (state.status) {
            NSArray * dataArray = dic[@"data"];
            if (![dataArray isKindOfClass:[NSArray class]]) {
                return ;
            }
            if (self.titlesArray.count == 0) {
                
                [self.allListDatasArray removeAllObjects];
                
                for (NSInteger i = 0; i < dataArray.count; i++) {
                    NSDictionary * titleDataDict = dataArray[i];
                    NSString * coinString = [titleDataDict[@"quote-currency"] uppercaseString];
                    
                    if (![self.titlesArray containsObject:coinString]) {
                        [self.titlesArray addObject:coinString];
                    }
                }
                [self setupNavigationBar:self.titlesArray];
                
                NSArray * datas = [JYSCurrencyTradingModel mj_objectArrayWithKeyValuesArray:dataArray];
                
                [self.allListDatasArray addObjectsFromArray:datas];
                
                if (self.childArray.count && self.allListDatasArray.count) {
                    for (NSInteger j = 0; j < self.childArray.count; j++) {
                        JYSCurrencyTradingListViewController * listVC = self.childArray[j];
                        [listVC setListData:self.allListDatasArray titleString:self.titlesArray[j]];
                    }
                }
            }
        }
    } fail:^(NSError *error) {
        
    } showHUD:NO];
}

- (void)requestAnnouncementData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSAnnouncementURL params:nil HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            XSLog(@"%@",dic);
            [self.announceArray removeAllObjects];
            
            NSArray * datas = dic[@"data"];
            
            if ([datas isKindOfClass:[NSArray class]]) {
                
                NSArray * dataArr = [JYSCurrencyAnnouncementModel mj_objectArrayWithKeyValuesArray:datas];
                [self.announceArray addObjectsFromArray:dataArr];
                
                if (self.announceArray.count) {
                    [self.view addSubview:self.announcementView];
                    [self.view bringSubviewToFront:self.announcementView];
                    [self.announcementView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.mas_equalTo(self.view);
                        make.height.mas_equalTo(FontNum(44));
                    }];
                    
                    [self.announcementView setAnnouncementDataWithModel:self.announceArray];
                }
            }
        }
    } fail:^(NSError *error) {
        
    } showHUD:NO];
}

#pragma mark 检测是否有网，无网则提示刷新
- (void)checkCurrentNetworkStatus {
    if (![XSHTTPManager isHaveNetwork]) {
        UIAlertController * netStatusAlert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"当前无网络，请检查网络设置") preferredStyle:UIAlertControllerStyleAlert];
        
        @weakify(self);
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"刷新") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            [self requestData];
        }];
        
        UIAlertAction *action2=[UIAlertAction actionWithTitle:Localized(@"go_set") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if( [[UIApplication sharedApplication] canOpenURL:url] ) {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
                    
                }];
            }
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        
        [netStatusAlert addAction:action1];
        [netStatusAlert addAction:action2];
        //        [netStatusAlert addAction:cancel];
        
        [self presentViewController:netStatusAlert animated:YES completion:nil];
    }
}

- (void)setupNavigationBar:(NSArray *)titlesArray {
    //    NSArray *titleArr = @[@"CC", @"CTC", @"ABC", @"DDD", @"NBA", @"YY"];//, @"ICBC", @"WTO", @"VIP"];
    if (titlesArray.count ==0) {
        return;
    }
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleFont = [UIFont systemFontOfSize:FontNum(14) weight:UIFontWeightSemibold];
    configure.titleColor = COLOR_999999;
    configure.titleSelectedColor = XSYCOLOR(0x232426);
    configure.indicatorColor = JYSMainSelelctColor;
    configure.indicatorDynamicWidth = FontNum(2);
    /// pageTitleView
    // 这里的 - 10 是为了让 SGPageTitleView 超出父视图，给用户一种效果体验
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) delegate:self titleNames:titlesArray configure:configure];
    //    _pageTitleView.backgroundColor = [UIColor clearColor];
    // 对 navigationItem.titleView 的包装，为的是 让View 占据整个视图宽度
    [self.view addSubview:self.pageTitleView];
    
    
    //    oneVC.view.backgroundColor = [UIColor redColor];
    //    NSMutableArray *childArr = [NSMutableArray array];
    
    for (NSInteger i = 0; i < self.titlesArray.count; i++) {
        JYSCurrencyTradingListViewController * vc = [[JYSCurrencyTradingListViewController alloc] init];
        [vc setListData:self.allListDatasArray titleString:self.titlesArray[i]];
        [self.childArray addObject:vc];
    }
    XSLog(@"%@",self.childArray);
    //    NSArray *childArr = @[oneVC, twoVC, threeVC, fourVC, fiveVC, sixVC];//, sevenVC, eightVC, nineVC];
    /// pageContentView
    
    NSUInteger navHeight;
    NSUInteger tabBarHeight;
    if (SCREEN_HEIGHT == 812) {
        navHeight = 88;
        tabBarHeight = 83;
    } else {
        navHeight = 64;
        tabBarHeight = 49;
    }
    
    CGFloat contentViewHeight = SCREEN_HEIGHT - 44-tabBarHeight- navHeight;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:self.childArray];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
    
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}


#pragma mark --查询市场常见汇率
- (void)getSymbolRate
{
    [AppConfigManager ShardInstnce].rateConfig.rate = @"1";
    NSDictionary *param = @{
                            @"currency":@"cny"
                            };
    __weak __typeof__(self) wSelf = self;
    
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSSymbolRateURL params:param HUDShowView:self.view HUDAnimated:NO success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSDictionary *dicData = dic[@"data"];
            if ([dicData isKindOfClass:[NSDictionary class]]) {
               SymbolRateModel*tSymbolRateModel = [SymbolRateModel mj_objectWithKeyValues:dic[@"data"]];
                [AppConfigManager ShardInstnce].symbolRateModel = tSymbolRateModel;
                for (RateTypesModel*model in tSymbolRateModel.types) {
                    if ([tSymbolRateModel.default_coin isEqualToString:model.currency]) {
                        [AppConfigManager ShardInstnce].rateConfig = model;
                    }
                }
            }
        }else{
            Alert(state.info);
        }
    } fail:^(NSError *error) {
    } showHUD:NO];
}


- (void)dealloc
{
    XSLog(@"%s",__FUNCTION__);
    //    [[SRWebSocketTool sharedSRWebSocketTool] closeWebSocket];
}


@end
