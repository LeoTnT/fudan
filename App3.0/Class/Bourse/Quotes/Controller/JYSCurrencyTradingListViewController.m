//
//  JYSCurrencyTradingListViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradingListViewController.h"
#import "JYSCurrencyTradingListCell.h"
#import "JYSCurrencyTradingListTopView.h"
#import "JYSCurrencyTradingViewController.h"
#import "JYSCurrencyTradingModel.h"
#import "JYSKLineViewController.h"//k线图界面
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
@interface JYSCurrencyTradingListViewController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 排序View */
//@property (nonatomic, strong) JYSCurrencyTradingListTopView * topSortingView;

/** 数据 */
@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, strong) NSMutableArray * rateArray;

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;
@property (nonatomic, copy) NSString *rateStr;
@property (nonatomic, copy) NSString *titleStr;

@end

@implementation JYSCurrencyTradingListViewController

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (NSMutableArray *)rateArray {
    if (_rateArray == nil) {
        _rateArray = [[NSMutableArray alloc] init];
    }
    return _rateArray;
}

//- (JYSCurrencyTradingListTopView *)topSortingView {
//    if (_topSortingView == nil) {
//        _topSortingView = [[JYSCurrencyTradingListTopView alloc] init];
//        _topSortingView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
//    }
//    return _topSortingView;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
}

-(void)setListData:(NSArray *)listArr titleString:(NSString *)title {
    self.titleStr = title;
    [self.dataArray removeAllObjects];
    [self.rateArray removeAllObjects];

    if (listArr.count > 0) {
        NSString *base_coin = [AppConfigManager ShardInstnce].symbolRateModel.base_coin;
        
        for (NSInteger i = 0; i < listArr.count; i++) {
            JYSCurrencyTradingModel * model = listArr[i];
            if (!isEmptyString(base_coin)) {
                //基础兑换币种数据组
                if([[model.ucoin uppercaseString]  isEqualToString:base_coin]|| [[model.quotecurrency uppercaseString] isEqualToString:base_coin])
                {
                    [self.rateArray addObject:model];
                }
            }
           
            
            if ([model.ucoin isEqualToString:title] || [[model.quotecurrency uppercaseString] isEqualToString:title]) {
                [self.dataArray addObject:model];
            }
        }
        if (!isEmptyString(base_coin)) {

        if ([[title uppercaseString]  isEqualToString:base_coin]) {
            self.rateStr = @"1";
        }else{
            for (JYSCurrencyTradingModel * model  in self.rateArray ) {
            if ([[model.gcoin uppercaseString]  isEqualToString:title]) {
                self.rateStr = model.close;
            }
            }
        }

        }
        
        [self.tableView reloadData];
    }
}

static NSString * const cellID = @"JYSCurrencyTradingListCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSCurrencyTradingListCell class] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];

    self.shouldShowEmptyView = YES;
//    self.tableView.tableHeaderView = self.topSortingView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSCurrencyTradingListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataArray.count) {
        JYSCurrencyTradingModel * model = self.dataArray[indexPath.row];
        cell.titleStr = self.titleStr;
        [cell setDataWithModel:model rateStr:self.rateStr];
    }
    
    return cell;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    return self.topSortingView;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(66);
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 40;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    JYSCurrencyTradingDetailViewController * tradingDetailVC = [[JYSCurrencyTradingDetailViewController alloc] init];
//    [self.navigationController pushViewController:tradingDetailVC animated:YES];
    JYSKLineViewController * klineVC = [[JYSKLineViewController alloc] init];
    
    if (self.dataArray.count) {
        JYSCurrencyTradingModel * model = self.dataArray[indexPath.row];
        if (isEmptyString(model.ucoin)||isEmptyString(model.gcoin)) {
            model.ucoin = model.quotecurrency;
            model.gcoin = model.bsecurrency;
        }
        klineVC.currencyModel = model;
        [self.navigationController pushViewController:klineVC animated:YES];
    }
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"暂无订阅数据");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

@end
