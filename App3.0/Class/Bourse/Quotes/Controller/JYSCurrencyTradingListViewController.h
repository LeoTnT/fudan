//
//  JYSCurrencyTradingListViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface JYSCurrencyTradingListViewController : XSBaseTableViewController

- (void)setListData:(NSArray *)listArr titleString:(NSString *)title;

@end
