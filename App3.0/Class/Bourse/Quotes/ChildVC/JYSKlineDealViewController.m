//
//  JYSKlineDealViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKlineDealViewController.h"
#import "JYSlineDealCell.h"
#import "JYSCurrencyTradingModel.h"
#import "SRWebSocketTool.h"
#import "JYSKlineDealModel.h"
@interface JYSKlineDealViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *kLineDealTableView;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;

/** dataArray */
@property (nonatomic, strong) NSMutableArray * dataArray;

/** 请求字符串 */
@property (nonatomic, copy) NSString * requestString;

@end

@implementation JYSKlineDealViewController

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

static NSString * const cellID = @"JYSlineDealCellID";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.kLineDealTableView registerNib:[UINib nibWithNibName:NSStringFromClass([JYSlineDealCell class]) bundle:nil] forCellReuseIdentifier:cellID];
    
    [self requestData];
    
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:self.requestString key:@"unsub" deep:nil];
}

- (void)requestData {
    NSString * coinNameStr = self.currencyModel.symbol;
    self.requestString =[coinNameStr stringByAppendingString:@".trade.detail"];
    
    XSLog(@"%@",self.requestString);
    
    //请求最新成交记录
    [[SRWebSocketTool sharedSRWebSocketTool] sendMessage:self.requestString key:@"req" deep:nil];

    //市场最新成交记录
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:self.requestString key:@"sub" deep:nil];
    
    
    __weak typeof(self) weakSelf = self;
    [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
        
        XSLog(@"key =%@   =%@  =%@ ",key,message,dic);
        NSArray * dictAllKeys = [dic allKeys];
        if ([dictAllKeys containsObject:@"rep"] ||[dictAllKeys containsObject:@"ch"]) {
            NSString * typeString;
            if ([dictAllKeys containsObject:@"rep"]) {
                typeString = dic[@"rep"];
            } else {
                typeString = dic[@"ch"];
            }
            if (isEmptyString(weakSelf.requestString)) {
                return;
            }
            if ([typeString containsString:self.requestString]) {
                
                if ([dictAllKeys containsObject:@"rep"]) {
                    [weakSelf.dataArray removeAllObjects];
                }
                NSArray * datasArr = dic[@"data"];
                
                
                if ([datasArr isKindOfClass:[NSArray class]]) {
                    if (datasArr.count >50) {
                        [weakSelf.dataArray removeAllObjects];
                    }
                    
                    
                    [weakSelf.dataArray addObjectsFromArray:[JYSKlineDealModel mj_objectArrayWithKeyValuesArray:datasArr]];
                    
                    if (weakSelf.dataArray.count >100) {
                        [weakSelf.dataArray removeObjectsInRange:NSMakeRange(0, weakSelf.dataArray.count - 50)];
                    }
//                    weakSelf.dataArray = [NSMutableArray arrayWithArray:[[(NSArray*)weakSelf.dataArray reverseObjectEnumerator] allObjects]];
                    [weakSelf.kLineDealTableView reloadData];
                }
                //                }
            }
        }
        [weakSelf.kLineDealTableView reloadData];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSlineDealCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataArray.count) {
        
        NSArray * dealArray = [[self.dataArray reverseObjectEnumerator] allObjects];
        
        XSLog(@"%@\n=================\n%@",self.dataArray,dealArray);
        
        JYSKlineDealModel * model= dealArray[indexPath.row];
        [cell setKlineDealCellDataWithModel:model];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.tableHeaderView;
}

@end
