//
//  JYSKLineViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineViewController.h"
//#import "JYSKLineHeaderView.h"
#import "JYSKlineNativeHeaderView.h"
#import "JYSKLineTableView.h"
#import "JYSKlineDepthViewController.h"
#import "JYSKlineDealViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "JYSIntroduceViewController.h"
#import "JYSCurrencyTradingModel.h"
#import "AppDelegate.h"
#import "JYSKLineLandscapeVC.h"
#import "TradingCenterQuotesFullScreenVC.h"
#import "LoginViewController.h"

@interface JYSKLineViewController ()<UITableViewDataSource,UITableViewDelegate, SGPageTitleViewDelegate, SGPageContentViewDelegate,JYSKLineBaseChildVCDelegate>

@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;
/** headerView */
//@property (nonatomic, strong) JYSKLineHeaderView * headerView;
@property (nonatomic, strong) JYSKlineNativeHeaderView * headerView;

/** tableView */
@property (nonatomic, strong) JYSKLineTableView * kLineTableView;

@property (nonatomic, strong) UIScrollView *childVCScrollView;

/** 标题 */
@property (nonatomic, copy) NSString * titleString;

/** 头部高度 */
@property (nonatomic, assign) CGFloat headerHeight;



@end

@implementation JYSKLineViewController
{
    CGFloat KLineBottomViewHeight;
}
//- (JYSKLineHeaderView *)headerView {
//    if (_headerView == nil) {
//        _headerView = [[JYSKLineHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.headerHeight)];
////        _headerView.delegate = self;
//    }
//    return _headerView;
//}

- (JYSKlineNativeHeaderView *)headerView {
    if (_headerView == nil) {
        _headerView = [[JYSKlineNativeHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.headerHeight)];
        //        _headerView.delegate = self;
    }
    return _headerView;
}


static CGFloat const KLineTitleViewHeight = 44;
//static CGFloat const KLineBottomViewHeight = 60+kTabbarSafeBottomMargin;
//static CGFloat const KLineVCTopViewHeight = 420;
//static CGFloat const PersonalCenterVCNavHeight = 64;
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.titleString = [NSString stringWithFormat:@"%@"]
    
//    self.headerHeight = FontNum(420);
    self.headerHeight = 420;
    KLineBottomViewHeight = 60+kTabbarSafeBottomMargin;
    self.navigationController.fd_fullscreenPopGestureRecognizer.enabled = NO;
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:[UIColor whiteColor] titleFont:[UIFont systemFontOfSize:FontNum(15)] target:self action:@selector(rightBarButtonItemClicked) titleString:@"全屏"];
    
    [self setUpUI];
}

- (void)fullScreenBtnClick {
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.isEable = YES;
    
    JYSKLineLandscapeVC *stockChartVC = [JYSKLineLandscapeVC new];
    stockChartVC.symbolString = self.currencyModel.symbol;
    stockChartVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:stockChartVC animated:YES completion:nil];
}

- (void)setCurrencyModel:(JYSCurrencyTradingModel *)currencyModel {
    _currencyModel = currencyModel;
    
    self.titleString = [NSString stringWithFormat:@"%@/%@",currencyModel.gcoin,currencyModel.ucoin];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setNavigationStyleBlack:YES title:self.titleString];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self setNavigationStyleBlack:NO title:nil];
}

- (void)setUpUI {
//    http://tradefeature.abc123rt.com/index/kline/chart.html?period=1min&currency=ethcny&type=1&lang=zh
    
    self.kLineTableView = [[JYSKLineTableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:(UITableViewStylePlain)];
    self.kLineTableView.delegate = self;
    self.kLineTableView.dataSource = self;
    [self.kLineTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    self.kLineTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//    self.kLineTableView.backgroundColor = XSYCOLOR(0x181829);
    self.kLineTableView.sectionHeaderHeight = KLineTitleViewHeight;
    self.kLineTableView.rowHeight = self.view.frame.size.height - KLineTitleViewHeight;
    self.kLineTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.kLineTableView];
    self.kLineTableView.backgroundColor = XSYCOLOR(0x181829);
    [self.kLineTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, KLineBottomViewHeight, 0));
    }];
    
    self.kLineTableView.tableHeaderView = self.headerView;
    [self.headerView.fullScreenBtn addTarget:self action:@selector(fullScreenAction) forControlEvents:UIControlEventTouchUpInside];
//    [self.headerView setKlineSymbolString:self.currencyModel.symbol];
    
    
    UIView * bottomView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:XSYCOLOR(0x292C39)];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(KLineBottomViewHeight);
    }];
    
    UIButton * buyButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(buyButtonClicked) titleColor:[UIColor whiteColor] titleFont:FontNum(16) backgroundColor:[UserInstance ShardInstnce].roseColor image:nil backgroundImage:nil title:Localized(@"buy")];
    [bottomView addSubview:buyButton];
    [buyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.centerX.mas_equalTo(bottomView).multipliedBy(0.5);
        make.size.mas_equalTo(CGSizeMake(FontNum(162), FontNum(40)));
    }];
    
    UIButton * soldButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(soldButtonClicked) titleColor:[UIColor whiteColor] titleFont:FontNum(16) backgroundColor:[UserInstance ShardInstnce].fellColor image:nil backgroundImage:nil title:Localized(@"selltwo")];
    [bottomView addSubview:soldButton];
    [soldButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.centerX.mas_equalTo(bottomView).multipliedBy(1.5);
        make.size.mas_equalTo(CGSizeMake(FontNum(162), FontNum(40)));
    }];
    
    UIButton * fullScreenButton = [XSUITool buttonWithFrame:CGRectMake(mainWidth-FontNum(30), 0, FontNum(30), FontNum(30)) buttonType:UIButtonTypeCustom Target:self action:@selector(fullScreenBtnClick) titleColor:nil titleFont:10 backgroundColor:[UIColor clearColor] image:nil backgroundImage:nil title:nil];
    [self.headerView addSubview:fullScreenButton];
    
    [fullScreenButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(self.headerView);
        make.size.mas_equalTo(CGSizeMake(FontNum(30), FontNum(30)));
    }];
    
    [self.headerView setDataWithCoinModel:self.currencyModel];
}

- (void)buyButtonClicked {
//    [[UserSingle sharedUserSingle].siginleSubject sendNext:RACTuplePack(@"asd",self.currencyModel.symbol];
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        LoginViewController *login = [[LoginViewController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [[UserSingle sharedUserSingle].siginleSubject sendNext:RACTuplePack(@"buy",self.currencyModel)];
        self.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self removeFromParentViewController];
    }
}

- (void)soldButtonClicked {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        LoginViewController *login = [[LoginViewController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [[UserSingle sharedUserSingle].siginleSubject sendNext:RACTuplePack(@"sell",self.currencyModel)];
        self.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self removeFromParentViewController];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.childVCScrollView && _childVCScrollView.contentOffset.y > 0) {
        self.kLineTableView.contentOffset = CGPointMake(0, self.headerHeight);
    }
    CGFloat offSetY = scrollView.contentOffset.y;
    if (offSetY < self.headerHeight) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pageTitleViewToTop" object:nil];
    }
}

-(void)headerWebDidFinisheLoadWith:(CGFloat)height {
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
//    self.kLineTableView.tableHeaderView.xs_height = height;
//    self.headerHeight = height;
}


- (void)setNavigationStyleBlack:(BOOL)isBlack title:(NSString *)title {
    
    if (title) {
        self.navigationItem.title = title;
    }
    self.navigationController.navigationBar.translucent = YES;
    
    if (isBlack) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        [self.navigationController.navigationBar setBackgroundImage:[UIImage yy_imageWithColor:XSYCOLOR(0x131F30)]  forBarMetrics:UIBarMetricsDefault];
        
        NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
        attrs[NSForegroundColorAttributeName] = XSYCOLOR(0xFFFFFF);
        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(18)];
        [self.navigationController.navigationBar setTitleTextAttributes:attrs];
        [self.navigationController.navigationBar setShadowImage:[UIImage xl_imageWithColor:[UIColor clearColor] size:CGSizeMake(SCREEN_WIDTH, 0.5)]];
        self.navigationController.navigationBar.translucent = NO;
    } else {

        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        [self.navigationController.navigationBar setBackgroundImage:
         nil forBarMetrics:UIBarMetricsDefault];
//        NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
//        attrs[NSForegroundColorAttributeName] = XSYCOLOR(0x111111);
//
//        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:18];
//        [self.navigationController.navigationBar setTitleTextAttributes:attrs];
//        [self.navigationController.navigationBar setShadowImage:[UIImage xl_imageWithColor:XSYCOLOR(0xE7E7E7) size:CGSizeMake(SCREEN_WIDTH, 0.5)]];
        NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
        attrs[NSForegroundColorAttributeName] = XSYCOLOR(0x333333);
        //    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(18)];
        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
        [self.navigationController.navigationBar setTitleTextAttributes:attrs];
        
        [self.navigationController.navigationBar setShadowImage:nil];
        self.navigationController.navigationBar.translucent = NO;
    }
}

- (SGPageTitleView *)pageTitleView {
    if (!_pageTitleView) {
        NSArray *titleArr = @[Localized(@"kinf_depth"), Localized(@"kinf_exchange"), Localized(@"jianjie")];
        SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
        configure.titleColor = XSYCOLOR(0x636A8D);
        configure.titleSelectedColor = XSYCOLOR(0x5B85E3);
        configure.indicatorColor = XSYCOLOR(0x5B85E3);
        /// pageTitleView
        _pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, self.view.frame.size.width, KLineTitleViewHeight) delegate:self titleNames:titleArr configure:configure];
        _pageTitleView.backgroundColor = XSYCOLOR(0x181829);
    }
    return _pageTitleView;
}

- (SGPageContentView *)pageContentView {
    if (!_pageContentView) {
        JYSKlineDepthViewController *oneVC = [[JYSKlineDepthViewController alloc] init];
        oneVC.currencyModel = self.currencyModel;
        oneVC.delegate = self;
        
        JYSKlineDealViewController *twoVC = [[JYSKlineDealViewController alloc] init];
        twoVC.currencyModel = self.currencyModel;
        twoVC.delegate = self;

        JYSIntroduceViewController *threeVC = [[JYSIntroduceViewController alloc] init];
//        threeVC.delegate = self;
        threeVC.currency =self.currencyModel.gcoin;
        NSArray *childArr = @[oneVC, twoVC, threeVC];
        /// pageContentView
        CGFloat contentViewHeight = self.view.xs_height - KLineTitleViewHeight - KLineBottomViewHeight;
        _pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, contentViewHeight) parentVC:self childVCs:childArr];
        _pageContentView.backgroundColor = XSYCOLOR(0x181829);
        _pageContentView.delegatePageContentView = self;
    }
    return _pageContentView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.backgroundColor = XSYCOLOR(0x181829);
    [cell.contentView addSubview:self.pageContentView];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.pageTitleView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.view.xs_height - KLineTitleViewHeight;
}

#pragma mark - - - SGPageTitleViewDelegate - SGPageContentViewDelegate
- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    self.kLineTableView.scrollEnabled = NO;
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView offsetX:(CGFloat)offsetX {
    self.kLineTableView.scrollEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)jysKLineBaseChildVCScrollViewDidScroll:(UIScrollView *)scrollView {
    self.childVCScrollView = scrollView;
    if (self.kLineTableView.contentOffset.y < self.headerHeight) {
        scrollView.contentOffset = CGPointZero;
        scrollView.showsVerticalScrollIndicator = NO;
    } else {
        self.kLineTableView.contentOffset = CGPointMake(0, self.headerHeight);
        scrollView.showsVerticalScrollIndicator = YES;
    }
}

- (void)fullScreenAction {
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.isEable = YES;
    
    TradingCenterQuotesFullScreenVC *stockChartVC = [TradingCenterQuotesFullScreenVC new];
//    stockChartVC.idString = self.idStr;
     [stockChartVC setDataWithCoinModel:self.currencyModel];
    stockChartVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:stockChartVC animated:YES completion:nil];
}


@end
