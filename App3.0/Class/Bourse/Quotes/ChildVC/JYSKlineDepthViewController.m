//
//  JYSKlineDepthViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKlineDepthViewController.h"
#import "JYSKlineDepthCell.h"
#import "SRWebSocketTool.h"
#import "JYSCurrencyTradingModel.h"

@interface JYSKlineDepthViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *depthTableView;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *priceLb;
@property (weak, nonatomic) IBOutlet UILabel *buyTitleLb;
@property (weak, nonatomic) IBOutlet UILabel *sellTitleLb;

/** 深度 */
@property (nonatomic, copy) NSString * deepString;

/** 请求字符串 */
@property (nonatomic, copy) NSString * requestString;

/** 买 */
@property (nonatomic, strong) NSMutableArray * asksArray;

/** 卖 */
@property (nonatomic, strong) NSMutableArray * bidsArray;

/** 涨数量值总值 */
@property (nonatomic, assign) CGFloat sumRoseValue;
/** 跌数量值总值 */
@property (nonatomic, assign) CGFloat sumFellValue;

/** 涨数量值分子 前i项的和 */
//@property (nonatomic, assign) CGFloat numeratorRoseValue;
/** 跌数量值分子 */
//@property (nonatomic, assign) CGFloat numeratorFellValue;
/** 深度计算分子数组  涨 */
@property (nonatomic, strong) NSMutableArray * roseNumerators;
/** 深度计算分子数组  跌 */
@property (nonatomic, strong) NSMutableArray * fellNumerators;

@end

@implementation JYSKlineDepthViewController
- (NSMutableArray *)roseNumerators {
    if (_roseNumerators == nil) {
        _roseNumerators = [[NSMutableArray alloc] init];
    }
    return _roseNumerators;
}
- (NSMutableArray *)fellNumerators {
    if (_fellNumerators == nil) {
        _fellNumerators = [[NSMutableArray alloc] init];
    }
    return _fellNumerators;
}

- (NSMutableArray *)asksArray {
    if (_asksArray == nil) {
        _asksArray = [[NSMutableArray alloc] init];
    }
    return _asksArray;
}
- (NSMutableArray *)bidsArray {
    if (_bidsArray == nil) {
        _bidsArray = [[NSMutableArray alloc] init];
    }
    return _bidsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self makeUI];
    
    self.deepString = @"2";
    
    [self requestData];
}

static NSString * const cellID = @"JYSKlineDepthCellID";
- (void)makeUI {
    [self.depthTableView registerNib:[UINib nibWithNibName:NSStringFromClass([JYSKlineDepthCell class]) bundle:nil] forCellReuseIdentifier:cellID];
}

- (void)requestData {
    
    self.priceLb.text = [NSString stringWithFormat:@"%@(%@)",Localized(@"submit_price"),[self.currencyModel.ucoin uppercaseString]];
    self.buyTitleLb.text = [NSString stringWithFormat:@"%@(%@)",Localized(@"mpsl"),[self.currencyModel.gcoin uppercaseString]];
    self.sellTitleLb.text = [NSString stringWithFormat:@"%@(%@) %@",Localized(@"exchange_volume"),[self.currencyModel.gcoin uppercaseString],Localized(@"maipan")];
    
    NSString * coinNameStr = self.currencyModel.symbol;
    
    self.requestString =[coinNameStr stringByAppendingString:@".depth.step"];
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:self.requestString key:@"sub" deep:self.deepString];
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendMessage:self.requestString key:@"req" deep:self.deepString];
    
    NSString* parameterString = [self.requestString stringByAppendingString:self.deepString];
    XSLog(@"%@",parameterString);
    
    __weak typeof(self) weakSelf = self;
    [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
        
        XSLog(@"key =%@   =%@  =%@ ",key,message,dic);
        NSArray * dictAllKeys = [dic allKeys];
        
        if ([dictAllKeys containsObject:@"rep"] ||[dictAllKeys containsObject:@"ch"]) {
            NSString * typeString;
            if ([dictAllKeys containsObject:@"rep"]) {
                typeString = dic[@"rep"];
            } else {
                typeString = dic[@"ch"];
            }
            if (isEmptyString(parameterString)) {
                return;
            }
            if ([typeString hasSuffix:parameterString]) {
                [weakSelf.asksArray removeAllObjects];
                [weakSelf.bidsArray removeAllObjects];
                if ([dictAllKeys containsObject:@"tick"]) {
                    NSDictionary * tickDict = dic[@"tick"];
                    NSArray * tickDictKeys = [tickDict allKeys];
                    if ([tickDictKeys containsObject:@"asks"]) {
                        NSArray * asksA = tickDict[@"asks"];
                        if ([asksA isKindOfClass:[NSArray class]]) {
                            [weakSelf.asksArray addObjectsFromArray:tickDict[@"asks"]];

                            [self.roseNumerators removeAllObjects];
//                            NSMutableArray * temp1Array = [NSMutableArray array];
                            for (int i = 0; i < weakSelf.asksArray.count; i++) {
                                NSArray * asksArr = weakSelf.asksArray[i];
                                [self.roseNumerators addObject:asksArr[1]];
                            }
                    
                            self.sumRoseValue = [[self.roseNumerators valueForKeyPath:@"@sum.floatValue"] floatValue];
                            
                            XSLog(@"%f",self.sumRoseValue);
                            
//                            __block NSInteger sum = 0;
//                            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//                            dispatch_apply(data.count, queue, ^(size_t i) {
//                                sum += [data[i] integerValue];
//                            });
//                            NSLog(@"sum = %ld",(long)sum);
                        }
                    }
                    if ([tickDictKeys containsObject:@"bids"]) {
                        NSArray * bidsA = tickDict[@"bids"];
                        if ([bidsA isKindOfClass:[NSArray class]]) {
                            [weakSelf.bidsArray addObjectsFromArray:tickDict[@"bids"]];
                            
                            [self.fellNumerators removeAllObjects];
//                            NSMutableArray * temp2Array = [NSMutableArray array];
                            for (int i = 0; i < weakSelf.bidsArray.count; i++) {
                                NSArray * bidsArr = weakSelf.bidsArray[i];
                                [self.fellNumerators addObject:bidsArr[1]];
                            }

                            self.sumFellValue = [[self.fellNumerators valueForKeyPath:@"@sum.floatValue"] floatValue];
                            
                            XSLog(@"%f",self.sumFellValue);
                        }
                    }
                    [weakSelf.depthTableView reloadData];
                }
            }
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.asksArray.count >= self.bidsArray.count?self.asksArray.count:self.bidsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSKlineDepthCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.asksArray.count) {
        NSArray * tempArr = [self.roseNumerators subarrayWithRange:NSMakeRange(0, indexPath.row)];
        CGFloat tempNum = [[tempArr valueForKeyPath:@"@sum.floatValue"] floatValue];
//        [cell setDataWithBidsArray:self.asksArray[indexPath.row] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] maxBidsCount:self.maxRoseValue];
        [cell setDataWithBidsArray:self.asksArray[indexPath.row] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] sumBidsCount:self.sumRoseValue Numerators:tempNum];
    } else {
//        [cell setDataWithBidsArray:@[] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] maxBidsCount:self.maxRoseValue];
        [cell setDataWithBidsArray:@[] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] sumBidsCount:self.sumRoseValue Numerators:0.0];
    }
    
    if (indexPath.row < self.bidsArray.count) {
//        [cell setDataWithAsksArray:self.bidsArray[indexPath.row] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] maxAsksCount:self.maxFellValue];
        NSArray * tempArr1 = [self.fellNumerators subarrayWithRange:NSMakeRange(0, indexPath.row)];
        CGFloat tempNum1 = [[tempArr1 valueForKeyPath:@"@sum.floatValue"] floatValue];
        
        [cell setDataWithAsksArray:self.bidsArray[indexPath.row] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] sumAsksCount:self.sumFellValue Numerators:tempNum1];
    } else {
//        [cell setDataWithAsksArray:@[] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] maxAsksCount:self.maxFellValue];
        [cell setDataWithAsksArray:@[] Index:[NSString stringWithFormat:@"%li",indexPath.row+1] sumAsksCount:self.sumFellValue Numerators:0.0];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(44);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.tableHeaderView;
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendNewMessage:self.requestString key:@"unsub" deep:self.deepString];
}

@end
