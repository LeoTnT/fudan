//
//  JYSKLineLandscapeVC.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface JYSKLineLandscapeVC : XSBaseViewController

/** 币种 */
@property (nonatomic, copy) NSString * symbolString;

@end
