//
//  TradingCenterQuotesFullScreenVC.h
//  App3.0
//
//  Created by sunzhenkun on 2018/1/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
@class JYSCurrencyTradingModel;

@interface TradingCenterQuotesFullScreenVC : XSBaseViewController

/** idString */
//@property (nonatomic, copy) NSString * idString;

- (void)setDataWithCoinModel:(JYSCurrencyTradingModel *)coinModel;

@end
