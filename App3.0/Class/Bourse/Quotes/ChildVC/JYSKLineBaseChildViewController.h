//
//  JYSKLineBaseChildViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JYSKLineBaseChildVCDelegate <NSObject>

- (void)jysKLineBaseChildVCScrollViewDidScroll:(UIScrollView *)scrollView;

@end

@interface JYSKLineBaseChildViewController : UIViewController

/** 代理 */
@property (nonatomic, weak) id<JYSKLineBaseChildVCDelegate>delegate;

@end
