//
//  JYSKLineViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
@class JYSCurrencyTradingModel;
@interface JYSKLineViewController : XSBaseViewController

/** 币数据模型 */
@property (nonatomic, strong) JYSCurrencyTradingModel * currencyModel;

@end
