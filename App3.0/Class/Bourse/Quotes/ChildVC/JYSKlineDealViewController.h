//
//  JYSKlineDealViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineBaseChildViewController.h"
@class JYSCurrencyTradingModel;
@interface JYSKlineDealViewController : JYSKLineBaseChildViewController

/** 交易数据模型 */
@property (nonatomic, strong) JYSCurrencyTradingModel * currencyModel;

@end
