//
//  JYSKLineBaseChildViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineBaseChildViewController.h"

@interface JYSKLineBaseChildViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation JYSKLineBaseChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageTitleViewToTop) name:@"pageTitleViewToTop" object:nil];
    
    self.scrollView.backgroundColor = XSYCOLOR(0x181829);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)pageTitleViewToTop {
    _scrollView.contentOffset = CGPointZero;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = scrollView;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(jysKLineBaseChildVCScrollViewDidScroll:)]) {
        [self.delegate jysKLineBaseChildVCScrollViewDidScroll:scrollView];
    }
}

@end
