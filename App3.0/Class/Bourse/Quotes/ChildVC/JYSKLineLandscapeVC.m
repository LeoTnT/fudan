//
//  JYSKLineLandscapeVC.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineLandscapeVC.h"
#import <WebKit/WebKit.h>
#import "AppDelegate.h"
@interface JYSKLineLandscapeVC ()<WKNavigationDelegate,UIScrollViewDelegate,WKScriptMessageHandler>

@property (assign, nonatomic) NSUInteger loadCount;
@property (strong, nonatomic) UIProgressView *progressView;
@property (strong, nonatomic) WKWebView *wkWebView;

/**  */
@property (nonatomic, strong) WKWebViewConfiguration * wkConfig;

@end

@implementation JYSKLineLandscapeVC

- (UIProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0);
        _progressView.tintColor = mainColor;
        _progressView.trackTintColor = [UIColor whiteColor];
        [self.view addSubview:_progressView];
    }
    return _progressView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
}

- (void)setUpUI {
    self.view.backgroundColor = XSYCOLOR(0x181829);
    
//    //网址
//    NSURL * theWebUrl = [NSURL URLWithString:@"http://200-coin.dsceshi.cn/index/kline/chart.html?period=1min&currency=ethcny&type=1&lang=zh"];
    //    self.currentUrlString = self.urlString;
    
    NSString * urlString = [NSString stringWithFormat:@"%@index/kline/chart.html?period=1min&type=1&lang=zh&currency=%@",JYSBaseURL,self.symbolString];
    XSLog(@"%@",urlString);
    //网址
    NSURL * theWebUrl = [NSURL URLWithString:urlString];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.preferences.minimumFontSize = 18;
    self.wkConfig = config;
    
    WKWebView *wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) configuration:config];
    
    //            if (@available(iOS 11.0, *)) {
    //                wkWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    //            }
    
    wkWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    wkWebView.backgroundColor = XSYCOLOR(0x181829);
    wkWebView.navigationDelegate = self;
    [self.view insertSubview:wkWebView belowSubview:self.progressView];
    
    //wkWebView.sc
    [wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [wkWebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:theWebUrl];
    [wkWebView loadRequest:request];
    
    WKUserContentController *userCC = config.userContentController;
    //JS调用OC 添加处理脚本
    //            [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"popToPersonalCenter"];
    [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"toPay"];
    [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"toAdvert"];
    
    self.wkWebView = wkWebView;
    
    UIButton * backBtn = [XSUITool buttonWithFrame:CGRectMake(mainWidth-FontNum(30), 0, FontNum(30), FontNum(30)) buttonType:UIButtonTypeCustom Target:self action:@selector(backBtnClick) titleColor:nil titleFont:10 backgroundColor:[UIColor clearColor] image:nil backgroundImage:nil title:nil];
    [self.view addSubview:backBtn];
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(FontNum(30), FontNum(30)));
    }];
}

- (void)backBtnClick {
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.isEable = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    XSLog(@"%@",NSStringFromSelector(_cmd));
    XSLog(@"%@",message.body);
    if ([message.name isEqualToString:@"toPay"]){
        NSArray *arr = message.body;
        XSLog(@"%@",arr);
    }
    if ([message.name isEqualToString:@"toAdvert"]){
        NSDictionary * adDataDict = message.body;
        XSLog(@"%@",adDataDict);
    }
}

-(void)paiedSuccessed {
    [self.wkWebView reload];
}

#pragma mark - wkWebView代理
// 如果不添加这个，那么wkwebview跳转不了AppStore
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if ([webView.URL.absoluteString hasPrefix:@"https://itunes.apple.com"]) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        decisionHandler(WKNavigationActionPolicyCancel);
    }else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}
-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object == self.wkWebView) {
            CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
            if (newprogress == 1) {
                self.progressView.hidden = YES;
                [self.progressView setProgress:0 animated:NO];
                
            }else {
                self.progressView.hidden = NO;
                [self.progressView setProgress:newprogress animated:YES];
            }
        }
    } else if ([keyPath isEqualToString:@"title"]){
        if (object == self.wkWebView) {
            XSLog(@"%@",self.wkWebView.title);
        }
    }
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.background='#181829'" completionHandler:nil];

}

- (void)setWebViewBackToFront {
    [self.wkWebView goBack];
}
// 记得取消监听
- (void)dealloc {
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.wkWebView removeObserver:self forKeyPath:@"title"];
    [self.wkConfig.userContentController removeScriptMessageHandlerForName:@"toPay"];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
