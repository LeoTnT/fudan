//
//  JYSKlineDealModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSKlineDealModel : NSObject

/** 成交量 */
@property (nonatomic, copy) NSString * amount;
/** 成交时间 */
@property (nonatomic, copy) NSString * time;
/** 消息ID */
@property (nonatomic, copy) NSString * ID;
/** 成交价 */
@property (nonatomic, copy) NSString * price;

/** 成交ID */
@property (nonatomic, copy) NSString * tradeId;
/** 成交方向 */
@property (nonatomic, copy) NSString * direction;

/** 时间戳 */
@property (nonatomic, copy) NSString * ts;

@end
