//
//  TradingCenterQuotesFullScreenVC.m
//  App3.0
//
//  Created by sunzhenkun on 2018/1/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TradingCenterQuotesFullScreenVC.h"
#import "Y_StockChartView.h"
//#import "NetWorking.h"
#import "Y_KLineGroupModel.h"
#import "UIColor+Y_StockChart.h"
#import "AppDelegate.h"
#import "JYSCurrencyTradingModel.h"
#import "SRWebSocketTool.h"


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)

@interface TradingCenterQuotesFullScreenVC ()<Y_StockChartViewDataSource>

@property (nonatomic, strong) Y_StockChartView *stockChartView;

@property (nonatomic, strong) Y_KLineGroupModel *groupModel;

@property (nonatomic, copy) NSMutableDictionary <NSString*, Y_KLineGroupModel*> *modelsDict;

///** 定时器 */
//@property (nonatomic, strong) NSTimer * loadDataTimer;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, copy) NSString *type;

/** 币种 */
@property (nonatomic, strong) JYSCurrencyTradingModel * coinModel;

@end

@implementation TradingCenterQuotesFullScreenVC

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarHidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
//    [self.loadDataTimer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentIndex = -1;
    
//    self.loadDataTimer = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
//    [[NSRunLoop currentRunLoop] addTimer:self.loadDataTimer forMode:NSRunLoopCommonModes];
    
    self.stockChartView.backgroundColor = [UIColor backgroundColor];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpViews];
    
    [self.modelsDict removeAllObjects];
    self.stockChartView.dataSource = self;
}

- (void)timerFired {
    [self reloadData];
}

- (void)setDataWithCoinModel:(JYSCurrencyTradingModel *)coinModel {
    self.coinModel = coinModel;
}

- (void)setUpViews {
//    UIImageView * closeImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"TradingCenter_Close"]];
//    [self.view addSubview:closeImageView];
//    [closeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.right.mas_equalTo(self.view);
//        make.size.mas_equalTo(CGSizeMake(20, 20));
//    }];
    
    UIButton * closeBtn = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:nil action:nil];
    [closeBtn setImage:[UIImage imageNamed:@"TradingCenter_Close"] forState:UIControlStateNormal];
    [self.view addSubview:closeBtn];
    [closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.right.mas_equalTo(-kTabbarSafeBottomMargin);
        make.size.mas_equalTo(CGSizeMake(FontNum(35), FontNum(35)));
    }];
    [self.view bringSubviewToFront:closeBtn];
}

- (void)closeBtnClick {
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.isEable = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableDictionary<NSString *, Y_KLineGroupModel *> *)modelsDict {
    if (_modelsDict == nil) {
        _modelsDict = [[NSMutableDictionary alloc] init];
    }
    return _modelsDict;
}

-(id)stockDatasWithIndex:(NSInteger)index {
    NSString * type;
    switch (index) {
        case 0:
        {
            type = @"1min";
        }
            break;
        case 1:
        {
            type = @"1min";
        }
            break;
        case 2:
        {
            type = @"1min";
        }
            break;
        case 3:
        {
            type = @"5min";
        }
            break;
        case 4:
        {
            type = @"30min";
        }
            break;
        case 5:
        {
            type = @"60min";
        }
            break;
        case 6:
        {
            type = @"1day";
        }
            break;
        case 7:
        {
            type = @"1week";
        }
            break;
            
        default:
            break;
    }
    
    self.currentIndex = index;
    self.type = type;
    
    if (![self.modelsDict objectForKey:type]) {
        [self reloadData];
    } else {
        return [self.modelsDict objectForKey:type].models;
    }
    
    return nil;
}

- (void)reloadData
{
//    [XSTool showProgressHUDWithView:self.view];
    
    if (self.type.length) {
    } else {
        return;
    }
    
    NSNumber * currentTime = [self getNowTimeTimestamp];
    NSNumber * beforeTime = [self getBeforeTimeTimestamp:1];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        XSLog(@"%@",[NSThread currentThread]);
        
        [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"unsub" period:self.type fromTime:nil toTime:nil ID:self.coinModel.ID];
        //请求K线图数据
        [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"req" period:self.type fromTime:beforeTime toTime:currentTime ID:self.coinModel.ID];
        [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"sub" period:self.type fromTime:beforeTime toTime:currentTime ID:self.coinModel.ID];
        
        __weak typeof(self) weakSelf = self;
        [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
            RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
            
            XSLog(@"key =%@   =%@  =%@ ",key,message,dic);
            NSArray * dictAllKeys = [dic allKeys];
            if ([dictAllKeys containsObject:@"rep"] ||[dictAllKeys containsObject:@"ch"]) {
                NSString * typeString;
                if ([dictAllKeys containsObject:@"rep"]) {
                    typeString = dic[@"rep"];
                } else {
                    typeString = dic[@"ch"];
                }
                if (weakSelf.type.length == 0) {
                    return ;
                }
                
                if ([typeString hasSuffix:weakSelf.type]) {
                    if ([dictAllKeys containsObject:@"tick"]) {
                        NSArray * datasArr = dic[@"tick"];
                        if ([datasArr isKindOfClass:[NSArray class]]) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                XSLog(@"%@",[NSThread currentThread]);
                                
                                Y_KLineGroupModel *groupModel = [Y_KLineGroupModel objectWithArray:datasArr];
                                //            XSLog(@"%@",groupModel);
                                weakSelf.groupModel = groupModel;
                                [weakSelf.modelsDict setObject:groupModel forKey:weakSelf.type];
                                [weakSelf.stockChartView reloadData];
                                if (datasArr.count == 0) {
                                    [weakSelf.stockChartView removeChildView];
                                }
                            });
                        }
                    }
                }
            }
        }];
    });
}

- (Y_StockChartView *)stockChartView
{
    if(!_stockChartView) {
        _stockChartView = [Y_StockChartView new];
        _stockChartView.currentScreenOrientation = ScreenOrientationHorizontal;
        _stockChartView.itemModels = @[
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"指标") type:Y_StockChartcenterViewTypeOther],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fens") type:Y_StockChartcenterViewTypeTimeLine],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen1") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen5") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen30") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen60") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"rixian") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"zhouxian") type:Y_StockChartcenterViewTypeKline],
                                       
                                       ];
        _stockChartView.backgroundColor = [UIColor orangeColor];
        _stockChartView.dataSource = self;
        [self.view addSubview:_stockChartView];
        [_stockChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if (IS_IPHONE_X) {
                make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 30, 0, 0));
            } else {
                make.edges.equalTo(self.view);
            }
            
        }];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
//        tap.numberOfTapsRequired = 2;
//        [self.view addGestureRecognizer:tap];
    }
    return _stockChartView;
}

//-(void)dismiss {
//    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//
//    appdelegate.isEable = NO;
//
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}
- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSNumber *)getNowTimeTimestamp{
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    NSNumber * currentTime = @([timeString integerValue]);
    
    return currentTime;
    
}

- (NSNumber *)getBeforeTimeTimestamp:(NSInteger)days{
    
    NSDate * date = [NSDate date];//当前时间
    NSDate *beforData = [NSDate dateWithTimeInterval:-24*60*60*days sinceDate:date];//之前时间
    
    NSTimeInterval a=[beforData timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    NSNumber * currentTime = @([timeString integerValue]);
    
    return currentTime;
    
}

- (void)dealloc
{
    [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"unsub" period:self.type fromTime:nil toTime:nil ID:self.coinModel.ID];
}



@end
