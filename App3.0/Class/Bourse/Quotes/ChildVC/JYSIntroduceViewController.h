//
//  JYSIntroduceViewController.h
//  App3.0
//
//  Created by xinshang on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineBaseChildViewController.h"

@interface JYSIntroduceViewController : JYSKLineBaseChildViewController

@property (nonatomic,copy) NSString *currency;

@end
