//
//  JYSIntroduceViewController.m
//  App3.0
//
//  Created by xinshang on 2018/5/14.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSIntroduceViewController.h"
#import "JYSTradeCoinModel.h"
@interface JYSIntroduceViewController ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,WKNavigationDelegate>

@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) JYSTradeCoinModel *coinModel;
@property (nonatomic, strong) UILabel *noteLb;
@property(nonatomic,strong) UIWebView *webView;
@property(nonatomic,assign)CGFloat webHeight;

/** tbaleView */
@property (nonatomic, strong) UITableView * tableView;

/** wkWebView */
@property (nonatomic, strong) WKWebView * footerWebView;
/** footerView */
@property (nonatomic, strong) UIView * footerView;

@end

@implementation JYSIntroduceViewController

- (WKWebView *)footerWebView {
    if (_footerWebView == nil) {
        _footerWebView = [[WKWebView alloc] init];
        _footerWebView.frame = CGRectMake(0, 0, mainWidth, CGFLOAT_MIN);
        _footerWebView.scrollView.bounces = NO;
        _footerWebView.scrollView.showsVerticalScrollIndicator = NO;
        //        _cellWebView.configuration
        _footerWebView.navigationDelegate = self;
    }
    return _footerWebView;
}

- (UIView *)footerView {
    if (_footerView == nil) {
        _footerView = [[UIView alloc] init];
        _footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(300));
        _footerView.backgroundColor = XSYCOLOR(0x181829);
    }
    return _footerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"jianjie");
    self.listArr = [NSMutableArray arrayWithArray:@[@"title",Localized(@"fxtime"),Localized(@"fxall"),Localized(@"ltAll"),Localized(@"发行价格"),Localized(@"bps"),Localized(@"guanwang"),Localized(@"qukuai_find")]];

    
    [self setUpUI];
}

- (void)setUpUI {
//    self.tableViewStyle = UITableViewStylePlain;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.view.xs_height) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = XSYCOLOR(0x181829);
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.footerView addSubview:self.footerWebView];
    self.tableView.tableFooterView = self.footerView;
//    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //    self.tableView.tableHeaderView = self.topSortingView;
    
    [self getCoinDetail];
    
//    if (self.coinModel.intro) {
//        [self.footerWebView loadHTMLString:self.coinModel.intro baseURL:[NSURL URLWithString:ImageBaseUrl]];
//    }
    
}
-(void)setCurrency:(NSString *)currency
{
    _currency = currency;
//    [self getCoinDetail];
}
- (void)getCoinDetail
{
    if (self.currency) {
        NSString *tCurrency = [NSString stringWithFormat:@"%@",self.currency];
        NSDictionary *param = @{
                                @"currency":tCurrency
                                };
        __weak __typeof__(self) wSelf = self;
        
        __weak typeof(self) weakSelf = self;
        [JYSAFNetworking getOrPostWithType:POST withUrl:JYSCoinDetailURL
                                    params:param HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
            if (state) {

                NSDictionary *dicData = dic[@"data"];
                if ([dicData isKindOfClass:[NSDictionary class]]) {
                    weakSelf.coinModel = [JYSTradeCoinModel mj_objectWithKeyValues:dic[@"data"]];
                    
                    if (weakSelf.coinModel.intro) {
                        NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
                        [weakSelf.footerWebView loadHTMLString:[headerString stringByAppendingString:weakSelf.coinModel.intro] baseURL:[NSURL URLWithString:ImageBaseUrl]];
                    }
                }
            }
            [wSelf.tableView reloadData];

        } fail:^(NSError *error) {
            
        } showHUD:YES];
    }
}


#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return self.listArr.count;
    }
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"UITableViewCell_%ld_%ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = Color(@"636A8D");
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.textColor = Color(@"FFFFFF");
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
        cell.backgroundColor = XSYCOLOR(0x181829);
        cell.textLabel.numberOfLines = 0;
    }
    if (indexPath.section == 0) {
        cell.detailTextLabel.text = @"";
        
        cell.textLabel.text = self.listArr[indexPath.row];
        cell.detailTextLabel.text = @"";
        if (indexPath.row == 0) {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
            
            cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
            
            NSString *textStr = [NSString stringWithFormat:@"▎%@",@""];
            
            if (self.coinModel) {
                textStr = [NSString stringWithFormat:@"▎%@",self.coinModel.name];
            }
            NSMutableAttributedString *attributeStr = [self setStringWithStr:textStr RangStr:@"▎"];
            cell.textLabel.attributedText=attributeStr;
            
            cell.backgroundColor = XSYCOLOR(0x181829);
            
        }else{
            cell.backgroundColor = XSYCOLOR(0x181829);
            cell.detailTextLabel.text = @"";
            
            if (!self.coinModel) {
                cell.detailTextLabel.text = @"";
            }else
            {
                //发行时间",@"发行总量",@"流通总量",@"发行价格",@"白皮书",@"官网",@"区块查询
                if (indexPath.row == 1) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.coinModel.release_time];
                }else if (indexPath.row == 2) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.coinModel.number];
                }else if (indexPath.row == 3) {
                    NSString *text = @" ";
                    if (self.coinModel.total.floatValue >0) {
                        text = [NSString stringWithFormat:@"%@",self.coinModel.total];
                    }
                    cell.detailTextLabel.text = text;
                }else if (indexPath.row == 4) {
                    NSString *textStr = [NSString stringWithFormat:@"%@",self.coinModel.raise_price];
                    if (isEmptyString(self.coinModel.raise_price)) {
                        textStr =@"--";
                    }
                    cell.detailTextLabel.text = textStr;
                }else if (indexPath.row == 5) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.coinModel.white_paper];
                }else if (indexPath.row == 6) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.coinModel.Website];
                }else if (indexPath.row == 7) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.coinModel.coin_search];
                }
            }
            
        }
    }else{
        if (indexPath.row == 0) {
            //            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            //
            //            cell.textLabel.textColor = Color(@"111111");
            //            NSString *textStr = @"—— 简介 ——";
            cell.textLabel.text = @"";
            
            if (!self.noteLb) {
                self.noteLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
                self.noteLb.textColor = Color(@"FFFFFF");
                [cell addSubview:self.noteLb];
            }
            
            NSString *textStr = [NSString stringWithFormat:@"—— %@ ——",Localized(@"jianjie")];
            NSMutableAttributedString *attributeStr = [self setStringWithStr2:textStr RangStr:Localized(@"jianjie")];
            self.noteLb.attributedText=attributeStr;
            self.noteLb.textAlignment = NSTextAlignmentCenter;
            
            //            cell.textLabel.textColor = Color(@"111111");
            //            NSMutableAttributedString *attributeStr = [self setStringWithStr2:textStr RangStr:@"简介"];
            //            cell.textLabel.attributedText=attributeStr;
            //            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            
        }else{
            NSString *textStr = @"     ";
            //            if (self.coinModel) {
            //                textStr = [NSString stringWithFormat:@"%@",self.coinModel.content];
            //            }
            //
            cell.textLabel.text = textStr;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.numberOfLines = 0;
            cell.backgroundColor = XSYCOLOR(0x181829);

        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 44;
    }else{
        if (indexPath.row == 0) {
            return 44;
        }else{
            //            NSString *textStr = @"     ";
            //            if (self.coinModel) {
            //                textStr = [NSString stringWithFormat:@"%@",self.coinModel.content];
            //                NSString *titleText = textStr;
            //                CGSize titleSize = [titleText boundingRectWithSize:CGSizeMake(mainWidth-10*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size;
            //                return titleSize.height+30;
            //
            //            }else{
            //                return 40;
            //            }
            return 1;
        }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        
        return nil;
//        if (!self.webView) {
//            self.webView = [[UIWebView alloc]init];
//            self.webView.frame = CGRectMake(0, 0,mainWidth, 100);
//            self.webView.delegate = self;
//            self.webView.backgroundColor = XSYCOLOR(0x181829);
//            self.webView.scrollView.backgroundColor = XSYCOLOR(0x181829);
//            self.webView.scrollView.delegate = self;
//        }
//
//        if (self.webHeight) {
//            self.webView.frame = CGRectMake(0, 0,mainWidth, self.webHeight);
//        }else{
//
//            if (self.coinModel) {
//                [self.webView loadHTMLString:[NSString stringWithFormat:@"%@",self.coinModel.intro] baseURL:nil];
//                [XSTool showProgressHUDWithView:self.view];
//            }
//
//        }
//
//        return self.webView;
//
    }else{
        
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = XSYCOLOR(0x181829);
        return view;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = XSYCOLOR(0x181829);
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section == 0) {
//        return CGFLOAT_MIN;
//    }else{
//        if (self.webHeight) {
//            return self.webHeight;
//        }
//        return 40;
//    }
    return CGFLOAT_MIN;
}



// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.background='#181829'" completionHandler:nil];
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'" completionHandler:nil];
    
    [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id data, NSError * _Nullable error) {
        CGFloat height = [data floatValue];
        //ps:js可以是上面所写，也可以是document.body.scrollHeight;在WKWebView中前者offsetHeight获取自己加载的html片段，高度获取是相对准确的，但是若是加载的是原网站内容，用这个获取，会不准确，改用后者之后就可以正常显示，这个情况是我尝试了很多次方法才正常显示的
        CGRect webFrame = webView.frame;
        if (height == 0) {
            webFrame.size.height = height+0;
        } else {
            webFrame.size.height = height+FontNum(20);
        }
        webView.frame = webFrame;
        XSLog(@"%f\n%f",webView.frame.size.height,webView.frame.size.width);
        
        self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, webFrame.size.height);
        self.tableView.tableFooterView.xs_height = webFrame.size.height;
        self.tableView.tableFooterView = self.footerView;
        
        [self.tableView reloadData];
    }];
}



#pragma mark-UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [XSTool hideProgressHUDWithView:self.view];
    self.webHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollHeight"]floatValue]; //此方法获取webview的内容高度（建议使用）
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.background='#181829'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'"];
    
    [self.tableView reloadData];
    //    [self showPictureDetail];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [XSTool hideProgressHUDWithView:self.view];
    
    self.webHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollHeight"]floatValue]; //此方法获取webview的内容高度（建议使用）
    [self.tableView reloadData];
}




- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:Color(@"111111") range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:range];
    return attributeStr;
    
}
- (NSMutableAttributedString *)setStringWithStr2:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:Color(@"FFFFFF") range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:range];
    return attributeStr;
    
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr =[NSMutableArray array];
    }
    return _listArr;
}

@end

