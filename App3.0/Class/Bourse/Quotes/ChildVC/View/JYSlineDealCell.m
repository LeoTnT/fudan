//
//  JYSlineDealCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSlineDealCell.h"
#import "JYSKlineDealModel.h"
#import "XSFormatterDate.h"

@interface JYSlineDealCell ()
@property (weak, nonatomic) IBOutlet UILabel *tradTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyOrSoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;



@end
@implementation JYSlineDealCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setKlineDealCellDataWithModel:(JYSKlineDealModel *)model {
//    self.tradTimeLabel.text = model.time;
    
//    self.tradTimeLabel.text = [XSFormatterDate dateWithTimeIntervalString:model.time];
    self.tradTimeLabel.text = [XSFormatterDate timeWithStyle:XSFormatterDateStyleHMS timeFormat:nil intervalString:model.time];
    
    if ([model.direction isEqualToString:@"buy"]) {
        self.buyOrSoldLabel.text = Localized(@"buy");
        self.buyOrSoldLabel.textColor = [UserInstance ShardInstnce].roseColor;
    } else {
        self.buyOrSoldLabel.text = Localized(@"buy");
        self.buyOrSoldLabel.textColor = [UserInstance ShardInstnce].fellColor;
    }
    self.priceLabel.text = [NSString stringWithFormat:@"%.4f",[model.price doubleValue]];
    self.amountLabel.text = [NSString stringWithFormat:@"%.4f",[model.amount doubleValue]];
}

@end
