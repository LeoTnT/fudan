//
//  JYSKLineHeaderView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKLineHeaderView.h"
#import <WebKit/WebKit.h>
@interface JYSKLineHeaderView ()<WKNavigationDelegate,UIScrollViewDelegate,WKScriptMessageHandler>

@property (assign, nonatomic) NSUInteger loadCount;
@property (strong, nonatomic) UIProgressView *progressView;
@property (strong, nonatomic) WKWebView *wkWebView;

/**  */
@property (nonatomic, strong) WKWebViewConfiguration * wkConfig;

@end

@implementation JYSKLineHeaderView

- (UIProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0);
        _progressView.tintColor = mainColor;
        _progressView.trackTintColor = [UIColor whiteColor];
        [self addSubview:_progressView];
    }
    return _progressView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.backgroundColor = XSYCOLOR(0x181829);

//    self.currentUrlString = self.urlString;
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.preferences.minimumFontSize = 18;
    self.wkConfig = config;
    
    self.wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FontNum(420)) configuration:config];
    
    //            if (@available(iOS 11.0, *)) {
    //                wkWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    //            }
    
    self.wkWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.wkWebView.backgroundColor = XSYCOLOR(0x181829);
    self.wkWebView.navigationDelegate = self;
    [self insertSubview:self.wkWebView belowSubview:self.progressView];
    
    //wkWebView.sc
    [self.wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.wkWebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];

    WKUserContentController *userCC = config.userContentController;
    //JS调用OC 添加处理脚本
    //            [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"popToPersonalCenter"];
    [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"toPay"];
    [userCC addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:@"toAdvert"];
    
    
//    self.webView = [[WKWebView alloc] init];
//    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
////    self.webView.scalesPageToFit = YES;
//    self.webView.scrollView.delegate = self;
//
//    self.webView.backgroundColor = XSYCOLOR(0x181829);
//    [self addSubview:self.webView];
//    self.webView.navigationDelegate = self;
//    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
//
//    //网址
//    NSURL * theWebUrl = [NSURL URLWithString:@"http://tradefeature.abc123rt.com/index/kline/chart.html?period=1min&currency=ethcny&type=1&lang=zh"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:theWebUrl];
//    [self.webView loadRequest:request];
}

- (void)setKlineSymbolString:(NSString *)symbolString {
    NSString * urlString = [NSString stringWithFormat:@"%@index/kline/chart.html?period=1min&type=1&lang=zh&currency=%@",JYSBaseURL,symbolString];
    XSLog(@"%@",urlString);
    //网址
    NSURL * theWebUrl = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:theWebUrl];
    [self.wkWebView loadRequest:request];
}

#pragma mark - WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    XSLog(@"%@",NSStringFromSelector(_cmd));
    XSLog(@"%@",message.body);
    if ([message.name isEqualToString:@"toPay"]){
        NSArray *arr = message.body;
        XSLog(@"%@",arr);
    }
    if ([message.name isEqualToString:@"toAdvert"]){
        NSDictionary * adDataDict = message.body;
        XSLog(@"%@",adDataDict);
    }
}

-(void)paiedSuccessed {
    [self.wkWebView reload];
}

#pragma mark - wkWebView代理
// 如果不添加这个，那么wkwebview跳转不了AppStore
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if ([webView.URL.absoluteString hasPrefix:@"https://itunes.apple.com"]) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        decisionHandler(WKNavigationActionPolicyCancel);
    }else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}
-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object == self.wkWebView) {
            CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
            if (newprogress == 1) {
                self.progressView.hidden = YES;
                [self.progressView setProgress:0 animated:NO];
                
            }else {
                self.progressView.hidden = NO;
                [self.progressView setProgress:newprogress animated:YES];
            }
        }
    } else if ([keyPath isEqualToString:@"title"]){
        if (object == self.wkWebView) {
            XSLog(@"%@",self.wkWebView.title);
        }
    }
}

- (void)setWebViewBackToFront {
    [self.wkWebView goBack];
}
// 记得取消监听
- (void)dealloc {
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.wkWebView removeObserver:self forKeyPath:@"title"];
    [self.wkConfig.userContentController removeScriptMessageHandlerForName:@"toPay"];
}



//// 页面加载完成之后调用
//- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
//{
//    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.background='#181829'" completionHandler:nil];
//
//    [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id data, NSError * _Nullable error) {
//        CGFloat height = [data floatValue];
//        //ps:js可以是上面所写，也可以是document.body.scrollHeight;在WKWebView中前者offsetHeight获取自己加载的html片段，高度获取是相对准确的，但是若是加载的是原网站内容，用这个获取，会不准确，改用后者之后就可以正常显示，这个情况是我尝试了很多次方法才正常显示的
//        CGRect webFrame = webView.frame;
//        if (height == 0) {
//            webFrame.size.height = height+0;
//        } else {
//            webFrame.size.height = height+FontNum(20);
//        }
//        webView.frame = webFrame;
//        XSLog(@"%f\n%f",webView.frame.size.height,webView.frame.size.width);
//
//        if ([self.delegate respondsToSelector:@selector(headerWebDidFinisheLoadWith:)]) {
//            [self.delegate headerWebDidFinisheLoadWith:webView.frame.size.height];
//        }
//
////        [self.rentHouseTableView reloadData];
//    }];
//}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
} 

//-(void)dealloc {
//    self.webView.scrollView.delegate = nil;
//}

@end
