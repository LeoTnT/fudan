//
//  JYSKLineHeaderView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JYSKLineHeaderViewDelegate <NSObject>

- (void)headerWebDidFinisheLoadWith:(CGFloat)height;

@end

@interface JYSKLineHeaderView : UIView

///** 币种 */
//@property (nonatomic, copy) NSString * symbolString;

- (void)setKlineSymbolString:(NSString *)symbolString;

/** 代理 */
@property (nonatomic, weak) id<JYSKLineHeaderViewDelegate>delegate;

@end
