//
//  JYSKlineNativeHeaderView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSCurrencyTradingModel;

@interface JYSKlineNativeHeaderView : UIView

- (void)setDataWithCoinModel:(JYSCurrencyTradingModel *)coinModel;

/** 全屏按钮 */
@property (nonatomic, strong) UIButton * fullScreenBtn;

@end
