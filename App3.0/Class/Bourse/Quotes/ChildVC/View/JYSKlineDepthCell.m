//
//  JYSKlineDepthCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKlineDepthCell.h"

@interface JYSKlineDepthCell ()

@property (weak, nonatomic) IBOutlet UILabel *buyingLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellingLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellingCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellingPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *roseBgView;
@property (weak, nonatomic) IBOutlet UIView *fellBgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *roseViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fellViewWidth;

@end

@implementation JYSKlineDepthCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    F95453
//00BF84
    
    self.buyPriceLabel.textColor = [UserInstance ShardInstnce].roseColor;
    self.sellingPriceLabel.textColor = [UserInstance ShardInstnce].fellColor;
    
    self.roseBgView.backgroundColor = [UIColor hexFloatColor:[UserInstance ShardInstnce].roseColorString alpha:0.2];
    self.fellBgView.backgroundColor = [UIColor hexFloatColor:[UserInstance ShardInstnce].fellColorString alpha:0.2];
    
    self.roseViewWidth.constant = self.fellViewWidth.constant = 100;
    
//    self.themeColor = isBuyView?[UserInstance ShardInstnce].roseColor:[UserInstance ShardInstnce].fellColor;
//
//    self.themeColor_a = isBuyView?[UIColor hexFloatColor:[UserInstance ShardInstnce].roseColorString alpha:0.1]:[UIColor hexFloatColor:[UserInstance ShardInstnce].fellColorString alpha:0.1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDataWithAsksArray:(NSArray *)Asks Index:(NSString *)index sumAsksCount:(CGFloat)asksCount Numerators:(CGFloat)numsCount {
//- (void)setDataWithAsksArray:(NSArray *)Asks Index:(NSString *)index maxAsksCount:(CGFloat)asksCount {
    if (Asks.count) {
        self.buyCountLabel.text = [NSString stringWithFormat:@"%.4f",[Asks[1] doubleValue]];
        self.buyPriceLabel.text = [NSString stringWithFormat:@"%.4f",[Asks[0] doubleValue]];
        self.roseViewWidth.constant = mainWidth/2*(numsCount/asksCount);
    } else {
        self.buyCountLabel.text = @"- -";
        self.buyPriceLabel.text = @"- -";
        self.roseViewWidth.constant = 0;
    }
    self.buyingLabel.text = index;
}

- (void)setDataWithBidsArray:(NSArray *)Blids Index:(NSString *)index sumBidsCount:(CGFloat)bidsCount Numerators:(CGFloat)numsCount {
//- (void)setDataWithBidsArray:(NSArray *)Blids Index:(NSString *)index maxBidsCount:(CGFloat)bidsCount {
    if (Blids.count) {
        self.sellingCountLabel.text = [NSString stringWithFormat:@"%.4f",[Blids[1] doubleValue]];
        self.sellingPriceLabel.text = [NSString stringWithFormat:@"%.4f",[Blids[0] doubleValue]];
        
        self.fellViewWidth.constant = mainWidth/2*(numsCount/bidsCount);
    } else {
        self.sellingCountLabel.text = @"- -";
        self.sellingPriceLabel.text = @"- -";
        self.fellViewWidth.constant = 0;
    }
    self.sellingLabel.text = index;
}

@end
