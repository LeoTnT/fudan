//
//  JYSKlineDepthCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSKlineDepthCell : UITableViewCell

//- (void)setDataWithAsksArray:(NSArray *)Asks Index:(NSString *)index maxAsksCount:(CGFloat)asksCount;
- (void)setDataWithAsksArray:(NSArray *)Asks Index:(NSString *)index sumAsksCount:(CGFloat)asksCount Numerators:(CGFloat)numsCount;

- (void)setDataWithBidsArray:(NSArray *)Blids Index:(NSString *)index sumBidsCount:(CGFloat)bidsCount Numerators:(CGFloat)numsCount;

//- (void)setDataWithIndex:(NSString *)index;

@end
