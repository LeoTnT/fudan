//
//  JYSlineDealCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSKlineDealModel;
@interface JYSlineDealCell : UITableViewCell

- (void)setKlineDealCellDataWithModel:(JYSKlineDealModel *)model;

@end
