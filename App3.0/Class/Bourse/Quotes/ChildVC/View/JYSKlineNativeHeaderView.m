//
//  JYSKlineNativeHeaderView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/29.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSKlineNativeHeaderView.h"
#import "Y_StockChartView.h"
#import "Y_KLineGroupModel.h"
#import "UIColor+Y_StockChart.h"
#import "SRWebSocketTool.h"
#import "JYSCurrencyTradingModel.h"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)

@interface JYSKlineNativeHeaderView ()<Y_StockChartViewDataSource>

@property (nonatomic, strong) Y_StockChartView *stockChartView;
@property (nonatomic, strong) Y_KLineGroupModel *groupModel;
@property (nonatomic, copy) NSMutableDictionary <NSString*, Y_KLineGroupModel*> *modelsDict;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, copy) NSString *type;

/** 币种 */
@property (nonatomic, strong) JYSCurrencyTradingModel * coinModel;

/** 当前时间 */
@property (nonatomic, strong) NSNumber * theCurrentTime;
/** 之前时间 */
@property (nonatomic, strong) NSNumber * theBeforTime;

@end

@implementation JYSKlineNativeHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI {
    self.currentIndex = -1;
    
    //开启定时器
    //    [self.loadDataTimer setFireDate:[NSDate distantPast]];
    
    self.backgroundColor = XSYCOLOR(0x181829);
    self.stockChartView.backgroundColor = [UIColor backgroundColor];
    
    self.fullScreenBtn = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:nil action:nil];
    [self.fullScreenBtn setImage:[UIImage imageNamed:@"full_Screen"] forState:UIControlStateNormal];
    [self addSubview:self.fullScreenBtn];
    
    [self.fullScreenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self).offset(-20);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
}

//- (void)setDataWithCoinName:(NSString *)coinName {
//    self.coinName = coinName;
//}

- (void)setDataWithCoinModel:(JYSCurrencyTradingModel *)coinModel {
    self.coinModel = coinModel;
    
    [self.modelsDict removeAllObjects];
    _stockChartView.dataSource = self;
}

- (NSMutableDictionary<NSString *, Y_KLineGroupModel *> *)modelsDict {
    if (_modelsDict == nil) {
        _modelsDict = [[NSMutableDictionary alloc] init];
    }
    return _modelsDict;
}

-(id)stockDatasWithIndex:(NSInteger)index {
    NSString * type;
    switch (index) {
        case 0:
        {
            type = @"1min";
        }
            break;
        case 1:
        {
            type = @"1min";
        }
            break;
        case 2:
        {
            type = @"1min";
        }
            break;
        case 3:
        {
            type = @"5min";
        }
            break;
        case 4:
        {
            type = @"30min";
        }
            break;
        case 5:
        {
            type = @"60min";
        }
            break;
        case 6:
        {
            type = @"1day";
        }
            break;
        case 7:
        {
            type = @"1week";
        }
            break;
            
        default:
            break;
    }
    
    self.currentIndex = index;
    self.type = type;
    if (![self.modelsDict objectForKey:type]) {
        [self reloadData];
    } else {
        return [self.modelsDict objectForKey:type].models;
    }
    
    return nil;
}

- (void)reloadData
{
    //    [XSTool showProgressHUDWithView:self];
    XSLog(@"%@",self.type);
    if (self.type.length) {
    } else {
        return;
    }
    
    NSNumber * currentTime = [self getNowTimeTimestamp];
    NSNumber * beforeTime = [self getBeforeTimeTimestamp:1];
    
    XSLog(@"%@---%@",currentTime,beforeTime);
    
    if (self.coinModel.symbol) {
        if (self.theCurrentTime && self.theBeforTime) {
            [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"unsub" period:self.type fromTime:self.theBeforTime toTime:self.theCurrentTime ID:self.coinModel.ID];
        }
        
        //请求K线图数据
        [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"req" period:self.type fromTime:beforeTime toTime:currentTime ID:self.coinModel.ID];
        [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"sub" period:self.type fromTime:beforeTime toTime:currentTime ID:self.coinModel.ID];
        
        self.theCurrentTime = currentTime;
        self.theBeforTime = beforeTime;
    }
    
    __weak typeof(self) weakSelf = self;
    [[SRWebSocketTool sharedSRWebSocketTool].webSocketMessage subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSString *key,NSString *message,NSDictionary *dic) = x;
        
        XSLog(@"key =%@   =%@  =%@ ",key,message,dic);
        NSArray * dictAllKeys = [dic allKeys];
        if ([dictAllKeys containsObject:@"rep"] ||[dictAllKeys containsObject:@"ch"]) {
            NSString * typeString;
            if ([dictAllKeys containsObject:@"rep"]) {
                typeString = dic[@"rep"];
            } else {
                typeString = dic[@"ch"];
            }
            if (weakSelf.type.length == 0) {
                return ;
            }
    
            if ([typeString hasSuffix:weakSelf.type]) {
                if ([dictAllKeys containsObject:@"tick"]) {
                    NSArray * datasArr = dic[@"tick"];
                    if ([datasArr isKindOfClass:[NSArray class]]) {
                        Y_KLineGroupModel *groupModel = [Y_KLineGroupModel objectWithArray:datasArr];
                        //            XSLog(@"%@",groupModel);
                        weakSelf.groupModel = groupModel;
                        [weakSelf.modelsDict setObject:groupModel forKey:weakSelf.type];
                        [weakSelf.stockChartView reloadData];
                        if (datasArr.count == 0) {
                            [weakSelf.stockChartView removeChildView];
                        }
                    }
                }
            }
        }
    }];
}

- (NSNumber *)getNowTimeTimestamp{
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
  
    NSNumber * currentTime = @([timeString integerValue]);
    
    return currentTime;
    
}

- (NSNumber *)getBeforeTimeTimestamp:(NSInteger)days{
    
    NSDate * date = [NSDate date];//当前时间
    NSDate *beforData = [NSDate dateWithTimeInterval:-24*60*60*days sinceDate:date];//之前时间
    
    NSTimeInterval a=[beforData timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    NSNumber * currentTime = @([timeString integerValue]);
    
    return currentTime;
    
}


-(void)refreshDataWithID:(NSString *)idStr {
    [self.modelsDict removeAllObjects];
//    self.idString = idStr;
    _stockChartView.dataSource = self;
    //    [self.stockChartView reloadData];
}

- (Y_StockChartView *)stockChartView
{
    if(!_stockChartView) {
        _stockChartView = [Y_StockChartView new];
        _stockChartView.currentScreenOrientation = ScreenOrientationVertical;
        _stockChartView.itemModels = @[
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"指标") type:Y_StockChartcenterViewTypeOther],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fens") type:Y_StockChartcenterViewTypeTimeLine],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen1") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen5") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen30") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen60") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"rixian") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"zhouxian") type:Y_StockChartcenterViewTypeKline],
                                       
                                       ];
        _stockChartView.dataSource = self;
        [self addSubview:_stockChartView];
        [_stockChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return _stockChartView;
}

- (void)runLoopRefreshData {
    [self reloadData];
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
    
    [[SRWebSocketTool sharedSRWebSocketTool] sendKlineRequestMessage:self.coinModel.symbol key:@"unsub" period:self.type fromTime:self.theBeforTime toTime:self.theCurrentTime ID:self.coinModel.ID];
}
@end
