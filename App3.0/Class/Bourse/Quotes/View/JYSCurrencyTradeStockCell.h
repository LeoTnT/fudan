//
//  JYSCurrencyTradeStockCell.h
//  App3.0
//
//  Created by xinshang on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradingStockCellView.h"

@interface JYSCurrencyTradeStockCell : UITableViewCell
@property (nonatomic ,strong) TradingStockCellView *stockView;
@property (nonatomic ,strong) UIButton *stockButton;

@end
