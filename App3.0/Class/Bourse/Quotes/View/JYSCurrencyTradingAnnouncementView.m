//
//  JYSCurrencyTradingAnnouncementView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradingAnnouncementView.h"
#import "JYSCurrencyAnnouncementCell.h"
#import "JYSCurrencyTradingModel.h"

@interface JYSCurrencyTradingAnnouncementView ()<SDCycleScrollViewDelegate>

@property (strong, nonatomic) SDCycleScrollView * announcementView;
/** 数据数组 */
@property (nonatomic, copy) NSArray * dataArray;
/** cellIndex */
@property (nonatomic, assign) NSUInteger cellIndex;

@end

@implementation JYSCurrencyTradingAnnouncementView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.35];
        self.cellIndex = 0;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI {
    UIImageView * bugleImageV = [XSUITool creatImageViewWithFrame:CGRectMake(0, 0, FontNum(16), FontNum(14)) backgroundColor:nil image:[UIImage imageNamed:@"jys_announcement"]];
    [self addSubview:bugleImageV];
    [bugleImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(9);
        make.centerY.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(FontNum(16), FontNum(14)));
    }];
    
    UIButton * closeButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(closeButtonClick) titleColor:nil titleFont:14 backgroundColor:nil image:[UIImage imageNamed:@"jys_announcement_close"] backgroundImage:nil title:nil];
    [self addSubview:closeButton];
    [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-5);
        make.centerY.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(FontNum(30), FontNum(30)));
    }];
    
//    self.announcementView.titleLabelBackgroundColor = [UIColor clearColor];
//    self.announcementView.titleLabelTextColor = JYSMainTextColor;
//
//    self.announcementView.titleLabelTextFont = [UIFont systemFontOfSize:FontNum(13)];
//    self.announcementView.titleLabelTextAlignment = NSTextAlignmentLeft;
//    self.announcementView.onlyDisplayText = YES;
//    self.announcementView.scrollDirection = UICollectionViewScrollDirectionVertical;
//    self.announcementView.delegate =self;
//    self.announcementView.backgroundColor = [UIColor clearColor];
    
    self.announcementView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
    self.announcementView.placeholderImage = [UIImage imageNamed:@""];
    self.announcementView.delegate = self;
    self.announcementView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    self.announcementView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.announcementView.autoScrollTimeInterval = 2.0;
    self.announcementView.backgroundColor = [UIColor clearColor];
    [self.announcementView disableScrollGesture];
    self.announcementView.localizationImageNamesGroup = @[@"homePage_placeholder",@"homePage_placeholder"];
    
    [self addSubview:self.announcementView];
    
    [self.announcementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bugleImageV.mas_right).offset(7);
        make.right.mas_equalTo(closeButton.mas_left).offset(-10);
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(FontNum(30));
    }];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.announcementView.titlesGroup = @[Localized(@"您推荐的会员"),Localized(@"交易产生手续费")];
    });
}

- (void)closeButtonClick {
    [self removeFromSuperview];
}

- (void)setAnnouncementDataWithModel:(NSArray *)announcementDatas {
    self.dataArray = announcementDatas;
    
    [self.announcementView reloadInputViews];
}

- (Class)customCollectionViewCellClassForCycleScrollView:(SDCycleScrollView *)view
{
    if (view != self.announcementView) {
        return nil;
    }
    return [JYSCurrencyAnnouncementCell class];
}

- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view
{
    JYSCurrencyAnnouncementCell *myCell = (JYSCurrencyAnnouncementCell *)cell;
    if (self.dataArray.count) {
        JYSCurrencyAnnouncementModel * model = self.dataArray[self.cellIndex];
        if (self.cellIndex == self.dataArray.count-1) {
            self.cellIndex = 0;
        } else {
            self.cellIndex++;
        }
        [myCell setDataWithBannerModel:model];
    }
}

@end
