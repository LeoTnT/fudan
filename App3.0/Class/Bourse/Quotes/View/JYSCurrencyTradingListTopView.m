//
//  JYSCurrencyTradingListTopView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradingListTopView.h"

@interface JYSCurrencyTradingListTopView ()
/** 成交量 */
@property (nonatomic, strong) UIButton * volumeBtn;
/** 最新价 */
@property (nonatomic, strong) UIButton * latestPriceBtn;
/** 日涨跌 */
@property (nonatomic, strong) UIButton * changeDayBtn;

/** selectedBtn */
@property (nonatomic, strong) UIButton * selectedBtn;

/** 是否是升序 */
@property (nonatomic, assign) BOOL isUp;

@end

@implementation JYSCurrencyTradingListTopView

static NSUInteger const menuBtnTag = 500;

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XSYCOLOR(0xE7EAF1);
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI {
    CGFloat space = 5.f;
    self.volumeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [XSUITool setButton:self.volumeBtn titleColor:XSYCOLOR(0x787F89 ) titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_default"] backgroundImage:nil forState:UIControlStateNormal title:Localized(@"sort_chengjiao")];
    [XSUITool setButton:self.volumeBtn titleColor:JYSMainSelelctColor titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_Down"] backgroundImage:nil forState:UIControlStateSelected title:Localized(@"sort_chengjiao")];
    CGFloat labelWidth = self.volumeBtn.titleLabel.intrinsicContentSize.width; //注意不能直接使用titleLabel.frame.size.width,原因为有时候获取到0值
    CGFloat imageWidth = self.volumeBtn.imageView.frame.size.width;
    
    [self.volumeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space)];
    [self.volumeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0,  imageWidth+space)];
    [self addSubview:self.volumeBtn];
    [self.volumeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
    
    self.latestPriceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [XSUITool setButton:self.latestPriceBtn titleColor:XSYCOLOR(0x787F89 ) titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_default"] backgroundImage:nil forState:UIControlStateNormal title:Localized(@"sort_newprice")];
    [XSUITool setButton:self.latestPriceBtn titleColor:JYSMainSelelctColor titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_Down"] backgroundImage:nil forState:UIControlStateSelected title:Localized(@"成交额")];
    
    [self.latestPriceBtn setImageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space)];
    [self.latestPriceBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0,  imageWidth+space)];
    [self addSubview:self.latestPriceBtn];
    [self.latestPriceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.mas_equalTo(self.volumeBtn.mas_right);
        make.width.mas_equalTo(self.volumeBtn);
        make.centerY.mas_equalTo(self);
    }];
    
    self.changeDayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [XSUITool setButton:self.changeDayBtn titleColor:XSYCOLOR(0x787F89 ) titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_default"] backgroundImage:nil forState:UIControlStateNormal title:Localized(@"sort_day_price")];
    [XSUITool setButton:self.changeDayBtn titleColor:JYSMainSelelctColor titleFont:FontNum(13) backgroundColor:nil image:[UIImage imageNamed:@"tradingHall_sorting_Down"] backgroundImage:nil forState:UIControlStateSelected title:Localized(@"sort_day_price")];
    
    [self.changeDayBtn setImageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space)];
    [self.changeDayBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0,  imageWidth+space)];
    [self addSubview:self.changeDayBtn];
    [self.changeDayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self);
        make.left.mas_equalTo(self.latestPriceBtn.mas_right);
        make.width.mas_equalTo(self.volumeBtn);
        make.centerY.mas_equalTo(self);
    }];
    
    self.volumeBtn.tag = menuBtnTag;
    self.latestPriceBtn.tag = menuBtnTag+1;
    self.changeDayBtn.tag = menuBtnTag+2;
    
    [self.volumeBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.latestPriceBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.changeDayBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //    self.finalPriceBtn.selected = YES;
    //    self.selectedBtn = self.finalPriceBtn;
    //    self.isUp = NO;
}

- (void)menuBtnClick:(UIButton *)button {
    if (button == self.selectedBtn) {
        if (self.isUp) {
            //降序
            [button setImage:[UIImage imageNamed:@"tradingHall_sorting_Down"] forState:UIControlStateSelected];
            self.isUp = NO;
        } else {
            //升序
            [button setImage:[UIImage imageNamed:@"tradingHall_sorting_Up"] forState:UIControlStateSelected];
            self.isUp = YES;
        }
    } else {
        //降序
        [self.selectedBtn setImage:[UIImage imageNamed:@"tradingHall_sorting_Down"] forState:UIControlStateSelected];
        self.selectedBtn.selected = NO;
        button.selected = YES;
        self.selectedBtn = button;
        self.isUp = NO;
    }
    if ([self.delegate respondsToSelector:@selector(menuBtnSelectIndex:upOrDown:)]) {
        [self.delegate menuBtnSelectIndex:button.tag-menuBtnTag upOrDown:self.isUp];
    }
}


@end
