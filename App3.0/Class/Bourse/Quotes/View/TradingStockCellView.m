//
//  TradingStockCellView.m
//  App3.0
//
//  Created by xinshang on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "TradingStockCellView.h"
#import "TradingStockCellView.h"
#import "Y_StockChartView.h"
//#import "NetWorking.h"
#import "Y_KLineGroupModel.h"
#import "UIColor+Y_StockChart.h"
//#import "StockChartViewController.h"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)

@interface TradingStockCellView ()<Y_StockChartViewDataSource>

@property (nonatomic, strong) Y_StockChartView *stockChartView;
@property (nonatomic, strong) Y_KLineGroupModel *groupModel;
@property (nonatomic, copy) NSMutableDictionary <NSString*, Y_KLineGroupModel*> *modelsDict;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, copy) NSString *type;

/** id */
@property (nonatomic, copy) NSString * idString;

@end

@implementation TradingStockCellView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI {
    self.currentIndex = 5;
    
    self.stockChartView.backgroundColor = [UIColor backgroundColor];
    self.backgroundColor = [UIColor whiteColor];
}

- (NSMutableDictionary<NSString *, Y_KLineGroupModel *> *)modelsDict {
    if (_modelsDict == nil) {
        _modelsDict = [[NSMutableDictionary alloc] init];
    }
    return _modelsDict;
}

-(id)stockDatasWithIndex:(NSInteger)index {
    NSString * type;
    switch (index) {
        case 0:
        {
            type = @"1min";
        }
            break;
        case 1:
        {
            type = @"1min";
        }
            break;
        case 2:
        {
            type = @"1min";
        }
            break;
        case 3:
        {
            type = @"5min";
        }
            break;
        case 4:
        {
            type = @"30min";
        }
            break;
        case 5:
        {
            type = @"60min";
        }
            break;
        case 6:
        {
            type = @"1day";
        }
            break;
        case 7:
        {
            type = @"1week";
        }
            break;
        default:
            break;
    }
    
    self.currentIndex = index;
    self.type = @"60min";
    if (![self.modelsDict objectForKey:self.type]) {
        [self reloadData];
    } else {
        return [self.modelsDict objectForKey:type].models;
    }
    
    return nil;
}

- (void)reloadData
{
    if (!self.idString) {
        return;
    }
    @weakify(self);
    //        [XSTool hideProgressHUDWithView:self];
    [XSTool showProgressHUDWithView:self];
//    [HTTPManager Y_coinInfoWithName:self.idString
//                               type:self.type
//                            success:^(NSDictionary *dic, resultObject *state)
//     {
//         @strongify(self);
//         [XSTool hideProgressHUDWithView:self];
//         //             XSLog(@"%@",dic);
//         if (state.status) {
//             NSArray * dataArr = dic[@"data"];
//             
//             Y_KLineGroupModel *groupModel = [Y_KLineGroupModel objectWithArray: dic[@"data"]];
//             self.groupModel = groupModel;
//             if (groupModel) {
//                 [self.modelsDict setObject:groupModel forKey:self.type];
//             }
//             //                 XSLog(@"%@",groupModel);
//             [self.stockChartView reloadData];
//             
//             if (dataArr.count == 0) {
//                 [self.stockChartView removeChildView];
//             }
//         } else {
//             //                 [XSTool showToastWithView:self Text:state.info];
//             [self.stockChartView removeChildView];
//         }
//     } fail:^(NSError *error) {
//         @strongify(self);
//         [XSTool hideProgressHUDWithView:self];
//         [XSTool showToastWithView:self Text:TOAST_REQUEST_FAIL];
//     }];
    
    
   
}

-(void)refreshDataWithID:(NSString *)idStr {
    [self.modelsDict removeAllObjects];
    self.idString = idStr;
    _stockChartView.dataSource = self;
    //    [self.stockChartView reloadData];
}

- (Y_StockChartView *)stockChartView
{
    if(!_stockChartView) {
        _stockChartView = [Y_StockChartView new];
        _stockChartView.currentScreenOrientation = ScreenOrientationVertical;
        _stockChartView.itemModels = @[
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"指标") type:Y_StockChartcenterViewTypeOther],
                                       //                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fens") type:Y_StockChartcenterViewTypeTimeLine],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fens") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen1") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen5") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen30") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"fen60") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"rixian") type:Y_StockChartcenterViewTypeKline],
                                       [Y_StockChartViewItemModel itemModelWithTitle:Localized(@"zhouxian") type:Y_StockChartcenterViewTypeKline],
                                       
                                       ];
        //        _stockChartView.backgroundColor = [UIColor orangeColor];
        _stockChartView.dataSource = self;
        [self addSubview:_stockChartView];
        [_stockChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self);
        }];
        [_stockChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
            //            if (IS_IPHONE_X) {
            //                make.left.top.right.mas_equalTo(self);
            //                //                make.top.mas_equalTo(self).offset(94);
            ////                make.height.mas_equalTo(make.width).multipliedBy(SCREEN_WIDTH/SCREEN_HEIGHT+0.1);
            //                make.bottom.mas_equalTo(self);
            //            } else {
            //                make.edges.equalTo(self);
            //            }
        }];
    }
    return _stockChartView;
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}


@end


