//
//  JYSCurrencyTradingListTopView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JYSCurrencyTradingListTopViewDelegate <NSObject>

- (void)menuBtnSelectIndex:(NSUInteger)index upOrDown:(BOOL)isUpOrDown;

@end

@interface JYSCurrencyTradingListTopView : UIView

/** delegate */
@property (nonatomic, weak) id<JYSCurrencyTradingListTopViewDelegate>delegate;

@end
