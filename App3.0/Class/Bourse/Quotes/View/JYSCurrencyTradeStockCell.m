//
//  JYSCurrencyTradeStockCell.m
//  App3.0
//
//  Created by xinshang on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradeStockCell.h"

@interface JYSCurrencyTradeStockCell ()


@end

@implementation JYSCurrencyTradeStockCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    [UserInstance ShardInstnce].isWhite = YES;
    if (_stockView == nil) {
        _stockView = [[TradingStockCellView alloc] init];
        _stockView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(500));
        
        [self addSubview:_stockView];
        self.stockButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGFloat cellHeight= 198;
        if (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0) {
            cellHeight= 160;
        }
        
        self.stockButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, cellHeight);
        [self addSubview:self.stockButton];
        
    }
    
}

@end

