//
//  JYSCurrencyAnnouncementCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSCurrencyAnnouncementModel;
@interface JYSCurrencyAnnouncementCell : UICollectionViewCell

- (void)setDataWithBannerModel:(JYSCurrencyAnnouncementModel *)model;

@end
