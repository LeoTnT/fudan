//
//  JYSCurrencementAnnouncementCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyAnnouncementCell.h"
#import "JYSCurrencyTradingModel.h"

@interface JYSCurrencyAnnouncementCell ()

@property (strong, nonatomic) UILabel *titleLabel;

@end

@implementation JYSCurrencyAnnouncementCell

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.contentView.backgroundColor = [UIColor whiteColor];
//    
    self.titleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"1234567890"];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.centerY.mas_equalTo(self.contentView);
    }];
}

- (void)setDataWithBannerModel:(JYSCurrencyAnnouncementModel *)model {
    
    NSMutableAttributedString *att1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@",Localized(@"您推荐的会员"),model.username,model.remark]];
    [att1 addAttributes:@{NSForegroundColorAttributeName:XSYCOLOR(0xF95453)} range:NSMakeRange(6, model.username.length)];
    
    self.titleLabel.attributedText = att1;
}

@end
