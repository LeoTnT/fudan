//
//  JYSCurrencyTradingAnnouncementView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSCurrencyTradingAnnouncementView : UIView

- (void)setAnnouncementDataWithModel:(NSArray *)announcementDatas;

@end
