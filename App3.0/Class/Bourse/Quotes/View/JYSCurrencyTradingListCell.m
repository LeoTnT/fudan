//
//  JYSCurrencyTradingListCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSCurrencyTradingListCell.h"
#import "JYSCurrencyTradingModel.h"

@interface JYSCurrencyTradingListCell ()

/** title */
@property (nonatomic, strong) UILabel * nameLabel;
/** title后面的小标题 */
@property (nonatomic, strong) UILabel * subtitleLabel;
/** 成交量 */
@property (nonatomic, strong) UILabel * volumeLabel;
/** 涨跌价格 */
@property (nonatomic, strong) UIButton * riseAndFallPriceButton;
/** 最新价 */
@property (nonatomic, strong) UILabel * priceLabel;
/** 日涨跌 */
@property (nonatomic, strong) UIView * percentageBgView;
/** 日涨跌 */
@property (nonatomic, strong) UILabel * percentageLabel;

@end

@implementation JYSCurrencyTradingListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    self.nameLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"BTC"];
    self.nameLabel.font = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(15);
        make.baseline.mas_equalTo(self.contentView.mas_centerY).multipliedBy(0.9);
    }];
    
    self.subtitleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"/cc"];
    [self.contentView addSubview:self.subtitleLabel];
    [self.subtitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_right).offset(2);
        make.baseline.mas_equalTo(self.nameLabel);
    }];
    
    self.volumeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentLeft title: [NSString stringWithFormat:@"%@ --",Localized(@"sort_chengjiao")]];
    [self.contentView addSubview:self.volumeLabel];
    [self.volumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.contentView.mas_centerY).multipliedBy(1.1);
    }];
    
    self.riseAndFallPriceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.riseAndFallPriceButton.userInteractionEnabled = NO;
    
    NSString * arrow_up_string;
    NSString * arrow_down_string;
    if ([[UserInstance ShardInstnce].roseColorString isEqualToString:JYSRedString]) {
        arrow_up_string = @"jys_trading_rise";
        arrow_down_string = @"jys_trading_drop";
    } else {
        arrow_up_string = @"jys_trading_rise_foreign";
        arrow_down_string = @"jys_trading_drop_foreign";
    }
    
    // homePage_ arrow_down
    [XSUITool setButton:self.riseAndFallPriceButton titleColor:[UserInstance ShardInstnce].fellColor titleFont:18 backgroundColor:nil image:[UIImage imageNamed:arrow_down_string] backgroundImage:nil forState:UIControlStateNormal title:@"97.60"];
    //    homePage_ arrow_up
    [XSUITool setButton:self.riseAndFallPriceButton titleColor:[UserInstance ShardInstnce].roseColor titleFont:18 backgroundColor:nil image:[UIImage imageNamed:arrow_up_string] backgroundImage:nil forState:UIControlStateSelected title:@"860.00"];
    [XSUITool setButton:self.riseAndFallPriceButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsZero titleEdgeInsets:UIEdgeInsetsZero];
    [self.contentView addSubview:self.riseAndFallPriceButton];
    [self.riseAndFallPriceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_centerX).multipliedBy(0.9);
        make.centerY.mas_equalTo(self.nameLabel);
    }];
    
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"￥1949.88"];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.riseAndFallPriceButton);
        make.top.mas_equalTo(self.volumeLabel);
    }];
    
    self.percentageBgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UserInstance ShardInstnce].roseColor];
    [XSUITool setView:self.percentageBgView cornerRadius:FontNum(2) borderWidth:CGFLOAT_MIN borderColor:nil];
    [self.contentView addSubview:self.percentageBgView];
    
    [self.percentageBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(FontNum(66), FontNum(27)));
    }];
    
    self.percentageLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:[UIColor whiteColor] titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"+9.18%"];
    self.percentageLabel.adjustsFontSizeToFitWidth = YES;
    self.percentageLabel.minimumScaleFactor = 0.8;
    [self.contentView addSubview:self.percentageLabel];
    [self.percentageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.percentageBgView).offset(2);
        make.right.mas_equalTo(self.percentageBgView).offset(-2);
        make.centerY.mas_equalTo(self.percentageBgView);
    }];
}

- (void)setDataWithModel:(JYSCurrencyTradingModel *)model rateStr:(NSString *)rateStr{
//    NSString * changeString = [NSString stringWithFormat:@"%@",model.change];
    NSString * changeString = [NSString stringWithFormat:@"%@",model.close];
    if (changeString.floatValue <0 && changeString.length>1) {//截取负号之后的数
        changeString = [changeString substringFromIndex:1];
    }
    //    XSLog(@"%@",changeString);
    
    if (model.gcoin) {
        self.nameLabel.text = model.gcoin;
        self.subtitleLabel.text = [NSString stringWithFormat:@"/%@",model.ucoin];
        self.volumeLabel.text = [NSString stringWithFormat:@"%@ %@",Localized(@"sort_chengjiao"),model.amount];
        
        //model.close 当前对应的币种价格
        //rateStr 当前币种对应的USDT价格
        //[AppConfigManager ShardInstnce].rateConfig.rate USDT对应的CNY价格
        
        if ([AppConfigManager ShardInstnce].rateConfig) {
            self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",[AppConfigManager ShardInstnce].rateConfig.symbol,[model.close floatValue]*rateStr.doubleValue*[AppConfigManager ShardInstnce].rateConfig.rate.doubleValue];
            
            if (!isEmptyString(self.titleStr)) {
                if ([self.titleStr isEqualToString:[AppConfigManager ShardInstnce].symbolRateModel.default_coin]) {
                    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",[AppConfigManager ShardInstnce].rateConfig.symbol,[model.close floatValue]];
                }
            }
        }else{
            self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",[model.close floatValue]];
        }
        
        
        self.riseAndFallPriceButton.selected = (model.change.floatValue >= 0);
        [self.riseAndFallPriceButton setTitle:changeString forState:UIControlStateNormal];
        [self.riseAndFallPriceButton setTitle:changeString forState:UIControlStateSelected];
        
        if ([model.chg floatValue] > 0) {
            [self.percentageBgView setBackgroundColor:[UserInstance ShardInstnce].roseColor];
            self.percentageLabel.text = [NSString stringWithFormat:@"+%.2f%%",[model.chg floatValue]];
        } else if ([model.chg floatValue] < 0) {
            [self.percentageBgView setBackgroundColor:[UserInstance ShardInstnce].fellColor];
            self.percentageLabel.text = [NSString stringWithFormat:@"%.2f%%",[model.chg floatValue]];
        } else {
            [self.percentageBgView setBackgroundColor:JYSGrayButtonColor];
            self.percentageLabel.text = [NSString stringWithFormat:@"+%.2f%%",[model.chg floatValue]];
        }
    } else {
        self.nameLabel.text = model.bsecurrency;
        self.subtitleLabel.text = [NSString stringWithFormat:@"/%@",model.quotecurrency];
        self.volumeLabel.text =[NSString stringWithFormat:@"%@ --",Localized(@"sort_chengjiao")] ;
        
        self.priceLabel.text = @"¥--";
        self.riseAndFallPriceButton.selected = (model.change.floatValue >= 0);
        [self.riseAndFallPriceButton setTitle:@"--" forState:UIControlStateNormal];
        [self.riseAndFallPriceButton setTitle:@"--" forState:UIControlStateSelected];
        self.percentageLabel.text = [NSString stringWithFormat:@"%@%%",@"--"];
        if ([model.chg floatValue] >= 0) {
            [self.percentageBgView setBackgroundColor:[UserInstance ShardInstnce].roseColor];
        } else {
            [self.percentageBgView setBackgroundColor:[UserInstance ShardInstnce].fellColor];
        }
    }
    
}

@end
