//
//  JYSCurrencyTradingListCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSCurrencyTradingModel;
@interface JYSCurrencyTradingListCell : UITableViewCell
@property (strong, nonatomic) NSString *titleStr;

- (void)setDataWithModel:(JYSCurrencyTradingModel *)model rateStr:(NSString *)rateStr;

@end
