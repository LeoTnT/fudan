//
//  JYSAssetManagementCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYSAssetManagementModel;

typedef void(^assetChargeMoneyBlock)(void);
typedef void(^assetMentionMoneyBlock)(void);
typedef void(^assetRecordBlock)(void);

@interface JYSAssetManagementCell : UITableViewCell

- (void)setAssetDataWithModel:(JYSAssetManagementModel *)model;

/** 充币block */
@property (nonatomic, copy) assetChargeMoneyBlock chargeMoneyBlock;

/** 提币 */
@property (nonatomic, copy) assetMentionMoneyBlock mentionMoneyBlock;

/** 记录 */
@property (nonatomic, copy) assetRecordBlock recordBlock;

@end
