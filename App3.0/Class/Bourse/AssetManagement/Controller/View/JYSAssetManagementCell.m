//
//  JYSAssetManagementCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAssetManagementCell.h"
#import "JYSAssetManagementModel.h"

@interface JYSAssetManagementCell ()


/** 小logo */
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
/** BTC */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
/** 估值 */
@property (weak, nonatomic) IBOutlet UILabel *appraisementLabel;
/** 可用 */
@property (weak, nonatomic) IBOutlet UILabel *canUserLabel;
/** 冻结 */
@property (weak, nonatomic) IBOutlet UILabel *lockLabel;



@end

@implementation JYSAssetManagementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setAssetDataWithModel:(JYSAssetManagementModel *)model {
    [self.logoImageView getImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.titleLabel.text = model.symbol;
    self.appraisementLabel.text = [NSString stringWithFormat:@"≈ %@CNY",model.close];
    self.canUserLabel.text = [NSString stringWithFormat:@"%@",model.number];
    self.lockLabel.text = model.locked;
}

- (IBAction)chargeMoneyButtonClick {
    if (self.chargeMoneyBlock) {
        self.chargeMoneyBlock();
    }
}
- (IBAction)mentionMoneyButtonClick {
    if (self.mentionMoneyBlock) {
        self.mentionMoneyBlock();
    }
}
- (IBAction)recordButtonClick {
    if (self.recordBlock) {
        self.recordBlock();
    }
}


@end
