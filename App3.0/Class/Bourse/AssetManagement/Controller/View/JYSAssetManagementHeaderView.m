//
//  JYSAssetManagementHeaderView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAssetManagementHeaderView.h"

@interface JYSAssetManagementHeaderView ()

/** 估值 */
@property (weak, nonatomic) IBOutlet UILabel *valuationLabel;
/** 保证金 */
@property (weak, nonatomic) IBOutlet UILabel *margin;
/** 隐藏0资产币种 */
@property (weak, nonatomic) IBOutlet UILabel *hiddenLabel;

@end

@implementation JYSAssetManagementHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
        [self setUpUI];
    }
    return self;
}

- (void)setValuationData:(NSString *)valuation {
    self.valuationLabel.text = valuation;
    self.margin.text = @"0";
}

- (void)setUpUI {
    self.hiddenLabel.userInteractionEnabled = YES;
    @weakify(self);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        if ([self.hiddenLabel.text isEqualToString:Localized(@"隐藏0资产币种")]) {
            self.hiddenLabel.text = Localized(@"显示0资产币种");
        } else {
            self.hiddenLabel.text = Localized(@"隐藏0资产币种");
        }
        if (self.hideAction) {
            self.hideAction();
        }
    }];
    [self.hiddenLabel addGestureRecognizer:tap];
}

@end
