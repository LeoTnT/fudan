//
//  JYSMentionMoneyHistoricalRecordVC.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"
@class JYSAssetManagementModel;
@interface JYSMentionMoneyHistoricalRecordVC : XSBaseTableViewController

/** 货币简称（非必须）|'eth' */
@property (copy, nonatomic) NSString *currency;

//- (void)setHistoricalRecordWithModel:(JYSAssetManagementModel *)model;

@end
