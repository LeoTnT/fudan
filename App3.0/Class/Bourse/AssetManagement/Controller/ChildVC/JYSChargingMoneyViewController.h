//
//  JYSChargingMoneyViewController.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
@class JYSAssetManagementModel;
@interface JYSChargingMoneyViewController : XSBaseViewController

- (void)setChargingMoneyWithModel:(JYSAssetManagementModel *)model;

@end
