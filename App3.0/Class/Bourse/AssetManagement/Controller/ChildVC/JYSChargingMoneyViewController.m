//
//  JYSChargingMoneyViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSChargingMoneyViewController.h"
#import "JYSAssetManagementModel.h"
#import "JYSMentionMoneyHistoricalRecordVC.h"

@interface JYSChargingMoneyViewController ()

/** model */
@property (nonatomic, strong) JYSAssetManagementModel * model;

/** 充币详情 model */
@property (nonatomic, strong) JYSChargingModel *chargingModel;

/** tip */
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong)  UIButton * saveQRButton;
@end

@implementation JYSChargingMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = Localized(@"charge_money");
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:JYSMainSelelctColor titleFont:[UIFont systemFontOfSize:FontNum(15) weight:UIFontWeightSemibold] target:self action:@selector(rightBarButtonItemClicked) titleString:Localized(@"history_record")];
    
//    [self setUpUI];
    
    [self requestData];
}

- (void)rightBarButtonItemClicked {
    JYSMentionMoneyHistoricalRecordVC * historicalRecordVC = [[JYSMentionMoneyHistoricalRecordVC alloc] init];
    historicalRecordVC.currency = self.model.symbol;
    [self.navigationController pushViewController:historicalRecordVC animated:YES];
}

//static CGFloat const space = 5.f;
- (void)setUpUI {
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *tScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    [self.view addSubview:tScrollView];
    
//    UIButton * coinButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(coinButtonClick) titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil image:[UIImage imageNamed:@"sort_down"] backgroundImage:nil title:@"BTC"];
    UIButton * coinButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(coinButtonClick) titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil image:nil backgroundImage:nil title:self.chargingModel.currency];
    
//    CGFloat labelWidth = coinButton.titleLabel.intrinsicContentSize.width; //注意不能直接使用titleLabel.frame.size.width,原因为有时候获取到0值
//    CGFloat imageWidth = coinButton.imageView.frame.size.width;
//    [XSUITool setButton:coinButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space) titleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0, imageWidth+space)];
    
    [XSUITool setButton:coinButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsZero titleEdgeInsets:UIEdgeInsetsZero];
    
    [tScrollView addSubview:coinButton];
    [coinButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainWidth/2).multipliedBy(0.9);
        make.top.mas_equalTo(30);
        make.width.mas_equalTo(FontNum(100));
    }];
    
    UIImageView * coinImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:[UIColor purpleColor] image:[UIImage imageNamed:@""]];
    [coinImageView getImageWithUrlStr:self.model.logo andDefaultImage:DefaultImage];
    [tScrollView addSubview:coinImageView];
    [coinImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(coinButton.mas_left).offset(-2);
        make.centerY.mas_equalTo(coinButton);
        make.size.mas_equalTo(CGSizeMake(FontNum(20), FontNum(20)));
    }];
    
    UIImageView * qrImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:[UIColor yellowColor] image:nil];
    [qrImageView getImageWithUrlStr:self.chargingModel.coinAddressImg andDefaultImage:DefaultImage];
    [tScrollView addSubview:qrImageView];
    [qrImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo((mainWidth- FontNum(157))/2);
        make.top.mas_equalTo(coinButton.mas_bottom).offset(33);
        make.size.mas_equalTo(CGSizeMake(FontNum(157), FontNum(157)));
    }];
    
    NSString *address = [NSString stringWithFormat:@"%@：%@",Localized(@"new_wallet_addr_data_title"),self.chargingModel.coinAddress];
    UILabel * walletAddressLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:address];
//    walletAddressLabel.numberOfLines = 0;
    [tScrollView addSubview:walletAddressLabel];
    [walletAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.width.mas_equalTo(mainWidth-40);
        make.top.mas_equalTo(qrImageView.mas_bottom).offset(16);
    }];
    
    if (self.model.address_type == 1) {
        NSString *tag = [NSString stringWithFormat:@"%@：%@",Localized(@"fill_tag"),self.chargingModel.tag];
        UILabel * tagLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:tag];
        tagLabel.numberOfLines = 0;
        [tScrollView addSubview:tagLabel];
        [tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(walletAddressLabel.mas_bottom).offset(10);
        }];
    }
    
    UIButton * toCopyAddressButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(toCopyAddress) titleColor:[UIColor whiteColor] titleFont:FontNum(16) backgroundColor:JYSMainSelelctColor image:nil backgroundImage:nil title:Localized(@"copy_addr")];
    [tScrollView addSubview:toCopyAddressButton];
    [toCopyAddressButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(qrImageView);
        make.top.mas_equalTo(walletAddressLabel.mas_bottom).offset(44);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(50)));
    }];
    
    _saveQRButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(toSaveQR) titleColor:JYSMainSelelctColor titleFont:FontNum(16) backgroundColor:[UIColor whiteColor] image:nil backgroundImage:nil title:Localized(@"save_ma")];
    [XSUITool setButton:_saveQRButton cornerRadius:0 borderWidth:1 borderColor:JYSMainSelelctColor];
    [tScrollView addSubview:_saveQRButton];
    [_saveQRButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(qrImageView);
        make.top.mas_equalTo(toCopyAddressButton.mas_bottom).offset(17);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(50)));
    }];
    //计算多行文字的size
    CGSize titleSize = [self.chargingModel.tips boundingRectWithSize:CGSizeMake(FontNum(344), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
    
    //tip温馨提示
    _tipLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@""];
    [tScrollView addSubview:_tipLabel];
    _tipLabel.numberOfLines = 0;
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(qrImageView);
        make.top.mas_equalTo(_saveQRButton.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), titleSize.height +30));
    }];
    _tipLabel.text = self.chargingModel.tips;
    
    CGFloat tHeight = 0;
    if (titleSize.height >10) {
        tHeight = titleSize.height;
        tScrollView.scrollEnabled = YES;
        tScrollView.showsHorizontalScrollIndicator = NO;
        tScrollView.showsVerticalScrollIndicator = NO;
        tScrollView.contentSize = CGSizeMake( 0,mainHeight+tHeight-30);
    }else{
        tScrollView.scrollEnabled = NO;
        tScrollView.contentSize = CGSizeMake( 0,0);

    }
}

- (void)setChargingMoneyWithModel:(JYSAssetManagementModel *)model {
    self.model = model;
}

- (void)coinButtonClick {
    
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSChargingMoneyURL params:@{@"currency":self.model.symbol} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            self.chargingModel = [JYSChargingModel mj_objectWithKeyValues:dic[@"data"]];
            [self setUpUI];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } showHUD:YES];
}

#pragma mark 复制地址
- (void)toCopyAddress {
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.chargingModel.coinAddress;
    [XSTool showToastWithView:self.view Text:Localized(@"clip_over")];
}
#pragma mark 保存二维码
- (void)toSaveQR {
    [XSTool showProgressHUDWithView:self.view];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.chargingModel.coinAddressImg]];
    UIImage *image = [UIImage imageWithData:data];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image: (UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    [XSTool hideProgressHUDWithView:self.view];
    [XSTool showToastWithView:self.view Text:@"二维码已保存到手机"];
}
@end
