//
//  JYSMentionMoneyHistoricalRecordVC.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMentionMoneyHistoricalRecordVC.h"
#import "JYSMentionMoneyHistoricalCell.h"
#import "JYSAssetManagementModel.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface JYSMentionMoneyHistoricalRecordVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

/** 0:全部(默认)1:充值2:提币 */
@property (assign, nonatomic) NSInteger recordType;

@property (strong, nonatomic) NSMutableArray *recordArray;

@property (assign, nonatomic) NSInteger page;

@end

@implementation JYSMentionMoneyHistoricalRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = Localized(@"history");
    
    self.recordArray = [NSMutableArray array];
    self.recordType = 0;
    [self setUpUI];
}

static NSString * const cellID = @"JYSMentionMoneyHistoricalCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSMentionMoneyHistoricalCell class] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    @weakify(self);
    self.showRefreshHeader = YES;
    [self setRefreshHeader:^{
        @strongify(self);
        self.page = 1;
        [self requestData];
    }];
    
    self.showRefreshFooter = YES;
    [self setRefreshFooter:^{
        @strongify(self);
        self.page++;
        [self requestData];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    //    self.tableView.tableHeaderView = self.topSortingView;
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSCoinRecordURL params:@{@"type":@(self.recordType),@"currency":isEmptyString(self.currency)?@"":self.currency,@"page":@(self.page)} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        [self endRefresh];
        if (state.status) {
            NSArray *array = dic[@"data"][@"data"];
            if (self.page == 1) {
                [self.recordArray removeAllObjects];
                for (NSDictionary *temp in array) {
                    JYSCoinRecordItem *item = [JYSCoinRecordItem mj_objectWithKeyValues:temp];
                    [self.recordArray addObject:item];
                }
            } else {
                for (NSDictionary *temp in array) {
                    JYSCoinRecordItem *item = [JYSCoinRecordItem mj_objectWithKeyValues:temp];
                    [self.recordArray addObject:item];
                }
                
            }
            if (array.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            [self.tableView reloadData];
        } else {
            
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}

#pragma mark 无数据界面

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"暂无数据";
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSMentionMoneyHistoricalCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.recordItem = self.recordArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(152);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
