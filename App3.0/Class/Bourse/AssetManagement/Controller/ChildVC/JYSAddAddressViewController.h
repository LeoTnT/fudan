//
//  JYSAddAddressViewController.h
//  App3.0
//
//  Created by mac on 2018/5/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"
#import "JYSAssetManagementModel.h"

@interface JYSAddAddressViewController : XSBaseTableViewController

@property (nonatomic, strong) JYSAssetManagementModel *model;

@end
