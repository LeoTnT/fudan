//
//  JYSAddAddressViewController.m
//  App3.0
//
//  Created by mac on 2018/5/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAddAddressViewController.h"

@interface JYSAddAddressViewController ()
@property (nonatomic, strong) UITextField *tagTF;
@property (nonatomic, strong) UITextField *addressTF;
@property (nonatomic, strong) UITextField *remarkTF;
@end

@implementation JYSAddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = Localized(@"添加地址");
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self setSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubviews {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 100)];
    self.tableView.tableFooterView = footerView;
    UIButton *addBtn = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(addAddressClick) titleColor:[UIColor whiteColor] titleFont:16 backgroundColor:mainColor image:nil backgroundImage:nil title:Localized(@"add_wallet_addr_btn")];
    [footerView addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(50, 16, 0, 16));
    }];
    
}

- (void)addAddressClick {
    if (isEmptyString(self.addressTF.text)) {
        [XSTool showToastWithView:self.view Text:Localized(@"new_wallet_addr_data_hint")];
        return;
    }
    if (self.model.address_type == 1 && isEmptyString(self.tagTF.text)) {
        [XSTool showToastWithView:self.view Text:Localized(@"new_wallet_addr_remark_hint")];
        return;
    }
    
    NSMutableDictionary *params = [@{@"currency":self.model.symbol,@"address":self.addressTF.text} mutableCopy];
    if (!isEmptyString(self.remarkTF.text)) {
        [params setObject:self.remarkTF.text forKey:@"remark"];
    }
    if (self.model.address_type) {
        [params setObject:self.tagTF.text forKey:@"tag"];
    }
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSWithdrawAddressInsertURL params:params HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 添加成功
            [XSTool showToastWithView:self.view Text:@"添加成功！"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    } showHUD:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.model.address_type == 1) {
        return 3;
    }
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(44);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *titles = @[Localized(@"new_wallet_addr_data_title"), Localized(@"note"), Localized(@"new_wallet_addr_remark_title")];
    NSArray *placeholders = @[Localized(@"new_wallet_addr_data_hint"), Localized(@"请输入备注"), Localized(@"请输入标签")];
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.textLabel.font = SYSTEM_FONT(15);
    cell.textLabel.text = titles[indexPath.row];
    
    UITextField *tf = [[UITextField alloc] init];
    tf.placeholder = placeholders[indexPath.row];
    tf.textAlignment = NSTextAlignmentRight;
    tf.font = SYSTEM_FONT(15);
    [cell.contentView addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 100, 0, 16));
    }];
    if (indexPath.row == 0) {
        self.addressTF = tf;
    } else if (indexPath.row == 1) {
        self.remarkTF = tf;
    } else {
        self.tagTF = tf;
    }
    
    return cell;
}
@end
