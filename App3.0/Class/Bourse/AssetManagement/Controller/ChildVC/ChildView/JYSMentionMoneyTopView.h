//
//  JYSMentionMoneyTopView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYSAssetManagementModel.h"

@interface JYSMentionMoneyTopView : UIView

@property (nonatomic, strong) JYSWithdrawModel *wdModel;

@property (nonatomic ,copy)void (^mentionMoney)(NSString *amount, NSString *address);

//@property (nonatomic ,copy)void (^chooseAddressBlock)(void);

/** 选择地址按钮 */
@property (nonatomic, strong) UIButton * chooseAddressButton;

/** 地址label */
@property (nonatomic, strong) UILabel * addressLabel;

- (void)setChooseAddressDatas:(NSArray *)addresses;

//- (void)hideAddressTableView;

@end
