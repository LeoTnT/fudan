//
//  JYSMentionMoneyChoseAddressView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMentionMoneyChoseAddressView.h"
#import "JYSAssetManagementModel.h"

@interface JYSMentionMoneyChoseAddressView ()<UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic, strong) UITableView * addreessTableView;

/** footerView */
@property (nonatomic, strong) UIView * footerView;

/** 地址数组 */
@property (nonatomic, copy) NSArray * addressesArray;

@end

@implementation JYSMentionMoneyChoseAddressView

- (UIView *)footerView {
    if (_footerView == nil) {
        _footerView = [[UIView alloc] init];
        _footerView.frame = CGRectMake(0, 0, self.xs_width, FontNum(50));
        
        UIButton * addAddressBtn = [XSUITool buttonWithFrame:_footerView.bounds buttonType:UIButtonTypeCustom Target:self action:@selector(addAddressBtnClick) titleColor:COLOR_666666 titleFont:FontNum(15) backgroundColor:BG_COLOR image:[UIImage imageNamed:@"jys_add_address"] backgroundImage:nil title:Localized(@"添加地址")];
        [_footerView addSubview:addAddressBtn];
        [addAddressBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return _footerView;
}

static NSString * const cellID = @"cellID";
- (UITableView *)addreessTableView {
    if (_addreessTableView == nil) {
        _addreessTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _addreessTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _addreessTableView.bounces = NO;
        _addreessTableView.backgroundColor = BG_COLOR;
        _addreessTableView.showsVerticalScrollIndicator = NO;
        [_addreessTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
        _addreessTableView.delegate = self;
        _addreessTableView.dataSource = self;
    }
    return _addreessTableView;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = BG_COLOR;
        [self setUpUI];
    }
    return self;
}

- (void)setAddressDatasWithArray:(NSArray *)addresses {
    self.addressesArray = addresses;

    [self.addreessTableView reloadData];
}

-(void)setUpUI {
    
    [self addSubview:self.addreessTableView];
    [self.addreessTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        
    }];
    
    UIView * headerView = [XSUITool creatViewWithFrame:CGRectMake(0, 0, self.xs_width, FontNum(50)) backgroundColor:JYSMainSelelctColor];
    self.addreessTableView.tableHeaderView = headerView;
    
    UILabel * choseLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:[UIColor whiteColor] titleFont:FontNum(17) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:Localized(@"choose_aadrs")];
    [headerView addSubview:choseLab];
    [choseLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(headerView);
    }];
    
    self.addreessTableView.tableFooterView = self.footerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.addressesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.addressesArray.count) {
        
        JYSCoinAddressModel *model  = self.addressesArray[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@:%@",model.remark,model.address];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(50);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (self.selectedAddressBlock) {
//        self.selectedAddressBlock(self.addressesArray[indexPath.row], indexPath.row);
//    }
    
    if ([self.delegate respondsToSelector:@selector(selectedAddress:index:)]) {
        JYSCoinAddressModel *model  = self.addressesArray[indexPath.row];
        [self.delegate selectedAddress:model.address index:indexPath.row];
    }
    
}

/// 如何返回 NO 的话 specified item 将无法被编辑。
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (self.modelArray_QSBMyStoreListModel.count == 0) {
//        return NO;
//    }
//    if (indexPath.row % 2 == 0) {
//        return YES;
//    }
//    return NO;
    return YES;
}

/// 不写在 iOS8.3 的系统上不能操作。
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

/// 设置左滑自定义界面
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.addressesArray.count <= 0) {
        return nil;
    }
    
    NSArray *actionArray;
    
    NSString *title = NSLocalizedString(@"删除", nil);
    UITableViewRowAction *deleteRoWAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:title handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {//title可自已定义
        XSLog(@"删除%ld行",indexPath.row);
        if ([self.delegate respondsToSelector:@selector(deleteAddressWithIndex:)]) {
            [self.delegate deleteAddressWithIndex:indexPath.row];
        }
    }];
    
    actionArray = @[deleteRoWAction];
    
    return actionArray;//最后返回这俩个RowAction 的数组
}


- (void)addAddressBtnClick {
//    if (self.addAddressBlock) {
//        self.addAddressBlock();
//    }
    if ([self.delegate respondsToSelector:@selector(addWalletAddress)]) {
        [self.delegate addWalletAddress];
    }
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end
