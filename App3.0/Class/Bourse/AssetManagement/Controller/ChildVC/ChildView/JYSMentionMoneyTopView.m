//
//  JYSMentionMoneyTopView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMentionMoneyTopView.h"

@interface JYSMentionMoneyTopView ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

/** 币种 */
@property (nonatomic, strong) UIButton * coinButton;
@property (nonatomic, strong) UIImageView * coinImageView;
/** 余额 */
@property (nonatomic, strong) UILabel * assetsLabel;
/** 日提币额度 */
@property (nonatomic, strong) UILabel * dailyLimitLabel;
/** 今日已用 */
@property (nonatomic, strong) UILabel * todayUseLabel;
/** 提现数 */
@property (nonatomic, strong) UITextField * withdrawalTextField;
/** 可用 */
@property (nonatomic, strong) UILabel * canUseLabel;
/** 手续费 */
@property (nonatomic, strong) UILabel * poundageLabel;
/** 实际到账 */
@property (nonatomic, strong) UILabel * actualToAccountLabel;
/** tip */
@property (nonatomic, strong) UILabel * tipLabel;

/** 背景View */
@property (nonatomic, strong) UIView * bgAView;
/** 地址数组 */
@property (nonatomic, strong) NSArray * addressDatas;

@property (nonatomic, strong) UIButton * mentionMoneyButton;
@end

@implementation JYSMentionMoneyTopView

static NSUInteger const rowHeight = 44;//行高
static NSUInteger const maxRows = 5;//最多一屏显示行数

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XSYCOLOR(0xE7EAF1);
        [self setUpUI];
    }
    return self;
}



//static CGFloat const space = 5.f;
-(void)setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    // [UIImage imageNamed:@"sort_down"]
    UIButton * coinButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(coinButtonClick) titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil image:nil backgroundImage:nil title:@"BTC"];

    coinButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self addSubview:coinButton];
    [coinButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_centerX).multipliedBy(0.9);
        make.top.mas_equalTo(self).offset(40);
        make.width.mas_equalTo(FontNum(100));
    }];
    self.coinButton = coinButton;
    
    UIImageView * coinImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:[UIColor whiteColor] image:[UIImage imageNamed:@""]];
    
    [self addSubview:coinImageView];
    [coinImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(coinButton.mas_left).offset(-2);
        make.centerY.mas_equalTo(coinButton);
        make.size.mas_equalTo(CGSizeMake(FontNum(20), FontNum(20)));
    }];
    self.coinImageView = coinImageView;
    
    //资产
    UILabel * assetsLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(21) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"0.0  %@",Localized(@"assets")]];
    [self addSubview:assetsLabel];
    [assetsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(20);
        make.right.mas_equalTo(self).offset(-20);
        make.top.mas_equalTo(coinButton.mas_bottom).offset(17);
    }];
    self.assetsLabel = assetsLabel;
    
    //日提币额度
    UILabel * dailyLimitLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"%@：0.0",Localized(@"日提币额度")]];
    [self addSubview:dailyLimitLabel];
    [dailyLimitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(assetsLabel.mas_bottom).offset(16);
    }];
    self.dailyLimitLabel = dailyLimitLabel;
    
    //今日已用
    UILabel * todayUseLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_666666 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:[NSString stringWithFormat:@"%@：0.0",Localized(@"has_use")]];
    [self addSubview:todayUseLabel];
    [todayUseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(dailyLimitLabel.mas_bottom).offset(5);
    }];
    self.todayUseLabel = todayUseLabel;
    
    //选择地址
    self.addressLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:XSYCOLOR(0xCDCDCD) titleFont:FontNum(15) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:Localized(@"choose_aadrs")];
    [self addSubview:self.addressLabel];
    
    self.chooseAddressButton = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:nil action:nil];
    [self addSubview:self.chooseAddressButton];
    [self.chooseAddressButton setImage:[UIImage imageNamed:@"sort_down"] forState:UIControlStateNormal];
    [XSUITool setButton:self.chooseAddressButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentRight contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 12) titleEdgeInsets:UIEdgeInsetsZero];
    [XSUITool setButton:self.chooseAddressButton cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [self.chooseAddressButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(todayUseLabel.mas_bottom).offset(22);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(44)));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.chooseAddressButton).offset(12);
        make.right.mas_lessThanOrEqualTo(self.chooseAddressButton.mas_right).offset(-40);
        make.centerY.mas_equalTo(self.chooseAddressButton);
    }];
    
    self.bgAView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:nil];
    [XSUITool setView:self.bgAView cornerRadius:0 borderWidth:1 borderColor:XSYCOLOR(0xDDDDDD)];
    [self addSubview:self.bgAView];
    [self.bgAView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.chooseAddressButton.mas_bottom).offset(21);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(44)));
    }];
    
    self.withdrawalTextField = [XSUITool creatTextFieldWithFrame:CGRectZero backgroundColor:nil font:FontNum(15) textAlignment:NSTextAlignmentLeft delegate:self placeholderColor:COLOR_999999 textColor:JYSMainTextColor placeholder:[NSString stringWithFormat:@"%@：0.0",Localized(@"最小提币数量")] text:nil];
//    [self.withdrawalTextField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventValueChanged];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textVauleChanged:)name:UITextFieldTextDidChangeNotification object:self.withdrawalTextField];
    [self.bgAView addSubview:self.withdrawalTextField];
    [self.withdrawalTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.bgAView);
        make.left.mas_equalTo(12); make.width.mas_equalTo(self.bgAView).multipliedBy(0.8);
    }];
    
    UIButton * allButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(allButtonClicked) titleColor:JYSMainSelelctColor titleFont:FontNum(15) backgroundColor:nil image:nil backgroundImage:nil title:Localized(@"alls")];
    [self.bgAView addSubview:allButton];
    [allButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(self.bgAView);
        make.width.mas_equalTo(self.bgAView).multipliedBy(0.2);
    }];
    
    //可用
    UILabel * canUseLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:Localized(@"can_use")];
    [self addSubview:canUseLab];
    [canUseLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgAView.mas_bottom).offset(12);
        make.left.mas_equalTo(self.bgAView);
    }];
    
    //可用
    UILabel * canUseLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainSelelctColor titleFont:FontNum(12) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"2.000000 btc"];
    [self addSubview:canUseLabel];
    [canUseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(canUseLab);
        make.left.mas_equalTo(canUseLab.mas_right).offset(5);
    }];
    self.canUseLabel = canUseLabel;
    
    //手续费
    UILabel * poundageLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:Localized(@"sxf")];
    [self addSubview:poundageLab];
    [poundageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(canUseLab);
//        make.top.mas_equalTo(canUseLab.mas_baseline).offset(28);
        make.top.mas_equalTo(canUseLab.mas_baseline).offset(0);
    }];
    
    //手续费
    UILabel * poundageLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"0.00"];
    [self addSubview:poundageLabel];
    [poundageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgAView);
        make.centerY.mas_equalTo(poundageLab);
    }];
    self.poundageLabel = poundageLabel;
    
    //实际到账
    UILabel * actualToAccountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:Localized(@"sjdz")];
    [self addSubview:actualToAccountLab];
    [actualToAccountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(canUseLab);
        make.top.mas_equalTo(poundageLab.mas_baseline).offset(17);
    }];
    
    //实际到账
    UILabel * actualToAccountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"0.00"];
    [self addSubview:actualToAccountLabel];
    [actualToAccountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgAView);
        make.centerY.mas_equalTo(actualToAccountLab);
    }];
    self.actualToAccountLabel = actualToAccountLabel;
    
    _mentionMoneyButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(mentionMoneyButtonClick) titleColor:[UIColor whiteColor] titleFont:FontNum(16) backgroundColor:JYSMainSelelctColor image:nil backgroundImage:nil title:Localized(@"tibi")];
    [self addSubview:_mentionMoneyButton];
    [_mentionMoneyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(actualToAccountLab.mas_bottom).offset(29);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(50)));
    }];
    
    
    //tip温馨提示
    _tipLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@""];
    [self addSubview:_tipLabel];
    _tipLabel.numberOfLines = 0;
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(_mentionMoneyButton.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), FontNum(100)));
    }];
    self.dailyLimitLabel.hidden = YES;
    self.todayUseLabel.hidden = YES;
    self.canUseLabel.hidden = YES;
    canUseLab.hidden = YES;
    self.dailyLimitLabel.text = @"";
    self.todayUseLabel.text = @"";
    self.canUseLabel.text = @"";
}
/**
 @method 获取指定宽度width的字符串在UITextView上的高度
 @param textView 待计算的UITextView
 @param Width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
- (float) heightForString:(UITextView *)textView andWidth:(float)width{
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit.height;
}

- (void)setWdModel:(JYSWithdrawModel *)wdModel {
    _wdModel = wdModel;
    _tipLabel.text = wdModel.tips;
    //计算多行文字的size
    CGSize titleSize = [wdModel.tips boundingRectWithSize:CGSizeMake(FontNum(344), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
    [_tipLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(_mentionMoneyButton.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(FontNum(344), titleSize.height+30));
    }];
   
    [self.coinButton setTitle:wdModel.currency forState:UIControlStateNormal];
    [self.coinImageView getImageWithUrlStr:wdModel.logo andDefaultImage:DefaultImage];
    self.assetsLabel.text = [NSString stringWithFormat:@"%@ %@",wdModel.remain,Localized(@"assets")];
//    self.dailyLimitLabel.text = [NSString stringWithFormat:@"%@：%@ %@",Localized(@"日提币额度"),wdModel.max_day,wdModel.currency];
//    self.todayUseLabel.text = [NSString stringWithFormat:@"%@：%@ %@",Localized(@"has_use"),wdModel.withdraw_day,wdModel.currency];
    
    if (wdModel.min.floatValue == 0) {
        self.withdrawalTextField.placeholder = [NSString stringWithFormat:@"%@",Localized(@"请输入提币数量")];
    }else{
    self.withdrawalTextField.placeholder = [NSString stringWithFormat:@"%@ %@%@",Localized(@"最小提币数量"),wdModel.min,wdModel.currency];
    }
//    self.canUseLabel.text = [NSString stringWithFormat:@"%@ %@",[self calculateCanUseValue],self.wdModel.currency];
    
    if (self.wdModel.fee_type == 1) {
        // 百分比
        self.poundageLabel.text = [NSString stringWithFormat:@"%.6f",[self.withdrawalTextField.text doubleValue] * [self.wdModel.fee_set doubleValue]];
//        self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.poundageLabel.text doubleValue]];
    } else {
        // 固定值
        self.poundageLabel.text = self.wdModel.fee_set;
//        self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.wdModel.fee_set doubleValue]];
    }
    if ([self getTotalValue]) {
        self.actualToAccountLabel.text = [self getTotalValue];
    }else{
    self.actualToAccountLabel.text = @"0.00";
    }
    self.dailyLimitLabel.hidden = YES;
    self.todayUseLabel.hidden = YES;
    self.dailyLimitLabel.text = @"";
    self.todayUseLabel.text = @"";
}

- (void)textFieldDidChanged:(UITextField *)textField {
    if ([textField isEqual:self.withdrawalTextField]) {
        self.actualToAccountLabel.text = [self getTotalValue];

        // 计算手续费和实际到账
        if (self.wdModel.fee_type == 1) {
            // 百分比
            self.poundageLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] * [self.wdModel.fee_set doubleValue]*0.01];
//            self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.poundageLabel.text doubleValue]];
        } else {
            // 固定值
            self.poundageLabel.text = self.wdModel.fee_set;
//            self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.wdModel.fee_set doubleValue]];
        }
        
    }
}

- (void)textVauleChanged:(NSNotification *)notification {
    UITextField *textField = notification.object;
    
    if ([textField isEqual:self.withdrawalTextField]) {
        // 计算手续费和实际到账
        self.poundageLabel.text = [self calculateCanUseValue];
        self.actualToAccountLabel.text = [self getTotalValue];

        if (self.wdModel.fee_type == 1) {
            // 百分比
            self.poundageLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] * [self.wdModel.fee_set doubleValue]*0.01];
//            self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.poundageLabel.text doubleValue]];
        } else {
            // 固定值
            self.poundageLabel.text = self.wdModel.fee_set;
            
            NSString * feeStr = [NSString stringWithFormat:@"%.6f",[textField.text doubleValue] - [self.wdModel.fee_set doubleValue]];
            
            if ([feeStr integerValue] < 0) {
                feeStr = @"0";
            }
//            self.actualToAccountLabel.text = feeStr;
        }
    }
}

- (NSString *)calculateCanUseValue {
    if ([self.wdModel.max_day doubleValue] == 0) {
        return self.wdModel.remain;
    } else {
//        double lower = [self.wdModel.remain doubleValue] > [self.wdModel.max_day doubleValue]?[self.wdModel.max_day doubleValue]:[self.wdModel.remain doubleValue];
//        double canUse = lower - [self.wdModel.withdraw_day doubleValue];

        //剩余今日可用额度
        double dayRemian = [self.wdModel.max_day doubleValue]-[self.wdModel.withdraw_day doubleValue];
        double canUse = dayRemian > [self.wdModel.remain doubleValue]?[self.wdModel.remain doubleValue]:dayRemian;
        
        if (canUse <=0) {
            canUse = 0;
        }
        return [NSString stringWithFormat:@"%.6f",canUse];
    }
}

- (NSString *)getTotalValue {
    
    if (isEmptyString(self.withdrawalTextField.text)) {
        return @"0.000000";
        
    }
    if (self.wdModel.fee_type == 1) {//百分比
        if(self.withdrawalTextField.text.doubleValue*(1+self.wdModel.fee_set.doubleValue*0.01) <= [self calculateCanUseValue].doubleValue) {//提现输入的金额,从剩余的金额中扣除
            return [NSString stringWithFormat:@"%@",self.withdrawalTextField.text];
//            CGFloat total = self.withdrawalTextField.text.doubleValue*(1-self.wdModel.fee_set.doubleValue*0.01);
//            return [NSString stringWithFormat:@"%.6f",total];
        }else if (self.withdrawalTextField.text.doubleValue >= [self calculateCanUseValue].doubleValue){
            CGFloat total = self.withdrawalTextField.text.doubleValue*(1-self.wdModel.fee_set.doubleValue*0.01);
            return [NSString stringWithFormat:@"%.6f",total];
        }else{
            CGFloat total = [self calculateCanUseValue].doubleValue*(1-self.wdModel.fee_set.doubleValue*0.01);
            return [NSString stringWithFormat:@"%.6f",total];
            
        }
    }else{//固定值
        if (self.withdrawalTextField.text.doubleValue+self.wdModel.fee_set.doubleValue <= [self calculateCanUseValue].doubleValue) {
            return [NSString stringWithFormat:@"%@",self.withdrawalTextField.text];
        }else if (self.withdrawalTextField.text.doubleValue >= [self calculateCanUseValue].doubleValue){
            CGFloat total = self.withdrawalTextField.text.doubleValue-self.wdModel.fee_set.doubleValue;
            return [NSString stringWithFormat:@"%.6f",total];
        }else{
            CGFloat total = [self calculateCanUseValue].doubleValue-self.wdModel.fee_set.doubleValue;
            return [NSString stringWithFormat:@"%.6f",total];
            
        }
    }
}



- (void)coinButtonClick {
    
}

//- (void)chooseAdress {
//    if (self.chooseAddressBlock) {
//        self.chooseAddressBlock();
//    }
//}

- (void)allButtonClicked {
    self.withdrawalTextField.text = [self calculateCanUseValue];
    self.actualToAccountLabel.text = [self getTotalValue];
    // 计算手续费和实际到账
    if (self.wdModel.fee_type == 1) {
        // 百分比
        self.poundageLabel.text = [NSString stringWithFormat:@"%.6f",[self.withdrawalTextField.text doubleValue] * [self.wdModel.fee_set doubleValue]*0.01];
//        self.actualToAccountLabel.text = [NSString stringWithFormat:@"%.6f",[self.withdrawalTextField.text doubleValue] - [self.poundageLabel.text doubleValue]];
    } else {
        // 固定值
        self.poundageLabel.text = self.wdModel.fee_set;
        
//        NSString * feeStr = [NSString stringWithFormat:@"%.6f",[self.withdrawalTextField.text doubleValue] - [self.wdModel.fee_set doubleValue]];
//
//        if ([feeStr integerValue] < 0) {
//            feeStr = @"0";
//        }
//        self.actualToAccountLabel.text = feeStr;
    }
}

#pragma mark 提币
- (void)mentionMoneyButtonClick {
    if (self.mentionMoney) {
        self.mentionMoney(self.withdrawalTextField.text, self.addressLabel.text);
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self touchesTheScreen];
}

- (void)touchesTheScreen {
    [self endEditing:YES];
//    [self hideChooseAddressView];
    if (self.chooseAddressButton.selected) {
        [UIView animateWithDuration:0.25 animations:^{
            self.chooseAddressButton.imageView.transform=CGAffineTransformRotate(self.chooseAddressButton.imageView.transform, M_PI);
        }];
    }
    self.chooseAddressButton.selected = NO;
}


-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.withdrawalTextField];
}

@end
