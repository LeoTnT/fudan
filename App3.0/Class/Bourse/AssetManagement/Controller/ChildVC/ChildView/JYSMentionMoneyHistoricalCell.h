//
//  JYSMentionMoneyHistoricalCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYSAssetManagementModel.h"

@interface JYSMentionMoneyHistoricalCell : UITableViewCell
@property (strong, nonatomic) JYSCoinRecordItem *recordItem;
@end
