//
//  JYSMentionMoneyHistoricalCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMentionMoneyHistoricalCell.h"
#import "XSFormatterDate.h"

@interface JYSMentionMoneyHistoricalCell ()

/** 时间 */
@property (nonatomic, strong) UILabel * timeLabel;

/** 充提币 */
@property (nonatomic, strong) UILabel * mentionMoneyCountTitle;
/** 提币数量 */
@property (nonatomic, strong) UILabel * mentionMoneyCountLabel;
/** 实际到账 */
@property (nonatomic, strong) UILabel * actualToAccountLabel;
/** 钱包地址 */
@property (nonatomic, strong) UILabel * walletAddressLabel;

/** 审核状态 */
@property (nonatomic, strong) UILabel * reviewStatusLabel;

/** 提币 */
@property (nonatomic, strong) UILabel * typeLabel;

@end

@implementation JYSMentionMoneyHistoricalCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.backgroundColor = XSYCOLOR(0xf1f1f1);
    
    UIView * bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 15));
    }];
    
    UIImageView * bgImageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"jys_cell_bgImage"]];
    [self.contentView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-2, 2, -5, 2));
    }];
    
    UILabel * mentionMoneyCountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"提币数量")];
    [self.contentView addSubview:mentionMoneyCountLab];
    [mentionMoneyCountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.baseline.mas_equalTo(bgView.mas_centerY).multipliedBy(0.4);
    }];
    self.mentionMoneyCountTitle = mentionMoneyCountLab;
    
    self.mentionMoneyCountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2.00000000 CLC"];
    [self.contentView addSubview:self.mentionMoneyCountLabel];
    [self.mentionMoneyCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mentionMoneyCountLab.mas_right).offset(10);
        make.centerY.mas_equalTo(mentionMoneyCountLab);
    }];
    
    UILabel * actualToAccountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"sjdz")];
    [self.contentView addSubview:actualToAccountLab];
    [actualToAccountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mentionMoneyCountLab);
        make.baseline.mas_equalTo(bgView.mas_centerY).multipliedBy(0.75);
    }];
    
    self.actualToAccountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"1.99999999 CLC"];
    [self.contentView addSubview:self.actualToAccountLabel];
    [self.actualToAccountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(actualToAccountLab.mas_right).offset(10);
        make.centerY.mas_equalTo(actualToAccountLab);
    }];
    
    UILabel * walletAddressLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"new_wallet_addr_data_title")];
    [self.contentView addSubview:walletAddressLab];
    walletAddressLab.adjustsFontSizeToFitWidth = YES;
    walletAddressLab.minimumScaleFactor = 0.8;
    [walletAddressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.width.mas_equalTo(FontNum(60)); make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.05);
    }];
    
    self.walletAddressLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"CH9vtSMt1x9yueionpZ4QoUeG5BrKvfrMw"];
    [self.contentView addSubview:self.walletAddressLabel];
    self.walletAddressLabel.numberOfLines = 2;
    [self.walletAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(walletAddressLab.mas_right).offset(10);
        make.right.mas_equalTo(bgView).offset(-12);
        make.centerY.mas_equalTo(walletAddressLab);
    }];
    
    self.timeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2018-03-24 16:12"];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.55);
    }];
    
    self.reviewStatusLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"拒绝"];
    [self.contentView addSubview:self.reviewStatusLabel];
    [self.reviewStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-12);
        make.centerY.mas_equalTo(mentionMoneyCountLab);
    }];
    
    UILabel * reviewStatusLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"审核状态"];
    [self.contentView addSubview:reviewStatusLab];
    [reviewStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.reviewStatusLabel.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.reviewStatusLabel);
    }];
    
    self.typeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainSelelctColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"tibi")];
    [self.contentView addSubview:self.typeLabel];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-12);
        make.centerY.mas_equalTo(self.timeLabel);
    }];
}

- (void)setRecordItem:(JYSCoinRecordItem *)recordItem {
    _recordItem = recordItem;
    
    if (recordItem.status == 1||recordItem.status == 2||recordItem.status == 3||recordItem.status == 4||recordItem.status == 5||recordItem.status == 6||recordItem.status == 7) {
        recordItem.type = 1;
        self.typeLabel.text = Localized(@"tibi");
    }else if (recordItem.status == 10||recordItem.status == 11) {
        recordItem.type = 0;
        self.typeLabel.text = Localized(@"charge_money");

    }
    if (recordItem.type == 1) {
        self.mentionMoneyCountTitle.text = Localized(@"提币数量");
    } else {
        self.mentionMoneyCountTitle.text = Localized(@"充币数量");
    }
    
    
    /**   1 => '进入区块',
     2 => '提币成功',
     3 => '提币已申请',
     4 => '提币已审核',
     5 => '提币已拒绝',
     6 => '提币未知状态',
     7 => '提币自动审核',
     10 => '充值成功',
     11 => '充值小于最小到账数量'
     */
    NSString *statusTr = @"未知状态";
    if (recordItem.status == 1) {
        statusTr = Localized(@"进入区块");
    }else if (recordItem.status == 2)
    {
        statusTr = Localized(@"提币成功");
    }else if (recordItem.status == 3)
    {
        statusTr = Localized(@"提币已申请");
    }else if (recordItem.status == 4)
    {
        statusTr = Localized(@"提币已审核");
    }else if (recordItem.status == 5)
    {
        statusTr = Localized(@"提币已拒绝");
    }else if (recordItem.status == 6)
    {
        statusTr = Localized(@"提币未知状态");
    }else if (recordItem.status == 7)
    {
        statusTr = Localized(@"提币自动审核");
    }else if (recordItem.status == 10)
    {
        statusTr = Localized(@"充值成功");
    }else if (recordItem.status == 11)
    {
        statusTr = Localized(@"充值小于最小到账数量");
    }
    
    
    self.reviewStatusLabel.text = statusTr;
    
    double total = [recordItem.amount doubleValue];
    self.mentionMoneyCountLabel.text = [NSString stringWithFormat:@"%.6f %@",total,recordItem.currency_name];
    self.actualToAccountLabel.text = [NSString stringWithFormat:@"%@ %@",recordItem.real_amount,recordItem.currency_name];
    self.walletAddressLabel.text = recordItem.destination;
    self.timeLabel.text = [XSFormatterDate dateWithTimeIntervalString:recordItem.w_time];
    
    
}
@end
