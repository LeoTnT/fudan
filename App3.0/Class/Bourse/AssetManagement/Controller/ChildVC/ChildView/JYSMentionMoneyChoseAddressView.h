//
//  JYSMentionMoneyChoseAddressView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/17.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JYSMentionMoneyChoseAddressViewDelegate <NSObject>

- (void)addWalletAddress;
- (void)selectedAddress:(NSString *)addressString index:(NSUInteger)index;
- (void)deleteAddressWithIndex:(NSUInteger)index;

@end

@interface JYSMentionMoneyChoseAddressView : UIView

/** 代理 */
@property (nonatomic, weak) id<JYSMentionMoneyChoseAddressViewDelegate>delegate;

/** 地址数组 */
//@property (nonatomic, copy) NSArray * addressesArray;

//@property (nonatomic ,copy)void (^addAddressBlock)(void);
//
//@property (nonatomic ,copy)void (^selectedAddressBlock)(NSString *addressString, NSUInteger index);

- (void)setAddressDatasWithArray:(NSArray *)addresses;

@end
