//
//  JYSMentionMoneyViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMentionMoneyViewController.h"
#import "JYSMentionMoneyTopView.h"
#import "JYSMentionMoneyHistoricalRecordVC.h"
#import "JYSAssetManagementModel.h"
#import "JYSMentionMoneyChoseAddressView.h"
#import "JYSPopoverView.h"
#import "JYSAddAddressViewController.h"
#import "RSAEncryptor.h"
#import "InputAlertNewTextFieldView.h"
#import "ScanViewController.h"

@interface JYSMentionMoneyViewController ()<JYSMentionMoneyChoseAddressViewDelegate,InputAlertNewTextFieldViewDelegate>
{
    BOOL _isTimer;// 是否在倒计时
    BOOL _isTimer_email;// 是否在倒计时
    
}
@property (nonatomic ,strong) dispatch_source_t timer;
@property (nonatomic ,strong) dispatch_source_t timer_email;

/** 地址数组 */
@property (nonatomic, strong) NSMutableArray * addressArray;

/** view */
@property (nonatomic, strong) JYSMentionMoneyTopView * topView;

/** model */
@property (nonatomic, strong) JYSAssetManagementModel * model;

@property (nonatomic, strong) JYSWithdrawModel *withdrawModel;

/** 添加地址 */
@property (nonatomic, strong) JYSMentionMoneyChoseAddressView * choseArrassView;
/** 添加地址弹框 */
@property (nonatomic ,strong) JYSPopoverView * addAddressPopoverView;
@property(nonatomic,strong)  InputAlertNewTextFieldView *alertNewView;

@property (nonatomic, strong) NSString *up_amount;
@property (nonatomic, strong) NSString *up_address;

@end

@implementation JYSMentionMoneyViewController

- (NSMutableArray *)addressArray {
    if (_addressArray == nil) {
        _addressArray = [[NSMutableArray alloc] init];
    }
    return _addressArray;
}

- (JYSMentionMoneyTopView *)topView {
    if (_topView == nil) {
        _topView = [[JYSMentionMoneyTopView alloc] init];
        
    }
    return _topView;
}

- (JYSMentionMoneyChoseAddressView *)choseArrassView {
    if (_choseArrassView == nil) {
        _choseArrassView = [[JYSMentionMoneyChoseAddressView alloc] init];
        _choseArrassView.frame = CGRectMake(0, 0, FontNum(320), FontNum(100));
        _choseArrassView.delegate = self;
    }
    return _choseArrassView;
}

- (void)setMentionMoneyWithModel:(JYSAssetManagementModel *)model {
    _model = model;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"tibi");
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:JYSMainSelelctColor titleFont:[UIFont systemFontOfSize:FontNum(15) weight:UIFontWeightSemibold] target:self action:@selector(rightBarButtonItemClicked) titleString:Localized(@"history_record")];
    
    [self setUpUI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestData];
}

- (void)rightBarButtonItemClicked {
    JYSMentionMoneyHistoricalRecordVC * historicalRecordVC = [[JYSMentionMoneyHistoricalRecordVC alloc] init];
    historicalRecordVC.currency = self.model.symbol;
    [self.navigationController pushViewController:historicalRecordVC animated:YES];
}

- (void)setUpUI {
    self.view.backgroundColor = [UIColor whiteColor];
        
    [self.view addSubview:self.topView];
    [self.topView.chooseAddressButton addTarget:self action:@selector(chooseAddressButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.height.mas_equalTo(FontNum(480+150));
    }];
    
    @weakify(self);
    [self.topView setMentionMoney:^(NSString *amount, NSString *address) {
        @strongify(self);
        if (isEmptyString(amount)) {
            [XSTool showToastWithView:self.view Text:@"请输入提币数量！"];
            return;
        }
        if (isEmptyString(address) || [address isEqualToString:Localized(@"choose_aadrs")]) {
            [XSTool showToastWithView:self.view Text:@"请选择提币地址！"];
            return;
        }
        self.up_amount = amount;
        self.up_address = address;
        NSString *email = @"";
        NSString *mobile = @"";
        BOOL isPay = YES;
        if (self.withdrawModel.show_mobile.intValue) {
            mobile = self.withdrawModel.mobile;
            isPay = NO;
        }
        if (self.withdrawModel.show_email.intValue) {
            email = self.withdrawModel.email;
            isPay = NO;
        }
        if (self.withdrawModel.show_google.intValue) {
            isPay = NO;
        }
        
        self.alertNewView = [[InputAlertNewTextFieldView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) mobile:mobile email:email isGoogle:self.withdrawModel.show_google.intValue isPayPSW:isPay];
        self.alertNewView.delegate = self;
        [[UIApplication sharedApplication].delegate.window addSubview:self.alertNewView];
        
    }];

}
#pragma mark 选择地址
- (void)chooseAddressButtonClicked {
//    JYSPopoverView *alertview = [[JYSPopoverView alloc] initWithAlertView:self.choseArrassView];
//    wSelf.addAddressPopoverView = alertview;
//    //动画样式
//    wSelf.addAddressPopoverView.viewAnimateStyle = ViewAnimateScale;
//    wSelf.addAddressPopoverView.isBGClose = YES;
    [self createAlertSheetView];
}


- (void)createAlertSheetView
{
    UIAlertController *sheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    __weak __typeof__(self) wSelf = self;

    UIAlertAction *selAdress = [UIAlertAction actionWithTitle:@"选择地址" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        JYSPopoverView *alertview = [[JYSPopoverView alloc] initWithAlertView:self.choseArrassView];
        wSelf.addAddressPopoverView = alertview;
        //动画样式
        wSelf.addAddressPopoverView.viewAnimateStyle = ViewAnimateScale;
        wSelf.addAddressPopoverView.isBGClose = YES;
    }];
    
    UIAlertAction *QR_code = [UIAlertAction actionWithTitle:@"扫描二维码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [wSelf QRCodeAction];
    }];
    UIAlertAction *paste = [UIAlertAction actionWithTitle:@"从复制版粘贴" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //系统级别
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSLog(@"粘贴的内容为:%@",pasteboard.string);
        
        if (!isEmptyString(pasteboard.string)) {
            wSelf.topView.addressLabel.text = pasteboard.string;
        }else{
            Alert(@"暂无粘贴内容!");
        }
    }];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [sheet addAction:selAdress];
    [sheet addAction:QR_code];
    [sheet addAction:paste];
    [sheet addAction:cancle];
    
    //修改按钮
    if(NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_9_0) {
        // iOS 9.0 以上系统的处理
        [cancle setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [selAdress setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [QR_code setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [paste setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    }else{
        // iOS 9.0 以下系统的处理
    }
    
   
    
    [self presentViewController:sheet animated:YES completion:^{
    }];
}




-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.choseArrassView removeFromSuperview];
    self.choseArrassView = nil;
//    [self setUpAddAddress];
    
    if (self.addressArray) {
        NSUInteger rowsNumber;
        if (self.addressArray.count > 5) {
            rowsNumber = 5;
        } else {
            rowsNumber = self.addressArray.count;
        }
        self.choseArrassView.frame = CGRectMake(0, 0, FontNum(320), FontNum(100)+FontNum(50)*rowsNumber);
        
        [self.choseArrassView setAddressDatasWithArray:self.addressArray];
    }
}

//- (void)setUpAddAddress {
//    @weakify(self);
//    [self.topView setChooseAddressBlock:^{
//        @strongify(self);
//        //        UIView * bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FontNum(320), FontNum(100))];
//        //        bgView.backgroundColor = [UIColor clearColor];
//        //        self.choseArrassView.frame = bgView.bounds;
//        //        [bgView addSubview:self.choseArrassView];
//
//
//
//    }];
//
//
//
////    self.choseArrassView.addAddressBlock = ^{
////        @strongify(self);
////        __weak typeof(self) weakSelf = self;
////        [self.addAddressPopoverView closeAlertView:^{
////            [weakSelf performSelector:@selector(jumpToAddAddressVC) withObject:nil afterDelay:0.55f];
////        }];
////    };
//
////    self.choseArrassView.selectedAddressBlock = ^(NSString *addressString, NSUInteger index) {
////        @strongify(self);
////        __weak typeof(self) weakSelf = self;
////        [self.addAddressPopoverView closeAlertView:^{
////            weakSelf.topView.addressLabel.text = addressString;
////        }];
////    };
//}

#pragma mark 选择地址代理
-(void)addWalletAddress {
    __weak typeof(self) weakSelf = self;
    [self.addAddressPopoverView closeAlertView:^{
        [weakSelf performSelector:@selector(jumpToAddAddressVC) withObject:nil afterDelay:0.55f];
    }];
}
-(void)selectedAddress:(NSString *)addressString index:(NSUInteger)index {
    __weak typeof(self) weakSelf = self;
    [self.addAddressPopoverView closeAlertView:^{
        weakSelf.topView.addressLabel.text = addressString;
    }];
}
-(void)deleteAddressWithIndex:(NSUInteger)index {
    
    JYSCoinAddressModel *temp = self.withdrawModel.coin_address[index];
    
    NSString * idString = temp.ID;
    
    __weak typeof(self) weakSelf = self;
    [self.addAddressPopoverView closeAlertView:^{
        [weakSelf requestDeleteAddressDataWithID:idString];
    }];

}

- (void)requestDeleteAddressDataWithID:(NSString *)idString {
    //弹出密码输入框
    UIAlertController *pwdAlertVC=[UIAlertController alertControllerWithTitle:Localized(@"input_pay_pwd") message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conAction=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *pwdField=pwdAlertVC.textFields.firstObject;
        if (!pwdField.text.length) {
            [XSTool showToastWithView:self.view Text:Localized(@"input_pay_pwd")];
        }else{
             NSString *encryptPassWord = [RSAEncryptor encryptString:pwdField.text];
            
            __weak typeof(self) weakSelf = self;
            [JYSAFNetworking getOrPostWithType:POST withUrl:JYSWithdrawAddressDeleteURL params:@{@"id":idString,@"pay_password":encryptPassWord} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    [XSTool showToastWithView:weakSelf.view Text:Localized(@"del_success")];
                    [weakSelf.choseArrassView removeFromSuperview];
                    weakSelf.choseArrassView = nil;
            
//                    [weakSelf setUpAddAddress];
                    
                    [weakSelf requestData];
                } else {
                    [XSTool showToastWithView:weakSelf.view Text:state.info];
                    
                    [weakSelf performSelector:@selector(chooseAddressButtonClicked) withObject:nil afterDelay:1.2];
                }
            } fail:^(NSError *error) {
                
            } showHUD:YES];
        }
    }];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [pwdAlertVC addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.secureTextEntry = YES;
    }];
    [pwdAlertVC addAction:conAction];
    [pwdAlertVC addAction:cancelAction];
    [self presentViewController:pwdAlertVC animated:YES completion:nil];
}


- (void)jumpToAddAddressVC {
    JYSAddAddressViewController *vc = [JYSAddAddressViewController new];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSWithdrawURL params:@{@"currency":self.model.symbol} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            [self.addressArray removeAllObjects];
           
            
            self.withdrawModel = [JYSWithdrawModel mj_objectWithKeyValues:dic[@"data"]];
            self.withdrawModel.currency = self.model.symbol;
            self.withdrawModel.logo = self.model.logo;
            self.topView.wdModel = self.withdrawModel;
            
            //计算多行文字的size
            CGSize titleSize = [self.withdrawModel.tips boundingRectWithSize:CGSizeMake(FontNum(344), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;

            [self.topView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.top.right.mas_equalTo(self.view);
                make.height.mas_equalTo(FontNum(480)+titleSize.height+20);
            }];
            
            for (JYSCoinAddressModel *temp in self.withdrawModel.coin_address) {
//                [self.addressArray addObject:temp.address];
                [self.addressArray addObject:temp];
            }
            NSUInteger rowsNumber;
            if (self.addressArray.count > 5) {
                rowsNumber = 5;
            } else {
                rowsNumber = self.addressArray.count;
            }
            self.choseArrassView.frame = CGRectMake(0, 0, FontNum(320), FontNum(100)+FontNum(50)*rowsNumber);
            
            [self.choseArrassView setAddressDatasWithArray:self.addressArray];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } showHUD:YES];
}



//身份确认
-(void)confirmBtnCileck:(NSString *)codeText email_code:(NSString *)email_code google:(NSString *)google psw:(NSString *)psw
{
    
    NSString *mobile = @"";
    NSString *email = @"";
    BOOL isPay = YES;
    if (self.withdrawModel.show_mobile.intValue) {
        mobile = self.withdrawModel.mobile;
        isPay = NO;
    }
    if (self.withdrawModel.show_email.intValue) {
        email = self.withdrawModel.email;
        isPay = NO;
    }
    if (self.withdrawModel.show_google.intValue) {
        isPay = NO;
    }
    
    
    if (isEmptyString(codeText)&&self.withdrawModel.show_mobile.intValue) {
        Alert(@"请输入短信验证码");
        return;
    }
    if (isEmptyString(email_code)&&self.withdrawModel.show_email.intValue) {
        Alert(Localized(@"_send_dialog_sms_hint"));
        return;
    }
    if (isEmptyString(google)&&self.withdrawModel.show_google.intValue) {
        Alert(Localized(@"cc_sms_ingoogle_hint"));
        return;
    }
    if (isEmptyString(psw)&&isPay) {
        Alert(@"input_pay_pwd");
        return;
    }
    
    [self mentionMoneyRequestWithMobile_verify:codeText email_verify:email_code google_verify:google pay_verify:psw];
}





//验证码
-(void)codeBtnSelected:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (isEmptyString(self.withdrawModel.mobile)) {
        Alert(@"获取用户手机号失败");
        return;
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [XSTool showProgressHUDWithView:self.alertNewView];
    [HTTPManager getSmsVerifyWithDic:@{@"mobile":self.withdrawModel.mobile,@"type":@"9"}
                             success:^(NSDictionary *dic, resultObject *state) {
                                 [XSTool hideProgressHUDWithView:self.alertNewView];
                                 // 验证码发送成功
                                 if (state.status) {
                                     [XSTool showToastWithView:self.alertNewView Text:@"发送成功"];
                                     _isTimer = YES; // 设置倒计时状态为YES
                                     sender.enabled = NO; // 设置按钮为不可点击
                                     
                                     _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                                     [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                                         //设置按钮的样式
                                         dispatch_source_cancel(_timer);
                                         [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                                         sender.enabled = YES; // 设置按钮可点击
                                         
                                         _isTimer = NO; // 倒计时状态为NO
                                     } otherAction:^(int time) {
                                         [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
                                     }];
                                     
                                 } else {
                                     if (state.info.length>0) {
                                         [XSTool showToastWithView:self.alertNewView Text:state.info];
                                     }
                                     sender.enabled = YES; // 设置按钮为可点击
                                 }
                             } fail:^(NSError *error)
     {
         [XSTool showToastWithView:self.alertNewView Text:@"验证码发送失败"];
         sender.enabled = YES; // 设置按钮为可点击
         [XSTool hideProgressHUDWithView:self.alertNewView];
     }];
    
}

//邮箱验证码
-(void)emailBtnSelected:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (isEmptyString(self.withdrawModel.email)) {
        Alert(@"获取邮箱号失败");
        return;
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [XSTool showProgressHUDWithView:self.alertNewView];
    [HTTPManager getEmailVerifyWithDic:@{@"email":self.withdrawModel.email,@"type":@"4"}
                               success:^(NSDictionary *dic, resultObject *state){
                                   [XSTool hideProgressHUDWithView:self.alertNewView];
                                   // 验证码发送成功
                                   if (state.status) {
                                       [XSTool showToastWithView:self.alertNewView Text:@"发送成功"];
                                       _isTimer_email = YES; // 设置倒计时状态为YES
                                       sender.enabled = NO; // 设置按钮为不可点击
                                       
                                       _timer_email = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                                       [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer_email walltime:59 stop:^{
                                           //设置按钮的样式
                                           dispatch_source_cancel(_timer_email);
                                           [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                                           sender.enabled = YES; // 设置按钮可点击
                                           
                                           _isTimer_email = NO; // 倒计时状态为NO
                                       } otherAction:^(int time) {
                                           [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
                                       }];
                                       
                                   } else {
                                       if (state.info.length>0) {
                                           [XSTool showToastWithView:self.alertNewView Text:state.info];
                                       }
                                       sender.enabled = YES; // 设置按钮为可点击
                                   }
                               } fail:^(NSError *error)
     {
         [XSTool hideProgressHUDWithView:self.alertNewView];
         [XSTool showToastWithView:self.alertNewView Text:@"验证码发送失败"];
         sender.enabled = YES; // 设置按钮为可点击
     }];
}


- (void)mentionMoneyRequestWithMobile_verify:(NSString *)mobile_verify
               email_verify:(NSString *)email_verify
              google_verify:(NSString *)google_verify
                 pay_verify:(NSString *)pay_verify{
    
    NSDictionary *param = [@{
                             @"currency":self.model.symbol,
                             @"amount":self.up_amount,
                             @"address":self.up_address
                             }mutableCopy];
    
    if (!isEmptyString(email_verify) ) {
        [param setValue:email_verify forKey:@"email_verify"];
    }
    
    if (!isEmptyString(mobile_verify) ) {
        [param setValue:mobile_verify forKey:@"mobile_verify"];
    }
    
    if (!isEmptyString(google_verify) ) {
        [param setValue:google_verify forKey:@"google_verify"];
    }
    if (!isEmptyString(pay_verify) ) {
        //密码加密
        NSString *encryptPassWord = [RSAEncryptor encryptString:pay_verify];
        [param setValue:encryptPassWord forKey:@"pay_verify"];
    }
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSWithdrawSaveURL params:param HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            // 提币成功
            [XSTool showToastWithView:self.view Text:@"提币成功！"];
            
            JYSMentionMoneyHistoricalRecordVC *vc = [[JYSMentionMoneyHistoricalRecordVC alloc] init];
            vc.currency = self.model.symbol;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    } showHUD:YES];
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.topView hideAddressTableView];
//}




#pragma mark ---------扫一扫----------
- (void)QRCodeAction
{
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 6;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;

    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    
    //SubLBXScanViewController继承自LBXScanViewController
    //添加一些扫码或相册结果处理
    ScanViewController *scanVC = [[ScanViewController alloc] init];
    scanVC.style = style;
    scanVC.isCoinAdrress = YES;
    __weak __typeof__(self) wSelf = self;
    scanVC.qrBlock = ^(NSString *content) {
        if (!isEmptyString(content)) {
            wSelf.topView.addressLabel.text = content;
        }
    };
    scanVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:scanVC animated:YES];
}



@end
