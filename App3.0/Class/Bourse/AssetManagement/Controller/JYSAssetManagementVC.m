//
//  JYSAssetManagementVC.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAssetManagementVC.h"
#import "JYSAssetManagementCell.h"
#import "JYSAssetManagementHeaderView.h"
#import "JYSAssetManagementModel.h"
#import "JYSChargingMoneyViewController.h"//充币
#import "JYSMentionMoneyViewController.h"//提币
#import "JYSMentionMoneyHistoricalRecordVC.h"//历史记录
@interface JYSAssetManagementVC ()

/** 头部视图 */
@property (nonatomic, strong) JYSAssetManagementHeaderView * headerView;

/** 数据源数组 */
@property (nonatomic, strong) NSMutableArray * dataArray;

/** 资产大于0的数据数组 */
@property (nonatomic, strong) NSMutableArray * showArray;

@property (nonatomic, assign) BOOL isHiden;

@end

@implementation JYSAssetManagementVC

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (NSMutableArray *)showArray {
    if (_showArray == nil) {
        _showArray = [[NSMutableArray alloc] init];
    }
    return _showArray;
}

- (JYSAssetManagementHeaderView *)headerView {
    if (_headerView == nil) {
        _headerView = [[JYSAssetManagementHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(185));
    }
    return _headerView;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHiden = NO;
//    [self actionCustomLeftBtnWithNrlImage:@"jys_back" htlImage:nil title:nil action:^{
//        self.tabBarController.navigationController.navigationBar.translucent = YES;
//        [self.tabBarController.navigationController popViewControllerAnimated:YES];
//    }];
    
    [self setUpUI];
}

static NSString * const cellID = @"JYSAssetManagementCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSAssetManagementCell class] forCellReuseIdentifier:cellID];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JYSAssetManagementCell class]) bundle:nil] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    @weakify(self);
    self.headerView.hideAction = ^{
        @strongify(self);
        if (self.isHiden) {
            self.showArray = self.dataArray;
        } else {
            NSMutableArray *mArr = [NSMutableArray array];
            for (JYSAssetManagementModel *model in self.dataArray) {
                if (model.number.floatValue > 0) {
                    [mArr addObject:model];
                }
            }
            self.showArray = mArr;
        }
        self.isHiden = !self.isHiden;
        [self.tableView reloadData];
    };
}

- (void)requestData {
    __weak typeof(self) weakSelf = self;
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSAssetManagementURL params:nil HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            NSArray * listArr = dataDict[@"list"];
            [weakSelf.headerView setValuationData:dataDict[@"sum"]];
            if ([listArr isKindOfClass:[NSArray class]]) {
                
                [weakSelf.dataArray removeAllObjects];
                
                NSArray * datas = [JYSAssetManagementModel mj_objectArrayWithKeyValuesArray:listArr];
                [weakSelf.dataArray addObjectsFromArray:datas];
                weakSelf.showArray = weakSelf.dataArray;
                [weakSelf.tableView reloadData];
            }
        } else {
            [XSTool showToastWithView:weakSelf.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSAssetManagementCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    if (self.showArray.count) {
        JYSAssetManagementModel * model = self.showArray[indexPath.row];
        [cell setAssetDataWithModel:model];
    }
    
    __weak typeof(self) weakSelf = self;
    //充币
    cell.chargeMoneyBlock = ^{
        JYSAssetManagementModel *model = self.showArray[indexPath.row];
        if ([model.exchange_status integerValue] == 0 || [model.exchange_status integerValue] == 2) {
            [XSTool showToastWithView:self.view Text:@"此币种充币暂不可用"];
            return;
        }
        JYSChargingMoneyViewController * chargingMoneyVC = [[JYSChargingMoneyViewController alloc] init];
        [chargingMoneyVC setChargingMoneyWithModel:self.showArray[indexPath.row]];
        [weakSelf.navigationController pushViewController:chargingMoneyVC animated:YES];
    };
    
    //提币
    cell.mentionMoneyBlock = ^{
        JYSAssetManagementModel *model = self.showArray[indexPath.row];
        if ([model.exchange_status integerValue] == 0 || [model.exchange_status integerValue] == 1) {
            [XSTool showToastWithView:self.view Text:@"此币种提币暂不可用"];
            return;
        }
        JYSMentionMoneyViewController * mentionMoneyVC = [[JYSMentionMoneyViewController alloc] init];
        [mentionMoneyVC setMentionMoneyWithModel:self.showArray[indexPath.row]];
        [weakSelf.navigationController pushViewController:mentionMoneyVC animated:YES];
    };
    
    //记录
    cell.recordBlock = ^{
        JYSMentionMoneyHistoricalRecordVC * mentionHistoricalVC = [[JYSMentionMoneyHistoricalRecordVC alloc] init];
        JYSAssetManagementModel *model = self.showArray[indexPath.row];
        mentionHistoricalVC.currency = model.symbol;
        [weakSelf.navigationController pushViewController:mentionHistoricalVC animated:YES];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(131);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FontNum(185);
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end
