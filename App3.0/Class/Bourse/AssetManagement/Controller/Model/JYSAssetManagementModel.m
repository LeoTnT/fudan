//
//  JYSAssetManagementModel.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAssetManagementModel.h"

@implementation JYSCoinRecordItem
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end

@implementation JYSCoinAddressModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID":@"id"
             };
}
@end

@implementation JYSWithdrawModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"coin_address":@"JYSCoinAddressModel"};
}
@end

@implementation JYSChargingModel
@end

@implementation JYSAssetManagementModel

+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
      @"ID":@"id"
      };
}

@end

@implementation JYSVerticalWithdrawModel
@end

