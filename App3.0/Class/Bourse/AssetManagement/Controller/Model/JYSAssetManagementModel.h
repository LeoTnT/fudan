//
//  JYSAssetManagementModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JYSCoinRecordItem : NSObject
/** id */
@property (nonatomic, copy) NSString * ID;
/**  */
@property (nonatomic, copy) NSString * user_id;
/**  */
@property (nonatomic, copy) NSString * user_name;
/**  */
@property (nonatomic, copy) NSString * currency_id;
/** 提币名称 */
@property (nonatomic, copy) NSString * currency_name;
/** 输入数量 */
@property (nonatomic, copy) NSString * amount;
/** 20180525新增，提币实际进入链的数量 */
@property (nonatomic, copy) NSString * real_amount;
/** 手续费 */
@property (nonatomic, copy) NSString * poundage;
/** 钱包地址 */
@property (nonatomic, copy) NSString * destination;
/**  */
@property (nonatomic, copy) NSString * w_time;
/** '0:充值，1:提币' */
@property (nonatomic, assign) unsigned int type;
/** 1:txhash, 2:提币成功, 3,提币已申请,4,提币已审核5,提币已拒绝10:充值成功(3,4,1,2,5)为提币状态，10为充币状态 */

/*
 1 => '进入区块',
2 => '提币成功',
3 => '提币已申请',
4 => '提币已审核',
5 => '提币已拒绝',
6 => '提币未知状态',
7 => '提币自动审核',
10 => '充值成功',
11 => '充值小于最小到账数量'
*/
@property (nonatomic, assign) unsigned int status;
@end

@interface JYSCoinAddressModel : NSObject
/**  */
@property (nonatomic, copy) NSString * ID;
/**  */
@property (nonatomic, copy) NSString * coin_id;
/**  */
@property (nonatomic, copy) NSString * address;
/**  */
@property (nonatomic, copy) NSString * remark;
@end

@interface JYSWithdrawModel : NSObject
/** 手续费币种 */
@property (nonatomic, copy) NSString * fee_currency;
/** 币种余额 */
@property (nonatomic, copy) NSString * remain;
/** 今日已申请提币数量 */
@property (nonatomic, copy) NSString * withdraw_day;
/** 提币手续费1:百分比，2:固定值 */
@property (nonatomic, assign) unsigned int fee_type;
/** 提币具体设置 */
@property (nonatomic, copy) NSString * fee_set;
/** 提币最小数量 */
@property (nonatomic, copy) NSString * min;
/** 提币最大数量 */
@property (nonatomic, copy) NSString * max;
/** 日提币最大数量 */
@property (nonatomic, copy) NSString * max_day;
/** 该币种已添加的钱包地址 */
@property (nonatomic, strong) NSArray * coin_address;

/** 币种 */
@property (nonatomic, copy) NSString * currency;
@property (nonatomic, copy) NSString * logo;

/** 开启短信验证 */
@property (nonatomic,copy) NSString *show_mobile;
/** 开启google验证 */
@property (nonatomic,copy) NSString *show_google;
/** 开启email验证 */
@property (nonatomic,copy) NSString *show_email;

@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *tips;

@end

@interface JYSChargingModel : NSObject
/** 钱包地址 */
@property (nonatomic, copy) NSString * coinAddress;
/** 钱包地址二维码 */
@property (nonatomic, copy) NSString * coinAddressImg;
/** 币种 */
@property (nonatomic, copy) NSString * currency;
/** 最小充币到账数量 0无限制 */
@property (nonatomic, copy) NSString * reserveMin;
/** 充币TAG */
@property (nonatomic, copy) NSString * tag;
@property (nonatomic, copy) NSString * tips;
@end

@interface JYSAssetManagementModel : NSObject

/** id */
@property (nonatomic, copy) NSString * ID;
/** OMG */
@property (nonatomic, copy) NSString * symbol;
/** id */
@property (nonatomic, copy) NSString * is_address;
/** "0":普通模式，"1":钱包地址+tag */
@property (nonatomic, assign) unsigned int address_type;
/** 0:不能冲币,不能提币1:只冲币2:只提币3:冲币提币 */
@property (nonatomic, copy) NSString * exchange_status;
/** logo */
@property (nonatomic, copy) NSString * logo;

/** 锁定数量 */
@property (nonatomic, copy) NSString * locked;
/** 余额 */
@property (nonatomic, copy) NSString * number;
/** 最新成交价，“”:没有对应的市场 */
@property (nonatomic, copy) NSString * close;
/** 待激活 */
@property (nonatomic, copy) NSString * inactive;


@end


@interface JYSVerticalWithdrawModel : NSObject
/** 开启短信验证 */
@property (nonatomic,copy) NSString *show_mobile;
/** 开启google验证 */
@property (nonatomic,copy) NSString *show_google;
/** 开启邮箱验证 */
@property (nonatomic,copy) NSString *show_email;

/** 是否绑定过邮箱 */
@property (nonatomic,copy) NSString *is_email_verify;
/**是否绑定过手机号 */
@property (nonatomic,copy) NSString *is_mobile_verify;
/**是否绑定过google */
@property (nonatomic,copy) NSString *is_google_verify;


@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *country_code;

@end

/*
 "show_mobile": 0,
 "show_google": 0,
 "show_email": 0,
 "is_email_verify": 0,
 "is_mobile_verify": 1,
 "is_google_verify": 1
 */

