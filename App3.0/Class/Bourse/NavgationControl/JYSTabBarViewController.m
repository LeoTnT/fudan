//
//  JYSTabBarViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSTabBarViewController.h"
#import "JYSNavigationViewController.h"
#import "TabMallVC.h"
#import "JYSCurrencyQuotesViewController.h"//行情
#import "JYSCurrencyTradingViewController.h"//币币交易
#import "BaseTransController.h"//法币交易
#import "WalletViewController.h"//资产管理
#import "JYSAssetManagementVC.h"//资产管理
#import "JYSMineViewController.h"//我的
#import "AppDelegate.h"

@interface JYSTabBarViewController ()

@end

@implementation JYSTabBarViewController

+(void)initialize {
    UITabBarItem * item = [UITabBarItem appearance];
    
    NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    
    //设置选中状态下的显示
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = XSYCOLOR(0x5888ED);
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    JYSCurrencyQuotesViewController * marketVC = [[JYSCurrencyQuotesViewController alloc] init];
    JYSCurrencyTradingViewController * tradingVC1 = [[JYSCurrencyTradingViewController alloc] init];
    BaseTransController * tradingVC2 = [[BaseTransController alloc]init];
//    WalletViewController * walletVC = [[WalletViewController alloc]init];
    JYSAssetManagementVC * assetManagementVC = [[JYSAssetManagementVC alloc] init];
    
    JYSTabUserVC * mineVC = [[JYSTabUserVC alloc]init];
    [BaseTool getCountryList];
    [self setupChildVc:marketVC title:Localized(@"quotation") image:@"jys_tabBar_market" selectedImage:@"jys_tabBar_market_select"];
    [self setupChildVc:tradingVC1 title:Localized(@"coincoin") image:@"jys_tabBar_coinTrading" selectedImage:@"jys_tabBar_coinTrading_select"];
    [self setupChildVc:tradingVC2 title:Localized(@"frenchcurrency") image:@"jys_tabBar_fbTrading" selectedImage:@"jys_tabBar_fbTrading_select"];
    [self setupChildVc:assetManagementVC title:Localized(@"assetmanagement") image:@"jys_tabBar_asset" selectedImage:@"jys_tabBar_asset_select"];
    [self setupChildVc:mineVC title:Localized(@"my") image:@"jys_tabBar_mine" selectedImage:@"jys_tabBar_mine_select"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [mineVC getMessageCount];
    });
}

/**
 * 初始化子控制器
 */
- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    // 设置文字和图片
    vc.navigationItem.title = title;
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    //    vc.view.backgroundColor = MTBGViewColor;
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.automaticallyAdjustsScrollViewInsets = NO;
    // 包装成自定义的导航控制器, 添加导航控制器为tabbarcontroller的子控制器
    JYSNavigationViewController *nav = [[JYSNavigationViewController alloc] initWithRootViewController:vc];
    
    [self addChildViewController:nav];
}

- (void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end
