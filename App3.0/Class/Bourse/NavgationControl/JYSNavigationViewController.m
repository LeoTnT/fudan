//
//  JYSNavigationViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSNavigationViewController.h"

@interface JYSNavigationViewController ()<UINavigationControllerDelegate>

@end

@implementation JYSNavigationViewController

+(void)initialize {
    UINavigationBar * bar = [UINavigationBar appearance];
    
    NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = XSYCOLOR(0x333333);
//    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(18)];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
    [bar setTitleTextAttributes:attrs];
    
    [bar setShadowImage:nil];
    bar.translucent = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.edgesForExtendedLayout = UIRectEdgeNone;
    
    XSLog(@"viewController =%@",viewController);
    if (self.childViewControllers.count > 0) {
        UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        [backBtn setTitle:@"返回" forState:UIControlStateNormal];
        [backBtn setImage:[[UIImage imageNamed:@"jys_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        
        backBtn.xs_size = CGSizeMake(FontNum(30), FontNum(30));
        // 让按钮内部的所有内容左对齐
        backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        // 让按钮的内容往左边偏移10
//        backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -1`0, 0, 0);
        [backBtn setTitleColor:JYSMainTextColor forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        // 修改导航栏左边的item   注意:导航栏进行自定义后 系统自带的手势就会失效
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        // 当进行push控制器操作的时候隐藏底部tabbar
        viewController.hidesBottomBarWhenPushed = YES;
    }
    // 这句super的push要放在后面, 让viewController可以覆盖上面设置的leftBarButtonItem
    [super pushViewController:viewController animated:animated];
}

- (void)back
{
    [self popViewControllerAnimated:YES];
}

- (void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end
