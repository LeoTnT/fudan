//
//  JYSMineCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMineCell.h"

@interface JYSMineCell ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation JYSMineCell

- (void)setMineDataWithDict:(NSDictionary *)mineDict {
    self.logoImageView.image = [UIImage imageNamed:mineDict[@"image"]];
    self.titleLabel.text = mineDict[@"title"];
}

@end
