//
//  JYSRealNameVerifyHeaderView.h
//  App3.0
//
//  Created by mac on 2018/6/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYSRealNameVerificationModel;

@protocol JYSRealNameVerificationDelegate <NSObject>

-(void)choosePaperworkTypeBtnClicked;

-(void)frontImageBtnClicked;

-(void)behindImageBtnClicked;

- (void)handleImageBtnClicked;

-(void)upLoadDataWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum;

@end

@interface JYSRealNameVerifyHeaderView : UIView

/** 键盘是否显示 */
@property (nonatomic, assign) BOOL isKeyboardShow;
/** delegate */
@property (nonatomic, weak) id<JYSRealNameVerificationDelegate>delegate;
/**设置当前审核状态界面显示*/
-(void)setStatus:(JYSRealNameVerificationModel *)model;

/**设置身份证照*/
-(void)setPaperworkText:(NSString *)paperworkText;
/**设置身份证照*/
-(void)setFrontImage:(UIImage *)frontImage;
-(void)setBehindImage:(UIImage *)behindImage;
-(void)setHandleImage:(UIImage *)handleImage;
/**是否隐藏底部按钮*/
-(void)setFooterViewHide:(BOOL)footerHide;

@end
