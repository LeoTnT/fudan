//
//  InputAlertNewTextFieldView.h
//  App3.0
//
//  Created by xinshang on 2018/6/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol InputAlertNewTextFieldViewDelegate <NSObject>
@optional
-(void)codeBtnSelected:(UIButton *)sender;
-(void)emailBtnSelected:(UIButton *)sender;

-(void)confirmBtnCileck:(NSString *)codeText email_code:(NSString *)email_code google:(NSString *)google psw:(NSString *)psw;


-(void)cancelBtnCileck;
@end

@interface InputAlertNewTextFieldView : UIView

@property(nonatomic,weak)id<InputAlertNewTextFieldViewDelegate> delegate;
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UILabel *phoneLb;


- (instancetype)initWithFrame:(CGRect)frame
                       mobile:(NSString *)mobile
                        email:(NSString *)email
                     isGoogle:(BOOL)isGoogle
                     isPayPSW:(BOOL)isPayPSW;
@end
