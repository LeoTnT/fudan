//
//  InputAlertNewTextFieldView.m
//  App3.0
//
//  Created by xinshang on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "InputAlertNewTextFieldView.h"

/** 屏幕宽高 */
#define kScreenBounds [UIScreen mainScreen].bounds
#define KScreenWidth [[UIScreen mainScreen]bounds].size.width
#define KScreenHeight [[UIScreen mainScreen]bounds].size.height

//RGB
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]


#define space 10.0

#define lbWidth bgWidth- space*2
#define lbHeight 44.0

@interface InputAlertNewTextFieldView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, assign) CGFloat bgHeight;

@property (nonatomic, strong) UIButton *completionBtn;
@property (nonatomic, strong) UIView* line1;
@property (nonatomic, strong) UIView* line2;

//@property (nonatomic, strong) UIView *codeView;


@property (nonatomic, strong) UIView *mobileView;
@property (nonatomic, strong) UIView *googleView;
@property (nonatomic, strong) UIView *emailView;
@property (nonatomic, strong) UIView *payView;

@property (nonatomic, strong) UILabel *codeLb;
@property (nonatomic, strong) UITextField *codeTF;
@property (nonatomic, strong) UIButton *codeBtn;


@property (nonatomic, strong) UILabel *google_codeLb;
@property (nonatomic, strong) UITextField *google_codeTF;



@property (nonatomic, strong) UILabel *payPSWLb;
@property (nonatomic, strong) UITextField *payPSWTF;

@property (nonatomic, strong) UILabel *googleLb;
@property (nonatomic, strong) UITextField *googleTF;

@property (nonatomic, strong) UILabel *email_numLb;
@property (nonatomic, strong) UILabel *email_codeLb;
@property (nonatomic, strong) UITextField *email_codeTF;
@property (nonatomic, strong) UIButton *email_codeBtn;


@end

@implementation InputAlertNewTextFieldView




- (instancetype)initWithFrame:(CGRect)frame
                       mobile:(NSString *)mobile
                        email:(NSString *)email
                    isGoogle:(BOOL)isGoogle
                     isPayPSW:(BOOL)isPayPSW

{
    
    if (self == [super initWithFrame:frame]) {

        
        
        self.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
        self.backgroundColor = RGBA(51, 51, 51, 0.3);
        
      
       self.bgHeight = 100.0f;
        CGFloat mobileH = 0.0;
        CGFloat googleH = 0.0;
        CGFloat payH = 0.0;
        CGFloat eamilH = 0.0;

        if (!isEmptyString(mobile)) {
            mobileH = lbHeight + space;
        }
        if (!isEmptyString(email)) {
            eamilH = lbHeight + space;
        }
        if (isGoogle) {
            googleH = lbHeight/2+space;
        }
        
        if (isPayPSW) {
            payH = lbHeight/2+space;
        }
        self.bgHeight = self.bgHeight + eamilH + mobileH +googleH +payH;

        
        CGFloat bgWidth  = KScreenHeight <= 568.0 ?KScreenWidth*0.85:KScreenWidth*0.75;

        
        //背景视图
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake((KScreenWidth-bgWidth)/2, KScreenHeight, bgWidth, self.bgHeight)];
        self.bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bgView];
        self.bgView.layer.masksToBounds = YES;
        self.bgView.layer.cornerRadius = 6;
        
        //titleLb
        self.titleLb = [[UILabel alloc] init];
        self.titleLb.textAlignment = NSTextAlignmentCenter;
        [self.bgView addSubview:self.titleLb];
        self.titleLb.text = Localized(@"身份验证");
        self.titleLb.font = [UIFont boldSystemFontOfSize:16];
        self.titleLb.textColor = [UIColor blackColor];
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(bgWidth);
            make.height.mas_equalTo(lbHeight);
        }];
        
        //显示动画
        [self showAnimation];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
        
        //短信view
        self.mobileView = [[UIView alloc] init];
        self.bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.mobileView];
        [self.mobileView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLb.mas_bottom);
            make.right.mas_equalTo(self.bgView).offset(-space*0.8);
            make.left.mas_equalTo(self.bgView).offset(space*0.8);
            make.height.mas_equalTo(lbHeight+space);
        }];
        
        self.phoneLb = [[UILabel alloc] init];
        [self.mobileView addSubview:self.phoneLb];
        self.phoneLb.text = [NSString stringWithFormat:@"%@:%@",Localized(@"code_tip"),mobile];
        self.phoneLb.font = [UIFont systemFontOfSize:13.5];
        self.phoneLb.textColor = [UIColor blackColor];
        [self.phoneLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mobileView.mas_top);
            make.left.mas_equalTo(self.mobileView);
            make.right.mas_equalTo(self.mobileView);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        self.codeLb = [[UILabel alloc] init];
        [self.mobileView addSubview:self.codeLb];
        self.codeLb.text = Localized(Localized(@"验证码:"));
        self.codeLb.font = [UIFont systemFontOfSize:13.5];
        self.codeLb.textColor = [UIColor blackColor];
        [self.codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mobileView).offset(lbHeight/2+space/2);
            make.left.mas_equalTo(self.phoneLb);
            make.width.mas_equalTo(55);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        //codeBtn
        self.codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.mobileView addSubview:self.codeBtn];
        [self.codeBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        self.codeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.codeBtn setTitle:Localized(@"sms_send_dialog_get") forState:UIControlStateNormal];
        [self.codeBtn setTitleColor:RGBA(63, 142, 247, 1) forState:UIControlStateNormal];
        [self.codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.codeLb);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        
        //codeTF
        self.codeTF = [[UITextField alloc] init];
        [self.mobileView addSubview:self.codeTF];
        self.codeTF.placeholder = Localized(@"请输入短信验证码");
        [self.codeTF setValue:[UIFont boldSystemFontOfSize:10] forKeyPath:@"_placeholderLabel.font"];
        self.codeTF.font = [UIFont systemFontOfSize:14];
        self.codeTF.textColor = [UIColor blackColor];
        
        [self.codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.codeLb);
            make.left.mas_equalTo(self.codeLb.mas_right).offset(0);
            make.width.mas_equalTo(lbWidth-80-50);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        if (isEmptyString(mobile)) {
            [self.mobileView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.titleLb.mas_bottom).offset(0);
                make.right.mas_equalTo(-space);
                make.left.mas_equalTo(space);
                make.height.mas_equalTo(0.1);
            }];
            
            self.codeTF.text = @"";
            self.codeTF.placeholder = @"";
            self.codeLb.text = @"";
            self.phoneLb.text = @"";

            self.phoneLb.hidden = YES;
            self.codeTF.hidden = YES;
            self.codeLb.hidden = YES;
            self.codeBtn.hidden = YES;

        }
        
        
        
        
        //邮箱view
        self.emailView = [[UIView alloc] init];
        self.emailView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.emailView];
        [self.emailView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mobileView.mas_bottom);
            make.right.mas_equalTo(self.bgView).offset(-space*0.8);
            make.left.mas_equalTo(self.bgView).offset(space*0.8);
            make.height.mas_equalTo(lbHeight+space);
        }];
        
        self.email_numLb = [[UILabel alloc] init];
        [self.emailView addSubview:self.email_numLb];
        self.email_numLb.text = [NSString stringWithFormat:@"%@:%@",Localized(@"邮箱验证码将发送至"),email];
        self.email_numLb.font = [UIFont systemFontOfSize:13.5];
        self.email_numLb.textColor = [UIColor blackColor];
        [self.email_numLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.emailView.mas_top);
            make.left.mas_equalTo(self.emailView);
            make.right.mas_equalTo(self.emailView);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        self.email_codeLb = [[UILabel alloc] init];
        [self.emailView addSubview:self.email_codeLb];
        self.email_codeLb.text = Localized(Localized(@"验证码:"));
        self.email_codeLb.font = [UIFont systemFontOfSize:13.5];
        self.email_codeLb.textColor = [UIColor blackColor];
        [self.email_codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.emailView).offset(lbHeight/2+space/2);
            make.left.mas_equalTo(self.email_numLb);
            make.width.mas_equalTo(55);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        //email_codeBtn
        self.email_codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.emailView addSubview:self.email_codeBtn];
        [self.email_codeBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        self.email_codeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.email_codeBtn setTitle:Localized(@"sms_send_dialog_get") forState:UIControlStateNormal];
        [self.email_codeBtn setTitleColor:RGBA(63, 142, 247, 1) forState:UIControlStateNormal];
        [self.email_codeBtn addTarget:self action:@selector(emailBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.email_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.email_codeLb);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        
        //email_codeTF
        self.email_codeTF = [[UITextField alloc] init];
        [self.emailView addSubview:self.email_codeTF];
        self.email_codeTF.placeholder = Localized(@"_send_dialog_sms_hint");
        [self.email_codeTF setValue:[UIFont boldSystemFontOfSize:10] forKeyPath:@"_placeholderLabel.font"];
        self.email_codeTF.font = [UIFont systemFontOfSize:14];
        self.email_codeTF.textColor = [UIColor blackColor];
        
        [self.email_codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.email_codeLb);
            make.left.mas_equalTo(self.email_codeLb.mas_right).offset(0);
            make.width.mas_equalTo(lbWidth-80-50);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        if (isEmptyString(email)) {
            [self.emailView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.mobileView.mas_bottom).offset(0);
                make.right.mas_equalTo(-space);
                make.left.mas_equalTo(space);
                make.height.mas_equalTo(0.1);
            }];
            
            self.email_codeTF.text = @"";
            self.email_codeTF.placeholder = @"";
            self.email_codeLb.text = @"";
            self.email_numLb.text = @"";
            
            self.email_numLb.hidden = YES;
            self.email_codeTF.hidden = YES;
            self.email_codeLb.hidden = YES;
            self.email_codeBtn.hidden = YES;
        }
        
        
        
        
        //googleView
        self.googleView = [[UIView alloc] init];
        [self.bgView addSubview:self.googleView];
        [self.googleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.emailView.mas_bottom);
            make.right.mas_equalTo(self.bgView).offset(-space*0.8);
            make.left.mas_equalTo(self.bgView).offset(space*0.8);
            make.height.mas_equalTo(lbHeight/2+space);
        }];
        
        
        //googleLb
        self.googleLb = [[UILabel alloc] init];
        [self.googleView addSubview:self.googleLb];
        self.googleLb.text = Localized(@"验证码:");
        self.googleLb.font = [UIFont systemFontOfSize:13.5];
        self.googleLb.textColor = [UIColor blackColor];
        [self.googleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.googleView);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(55);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        self.googleTF = [[UITextField alloc] init];
//        self.googleTF.secureTextEntry = YES;
        [self.googleView addSubview:self.googleTF];
        self.googleTF.placeholder = Localized(@"cc_sms_ingoogle_hint");
        [self.googleTF setValue:[UIFont boldSystemFontOfSize:13.5] forKeyPath:@"_placeholderLabel.font"];
        self.googleTF.font = [UIFont systemFontOfSize:15];
        self.googleTF.textColor = [UIColor blackColor];
        [self.googleTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_googleLb.mas_top);
            make.left.mas_equalTo(_googleLb.mas_right).offset(3);
            make.width.mas_equalTo(lbWidth-100);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        
        if (!isGoogle) {
            [self.googleView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.emailView.mas_bottom);
                make.right.mas_equalTo(-space);
                make.left.mas_equalTo(space);
                make.height.mas_equalTo(0.1);
            }];
            self.googleLb.text = @"";
            self.googleTF.placeholder = @"";
            self.googleLb.hidden = YES;
            self.googleTF.hidden = YES;
        }
        
        
        
        
        //payView
        self.payView = [[UIView alloc] init];
        [self.bgView addSubview:self.payView];
        [self.payView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.googleView.mas_bottom);
            make.right.mas_equalTo(self.bgView).offset(-space*0.8);
            make.left.mas_equalTo(self.bgView).offset(space*0.8);
            make.height.mas_equalTo(lbHeight/2+space);
        }];
        
        
        //payLb
        self.payPSWLb = [[UILabel alloc] init];
        [self.payView addSubview:self.payPSWLb];
        self.payPSWLb.text = Localized(@"支付密码:");
        self.payPSWLb.font = [UIFont systemFontOfSize:13.5];
        self.payPSWLb.textColor = [UIColor blackColor];
        [self.payPSWLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.payView);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        
        self.payPSWTF = [[UITextField alloc] init];
        self.payPSWTF.secureTextEntry = YES;
        [self.payView addSubview:self.payPSWTF];
        self.payPSWTF.placeholder = Localized(@"input_pay_pwd");
        [self.payPSWTF setValue:[UIFont boldSystemFontOfSize:13.5] forKeyPath:@"_placeholderLabel.font"];
        
        self.payPSWTF.font = [UIFont systemFontOfSize:15];
        self.payPSWTF.textColor = [UIColor blackColor];
        [self.payPSWTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_payPSWLb.mas_top);
            make.left.mas_equalTo(_payPSWLb.mas_right).offset(3);
            make.width.mas_equalTo(lbWidth-100);
            make.height.mas_equalTo(lbHeight/2);
        }];
        
        
        
        if (!isPayPSW) {
            [self.payView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.googleView.mas_bottom);
                make.right.mas_equalTo(-space);
                make.left.mas_equalTo(space);
                make.height.mas_equalTo(0.1);
            }];
            self.payPSWLb.text = @"";
            self.payPSWTF.placeholder = @"";
            self.payPSWLb.hidden = YES;
            self.payPSWTF.hidden = YES;
        }
        
        
        
        
        self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.bgView addSubview:self.cancelBtn];
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(bgWidth/2);
            make.height.mas_equalTo(44);
        }];
        
        self.cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.cancelBtn setTitle:Localized(@"cancel_btn") forState:UIControlStateNormal];
        [self.cancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
        [self.cancelBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        
        self.completionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.bgView addSubview:self.completionBtn];
        [self.completionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(bgWidth/2);
            make.height.mas_equalTo(44);
        }];
        
        
        
        
        self.completionBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.completionBtn setTitle:Localized(@"material_dialog_default_title") forState:UIControlStateNormal];
        [self.completionBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
        [self.completionBtn setTitleColor:mainColor forState:UIControlStateNormal];
        [self.completionBtn addTarget:self action:@selector(completionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        //线1
        _line1 = [UIView new];
        [self.bgView addSubview:_line1];
        [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(-50);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(KScreenWidth);
            make.height.mas_equalTo(0.5);
            
        }];
        _line1.backgroundColor = RGBA(224, 224, 224, 1);
        
        
        //线2
        _line2 = [UIView new];
        [self.bgView addSubview:_line2];
        [_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(bgWidth/2-0.5);
            make.width.mas_equalTo(0.5);
            make.height.mas_equalTo(50);
            
        }];
        _line2.backgroundColor = RGBA(224, 224, 224, 1);
        
        NSString *titleStr = Localized(@"身份验证");
        if (!isEmptyString(mobile)&&isEmptyString(email)&&!isGoogle&&!isPayPSW) {//短信认证
            titleStr = Localized(@"security_sms_open");
        }else if (isEmptyString(mobile)&&!isEmptyString(email)&&!isGoogle&&!isPayPSW) {//邮箱认证
            titleStr = Localized(@"security_email_open");
        }else if (isEmptyString(mobile)&&isEmptyString(email)&&isGoogle&&!isPayPSW) {//谷歌认证
            titleStr = Localized(@"security_google_open");
        }else if (isEmptyString(mobile)&&isEmptyString(email)&&!isGoogle&&isPayPSW) {//支付验证
            titleStr = Localized(@"支付验证");
        }
        self.titleLb.text = titleStr;
        });

    }
    return self;
    
}


//- (NSString *)getTitle{
//
//
//}
#pragma mark-----获取
- (void)codeBtnClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(codeBtnSelected:)]) {
        [self.delegate codeBtnSelected:sender];
    }
}
//邮箱
- (void)emailBtnClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(emailBtnSelected:)]) {
        [self.delegate emailBtnSelected:sender];
    }
}

#pragma mark-----取消
- (void)cancelBtnClick{
    [self hideAnimation];
    if ([self.delegate respondsToSelector:@selector(cancelBtnCileck)]) {
        [self.delegate cancelBtnCileck];
    }
    
}

#pragma mark-----完成
- (void)completionBtnClick:(UIButton *)sender{
    
        [self hideAnimation];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(confirmBtnCileck: email_code: google: psw:)]) {
        [self.delegate confirmBtnCileck:self.codeTF.text email_code:self.email_codeTF.text google:self.googleTF.text psw:self.payPSWTF.text];
    }
    
//    if (self.isGoogle) {
//        if (_delegate && [_delegate respondsToSelector:@selector(confirmBtnCileck:)]) {
//            [self.delegate confirmBtnCileck:self.codeTF.text];
//        }
//
//    }else{
//        if (_delegate && [_delegate respondsToSelector:@selector(confirmBtnCileck: psw:)]) {
//            [self.delegate confirmBtnCileck:self.codeTF.text psw:self.payPSWTF.text];
//        }
//    }
}

#pragma mark-----隐藏的动画
- (void)hideAnimation{
    self.bgView.hidden = YES;
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.bgView.frame;
        frame.origin.y = KScreenHeight;
        self.bgView.frame = frame;
    } completion:^(BOOL finished) {
        
        [self.bgView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

#pragma mark-----显示的动画
- (void)showAnimation{
    
    self.bgView.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{

        CGRect frame = self.bgView.frame;
        frame.origin.y = (KScreenHeight-self.bgHeight)/2;
        self.bgView.frame = frame;
    }];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self endEditing:YES];
}
@end


