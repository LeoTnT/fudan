//
//  JYSRealNameVerificationModel.h
//  App3.0
//
//  Created by mac on 2018/6/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPManager+Mine.h"

@interface JYSRealNameVerificationModel : NSObject

/** 身份证正反面 */
@property (nonatomic, copy) NSArray * img_card;

/** 用户编号 */
@property (nonatomic, copy) NSString * username;

/** 认证状态(-1未提交|0审核中|1通过|2拒绝) */
@property (nonatomic, copy) NSString * status;

/** 认证评论 */
@property (nonatomic, copy) NSString * comment;

/** 用户id */
@property (nonatomic, copy) NSString * user_id;

/** 姓名 */
@property (nonatomic, copy) NSString * truename;

/** 身份证号 */
@property (nonatomic, copy) NSString * card_no;

/** 营业执照(商家认证才有) */
@property (nonatomic, copy) NSString * img_license;

/** 组织结构代码证(商家认证才有) */
@property (nonatomic, copy) NSString * img_zuzhi;

/** 证件类型数组  */
@property (nonatomic, copy) NSArray * card_types;

/** 证件名  */
@property (nonatomic, copy) NSString * card_name;

/** 身份证图片正面 */
@property (nonatomic, copy) NSString * img_card_a;
/** 身份证图片背面 */
@property (nonatomic, copy) NSString * img_card_b;
/** 身份证图片手持 */
@property (nonatomic, copy) NSString * img_card_c;

@end

@interface RealNameVerCardTypeModel : NSObject

/** 组织结构代码证(商家认证才有) */
@property (nonatomic, copy) NSString * card_id;

/** 证件名  */
@property (nonatomic, copy) NSString * card_name;


@end
