//
//  JYSRealNameVerificationModel.m
//  App3.0
//
//  Created by mac on 2018/6/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSRealNameVerificationModel.h"

@implementation JYSRealNameVerificationModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"card_types":@"RealNameVerCardTypeModel"
             };
}
@end

@implementation RealNameVerCardTypeModel

@end
