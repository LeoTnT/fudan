//
//  JYSMineModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSMineModel : NSObject

/** 用户id */
@property (nonatomic, copy) NSString * userid;

/** 用户真实名称 */
@property (nonatomic, copy) NSString * truename;

/** 用户头像地址 */
@property (nonatomic, copy) NSString * logo;

/** 交易数 */
@property (nonatomic, copy) NSString * trade_count;

/** 好评率 */
@property (nonatomic, copy) NSString * praise_rate;

/** 信任 */
@property (nonatomic, copy) NSString * trust_me_count;

/** 用户名 */
@property (nonatomic, copy) NSString * username;

/**  */
@property (nonatomic, copy) NSString * approve_user;

/**  */
@property (nonatomic, copy) NSString * register_time;

@end
