//
//  JYSMineViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMineViewController.h"
#import "JYSMineCell.h"
#import "MyOrderController.h"
#import "JYSC2COrderViewController.h"//C2C订单
#import "JYSEntrustOrdersViewController.h"//委托订单
#import "BaseMyAdverController.h"
#import "JYSMyAdvertisingViewController.h"//我的广告
#import "MerchantApplicationController.h"//商家申请
#import "BindingViewController.h"//支付方式
#import "JYSMineModel.h"
#import "UIButton+XSWebImage.h"

@interface JYSMineViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *headPortraitButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UITableView *mineTableView;

/** mineArray */
@property (nonatomic, strong) NSArray * mineArray;

@end

@implementation JYSMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self actionCustomLeftBtnWithNrlImage:@"jys_back" htlImage:nil title:nil action:^{
//        self.tabBarController.navigationController.navigationBar.translucent = YES;
//        [self.tabBarController.navigationController popViewControllerAnimated:YES];
//    }];
    
    self.mineArray = @[@{@"image":@"jys_mine_IDCard_c2cOrder",@"title":Localized(@"c2corder")},@{@"image":@"jys_mine_myAD",@"title":Localized(@"my_advert")},@{@"image":@"jys_mine_zhiFuFangShi",@"title":Localized(@"recharge_wallet_pay_select")}];
    
    [self setUpUI];
    
    [self requestData];
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:@"/api/v1/cuser/cuser/UserCenter" params:nil HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        XSLog(@"%@",dic);
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                JYSMineModel * mineModel = [JYSMineModel mj_objectWithKeyValues:dataDict];
                
                [self setHeaderDataWithModel:mineModel];
            }
            
            
        }
    } fail:^(NSError *error) {
        
    } showHUD:YES];
//    [JYSAFNetworking getOrPostWithType:POST withUrl:@"/api/v1/user/user/GetUserInfo" params:nil HUDShowView:self.view HUDAnimated:YES success:^(id response) {
//        XSLog(@"%@",response);
//    } fail:^(NSError *error) {
//
//    } showHUD:YES];
}

- (void)setHeaderDataWithModel:(JYSMineModel *)model {
    [self.headPortraitButton getBackImageWithUrlStr:model.logo andDefaultImage:[UIImage imageNamed:@"jys_mine_headPortrait"] forState:UIControlStateNormal];
    
    self.userNameLabel.text = model.username;
    
    self.headerDetailLabel.text = [NSString stringWithFormat:@"%@%@ | %@%@ | %@%@",Localized(@"trade"),model.trade_count,Localized(@"favourable"),model.praise_rate,Localized(@"trust"),model.trust_me_count];
    
}

static NSString * mineCellID = @"mineCellID";
- (void)setUpUI {
    [self.mineTableView registerNib:[UINib nibWithNibName:NSStringFromClass([JYSMineCell class]) bundle:nil] forCellReuseIdentifier:mineCellID];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mineArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSMineCell * cell = [tableView dequeueReusableCellWithIdentifier:mineCellID forIndexPath:indexPath];
    
    if (self.mineArray.count) {
        [cell setMineDataWithDict:self.mineArray[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(65);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dataDict = self.mineArray[indexPath.row];
    if ([dataDict[@"title"] isEqualToString:Localized(@"c2corder")]) {
        MyOrderController * c2cOrderVC = [[MyOrderController alloc] init];
        [self.navigationController pushViewController:c2cOrderVC animated:YES];
    } else if ([dataDict[@"title"] isEqualToString:Localized(@"entrusetorder")]) {
        JYSEntrustOrdersViewController * entrustOrdersVC = [[JYSEntrustOrdersViewController alloc] init];
        [self.navigationController pushViewController:entrustOrdersVC animated:YES];
    } else if ([dataDict[@"title"] isEqualToString:Localized(@"bussappli")]) {
        MerchantApplicationController * vertigyVC= [[MerchantApplicationController alloc] init];
        [self.navigationController pushViewController:vertigyVC animated:YES];
    } else if ([dataDict[@"title"] isEqualToString:Localized(@"my_advert")]) {
        BaseMyAdverController * myAdvertisingVC = [[BaseMyAdverController alloc] init];
        [self.navigationController pushViewController:myAdvertisingVC animated:YES];
    } else if ([dataDict[@"title"] isEqualToString:Localized(@"recharge_wallet_pay_select")]) {
        BindingViewController * bindTypeVC = [[BindingViewController alloc] init];
        [self.navigationController pushViewController:bindTypeVC animated:YES];
    }
}

-(void)dealloc {
    XSLog(@"%s",__FUNCTION__);
}

@end
