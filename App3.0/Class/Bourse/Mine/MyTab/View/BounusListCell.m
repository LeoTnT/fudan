//
//  BounusListCell.m
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "BounusListCell.h"


@interface BounusListCell()
/** 头像 */
@property (nonatomic, strong) UIImageView *userImgView;

/** 名称 */
@property (nonatomic, strong) UILabel *nameLb;
/** 推荐奖励*/
@property (nonatomic, strong) UILabel *typeLb;



/** +2BTC */
@property (nonatomic, strong) UILabel *priceLb;
/** date*/
@property (nonatomic, strong) UILabel *dateLb;

@end


@implementation BounusListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    CGFloat space = 10.0;
    CGFloat lbHeight = 26.0;
    CGFloat imgW = 44.0;
    self.backgroundColor = [UIColor whiteColor];
    
    //_imgView
    _userImgView = [[UIImageView alloc] init];
    _userImgView.layer.masksToBounds = YES;
    _userImgView.layer.cornerRadius = imgW/2;
    [self addSubview:_userImgView];
    [_userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:[UIColor blackColor] Font:[UIFont systemFontOfSize:14.5] Radius:0];
    _nameLb.numberOfLines = 0;
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_userImgView.mas_right).offset(5);
        make.top.mas_equalTo(_userImgView);
        make.height.mas_equalTo(lbHeight*1.6);
        make.width.mas_equalTo(mainWidth-120-imgW);
    }];
    
    
    //_priceLb
    _priceLb = [self getLabelWithTextColor:RedColor Font:[UIFont systemFontOfSize:17.5] Radius:0];
    _priceLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_priceLb];
    [_priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_nameLb);
        make.height.mas_equalTo(lbHeight);
        make.width.mas_equalTo(100);
    }];
    
    //_typeLb
    _typeLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:13] Radius:0];
    [self addSubview:_typeLb];
    [_typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb);
        make.bottom.mas_equalTo(_userImgView.mas_bottom).offset(4);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    //_dateLb
    _dateLb = [self getLabelWithTextColor:Color(@"999999") Font:[UIFont systemFontOfSize:14] Radius:0];
    _dateLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_dateLb];
    [_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(_typeLb);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    _userImgView.image = [UIImage imageNamed:@"F1_jiang"];//F1_jiang
    _nameLb.text = @"";
    _priceLb.text = @"";
    _typeLb.text = @"";
    _dateLb.text = @"";
    
    
    
}
-(void)setModel:(BonusListModel *)model
{
   NSString *type = [NSString stringWithFormat:@"%@",model.type];
    NSString *imgName = @"F1_jiang";
    //type奖励类型 1 推荐奖励 2手续费奖金
    NSString *typeStr = Localized(@"bonus_type_tuijian");
    if (type.intValue == 1) {
        imgName = @"F1_jian";
        typeStr = Localized(@"bonus_type_tuijian");
    }else if (type.intValue == 2) {
        imgName = @"F1_jiang";
        typeStr = Localized(@"bonus_type_tuijian");
    }
    _nameLb.text = [NSString stringWithFormat:@"%@",model.title];
    _typeLb.text = [NSString stringWithFormat:@"[%@]",typeStr];
    _dateLb.text = [NSString stringWithFormat:@"%@",model.w_time];
    
    _userImgView.image = [UIImage imageNamed:imgName];
    NSString *priceText = [NSString stringWithFormat:@"%@ %@",model.number,[model.coin_name uppercaseString]];
    
    NSMutableAttributedString *attributeStr = [self setStringWithStr:priceText RangStr:[NSString stringWithFormat:@"%@",[model.coin_name uppercaseString]]];
    _priceLb.attributedText=attributeStr;
    
    _typeLb.hidden = YES;
}
- (NSMutableAttributedString *)setStringWithStr:(NSString *)str RangStr:(NSString *)rangStr{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range=[[attributeStr string]rangeOfString:rangStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:RedColor range:range];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:range];
    return attributeStr;
    
}

- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}


@end


