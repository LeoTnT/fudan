//
//  MyTeamCell.m
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MyTeamCell.h"
@interface MyTeamCell()
/** 头像 */
@property (nonatomic, strong) UIImageView *userImgView;

/** 名称 */
@property (nonatomic, strong) UILabel *nameLb;
/** ID*/
@property (nonatomic, strong) UILabel *IDLb;



/** mobile */
@property (nonatomic, strong) UILabel *mobileLb;
/** 日期*/
@property (nonatomic, strong) UILabel *dateLb;

@end


@implementation MyTeamCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    CGFloat space = 10.0;
    CGFloat lbHeight = 22.0;
    CGFloat imgW = 44.0;
    self.backgroundColor = [UIColor whiteColor];
    
    //_imgView
    _userImgView = [[UIImageView alloc] init];
    _userImgView.layer.masksToBounds = YES;
    _userImgView.layer.cornerRadius = imgW/2;
    [self addSubview:_userImgView];
    [_userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:[UIColor blackColor] Font:[UIFont systemFontOfSize:15.5] Radius:0];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_userImgView.mas_right).offset(5);
        make.centerY.mas_equalTo(_userImgView);
//        make.height.mas_equalTo(lbHeight);
    }];
    
    
    //_mobileLb
    _mobileLb = [self getLabelWithTextColor:COLOR_999999 Font:[UIFont systemFontOfSize:15.5] Radius:0];
    _mobileLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_mobileLb];
    [_mobileLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.bottom.mas_equalTo(self.mas_centerY).offset(-3);
//        make.height.mas_equalTo(lbHeight);
    }];
    
    //_IDLb
    _IDLb = [self getLabelWithTextColor:COLOR_999999 Font:[UIFont systemFontOfSize:14] Radius:0];
    [self addSubview:_IDLb];
    [_IDLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_nameLb);
        make.bottom.mas_equalTo(_userImgView.mas_bottom);
        make.height.mas_equalTo(lbHeight);
    }];
    
    
    //_dateLb
    _dateLb = [self getLabelWithTextColor:COLOR_999999 Font:[UIFont systemFontOfSize:14] Radius:0];
    _dateLb.textAlignment = NSTextAlignmentRight;
    [self addSubview:_dateLb];
    [_dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-space);
        make.top.mas_equalTo(self.mas_centerY).offset(3);;
//        make.height.mas_equalTo(lbHeight);
    }];
    
    
    _line = [[UIView alloc] init];
    _line.backgroundColor = Color(@"3D4250");
    [self addSubview:_line];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15*wScale);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_userImgView.mas_bottom).offset(10);
        make.height.mas_equalTo(0.8);
    }];
    
    
    _userImgView.image = avatarDefaultImg;
    _nameLb.text = @"";
    _mobileLb.text = @"";
//    _IDLb.text = @"ID:";
    _dateLb.text = @"";
    
    
    
}

-(void)setModel:(MyTeamModel *)model
{
    _model = model;
    [_userImgView getImageWithUrlStr:model.logo andDefaultImage:avatarDefaultImg];

    _nameLb.text = [NSString stringWithFormat:@"%@",model.username];
    _mobileLb.text = [NSString stringWithFormat:@"%@",model.mobile];
//    _IDLb.text = [NSString stringWithFormat:@"ID:%@",model.ID];
    _IDLb.hidden = YES;
    _dateLb.text = [NSString stringWithFormat:@"%@",model.w_time];
}

-(NSString *)nameSuitScanf:(NSString*)name{
    
    
    NSString *suitName = [NSString stringWithFormat:@"%@",name];
    if (suitName.length >5) {//如果是手机号码的话
        
        NSString *numberString = [suitName stringByReplacingCharactersInRange:NSMakeRange(2, 3) withString:@"***"];
        
        return numberString;
        
    }
    return suitName;
}



- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
//    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}


@end

