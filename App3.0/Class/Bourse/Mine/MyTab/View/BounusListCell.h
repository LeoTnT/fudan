//
//  BounusListCell.h
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BonusListModel.h"

@interface BounusListCell : UITableViewCell
@property (nonatomic,strong) BonusListModel * model;

@end
