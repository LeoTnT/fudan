//
//  MyTeamModel.h
//  App3.0
//
//  Created by xinshang on 2018/1/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyTeamModel : NSObject
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *w_time;
@property (nonatomic,copy) NSString *mobile;

@end

/*
 "head_logo": "",
 "username": "cn966248",
 "mobile": "",
 "id": "6468",
 "time": "2018-01-13 15:15"
 */
