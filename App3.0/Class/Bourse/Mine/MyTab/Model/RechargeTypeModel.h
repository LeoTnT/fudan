//
//  RechargeTypeModel.h
//  App3.0
//
//  Created by xinshang on 2018/1/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeTypeModel : NSObject
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *account;
@end
