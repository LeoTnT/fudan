//
//  BonusListModel.h
//  App3.0
//
//  Created by xinshang on 2018/1/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BonusListModel : NSObject
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *number;
@property (nonatomic,copy) NSString *w_time;
@property (nonatomic,copy) NSString *coin_name;
@property (nonatomic,copy) NSString *remain;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *intro;
@end

/*
 "username": "shengnan",
 "number": "1.00",
 "remain": "2.00",
 "coin_name": "CNY",
 "type": "实名认证",
 "w_time": "2018-05-21 15:15",
 "intro": "shengnan",
 "title": "会员s***n实名认证"
 */
