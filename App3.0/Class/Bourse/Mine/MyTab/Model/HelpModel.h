//
//  HelpModel.h
//  YunWallent
//
//  Created by xinshang on 2018/1/22.
//  Copyright © 2018年 xinshang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelpModel : NSObject
@property (nonatomic,copy) NSString * ID;
@property (nonatomic,copy) NSString * title;
@property (nonatomic,copy) NSString * content;
@property (nonatomic,copy) NSString * time;
@property (nonatomic,copy) NSString * author;
@property (nonatomic,copy) NSString * name;
@property (nonatomic,copy) NSString * parent_title;

@end


@interface HelpHomeModel : NSObject
@property (nonatomic,copy) NSString * ID;
@property (nonatomic,copy) NSString * name;

@end

/*
 "createtime": "1516605302",
 "title": "第五",
 "content": "<p>\r\n\t是打发\r\n<\/p>\r\n<p>\r\n\t阿斯蒂芬\r\n<\/p>\r\n<p>\r\n\t阿斯蒂芬\r\n<\/p>\r\n<p>\r\n\tasd风as地方\r\n<\/p>\r\n<p>\r\n\tasdf 阿斯蒂芬\r\n<\/p>\r\n<p>\r\n\tsdaf&nbsp;\r\n<\/p>"
 }, {
 "createtime": "1516605292",
 */
