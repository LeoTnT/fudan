//
//  BonusPopVC.m
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "BonusPopVC.h"
#import "RechargeTypeModel.h"

@interface BonusPopVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_arr1;
    NSArray *_arr2;
    __weak id<BonusPopViewDelegate>delegate;
}
@property(nonatomic, strong) UITableView *tableView;
@end

@implementation BonusPopVC
@synthesize delegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _arr1 = [NSArray array];
//    _arr2 = @[@"chat_add_group",@"chat_add_friend",@"chat_add_qr"];

    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setListArr:(NSArray *)listArr
{
    _listArr = listArr;
    _arr1 = listArr;
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arr1.count;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (delegate) {
        [delegate menuClick:indexPath.row];
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    RechargeTypeModel *model = _arr1[indexPath.row];
    NSString *text= [NSString stringWithFormat:@"  %@",model.name];
    cell.textLabel.text =[text uppercaseString]  ;
    cell.textLabel.textColor = [UIColor blackColor];
    NSLog(@"%@",_arr1[indexPath.row]);
//    cell.textLabel.textAlignment = NSTextAlignmentCenter;
//    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",_arr2[indexPath.row]]];
    
    return cell;
    
    
}


//重置本控制器的大小
-(CGSize)preferredContentSize{
    
    if (self.popoverPresentationController != nil) {
        CGSize tempSize ;
        tempSize.height = self.view.frame.size.height;
        tempSize.width  = 90;
        CGSize size = [self.tableView sizeThatFits:tempSize];  //返回一个完美适应tableView的大小的 size
        return size;
    }else{
        return [super preferredContentSize];
    }
    
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = YES;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}
@end

