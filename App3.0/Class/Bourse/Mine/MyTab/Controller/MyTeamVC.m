//
//  MyTeamVC.m
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MyTeamVC.h"
#import "MyTeamCell.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface MyTeamVC ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
@property (nonatomic,strong) NSMutableArray *teamArr;
@property (nonatomic,strong) UIView *footView;
@property (nonatomic,strong) UILabel *noteLb;
@property (nonatomic,assign) NSInteger page;

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation MyTeamVC
-(NSMutableArray *)teamArr
{
    if (!_teamArr) {
        _teamArr = [NSMutableArray array];
    }
    return _teamArr;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.page = 1;
    [self setBlackLeftBackBtn];
    self.navigationItem.title = Localized(@"personal_my_members");
    [self SetUpUI];
}
- (void)getMyTree
{
    self.shouldShowEmptyView = YES;
    [self.tableView.mj_footer resetNoMoreData];
    
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager yun_getMyTreeWithPage:[NSString stringWithFormat:@"%ld",self.page]
                               success:^(NSDictionary *dic, resultObject *state)
     {
         @strongify(self);
         [self tableViewEndRefreshing];
         if (state.status) {
             if (self.page == 1) {
                 [self.teamArr removeAllObjects];
             }
             NSArray *tDic = dic[@"data"][@"data"];
             if (tDic) {
                 if (tDic.count < [dic[@"data"][@"per_page"] integerValue]) {
                     self.tableView.mj_footer.hidden = YES;
                 } else {
                     self.tableView.mj_footer.hidden = NO;
                 }
                 
                 if (tDic.count >0) {
                     [self.teamArr addObjectsFromArray:[MyTeamModel mj_objectArrayWithKeyValuesArray:tDic]];
                     
                     
                     
                 }else
                 {
                     if (self.page > 0) {
                         [self.tableView.mj_footer endRefreshingWithNoMoreData];
                     }
                 }
                 
                 
             }
         }else{
             Alert(state.info);
         }
         [self.tableView reloadData];
         
     } fail:^(NSError *error) {
         @strongify(self);
         [self tableViewEndRefreshing];
         Alert(NetFailure);
     }];
}
- (void)SetUpUI
{
    
    self.page = 0;
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        
    }];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    
    @weakify(self);
    self.refreshHeader = ^{
        @strongify(self);
        self.page = 1;
        [self getMyTree];
    };
    self.refreshFooter= ^{
        @strongify(self);
        self.page ++;
        [self getMyTree];
    };
    self.tableView.mj_footer.hidden = YES;
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.teamArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyTeamCell";
    MyTeamCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MyTeamCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (self.teamArr.count >0) {
        cell.line.hidden = NO;
        cell.model = self.teamArr[indexPath.row];
        if (indexPath.row == (self.teamArr.count-1)) {
            cell.line.hidden = YES;
        }
        
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 78;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.teamArr.count >0) {
        
        return CGFLOAT_MIN;
    }
    return 44;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (!self.footView) {
        self.footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
        self.footView.backgroundColor = BG_COLOR;
        
        self.noteLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 44)];
        self.noteLb.textColor = Color(@"666666");
        self.noteLb.font = [UIFont boldSystemFontOfSize:15];
        self.noteLb.textAlignment = NSTextAlignmentCenter;
        [self.footView addSubview:self.noteLb];
        
    }
    if (self.teamArr.count >0) {
        self.noteLb.text = [NSString stringWithFormat:@"%@%ld%@",Localized(@"共"),self.teamArr.count,Localized(@"位成员")];
    }
    return self.footView;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = BG_COLOR;
    
    return view;
}

- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"暂无团队成员");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

@end
