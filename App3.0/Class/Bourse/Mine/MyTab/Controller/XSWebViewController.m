//
//  XSWebViewController.m
//  App3.0
//
//  Created by xinshang on 2017/7/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSWebViewController.h"
#import <WebKit/WebKit.h>

@interface XSWebViewController ()<WKNavigationDelegate, WKUIDelegate, UIGestureRecognizerDelegate>
@property(nonatomic,strong)WKWebView *webView;
@property(nonatomic,strong)UIButton *reloadBtn;
@end


@implementation XSWebViewController

#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
//    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
//
//    [self setTitle:self.titleStr isBack:YES isBgImg:YES];
    [self setNavTitle:self.titleStr isShowBack:YES];

//    self.titleStr = @"注册协议";
//    self.autoRightSlider = YES;
    [self setSubViews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (self.urlStr.length >0) {

    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    }
}
#pragma mark-Lazy Loading
- (WKWebView *)webView
{
    if (_webView == nil) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
        _webView.scrollView.bounces = NO;
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        //        NSString *guid = [ud objectForKey:APPAUTH_KEY];
        //        NSString *urlString = [NSString stringWithFormat:Url_WebMall,guid];
        //        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10]];
        if (self.urlStr.length >0) {
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlStr]]];
            [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
        }else if (self.contentStr.length >0)
            [_webView loadHTMLString:self.contentStr baseURL:nil];
        }

      
    return _webView;
}

#pragma mark-Private
-(void)setSubViews{
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor=BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    //    [self actionCustomRightBtnWithNrlImage:@"mall_exchange" htlImage:nil title:nil action:^{
    //        @strongify(self);
    //        [self.webView reload];
    //    }];
    self.reloadBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 18, 18)];
    [self.reloadBtn  setBackgroundImage:[UIImage imageNamed:@"mall_exchange"] forState:UIControlStateNormal];
    [self.reloadBtn  addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.reloadBtn ];
    self.navigationItem.rightBarButtonItem = item;
    [self.view addSubview:self.webView];
}
-(void)refreshData{
    [self.webView reload];
}

#pragma mark - gesture
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
    
    // 判断方向
    CGPoint translatedPoint = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self.view];
    if (translatedPoint.x < 0) {
        return NO;
    }
    
    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
    if(self.navigationController.childViewControllers.count == 1){
        // 表示用户在根控制器界面，就不需要触发滑动手势，
        return NO;
    }
    return YES;
}



//监听网页加载进度  加载完成，刷新按钮旋转
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (self.webView.estimatedProgress== 1) {
            [UIView animateWithDuration:0.7                                                                                                                                                                                                                                  animations:^{
                self.reloadBtn.transform=CGAffineTransformRotate(self.reloadBtn.transform, M_PI);
            }];
        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    [XSTool showProgressHUDWithView:self.view];
}
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    [XSTool hideProgressHUDWithView:self.view];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [XSTool hideProgressHUDWithView:self.view];
}

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
}

// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
}
//-(void)viewDidDisappear:(BOOL)animated{
//    [super viewDidDisappear:animated];
//    self.navigationController.navigationBarHidden=YES;
//}

@end
