//
//  VeticalMobileViewController.m
//  App3.0
//
//  Created by xinshang on 2018/2/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "VeticalMobileViewController.h"

@interface VeticalMobileViewController ()
{
    BOOL _isTimer;// 是否在倒计时
    
}
@property (nonatomic, strong) UITextField *phoneNumTF;//手机号
@property (nonatomic, strong) UITextField *codeTF;//手机号验证码
@property (nonatomic,strong) UIButton *codeBtn;
@property (nonatomic ,strong) dispatch_source_t timer;

@property (nonatomic, strong) NSString *mobielText;//

@end
#define cellHeight 55

@implementation VeticalMobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self mobileVerifyIndex];
}

#pragma mark - 设置子视图
-(void)setUpUI{
    
    //    [self setBlackLeftBackBtn];
    [self setNavTitle:@"手机号认证" isShowBack:YES];
    
    
    
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = BG_COLOR;
}

- (void)mobileVerifyIndex
{

    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
  [HTTPManager yun_mobileVerifyIndexSuccess:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             NSDictionary *tDic = dic[@"content"];
             if (tDic) {
                 NSString *message = [NSString stringWithFormat:@"%@%@%@,%@",Localized(@"已认证"),dic[@"content"][@"mobile"],Localized(@"手机号"),Localized(@"无法重复认证")];
//                 UIAlertView *alertVC= [UIAlertView alertViewWithTitle:Localized(@"提示") message:message  cancelButtonTitle:nil otherButtonTitles:@[Localized(@"确定")] completionBlock:^(UIAlertView *alertView, NSInteger selectedButtonIndex) {
//                         [wSelf.navigationController popViewControllerAnimated:YES];
//                 } cancelBlock:nil];
//                 [alertVC show];
                 [XSTool showToastWithView:self.view Text:message delay:2.1];
                 wSelf.mobielText = [NSString stringWithFormat:@"%@",dic[@"content"][@"mobile"]];
                 wSelf.phoneNumTF.text =  wSelf.mobielText;
                 wSelf.phoneNumTF.enabled = NO;
                 wSelf.codeBtn.enabled = NO;
             }
            }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        
        if (!isEmptyString(self.mobielText)) {
            return 1;
        }
        return 2;
    }
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"UITableViewCell"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(20, cellHeight-1, mainWidth-35, 1)];
            line.backgroundColor = mainGrayColor;
            [cell addSubview:line];
        }
        NSArray *imgArray = @[@"a_phone",@"a_code"];
        
        cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imgArray[indexPath.row]]];
        
        
        
        if (indexPath.row==0)
        {
            if (!self.phoneNumTF) {
                CGRect tFrame = CGRectMake(42, 0, mainWidth-90, cellHeight);
                self.phoneNumTF = [self creatTextFieldWithPlaceTilte:@"请输入手机号" WithFrame:tFrame];
                self.phoneNumTF.keyboardType = UIKeyboardTypeNumberPad;
                [cell addSubview:self.phoneNumTF];
            }
        }else if (indexPath.row == 1) {
            
            if (!self.codeTF) {
                CGRect tFrame = CGRectMake(42, 0, mainWidth-150, cellHeight);
                self.codeTF = [self creatTextFieldWithPlaceTilte:@"请输入手机验证码" WithFrame:tFrame];
                self.codeTF.keyboardType = UIKeyboardTypeNumberPad;
                [cell addSubview:self.codeTF];
            }
            if (!self.codeBtn) {
                
                self.codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                self.codeBtn.frame = CGRectMake(mainWidth-100, (cellHeight-28)/2, 90, 28);
                self.codeBtn.layer.masksToBounds = YES;
                self.codeBtn.layer.cornerRadius = 3;
                self.codeBtn.layer.borderWidth = 0.5;
                self.codeBtn.layer.borderColor = mainColor.CGColor;
                self.codeBtn.backgroundColor = mainColor;
                [self.codeBtn setTitle:Localized(@"获取验证码") forState:UIControlStateNormal];
                [self.codeBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
                [self.codeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self.codeBtn addTarget:self action:@selector(codeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:self.codeBtn];
            }
            cell.layer.masksToBounds = YES;
        }
        
        return cell;
        
    }else{
        static NSString *CellIdentifier = @"CellRegisterBtn";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+1000, 0,0);
        }
        UIButton *sureBtn = [self creatSureBtn];
        [cell addSubview:sureBtn];
        return cell;
    }
    
  
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            if (!isEmptyString(self.mobielText)) {
                return CGFLOAT_MIN;
            }
        }
        
        return cellHeight;
    }
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = BG_COLOR;
    
    return view;
}



#pragma mark - 获取验证码
-(void)codeBtnAction:(UIButton *) sender{
    [self.view endEditing:YES];
    if (isEmptyString(self.phoneNumTF.text)) {
        [XSTool showToastWithView:self.view Text:@"请输入手机号"];
        return;
    }
    
    
    
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager yun_sendSmsMessageWithMobile:_phoneNumTF.text Success:^(NSDictionary *dic, resultObject *state) {
//        [XSTool hideProgressHUDWithView:self.view];
//        // 验证码发送成功
//        if (state.status) {
//            [XSTool showToastWithView:self.view Text:@"发送成功"];
//            _isTimer = YES; // 设置倒计时状态为YES
//            sender.enabled = NO; // 设置按钮为不可点击
//            
//            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
//            [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
//                //设置按钮的样式
//                dispatch_source_cancel(_timer);
//                [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
//                sender.enabled = YES; // 设置按钮可点击
//                
//                _isTimer = NO; // 倒计时状态为NO
//            } otherAction:^(int time) {
//                [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
//            }];
//            
//        } else {
//            if (state.info.length>0) {
//                [XSTool showToastWithView:self.view Text:state.info];
//            }
//            sender.enabled = YES; // 设置按钮为可点击
//        }
//    } fail:^(NSError *error)
//     {
//         [XSTool showToastWithView:self.view Text:@"验证码发送失败"];
//         sender.enabled = YES; // 设置按钮为可点击
//         [XSTool hideProgressHUDWithView:self.view];
//     }];
    
    
    
}
- (UIButton *)creatSureBtn
{
    CGFloat space = 18;
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(space,30, mainWidth - space*2, 45);
    [Btn setTitle:Localized(@"确定") forState:UIControlStateNormal];
    [Btn setBackgroundColor:mainColor];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 5;
    [Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(sureBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    return Btn;
}

#pragma mark----确定
//确定
- (void)sureBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (self.phoneNumTF.enabled == NO) {
        return;
    }
    
    if (isEmptyString(self.phoneNumTF.text)) {
        Alert(@"请输入手机号");
        return;
    }
    if (isEmptyString(self.codeTF.text)) {
        Alert(@"请输入验证码");
        return;
    }
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager yun_mobileVerifyWithMobile:self.phoneNumTF.text
                                     verify:self.codeTF.text
                                    success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             Alert(state.info);
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [wSelf.navigationController popViewControllerAnimated:YES];
             });
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

- (UITextField *)creatTextFieldWithPlaceTilte:(NSString*)title WithFrame:(CGRect)frame
{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.textAlignment = NSTextAlignmentLeft;
    textField.placeholder = Localized(title);
    textField.textColor = [UIColor whiteColor];
    textField.font = [UIFont systemFontOfSize:15];
    textField.delegate = self;
    [textField setValue:Color(@"787F89") forKeyPath:@"_placeholderLabel.textColor"];
    
    UILabel *placeholderLabel = [textField valueForKeyPath:@"_placeholderLabel"];
    placeholderLabel.numberOfLines = 0;
    return textField;
}


@end



