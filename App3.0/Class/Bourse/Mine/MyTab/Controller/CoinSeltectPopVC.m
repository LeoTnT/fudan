//
//  CoinSeltectPopVC.m
//  App3.0
//
//  Created by xinshang on 2018/1/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "CoinSeltectPopVC.h"

@interface CoinSeltectPopVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_arr1;
    NSArray *_arr2;
    __weak id<CoinSeltectPopViewDelegate>delegate;
}
@property(nonatomic, strong) UITableView *tableView;
@end

@implementation CoinSeltectPopVC
@synthesize delegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = Color(@"282F37");

    _arr1 = self.typeArr;
    //    _arr2 = @[@"chat_add_group",@"chat_add_friend",@"chat_add_qr"];
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arr1.count;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (delegate) {
        [delegate menuClick:indexPath.row];
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str = @"CoinSeltectPopCell";
    CoinSeltectPopCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[CoinSeltectPopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    RechargeTypeModel *model = _arr1[indexPath.row];
    if (!model.name || [model.name isEqualToString:@"null"]) {
        model.name = model.account;
    }
    NSString *text = [NSString stringWithFormat:@"%@",model.name];
    cell.nameLb.text = [text uppercaseString];
    [cell.logoImg getImageWithUrlStr:[NSString stringWithFormat:@"%@",model.logo] andDefaultImage:DefaultImage];
    NSLog(@"%@",_arr1[indexPath.row]);
    
    
    
    
    //    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    //    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",_arr2[indexPath.row]]];
    
    return cell;
    
    
}


//重置本控制器的大小
-(CGSize)preferredContentSize{
    
    if (self.popoverPresentationController != nil) {
        CGSize tempSize ;
        tempSize.height = self.view.frame.size.height;
        tempSize.width  = 120;
        CGSize size = [self.tableView sizeThatFits:tempSize];  //返回一个完美适应tableView的大小的 size
        return size;
    }else{
        return [super preferredContentSize];
    }
    
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = YES;
        _tableView.backgroundColor = Color(@"282F37");
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}
@end

@implementation CoinSeltectPopCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    CGFloat space = 10.0;
    CGFloat lbHeight = 26.0;
    CGFloat imgW = 25.0;
    self.backgroundColor = mainColor;
    
    //_logoImg
    _logoImg = [[UIImageView alloc] init];
    _logoImg.layer.masksToBounds = YES;
    _logoImg.layer.cornerRadius = imgW/2;
    [self addSubview:_logoImg];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space*3);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(imgW);
        make.height.mas_equalTo(imgW);
    }];
    
    //_nameLb
    _nameLb = [self getLabelWithTextColor:[UIColor whiteColor] Font:[UIFont systemFontOfSize:15] Radius:0];
    [self addSubview:_nameLb];
    [_nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_logoImg.mas_right).offset(5);
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(lbHeight);
        make.width.mas_equalTo(100);
    }];
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = Color(@"3D4250");
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(space);
        make.bottom.mas_equalTo(self).offset(-0.5);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(-space);
    }];
}
- (UILabel *)getLabelWithTextColor:(UIColor *)color Font:(UIFont*)font Radius:(CGFloat)radius
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    //    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = radius;
    return label;
}

@end


