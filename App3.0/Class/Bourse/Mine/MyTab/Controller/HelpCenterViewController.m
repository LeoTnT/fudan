//
//  HelpCenterViewController.m
//  YunWallent
//
//  Created by xinshang on 2018/1/22.
//  Copyright © 2018年 xinshang. All rights reserved.
//

#import "HelpCenterViewController.h"
#import "HelpModel.h"
#import "XSWebViewController.h"

@interface HelpCenterViewController ()
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,assign) NSInteger page;



@end
#define cellHeight 44

@implementation HelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"D1_bg"]]];
    [self setUpUI];
    
    
}


- (void)setUpUI {
    
    if (isEmptyString(self.titleStr)) {
        self.titleStr = @"帮助中心";
    }
    [self setNavTitle:self.titleStr isShowBack:YES];

    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height-0);
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 0;
        [wSelf upLoadOrderListIsRefresh:YES];
    }];
    self.showRefreshFooter = NO;

    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf upLoadOrderListIsRefresh:YES];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

}

- (void)upLoadOrderListIsRefresh:(BOOL)isRefresh{
    if (self.dataArray.count >0&&isRefresh ==NO) {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        return;
    }
    
    [self.tableView.mj_footer resetNoMoreData];

//    NSDictionary *param = @{
//                            @"page":@"1",
//                            @"numberPerPage":@"5",
//                            @"method":@"queryNotice"
//                            };
//    __weak __typeof__(self) wSelf = self;
//    [self.tableView.mj_footer resetNoMoreData];
//    [XSTool showProgressHUDWithView:self.view];
//    [XSHTTPManager post:nil parameters:param success:^(NSDictionary *dic, resultObject *state) {
//        [XSTool hideProgressHUDWithView:wSelf.view];
//        [wSelf handleWithDic:dic resultObject:state];
//    } failure:^(NSError *error) {
//        [XSTool hideProgressHUDWithView:wSelf.view];
//        Alert(NetFailure);
//    }];
    if (isEmptyString(self.ID)) {
        Alert(@"获取信息失败!");
        return;
    }
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager yun_getNoticeChildrenWithID:self.ID
                                   success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
            [wSelf handleWithDic:dic resultObject:state];
     
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}

- (void)handleWithDic:(NSDictionary*)dic resultObject:(resultObject *)state
{
    if (self.page <= 0) {
        [self.dataArray removeAllObjects];
    }
    [self tableViewEndRefreshing];
    if (state.status) {
        NSArray *dataArr = dic[@"content"];
        if (dataArr.count > 0) {
            [self.dataArray addObjectsFromArray:[HelpModel
                                                 mj_objectArrayWithKeyValuesArray:dataArr]];
        }
//        if (self.page > 1 && dataArr.count <= 0) {
//            [self.tableView.mj_footer endRefreshingWithNoMoreData];
//        }
        if (self.dataArray.count >0) {
            [self.tableView reloadData];
        }else {
        }
        
    }else {
        if (dic[@"info"]) {
            Alert(dic[@"info"]);
        }
    }
    [self.tableView reloadData];
    
}



#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.backgroundColor = [UIColor clearColor];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(18*wScale, cellHeight-0.8, mainWidth-18*wScale, 0.8)];
        line.backgroundColor = LINE_COLOR;
        [cell addSubview:line];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if (self.dataArray) {
        HelpModel *model = self.dataArray[indexPath.row];
//        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",model.time];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",model.name];
        cell.detailTextLabel.text = @"";
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.dataArray.count >0) {
        HelpModel *model = self.dataArray[indexPath.row];

        
        [self getNoticeDetailWithType:model.name];
        
    }
}

//获取公告详情
- (void)getNoticeDetailWithType:(NSString *)type{
    if (isEmptyString(type)) {
        return;
    }
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager yun_getNoticeDetailWithType:type
                               success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
                       NSArray *listArr = [NSArray arrayWithArray:[HelpModel mj_objectArrayWithKeyValuesArray:dic[@"content"]]];
//             [self.dataArray addObjectsFromArray:[HelpModel
//                                                  mj_objectArrayWithKeyValuesArray:dataArr]];
             if (listArr.count >0) {
                 HelpModel *webModel = listArr.firstObject;
                 XSWebViewController *vc = [[XSWebViewController alloc] init];
                 vc.titleStr = [NSString stringWithFormat:@"%@",webModel.title];
                 vc.contentStr = [NSString stringWithFormat:@"%@",webModel.content];
                 [self.navigationController pushViewController:vc animated:YES];
             }else{
                 Alert(@"获取信息失败");
             }
             
            
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}

//获取CLC
-(void)getCLCBtnClick:(UIButton *)sender
{
    self.tabBarController.selectedIndex = 1;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
@end


