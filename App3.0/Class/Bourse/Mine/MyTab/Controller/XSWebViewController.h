//
//  XSWebViewController.h
//  App3.0
//
//  Created by xinshang on 2017/7/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface XSWebViewController : XSBaseViewController
@property(nonatomic,strong)NSString *urlStr;
@property(nonatomic,strong)NSString *contentStr;
@property(nonatomic,strong)NSString *titleStr;

@end
