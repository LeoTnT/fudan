//
//  HelpCenterHomeViewController.m
//  App3.0
//
//  Created by xinshang on 2018/5/8.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HelpCenterHomeViewController.h"
#import "HelpModel.h"
#import "HelpCenterViewController.h"

@interface HelpCenterHomeViewController ()
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,assign) NSInteger page;



@end
#define cellHeight 44

@implementation HelpCenterHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"D1_bg"]]];
    [self setUpUI];
    
    
}


- (void)setUpUI {
    
    [self setNavTitle:@"帮助中心" isShowBack:YES];
    
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, self.view.frame.size.height-0);
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.showRefreshFooter = NO;
    
    __weak __typeof__(self) wSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        wSelf.page = 0;
        [wSelf upLoadOrderListIsRefresh:YES];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        wSelf.page++;
        [wSelf upLoadOrderListIsRefresh:YES];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
}

- (void)upLoadOrderListIsRefresh:(BOOL)isRefresh{
    if (self.dataArray.count >0&&isRefresh ==NO) {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        return;
    }
    
    [self.tableView.mj_footer resetNoMoreData];
    

    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    
//    [HTTPManager yun_getNoticeTypeSuccess:^(NSDictionary *dic, resultObject *state) {
//        [XSTool hideProgressHUDWithView:wSelf.view];
//        [wSelf handleWithDic:dic resultObject:state];
//        
//    } fail:^(NSError *error) {
//        [XSTool hideProgressHUDWithView:wSelf.view];
//        Alert(NetFailure);
//    }];
    

    
}

- (void)handleWithDic:(NSDictionary*)dic resultObject:(resultObject *)state
{
    if (self.page <= 0) {
        [self.dataArray removeAllObjects];
    }
    [self tableViewEndRefreshing];
    if (state.status) {
        NSArray *dataArr = dic[@"content"];
        if (dataArr.count > 0) {
            [self.dataArray addObjectsFromArray:[HelpHomeModel
                                                 mj_objectArrayWithKeyValuesArray:dataArr]];
        }
//        if (self.page > 1 && dataArr.count <= 0) {
//            [self.tableView.mj_footer endRefreshingWithNoMoreData];
//        }
        if (self.dataArray.count >0) {
            [self.tableView reloadData];
        }else {
        }
        
    }else {
        if (dic[@"info"]) {
            Alert(dic[@"info"]);
        }
    }
    [self.tableView reloadData];
    
}



#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.backgroundColor = [UIColor clearColor];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(18*wScale, cellHeight-0.8, mainWidth-18*wScale, 0.8)];
        line.backgroundColor = LINE_COLOR;
        [cell addSubview:line];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if (self.dataArray) {
        HelpHomeModel *model = self.dataArray[indexPath.row];
//        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",model.time];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",model.name];
        cell.detailTextLabel.text =  @"";
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.dataArray.count >0) {
        HelpHomeModel *model = self.dataArray[indexPath.row];
        
//        XSWebViewController *vc = [[XSWebViewController alloc] init];
//        vc.titleStr = [NSString stringWithFormat:@"%@",model.title];
//        vc.contentStr = [NSString stringWithFormat:@"%@",model.content];
//        [self.navigationController pushViewController:vc animated:YES];
//        
        
        HelpCenterViewController *vc =[[HelpCenterViewController alloc] init];
        vc.ID = model.ID;
        vc.titleStr = model.name;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//获取CLC
-(void)getCLCBtnClick:(UIButton *)sender
{
    self.tabBarController.selectedIndex = 1;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
@end



