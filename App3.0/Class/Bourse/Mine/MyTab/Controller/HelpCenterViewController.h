//
//  HelpCenterViewController.h
//  YunWallent
//
//  Created by xinshang on 2018/1/22.
//  Copyright © 2018年 xinshang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCenterViewController : XSBaseTableViewController
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *titleStr;

@end
