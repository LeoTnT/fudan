//
//  BonusPopVC.h
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BonusPopViewDelegate <NSObject>

- (void)menuClick:(NSInteger)index;

@end

@interface BonusPopVC : UIViewController
@property(nonatomic, weak) id<BonusPopViewDelegate>delegate;
@property (nonatomic,strong) NSArray * listArr;


@end
