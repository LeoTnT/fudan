//
//  BonusListVC.m
//  App3.0
//
//  Created by xinshang on 2018/1/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "BonusListVC.h"
#import "BounusListCell.h"
#import "BonusPopVC.h"
#import "RechargeTypeModel.h"
#import "CoinSeltectPopVC.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface BonusListVC ()<BonusPopViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic,strong) BonusPopVC * popVC;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic,strong) NSArray * typeArr;
@property (nonatomic,copy) NSString * selType;
@property (nonatomic,assign) NSInteger page;
@end

@implementation BonusListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavTitle:Localized(@"personal_my_jiangjin") isShowBack:YES];
    [self SetUpUI];
    [self getAwardInfo];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.popVC) {
        [self dismissViewControllerAnimated:self.popVC completion:nil];
    }
}

//获取所有虚拟币的种类
- (void)getCoinList
{
    if (self.typeArr.count >0) {
        [self popView];
        return;
    }
    
//    __weak __typeof__(self) wSelf = self;
//    [XSTool showProgressHUDWithView:self.view];
//    [HTTPManager yun_getCoinListWithLogo:@"2"
//                                 success:^(NSDictionary *dic, resultObject *state)
//     {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         if (state.status) {
//             NSDictionary *tDic = dic[@"content"];
//             if (tDic) {
//                NSArray *modelArr  = [NSArray arrayWithArray:[RechargeTypeModel mj_objectArrayWithKeyValuesArray:tDic[@"type"]]];
//                 NSMutableArray *mArr = [NSMutableArray array];
//                 for (RechargeTypeModel *modle in modelArr) {
//                     [mArr addObject:modle.name];
//                 }
//                 wSelf.typeArr = mArr;
//                 if (wSelf.typeArr.count >0) {
//                     if (!wSelf.selType) {
//                         wSelf.selType = wSelf.typeArr.firstObject;
//                         [self popView];
//                     }
//                 }
//             }
//             
//         }else{
//             Alert(state.info);
//         }
//         
//     } fail:^(NSError *error) {
//         [XSTool hideProgressHUDWithView:wSelf.view];
//         Alert(NetFailure);
//     }];
}


//获取奖金记录
- (void)getAwardInfo
{
    [self.tableView.mj_footer resetNoMoreData];
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager yun_getAwardInfoWithType:@""
                                     page:[NSString stringWithFormat:@"%ld",self.page]
                                  account:self.selType
                                  success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         [self tableViewEndRefreshing];

         if (state.status) {
            
             if (self.page <= 0) {
                 [self.listArr removeAllObjects];
             }
             NSArray *tDic = dic[@"data"][@"list"][@"data"];
             if (tDic) {
                
                 if (tDic.count >0) {
                     [self.listArr addObjectsFromArray:[BonusListModel mj_objectArrayWithKeyValuesArray:tDic]];
                 }else
                 {
                     if (self.page > 0) {
                         [self.tableView.mj_footer endRefreshingWithNoMoreData];
                     }
                 }

             }
             
             NSDictionary *pDic = dic[@"data"][@"coin"];
             NSArray *arr1 = pDic.allKeys;
             NSArray *arr2 = pDic.allValues;
             NSMutableArray *mArr = [NSMutableArray array];
             for (int i = 0; i < arr1.count; i++) {
                 NSString *str = [NSString stringWithFormat:@"%@",arr1[i]];
                 if (![str isEqualToString:@"9999"]) {
                     RechargeTypeModel *model = [RechargeTypeModel new];
                     model.name = arr2[i];
                     model.account = arr1[i];
                     [mArr addObject:model];
                 }
                
             }
             self.typeArr = mArr;
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [self tableViewEndRefreshing];
         Alert(NetFailure);
     }];
}
- (void)SetUpUI
{
    @weakify(self);
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"bonus_right_title") action:^{
        @strongify(self);
        if (self.selType) {
            [self popView];
        }else{
        [self getCoinList];
        }
    }];
    
    self.view.backgroundColor = BG_COLOR;
    self.tableViewStyle = UITableViewStylePlain;
    //    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = BG_COLOR;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        
    }];
 
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;

    self.refreshHeader = ^{
        @strongify(self);
        self.page = 0;
        [self getAwardInfo];
    };
    self.refreshFooter= ^{
        @strongify(self);
        self.page ++;
        [self getAwardInfo];
    };
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark 无数据界面

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"no_data");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.listArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BounusListCell";
    BounusListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BounusListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (self.listArr.count >0) {
        cell.model = self.listArr[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 78;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = BG_COLOR;
    
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = BG_COLOR;
    
    return view;
}

#pragma mark - navigationItem Action
-(void)popView{
    self.page = 0;
    _popVC = [[BonusPopVC alloc] init];
    _popVC.modalPresentationStyle = UIModalPresentationPopover;
    
    //设置依附的按钮
    _popVC.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    //可以指示小箭头颜色
    _popVC.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    //content尺寸
    _popVC.preferredContentSize = CGSizeMake(90, 400);
    
    //pop方向
    _popVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    //delegate
    _popVC.popoverPresentationController.delegate = self;
    _popVC.delegate = self;
    [self presentViewController:_popVC animated:YES completion:nil];
    _popVC.listArr = self.typeArr;

}

//代理方法 ,点击即可dismiss掉每次init产生的PopViewController
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}
- (void)menuClick:(NSInteger)index
{
    [_popVC dismissViewControllerAnimated:YES completion:nil];
    RechargeTypeModel *model = self.typeArr[index];
    self.selType = model.account;
    self.page = 0;
     [self getAwardInfo];
}
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr =[NSMutableArray array];
    }
    return _listArr;
}

- (void)tableViewEndRefreshing {
    [XSTool hideProgressHUDWithView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
@end

