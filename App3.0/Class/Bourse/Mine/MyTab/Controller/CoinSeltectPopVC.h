//
//  CoinSeltectPopVC.h
//  App3.0
//
//  Created by xinshang on 2018/1/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RechargeTypeModel.h"

@protocol CoinSeltectPopViewDelegate <NSObject>

- (void)menuClick:(NSInteger)index;

@end

@interface CoinSeltectPopVC : UIViewController
@property(nonatomic, weak) id<CoinSeltectPopViewDelegate>delegate;
@property (nonatomic,strong) NSArray * typeArr;

@end

@interface CoinSeltectPopCell : UITableViewCell
/** 头像 */
@property (nonatomic, strong) UIImageView *logoImg;
/** 名称 */
@property (nonatomic, strong) UILabel *nameLb;
@end
