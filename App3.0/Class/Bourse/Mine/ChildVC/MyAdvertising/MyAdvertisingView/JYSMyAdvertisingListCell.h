//
//  JYSMyAdvertisingListCell.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSMyAdvertisingListCell : UITableViewCell

- (void)setCellStlyeWithType:(NSString *)typeString;

@end
