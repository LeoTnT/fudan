//
//  JYSMyAdvertisingListCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMyAdvertisingListCell.h"

@interface JYSMyAdvertisingListCell ()

/** 小logo */
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
/** BTC */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
/** sell_ads */
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
/** 报价  1CNY */
@property (weak, nonatomic) IBOutlet UILabel *quotationPriceLabel;
/** 交易限额 */
@property (weak, nonatomic) IBOutlet UILabel *tradingLimitsLabel;
/** 创建时间 */
@property (weak, nonatomic) IBOutlet UILabel *buildTimeLabel;
/** 当前状态 进行中 */
@property (weak, nonatomic) IBOutlet UILabel *currentStatus;
/** 银行卡图标 */
@property (weak, nonatomic) IBOutlet UIImageView *bankCardImage;
/** 微信图标 */
@property (weak, nonatomic) IBOutlet UIImageView *weChatImage;
/** 支付宝图标 */
@property (weak, nonatomic) IBOutlet UIImageView *zhiFuBaoImage;
/** 微信左约束 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *weChatLeftConstraint;
/** 支付宝左约束 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *zfbLeftConstraint;

@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *shelfButton;

@end

@implementation JYSMyAdvertisingListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.weChatLeftConstraint.constant = FontNum(48);
    self.zfbLeftConstraint.constant = FontNum(100);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellStlyeWithType:(NSString *)typeString {
    if ([typeString integerValue] == 1) {
        [self.shelfButton setTitle:Localized(@"undercarriage") forState:UIControlStateNormal];
    } else {
        [self.shelfButton setTitle:Localized(@"up_self") forState:UIControlStateNormal];
    }
}

@end
