//
//  JYSMyAdvertisingDropDownView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^JYSMyAdvertisingDropDownBlock)(NSString * title,NSUInteger selectedIndex);

@interface JYSMyAdvertisingDropDownView : UIView

- (void)setTopMenuData:(NSArray *)topMenuDatas selectIndex:(NSUInteger)selectIndex dropDownBlock:(JYSMyAdvertisingDropDownBlock)block;

-(void)showView;

-(void)hideView;

- (void)initialHideMenuView;

@end

@interface JYSMyAdvertisingDropDownCell : UITableViewCell

- (void)setCoinTitleWith:(NSString *)title;

@end
