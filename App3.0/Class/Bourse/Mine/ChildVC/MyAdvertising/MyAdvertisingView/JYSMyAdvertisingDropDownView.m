//
//  JYSMyAdvertisingDropDownView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMyAdvertisingDropDownView.h"

@interface JYSMyAdvertisingDropDownView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,copy)JYSMyAdvertisingDropDownBlock dropDownBlock;

/** 下拉tableview */
@property (nonatomic, strong) UITableView * dropDownTableView;

/** titlesArray */
@property (nonatomic, strong) NSArray * topMenuDatasArray;
/** 选中行 */
@property (nonatomic, assign) NSUInteger selectedIndex;

@end

@implementation JYSMyAdvertisingDropDownView

- (UITableView *)dropDownTableView {
    if (_dropDownTableView == nil) {
        _dropDownTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _dropDownTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _dropDownTableView.showsVerticalScrollIndicator = NO;
        _dropDownTableView.backgroundColor = [UIColor whiteColor];
        _dropDownTableView.delegate = self;
        _dropDownTableView.dataSource = self;
    }
    return _dropDownTableView;
}

static NSUInteger const rowHeight = 44;//行高
static NSUInteger const maxRows = 7;//最多一屏显示行数

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

static NSString * const cellID = @"JYSMyAdvertisingDropDownCellID";
-(void)setUpUI {
    self.backgroundColor = XSYCOLOR_a(0x111111, 0.6);
    [self addSubview:self.dropDownTableView];
    
    [self.dropDownTableView registerClass:[JYSMyAdvertisingDropDownCell class] forCellReuseIdentifier:cellID];
    [self.dropDownTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.height.mas_equalTo(FontNum(308));
    }];
    
    self.hidden = YES;
}

- (void)setTopMenuData:(NSArray *)topMenuDatas selectIndex:(NSUInteger)selectIndex dropDownBlock:(JYSMyAdvertisingDropDownBlock)block {
    self.topMenuDatasArray = topMenuDatas;
    self.selectedIndex = selectIndex;
    self.dropDownBlock = block;
    
    [self.dropDownTableView reloadData];
    
    [self.dropDownTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.dropDownBlock) {
        NSString * titleString = self.topMenuDatasArray[self.selectedIndex];
        self.dropDownBlock(titleString, self.selectedIndex);
    }
    [self hideView];
}

-(void)showView {
    self.alpha = 1;
    self.hidden = NO;
    NSUInteger count = self.topMenuDatasArray.count;
    if (count > maxRows) {
        count = maxRows;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self);
            make.height.mas_equalTo(count * FontNum(rowHeight));
        }];
        [self layoutIfNeeded];
    }];
}

-(void)hideView {
    [UIView animateWithDuration:0.3 animations:^{
        [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self);
            make.height.mas_equalTo(CGFLOAT_MIN);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
        }];
        [self layoutIfNeeded];
    }];
}

- (void)initialHideMenuView {
    [self.dropDownTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.height.mas_equalTo(CGFLOAT_MIN);
    }];
    self.alpha = 0;
    self.hidden = YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.topMenuDatasArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSMyAdvertisingDropDownCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.topMenuDatasArray.count) {
        [cell setCoinTitleWith:self.topMenuDatasArray[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(44);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedIndex != indexPath.row) {
//        NSIndexPath * selectIndex = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
//        JYSMyAdvertisingDropDownCell * cell = (JYSMyAdvertisingDropDownCell *)[tableView cellForRowAtIndexPath:selectIndex];
//        [cell setSelected:NO animated:YES];
//
//        JYSMyAdvertisingDropDownCell * clickedCell = (JYSMyAdvertisingDropDownCell *)[tableView cellForRowAtIndexPath:indexPath];
//        [clickedCell setSelected:YES animated:YES];
        self.selectedIndex = indexPath.row;
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        if (self.dropDownBlock) {
            NSString * titleString = self.topMenuDatasArray[indexPath.row];
            self.dropDownBlock(titleString, self.selectedIndex);
        }
        [self hideView];
    }
}

@end


@interface JYSMyAdvertisingDropDownCell ()

/** title */
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation JYSMyAdvertisingDropDownCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.titleLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil textAlignment:NSTextAlignmentCenter title:@"BTC"];
    self.titleLabel.font = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.contentView);
    }];
}

- (void)setCoinTitleWith:(NSString *)title {
    self.titleLabel.text = title;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (self.selected) {
        self.titleLabel.textColor = JYSMainSelelctColor;
    } else {
        self.titleLabel.textColor = JYSMainTextColor;
    }
}

@end

