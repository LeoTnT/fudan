//
//  JYSMyAdvertisingViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSMyAdvertisingViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "JYSMyAdvertisingListVC.h"
#import "JYSMyAdvertisingDropDownView.h"

@interface JYSMyAdvertisingViewController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>
/** titleButton */
@property (nonatomic, strong) UIButton * titleButton;
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

/** 下拉选择菜单 */
@property (nonatomic, strong) JYSMyAdvertisingDropDownView * dropDownView;
/** 下拉菜单选中index */
@property (nonatomic, assign) NSUInteger menuSelectedIndex;
/** 菜单数据数组 */
@property (nonatomic, strong) NSArray * topMenus;

@end

@implementation JYSMyAdvertisingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.topMenus = @[@"USDT/QC",@"BTC/QC",@"BCC/QC",@"LTC/QC",@"ETH/QC",@"ETC/QC",@"EOS/QC"];
    self.menuSelectedIndex = 0;
    
    [self setupNavigationBar];
    
    [self setUpUI];
    [self requestData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.dropDownView initialHideMenuView];
}

static CGFloat const space = 5.f;
- (void)setupNavigationBar {
    self.titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.titleButton.frame = CGRectMake(0, 0, FontNum(150), FontNum(20));
    
    [self setTitleBtnData:self.topMenus[self.menuSelectedIndex]];
    
    [self.titleButton addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = self.titleButton;
}

- (void)setTitleBtnData:(NSString *)title {
    [XSUITool setButton:self.titleButton titleColor:JYSMainTextColor titleFont:FontNum(18) backgroundColor:nil image:[UIImage imageNamed:@"mall_down"] backgroundImage:nil forState:UIControlStateNormal title:title];
    
    CGFloat labelWidth = self.titleButton.titleLabel.intrinsicContentSize.width; //注意不能直接使用titleLabel.frame.size.width,原因为有时候获取到0值
    CGFloat imageWidth = self.titleButton.imageView.frame.size.width;
    [XSUITool setButton:self.titleButton contentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter contentVerticalAlignment:UIControlContentVerticalAlignmentCenter imageEdgeInsets:UIEdgeInsetsMake(0, labelWidth+space, 0, -labelWidth-space) titleEdgeInsets:UIEdgeInsetsMake(0, - imageWidth-space, 0,  imageWidth+space)];
    self.titleButton.titleLabel.font = [UIFont systemFontOfSize:FontNum(18) weight:UIFontWeightSemibold];
}

- (void)setUpUI {
    NSArray *titleArr = @[Localized(@"have_in_hand"), Localized(@"has_shelf")];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleFont = [UIFont systemFontOfSize:FontNum(14) weight:UIFontWeightSemibold];
    configure.titleColor = COLOR_999999;
    configure.titleSelectedColor = XSYCOLOR(0x232426);
    configure.indicatorColor = JYSMainSelelctColor;
    configure.indicatorDynamicWidth = FontNum(2);
    /// pageTitleView
    // 这里的 - 10 是为了让 SGPageTitleView 超出父视图，给用户一种效果体验
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) delegate:self titleNames:titleArr configure:configure];
    //    _pageTitleView.backgroundColor = [UIColor clearColor];
    // 对 navigationItem.titleView 的包装，为的是 让View 占据整个视图宽度
    [self.view addSubview:self.pageTitleView];
    
    JYSMyAdvertisingListVC *oneVC = [[JYSMyAdvertisingListVC alloc] init];
    oneVC.view.backgroundColor = [UIColor yellowColor];
    oneVC.typeString = @"1";
    
    JYSMyAdvertisingListVC *twoVC = [[JYSMyAdvertisingListVC alloc] init];
    twoVC.view.backgroundColor = [UIColor orangeColor];
    twoVC.typeString = @"2";
    
    NSUInteger navHeight;
    NSUInteger tabBarHeight;
    if (SCREEN_HEIGHT == 812) {
        navHeight = 88;
        tabBarHeight = 83;
    } else {
        navHeight = 64;
        tabBarHeight = 49;
    }
    
    NSArray *childArr = @[oneVC, twoVC];
    /// pageContentView
    CGFloat contentViewHeight = self.view.frame.size.height - 44-navHeight;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
    
    self.dropDownView = [[JYSMyAdvertisingDropDownView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT, FontNum(308))];
    [self.view addSubview:self.dropDownView];
    [self.dropDownView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
}

- (void)requestData {
    //    NSArray * topMenus = @[@"USDT/QC",@"BTC/QC",@"BCC/QC",@"LTC/QC",@"ETH/QC",@"ETC/QC",@"EOS/QC",@"QTUM/QC",@"HSR/QC",@"BCD/QC",@"DASH/QC",@"SBTC/QC",@"MMTC/QW"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __weak typeof(self) weakSelf = self;
        [self.dropDownView setTopMenuData:self.topMenus selectIndex:self.menuSelectedIndex dropDownBlock:^(NSString *title, NSUInteger selectedIndex) {
            weakSelf.menuSelectedIndex = selectedIndex;
            [weakSelf setTitleBtnData:title];
            weakSelf.titleButton.selected = NO;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [UIView animateWithDuration:0.25 animations:^{
                strongSelf.titleButton.imageView.transform=CGAffineTransformRotate(strongSelf.titleButton.imageView.transform, M_PI);
            }];
        }];
    });
}

- (void)titleBtnClick:(UIButton *)button {
    self.titleButton.selected = !self.titleButton.selected;
    [UIView animateWithDuration:0.25 animations:^{
        self.titleButton.imageView.transform=CGAffineTransformRotate(self.titleButton.imageView.transform, M_PI);
    }];
    
    if (self.titleButton.selected) {
        [self.dropDownView showView];
    } else {
        [self.dropDownView hideView];
    }
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}
@end
