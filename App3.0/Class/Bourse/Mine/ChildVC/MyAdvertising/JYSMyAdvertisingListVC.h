//
//  JYSMyAdvertisingListVC.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface JYSMyAdvertisingListVC : XSBaseTableViewController

/** type */
@property (nonatomic, copy) NSString * typeString;

@end
