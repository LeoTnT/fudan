//
//  JYSRealNameVerificationViewController.m
//  App3.0
//
//  Created by mac on 2018/6/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSRealNameVerificationViewController.h"
#import "JYSRealNameVerificationModel.h"
#import "JYSRealNameVerifyHeaderView.h"
#import "TOCropViewController.h"
#import "XSClipImage.h"
#import "JYSChoosePaperworkView.h"

@interface JYSRealNameVerificationViewController ()<UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate,JYSRealNameVerificationDelegate>
/** 表格 */
@property (nonatomic, strong) UITableView * verificationTableView;

@property (nonatomic, strong) UIImagePickerController *imagePickerVC;
@property (nonatomic, assign) BOOL flag;

/** pickerView */
@property (nonatomic, strong) JYSChoosePaperworkView * paperworkPickerView;

/** 图片数组 */
@property (nonatomic, strong) NSMutableArray * idCardImages;
/** headerView */
@property (nonatomic, strong) JYSRealNameVerifyHeaderView * headerView;
/** 是否是正面照 */
@property (nonatomic, assign) NSUInteger imageIndex;
/** 正面照 */
@property (nonatomic, strong) UIImage * frontImage;
/** 反面照 */
@property (nonatomic, strong) UIImage * behindImage;
/** 手持身份照偏 */
@property (nonatomic, strong) UIImage * handleImage;

/** paperwork */
@property (nonatomic, copy) NSArray * paperworkArray;

/** 证件model */
@property (nonatomic, strong) RealNameVerCardTypeModel * selectCardTypeModel;
@end

@implementation JYSRealNameVerificationViewController

- (JYSChoosePaperworkView *)paperworkPickerView {
    if (_paperworkPickerView == nil) {
        _paperworkPickerView = [[JYSChoosePaperworkView alloc] init];
    }
    return _paperworkPickerView;
}

-(NSMutableArray *)idCardImages
{
    if (!_idCardImages) {
        _idCardImages = [[NSMutableArray alloc] init];
    }
    return _idCardImages;
}

-(UITableView *)verificationTableView
{
    if (!_verificationTableView) {
        _verificationTableView = [[UITableView alloc] init];
        _verificationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _verificationTableView.backgroundColor = BG_COLOR;
        _verificationTableView.delegate = self;
        _verificationTableView.showsVerticalScrollIndicator = NO;
    }
    return _verificationTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    self.paperworkArray = @[@{@"name":@"身份证",@"ID":@"1"},@{@"name":@"护照",@"ID":@"2"},@{@"name":@"驾驶证",@"ID":@"3"}];
    
    self.navigationItem.title = Localized(@"user_approve_suggest_no_btn");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setUpUI];
    [self getVerificationInfoData];
}
#pragma mark UI
- (void)setUpUI {
    self.verificationTableView.frame = self.view.frame;
    [self.view addSubview:self.verificationTableView];
    [self.verificationTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.headerView = [[JYSRealNameVerifyHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FontNum(1095))];
    self.headerView.delegate = self;
    self.verificationTableView.tableHeaderView = self.headerView;
    self.verificationTableView.tableHeaderView.xs_height = FontNum(1095);
    
    [self.view addSubview:self.paperworkPickerView];
    [self.paperworkPickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(FontNum(260));
        make.bottom.mas_equalTo(FontNum(260));
    }];
}

#pragma mark RealNameVerificationDelegate
-(void)choosePaperworkTypeBtnClicked {
    [self showPaperworkPickerView];
}

-(void)frontImageBtnClicked {
    self.imageIndex = 1;
    [self choosePhotoes];
}
-(void)behindImageBtnClicked {
    self.imageIndex = 2;
    [self choosePhotoes];
}

-(void)handleImageBtnClicked {
    self.imageIndex = 3;
    [self choosePhotoes];
}


-(void)upLoadDataWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum {
    [self submitInfoWithRealName:realName IDCardNum:idCardNum];
}

#pragma mark 获取认证进度
- (void)getVerificationInfoData {
    [XSTool showProgressHUDWithView:self.view];
    
    @weakify(self);
    [HTTPManager requestVerificationData:nil success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            XSLog(@"%@",dic);
            JYSRealNameVerificationModel * model = [JYSRealNameVerificationModel mj_objectWithKeyValues:dataDict];
            
            [self setData:model];
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}
#pragma mark 实名认证进度状态
- (void)setData:(JYSRealNameVerificationModel *)model {
    
    //    model.status = @"2";
    
    if ([model.status integerValue] == 0 || [model.status integerValue] == 1) {
        self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(1000));
        self.verificationTableView.tableHeaderView.xs_height = FontNum(1000);
        [self.headerView setFooterViewHide:YES];
    } else {
        self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, FontNum(1095));
        self.verificationTableView.tableHeaderView.xs_height = FontNum(1095);
        [self.headerView setFooterViewHide:NO];
    }
    [self.verificationTableView reloadData];
    [self.headerView setStatus:model];
    
    self.paperworkArray = model.card_types;
    
    __weak typeof(self) weakSelf = self;
    [self.paperworkPickerView setPaperworks:self.paperworkArray determineBlock:^(RealNameVerCardTypeModel * cardTypeModel) {
        //        XSLog(@"%@",paperworkName);
        weakSelf.selectCardTypeModel = cardTypeModel;
        [weakSelf.headerView setPaperworkText:cardTypeModel.card_name];
        
        [weakSelf dismissPaperworkPickerView];
    } cancelBlock:^{
        [weakSelf dismissPaperworkPickerView];
    }];
    
}

#pragma mark scroll tableView滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.headerView.isKeyboardShow) {
        [self.view endEditing:YES];
    }
}

#pragma mark 身份证图片选择与上传
- (void)choosePhotoes{
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //打开相册
        UIImagePickerController * imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        imagePickVC.delegate = self;
        imagePickVC.allowsEditing = NO;
        self.imagePickerVC = imagePickVC;
        
        self.flag = NO;
        
        [self presentViewController:imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)takeAPhoto {
    UIImagePickerController * imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    imagePickVC.delegate=self;
    self.imagePickerVC = imagePickVC;
    [self presentViewController:imagePickVC animated:YES completion:^{
        self.flag = YES;
    }];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (self.flag) {
        //获取原始照片
        UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        //保存图片到相册
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
        toVC.delegate=self;
        
        //        [picker pushViewController:toVC animated:YES];
        
        [picker presentViewController:toVC animated:NO completion:nil];
        
    } else {
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"]) {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
            toVC.delegate=self;
            [picker pushViewController:toVC animated:YES];
        }
    }
}

#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.flag) {
        //        [self.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
            //            XSLog(@"retain  count = %ld\n",CFGetRetainCount((__bridge  CFTypeRef)(weakSelf.imagePickerVC)));
            [weakSelf.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        }];
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [self dismissViewControllerAnimated:YES completion:^{
        
        UIImage *tempImage;
        NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
        if (imageData.length>=1024*1024*2) {//2M以及以上
            if (imageData.length<1024*1024*4) {
                imageData = UIImageJPEGRepresentation(image, 0.4);
            } else if (imageData.length<1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.2);
                
            } if (imageData.length>1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.1);
                
            }
            tempImage = [UIImage imageWithData:imageData];
        } else {
            tempImage = [UIImage imageWithData:imageData];
            
        }
        
        if (self.imageIndex == 1) {
            self.frontImage = tempImage;
            [self.headerView setFrontImage:self.frontImage];
        } else if (self.imageIndex == 2) {
            self.behindImage = tempImage;
            [self.headerView setBehindImage:self.behindImage];
        } else if (self.imageIndex == 3) {
            self.handleImage = tempImage;
            [self.headerView setHandleImage:self.handleImage];
        }
    }];
}

#pragma mark 提交实名信息数据
- (void)submitInfoWithRealName:(NSString *)realName IDCardNum:(NSString *)idCardNum {
    if (self.idCardImages.count) {
        [self.idCardImages removeAllObjects];
    }
    if (self.frontImage) {
        [self.idCardImages addObject:self.frontImage];
    }
    if (self.behindImage) {
        [self.idCardImages addObject:self.behindImage];
    }
    if (self.handleImage) {
        [self.idCardImages addObject:self.handleImage];
    }
    
    if (self.idCardImages.count >= 2 && realName.length > 0 && idCardNum.length > 0) {
        
        if (!self.selectCardTypeModel.card_id) {
            [XSTool showToastWithView:self.view Text:@"请选择证件类型"];
            return;
        }
        
        NSDictionary *param=@{@"type":@"idcard",@"formname":@"file"};
        NSMutableString *tempImageString=[NSMutableString string];
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        [HTTPManager upLoadPhotosWithDic:param andDataArray:self.idCardImages WithSuccess:^(NSDictionary *dic, BOOL state) {
            @strongify(self);
            if([dic[@"status"] integerValue]==1){//发表
                for (NSString *str in dic[@"data"]) {
                    [tempImageString appendString:[NSString stringWithFormat:@"%@,",str]];
                }
                NSString  *submitImageString = [tempImageString substringToIndex:tempImageString.length-1];
                //提交认证
                NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                NSString *nameString = [[NSString alloc]initWithString:[realName stringByTrimmingCharactersInSet:whiteSpace]];
                NSString *numberString = [[NSString alloc]initWithString:[idCardNum stringByTrimmingCharactersInSet:whiteSpace]];
                [HTTPManager submitIdentityCarInfoWithImageString:submitImageString trueName:nameString cardType:self.selectCardTypeModel.card_id card:numberString  Success:^(NSDictionary * _Nullable dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [XSTool showToastWithView:self.view Text:state.info];
                        [self performSelector:@selector(popViewController) withObject:self afterDelay:0.5];
                        
                    } else {
                        [XSTool showToastWithView:self.view Text:state.info];
                    }
                } failure:^(NSError * _Nonnull error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:NetFailure];
                }];
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:NetFailure];
        }];
    } else {
        [XSTool showToastWithView:self.view Text:@"请完善认证信息"];
    }
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showPaperworkPickerView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.paperworkPickerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.view).offset(0);
        }];
        [self.view layoutIfNeeded];
    }];
}

- (void)dismissPaperworkPickerView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.paperworkPickerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.view).offset(FontNum(260));
        }];
        [self.view layoutIfNeeded];
    }];
}
@end
