//
//  JYSSecurityViewController.m
//  App3.0
//
//  Created by xinshang on 2018/6/30.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSSecurityViewController.h"

#import "JYSAssetManagementModel.h"
#import "BindMobileViewController.h"
#import "BindEmailViewController.h"
#import "VeticalGoogleViewController.h"
#import "InputAlertNewTextFieldView.h"

@interface JYSSecurityViewController ()<InputAlertNewTextFieldViewDelegate>
{
    BOOL _isTimer;// 是否在倒计时
    BOOL _isTimer_email;// 是否在倒计时
    
}
@property (nonatomic ,strong) dispatch_source_t timer;
@property (nonatomic ,strong) dispatch_source_t timer_email;

@property (nonatomic,strong) NSArray *dataArr;
@property (nonatomic, strong) UISwitch *codeSwitch;
@property (nonatomic, strong) UISwitch *googleSwitch;
@property (nonatomic, strong) UISwitch *emailSwitch;

@property (nonatomic, strong) JYSVerticalWithdrawModel *verticalModel;
@property(nonatomic,strong)  InputAlertNewTextFieldView *alertNewView;
@property (nonatomic,copy) NSString *type;

@end

@implementation JYSSecurityViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:Localized(@"bc_security_verify_title") isShowBack:YES];
    
    self.view.backgroundColor = BG_COLOR;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
        self.dataArr = @[Localized(@"security_sms_open"),Localized(@"security_google_open"),Localized(@"security_email_open")];
//    self.dataArr = @[@"短信验证",@"谷歌验证"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.frame = CGRectMake(0, 0, mainWidth, mainHeight);
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = BG_COLOR;
    [self GetWithdrawVerify];
}


- (void)GetWithdrawVerify
{
    
    __weak __typeof__(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager GetWithdrawVerifySuccess:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             if ([dic[@"data"] isKindOfClass:[NSDictionary class]]) {
                 self.verticalModel = [JYSVerticalWithdrawModel mj_objectWithKeyValues:dic[@"data"]];
             }
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
    
}


#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor blackColor];
//        cell.backgroundColor = mainColor;
    }
    NSString *textStr = [NSString stringWithFormat:@"%@",self.dataArr[indexPath.section]];
    cell.textLabel.text = textStr;
    if ([textStr isEqualToString:Localized(@"security_sms_open")]) {
        [cell addSubview:self.codeSwitch];
        [self.codeSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.mas_equalTo(cell);
        }];
        if (self.verticalModel.show_mobile.intValue) {
            [self.codeSwitch setOn:YES];
        }else{
            [self.codeSwitch setOn:NO];
        }
    }else if ([textStr isEqualToString:Localized(@"security_google_open")]) {
        [cell addSubview:self.googleSwitch];
        [self.googleSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.mas_equalTo(cell);
        }];
        
        if (self.verticalModel.show_google.intValue) {
            [self.googleSwitch setOn:YES];
        }else{
            [self.googleSwitch setOn:NO];
        }
        
    }else if ([textStr isEqualToString:Localized(@"security_email_open")]) {
        [cell addSubview:self.emailSwitch];
        [self.emailSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.mas_equalTo(cell);
        }];
        if (self.verticalModel.show_email.intValue) {
            [self.emailSwitch setOn:YES];
        }else{
            [self.emailSwitch setOn:NO];
        }
    }
    
    
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = BG_COLOR;
    
    return view;
}

-(UISwitch *)codeSwitch
{
    if (!_codeSwitch) {
        _codeSwitch = [[UISwitch alloc] init];
        [_codeSwitch addTarget:self action:@selector(codeSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
        CGRect frame = _codeSwitch.frame;
        frame.origin.x = self.view.frame.size.width - 10 - frame.size.width;
        frame.origin.y = 10;
        _codeSwitch.frame = frame;
    }
    return _codeSwitch;
}

-(UISwitch *)googleSwitch
{
    if (!_googleSwitch) {
        _googleSwitch = [[UISwitch alloc] init];
        [_googleSwitch addTarget:self action:@selector(googleSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
        CGRect frame = _googleSwitch.frame;
        frame.origin.x = self.view.frame.size.width - 10 - frame.size.width;
        frame.origin.y = 10;
        _googleSwitch.frame = frame;
    }
    return _googleSwitch;
}

-(UISwitch *)emailSwitch
{
    if (!_emailSwitch) {
        _emailSwitch = [[UISwitch alloc] init];
        [_emailSwitch addTarget:self action:@selector(emailSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
        CGRect frame = _emailSwitch.frame;
        frame.origin.x = self.view.frame.size.width - 10 - frame.size.width;
        frame.origin.y = 10;
        _emailSwitch.frame = frame;
    }
    return _emailSwitch;
}


#pragma mark - Action
//'mobile','email','google'
- (void)codeSwitchValueChanged:(UISwitch *)control
{
    self.type = @"mobile";
    if (self.verticalModel.is_mobile_verify.intValue <1) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"请开启手机认证后继续操作!") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            BindMobileViewController *vc =[[BindMobileViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action1];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        if (isEmptyString(self.verticalModel.mobile)) {
            Alert(@"获取手机号失败!");
            return;
        }
        [self showAlertWithMobile:self.verticalModel.mobile email:@"" google:NO psw:NO];
    }
    
    [self.tableView reloadData];
    
}
- (void)googleSwitchValueChanged:(UISwitch *)control
{
    self.type = @"google";
    
    if (self.verticalModel.is_google_verify.intValue <1) {
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"请开启谷歌认证后继续操作!") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            VeticalGoogleViewController *vc =[[VeticalGoogleViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action1];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        [self showAlertWithMobile:@"" email:@"" google:YES psw:NO];
        
    }
    [self.tableView reloadData];
    
}
- (void)emailSwitchValueChanged:(UISwitch *)control
{
    self.type = @"email";
    
    if (self.verticalModel.is_email_verify.intValue <1) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"请开启邮箱认证后继续操作!") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            BindEmailViewController *vc =[[BindEmailViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action1];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        if (isEmptyString(self.verticalModel.email)) {
            Alert(@"获取邮箱号失败!");
            return;
        }
        [self showAlertWithMobile:@"" email:self.verticalModel.email google:NO psw:NO];
    }
    [self.tableView reloadData];
    
}
-(void)cancelBtnCileck
{
    [self.tableView reloadData];
}

- (void)showAlertWithMobile:(NSString *)mobile email:(NSString *)email google:(BOOL)isGoogle psw:(BOOL)isPsw
{
    self.alertNewView = [[InputAlertNewTextFieldView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) mobile:mobile  email:email isGoogle:isGoogle isPayPSW:isPsw];
    self.alertNewView.delegate = self;
    [[UIApplication sharedApplication].delegate.window addSubview:self.alertNewView];
}

-(void)confirmBtnCileck:(NSString *)codeText email_code:(NSString *)email_code google:(NSString *)google psw:(NSString *)psw
{
    __weak __typeof__(self) wSelf = self;
    if (isEmptyString(self.type)) {
        Alert(@"获取验证类型失败!");
        return;
    }
    NSString *verify =@"";
    if ([self.type isEqualToString:@"mobile"]) {
        verify = codeText;
    }else if ([self.type isEqualToString:@"google"]) {
        verify = google;
    }
    else if ([self.type isEqualToString:@"email"]) {
        verify = email_code;
    }
    if (isEmptyString(verify)) {
        if ([self.type isEqualToString:@"mobile"]) {
            Alert(@"请输入手机验证码");
            return;
        }else if ([self.type isEqualToString:@"google"]) {
            Alert(Localized(@"cc_sms_ingoogle_hint"));
            return;
        }else if ([self.type isEqualToString:@"email"]) {
            Alert(Localized(@"_send_dialog_sms_hint"));
            return;
        }
        
    }
    [XSTool showProgressHUDWithView:self.view];
    

    
    [HTTPManager setWithdrawVerifyWithDic:@{@"type":self.type,@"verify":verify}
                             success:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:wSelf.view];
         if (state.status) {
             if (state.info) {
                 Alert(state.info);
             }else{
                 Alert(@"设置成功!");
             }
             [self GetWithdrawVerify];
         }else{
             Alert(state.info);
         }
         [wSelf.tableView reloadData];
         
     } fail:^(NSError *error) {
         [XSTool hideProgressHUDWithView:wSelf.view];
         Alert(NetFailure);
     }];
}



//验证码
-(void)codeBtnSelected:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (isEmptyString(self.verticalModel.mobile)) {
        Alert(@"获取用户手机号失败");
        return;
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [XSTool showProgressHUDWithView:self.alertNewView];
    [HTTPManager getSmsVerifyWithDic:@{@"mobile":self.verticalModel.mobile,@"type":@"9"}
                             success:^(NSDictionary *dic, resultObject *state) {
                                 [XSTool hideProgressHUDWithView:self.alertNewView];
                                 // 验证码发送成功
                                 if (state.status) {
                                     [XSTool showToastWithView:self.alertNewView Text:@"发送成功"];
                                     _isTimer = YES; // 设置倒计时状态为YES
                                     sender.enabled = NO; // 设置按钮为不可点击
                                     
                                     _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                                     [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer walltime:59 stop:^{
                                         //设置按钮的样式
                                         dispatch_source_cancel(_timer);
                                         [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                                         sender.enabled = YES; // 设置按钮可点击
                                         
                                         _isTimer = NO; // 倒计时状态为NO
                                     } otherAction:^(int time) {
                                         [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
                                     }];
                                     
                                 } else {
                                     if (state.info.length>0) {
                                         [XSTool showToastWithView:self.alertNewView Text:state.info];
                                     }
                                     sender.enabled = YES; // 设置按钮为可点击
                                 }
                             } fail:^(NSError *error)
     {
         [XSTool showToastWithView:self.alertNewView Text:@"验证码发送失败"];
         sender.enabled = YES; // 设置按钮为可点击
         [XSTool hideProgressHUDWithView:self.alertNewView];
     }];
    
}

//邮箱验证码
-(void)emailBtnSelected:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (isEmptyString(self.verticalModel.email)) {
        Alert(@"获取邮箱号失败");
        return;
    }
    
    sender.enabled = NO; // 设置按钮为不可点击
    // 获取验证码
    [XSTool showProgressHUDWithView:self.alertNewView];
    [HTTPManager getEmailVerifyWithDic:@{@"email":self.verticalModel.email,@"type":@"4"}
                               success:^(NSDictionary *dic, resultObject *state){
                                   [XSTool hideProgressHUDWithView:self.alertNewView];
                                   // 验证码发送成功
                                   if (state.status) {
                                       [XSTool showToastWithView:self.alertNewView Text:@"发送成功"];
                                       _isTimer_email = YES; // 设置倒计时状态为YES
                                       sender.enabled = NO; // 设置按钮为不可点击
                                       
                                       _timer_email = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
                                       [XSTool setTimeQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) dispatch_source:_timer_email walltime:59 stop:^{
                                           //设置按钮的样式
                                           dispatch_source_cancel(_timer_email);
                                           [sender setTitle:Localized(@"重新发送") forState:UIControlStateNormal];
                                           sender.enabled = YES; // 设置按钮可点击
                                           
                                           _isTimer_email = NO; // 倒计时状态为NO
                                       } otherAction:^(int time) {
                                           [sender setTitle:[NSString stringWithFormat:@"%@(%.2d)",Localized(@"重新发送"), time] forState:UIControlStateNormal];
                                       }];
                                       
                                   } else {
                                       if (state.info.length>0) {
                                           [XSTool showToastWithView:self.alertNewView Text:state.info];
                                       }
                                       sender.enabled = YES; // 设置按钮为可点击
                                   }
                               } fail:^(NSError *error)
     {
         [XSTool hideProgressHUDWithView:self.alertNewView];
         [XSTool showToastWithView:self.alertNewView Text:@"验证码发送失败"];
         sender.enabled = YES; // 设置按钮为可点击
     }];
}

@end


