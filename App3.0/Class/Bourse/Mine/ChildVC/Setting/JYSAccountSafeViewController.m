//
//  JYSAccountSafeViewController.m
//  App3.0
//
//  Created by 沈浩 on 2018/6/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSAccountSafeViewController.h"
#import "BindAccountViewController.h"
#import "AccountBindingViewController.h"//账号绑定
#import "BindBankCardViewController.h"
#import "ModifyPassWordViewController.h"
#import "VertifyViewController.h"
#import "JYSRealNameVerificationViewController.h"
#import "VeticalGoogleViewController.h"
#import "BindEmailViewController.h"
#import "JYSSecurityViewController.h"

@interface JYSAccountSafeViewController ()
@property (nonatomic, strong) NSMutableArray *contentArray;
@property (nonatomic, strong) UserParser *tUserInfo;

@end

@implementation JYSAccountSafeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //账号与安全
    self.navigationItem.title = Localized(@"account_safe");
    self.contentArray = [NSMutableArray arrayWithArray:@[Localized(@"setting_tv_baccount"),Localized(@"setting_tv_pwd"),Localized(@"user_approve_suggest_no_btn"),Localized(@"setting_tv_google"),Localized(@"bc_security_verify_title")]];
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getInfo];
}
-(void)getInfo{
    [XSTool showProgressHUDWithView:self.view];
        [HTTPManager getUserInfoSuccess:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                UserParser *tUserInfo = [UserParser mj_objectWithKeyValues:dic];
                [UserInstance ShardInstnce].userInfo = tUserInfo.data;
            }
            [self .tableView reloadData];
        } failure:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
        }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contentArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idString = @"installCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idString];
    if (cell==nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idString];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    }
    cell.textLabel.text = self.contentArray[indexPath.row];
    
    if (indexPath.row == 2) {//实名认证
        if ([UserInstance ShardInstnce].userInfo.approve_user.intValue ==1) {
            cell.detailTextLabel.text =Localized(@"已认证");
        }
        
    }else if (indexPath.row == 3) {//google认证
        if ([UserInstance ShardInstnce].userInfo.is_google_verify.intValue ==1) {
            cell.detailTextLabel.text =Localized(@"已认证");
        }
        
    }else{
        cell.detailTextLabel.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *VC;
    switch (indexPath.row) {
        case 0:
        {
            //账号绑定
            VC = [[AccountBindingViewController alloc] init];
        }
            break;
        case 1:
        {
            //修改密码
            VC = [[ModifyPassWordViewController alloc] init];
        }
            break;
        case 2:
        {
            //实名认证
            VC = [[JYSRealNameVerificationViewController alloc] init];
        }
            break;
        case 3:{
            //谷歌认证
            VC = [[VeticalGoogleViewController alloc] init];
        }
            break;
        case 4:{
            //开启或关闭安全验证
            VC = [[JYSSecurityViewController alloc] init];
        }
            break;
        default:
            break;
    }
    [self.navigationController pushViewController:VC animated:YES];
}
@end
