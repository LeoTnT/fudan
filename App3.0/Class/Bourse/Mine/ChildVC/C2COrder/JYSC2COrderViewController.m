//
//  JYSC2COrderViewController.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSC2COrderViewController.h"
#import <SGPagingView/SGPagingView.h>
#import "JYSC2COrderListVC.h"
@interface JYSC2COrderViewController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>

@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

@end

@implementation JYSC2COrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = Localized(@"c2corder");
    
    [self setUpUI];
    
    [self requestData];
}

- (void)requestData {
    [JYSAFNetworking getOrPostWithType:POST withUrl:@"/api/v1/basic/product/Lists" params:@{@"page":@"1",@"recommend":@1,@"_order":@"rand"} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        XSLog(@"%@",dic);
    } fail:^(NSError *error) {
        
    } showHUD:YES];
}

- (void)setUpUI {
    NSArray *titleArr = @[Localized(@"alls"), Localized(@"no_pay"),Localized(@"have_pay"), Localized(@"appealling"),Localized(@"has_been_cancelled")];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleFont = [UIFont systemFontOfSize:FontNum(14) weight:UIFontWeightSemibold];
    configure.titleColor = COLOR_999999;
    configure.titleSelectedColor = XSYCOLOR(0x232426);
    configure.indicatorColor = JYSMainSelelctColor;
    configure.indicatorDynamicWidth = FontNum(2);
    /// pageTitleView
    // 这里的 - 10 是为了让 SGPageTitleView 超出父视图，给用户一种效果体验
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) delegate:self titleNames:titleArr configure:configure];
    //    _pageTitleView.backgroundColor = [UIColor clearColor];
    // 对 navigationItem.titleView 的包装，为的是 让View 占据整个视图宽度
    [self.view addSubview:self.pageTitleView];
    
    JYSC2COrderListVC *oneVC = [[JYSC2COrderListVC alloc] init];
    oneVC.view.backgroundColor = [UIColor yellowColor];
    
    JYSC2COrderListVC *twoVC = [[JYSC2COrderListVC alloc] init];
    twoVC.view.backgroundColor = [UIColor orangeColor];
    
    JYSC2COrderListVC *threeVC = [[JYSC2COrderListVC alloc] init];
    threeVC.view.backgroundColor = [UIColor purpleColor];
    
    JYSC2COrderListVC *fourVC = [[JYSC2COrderListVC alloc] init];
    fourVC.view.backgroundColor = [UIColor greenColor];
    
    JYSC2COrderListVC *fiveVC = [[JYSC2COrderListVC alloc] init];
    fiveVC.view.backgroundColor = [UIColor redColor];
    
    NSUInteger navHeight;
    NSUInteger tabBarHeight;
    if (SCREEN_HEIGHT == 812) {
        navHeight = 88;
        tabBarHeight = 83;
    } else {
        navHeight = 64;
        tabBarHeight = 49;
    }
    
    NSArray *childArr = @[oneVC, twoVC, threeVC, fourVC,fiveVC];
    /// pageContentView
    CGFloat contentViewHeight = self.view.frame.size.height - 44-navHeight;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

@end
