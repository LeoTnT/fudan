//
//  JYSC2COrderListCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSC2COrderListCell.h"

@interface JYSC2COrderListCell ()

/** logo */
@property (nonatomic, strong) UIImageView * logoImageView;
/** name */
@property (nonatomic, strong) UILabel * nameLabel;
/** 时间 */
@property (nonatomic, strong) UILabel * timeLabel;
/** 交易金额 */
@property (nonatomic, strong) UILabel * transactionAmountLabel;
/** BTC */
@property (nonatomic, strong) UILabel * coinLabel;

/** 状态-未付款、已付款等 */
@property (nonatomic, strong) UILabel * statusLabel;

@end

@implementation JYSC2COrderListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.backgroundColor = BG_COLOR;
    
    UIView * bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 15));
    }];
    
    UIImageView * bgImageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"jys_cell_bgImage"]];
    [self.contentView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-2, 2, -5, 2));
    }];
    
    self.logoImageView = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"user_fans_avatar"]];
    [XSUITool setImageView:self.logoImageView cornerRadius:FontNum(20) borderWidth:0 borderColor:nil];
    [self.contentView addSubview:self.logoImageView];
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(12);
        make.bottom.mas_equalTo(bgView.mas_centerY).multipliedBy(0.9);
        make.size.mas_equalTo(CGSizeMake(FontNum(40), FontNum(40)));
    }];
    
    self.nameLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(17) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"- -"];
    self.nameLabel.font = [UIFont systemFontOfSize:FontNum(17) weight:UIFontWeightSemibold];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logoImageView.mas_right).offset(11);
        make.centerY.mas_equalTo(self.logoImageView);
    }];
    
    UILabel * transactionAmountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"trade_mony")];
    [self.contentView addSubview:transactionAmountLab];
    [transactionAmountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logoImageView);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.15);
    }];
    
    self.transactionAmountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"1.99999999 CNY"];
    [self.contentView addSubview:self.transactionAmountLabel];
    [self.transactionAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(transactionAmountLab.mas_right).offset(10);
        make.centerY.mas_equalTo(transactionAmountLab);
    }];
    
    self.timeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2018-03-24 16:12"];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logoImageView);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.5);
    }];
    
    UILabel * buyLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:[UserInstance ShardInstnce].roseColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"buy")];
    [self.contentView addSubview:buyLab];
    
    self.coinLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"BTC"];
    [self.contentView addSubview:self.coinLabel];
    [self.coinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-13);
        make.centerY.mas_equalTo(self.logoImageView);
    }];
    
    [buyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.coinLabel.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.logoImageView);
    }];
    
    self.statusLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainSelelctColor titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"no_pay")];
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-13);
        make.centerY.mas_equalTo(transactionAmountLab);
    }];
    
}

@end
