//
//  JYSEntrustOrdersCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSEntrustOrdersCell.h"

@interface JYSEntrustOrdersCell ()

/** 状态按钮 买or卖 */
@property (nonatomic, strong) UIButton * typeBtn;

/** 币名 */
@property (nonatomic, strong) UILabel * coinNameLabel;
/** 委托数量 */
@property (nonatomic, strong) UILabel * amountLabel;
/** 已成交量 */
@property (nonatomic, strong) UILabel * transactionVolumeLabel;
/** 单价 */
@property (nonatomic, strong) UILabel * priceLabel;
/** 时间 */
@property (nonatomic, strong) UILabel * timeLabel;
/** 撤单按钮 */
@property (nonatomic, strong) UIButton * cancelButton;

@end

@implementation JYSEntrustOrdersCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
//    self.backgroundColor = BG_COLOR;
    
    UIView * bgView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 15));
    }];
    
    UIImageView * bgImageV = [XSUITool creatImageViewWithFrame:CGRectZero backgroundColor:nil image:[UIImage imageNamed:@"jys_cell_bgImage"]];
    [self.contentView addSubview:bgImageV];
    [bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-2, 2, -5, 2));
    }];
    
    NSString * rosebgImageNameString;
    NSString * fellbgImageNameString;
    if ([[UserInstance ShardInstnce].roseColorString isEqualToString:JYSRedString]) {
        rosebgImageNameString = @"entrustmentRecords_tag_buy";
        fellbgImageNameString = @"entrustmentRecords_tag_sell";
    } else {
        rosebgImageNameString = @"entrustmentRecords_tag_sell";
        fellbgImageNameString = @"entrustmentRecords_tag_buy";
    }
    
    self.typeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [XSUITool setButton:self.typeBtn titleColor:[UIColor whiteColor] titleFont:12 backgroundColor:nil image:nil backgroundImage:[UIImage imageNamed:rosebgImageNameString] forState:UIControlStateNormal title:Localized(@"buyone")];
    [XSUITool setButton:self.typeBtn titleColor:[UIColor whiteColor] titleFont:12 backgroundColor:nil image:nil backgroundImage:[UIImage imageNamed:fellbgImageNameString] forState:UIControlStateSelected title:Localized(@"sell")];
    self.typeBtn.userInteractionEnabled = NO;
    [XSUITool setButton:self.typeBtn contentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft contentVerticalAlignment:UIControlContentVerticalAlignmentTop imageEdgeInsets:UIEdgeInsetsZero titleEdgeInsets:UIEdgeInsetsMake(2, 2, 0, 0)];
    
    [self.contentView addSubview:self.typeBtn];
    [self.typeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(FontNum(31), FontNum(31)));
    }];
    
    UILabel * coinTypeLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"orders_bType_title")];
    [self.contentView addSubview:coinTypeLab];
    [coinTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).offset(13);
        make.baseline.mas_equalTo(bgView.mas_centerY).multipliedBy(0.6);
    }];
    
    UILabel * amountLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"order_weituo_count")];
    [self.contentView addSubview:amountLab];
    [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(coinTypeLab);
        make.centerY.mas_equalTo(bgView);
    }];
    
    UILabel * priceLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"orders_price_title")];
    [self.contentView addSubview:priceLab];
    
    self.priceLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.19000000"];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView).offset(-13);
        make.centerY.mas_equalTo(coinTypeLab);
    }];
    
    [priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.priceLabel.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.priceLabel);
    }];
    
    self.coinNameLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"ETH/BTC"];
    [self.contentView addSubview:self.coinNameLabel];
    [self.coinNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(coinTypeLab.mas_right).offset(10);
        make.centerY.mas_equalTo(coinTypeLab);
    }];
    
    self.amountLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"80.21000000"];
    [self.contentView addSubview:self.amountLabel];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(amountLab.mas_right).offset(10);
        make.centerY.mas_equalTo(amountLab);
    }];
    
    self.transactionVolumeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:JYSMainTextColor titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"0.00000000"];
    [self.contentView addSubview:self.transactionVolumeLabel];
    [self.transactionVolumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.priceLabel);
        make.centerY.mas_equalTo(amountLab);
    }];
    
    UILabel * transactionVolumeLab = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(14) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:Localized(@"order_finish_count")];
    [self.contentView addSubview:transactionVolumeLab];
    [transactionVolumeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.transactionVolumeLabel.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.transactionVolumeLabel);
    }];
    
    self.timeLabel = [XSUITool creatLabelWithFrame:CGRectZero titleColor:COLOR_999999 titleFont:FontNum(13) backgroundColor:nil textAlignment:NSTextAlignmentLeft title:@"2018-03-24 16:12"];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(coinTypeLab);
        make.top.mas_equalTo(bgView.mas_centerY).multipliedBy(1.48);
    }];
    
    self.cancelButton = [XSUITool buttonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:nil action:nil titleColor:[UIColor whiteColor] titleFont:FontNum(13) backgroundColor:JYSMainSelelctColor image:nil backgroundImage:nil title:Localized(@"order_cancel_btn")];
    //    [self.cancelButton setBackgroundColor:XSYCOLOR(0x6A8EBD)];
    self.cancelButton.userInteractionEnabled = NO;
    [self.contentView addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bgView.mas_right).offset(-12);
        make.centerY.mas_equalTo(self.timeLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FontNum(60), FontNum(27)));
    }];
}

@end
