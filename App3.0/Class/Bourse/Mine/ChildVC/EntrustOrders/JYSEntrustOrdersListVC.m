//
//  JYSEntrustOrdersListVC.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSEntrustOrdersListVC.h"
#import "JYSEntrustOrdersCell.h"
@interface JYSEntrustOrdersListVC ()

/** 页码 */
@property (nonatomic, assign) NSUInteger page;
/** 数据源数组 */
@property (nonatomic, strong) NSMutableArray * dataArray;

@end

@implementation JYSEntrustOrdersListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    
    [self downRefresh];
    [self upRefresh];
}

static NSString * const cellID = @"JYSEntrustOrdersCellID";
- (void)setUpUI {
    self.tableViewStyle = UITableViewStylePlain;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView registerClass:[JYSEntrustOrdersCell class] forCellReuseIdentifier:cellID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
}

#pragma mark 下拉刷新
- (void)downRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshHeader:^{
        weakSelf.page = 1;
        [weakSelf requestOrderList];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)upRefresh {
    __weak typeof(self) weakSelf = self;
    [self setRefreshFooter:^{
        weakSelf.page++;
        [weakSelf requestOrderList];
    }];
    self.tableView.mj_footer.hidden = YES;
}

-(void)endRefresh
{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestOrderList {
    NSString * pageString = [NSString stringWithFormat:@"%ld",self.page];
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    
    parameters[@"page"] = pageString;
    parameters[@"state"] = self.stateString;
    
    [JYSAFNetworking getOrPostWithType:POST withUrl:JYSEntrustOrderURL params:parameters HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
        [self endRefresh];
        if (state.status) {
            NSDictionary * dataDict = dic[@"data"];
            NSDictionary * listDict = dataDict[@"list"];
            NSArray * dataArr = listDict[@"data"];
            if (dataArr.count < [listDict[@"per_page"] integerValue]) {
                self.tableView.mj_footer.hidden = YES;
            } else {
                self.tableView.mj_footer.hidden = NO;
            }
            
//            NSArray * datas = []
            
        }
    } fail:^(NSError *error) {
        [self endRefresh];
    } showHUD:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYSEntrustOrdersCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FontNum(135);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
