//
//  JYSEntrustOrdersListVC.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"
@interface JYSEntrustOrdersListVC : XSBaseTableViewController

/** 0|null:全部，1:交易中，2:已成交，3:已撤销 */
@property (nonatomic, copy) NSString * stateString;

@end
