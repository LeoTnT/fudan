//
//  JYSConst.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark 接口
/** 交易所行情头部菜单 */
extern NSString *const JYSCurrencyTitlesURL;

/** 资产管理 */
extern NSString * const JYSAssetManagementURL;

/** 委托订单列表 */
extern NSString * const JYSEntrustOrderURL;

/** 货币下拉列表页 */
extern NSString * const JYSPullDownMenuURL;

/**  历史成交记录 */
extern NSString * const JYSHistoricalTransactionURL;

/**  市场搜索 */
extern NSString * const JYSTradeSearchURL;

/**  查询用户某币种可用余额  */
extern NSString * const JYSTradeWalletRemainURL;

/**  购买货币提交页  */
extern NSString * const JYSTradeBuyURL;

/**  卖出货币提交页  */
extern NSString * const JYSTradeSellURL;

/**  货币简介页  */
extern NSString * const JYSCoinDetailURL;

/**  充币接口  */
extern NSString * const JYSChargingMoneyURL;

/**  提币展示接口  */
extern NSString * const JYSWithdrawURL;

/**  提币接口  */
extern NSString * const JYSWithdrawSaveURL;

/**  充提币历史记录  */
extern NSString * const JYSCoinRecordURL;

/**  添加提现地址  */
extern NSString * const JYSWithdrawAddressInsertURL;

/**  提现地址列表  */
extern NSString * const JYSWithdrawAddressListURL;

/**  删除提现地址  */
extern NSString * const JYSWithdrawAddressDeleteURL;

/**  查询市场的常用信息(数量价格的小数精度等）  */
extern NSString * const JYSSymbolBInfoURL;

/**  取消订单  */
extern NSString * const JYSCancellationsURL;

/**  推荐人赠币消息  */
extern NSString * const JYSAnnouncementURL;

/**  c2c商家入驻缴费  */
extern NSString * const CTCMerchantPay;
/**  查询市场常用汇率 */
extern NSString * const JYSSymbolRateURL;


#pragma mark Notification Key
/**  推荐人赠币消息  */
extern NSString * const MyADNotification;

extern NSString * const MyC2COrderNotification;
