//
//  NSPredicate+Search.m
//  App3.0
//
//  Created by sunzhenkun on 2018/6/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "NSPredicate+Search.h"

@implementation NSPredicate (Search)

+ (NSPredicate *)predicateWithSearch:(NSString *)pSearch searchTerm:(NSString*)searchTerm{
    
    pSearch = [pSearch stringByReplacingOccurrencesOfString:@"`" withString:@""];
    pSearch = [pSearch stringByReplacingOccurrencesOfString:@"'" withString:@""];
    pSearch = [pSearch stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    NSString *compareOr = @" OR";
    
    NSPredicate *predicate;
    
    NSString *stringWhere = @"(1 == 2)";
    int countOr = [self getWordCount:pSearch delimitador:compareOr];
    for (int counterA = 1; counterA <= countOr; counterA++) {
        NSString *partSearch = [self getWordNum:pSearch indice:counterA delimitador:compareOr];
        if (!([partSearch stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0)) {
            
            int countSpaces = [self getWordCount:partSearch delimitador:@" "];
            
            NSString *stringWhereSpaces = @"";
            
            for (int counterB = 1; counterB <= countSpaces; counterB++) {
                NSString *partSearchSpaces = [self getWordNum:partSearch indice:counterB delimitador:@" "];
                if (!([partSearchSpaces stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0)) {
                    partSearchSpaces = [NSString stringWithFormat:@"*%@*", partSearchSpaces];
                    stringWhereSpaces = [NSString stringWithFormat:@"%@ %@ %@ like[cd] '%@' ", stringWhereSpaces, (stringWhereSpaces.length == 0 ? @"" : @" AND "), searchTerm, partSearchSpaces];
                }
            }
            
            stringWhere = [NSString stringWithFormat:@" %@ OR (%@)", stringWhere, stringWhereSpaces];
        }
    }
    predicate = [NSPredicate predicateWithFormat:stringWhere];
    return predicate;
}

+ (NSString *) getWordNum:(NSString *)texto indice:(int) indice delimitador:(NSString *)delimitador{
    texto=[delimitador stringByAppendingString:texto];
    texto=[texto stringByAppendingString:delimitador];
    NSArray *substrings = [texto componentsSeparatedByString:delimitador];
    NSString *first = [substrings objectAtIndex:indice];
    return first;
}


+ (int) getWordCount :(NSString *)texto delimitador:(NSString *)delimitador{
    NSArray *substrings = [texto componentsSeparatedByString:delimitador];
    NSString *tamanhoStr=[NSString stringWithFormat:@"%lu",(unsigned long)substrings.count];
    int qtdWord;
    qtdWord = [tamanhoStr intValue];
    return qtdWord;
}

@end
