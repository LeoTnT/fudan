//
//  NSPredicate+Search.h
//  App3.0
//
//  Created by sunzhenkun on 2018/6/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPredicate (Search)

+ (NSPredicate *)predicateWithSearch:(NSString *)pSearch searchTerm:(NSString*)searchTerm;

@end
