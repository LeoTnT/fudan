//
//  UIScrollView+ScalableCover.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScalableCover : UIImageView

@property (nonatomic, strong) UIScrollView *scrollView;

/** 高度 */
@property (nonatomic, assign) CGFloat MaxHeight;

@end

@interface UIScrollView (ScalableCover)

@property (nonatomic, weak) ScalableCover *scalableCover;

- (void)addScalableCoverWithImage:(UIImage *)image withMaxHeight:(CGFloat)MaxHeight;
- (void)removeScalableCover;

@end
