//
//  UIScrollView+ScalableCover.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/28.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UIScrollView+ScalableCover.h"
#import <objc/runtime.h>

static NSString * const kContentOffset = @"contentOffset";
static NSString * const kScalableCover = @"scalableCover";

@implementation UIScrollView (ScalableCover)

- (void)setScalableCover:(ScalableCover *)scalableCover
{
    [self willChangeValueForKey:kScalableCover];
    objc_setAssociatedObject(self, @selector(scalableCover),
                             scalableCover,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self didChangeValueForKey:kScalableCover];
}

- (ScalableCover *)scalableCover
{
    return objc_getAssociatedObject(self, &kScalableCover);
}

- (void)addScalableCoverWithImage:(UIImage *)image withMaxHeight:(CGFloat)MaxHeight
{
//    ScalableCover *cover = [[ScalableCover alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, MaxHeight*SCREEN_SCALE)];
    ScalableCover *cover = [[ScalableCover alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, MaxHeight)];
    cover.backgroundColor = [UIColor clearColor];
    cover.image = image;
    cover.scrollView = self;
    cover.MaxHeight = MaxHeight;
    
    [self addSubview:cover];
    [self sendSubviewToBack:cover];
    self.scalableCover = cover;
}

- (void)removeScalableCover
{
    [self.scalableCover removeFromSuperview];
    self.scalableCover = nil;
}

@end

@implementation ScalableCover
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setScrollView:(UIScrollView *)scrollView
{
    [_scrollView removeObserver:scrollView forKeyPath:kContentOffset];
    _scrollView = scrollView;
    [_scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)removeFromSuperview
{
    [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
    XSLog(@"😄----removeed");
    [super removeFromSuperview];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.scrollView.contentOffset.y < 0) {
        CGFloat offset = -self.scrollView.contentOffset.y;
//        self.frame = CGRectMake(-offset, -offset, _scrollView.bounds.size.width + offset * 2, self.MaxHeight*SCREEN_SCALE + offset);
        self.frame = CGRectMake(-offset, -offset, _scrollView.bounds.size.width + offset * 2, self.MaxHeight + offset);
    } else {
//        self.frame = CGRectMake(0, 0, _scrollView.bounds.size.width, self.MaxHeight*SCREEN_SCALE);
        self.frame = CGRectMake(0, 0, _scrollView.bounds.size.width, self.MaxHeight);
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self setNeedsLayout];
}


@end
