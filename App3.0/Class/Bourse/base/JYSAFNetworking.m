//
//  JYSAFNetworking.m
//  JYSAFNetworking
//
//  Created by sunzhenkun on 2017/9/24.
//  Copyright © 2017年 sunzhenkun. All rights reserved.
//

#import "JYSAFNetworking.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import <Reachability/Reachability.h>
#import "AppDelegate.h"

@implementation JYSAFNetworking

+ (JYSAFNetworking *)sharedNetworkingTool
{
    static JYSAFNetworking * handler = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        handler = [[JYSAFNetworking alloc] init];
    });
    return handler;
}

+ (JYSURLSessionTask *)getOrPostWithType:(HttpMethod)httpMethod withUrl:(NSString *)url params:(NSDictionary *)params HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail showHUD:(BOOL)showHUD {
    return [self baseRequestType:httpMethod url:url params:params toShowView:showView animated:animated success:success fail:false showHUD:showHUD];
}

+ (JYSURLSessionTask *)baseRequestType:(HttpMethod)type url:(NSString *)url params:(NSDictionary *)params toShowView:(UIView *)showView animated:(BOOL)animated success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail showHUD:(BOOL)showHUD {
    if (url == nil) {
        return nil;
    }
    
    NSDictionary *param =  [XSTool convertToParamFromParameters:params];
    
//    NSString * urlString = [JYSBaseURL stringByAppendingString:url];
    NSString * urlString = [NSString stringWithFormat:@"%@%@",JYSBaseURL,url];
    if (showHUD == YES) {
        [MBProgressHUD showHUDAddedTo:showView animated:animated];
    }
    
    //检查地址中是否有中文
    NSString * urlStr = [NSURL URLWithString:urlString]?urlString:[self strUTF8Encoding:urlString];
    
    AFHTTPSessionManager * manager = [self getAFManager];
    
    JYSURLSessionTask * sessionTask = nil;
    
    if (type == GET) {
        sessionTask = [manager GET:urlStr parameters:param progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//            if (success) {
//                success(responseObject);
//            }
            if (showHUD == YES) {
                [MBProgressHUD hideHUDForView:showView animated:animated];
            }
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil] ;
            resultObject *res = [resultObject mj_objectWithKeyValues:dict];
            
            if (res && dict) {
                if ([res.info isEqualToString:@"无效用户"]) {
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"err_user_login_another_device") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
                    }];
                    [alert addAction:action1];
                    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
                    return;
                }
                success(dict,res);
            } else {
                res = [resultObject new];
                res.status = NO;
                res.info = @"返回数据为空";
                success(nil,res);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (fail) {
                fail(error);
            }
            
            if (showHUD) {
                [MBProgressHUD hideHUDForView:showView animated:animated];
            }
        }];
    } else {
        sessionTask = [manager POST:urlStr parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (showHUD) {
                [MBProgressHUD hideHUDForView:showView animated:animated];
            }
            if (success) {
                NSLog(@"param:%@",param);
                [self printJson:responseObject path:urlStr];
                NSDictionary *dict = responseObject;
                
                resultObject *res = [resultObject mj_objectWithKeyValues:dict];
                
                if (res && dict) {
                    if ([res.info isEqualToString:@"无效用户"]) {
                        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"err_user_login_another_device") preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
                        }];
                        [alert addAction:action1];
                        AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                        [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
                        return;
                    }
                    success(dict,res);
                } else {
                    res = [resultObject new];
                    res.status = NO;
                    res.info = @"返回数据为空";
                    success(nil,res);
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (fail) {
                fail(error);
            }
            if (showHUD) {
                [MBProgressHUD hideHUDForView:showView animated:animated];
            }
        }];
    }
    
    return sessionTask;
}

//打印JSON数据
+(void)printJson:(id)responseData path:(NSString *)path

{
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseData options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSLog(@"\n -------↓↓↓↓-Json-↓↓↓↓---------%@----------URL---》 \n %@ \n 《----------↑↑↑↑-Json-↑↑↑↑---------------------------- \n",path,jsonString);
    
}



+ (JYSURLSessionTask *)uploadWithImages:(NSArray *)imageArr url:(NSString *)url fileName:(NSString *)fileName names:(NSArray *)nameArr params:(NSDictionary *)params HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated progress:(JYSUploadProgress)progress success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail showHUD:(BOOL)showHUD {
    if (url == nil) {
        return nil;
    }
    
    NSString * urlString = [ImageBaseUrl stringByAppendingString:url];
    
    if (showHUD) {
        [MBProgressHUD showHUDAddedTo:showView animated:animated];
    }
    
    //检查地址中是否有中文
    NSString * urlStr = [NSURL URLWithString:urlString] ? urlString : [self strUTF8Encoding:urlString];
    
    AFHTTPSessionManager * manager = [self getAFManager];
    JYSURLSessionTask * sessionTask = nil;
    
    sessionTask = [manager POST:urlStr parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < imageArr.count; i++) {
            UIImage * image = (UIImage *)imageArr[i];
            NSData * imageData = UIImageJPEGRepresentation(image, 1.0);
            NSString * imageFileName = fileName;
            if (fileName == nil || ![fileName isKindOfClass:[NSString class]] || fileName.length == 0) {
                NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
                formatter.dateFormat = @"yyyyMMddHHmmss";
                NSString * str = [formatter stringFromDate:[NSDate date]];
                imageFileName = [NSString stringWithFormat:@"%@.png",str];
            }
            NSString * nameString = (NSString *)nameArr[i];
            
            [formData appendPartWithFileData:imageData name:nameString fileName:imageFileName mimeType:@"image/jpg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"上传进度---%lld,总进度---%lld",uploadProgress.completedUnitCount,uploadProgress.totalUnitCount);
        if (progress) {
            progress(uploadProgress.completedUnitCount,uploadProgress.totalUnitCount);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            //            success(responseObject);
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil] ;
            resultObject *res = [resultObject mj_objectWithKeyValues:dict];
            if (res && dict) {
                success(dict,res);
            } else {
                res = [resultObject new];
                res.status = NO;
                res.info = @"返回数据为空";
                success(nil,res);
            }
        }
        if (showHUD) {
            [MBProgressHUD hideHUDForView:showView animated:animated];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (fail) {
            fail(error);
        }
        if (showHUD) {
            [MBProgressHUD hideHUDForView:showView animated:animated];
        }
    }];
    
    return sessionTask;
}

+ (JYSURLSessionTask *)downloadWithUrl:(NSString *)url saveToPath:(NSString *)saveToPath HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated progress:(JYSDownloadProgress)progress success:(JYSResponseSuccess)success fail:(JYSResponseFail)fail showHUD:(BOOL)showHUD {
    if (url == nil) {
        return nil;
    }
    
    NSString * urlString = [ImageBaseUrl stringByAppendingString:url];
    
    if (showHUD) {
        [MBProgressHUD showHUDAddedTo:showView animated:animated];
    }
    
    NSURLRequest * downloadRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPSessionManager * manager = [self getAFManager];
    JYSURLSessionTask * sessionTask = nil;
    
    sessionTask = [manager downloadTaskWithRequest:downloadRequest progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"下载进度---%.1f",1.0 * downloadProgress.completedUnitCount/downloadProgress.totalUnitCount);
        
        //回到主线程刷新UI
        dispatch_async(dispatch_get_main_queue(), ^{
            if (progress) {
                progress(downloadProgress.completedUnitCount, downloadProgress.totalUnitCount);
            }
        });
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        if (!saveToPath) {
            NSURL * downloadURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            NSLog(@"默认路径---%@",downloadURL);
            
            return [downloadURL URLByAppendingPathComponent:[response suggestedFilename]];
        } else {
            NSURL * downloadURL = [NSURL fileURLWithPath:saveToPath];
            NSLog(@"目标下载路径---%@",downloadURL);
            
            return [downloadURL URLByAppendingPathComponent:[response suggestedFilename]];
        }
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error == nil) {
            if (success) {
                success([filePath path]);//返回完整路径
            }
        } else {
            if (fail) {
                fail(error);
            }
        }
        
        if (showHUD == YES) {
            [MBProgressHUD hideHUDForView:showView animated:animated];
        }
    }];
    
    //开始下载
    [sessionTask resume];
    
    return sessionTask;
}

+ (AFHTTPSessionManager *)getAFManager {
    static AFHTTPSessionManager * httpManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
        httpManager = [AFHTTPSessionManager manager];
        httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
        httpManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        httpManager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        httpManager.requestSerializer.timeoutInterval = 30;
        httpManager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                                      @"text/html",
                                                                                      @"text/json",
                                                                                      @"text/plain",
                                                                                      @"text/javascript",
                                                                                      @"text/xml",
                                                                                      @"image/*"]];
    });
    
    return httpManager;
}

#pragma mark - 开始监听程序在运行中的网络连接变化
+ (void)startMonitoring
{
    // 1、获取网络监控的管理者
    AFNetworkReachabilityManager * mgr = [AFNetworkReachabilityManager sharedManager];
    
    // 2、设置网络状态改变后的处理
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown://未知网络
                [JYSAFNetworking sharedNetworkingTool].networkStatus = StatusUnknown;
                break;
            case AFNetworkReachabilityStatusNotReachable://没有网络（断网）
                [JYSAFNetworking sharedNetworkingTool].networkStatus = StatusNotReachable;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN://手机自带网络
                [JYSAFNetworking sharedNetworkingTool].networkStatus = StatusReachableViaWWAN;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi://WIFI
                [JYSAFNetworking sharedNetworkingTool].networkStatus = StatusReachableViaWiFi;
                break;
            default:
                break;
        }
    }];
    
    [mgr startMonitoring];
}

+ (networkStatus)cheakNetStatus {
    [self startMonitoring];
    
    if ([JYSAFNetworking sharedNetworkingTool].networkStatus == StatusReachableViaWiFi) {
        return StatusReachableViaWiFi;
    } else if ([JYSAFNetworking sharedNetworkingTool].networkStatus == StatusNotReachable) {
        return StatusNotReachable;
    } else if ([JYSAFNetworking sharedNetworkingTool].networkStatus == StatusReachableViaWWAN) {
        return StatusReachableViaWWAN;
    } else {
        return StatusUnknown;
    }
}

+ (BOOL)isHaveNetwork {
    Reachability * conn = [Reachability reachabilityForInternetConnection];
    
    if ([conn currentReachabilityStatus] == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

+ (NSString *)strUTF8Encoding:(NSString *)str{
    if (([UIDevice currentDevice].systemVersion.floatValue >= 9.0f)) {
        return [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    } else {
        return [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
}

- (void)dealloc
{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

@end
