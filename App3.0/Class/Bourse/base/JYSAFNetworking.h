//
//  JYSAFNetworking.h
//  JYSAFNetworking
//
//  Created by sunzhenkun on 2017/9/24.
//  Copyright © 2017年 sunzhenkun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  手机网络状态
 */
typedef NS_ENUM(NSInteger, networkStatus){
    StatusUnknown           = -1, //未知网络
    StatusNotReachable      = 0,    //没有网络
    StatusReachableViaWWAN  = 1,    //手机自带网络
    StatusReachableViaWiFi  = 2     //wifi
};

/**
 *  请求方式 GET OR POST
 */
typedef NS_ENUM(NSInteger, HttpMethod) {
    GET,
    POST
};

typedef void(^JYSResponseSuccess)(id response);

typedef void(^JYSResponseFail)(NSError * error);

typedef NSURLSessionTask JYSURLSessionTask;

typedef void(^JYSUploadProgress)(int64_t bytesProgress, int64_t totalBytesProgress);

typedef void(^JYSDownloadProgress)(int64_t bytesProgress, int64_t totalBytesProgress);

@interface JYSAFNetworking : NSObject

@property (nonatomic,assign)networkStatus networkStatus;

+ (JYSAFNetworking *)sharedNetworkingTool;

/**
 * 开启网络监测
 */
+ (void)startMonitoring;

/**
 *  获取网络状态
 */
+ (networkStatus)cheakNetStatus;

/**
 *  是否有网络连接
 */
+ (BOOL)isHaveNetwork;

/**
 *  post 或者 get 请求方法,block回调
 *  @param httpMethod       网络请求类型
 *  @param url              请求连接，根路径
 *  @param params           参数字典
 *  @param showView         HUD 展示view
 *  @param animated         HUD 是否有动画
 *  @param success          请求成功返回数据
 *  @param fail             请求失败
 *  @param showHUD          是否显示HUD
 */
+ (JYSURLSessionTask *)getOrPostWithType:(HttpMethod)httpMethod withUrl:(NSString *)url params:(NSDictionary *)params HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail showHUD:(BOOL)showHUD;


/**
 *  上传图片方法 支持多张上传和单张上传
 *  @param imageArr      上传的图片数组
 *  @param url           请求连接，根路径
 *  @param fileName      图片的名称(如果不传则以当前时间命名)
 *  @param nameArr       上传图片时参数数组 <后台 处理文件的[字段]>
 *  @param params        参数字典
 *  @param showView      HUD 展示view
 *  @param animated      HUD 是否有动画
 *  @param progress      上传进度
 *  @param success       请求成功返回数据
 *  @param fail          请求失败返回数据
 *  @param showHUD       是否显示HUD
 */
+ (JYSURLSessionTask *)uploadWithImages:(NSArray *)imageArr url:(NSString *)url fileName:(NSString *)fileName names:(NSArray *)nameArr params:(NSDictionary *)params HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated progress:(JYSUploadProgress)progress success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail showHUD:(BOOL)showHUD;

/**
 *  下载文件方法
 *  @param url           下载地址
 *  @param saveToPath    文件保存的路径,如果不传则保存到Documents目录下，以文件本来的名字命名
 *  loadingImageArr      loading图片数组
 *  @param showView      HUD 展示view
 *  @param animated      HUD 是否有动画
 *  @param progress      下载进度回调
 *  @param success       下载完成
 *  @param fail          失败
 *  @param showHUD       是否显示HUD
 *  @return              返回请求任务对象，便于操作
 */
+ (JYSURLSessionTask *)downloadWithUrl:(NSString *)url saveToPath:(NSString *)saveToPath HUDShowView:(UIView *)showView HUDAnimated:(BOOL)animated progress:(JYSDownloadProgress)progress success:(JYSResponseSuccess)success fail:(JYSResponseFail)fail showHUD:(BOOL)showHUD;

@end
