//
//  JYSConst.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/11.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSConst.h"


#pragma mark 接口
/** 交易所行情头部菜单 */
NSString * const JYSCurrencyTitlesURL = @"/api/v1/trade/common/Symbols";

///** 资产管理 */
//NSString * const JYSAssetManagementURL = @"/api/v1/trade/wallet/Lists";
/** 资产管理 */
NSString * const JYSAssetManagementURL = @"/api/v1/trade/wallet/ListsNew";

/** 委托订单列表 */
NSString * const JYSEntrustOrderURL = @"/api/v1/trade/trade/GetDealOrderList";

/**  货币下拉列表页 */
NSString * const JYSPullDownMenuURL = @"/api/v1/basic/symbol/Lists";

/**  历史成交记录 */
NSString * const JYSHistoricalTransactionURL = @"/api/v1/trade/trade/GetDealOrderMatchList";

/**  市场搜索 */
NSString * const JYSTradeSearchURL = @"/api/v1/trade/trade/Search";

/**  查询用户某币种可用余额  */
NSString * const JYSTradeWalletRemainURL = @"/api/v1/trade/wallet/Remain";

/**  购买货币提交页  */
NSString * const JYSTradeBuyURL = @"/api/v1/trade/trade/Buy";

/**  卖出货币提交页  */
NSString * const JYSTradeSellURL = @"/api/v1/trade/trade/Sell";

/**  货币简介页  */
NSString * const JYSCoinDetailURL = @"/api/v1/basic/coin/CoinDetail";

/**  充币接口  */
NSString * const JYSChargingMoneyURL = @"/api/v1/trade/wallet/Recharge";

/**  提币展示接口  */
NSString * const JYSWithdrawURL = @"/api/v1/trade/wallet/Withdraw";

/**  提币接口  */
NSString * const JYSWithdrawSaveURL = @"/api/v1/trade/wallet/WithdrawSave";

/**  充提币历史记录  */
NSString * const JYSCoinRecordURL = @"/api/v1/trade/record/CoinTradeRecord";

/**  添加提现地址  */
NSString * const JYSWithdrawAddressInsertURL = @"/api/v1/trade/withdrawaladdress/Insert";

/**  提现地址列表  */
NSString * const JYSWithdrawAddressListURL = @"/api/v1/trade/withdrawaladdress/Lists";

/**  删除提现地址  */
NSString * const JYSWithdrawAddressDeleteURL = @"/api/v1/trade/withdrawaladdress/Delete";

/**  查询市场的常用信息(数量价格的小数精度等）  */
NSString * const JYSSymbolBInfoURL = @"/api/v1/basic/symbol/BInfo";

/**  取消订单  */
NSString * const JYSCancellationsURL = @"/api/v1/trade/trade/Cancel";

/**  推荐人赠币消息  */
NSString * const JYSAnnouncementURL = @"/api/v1/user/user/GetIntroNotice";

/**  c2c商家入驻缴费  */
NSString * const CTCMerchantPay = @"/api/v1/user/register/FinishEnterOrder";

/**  查询市场常用汇率 */
NSString * const JYSSymbolRateURL = @"/api/v1/basic/symbol/Rate";


#pragma mark Notification Key
NSString * const MyADNotification = @"MyADNotification";
NSString * const MyC2COrderNotification = @"MyC2COrderNotification";
