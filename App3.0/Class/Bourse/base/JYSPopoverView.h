//
//  NWPopoverView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/3/21.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^cellDidSelectBlock)(NSString *indexString,NSInteger index);

typedef void(^closeAlertviewBlock)();

//动画样式
typedef enum viewAnimateDirection {
    ViewAnimateFromTop,
    ViewAnimateFromLeft,
    ViewAnimateFromRight,
    ViewAnimateFromBottom,
    ViewAnimateScale,
    ViewAnimateNone
} viewAnimateStyle;

@interface JYSPopoverView : UIView

@property (nonatomic,copy)void(^cellDidSelectBlock)(NSString *indexString,NSInteger index);

@property (nonatomic,copy)void(^closeAlertviewBlock)();

@property (nonatomic,assign) viewAnimateStyle viewAnimateStyle;

//点击背景是否关闭
@property (assign,nonatomic) BOOL isBGClose;

- (instancetype)initWithActionSheetArray:(NSArray *)indexTextArray;

- (void)cellDidSelectBlock:(cellDidSelectBlock) cellDidSelectBlock;

- (instancetype) initWithAlertView:(UIView *)alertView;

- (void) closeAlertView:(closeAlertviewBlock) closeAlertviewBlock;

@end

@interface NWActionSheetCell : UITableViewCell

@end
