
#import "QR_CodeViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "QRView.h"

@interface QR_CodeViewController ()<AVCaptureMetadataOutputObjectsDelegate,
UIAlertViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

{
     UIButton *Capture;
    UIButton *guanbi;
}
@property (strong,nonatomic)AVCaptureDevice * device;
@property (strong,nonatomic)AVCaptureDeviceInput * input;
@property (strong,nonatomic)AVCaptureMetadataOutput * output;
@property (strong,nonatomic)AVCaptureSession * session;
@property (strong,nonatomic)AVCaptureVideoPreviewLayer * preview;

@end

@implementation QR_CodeViewController



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *grayview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 70)];
    grayview.backgroundColor = mainColor;
    grayview.alpha = 0.7;
    [self.view addSubview:grayview];
    
    Capture = [[UIButton alloc]initWithFrame:CGRectMake(15, 25, 30, 30)];
    [Capture setImage:[UIImage imageNamed:@"Light"] forState:(UIControlStateNormal)];
    [Capture addTarget:self action:@selector(capture:) forControlEvents:(UIControlEventTouchUpInside)];
    Capture.hidden = NO;
    [grayview addSubview:Capture];
    guanbi = [[UIButton alloc]initWithFrame:CGRectMake(15, 25, 30, 30)];
    [guanbi setImage:[UIImage imageNamed:@"Light"] forState:(UIControlStateNormal)];    guanbi.hidden = YES;
    [guanbi addTarget:self action:@selector(guanbi:) forControlEvents:(UIControlEventTouchUpInside)];
    [grayview addSubview:guanbi];
    UILabel * labIntroudction= [[UILabel alloc] initWithFrame:CGRectMake(55,15, mainWidth-55, 50)];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.numberOfLines=0;
    labIntroudction.font =[UIFont systemFontOfSize:12];
    labIntroudction.textAlignment = NSTextAlignmentCenter;
    labIntroudction.textColor=MY_ORDER_SELECTED_COLOR;
    labIntroudction.text = Localized(@"将条形码或二维码置于取景框内系统会自动扫描");
    [grayview addSubview:labIntroudction];

    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitleColor:MY_ORDER_SELECTED_COLOR forState:UIControlStateNormal];
    [rightBtn setTitle:Localized(@"相册") forState:UIControlStateNormal];
    [grayview addSubview:rightBtn];
    
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(grayview.mas_right).offset(-10);
        make.centerY.mas_equalTo(Capture.mas_centerY);
    }];
    
    [labIntroudction mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(55);
        make.right.mas_equalTo(rightBtn.mas_left).offset(-10);
        make.centerY.mas_equalTo(rightBtn);
    }];
    
    [rightBtn addTarget:self action:@selector(showImagePickerController) forControlEvents:UIControlEventTouchUpInside];
    [self setupCamera];

}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //获取选中的照片
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
    }
    //初始化  将类型设置为二维码
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:nil];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //设置数组，放置识别完之后的数据
        NSArray *features = [detector featuresInImage:[CIImage imageWithData:UIImagePNGRepresentation(image)]];
        
        NSLog(@"features = = = = = %@",features);
        //判断是否有数据（即是否是二维码）
        if (features.count >= 1) {
            //取第一个元素就是二维码所存放的文本信息
            CIQRCodeFeature *feature = features[0];
            NSString *scannedResult = feature.messageString;
            //通过对话框的形式呈现
            [self handleDecode:scannedResult];
        }else{
//            [UIView setAnimationsEnabled:YES];
//            _line.hidden = NO;
            [self alertControllerMessage:Localized(@"请选择二维码")];
            
        }
    }];
}
-(void)alertControllerMessage:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void) showImagePickerController {
    UIImagePickerController *imagrPicker = [[UIImagePickerController alloc]init];
    imagrPicker.delegate = self;
    imagrPicker.allowsEditing = YES;
    //将来源设置为相册
    imagrPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;

    [self presentViewController:imagrPicker animated:YES completion:nil];


}

- (void)back:(UIButton *)btn{
    if (self.device) {
        self.device = nil;
    }
    if (self.session) {
        [self.session stopRunning];
        self.session = nil;
    }
    if (self.preview) {
        [self.preview removeFromSuperlayer];
    }

    if (self.input) {
        self.input = nil;
    }
    if (self.output) {
        self.output = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupCamera
{
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError * error;
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:&error];

    
    if (error) {
        NSLog(@"没有摄像头-%@", error.localizedDescription);
        [MBProgressHUD showMessage:@"没有摄像头" view:self.view hideTime:.8 doSomeThing:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        return;
    }
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [ _output setRectOfInterest : CGRectMake (0.2, 0.2, 0.6, 0.6)];
    
    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input])
    {
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.output])
    {
        [_session addOutput:self.output];
    }
    
    /*二维码
     //     条码类型 AVMetadataObjectTypeQRCode
     if ((self.output.metadataObjectTypes = [NSArray arrayWithObject:AVMetadataObjectTypeQRCode])) {
     //        self.output.metadataObjectTypes = [NSArray arrayWithObject:AVMetadataObjectTypeQRCode];
     self.output.metadataObjectTypes = [NSArray arrayWithObject:AVMetadataObjectTypeQRCode];
     }else{
     NSLog(@"此设备不支持扫描二维码");
     return;
     }
     */
    
    // 条码类型 AVMetadataObjectTypeQRCode
    if ([_output.availableMetadataObjectTypes containsObject:
         AVMetadataObjectTypeQRCode]||
        [_output.availableMetadataObjectTypes containsObject:
         AVMetadataObjectTypeCode128Code]) {
            _output.metadataObjectTypes =[NSArray arrayWithObjects:
                                          AVMetadataObjectTypeQRCode,
                                          AVMetadataObjectTypeCode39Code,
                                          AVMetadataObjectTypeCode128Code,
                                          AVMetadataObjectTypeCode39Mod43Code,
                                          AVMetadataObjectTypeEAN13Code,
                                          AVMetadataObjectTypeEAN8Code,
                                          AVMetadataObjectTypeCode93Code, nil];
        }
    
    
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:self.session];
    _preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _preview.frame =CGRectMake(0,0,mainWidth,mainHeight);
    [self.view.layer insertSublayer:self.preview atIndex:0];
    [_session startRunning];

    QRView * imageView = [[QRView alloc]initWithFrame:CGRectMake(50, 140, mainWidth-100, mainWidth-100)];
    imageView.image = [UIImage imageNamed:@"pick_bg"];
    imageView.transparentArea = CGSizeMake(mainWidth-100, mainWidth-100);
    [self.view addSubview:imageView];
    
    UIView *layerview = [[UIView alloc]initWithFrame:CGRectMake(0, mainHeight-70, mainWidth, 70)];
    layerview.backgroundColor = mainColor;
    layerview.alpha = 0.7;
    [self.view addSubview:layerview];
    
    UILabel * labIntroudction= [[UILabel alloc] initWithFrame:CGRectMake(45,10, mainWidth-45, 50)];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.numberOfLines=0;
    labIntroudction.font =[UIFont systemFontOfSize:12];
    labIntroudction.textAlignment = NSTextAlignmentCenter;
    labIntroudction.textColor=MY_ORDER_SELECTED_COLOR;
    labIntroudction.text=@"建议与镜头保持10cm距离，尽量避免光和阴影";
    [layerview addSubview:labIntroudction];
    
    UIButton *Cancelbtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 20, 60, 30)];
    Cancelbtn.layer.cornerRadius = 5;
    Cancelbtn.layer.masksToBounds = YES;
    [Cancelbtn setTitle:Localized(@"cancel_btn") forState:(UIControlStateNormal)];
    Cancelbtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [Cancelbtn setTitleColor:MY_ORDER_SELECTED_COLOR forState:(UIControlStateNormal)];
    [Cancelbtn addTarget:self action:@selector(cancelbtn:) forControlEvents:(UIControlEventTouchUpInside)];
    Cancelbtn.backgroundColor =[UIColor whiteColor];
    [layerview addSubview:Cancelbtn];
    
 

}


-(void)cancelbtn:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
// 处理二维码扫描
-(void)handleDecode:(NSString *)qrstr{
    NSLog(@"---%@",qrstr);


    NSArray *arr = [qrstr componentsSeparatedByString:@"?"];
    NSString *head = arr[0];
    NSLog(@"head  =%@",arr[0]);
    
    if ([self isPureInt:head]){
//        [self requestCommodityProduct:head];
    } else {
        [XSTool showProgressHUDWithView:self.view];
        
        [self QRCondRequest:qrstr];
        
    }

    
    

}
//- (void) popLastViewControllerWithCode:(NSString *) scanResult {
//    GoodsStandardViewController *controller = (GoodsStandardViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
//    NSMutableString *mCodeString = [NSMutableString stringWithString:[scanResult substringToIndex:scanResult.length-1]];
//    if ([scanResult containsString:@"org.gs1.EAN-13"]) {
//        //EAN码符号有标准版（EAN-13）和缩短版（EAN-8）两种，我国的通用商品条码与其等效，日常购买的商品包装上所印的条码一般就是EAN码
//        //690 12345 6789 2
//        //截取商品编码
//        controller.barCode =  [mCodeString substringFromIndex:8];
//        NSLog(@"controller.barCode===%@",controller.barCode);
//    }
//
//    //690 1234 1
//    if ([scanResult isEqualToString:@"org.gs1.EAN-8"]) {
//        //截取商品编码
//        controller.barCode =  [mCodeString substringFromIndex:3];
//        NSLog(@"controller.barCode===%@",controller.barCode);
//    }
//    [self.navigationController popToViewController:controller animated:YES];
//
//
//}



//- (void) requestCommodityProduct:(NSString *)str {
//
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [HTTPManager getCommodityProduct:str success:^(NSDictionary *dic, resultObject *state) {
//
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if (state) {
//
//            NSString *ID = dic[@"data"][@"pid"];
//            GoodsDetailViewController *vc = [GoodsDetailViewController new];
//            vc.goodsID = ID;
//            [XSTool hideProgressHUDWithView:self.view];
//            [self.navigationController pushViewController:vc animated:YES];
//
//        }else{
//
//
//            [MBProgressHUD showMessage:dic[@"info"] view:self.view hideTime:2 doSomeThing:^{
//                [self.navigationController popViewControllerAnimated:YES];
//            }];
//        }
//
//
//    } fail:^(NSError *error) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [MBProgressHUD showMessage:NetFailure view:self.view];
//    }];
//}

- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

/**
 product";//商品
 "supply";//店铺
 "app";//客户端
 "user";//个人,主要用于二维码
 */
- (void)handleQRCode:(NSString *)QRInformation {
    //    http://txzy.xsy.dsceshi.cn/mobile/product/details?qrtype=product&id=2&userid=212
    
    [QRInformation containsString:@"&"] ? [self QRCondRequest:QRInformation]:[self barCodeRequest:QRInformation];
    
}



/**
 条形码
 */
- (void) barCodeRequest:(NSString *)sender {
    
    
}

/**
 二维码
 */
- (void) QRCondRequest:(NSString *)QRInformation {
    [XSTool hideProgressHUDWithView:self.view];
    NSString *paramStr = [QRInformation componentsSeparatedByString:@"?"][1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![QRInformation containsString:ImageBaseUrl] || arr.count < 2) {
        if ([QRInformation hasPrefix:@"http"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
        } else {
            [XSTool showToastWithView:self.view Text:@"无法识别"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
//        GoodsDetailViewController *vc = [GoodsDetailViewController new];
//        vc.goodsID = qrid;
//        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
//        S_StoreInformation *infor = [S_StoreInformation new];
//        infor.storeInfor = qrid;
//
//        [XSTool hideProgressHUDWithView:self.view];
//        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"%@%@",Localized(@"您已安装"),APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
//        if ([qrid isEqualToString:[UserSingle sharedUserSingle].user.uid]) {
//            PersonViewController *personVC = [[PersonViewController alloc] init];
//            [self.navigationController pushViewController:personVC animated:YES];
//        } else {
//            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
//            [self.navigationController pushViewController:perVC animated:YES];
//        }
    } else if ([qrtype isEqualToString:@"TYPE_NORMAL_GROUP"]){
        [XSTool showProgressHUDWithView:self.view];
        @weakify(self);
        // 从服务器拉取群信息
//        [HTTPManager CheckUserInGroupWithGroupId:qrid success:^(NSDictionary * dic, resultObject *state){
//            @strongify(self);
//            [XSTool hideProgressHUDWithView:self.view];
//            if (state.status) {
//                GroupDataModel *groupModel = [GroupDataModel mj_objectWithKeyValues:dic[@"data"]];
//                if (groupModel.in_group) {
////                    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:groupModel.gid type:EMConversationTypeGroupChat createIfNotExist:YES];
////                    ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
////                    chatVC.title = groupModel.name;
////                    chatVC.avatarUrl = groupModel.avatar;
////                    chatVC.hidesBottomBarWhenPushed = YES;
//
//                    XMGroupChatController * chatVC = [XMGroupChatController new];
//
//                    [self.navigationController pushViewController:chatVC animated:YES];
//                } else {
//                    GroupQRViewController *vc = [[GroupQRViewController alloc] init];
//                    vc.groupModel = groupModel;
//                    [self.navigationController pushViewController:vc animated:YES];
//                }
//            } else {
//                [XSTool showToastWithView:self.view Text:state.info];
//            }
//        } fail:^(NSError * _Nonnull error) {
//            [XSTool hideProgressHUDWithView:self.view];
//            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
//        }];
    } else if ([qrtype isEqualToString:@"supply_pay"]){
//        S_CashierController *cashier =[S_CashierController new];
//        cashier.showTitle = qrid;
//        cashier.supply_name = qrid;
//        [self.navigationController pushViewController:cashier animated:YES];
    } else if ([qrtype isEqualToString:@"quickpay"]){
//        [XSTool showProgressHUDWithView:self.view];
//        [HTTPManager supplyGetSupplyInfoWithUid:qrid success:^(NSDictionary *dic, resultObject *state) {
//            [XSTool hideProgressHUDWithView:self.view];
//            if (state.status) {
//                BusinessSupplyParser *supplyParser = [BusinessSupplyParser mj_objectWithKeyValues:dic[@"data"]];
//
//                //是否开通收银台
//                if ([supplyParser.is_enable_quickpay intValue]==1) {
//                    S_CashierController *cashier =[S_CashierController new];
//                    NSString *str = arr[2],*supplyId;
//                    NSString *delStr = @"intro=";
//                    if ([str  containsString:delStr]) {
//                        supplyId = [str substringFromIndex:delStr.length];
//                    }
//
//                    cashier.showTitle = supplyParser.name;//[NSString stringWithFormat:@"商家账号：%@",supplyId];
//                    cashier.supply_name = supplyId;
//                    cashier.quick_percent_pay = [supplyParser.quick_pay integerValue];
//                    [self.navigationController pushViewController:cashier animated:YES];
//                } else {
//                    [XSTool showToastWithView:self.view Text:@"尚未开通收银台！"];
//                }
//
//
//            } else {
//                [XSTool showToastWithView:self.view Text:state.info];
//
//            }
//        }fail:^(NSError *error) {
//            [XSTool hideProgressHUDWithView:self.view];
//            [XSTool showToastWithView:self.view Text:NetFailure];
//        }];
        
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:QRInformation]];
    }
}




//关闭闪光灯
-(void)guanbi:(UIButton *)btn{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device setTorchMode:AVCaptureTorchModeOff];
    [device setFlashMode:AVCaptureFlashModeOff];
    Capture.hidden = NO;
    guanbi.hidden = YES;
    
}
//打开闪光灯
-(void)capture:(UIButton *)btn{
    Capture.hidden = YES;
    guanbi.hidden = NO;
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    
    // Create device input and add to current session
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error: nil];
    [session addInput:input];
    
    // Create video output and add to current session
    AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
    [session addOutput:output];
    
    // Start session configuration
    [session beginConfiguration];
    [device lockForConfiguration:nil];
    
    // Set torch to on
    [device setTorchMode:AVCaptureTorchModeOn];
    [session commitConfiguration];
    
}
 
//TODO:UIAlertView 代理delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //点击cancle重新开始扫描二维码
    if(alertView.tag == 1)
        [_session startRunning];
}


#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    //拿到扫描到的字符串信息，继续下一步的操作.....
    NSString *stringValue;
    
    if ([metadataObjects count] >0)
    {
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }
    
    [self handleDecode:stringValue];
    
    [_session stopRunning];
}



- (void)dealloc {
    
    NSLog(@" - -dealloc - - - - - ");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
