//
//  BITHeader.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#ifndef BITHeader_h
#define BITHeader_h

#define NAFilePath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]
#define NAFilePathAppend(fileName) [NAFilePath stringByAppendingPathComponent:fileName]

#define NAAccountFilePath [NAFilePath stringByAppendingPathComponent:@"account.arch"]

#define BIT_LIST [NAFilePath stringByAppendingPathComponent:@"BIT_LIST.arch"]

//  国际化
#define APP_Internationalization   @"myLanguage"

#define REQUEST_URL(args) [[NSString stringWithFormat:@"%@/api/v1/%@",ImageBaseUrl,args] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

#define main_BackColor           [UIColor hexFloatColor:@"ffffff"] //0090ff


// 受信任的名字显示
//#define TRURE_NAME [UIColor hexFloatColor:@"B9BABD"]
#define TRURE_NAME [UIColor hexFloatColor:@"CDCDCD"]


// 我的订单    默认颜色
#define MY_ORDER_NORMAL_COLOR       [UIColor hexFloatColor:@"999999"]
// 各种暗红    选中颜色
#define MY_ORDER_SELECTED_COLOR      [UIColor hexFloatColor:@"EB4554"]
// 卖BIT颜色
#define MY_SELL_B_COLOR   [UIColor hexFloatColor:@"40CB61"]
// 线的颜色
//#define MAIN_MAIN_LINE_COLOR [UIColor hexFloatColor:@"252839"]
#define MAIN_MAIN_LINE_COLOR [UIColor hexFloatColor:@"E5E5E5"]




//本地存储guid使用的key
#define APPAUTH_KEY          @"appauth"
//本地存储是否展示实名认证提示框使用的key
#define APPCERTAUTH_KEY          @"appCertAuth"
/* - - - - -BUG 监测 - - - - - - - -*/
// 项目标示
//#define Project_Mark         @"metoojiaoyi"
// 项目密钥
//#define Project_Secret_key   @"metoo159cc"
#define Project_IP           @"http://ck.xinshangyun.com:8089/YXS/xs/java/api"
#define CashReport           @"cashReport"

#ifdef __OBJC__
#import <SGPagingView/SGPagingView.h>
#import "BaseViewController.h"
#import "BaseNaviController.h"
#import "UserSingle.h"
#import "HTTPManager.h"
#import "BaseConfig.h"
#import "MainModel.h"
#import "ChildView.h"
#import "QR_CodeViewController.h"
#import "UILabel+BaseLabel.h"
#import "ContactModel.h"
#import "XMGroupChatController.h"
#import "NSBundle+Language.h"
#endif


#define USER_SUBJECT [UserSingle sharedUserSingle].siginleSubject


#endif /* BITHeader_h */
