//
//  HTTPManager.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface HTTPManager : NSObject
//
///*
// 普通登录
// */
//+ (void)doLoginin:(NSString *)name password:(NSString *)password success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 获取短信验证码
// */
//+ (void)getSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 验证短信验证码
// */
//+ (void)checkSmsVerifyValidWithMobile:(NSString *)mobile smsVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 短信验证码登录
// */
//+ (void)loginBySms:(NSString *)name verify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 检测手机号是否存在
// */
//+ (void)checkMobileExist:(NSString *)mobile Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 重置登录密码
// */
//+ (void)resetLoginPwdWithMobile:(NSString *)mobile pwdType:(NSString *)pwdType smsVerify:(NSString *)smsVerify andNewPwd:(NSString *)newPwd  account:(NSString *)account  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**退出*/
//+(void)LoginOutWithName:(NSString *) name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//
///**更换绑定手机号*/
//+ (void)setupMobile:(NSString *) mobile verify:(NSString *) verify token:(NSString *) token success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
//
///**验证密码*/
//+ (void)verifyPassWordWithPassword:(NSString *) password type:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
//
///**
// 给原手机号发验证码
// */
//+ (void)mobileVerifyWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**
// 原手机号验证
// */
//+ (void)mobileValidateWithVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**获取系统内的协议*/
//+ (void)getAgreementWithType:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
//
//
///**
// 自动登录
// */
//+ (void)autoLoginWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//#pragma mark  - - - - - - - --  - - - 注册
////注册
//+ (void)registerWithIntro:(NSString *)intro nickName:(NSString *)nickname mobile:(NSString *)mobile verify:(NSString *)verify password:(NSString *)pwd userName:(NSString *)userName success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///*
// 注册时获取短信验证码，需要额外参数type:“reg”
// */
//+ (void)regGetSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**
// 用户注册表单信息
// 字段           类型及范围    说明
// show           array    需要显示的注册项
// |--intro       string    邀请码
// |--mobile      string    手机号
// |--nickname    string    昵称
// |--province    string    区域
// |--address     string    详细地址
// |--agreement    string    协议
// |--verify      string    验证码
// require        array    必须的注册项
// |--同上         string    同上
// introducer     array    系统推荐的邀请码
// intro          string    未知
// */
//+ (void)getRegisterInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//
//
////修改资金密码
//+(void)resetPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
////修改登录密码
//+(void)resetLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
////找回资金密码
//+(void)findPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
////找回登录密码
//+(void)findLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//
//
//
//
///**商家入驻详情信息*/
//+ (void)getSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**商家行业信息*/
//+ (void)getIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
///**商家入驻*/
//+ (void)supplyRegisterWithAgreement:(NSString *)agreement name:(NSString *) name industry_id:(NSString *) industry_id logo:(NSString *) logo truename:(NSString *) truename card_no:(NSString *) card_no img_card:(NSString *) img_card img_license:(NSString *) img_license img_zuzhi:(NSString *) img_zuzhi province:(NSString *) province city:(NSString *) city county:(NSString *) county town:(NSString *) town province_code:(NSString *) province_code city_code:(NSString *) city_code county_code:(NSString *) county_code town_code:(NSString *) town_code detail:(NSString *) detail success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//
//
//+(void)bindEmailRequestWithPassword:(NSString *)pwd Email:(NSString *)email success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//
//
//
//+ (void)getShareInfo:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
//
//
//+ (void)searchUserWithString:(NSString *)string success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//
//@end
