//
//  HTTPManager.m
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "HTTPManager.h"

//@implementation HTTPManager
//
//
//+ (void)doLoginin:(NSString *)name password:(NSString *)password success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    
//    NSDictionary *param = @{@"name":name,@"passwd":password};
//    
//    [XSHTTPManager post:LoginUrl parameters:param success:success failure:fail];
//}
//
//
//+ (void)getSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    
//    NSDictionary *param = @{@"mobile":mobile};
//    
//    [XSHTTPManager post:GetSmsVerifyUrl parameters:param success:success failure:fail];
//}
//
//+ (void)checkSmsVerifyValidWithMobile:(NSString *)mobile smsVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary *param = @{@"mobile":mobile,@"verify":verify};
//    [XSHTTPManager post:CheckSmsVerifyValidUrl parameters:param success:success failure:fail];
//}
//
//+ (void)loginBySms:(NSString *)name verify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary *param = @{@"name":name,@"verify":verify};
//    [XSHTTPManager post:LoginBySmsUrl parameters:param success:success failure:fail];
//}
//
//+ (void)checkMobileExist:(NSString *)mobile Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary *param = @{@"mobile":mobile};
//    [XSHTTPManager post:Url_CheckMobileExist parameters:param success:success failure:fail];
//}
//
//+ (void)resetLoginPwdWithMobile:(NSString *)mobile pwdType:(NSString *)pwdType smsVerify:(NSString *)smsVerify andNewPwd:(NSString *)newPwd  account:(NSString *)account  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    
//    NSDictionary *param = @{@"mobile":mobile,@"mode":@(ResetPwdByMobile),@"type":pwdType,@"verify":smsVerify,@"password":newPwd};
//    [XSHTTPManager post:ResetLoginPwdUrl parameters:param success:success failure:fail];
//}
//
//+(void)LoginOutWithName:(NSString *)name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    NSDictionary *param = @{@"name":name};
//    [XSHTTPManager post:LoginOutUrl parameters:param success:success failure:fail];
//}
//
//+ (void)setupMobile:(NSString *)mobile verify:(NSString *)verify token:(NSString *)token success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
//    NSDictionary *param = @{@"mobile":mobile,@"token":token,@"verify":verify};
//    [XSHTTPManager post:ChangeMobile parameters:param success:success failure:failure];
//}
//
//+ (void)verifyPassWordWithPassword:(NSString *) password type:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
//    NSDictionary *param = @{@"oldPwd":password,@"type":type};
//    
//    [XSHTTPManager post:VerifyPassword parameters:param success:success failure:failure];
//}
//
//+ (void)mobileVerifyWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
//    [XSHTTPManager post:Url_MobileVerify parameters:nil success:success failure:failure];
//}
//
//+ (void)mobileValidateWithVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
//    [XSHTTPManager post:Url_MobileValidate parameters:@{@"verify":verify} success:success failure:failure];
//}
//
//+ (void)autoLoginWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    
//    [XSHTTPManager post:AutoLoginUrl parameters:nil success:success failure:fail];
//}
//
//+ (void)getRegisterInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
//    [XSHTTPManager post:Url_GetRegisterInfo parameters:nil success:success failure:fail];
//}
//
//+ (void)regGetSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary *param = @{@"mobile":mobile, @"type":@"reg"};
//    [XSHTTPManager post:GetSmsVerifyUrl parameters:param success:success failure:fail];
//}
//
//
//
//#pragma mark- 修改资金密码
//+(void)resetPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    [XSHTTPManager post:InstallResetPayPwdUrl parameters:dic success:success failure:fail];
//}
//#pragma mark- 修改登录密码
//+(void)resetLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    [XSHTTPManager post:InstallResetLoginPwdUrl parameters:dic success:success failure:fail];
//}
////找回资金密码
//+(void)findPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    [XSHTTPManager post:InstallFindPayPwdUrl parameters:dic success:success failure:fail];
//}
////找回登录密码
//+(void)findLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    [XSHTTPManager post:InstallFindLoginPwdUrl parameters:dic success:success failure:fail];
//}
//
//
//
///**商家入驻详情信息*/
//+ (void)getSupplyRegisterEnterInfoSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
//    NSString *path = @"/api/v1/csupplier/csupplier/Check";
//    [XSHTTPManager post:path parameters:nil success:success failure:fail];
//}
//
///**商家行业信息*/
//+ (void)getIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
//    NSString *path = @"/api/v1/basic/industry/Lists";
//    [XSHTTPManager post:path parameters:@{@"limit":@100} success:success failure:fail];
//}
//
///**商家入驻*/
//+ (void)supplyRegisterWithAgreement:(NSString *)agreement name:(NSString *) name industry_id:(NSString *) industry_id logo:(NSString *) logo truename:(NSString *) truename card_no:(NSString *) card_no img_card:(NSString *) img_card img_license:(NSString *) img_license img_zuzhi:(NSString *) img_zuzhi province:(NSString *) province city:(NSString *) city county:(NSString *) county town:(NSString *) town province_code:(NSString *) province_code city_code:(NSString *) city_code county_code:(NSString *) county_code town_code:(NSString *) town_code detail:(NSString *) detail success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
//    NSString *path = @"/api/v1/user/register/Enter";
//    NSDictionary *param = @{
//                            @"agreement":agreement,
//                            @"name":name,
//                            @"truename":truename,
//                            @"industry_id":industry_id,
//                            @"logo":logo,
//                            @"card_no":card_no,
//                            @"img_card":img_card,
//                            @"img_license":img_license,
//                            @"img_zuzhi":img_zuzhi?img_zuzhi:@"",
//                            @"province":province,
//                            @"city":city,
//                            @"county":county,
//                            @"town":town,
//                            @"province_code":province_code,
//                            @"city_code":city_code,
//                            @"county_code":county_code,
//                            @"town_code":town_code,
//                            @"detail":detail
//                            };
//    
//    [XSHTTPManager post:path parameters:param success:success failure:fail];
//    
//}
//
//
//
//+(void)bindEmailRequestWithPassword:(NSString *)pwd Email:(NSString *)email success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary * parameters = @{@"pwd":pwd, @"email":email};
//    [XSHTTPManager post:Url_AccountBinding_EmailBind parameters:parameters success:success failure:fail];
//}
//
//
//
//
//+ (void)searchUserWithString:(NSString *)string success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
//{
//    NSDictionary *param = @{@"search_item":string};
//    [XSHTTPManager post:Url_New_UserSearch parameters:param success:success failure:fail];
//}
//
//
//
//+ (void)getShareInfo:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
//    
//    [XSHTTPManager post:Url_GetShareInfo parameters:params success:success failure:failure];
//}
//
//@end
