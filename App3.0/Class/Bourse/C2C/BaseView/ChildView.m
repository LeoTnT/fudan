//
//  ChildView.m
//  NMFDemo
//
//  Created by Meng Fan on 2017/6/7.
//  Copyright © 2017年 Haowan. All rights reserved.
//

#import "ChildView.h"

#define kATTR_VIEW_HEIGHT  (4 * GET_HEIGT(44) + 50)


@interface ChildView () <UIGestureRecognizerDelegate>



@end

@implementation ChildView
{
    NSMutableArray *dataSource;
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
        
    }
    return self;
}

#pragma mark - setupViews
/**
 *  设置视图的基本内容
 */
- (void)setupViews {
    // 添加手势，点击背景视图消失
    UITapGestureRecognizer *tapBackGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeView)];
    tapBackGesture.delegate = self;
    [self addGestureRecognizer:tapBackGesture];
    
    
    UIView *contentView = [[UIView alloc] initWithFrame:(CGRect){0, mainHeight - kATTR_VIEW_HEIGHT, mainWidth, kATTR_VIEW_HEIGHT}];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    self.contentView = contentView;
}

#pragma mark - UIGestureRecognizerDelegate
//确定点击范围
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isDescendantOfView:self.contentView]) {
        return NO;
    }
    return YES;
}


#pragma mark - public
/**
 *  显示属性选择视图
 *
 *  @param showDirection 要在哪个视图中显示
 */
- (void)showInView:(ShowViewDirection )showDirection{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
     __weak typeof(self) _weakSelf = self;
    self.contentView.frame = CGRectMake(0, mainHeight, mainWidth, kATTR_VIEW_HEIGHT);;
    [self creatSubView];
     [UIView animateWithDuration:0.3 animations:^{
        _weakSelf.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _weakSelf.contentView.frame = CGRectMake(0, mainHeight - kATTR_VIEW_HEIGHT, mainWidth, kATTR_VIEW_HEIGHT);
    }];
}

/**
 *  属性视图的消失
 */
- (void)removeView {
    __weak typeof(self) _weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        _weakSelf.backgroundColor = [UIColor clearColor];
        _weakSelf.contentView.frame = CGRectMake(0, mainHeight, mainWidth, kATTR_VIEW_HEIGHT);
    } completion:^(BOOL finished) {
        [_weakSelf removeFromSuperview];
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, kATTR_VIEW_HEIGHT) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.backgroundColor = main_BackColor;
        [self.contentView addSubview:_tableView];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}


    
//   en 英文  language = @"zh-Hans"; // 简体中文
 
 - (void) creatSubView {
    
     NSArray *_arr = @[
                    @{@"title":@"Auto",
                      @"ls":@"auto",
                      @"selected":@"1"
                      },
                    @{@"title":@"中文简体",
                      @"ls":@"zh-Hans",
                      @"selected":@"0"
                      },
                    @{@"title":@"中文繁體",
                      @"ls":@"zh-Hant",
                      @"selected":@"0"
                      },
                    @{@"title":@"English",
                      @"ls":@"en",
                      @"selected":@"0"
                      }
                    ];
     dataSource = [NSMutableArray arrayWithArray:_arr];
     
     
     NSString *myLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:APP_Internationalization];
     
          [dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
              NSDictionary *dic = dataSource[idx];
              NSMutableDictionary *muDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
             if (isEmptyString(myLanguage) && idx == 0 ) {
                 [muDic setValue:@"1" forKeyPath:@"selected"];
              }else{
                  if ([muDic[@"ls"] isEqualToString:myLanguage]) {
                     [muDic setValue:@"1" forKey:@"selected"];
                 }else{
                     [muDic setValue:@"0" forKey:@"selected"];
                 }
             }
             
              [dataSource replaceObjectAtIndex:idx withObject:muDic];
         }];
 
//     [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
     
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = dataSource[indexPath.row];
    if (self.getSelected) {
        self.getSelected(dic[@"ls"]);
    }
     [self removeView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    UIImageView *imageView;
    UILabel *title;
    NSDictionary *dic = dataSource[indexPath.row];
    BOOL selected = [dic[@"selected"] boolValue];
    NSString * imageName = selected ? @"L_selected":@"L_normal";
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    
        imageView = [BaseTool imageWithName:imageName superView:cell];
         title = [BaseTool labelWithTitle:dic[@"title"] textAlignment:0 font:[UIFont systemFontOfSize:15] titleColor:[UIColor whiteColor]];
          [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.centerY.mas_equalTo(cell);
        }];
         [cell addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(imageView);
            make.left.mas_equalTo(imageView.mas_right).offset(10);
        }];
        cell.backgroundColor = main_BackColor;
    }
    
    imageView.image = [UIImage imageNamed:imageName];
    
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return GET_HEIGT(44);
}


@end

@implementation ShowPopView



@end


@implementation BaseBackView

{
    BOOL isFirst;
}
/**
 获取到点击的View
 */
-(UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *subview = [super hitTest:point withEvent:event];
    
    if(UIEventTypeTouches == event.type)
    {
        BOOL touchedInside = subview != self;
        if(!touchedInside)
        {
            for(UIView *s in self.subviews)
            {
                if(s == subview)
                {
                    //touched inside
                    touchedInside = YES;
                    break;
                }
            }
        }
        
        
        if (!touchedInside &&  _touchOutSide && !isFirst) {
            isFirst = YES;
            _touchOutSide();
        }
        
    }
    
    
    return subview;
}

@end

@implementation BaseNaviTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}
- (void)setFrame:(CGRect)frame {
    [super setFrame:CGRectMake(0, mainWidth/2-mainWidth/4, frame.size.width, frame.size.height)];
}

@end


//@implementation BaseView
//
//- (instancetype)init {
//    if (self = [super init]) {
//
//        [self setSubViews];
//    }
//    return  self;
//}
//
//- (void) setSubViews {
//
//}
//
//@end
