//
//  BaseViewController.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, assign) BOOL showBackBtn;
@property (nonatomic, assign) BOOL shareFriend;
@property (nonatomic,   copy) NSString *itemTitle;
@property (nonatomic,   copy) NSString *requestURL;
@property (nonatomic, strong) UIButton *navLeftBtn;
@property (nonatomic, strong) UIButton *navRightBtn;

@property (nonatomic,assign,readonly) NSInteger navi_Height;

@property (nonatomic,assign,readonly) NSInteger tab_barHeight;

@property (nonatomic ,copy)NSString *navi_title;
- (void)setWhiteLeftBackBtn;
- (void)actionCustomLeftBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                  title:(NSString *)title
                                 action:(void(^)())btnClickBlock;
- (void)actionCustomRightBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                   title:(NSString *)title
                                  action:(void(^)())btnClickBlock;


// 选择图片
- (void)choosePhotoes;

// 获取图片
- (void) getImageWithController:(UIImage *)image;

- (void) shareActiveAction:(UIImage *)image;

@end
