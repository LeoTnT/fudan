//
//  BaseSlider.m
//  BIT
//
//  Created by Sunny on 2018/4/25.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseSlider.h"

@implementation BaseSlider

- (CGRect)minimumValueImageRectForBounds:(CGRect)bounds {

    XSLog(@"minimumValueImageRectForBounds =%@",NSStringFromCGRect(bounds));
    return CGRectZero;
}

- (CGRect)maximumValueImageRectForBounds:(CGRect)bounds {
    
    XSLog(@"maximumValueImageRectForBounds =%@",NSStringFromCGRect(bounds));
    return CGRectZero;
}

@end
