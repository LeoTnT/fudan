//
//  ChildView.h
//  NMFDemo
//
//  Created by Meng Fan on 2017/6/7.
//  Copyright © 2017年 Haowan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ShowViewDirectionNoraml, //从下往上
    ShowViewDirectionUp,
} ShowViewDirection;
@interface ChildView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,copy)void (^getSelected)(NSString *string);
@property (nonatomic, weak) UIView *contentView;
@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic ,assign)ShowViewDirection type;
/**
 *  显示属性选择视图
 *
 *  @param showDirection 要在哪个视图中显示
 */
- (void)showInView:(ShowViewDirection )showDirection;

/**
 *  属性视图的消失
 */
- (void)removeView;


- (void) creatSubView;

@end


@interface ShowPopView :NSObject



@end


 
@interface BaseBackView :UIView

@property (nonatomic ,copy)void (^touchOutSide)();
@end

@interface BaseNaviTitleView :UIView

@end

//@interface BaseView :UIView
//
//- (void) setSubViews;
//@end
