//
//  TransBModel.h
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyAdverModel.h"

/*
 charge = 0;
 "coin_num" = "5.000000";
 "coin_num_all" = "<null>";
 "coin_type" = METO;
 comment = "\U975e\U8bda\U52ff\U6270";
 country = CN;
 freeze = "0.000000";
 id = 135;
 mobile = 15288979809;
 "money_type" = CNY;
 "pay_time_term" = 30;
 "pay_type" =                 (
 2
 );
 price = "2.000000";
 "price_height" = "2.000000";
 "price_lowest" = "0.000000";
 "price_max" = "10.00";
 "price_min" = "1.00";
 status = 1;
 "supplier_id" = 380;
 "supplier_info" =                 {
 "first_trade_time" = "2018-04-18 10:06:41";
 "history_count" = "1+METO";
 logo = "http://huobi-lsl.dsceshi.cn/upload/avatar/bb49ba128c3dc56102ba449d5375b988/20180419/0879f2664fb4c9e087a032929b579c40.JPEG";
 nickname = Lucy;
 "pass_avg_time" =                     {
 "avg_time" = 0;
 num = 0;
 };
 "praise_rate" = "0.00%";
 "register_time" = "2018-04-16 13:12:27";
 "trade_count" = 1;
 truename = "\U5f20\U5065";
 "trust_me_count" = 0;
 userid = 380;
 username = 168847;
 };
 "supplier_name" = 168847;
 "trade_button" = "Buy METO";
 type = 2;
 "u_time" = 1523857796;
 "w_time" = 1523857796;
 }

 */
@interface Supplier_info :NSObject

@property (nonatomic,copy) NSString *history_count;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *praise_rate;
@property (nonatomic,assign) NSInteger trade_count;
@property (nonatomic,copy) NSString *truename;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,assign) NSInteger trust_me_count;
@property (nonatomic,copy) NSString *userid;
@property (nonatomic,copy) NSString *username;

- (NSString *)getShowName;

@end

@interface TransBModel : MyAdverModel

@property (nonatomic,strong)Supplier_info *supplier_info;
@property (nonatomic,copy)NSString *trade_button;

@end



/*
 coin_type=选择货币；例：coin_type='BTC'
 
 country=选择国家；例：country='AC'
 
 price_min=价格区间的最低价，    price_max=价格区间的最高价
 
 pay_type=付款方式；例：pay_type=1
 
 money_type=货币类型；例：money_type='CNY'  这个参数不用传，没用；就一种：人民币
 
 page：*第几页        【不传默认显示第一页】
 
 limit ：*一页显示几条：【不传默认每页显示5条】
 */
@interface RequestCtradeBModel : NSObject
@property (nonatomic,copy) NSString *coin_type;
@property (nonatomic,copy) NSString *price_min;
@property (nonatomic,copy) NSString *price_max;
@property (nonatomic,copy) NSString *pay_type;
@property (nonatomic,copy) NSString *money_type;

@property (nonatomic,copy) NSString *mtype;
@property (nonatomic,copy) NSString *ctype;
@property (nonatomic,copy) NSString *country;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger limit;

@property (nonatomic,copy) NSString *name;


@end



