//
//  TransBModel.m
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TransBModel.h"

@implementation TransBModel

@end

@implementation RequestCtradeBModel

- (instancetype)init {
    if (self = [super init]) {
        
        self.page = 1;
        self.limit = 10;
    }
    return self;
}

- (NSString *)mtype {
    return [UserSingle sharedUserSingle].trans_sellType;
}
@end


@implementation  Supplier_info

- (NSString *)getShowName {
    if (self.nickname.length > 0) {
        return self.nickname;
    }
    return self.username;
}

@end
