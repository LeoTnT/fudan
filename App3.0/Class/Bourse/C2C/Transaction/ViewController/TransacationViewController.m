//
//  TransacationViewController.m
//  BIT
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//


#import "TransacationViewController.h"
#import "TransacationCell.h"
#import "BaseTransController.h"
#import "BuyBInforController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface TransacationViewController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation TransacationViewController
{
    BaseTransController *vc;
    
    NSString *sellType , *country;
}



-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([vc isKindOfClass:[BaseTransController class]]) {
        [self loadRefreshData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

 
  
    self.dataSource = [NSMutableArray array];
    [self.tableView registerNib:[UINib nibWithNibName:@"TransacationCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self setupRefesh];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    vc = self.navigationController.viewControllers.lastObject;
    if ([vc isKindOfClass:[BaseTransController class]]) {
 
 
        @weakify(self);

        [vc.sellButton addTarget:self action:@selector(sellAction:) forControlEvents:UIControlEventTouchUpInside];
        [vc.buyButton addTarget:self action:@selector(butAction:) forControlEvents:UIControlEventTouchUpInside];
//        [[vc rac_valuesForKeyPath:@"country" observer:nil]subscribeNext:^(id  _Nullable x) {
//            @strongify(self);
//            NSLog(@" - - - %@",x);
//            NSString *code = x;
//            if (!isEmptyString(code)) {
//                vc.country = x;
//                country = x;
//                self.page = 1;
//                [self loadRefreshData];
//            }
//        }];
        
        [RACObserve(vc, country) subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            NSLog(@" - - - %@",x);
            NSString *code = x;
            if (!isEmptyString(code)) {
                self.requestModel.country = x;
                country = x;
                self.page = 1;
                //                [self loadRefreshData];
            }
        }];
        
    }else{
         [self loadRefreshData];
     }
    
    self.shouldShowEmptyView = YES;
}

- (void) sellAction:(UIButton *)sender {
//    sender.selected = YES;
//    if (sender.selected) {
    vc.sellButton.backgroundColor = TOP_SELECTED_COLOR;
    vc.sellButton.selected = YES;
    vc.buyButton.backgroundColor = TOP_NORMAL_COLOR;
    vc.buyButton.selected = NO;
    
    [vc.sellButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [vc.buyButton setTitleColor:JYSMainSelelctColor forState:UIControlStateNormal];
    
    
    [UserSingle sharedUserSingle].trans_sellType = @"sell";
    self.requestModel.page = 1;
    self.requestModel.limit = 10;
    vc.buyButton.userInteractionEnabled = NO;
    vc.sellButton.userInteractionEnabled = NO;
//     }
    [self.tableView.mj_header beginRefreshing];
}


- (void) butAction:(UIButton *)sender {
//    sender.selected = YES;
//     if (sender.selected) {
    vc.buyButton.backgroundColor =TOP_SELECTED_COLOR;
    vc.buyButton.selected = YES;
    vc.sellButton.backgroundColor = TOP_NORMAL_COLOR;
    vc.sellButton.selected = NO;
    
    [vc.sellButton setTitleColor:JYSMainSelelctColor forState:UIControlStateNormal];
    [vc.buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [UserSingle sharedUserSingle].trans_sellType = @"buy";
    self.requestModel.page = 1;
    self.requestModel.limit = 10;
    vc.buyButton.userInteractionEnabled = NO;
    vc.sellButton.userInteractionEnabled = NO;
//      }
    
    [self.tableView.mj_header beginRefreshing];
    
    
}




- (void)loadRefreshData {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.requestModel.page = self.page;
//    self.requestModel.mtype = @;
    
    NSString * requestUrlString;
    if ([self.isFromString isEqualToString:@"searchUser"]) {
        requestUrlString = @"cuser/center/SearchUser";
    } else {
        requestUrlString = @"cuser/center/Ctrade";
    }
    
    NSLog(@"self.requestModel.mtype  =%@",self.requestModel.mj_keyValues);
     [[XSHTTPManager rac_POSTURL:requestUrlString params:self.requestModel.mj_keyValues] subscribeNext:^(resultObject *object) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
          if (self.page == 1) [self.dataSource removeAllObjects];
         if (object.status) {
            NSArray *arr = [TransBModel mj_objectArrayWithKeyValuesArray:object.data[@"data"]];
            if (arr.count ==0) [MBProgressHUD showMessage:Localized(@"no_more_data") view:self.view];
            [self.dataSource addObjectsFromArray:arr];
         }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        [self.tableView reloadData];
         [self endRefresh];
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
        [self endRefresh];
    }completed:^{
        if ([vc isKindOfClass:[BaseTransController class]]) {
            vc.buyButton.userInteractionEnabled = YES;
            vc.sellButton.userInteractionEnabled = YES;
        }
     }];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TransacationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    @weakify(self);
    cell.model = self.dataSource[indexPath.section];
    [cell setBuyBlock:^(TransBModel *model) {
        @strongify(self);
         BuyBInforController *vc = [BuyBInforController new];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
        
    }];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return  110;
}


#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text;
    if ([self.isFromString isEqualToString:@"searchAD"]) {
        text = Localized(@"暂无所搜广告信息");
    } else if ([self.isFromString isEqualToString:@"searchUser"]) {
        text = Localized(@"暂无所搜用户信息");
    } else {
        text = Localized(@"暂无该货币交易信息");
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


@end
