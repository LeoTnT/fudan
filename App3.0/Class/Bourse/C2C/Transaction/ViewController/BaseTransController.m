//
//  BaseTransController.m
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseTransController.h"
#import "TransacationViewController.h"
#import "AwesomeMenu.h"
#import "TransBModel.h"
#import "BITSearchVC.h"
#import "PchAderViewController.h"
#import "BaseHelpProblemViewController.h"
#import "ChouseCountryController.h"


@interface BaseTransController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate,AwesomeMenuDelegate, SDCycleScrollViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

@property (nonatomic,strong) UIView *backView;
@property (nonatomic,strong) AwesomeMenu *menu;

@property (strong, nonatomic) SDCycleScrollView *bannerView;
@property (strong, nonatomic) SDCycleScrollView *noticeView;

@property (nonatomic,strong) UIButton *leftButton;

/** 选中币名 */
@property (nonatomic, copy) NSString * selectCoinString;

@end

@implementation BaseTransController
{
    NSMutableArray *dataSource;
    CGRect menFrame;
    CGFloat naviHeight;
    CGPoint menuPoint;
    NSArray *titleArr;
    
 }

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.menu];
 
}


- (void)viewWillDisappear:(BOOL)animated {
    [self.menu removeFromSuperview];
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [BaseTool viewWithColor:[UIColor colorWithWhite:1 alpha:.8]];
        _backView.frame = [UIApplication sharedApplication].keyWindow.frame;
    }
    return _backView;
}

-(AwesomeMenu *)menu {
    if (!_menu) {
        UIImage *storyMenuItemImage1 = [UIImage imageNamed:@"Tran_fbgm_no"];
        UIImage *storyMenuItemImage = [UIImage imageNamed:@"Tran_fbgg_no"];
        AwesomeMenuItem *starMenuItem1 = [[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage1
                                                               highlightedImage:nil
                                                                   ContentImage:nil
                                                        highlightedContentImage:nil];
        
        AwesomeMenuItem *starMenuItem2 = [[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:nil
                                                                   ContentImage:nil
                                                        highlightedContentImage:nil];
        
        
        NSArray *menuItems = [NSArray arrayWithObjects:starMenuItem1, starMenuItem2, nil];
 
        AwesomeMenuItem *startItem = [[AwesomeMenuItem alloc] initWithImage:[UIImage imageNamed:@"Trac_fb"]
                                                           highlightedImage:nil
                                                               ContentImage:nil
                                                    highlightedContentImage:nil];
        
       
        AwesomeMenu *menu = [[AwesomeMenu alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) startItem:startItem menuItems:menuItems];
        menu.delegate = self;
//        menu.menuWholeAngle = -M_PI_2;

        menu.animationDuration = 0.3f;
        
        
        
//        menu.startPoint = CGPointMake(300, 300 );
         _menu = menu;
    }
    return _menu;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
     menFrame = [UIApplication sharedApplication].keyWindow.bounds;
    naviHeight = IS_iPhoneX ? 88:64;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 , FontNum(140), 30)];
    self.navigationItem.titleView = titleView;
    titleView.layer.borderColor = JYSMainSelelctColor.CGColor;
    titleView.layer.borderWidth = .5;
    titleView.layer.cornerRadius = 5;
    titleView.layer.masksToBounds = YES;
    
    UIButton *buyButton = [BaseTool buttonWithTitle:Localized(Localized(@"buy_coin")) titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14] superView:titleView];
    buyButton.selected = YES;
    buyButton.frame = CGRectMake(0, 0,FontNum(70), 30);
    UIButton *sellButton = [BaseTool buttonWithTitle:Localized(@"sale_coin") titleColor:JYSMainSelelctColor font:[UIFont systemFontOfSize:14] superView:titleView];
    buyButton.selected = NO;

    sellButton.frame = CGRectMake(CGRectGetMaxX(buyButton.frame), 0, FontNum(70), 30);
    buyButton.backgroundColor =JYSMainSelelctColor;
    sellButton.backgroundColor = main_BackColor;
 
    
    self.sellButton = sellButton;
    self.buyButton = buyButton;
    
    //搜索
    UIButton *searchButton = [UIButton new];
    [searchButton setBackgroundImage:[UIImage imageNamed:@"contact_search"] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    self.navigationItem.rightBarButtonItem = rightItem;
    searchButton.frame = CGRectMake(0, 0, 16, 16);
    [searchButton addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
    


//    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, mainWidth, GET_HEIGT(150)) delegate:self placeholderImage:[UIImage imageNamed:@"Trac_banner"]];
//    [self.view addSubview:self.bannerView];
//
//    UIImageView *image1 = [BaseTool imageWithName:@"Tran_lb" superView:self.view];
//    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(12);
//        make.top.mas_equalTo(self.bannerView.mas_bottom).offset(12);
//    }];

//    UIButton *btn = [BaseTool buttonWithImage:@"Tran_close" selected:nil superView:self.view];
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-18);
//        make.centerY.mas_equalTo(image1);
//    }];
//    [self.view layoutIfNeeded];
//
//    self.noticeView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(CGRectGetMaxX(image1.frame), CGRectGetMidY(image1.frame)-10, CGRectGetMinX(btn.frame)-CGRectGetMaxX(image1.frame), 20) delegate:self placeholderImage:nil];
//    self.noticeView.titleLabelBackgroundColor = [UIColor clearColor];
//    self.noticeView.titleLabelTextColor = [UIColor hexFloatColor:@"B9BABD"];
//    self.noticeView.titleLabelTextFont = [UIFont systemFontOfSize:13];
//    self.noticeView.titleLabelTextAlignment = NSTextAlignmentLeft;
//    self.noticeView.onlyDisplayText = YES;
//    self.noticeView.scrollDirection = UICollectionViewScrollDirectionVertical;
//    self.noticeView.delegate =self;
//    self.noticeView.backgroundColor = XSYCOLOR(0xF4FBFF);
//    [self.view addSubview:self.noticeView];
    
    
//    UIButton * backBtn = [XSUITool buttonWithFrame:CGRectMake(0, 0, 25, 25) buttonType:UIButtonTypeCustom Target:self action:@selector(backBtnClick) titleColor:nil titleFont:15 backgroundColor:nil image:[UIImage imageNamed:@"nav_back"] backgroundImage:nil title:nil];
//    backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
//    
//    UIBarButtonItem * backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    UIButton *leftButton = [BaseTool buttonTitle:Localized(@"china") image:nil superView:nil];
    [leftButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    leftButton.titleLabel.font = [UIFont systemFontOfSize:14];
    leftButton.contentEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
//    [leftButton setImagePosition:(LXMImagePositionRight) spacing:2];
    leftButton.frame = CGRectMake(0, 0, 60, 30);
    [leftButton addTarget:self action:@selector(changeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * countryBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItems = @[countryBarButtonItem];
//    [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
     self.leftButton = leftButton;
    UIImageView *imageView = [BaseTool imageWithName:@"mall_down" superView:leftButton];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftButton.mas_right).offset(1);
        make.centerY.mas_equalTo(leftButton);
    }];
    
    
    NSArray *aaaa =  [BaseTool getBIT_LIST];
    if (aaaa) {
        dataSource = [[NSMutableArray alloc] initWithArray:aaaa];
        [self setupPageView];
    }else{
        dataSource = [NSMutableArray array];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CoinTypes" params:nil] subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (object.status) {
                NSArray *arr = object.data[@"data"];
                [arr.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                    [dataSource addObject:x];
                }completed:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (dataSource.count !=0) {
                            [self setupPageView];
                        }
                    });
                }];
            }else{
                [MBProgressHUD showMessage:object.info view:self.view];
            }
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        }];
    }
    
//    [[XSHTTPManager rac_POSTURL:@"ctools/ctools/GetAds" params:nil] subscribeNext:^(resultObject *object) {
//        
//        if (object.status) {
//            NSArray *arr = object.data[@"data"];
//            self.bannerView.localizationImageNamesGroup = arr;
//        }
//        
//    } error:^(NSError * _Nullable error) {
//        
//    }];
    
    
//    [[XSHTTPManager rac_POSTURL:@"basic/noticesys/Lists" params:nil] subscribeNext:^(resultObject *object) {
//
//        if (object.data) {
//            titleArr = object.data[@"data"];
//            self.noticeView.titlesGroup = titleArr;
//        }
//    }];
}

- (void)backBtnClick {
    self.tabBarController.navigationController.navigationBar.translucent = YES;
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    if ([cycleScrollView isEqual:self.noticeView]) {
         BaseHelpProblemViewController *vc = [BaseHelpProblemViewController new];
        vc.isList = YES;
        vc.title =@"公告";
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (void) changeButtonAction:(UIButton *)sender {
 
    ChouseCountryController *vc = [ChouseCountryController new];
    @weakify(self);
    [vc setChouseCountry:^(CountryModel *mode) {
        @strongify(self);
        [self.leftButton setTitle:mode.name forState:UIControlStateNormal];
        self.country = mode.code;
    }];
    [self.navigationController pushViewController:vc animated:YES];
 
}


- (void)setupPageView {
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = MY_ORDER_NORMAL_COLOR;
    configure.titleSelectedColor = MY_ORDER_SELECTED_COLOR;
    configure.indicatorColor = JYSMainSelelctColor;
     [self.view layoutIfNeeded];
    
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0,self.isNotTab? kStatusBarAndNavigationBarHeight:0, self.view.frame.size.width, 40) delegate:self titleNames:dataSource configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];
    self.selectCoinString = dataSource[self.pageTitleView.selectedIndex];
    [UserSingle sharedUserSingle].trans_sellType = @"buy";
    NSMutableArray *chia = [NSMutableArray array];
    [dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        TransacationViewController *fourVC = [[TransacationViewController alloc] init];
        RequestCtradeBModel *model = [RequestCtradeBModel new];
         model.mtype = @"buy";
        model.ctype = dataSource[idx];
        fourVC.requestModel= model;
        [chia addObject:fourVC];
    }];
    NSArray *childArr = chia;
    CGFloat sssss = CGRectGetMaxY(self.pageTitleView.frame)  +10;
    CGFloat contentViewHeight = self.view.frame.size.height-sssss ;
//    if (mainHeight == 568) {
//        contentViewHeight = self.view.frame.size.height -self.tab_barHeight -sssss- 50;
//    }else {
//        contentViewHeight = self.view.frame.size.height -2 *self.tab_barHeight -sssss -10 ;
//    }
    
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, sssss,mainWidth, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
    
 
    
}


- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    self.selectCoinString = dataSource[selectedIndex];
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx {
 
    [self getBusinessInformationWithIndex:idx];
}


- (void)awesomeMenuWillAnimateOpen:(AwesomeMenu *)menu {

//      [self.backView addSubview:menu];
//     [[UIApplication sharedApplication].keyWindow addSubview:self.backView];
    menu.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
 
}

- (void)awesomeMenuWillAnimateClose:(AwesomeMenu *)menu {
    menu.backgroundColor = [UIColor clearColor];
//      [self.backView removeFromSuperview];
//    [self.view addSubview:menu];
//     [[UIApplication sharedApplication].keyWindow addSubview:self.menu];
 }
-(void)search:(UIButton *)sender{
    [self.navigationController pushViewController:[BITSearchVC new] animated:YES];
}

// 获取商家认证状态
- (void) getBusinessInformationWithIndex:(NSInteger)idx {
    [XSTool showProgressHUDWithView:self.view];
    [[XSHTTPManager rac_POSTURL:@"cuser/cuser/UserCenter" params:nil] subscribeNext:^(resultObject *object) {
        [XSTool hideProgressHUDWithView:self.view];
        if (object.status) {
            NSInteger approve_user = [object.data[@"approve_user"] integerValue];
            NSInteger approve_supply = [object.data[@"approve_supply"] integerValue];
            if (approve_user != 1) {
                
//                [UIAlertView showWithTitle:Localized(@"您还未实名认证，请实名认证") message:nil cancelButtonTitle:Localized(@"cancel_btn") otherButtonTitles:@[@"确定"] tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
//                    if (buttonIndex == 1) {
//                        [XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"];
//
//                    }
//                }];
                
                [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"您还未实名认证，请实名认证") message:nil CallBackBlock:^(NSInteger btnIndex) {
                    if (btnIndex == 1) {
                        
                        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
                    }
                } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
                
            } else if (approve_supply != 1 && [AppConfigManager ShardInstnce].appConfig.supply_enter) {
                [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"您还没有成为商家,是否申请成为商家") message:nil CallBackBlock:^(NSInteger btnIndex) {
                    if (btnIndex == 1) {
                        
                        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"MerchantApplicationController"] animated:YES];
                    }
                } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
            } else {
                PchAderViewController *vc = [PchAderViewController new];
                vc.type = idx == 0? 1:2;
                [self.navigationController pushViewController:vc animated:YES];
            }

        } else {
            [XSTool showToastWithView:self.view Text:object.info];
        }
    } error:^(NSError * _Nullable error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
    
}
@end

