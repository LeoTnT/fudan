//
//  BuyBInforController.m
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BuyBInforController.h"
#import "BuyHeaderView.h"
#import "BuyConvertCell.h"
#import "TransactionTipsCell.h"
#import "OrderAlertView.h"
#import "MyOrderInforController.h"
#import "HelpCenterVC.h"
#import "RSAEncryptor.h"

@interface BuyBInforController ()

@property (copy, nonatomic) NSString *amount;   // 要购买的金额（CNY人民币）

@property (copy, nonatomic) NSString *number_BIT;   // 数量

@property (nonatomic,strong) BuyConvertCell *buyCell;

@property (nonatomic,copy) NSString *B_Type;

@end

@implementation BuyBInforController
{
    UIButton *buyButton ;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.autoHideKeyboard = YES;
    @weakify(self);
    
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"help") action:^{
        @strongify(self);
        HelpCenterVC *vc = [HelpCenterVC new];
        vc.isAppeal = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    CGFloat tabHeight =  self.navi_Height + tabBarHeight;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(mainHeight - tabHeight);
    }];
    [self.tableView registerNib:[UINib nibWithNibName:@"BuyConvertCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TransactionTipsCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    
    UIView *topBackView = [BaseTool viewWithColor:[UIColor whiteColor]];
    UIView *headerView = [BaseTool viewWithColor:[UIColor whiteColor]];
    
    // 顶部信息视图
    BuyHeaderView *view = [BuyHeaderView initializeBuyHeaderView];
    view.model = self.model;
    
    UIFont *font = [UIFont systemFontOfSize:14];
     NSString *commm = [NSString stringWithFormat:@"%@%@",Localized(@"advert_des"),self.model.comment];
    UILabel *infor = [BaseTool labelWithTitle:commm textAlignment:0 font:font titleColor:[UIColor hexFloatColor:@"999999"]];
    infor.numberOfLines = 0;
    
    [topBackView addSubview:view];
    [topBackView addSubview:infor];
    [self.view addSubview:topBackView];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.tableView);
        make.height.mas_equalTo(135);
    }];
    [self.view addSubview:topBackView];
    
    [infor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_bottom) ;
        make.left.mas_equalTo(13);
        make.right.mas_equalTo(-13);
    }];

    [topBackView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(infor.mas_bottom).offset(10);
    }];

    [self.view layoutIfNeeded];
   CGFloat hhh =  CGRectGetHeight(topBackView.frame);
    
    
    [topBackView removeFromSuperview];
    
    [headerView addSubview:view];
    [view mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.left.right.top.mas_equalTo(headerView);
        make.height.mas_equalTo(135);
    }];

    [headerView addSubview:infor];
    [infor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_bottom) ;
        make.left.mas_equalTo(13);
        make.right.mas_equalTo(-13);
    }];
     headerView.frame = CGRectMake(0, 64, mainWidth, hhh);
    self.tableView.tableHeaderView = headerView;

    
 
    
    
    UIView *footerView = [BaseTool viewWithColor:[UIColor whiteColor]];

    [self.view addSubview:footerView];
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tableView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(mainWidth, 50));
    }];
    
    
    UIButton *contact = [BaseTool buttonTitle:Localized(@"relative_user") image:@"Buy_infor_contact" superView:footerView];
    [contact setTitleColor:[UIColor hexFloatColor:@"5888ED"] forState:UIControlStateNormal];
    [contact setImagePosition:(LXMImagePositionLeft) spacing:10];
    [contact addTarget:self action:@selector(contactAction) forControlEvents:UIControlEventTouchUpInside];
    [contact mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(footerView);
        make.left.mas_equalTo(12);
    }];
    
    
    
    buyButton = [BaseTool buttonWithTitle:Localized(@"购买") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] superView:footerView];
    NSString *B_Type;
    if (self.model.type.integerValue == 2) {
        B_Type = Localized(@"购买");
        [buyButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
        self.buyCell.buyNumber.placeholder = Localized(@"请输入购买数量");
    }else if (self.model.type.integerValue == 1){
        B_Type = Localized(@"selltwo");
        [buyButton setBackgroundColor:MY_SELL_B_COLOR];
        [buyButton setTitle:B_Type forState:UIControlStateNormal];
        self.buyCell.buyNumber.placeholder = Localized(@"请输入卖出数量");
    }

    [buyButton addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [buyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, mainWidth-125, 0, 0));
    }];

    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[RACSignal combineLatest:@[self.buyCell.buyWorth.rac_textSignal,
                                self.buyCell.buyNumber.rac_textSignal]]
     subscribeNext:^(RACTuple * x) {
         self.amount = self.buyCell.buyWorth.text;
         self.number_BIT = self.buyCell.buyNumber.text;
         NSLog(@"number_BIT  =%@  self.amount =%@",self.number_BIT,self.amount);
     }];
    
//    self.amount = RACObserve(self.buyCell.buyWorth, text);
//    self.number_BIT = RACObserve(self.buyNumber.buyWorth, text);
}

- (void)contactAction {
    Supplier_info *infor = self.model.supplier_info;
    if (!infor || isEmptyString(infor.userid)) {
        return;
    }
    if ([infor.userid isEqualToString:[UserInstance ShardInstnce].uid]) {
        [XSTool showToastWithView:self.view Text:@"不能与自己聊天！"];
        return;
    }
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:infor.userid title:infor.nickname avatarURLPath:infor.logo];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    [self.navigationController pushViewController:chatVC animated:YES];
}

- (void)buyAction {
//    if ([self.amount floatValue] < self.model.price_min || [self.amount floatValue] > self.model.price_max) {
//        [MBProgressHUD showMessage:@"交易金额错误！" view:self.view];
//        return;
//    }
    if ([self.amount floatValue] < self.model.price_min) {
        [MBProgressHUD showMessage:@"输入金额低于交易限额" view:self.view];
        return;
    } else if ([self.amount floatValue] > self.model.price_max) {
        [MBProgressHUD showMessage:@"输入金额高于交易限额" view:self.view];
        return;
    }
    
    
    NSDictionary *alertModel = @{@"price":[NSString stringWithFormat:@"%f",self.model.price],
                                 @"amount":isEmptyString(self.amount)?@"0":self.amount,
                                 @"number":isEmptyString(self.number_BIT)?@"0":self.number_BIT,
                                 @"coin_type":isEmptyString(self.model.coin_type)?@"":self.model.coin_type,
                                 @"type":self.model.type
                                 };
    
    
    ShowOrderAler *alerView = [ShowOrderAler new];
    alerView.type = OrderAlertTypeMakeOrder;
    alerView.model = alertModel;
    [alerView show];
    
    [[alerView.sureButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
        if ([self.model.type integerValue] == 1) {
            [self sellAction];
            
        }else{
            [self buy];
        }
        [alerView hidden];
    }];
    
 
    
 }

- (void) sellAction {
    if (isEmptyString(self.model.ID) || isEmptyString(self.amount)) {
        [MBProgressHUD showMessage:@"订单有误" view:self.view];
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"请输入资金密码" preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alert) weakAlert = alert;
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         if ([weakAlert.textFields.lastObject text].length==0||[[weakAlert.textFields.lastObject text] isEqualToString:@""]) {
            [XSTool showToastWithView:self.view Text:@"请填写资金密码！"];
        } else {
            
            NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:[weakAlert.textFields.lastObject text]]];
 
            NSDictionary *dic = @{@"id":self.model.ID,@"amount":self.amount,@"paypwd":encryptOldPassWord};
            [self sell:dic];
        }
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // 添加文本框
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.secureTextEntry = YES;
        textField.textColor = [UIColor blackColor];
    }];
    // 弹出对话框
    [self presentViewController:alert animated:YES completion:nil];
    
    
    

}


- (void) sell:(NSDictionary *)dic {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//     [[XSHTTPManager rac_POSTURL:@"cuser/corder/Finish" params:dic] subscribeNext:^(resultObject *object) {
    [[XSHTTPManager rac_POSTURL:@"cuser/center/Sell" params:dic] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
             [MBProgressHUD showMessage:@"出售成功" view:self.view hideTime:1 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}



- (void) buy {
    
    if (isEmptyString(self.model.ID) || isEmptyString(self.amount)) {
        [MBProgressHUD showMessage:@"订单有误" view:self.view];
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/center/Buy" params:@{@"id":self.model.ID,@"amount":self.amount}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            MyOrderInforController *vc = [[MyOrderInforController alloc] init];
            vc.navigationItem.title = @"购买订单";
            vc.order_num = object.returnValue;
            [self.navigationController pushViewController:vc animated:YES];
        } else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
}

- (void)setModel:(TransBModel *)model {
    _model = model;
//    NSString *B_Type;
    if (model.type.integerValue == 2) {
//        B_Type = Localized(@"购买");
        [buyButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
     }else if (model.type.integerValue == 1){
//        B_Type = Localized(@"卖出");
         [buyButton setBackgroundColor:MY_SELL_B_COLOR];
//         [buyButton setTitle:B_Type forState:UIControlStateNormal];
     }
     self.title = [NSString stringWithFormat:@"%@",model.trade_button];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        self.buyCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        self.buyCell.model = self.model;
        @weakify(self);
        [self.buyCell setValueChanged:^(NSString *value) {
            @strongify(self);
            self.amount = value;
        }];
        return self.buyCell;
    }
    TransactionTipsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    return indexPath.section == 0 ?125: 150;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1;
}

@end
