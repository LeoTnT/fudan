//
//  TransacationViewController.h
//  BIT
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//


#define TOP_SELECTED_COLOR  mainColor
#define TOP_NORMAL_COLOR  [UIColor whiteColor]


#import "BaseViewController.h"
#import "TransBModel.h"
@interface TransacationViewController : XSBaseTablViewController


/** 来自哪个视图控制器 */
@property (nonatomic, copy) NSString * isFromString;

@property (nonatomic,strong) RequestCtradeBModel *requestModel;


@end
