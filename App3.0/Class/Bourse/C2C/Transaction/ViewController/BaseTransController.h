//
//  BaseTransController.h
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"



@interface BaseTransController : BaseViewController

@property (nonatomic,strong) UIButton *buyButton;
@property (nonatomic,strong) UIButton *sellButton;


@property (nonatomic,copy) NSString *country;
@property (nonatomic,assign)BOOL isNotTab;
@end
