//
//  TransacationCell.m
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TransacationCell.h"

@implementation TransacationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.buyAction.layer.cornerRadius = 2;
    self.buyAction.layer.masksToBounds = YES;
    self.logo.layer.cornerRadius = 38/2;
    self.logo.layer.masksToBounds = YES;
}
 
- (IBAction)butButtonAction:(UIButton *)sender {
    if (self.buyBlock) {
        self.buyBlock(self.model);
    }
}

- (void)setModel:(TransBModel *)model {
    _model = model;
    [self.logo yy_setImageWithURL:[NSURL URLWithString:model.supplier_info.logo] placeholder:[UIImage imageNamed:@"no_pic"]];
    
//    self.name.text = [model.supplier_info getShowName];
    
        if (model.supplier_info.nickname.length > 0) {
            self.name.text =  model.supplier_info.nickname;
        }else{
            if (model.supplier_info.username.length >4) {
                NSString *supplier_name =[NSString stringWithFormat:@"%@",model.supplier_info.username];
                NSString *str1 = [supplier_name substringToIndex:2];//截取前两位
                NSString *str2 = [supplier_name substringFromIndex:supplier_name.length-2];//截取后两位
                self.name.text = [NSString stringWithFormat:@"%@***%@",str1,str2];
            }else{
                self.name.text =  model.supplier_info.username;

            }
        }

    
    self.jy.text = [NSString stringWithFormat:@"%@:%ld",Localized(@"trade"),(long)model.supplier_info.trade_count];
    self.hp.text = [NSString stringWithFormat:@"%@:%@",Localized(@"favourable"),model.supplier_info.praise_rate];
    self.xr.text = [NSString stringWithFormat:@"%@:%ld",Localized(@"trust"),model.supplier_info.trust_me_count];
    self.xe.text = [NSString stringWithFormat:@"%.2f~%.2fCNY",model.price_min,model.price_max];
    self.price.text = [NSString stringWithFormat:@"%.2fCNY",model.price];
    if (model.pay_type.count !=0) {
        NSArray *arr = model.pay_type;
        NSMutableArray *payStringArr =[BaseTool setLabelTitleWithNumber:arr];
         NSArray *arrrLabel = @[self.pt_zfb,self.pt_wx,self.pt_yhk];
         [arrrLabel enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
             UIImageView *label = arrrLabel[idx];
            if (payStringArr.count >idx) {
                label.image = [UIImage imageNamed:payStringArr[idx]];//payStringArr[idx];
                label.hidden = NO;
            }else{
                label.hidden = YES;
            }
        }];
        

    }
    
//    NSString *B_Type;
    if (model.type.integerValue == 2) {
//        B_Type = @"购买";
        self.buyAction.backgroundColor = MY_ORDER_SELECTED_COLOR;
    }else if (model.type.integerValue == 1){
//        B_Type = @"卖出";
        self.buyAction.backgroundColor = MY_SELL_B_COLOR;
    }
//    self.coin_number.text = isEmptyString(model.coin_num)?@"0":model.coin_num;
    [self.buyAction setTitle:model.trade_button forState:UIControlStateNormal];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
