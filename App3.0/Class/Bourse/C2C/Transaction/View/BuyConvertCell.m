//
//  BuyConvertCell.m
//  BIT
//
//  Created by mac on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BuyConvertCell.h"

@implementation BuyConvertCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [self.buyNumber setValue:[UIColor hexFloatColor:@"999999"] forKeyPath:@"_placeholderLabel.textColor"];

    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(TransBModel *)model {
    _model = model;
    self.limit.text = [NSString stringWithFormat:@"%@%@",Localized(@"remainder_count"),model.coin_num];
    self.buyWorth.placeholder = [NSString stringWithFormat:@"%.2f~%.2f",model.price_min,model.price_max];
    
    if ([model.type integerValue] == 2) {
        self.showInfor.text = Localized(@"你想要购买多少？");
        [self.buyAllButton setTitle:Localized(@"全部买入") forState:UIControlStateNormal];
    }else{
        self.showInfor.text = Localized(@"你想要卖出多少？");
        [self.buyAllButton setTitle:Localized(@"全部卖出") forState:UIControlStateNormal];
    }
    if (!isEmptyString(model.coin_type)) {
        
        self.B_type.text = model.coin_type;
    }
}

- (IBAction)textDidChanged:(id)sender {
    if ([sender isEqual:self.buyWorth]) {
        CGFloat num = [self.buyWorth.text floatValue]/self.model.price;
        self.buyNumber.text = [NSString stringWithFormat:@"%.2f",num];
    } else {
        CGFloat worth = [self.buyNumber.text floatValue]*self.model.price;
        self.buyWorth.text = [NSString stringWithFormat:@"%.2f",worth];
    }
    
    if (self.valueChanged) {
        self.valueChanged(self.buyWorth.text);
    }
}

- (IBAction)buyAllButtonClick {
    self.buyNumber.text = self.model.coin_num;
    CGFloat worth = [self.buyNumber.text floatValue]*self.model.price;
    self.buyWorth.text = [NSString stringWithFormat:@"%.2f",worth];
    
    if (self.valueChanged) {
        self.valueChanged(self.buyWorth.text);
    }
}


@end
