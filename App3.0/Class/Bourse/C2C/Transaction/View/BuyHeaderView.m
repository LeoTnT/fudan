//
//  BuyHeaderView.m
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BuyHeaderView.h"

@implementation BuyHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.avatar.layer.cornerRadius = 19;
    self.avatar.layer.masksToBounds = YES;
}

+ (instancetype)initializeBuyHeaderView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"BuyHeaderView" owner:nil options:nil].lastObject;
}

- (void)setModel:(TransBModel *)model {
    _model = model;
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:model.supplier_info.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
//    self.userName.text = model.supplier_name;
    
    if (model.supplier_info.nickname.length > 0) {
        self.userName.text =  model.supplier_info.nickname;
    }else{
        if (model.supplier_info.username.length >4) {
            NSString *supplier_name =[NSString stringWithFormat:@"%@",model.supplier_info.username];
            NSString *str1 = [supplier_name substringToIndex:2];//截取前两位
            NSString *str2 = [supplier_name substringFromIndex:supplier_name.length-2];//截取后两位
            self.userName.text = [NSString stringWithFormat:@"%@***%@",str1,str2];
        }else{
            self.userName.text =  model.supplier_info.username;
            
        }
    }

    
    
    self.userDesc.text = [NSString stringWithFormat:@"%@%.2f~%.2f CNY",Localized(@"trade_limit_money"),model.price_min,model.price_max];
    self.tradeCount.text = [NSString stringWithFormat:@"%ld",(long)model.supplier_info.trade_count];
    self.trustCount.text = [NSString stringWithFormat:@"%ld",(long)model.supplier_info.trust_me_count];
    self.praiseRate.text = model.supplier_info.praise_rate;
    self.histroy.text = model.supplier_info.history_count;
    self.price.text = [NSString stringWithFormat:@"%.2f CNY",model.price];
    if (model.pay_type.count !=0) {
        NSArray *arr = model.pay_type;
        NSMutableArray *payStringArr =[BaseTool setLabelTitleWithNumber:arr];
 
        
        NSArray *arrrLabel = @[self.image1,self.image2,self.image3];
        
        [arrrLabel enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            UIImageView *label = arrrLabel[idx];
            if (payStringArr.count >idx) {
                label.image = [UIImage imageNamed:payStringArr[idx]];
                label.hidden = NO;
            }else{
//                label.text = @"";
                label.hidden = YES;
            }
        }];
        
        
    }
    
    
}
@end
