//
//  TransacationCell.h
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransBModel.h"
@interface TransacationCell : UITableViewCell
@property (nonatomic,copy) void (^buyBlock)(TransBModel *model);


@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *jy;

@property (weak, nonatomic) IBOutlet UILabel *hp;
@property (weak, nonatomic) IBOutlet UILabel *xr;

@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *xe;
@property (weak, nonatomic) IBOutlet UIButton *buyAction;
@property (weak, nonatomic) IBOutlet UILabel *xm;

@property (weak, nonatomic) IBOutlet UIImageView *pt_zfb;
@property (weak, nonatomic) IBOutlet UIImageView *pt_wx;
@property (weak, nonatomic) IBOutlet UIImageView *pt_yhk;

@property (nonatomic,strong) TransBModel *model;

@end
