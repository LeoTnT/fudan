//
//  BuyHeaderView.h
//  BIT
//
//  Created by Sunny on 2018/3/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransBModel.h"

@interface BuyHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userDesc;
@property (weak, nonatomic) IBOutlet UILabel *tradeCount;
@property (weak, nonatomic) IBOutlet UILabel *trustCount;
@property (weak, nonatomic) IBOutlet UILabel *praiseRate;
@property (weak, nonatomic) IBOutlet UILabel *histroy;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;


@property (strong, nonatomic) TransBModel *model;

+ (instancetype)initializeBuyHeaderView;

@end
