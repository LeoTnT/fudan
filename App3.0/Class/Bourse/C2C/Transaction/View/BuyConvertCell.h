//
//  BuyConvertCell.h
//  BIT
//
//  Created by mac on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransBModel.h"

@interface BuyConvertCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *buyWorth;
@property (weak, nonatomic) IBOutlet UITextField *buyNumber;
@property (weak, nonatomic) IBOutlet UILabel *limit;
@property (weak, nonatomic) IBOutlet UILabel *showInfor;
@property (weak, nonatomic) IBOutlet UILabel *B_type;

@property (weak, nonatomic) IBOutlet UIButton *buyAllButton;

@property (strong, nonatomic) TransBModel *model;
@property (nonatomic,copy) void (^valueChanged)(NSString *value);
@end
