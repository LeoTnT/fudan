//
//  BaseConfig.m
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseConfig.h"
#import "AppDelegate.h"
@implementation BaseConfig

+ (UITabBarController *)enterMainViewController
{
    NSArray *arr = @[
                     @{@"normal":@"hq_N",
                       @"selected":@"hq_S",
                       @"title":@"币行情",
                       @"vc":@"B_ListViewController"
                       },
                     @{@"normal":@"social_N",
                       @"selected":@"social_S",
                       @"title":@"社交",
                       @"vc":@"TabFriendsVC"
                       },
                     
                     @{@"normal":@"jy_N",
                       @"selected":@"jy_S",
                       @"title":@"交易",
                       @"vc":@"BaseTransController"
                       },
                     @{@"normal":@"w_N",
                       @"selected":@"w_S",
                       @"title":@"钱包",
                       @"vc":@"WalletViewController"
                       },
                     @{@"normal":@"main_N",
                       @"selected":@"main_S",
                       @"title":Localized(@"my"),
                       @"vc":@"MainViewController"
                       }];
    NSMutableArray *tabArr = [NSMutableArray array];
    for (NSInteger x = 0; x < arr.count; x ++) {
        NSDictionary *dic = arr[x];
        UIViewController *vc = [NSClassFromString(dic[@"vc"]) new];
        BaseNaviController *userNav = [BaseConfig naviNormalImage:dic[@"normal"] selectedImage:dic[@"selected"] title:Localized(dic[@"title"]) controller:vc tag:9004];
        [tabArr addObject:userNav];
    }
 
 
    
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = tabArr;
     [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"686B79"]} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor hexFloatColor:@"EB4554"]} forState:UIControlStateSelected];
     tabBarController.tabBar.barTintColor = mainColor;
     [[UINavigationBar appearance] setBackgroundImage:[UIImage xl_imageWithColor:mainColor size:CGSizeZero] forBarMetrics:UIBarMetricsDefault];
     // 设置TabBar标题偏移
    [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    
    return tabBarController;
}


+ (BaseNaviController *) naviNormalImage:(NSString *)normal selectedImage:(NSString *)selecteImage title:(NSString *)title controller:(UIViewController *)viewControler tag:(NSInteger )tag{
    
    BaseNaviController *navi = [[BaseNaviController alloc] initWithRootViewController:viewControler];
    UIImage *unselectedImage = [UIImage imageNamed:normal];
    UIImage *selectedImage = [UIImage imageNamed:selecteImage];
    navi.base_interactivePopGestureRecognizerType = InteractivePopGestureRecognizerEdge;
    navi.topViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:title
                                                                      image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                              selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    navi.topViewController.tabBarItem.tag = tag;
    
    
    return navi;
    
}


+ (void) setRootViewController {
    
    

    
    [UIApplication sharedApplication].keyWindow.rootViewController = [BaseConfig enterMainViewController];
}


@end


@implementation ShowListModel

@end
