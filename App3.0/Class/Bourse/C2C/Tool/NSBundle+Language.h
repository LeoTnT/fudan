//
//  NSBundle+Language.h
//  lianxiceshi
//
//  Created by 杨鑫 on 2017/5/11.
//  Copyright © 2017年 杨鑫. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)
    
+ (void)setLanguage:(NSString *)language;
    
@end
