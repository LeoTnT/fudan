//
//  UILabel+BaseLabel.m
//  BIT
//
//  Created by Sunny on 2018/4/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "UILabel+BaseLabel.h"

@implementation UILabel (BaseLabel)

+ (void)load{
    Method imp = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myImp = class_getInstanceMethod([self class], @selector(myInitWithCoder:));
    method_exchangeImplementations(imp, myImp);
    
}

- (id)myInitWithCoder:(NSCoder *)aDecode{
    
    [self myInitWithCoder:aDecode];
    if (self) {
        // 部分不想改变字体的 把tag值设置成555跳过
        if (self.tag != 555) {
            CGFloat fontSize = self.font.pointSize;
             self.font = [UIFont systemFontOfSize:fontSize * getSizeScale()];
        }
    }
    return self;
}


@end

@implementation UIButton (BaseButton)

+ (void)load{
    Method imp = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myImp = class_getInstanceMethod([self class], @selector(myInitWithCoder:));
    method_exchangeImplementations(imp, myImp);
}

- (id)myInitWithCoder:(NSCoder *)aDecode{
    
    [self myInitWithCoder:aDecode];
    if (self) {
        // 部分不想改变字体的 把tag值设置成555跳过
        if (self.titleLabel.tag != 555) {
            CGFloat fontSize = self.titleLabel.font.pointSize;
            self.titleLabel.font = [UIFont systemFontOfSize:fontSize * getSizeScale()];
        }
    }
    return self;
}

@end

@implementation UIFont (BaseFont)

+ (void)load {
     Method newMethod = class_getClassMethod([self class], @selector(adjustFont:));
     Method method = class_getClassMethod([self class], @selector(systemFontOfSize:));
     method_exchangeImplementations(newMethod, method);
}

+ (UIFont *)adjustFont:(CGFloat)fontSize {
    UIFont *newFont = nil;
    newFont = [UIFont adjustFont:fontSize *getSizeScale()];
    return newFont;
}
@end
