//
//  UserSingle.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#define singletonInterface(className)      + (instancetype)shared##className;

#define singletonImplementation(className) \
static className *_instance; \
+ (id)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
+ (instancetype)shared##className \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}

#import "LoginModel.h"

 
@interface UserSingle : NSObject
singletonInterface(UserSingle)

@property (nonatomic ,strong)LoginDataParser *user;
@property (nonatomic,strong) RACSubject *siginleSubject;
 
@property (nonatomic,strong) NSMutableArray *tableData;
@property (nonatomic,strong) NSMutableArray *tableIndexData;

@property (nonatomic,copy) NSString *trans_sellType;;


- (void)saveAccount:(NSDictionary *)account;
- (void)deleteAccount:(NSDictionary *)account;
- (NSArray *)getAccounts;
- (void)logout;

@end

//@interface UserDataParser : NSObject
//
//@property (nonatomic, copy) NSString *username;
//@property (nonatomic, copy) NSString *mobile;
//@property (nonatomic, copy) NSString *email;
//@property (nonatomic, copy) NSString *approve_mobile;
//@property (nonatomic, copy) NSString *approve_user;//实名认证 -1未提交 0已提交 1通过 2拒绝
//@property (nonatomic, copy) NSString *approve_supply;// 店铺是否认证 (1是/0否/-1未提交/2拒绝)
//@property (nonatomic, copy) NSString *nickname;
//@property (nonatomic, copy) NSString *rank;
//@property (nonatomic, copy) NSString *logo;
//@property (nonatomic, copy) NSString *password;
//@property (nonatomic, copy) NSString *pay_password;//资金密码 (未设置为空/设置为******)
//@property (nonatomic, copy) NSString *sex;
//@property (nonatomic, copy) NSString *truename;
//@property (nonatomic, copy) NSString *logo_fandom_bg;
//@property (nonatomic,copy) NSString *card_no;//身份证号
//
//@property (nonatomic, copy) NSString *img_card;
//@property (nonatomic, copy) NSString *comment;
//@property (nonatomic, assign) unsigned int isSign;
//
//@property (nonatomic, copy) NSString *rank_name;    // 等级
//@property (nonatomic, copy) NSString *fav_product_num;  // 收藏商品
//@property (nonatomic, copy) NSString *fav_supply_num;   // 收藏店铺
//@property (nonatomic, copy) NSString *history_num;      // 浏览记录
//@end

//@interface UserParser : NSObject
//
//@property (nonatomic, retain) UserDataParser *data;
//@end
