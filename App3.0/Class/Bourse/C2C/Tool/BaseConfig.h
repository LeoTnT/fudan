//
//  BaseConfig.h
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseConfig : NSObject<UITabBarControllerDelegate>

+ (UITabBarController *)enterMainViewController;
+ (void) setRootViewController;
@end


@interface ShowListModel :NSObject

@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *image;
@property (nonatomic ,copy)NSString *vcName;
@property (nonatomic ,copy)NSString *detailTextLabel;
@end
