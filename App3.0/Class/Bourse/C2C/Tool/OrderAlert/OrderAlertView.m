//
//  OrderAlertView.m
//  BIT
//
//  Created by Sunny on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "OrderAlertView.h"

@implementation OrderAlertView

-(void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
}

+ (instancetype)initializeOrderAlertView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"OrderAlertView" owner:nil options:nil].lastObject;
}



- (IBAction)cancelAction:(UIButton *)sender {
    
}

- (IBAction)sureAction:(id)sender {
}


@end


@implementation OrderCancelAlert

-(void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    self.orderCancelTipLabel.text = [NSString stringWithFormat:@"如果您已向卖家付款，请千万不要取消交易。取消规则：买方当日连续%d笔取消，会限制当日买入功能。",[AppConfigManager ShardInstnce].appConfig.c2c_order_cancel_num];
    
}
+ (instancetype)initializeOrderCancelAlert{
    
    return [[NSBundle mainBundle] loadNibNamed:@"OrderCancelAlert" owner:nil options:nil].lastObject;
}


- (IBAction)thinkAboutAction:(UIButton *)sender {
}
- (IBAction)sureAction:(id)sender {
}

- (IBAction)isSelected:(UIButton *)sender {
    sender.selected = !sender.selected;
}


@end


@implementation OrderEvaluateAlert

-(void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    self.label1.layer.cornerRadius = 5;
    self.label1.layer.masksToBounds = YES;
    self.label2.layer.cornerRadius = 5;
    self.label2.layer.masksToBounds = YES;
    self.evaluate= @"1";
    
}
+ (instancetype)initializeOrderEvaluateAlert {
    
    return [[NSBundle mainBundle] loadNibNamed:@"OrderEvaluateAlert" owner:nil options:nil].lastObject;
}


- (IBAction)smileAction:(UIButton *)sender {
    self.label1.backgroundColor = MY_ORDER_SELECTED_COLOR;
    self.label1.textColor = [UIColor whiteColor];
    self.label2.backgroundColor = [UIColor whiteColor];
    self.label2.textColor = MAIN_MAIN_LINE_COLOR;
    self.evaluate= @"1";
}

- (IBAction)cryAction:(UIButton *)sender {
    self.label1.backgroundColor = [UIColor whiteColor];
    self.label1.textColor = MAIN_MAIN_LINE_COLOR;
    self.label2.backgroundColor = MY_ORDER_SELECTED_COLOR;
    self.label2.textColor = [UIColor whiteColor];
    self.evaluate= @"3";
}

- (IBAction)sureAction:(id)sender {
}
- (IBAction)cancelAction:(UIButton *)sender {
}

@end



@implementation ShowQRCodeView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat imagesize = (mainWidth - 40*2) ;
        UIView *backView = [BaseTool viewWithColor:[UIColor whiteColor]];
        backView.layer.cornerRadius = 5;
        backView.layer.masksToBounds = YES;
        [self addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(self).offset(-50);
            make.size.mas_equalTo(CGSizeMake(imagesize, imagesize));
        }];
        
        _imageView = [BaseTool imageWithName:@"no_pic" superView:backView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(40, 40, 40, 40));
        }];
        
        
        self.closeButton = [BaseTool buttonWithImage:@"showQR_close" selected:nil superView:self];
        [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.top.mas_equalTo(backView.mas_bottom).offset(24);
            make.size.mas_equalTo(CGSizeMake(38, 38));
        }];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(dealLongPress:)];
        [_imageView addGestureRecognizer:longPress];
    }
    return self;
}

- (void)setSubViews {
    [super setSubViews];
    CGFloat imagesize = (mainWidth - 40*2) ;
    UIView *backView = [BaseTool viewWithColor:[UIColor whiteColor]];
    backView.layer.cornerRadius = 5;
    backView.layer.masksToBounds = YES;
    [self addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self).offset(-50);
        make.size.mas_equalTo(CGSizeMake(imagesize, imagesize));
    }];
    
    _imageView = [BaseTool imageWithName:@"no_pic" superView:backView];
     [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 40, 40, 40));
    }];

    
    self.closeButton = [BaseTool buttonWithImage:@"showQR_close" selected:nil superView:self];
     [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(backView.mas_bottom).offset(24);
        make.size.mas_equalTo(CGSizeMake(38, 38));
    }];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(dealLongPress:)];
    [_imageView addGestureRecognizer:longPress];
    

}


-(void)dealLongPress:(UIGestureRecognizer*)gesture{
 
    if (!_imageView.image) {
        return;
    }
 
    if (self.pressBlock) {
        self.pressBlock();
    }
    
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
     UIAlertAction *action1 = [UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:Localized(@"person_qr_save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 
        [self loadImageFinished:_imageView.image];
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"识别二维码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *resultOfQR = [BaseTool touchQRImageGetStringWithImage:_imageView.image];
        if (isEmptyString(resultOfQR)) {
            [MBProgressHUD showMessage:@"识别错误" view:self];
            return;
        }
        NSURL *url = [NSURL URLWithString:resultOfQR];
        
        if ([resultOfQR containsString:@"wxp://"]) {
            url = [NSURL URLWithString:@"weixin://"];
            [[UIApplication sharedApplication] openURL:url];
        }else{
            BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:url];
             if (canOpen) {
                 [[UIApplication sharedApplication] openURL:url];
            }
         }
 
    }];
 
    //添加按钮
    [alertC addAction:action1];
    [alertC addAction:action2];
    [alertC addAction:action3];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertC animated:YES completion:nil];
 
}


- (void)loadImageFinished:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    if (!error) {
        [MBProgressHUD showMessage:Localized(@"my_skill_sava_success") view:self];
    }
    
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
}


@end

@implementation ShowOrderAler
{
    UIView *subViews;
}

- (instancetype)init {
    if (self = [super init]) {
        
        self.frame = [UIApplication sharedApplication].keyWindow.bounds;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
         [[UIApplication sharedApplication].keyWindow addSubview:self];
    }
    return self;
}

- (void)setType:(OrderAlertType)type {
    _type = type;
    
    switch (type) {
        case OrderAlertTypeMakeOrder:{
             OrderAlertView *view = [OrderAlertView initializeOrderAlertView];
            self.sureButton = view.sureBUtton;
            [[view.sureBUtton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                [self hidden];
            }];
            [[view.cancelButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                [self hidden];
            }];
             subViews = view;
            
        }
            break;
        case OrderAlertTypeCancelOrder:
        {
            OrderCancelAlert *view = [OrderCancelAlert initializeOrderCancelAlert];
            self.sureButton = view.sureButton;
            

            [[view.canSure rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                self.isGoOn = x.selected;
            }];
 
            [[view.thinkButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                
                [self hidden];
            }];
            subViews = view;
         }
            break;
        case OrderAlertTypeEvaluateOrder:
        {
            OrderEvaluateAlert *view = [OrderEvaluateAlert initializeOrderEvaluateAlert];
            self.sureButton = view.sureButton;
 
            [[view.cancelButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                [self hidden];
            }];
            subViews = view;
 
        }
            break;
 
            
            case OrderAlertTypeShowQRCode:
        {
         
            ShowQRCodeView *view = [ShowQRCodeView new];
            view.alpha = 0;
            view.frame = CGRectMake(0, 0, 0, 0);
            [UIView animateWithDuration:.25 animations:^{
            view.frame = CGRectMake(0, 0, mainWidth, mainHeight);
                view.alpha = 1;
            }];
             [self addSubview:view];
            [[view.closeButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
                [UIView animateWithDuration:.25 animations:^{
//                     view.alpha = 0;
                    view.frame = CGRectMake(0, mainHeight, mainWidth, mainHeight);
                }completion:^(BOOL finished) {
                    [self removeFromSuperview];
                }];
            }];
            [view setPressBlock:^{
                [UIView animateWithDuration:.25 animations:^{
                     view.frame = CGRectMake(0, mainHeight, mainWidth, mainHeight);
                }completion:^(BOOL finished) {
                    [self removeFromSuperview];
                }];
            }];
            subViews = view;
        }
            
            break;
    }
    
}


//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self hidden];
//}


- (void) hidden {
    
     [UIView animateWithDuration:.15 animations:^{
        subViews.frame = CGRectMake((mainWidth)/2, (mainHeight)/2, 0, 0);
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
    
}

- (void) show {
    
    subViews.frame = CGRectMake((mainWidth)/2, (mainHeight)/2, 0, 0);
    
     [UIView animateWithDuration:.15 animations:^{
         
         subViews.frame = CGRectMake((mainWidth-280)/2, (mainHeight - 224)/2 - 50, 280, 224);
    }];
     [self addSubview:subViews];
    
}

- (void)dealloc {
    
    XSLog(@" - - -");
}

- (void)setModel:(NSDictionary *)model {
    _model = model;
    
    switch (self.type) {
        case OrderAlertTypeMakeOrder:
        {
            OrderAlertView *view = (OrderAlertView *)subViews;
            view.number.text = [NSString stringWithFormat:@"%@ %@",model[@"number"],model[@"coin_type"]];
            view.price.text =  [NSString stringWithFormat:@"%@ CNY",model[@"price"]];
            view.je.text =[NSString stringWithFormat:@"%@ CNY",model[@"amount"]];
            view.infor.text = [NSString stringWithFormat:@"提醒:请确认价格再下单，下单后此交易的%@将托管锁定，请放心购买。",model[@"coin_type"]];
      
            if ([model[@"type"] integerValue] == 1) {
                view.priceTitle.text = @"出售价格";
                view.moneyTitle.text = @"出售金额";
                view.numberTitle.text = @"出售数量";
                [view.cancelButton setTitle:@"放弃出售" forState:UIControlStateNormal];
                [view.sureBUtton setTitle:@"确认出售" forState:UIControlStateNormal];
            }else {
                view.priceTitle.text = @"购买价格";
                view.moneyTitle.text = @"购买金额";
                view.numberTitle.text = @"购买数量";
                [view.cancelButton setTitle:Localized(@"cancel_btn") forState:UIControlStateNormal];
                [view.sureBUtton setTitle:Localized(@"material_dialog_default_title") forState:UIControlStateNormal];
            }
            
        }
            break;
        case OrderAlertTypeCancelOrder:
        {
            
        }
            break;
        case OrderAlertTypeEvaluateOrder:
        {
            
        }
            break;
            
        case OrderAlertTypeShowQRCode:
        {
            ShowQRCodeView *view = (ShowQRCodeView *)subViews;
            view.imageView.image = self.model[@"image"];
 
        }
            
            break;
      }
}


- (NSString *)evaluate {
    OrderEvaluateAlert *view = (OrderEvaluateAlert *)subViews;
    view.evaluate = view.evaluate;
    return view.evaluate;
}
@end
