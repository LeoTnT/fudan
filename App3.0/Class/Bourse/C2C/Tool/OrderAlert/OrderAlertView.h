//
//  OrderAlertView.h
//  BIT
//
//  Created by Sunny on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderAlertView : UIView
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *je;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *infor;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *priceTitle;
@property (weak, nonatomic) IBOutlet UILabel *moneyTitle;
@property (weak, nonatomic) IBOutlet UILabel *numberTitle;

@property (weak, nonatomic) IBOutlet UIButton *sureBUtton;
+ (instancetype)initializeOrderAlertView;
@end



@interface OrderCancelAlert :UIView
@property (weak, nonatomic) IBOutlet UIButton *thinkButton;
@property (weak, nonatomic) IBOutlet UIButton *sureButton;

@property (weak, nonatomic) IBOutlet UIButton *canSure;

@property (weak, nonatomic) IBOutlet UILabel *orderCancelTipLabel;


+ (instancetype)initializeOrderCancelAlert;

@end


@interface OrderEvaluateAlert :UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *sureButton;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@property (nonatomic,copy) NSString *evaluate;

+ (instancetype)initializeOrderEvaluateAlert;

@end


@interface ShowQRCodeView:BaseView

@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UIButton *closeButton;

@property (nonatomic,copy) void(^pressBlock)();

@end

typedef enum : NSUInteger {
    OrderAlertTypeMakeOrder,
    OrderAlertTypeCancelOrder,
    OrderAlertTypeEvaluateOrder,
    OrderAlertTypeShowQRCode
} OrderAlertType;

@interface ShowOrderAler :UIView

@property (nonatomic,strong) UIButton *sureButton;

@property (nonatomic,assign) BOOL isGoOn;

@property (nonatomic,assign) OrderAlertType type;

@property (nonatomic,strong) NSDictionary *model;
@property (nonatomic,copy) NSString *evaluate;
- (void) show;
- (void) hidden;
@end
