//
//  UserSingle.m
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "UserSingle.h"
#import "BaseTool.h"


@implementation UserSingle
singletonImplementation(UserSingle)
-(RACSubject *)siginleSubject {
    if (!_siginleSubject) {
        _siginleSubject = [RACSubject subject];
    }
    return _siginleSubject;
}
- (LoginDataParser *)user {
    if (!_user) {
        _user = [BaseTool user];
        _user.headImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_user.logo]]];
     }
    return _user;
}

- (void)logout
{
 
     [BaseTool logout];
   
}

- (void)saveAccount:(NSDictionary *)account {
    NSArray *listArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
    NSMutableArray *mutCopyArr = [NSMutableArray arrayWithArray:listArr];
    for (NSDictionary *dic in listArr) {
        if ([dic[@"account"] isEqualToString:account[@"account"]]) {
            [mutCopyArr removeObject:dic];
            break;
        }
    }
     [mutCopyArr insertObject:account atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:mutCopyArr forKey:ACCOUNT_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)deleteAccount:(NSDictionary *)account {
    NSArray *listArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
    NSMutableArray *mutCopyArr = [NSMutableArray arrayWithArray:listArr];
    for (NSDictionary *dic in listArr) {
        if ([dic[@"account"] isEqualToString:account[@"account"]]) {
            [mutCopyArr removeObject:dic];
            break;
        }
    }
     [[NSUserDefaults standardUserDefaults] setObject:mutCopyArr forKey:ACCOUNT_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getAccounts {
     return [[NSUserDefaults standardUserDefaults] arrayForKey:ACCOUNT_LIST];
}
@end
//@implementation UserDataParser
//
//@end

//@implementation UserParser
// 
//@end


