//
//  UILabel+BaseLabel.h
//  BIT
//
//  Created by Sunny on 2018/4/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
 
static inline CGFloat getSizeScale () {
    CGFloat SizeScale = mainHeight >=667 ? 1 :mainHeight /667;
    return SizeScale;
}

static inline CGFloat size_Fit(CGFloat size) {
    CGFloat SizeScale = getSizeScale() *size;
    return SizeScale;
}

@interface UIFont (BaseLabel)

@end



@interface UIFont (BaseButton)

@end



@interface UIFont (BaseFont)

@end
