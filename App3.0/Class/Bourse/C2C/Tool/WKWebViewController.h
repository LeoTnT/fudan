//
//  WKWebViewController.h
//  BIT
//
//  Created by Sunny on 2018/4/11.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

//typedef NS_ENUM(NSInteger,WKWebViewURLType) {
//    WKWebViewURLNormal = 0,
//
//};


@interface WKWebViewController : BaseViewController

@property (nonatomic, strong) NSArray *weakScriptArray;

@property (nonatomic ,strong) WKWebView *webView;

@property (nonatomic, copy) NSString *urlStr;

@property (nonatomic, strong) UIButton *closeButton;

@property(nonatomic,strong)UIButton *reloadBtn;

@property (nonatomic ,assign,)WKWebViewURLType urlType;

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message;

- (void) setSubViews;

- (void)closeClick;

/**返回的控制器可能需要刷新界面*/
- (void)backClick;


@end


//@interface WeakScriptMessageDelegate : NSObject<WKScriptMessageHandler>
//
//@property (nonatomic, assign) id<WKScriptMessageHandler> scriptDelegate;
//
//- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;
//
//@end
