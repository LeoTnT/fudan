//
//  BaseNaviController.m
//  BIT
//
//  Created by apple on 2018/3/17.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseNaviController.h"
#import "PchAderViewController.h"
@interface BaseNaviController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>

@end

@implementation BaseNaviController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
//    [self.navigationBar setBackgroundImage:[UIImage yy_imageWithColor:mainColor] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    @weakify(self);
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        @strongify(self);
        self.interactivePopGestureRecognizer.delegate = self;
    }
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"%s",__FUNCTION__);
     if (self.childViewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
      if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = YES;
    }
     [super pushViewController:viewController animated:YES];
}


#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
 
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
         navigationController.interactivePopGestureRecognizer.enabled = YES;
     }
    
 }

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    
    
}

#pragma mark - 手势具体方法
- (void)panGestureRecognizer:(UIPanGestureRecognizer *)gesture{
    CGPoint point = [gesture translationInView:gesture.view];
    CGFloat progress = point.x / gesture.view.bounds.size.width;
 
    if (gesture.state == UIGestureRecognizerStateBegan) {
        self.percentDrivenInteractiveTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self popViewControllerAnimated:YES];
    }
    else if (gesture.state == UIGestureRecognizerStateChanged) {
        [self.percentDrivenInteractiveTransition updateInteractiveTransition:progress];
    }
    else if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateEnded) {
        //判断手势滑动距离是否超过屏幕的一半，如果超过一半则完成pop动画
        if (progress > 0.5) {
            [self.percentDrivenInteractiveTransition finishInteractiveTransition];
        }
        else{
            [self.percentDrivenInteractiveTransition cancelInteractiveTransition];
        }
        self.percentDrivenInteractiveTransition = nil;
    }
}

- (void)setBase_interactivePopGestureRecognizerType:(InteractivePopGestureRecognizerType)base_interactivePopGestureRecognizerType {
 
    switch (base_interactivePopGestureRecognizerType) {
        case InteractivePopGestureRecognizerNone:
 
             break;
        case InteractivePopGestureRecognizerEdge:
        {
            //添加边缘返回手势
            UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
            edgePanGestureRecognizer.edges = UIRectEdgeLeft;
            [self.view addGestureRecognizer:edgePanGestureRecognizer];
        }
            break;
        case InteractivePopGestureRecognizerFullScreen:
        {
            //添加全屏返回手势
            UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
            [self.interactivePopGestureRecognizer.view addGestureRecognizer:panGestureRecognizer];
        }
            break;
    }
}


#pragma mark - 判断页面数，页面数>1时执行手势
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (self.viewControllers.count > 1) {
        return YES;
    }
    else{
        return NO;
    }
}

@end
