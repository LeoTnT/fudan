//
//  WKWebViewController.m
//  BIT
//
//  Created by Sunny on 2018/4/11.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "WKWebViewController.h"

@interface WKWebViewController ()<WKNavigationDelegate,WKScriptMessageHandler,UIScrollViewDelegate>
@property (nonatomic ,strong)WKWebViewConfiguration *configur;
@property (nonatomic, strong) UIButton *backButton;
@end

@implementation WKWebViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self setSubViews];
    [self.view addSubview:self.webView];
 
}

-(void)dealloc{
    
    [self.weakScriptArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[self.webView configuration].userContentController removeScriptMessageHandlerForName:self.weakScriptArray[idx]];
    }];
    
}

#pragma mark-Lazy Loading
-(WKWebViewConfiguration *)configur {
    if (!_configur) {
        _configur = [[WKWebViewConfiguration alloc] init];
        WKPreferences *preferences = [[WKPreferences alloc] init];
        _configur.preferences = preferences;
        preferences.javaScriptEnabled = YES;
    }
    return _configur;
}

- (WKWebView *)webView
{
    if (_webView == nil) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-self.navi_Height ) configuration:self.configur];
         _webView.navigationDelegate = self;
        _webView.allowsBackForwardNavigationGestures = YES;
 
         @weakify(self);
        [RACObserve(_webView, estimatedProgress)  subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self observeWebChang];
         }];
        
     }
    return _webView;
}

#pragma mark-Private
-(void)setSubViews{
    
    self.backButton = [[UIButton alloc]  initWithFrame:CGRectMake(13, self.navi_Height/2-20/2, 20, 20)];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left1 = [[UIBarButtonItem alloc] initWithCustomView:self.backButton];
    
    self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.backButton.frame)+10, self.navi_Height/2-20/2, 15 ,15)];
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"nav_close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left2 = [[UIBarButtonItem alloc] initWithCustomView:self.closeButton];
    
    UIBarButtonItem *fixBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixBar.width = 20;
    
    self.navigationItem.leftBarButtonItems = @[left1,fixBar,left2];
    
    self.reloadBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-13-20, self.navi_Height/2-20/2, 15, 15)];
    [self.reloadBtn  setBackgroundImage:[UIImage imageNamed:@"mall_exchange"] forState:UIControlStateNormal];
    [self.reloadBtn  addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.reloadBtn];
    self.navigationItem.rightBarButtonItem = item;
    
}


- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
     [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10]];
}

-(void)setUrlType:(WKWebViewURLType)urlType {
    _urlType = urlType;
 
    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
    [self.weakScriptArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [userContentController addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:self.weakScriptArray[idx]];
    }];
    self.configur.userContentController = userContentController;
  
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10]];
 
    
}
- (void)backClick {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)closeClick {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)refreshData{
    [self.webView reload];
}

- (void) observeWebChang {
    
    if (self.webView.estimatedProgress== 1) {
        [UIView animateWithDuration:0.7                                                                                                                                                                                                                                  animations:^{
            self.reloadBtn.transform=CGAffineTransformRotate(self.reloadBtn.transform, M_PI);
        }];
    }else{
        [UIView animateWithDuration:0.7                                                                                                                                                                                                                                  animations:^{
            self.reloadBtn.transform=CGAffineTransformRotate(self.reloadBtn.transform, M_PI);
        }];
    }
 
}

#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    //    [XSTool showProgressHUDTOView:self.view withText:nil];
 
}
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    //    [XSTool hideProgressHUDWithView:self.view];
 
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    //    [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    //    [XSTool hideProgressHUDWithView:self.view];
 }

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
}

// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
     //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
 
    
}

@end


//@implementation WeakScriptMessageDelegate
//
//- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate
//{
//    self = [super init];
//    if (self) {
//        _scriptDelegate = scriptDelegate;
//    }
//    return self;
//}
//
//- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
//{
//    [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
//}
//
//@end
