//
//  SearchVC.m
//  BIT
//
//  Created by 孙亚男 on 2018/4/4.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BITSearchVC.h"
#import "SearchADVC.h"
#import "SearchUserVC.h"

@interface BITSearchVC ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;
@end

@implementation BITSearchVC
#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.edgesForExtendedLayout=UIRectEdgeNone;
//    @weakify(self);
//    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:@"" action:^{
//        @strongify(self);
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
    
    self.navigationItem.title=Localized(@"search");
    [self setupPageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
- (void)setupPageView {
    
    NSArray *titleArr = @[@"找广告",@"找用户"];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = MY_ORDER_NORMAL_COLOR;
    configure.titleSelectedColor = mainColor;
    configure.indicatorColor = mainColor;
    configure.titleFont = [UIFont boldSystemFontOfSize:15];
    //    configure.indicatorAdditionalWidth = 100; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
     /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];
    
    NSMutableArray *chia = [NSMutableArray array];
    SearchADVC *adVC=[SearchADVC new];
    [chia addObject:adVC];
    SearchUserVC *userVC=[SearchUserVC new];
    [chia addObject:userVC];
    
     NSArray *childArr = chia;
    CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame);
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

@end
