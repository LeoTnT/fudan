//
//  SearchUserVC.m
//  BIT
//
//  Created by 孙亚男 on 2018/4/4.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchUserVC.h"
#import "TransacationViewController.h"
@interface SearchUserVC ()
@property(nonatomic,strong)UITextField *inputField;
@end

@implementation SearchUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.inputField=[[UITextField alloc] initWithFrame:CGRectMake(0, 10, mainWidth, 44)];
     self.inputField.placeholder=@"请输入用户昵称";
     self.inputField.font=[UIFont systemFontOfSize:14];
//     self.inputField.backgroundColor=[UIColor hexFloatColor:@"161823"];
    self.inputField.backgroundColor=main_BackColor;
    self.inputField.leftViewMode=UITextFieldViewModeAlways;
    UIView *leftView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    self.inputField.leftView=leftView;
    [self.inputField setValue:[UIColor hexFloatColor:@"5B5C64"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.view addSubview:self.inputField];
    UIButton *searchBtn=[[UIButton alloc] initWithFrame:CGRectMake(30, 76, mainWidth-60, 44)];
    [self.view addSubview:searchBtn];
    [searchBtn setTitle:Localized(@"search") forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    searchBtn.titleLabel.font=[UIFont systemFontOfSize:16];
 
    [searchBtn setBackgroundColor:mainColor];
    [searchBtn addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)search:(UIButton *)btn{
    [self.view endEditing:YES];
    if (self.inputField.text.length==0) {
        [XSTool showToastWithView:self.view Text:@"请输入用户昵称"];
        return;
    }
 
    NSDictionary *paraDic=@{@"name":self.inputField.text};
 
    TransacationViewController *vc = [TransacationViewController new];
    vc.isFromString = @"searchUser";
    vc.requestModel = [RequestCtradeBModel mj_objectWithKeyValues:paraDic];
    vc.title = @"搜索结果";
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
@end
