//
//  SearchADVC.m
//  BIT
//
//  Created by 孙亚男 on 2018/4/4.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchADVC.h"
#import "XSDatePickerView.h"
#import "ChouseCountryController.h"
#import "PickerViewSheet.h"
#import "TransacationViewController.h"

@implementation SearchPayTypeModel
@end

@interface SearchADVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,XSDatePickerDelegate>
@property(nonatomic,strong)UIButton *searchBtn;
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)UITextField *lowField,*highField;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)NSMutableArray *coinTypeArray,*payTypeArray,*moneyTypeArray;

//用到的数据
@property(nonatomic,copy)NSString *coinTypeStr;//币种
@property(nonatomic,strong)CountryModel *countryModel;
@property(nonatomic,strong)SearchPayTypeModel *payTypeModel,*moneyTypeModel;//付款方式,货币类型
@end

@implementation SearchADVC
-(UITextField *)lowField{
    if (!_lowField) {
        _lowField=[[UITextField alloc] initWithFrame:CGRectMake(mainWidth-215, 7.5, 80, 29)];
        _lowField.delegate=self;
        _lowField.placeholder=@"最低价";
        _lowField.keyboardType=UIKeyboardTypeDecimalPad;
        _lowField.font=[UIFont systemFontOfSize:13];
        _lowField.textAlignment=NSTextAlignmentCenter;
        _lowField.backgroundColor=[UIColor hexFloatColor:@"F0F3F6"];
         _lowField.borderStyle = UITextBorderStyleRoundedRect;
        [_lowField setValue:[UIColor hexFloatColor:@"5B5C64"] forKeyPath:@"_placeholderLabel.textColor"];
    }
    return _lowField;
}
-(UITextField *)highField{
    if (!_highField) {
        _highField=[[UITextField alloc] initWithFrame:CGRectMake(mainWidth-110, 7.5, 80, 29)];
        _highField.delegate=self;
        _highField.placeholder=@"最高价";
        _highField.font=[UIFont systemFontOfSize:13];
         _highField.keyboardType=UIKeyboardTypeDecimalPad;
          _highField.backgroundColor=[UIColor hexFloatColor:@"F0F3F6"];
        _highField.borderStyle = UITextBorderStyleRoundedRect;
         _highField.textAlignment=NSTextAlignmentCenter;
            [_highField setValue:[UIColor hexFloatColor:@"5B5C64"] forKeyPath:@"_placeholderLabel.textColor"];
    }
    return _highField;
}
-(UIView *)line{
    if (!_line) {
        _line=[[UIView alloc] initWithFrame:CGRectMake(mainWidth-110-6.5-12, 21.5, 12, 1)];
//        _line.backgroundColor=[UIColor hexFloatColor:@"C9C9C9"];
        _line.backgroundColor=[UIColor hexFloatColor:@"E5E5E5"];
    }
    return _line;
}
#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.payTypeModel = [SearchPayTypeModel new];
    self.countryModel = [CountryModel new];
    self.countryModel.ID = @"49";
    self.countryModel.code = @"CN";
    self.countryModel.name = Localized(@"china");
//    self.view.backgroundColor=[UIColor hexFloatColor:@"0E0F1A"];
    self.view.backgroundColor=main_BackColor;
    self.titleArray=@[Localized(@"choose_coin_type"),@"选择国家",@"价格区间(元)",Localized(@"pay_type") ];
    self.tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
//    self.tableView.backgroundColor=[UIColor hexFloatColor:@"161823"];
    self.tableView.backgroundColor=main_BackColor;
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth,88)];
        UIButton *searchBtn=[[UIButton alloc] initWithFrame:CGRectMake(30, 22, mainWidth-60, 44)];
        [view addSubview:searchBtn];
        [searchBtn setTitle:Localized(@"search") forState:UIControlStateNormal];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        searchBtn.titleLabel.font=[UIFont systemFontOfSize:16];
 
        [searchBtn setBackgroundColor:mainColor];
        [searchBtn addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView=view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Private
-(void)search:(UIButton *)sender{
    if (self.coinTypeStr.length==0) {
        [XSTool showToastWithView:self.view Text:@"请选择币种"];
        return;
    }
 
    if (self.lowField.text.length==0|| self.highField.text.length==0 ||[self.highField.text floatValue]<[self.lowField.text floatValue]) {
        [XSTool showToastWithView:self.view Text:@"请重新输入价格区间"];
        return;
    }
    if (self.payTypeModel.code.length==0) {
        [XSTool showToastWithView:self.view Text:@"请选择付款方式"];
        return;
    }
//    if (self.moneyTypeModel.code.length==0) {
//        [XSTool showToastWithView:self.view Text:@"请选择货币类型"];
//        return;
//    }
//    搜索参数：
//
//    coin_type=选择货币；例：coin_type='BTC'
//
//    country=选择国家；例：country='AC'
//
//    price_min=价格区间的最低价，    price_max=价格区间的最高价
//
//    pay_type=付款方式；例：pay_type=1
//
//    money_type=货币类型；例：money_type='CNY'  这个参数不用传，没用；就一种：人民币
//
//    page：*第几页    【不传默认显示第一页】
//
//    limit ：*一页显示几条：【不传默认每页显示5条】
//
//    【搜索用户时：传一个参数 name，例：name='张三'】
//
//
//    提交，都是提交到：center/center/Ctrade
    
    //跳转 TransacationViewController控制器
    
    //这是参数  除了page和limit
    
//    @property (nonatomic,copy) NSString *mtype;
//    @property (nonatomic,copy) NSString *ctype;
//    @property (nonatomic,copy) NSString *country;
//    @property (nonatomic,assign) NSInteger page;
//    @property (nonatomic,assign) NSInteger limit;
//
//    @property (nonatomic,copy) NSString *name;

    NSDictionary *paraDic=@{@"coin_type":self.coinTypeStr,@"country":self.countryModel.code,@"price_min":self.lowField.text,@"price_max":self.highField.text,@"pay_type":self.payTypeModel.code};
    
    TransacationViewController *vc = [TransacationViewController new];
    vc.isFromString = @"searchAD";
    vc.requestModel = [RequestCtradeBModel mj_objectWithKeyValues:paraDic];
    vc.title = @"搜索结果";
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
#pragma mark-TableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"normalCell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
//        cell.backgroundColor=[UIColor hexFloatColor:@"161823"];
        cell.backgroundColor=main_BackColor;
        cell.textLabel.font=[UIFont systemFontOfSize:14];
//        cell.textLabel.textColor=[UIColor hexFloatColor:@"FEFEFE"];
        cell.textLabel.textColor=[UIColor blackColor];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.font=[UIFont systemFontOfSize:14];
        cell.detailTextLabel.textColor=[UIColor hexFloatColor:@"5B5C64"];
        UIView *line1=[[UIView alloc] initWithFrame:CGRectMake(12, 43.5, mainWidth-24, 0.5)];
//        line1.backgroundColor=[UIColor colorWithRed:34/255.0 green:36/255.0 blue:52/255.0 alpha:1];
        line1.backgroundColor=XSYCOLOR(0xe5e5e5);
        [cell.contentView addSubview:line1];
    }
    cell.textLabel.text=self.titleArray[indexPath.row];
    cell.detailTextLabel.text=Localized(@"choose");

    if (self.coinTypeStr.length && indexPath.row==0) {
        cell.detailTextLabel.text=self.coinTypeStr;
    }
    if (self.countryModel.name.length && indexPath.row==1) {
        
          cell.detailTextLabel.text=self.countryModel.name;
    }
    if (self.payTypeModel.code.length && indexPath.row==3) {
         cell.detailTextLabel.text=self.payTypeModel.code_desc;
    }
    if (self.moneyTypeModel.code.length && indexPath.row==4) {
        cell.detailTextLabel.text=self.moneyTypeModel.code_desc;
    }
    if(indexPath.row==2){
        [cell.contentView addSubview:self.lowField];
        [cell.contentView addSubview:self.line];
         [cell.contentView addSubview:self.highField];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.detailTextLabel.text=@"";
    }
    if (![cell.detailTextLabel.text isEqualToString:Localized(@"choose")]) {
        cell.detailTextLabel.textColor=[UIColor blackColor];
    }else{
        cell.detailTextLabel.textColor=[UIColor hexFloatColor:@"5B5C64"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
//    view.backgroundColor=[UIColor hexFloatColor:@"0E0F1A"];
    view.backgroundColor=main_BackColor;
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        if (self.coinTypeArray.count==0) {
            [XSTool showProgressHUDWithView:self.view];
            [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CoinTypes" params:nil] subscribeNext:^(resultObject *object) {
                [XSTool hideProgressHUDWithView:self.view];
                if (object.status) {
                  self.coinTypeArray = [NSMutableArray arrayWithArray:object.data[@"data"]];
                //展示选择钱币类型
                    PickerViewSheet *pickerView = [PickerViewSheet pickerView];
                    pickerView.BIT_List = self.coinTypeArray;
                    pickerView.pickerType = ShowPickerTypeBITList;
                    [pickerView setCompletionHandler:^(NSString *bit) {
                        @weakify(self);
                        self_weak_.coinTypeStr = bit;
                        [self_weak_.tableView reloadData];
                    }];
                    [pickerView show];
                }else{
                    [XSTool showToastWithView:self.view Text:object.info];
                }
            } error:^(NSError * _Nullable error) {
                 [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }completed:^{
            }];
        }else{
               //展示选择钱币类型
            PickerViewSheet *pickerView = [PickerViewSheet pickerView];
            pickerView.BIT_List = self.coinTypeArray;
            pickerView.pickerType = ShowPickerTypeBITList;
            [pickerView setCompletionHandler:^(NSString *bit) {
                @weakify(self);
                self_weak_.coinTypeStr = bit;
                [self_weak_.tableView reloadData];
            }];
            [pickerView show];
        }
    }else if (indexPath.row==1){
        ChouseCountryController *vc=[ChouseCountryController new];
        [vc setChouseCountry:^(CountryModel *mode) {
            self.countryModel=mode;
            [self.tableView reloadData];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row==3){
        if (self.payTypeArray.count==0) {
            //付款方式
            [XSTool showProgressHUDWithView:self.view];
            [[XSHTTPManager rac_POSTURL:@"cwallet/casset/GetPayTypes" params:nil] subscribeNext:^(resultObject *object) {
                [XSTool hideProgressHUDWithView:self.view];
                if (object.status) {
                    self.payTypeArray =[NSMutableArray arrayWithArray:[WalletPaytypeModel mj_objectArrayWithKeyValuesArray:object.data[@"data"]]];
 
                    //展示选择钱币类型
                    PickerViewSheet *pickerView = [PickerViewSheet pickerView];
                    pickerView.isSelectedMore = YES;
                    pickerView.BIT_List = self.payTypeArray;
                    pickerView.pickerType = ShowPickerTypePayTypeList;
                    [pickerView setCompletionHandler:^(NSArray *arr) {
                        @weakify(self);
                        NSMutableArray *showTextArr = [NSMutableArray array];
                        NSMutableArray *showCodeArr = [NSMutableArray array];
                        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            WalletPaytypeModel *model = arr[idx];
                            [showTextArr addObject:model.code_desc];
                            [showCodeArr addObject:model.code];
                        }];
                        self.payTypeModel.code_desc = [showTextArr componentsJoinedByString:@"/"];
                        self.payTypeModel.code = [showCodeArr componentsJoinedByString:@","];
                        
                        [self_weak_.tableView reloadData];
                    }];
                    [pickerView show];
                }else{
                    [XSTool showToastWithView:self.view Text:object.info];
                }
            } error:^(NSError * _Nullable error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }completed:^{
            }];
        }else{
 
            PickerViewSheet *pickerView = [PickerViewSheet pickerView];
            pickerView.isSelectedMore = YES;
            pickerView.BIT_List = self.payTypeArray;
            pickerView.pickerType = ShowPickerTypePayTypeList;
            [pickerView setCompletionHandler:^(NSArray *arr) {
                @weakify(self);
                NSMutableArray *showTextArr = [NSMutableArray array];
                NSMutableArray *showCodeArr = [NSMutableArray array];
                [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    WalletPaytypeModel *model = arr[idx];
                    [showTextArr addObject:model.code_desc];
                    [showCodeArr addObject:model.code];
                }];
                
                self.payTypeModel.code = [showCodeArr componentsJoinedByString:@","];
                self.payTypeModel.code_desc = [showTextArr componentsJoinedByString:@","];
                
                [self_weak_.tableView reloadData];
            }];
            [pickerView show];
        }
    }else if (indexPath.row==4){
        if (self.moneyTypeArray.count==0) {
            //货币类型
            [XSTool showProgressHUDWithView:self.view];
            [[XSHTTPManager rac_POSTURL:@"cwallet/casset/GetMoneyTypes" params:nil] subscribeNext:^(resultObject *object) {
                [XSTool hideProgressHUDWithView:self.view];
                if (object.status) {
                    self.moneyTypeArray =[NSMutableArray arrayWithArray:[SearchPayTypeModel mj_objectArrayWithKeyValuesArray:object.data[@"data"]]];
                    NSMutableArray *tempArray=[NSMutableArray array];
                    for (int i=0; i<self.moneyTypeArray.count; i++) {
                        SearchPayTypeModel *model=self.moneyTypeArray[i];
                        [tempArray addObject:model.code_desc];
                    }
                    //展示选择钱币类型
                    PickerViewSheet *pickerView = [PickerViewSheet pickerView];
                    pickerView.BIT_List = tempArray;
                    pickerView.pickerType = ShowPickerTypeBITList;
                    [pickerView setCompletionHandler:^(NSString *bit) {
                        @weakify(self);
                        for (SearchPayTypeModel *model1 in self.moneyTypeArray) {
                            if ([model1.code_desc isEqualToString:bit]) {
                                self_weak_.moneyTypeModel = model1;
                                break;
                            }
                        }
                        [self_weak_.tableView reloadData];
                    }];
                    [pickerView show];
                }else{
                    [XSTool showToastWithView:self.view Text:object.info];
                }
            } error:^(NSError * _Nullable error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }completed:^{
            }];
        }else{
            NSMutableArray *tempArray=[NSMutableArray array];
            for (int i=0; i<self.moneyTypeArray.count; i++) {
                SearchPayTypeModel *model=self.moneyTypeArray[i];
                [tempArray addObject:model.code_desc];
            }
            //展示选择钱币类型
            PickerViewSheet *pickerView = [PickerViewSheet pickerView];
            pickerView.BIT_List = tempArray;
            pickerView.pickerType = ShowPickerTypeBITList;
            [pickerView setCompletionHandler:^(NSString *bit) {
                @weakify(self);
                for (SearchPayTypeModel *model1 in self.moneyTypeArray) {
                    if ([model1.code_desc isEqualToString:bit]) {
                        self_weak_.moneyTypeModel = model1;
                        break;
                    }
                }
                [self_weak_.tableView reloadData];
            }];
            [pickerView show];
        }
    }
 
}
@end
