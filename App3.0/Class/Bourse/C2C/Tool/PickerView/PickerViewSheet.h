//
//  PickerViewSheet.h
//  BIT
//
//  Created by Sunny on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"
typedef enum : NSUInteger {
    ShowPickerTypeBITList,
    ShowPickerTypePayTypeList, // 支付方式选择
    
} ShowPickerType;

@interface PickerViewSheet : UIView

//@property (nonatomic, strong) HYZ_China *china;
/*地址选择器的列数, 默认为3
 *如需修改, 择传入列数
 */


@property(nonatomic, assign) NSInteger pickerRow;

/**
 币列表
 */
@property (nonatomic,strong) NSArray *BIT_List;

@property (nonatomic,assign) ShowPickerType pickerType;

@property (nonatomic,assign) BOOL isSelectedMore; // 支付方式选择


@property (nonatomic,assign) BOOL isDefaultRow;


@property (nonatomic, copy) void(^completionHandler)(id bit);

+ (instancetype)pickerView;

- (void)show;

@end



