//
//  PickerViewSheet.m
//  BIT
//
//  Created by Sunny on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "PickerViewSheet.h"
#import "TestCell.h"
@interface PickerViewSheet()<UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (strong, nonatomic) UITableView       *tableView;
@end

@implementation PickerViewSheet
{
    NSInteger defaultSelectedRow;
    UIView *titleView;
    CGFloat height;
    NSMutableArray *selectedPayType;
}

+ (instancetype)pickerView {
    PickerViewSheet *pickerView = [[PickerViewSheet alloc] initWithFrame:[UIScreen mainScreen].bounds];
    pickerView.alpha = 0;
    pickerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    return pickerView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        selectedPayType = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    defaultSelectedRow = 0;
 
    height = 236.0;
    
    CGFloat titleViewHeight = 40;
    CGFloat buttonWidth = 70;
    CGFloat buttonHeigth = 40;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, mainHeight, mainWidth, height)];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    self.contentView = contentView;
    
    titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, titleViewHeight)];
    titleView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:titleView];
    
    //    UIFont *font = [UIFont systemFontOfSize:17.0f];
    UIColor *color = [UIColor hexFloatColor:@"999999"];
    CGFloat buttonOffset = 10.0f;
 
    CGRect leftBtnFrame = CGRectMake(buttonOffset, 0, buttonWidth, buttonHeigth);
    UIButton *leftButton =  [BaseTool buttonWithTitle:Localized(@"cancel_btn") titleColor:[UIColor hexFloatColor:@"999999"] font:[UIFont systemFontOfSize:17] superView:titleView];
    leftButton.frame = leftBtnFrame;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    leftButton.backgroundColor = [UIColor yellowColor];
    [titleView addSubview:leftButton];
    [leftButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
 
    
    CGRect rightBtnFrame = CGRectMake(self.frame.size.width - buttonWidth - buttonOffset, 0, buttonWidth, buttonHeigth);
    UIButton *rightButton =  [BaseTool buttonWithTitle:Localized(@"material_dialog_positive_text") titleColor:mainColor font:[UIFont systemFontOfSize:17] superView:titleView];
    rightButton.frame = rightBtnFrame;
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [rightButton addTarget:self action:@selector(doneButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//    tap.delegate=self;//这句不要漏掉
//    [self addGestureRecognizer:tap];
    
//    UITapGestureRecognizer *emptyTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emptyTap)];
//    [self.contentView addGestureRecognizer:emptyTap];

}

- (void)setPickerType:(ShowPickerType)pickerType {
    _pickerType = pickerType;
    
    if (_pickerType == ShowPickerTypeBITList) {
        UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, titleView.frame.size.height, mainWidth, height)];
        pickerView.backgroundColor = [UIColor whiteColor];
        pickerView.delegate = self;
        pickerView.dataSource = self;
        [self.contentView addSubview:pickerView];
        self.pickerView = pickerView;

    }else if (_pickerType == ShowPickerTypePayTypeList){
        
        
        self.tableView.frame = CGRectMake(0, titleView.frame.size.height, mainWidth, height);
        [self.contentView addSubview:self.tableView];
        
        [self.tableView setEditing:YES animated:YES];
        
        
//        [self showEitingView:YES];
//        self.contentView.layer.masksToBounds = YES;
        

//        PickerTableView *view = [[PickerTableView alloc] initWithFrame:CGRectMake(0, titleView.frame.size.height, mainWidth, height)];
//        view.dataSource = self.BIT_List;
        
        
    }
}

- (void)cancelButtonClick {
    [self dismiss];
}

- (void)doneButtonClick {
    [self dismiss];
    
    // 给外界回调
    if (self.completionHandler) {
        if (self.BIT_List.count !=0) {
             if (self.pickerType == ShowPickerTypeBITList) {
                NSString *bit = self.BIT_List[defaultSelectedRow];
                self.completionHandler(bit);
            }else if (self.pickerType == ShowPickerTypePayTypeList){
                
                [[self.tableView indexPathsForSelectedRows] enumerateObjectsUsingBlock:^(NSIndexPath * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                     NSInteger row = obj.row;
                    WalletPaytypeModel *model = self.BIT_List[row];
                    [selectedPayType addObject:model];
                 }];
                
                 self.completionHandler(selectedPayType);
            }else{
                self.completionHandler(@"");
            }
        }
    }
}

- (void)tap {
    
//    [self dismiss];
}

- (void)emptyTap {
    // 不做消失处理
}

- (void)show {
    
    self.alpha = 1.0;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    [UIView animateWithDuration:0.25 animations:^{
        
        CGRect frame = self.contentView.frame;
        frame.origin.y -= frame.size.height;
        self.contentView.frame = frame;
        
    } completion:^(BOOL finished) {
        //        if (self.pickerView && self.china.chooseProvince) {
        //            // 当有值时, 滚动地址到当前行
        //            [self refresh];
        //        }
        
        [self refresh];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        
        CGRect frame = self.contentView.frame;
        frame.origin.y += frame.size.height;
        self.contentView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0;
        [self removeFromSuperview];
        
    }];
}



#pragma mark - <UIPickerViewDelegate, UIPickerViewDataSource>

// 列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (self.pickerRow) {
        return self.pickerRow;
    }
    return 1;
}

// 行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.BIT_List.count;

}

// 显示内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
 
    
    if (self.pickerType == ShowPickerTypeBITList) {
        return self.BIT_List[row];
    }else if (self.pickerType == ShowPickerTypePayTypeList){
        WalletPaytypeModel *model = self.BIT_List[row];
        return model.code_desc;
    }else{
        return self.BIT_List[row];
    }
    
 
}

// 显示内容控件
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    //设置分割线的颜色
    for (UIView *singleLine in pickerView.subviews) {
        if (singleLine.frame.size.height < 1) {
            //            singleLine.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
            singleLine.backgroundColor = [UIColor colorWithRed:214 / 255.0 green:214 / 255.0 blue:214 / 255.0 alpha:1.0];
        }
    }
    
    UILabel* pickerLabel = (UILabel *)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // pickerLabel.minimumScaleFactor = 8.0;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        // [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14.0f];
        pickerLabel.textColor = [UIColor blackColor];
        //        pickerLabel.numberOfLines = 0;
    }
    
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

// 每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}

// 选中行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0: {
            defaultSelectedRow = row;
        }
            break;
 
    }
}

- (void)refresh {
    if (self.pickerView) {
//        NSInteger provinceRow = [self.china.provinceList indexOfObject:self.china.chooseProvince];
//        if (provinceRow == NSNotFound) {
//            provinceRow = 0;
//        }
//         [self.pickerView selectRow:provinceRow inComponent:0 animated:YES];
 
    }
}


- (void)showEitingView:(BOOL)isShow{
    
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
}

#pragma mark -- UITabelViewDelegate And DataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.BIT_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    WalletPaytypeModel *model = self.BIT_List[indexPath.row];
    cell.textLabel.text = model.code_desc;
     return cell;
    
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {//判断如果点击的是tableView的cell，就把手势给关闭了
        return NO;//关闭手势
    }//否则手势存在
    return YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isSelectedMore) {
         if (self.completionHandler) {
             WalletPaytypeModel *model = self.BIT_List[indexPath.row];
            [selectedPayType addObject:model];
            self.completionHandler(selectedPayType);
            [self dismiss];
        }
         return;
    }
    if (tableView.isEditing) {
        return;
    }
 
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.dataSource      = self;
        _tableView.delegate        = self;
         _tableView.tableFooterView = [[UIView alloc] init];
        
        [_tableView registerClass:[TestCell class] forCellReuseIdentifier:@"Cell"];
    }
    return _tableView;
}
@end
