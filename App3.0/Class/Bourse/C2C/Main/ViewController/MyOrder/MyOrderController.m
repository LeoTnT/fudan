//
//  MyOrderController.m
//  BIT
//
//  Created by apple on 2018/3/20.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyOrderController.h"
#import "BaseOrderViewController.h"
#import "HelpCenterVC.h"

@interface MyOrderController () <SGPageTitleViewDelegate, SGPageContentViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;


@end

@implementation MyOrderController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
[self.navigationController setNavigationBarHidden:NO animated:YES];
//    [self.navigationController.navigationBar lt_setBackgroundColor:mainColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.navi_title = Localized(@"personal_my_orders");
    
    [self setupPageView];
    
    @weakify(self);
//    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"帮助") action:^{
//        @strongify(self);
//        HelpCenterVC *vc = [HelpCenterVC new];
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:nil action:^{
        @strongify(self);
        [BaseTool navigationControllersIsContainViewController:@"MyOrderInforController" viewController:^(BOOL isContainVC, UIViewController *getViewController, NSString *containName) {
            if (isContainVC) {
               UIViewController *vc = self.navigationController.viewControllers[1];
                [self.navigationController popToViewController:vc animated:YES];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }];
    self.view.backgroundColor = mainColor;
}

- (void)setupPageView {
 
    NSArray *titleArr = @[Localized(@"alls"), Localized(Localized(@"no_pay")), Localized(@"have_pay"), Localized(@"appealling"), Localized(@"has_been_cancelled"), Localized(@"have_finish")];
    NSArray *modelArr = @[@{@"status":@"9",
                             },
                          @{@"status":@"0",
                             @"is_pay":@"0"
                            },
                          @{@"status":@"0",
                             @"is_pay":@"1"
                            },
                          @{@"status":@"3",
                             },
                          @{@"status":@"2",
                             },
                          @{@"status":@"1",
                             }];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = [UIColor hexFloatColor:@"999999"];
    configure.titleSelectedColor = [UIColor hexFloatColor:@"232426"];
    configure.indicatorColor = [UIColor hexFloatColor:@"5B85E3"];
//    configure.indicatorAdditionalWidth = 100; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
//    CGFloat naviHeight = kDevice_Is_iPhoneX ? 88:64;
    CGFloat naviHeight = 0;
    /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, naviHeight, self.view.frame.size.width, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];

    NSMutableArray *chia = [NSMutableArray array];
    [titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BaseOrderViewController *fourVC = [[BaseOrderViewController alloc] init];
        NSDictionary *dic = modelArr[idx];
        RequestOrderList *model = [RequestOrderList mj_objectWithKeyValues:dic];
        model.limit = 10;
        model.page =1;
        fourVC.requestParams = model;
        [chia addObject:fourVC];
    }];
    NSArray *childArr = chia;
      CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame) -  self.tabBarController.tabBar.frame.size.height;;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

 

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self refreshOrderList];
    
 }
- (void)refreshOrderList
{
    if (self.index) {
        [self pageTitleView:self.pageTitleView selectedIndex:self.index];
    }
    
}



- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}


@end
