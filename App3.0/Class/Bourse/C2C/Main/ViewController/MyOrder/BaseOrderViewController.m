//
//  BaseOrderViewController.m
//  BIT
//
//  Created by apple on 2018/3/20.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseOrderViewController.h"
#import "MyOrderCell.h"
#import "MyOrderModel.h"
#import "MyOrderInforController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
@interface BaseOrderViewController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation BaseOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRefreshData) name:MyC2COrderNotification object:nil];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MyOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
//    self.dataSource = [NSMutableArray array];
    [self setupRefesh];
    [self loadRefreshData];
}


- (void)loadRefreshData {
    self.shouldShowEmptyView = YES;
    NSLog(@" page =%@",self.requestParams.mj_keyValues);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     self.requestParams.page = self.page;
      [[XSHTTPManager rac_POSTURL:@"cuser/corder/MyOrder" params:self.requestParams.mj_keyValues] subscribeNext:^(resultObject *objec) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          if (self.page <= 1) [self.dataSource removeAllObjects];
          if (objec.status) {
               NSArray *arr = objec.data[@"data"];
              NSArray *modelList = [MyOrderModel mj_objectArrayWithKeyValuesArray:arr];
              [self.dataSource addObjectsFromArray:modelList];
          }else{
              [MBProgressHUD showMessage:objec.info view:self.view];
          }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
        
    }completed:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRefresh];
             [self.tableView reloadData];
        });
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CGFloat df = ((38 * mainWidth)/375 )/2;
    cell.headImage.layer.cornerRadius = df;
    cell.headImage.layer.masksToBounds = YES;
    cell.model = self.dataSource[indexPath.section];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MyOrderModel *model = self.dataSource[indexPath.section];
    MyOrderInforController *vc = [MyOrderInforController new];
    vc.order_num = model.order_num;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
     return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IS_IPHONE_5) {
        return GET_HEIGT(100);
    }
    return GET_HEIGT(91);
    
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"暂无我的广告数据");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

@implementation RequestOrderList

- (instancetype)init {
    if (self = [super init]) {
        self.limit = 10;
    }
    return self;
}

@end
