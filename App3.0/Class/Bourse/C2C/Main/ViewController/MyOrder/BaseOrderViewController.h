//
//  BaseOrderViewController.h
//  BIT
//
//  Created by apple on 2018/3/20.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseOrderViewController.h"


@interface RequestOrderList : NSObject

@property (nonatomic ,copy)NSString *status;
@property (nonatomic ,copy)NSString *is_pay;
@property (nonatomic ,assign)NSInteger page;
@property (nonatomic ,assign)NSInteger limit;
@end

@interface BaseOrderViewController : XSBaseTablViewController

@property (nonatomic ,strong)RequestOrderList *requestParams;
@end
