//
//  MyOrderController.h
//  BIT
//
//  Created by apple on 2018/3/20.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"

@interface MyOrderController : BaseViewController

@property (nonatomic, strong) SGPageContentView *pageContentView;

@property (nonatomic,assign) NSInteger index;

@end
