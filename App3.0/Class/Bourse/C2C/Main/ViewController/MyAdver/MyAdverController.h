//
//  MyAdverController.h
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"

@interface RequestMyAdver :NSObject

@property (nonatomic ,copy)NSString *status;
@property (nonatomic ,assign)NSInteger page;
@property (nonatomic ,assign)NSInteger limit;
@property (nonatomic ,copy) NSString *coin_type;
@end

@interface MyAdverController : XSBaseTablViewController

@property (nonatomic ,strong)RequestMyAdver *requestModel;

@end
