//
//  MyAdverController.m
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyAdverController.h"
#import "MyAdverCell.h"
#import "MyAdverModel.h"
#import "MyOrderInforController.h"
#import "BaseMyAdverController.h"
#import "PchAderViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface MyAdverController ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;

@end

@implementation MyAdverController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)myADNotificationAction {
    [self loadRefreshData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.shouldShowEmptyView = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myADNotificationAction) name:MyADNotification object:nil];
    
    self.tableView.frame = self.view.bounds;
    [self setWhiteLeftBackBtn];
    [self.tableView registerNib:[UINib nibWithNibName:@"MyAdverCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    self.dataSource = [NSMutableArray array];
    [self setupRefesh];
    [self loadRefreshData];
    
    BaseMyAdverController *vc = self.navigationController.viewControllers.lastObject;
    [[RACObserve(vc.naviTitle, text) skip:1] subscribeNext:^(id  _Nullable x) {
        NSString *sss = Localized(@"my_advert");
        self.requestModel.coin_type = [x substringFromIndex:sss.length];
        self.requestModel.page = 1;
        self.requestModel.limit = 10;
        [self loadRefreshData];
    }];
}

- (void)loadRefreshData {
    self.shouldShowEmptyView = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.requestModel.page = self.page;
    NSLog(@" page =%@",self.requestModel.mj_keyValues);
    [[XSHTTPManager rac_POSTURL:@"cuser/cad/AdList" params:self.requestModel.mj_keyValues] subscribeNext:^(resultObject *objec) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.page == 1) [self.dataSource removeAllObjects];
        if (objec.status) {
            NSArray *arr = objec.data[@"data"];
            NSArray *modelList = [MyAdverModel mj_objectArrayWithKeyValuesArray:arr];
            [self.dataSource addObjectsFromArray:modelList];
        }else{
            [MBProgressHUD showMessage:objec.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self endRefresh];
        });
    }];
}

- (void) dropOffWithID:(MyAdverModel *)adModel {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString * requestUrl;
    if (adModel.status == 1) {
        requestUrl = @"cuser/cad/Cancel";
    } else if (adModel.status == 3) {
        requestUrl = @"cuser/cad/UpAd";
    }
    
    [[XSHTTPManager rac_POSTURL:requestUrl params:@{@"id":adModel.ID}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            if (adModel.status == 1) {
                [MBProgressHUD showMessage:@"下架成功" view:self.view];
            } else if (adModel.status == 3) {
                [MBProgressHUD showMessage:@"重新上架成功" view:self.view];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:MyADNotification object:nil];
            [self loadRefreshData];
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
    }];
}



- (void)checkEditWithID:(NSString *)ID coinStr:(NSString *)coinStr{
    
    //是否可编辑
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/cad/EditCheck" params:@{@"id":ID}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            PchAderViewController *vc = [PchAderViewController new];
            vc.DetailID = ID;
            vc.coinNameString = coinStr;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyAdverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.dataSource[indexPath.row];
    @weakify(self);
    [cell setMyDropOff:^(MyAdverModel *model) {
        @strongify(self);
        [self dropOffWithID:model];
    }];
    
    [cell setMyEditAction:^(MyAdverModel *model) {
        @strongify(self);
        [self  checkEditWithID:model.ID coinStr:model.coin_type];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IS_IPHONE_5) {
        return GET_HEIGT(186);
    }
    return GET_HEIGT(176);
}

#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.shouldShowEmptyView;
}

//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_logo"];
}
//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"暂无我的广告数据");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end


@implementation RequestMyAdver

@end
