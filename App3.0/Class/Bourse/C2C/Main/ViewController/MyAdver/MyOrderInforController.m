//
//  MyOrderInforController.m
//  BIT
//
//  Created by apple on 2018/3/24.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyOrderInforController.h"
#import "MyOrderInforView.h"
#import "MyOrderModel.h"
#import "MyOrderTipsCell.h"
#import "OrderAlertView.h"
#import "MyOrderController.h"
#import "HelpCenterVC.h"
#import "RSAEncryptor.h"
#import "JYSPayTypeCell.h"

@interface MyOrderInforController ()<MyOrderInforViewDelegate>
@property (strong, nonatomic) MyOrderModel *myOrderModel;
@property (strong, nonatomic) NSMutableArray *payWaysArray;
@property (strong, nonatomic) MyOrderInforView *inforView;

/**  */
@property (nonatomic, strong) UIView *bottomView1;
@property (nonatomic, strong) UIView *bottomView2;


@end

@implementation MyOrderInforController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.navi_title = Localized(@"购买订单");
    
    
    // 后台唤起监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestOrderData)
                                                 name:KNOTIFICATION_TIMECHANGE
                                               object:nil];
    
    [self setWhiteLeftBackBtn];
    @weakify(self);
//    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"帮助") action:^{
//        @strongify(self);
//        HelpCenterVC *vc = [HelpCenterVC new];
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitleColor:JYSMainTextColor titleFont:[UIFont systemFontOfSize:15] target:self action:@selector(rightBarButtonItemClicked) titleString:Localized(@"help")];
//
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        NSArray *arr = self.navigationController.viewControllers;
        if (arr.count >2) {
            [self.navigationController popToViewController:arr[1] animated:YES];
        }else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }];
    [self.navRightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    CGFloat tabHeight =  self.navi_Height + tabBarHeight;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(mainHeight - tabHeight);
    }];
    self.tableView.backgroundColor = RGB(241, 241, 241);
    [self.tableView registerNib:[UINib nibWithNibName:@"MyOrderTipsCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    [self requestOrderData];
    [self setHeaderRefresh];
}
- (void) loadRefreshData {
    [self requestOrderData];

}
- (void)requestOrderData {
    if (isEmptyString(self.order_num)) {
        [MBProgressHUD showMessage:@"订单有误" view:self.view];
        return;
    }
   
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/corder/Detail" params:@{@"order_num":self.order_num}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self endRefresh];

        if (object.status) {
            [self.payWaysArray removeAllObjects];
            self.myOrderModel = [MyOrderModel mj_objectWithKeyValues:object.data];
            
            if (self.myOrderModel.trade_user_info.pay_info.wx.code == 1) {
                PayInfoItem *item = self.myOrderModel.trade_user_info.pay_info.wx;
                [self.payWaysArray addObject:item];
            }
            if (self.myOrderModel.trade_user_info.pay_info.yhk.code == 1) {
                PayInfoItem *item = self.myOrderModel.trade_user_info.pay_info.yhk;
                [self.payWaysArray addObject:item];
            }
            if (self.myOrderModel.trade_user_info.pay_info.zfb.code == 1) {
                PayInfoItem *item = self.myOrderModel.trade_user_info.pay_info.zfb;
                [self.payWaysArray addObject:item];
            }

            //买家倒计时,自动结束取消订单
            if ([self.myOrderModel.tpl isEqualToString:@"detail_buy"]) {
                
            if (self.myOrderModel.pay_time_term.intValue >0 && self.myOrderModel.flush_time.intValue == 0&&self.myOrderModel.status == 0&&self.myOrderModel.is_pay == 0)
            {
                [self cancelRequest];
            }
            }
            
            [self setNeedRefreshView];
        }else{
            
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        [self.tableView reloadData];

    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
        [self endRefresh];

    }];
}

- (void)rightBarButtonItemClicked {
    HelpCenterVC *vc = [HelpCenterVC new];
    vc.orderNum = self.myOrderModel.order_num;
    vc.isAppeal = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)payWaysArray {
    if (!_payWaysArray) {
        _payWaysArray = [NSMutableArray array];
    }
    return _payWaysArray;
}

- (void) setContentView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, GET_HEIGT(225))];
    self.inforView = [MyOrderInforView loadMyOrderInforView];
    [self.inforView.evaBtn addTarget:self action:@selector(evaAction:) forControlEvents:UIControlEventTouchUpInside];
    self.inforView.orderModel = self.myOrderModel;

    self.inforView.frame = CGRectMake(0, 0, mainWidth, GET_HEIGT(225));
    [view addSubview:self.inforView];
    self.inforView.delegate = self;
    view.backgroundColor = [UIColor whiteColor];

    UIView *vvv = [BaseTool viewWithColor:RGB(241, 241, 241)];
    [self.inforView addSubview:vvv];

    
    self.tableView.tableHeaderView = view;
    [vvv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.inforView);
        make.height.mas_equalTo(10);
    }];
    
}


- (void) sureReceiveMoney:(NSDictionary *)dic{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/corder/Finish" params:dic] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            
            [MBProgressHUD showMessage:@"放行成功" view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


// 发币
- (void) sendCoin {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"请输入资金密码" preferredStyle:UIAlertControllerStyleAlert];
    //添加按钮
    __weak typeof(alert) weakAlert = alert;
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([weakAlert.textFields.lastObject text].length==0||[[weakAlert.textFields.lastObject text] isEqualToString:@""]) {
            [XSTool showToastWithView:self.view Text:@"请填写资金密码！"];
        } else {
            
            NSString *encryptOldPassWord = [NSString stringWithFormat:@"%@",[RSAEncryptor encryptString:[weakAlert.textFields.lastObject text]]];
            
            NSDictionary *dic = @{@"id":self.myOrderModel.ID,@"paypwd":encryptOldPassWord};
            [self sureReceiveMoney:dic];
        }
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // 添加文本框
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.secureTextEntry = YES;
        textField.textColor = [UIColor blackColor];
    }];
    // 弹出对话框
    [self presentViewController:alert animated:YES completion:nil];
}




- (void) evaAction:(UIButton *)sender {
    
    if (isEmptyString(self.myOrderModel.ID)) {
        [MBProgressHUD showMessage:@"订单有误" view:self.view];
        return;
    }
    
    NSInteger status = self.myOrderModel.status;
    if (status == 1) {
        if (self.myOrderModel.is_evaluate == 0) {
            [self pingjia];
        }
    }else if (status == 0){
        if ([self isSeller]) {
            NSInteger is_Pay = self.myOrderModel.is_pay;
            if ( is_Pay == 1) {
                [self sendCoin];
            }
            
        }
    }
    
}

- (void) pingjia {
    ShowOrderAler *alert = [ShowOrderAler new];
    alert.type = OrderAlertTypeEvaluateOrder;
    alert.model = @{@"id":self.myOrderModel.ID};
    [alert show];
    
    [[alert.sureButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
        [alert hidden];
        if (isEmptyString(alert.evaluate)) {
            [MBProgressHUD showMessage:@"您还没评价！" view:[UIApplication sharedApplication].keyWindow];
            return ;
        }
        NSDictionary *dic = @{@"id":self.myOrderModel.ID,
                              @"evaluate":alert.evaluate
                              };
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [[XSHTTPManager rac_POSTURL:@"cuser/ctrust/DoEvaluate" params:dic] subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if (object.status) {
                [MBProgressHUD showMessage:Localized(@"evaluate_success") view:self.view hideTime:1.5 doSomeThing:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else {
                [MBProgressHUD showMessage:object.info view:[UIApplication sharedApplication].keyWindow];
            }
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD showMessage:NetFailure view:[UIApplication sharedApplication].keyWindow];
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        }];
    }];
    
}

- (void)setNeedRefreshView {
    [self setContentView];
    if ((self.myOrderModel.status != 0 ) && self.myOrderModel.status != 2) {
        return;
    }
    
    NSString *tpl = self.myOrderModel.tpl;
    if ([tpl isEqualToString:@"detail_sell"]) {
        self.inforView.backgroundColor = [UIColor clearColor];
        [self.view layoutIfNeeded];
        self.bottomView1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame), mainWidth, 50)];
        self.bottomView1.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.bottomView1];
        
        UIButton *contact = [BaseTool buttonTitle:Localized(@"relative_user") image:@"Buy_infor_contact" superView:self.bottomView1];
        contact.titleLabel.font = [UIFont systemFontOfSize:16];
        [contact setTitleColor:mainColor forState:UIControlStateNormal];
        [contact setImagePosition:(LXMImagePositionLeft) spacing:10];
        [contact addTarget:self action:@selector(contactAction) forControlEvents:UIControlEventTouchUpInside];
        
        if (self.myOrderModel.is_pay == 1) {
            contact.frame = self.bottomView1.bounds;
            return;
        }
        [contact mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(self.bottomView1);
            make.width.mas_equalTo(mainWidth);
        }];
        
        
    }else if([tpl isEqualToString:@"detail_buy"]){
        
        self.inforView.backgroundColor = [UIColor clearColor];
        [self.view layoutIfNeeded];
        if (! self.bottomView2) {
            self.bottomView2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame), mainWidth, 50)];
            [self.view addSubview:self.bottomView2];
        }
       
        for (UIView *view in self.bottomView2.subviews) {
            [view removeFromSuperview];
        }
        UIButton *contact = [BaseTool buttonTitle:Localized(@"relative_user") image:@"Buy_infor_contact" superView:self.bottomView2];
        contact.titleLabel.font = [UIFont systemFontOfSize:16];
        [contact setTitleColor:mainColor forState:UIControlStateNormal];
        [contact setImagePosition:(LXMImagePositionLeft) spacing:10];
        [contact addTarget:self action:@selector(contactAction) forControlEvents:UIControlEventTouchUpInside];
        
        if (self.myOrderModel.is_pay == 1) {
            contact.frame = self.bottomView2.bounds;
            return;
        }
        [contact mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(self.bottomView2);
            make.width.mas_equalTo(mainWidth/3);
        }];
        
        UIView *bb = [BaseTool viewWithColor:[UIColor hexFloatColor:@"F95453"]];
        bb.alpha = .3;
        [self.bottomView2 addSubview:bb];
        [bb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.bottomView2);
            make.left.mas_equalTo(contact.mas_right);
            make.width.mas_equalTo(mainWidth/3);
        }];
        UIButton *cancel = [BaseTool buttonWithTitle:Localized(@"cancle_order") titleColor:[UIColor hexFloatColor:@"F95453"] font:[UIFont systemFontOfSize:16] superView:self.bottomView2];
        cancel.backgroundColor = [UIColor clearColor];
        [cancel addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [cancel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.bottomView2);
            make.left.mas_equalTo(contact.mas_right);
            make.width.mas_equalTo(mainWidth/3);
        }];
        
        UIButton *buy = [BaseTool buttonWithTitle:Localized(@"has_pay") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] superView:self.bottomView2];
        [buy setBackgroundColor:[UIColor hexFloatColor:@"F95453"]];
        
        [buy addTarget:self action:@selector(havePayAction) forControlEvents:UIControlEventTouchUpInside];
        [buy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.mas_equalTo(self.bottomView2);
            make.left.mas_equalTo(cancel.mas_right);
        }];
        
        if (self.myOrderModel.status == 0) {
            UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 60)];
            self.tableView.tableFooterView = footerView;
            UILabel *label = [BaseTool labelWithTitle:Localized(@"pay_tixing_four") textAlignment:NSTextAlignmentLeft font:[UIFont systemFontOfSize:12] titleColor:[UIColor hexFloatColor:@"F95453"]];
            label.numberOfLines = 0;
            footerView.backgroundColor = RGB(241, 241, 241);
            [footerView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(12, 12, 12, 12));
            }];
        } else if (self.myOrderModel.status == 2) {
            buy.hidden = YES;
            cancel.hidden = YES;
            [cancel setTitle:@"" forState:UIControlStateNormal];
            [contact mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
    
}

- (void)contactAction {
    
    TradeUserInfo *infor =self.myOrderModel.trade_user_info;
    NSString *uid = self.myOrderModel.talk_targert_userid;
    if (isEmptyString(uid)) {
        return;
    }
    XMChatController *chatVC = [XMChatController new];
    SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:uid title:infor.nickname avatarURLPath:infor.logo];
    chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
    [self.navigationController pushViewController:chatVC animated:YES];
    
}


// 是不是卖家
- (BOOL) isSeller {
    
    return [self.myOrderModel.tpl isEqualToString:@"detail_sell"] ? YES:NO;
}

// 倒计时结束刷新
- (void)refreshAction
{
    [self requestOrderData];

//    [self cancelRequest];
}

// 买家-取消订单
- (void)cancelAction {
    
    if ([self isSeller]) {
        if (isEmptyString(self.myOrderModel.ID)) {
            return;
        }
        [self cancelRequest];

    }else{
        
        ShowOrderAler *alert = [ShowOrderAler new];
        alert.type = OrderAlertTypeCancelOrder;
        if (isEmptyString(self.myOrderModel.ID)) {
            return;
        }
        alert.model = @{@"id":self.myOrderModel.ID};
        [[alert.sureButton rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
            if (alert.isGoOn) {
                [self cancelRequest];
                [alert hidden];
                
            }else{
                [MBProgressHUD showMessage:Localized(@"please_selecet_paychckbox") view:[UIApplication sharedApplication].keyWindow];
            }
        }];
        [alert show];
    }
    
}



- (void) cancelRequest {
    if (isEmptyString(self.myOrderModel.ID)) {
        return;
    }
    
    if ([self isSeller]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:MyC2COrderNotification object:nil];

    }else{
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[XSHTTPManager rac_POSTURL:@"cuser/corder/Cancel" params:@{@"id":self.myOrderModel.ID}] subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (object.status) {
//                MyOrderController *vc = [MyOrderController new];
//                vc.index = 4;
//                [self.navigationController pushViewController:vc animated:YES];
                [self requestOrderData];
                [[NSNotificationCenter defaultCenter] postNotificationName:MyC2COrderNotification object:nil];

            }else{
                [self.tableView reloadData];
                [MBProgressHUD showMessage:object.info view:[UIApplication sharedApplication].keyWindow];
                
            }
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        }];
    }
    
}

// 买家-确认我已付款
- (void)havePayAction {
    if (isEmptyString(self.myOrderModel.ID)) {
        return;
    }
    
    if ([self isSeller]) {
        
    }else{
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[XSHTTPManager rac_POSTURL:@"cuser/corder/Dopay" params:@{@"id":self.myOrderModel.ID}] subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (object.status) {
                [[NSNotificationCenter defaultCenter] postNotificationName:MyC2COrderNotification object:nil];
//                MyOrderInforController *vc = [MyOrderInforController new];
//                vc.order_num = self.order_num;
//                [self.navigationController pushViewController:vc animated:YES];
                [self requestOrderData];

            }else{
                [self.bottomView1 removeFromSuperview];
                self.bottomView1 = nil;
                [self.bottomView2 removeFromSuperview];
                self.bottomView2 = nil;
                self.tableView.tableFooterView = nil;
                
                [self requestOrderData];
                
                [MBProgressHUD showMessage:object.info view:[UIApplication sharedApplication].keyWindow];
            }
            
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        }];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.myOrderModel || self.myOrderModel.status > 0 || self.myOrderModel.is_pay == 1 || [self isSeller]) {
        return 0;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section==0?self.payWaysArray.count+1:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row < self.payWaysArray.count) {
            //            NSArray *image = @[@"order_bankcard",@"order_wechat",@"order_alipay"];
            PayInfoItem *item = self.payWaysArray[indexPath.row];
            JYSPayTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell1"];
            if (!cell) {
                cell =[[JYSPayTypeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
                 cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                 cell.textLabel.font = [UIFont systemFontOfSize:16];
                cell.detailTextLabel.textColor = [UIColor hexFloatColor:@"999999"];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
                
                UIView *line = [UIView new];
                line.backgroundColor = [UIColor hexFloatColor:@"dddddd"];
                [cell.contentView addSubview:line];
                [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(12);
                    make.right.bottom.mas_equalTo(cell.contentView);
                    make.height.mas_equalTo(1);
                }];
                
                UIButton *btn = [BaseTool buttonWithImage:nil selected:nil superView:cell.contentView];
                btn.userInteractionEnabled = NO;
                btn.tag = 1001;
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(cell.contentView);
                    make.right.mas_equalTo(-12);
                }];
            }
            NSString * defaultImageStr;
            if ([item.code_desc isEqualToString:Localized(@"wechat")]) {
                defaultImageStr = @"bingding_weixin";
            } else if ([item.code_desc isEqualToString:Localized(@"alipay")]) {
                defaultImageStr = @"bingding_alpay";
            } else {
                defaultImageStr = @"bingding_bankcard";
            }
            
            [cell.imageView getImageWithUrlStr:item.info.picture andDefaultImage:[UIImage imageNamed:defaultImageStr]];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",item.info.bankuser,item.info.bankcard];
            //            cell.imageView.image = [UIImage imageNamed:image[indexPath.row]];
            
            UIButton *btn = [cell.contentView viewWithTag:1001];
            if (btn) {
                if ([item.code_desc isEqualToString:Localized(@"wechat")]||[item.code_desc isEqualToString:Localized(@"alipay")]) {
                    [btn setImage:[UIImage imageNamed:@"order_qrcode"] forState:UIControlStateNormal];
                } else {
                    [btn setImage:[UIImage imageNamed:@"order_copy"] forState:UIControlStateNormal];
                }
            }
            
            return cell;
        } else {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell2"];
            if (!cell) {
                cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.textLabel.text = Localized(@"pay_note");
                cell.textLabel.textColor = [UIColor hexFloatColor:@"999999"];;
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                cell.detailTextLabel.textColor = mainColor;
                cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
                cell.backgroundColor = [UIColor hexFloatColor:@"F4FBFF"];
            }
            cell.detailTextLabel.text = self.myOrderModel.pay_code;
            return cell;
        }
        
    }
    MyOrderTipsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.tipsLabel.text = [NSString stringWithFormat:@"%@\n2%@\n3.%@%d%@，%@",Localized(@"pay_tixing_one"),Localized(@"pay_tixing_two"),Localized(@"买方当日连续"),[AppConfigManager ShardInstnce].appConfig.c2c_order_cancel_num,Localized(@"笔取消"),Localized(@"广告方当日取消率超过30%%会限制当天的买入功能。")];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row != self.payWaysArray.count) {
        PayInfoItem *item = self.payWaysArray[indexPath.row];
        if ([item.code_desc isEqualToString:Localized(@"wechat")]||[item.code_desc isEqualToString:Localized(@"alipay")]) {
            // 显示二维码
            if (!isEmptyString(item.info.picture)) {
                ShowOrderAler *aler = [ShowOrderAler new];
                aler.type = OrderAlertTypeShowQRCode;
                aler.model = @{@"image":item.info.codeImage};
            }else {
                [MBProgressHUD showMessage:@"二维码有误" view:self.view];
            }
            //            picture
        } else {
            if (isEmptyString(item.info.bankcard)) {
                Alert(Localized(@"data_error"));
                return;
            }
            UIPasteboard *paste = [UIPasteboard generalPasteboard];
            paste.string = item.info.bankcard;
            if ([item.code_desc isEqualToString:Localized(@"bank_card")]) {
                [MBProgressHUD showMessage:Localized(@"bank_num_has_copy") view:self.view];
            } else {
                UIPasteboard *paste = [UIPasteboard generalPasteboard];
                paste.string = item.info.bankcard;
                if ([item.code_desc isEqualToString:Localized(@"bank_card")]) {
                    [MBProgressHUD showMessage:Localized(@"bank_num_has_copy") view:self.view];
                } else {
                    [MBProgressHUD showMessage:@"支付宝账号已复制" view:self.view];
                }
                
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 3) {
            return 44;
        }
        return 60;
    }
    return 165;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 10;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}
@end
