//
//  BaseMyAdverController.m
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseMyAdverController.h"
#import "MyAdverController.h"
#import "ChildView.h"
@interface BaseMyAdverController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic ,strong)BaseBackView *popBackView;
@property (nonatomic,strong) UIButton *backView;
@property (nonatomic,strong) UIImageView *imageView;
@end

@implementation BaseMyAdverController
{
    CGFloat titithei;
    NSMutableArray *dataSource;
    BOOL isShow;
 
}
 
- (void)viewDidLoad {
    [super viewDidLoad];
     titithei = IS_iPhoneX ? 88:64;
//    self.navi_title = @"我的广告BTC";
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    dataSource = [NSMutableArray array];
//    UIButton *titleButton = [BaseTool buttonTitle:@"我的广告BTC" image:@"adver_jt" superView:nil];
    self.navigationItem.titleView.userInteractionEnabled = YES;
    _naviTitle = [BaseTool labelWithTitle:Localized(@"my_advert") textAlignment:0 font:[UIFont systemFontOfSize:18] titleColor:[UIColor blackColor]];
    UIView *bbbb = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    bbbb.userInteractionEnabled = YES;
    _backView = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backView setTitle:@"                                " forState:UIControlStateNormal];
    [bbbb addSubview:_backView];
    [_backView addSubview:_naviTitle];
    _backView.backgroundColor = [UIColor clearColor];
     _imageView = [BaseTool imageWithName:@"mall_down" superView:_backView];
     _imageView.transform = CGAffineTransformMakeRotation(M_PI);
     self.navigationItem.titleView = bbbb;
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(bbbb);
    }];
     [_naviTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bbbb);
     }];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bbbb);
        make.left.mas_equalTo(_naviTitle.mas_right).offset(2);
    }];
    
    UIButton *topButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [topButton setTitle:@"                                " forState:UIControlStateNormal];
    [_backView addSubview:topButton];
    [topButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_backView);
    }];
    
//

    @weakify(self);
    [topButton addTarget:self action:@selector(changeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
 
     [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.view.backgroundColor = mainColor;
    
    [self setupPageView];

}

- (void) changeButtonAction:(UIButton *)sender {
    isShow = !isShow;
    isShow ? [self showPopView]: [self removePopView];
    
    [UIView animateWithDuration:.25 animations:^{
        _imageView.transform = isShow ? CGAffineTransformMakeRotation(0):CGAffineTransformMakeRotation(M_PI);
    }];
}
 

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.popBackView removeFromSuperview];
    [self.tableView removeFromSuperview];
    self.popBackView = nil;
    self.tableView = nil;
}

- (void) removePopView {
 
    [UIView animateWithDuration:.25 animations:^{
        self.popBackView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, 0);
        self.tableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, 0);
    }completion:^(BOOL finished) {
         [self.popBackView removeFromSuperview];
        [self.tableView removeFromSuperview];
        self.popBackView = nil;
        self.tableView = nil;
    }];
    
}


- (void) showPopView {
    self.popBackView.frame = CGRectMake(0, titithei, mainWidth, 0);
    self.tableView.frame = CGRectMake(0, 0, mainWidth, 0);
    [UIView animateWithDuration:.25 animations:^{
        self.popBackView.frame = CGRectMake(0, titithei, mainWidth, mainHeight);
        self.tableView.frame = CGRectMake(0, 0, mainWidth, dataSource.count * 40 + 50);
    }];
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];

    
}

- (void) showAllAdverList {
    
}

- (void)setupPageView {
    
    NSArray *titleArr = @[Localized(@"have_in_hand"), Localized(@"has_shelf") ];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = [UIColor hexFloatColor:@"999999"];
    configure.titleSelectedColor = [UIColor hexFloatColor:@"232426"];
    configure.indicatorColor = [UIColor hexFloatColor:@"5B85E3"];
     /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0,0, self.view.frame.size.width, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];
    
    NSArray *arrList = @[@{@"status":@"1"},@{@"status":@"3"}];
    NSMutableArray *childArr = [NSMutableArray array];
    [titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    
//        UIViewController *oneVC = [UIViewController new];
        MyAdverController *oneVC = [[MyAdverController alloc] init];
        RequestMyAdver *model = [RequestMyAdver mj_objectWithKeyValues:arrList[idx]];
        model.limit = 10;
        oneVC.requestModel = model;
        [childArr addObject:oneVC];
    }];
 
    CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame);
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
    
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CoinTypes" params:nil] subscribeNext:^(resultObject *object) {
        if (object.status) {
            NSArray *arr = object.data[@"data"];
            [arr.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                
                [dataSource addObject:@{@"title":[NSString stringWithFormat:@"%@%@",Localized(@"my_advert"),x],
                                        @"type":x
                                        }];
                

                
            } completed:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            }];
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
}



- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (BaseBackView *)popBackView {
    if (!_popBackView) {
        _popBackView = [BaseBackView new];
        @weakify(self);
        _popBackView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        [_popBackView setTouchOutSide:^{
            isShow = YES;
            @strongify(self);
           [self changeButtonAction:self.backView];
        }];
        _popBackView.frame = CGRectMake(0, titithei, mainWidth, 0);
        [[UIApplication sharedApplication].keyWindow addSubview:_popBackView];
 
    }
    return _popBackView;
}

-(UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 40 * 10) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.frame = _popBackView.bounds;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = _popBackView.backgroundColor;
        [self.popBackView addSubview:_tableView];
     }
    return _tableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
     cell.textLabel.text = dataSource[indexPath.row][@"title"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    isShow = YES;
    [self changeButtonAction:self.backView];
    NSDictionary *dic = dataSource[indexPath.row];
    NSString *title = dic[@"title"];
    _naviTitle.text = title ;
    
}

@end


