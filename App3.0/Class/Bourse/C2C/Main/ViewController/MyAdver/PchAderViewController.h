//
//  PchAderViewController.h
//  BIT
//
//  Created by Sunny on 2018/3/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"
 
@interface PchAderViewController : XSBaseTablViewController

@property (nonatomic,copy) NSString *DetailID;
//type：广告类型，*必须传 ；值1是委托买入，值2是委托售出
@property (nonatomic,assign) NSInteger type;

/** 上个界面选中的币名 */
@property (nonatomic, copy) NSString * coinNameString;


@end


