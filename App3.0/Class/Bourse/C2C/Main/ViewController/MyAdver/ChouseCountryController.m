//
//  ChouseCountryController.m
//  BIT
//
//  Created by Sunny on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "ChouseCountryController.h"

@interface ChouseCountryController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray * tableData;

@property (nonatomic, strong) NSMutableArray * resultData;

@property (nonatomic, strong) NSArray * tableIndexData;

@property (nonatomic, strong) NSMutableArray * resultIndexData;

@property (nonatomic, assign) BOOL  searchActive;

@property (nonatomic,strong) UITableView *tableView;

@end

@implementation ChouseCountryController
{
    UISearchBar * mSearchBar ;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = main_BackColor;
        _tableView.sectionIndexColor = COLOR_666666;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
#ifdef __IPHONE_11_0
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
        }
#endif
        [self.view addSubview:_tableView];
 
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.00001)];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"addr_choose");
//    @weakify(self);
//    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
//        @strongify(self);
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
//    self.edgesForExtendedLayout = UIRectEdgeNone;
    mSearchBar = [[UISearchBar alloc] init];
    mSearchBar.delegate = self;
    mSearchBar.barTintColor = XSYCOLOR(0xf4f4f4);
    [mSearchBar setSearchFieldBackgroundImage:[UIImage yy_imageWithColor:main_BackColor size:CGSizeMake(mainWidth- 32, 36)] forState:UIControlStateNormal];
    UITextField*searchField = [mSearchBar valueForKey:@"_searchField"];
    searchField.layer.cornerRadius = 5;
    searchField.layer.masksToBounds = YES;
    mSearchBar.placeholder = Localized(@"search");
//    searchField.textColor = [UIColor whiteColor];
    [mSearchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    mSearchBar.frame = CGRectMake(0, 0, mainWidth, 55);
    [self.view addSubview:mSearchBar];
//
    self.tableView.frame = CGRectMake(0, CGRectGetMaxY(mSearchBar.frame), mainWidth, mainHeight - CGRectGetMaxY(mSearchBar.frame)-40);
    self.resultIndexData = [NSMutableArray array];
    self.tableData = [UserSingle sharedUserSingle].tableData;
    self.tableIndexData = [UserSingle sharedUserSingle].tableIndexData;
 
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}
//将汉字转为拼音 是否支持全拼可选
- (NSString *)transformToPinyin:(NSString *)aString isQuanPin:(BOOL)quanPin
{
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:aString];
    CFStringTransform((CFMutableStringRef)str, NULL, kCFStringTransformMandarinLatin,NO);
    
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str, NULL, kCFStringTransformStripDiacritics,NO);
    NSArray *pinyinArray = [str componentsSeparatedByString:@" "];
    NSMutableString *allString = [NSMutableString new];
    
    if (quanPin)
    {
        int count = 0;
        for (int  i = 0; i < pinyinArray.count; i++)
        {
            for(int i = 0; i < pinyinArray.count;i++)
            {
                if (i == count) {
                    [allString appendString:@"#"];
                    //区分第几个字母
                }
                [allString appendFormat:@"%@",pinyinArray[i]];
            }
            [allString appendString:@","];
            count ++;
        }
    }
    
    NSMutableString *initialStr = [NSMutableString new];
    //拼音首字母
    for (NSString *s in pinyinArray)
    {
        if (s.length > 0)
        {
            [initialStr appendString:[s substringToIndex:1]];
        }
    }
    [allString appendFormat:@"#%@",initialStr];
    [allString appendFormat:@",#%@",aString];
    return allString;
}



#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //加个多线程，否则数量量大的时候，有明显的卡顿现象
    //这里最好放在数据库里面再进行搜索，效率会更快一些
    if (searchText.length == 0)
    {
        _searchActive = NO;
        [self.tableView reloadData];
        return;
    }
    _searchActive = YES;
    _resultData = [NSMutableArray array];
    _resultIndexData = [NSMutableArray array];
    
    dispatch_queue_t globalQueue = dispatch_get_global_queue(0, 0);
    dispatch_async(globalQueue, ^{
        //遍历需要搜索的所有内容，其中self.dataArray为存放总数据的数组
        [self.tableData enumerateObjectsUsingBlock:^(NSMutableArray * obj, NSUInteger aIdx, BOOL * _Nonnull stop) {
            //刚进来 && 第一个数组不为空时 插入一个数组在第一个位置
            if (_resultData.count == 0 || [(NSArray *)[_resultData lastObject] count] != 0)
            {
                [_resultData addObject:[NSMutableArray array]];
            }
            
            [obj enumerateObjectsUsingBlock:^(CountryModel * model, NSUInteger bIdx, BOOL * _Nonnull stop) {
                
                NSString *tempStr = model.name;
                //----------->把所有的搜索结果转成成拼音
                NSString *pinyin = [self transformToPinyin:tempStr isQuanPin:NO];
                
                if ([pinyin rangeOfString:searchText options:NSCaseInsensitiveSearch].length > 0)
                {
                    //把搜索结果存放self.resultArray数组
                    [_resultData.lastObject addObject:model];
                    if (_resultIndexData == 0 || ![_resultIndexData.lastObject isEqualToString:_tableIndexData[aIdx]])
                    {
                        [_resultIndexData addObject:_tableIndexData[aIdx]];
                    }
                }
            }];
        }];
        //回到主线程
        if ([(NSArray *)(_resultData.lastObject) count] == 0)
        {
            [_resultData removeLastObject];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    });
}

#pragma mark - Scroll View Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _searchActive ? _resultData.count : _tableData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _searchActive ? [(NSArray *)_resultData[section] count] : [(NSArray *)_tableData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CountryModel * m = _searchActive ? _resultData[indexPath.section][indexPath.row] : _tableData[indexPath.section][indexPath.row];
    cell.textLabel.text = m.name;
    [cell lineLabel];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CountryModel * m = _searchActive ? _resultData[indexPath.section][indexPath.row] : _tableData[indexPath.section][indexPath.row];
 
    if(self.chouseCountry){
        self.chouseCountry(m);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _searchActive ? _resultIndexData : _tableIndexData;
}

//返回每个索引的内容
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    return _searchActive ? _resultIndexData[section] : _tableIndexData[section];
//}

//返回当用户触摸到某个索引标题时列表应该跳至的区域的索引。
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    //    [SVProgressHUD showImage:nil status:title];
    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}
#pragma mark - Table View Delegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section >= _searchActive ? _resultIndexData.count : _tableIndexData.count) {
        return nil;
    }
    UIView *backView = [BaseTool viewWithColor:main_BackColor];
    backView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 30);
    UILabel * tempLab = [UILabel new];
    tempLab.text = _searchActive ? _resultIndexData[section] : _tableIndexData[section];
    tempLab.backgroundColor = main_BackColor;
    [backView addSubview:tempLab];
    [tempLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backView);
        make.left.mas_equalTo(16);
    }];
    return backView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

@end
