//
//  PchAderViewController.m
//  BIT
//
//  Created by Sunny on 2018/3/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "PchAderViewController.h"
#import "PCHHeaderView.h"
#import "MyAdverModel.h"
#import "XSDatePickerView.h"
#import "ChouseCountryController.h"
#import "PickerViewSheet.h"
#import "SDRangeSliderView.h"
#import "MerchantApplicationController.h"
#import "BaseSlider.h"
#import "NewVertifyController.h"
#import "JYSRealNameVerificationViewController.h"
#import "BindingViewController.h"

@interface PchAderViewController ()<XSDatePickerDelegate,UITextViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (nonatomic,strong) PCHHeaderView *pchHeaderView;

@property (nonatomic,strong) UILabel *coin_type; // 币种
@property (nonatomic,strong) UILabel *BIT_address;// 所在地
@property (nonatomic,strong) UILabel *chouseBIT;//选择货币

/** 货币单位 */
@property (nonatomic, strong) UILabel *unit1Label;
/** 货币单位 */
@property (nonatomic, strong) UILabel *unit2Label;
/** 货币单位 */
@property (nonatomic, strong) UILabel *unit3Label;
/** 货币单位 */
//@property (nonatomic, strong) UILabel *unit4Label;

@property (nonatomic,strong) UITextField *tradingPrice;// 交易价格
@property (nonatomic,strong) UITextField *inputField;

//@property (nonatomic,strong) UITextField *price_MAX;
@property (nonatomic,strong) UITextField *minimumTextF;
@property (nonatomic,strong) UITextField *maximumTextF;
@property (nonatomic,strong) UITextField *pay_time_termTextF;

@property (nonatomic,strong) UILabel *chousePayType;// 付款方式
@property (nonatomic,strong) UITextView *comment;
@property (nonatomic,strong) UILabel *numberLabel;

@property (nonatomic,strong) SDRangeSliderView *slider;


@property (nonatomic,strong) UISwitch *adverStatus;
@property (nonatomic,strong) UISwitch *approveStatues;

@property (nonatomic,strong) NSMutableArray *allCoinTypeArray;//平台所有的币种类型
/** 所有货币CNY等 */
@property (nonatomic, strong) NSMutableArray * allCurrenciesArray;
/** 所有货币CNY等 */
@property (nonatomic, strong) NSMutableArray * allCurrencies;

@property (nonatomic,strong) NSMutableArray *payTypeDictionary;//已设置的付款方式


/**
 钱包各货币余额
 */
@property (nonatomic,strong) NSMutableArray *walletMoneyDictionary;

@property (nonatomic,strong)NewAdModel *adNewModel;

@property (nonatomic,strong)UIView *leftSlView;
@property (nonatomic,strong)UIView *rightSlView;

@property (nonatomic,strong) UILabel *referenceLabel;

/** 选中币种 */
@property (nonatomic, copy) NSString * selectedCoinName;

/** m */
@property (nonatomic, strong) MyAdCoinPayTimeModel * coinPayTimeModel;

@end

@implementation PchAderViewController
{
    NSArray *listArr;
    UIFont *font15;
    MyAdverModel *adverModel;
    NSString *commmmString;
    NSInteger approve_supply;// 认证状态
    double inputTextView;
    NSString *persent;
    NSMutableArray *BIT_infor;  // 市场参考价model
    
    NSString *scprice;// 市场参考价
}
-(PCHHeaderView *)pchHeaderView {
    if (!_pchHeaderView) {
        
        _pchHeaderView = [PCHHeaderView loadPCHHeaderView];
        _pchHeaderView.frame = CGRectMake(0, 0, mainWidth, 80);
        
    }
    return _pchHeaderView;
}

- (NSMutableArray *)payTypeDictionary {
    if (_payTypeDictionary == nil) {
        _payTypeDictionary = [[NSMutableArray alloc] init];
    }
    return _payTypeDictionary;
}

- (NSMutableArray *)allCurrenciesArray {
    if (_allCurrenciesArray == nil) {
        _allCurrenciesArray = [[NSMutableArray alloc] init];
    }
    return _allCurrenciesArray;
}

- (NSMutableArray *)allCurrencies {
    if (_allCurrencies == nil) {
        _allCurrencies = [[NSMutableArray alloc] init];
    }
    return _allCurrencies;
}

//
//-(void)sliderValueChanged:(UISlider *)slider
//{
//    NSLog(@"slider value%f",slider.value);
//}

- (void)setType:(NSInteger)type {
    _type = type;
    if (self.DetailID) {
        self.title = type == 2? Localized(@"编辑出售广告"):Localized(@"编辑购买广告");
    }else{
        self.title = type == 2? Localized(@"send_sale_advert"):Localized(@"send_buy_advert");
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    
    if (!isEmptyString(self.DetailID) && adverModel) {
        
        RAC(self.coin_type,text) = RACObserve(adverModel, coin_type);
        RAC(self.BIT_address,text)= RACObserve(adverModel, country_desc);
        RAC(self.tradingPrice,text) = [RACObserve(adverModel, price) map:^id _Nullable(id  _Nullable value) {
            return [value description];
        }];
        
        [self.tableView reloadData];
     }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [BaseTool hiddenInternet];
 
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//     [self getBusinessInformation];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.autoHideKeyboard = YES;
    
    self.adNewModel = [NewAdModel new];
    
    [[XSHTTPManager rac_POSTURL:@"casset/casset/Quotation" params:nil] subscribeNext:^(resultObject *object) {
        if (object.status) {
            BIT_infor= object.data[@"data"];
            [BIT_infor.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                NSDictionary *dic =x;
                NSString *name = dic[@"name"];
                if ([name isEqualToString:@"btc"]) {
                    scprice = dic[@"newPrice"];
                    NSString *ssss = [NSString stringWithFormat:@"%@%@)",Localized(@"溢价设置(市场参考价格"),isEmptyString(scprice)?@"0":scprice];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.referenceLabel.text = ssss;
                    });
                    
                }
            }];
        }
    }];
    
    persent = @"0";
    
    commmmString = Localized(@"advert_note");
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(self.view);
    }];
    
    
    //货币类型
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CoinTypes" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            self.allCoinTypeArray = [NSMutableArray arrayWithArray:object.data[@"data"]];
            if (self.allCoinTypeArray.count >0) {
                NSString *sysmbol = [NSString stringWithFormat:@"%@",self.allCoinTypeArray.firstObject];
                if (!isEmptyString(sysmbol)) {
                    self.coin_type.text = sysmbol;
                    self.adNewModel.coin_type = sysmbol;
                    [self requestCoinPayTime];
                    [self getPoundageWithSysmbol:sysmbol];
                }
                
            }
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
    
    [self requestAllCurrenciesData];
    
    //付款方式
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/Paytype" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"cwallet/cwallet/Paytype====%@",object.data);
        if (object.status) {
            [self.payTypeDictionary removeAllObjects];
            
            NSArray * dataArr = object.data[@"data"];
            for (int i = 0; i < dataArr.count; i++) {
                WalletPaytypeModel * model = [WalletPaytypeModel mj_objectWithKeyValues:dataArr[i]];
                if (model.info) {
                    [self.payTypeDictionary addObject:model];
                }
            }
            if (self.payTypeDictionary.count == 0) {
                [self showPayAlert];
            }
            
        } else {
            [MBProgressHUD showMessage:object.info view:self.view hideTime:1.5 doSomeThing:nil];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } completed:^{
        
    }];
    
//    self.pchHeaderView.threeLabel.text = [NSString stringWithFormat:@"钱包中至少需要有0.05%@，广告才会正常展示",self.coinNameString];
    
    //钱包余额
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/Mywallet" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"cwallet/cwallet/Mywallet:::::%@",object.data);
        if (object.status) {
            self.walletMoneyDictionary =  object.data[@"data"];
            __block NSString *money;
            [self.walletMoneyDictionary.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                NSDictionary *dic = x;
                NSString *name =dic[@"coin"];
                if ([[name uppercaseString] isEqualToString:[self.coinNameString uppercaseString]]) {
                    money = dic[@"remain"][@"number"];
                }
            }completed:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (money.doubleValue < 0.05) {
//                        NSString *showString = [NSString stringWithFormat:@"您的钱包余额已不足0.05%@，广告将无法正常展示。",self.coinNameString];
//                        [BaseTool showInternetInfor:showString taPAction:nil];
                    }else{
                        [BaseTool hiddenInternet];
                    }
                });
            }];
            
        } else {
            
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
    
    font15 = [UIFont systemFontOfSize:15];
    UIView *topbackView = [BaseTool viewWithColor:[UIColor whiteColor]];
    topbackView.frame = CGRectMake(0, 0, mainWidth, 100);
    self.tableView.tableHeaderView = topbackView;
    
    [topbackView addSubview:self.pchHeaderView];
    [self.pchHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(topbackView);
        make.height.mas_equalTo(topbackView);
    }];
    
    UIButton *footerView = [BaseTool buttonWithTitle:Localized(@"send_buy_advert") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:18] superView:nil];
    [footerView addTarget:self action:@selector(cadNewAd:) forControlEvents:UIControlEventTouchUpInside];
    footerView.frame = CGRectMake(0, 0, mainWidth, 55);
    footerView.backgroundColor = MY_ORDER_SELECTED_COLOR;
    self.tableView.tableFooterView = footerView;
    if (self.type == 2) {
        [footerView setTitle:Localized(@"send_sale_advert") forState:UIControlStateNormal];
        footerView.backgroundColor = Color(@"00BF84");
    }

    NSArray *sectionOne = @[
                            @{@"title":Localized(@"choose_coin_type"),
                              @"detail":Localized(@"choose"),
                              @"vcName":@"ModifyLoginPswViewController"
                              
                              },
                            @{@"title":Localized(@"choose_addr"),
                              @"detail":Localized(@"china"),
                              @"vcName":@"ModifyLoginPswViewController"
                              },
                            @{@"title":Localized(@"choose_coin"),
                              @"detail":@"CNY",
                              @"vcName":@"ModifyLoginPswViewController"
                              }];
    
    NSString *payWayStr = self.type == 2 ? Localized(@"收款方式") : Localized(@"pay_type");
    NSString *payDataStr1 = self.type == 2 ? Localized(@"收款期限") : Localized(@"pay_time");
    NSString *payDataStr2 = self.type == 2 ? Localized(@"请输入收款期限") : Localized(@"请输入付款期限");
    NSArray *sectionFour = @[
//                             @{@"title":@"最高价",
//                               @"detail":@"请输入最高成交价格",
//                               @"vcName":@"ModifyLoginPswViewController"
//                               },
                             @{@"title":Localized(@"min_price"),
                               @"detail":Localized(@"please_input_minprice"),
                               @"vcName":@"ModifyLoginPswViewController"
                               },
                             @{@"title":Localized(@"max_money"),
                               @"detail":Localized(@"please_input_maxmoney"),
                               @"vcName":@"ModifyLoginPswViewController"
                               },
                             @{@"title":payWayStr,
                               @"detail":Localized(@"choose"),
                               @"vcName":@"ModifyLoginPswViewController"
                               },
                             @{@"title":payDataStr1,
                               @"detail":payDataStr2,
                               @"vcName":@"ModifyLoginPswViewController"
                               }];
    
    
    
    listArr = @[sectionOne,
                @[@{@"title":Localized(@"trade_price"),
                    @"detail":Localized(@"请输入"),
                    @"vcName":@"ModifyLoginPswViewController"
                    }],
                @[@{@"title":@""}],
                sectionFour,
                @[@{@"title":@""}],
                
                //                @[@{@"title":@""}],
                //                @[@{@"title":@"仅限受信任的交易者",
                //                    @"detail":@"请输入付款期限",
                //                    @"vcName":@"ModifyLoginPswViewController"
                //                    }],
                //                @[@{@"title":@"仅限实名认证的交易者",
                //                    @"detail":@"请输入付款期限",
                //                    @"vcName":@"ModifyLoginPswViewController"
                //                    }]
                ];
}

- (void)showPayAlert{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"您暂未绑定支付方式,去绑定?") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        BindingViewController * bindTypeVC = [[BindingViewController alloc] init];
        [self.navigationController pushViewController:bindTypeVC animated:YES];

    }];
    UIAlertAction *action2=[UIAlertAction actionWithTitle:Localized(@"material_dialog_negative_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

    }];
    [alert addAction:action2];
    [alert addAction:action1];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    return;
}

#pragma mark 请求可交易法币列表
- (void)requestAllCurrenciesData {
    //货币类型
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CodeTypes" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            NSArray * dataArr = [MyAdverCurrenciesModel mj_objectArrayWithKeyValuesArray:object.data[@"data"]];
            [self.allCurrenciesArray addObjectsFromArray:dataArr];
            for (int i = 0; i < self.allCurrenciesArray.count; i++) {
                MyAdverCurrenciesModel * model = self.allCurrenciesArray[i];
                [self.allCurrencies addObject:model.code];
            }
//            [self.allCurrencies addObjectsFromArray:@[@"BBC",@"CBC",@"DBC"]];
            if (self.allCurrencies.count >0) {
                NSString *sysmbol = [NSString stringWithFormat:@"%@",self.allCurrencies.firstObject];
                if (!isEmptyString(sysmbol)) {
                    self.chouseBIT.text = sysmbol;
                    self.adNewModel.code_type = sysmbol;
                    self.unit1Label.text = sysmbol;
                    self.unit2Label.text = sysmbol;
                    self.unit3Label.text = sysmbol;
//                    self.unit4Label.text = sysmbol;
                }
            }
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } ];
}

#pragma mark 请求付款期限
- (void)requestCoinPayTime {
    XSLog(@"%@",self.adNewModel.coin_type);
    if (!isEmptyString(self.adNewModel.coin_type)) {
        [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/CoinPayTime" params:@{@"symbol":self.adNewModel.coin_type}] subscribeNext:^(resultObject *object) {
            if (object.status) {
                NSDictionary * coinDict = [object.data objectForKey:self.adNewModel.coin_type];
                XSLog(@"%@",coinDict);
                self.coinPayTimeModel = [MyAdCoinPayTimeModel mj_objectWithKeyValues:coinDict];
                
                if ([self.coinPayTimeModel.pay_time_type integerValue] == 1) {
                    self.pay_time_termTextF.text = self.coinPayTimeModel.c2c_pay_time;
                    self.adNewModel.pay_time_term = self.coinPayTimeModel.c2c_pay_time;
                    self.pay_time_termTextF.userInteractionEnabled = NO;
                } else {
                    self.pay_time_termTextF.text = @"";
                    self.pay_time_termTextF.userInteractionEnabled = YES;
                }
            }
        } error:^(NSError * _Nullable error) {
        }];
    }
}

- (void) cadNewAd:(UIButton*)sender {
    
    //    type：广告类型，*必须传 ；值1是委托买入，值2是委托售出
    self.adNewModel.price = self.tradingPrice.text;
//    self.adNewModel.price_height = self.price_MAX.text;
    self.adNewModel.price_min = self.minimumTextF.text;
    self.adNewModel.price_max = self.maximumTextF.text;
    self.adNewModel.pay_time_term = self.pay_time_termTextF.text;
    if (![self.comment.text isEqualToString:commmmString]) {
        self.adNewModel.comment = self.comment.text;
    }
    self.adNewModel.type = self.type;
    
    NSLog(@" - - - %@",self.adNewModel.mj_keyValues);
    
    NSString * requestURL;
    NSMutableDictionary * requestParams = [NSMutableDictionary dictionary];
    if (self.DetailID) {
        requestURL = @"cuser/cad/EditAd";
        requestParams = self.adNewModel.mj_keyValues;
        requestParams[@"id"] = self.DetailID;
    } else {
        requestURL = @"cuser/cad/NewAd";
        requestParams = self.adNewModel.mj_keyValues;
    }

//    if (!self.adNewModel.pay_type) {
//        [XSTool showToastWithView:self.view Text:@"请选择支付方式"];
//        return;
//    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:requestURL params:requestParams] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            NSString * tipString;
            if (self.DetailID) {
                tipString = @"修改成功";
            } else {
                tipString = Localized(@"add_skill_publish_successs");
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:MyADNotification object:nil];
            
            [MBProgressHUD showMessage:tipString view:self.view hideTime:1 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD showMessage:NetFailure view:self.view];
        
    }];
}



// 获取商家认证状态
- (void) getBusinessInformation {
    [XSTool showProgressHUDWithView:self.view];
    [[XSHTTPManager rac_POSTURL:@"cuser/cuser/UserCenter" params:nil] subscribeNext:^(resultObject *object) {
        [XSTool hideProgressHUDWithView:self.view];
        if (object.status) {
            approve_supply = [object.data[@"approve_supply"] integerValue];
            NSInteger approve_user = [object.data[@"approve_user"] integerValue];
            if (approve_user !=1) {
                
                [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"您还未实名认证，请实名认证") message:nil CallBackBlock:^(NSInteger btnIndex) {
                    if (btnIndex == 1) {
                        
                        [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
                    }
                } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
            }
            
            //            if (approve_supply != 1 ) {
            //                [UIAlertView showWithTitle:@"您还没有成为商家,是否申请成为商家" message:nil cancelButtonTitle:Localized(@"cancel_btn") otherButtonTitles:@[@"确定"] tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
            //                    if (buttonIndex ==1) {
            //                        MerchantApplicationController *vc = [MerchantApplicationController new];
            //                        [self.navigationController pushViewController:vc animated:YES];
            //                    }else {
            //                        [self.navigationController popViewControllerAnimated:YES];
            //                    }
            //                }];
            //             }
        } else {
            [XSTool showToastWithView:self.view Text:object.info];
        }
    } error:^(NSError * _Nullable error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
    
}

- (void)setDetailID:(NSString *)DetailID {
    _DetailID = DetailID;
    
    if (isEmptyString(DetailID)) return;
    //广告详情
    [[XSHTTPManager rac_POSTURL:@"cuser/cad/Detail" params:@{@"id":DetailID}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"cad/cad/Detail====%@",object.data);
        if (object.status) {
            adverModel = [MyAdverModel mj_objectWithKeyValues:object.data];
            self.type =[adverModel.type integerValue];
            self.adNewModel = [NewAdModel mj_objectWithKeyValues:object.data];
            self.adNewModel.pay_type = [adverModel.pay_type componentsJoinedByString:@","];
            [self.tableView reloadData];
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return listArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)listArr[section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellString = [NSString stringWithFormat:@"%ld+%ld",indexPath.section,indexPath.row];
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellString];
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellString];
        NSDictionary *dic = listArr[indexPath.section][indexPath.row];
        cell.textLabel.text = dic[@"title"];
        if (indexPath.section != 4) {
            cell.lineLabel.frame = CGRectMake(0, 55, mainWidth, 0.5);
        }
        switch (indexPath.section) {
            case 0:
                [self setFirstSubviewsWithCell:cell indexPath:indexPath dic:dic];
                break;
            case 1:{
                self.unit1Label = [BaseTool labelWithTitle:@"CNY" textAlignment:0 font:font15 titleColor:COLOR_666666];
                [cell addSubview:self.unit1Label];
                [self.unit1Label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(cell);
                    make.right.mas_equalTo(-12);
                }];
                
                UITextField *textField = [self createTextFieldWithTitle:dic[@"detail"]];
                textField.keyboardType =UIKeyboardTypeDecimalPad;
                [cell addSubview: textField];
                [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(cell);
                    make.right.mas_equalTo(self.unit1Label.mas_left).offset(-11);
                }];
                textField.delegate = self;
                textField.clearButtonMode = UITextFieldViewModeAlways;
                self.tradingPrice = textField;
                self.tradingPrice.delegate = self;
//                [[textField rac_signalForControlEvents:UIControlEventEditingDidEnd]subscribeNext:^(__kindof UIControl * _Nullable x) {
//                    UITextField *ttt = x;
//                    double jyPrice = [ttt.text doubleValue];
//                    double endPrice = jyPrice * ([self.inputField.text doubleValue]/100) + jyPrice;
//                    self.tradingPrice.text = [NSString stringWithFormat:@"%.3f",endPrice];
//                    inputTextView = [self.tradingPrice.text doubleValue];
//                    self.adNewModel.price = self.tradingPrice.text;
//                }];
            }
                break;
            case 2:
            {
                [self setTwoSubviewsWithCell:cell indexPath:indexPath dic:dic];
            }
                break;
            case 3:{
                [self setThreeSubviewsWithCell:cell indexPath:indexPath dic:dic];
                
            }
                
                break;
            case 4:
            {
                [self setFourSubviewsWithCell:cell indexPath:indexPath dic:dic];
            }
                break;
            case 5:
                
                break;
            case 6:
                
                break;
                
                
        }
        
    }
    
    if (!isEmptyString(self.adNewModel.comment)) {
        self.comment.text = self.adNewModel.comment;
        self.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.adNewModel.comment.length];
    }
    
    if (!isEmptyString(self.adNewModel.pay_time_term)) {
        self.pay_time_termTextF.text = self.adNewModel.pay_time_term;
    }
    
    if ([self.coinPayTimeModel.pay_time_type integerValue] == 1) {
        self.pay_time_termTextF.text = self.coinPayTimeModel.c2c_pay_time;
        self.adNewModel.pay_time_term = self.coinPayTimeModel.c2c_pay_time;
        self.pay_time_termTextF.userInteractionEnabled = NO;
    } else {
        self.pay_time_termTextF.text = @"";
        self.pay_time_termTextF.userInteractionEnabled = YES;
    }
    
    if (self.adNewModel.pay_type) {
        NSArray *arr = [self.adNewModel.pay_type componentsSeparatedByString:@","];
        NSMutableString *payString = [NSMutableString string];
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger payType = [arr[idx] integerValue];
            NSString *str;
            switch (payType) {
                case 1:str = Localized(@"bank_card");break;
                case 2:str = Localized(@"alipay");break;
                case 3:str = Localized(@"wechat");break;
            }
            if (isEmptyString(str)) {
                return;
            }
            [payString appendString:str];
        }];
        
        self.chousePayType.text = payString;
    }
    
//    if (!isEmptyString(self.adNewModel.price_height)) {
//        self.price_MAX.text = self.adNewModel.price_height;
//    }
//    if (self.adNewModel.price_min) {
//        self.minimumTextF.text = [NSString stringWithFormat:@"%@",self.adNewModel.price_min];
//    }
//    if (adverModel.price_max) {
//        self.maximumTextF.text = [NSString stringWithFormat:@"%@",self.adNewModel.price_max];
//    }
    if (self.adNewModel.price) {
//        self.tradingPrice.text = self.adNewModel.price;
//        inputTextView = [self.tradingPrice.text doubleValue];
        
    }
    self.inputField.text = persent;
    return cell;
}

- (void) setTwoSubviewsWithCell:(XSBaseTablewCell *)cell indexPath:(NSIndexPath *)indexPath dic:(NSDictionary *)dic {
    
    _slider = [[SDRangeSliderView alloc]initWithFrame:CGRectMake(44, cell.mj_h/2 - 5, mainWidth - 134 - 44, cell.mj_h)];
    _slider.minValue = -99;
    _slider.maxValue = 99;
    _slider.leftValue = 0;
    _slider.rightValue = 0;
    _slider.highlightLineColor = MY_ORDER_SELECTED_COLOR;
    _slider.lineColor = [UIColor hexFloatColor:@"292D40"];
    [cell addSubview:_slider];
    UIFont *font3 = [UIFont systemFontOfSize:13];
    UILabel *leftLabel = [BaseTool labelWithTitle:@"-99" textAlignment:0 font:font3 titleColor:[UIColor hexFloatColor:@"999999"]];
    UILabel *leftLabe2 = [BaseTool labelWithTitle:@"99" textAlignment:0 font:font3 titleColor:[UIColor hexFloatColor:@"999999"]];
    leftLabel.frame = CGRectMake(9, cell.mj_h/2 - 5, 30, 20);
    leftLabe2.frame = CGRectMake(CGRectGetMaxX(_slider.frame)+9, cell.mj_h/2 - 5, 30, 20);
    [cell addSubview:leftLabel];
    [cell addSubview:leftLabe2];
    
    UITextField *inputField = [UITextField new];
    inputField.font = font3;
    inputField.textAlignment = NSTextAlignmentCenter;
    inputField.keyboardType = UIKeyboardTypeDecimalPad;
    inputField.text = @"0";
    inputField.textColor = MY_ORDER_SELECTED_COLOR;
    [cell addSubview:inputField];
    inputField.backgroundColor = main_BackColor;
    [inputField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(leftLabe2);
        make.left.mas_equalTo(leftLabe2.mas_right).offset(8);
        make.width.mas_greaterThanOrEqualTo(50);
        make.height.mas_equalTo(30);
    }];
    UILabel *leftLabe3 = [BaseTool labelWithTitle:@"%" textAlignment:0 font:font3 titleColor:[UIColor hexFloatColor:@"999999"]];
    [cell addSubview:leftLabe3];
    self.inputField = inputField;
    [leftLabe3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(inputField);
        make.left.mas_equalTo(inputField.mas_right).offset(5);
        make.right.mas_greaterThanOrEqualTo(cell.mas_right).offset(-5);
    }];
    
    
    [[inputField.rac_textSignal skip:1] subscribeNext:^(NSString * _Nullable x) {
        persent = x;
        double inputText = [x doubleValue];
        
        self.slider.rightValue = inputText;
        [self.slider update];
        double jyPrice = [self.tradingPrice.text doubleValue];
        double endPrice = jyPrice * inputText/100 + jyPrice;
        self.tradingPrice.text = [NSString stringWithFormat:@"%.3f",endPrice];
        self.adNewModel.price = self.tradingPrice.text;
    }];
    @weakify(self);
    [self.slider eventValueDidChanged:^(double left, double right) {
        @strongify(self);
         double tem =right;
        [self.tradingPrice resignFirstResponder];
        
         inputField.text = [NSString stringWithFormat:@"%.3f",tem];
        double endPrice = inputTextView * tem/100 + inputTextView;
        self.tradingPrice.text = [NSString stringWithFormat:@"%.3f",endPrice];
        self.adNewModel.price = self.tradingPrice.text;

    }];
}


- (void) setFourSubviewsWithCell:(XSBaseTablewCell *)cell indexPath:(NSIndexPath *)indexPath dic:(NSDictionary *)dic {
    
    UITextView *textView = [UITextView new];
    textView.frame = CGRectMake(5, 0, mainWidth - 10, 130);
    textView.delegate = self;
    textView.tintColor = [UIColor blackColor];
    textView.backgroundColor = [UIColor whiteColor];
    [cell addSubview:textView];
    
    UILabel *numberLabel = [BaseTool labelWithTitle:@"500" textAlignment:0 font:[UIFont systemFontOfSize:13] titleColor:[UIColor whiteColor]];
    [cell addSubview:numberLabel];
    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(textView).offset(-5);
        make.bottom.mas_equalTo(textView).offset(-5);
    }];
    self.numberLabel = numberLabel;
    self.comment = textView;
    if (!isEmptyString(adverModel.comment)) {
        self.comment.textColor = [UIColor blackColor];
    }else{
        
        self.comment.text = Localized(@"advert_note");
        self.comment.textColor = [UIColor hexFloatColor:@"676872"];
    }
}



- (void) showCoin_type {
    [self.view endEditing:YES];
    PickerViewSheet *pickerView = [PickerViewSheet pickerView];
    pickerView.pickerType = ShowPickerTypeBITList;
    pickerView.BIT_List = self.allCoinTypeArray;
         @weakify(self);
    [pickerView setCompletionHandler:^(NSString *bit) {
        @strongify(self);
        self.coin_type.text = bit;
        self.adNewModel.coin_type = bit;
        [self requestCoinPayTime];
        [self getPoundageWithSysmbol:bit];
//        threeLabel
//        self.pchHeaderView.threeLabel.text = [NSString stringWithFormat:@"钱包中至少需要有0.05%@，广告才会正常展示",bit];
         if (BIT_infor) {
             [BIT_infor.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
                 NSDictionary *dic =x;
                 NSString *name = dic[@"name"];
                 if ([name isEqualToString:[bit lowercaseString]]) {
                     scprice = dic[@"newPrice"];
                     NSString *ssss = [NSString stringWithFormat:@"%@%@)",Localized(@"溢价设置(市场参考价格"),isEmptyString(scprice)?@"0":scprice];
                     dispatch_async(dispatch_get_main_queue(), ^{
                     self.referenceLabel.text = ssss;
                     });
                     
                 }
             }];
        }
       __block NSString *money;
        [self.walletMoneyDictionary.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
            NSDictionary *dic = x;
            NSString *name =dic[@"coin"];
            if ([name isEqualToString:[bit lowercaseString]]) {
              money = dic[@"remain"][@"number"];
            }
         }completed:^{
             dispatch_async(dispatch_get_main_queue(), ^{
                if (money.doubleValue < 0.05 && money) {
//                    NSString *showString = [NSString stringWithFormat:@"您的钱包余额已不足0.05%@，广告将无法正常展示。",bit];
//                    [BaseTool showInternetInfor:showString taPAction:nil];
                }else{
                    [BaseTool hiddenInternet];
                }
            });
        }];
  
    }];
    [pickerView show];
}

- (void)getPoundageWithSysmbol:(NSString *)symbol {
    
    if (isEmptyString(symbol)) return;
    //广告详情
    [[XSHTTPManager rac_POSTURL:@"cuser/cad/CoinCharge" params:@{@"symbol":symbol,@"type":@(self.type)}] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"cuser/cad/Info====%@",object.data);
        if (object.status) {

            self.pchHeaderView.twoLabel.text = [NSString stringWithFormat:@"%@%@%%%@",Localized(@"advert_des_one_1"), object.returnValue,Localized(@"advert_des_one_2")];
            self.pchHeaderView.oneLabel.text = [NSString stringWithFormat:@"%@",Localized(@"send_advert_free")];
            self.pchHeaderView.threeLabel.text = [NSString stringWithFormat:Localized(@"发布广告,系统会自动冻结所需广告费,广告若下架会退回冻结的广告费")];
            
        } else {
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
    
    
}

- (void) setFirstSubviewsWithCell:(XSBaseTablewCell *)cell indexPath:(NSIndexPath *)indexPath dic:(NSDictionary *)dic {
    
    cell.textLabel.text = dic[@"title"];
    UIImageView *imageView = [BaseTool imageWithName:@"cart_gostore_arrow" superView:cell];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell);
        make.right.mas_equalTo(cell.mas_right).offset(-12);
    }];
    
    UILabel * label = [BaseTool labelWithTitle:dic[@"detail"] textAlignment:NSTextAlignmentRight font:font15 titleColor:[UIColor blackColor]];
    [cell addSubview:label];
    label.userInteractionEnabled = YES;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell);
        make.right.mas_equalTo(imageView.mas_left).offset(-12);
        make.width.mas_equalTo(mainWidth/2);
    }];
    @weakify(self);
    switch (indexPath.row) {
        case 0:
        {
            self.coin_type = label;
            self.adNewModel.coin_type = self.coin_type.text;
            [self.coin_type addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
                @strongify(self);
                [self showCoin_type];
             }]];
 
    }
            break;
        case 1:
        {
            self.BIT_address = label;
            self.adNewModel.country = @"CN";
            [self.BIT_address addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
                @strongify(self);
                self.adNewModel.country =  self.BIT_address.text;
                ChouseCountryController *vc = [ChouseCountryController new];
                [vc setChouseCountry:^(CountryModel *model) {
                    self.BIT_address.text = model.name;
                    self.adNewModel.country = model.code;
                }];
                [self.navigationController pushViewController:vc animated:YES];
            }]];
        }
            break;
        case 2:
        {
            self.chouseBIT = label;
            [self.chouseBIT addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
                @strongify(self);

                PickerViewSheet *pickerView = [PickerViewSheet pickerView];
                pickerView.pickerType = ShowPickerTypeBITList;
                pickerView.BIT_List = self.allCurrencies;
                [pickerView setCompletionHandler:^(NSString *bit) {
                    @weakify(self);
                    self_weak_.chouseBIT.text = bit;
                    self.adNewModel.code_type = bit;
                    self_weak_.unit1Label.text = bit;
                    self_weak_.unit2Label.text = bit;
                    self_weak_.unit3Label.text = bit;
//                    self_weak_.unit4Label.text = bit;
                }];
                [pickerView show];
            }]];
        }
            break;
            
    }
    
}

/**
 section of three
 
 @param cell cell description
 @param indexPath indexPath description
 @param dic dic description
 */
- (void) setThreeSubviewsWithCell:(XSBaseTablewCell *)cell indexPath:(NSIndexPath *)indexPath dic:(NSDictionary *)dic{
    
    cell.textLabel.text = dic[@"title"];
    UITextField *textField;
    if (indexPath.row >= 0 && indexPath.row <=1) {
        
        UILabel * label = [BaseTool labelWithTitle:@"CNY" textAlignment:0 font:font15 titleColor:[UIColor blackColor]];
        switch (indexPath.row) {
            case 0:
                self.unit2Label = label;
                break;
            case 1:
                self.unit3Label = label;
                break;
//            case 2:
//                self.unit4Label = label;
//                break;
            default:
                break;
        }
        [cell addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell);
            make.right.mas_equalTo(-12);
        }];
        textField = [self createTextFieldWithTitle:dic[@"detail"]];
        [cell addSubview: textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell);
            make.right.mas_equalTo(label.mas_left).offset(-11);
        }];
        
    }else if (indexPath.row == 2){
        UIImageView *imageView = [BaseTool imageWithName:@"accessory" superView:cell];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell);
            make.right.mas_equalTo(cell.mas_right).offset(-12);
        }];
        
        UILabel *label = [BaseTool labelWithTitle:Localized(@"choose") textAlignment:NSTextAlignmentRight font:font15 titleColor:[UIColor blackColor]];
        @weakify(self);
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
            @strongify(self);
            [self.view endEditing:YES];
            
            if (self.payTypeDictionary.count == 0) {
                [self showPayAlert];
//                [XSTool showToastWithView:self.view Text:@"您暂未绑定支付方式"];
                return;
            }
            
            PickerViewSheet *pickerView = [PickerViewSheet pickerView];
            pickerView.BIT_List = self.payTypeDictionary;
            pickerView.pickerType = ShowPickerTypePayTypeList;
             [pickerView setCompletionHandler:^(NSArray *arr) {
                NSMutableArray *showTextArr = [NSMutableArray array];
                 NSMutableArray *showCodeArr = [NSMutableArray array];
                 [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                     WalletPaytypeModel *model = arr[idx];
                     [showTextArr addObject:model.code_desc];
                     [showCodeArr addObject:model.code];
                 }];
                label.text = [showTextArr componentsJoinedByString:@","];
                self.adNewModel.pay_type = [showCodeArr componentsJoinedByString:@","];
            }];
            [pickerView show];
            
            
        }]];
        [cell addSubview: label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(imageView.mas_left).offset(-12);
            make.centerY.mas_equalTo(cell);
            make.width.mas_equalTo(mainWidth/2);
            make.height.mas_equalTo(40);
        }];
        self.chousePayType = label;
    }else{
        
        UILabel *label = [BaseTool labelWithTitle:Localized(@"fen") textAlignment:0 font:font15 titleColor:COLOR_666666];
        [cell addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell);
            make.right.mas_equalTo(-12);
        }];
        textField = [self createTextFieldWithTitle:dic[@"detail"]];
        [cell addSubview: textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell);
            make.right.mas_equalTo(label.mas_left).offset(-11);
        }];
    }
    
    switch (indexPath.row) {
//        case 0:self.price_MAX = textField;break;
        case 0:self.minimumTextF = textField;break;
        case 1:self.maximumTextF = textField;break;
        case 2:break;
        case 3:self.pay_time_termTextF = textField;break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    [self.view endEditing:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 2) {
         UIView *headerView = [BaseTool viewWithColor:main_BackColor];
        headerView.frame = CGRectMake(0, 0, mainWidth, 40);
        UIFont *font3 = [UIFont systemFontOfSize:13];
        NSString *ssss = [NSString stringWithFormat:@"%@%@)",Localized(@"溢价设置(市场参考价格"),isEmptyString(scprice)?@"0":scprice];
        self.referenceLabel = [BaseTool labelWithTitle:ssss textAlignment:0 font:font3 titleColor:[UIColor hexFloatColor:@"676872"]];
        [headerView addSubview:self.referenceLabel];
 
        [self.referenceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(13);
            make.centerY.mas_equalTo(headerView).offset(-5);
        }];
        return headerView;
    }
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
 
    if (section ==2) {
        return 40;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 2) {
        UIView *footerView = [BaseTool viewWithColor:main_BackColor];
        UIFont *font3 = [UIFont systemFontOfSize:13];
        UILabel *label1 = [BaseTool labelWithTitle:Localized(@"advert_trade_des") textAlignment:0 font:font3 titleColor:[UIColor hexFloatColor:@"676872"]];
        label1.numberOfLines = 0;
        [footerView addSubview:label1];
        
        [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(13);
            make.top.mas_equalTo(7);
            make.right.mas_equalTo(-13);
        }];
        return footerView;
    }else{
        return [UIView new];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section != 3 && section != 2) {
        return 0;
    }
    if (section == 2) {
        return 65;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 4) {
        return 130;
    }
    return 55;
}

- (UITextField *) createTextFieldWithTitle:(NSString *)title {
    UITextField *textField = [UITextField new];
    textField.placeholder = title;
    textField.font = font15;
    textField.textAlignment = NSTextAlignmentRight;
    [textField setValue:[UIColor hexFloatColor:@"999999"] forKeyPath:@"_placeholderLabel.textColor"];
//    textField.textColor = [UIColor whiteColor];
    return textField;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.comment.textColor = [UIColor blackColor];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    textView.text = nil;
    self.comment.textColor = [UIColor blackColor];
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (isEmptyString(textView.text)) {
        self.comment.text = commmmString;
        self.numberLabel.text = @"500";
        self.comment.textColor = [UIColor hexFloatColor:@"676872"];
     }
    
    if (![textView.text isEqualToString:commmmString]) {
        self.adNewModel.comment = self.comment.text;
    }

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSInteger existedLength = textView.text.length;
    NSInteger selectedLength = range.length;
    NSInteger replaceLength = text.length;
    NSInteger pointLength = existedLength - selectedLength + replaceLength;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",pointLength];
     //超过16位 就不能在输入了
    if (pointLength > 500) {
        
        return NO;
    }else{
        return YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.tradingPrice == textField) {
        inputTextView = self.tradingPrice.text.doubleValue;
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    NSLog(@"%@====%@",textField.text,_moneyTextField.text);
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location==NSNotFound) {
        isHaveDian=NO;
    }
    if ([string length]>0)
    {
        unichar single=[string characterAtIndex:0];//当前输入的字符
        if ((single >='0' && single<='9') || single=='.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length]==0){
                if(single == '.'){
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
            }
            if (single=='.')
            {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian=YES;
                    return YES;
                }else
                {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            else
            {
                if (isHaveDian)//存在小数点
                {
                    NSInteger precision = 2;
                    if (textField == self.tradingPrice) {//价格小数位数
                        //判断小数点的位数
                        NSRange ran=[textField.text rangeOfString:@"."];
                        NSUInteger tt=range.location-ran.location;
                        if (tt <= precision){
                            return YES;
                        }else{
                            return NO;
                        }
                    }
                    return YES;
                }else
                {
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            //            [self alertView:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
    
}

@end


 
