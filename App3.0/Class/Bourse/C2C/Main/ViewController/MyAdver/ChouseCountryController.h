//
//  ChouseCountryController.h
//  BIT
//
//  Created by Sunny on 2018/4/7.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChouseCountryController : BaseViewController


@property (nonatomic,copy) void (^chouseCountry)(CountryModel *mode);


@end
