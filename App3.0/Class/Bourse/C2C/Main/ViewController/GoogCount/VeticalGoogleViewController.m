//
//  VeticalGoogleViewController.m
//  App3.0
//
//  Created by xinshang on 2018/2/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "VeticalGoogleViewController.h"
#import "VerticalModel.h"

@interface VeticalGoogleViewController ()

@property (nonatomic, strong) UITextField *secretPSWTF;//秘钥
@property (nonatomic, strong) UITextField *codeTF;//秘钥验证码
@property (nonatomic,strong)  UIButton *tCopyBtn;
@property (nonatomic,strong)  NSString *desText;
@property (nonatomic,strong)  NSString *noteText;
@property (nonatomic,strong)  VerticalGoogleModel *googleModel;
@property (nonatomic,strong) UIButton *sureBtn;

@end
#define cellHeight 44

@implementation VeticalGoogleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"谷歌认证");
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setUpUI];
    [self mobileVerifyIndex];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

#pragma mark - 设置子视图
-(void)setUpUI{
    
    //    [self setBlackLeftBackBtn];
//    [self setNavTitle:@"谷歌认证" isShowBack:NO];
    
//    @weakify(self);
//    [self actionCustomLeftBtnWithNrlImage:@"back_Y" htlImage:nil title:nil action:^{
//        @strongify(self);
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
//
//    UIButton* leftBtn= [UIButton buttonWithType:UIButtonTypeCustom];
//    [leftBtn setImage:[UIImage imageNamed:@"back_Y"] forState:UIControlStateNormal];
//    leftBtn.frame = CGRectMake(0, 0, 40, 40);
//    UIBarButtonItem* leftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
//    [leftBtn addTarget:self action:@selector(backBrnAction) forControlEvents:UIControlEventTouchUpInside];
//    [self.navigationItem setLeftBarButtonItem:leftBtnItem];
 
}


- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    
   BOOL code = [dic[@"code"] boolValue];
    
    if (code) {
//        NSString *message = [NSString stringWithFormat:@"%@,%@:%@",Localized(@"已认证"),Localized(@"谷歌验证码"),self.googleModel.verify];
        [MBProgressHUD showMessage:@"您已绑定谷歌验证" view:self.view hideTime:1.5 doSomeThing:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];

    }else {
        [self mobileVerifyIndex];
    }
}

- (void)mobileVerifyIndex
{
    
    [HTTPManager yun_googleVerifyIndexSuccess:^(NSDictionary *dic, resultObject *state)
     {
         [XSTool hideProgressHUDWithView:self.view];
         if (state.status) {
             NSDictionary *tDic = dic[@"data"];
             if (tDic) {
                 self.googleModel = [VerticalGoogleModel mj_objectWithKeyValues:tDic];

                 if ([self.googleModel.is_google_verify integerValue] > 0) {
//                     NSString *message = [NSString stringWithFormat:@"%@%@:%@",Localized(@"user_approve_process_over"),Localized(@"谷歌验证码"),self.googleModel.title];
//                      [MBProgressHUD showMessage:message view:self.view hideTime:1.5 doSomeThing:nil];
                 }
             }
         }else{
             [MBProgressHUD showMessage:state.info view:self.view];
         }
         [self.tableView reloadData];

     } fail:^(NSError *error) {
     }];
}

#pragma mark --- UITableViewDataSource and UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"UITableViewCell"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell.backgroundColor = Color(@"282F37");
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
         
        }
       
        if (indexPath.row==0)
        {
            if (!self.secretPSWTF) {
                CGRect tFrame = CGRectMake(56, 0, mainWidth-160, cellHeight);
                self.secretPSWTF = [self creatTextFieldWithPlaceTilte:@"" WithFrame:tFrame];
                self.secretPSWTF.enabled = NO;
                [cell addSubview:self.secretPSWTF];
                UIView *line = [[UIView alloc] initWithFrame:CGRectMake(13, cellHeight-1, mainWidth-13, 1)];
                line.backgroundColor = LINE_COLOR_NORMAL;
                [cell addSubview:line];
                
            }
            if (!self.tCopyBtn) {
                
                self.tCopyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                self.tCopyBtn.frame = CGRectMake(mainWidth-80, (cellHeight-28)/2, 65, 28);
                self.tCopyBtn.layer.masksToBounds = YES;
                self.tCopyBtn.layer.cornerRadius = 3;
//                self.tCopyBtn.layer.borderWidth = 0.5;
//                self.tCopyBtn.layer.borderColor = Color(@"3D4250").CGColor;
//                self.tCopyBtn.backgroundColor = Color(@"3D4250");
                self.tCopyBtn.backgroundColor = mainColor;
                [self.tCopyBtn setTitle:Localized(@"复制") forState:UIControlStateNormal];
                [self.tCopyBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
                [self.tCopyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self.tCopyBtn addTarget:self action:@selector(tCopyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:self.tCopyBtn];
            }
            if (!isEmptyString(self.googleModel.secret)) {
                self.secretPSWTF.text = [NSString stringWithFormat:@"%@",self.googleModel.secret];
            }
            
            cell.textLabel.text = Localized(@"秘钥");
        }else if (indexPath.row == 1) {
            
            if (!self.codeTF) {
                CGRect tFrame = CGRectMake(105, 0, mainWidth-120, cellHeight);
                self.codeTF = [self creatTextFieldWithPlaceTilte:Localized(@"cc_sms_ingoogle_hint") WithFrame:tFrame];
                [cell addSubview:self.codeTF];
            
            }
            
            cell.textLabel.text = Localized(@"谷歌验证码");
            if (self.googleModel.is_google_verify.intValue>0){
                cell.textLabel.text = Localized(@"原谷歌验证码");
                self.codeTF.placeholder = Localized(@"请输入原谷歌验证码");
                [self.codeTF setValue:Color(@"787F89") forKeyPath:@"_placeholderLabel.textColor"];
                self.codeTF.frame = CGRectMake(115, 0, mainWidth-130, cellHeight);
                
            }else{
                cell.textLabel.text = Localized(@"谷歌验证码");
                self.codeTF.placeholder = Localized(@"请输入谷歌验证码");
                [self.codeTF setValue:Color(@"787F89") forKeyPath:@"_placeholderLabel.textColor"];
                self.codeTF.frame =  CGRectMake(105, 0, mainWidth-120, cellHeight);
            }
        }else if (indexPath.row == 2) {
            cell.textLabel.font = [UIFont systemFontOfSize:13.5];
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.textColor = Color(@"999999");

            NSString *screct = @"***";
            if (!isEmptyString(self.googleModel.secret)) {
                screct = self.googleModel.secret;
            }
            self.desText = [NSString stringWithFormat:@"1.%@“Google Authenticator”%@\n2.%@%@ %@%@，%@。\n3.%@“%@”，%@“%@”%@。",Localized(@"安装在"),Localized(@"应用程序中，点击右上角“+”号，然后选择“手动输入验证码”。"),Localized(@"黏贴上面的“秘钥”"),screct,Localized(@"到“秘钥”输入框并填写您的"),Localized(@"BTCBL账号"),Localized(@"点击“完成”"),Localized(@"复制"),Localized(@"谷歌验证码"),Localized(@"黏贴到上面的"),Localized(@"谷歌验证码"),Localized(@"输入栏")];
            NSMutableAttributedString *attributeStr = [self setStringWithStr:self.desText
                                                                    RangStr1:@"Google Authenticator"
                                                                    RangStr2:screct
                                                                    RangStr3:Localized(@"BTCBL账号")
                                                                    RangStr4:Localized(@"谷歌验证码")
                                                                    RangStr5:Localized(@"谷歌验证码”输入栏")
                                                                    RangStr6:Localized(@"”输入栏")
                                                       ];
             cell.textLabel.attributedText=attributeStr;
            cell.textLabel.numberOfLines = 0;

//            cell.textLabel.text = self.desText;
            
        }else if (indexPath.row == 3) {
            cell.textLabel.font = [UIFont systemFontOfSize:13.5];
            cell.textLabel.numberOfLines = 0;
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.textColor = Color(@"999999");
            self.noteText = [NSString stringWithFormat:@"%@", Localized(@"备注：请勿删除此双重验证密码账户，否则会导致您无法进行账户操作；如果误删，您可以通过重置秘钥重新获取。")];
            cell.textLabel.text = self.noteText;
            
        }
        
        return cell;
        
    }else{
        static NSString *CellIdentifier = @"CellRegisterBtn";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+1000, 0,0);
        }
        if (!self.sureBtn) {
            self.sureBtn = [self creatSureBtn];
            [cell addSubview:self.sureBtn];
        }
        
        if (self.googleModel.is_google_verify.intValue) {
            [self.sureBtn setTitle:Localized(@"解绑 GOOGLE 认证") forState:UIControlStateNormal];
        }else{
            [self.sureBtn setTitle:Localized(@"cc_save_btn_google") forState:UIControlStateNormal];

        }
        
        return cell;
    }
}


- (NSMutableAttributedString *)setStringWithStr:(NSString *)str
                                       RangStr1:(NSString *)rangStr1
                                       RangStr2:(NSString *)rangStr2
                                       RangStr3:(NSString *)rangStr3
                                       RangStr4:(NSString *)rangStr4
                                       RangStr5:(NSString *)rangStr5
                                       RangStr6:(NSString *)rangStr6{
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:str];
    //获取要调整颜色的文字位置,调整颜色
    NSRange range1=[[attributeStr string]rangeOfString:rangStr1];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range1];
    NSRange range2=[[attributeStr string]rangeOfString:rangStr2];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range2];
    NSRange range3=[[attributeStr string]rangeOfString:rangStr3];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range3];
    NSRange range4=[[attributeStr string]rangeOfString:rangStr4];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range4];
    
    NSRange range5=[[attributeStr string]rangeOfString:rangStr5];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range5];
    NSRange range6=[[attributeStr string]rangeOfString:rangStr6];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:mainColor range:range6];

//    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.5] range:range];
    return attributeStr;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CGFloat space =10;
        if (indexPath.row == 2) {
            CGSize titleSize = [self.desText boundingRectWithSize:CGSizeMake(mainWidth-space*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size;
            return titleSize.height +10;

        }else if (indexPath.row == 3) {
            CGSize titleSize = [self.noteText boundingRectWithSize:CGSizeMake(mainWidth-space*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size;
            return titleSize.height +10;
            
        }
        return cellHeight;
        
    }
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 10)];
    view.backgroundColor = main_BackColor;
    
    return view;
}



#pragma mark - 复制秘钥
-(void)tCopyBtnAction:(UIButton *) sender{
    if (!isEmptyString(self.googleModel.secret)) {
        NSString *text = [NSString stringWithFormat:@"%@",self.googleModel.secret];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = text;
//         Alert(@"秘钥复制成功!");
        [MBProgressHUD showMessage:Localized(@"copy_success") view:self.view];
    }else{
    }
}
- (UIButton *)creatSureBtn
{
    CGFloat space = 18;
    UIButton *Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    Btn.frame = CGRectMake(space,30, mainWidth - space*2, 45);
    [Btn setTitle:Localized(@"cc_save_btn_google") forState:UIControlStateNormal];
    [Btn setBackgroundColor:mainColor];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 5;
    [Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(sureBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    return Btn;
}

#pragma mark----确定
//确定
- (void)sureBtnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
 
    if (isEmptyString(self.codeTF.text)) {
        if (self.googleModel.is_google_verify.intValue) {//已绑定===>解绑
        [MBProgressHUD showMessage:Localized(@"请输入原谷歌验证码") view:self.view];
        }else{
        [MBProgressHUD showMessage:Localized(@"cc_sms_ingoogle_hint") view:self.view];
        }
        return;
    }
    NSDictionary *dic = [NSDictionary dictionary];
    NSString *googleBindUrl = @"user/login/GoogleBind";
    if (self.googleModel.is_google_verify.intValue) {//已绑定===>解绑
        googleBindUrl = @"user/login/GoogleUnBind";
        dic =@{@"google_verify":self.codeTF.text};
    }else{
        googleBindUrl = @"user/login/GoogleBind";
        dic =@{@"secret":self.googleModel.secret, @"google_verify":self.codeTF.text};
    }
    @weakify(self);
    [XSTool showProgressHUDWithView:self.view];
    [[XSHTTPManager rac_POSTURL:googleBindUrl params:dic] subscribeNext:^(resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.codeTF.text = @"";
            if (self.googleModel.is_google_verify.intValue) {//已绑定===>解绑
                [MBProgressHUD showMessage:@"解绑成功" view:self.view hideTime:1.5 doSomeThing:^{
                    [self mobileVerifyIndex];
                }];
            }else{
                [MBProgressHUD showMessage:@"验证成功" view:self.view hideTime:1.5 doSomeThing:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
        }else{
            [MBProgressHUD showMessage:state.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [XSTool hideProgressHUDWithView:self.view];
        [MBProgressHUD showMessage:NetFailure view:self.view];
        
    }];
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

- (UITextField *)creatTextFieldWithPlaceTilte:(NSString*)title WithFrame:(CGRect)frame
{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.textAlignment = NSTextAlignmentLeft;
    textField.placeholder = Localized(title);
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:15];
    textField.delegate = self;
//    [textField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    UILabel *placeholderLabel = [textField valueForKeyPath:@"_placeholderLabel"];
    placeholderLabel.numberOfLines = 0;
    return textField;
}


@end





