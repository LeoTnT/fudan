//
//  MerchantApplicationController.h
//  BIT
//
//  Created by Sunny on 2018/3/28.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"

@interface MerchantApplicationController : XSBaseTablViewController
@property (nonatomic, strong) NSArray *areaNameArray;
@property (nonatomic, strong) NSArray *areaIdArray;//省市区
@end
