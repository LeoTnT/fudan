//
//  JYSChoosePaperworkView.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSChoosePaperworkView.h"
#import "RealNameVerificationModel.h"
#import "JYSRealNameVerificationModel.h"

@interface JYSChoosePaperworkView ()<UIPickerViewDelegate,UIPickerViewDataSource>

/** pickerView */
@property (nonatomic, strong) UIPickerView * pickerView;

/** dataArray */
@property (nonatomic, copy) NSArray * dataArray;

/** 选中证件类型 */
@property (nonatomic, strong) RealNameVerCardTypeModel * selectCardModel;



@end

@implementation JYSChoosePaperworkView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    self.backgroundColor = [UIColor whiteColor];
    
    UIView * topView = [XSUITool creatViewWithFrame:CGRectZero backgroundColor:BG_COLOR];
    [self addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.height.mas_equalTo(FontNum(44));
    }];
    
    UIButton * cancelButton = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(cancelButtonClicked)];
    [XSUITool setButton:cancelButton titleColor:JYSMainTextColor titleFont:FontNum(15) backgroundColor:nil image:nil backgroundImage:nil forState:UIControlStateNormal title:Localized(@"cancel_btn")];
    [topView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.mas_equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(FontNum(50), FontNum(30)));
    }];
    
    UIButton * determineButton = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(determineButtonClicked)];
    [XSUITool setButton:determineButton titleColor:JYSMainTextColor titleFont:FontNum(15) backgroundColor:nil image:nil backgroundImage:nil forState:UIControlStateNormal title:Localized(@"material_dialog_positive_text")];
    [topView addSubview:determineButton];
    [determineButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(topView).offset(-10);
        make.centerY.mas_equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(FontNum(50), FontNum(30)));
    }];
    
    self.pickerView = [[UIPickerView alloc] init];
    [self addSubview:self.pickerView];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.top.mas_equalTo(topView.mas_bottom);
    }];
}

- (void)setPaperworks:(NSArray *)paperworks determineBlock:(selectedPaperworkBlock)deterBlock cancelBlock:(cancelChoosePaperworkBlock)cancBlock {
    self.dataArray = paperworks;
    
    self.selectedBlock = deterBlock;
    self.cancelBlock = cancBlock;
    
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
}

- (void)cancelButtonClicked {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

- (void)determineButtonClicked {
    if ([self anySubViewScrolling:self.pickerView]) {
        XSLog(@"正在滚动中~~~~");
    } else {
        if (self.selectedBlock) {
            self.selectedBlock(self.selectCardModel);
        }
    }
}

- (BOOL)anySubViewScrolling:(UIView *)view{
    if ([view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)view;
        if (scrollView.dragging || scrollView.decelerating) {
            return YES;
        }
    }
    for (UIView *theSubView in view.subviews) {
        if ([self anySubViewScrolling:theSubView]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.dataArray.count;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = nil;
    if (view != nil) {
        label = (UILabel *)view;
        //设置bound
    }else{
        label = [[UILabel alloc] init];
    }
    
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    if (self.dataArray.count) {
        RealNameVerCardTypeModel * typeModel = self.dataArray[row];
        label.text = typeModel.card_name;
        RealNameVerCardTypeModel * typeModel_1 = self.dataArray[0];
        self.selectCardModel = typeModel_1;
    }
    
    return label;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    RealNameVerCardTypeModel * typeModel = self.dataArray[row];
    self.selectCardModel = typeModel;
}



-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 44;
}

@end
