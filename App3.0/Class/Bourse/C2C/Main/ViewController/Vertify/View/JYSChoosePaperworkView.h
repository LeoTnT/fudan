//
//  JYSChoosePaperworkView.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/22.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RealNameVerCardTypeModel;
typedef void(^selectedPaperworkBlock)(RealNameVerCardTypeModel * cardTypeModel);
typedef void(^cancelChoosePaperworkBlock)(void);

@interface JYSChoosePaperworkView : UIView

/** 确定 */
@property (nonatomic, copy) selectedPaperworkBlock selectedBlock;
/** 取消 */
@property (nonatomic, copy) cancelChoosePaperworkBlock cancelBlock;

- (void)setPaperworks:(NSArray *)paperworks determineBlock:(selectedPaperworkBlock)deterBlock cancelBlock:(cancelChoosePaperworkBlock)cancBlock;

@end
