//
//  MerchantApplicationController.m
//  BIT
//
//  Created by Sunny on 2018/3/28.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MerchantApplicationController.h"
#import "ProvinceViewController.h"
#import "OrderPayViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface MerchantApplicationController ()<UITextViewDelegate,UITextFieldDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *reasonLabel;
@property (nonatomic,assign) BOOL isRefued;
@property (nonatomic,strong) UITextField *truenameLabel;
@property (nonatomic,strong) UITextField *card_noLabel;

@property (nonatomic,strong) UIImageView *img_card1;
@property (nonatomic,strong) UIImageView *img_card2;
@property (nonatomic,strong) UIImageView *img_card3;

@property (nonatomic,strong) UIImage *img_1;
@property (nonatomic,strong) UIImage *img_2;
@property (nonatomic,strong) UIImage *img_3;

@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) NSMutableString *fileImgStr;


@property (nonatomic,strong) UITextField *phone;
@property (nonatomic,strong) UITextField *addressLabel;
@property (nonatomic,strong) UITextField *addressInfor;
@property (nonatomic,strong) UITextField *weixinAccount;

@property (nonatomic,strong) UITextView *inputInfor;
@property (nonatomic,strong) RegisterEnterModel *upInforModel;

@property (nonatomic,strong) BusinessRegisterParser *businessParser;
@property (nonatomic, strong) BusinessApplyIndustryParser *industryParser;
@property (nonatomic,strong) AreaListModel *listModel;

/** 是否显示空白页 */
@property (nonatomic, assign) BOOL shouldShowEmptyView;


/**
 - 1 提交申请 0 审核中 1 已认证 2 失败
 */
//@property (nonatomic, assign) NSInteger apptove; // 认证状态
@end

@implementation MerchantApplicationController
{
    NSInteger selectedIndex;
    NSMutableArray *imageArr;
    UIButton *sureButton;
    UIView *footerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"委托商申请");
    self.autoHideKeyboard = YES;
    [self getBusinessInformation];

    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];


//    NSArray *arr = @[
//                     @[
//                         @{@"title":Localized(@"user_approve_name"),
//                           @"placeholder":Localized(@"user_approve_name_hint")
//                           },
//                         @{@"title":Localized(@"user_approve_cardno"),
//                           @"placeholder":Localized(@"user_approve_cardno_hint")
//                           }
//                         ],
//                     @[@{@"title":Localized(@"user_approve_card_front"),@"title1":Localized(@"上传图片大小控制在3M以内！"),@"image":@"jys_mine_IDCard_front"}],
//                     @[@{@"title":Localized(@"user_approve_card_back"),@"title1":Localized(@"上传图片大小控制在3M以内！"),@"image":@"jys_mine_IDCard_back"}],
//                     @[@{@"title":Localized(@"user_idcard_text"),@"title1":Localized(@"上传图片大小控制在3M以内！"),@"image":@"jys_mine_IDCard_hand"}],
//                     @[
//                         @{@"title":@"手机号码",
//                           @"placeholder":@"请输入手机号码"
//                           }
////                         ,
////                         @{@"title":@"联系地址",
////                           @"placeholder":@"请选择省市县/区"
////                           },
////                         @{@"title":@"详细地址",
////                           @"placeholder":@"例如街道、小区、楼栋、单元号"
////                           },
////                         @{@"title":@"微信账号",
////                           @"placeholder":@"请输入微信账号"
////                           }
//                         ],
//                     @[
//                         @{@"title":Localized(@"input_note"),
//                           @"placeholder":@"例如街道、小区、楼栋、单元号"
//                           }
//                         ]
//                     ];
    
    
    NSArray *arr = @[
                     @[
                         @{@"title":Localized(@"user_approve_name"),
                           @"placeholder":Localized(@"user_approve_name_hint")
                           },
                         @{@"title":Localized(@"user_approve_cardno"),
                           @"placeholder":Localized(@"user_approve_cardno_hint")
                           }
                         ],
                     @[@{@"title":Localized(@"user_approve_card_front"),@"title1":@"",@"image":@"jys_mine_IDCard_front"}],
                     @[@{@"title":Localized(@"user_approve_card_back"),@"title1":@"",@"image":@"jys_mine_IDCard_back"}],
                     @[@{@"title":Localized(@"user_idcard_text"),@"title1":@"",@"image":@"jys_mine_IDCard_hand"}]
//                     @[
//                         @{@"title":@"手机号码",
//                           @"placeholder":@"请输入手机号码"
//                           }
//                         ],
//                     @[
//                         @{@"title":Localized(@"input_note"),
//                           @"placeholder":@"例如街道、小区、楼栋、单元号"
//                           }
//                         ]
                     ];
    

    self.dataSource = [NSMutableArray arrayWithArray:arr];
    
     footerView=  [BaseTool viewWithColor:[UIColor whiteColor]];
    footerView.frame = CGRectMake(0, 0, mainWidth, 93);
    self.tableView.tableFooterView = footerView;
    footerView.frame = CGRectMake(0, 0, mainWidth, 93);
    sureButton = [BaseTool buttonTitle:Localized(@"submit_apply") image:nil superView:footerView];
    [sureButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
    sureButton.layer.cornerRadius = 25;
    sureButton.layer.masksToBounds = YES;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.centerX.mas_equalTo(self.view);
        make.width.mas_equalTo(mainWidth - 50);
        make.height.mas_equalTo(50);
    }];
    [sureButton addTarget:self action:@selector(upInfor) forControlEvents:UIControlEventTouchUpInside];
    
    self.upInforModel = [RegisterEnterModel new];
 
    
    [[UserSingle sharedUserSingle].siginleSubject subscribeNext:^(id  _Nullable x) {
        RACTupleUnpack(NSNumber *type,AreaListModel *listModel) = x;
         if ([type isEqualToNumber:@1]) {
            self.listModel = listModel;
            self.addressLabel.text = listModel.component_pdetail;
        }
        
    }];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
//    self.shouldShowEmptyView = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.areaNameArray) {
        self.addressLabel.text = [self.areaNameArray componentsJoinedByString:@"-"];
        
        self.upInforModel.province = self.areaNameArray[0];
        self.upInforModel.city = self.areaNameArray[1];
        self.upInforModel.county = self.areaNameArray[2];
        self.upInforModel.town = self.areaNameArray[3];
        self.upInforModel.province_code = self.areaIdArray[0];
        self.upInforModel.city_code = self.areaIdArray[1];
        self.upInforModel.county_code = self.areaIdArray[2];
        self.upInforModel.town_code = self.areaIdArray[3];
    }

}


- (void) getBusinessInformation {
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [[XSHTTPManager rac_POSTURL:@"cuser/csupplier/Info" params:nil] subscribeNext:^(resultObject *object) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        
        if (object.status) {
            id ddd = object.data;
            if (![ddd isKindOfClass:[NSDictionary class]]) {
                return ;
            }
            BusinessRegisterParser *parser = [BusinessRegisterParser mj_objectWithKeyValues:ddd];
            self.isRefued = NO;
            if ([parser.approve_supply integerValue] == 1 ) {
                
                self.navigationItem.title = [NSString stringWithFormat:@"%@(%@)",Localized(@"委托商申请"),Localized(@"审核通过")];
                self.shouldShowEmptyView = YES;
                self.dataSource = [NSMutableArray array];
                footerView.backgroundColor = [UIColor groupTableViewBackgroundColor];

                sureButton.hidden = YES;
            }else if ([parser.approve_supply integerValue] == 0){
                self.navigationItem.title =[NSString stringWithFormat:@"%@(%@)",Localized(@"委托商申请"),Localized(Localized(@"shenhezhong"))];
                [sureButton setTitle:Localized(@"已提交") forState:UIControlStateNormal];
                self.isRefued = NO;
                self.shouldShowEmptyView = NO;
                footerView.backgroundColor = [UIColor whiteColor];

                sureButton.enabled = NO;
                [sureButton setBackgroundColor:mainGrayColor];
                if ([parser.is_enter_pay integerValue] == 0) {
                    self.navigationItem.title = [NSString stringWithFormat:@"%@(%@)",Localized(@"委托商申请"),Localized(@"待支付")];
                    [sureButton setTitle:Localized(@"去支付") forState:UIControlStateNormal];
                    sureButton.enabled = YES;
                    [sureButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
                }
            }else if ([parser.approve_supply integerValue] == 2){
                self.navigationItem.title =  [NSString stringWithFormat:@"%@(%@)",Localized(@"委托商申请"),Localized(@"已拒绝")];
                self.shouldShowEmptyView = NO;
                self.isRefued = YES;
                [sureButton setTitle:Localized(@"重新提交") forState:UIControlStateNormal];
                sureButton.enabled = YES;
                [sureButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
            }
            
            [MBProgressHUD showMessage:object.data[@"desc"] view:self.view hideTime:1 doSomeThing:nil];
            
            if ([object.data[@"approve_supply"] integerValue] >=0 ) {
                self.businessParser = parser;
                self.navigationItem.title = [NSString stringWithFormat:@"%@(%@)",Localized(@"委托商申请"),self.businessParser.approve_desc];
            } else {
                self.navigationItem.title = Localized(@"委托商申请");

            }
            
            self.truenameLabel.text = self.businessParser.truename;
            self.card_noLabel.text = self.businessParser.card_no;
            
            [self.tableView reloadData];
            
            
        } else {
            [XSTool showToastWithView:self.view Text:object.info];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    

}

- (void)merchantPay:(NSString *)order_id {
    
    NSString *titleStr = [NSString stringWithFormat:@"%@:%@%@",Localized(@"确认支付委托商入驻费"),
                          [AppConfigManager ShardInstnce].appConfig.enter_pay_number,[AppConfigManager ShardInstnce].appConfig.enter_pay_coin_type];
    [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:titleStr message:nil CallBackBlock:^(NSInteger btnIndex) {
        if (btnIndex == 1) {
            
            [JYSAFNetworking getOrPostWithType:POST withUrl:CTCMerchantPay params:@{@"order_id":order_id} HUDShowView:self.view HUDAnimated:YES success:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    // 添加成功
                    [XSTool showToastWithView:self.view Text:@"支付成功！"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
            } fail:^(NSError *error) {
                [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
            } showHUD:YES];
            
        }
    } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
    
}

- (void) upInfor {
    if (self.businessParser && [self.businessParser.approve_supply integerValue] == 0 && [self.businessParser.is_enter_pay integerValue] == 0) {
        // 继续支付
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager supplyCreateEnterOrderSuccess:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
//                OrderPayViewController *order= [OrderPayViewController new];
//                order.orderId = dic[@"data"][@"order_id"];
//                order.tableName = dic[@"data"][@"table_name"];
//                order.isSupply = YES;
//                [self.navigationController pushViewController:order animated:YES];
                [self merchantPay:dic[@"data"][@"order_id"]];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            Alert(NetFailure);
        }];
        return;
    }
    imageArr = [NSMutableArray array];
    [[RACSignal combineLatest:@[self.truenameLabel.rac_textSignal,
                                self.card_noLabel.rac_textSignal,
//                                self.phone.rac_textSignal,
//                                self.addressInfor.rac_textSignal,
//                                self.weixinAccount.rac_textSignal,
//                                self.inputInfor.rac_textSignal
                                ]] subscribeNext:^(RACTuple * x) {
        self.upInforModel.truename = x.first;
        self.upInforModel.card_no = x.second;
//        self.upInforModel.mobile = x.third;
//        self.upInforModel.license_address = x.fourth;
//        self.upInforModel.weixin = x.fifth;
//        self.upInforModel.remark = x.last;
     }];
    
    if (!self.img_card1.image||!self.img_card2.image||!self.img_card3.image) {
        [MBProgressHUD showMessage:@"请完善资料" view:self.view];
        return;
    }
    
    [imageArr addObject:self.img_card1.image];
    [imageArr addObject:self.img_card2.image];
    [imageArr addObject:self.img_card3.image];
    
    if (imageArr.count < 3) {
        [MBProgressHUD showMessage:@"请完善资料" view:self.view];
        return;
    }
    if (isEmptyString(self.upInforModel.truename) || isEmptyString(self.upInforModel.card_no)) {

        [MBProgressHUD showMessage:@"请完善资料" view:self.view];
        return;
    }
 
    
    
    
    

    [XSTool showProgressHUDTOView:self.view withText:@"上传中..."];
    
    self.index = 0;
    self.fileImgStr=[NSMutableString string];
    
    [self supplyUpLoadWithImage];
    
    
}
- (void) supplyUpLoadWithImage
{
    if (imageArr.count < 3) {
        return;
    }
    XSLog(@"%ld",self.index);
    @weakify(self);
    NSDictionary *param=@{@"type":@"approvesupply",@"formname":@"file"};
    [HTTPManager JYSupLoadDataIsVideo:NO WithDic:param andData:imageArr[self.index]  WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
        @strongify(self);
        if (state) {
            NSString *file=dic[@"data"];
//            @weakify(self);
            if (self.index <2) {
                self.index++;
                [self.fileImgStr appendString:[NSString stringWithFormat:@"%@,",file]];
                [self supplyUpLoadWithImage];
            }else{
                [self.fileImgStr appendString:[NSString stringWithFormat:@"%@",file]];
                self.upInforModel.img_card = self.fileImgStr;
                NSDictionary *ddd = self.listModel.mj_keyValues;
                NSMutableDictionary *ppp = self.upInforModel.mj_keyValues;
                [ddd enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                    if (![key isEqualToString:@"component_pdetail"]) {
                        
                        [ppp setObject:obj forKey:key];
                    }
                }];
                [self supplyApprove:ppp];
            }
        }else{
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
- (void) supplyApprove:(NSDictionary *)params {
    
    ///api/v1/supply/supply/SupplyApprove
    [[XSHTTPManager rac_POSTURL:@"cuser/supply/SupplyApprove" params:params] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            //            [MBProgressHUD showMessage:object.info view:self.view hideTime:1 doSomeThing:^{
            //                [self.navigationController popViewControllerAnimated:YES];
            //            }];
            NSString *ord = object.data[@"order_id"];
            
            if (isEmptyString(ord) || ![AppConfigManager ShardInstnce].appConfig.supply_enter_pay) {
                Alert(object.info);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.80 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
//                OrderPayViewController *order= [OrderPayViewController new];
//                order.orderId = ord;
//                order.tableName = object.data[@"table_name"];
//                order.isSupply = YES;
//                [self.navigationController pushViewController:order animated:YES];
                [self merchantPay:ord];
            }
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
 
    if ([textField isEqual:self.addressLabel]) {
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= 1 && indexPath.section <= 3) {
        //已通过和审核中
        if (self.businessParser && (self.businessParser.approve_supply.intValue == 1 ||self.businessParser.approve_supply.intValue == 0)) {
            return;
        }
        selectedIndex = indexPath.section;
        [self choosePhotoes];
    }
}


- (void)getImageWithController:(UIImage *)image {
    
    if (selectedIndex == 1) {
        self.img_1 = image;
        self.img_card1.image = image;
    }else if(selectedIndex == 2){
        self.img_2 = image;
        self.img_card2.image = image;
    }else if(selectedIndex == 3){
        self.img_3 = image;
        self.img_card3.image = image;
    }
//    [self.tableView reloadData];
    
}

-(UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [BaseTool labelWithTitle:Localized(@"business_approve_suggest_detail") textAlignment:0 font:[UIFont systemFontOfSize:13] titleColor:[UIColor hexFloatColor:@"999999"]];
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}


-(UILabel *)reasonLabel {
    if (!_reasonLabel) {
        _reasonLabel = [BaseTool labelWithTitle:Localized(@"fail_reson") textAlignment:0 font:[UIFont systemFontOfSize:13] titleColor:[UIColor hexFloatColor:@"F95453"]];
        _reasonLabel.backgroundColor = Color(@"FFF2F2");
        _reasonLabel.numberOfLines = 2;
    }
    return _reasonLabel;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
        return [(NSArray *)self.dataSource[section] count];
    
 }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *ss = [NSString stringWithFormat:@"cell%ld",indexPath.section];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ss];
 
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ss];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];

        NSDictionary *dic = self.dataSource[indexPath.section][indexPath.row];
         if (indexPath.section == 0 ||indexPath.section == 4) {
             UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, cell.mj_h, mainWidth, .5)];
             line.backgroundColor = MAIN_MAIN_LINE_COLOR;
             [cell addSubview:line];
             UITextField *textField;
            textField = [UITextField new];
             textField.delegate = self;
            textField.textAlignment = NSTextAlignmentRight;
            textField.textColor = JYSMainTextColor;
            textField.font = [UIFont systemFontOfSize:14];
            [cell addSubview:textField];
            [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell).offset(-12);
                make.centerY.mas_equalTo(cell);
                make.left.mas_equalTo(cell.mas_centerX).offset(-100);
                make.height.mas_equalTo(44);
            }];
            
            textField.placeholder = dic[@"placeholder"];
            cell.textLabel.text = dic[@"title"];
            [textField setValue:TRURE_NAME forKeyPath:@"_placeholderLabel.textColor"];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.textColor = JYSMainTextColor;
            
 
             if (indexPath.row == 1 && indexPath.section ==4 ) {
                 
                 UILabel *tapLabel = [BaseTool labelWithTitle:@"       " textAlignment:0 font:nil titleColor:nil];
                 tapLabel.userInteractionEnabled = YES;
                 tapLabel.backgroundColor = [UIColor clearColor];
                 [cell addSubview:tapLabel];
                 [tapLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                     make.edges.mas_equalTo(textField);
                 }];
                 @weakify(self);
                 [tapLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
                     @strongify(self);
                     ProvinceViewController *vc = [ProvinceViewController new];
                     [self.navigationController pushViewController:vc animated:YES];
                 }]];
             }
             
             if (indexPath.section == 0) {
                 switch (indexPath.row) {
                     case 0:self.truenameLabel = textField;break;
                     case 1:self.card_noLabel = textField;break;
                   }
                 
                 self.truenameLabel.enabled = YES;
                 self.card_noLabel.enabled = YES;
                 //已通过和审核中
                 if (self.businessParser && (self.businessParser.approve_supply.intValue == 1 ||self.businessParser.approve_supply.intValue == 0)) {
                     self.truenameLabel.enabled = NO;
                     self.card_noLabel.enabled = NO;
                 }
             }else{
                 switch (indexPath.row) {
                     case 0:self.phone = textField;break;
                     case 1:self.addressLabel = textField;break;
                     case 2:self.addressInfor = textField;break;
                     case 3:self.weixinAccount = textField;break;
                 }
                 self.phone.enabled = YES;
                 //已通过和审核中
                 if (self.businessParser && (self.businessParser.approve_supply.intValue == 1 ||self.businessParser.approve_supply.intValue == 0)) {
                     self.phone.enabled = NO;
                 }
             }
             
         } else if(indexPath.section == 5){
             
             _inputInfor = [UITextView new];
             _inputInfor.backgroundColor = [UIColor whiteColor];
             _inputInfor.delegate = self;
             _inputInfor.text = Localized(@"input_note");
             _inputInfor.textColor = [UIColor hexFloatColor:@"999999"];
             _inputInfor.frame = CGRectMake(12, 5, mainWidth- 24, 150-5);
             [cell addSubview:_inputInfor];
             self.inputInfor.editable = YES;
             //已通过和审核中
             if (self.businessParser && (self.businessParser.approve_supply.intValue == 1 ||self.businessParser.approve_supply.intValue == 0)) {
                 self.inputInfor.editable = NO;
             }
             
         }else {
            UIImageView *imageView = [BaseTool imageWithName:dic[@"image"] superView:cell];
             [imageView setUserInteractionEnabled:NO];
             [cell addSubview:imageView];
             [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.centerX.mas_equalTo(cell);
                 make.top.mas_equalTo(28);
                 make.left.mas_equalTo(59);
                 make.right.mas_equalTo(-59);
                 make.height.mas_equalTo(GET_HEIGT(160));
             }];
             
             UILabel *showTitle = [BaseTool labelWithTitle:dic[@"title"] textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:JYSMainTextColor];
             [cell addSubview:showTitle];
             UILabel *showTitle2 = [BaseTool labelWithTitle:dic[@"title1"] textAlignment:0 font:[UIFont systemFontOfSize:11] titleColor:[UIColor hexFloatColor:@"999999"]];
             [cell addSubview:showTitle2];
             [showTitle mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.centerX.mas_equalTo(imageView);
                 make.top.mas_equalTo(imageView.mas_bottom).offset(19);
             }];
             [showTitle2 mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.centerX.mas_equalTo(imageView);
                 make.top.mas_equalTo(showTitle.mas_bottom).offset(13);
             }];
             
             switch (indexPath.section) {
                 case 1:self.img_card1 = imageView;break;
                 case 2:self.img_card2 = imageView;break;
                 case 3:self.img_card3 = imageView;break;
             }
         }
        
     }
    
    if (self.businessParser) {
        if (self.businessParser.approve_supply.integerValue == 0 || self.businessParser.approve_supply.integerValue == 1) {
            
            self.truenameLabel.enabled = NO;
            self.card_noLabel.enabled = NO;
            self.phone.enabled = NO;
            self.inputInfor.editable = NO;
        } else {
            self.truenameLabel.enabled = YES;
            self.card_noLabel.enabled = YES;
            self.phone.enabled = YES;
            self.inputInfor.editable = YES;
        }
        [self.img_card3 yy_setImageWithURL:[NSURL URLWithString:self.businessParser.img_card_sc] placeholder:[UIImage imageNamed:@"jys_mine_IDCard_hand"] options:(YYWebImageOptionProgressiveBlur) completion:nil];
        NSArray *imaccArr = self.businessParser.img_card;
        if (imaccArr.count == 2) {
            [self.img_card1 yy_setImageWithURL:[NSURL URLWithString:imaccArr.firstObject] placeholder:[UIImage imageNamed:@"jys_mine_IDCard_front"]options:(YYWebImageOptionProgressiveBlur) completion:nil];
            [self.img_card2 yy_setImageWithURL:[NSURL URLWithString:imaccArr.lastObject] placeholder:[UIImage imageNamed:@"jys_mine_IDCard_back"] options:(YYWebImageOptionProgressiveBlur) completion:nil];
        }
        if (self.img_1) {
            self.img_card1.image = self.img_1;
        }if (self.img_2) {
            self.img_card2.image = self.img_2;
        }if (self.img_3) {
            self.img_card3.image = self.img_3;
        }

        self.phone.text = self.businessParser.mobile;
        NSString *addressString = [NSString stringWithFormat:@"%@%@%@",self.businessParser.province,self.businessParser.county,self.businessParser.town];
        self.addressLabel.text = isEmptyString(addressString)?@"":addressString;
        self.addressInfor.text = isEmptyString(self.businessParser.detail)?@"":self.businessParser.detail;
        self.weixinAccount.text = self.businessParser.weixin;
        self.inputInfor.text = self.businessParser.remark;
        if (!isEmptyString(self.businessParser.detail)) {
             self.addressInfor.text = self.businessParser.detail;
        }
       
    }

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section ==0 || indexPath.section ==4){
        return 44;
    }else if(indexPath.section == 5){
 
        return 150;
 
    }else{
        return (GET_HEIGT(160)+78 + 28);
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) return nil;
    
    UIView * view = [BaseTool viewWithColor:BG_COLOR];
    [view addSubview:self.titleLabel];
   
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(44);

    }];
    if (self.isRefued) {
        [view addSubview:self.reasonLabel];
        [self.reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(44);
        }];
    }
    if (self.isRefued) {
        self.reasonLabel.text = [NSString stringWithFormat:@"%@:%@",Localized(@"fail_reson"),self.businessParser.comment];
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.isRefued) {
        if (section == 0) return 44*2;
    }else{
        if (section == 0) return 44;
    }
    return CGFLOAT_MIN;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == 4) return CGFLOAT_MIN;
 return CGFLOAT_MIN;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:Localized(@"input_note")]) {
        textView.text = @"";
        textView.textColor = JYSMainTextColor;
    }
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (isEmptyString(textView.text)) {
        textView.text = Localized(@"input_note");
        textView.textColor = [UIColor hexFloatColor:@"999999"];
    }else{
        textView.textColor = JYSMainTextColor;
    }
}


#pragma mark 无数据界面
- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {

    return self.shouldShowEmptyView;
}
//图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"supply_review_success"];
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *title = Localized(@"user_approve_process_over");
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName:[UIColor blackColor]
                                 };
    return [[NSAttributedString alloc] initWithString:title attributes:attributes];
}

//详情描述
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = Localized(@"恭喜您，委托商申请审核通过");
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


@end
