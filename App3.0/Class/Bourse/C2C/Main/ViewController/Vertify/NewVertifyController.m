//
//  NewVertifyController.m
//  BIT
//
//  Created by Sunny on 2018/3/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "NewVertifyController.h"
#import "VertifyView.h"
#import "JYSChoosePaperworkView.h"
#import "RealNameVerificationModel.h"
#import "JYSRealNameVerificationModel.h"

@interface NewVertifyController ()

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UITextField *name;
@property (nonatomic,strong) UITextField *paperworkTypeTF;//证件类型
@property (nonatomic,strong) UITextField *identityCard;

@property (nonatomic,strong) UIImageView *img_card1;
@property (nonatomic,strong) UIImageView *img_card2;

/** pickerView */
@property (nonatomic, strong) JYSChoosePaperworkView * paperworkPickerView;

/** paperwork */
@property (nonatomic, copy) NSArray * paperworkArray;
/**
 - 1 提交申请 0 审核中 1 已认证 2 失败
 */
@property (nonatomic, assign) NSInteger apptove; // 认证状态
@end

@implementation NewVertifyController
{
    NSInteger selectedIndex;
    NSMutableArray *imageArr;
//    VertifyView *topView;
    UIButton *sureButton;
    UIView *footerView;
}

- (JYSChoosePaperworkView *)paperworkPickerView {
    if (_paperworkPickerView == nil) {
        _paperworkPickerView = [[JYSChoosePaperworkView alloc] init];
    }
    return _paperworkPickerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationItem.title = Localized(@"user_approve_suggest_no_btn");
    
    self.paperworkArray = @[@{@"name":@"身份证",@"ID":@"1"},@{@"name":@"护照",@"ID":@"2"},@{@"name":@"驾驶证",@"ID":@"3"}];
    
    imageArr = [NSMutableArray arrayWithObjects:@"",@"", nil];
    NSArray *arr = @[
                     @[@{@"title":Localized(@"user_approve_name"),
                         @"placeholder":Localized(@"user_approve_name_hint")
                         },
                       @{@"title":Localized(@"card_type"),
                         @"placeholder":@"请选择证件类型"
                         },
                       @{@"title":@"证件号码",
                         @"placeholder":@"请输入证件号码"
                         }],
                     @[@{@"title":Localized(@"user_approve_card_front"),@"title1":Localized(@"上传图片大小控制在3M以内！"),@"image":@"jys_mine_IDCard_front"}],
                     @[@{@"title":Localized(@"user_approve_card_back"),@"title1":Localized(@"上传图片大小控制在3M以内！"),@"image":@"jys_mine_IDCard_back"}]
                     ];
    self.dataSource = [NSMutableArray arrayWithArray:arr];
//    topView = [VertifyView loadVertifyView];
//    topView.backgroundColor = [UIColor whiteColor];
//    UIView *headerView = [BaseTool viewWithColor:[UIColor whiteColor]];
//    headerView.frame = CGRectMake(0, 0, mainWidth, GET_HEIGT(108));
//    topView.frame = headerView.bounds;
//    [headerView addSubview:topView];
//    self.tableView.tableHeaderView = headerView;
    footerView=  [BaseTool viewWithColor:[UIColor clearColor]];
    footerView.frame = CGRectMake(0, 0, mainWidth, 93);
    self.tableView.tableFooterView = footerView;
 
    [self.view addSubview:self.paperworkPickerView];
    [self.paperworkPickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(FontNum(260));
        make.bottom.mas_equalTo(FontNum(260));
    }];
    
    __weak typeof(self) weakSelf = self;
    [self.paperworkPickerView setPaperworks:self.paperworkArray determineBlock:^(RealNameVerCardTypeModel *cardTypeModel) {
        XSLog(@"%@",cardTypeModel.card_name);
        weakSelf.paperworkTypeTF.text = cardTypeModel.card_name;
        
        [weakSelf dismissPaperworkPickerView];
    } cancelBlock:^{
        [weakSelf dismissPaperworkPickerView];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getInfo];
}

- (void) setFooterViewWithType:(NSInteger )type {
 
    for (UIView *backView in footerView.subviews) {
        [backView removeFromSuperview];
    }
    
    if (type == -1 || type == 0) {
        footerView.frame = CGRectMake(0, 0, mainWidth, 93);
        sureButton = [BaseTool buttonTitle:Localized(@"bug_submit_do") image:nil superView:footerView];
        [sureButton setBackgroundColor:MY_ORDER_SELECTED_COLOR];
        sureButton.layer.cornerRadius = 25;
        sureButton.layer.masksToBounds = YES;
        
        [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(22, 25, 22, 25));
        }];
        [sureButton addTarget:self action:@selector(upInfor) forControlEvents:UIControlEventTouchUpInside];
    }else if (type == 1) {
        footerView.frame = CGRectMake(0, 0, mainWidth, 300);
          UIImageView *imageV = [BaseTool imageWithName:@"ver_passImage" superView:footerView];
         [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(32);
            make.centerX.mas_equalTo(footerView);
         }];
        
        UILabel *infor = [BaseTool labelWithTitle:@"认证成功!" textAlignment:0 font:[UIFont systemFontOfSize:16] titleColor:[UIColor hexFloatColor:@"666666"]];
        [footerView addSubview:infor];
        [infor mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(footerView);
            make.top.mas_equalTo(imageV.mas_bottom).offset(19);
        }];
    }else {
        footerView.frame = CGRectMake(0, 0, mainWidth, 300);
        
        UIButton *infor = [BaseTool buttonTitle:Localized(@"indentify_fail") image:@"ver_failedImage" superView:footerView];
        [infor setImagePosition:(LXMImagePositionTop) spacing:5];
        [infor mas_makeConstraints:^(MASConstraintMaker *make) {
           make.centerX.mas_equalTo(footerView);
            make.top.mas_equalTo(52);
        }];
        
        UIButton *reUpInfor = [BaseTool buttonWithTitle:@"重新认证" titleColor:[UIColor hexFloatColor:@"666666"] font:[UIFont systemFontOfSize:14] superView:footerView];
         reUpInfor.layer.cornerRadius= 20;
        reUpInfor.layer.masksToBounds = YES;
        reUpInfor.backgroundColor =mainColor;
        [reUpInfor mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(footerView);
            make.top.mas_equalTo(infor.mas_bottom).offset(20);
            make.size.mas_equalTo(CGSizeMake(140, 40));
        }];
        @weakify(self);
        [reUpInfor setBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            @strongify(self);
            self.apptove = -1;
//            topView.showProgress = self.apptove;
            [self setFooterViewWithType:self.apptove];
            [self.tableView reloadData];
        }];
    }
    
 }



- (void) upInfor {
    
    UserApproveModel *model = [UserApproveModel new];
    
    //提交认证
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *nameString = [[NSString alloc]initWithString:[self.name.text stringByTrimmingCharactersInSet:whiteSpace]];
    NSString *numberString = [[NSString alloc]initWithString:[self.identityCard.text stringByTrimmingCharactersInSet:whiteSpace]];
    model.trueName = nameString;
    model.card = numberString;
    [imageArr replaceObjectAtIndex:0 withObject:self.img_card1.image];
    [imageArr replaceObjectAtIndex:1 withObject:self.img_card2.image];
    
    if (isEmptyString(model.trueName) || isEmptyString(model.card) || imageArr.count !=2) {
        [MBProgressHUD showMessage:@"请完善信息" view:self.view];
        return;
    }
    NSDictionary *param=@{@"type":@"idcard",@"formname":@"file"};
    NSMutableString *tempImageString=[NSMutableString string];
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager upLoadPhotosWithDic:param andDataArray:imageArr WithSuccess:^(NSDictionary *dic, BOOL state) {
        if([dic[@"status"] integerValue]==1){
            for (NSString *str in dic[@"data"]) {
                [tempImageString appendString:[NSString stringWithFormat:@"%@,",str]];
            }
            NSString  *submitImageString = [tempImageString substringToIndex:tempImageString.length-1];
            
            [HTTPManager submitIdentityCarInfoWithImageString:submitImageString trueName:nameString card:numberString  Success:^(NSDictionary * _Nullable dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];;
                if (state.status) {
                    [XSTool showToastWithView:self.view Text:state.info];
                    [self performSelector:@selector(popViewController) withObject:self afterDelay:0.5];
                    
                } else {
                    [XSTool showToastWithView:self.view Text:state.info];
                }
                
            } failure:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
                
            }];
            
        }else{
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
 
    
}

- (void) popViewController {

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setApptove:(NSInteger)apptove {
    _apptove = apptove;
    if (self.apptove==0 ) {
        for (UIView *view in self.tableView.subviews) {
            view.userInteractionEnabled = NO;
        }
    }
}

- (void) getInfo {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
      [[XSHTTPManager rac_POSTURL:@"/api/v1/user/user/GetUserInfo" params:nil] subscribeNext:^(resultObject *state) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {
            UserParser *parser = [UserParser mj_objectWithKeyValues:state];
            self.apptove = [parser.data.approve_user intValue];
             if (self.apptove!=-1) {
                self.name.text = parser.data.truename;
                self.identityCard.text = parser.data.card_no;
                NSArray *array = [parser.data.img_card componentsSeparatedByString:@","];
                if (array.count == 2) {
                    [self.img_card1 yy_setImageWithURL:[NSURL URLWithString:array.firstObject] placeholder:[UIImage imageNamed:@"jys_mine_IDCard_front"]];
                    [self.img_card2 yy_setImageWithURL:[NSURL URLWithString:array.lastObject] placeholder:[UIImage imageNamed:@"jys_mine_IDCard_back"]];
                }
                 [self.tableView reloadData];

             }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } error:^(NSError * _Nullable error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [XSTool showProgressHUDTOView:self.view withText:NetFailure];
    }completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
//            topView.showProgress = self.apptove;
           [self setFooterViewWithType:self.apptove];
            [self.tableView reloadData];
        });
        
    }];
    
    
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0) {
        
        selectedIndex = indexPath.section;
        [self choosePhotoes];
    }
}


- (void)getImageWithController:(UIImage *)image {
    
    if (selectedIndex == 1) {
        self.img_card1.image = image;
    }else{
        self.img_card2.image = image;
    }
    [self.tableView reloadData];
 
}

-(UILabel *)titleLabel {
    if (!_titleLabel) {
         _titleLabel = [BaseTool labelWithTitle:Localized(@"说明：实名认证后可通过认证信息进行账号找回，保证资金安全！") textAlignment:0 font:[UIFont systemFontOfSize:11] titleColor:[UIColor hexFloatColor:@"999999"]];
     }
    return _titleLabel;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.apptove >= 1 ? 0 : [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray *)self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *ss = [NSString stringWithFormat:@"cell%ld",indexPath.section];

    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:ss];
    UITextField *textField;
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ss];
         cell.backgroundColor = [UIColor whiteColor];
        switch (indexPath.section) {
            case 0:{
                textField = [UITextField new];
                textField.textAlignment = NSTextAlignmentRight;
                textField.textColor = JYSMainTextColor;
                textField.font = [UIFont systemFontOfSize:14];
                [cell addSubview:textField];
                [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(cell).offset(-12);
                    make.centerY.mas_equalTo(cell);
                    make.left.mas_equalTo(cell.mas_centerX);
                }];
                
                NSDictionary *dic = self.dataSource[indexPath.section][indexPath.row];
                textField.placeholder = dic[@"placeholder"];
                cell.textLabel.text = dic[@"title"];
                [textField setValue:TRURE_NAME forKeyPath:@"_placeholderLabel.textColor"];
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                cell.textLabel.textColor = JYSMainTextColor;
//                if (indexPath.row == 1 ) {
//                    textField.keyboardType = UIKeyboardTypeNumberPad;
//                }
                switch (indexPath.row) {
                    case 0:self.name = textField;break;
                    case 1:
                    {
                        self.paperworkTypeTF = textField;
                        
                        UIButton * choosePaperworkBtn = [XSUITool creatButtonWithFrame:CGRectZero buttonType:UIButtonTypeCustom Target:self action:@selector(choosePaperworkButtonClicked)];
                        [cell addSubview:choosePaperworkBtn];
                        [choosePaperworkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.left.right.top.bottom.mas_equalTo(textField);
                        }];
                        
                    }
                        break;
                    case 2:self.identityCard = textField;break;
 
                }
            }
                 break;
                
            case 1:
             case 2:{
 
                NSDictionary *dic= self.dataSource[indexPath.section][indexPath.row];
                UIImageView *imageView = [BaseTool imageWithName:dic[@"image"] superView:cell];
                [cell addSubview:imageView];
                [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(cell);
                    make.top.mas_equalTo(28);
                    make.left.mas_equalTo(59);
                    make.right.mas_equalTo(-59);
                    make.height.mas_equalTo(GET_HEIGT(160));
                }];
 
                UILabel *showTitle = [BaseTool labelWithTitle:dic[@"title"] textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:JYSMainTextColor];
                 [cell addSubview:showTitle];
                UILabel *showTitle2 = [BaseTool labelWithTitle:dic[@"title1"] textAlignment:0 font:[UIFont systemFontOfSize:11] titleColor:[UIColor hexFloatColor:@"999999"]];
                 [cell addSubview:showTitle2];
                 [showTitle mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(imageView);
                    make.top.mas_equalTo(imageView.mas_bottom).offset(19);
                }];
                 [showTitle2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(imageView);
                    make.top.mas_equalTo(showTitle.mas_bottom).offset(13);
                }];
                 
                 if (indexPath.section == 1) {
                     self.img_card1 = imageView;
                 }else{
                     self.img_card2 = imageView;
                 }
                 UIImageView *shade;
                  if (self.apptove == 1) {
                     shade = [BaseTool imageWithName:@"ver_zz" superView:imageView];
                     shade.alpha = .8;
                     [shade mas_makeConstraints:^(MASConstraintMaker *make) {
                         make.edges.mas_equalTo(imageView);
                     }];
                  }else{
                      [shade removeFromSuperview];
                  }
                 
                
            }
                break;
         }

    }
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    return indexPath.section ==0 ?44 : (GET_HEIGT(160)+78 + 28);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) return nil;
    
    UIView * view = [BaseTool viewWithColor:main_BackColor];
    [view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(view);
        make.left.mas_equalTo(12);
    }];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section != 0) return 0;
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 12;
}

- (void)choosePaperworkButtonClicked {
    XSLog(@"选择证件类型");
    [self showPaperworkPickerView];
}

- (void)showPaperworkPickerView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.paperworkPickerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.view).offset(0);
        }];
        [self.view layoutIfNeeded];
    }];
}

- (void)dismissPaperworkPickerView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.paperworkPickerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.view).offset(FontNum(260));
        }];
        [self.view layoutIfNeeded];
    }];
}


@end
