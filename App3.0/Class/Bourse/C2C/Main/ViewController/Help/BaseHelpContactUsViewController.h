//
//  BaseHelpContactUsViewController.h
//  BIT
//
//  Created by nilin on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseHelpContactUsViewController : XSBaseTablViewController

/** 订单编号 */
@property (nonatomic, copy) NSString * orderNumber;

@end
