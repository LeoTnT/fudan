//
//  BaseHelpProblemViewController.m
//  BIT
//
//  Created by nilin on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseHelpProblemViewController.h"
#import "HelpCenterModel.h"
#import "BaseHelpProblemDetailViewController.h"
#import "XSShoppingWebViewController.h"

@interface BaseHelpProblemViewController ()

@end

@implementation BaseHelpProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.dataSource = [NSMutableArray array];
     self.tableView.frame = self.view.bounds;
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
//    [self setupRefesh];
    [self loadRefreshData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadRefreshData {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (self.isList) {
        [[XSHTTPManager rac_POSTURL:@"basic/help/Lists" params:nil]subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (self.page == 1) {
                [self.dataSource removeAllObjects];
            }
            if (object.status) {
                HelpProblemListParser *parser =  [HelpProblemListParser mj_objectWithKeyValues:object.data];
                NSArray *arr = parser.data;
                [self.dataSource addObjectsFromArray:arr];
             }else{
                [MBProgressHUD showMessage:object.info view:self.view];
            }
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        } completed:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self endRefresh];
            });
        }];

    }else{
        
        [[XSHTTPManager rac_POSTURL:@"basic/help/Lists" params:nil]subscribeNext:^(resultObject *object) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (self.page == 1) {
                [self.dataSource removeAllObjects];
            }
            if (object.status) {
                HelpProblemListParser *parser =  [HelpProblemListParser mj_objectWithKeyValues:object.data];
                NSArray *arr = parser.data;
                [self.dataSource addObjectsFromArray:arr];
                //            [self.tableView reloadData];
            }else{
                [MBProgressHUD showMessage:object.info view:self.view];
            }
        } error:^(NSError * _Nullable error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showMessage:NetFailure view:self.view];
        } completed:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self endRefresh];
            });
        }];
    }
    
    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     HelpProblemListDetailParser  *model = self.dataSource[indexPath.row];
    BaseHelpProblemDetailViewController *controller = [[BaseHelpProblemDetailViewController alloc] init];
    controller.ID = model.ID;
    controller.contentString = model.content;
    controller.navi_title = model.title;
    [self.navigationController pushViewController:controller animated:YES];
    
//    if (self.dataSource.count) {
//        HelpProblemListDetailParser * model = self.dataSource[indexPath.row];
//
//        if (!isEmptyString(model.url)) {
//            NSString * urlString;
//            if ([model.url hasPrefix:@"http"]) {
//                urlString = model.url;
//            } else {
//                urlString = [JYSBaseURL stringByAppendingString:model.url];
//            }
//
//            XSShoppingWebViewController * shoppingWebVC = [[XSShoppingWebViewController alloc] init];
//            NSString *gui = [XSTool getStrUseKey:APPAUTH_KEY];
//            shoppingWebVC.urlString = [NSString stringWithFormat:@"%@?device=%@",urlString,gui];
//            shoppingWebVC.titleString = model.title;
//            [self.navigationController pushViewController:shoppingWebVC animated:YES];
//        }
//    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //    cell.backgroundColor = main_BackColor;
    
//    cell.lineLabel.frame = CGRectMake(0, GET_HEIGT(70),mainWidth, 0.5);
   HelpProblemListDetailParser  *model = self.dataSource[indexPath.row];

//    UILabel *titleLable = [UILabel ]
    [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell);
        make.left.mas_equalTo(cell).offset(16);
    }];

    cell.titleName.text = model.title;
    cell.titleName.textColor = JYSMainTextColor;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return GET_HEIGT(50);
}
@end
