//
//  BaseHelpContactUsViewController.m
//  BIT
//
//  Created by nilin on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseHelpContactUsViewController.h"

@interface BaseHelpContactUsViewController ()<UITextFieldDelegate,UITextViewDelegate>
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic, strong) UITextField *mobileText;
@property (nonatomic, strong) UITextField *orderNumText;
@property (nonatomic, strong) UITextView *contentText;
@property (nonatomic, strong) NSString *type;
@end

@implementation BaseHelpContactUsViewController

-(UITextField *)mobileText {
    if (!_mobileText) {
        _mobileText = [[UITextField alloc] init];
//        _mobileText.textColor = [UIColor whiteColor];
        _mobileText.textAlignment = NSTextAlignmentRight;
        _mobileText.delegate = self;
        _mobileText.placeholder = Localized(@"input_mobile");
          [_mobileText setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    }
    return _mobileText;
}

-(UITextField *)orderNumText {
    if (!_orderNumText) {
        _orderNumText = [[UITextField alloc] init];
//        _orderNumText.textColor = [UIColor whiteColor];
        _orderNumText.textAlignment = NSTextAlignmentRight;
        _orderNumText.delegate = self;
        _orderNumText.placeholder = Localized(@"input_order_num");
        [_orderNumText setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    }
    return _orderNumText;
}
-(UITextView *)contentText {
    if (!_contentText) {
        _contentText = [UITextView new];
        _contentText.backgroundColor = [UIColor whiteColor];
        _contentText.text = Localized(@"input_appeal_content");
        _contentText.textColor = COLOR_666666;
        _contentText.font = [UIFont systemFontOfSize:17];
        _contentText.returnKeyType = UIReturnKeyDone;
        _contentText.delegate = self;

    }
    return _contentText;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.type = @"0";
    self.view.backgroundColor = mainColor;
    self.tableView.frame = CGRectMake(0, CGRectGetMinY(self.view.frame), mainWidth, CGRectGetHeight(self.view.frame)-150);
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
//    [self setupRefesh];
//    [self loadRefreshData];
   
    CGFloat height = kStatusBarAndNavigationBarHeight;
    self.submitButton =[[UIButton alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(self.view.frame)-120-height, mainWidth-60, 50)];
    [self.view addSubview:self.submitButton];
    [self.submitButton setTitle:Localized(@"bug_submit_do") forState:UIControlStateNormal];
    [self.submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.submitButton.titleLabel.font=[UIFont systemFontOfSize:16];
    self.submitButton.layer.cornerRadius=22;
    self.submitButton.layer.masksToBounds=YES;
    [self.submitButton setBackgroundColor:[UIColor hexFloatColor:@"EB4554"]];
    [self.submitButton addTarget:self action:@selector(submitInformation) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)submitInformation {
    
    /**
     cappeal/cappeal/Appeal
     
     参数：
     
     type：申诉类型；暂时写死，只有一种订单可申诉（付款了未放行）；传值 type=1 就行
     
     order_num：订单编号；*必须；例：order_num='1804080855195894096'
     
     mobile：联系电话；*必须；例：mobile='13312341234'
     
     content:描述信息备注；*必须；
     */
    
    if (self.type.intValue == 0) {
        [XSTool showToastWithView:self.view Text:Localized(@"请选择申诉类型")];
        return;
    }
    
    if (isEmptyString(self.mobileText.text)) {
        [XSTool showToastWithView:self.view Text:Localized(@"input_mobile")];
        return;
    }
    
    if (isEmptyString(self.orderNumText.text)) {
        [XSTool showToastWithView:self.view Text:Localized(@"input_order_num")];
        return;
    }
    
    if (isEmptyString(self.contentText.text)||[self.contentText.text isEqualToString:Localized(@"input_appeal_content")]) {
        [XSTool showToastWithView:self.view Text:Localized(@"input_appeal_content")];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/cappeal/Appeal" params:@{@"type":self.type,@"mobile":self.mobileText.text,@"order_num":self.orderNumText.text,@"content":self.contentText.text}]subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (object.status) {
            
            [MBProgressHUD showMessage:@"成功提交" view:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } completed:^{
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 2;
    }else if (section==1) {
        return 2;
    } else {
        return 1;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    HelpProblemListDetailParser  *model = self.dataSource[indexPath.row];
//    BaseHelpProblemDetailViewController *controller = [[BaseHelpProblemDetailViewController alloc] init];
//    controller.ID = model.ID;
//    controller.navi_title = model.title;
//    [self.navigationController pushViewController:controller animated:YES];
    
    if (indexPath.section==0) {
        if (indexPath.row == 0) {
            self.type = @"1";
        }else if (indexPath.row == 1) {
            self.type = @"2";
        }
        
         [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    cell.titleName.textColor = [UIColor whiteColor];
    if (indexPath.section==0) {
        //类型
        cell.headerImageView.image = [UIImage imageNamed:@"checkbox_unselected"];
        [cell.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(cell.contentView).offset(6);
            make.width.mas_equalTo(18);
            make.height.mas_equalTo(18);
        }];
        
//        cell.titleName.text = Localized(@"pay_not_getmoney");
        
        if (indexPath.row == 0) {
             cell.titleName.text = Localized(@"对方未放行");
        }else if (indexPath.row == 1) {
            cell.titleName.text = Localized(@"对方未付款");
        }
        
        [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(cell.headerImageView.mas_right).offset(2);
             make.right.mas_equalTo(cell.contentView).offset(-16);
        }];
        if (indexPath.row == 0&&self.type.intValue==1) {
            cell.headerImageView.image = [UIImage imageNamed:@"checkbox_selected"];
        }else if (indexPath.row == 1&&self.type.intValue==2) {
            cell.headerImageView.image = [UIImage imageNamed:@"checkbox_selected"];
        }
        

    } else if (indexPath.section==1) {
        //两个输入框
        if (indexPath.row==0) {
           cell.titleName.text = Localized(@"phone");
            
            [cell.contentView addSubview:self.mobileText];
            [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.left.mas_equalTo(cell.contentView).offset(16);
               
            }];
            [self.mobileText mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(cell.contentView).offset(-16);
                make.left.mas_equalTo(cell.titleName.mas_right).with.mas_offset(10);
            }];
            self.mobileText.text = [UserInstance ShardInstnce].mobile;
        } else {
            cell.titleName.text = Localized(@"order_no");
            [cell.contentView addSubview:self.orderNumText];
            [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.left.mas_equalTo(cell.contentView).offset(16);
                
            }];
            [self.orderNumText mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(cell.contentView);
                make.right.mas_equalTo(cell.contentView).offset(-16);
                make.left.mas_equalTo(cell.titleName.mas_right).with.mas_offset(10);
            }];
            self.orderNumText.text = self.orderNumber;
        }
        
       

        
    } else {
        //文本输入框
        [cell.contentView addSubview:self.contentText];
        [self.contentText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(cell.contentView).offset(16);
             make.right.mas_equalTo(cell.contentView).offset(-16);
            make.top.bottom.mas_equalTo(cell.contentView);
        }];
        
    }
   
    
    //    cell.backgroundColor = main_BackColor;
    
    //    cell.lineLabel.frame = CGRectMake(0, GET_HEIGT(70),mainWidth, 0.5);
//    HelpProblemListDetailParser  *model = self.dataSource[indexPath.row];
//
//    //    UILabel *titleLable = [UILabel ]
//    [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(cell);
//        make.left.mas_equalTo(cell).offset(16);
//    }];
//
//    cell.titleName.text = model.title;
//    cell.titleName.textColor = TRURE_NAME;
    
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section<2) {
        return GET_HEIGT(50);
    } else {
        return GET_HEIGT(120);
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==0) {
        return 10;
    } else if (section==1) {
        return 40;
    } else {
        return 0.01;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 40;
    } else {
        return 0.01;
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section==1) {
        //描述
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, mainWidth-15*2, 40)];
        label.text = Localized(@"des_message");
//        label.textColor = [UIColor whiteColor];
        return label;
    } else {
        return nil;
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, mainWidth-15*2, 40)];
        label.text = Localized(@"appeal_type");
//        label.textColor = [UIColor whiteColor];
        return label;
    } else {
        return nil;
    }
}

#pragma mark - textview
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
//    textView.text = nil;
    if ([textView.text isEqualToString:Localized(@"input_appeal_content")]) {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (isEmptyString(textView.text)) {
        textView.text = Localized(@"input_appeal_content");
        textView.textColor = COLOR_666666;

    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];

    return YES;
}


@end
