//
//  BaseHelpProblemDetailViewController.h
//  BIT
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseHelpProblemDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *ID;

/** 内容 */
@property (nonatomic, copy) NSString * contentString;

@end
