//
//  BaseHelpProblemDetailViewController.m
//  BIT
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseHelpProblemDetailViewController.h"
#import "HelpCenterModel.h"

@interface BaseHelpProblemDetailViewController ()<UIWebViewDelegate>
@property (nonatomic ,strong) UIWebView *webView;

@end

@implementation BaseHelpProblemDetailViewController

-(UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] init];
        _webView.delegate = self;
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
    if (!isEmptyString(self.contentString)) {
        
        [self.webView loadHTMLString:[headerString stringByAppendingString:self.contentString] baseURL:nil];
        [self.view addSubview:self.webView];
        [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.mas_equalTo(self.view);
        }];
    }
    
//    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"basic/notice/Info" params:@{@"id":self.ID}]subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if (object.status) {
            HelpProblemListDetailParser *parser = [HelpProblemListDetailParser mj_objectWithKeyValues:object.data];
         
            NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
            if (!isEmptyString(parser.content)) {
                
                [self.webView loadHTMLString:[headerString stringByAppendingString:parser.content] baseURL:nil];
                [self.view addSubview:self.webView];
                [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.bottom.left.right.mas_equalTo(self.view);
                }];
            }
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } completed:^{
        
    }];
}



@end
