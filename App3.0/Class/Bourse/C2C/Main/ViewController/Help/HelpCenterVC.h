//
//  HelpCenterVC.h
//  BIT
//
//  Created by nilin on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCenterVC : BaseViewController

/** 订单编号 */
@property (nonatomic, copy) NSString * orderNum;

@property (nonatomic, assign) BOOL isAppeal;//是否展示申诉页面
@end
