//
//  HelpCenterVC.m
//  BIT
//
//  Created by nilin on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "HelpCenterVC.h"
#import "BaseHelpProblemViewController.h"
#import "BaseHelpContactUsViewController.h"
#import "FeedbackViewController.h"

@interface HelpCenterVC ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;

@end

@implementation HelpCenterVC


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    //    [self.navigationController.navigationBar lt_setBackgroundColor:mainColor];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self setupPageView];
    
     self.navi_title = Localized(@"personal_my_help");
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.view.backgroundColor = mainColor;
  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupPageView {
    
    NSArray *titleArr = @[Localized(@"always_see_question"), Localized(@"call_us")];
    
    if (self.isAppeal) {
        titleArr = @[Localized(@"always_see_question"), Localized(@"order_appeal")];
    }
    
  
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = MY_ORDER_NORMAL_COLOR;
    configure.titleSelectedColor = mainColor;
    configure.indicatorColor = mainColor;
    //    configure.indicatorAdditionalWidth = 100; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
    //    CGFloat naviHeight = kDevice_Is_iPhoneX ? 88:64;
    CGFloat naviHeight = 0;
    /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, naviHeight, self.view.frame.size.width, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];
    
    NSMutableArray *chia = [NSMutableArray array];
    [titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (idx==0) {
            BaseHelpProblemViewController *controller = [[BaseHelpProblemViewController alloc] init];
             [chia addObject:controller];
        } else if (idx==1) {
            
            if (self.isAppeal) {
                BaseHelpContactUsViewController *controller = [[BaseHelpContactUsViewController alloc] init];
                controller.orderNumber = self.orderNum;
                [chia addObject:controller];
            }else{
            //走反馈接口
            FeedbackViewController *controller = [[FeedbackViewController alloc] init];
                controller.isAd = YES;
                [chia addObject:controller];
            }
          
        }
    }];
    NSArray *childArr = chia;
    CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame)- self.navi_Height;
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}


@end
