//
//  BaseCreditController.h
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseViewController.h"

@interface TrustModel:NSObject

@property (nonatomic,assign) NSInteger type;

@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger limit;

@end


@interface BaseCreditController : BaseViewController
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentView *pageContentView;
@end
