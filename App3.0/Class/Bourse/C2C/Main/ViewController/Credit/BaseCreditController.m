//
//  BaseCreditController.m
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BaseCreditController.h"
#import "CreditViewController.h"
#import "SGPageTitleView.h"
@interface BaseCreditController ()<SGPageTitleViewDelegate, SGPageContentViewDelegate>


@end

@implementation BaseCreditController
{
    NSArray *titleArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.title = Localized(@"受信任的");
    [self setupPageView];
//    @weakify(self);
//    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
//        @strongify(self);
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
    self.view.backgroundColor = mainColor;
}

- (void)setupPageView {
    
    titleArr = @[Localized(@"受信任的"), Localized(@"信任我的") ];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.titleColor = MY_ORDER_NORMAL_COLOR;
    configure.titleSelectedColor = MY_ORDER_SELECTED_COLOR;
    configure.indicatorColor = MY_ORDER_SELECTED_COLOR;
    
    /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = mainColor;
    self.pageTitleView.isShowBottomSeparator = NO;
    [self.view addSubview:_pageTitleView];

 
    NSMutableArray * childArr = [NSMutableArray array];
    [titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        TrustModel *model = [TrustModel new];
        model.type = idx + 1;
        CreditViewController *oneVC = [[CreditViewController alloc] init];
        oneVC.model = model;
        [childArr addObject:oneVC];
    }];
    
    
    
    
    CGFloat contentViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame);
    self.pageContentView = [[SGPageContentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), self.view.frame.size.width, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentView.delegatePageContentView = self;
    [self.view addSubview:_pageContentView];
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentView setPageContentViewCurrentIndex:selectedIndex];
    self.navi_title = titleArr[selectedIndex];
}

- (void)pageContentView:(SGPageContentView *)pageContentView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
     self.navi_title = titleArr[originalIndex];
}


@end


@implementation TrustModel

- (instancetype)init {
    if (self = [super init]) {
        self.page = 1;
        self.limit = 10;
    }
    return self;
}

@end
