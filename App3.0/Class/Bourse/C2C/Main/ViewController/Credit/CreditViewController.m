//
//  CreditViewController.m
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "CreditViewController.h"
#import "TrustListModel.h"
//#import "SGPageTitleView.h"
#import <SGPagingView/SGPageTitleView.h>

@implementation CreditViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSource = [NSMutableArray array];
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
    [self setupRefesh];
    [self loadRefreshData];
 
}

- (void)loadRefreshData {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"ctrust/ctrust/Trust" params:self.model.mj_keyValues]subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.page == 1) {
            [self.dataSource removeAllObjects];
        }
        if (object.status) {
            NSArray *arr = [TrustListModel mj_objectArrayWithKeyValuesArray:object.data[@"data"]];
            [self.dataSource addObjectsFromArray:arr];
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    } completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRefresh];
            [self.tableView reloadData];
            NSArray *arr = self.dataSource;
            NSString *naviTitle = self.model.type == 1 ? [NSString stringWithFormat:@"%@(%ld)",Localized(@"受信任的"),arr.count]:[NSString stringWithFormat:@"%@(%ld)",Localized(@"信任我的"),arr.count];
            NSLog(@"asasd  =%@",naviTitle);
//            BaseCreditController * vc = (BaseCreditController *)self.navigationController.topViewController;
//            SGPageTitleButton *button = vc.pageTitleView.btnMArr[self.model.type - 1];
//            [button setTitle:naviTitle forState:UIControlStateNormal];

        
        });
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
//    cell.backgroundColor = main_BackColor;

    cell.lineLabel.frame = CGRectMake(0, GET_HEIGT(70),mainWidth, 0.5);
    TrustListModel *model = self.dataSource[indexPath.row];
    [cell.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(GET_HEIGT(16));
        make.size.mas_equalTo(CGSizeMake(GET_HEIGT(38), GET_HEIGT(38)));
    }];
    [cell.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.headerImageView);
        make.left.mas_equalTo(cell.headerImageView.mas_right).offset(16);
    }];
    [cell.headerImageView yy_setImageWithURL:[NSURL URLWithString:model.logo] placeholder:[UIImage imageNamed:@"no_pic"]];
    cell.titleName.text = model.truename;
    cell.titleName.textColor = TRURE_NAME;
    cell.headerImageView.layer.cornerRadius = GET_HEIGT(19);
    cell.headerImageView.layer.masksToBounds = YES;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    return GET_HEIGT(70);
}

@end
