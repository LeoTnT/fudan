//
//  ChouseBankViewController.m
//  BIT
//
//  Created by Sunny on 2018/4/11.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "ChouseBankViewController.h"
#import "BankCardModel.h"

@interface ChouseBankViewController ()
@property (nonatomic, strong) NSMutableArray *typeArray;//银行卡类型数组
@end
@implementation ChouseBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"选择银行卡");
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
    //弹出选择银行类别的警告框
    [HTTPManager getBankTypeSuccess:^(NSDictionary * _Nullable dic, resultObject *state) {
        if (state.status) {
            self.typeArray = [NSMutableArray arrayWithArray:[CardTypeDataParser mj_objectArrayWithKeyValuesArray:dic[@"data"]]];
            [self.tableView reloadData];
        } else {
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
 
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
   return  self.typeArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CardTypeDataParser *model = self.typeArray[indexPath.row];
    cell.textLabel.text = model.name;
    cell.textLabel.textColor = [UIColor whiteColor];
    [cell lineLabel];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CardTypeDataParser *model = self.typeArray[indexPath.row];
    if (self.chouseBankCard) {
        self.chouseBankCard(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
