//
//  WChatViewController.m
//  BIT
//
//  Created by Sunny on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "WChatViewController.h"
#import "RSAEncryptor.h"
#import "XSQR.h"

@interface WChatViewController ()

@property (nonatomic,strong) UITextField *nameTF;
@property (nonatomic,strong) UITextField *accountTF;
@property (nonatomic,strong) UITextField *passWordTF;

@property (nonatomic,strong) UIImageView *codeImageView;
@end

@implementation WChatViewController
{
    NSArray *listArr;
    UIImage *picture;
    NSInteger apptove;
    NSInteger isCheck;
    NSDictionary *dcicc;
    BOOL pictureChanged;
}


//- (void)setModel:(JYSAccountInfoModel *)model {
//    _model = model;
//    NSDictionary *dic = model;
//    id infor = dic.allValues.firstObject;
//    if ([infor isKindOfClass:[NSDictionary class]]) {
//        dcicc = infor;
//        isCheck = [dcicc[dic.allKeys.firstObject] integerValue];
//     }
//
//
//
//}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableViewHeaderView.frame = CGRectMake(0, 0, mainWidth, 44);
    UILabel *headerLabel = [BaseTool labelWithTitle:Localized(@"pay_des") textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"999999"]];
    [self.tableViewHeaderView addSubview:headerLabel];
    [headerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.tableViewHeaderView);
        make.width.mas_equalTo(mainWidth - 24);
        make.left.mas_equalTo(12);
        
    }];
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    CGFloat tabHeight =  self.navi_Height + tabBarHeight;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(mainHeight - tabHeight);
    }];

    NSString *titt = self.type.integerValue == 1 ? Localized(@"alipay_amount"):Localized(@"wechat_amount");
    
    listArr = @[
               @[ @{@"title":Localized(@"name")},
                @{@"title":titt}],
                @[@{@"title":@""}],
                @[@{@"title":Localized(@"pay_pd")}]
                ];
    
    
    
//    self.tableViewFooterView.frame = CGRectMake(0, 0, mainWidth, 50);
    
    UIButton *btn = [BaseTool buttonWithTitle:Localized(@"save") titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] superView:self.tableViewFooterView];
    btn.backgroundColor = mainColor;
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(saveButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.tableView.mas_bottom);
        make.height.mas_equalTo(50);
    }];
//     - 1 提交申请 0 审核中 1 已认证 2 失败
    [[XSHTTPManager rac_POSTURL:@"user/approve/GetApproveInfo" params:nil] subscribeNext:^(resultObject *object) {
        
        if (object.status) {
            
            apptove =  [object.data[@"status"] integerValue];
            
            if (apptove == -1) {
                [MBProgressHUD showMessage:@"请实名认证" view:self.view hideTime:1 doSomeThing:^{
                    
                }];
            }else if (apptove == 0){
                
                [MBProgressHUD showMessage:@"正在实名认证" view:self.view hideTime:1 doSomeThing:^{
                    
                }];
            }else if (apptove == 2){
                [MBProgressHUD showMessage:@"实名认证失败" view:self.view hideTime:1 doSomeThing:^{
                    
                }];
            }else {
                
                NSString *name = object.data[@"truename"];
                if (!isEmptyString(name)) {
                    self.nameTF.text =name;
                    [self.tableView reloadData];
                }
            }
            
        }

    }];
    
 }

- (void) saveButtonAction {
    
    if (apptove != 1) {[MBProgressHUD showMessage:@"请完善实名认证" view:self.view];return;}
    
    if (!picture) {[MBProgressHUD showMessage:@"请选择图片" view:self.view];return;}
    
    if (isEmptyString(self.accountTF.text) || isEmptyString(self.nameTF.text) || isEmptyString(self.passWordTF.text)) {
        [MBProgressHUD showMessage:@"请完善信息" view:self.view];
        return;
    }
    
    if (self.model) {
        if (pictureChanged) {
            NSDictionary *param=@{@"type":@"bankcard",@"formname":@"file"};
            
            [XSTool showProgressHUDWithView:self.view];
            
            NSArray *imageArr = @[picture];
            [HTTPManager upLoadPhotosWithDic:param andDataArray:imageArr WithSuccess:^(NSDictionary *dic, BOOL state) {
                if([dic[@"status"] integerValue]==1){//发表
                    
                    
                    NSString *bank_id = self.bank_id;
                    NSString *imageName = [dic[@"data"] componentsJoinedByString:@","];
                    
                    NSString *paypwd = [RSAEncryptor encryptString:self.passWordTF.text];
                    NSDictionary *upDic = @{
                                            @"id":self.model.ID,
                                          @"bank_id":bank_id,
                                          @"bankcard":self.accountTF.text,
                                          @"truename":self.nameTF.text,
                                          @"paypwd":paypwd,
                                          @"picture":imageName
                                          };
                    
                    [self upInfor:upDic];
                    
                }else{
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * _Nonnull error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:NetFailure];
            }];
        } else {
            NSDictionary *dic = @{
                                  @"id":self.model.ID,
                                  @"bank_id":self.bank_id,
                                  @"bankcard":self.accountTF.text,
                                  @"truename":self.nameTF.text,
                                  @"paypwd":[RSAEncryptor encryptString:self.passWordTF.text],
                                  @"picture":self.model.picture
                                  };
            [self upInfor:dic];
        }
        
        return;
    }
    
    NSDictionary *param=@{@"type":@"bankcard",@"formname":@"file"};
    
    [XSTool showProgressHUDWithView:self.view];
    
    NSArray *imageArr = @[picture];
    [HTTPManager upLoadPhotosWithDic:param andDataArray:imageArr WithSuccess:^(NSDictionary *dic, BOOL state) {
        if([dic[@"status"] integerValue]==1){//发表
            
            
            NSString *bank_id = self.bank_id;
            NSString *imageName = [dic[@"data"] componentsJoinedByString:@","];
            
            NSString *paypwd = [RSAEncryptor encryptString:self.passWordTF.text];
            NSDictionary *dic = @{
                        @"bank_id":bank_id,
                        @"bankcard":self.accountTF.text,
                        @"truename":self.nameTF.text,
                        @"paypwd":paypwd,
                        @"picture":imageName
                        };
 
            [self upInfor:dic];
            
        }else{
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];

    
    
}


- (void) upInfor:(NSDictionary *)dic {

    
    [[XSHTTPManager rac_POSTURL:@"user/bank/Insert" params:dic] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            [MBProgressHUD showMessage:Localized(@"my_skill_sava_success") view:self.view hideTime:1.5 doSomeThing:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
        }else{
            [MBProgressHUD showMessage:object.info view:self.view];
        }
        
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray *)listArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    UITextField *textField;
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
 
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        
        if (indexPath.section != 1) {
            cell.lineLabel.frame = CGRectMake(0, 49, mainWidth, 0.5);
            NSDictionary *dic = listArr[indexPath.section][indexPath.row];
            cell.textLabel.text = dic[@"title"];
            textField = [UITextField new];
            textField.placeholder = Localized(@"请输入");
            textField.font = [UIFont systemFontOfSize:15];
            textField.textAlignment = NSTextAlignmentRight;
            [textField setValue:[UIColor hexFloatColor:@"999999"] forKeyPath:@"_placeholderLabel.textColor"];
 
            textField.frame = CGRectMake(0, 0, mainWidth/2, 30);
            cell.accessoryView = textField;
            
            switch (indexPath.section) {
                case 0:
                {
                    if (indexPath.row == 0) {
                        self.nameTF = textField;
                    }else{
                        self.accountTF = textField;
                    }
                }
                    break;
                case 2:{
                    textField.secureTextEntry = YES;
                    self.passWordTF = textField;
                }
                    break;
            }
        }
        
        
        if (indexPath.section ==1) {
            
            [cell addSubview:self.codeImageView];
             [self.codeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(cell);
                make.size.mas_equalTo(CGSizeMake(GET_HEIGT(155), GET_HEIGT(155)));
                make.top.mas_equalTo(16);
            }];
               NSString *titt = self.type.integerValue == 1 ? Localized(@"alipay_ma"):Localized(@"wechat_ma");
            UILabel *label = [BaseTool labelWithTitle:titt textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:[UIColor blackColor]];
            [cell addSubview:label];
             [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.codeImageView);
                make.top.mas_equalTo(self.codeImageView.mas_bottom).offset(17);
            }];
            UILabel *label1 = [BaseTool labelWithTitle:Localized(@"upload_suport") textAlignment:0 font:[UIFont systemFontOfSize:12] titleColor:[UIColor hexFloatColor:@"999999"]];
            [cell addSubview:label1];
            [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.codeImageView);
                make.top.mas_equalTo(label.mas_bottom).offset(9);
            }];
          }
     }
    
    if (self.model) {

        self.nameTF.text = self.model.bankuser;
        self.accountTF.text = self.model.bankcard;
//        self.codeImageView.image = [XSQR createQrImageWithContentString:self.model.picture type:XSQRTypeSupply];
        [self.codeImageView getImageWithUrlStr:self.model.picture andDefaultImage:[UIImage imageNamed:@"bingding_code"]];
        picture = self.codeImageView.image;
        
    }
      return cell;
}

- (void)getImageWithController:(UIImage *)image {
    picture = image;
    self.codeImageView.image = image;
    pictureChanged = YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        
        [self choosePhotoes];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.11;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section != 0) {
         return 11;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return GET_HEIGT(155) + 69 +16;
    }
    return 49;
}

- (UIImageView *)codeImageView {
    if (!_codeImageView) {
        
        _codeImageView = [BaseTool imageWithName:@"bingding_code" superView:nil];
    }
    return _codeImageView;
}

@end
