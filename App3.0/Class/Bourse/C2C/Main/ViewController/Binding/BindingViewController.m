//
//  BindingViewController.m
//  BIT
//
//  Created by Sunny on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "BindingViewController.h"
#import "WChatViewController.h"
#import "NewVertifyController.h"
#import "JYSRealNameVerificationViewController.h"
#import "JYSAccountDataModel.h"

@interface BindingViewController ()
@property (nonatomic,assign) NSInteger apptove;
/** account */
@property (nonatomic, strong) JYSAccountDataModel * accountModel;

/**  */
@property (nonatomic, strong) NSMutableArray * dataArray;

@end

@implementation BindingViewController
{
    NSArray *listArr;
}

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    
    [self getInfo];
//    [[XSHTTPManager rac_POSTURL:@"cuser/cuser/Account" params:nil] subscribeNext:^(resultObject *object) {
//         if (object.status) {
//            NSDictionary *zhf = object.data[@"zfb_id"];
//            NSDictionary *yhk = object.data[@"yhk_id"];
//            NSDictionary *wx = object.data[@"wx_id"];
//            [arr addObject:@{@"yhk_id":yhk?yhk:@""}];
//             [arr addObject:@{@"wx_id":wx?wx:@""}];
//            [arr addObject:@{@"zfb_id":zhf?zhf:@""}];
//              [self.tableView reloadData];
//        }
//
//    } error:^(NSError * _Nullable error) {
//
//    }];
}

- (void) getInfo {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cuser/cuser/Account" params:nil] subscribeNext:^(resultObject *state) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (state.status) {            
            self.accountModel = [JYSAccountDataModel mj_objectWithKeyValues:state.data];
            self.apptove = self.accountModel.approve_user;
            [self.tableView reloadData];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [XSTool showProgressHUDTOView:self.view withText:NetFailure];
    }];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = Localized(@"bind_pay_amount");
    
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
//       [self getInfo];
    UIView *headerView = [BaseTool viewWithColor:BG_COLOR];
    headerView.frame = CGRectMake(0, 0, mainWidth, 44);
    self.tableView.tableHeaderView = headerView;
    
    UILabel *label = [BaseTool labelWithTitle:Localized(@"bind_pay_des") textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"999999"]];
    [headerView addSubview:label];
    label.adjustsFontSizeToFitWidth = YES;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(headerView);
        make.left.mas_equalTo(13);
        make.right.mas_equalTo(-13);
    }];
//    bingding_alpay.imageset
//    bingding_bankcard.imageset
//    bingding_weixin.imageset
    
    
    listArr = @[
                @{@"imageView":@"jys_mine_pay_bankCard",
                  @"title":Localized(@"bank_amount"),
                  @"vc":@"BindBankCardViewController"
                  },
                @{@"imageView":@"jys_mine_pay_weiXin",
                  @"title":Localized(@"wechat_amount"),
                  @"vc":@"WChatViewController",
                  @"type":@"0"
    
                  },
                @{@"imageView":@"jys_mine_pay_zhiFuBao",
                  @"title":Localized(@"alipay_amount"),
                  @"vc":@"WChatViewController",
                  @"type":@"1"
                  
                  }
                ];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
        cell.textLabel.textColor = JYSMainTextColor;
        cell.textLabel.font = [UIFont systemFontOfSize:FontNum(17)];
        [cell lineLabel];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = Localized(@"no_bind");
        cell.detailTextLabel.font = [UIFont systemFontOfSize:FontNum(14)];
        NSDictionary *dic = listArr[indexPath.row];
        cell.imageView.image =[UIImage imageNamed:dic[@"imageView"]];
        cell.textLabel.text = dic[@"title"];
    }
    
 
//    if (arr.count !=0) {
//
//        NSDictionary *dic = arr[indexPath.row];
//        id infor = dic.allValues.firstObject;
//        if ([infor isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *dcicc = infor;
//             NSInteger isPass = [dcicc[dic.allKeys.lastObject] integerValue];
//             cell.detailTextLabel.text = isPass !=0 ? Localized(@"have_bind"):Localized(@"no_bind");
//        }
//    }
//
//
    
    
    if (self.accountModel) {
        switch (indexPath.row) {
            case 0:
                {
                    cell.detailTextLabel.text = [self.accountModel.yhk_id.yhk_id integerValue]? Localized(@"have_bind"):Localized(@"no_bind");
                }
                break;
            case 1:
            {
                cell.detailTextLabel.text = [self.accountModel.wx_id.wx_id integerValue]? Localized(@"have_bind"):Localized(@"no_bind");
            }
                break;
            case 2:
            {
                cell.detailTextLabel.text = [self.accountModel.zfb_id.zfb_id integerValue]? Localized(@"have_bind"):Localized(@"no_bind");
            }
                break;
            default:
                break;
        }
        
    }
    
    
    return cell;
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = listArr[indexPath.row];
    if (self.apptove != 1) {
        
        [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:Localized(@"您还未实名认证，请实名认证") message:nil CallBackBlock:^(NSInteger btnIndex) {
            if (btnIndex == 1) {
                
                [self.navigationController pushViewController:[XSTool createViewControllerWithClassName:@"JYSRealNameVerificationViewController"] animated:YES];
            }
        } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];
        
        return;
    }
    NSString *vcName = dic[@"title"];
    if ([vcName isEqualToString:@"微信账号"]) {
        WChatViewController *vc = [WChatViewController new];
        vc.title = [NSString stringWithFormat:@"%@%@",Localized(@"绑定"),vcName];
        vc.type = dic[@"type"];
        vc.bank_id = self.accountModel.wx_id.default_bank_id;
        if ([self.accountModel.wx_id.wx_id integerValue]) {
            vc.model = self.accountModel.wx_id.info;
        }
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([vcName isEqualToString:Localized(@"alipay_amount")]) {
        WChatViewController *vc = [WChatViewController new];
        vc.title = [NSString stringWithFormat:@"%@%@",Localized(@"绑定"),vcName];
        vc.type = dic[@"type"];
        vc.bank_id = self.accountModel.zfb_id.default_bank_id;
        if ([self.accountModel.zfb_id.zfb_id integerValue]) {
            vc.model = self.accountModel.zfb_id.info;
        }
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        UIViewController *vc = [NSClassFromString(dic[@"vc"]) new];
         [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
