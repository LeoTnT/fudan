//
//  WChatViewController.h
//  BIT
//
//  Created by Sunny on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYSAccountDataModel.h"

@interface WChatViewController : XSBaseTablViewController

@property (nonatomic,strong) JYSAccountInfoModel *model;

@property (nonatomic,copy) NSString *type;

@property (nonatomic,copy) NSString *bank_id;

@end
