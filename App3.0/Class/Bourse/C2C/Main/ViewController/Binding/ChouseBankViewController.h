//
//  ChouseBankViewController.h
//  BIT
//
//  Created by Sunny on 2018/4/11.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CardTypeDataParser;
@interface ChouseBankViewController : XSBaseTablViewController

@property (nonatomic,copy) void (^chouseBankCard)(CardTypeDataParser *model);


@end
