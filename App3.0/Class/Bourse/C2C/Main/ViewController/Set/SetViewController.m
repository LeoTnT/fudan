//
//  SetViewController.m
//  BIT
//
//  Created by apple on 2018/3/22.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SetViewController.h"
#import "ChildView.h"
#import "VeticalGoogleViewController.h"
#import "BindEmailViewController.h"
@interface SetViewController ()



@end

@implementation SetViewController
{
    NSArray *listArr;
    NSDictionary *app_desc;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"csetup/csetup/Core" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            app_desc = object.data;
            [self.tableView reloadData];
        }
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navi_title = Localized(@"me_item_setting");
 
    
    listArr = @[
                @{@"sectionTitle":@"个人安全设置",
                  @"row":@[
                          @{@"title":Localized(@"user_approve_suggest_no_btn"),
                            @"detail":@"未认证",
                            @"vcName":@"NewVertifyController"
                            },
                          @{@"title":Localized(@"pay_pd"),
                            @"detail":@"未认证",
                            @"vcName":@"ModifyOldPayViewController"
                             }
                          ]
                  },
                @{@"sectionTitle":@"账户安全设置",
                  @"row":@[
                          @{@"title":Localized(@"bind_mobile_title"),
                            @"detail":@"未认证",
                            @"vcName":@"InputPassWordViewController"
                            },
                          @{@"title":Localized(@"绑定邮箱"),
                            @"detail":@"未认证",
                            @"vcName":@"BindEmailViewController"
                            },
                          @{@"title":Localized(@"bind_pay_amount"),
                            @"detail":@"支付宝/微信/银行卡",
                            @"vcName":@"BindingViewController"
                            },
                          @{@"title":@"登录密码",
                            @"detail":@"已设置",
                            @"vcName":@"ModifyLoginPswViewController"
                            },
                          @{@"title":@"绑定谷歌验证",
                            @"detail":@"未认证",
                            @"vcName":@"VeticalGoogleViewController"
                            }
                          ]
                  },
                @{@"sectionTitle":@"系统语言设置",
                  @"row":@[
                          @{@"title":Localized(@"语言"),
                            @"detail":@"未认证",
                            @"vcName":@"yuyan"
                            },
                          @{@"title":Localized(@"clear_cash"),
                            @"detail":@"未认证",
                            @"vcName":Localized(@"clear_cash")
                            }
                          ]
                  },
                ];
    
     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;


}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return listArr.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *arr = listArr[section][@"row"];
    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[XSBaseTablewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
        cell.backgroundColor = mainColor;
        cell.lineLabel.frame = CGRectMake(0,  GET_HEIGT(49), mainWidth, 0.5);
        UIImage *image = [UIImage imageNamed:@"accessory"];
        UIImageView *imageView = [UIImageView new];
        imageView.image = image;
         cell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [cell addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-12);
            make.centerY.mas_equalTo(cell).offset(2);
        }];
        
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font =[UIFont systemFontOfSize:15];
        NSDictionary *dic = listArr[indexPath.section];
        NSArray *arr = dic[@"row"];
        cell.textLabel.text = Localized(arr[indexPath.row][@"title"]);
        cell.detailTextLabel.text = Localized(arr[indexPath.row][@"detail"]);
        cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
        cell.detailTextLabel.textColor = [UIColor hexFloatColor:@"666666"];
    }
   
 
    NSInteger code ;
    NSString *code_desc,*value;
    NSDictionary *infor;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: infor = app_desc[@"approve_user"];break;
            case 1:infor = app_desc[@"pay_password"];break;
        }
 
        
    }else if (indexPath.section == 1){
        switch (indexPath.row) {
            case 0: infor = app_desc[@"mobile"];  break;
            case 1: infor = app_desc[@"email"]; break;
            case 2: code_desc = @"支付宝/微信/银行卡"; break;
            case 3: code_desc = @"已设置"; break;
            case 4: infor = app_desc[@"google_is_open"]; break;
        }

    }else {
        if (indexPath.row == 1) {
            code_desc = [BaseTool getCacheSize];
        }else{
            NSString *myLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:APP_Internationalization];
            if (isEmptyString(myLanguage)) {
                 code_desc = @"Auto";
            }else {
                 if ([myLanguage isEqualToString:@"zh-Hans"]) {
                    code_desc = @"中文简体";
                }else if ([myLanguage isEqualToString:@"zh-Hant"]){
                    code_desc = @"中文繁體";
                }else{
                    code_desc = @"English";
                }
            }
            
        }
    }
     if (infor) {
         code = [infor[@"code"] boolValue];
        code_desc = infor[@"code_desc"];
        value = infor[@"value"];
         if (indexPath.section == 0 && indexPath.row == 1) {
            code_desc =  code_desc;
        }else{
        code_desc = code ? value : code_desc;
        }
    }
 
        cell.detailTextLabel.text = Localized(code_desc);
   
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
 
    
    NSDictionary *dic = listArr[indexPath.section];
    NSArray *arr = dic[@"row"];
    NSString *name = arr[indexPath.row][@"vcName"];
    if ([name isEqualToString:@"yuyan"]) {
         [self changeLanguage:indexPath];
    }else if ([name isEqualToString:Localized(@"clear_cash")]){
        [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:@"缓存清除" message:@"确定清除缓存?" CallBackBlock:^(NSInteger btnIndex) {
            if (btnIndex == 1) {
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *cacheFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
                [fileManager removeItemAtPath:cacheFilePath error:nil];
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"material_dialog_positive_text"), nil];

    }else if (!isEmptyString(name)) {
        if ([name isEqualToString:@"VeticalGoogleViewController"]) {
    
            VeticalGoogleViewController *vc = [VeticalGoogleViewController new];
            vc.dic = app_desc[@"google_is_open"];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            
            if ([name isEqualToString:@"BindEmailViewController"]) {
//                isApprove
               NSDictionary *infor = app_desc[@"email"];
                BindEmailViewController * vc = [BindEmailViewController new];
                vc.isApprove = [infor[@"code"] boolValue];
                [self.navigationController pushViewController:vc animated:YES];
                
            }else {
                UIViewController *vc =[NSClassFromString(name) new];
                [self.navigationController pushViewController:vc animated:YES];
            }
            

        }
    }
    
}

- (void) changeLanguage:(NSIndexPath *)indexPath {
    
    ChildView *popView = [[ChildView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight)];
    [popView showInView:ShowViewDirectionNoraml];
    [popView setGetSelected:^(NSString *string) {
        XSBaseTablewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = string;
        [BaseTool changeLangrageWithName:string];
    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSDictionary *dic = listArr[section];
    NSString *ss = dic[@"sectionTitle"];
    UILabel *label = [BaseTool labelWithTitle:ss textAlignment:0 font:[UIFont systemFontOfSize:14] titleColor:[UIColor hexFloatColor:@"999999"]];
    UIView *view = [BaseTool viewWithColor:main_BackColor];
    [view addSubview:label];
    label.frame = CGRectMake(12, 22-15, 100, 30);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return GET_HEIGT(49);
}




@end
