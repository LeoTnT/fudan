//
//  PCHHeaderView.h
//  BIT
//
//  Created by Sunny on 2018/3/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCHHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *oneLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeLabel;

+ (instancetype)loadPCHHeaderView;
@end
