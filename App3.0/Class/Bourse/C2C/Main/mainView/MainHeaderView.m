//
//  MainHeaderView.m
//  BIT
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MainHeaderView.h"

@implementation MainHeaderView


- (void)setHeaderModel:(MainModel *)headerModel {
    _headerModel = headerModel;
    self.name.text = headerModel.nickname;
    [self.headImage yy_setImageWithURL:[NSURL URLWithString:headerModel.logo] placeholder:[UIImage imageNamed:@"ctc_wallet_logo1"] options:(YYWebImageOptionProgressiveBlur|YYWebImageOptionSetImageWithFadeAnimation) completion:nil];
//    [self.headImage sd_setImageWithURL:[NSURL URLWithString:headerModel.logo] placeholderImage:[UIImage imageNamed:@"ctc_wallet_logo1"]];
    self.jy.text = [NSString stringWithFormat:@"%@%d",Localized(@"trade"),headerModel.trade_count];
    self.hp.text = [NSString stringWithFormat:@"%@%@",Localized(@"favourable"),headerModel.praise_rate];
    self.xr.text = [NSString stringWithFormat:@"%@%d",Localized(@"trust"),headerModel.trust_me_count];
    
}

+ (instancetype)initializeMainHeaderView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"MainHeaderView" owner:nil options:nil].lastObject;
}


@end
