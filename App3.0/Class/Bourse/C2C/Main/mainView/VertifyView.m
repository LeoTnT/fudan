//
//  VertifyView.m
//  BIT
//
//  Created by Sunny on 2018/3/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "VertifyView.h"

@implementation VertifyView

+ (instancetype)loadVertifyView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"VertifyView" owner:nil options:nil].lastObject;
}


// - 1 不用设置 0 审核中 1 已认证
- (void)setShowProgress:(NSInteger)showProgress {
    _showProgress = showProgress;
    switch (showProgress) {
        case 0:
            self.topOne.showProgress = 1;
            self.topTwo.showProgress = 0;
            self.label2.textColor = [UIColor whiteColor];
            break;
        case 1:
            self.topOne.showProgress = 1;
            self.topTwo.showProgress = 1;
            self.topThree.showProgress = 1;
            self.label2.textColor = [UIColor whiteColor];
            self.label3.textColor = [UIColor whiteColor];
            break;
        case 2:
            self.topOne.showProgress = 1;
            self.topTwo.showProgress = 1;
            self.topThree.showProgress = 2;
            self.label3.text = Localized(@"认证失败");
            self.label2.textColor = [UIColor whiteColor];
            self.label3.textColor = [UIColor whiteColor];
            break;
    }
}


@end

@implementation VertifyTopView

- (void)awakeFromNib {
    
    [super awakeFromNib];
     self.layer.cornerRadius = 17/2;
    self.layer.masksToBounds = YES;
    
}
- (void)setShowProgress:(NSInteger)showProgress {
    _showProgress = showProgress;
    
    switch (showProgress) {
        case 0:
            self.backgroundColor = MY_ORDER_SELECTED_COLOR;
            break;
        case 1:
            self.image = [UIImage imageNamed:@"ver_pass"];
            
            break;
            case 2:
            self.image = [UIImage imageNamed:@"ver_failure"];
            
            break;
     }
}

@end
