//
//  VertifyView.h
//  BIT
//
//  Created by Sunny on 2018/3/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VertifyTopView :UIImageView

@property (nonatomic,assign)NSInteger showProgress;

@end

@interface VertifyView : UIView
@property (weak, nonatomic) IBOutlet VertifyTopView *topOne;
@property (weak, nonatomic) IBOutlet VertifyTopView *topTwo;
@property (weak, nonatomic) IBOutlet VertifyTopView *topThree;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UIView *line3;
@property (nonatomic,assign)NSInteger showProgress;
+ (instancetype)loadVertifyView;


@end

