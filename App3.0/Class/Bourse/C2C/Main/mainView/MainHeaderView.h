//
//  MainHeaderView.h
//  BIT
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *hp;
@property (weak, nonatomic) IBOutlet UILabel *xr;

@property (weak, nonatomic) IBOutlet UILabel *jy;

@property (nonatomic ,strong)MainModel *headerModel;
+ (instancetype)initializeMainHeaderView ;

@end
