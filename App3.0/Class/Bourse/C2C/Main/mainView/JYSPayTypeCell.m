//
//  JYSPayTypeCell.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "JYSPayTypeCell.h"

@implementation JYSPayTypeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
    }
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(20, self.contentView.xs_height/2-20, 40, 40);
    self.textLabel.xs_x = self.imageView.xs_right+10;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
