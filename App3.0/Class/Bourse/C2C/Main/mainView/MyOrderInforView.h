//
//  MyOrderInforView.h
//  BIT
//
//  Created by apple on 2018/3/24.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderModel.h"
@protocol MyOrderInforViewDelegate <NSObject>

- (void)refreshAction;

@end


@interface MyOrderInforView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *payState;
@property (weak, nonatomic) IBOutlet UILabel *dealNum;  // 交易量
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *tradeNum;     // 交易数量
@property (weak, nonatomic) IBOutlet UILabel *tradePrice;   // 交易价格
@property (weak, nonatomic) IBOutlet UILabel *orderNo;      // 订单编号
@property (weak, nonatomic) IBOutlet UIButton *evaBtn;

@property (weak, nonatomic) IBOutlet UILabel *time;

@property (strong, nonatomic) MyOrderModel *orderModel;


/** 代理 */
@property (nonatomic, weak) id<MyOrderInforViewDelegate> delegate;



+ (instancetype)loadMyOrderInforView;
@end
