//
//  MyAdverCell.m
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyAdverCell.h"

@implementation MyAdverCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.pullOff.layer.borderColor = MY_ORDER_SELECTED_COLOR.CGColor;
    self.editButton.layer.borderColor = [UIColor hexFloatColor:@"999999"].CGColor;
 
}

//'status'：广告状态；1=正常（进行中）、2=过期（暂时没用）3=取消（已下架）4=已完成
//'pay_type'：付款方式；数组格式，1=银行卡、2=支付宝、3=微信

 
- (void)setModel:(MyAdverModel *)model {
    _model = model;
    self.type_desc.text = model.type_desc;
    self.w_time.text = model.w_time;
    switch (model.status) {
        case 1:
            self.pullOff.hidden = NO;
            [self.pullOff setTitle:Localized(@"undercarriage") forState:UIControlStateNormal];
            self.status.text = Localized(@"have_in_hand");
            break;
        case 2:
            self.pullOff.hidden = YES;
            self.status.text = Localized(@"have_down");
            break;
        case 3:
            self.pullOff.hidden = NO;
            [self.pullOff setTitle:Localized(@"up_self") forState:UIControlStateNormal];
            self.status.text = Localized(@"has_shelf");
            break;
        case 4:
            self.pullOff.hidden = YES;
            self.status.text = Localized(@"have_finish");
            break;
 
    }
    self.logo.image = model.type.integerValue ==1 ? [UIImage imageNamed:@"adver_buy"]:[UIImage imageNamed:@"adver_sell"];
 
    self.price.text = [NSString stringWithFormat:@"%.2f CNY",model.price];
    self.price_xe.text = [NSString stringWithFormat:@"%.2f～%.2fCNY",model.price_min,model.price_max];
 
    
    if (model.pay_type.count !=0) {
        NSArray *arr = model.pay_type;
        NSMutableArray *payStringArr =[BaseTool setLabelTitleWithNumber:arr];
        NSArray *arrrLabel = @[self.pt_zfb,self.pt_wx,self.pt_yhk];
        [arrrLabel enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIImageView *label = arrrLabel[idx];
            if (payStringArr.count >idx) {
                label.image = [UIImage imageNamed:payStringArr[idx]];//payStringArr[idx];
                label.hidden = NO;
            }else{
                label.hidden = YES;
            }
        }];
    }
    
}

- (IBAction)DropOff:(UIButton *)sender {
    
    if (self.myDropOff) {
        self.myDropOff(self.model);
    }
}

- (IBAction)editAction:(UIButton *)sender {
    
    if (self.myEditAction) {
        self.myEditAction(self.model);
    }
}

@end
