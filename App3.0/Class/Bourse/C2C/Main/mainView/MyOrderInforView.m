//
//  MyOrderInforView.m
//  BIT
//
//  Created by apple on 2018/3/24.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyOrderInforView.h"

@implementation MyOrderInforView


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIView *topbg = [BaseTool viewWithColor:nil];
    topbg.frame = CGRectMake(0, 0, mainWidth -20, 198);
     [self.bgView addSubview:topbg];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = topbg.bounds;
    gradientLayer.colors = @[(__bridge id)[UIColor hexFloatColor:@"33C3FF"].CGColor, (__bridge id)[UIColor hexFloatColor:@"3285FF"].CGColor];
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    [topbg.layer  insertSublayer:gradientLayer atIndex:0];
     topbg.layer.cornerRadius = 13;
    topbg.layer.masksToBounds = YES;
    
    self.bgView.layer.cornerRadius = 13;
    self.bgView.layer.shadowOffset = CGSizeMake(1, 2);
    self.bgView.layer.shadowOpacity = 0.8;
    self.bgView.layer.shadowColor = [UIColor hexFloatColor:@"8AAFFD"].CGColor;
    [self.bgView sendSubviewToBack:topbg];
    
    self.logo.layer.cornerRadius = 19;
    self.logo.layer.masksToBounds = YES;
    
    self.evaBtn.layer.cornerRadius = 5;
    self.evaBtn.layer.masksToBounds = YES;

    
}

+ (instancetype)loadMyOrderInforView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"MyOrderInforView" owner:nil options:nil].lastObject;
}


-(NSString *)getMMSSFromSS:(NSString *)totalTime{
    
    NSInteger seconds = [totalTime integerValue];
    
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@%@%@%@",str_minute,Localized(@"fen"),str_second,Localized(@"秒")];
    
    NSLog(@"format_time : %@",format_time);
    
    return format_time;
    
}
//status：一般订单 是0，支付未支付再根据is_pay判断；1完成 2取消 3申诉
- (void)setOrderModel:(MyOrderModel *)orderModel {
    _orderModel = orderModel;
    [self.logo getImageWithUrlStr:orderModel.trade_user_info.logo andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    self.nickName.text = orderModel.trade_user_info.nickname;
    self.tradePrice.text = [NSString stringWithFormat:@"%.2f CNY/%@",orderModel.coin_price.doubleValue,orderModel.coin_type];
    self.tradeNum.text = orderModel.coin_num;
    self.dealNum.text = [NSString stringWithFormat:@"%@ %ld",Localized(@"trade_30"),(long)orderModel.trade_user_info.trade_count];
    self.orderNo.text = orderModel.order_num;
    self.money.text = [NSString stringWithFormat:@"%.2fCNY",orderModel.amount.doubleValue];
    //交易状态  默认0 待确认     1完成    2 取消 3申诉
    //is_pay 'is_pay' => 是否付款 1已付款、0未付款
    
    //    BIT_LOG(@"orderModel.tpl  =%@  orderModel.status =%d  orderModel.is_pay=%d",orderModel.tpl,orderModel.status,orderModel.is_pay);
    // 卖家
    if (orderModel.status == 1) {
        if (orderModel.is_evaluate == 0 && orderModel.is_pay == 1) {
            self.evaBtn.hidden = NO;
        } else {
            self.evaBtn.hidden = YES;
        }
    }else {
        self.evaBtn.hidden = YES;
    }
    
    if ([orderModel.tpl isEqualToString:@"detail_sell"]) {
        
        NSArray *payState = @[Localized(@"等待付款"),Localized(@"交易完成"),Localized(@"交易取消"),Localized(@"appealling")];
        self.payState.text = payState[orderModel.status];
        if (orderModel.status == 0 && orderModel.is_pay == 1) {
            self.payState.text = Localized(@"买家已付款");
            self.evaBtn.hidden = NO;
            
            [self.evaBtn setTitle:Localized(@"放行货币") forState:UIControlStateNormal];
            //            self.canceHEight.constant = 30;
        }else {
            //            self.canceHEight.constant = 0;
            if (orderModel.status == 1 && orderModel.is_evaluate == 0 && orderModel.is_pay == 1) {
                self.payState.text = Localized(@"me_corder_evaluate");
            }
        }
    }else if ([orderModel.tpl isEqualToString:@"detail_buy"]){
        
        NSArray *payState = @[Localized(@"等待付款"),Localized(@"交易完成"),Localized(@"交易取消"),Localized(@"appealling")];
        self.payState.text = payState[orderModel.status];
        if (orderModel.status == 0 && orderModel.is_pay == 1) {
            self.payState.text = Localized(@"waitting_confirm");
        }
        [self.evaBtn setTitle:Localized(@"trade_evaluate") forState:UIControlStateNormal];
        
        if (orderModel.status == 1 && orderModel.is_evaluate == 0 && orderModel.is_pay == 1) {
            self.payState.text = Localized(@"me_corder_evaluate");
        }
        
    }else {
        
    }
    
    
    
    
//    flush_time
    if (!isEmptyString(orderModel.flush_time)) {
        //  // 订单状态；0=进行中（支付未支付再根据is_pay判断）、1=完成 、2=取消、 3=申诉
         if (orderModel.flush_time.integerValue != 0  ) {

             
             __block NSInteger ssss = orderModel.flush_time.integerValue;
             @weakify(self);
             [[[RACSignal interval:1 onScheduler:[RACScheduler mainThreadScheduler]] take:ssss] subscribeNext:^(id x) {
                 @strongify(self);
                 ssss --;
                 if (ssss == 0)
                 {
                     self.time.text = Localized(@"订单支付超时");
                     if (orderModel.status == 0 && orderModel.is_pay == 0) {
                         self.time.text = @"";
                         [self refreshOrder];

                     }
                 }else{
                 if (orderModel.status == 2 && orderModel.is_pay == 0) {
                     self.time.text = @"";
                     
                 } else {
                     self.time.text = [self getMMSSFromSS:[NSString stringWithFormat:@"%ld",(long)ssss]];
                 }
                 }
                 
             }];
        }else{

            if (orderModel.status == 2 && orderModel.is_pay == 0 && orderModel.cancel_id == 0) {
                self.time.text = Localized(@"订单支付超时");
            } else {
                self.time.text = @"";
            }
        }
     }
 

    
//    if (orderModel.status != 0) {
//        self.cancelOrder.hidden = YES;
//        self.canceHEight.constant = 0;
//    }
    
  

    

}

- (void)refreshOrder
{
    if ([self.delegate respondsToSelector:@selector(refreshAction)]) {
        [self.delegate refreshAction];
    }
    
}

@end
