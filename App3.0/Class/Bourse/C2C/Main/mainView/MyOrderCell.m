//
//  MyOrderCell.m
//  BIT
//
//  Created by apple on 2018/3/20.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyOrderCell.h"

@implementation MyOrderCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.headImage.layer.cornerRadius = (38/375 * mainWidth)/2;
    self.headImage.layer.masksToBounds = YES;
 
    self.layer.shadowColor = [UIColor blackColor].CGColor;
}

- (void)setModel:(MyOrderModel *)model {
    _model = model;
//    [self.headImage yy_setImageWithURL:[NSURL URLWithString:model.logo] placeholder:[UIImage imageNamed:@"no_pic"]];
    [self.headImage getImageWithUrlStr:model.trade_user_info.logo andDefaultImage:DefaultAvatarImage];
    
//    self.name.text = model.supplier_name;
    if (model.trade_user_info.nickname.length > 0) {
        self.name.text =  model.trade_user_info.nickname;
    }else{
        if (model.trade_user_info.username.length >4) {
            NSString *supplier_name =[NSString stringWithFormat:@"%@",model.trade_user_info.username];
            NSString *str1 = [supplier_name substringToIndex:2];//截取前两位
            NSString *str2 = [supplier_name substringFromIndex:supplier_name.length-2];//截取后两位
            self.name.text = [NSString stringWithFormat:@"%@***%@",str1,str2];
        }else{
            self.name.text =  model.trade_user_info.username;
            
        }
    }

    
    NSAttributedString *attributedText;
    if ([model.trade_type containsString:Localized(@"buy")]) {
        
        attributedText = [NSAttributedString lbd_changeCorlorWithColor:[UIColor hexFloatColor:@"F95453"] TotalString:model.trade_type SubStringArray:@[Localized(@"buy")]];
    }else if ([model.trade_type containsString:Localized(@"selltwo")]){
        attributedText = [NSAttributedString lbd_changeCorlorWithColor:[UIColor hexFloatColor:@"F95453"] TotalString:model.trade_type SubStringArray:@[Localized(@"selltwo")]];
    }
    if (attributedText) {
    self.B_name.attributedText = attributedText;
    }
    
    self.price.text = [NSString stringWithFormat:@"%@CNY",model.amount];
    self.finashTime.text = model.w_time;
    self.orderType.text = model.ststus_desc;
    
    
    
}
 
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
