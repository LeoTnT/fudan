//
//  MyAdverCell.h
//  BIT
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAdverModel.h"
@interface MyAdverCell : UITableViewCell

// 编辑
@property (nonatomic,copy) void(^myEditAction)(MyAdverModel *model);
// 下架
@property (nonatomic,copy) void(^myDropOff)(MyAdverModel *model);

@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *pullOff;
@property (weak, nonatomic) IBOutlet UILabel *price;

@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet UILabel *type_desc;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *price_xe;
@property (weak, nonatomic) IBOutlet UILabel *w_time;
@property (weak, nonatomic) IBOutlet UIImageView *pt_zfb;
@property (weak, nonatomic) IBOutlet UIImageView *pt_wx;
@property (weak, nonatomic) IBOutlet UIImageView *pt_yhk;


@property (nonatomic,strong) MyAdverModel *model;

@end

