//
//  MyOrderTipsCell.h
//  BIT
//
//  Created by mac on 2018/4/8.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderTipsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;

@end
