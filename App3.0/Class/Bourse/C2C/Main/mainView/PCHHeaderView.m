//
//  PCHHeaderView.m
//  BIT
//
//  Created by Sunny on 2018/3/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "PCHHeaderView.h"

@implementation PCHHeaderView

+ (instancetype)loadPCHHeaderView {
    
    return [[NSBundle mainBundle] loadNibNamed:@"PCHHeaderView" owner:nil options:nil].lastObject;
}


@end
