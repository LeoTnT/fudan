//
//  HelpCenterModel.m
//  BIT
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "HelpCenterModel.h"

@implementation HelpProblemListParser

+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"HelpProblemListDetailParser"};
}
@end

@implementation HelpProblemListDetailParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end

@implementation HelpCenterModel

@end
