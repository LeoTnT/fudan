//
//  TrustListModel.h
//  BIT
//
//  Created by Sunny on 2018/3/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 id = 49;
 logo = "";
 truename = "\U54d2\U54d2\U54d2";
 uid = 252;
 */
@interface TrustListModel : NSObject
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *truename;
@property (nonatomic,copy) NSString *uid;
    
@end


