//
//  MainModel.h
//  BIT
//
//  Created by apple on 2018/3/23.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainModel : NSObject

@property (nonatomic,copy) NSString *truename;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,assign) int trade_count;

@property (nonatomic,assign) int userid;

@property (nonatomic,copy) NSString *praise_rate;

@property (nonatomic,assign) int trust_me_count;

@property (nonatomic,copy) NSString *logo;


@end
//@interface AreaParser : NSObject
//
//@property (nonatomic, copy) NSString *code;
//@property (nonatomic, copy) NSString *name;
//@end


//@interface AddressAreaParser : NSObject
//@property (nonatomic, copy) NSString *code;
//@property (nonatomic, copy) NSString *name;
//@end
//@interface AddressParser : NSObject
//@property (nonatomic, copy) NSString *ID;
//@property (nonatomic, copy) NSString *is_default;
//@property (nonatomic, copy) NSString *user_id;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *mobile;
//@property (nonatomic, copy) NSString *zipcode;
//@property (nonatomic, copy) NSString *w_time;
//@property (nonatomic, strong) AddressAreaParser *province;
//@property (nonatomic, strong) AddressAreaParser *city;
//@property (nonatomic, strong) AddressAreaParser *county;
//@property (nonatomic, strong) AddressAreaParser *town;
//@property (nonatomic, copy) NSString *detail;
//@property (nonatomic, copy) NSString *area;
//@end
//
//@interface AppointAddressParser : NSObject
//@property (nonatomic, copy) NSString *ID;
//@property (nonatomic, copy) NSString *is_default;
//@property (nonatomic, copy) NSString *user_id;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *mobile;
//@property (nonatomic, copy) NSString *province_code;
//@property (nonatomic, copy) NSString *city_code;
//@property (nonatomic, copy) NSString *county_code;
//@property (nonatomic, copy) NSString *town_code;
//@property (nonatomic, copy) NSString *province;
//@property (nonatomic, copy) NSString *county;
//@property (nonatomic, copy) NSString *city;
//@property (nonatomic, copy) NSString *town;
//@property (nonatomic, copy) NSString *detail;
//@property (nonatomic, copy) NSString *w_time;
//@end
//@interface ReceiverAddressModel : NSObject
//
//@end

//@interface Address : NSObject
//@property (assign,nonatomic) NSInteger areaId;
//@property (strong,nonatomic) NSString *name;
//@property (strong,nonatomic) NSString *indexChar;
//@property (assign,nonatomic) NSInteger level;
//@property (assign,nonatomic) NSInteger hot;
//@property (assign,nonatomic) NSInteger commend;
//@property (assign,nonatomic) NSInteger postCode;
//@property (assign,nonatomic) NSInteger parentId;
//@property (assign,nonatomic) NSInteger areaCode;
//@property (assign,nonatomic) NSInteger fatherCode;
//@property (strong,nonatomic) NSMutableArray *sonAddress;
//@end



// 实名认证
@interface UserApproveModel :NSObject
@property (nonatomic,copy) NSString *imageString;
@property (nonatomic,copy) NSString *trueName;
@property (nonatomic,copy) NSString *card;

@end

//@"img_card": imageString,
//@"truename": trueName,
//@"card_no": card,


/*
 ①truename：真实姓名，*必须
 
 ②card_no：身份证编号，*必须
 
 ③img_card：身份证 正反面照片，数组格式，*必须
 
 ④img_card_sc：手持身份证照片，*必须
 
 ⑤remark：备注，非必填
 
 ⑥mobile：手机号码，*必须
 
 ⑦weixin：微信账号，当前是非必须
 
 ⑧province：省份名
 
 ⑨city：城市名
 
 ⑩county：县名
 
 11province_code：省代码
 
 12city_code：市代码
 
 13county_code县代码
 
 14 detail详细地址
 */
@interface RegisterEnterModel :NSObject
@property (nonatomic ,copy)NSString *truename;
@property (nonatomic ,copy)NSString *card_no;
@property (nonatomic ,copy)NSString *img_card;
@property (nonatomic,copy) NSString *img_card_sc;

@property (nonatomic ,copy)NSString *mobile;


@property (nonatomic ,copy)NSString *province;
@property (nonatomic ,copy)NSString *city;
@property (nonatomic ,copy)NSString *county;
@property (nonatomic ,copy)NSString *town;

@property (nonatomic ,copy)NSString *province_code;
@property (nonatomic ,copy)NSString *city_code;
@property (nonatomic ,copy)NSString *county_code;
@property (nonatomic ,copy)NSString *town_code;

@property (nonatomic ,copy)NSString *license_address;
@property (nonatomic,copy) NSString *weixin;

@property (nonatomic ,copy)NSString *img_license;
@property (nonatomic ,copy)NSString *img_zuzhi;
@property (nonatomic ,copy)NSString *remark;
@property (nonatomic,copy) NSString *detail;


@end


@interface AreaListModel :NSObject

@property (nonatomic,copy) NSString *component_pdetail;


@property (nonatomic ,copy)NSString *province;
@property (nonatomic ,copy)NSString *city;
@property (nonatomic ,copy)NSString *county;
@property (nonatomic ,copy)NSString *town;

@property (nonatomic ,copy)NSString *province_code;
@property (nonatomic ,copy)NSString *city_code;
@property (nonatomic ,copy)NSString *county_code;
@property (nonatomic ,copy)NSString *town_code;

@end


//@interface BusinessRegisterParser : NSObject
//@property (nonatomic, copy) NSString *approve_supply;
//@property (nonatomic, copy) NSString *disabled;
//@property (nonatomic, copy) NSString *nickname;
//@property (nonatomic, copy) NSString *username;
//@property (nonatomic, copy) NSString *logo;
//@property (nonatomic, copy) NSString *sname;
//@property (nonatomic, copy) NSString *industry_id;
//@property (nonatomic, copy) NSString *is_enter_pay;
//@property (nonatomic, copy) NSString *truename;
//@property (nonatomic, copy) NSString *card_no;
//@property (nonatomic, strong) NSArray *img_card;
//@property (nonatomic, copy) NSString *img_license;
//@property (nonatomic, copy) NSString *img_zuzhi;
//@property (nonatomic, copy) NSString *remark;
//@property (nonatomic, copy) NSString *province;
//@property (nonatomic, copy) NSString *city;
//@property (nonatomic, copy) NSString *county;
//@property (nonatomic, copy) NSString *town;
//@property (nonatomic, copy) NSString *province_code;
//@property (nonatomic, copy) NSString *city_code;
//@property (nonatomic, copy) NSString *county_code;
//@property (nonatomic, copy) NSString *town_code;
//@property (nonatomic, copy) NSString *detail;
//@property (nonatomic, copy) NSString *approve_desc;
//@property (nonatomic, copy) NSString *is_open_pay;
//@property (nonatomic, copy) NSString *enter_pay_detail;
//@property (nonatomic, copy) NSString *enter_pay_number;
//@property (nonatomic, strong) NSArray *industry;
//@property (nonatomic, copy) NSString *qrcode;
//@property (nonatomic, copy) NSString *comment;
//@property (nonatomic,copy) NSString *mobile;
//
//@property (nonatomic,copy) NSString *img_card_sc;
//@property (nonatomic,copy) NSString *weixin;
//
//@end

//@interface BusinessIndustryParser : NSObject
//@property (nonatomic, copy) NSString *iid;
//@property (nonatomic, copy) NSString *iname;
//
//@end
//
//@interface BusinessApplyIndustryParser : NSObject
//@property (nonatomic, copy) NSString *ID;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *image;
//
//@end


/*
 code：国家编号
 
 name：国家名称
 
 is_hot：是否热门
 */
@interface CountryModel:NSObject

@property (nonatomic,copy) NSString *ID;

@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *is_hot;

@end


/*
 ①type：广告类型，*必须传 ；值1是委托买入，值2是委托售出
 
 ②coin_type：货币类型，*必须传；例BTC
 
 country：国家code，从接口获取（ctools/ctools/GetCountrys）
 
 ④price：货币单价，*必须传
 
 price_height：最高价，选填；（pc端是price_lowest）
 
 ⑤price_min：单笔最低限额，*必须传
 
 ⑥price_max：单笔最高限额，*必须传
 
 ⑦pay_type：付款方式，*必须传，字符串拼接形式，例：1,2,3
 
 ⑧comment：留言备注，非必须
 
 ⑨pay_time_term：收付款时间限制，分钟数；例：15
 */
@interface NewAdModel :NSObject

@property (nonatomic,assign) NSInteger type;

@property (nonatomic,copy) NSString *coin_type;
@property (nonatomic,copy) NSString *country;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *price_height;
@property (nonatomic,copy) NSString *price_min;
@property (nonatomic,copy) NSString *price_max;

@property (nonatomic,copy) NSString *pay_type;

@property (nonatomic,copy) NSString *comment;

@property (nonatomic,copy) NSString *pay_time_term;

/** 可交易法币 */
@property (nonatomic, copy) NSString * code_type;

@end

/*
 code = 1;
 "code_desc" = "\U94f6\U884c\U5361";
 info =             {
 bankcard = 6218888888888888888;
 bankuser = 888888;
 picture = "<null>";
 };
 */
@interface WalletPaytypeModel :NSObject
@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *code_desc;
@property (nonatomic,strong) NSDictionary *info;
@end


//@interface CardTypeDataParser : NSObject
//
//@property (nonatomic, copy) NSString *ID;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *image;
//
//@end
//
//
//@interface BankCardDataParser : NSObject
//@property (nonatomic, copy) NSString *bank_id;
//@property (nonatomic, copy) NSString *ID;
//@property (nonatomic, copy) NSString *is_default;
//@property (nonatomic, copy) NSString *bankname;
//@property (nonatomic, copy) NSString *bankaddress;
//@property (nonatomic, copy) NSString *bankcard;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *bankuser;
//@property (nonatomic, copy) NSString *image;
//
//@property (nonatomic ,copy)NSString *province_code;
//@property (nonatomic ,copy)NSString *city_code;
//@property (nonatomic ,copy)NSString *country_code;
//
//@property (nonatomic ,copy)NSString *province ;
//@property (nonatomic ,copy)NSString *city ;
//@property (nonatomic ,copy)NSString *country;
//@end

