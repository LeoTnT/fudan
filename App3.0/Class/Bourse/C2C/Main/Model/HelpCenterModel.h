//
//  HelpCenterModel.h
//  BIT
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelpProblemListParser:NSObject

@property (nonatomic, strong)  NSArray *data;

@end

@interface HelpProblemListDetailParser:NSObject

@property (nonatomic, copy)  NSString *ID;
@property (nonatomic, copy)  NSString *title;
@property (nonatomic, copy)  NSString *content;
@property (nonatomic, copy)  NSString *w_time;
@property (nonatomic, copy)  NSString *u_time;

@property (nonatomic, copy)  NSString *url;

@end


//@interface HelpCenterDetailParser:NSObject
//
//@property (nonatomic, copy)  NSString *ID;
//@property (nonatomic, copy)  NSString *title;
//@property (nonatomic, copy)  NSString *content;
//@property (nonatomic, copy)  NSString *w_time;
//@property (nonatomic, copy)  NSString *u_time;
//@end

@interface HelpCenterModel : NSObject

@end
