//
//  VerticalModel.h
//  App3.0
//
//  Created by xinshang on 2018/2/7.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VerticalModel : NSObject
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *number;
@property (nonatomic,copy) NSString *time;
@property (nonatomic,copy) NSString *reason;

@end
/*
 "name": "薛佳佳",
 "type": "身份证",
 "status": "已认证",
 "number": "371321198609183417",
 "time": "2018-01-29 11:37",
 "reason": "\t55\t\t\t"

 */

@interface VerticalGoogleUrlModel : NSObject
@property (nonatomic,copy) NSString *qrcode;                 // 交易所简称
@property (nonatomic,copy) NSString *url;             // 编号
@end

@interface VerticalGoogleModel : NSObject
@property (nonatomic,copy) NSString *title;                 // 交易所简称
@property (nonatomic,copy) NSString *http_host;             // 编号
@property (nonatomic,copy) NSString *qrCodeUrl;
@property (nonatomic,copy) NSString *secret;                // 密钥
@property (nonatomic,copy) NSString *is_google_verify;      // 当前用户验证状态
@property (nonatomic,strong) VerticalGoogleUrlModel *url;
@end

/*
 "content": {
 "secret": "PHHYAB5UI4TMRAZD",
 "qrCodeUrl": "https:\/\/cointrade.com\/Upload\/ad\/user\/otpauth:\/\/totp\/cointrade-yf.dsceshi.cn?secret=PHHYAB5UI4TMRAZD&issuer=å¸é¾äº¤æ.png",
 "verify": ""
 }
 */

