//
//  JYSAccountDataModel.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYSAccountInfoModel : NSObject

/**  */
@property (nonatomic, copy) NSString * bank_id;
/**  */
@property (nonatomic, copy) NSDictionary * bankaddress;
/**  */
@property (nonatomic, copy) NSString * bankcard;
/**  */
@property (nonatomic, copy) NSString * bankname;
/**  */
@property (nonatomic, copy) NSString * bankuser;
/**  */
@property (nonatomic, copy) NSString * ID;
/**  */
@property (nonatomic, copy) NSString * image;
/**  */
@property (nonatomic, copy) NSString * name;
/**  */
@property (nonatomic, copy) NSString * picture;

@end

@interface JYSAccountYHKModel : NSObject

/**  */
@property (nonatomic, copy) NSString * default_bank_id;
/**  */
@property (nonatomic, strong) JYSAccountInfoModel * info;
/**  */
@property (nonatomic, copy) NSString * yhk_id;

@end

@interface JYSAccountWXModel : NSObject

/**  */
@property (nonatomic, copy) NSString * default_bank_id;
/**  */
@property (nonatomic, strong) JYSAccountInfoModel * info;
/**  */
@property (nonatomic, copy) NSString * wx_id;

@end

@interface JYSAccountZFBModel : NSObject

/**  */
@property (nonatomic, copy) NSString * default_bank_id;
/**  */
@property (nonatomic, strong) JYSAccountInfoModel * info;
/**  */
@property (nonatomic, copy) NSString * zfb_id;

@end

@interface JYSAccountDataModel : NSObject

//* 银行卡
//@property (nonatomic, copy) NSDictionary * yhk_id;
//* 微信
//@property (nonatomic, copy) NSDictionary * wx_id;
// 支付宝
//@property (nonatomic, copy) NSDictionary * zfb_id;

/** 银行卡 */
@property (nonatomic, strong) JYSAccountYHKModel * yhk_id;
/** 微信 */
@property (nonatomic, strong) JYSAccountWXModel * wx_id;
/** 支付宝 */
@property (nonatomic, strong) JYSAccountZFBModel * zfb_id;

@property (nonatomic, assign) NSInteger approve_user;
@end


