//
//  MyAdverModel.h
//  BIT
//
//  Created by apple on 2018/3/23.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 "coin_num" = 1;
 "coin_type" = BTC;
 comment = 1;
 freeze = "0.000000";
 id = 80;
 "pay_info" =     (
 {
 "bank_id" = 1;
 bankaddress = "<null>";
 bankcard = 13000000000;
 bankname = "<null>";
 bankuser = "\U554a\U554a\U554a\U5728";
 id = 125;
 image = "/static/common/images/banks/1.png";
 "is_default" = 0;
 name = "\U652f\U4ed8\U5b9d";
 picture = "http://wangjialintest.oss-cn-shanghai.aliyuncs.com/20180319%5Cb5a937bf9b81bec9ec8cde09ae85c002.jpg";
 }
 );
 "pay_time_term" = 15;
 "pay_type" =     (
 2
 );
 price = 1;
 "price_max" = "1.00";
 "price_min" = "1.00";
 status = 1;
 "supplier_id" = 265;
 "supplier_name" = 152441;
 type = 1;
 "type_desc" = "BTC\U8d2d\U4e70\U5e7f\U544a";
 "u_time" = 1521883162;
 "w_time" = "2018-03-24 17:19";
 */


@interface Pay_infoModel :NSObject

@property (nonatomic,copy) NSString *bank_id;
@property (nonatomic,copy) NSString *bankaddress;
@property (nonatomic,copy) NSString *bankcard;
@property (nonatomic,copy) NSString *bankname;
@property (nonatomic,copy) NSString *bankuser;
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *image;
@property (nonatomic,copy) NSString *is_default;

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *picture;



@end

@interface MyAdverPoundageModel : NSObject

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *symbol;

@property (nonatomic,copy) NSString *is_c2c;
@property (nonatomic,copy) NSString *c2c_sell_poundage;
@property (nonatomic,copy) NSString *c2c_buy_poundage;

/*
 "name": "鸿积分",
 "symbol": "HJF",
 "is_c2c": 1,
 "c2c_sell_poundage": "0",
 "c2c_buy_poundage": "0"
 */
@end

@interface MyAdverCurrenciesModel : NSObject

@property (nonatomic,copy) NSString *code;     // 货币类型
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *single_limit;
@property (nonatomic,copy) NSString *aggregate_limit;

@end

@interface MyAdverModel : NSObject

@property (nonatomic,copy) NSString *coin_type;     // 货币类型
@property (nonatomic,copy) NSString *comment;
@property (nonatomic,copy) NSString *freeze;
@property (nonatomic,copy) NSString *ID;

@property (nonatomic,copy) Pay_infoModel *pay_info;


@property (nonatomic,copy) NSString *coin_num;
@property (nonatomic,copy) NSString *pay_time_term;
@property (nonatomic ,strong)NSArray *pay_type;     // 支持付款类型；1=银行卡、2=支付宝、3=微信
@property (nonatomic,assign) CGFloat price;       // 货币单价

@property (nonatomic,assign) double price_min;   // 最低限额
@property (nonatomic,assign) double price_max;   // 最高限额
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,assign) int supplier_id;
@property (nonatomic,copy) NSString *supplier_name;


@property (nonatomic,copy) NSString *type;      // 交易类型；type=1 卖币（对应商家是买币）、type=2买币（对应商家卖币）
@property (nonatomic,copy) NSString *type_desc;
@property (nonatomic,copy) NSString *u_time;
@property (nonatomic,copy) NSString *w_time;

@property (nonatomic,copy) NSString *country_desc;
@property (nonatomic,copy) NSString *country;

@property (nonatomic,copy) NSString *price_height;



 @end

@interface MyAdCoinPayTimeModel : NSObject

@property (nonatomic,copy) NSString *pay_time_type;
@property (nonatomic,copy) NSString *c2c_pay_time;
@property (nonatomic,copy) NSString *symbol;

@end

