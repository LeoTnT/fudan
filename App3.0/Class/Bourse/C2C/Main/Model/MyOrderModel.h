//
//  MyOrderModel.h
//  BIT
//
//  Created by apple on 2018/3/23.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayInfoItemDetail :NSObject

@property (nonatomic,copy) NSString *bankcard;
@property (nonatomic,copy) NSString *bankuser;
@property (nonatomic,copy) NSString *picture;
@property (nonatomic,strong) UIImage *codeImage;
@end

@interface PayInfoItem :NSObject

@property (nonatomic,assign) NSInteger code;
@property (nonatomic,copy) NSString *code_desc;
@property (nonatomic,strong) PayInfoItemDetail *info;

@end

@interface PayInfo :NSObject

@property (nonatomic,strong) PayInfoItem *wx;
@property (nonatomic,strong) PayInfoItem *yhk;
@property (nonatomic,strong) PayInfoItem *zfb;

@end

@interface TradeUserInfo :NSObject
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *truename;
@property (nonatomic,copy) NSString *username;

@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *praise_rate;
@property (nonatomic,assign) NSInteger trade_count;
@property (nonatomic,assign) NSInteger trust_me_count;
@property (nonatomic,strong) PayInfo *pay_info;


@end

/*
 amount = "4.000000";
 "coin_num" = "1.333333";
 "coin_price" = "3.000000";
 "coin_type" = BTC;
 "customer_id" = 265;
 "customer_name" = 152441;
 id = 117;
 "is_pay" = 0;
 mark = "<null>";
 "order_num" = 1803231333175706512;
 status = 0;
 "ststus_desc" = Unpaid;
 "supplier_id" = 1;
 "supplier_name" = 888888;
 tpl = "detail_sell";
 "trade_type" = BuyBTC;
 type = 2;
 "w_time" = "2018-03-23";
 */
@interface MyOrderModel : NSObject

@property (nonatomic,copy) NSString *ID;                // 订单的id

@property (nonatomic,copy) NSString *logo;

/** 取消订单人的ID */
@property (nonatomic, assign) int cancel_id;

@property (nonatomic,assign) int status;                // 订单状态；0=进行中（支付未支付再根据is_pay判断）、1=完成 、2=取消、 3=申诉

@property (nonatomic,strong) NSString *tpl;             // 订单类型【detail_buy=买入的订单，detail_sell=卖出的订单】

@property (nonatomic,assign) int supplier_id;

@property (nonatomic,strong) NSString *amount;          // 订单总交易额

@property (nonatomic,strong) NSString *coin_num;        // 货币数量

@property (nonatomic,copy) NSString *flush_time;

@property (nonatomic,assign) int type;

@property (nonatomic,strong) NSString *supplier_name;

@property (nonatomic,assign) int is_pay;

@property (nonatomic,assign) int is_evaluate;           // 订单是否评价，1已评价、0未评价

@property (nonatomic,strong) NSString *pay_code;        // 付款代号

@property (nonatomic,strong) NSString *coin_price;      // 货币单价

@property (nonatomic,strong) NSString *w_time;          // 订单创建时间

@property (nonatomic,strong) NSString *ststus_desc;

@property (nonatomic,strong) NSString *trade_type;      // 交易类型描述

@property (nonatomic,copy) NSString *customer_id;

@property (nonatomic,strong) NSString *order_num;       // 订单编号

@property (nonatomic,strong) NSString *customer_name;

@property (nonatomic,strong) NSString *coin_type;       // 这笔订单的 货币类型

@property (nonatomic,strong) TradeUserInfo *trade_user_info;       // 交易对象的信息

@property (nonatomic,strong) NSString *talk_targert_userid;       // 联系对方uid
@property (nonatomic,strong) NSString *pay_time_term;//1 这个字段是倒计时分钟数，为0的话就是不限制时间

@end
