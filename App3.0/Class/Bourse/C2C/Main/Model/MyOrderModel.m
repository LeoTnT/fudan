//
//  MyOrderModel.m
//  BIT
//
//  Created by apple on 2018/3/23.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MyOrderModel.h"

@implementation PayInfoItemDetail
-(void)setPicture:(NSString *)picture {
    _picture = picture;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
         if (!isEmptyString(picture)) {
            self.codeImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:picture]]];
        }else{
            self.codeImage = [UIImage imageNamed:@"no_pic"];
        }
    });
    
}
 
@end

@implementation PayInfoItem
@end

@implementation PayInfo
@end

@implementation TradeUserInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"ID":@"id"};
}

@end

@implementation MyOrderModel


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"ID":@"id"};
}

@end
