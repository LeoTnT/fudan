//
//  CTCAssetModel.h
//  App3.0
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

//余额


@interface CTCAssetBlanceDetailParser : NSObject
@property (nonatomic, copy) NSString *IETC;
@property (nonatomic, copy) NSString *IECC;
@property (nonatomic, copy) NSString *FRTC;
@property (nonatomic, copy) NSString *KMCC;
@property (nonatomic, copy) NSString *ETH;
@property (nonatomic, copy) NSString *NB;
@property (nonatomic, copy) NSString *EOS;
@property (nonatomic, copy) NSString *TBX;
@property (nonatomic, copy) NSString *VEN;
@property (nonatomic, copy) NSString *QTUM;
@property (nonatomic, copy) NSString *ICX;
@property (nonatomic, copy) NSString *OMG;
@property (nonatomic, copy) NSString *BNB;
@property (nonatomic, copy) NSString *DGD;
@property (nonatomic, copy) NSString *PPT;

@end

@interface CTCAssetBlanceParser : NSObject
@property (nonatomic, copy) NSString *sum;
@property (nonatomic, strong) NSDictionary *list;
@end


@interface CTCAssetSearchParser : NSObject
@property (nonatomic, strong) NSArray *data;
@end

@interface CTCAssetSearchDataDetailParser : NSObject
@property (nonatomic, copy) NSString *contract;
@property (nonatomic, copy) NSString *full_name;
@property (nonatomic, copy) NSString *gas;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img_path;
@property (nonatomic, copy) NSString *short_name;
@end

@interface CTCAssetModel : NSObject

@end
