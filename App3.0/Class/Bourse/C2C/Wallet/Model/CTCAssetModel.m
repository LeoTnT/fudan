//
//  CTCAssetModel.m
//  App3.0
//
//  Created by nilin on 2018/4/9.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "CTCAssetModel.h"

@implementation CTCAssetBlanceDetailParser

@end

@implementation CTCAssetBlanceParser

@end

@implementation CTCAssetSearchParser
+(NSDictionary *)mj_objectClassInArray {
    return @{@"data":@"CTCAssetSearchDataDetailParser"};
}
@end

@implementation CTCAssetSearchDataDetailParser
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end

@implementation CTCAssetModel

@end
