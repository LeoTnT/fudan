//
//  CTCWalletModel.h
//  App3.0
//
//  Created by nilin on 2018/3/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

//uid,walletname,avatar,privateKey,publicKey,mnemonic,password,keyStore,backup,nowWallet,walletTint,address
@interface CTCWalletDataParser : NSObject
@property (nonatomic,copy)NSString* uid;
@property (nonatomic,copy)NSString* walletname;
@property (nonatomic,copy)NSString* avatar;
@property (nonatomic,copy)NSString* privateKey;
@property (nonatomic,copy)NSString* publicKey;
@property (nonatomic,strong)NSString* mnemonic;
@property (nonatomic,strong)NSString* password;
@property (nonatomic,strong)NSString* keyStore;
@property (nonatomic,strong)NSString* backup;
@property (nonatomic,strong)NSString* nowWallet;
@property (nonatomic,strong)NSString* walletTint;
@property (nonatomic, strong) NSString *address;
@end


@interface CTCCoinListParser:NSObject
@property (nonatomic, strong) NSArray *data;

@end

@interface CTCCoinListDetailParser:NSObject
@property (nonatomic,strong)NSString *ID;
@property (nonatomic,strong)NSString *icon;
@property (nonatomic,strong)NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *coinAddress;
@end



@interface CTCWalletModel : NSObject

@end
