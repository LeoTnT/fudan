//
//  CTCWalletModel.m
//  App3.0
//
//  Created by nilin on 2018/3/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "CTCWalletModel.h"
@implementation CTCWalletDataParser
- (id) initWithCoder: (NSCoder *)coder
{
    
    if (self = [super init])
    
    {
        self.uid = [coder decodeObjectForKey:@"uid"];
        self.walletname = [coder decodeObjectForKey:@"walletname"];
        self.walletTint = [coder decodeObjectForKey:@"walletTint"];
        self.nowWallet = [coder decodeObjectForKey:@"nowWallet"];
        self.backup = [coder decodeObjectForKey:@"backup"];
        self.password = [coder decodeObjectForKey:@"password"];
        self.privateKey = [coder decodeObjectForKey:@"privateKey"];
        self.publicKey = [coder decodeObjectForKey:@"publicKey"];
        self.avatar = [coder decodeObjectForKey:@"avatar"];
        self.mnemonic = [coder decodeObjectForKey:@"mnemonic"];
        self.keyStore = [coder decodeObjectForKey:@"keyStore"];
        self.address = [coder decodeObjectForKey:@"address"];
    }
    
    return self;
    
}

- (void) encodeWithCoder: (NSCoder *)coder

{
    [coder encodeObject:self.uid forKey:@"uid"];
    [coder encodeObject:self.walletname forKey:@"walletname"];
    [coder encodeObject:self.walletTint forKey:@"walletTint"];
    [coder encodeObject:self.nowWallet forKey:@"nowWallet"];
    [coder encodeObject:self.backup forKey:@"backup"];
    [coder encodeObject:self.password forKey:@"password"];
    [coder encodeObject:self.privateKey forKey:@"privateKey"];
    [coder encodeObject:self.publicKey forKey:@"publicKey"];
    [coder encodeObject:self.avatar forKey:@"avatar"];
    [coder encodeObject:self.keyStore forKey:@"keyStore"];
    [coder encodeObject:self.mnemonic forKey:@"mnemonic"];
    [coder encodeObject:self.address forKey:@"address"];
}
@end

@implementation CTCCoinListParser
+(NSDictionary *)mj_objectClassInArray {
    
    return @{@"data":@"CTCCoinListDetailParser"};
}

@end

@implementation CTCCoinListDetailParser
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end

@implementation CTCWalletModel

@end
