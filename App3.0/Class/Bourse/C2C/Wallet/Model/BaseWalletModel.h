//
//  BaseWalletModel.h
//  BIT
//
//  Created by Sunny on 2018/4/26.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseWalletModel : NSObject

@property (nonatomic,copy) NSString *btc_total;
@property (nonatomic,copy) NSString *total;

@property (nonatomic,strong) NSArray *coin;

@end

/*
 "btc_total" = "0.00003375";
 freeze = 0;
 logo = "/DmsAdmin/Tpl/User/822ace/Public/images/coin_cc.png";
 name = cc;
 remain = "0.30710253";
 total = "0.30710253";
 
 
 "name": "币种",
 "freeze": 冻结数量,
 "remain": "剩余可用数量",
 "total": "没用",
 "btc_total": "没用",
 "logo": "图标url"
 
 */
//remain是余额  freeze是冻结的


@interface WalleteCoinMode : NSObject
@property (nonatomic,copy) NSString *btc_total;
@property (nonatomic,copy) NSString *freeze;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *remain;
@property (nonatomic,copy) NSString *total;
@end


/*
 "coin_type" = ETH;
 freeze = "0.000000";
 logo = "http://wangjialintest.oss-cn-shanghai.aliyuncs.com/20180327%5Ce8a210324d48fa24cf69157096eb58b5.png";
 newPrice = "2620.75000000";
 percent = 0;
 remain = "0.000000";
 "rise_amount" = 0;
 "trade_num" = 0;
 valuation = "0.00";
 
 
 
 'percent'
 'rise_amount'
 'trade_num'
 'newPrice'
 'valuation'
 
 。。。其中newPrice是当前币种市场价。valuation是价值估值
 */
@interface AssetModel :NSObject
@property (nonatomic,copy) NSString *coin_type;
@property (nonatomic,copy) NSString *freeze;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *remain;
@property (nonatomic,copy) NSString *a_newPrice;
@property (nonatomic,copy) NSString *valuation;


@property (nonatomic,copy) NSString *percent;
@property (nonatomic,copy) NSString *rise_amount;
@property (nonatomic,copy) NSString *trade_num;


@end



