//
//  WalletCell.m
//  BIT
//
//  Created by Sunny on 2018/4/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "WalletCell.h"

@interface WalletCell ()
@property (nonatomic,strong) UILabel *walletCount;
@property (nonatomic,strong) UIButton *rechargeBtn;
@property (nonatomic,strong) UIButton *TransferBtn;
@property (nonatomic,strong) UILabel *walletPrice;
@property (nonatomic,strong) UILabel *walletMargin;


@end

@implementation WalletCell


- (void)setIsFirst:(BOOL)isFirst {
    _isFirst = isFirst;
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;
 
    gradientLayer.colors = isFirst ? @[(id)[UIColor hexFloatColor:@"FF466D"].CGColor,(id)[UIColor hexFloatColor:@"FC204E"].CGColor]:@[(id)[UIColor hexFloatColor:@"3C99F5"].CGColor,(id)[UIColor hexFloatColor:@"7242DD"].CGColor];  // 设置渐变颜色
    gradientLayer.startPoint = CGPointMake(0, 0);   //
    gradientLayer.endPoint = CGPointMake(1, 0);     //
 
    [self.contentView.layer  insertSublayer:gradientLayer atIndex:0];
    self.walletCount.text = isFirst ?Localized(@"法币交易账户"):Localized(@"币币交易账户");
}


- (void)rechargeBtnAction:(UIButton *)sender {
    if (self.rechargeBlock) {
        self.rechargeBlock();
    }
}

- (void)transferBtnAction:(UIButton *)sender {
    if (self.transferBlock) {
        self.transferBlock();
    }
}

- (void)setContentView {
    [super setContentView];
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
    
    UIFont *font = [UIFont systemFontOfSize:18];
    self.walletCount = [BaseTool labelWithTitle:@"  " textAlignment:0 font:font titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.walletCount];
    self.rechargeBtn = [BaseTool creatButtonWithtitle:Localized(@"charge_money") superView:self.contentView];
    self.rechargeBtn.layer.cornerRadius = 15;
    self.rechargeBtn.layer.masksToBounds = YES;
    self.rechargeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    
    
    self.TransferBtn = [BaseTool creatButtonWithtitle:Localized(@"transfers") superView:self.contentView];
    self.TransferBtn.layer.cornerRadius = 15;
    self.TransferBtn.layer.masksToBounds = YES;
    self.TransferBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    [self.rechargeBtn addTarget:self action:@selector(rechargeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.TransferBtn addTarget:self action:@selector(transferBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *font13 = [UIFont systemFontOfSize:12];
    UILabel *label1 = [BaseTool labelWithTitle:Localized(@"valuation") textAlignment:0 font:font13 titleColor:[UIColor whiteColor]];
    UILabel *label2 = [BaseTool labelWithTitle:Localized(@"margin") textAlignment:0 font:font13 titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:label1];
    [self.contentView addSubview:label2];
    
    self.walletPrice = [BaseTool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:font13 titleColor:[UIColor whiteColor]];
    self.walletMargin = [BaseTool labelWithTitle:@" " textAlignment:NSTextAlignmentCenter font:font13 titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.walletPrice];
    [self.contentView addSubview:self.walletMargin];
    
    
    
    
    [self.TransferBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(size_Fit(20));
        make.right.mas_equalTo(-size_Fit(12));
        make.width.mas_greaterThanOrEqualTo(size_Fit(60));
        make.height.mas_equalTo(30);
    }];

    [self.rechargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.TransferBtn);
        make.right.mas_equalTo(self.TransferBtn.mas_left).offset(-size_Fit(10));
        make.width.mas_equalTo(self.TransferBtn);
    }];
    
    
    [self.walletCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(size_Fit(16));
        make.centerY.mas_equalTo(self.rechargeBtn);
        make.right.mas_lessThanOrEqualTo(self.TransferBtn.mas_left).offset(-10);
    }];
    
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.walletCount);
        make.top.mas_equalTo(self.walletCount.mas_bottom).offset(size_Fit(39));
    }];
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rechargeBtn);
        make.centerY.mas_equalTo(label1);
    }];
    
    [self.walletPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.walletCount);
        make.top.mas_equalTo(label1.mas_bottom).offset(size_Fit(2));
        make.width.mas_lessThanOrEqualTo(size_Fit(325-28)/2-5);
    }];
    
    [self.walletMargin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rechargeBtn);
        make.centerY.mas_equalTo(self.walletPrice);
        make.width.mas_lessThanOrEqualTo(self.walletPrice);
    }];
    
    

    
    

    
}

- (void)setPrice:(NSString *)price {
    _price = price;
    self.walletPrice.text = price;
}


@end


@interface WalletListCell ()


@property (nonatomic,strong) UILabel *price;
@property (nonatomic,strong) UILabel *name;

@property (nonatomic,strong) UILabel *unitPrice;
@property (nonatomic,strong) UILabel *djPrice;

@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UIButton *rechargeBtn;
@property (nonatomic,strong) UIButton *transferBtn;
@end


@implementation WalletListCell

- (void)setContentView {
    [super setContentView];
     self.backgroundColor = mainColor;
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
    self.imageView  = [BaseTool imageWithName:nil superView:self.contentView];
    self.imageView.image = [UIImage xl_imageWithColor:mainColor size:CGSizeMake(22, 22)];
    self.imageView.layer.cornerRadius = 2;
    self.imageView.layer.masksToBounds = YES;
    UIFont *font = [UIFont systemFontOfSize:18];
    self.name = [BaseTool labelWithTitle:@" " textAlignment:0 font:font titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.name];
    self.price = [BaseTool labelWithTitle:@" " textAlignment:(NSTextAlignmentRight) font:font titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.price];

    
    UIFont *font13 = [UIFont systemFontOfSize:12];
    UILabel *label1 = [BaseTool labelWithTitle:[NSString stringWithFormat:@"%@:",Localized(@"can_use")] textAlignment:0 font:font13 titleColor:[UIColor whiteColor]];
    UILabel *label2 = [BaseTool labelWithTitle:[NSString stringWithFormat:@"%@:",Localized(@"freeze")] textAlignment:NSTextAlignmentRight font:font13 titleColor:[UIColor whiteColor]];
    [self.contentView addSubview:label1];
    [self.contentView addSubview:label2];
    
    self.unitPrice = [BaseTool labelWithTitle:@" " textAlignment:0 font:font titleColor:[UIColor whiteColor]];
    self.djPrice = [BaseTool labelWithTitle:@" " textAlignment:0 font:font titleColor:[UIColor whiteColor]];

    [self.contentView addSubview:self.unitPrice];
    [self.contentView addSubview:self.djPrice];
    
    self.rechargeBtn = [BaseTool creatButtonWithtitle:Localized(@"charge_money") superView:self.contentView];
    self.rechargeBtn.layer.cornerRadius = 15;
    self.rechargeBtn.layer.masksToBounds = YES;
    self.rechargeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.transferBtn = [BaseTool creatButtonWithtitle:Localized(@"transfers") superView:self.contentView];
    self.transferBtn.layer.cornerRadius = 15;
    self.transferBtn.layer.masksToBounds = YES;
    self.transferBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    
    [self.rechargeBtn addTarget:self action:@selector(rechargeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.transferBtn addTarget:self action:@selector(transferBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *linView = [BaseTool viewWithColor:MAIN_MAIN_LINE_COLOR];
    [self.contentView addSubview:linView];

    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(size_Fit(12));
        make.top.mas_equalTo(size_Fit(11));
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];

    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(60);
        make.centerY.mas_equalTo(self.imageView);
        make.left.mas_equalTo(self.imageView.mas_right).offset(10);
    }];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.imageView);
        make.right.mas_equalTo(self.contentView).offset(-size_Fit(12));
        make.left.mas_equalTo(self.name.mas_right).offset(10) ;
    }];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imageView);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(size_Fit(17));
    }];
    
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label1);
        make.right.mas_equalTo(-120);
    }];
    
    [self.unitPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label1);
        make.left.mas_equalTo(label1.mas_right).offset(2);
    }];
    [self.djPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label1);
        make.left.mas_equalTo(label2.mas_right).offset(2);
    }];

    [linView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(label1.mas_bottom).offset(size_Fit(18));
    }];
    
    [self.transferBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(linView.mas_bottom).offset(size_Fit(10));
        make.right.mas_equalTo(-size_Fit(12));
        make.width.mas_greaterThanOrEqualTo(size_Fit(60));
        make.height.mas_equalTo(30);
    }];
    
    [self.rechargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.transferBtn);
        make.right.mas_equalTo(self.transferBtn.mas_left).offset(-size_Fit(10));
        make.width.mas_equalTo(self.transferBtn);
    }];
   
}

- (void)rechargeBtnAction:(UIButton *)sender {
    if (self.rechargeBlock) {
        self.rechargeBlock();
    }
}

- (void)transferBtnAction:(UIButton *)sender {
    if (self.transferBlock) {
        self.transferBlock();
    }
}



/*
 "name": "币种",
 "freeze": 冻结数量,
 "remain": "剩余可用数量",
 "total": "没用",
 "btc_total": "没用",
 "logo": "图标url"
 */
- (void)setModel:(id )model {
    _model = model;
    if ([model isKindOfClass:[WalleteCoinMode class]]) {
        WalleteCoinMode *coinModel = (WalleteCoinMode *)model;
         [self.imageView sd_setImageWithURL:[NSURL URLWithString:coinModel.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.name.text = isEmptyString(coinModel.name)?Localized(@"无"):coinModel.name;
        self.price.text = [NSString stringWithFormat:@"%f",coinModel.freeze.doubleValue + coinModel.remain.doubleValue];
        self.djPrice.text = isEmptyString(coinModel.freeze)?@"0":coinModel.freeze;
        self.unitPrice.text = isEmptyString(coinModel.remain)?@"0":coinModel.remain;
    }else {
        AssetModel *coinModel = (AssetModel *)model;
         [self.imageView sd_setImageWithURL:[NSURL URLWithString:coinModel.logo] placeholderImage:[UIImage imageNamed:@"no_pic"]];
        self.name.text = isEmptyString(coinModel.coin_type)?Localized(@"无"):coinModel.coin_type;
        self.djPrice.text = isEmptyString(coinModel.remain)?@"0":coinModel.remain;
        self.unitPrice.text = isEmptyString(coinModel.remain)?@"0":coinModel.remain;
        self.price.text = [NSString stringWithFormat:@"%f",coinModel.freeze.doubleValue + coinModel.remain.doubleValue];
    }
//    self.unitPrice.text =model.
}


@end
