//
//  TranfHeaderView.m
//  BIT
//
//  Created by Sunny on 2018/4/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TranfHeaderView.h"

@interface TranfHeaderView()

@property (nonatomic,strong) UILabel *fromLabel;
@property (nonatomic,strong) UILabel *toLabel;

@end

@implementation TranfHeaderView

- (void)setSubViews {
    [super setSubViews];
    
    UIButton *backView = [BaseTool buttonTitle:nil image:nil superView:self];
    [self addSubview:backView];
    backView.layer.cornerRadius = 10;
    backView.layer.masksToBounds = YES;
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(12, 12, 12, 12));
    }];
    
    UIFont *font = [UIFont systemFontOfSize:15];
    self.fromLabel = [BaseTool labelWithTitle:[NSString stringWithFormat:@"%@%@",Localized(@"从"),Localized(@"coincoin")] textAlignment:0 font:font titleColor:JYSMainTextColor];
    self.toLabel = [BaseTool labelWithTitle:[NSString stringWithFormat:@"%@%@",Localized(@"至"),Localized(@"frenchcurrency")] textAlignment:0 font:font titleColor:JYSMainTextColor];
    [backView addSubview:self.fromLabel];
    [backView addSubview:self.toLabel];
    
    UIImageView *imageview = [BaseTool imageWithName:@"w_trans" superView:backView];
    self.imageview1 = [BaseTool imageWithName:@"accessory" superView:backView];
     [self.fromLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backView);
        make.left.mas_equalTo(backView).offset(13);
    }];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(backView);
    }];
 
    [self.imageview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backView);
        make.right.mas_equalTo(backView.mas_right).offset(-9);
    }];
    [self.toLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backView);
        make.right.mas_equalTo(self.imageview1.mas_left).offset(-3);
    }];
    @weakify(self);
    [[backView rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        self.imageview1.transform = x.selected ? CGAffineTransformMakeRotation(0):CGAffineTransformMakeRotation(M_PI_4);
        x.selected =! x.selected;
        if (self.TapAction) {
            self.TapAction(x);
        }

     }];
 
    
    
}

@end
