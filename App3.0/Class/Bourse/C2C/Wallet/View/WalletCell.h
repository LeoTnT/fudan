//
//  WalletCell.h
//  BIT
//
//  Created by Sunny on 2018/4/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseWalletModel.h"
@interface WalletCell : XSBaseCollectionCell



@property (nonatomic,copy) void(^rechargeBlock)();
@property (nonatomic,copy) void(^transferBlock)();

@property (nonatomic,assign) BOOL isFirst;

@property (nonatomic,copy) NSString *price;

@end


@interface WalletListCell : XSBaseCollectionCell
@property (nonatomic,copy) void(^rechargeBlock)();
@property (nonatomic,copy) void(^transferBlock)();

@property (nonatomic,strong) id model;

@end
