//
//  WalletViewController.m
//  BIT
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletCell.h"
#import "BaseWalletModel.h"
#import "TransferController.h"
#import "JYSChargingMoneyViewController.h"//充币
#import "JYSMentionMoneyViewController.h"//提币
@interface WalletViewController ()
@property(nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIView *sliderView;
@property(nonatomic, strong) NSMutableArray *walletArray;
@property(nonatomic, strong) BaseWalletModel *fbWalletModel;
@property (nonatomic, strong)  NSArray *defaultCoinArray;

@property (nonatomic,strong) UICollectionView *listCollectionView;

@property (nonatomic,strong) NSString *bb_price;;

@end

@implementation WalletViewController
{
    BOOL isShowFirst;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [self.tabBarController.navigationController popViewControllerAnimated:YES];
    }];
    
    self.navigationItem.title = Localized(@"assetmanagement");
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [self.collectionView registerClass:[WalletCell class] forCellWithReuseIdentifier:@"topCell"];
    self.collectionView.frame = CGRectMake(0, 0, mainWidth, size_Fit(156));
    self.collectionView.pagingEnabled = YES;
    self.flowLayout.minimumLineSpacing = 12;
    self.flowLayout.minimumInteritemSpacing = 12;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    self.flowLayout.itemSize = CGSizeMake(size_Fit(325), size_Fit(132));
    
    isShowFirst = YES;
    [self.listCollectionView registerClass:[WalletListCell class] forCellWithReuseIdentifier:@"listCell"];
    [self getFBCoin];
    [self setRefreshStyle];
    
    
}



- (UICollectionView *)listCollectionView {
    if (!_listCollectionView) {
        
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
        flowLayout.minimumLineSpacing = 12;
        flowLayout.minimumInteritemSpacing = 12;
        flowLayout.itemSize = CGSizeMake(mainWidth-24, size_Fit(127));
        _listCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _listCollectionView.dataSource = self;
        _listCollectionView.delegate = self;
        _listCollectionView.backgroundColor = main_BackColor;
        _listCollectionView.showsHorizontalScrollIndicator = NO;
        _listCollectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_listCollectionView];
        [_listCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.collectionView.mas_bottom);
            make.bottom.left.right.mas_equalTo(self.view);
        }];
        
        
    }
    return _listCollectionView;
}


- (BOOL) isTop:(UICollectionView *)collectionView{
    
    return [collectionView isEqual:self.collectionView] ? YES:NO;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   NSInteger ccc = !isShowFirst? self.walletArray.count: self.fbWalletModel.coin.count;
    return [self isTop:collectionView] ? 2:ccc;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    if ([self isTop:collectionView]) {
        
        WalletCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"topCell" forIndexPath:indexPath];
        cell.isFirst = indexPath.row ==0 ? YES:NO;
        cell.price= indexPath.row == 0 ? self.fbWalletModel.total:self.bb_price;
        @weakify(self);
        [cell setRechargeBlock:^{
            @strongify(self);
            [self trans:nil];
        }];
//        [cell setTransferBlock:^{
//            @strongify(self);
////            [self trans:nil];
//
//        }];
        
        [cell setTransferBlock:^{
            @strongify(self);
            
            [self mentionMoney];
            
        }];
        
        return cell;
    }
    WalletListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"listCell" forIndexPath:indexPath];
    
    id model = !isShowFirst? self.walletArray[indexPath.row]: self.fbWalletModel.coin[indexPath.row];
    cell.model =  model;
    
    @weakify(self);
//    [cell setRechargeBlock:^{
//        @strongify(self);
////        [self trans:model];
//    }];
    [cell setTransferBlock:^{
        @strongify(self);
        [self trans:model];
        
    }];
    
    return cell;
}

#pragma mark 提币
- (void)mentionMoney {
    JYSMentionMoneyViewController * mentionMoneyVC = [[JYSMentionMoneyViewController alloc] init];
    [self.navigationController pushViewController:mentionMoneyVC animated:YES];
}

- (void) trans:(NSDictionary *)dic {
//    TransferController *vc = [TransferController new];
//    vc.dic = dic;
//    [self.navigationController pushViewController:vc animated:YES];
    JYSChargingMoneyViewController * chargingMoneyVC = [[JYSChargingMoneyViewController alloc] init];
    [self.navigationController pushViewController:chargingMoneyVC animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.collectionView]) {
        NSInteger xxx = mainWidth - 24- self.flowLayout.itemSize.width;
        CGFloat index = self.flowLayout.itemSize.width-xxx +12;
        if (scrollView.contentOffset.x == 0) {
            isShowFirst = YES;
            
            [self getFBCoin];
        }else if(scrollView.contentOffset.x - index == 0) {
            isShowFirst = NO;
            [self getOtherCoin];
        }
 
    }
}

/**
 法币钱包
 */
- (void) getFBCoin {

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/GetBLWallet" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            self.fbWalletModel = [BaseWalletModel mj_objectWithKeyValues:object.data[@"content"]];
           
        }
        [self.collectionView reloadData];
         [self.listCollectionView reloadData];
        [self.listCollectionView.mj_header endRefreshing];
    } error:^(NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.listCollectionView.mj_header endRefreshing];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
}




/**
币币
 */
- (void) getOtherCoin {
 
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[XSHTTPManager rac_POSTURL:@"casset/casset/Asset" params:nil] subscribeNext:^(resultObject *object) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (object.status) {
            self.walletArray = [NSMutableArray array];
            NSArray *arr = object.data[@"data"];
           __block double pppp ;
            [arr.rac_sequence.signal subscribeNext:^(NSDictionary *dic) {
              AssetModel *model = [AssetModel mj_objectWithKeyValues:dic];
                pppp = model.valuation.doubleValue + pppp;
                [self.walletArray addObject:model];
            }completed:^{
                self.bb_price = [NSString stringWithFormat:@"%f",pppp];
            }];
            
            
        }
        [self.collectionView reloadData];
        [self.listCollectionView reloadData];
        [self.listCollectionView.mj_header endRefreshing];
        
    } error:^(NSError * _Nullable error) {
        [self.listCollectionView.mj_header endRefreshing];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showMessage:NetFailure view:self.view];
    }];
    
}

- (void) setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadNewData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.listCollectionView.mj_header=header;
 
}
- (void) loadNewData {
    
    isShowFirst ? [self getFBCoin]:[self getOtherCoin];
}

@end
