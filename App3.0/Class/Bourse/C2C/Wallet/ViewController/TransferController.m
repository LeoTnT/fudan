//
//  TransferController.m
//  BIT
//
//  Created by Sunny on 2018/4/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TransferController.h"
#import "TranfHeaderView.h"
#import "BaseWalletModel.h"
@interface TransferController ()
@property (nonatomic,strong) UILabel *naviTitle;
@property (nonatomic,strong) UIButton *backView;
@property (nonatomic,strong) UIImageView *imageView;
 @property (nonatomic ,strong)BaseBackView *popBackView;
@property (nonatomic,strong) NSMutableArray *listArr;
@end

@implementation TransferController
{
    NSMutableArray *dataSource;
    BOOL isOpenFirst;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    isOpenFirst = YES;
    self.listArr = [NSMutableArray array];
    _naviTitle = [BaseTool labelWithTitle:Localized(@"my_advert") textAlignment:0 font:[UIFont systemFontOfSize:18] titleColor:JYSMainTextColor];
    UIView *bbbb = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    bbbb.userInteractionEnabled = YES;
    _backView = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backView setTitle:@"                                " forState:UIControlStateNormal];
    [bbbb addSubview:_backView];
    [_backView addSubview:_naviTitle];
    _backView.backgroundColor = [UIColor clearColor];
    _imageView = [BaseTool imageWithName:@"business_logiatic_down" superView:_backView];
    _imageView.transform = CGAffineTransformMakeRotation(M_PI);
    self.navigationItem.titleView = bbbb;
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(bbbb);
    }];
    [_naviTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bbbb);
    }];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bbbb);
        make.left.mas_equalTo(_naviTitle.mas_right).offset(2);
    }];
    
    UIButton *topButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [topButton setTitle:@"                                " forState:UIControlStateNormal];
    [_backView addSubview:topButton];
    [topButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_backView);
    }];
    
    //
    dataSource = [NSMutableArray array];
//    @weakify(self);
    [topButton addTarget:self action:@selector(changeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//     [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
//        @strongify(self);
//        [self.navigationController popViewControllerAnimated:YES];
//    }];

    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
}


- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    if ([dic isKindOfClass:[WalleteCoinMode class]]) {
        WalleteCoinMode *model = (WalleteCoinMode *)dic;
        NSString *name =model.name;
        if (isEmptyString(name)) {
            return;
        }
        NSDictionary *params = @{@"coin":name};
        [[XSHTTPManager rac_POSTURL:@"cwallet/cwallet/GetRemain" params:params] subscribeNext:^(resultObject *object) {
            if (object.status) {
                
            }
        } error:^(NSError * _Nullable error) {
            
        }];
    }
}





- (void) changeButtonAction:(UIButton *)sender {
    sender.selected =!sender.selected;
    [UIView animateWithDuration:.25 animations:^{
        _imageView.transform = sender.selected ? CGAffineTransformMakeRotation(0):CGAffineTransformMakeRotation(M_PI);
    }];
    if (sender.selected) {
        [self showPopView];
    }else{
        
        [self removePopView];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.popBackView removeFromSuperview];
    [self.tableView removeFromSuperview];
    self.popBackView = nil;
    self.tableView = nil;
}

- (void) removePopView {
    
    [UIView animateWithDuration:.25 animations:^{
        self.popBackView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, 0);
        self.tableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, mainWidth, 0);
    }completion:^(BOOL finished) {
        [self.popBackView removeFromSuperview];
        [self.tableView removeFromSuperview];
        self.popBackView = nil;
        self.tableView = nil;
    }];
    
}


- (void) showPopView {
    self.popBackView.frame = CGRectMake(0, self.navi_Height, mainWidth, 0);
    self.tableView.frame = CGRectMake(0, 0, mainWidth, 0);
    [UIView animateWithDuration:.25 animations:^{
        self.popBackView.frame = CGRectMake(0, self.navi_Height, mainWidth, mainHeight);
        self.tableView.frame = CGRectMake(0, 0, mainWidth, dataSource.count * 40 + 50);
    }];
    [self.tableView registerClass:[XSBaseTablewCell class] forCellReuseIdentifier:@"cell"];
 
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XSBaseTablewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = @"sasas";
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    TranfHeaderView *headerView = [TranfHeaderView new];
    
    @weakify(self);
    [headerView setTapAction:^(UIControl *sender) {
        @strongify(self);
        
        
    }];
 
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 53;
}

@end
