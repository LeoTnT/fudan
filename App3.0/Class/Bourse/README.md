#  交易所主干  （Bourse） 
---
## Base
* 网络请求工具类+接口地址 
---
## Quotes（行情）
* JYSCurrencyQuotesViewController  （行情）
    * JYSKLineViewController（详情K线界面）
        * JYSKlineDepthViewController（深度列表）
        * JYSKlineDealViewController（交易列表）
        * JYSIntroduceViewController （简介）
---
## CurrencyTrading（币币交易）
* JYSCurrencyTradingViewController（币币交易界面）
    * JYSDetailBuyViewController（买）
    * JYSDetailSoldViewController（卖）
    * JYSCurrentCommissionedViewController（我的委托）
    * JYSHistoryDealViewController（历史交易）
    * JYSCurrencySearchViewController（搜索）
---
## C2C（原meto ）
* BaseTransController （法币交易）
---
## AssetManagement（资产管理）
* JYSAssetManagementVC（资产管理）
    * JYSChargingMoneyViewController（充币）
    * JYSMentionMoneyViewController （提币）
    * JYSMentionMoneyHistoricalRecordVC（历史记录）
---
## Mine（个人中心）
* JYSMineViewController（交易所个人中心）
---
## NavgationControl（导航）


