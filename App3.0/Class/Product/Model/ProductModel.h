//
//  ProductModel.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SortType) {
    SortDescending = 0, // 降序
    SortAscending = 1   // 升序
};

@interface ProductDetailParser : NSObject
@property (nonatomic, copy)NSString *money;
@property (nonatomic, copy)NSString *product_ext_id;
@property (nonatomic, copy)NSString *product_id;
@property (nonatomic, copy)NSString *product_no;
@property (nonatomic, copy)NSString *product_weight;
@property (nonatomic, copy)NSString *product_name;
@property (nonatomic, copy)NSString *spec_name;
@property (nonatomic, copy)NSString *spec_value;
@property (nonatomic, copy)NSString *stock;
@property (nonatomic, copy)NSString *sell_price;
@property (nonatomic, copy)NSString *market_price;
@property (nonatomic, copy)NSString *supply_price;
@property (nonatomic, copy)NSString *pv;
@property (nonatomic, copy)NSString *score;
@property (nonatomic, copy)NSString *coupon;
@property (nonatomic, copy)NSString *user_id;
@property (nonatomic, copy)NSString *username;
@property (nonatomic, strong)NSNumber *sell_type;
@property (nonatomic, copy)NSString *image;
@property (nonatomic, copy)NSString *image_thumb;
@property (nonatomic, strong)NSNumber *look_num;
@property (nonatomic, strong)NSNumber *recommend_type;
@property (nonatomic, copy)NSString *reward_coupon;
@property (nonatomic, copy)NSString *reward_score;
@property (nonatomic, copy)NSString *reward_power;
@end

@interface ProductDataParser : NSObject
@property (nonatomic, strong)NSArray *data;
@property (nonatomic, strong)NSNumber *current_page;
@property (nonatomic, strong)NSNumber *per_page;
@property (nonatomic, strong)NSNumber *total;
@end

@interface ProductParser : NSObject
@property (nonatomic, strong)ProductDataParser *data;
@end


/*
 page	必须	int	页码	1
 limit	可选	int	显示数量	20
 recommend	可选	int	推荐类型
 {
 "1": "推荐",
 "2": "热卖",
 "4": "新品"
 }
 cid	可选	int	产品分类ID
 stype	可选	int	产品销售类型
 {
 "1": {
 "name": "普通商品"
 },
 "2": {
 "name": "积分商品",
 "open": "[score]"
 }
 }
 1
 brand_id	可选	int	产品品牌ID
 priceStart	可选	float	价格查询（下限）开始
 priceEnd	可选	float	价格查询（上限）结束
 uid	可选	int	商家会员表ID（属于哪一个供货商）
 keyword	可选	string	产品名称模糊查询
 scateId	可选	int	供货商自定义产品分类ID
 auth	可选supply|无	string	供货商特有(=supply时，可查看未上架和未审核产品)
 price_type	可选sell_price|score	string	查询价格字段	sell_price
 fields	可选	string	查询信息字段
 _order	可选	string	排序方式	product_id默认look_num按人气|sell_num按销量|sell_price按价格|rand随机排序(如换一换)
 _sort	可选	string	升序降序	0降序|1升序|随机时不用传
 ext	supply需要获取商家数据时|MonthSale需要获取月销量时|Discount???
 */
@interface ProductModel : NSObject

@property (nonatomic ,copy)NSString *page;
@property (nonatomic ,copy)NSString *limit;
@property (nonatomic ,copy)NSString *recommend;
@property (nonatomic ,copy)NSString *cid;
@property (nonatomic ,copy)NSString *stype;
@property (nonatomic ,copy)NSNumber *brand_id;
@property (nonatomic ,copy)NSString *priceStart;
@property (nonatomic ,copy)NSString *priceEnd;
@property (nonatomic ,copy)NSString *uid;
@property (nonatomic ,copy)NSString *keyword;
@property (nonatomic ,copy)NSString *scateId;
@property (nonatomic ,copy)NSString *auth;
@property (nonatomic ,copy)NSString *price_type;

@property (nonatomic ,copy)NSString *fields;
@property (nonatomic ,copy)NSString *_order;
@property (nonatomic ,copy)NSString *_sort;
@property (nonatomic ,copy)NSString *ext;

@end




