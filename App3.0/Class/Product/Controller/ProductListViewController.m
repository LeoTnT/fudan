//
//  ProductListViewController.m
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductListCell.h"
#import "ProductListCollectionCell.h"
#import "ProductListCollectionDoubleCell.h"
#import "ProductListSortView.h"

#import "SearchVC.h"
#import "CartVC.h"
#import "GoodsDetailViewController.h"
#import "S_StoreCollectionView.h"
#import "GoodsTypeViewController.h"
#import "S_StoreInformation.h"
#import "BrandListViewController.h"

#define TOPBAR_HEIGHT 44.0f

@interface ProductListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate, ProductListCellDelegate, ProductListCollectionCellDelegate, ProductListCollectionDoubleCellDelegate, ProductListSortViewDelegate>
{
    
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (assign ,nonatomic) CGSize collectionCellSize;
@property (strong ,nonatomic) ProductListSortView *productListSortView;
@property (nonatomic, strong) UITextField *searchTextField;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) NSString *searchKeyword;

// 筛选
@property (strong, nonatomic) UIView *filterView;
@property (strong, nonatomic) UITextField *lowerTF;
@property (strong, nonatomic) UITextField *higherTF;
@end

@implementation ProductListViewController
// 注意const的位置
static NSString *const cellId = @"cellId";
static NSString *const cellDoubleId = @"cellDoubleId";
static NSString *const headerId = @"headerId";
static NSString *const footerId = @"footerId";
- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    _collectionCellSize = CGSizeMake(mainWidth, 150);
    [self.view addSubview:self.collectionView];
    [self setRefreshStyle];
    self.style = PLCollectionStyleDefault;
    _productListSortView = [[ProductListSortView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, TOPBAR_HEIGHT)];
    _productListSortView.delegate = self;
    [self.view addSubview:_productListSortView];
    
    // 添加筛选视图
    [self.view addSubview:self.filterView];
    self.filterView.hidden = YES;
    
//    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
//        [self popAction];
//    }];
    
//    [self actionCustomRightBtnWithNrlImage:@"mall_cart_gray" htlImage:nil title:nil action:^{
//        CartVC *cartVC=[[CartVC alloc] init];
//        cartVC.hidesBottomBarWhenPushed=YES;
//        [self xs_pushViewController:cartVC];
//    }];
    
    self.navigationItem.titleView = self.searchTextField;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)popAction {
    
    UIViewController *returnController;
    Class cla  = NSClassFromString(@"GoodsTypeViewController");
    //    Class cla1 = NSClassFromString(@"S_StoreViewController");
    Class cla2 = NSClassFromString(@"BrandListViewController");
    //    ||[controller isKindOfClass:cla1]/
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:cla]||[controller isKindOfClass:cla2]) {
            returnController = controller;
        }
        if (self.navigationController.viewControllers.count >2) {
            
            if ([self.navigationController.viewControllers[2] isKindOfClass:[NSClassFromString(@"SearchVC") class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
                return;
            }
        }
    }
    returnController ? [self.navigationController popToViewController:returnController animated:YES]:[self.navigationController popViewControllerAnimated:YES];
}

- (void)popHandleGesture:(UIGestureRecognizer *)gestureRecognizer {
    [self popAction];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.filterView.hidden = YES;
}

- (void)setRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestData];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:12];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.collectionView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestMoreData];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    self.collectionView.mj_footer=footer;
}

- (void)setModel:(ProductModel *)model {
    _model = model;
//    [self requestData:model];
    [XSTool showProgressHUDWithView:self.view];
    [self requestData];
}

- (void)requestData {
    self.model.page = @"1";
    [HTTPManager getProductListWithOrder:self.model.mj_keyValues Success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.collectionView.mj_header endRefreshing];
        if (state.status) {
            [self.dataArray removeAllObjects];
            ProductParser *parser = [ProductParser mj_objectWithKeyValues:dic];
            [self.dataArray addObjectsFromArray:parser.data.data];
            if (self.dataArray.count) {
                [self.collectionView reloadData];
            } else {
                [XSTool showToastWithView:self.view Text:@"暂时没有商品"];
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (void)requestMoreData {
    self.model.page = [NSString stringWithFormat:@"%d",[self.model.page intValue]+1];
    [HTTPManager getProductListWithOrder:self.model.mj_keyValues Success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        [self.collectionView.mj_footer endRefreshing];
        if (state.status) {
            
            ProductParser *parser = [ProductParser mj_objectWithKeyValues:dic];
            if (parser.data.data.count == 0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多"];
            } else {
                [self.dataArray addObjectsFromArray:parser.data.data];
                [self.collectionView reloadData];
            }
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
        
    } fail:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _searchKeyword = @"";
    }
    return self;
}

- (instancetype)initWithKeyword:(NSString *)keyword {
    self = [super init];
    if (self) {
        _searchKeyword = keyword;
        ProductModel *model = [ProductModel new];
        model.keyword = keyword;
        self.model = model;
    }
    return self;
}

#pragma mark - ProductListSortViewDelegate
- (void)sortButtonClick:(SortButton *)sortButton {
    SortButtonType type = sortButton.type;
    BOOL isAscend = sortButton.ascendingSort;
    NSString *_sor = isAscend ? @"1":@"0";
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *keyString = [[NSString alloc]initWithString:[self.searchKeyword stringByTrimmingCharactersInSet:whiteSpace]];
    
    ProductModel *searchModel = self.model;
    searchModel.keyword = keyString;
    searchModel._sort = _sor;
    switch (type) {
        case SortButtonDefault:searchModel._order = @"product_id";break;
        case SortButtonPopularity:searchModel._order = @"look_num";break;
        case SortButtonSales:searchModel._order = @"sell_num";break;
        case SortButtonPrice:searchModel._order = @"sell_price";break;
    }
    self.model = searchModel;
//    [self requestData:searchModel];
    [self requestData];
}

- (void)filterProduct {
    self.filterView.hidden = !self.filterView.hidden;
}

- (void)changeDisplayStyle {
    if (self.style == PLCollectionStyleDouble) {
        self.style = PLCollectionStyleDefault;
    } else {
        self.style = PLCollectionStyleDouble;
    }
}

#pragma mark - ProductListCellDelegate
- (void)gotoStoreClick:(NSString *)storeId {
    if (!self.filterView.hidden) {
        self.filterView.hidden = YES;
        return;
    }
    S_StoreInformation *storeInfo = [[S_StoreInformation alloc] init];
    storeInfo.storeInfor = storeId;
    storeInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeInfo animated:YES];
}

- (void)productListCellClick:(NSString *)productId {
    if (!self.filterView.hidden) {
        self.filterView.hidden = YES;
        return;
    }
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
    goodsDetailVC.goodsID = productId;
    goodsDetailVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

#pragma mark - ProductListCollectionDoubleCellDelegate
- (void)doubleCellGotoStoreClick:(NSString *)storeId {
    if (!self.filterView.hidden) {
        self.filterView.hidden = YES;
        return;
    }
    S_StoreInformation *storeInfo = [[S_StoreInformation alloc] init];
    storeInfo.storeInfor = storeId;
    storeInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeInfo animated:YES];
}

- (void)doubleCellProductListCellClick:(NSString *)productId {
    if (!self.filterView.hidden) {
        self.filterView.hidden = YES;
        return;
    }
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc] init];
    goodsDetailVC.goodsID = productId;
    goodsDetailVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.style == PLCollectionStyleDefault) {
        ProductListCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
        cell.delegate = self;
        cell.dataSource = self.dataArray[indexPath.row];
        return cell;
    } else {
        ProductListCollectionDoubleCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellDoubleId forIndexPath:indexPath];
        cell.delegate = self;
        cell.dataSource = self.dataArray[indexPath.row];
        return cell;
    }
    return nil;
}

//每一个分组的上左下右间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//定义每一个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _collectionCellSize;
}

//cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //cell被点击后移动的动画
    [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
}

// 和UITableView类似，UICollectionView也可设置段头段尾
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *headerView = [_collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerId forIndexPath:indexPath];
        if(headerView == nil)
        {
            headerView = [[UICollectionReusableView alloc] init];
        }
        headerView.backgroundColor = [UIColor grayColor];
        
        return headerView;
    }else if([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footerView = [_collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:footerId forIndexPath:indexPath];
        if(footerView == nil)
        {
            footerView = [[UICollectionReusableView alloc] init];
        }
        footerView.backgroundColor = [UIColor lightGrayColor];
        
        return footerView;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(mainWidth, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(mainWidth, 0);
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"productListCell";
    ProductListCell *cell = (ProductListCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[ProductListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    cell.dataSource = nil;
    return cell;
}

- (void)setStyle:(ProductListCollectionStyle)style {
    if (style == PLCollectionStyleDefault) {
        _style = PLCollectionStyleDefault;
        [self.collectionView registerClass:[ProductListCollectionCell class] forCellWithReuseIdentifier:cellId];
        _collectionCellSize = CGSizeMake(mainWidth, 150);
    } else if (style == PLCollectionStyleDouble) {
        _style = PLCollectionStyleDouble;
        [self.collectionView registerClass:[ProductListCollectionDoubleCell class] forCellWithReuseIdentifier:cellDoubleId];
        _collectionCellSize = CGSizeMake(mainWidth/2-0.5, mainWidth/2+130);
    }
    [self.collectionView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"%s",__FUNCTION__);
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[SearchVC class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            return NO;
        }
    }
    SearchVC *search = [[SearchVC alloc] initWithSearchBarStyle:SearchBarStyleMall];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
    return NO;
}

- (void)filterAction:(UIButton *)sender {
    if ([self.lowerTF.text intValue] > [self.higherTF.text intValue]) {
        [XSTool showToastWithView:self.view Text:@"请输入正确信息"];
        return;
    }
    ProductModel *prdModel = self.model;
//    prdModel.keyword = nil;
//    prdModel._order = @"product_id";
    prdModel.priceStart = self.lowerTF.text;
    prdModel.priceEnd = self.higherTF.text;
    self.model = prdModel;
//    [self requestData:prdModel];
    [self requestData];
    
//    [self getProductListDataWithOrder:@"product_id" sortType:1 keyword:@"" brandId:self.brand_id cId:self.cId uid:self.uid low_price:self.lowerTF.text high_price:self.higherTF.text];
    self.filterView.hidden = YES;
    [self.view endEditing:YES];
    self.lowerTF.text = @"";
    self.higherTF.text = @"";
}

#pragma mark - lazy loading
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        //此处必须要有创见一个UICollectionViewFlowLayout的对象
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
        //同一行相邻两个cell的最小间距
        layout.minimumInteritemSpacing = 0.5;
        //最小两行之间的间距
        layout.minimumLineSpacing = 0.5;
        
        _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, TOPBAR_HEIGHT, mainWidth, mainHeight-TOPBAR_HEIGHT) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        [_collectionView registerClass:[ProductListCollectionCell class] forCellWithReuseIdentifier:cellId];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerId];
        
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _collectionView;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _tableView;
}

- (UITextField *)searchTextField {
    if (_searchTextField == nil) {
        _searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 7, self.view.frame.size.width-100, 30)];
        _searchTextField.placeholder = @"搜索宝贝";
        _searchTextField.text=_searchKeyword;
        _searchTextField.font = [UIFont systemFontOfSize:14];
        _searchTextField.backgroundColor = BG_COLOR;
        _searchTextField.layer.masksToBounds = YES;
        _searchTextField.layer.cornerRadius = 15;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _searchTextField.frame.size.height, _searchTextField.frame.size.height)];
        UIImageView *leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_nav_search"]];
        leftImg.frame = CGRectMake(5, 5, leftView.frame.size.height-10, leftView.frame.size.height-10);
        [leftView addSubview:leftImg];
        
        _searchTextField.leftView = leftView;
        _searchTextField.leftViewMode = UITextFieldViewModeAlways;
        _searchTextField.delegate = self;
    }
    return _searchTextField;
}

- (UIView *)filterView {
    if (!_filterView) {
        _filterView = [[UIView alloc] initWithFrame:CGRectMake(0, TOPBAR_HEIGHT, mainWidth, 60)];
        _filterView.backgroundColor = BG_COLOR;
        
        UILabel *title = [[UILabel alloc] init];
        title.text = @"价格区间";
        title.font = [UIFont systemFontOfSize:16];
        [_filterView addSubview:title];
        
        UILabel *to = [[UILabel alloc] init];
        to.text = @"~";
        to.font = [UIFont systemFontOfSize:16];
        [_filterView addSubview:to];
        
        UILabel *yuan = [[UILabel alloc] init];
        yuan.text = @"元";
        yuan.font = [UIFont systemFontOfSize:16];
        [_filterView addSubview:yuan];
        
        _lowerTF = [[UITextField alloc] init];
        _lowerTF.keyboardType = UIKeyboardTypeNumberPad;
        _lowerTF.textAlignment = NSTextAlignmentCenter;
        [_filterView addSubview:_lowerTF];
        
        _higherTF = [[UITextField alloc] init];
        _higherTF.keyboardType = UIKeyboardTypeNumberPad;
        _higherTF.textAlignment = NSTextAlignmentCenter;
        [_filterView addSubview:_higherTF];
        
        UIButton *confirm = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirm setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        [confirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirm setBackgroundColor:mainColor];
        [confirm addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
        [_filterView addSubview:confirm];
        
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_filterView.mas_left).offset(10);
            make.centerY.mas_equalTo(_filterView);
        }];
        
        [_lowerTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(title.mas_right).offset(10);
            make.centerY.mas_equalTo(_filterView);
            make.width.mas_equalTo(60);
        }];
        
        [to mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_lowerTF.mas_right).offset(10);
            make.centerY.mas_equalTo(_filterView);
        }];
        
        [_higherTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(to.mas_right).offset(10);
            make.centerY.mas_equalTo(_filterView);
            make.width.mas_equalTo(60);
        }];
        
        [yuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_higherTF.mas_right).offset(5);
            make.centerY.mas_equalTo(_filterView);
        }];
        
        [confirm mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_filterView.mas_right).offset(-5);
            make.centerY.mas_equalTo(_filterView);
            make.size.mas_equalTo(CGSizeMake(60, 40));
        }];
    }
    return _filterView;
}
@end
