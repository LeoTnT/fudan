//
//  ProductListViewController.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
typedef NS_ENUM(NSInteger, ProductListCollectionStyle) {
    PLCollectionStyleDefault = 1,
    PLCollectionStyleDouble
};
@interface ProductListViewController : XSBaseViewController
- (instancetype)initWithKeyword:(NSString *)keyword;
@property (assign, nonatomic)ProductListCollectionStyle style;

//// 产品品牌ID
//@property (nonatomic, copy) NSNumber *brand_id;
//
//// 产品分类CID
//@property (nonatomic, copy) NSNumber *cId;
//
///**抢购8，全返1，普通0*/
//@property (nonatomic, copy) NSString *type;
//
///**商家新品上架*/
//@property (nonatomic, copy) NSString *uid;


@property (nonatomic ,strong)ProductModel *model;
@end

