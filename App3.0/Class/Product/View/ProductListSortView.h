//
//  ProductListSortView.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortButton.h"

@protocol ProductListSortViewDelegate <NSObject>

- (void)sortButtonClick:(SortButton *)sortButton;
- (void)changeDisplayStyle;
- (void)filterProduct;

@end
@interface ProductListSortView : UIView
@property (weak, nonatomic) id<ProductListSortViewDelegate>delegate;
@end
