//
//  SortButton.m
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SortButton.h"

@implementation SortButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitleColor:mainGrayColor forState:UIControlStateNormal];
        [self setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [self setImage:[UIImage imageNamed:@"mall_arrows_down"] forState:UIControlStateSelected];
        _ascendingSort = NO;
        [self.titleLabel setFont:[UIFont systemFontOfSize:14]];
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}

- (void)setAscendingSort:(BOOL)ascendingSort
{
    _ascendingSort = ascendingSort;
    if (ascendingSort) {
        [self setImage:[UIImage imageNamed:@"mall_arrows_up"] forState:UIControlStateSelected];
    } else {
        [self setImage:[UIImage imageNamed:@"mall_arrows_down"] forState:UIControlStateSelected];
    }
}

#pragma mark - private
-(void)layoutBtnWithBtn:(UIButton *)button{
    button.titleLabel.backgroundColor = button.backgroundColor;
    button.imageView.backgroundColor = button.backgroundColor;
    
    CGSize titleSize = button.titleLabel.bounds.size;
    CGSize imageSize = button.imageView.bounds.size;
    NSLog(@"titleSize :%@ \n imageSize:%@",NSStringFromCGSize(titleSize),NSStringFromCGSize(imageSize));
    //文字左移
    button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, 0.0, imageSize.width);
    //图片右移
    button.imageEdgeInsets = UIEdgeInsetsMake(0.0, titleSize.width, 0.0, -titleSize.width);
}
@end
