//
//  ProductListCollectionCell.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

@protocol ProductListCollectionCellDelegate <NSObject>

- (void)gotoStoreClick:(NSString *)storeId;
- (void)productListCellClick:(NSString *)productId;

@end

@interface ProductListCollectionCell : UICollectionViewCell
@property (weak, nonatomic) id<ProductListCollectionCellDelegate>delegate;
@property (strong , nonatomic) ProductDetailParser *dataSource;
@end
