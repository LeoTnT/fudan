//
//  ProductListCell.m
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ProductListCell.h"

@interface ProductListCell()
{
    
}
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productTitleLabel;
@property (nonatomic, strong) UILabel *productPriceLabel;
@property (nonatomic, strong) UILabel *productMarketPriceLabel;
@property (nonatomic, strong) UILabel *productFreightLabel;
@property (nonatomic, strong) UILabel *productInterestLabel;
@property (nonatomic, strong) UIButton *gotoStoreButton;
@end
@implementation ProductListCell
@synthesize delegate;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat space = 10.0f;
        CGFloat imageWidth = 80.0f;
        
        self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(space, space, imageWidth, imageWidth)];
        [self addSubview:self.productImageView];
        
        self.productTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productImageView.frame)+space, space, mainWidth-CGRectGetMaxX(self.productImageView.frame)-space*2, 20)];
        self.productTitleLabel.font = [UIFont systemFontOfSize:18.0f];
        self.productTitleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:self.productTitleLabel];
        
        self.productPriceLabel = [[UILabel alloc] init];
        self.productPriceLabel.font = [UIFont systemFontOfSize:18.0f];
        [self addSubview:self.productPriceLabel];
        
        self.productMarketPriceLabel = [[UILabel alloc] init];
        self.productMarketPriceLabel.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:self.productMarketPriceLabel];
        
        self.productFreightLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productImageView.frame)+space, CGRectGetMaxY(self.productImageView.frame)-20, 60, 20)];
        self.productFreightLabel.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:self.productFreightLabel];
        
        self.productInterestLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productFreightLabel.frame), CGRectGetMaxY(self.productImageView.frame)-20, mainWidth-CGRectGetMaxX(self.productFreightLabel.frame)-100, 20)];
        self.productInterestLabel.font = [UIFont systemFontOfSize:14.0f];
        self.productInterestLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.productInterestLabel];
        
        self.gotoStoreButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-100, CGRectGetMaxY(self.productImageView.frame)-20, 90, 20)];
        [self.gotoStoreButton setTitle:@"进入店铺" forState:UIControlStateNormal];
        [self.gotoStoreButton setTitleColor:mainColor forState:UIControlStateNormal];
        [self.gotoStoreButton addTarget:self action:@selector(gotoStore:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.gotoStoreButton];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCellAction:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)gotoStore:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(gotoStore:)]) {
        [delegate gotoStoreClick:@""];
    }
}

- (void)tapCellAction:(UITapGestureRecognizer *)sender
{
    if ([delegate respondsToSelector:@selector(productListCellClick:)]) {
        [delegate productListCellClick:@""];
    }
}

- (void)setDataSource:(ProductParser *)dataSource
{
    
}
@end
