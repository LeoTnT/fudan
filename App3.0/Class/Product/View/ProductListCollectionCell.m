//
//  ProductListCollectionCell.m
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ProductListCollectionCell.h"

@interface ProductListCollectionCell()
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productTitleLabel;
@property (nonatomic, strong) UILabel *productPriceLabel;
@property (nonatomic, strong) UILabel *productMarketPriceLabel;
@property (nonatomic, strong) UILabel *productFreightLabel;
@property (nonatomic, strong) UILabel *productInterestLabel;
@property (nonatomic, strong) UIButton *gotoStoreButton;
@property (nonatomic, strong) UILabel *privilegeLabel;//优惠信息
@end
@implementation ProductListCollectionCell
@synthesize delegate;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        CGFloat space = 10.0f;
        CGFloat imageWidth = 120.0f;
        
        _productImageView = [[UIImageView alloc] init];
        _productImageView.contentMode = UIViewContentModeScaleAspectFill;
        _productImageView.clipsToBounds = YES;
        [self addSubview:_productImageView];
        [_productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(space);
            make.size.mas_equalTo(CGSizeMake(imageWidth, imageWidth));
            make.centerY.mas_equalTo(self);
        }];
        
        _productTitleLabel = [UILabel new];
        _productTitleLabel.numberOfLines = 2;
        _productTitleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _productTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_productTitleLabel];
        [_productTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productImageView.mas_right).offset(space);
            make.right.mas_equalTo(self).offset(-space);
            make.top.mas_equalTo(_productImageView);
        }];
        
        _productPriceLabel = [[UILabel alloc] init];
        _productPriceLabel.font = [UIFont systemFontOfSize:18.0f];
        _productPriceLabel.textColor = [UIColor redColor];
        [self addSubview:_productPriceLabel];
        [_productPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productTitleLabel);
            make.top.mas_equalTo(_productTitleLabel.mas_bottom).offset(15);
        }];
        
        self.privilegeLabel = [[UILabel alloc] init];
        self.privilegeLabel.numberOfLines = 2;
        self.privilegeLabel.font = [UIFont systemFontOfSize:14];
        self.privilegeLabel.adjustsFontSizeToFitWidth = YES;
        [self addSubview:self.privilegeLabel];
        [self.privilegeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productPriceLabel);
            make.top.mas_equalTo(_productPriceLabel.mas_bottom).offset(space);
            make.right.mas_lessThanOrEqualTo(self).offset(-space);
        }];
        
        _productMarketPriceLabel = [[UILabel alloc] init];
        _productMarketPriceLabel.font = [UIFont systemFontOfSize:14.0f];
        _productMarketPriceLabel.textColor = [UIColor grayColor];
        [self addSubview:_productMarketPriceLabel];
        [_productMarketPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productPriceLabel.mas_right).offset(space);
            make.bottom.mas_equalTo(_productPriceLabel);
        }];
        /* 运费先删除
        _productFreightLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_productImageView.frame)+space, CGRectGetMaxY(_productImageView.frame)-20, 60, 20)];
        _productFreightLabel.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:_productFreightLabel];
         */
        
        _productInterestLabel = [UILabel new];
        _productInterestLabel.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:_productInterestLabel];
        [_productInterestLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productTitleLabel);
            make.bottom.mas_equalTo(_productImageView);
        }];
        
        _gotoStoreButton = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-80, CGRectGetMaxY(_productImageView.frame)-20, 70, 20)];
        [_gotoStoreButton setTitle:@"进入店铺" forState:UIControlStateNormal];
        [_gotoStoreButton setTitleColor:mainColor forState:UIControlStateNormal];
        [_gotoStoreButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_gotoStoreButton addTarget:self action:@selector(gotoStore:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_gotoStoreButton];
        [_gotoStoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-space);
            make.centerY.mas_equalTo(_productInterestLabel);
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCellAction:)];
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

- (void)gotoStore:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(gotoStoreClick:)]) {
        [delegate gotoStoreClick:[NSString stringWithFormat:@"%@",_dataSource.user_id]];
    }
}

- (void)tapCellAction:(UITapGestureRecognizer *)sender
{
    if ([delegate respondsToSelector:@selector(productListCellClick:)]) {
        [delegate productListCellClick:[NSString stringWithFormat:@"%@",_dataSource.product_id]];
    }
}

- (void)setDataSource:(ProductDetailParser *)dataSource
{
    _dataSource = dataSource;
    
    self.productImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.productImageView getImageWithUrlStr:dataSource.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];

    if ([dataSource.sell_type integerValue] == 100) {
        // 批发商品
        NSString *str1 = [NSString stringWithFormat:@"[批发]%@",dataSource.product_name];
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",str1]];
        [attri addAttribute:NSForegroundColorAttributeName value:mainColor range:NSMakeRange(0, 4)];
        self.productTitleLabel.attributedText=attri;
    } else {
        self.productTitleLabel.text = dataSource.product_name;
    }
    
    self.productPriceLabel.text = [NSString stringWithFormat:@"¥%@",dataSource.sell_price];

    if ([_dataSource.sell_type integerValue] == 2) {
        // 积分商品
        self.productPriceLabel.text = [NSString stringWithFormat:@"积分兑换:%@",dataSource.score];
    }
    
    // 文字中划线效果
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"¥%@",dataSource.market_price] attributes:attribtDic];
    self.productMarketPriceLabel.attributedText = attribtStr;
    self.productInterestLabel.text = [NSString stringWithFormat:@"%@人感兴趣",dataSource.look_num];
}
@end
