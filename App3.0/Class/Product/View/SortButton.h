//
//  SortButton.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SortButtonType) {
    SortButtonDefault = 1,
    SortButtonPopularity,
    SortButtonSales,
    SortButtonPrice
};
@interface SortButton : UIButton
@property (assign, nonatomic) BOOL ascendingSort; //是否为升序排序
@property (assign, nonatomic) SortButtonType type;
@end
