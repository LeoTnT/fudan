//
//  ProductListCell.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

@protocol ProductListCellDelegate <NSObject>

- (void)gotoStoreClick:(NSString *)storeId;
- (void)productListCellClick:(NSString *)productId;

@end
@interface ProductListCell : UITableViewCell
@property (weak, nonatomic) id<ProductListCellDelegate>delegate;
@property (strong , nonatomic) ProductParser *dataSource;
@end
