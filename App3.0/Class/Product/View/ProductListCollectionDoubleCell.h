//
//  ProductListCollectionDoubleCell.h
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

@protocol ProductListCollectionDoubleCellDelegate <NSObject>

- (void)doubleCellGotoStoreClick:(NSString *)storeId;
- (void)doubleCellProductListCellClick:(NSString *)productId;

@end

@interface ProductListCollectionDoubleCell : UICollectionViewCell
@property (strong , nonatomic) ProductDetailParser *dataSource;
@property (weak, nonatomic) id<ProductListCollectionDoubleCellDelegate>delegate;
@end
