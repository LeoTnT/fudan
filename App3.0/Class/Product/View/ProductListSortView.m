//
//  ProductListSortView.m
//  App3.0
//
//  Created by mac on 17/4/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ProductListSortView.h"
#import "SortButton.h"

@interface ProductListSortView ()
@property (strong, nonatomic) SortButton *defaultSortButton;
@property (strong, nonatomic) SortButton *popularitySortButton;
@property (strong, nonatomic) SortButton *salesSortButton;
@property (strong, nonatomic) SortButton *priceSortButton;
@property (strong, nonatomic) UIButton *filterButton;
@property (strong, nonatomic) UIButton *changeDisplayStyleButton;
@property (strong, nonatomic) NSMutableArray *sortButtonArray;
@end
@implementation ProductListSortView
@synthesize delegate;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat space = 10.0f;
        _sortButtonArray = [NSMutableArray array];
        _defaultSortButton = [SortButton buttonWithType:UIButtonTypeCustom];
        [_defaultSortButton setTitle:@"综合" forState:UIControlStateNormal];
        [_defaultSortButton addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
        _defaultSortButton.type = SortButtonDefault;
        _defaultSortButton.selected = YES;
        [self addSubview:_defaultSortButton];
        [_sortButtonArray addObject:_defaultSortButton];
        [_defaultSortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(space);
            make.centerY.mas_equalTo(self);
            make.width.mas_equalTo(45);
        }];
        
        _popularitySortButton = [SortButton buttonWithType:UIButtonTypeCustom];
        [_popularitySortButton setTitle:@"人气" forState:UIControlStateNormal];
        [_popularitySortButton addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
        _popularitySortButton.type = SortButtonPopularity;
        [self addSubview:_popularitySortButton];
        [_sortButtonArray addObject:_popularitySortButton];
        [_popularitySortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_defaultSortButton.mas_right);
            make.centerY.mas_equalTo(self);
            make.width.mas_equalTo(45);
        }];
        
        _salesSortButton = [SortButton buttonWithType:UIButtonTypeCustom];
        [_salesSortButton setTitle:@"销量" forState:UIControlStateNormal];
        [_salesSortButton addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
        _salesSortButton.type = SortButtonSales;
        [self addSubview:_salesSortButton];
        [_sortButtonArray addObject:_salesSortButton];
        [_salesSortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_popularitySortButton.mas_right);
            make.centerY.mas_equalTo(self);
            make.width.mas_equalTo(45);
        }];
        
        _priceSortButton = [SortButton buttonWithType:UIButtonTypeCustom];
        [_priceSortButton setTitle:Localized(@"price_exchange") forState:UIControlStateNormal];
        [_priceSortButton addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
        _priceSortButton.type = SortButtonPrice;
        [self addSubview:_priceSortButton];
        [_sortButtonArray addObject:_priceSortButton];
        [_priceSortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_salesSortButton.mas_right);
            make.centerY.mas_equalTo(self);
            make.width.mas_equalTo(45);
        }];
        
        _changeDisplayStyleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_changeDisplayStyleButton setImage:[UIImage imageNamed:@"mall_classify_1"] forState:UIControlStateNormal];
        [_changeDisplayStyleButton setImage:[UIImage imageNamed:@"mall_classify_2"] forState:UIControlStateSelected];
        [_changeDisplayStyleButton addTarget:self action:@selector(changeDisplayStyle:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_changeDisplayStyleButton];
        [_changeDisplayStyleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-space);
            make.centerY.mas_equalTo(self);
        }];
        
        _filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_filterButton setImage:[UIImage imageNamed:@"mall_filter"] forState:UIControlStateNormal];
        [_filterButton setTitle:@"筛选商品" forState:UIControlStateNormal];
        [_filterButton setTitleColor:[UIColor hexFloatColor:@"666666"] forState:UIControlStateNormal];
        [_filterButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_filterButton addTarget:self action:@selector(filterProduct:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_filterButton];
        [_filterButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_changeDisplayStyleButton.mas_left).offset(-space);
            make.centerY.mas_equalTo(self);
        }];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-1, mainWidth, 1)];
        lineView.backgroundColor = LINE_COLOR;
        [self addSubview:lineView];
    }
    return self;
}

- (void)sortAction:(SortButton *)sender
{
    sender.selected = YES;
    sender.ascendingSort = !sender.ascendingSort;
    for (SortButton *btn in self.sortButtonArray) {
        if (![btn isEqual:sender]) {
            btn.selected = NO;
            btn.ascendingSort = NO;
        }
    }
    
    if ([delegate respondsToSelector:@selector(sortButtonClick:)]) {
        [delegate sortButtonClick:sender];
    }
}

- (void)changeDisplayStyle:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if ([delegate respondsToSelector:@selector(changeDisplayStyle)]) {
        [delegate changeDisplayStyle];
    }
}

- (void)filterProduct:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(filterProduct)]) {
        [delegate filterProduct];
    }
}
@end
