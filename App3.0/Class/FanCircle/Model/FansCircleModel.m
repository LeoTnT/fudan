//
//  FansCircleModel.m
//  App3.0
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FansCircleModel.h"
@implementation Fan

@end
@implementation Fans
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"data" : @"Fan",
             };
}
@end

@implementation FanCircleShare

@end

//视频
@implementation Video

@end
//点赞用户
@implementation Thumbs_up_user

@end
//一条回复
@implementation Reply

@end
@implementation Status
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"imgs" : @"NSString",
             @"reply":@"Reply",
             @"thumbs_up_users":@"Thumbs_up_user",
             @"videos":@"Video"
             };
}

@end
@implementation FansCircleModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"data":@"Status"
             };
}

@end
