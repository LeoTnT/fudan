//
//  FansCircleModel.h
//  App3.0
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Fan : NSObject
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *nickname;
@property(nonatomic,copy)NSString *avatar;
@property(nonatomic,copy)NSString *relation;
@property(nonatomic,copy)NSString *showname;
@property(nonatomic,copy)NSString *uid;
@end

@interface Fans : NSObject
@property(nonatomic,strong)NSArray *data;
@end

@interface FanCircleShare : NSObject
@property(nonatomic,copy)NSString *image_url;
@property(nonatomic,copy)NSString *head;
@property(nonatomic,copy)NSString *url;
@end

@interface Status :NSObject
@property(nonatomic,copy)NSString *avatar;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,strong)NSArray *imgs;
@property(nonatomic,strong)NSNumber *is_owner;//是不是用户的动态 1：是 0：不是
@property(nonatomic,strong)NSNumber *is_thumbs_up;//当前用户有没有点赞
@property(nonatomic,copy)NSString *nickname;
@property(nonatomic,strong)NSArray *reply;//评论数组
@property(nonatomic,strong)NSNumber *reply_number;
@property(nonatomic,strong)NSNumber *thumbs_up_number;
@property(nonatomic,strong)NSArray *thumbs_up_users;//点赞数组
@property(nonatomic,strong)NSNumber *topic_id;//动态编号
@property(nonatomic,copy)NSString *username;//用户编号
@property(nonatomic,copy)NSString *uid;//用户编号
@property(nonatomic,strong)NSArray *videos;
@property(nonatomic,strong)NSNumber *w_time;//动态发表时间
@property(nonatomic,copy)NSString *position;
@property(nonatomic,copy)NSString *width;//图片的宽
@property(nonatomic,copy)NSString *height;//图片的高
@property(nonatomic,strong)FanCircleShare *share_info;
@property(nonatomic,assign)BOOL is_share;
@end

@interface Reply : NSObject
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *from_nickname;//评论来源者的昵称
@property(nonatomic,copy)NSString *from_username;//评论来源者的标识
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,strong)NSNumber *is_owner;
@property(nonatomic,strong)NSNumber *reply_id;
@property(nonatomic,copy)NSString *to_nickname;
@property(nonatomic,copy)NSString *to_username;
@property(nonatomic,strong)NSNumber *w_time;//时间
@end

@interface Thumbs_up_user : NSObject
@property(nonatomic,copy)NSString *nikename;
@property(nonatomic,copy)NSString *username;
@end

@interface Video : NSObject
@property(nonatomic,copy)NSString *img;
@property(nonatomic,copy)NSString *video;
@end

@interface FansCircleModel : NSObject
@property(nonatomic,strong)NSArray *data;
@end
