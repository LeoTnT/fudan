//
//  VideoHandleModel.h
//  App3.0
//
//  Created by syn on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol VideoHandleModelDelegate<NSObject>
-(void)sendVideoDataWithDic:(NSDictionary *)videoDic;
@end
@interface VideoHandleModel : NSObject
-(void)getVideoFirstImageAndDataAndUrlWithVideoUrl:(NSURL *)url;
@property(nonatomic,weak)id <VideoHandleModelDelegate>delegate;
@end
