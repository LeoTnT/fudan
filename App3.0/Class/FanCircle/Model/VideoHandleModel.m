//
//  VideoHandleModel.m
//  App3.0
//
//  Created by syn on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "VideoHandleModel.h"
#import <AVFoundation/AVFoundation.h>

@implementation VideoHandleModel
-(void)getVideoFirstImageAndDataAndUrlWithVideoUrl:(NSURL *)url{
    //视频路径
    NSString *urlStr=[url path];
    //保存
    if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)){
        UISaveVideoAtPathToSavedPhotosAlbum(urlStr, self, nil, nil);
    }
    //获取图片缩略图
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    //修改图片大小
    UIGraphicsBeginImageContext(CGSizeMake(mainWidth/3,150));
    [videoImage drawInRect:CGRectMake(0, 0, mainWidth/3,150)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //给视频取新的名称
    NSURL *newVideoUrl ; //一般.mp4
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];//用时间给文件全名，以免重复，在测试的时候其实可以判断文件是否存在若存在，则删除，重新生成文件即可
    [formater setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    newVideoUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mp4", [formater stringFromDate:[NSDate date]]]] ;//这个是保存在app自己的沙盒路径里，后面可以选择是否在上传后删除掉。我建议删除掉，免得占空间。
    //把视频从mov压缩转成mp4
    return [self convertVideoQuailtyWithInputURL:url outputURL:newVideoUrl videoFirstImage:scaledImage completeHandler:nil];
}
-(void) convertVideoQuailtyWithInputURL:(NSURL*)inputURL
                               outputURL:(NSURL*)outputURL videoFirstImage:(UIImage *)videoFirstImage
                         completeHandler:(void (^)(AVAssetExportSession*))handler
{
    
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    //  NSLog(resultPath);
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse= YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         if (exportSession.status==AVAssetExportSessionStatusCompleted) {
             NSData *videoData = [NSData dataWithContentsOfURL:outputURL];
             NSDictionary *videoDic=@{@"image":videoFirstImage,@"url":inputURL,@"videoData":videoData};
             if (self.delegate &&[self.delegate respondsToSelector:@selector(sendVideoDataWithDic:)]) {
                 [self.delegate sendVideoDataWithDic:videoDic];
             }
         }else{
            [XSTool showToastWithView:[UIApplication sharedApplication].keyWindow Text:@"视频转码失败"];
         }
     }];
}

@end
