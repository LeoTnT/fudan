//
//  SomeCanWatchViewController.m
//  App3.0
//
//  Created by mac on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SomeCanWatchViewController.h"
#import "FansListViewController.h"
#import "SetWatchPermissionViewController.h"
@interface SomeCanWatchViewController ()
/**确认按钮*/
@property(nonatomic,strong)UIButton *confirmBtn;
/**表格*/
@property(nonatomic,strong)UITableView *table;
@end

@implementation SomeCanWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置子视图
    [self setSubviews];
    self.view.backgroundColor=BG_COLOR;
}
#pragma mark-设置子视图
-(void)setSubviews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
         @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //确认按钮
    self.confirmBtn= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [self.confirmBtn setTitle:Localized(@"material_dialog_default_title") forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.confirmBtn];
    self.navigationItem.rightBarButtonItem = item;
    self.table=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 5*60)];
    self.table.backgroundColor=[UIColor whiteColor];
    self.table.dataSource=self;
    self.table.delegate=self;
    self.table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.table];
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID=@"cellID";
    UITableViewCell *cell;
    cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        cell.textLabel.font=[UIFont systemFontOfSize:16];
    }
    //内容
    if (indexPath.row==0) {
        cell.textLabel.text=@"我的粉丝";
    }else if(indexPath.row==1){
        cell.textLabel.text=@"一级会员";
    }else if(indexPath.row==2){
        cell.textLabel.text=@"二级会员";
    }else{
        cell.textLabel.text=@"三级会员";
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark-点击确认  返回，并传给前一个界面权限
-(void)confirm{
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[SetWatchPermissionViewController class]]) {
            ((SetWatchPermissionViewController *)vc).users=self.users;
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
    
}
#pragma mark-设置表格的行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
#pragma mark-点击某行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        FansListViewController *fansList=[[FansListViewController alloc] init];
        [self.navigationController pushViewController:fansList animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
