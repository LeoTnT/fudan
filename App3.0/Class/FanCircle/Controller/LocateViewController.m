//
//  MapTableViewController.m
//  One
//
//  Created by apple on 2017/4/19.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "LocateViewController.h"

@interface LocateViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)CLGeocoder *geoCoder;
/** 位置管理者 */
@property (nonatomic, strong) CLLocationManager *manager;

@property (nonatomic ,strong) MKMapView *mapView;
@property (nonatomic ,strong)NSMutableArray  *logisticsArr;
@property (nonatomic ,strong)UITableView *tableView;
@end

@implementation LocateViewController



{
    CLLocationCoordinate2D locationCenter;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.mapView removeFromSuperview];
    self.mapView.delegate=nil;
    self.manager=nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.manager.delegate = self;
    self.mapView.delegate = self;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, SCREEN_H_Height(300), self.view.frame.size.width, self.view.frame.size.height-SCREEN_H_Height(300)) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableArray *)logisticsArr {
    if (!_logisticsArr) {
        _logisticsArr = [NSMutableArray array];
    }
    return _logisticsArr;
}
- (CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}
#pragma mark - 懒加载
- (CLLocationManager *)manager
{
    if (!_manager) {
        _manager = [[CLLocationManager alloc] init];
        
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        [_manager setDistanceFilter:kCLDistanceFilterNone];
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            [_manager requestWhenInUseAuthorization];
        }
        

        
        
    }
    return _manager;
}



- (MKMapView *)mapView {
    if (!_mapView) {
        
        _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, SCREEN_H_Height(300))];
            _mapView.userTrackingMode = MKUserTrackingModeNone;
       
        [self.view addSubview:_mapView];
    }
    return _mapView;
}



#pragma mark - CLLocationManagerDelegate
/**
 *  更新到位置之后调用
 *
 *  @param manager   位置管理者
 *  @param locations 位置数组
 */
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"定位到了");
    
    
    CLLocation *location = [locations lastObject];
    //    CLLocationCoordinate2D coordinate = location.coordinate;
    
    NSLog(@" - -  - - -- %f  %f",location.coordinate.latitude,location.coordinate.longitude);
    CLLocationCoordinate2D coordinate;
    //判断是不是属于国内范围
    if (![WGS84TOGCJ02 isLocationOutOfChina:[location coordinate]]) {
        //转换后的coord
        
        
        coordinate = [WGS84TOGCJ02 transformFromWGSToGCJ:[location coordinate]];
        //        CLLocationCoordinate2D coordinate = location.coordinate;//经纬度
        NSLog(@"2222 - -  - - -- %f  %f",coordinate.latitude,coordinate.longitude);
    }
    
    [self getAddressByLatitude:coordinate.latitude longitude:coordinate.longitude];
    locationCenter = coordinate;
    [manager stopUpdatingLocation];
    
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(coordinate, 2000, 2000) animated:YES];
    MyAnnotation *pinAnnotation = [MyAnnotation new];
    pinAnnotation.coordinate = coordinate;
    [self.mapView addAnnotation:pinAnnotation];
    
}


// 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"定位失败");
}


- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    if (!locationCenter.latitude) return;
    
    MyAnnotation *pinAnnotation = [MyAnnotation new];
    pinAnnotation.coordinate = locationCenter;
    [self.mapView addAnnotation:pinAnnotation];
    
    
}


#pragma mark 根据坐标取得地名
-(void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude{
    
    //反地理编码
    CLLocation *location=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [self.geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (!error) {
            [self initialData:placemarks];
            [self getAroundInfoMationWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude)];
            [self.tableView reloadData];
            
        }else{
            //            haveGetUserLocation = NO;
            NSLog(@"error:%@",error.localizedDescription);
        }
        
    }];
}


#pragma mark - Initial Data
-(void)initialData:(NSArray *)places
{
    if ([self.logisticsArr count]  >0)    [self.logisticsArr removeAllObjects];
    
    
    LocationModel *model = [LocationModel new];
    model.name = @"不显示";
    model.isSelected = YES;
    [self.logisticsArr addObject:model];
    
    for (CLPlacemark *placemark in places) {
        
        
        LocationModel *model = [[LocationModel alloc]init];
        model.name = placemark.name;
        model.thoroughfare = placemark.thoroughfare;
        
        model.subThoroughfare = placemark.subThoroughfare;
        model.city = placemark.locality;
        model.coordinate = placemark.location.coordinate;
        model.isSelected = NO;
        [self.logisticsArr addObject:model ];
    }
    
    
    
}

-(void)getAroundInfomation:(NSArray *)array
{
    
    for (MKMapItem *item in array) {
        MKPlacemark * placemark = item.placemark;
        LocationModel *model = [[LocationModel alloc]init];
        model.name = placemark.name;
        model.thoroughfare = placemark.thoroughfare;
        model.subThoroughfare = placemark.subThoroughfare;
        model.city = placemark.locality;
        model.coordinate = placemark.location.coordinate;
        model.isSelected = NO;
        [self.logisticsArr addObject:model];
    }
    [self.tableView reloadData];
}


-(void)getAroundInfoMationWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 100, 100);
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc]init];
    request.region = region;
    request.naturalLanguageQuery = @"Restaurants";
    MKLocalSearch *localSearch = [[MKLocalSearch alloc]initWithRequest:request];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        if (!error) {
            [self getAroundInfomation:response.mapItems];
            
        }else{
            //            haveGetUserLocation = NO;
            NSLog(@"Quest around Error:%@",error.localizedDescription);
        }
    }];
}



// 每次添加大头针都会调用此方法  可以设置大头针的样式
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // 判断大头针位置是否在原点,如果是则不加大头针
    if([annotation isKindOfClass:[mapView.userLocation class]])return nil;
    if ([annotation isKindOfClass:[MyAnnotation class]]) {
        
        [self.mapView removeAnnotation:annotation];
    }
    
    return nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"获取位置";
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableFooterView = [UIView new];
    

    [self.manager startUpdatingLocation];


    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.mapView addGestureRecognizer:tap];
    
}

- (void)tapAction:(UITapGestureRecognizer  *)tap
{
    if ([self.logisticsArr count]  >0)  [self.logisticsArr removeAllObjects];
    
    
    CGPoint point = [tap locationInView:_mapView];
    locationCenter = [_mapView convertPoint:point toCoordinateFromView:_mapView];
    [self getAddressByLatitude:locationCenter.latitude longitude:locationCenter.longitude];
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(locationCenter, 2000, 3000) animated:YES];
    MyAnnotation *pinAnnotation = [MyAnnotation new];
    pinAnnotation.coordinate = locationCenter;
    [self.mapView addAnnotation:pinAnnotation];
    
    
}


static NSString *cellIdentifier = @"cell";


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.logisticsArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
        
    }
    
    LocationModel *model = self.logisticsArr[indexPath.row];
    if (model.isSelected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = model.name;
    cell.detailTextLabel.text = model.thoroughfare;
    return cell;
}


//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60.0)];
//    view.backgroundColor = self.tableView.backgroundColor;
//    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    indicatorView.center = view.center;
//    [indicatorView startAnimating];
//    [view addSubview:indicatorView];
//    
//    return  self.logisticsArr.count > 0 ? nil :view;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    //    [self.logisticsArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    //    LocationModel *model = self.logisticsArr[idx];
    //        if (idx == indexPath.row) {
    //            model.isSelected =   YES;
    //
    //        }else{
    //
    //            model.isSelected =  NO;
    //        }
    //
    //    }];
    
    //
    
    LocationModel *model = self.logisticsArr[indexPath.row];
    NSString *address = model.name;
    //    [self.tableView reloadData];
    if (_selectedIndexPath) {
        if (indexPath.row==0) {
            _selectedIndexPath(@"");//不显示
        }else{
            _selectedIndexPath(address?address:@"");
        }
        _selectedIndexPath = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    return  self.logisticsArr.count > 0 ? 0:60;
//}

//-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    NSLog(@"userLocation:longitude:%f---latitude:%f",userLocation.location.coordinate.longitude,userLocation.location.coordinate.latitude);
//    if (!haveGetUserLocation) {
//        if (self.mapView.userLocationVisible) {
//            haveGetUserLocation = YES;
//            [self getAddressByLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
//            [self addCenterLocationViewWithCenterPoint:self.mapView.center];
//        }
//
//    }
//}


@end


//@implementation MyAnnotation
//
//@end

@implementation LocationModel



@end



const double xsa = 6378245.0;
const double xsee = 0.00669342162296594323;
const double xspi = 3.14159265358979324;
const double xsx_pi = M_PI * 3000.0 / 180.0;

@implementation WGS84TOGCJ02

+(CLLocationCoordinate2D)transformFromWGSToGCJ:(CLLocationCoordinate2D)wgsLoc
{
    CLLocationCoordinate2D adjustLoc;
    if([self isLocationOutOfChina:wgsLoc]){
        adjustLoc = wgsLoc;
    }else{
        double adjustLat = [self transformLatWithX:wgsLoc.longitude - 105.0 withY:wgsLoc.latitude - 35.0];
        double adjustLon = [self transformLonWithX:wgsLoc.longitude - 105.0 withY:wgsLoc.latitude - 35.0];
        double radLat = wgsLoc.latitude / 180.0 * xspi;
        double magic = sin(radLat);
        magic = 1 - xsee * magic * magic;
        double sqrtMagic = sqrt(magic);
        adjustLat = (adjustLat * 180.0) / ((xsa * (1 - xsee)) / (magic * sqrtMagic) * xspi);
        adjustLon = (adjustLon * 180.0) / (xsa / sqrtMagic * cos(radLat) * xspi);
        adjustLoc.latitude = wgsLoc.latitude + adjustLat;
        adjustLoc.longitude = wgsLoc.longitude + adjustLon;
    }
    return adjustLoc;
}


+(CLLocationCoordinate2D)transformFromGCJToBaidu:(CLLocationCoordinate2D)p
{
    long double z = sqrt(p.longitude * p.longitude + p.latitude * p.latitude) + 0.00002 * sin(p.latitude * xsx_pi);
    long double theta = atan2(p.latitude, p.longitude) + 0.000003 * cos(p.longitude * xsx_pi);
    CLLocationCoordinate2D geoPoint;
    geoPoint.latitude = (z * sin(theta) + 0.006);
    geoPoint.longitude = (z * cos(theta) + 0.0065);
    return geoPoint;
}
//判断是不是在中国
+(BOOL)isLocationOutOfChina:(CLLocationCoordinate2D)location
{
    if (location.longitude < 72.004 || location.longitude > 137.8347 || location.latitude < 0.8293 || location.latitude > 55.8271) return YES;
    return NO;
}

+(double)transformLatWithX:(double)x withY:(double)y
{
    double lat = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * sqrt(fabs(x));
    lat += (20.0 * sin(6.0 * x * xspi) + 20.0 *sin(2.0 * x * xspi)) * 2.0 / 3.0;
    lat += (20.0 * sin(y * xspi) + 40.0 * sin(y / 3.0 * xspi)) * 2.0 / 3.0;
    lat += (160.0 * sin(y / 12.0 * xspi) + 3320 * sin(y * xspi / 30.0)) * 2.0 / 3.0;
    return lat;
}

+(double)transformLonWithX:(double)x withY:(double)y
{
    double lon = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * sqrt(fabs(x));
    lon += (20.0 * sin(6.0 * x * xspi) + 20.0 * sin(2.0 * x * xspi)) * 2.0 / 3.0;
    lon += (20.0 * sin(x * xspi) + 40.0 * sin(x / 3.0 * xspi)) * 2.0 / 3.0;
    lon += (150.0 * sin(x / 12.0 * xspi) + 300.0 * sin(x / 30.0 * xspi)) * 2.0 / 3.0;
    return lon;
}
+(CLLocationCoordinate2D)transformFromBaiduToGCJ:(CLLocationCoordinate2D)p

{
    
    double x = p.longitude - 0.0065, y = p.latitude - 0.006;
    
    double z = sqrt(x * x + y * y) - 0.00002 * sin(y * xsx_pi);
    
    double theta = atan2(y, x) - 0.000003 * cos(x * xsx_pi);
    
    CLLocationCoordinate2D geoPoint;
    
    geoPoint.latitude  = z * sin(theta);
    
    geoPoint.longitude = z * cos(theta);
    
    return geoPoint;
    
}

+(CLLocationCoordinate2D)transformFromGCJToWGS:(CLLocationCoordinate2D)p

{
    
    double threshold = 0.00001;
    
    // The boundary
    
    double minLat = p.latitude - 0.5;
    
    double maxLat = p.latitude + 0.5;
    
    double minLng = p.longitude - 0.5;
    
    double maxLng = p.longitude + 0.5;
    
    double delta = 1;
    
    int maxIteration = 30;
    
    // Binary search
    
    while(true)
        
    {
        
        CLLocationCoordinate2D leftBottom  = [[self class] transformFromWGSToGCJ:(CLLocationCoordinate2D){.latitude = minLat,.longitude = minLng}];
        
        CLLocationCoordinate2D rightBottom = [[self class] transformFromWGSToGCJ:(CLLocationCoordinate2D){.latitude = minLat,.longitude = maxLng}];
        
        CLLocationCoordinate2D leftUp      = [[self class] transformFromWGSToGCJ:(CLLocationCoordinate2D){.latitude = maxLat,.longitude = minLng}];
        
        CLLocationCoordinate2D midPoint    = [[self class] transformFromWGSToGCJ:(CLLocationCoordinate2D){.latitude = ((minLat + maxLat) / 2),.longitude = ((minLng + maxLng) / 2)}];
        
        delta = fabs(midPoint.latitude - p.latitude) + fabs(midPoint.longitude - p.longitude);
        
        if(maxIteration-- <= 0 || delta <= threshold)
            
        {
            
            return (CLLocationCoordinate2D){.latitude = ((minLat + maxLat) / 2),.longitude = ((minLng + maxLng) / 2)};
            
        }
        
        if(isContains(p, leftBottom, midPoint))
            
        {
            
            maxLat = (minLat + maxLat) / 2;
            
            maxLng = (minLng + maxLng) / 2;
            
        }
        
        else if(isContains(p, rightBottom, midPoint))
            
        {
            
            maxLat = (minLat + maxLat) / 2;
            
            minLng = (minLng + maxLng) / 2;
            
        }
        
        else if(isContains(p, leftUp, midPoint))
            
        {
            
            minLat = (minLat + maxLat) / 2;
            
            maxLng = (minLng + maxLng) / 2;
            
        }
        
        else
            
        {
            
            minLat = (minLat + maxLat) / 2;
            
            minLng = (minLng + maxLng) / 2;
            
        }
        
    }
    
}

static bool isContains(CLLocationCoordinate2D point, CLLocationCoordinate2D p1, CLLocationCoordinate2D p2)

{
    
    return (point.latitude >= MIN(p1.latitude, p2.latitude) && point.latitude <= MAX(p1.latitude, p2.latitude)) && (point.longitude >= MIN(p1.longitude,p2.longitude) && point.longitude <= MAX(p1.longitude, p2.longitude));
    
}
@end
