//
//  CommentViewController.h
//  App3.0
//
//  Created by mac on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : XSBaseViewController<UITextViewDelegate>
/**动态编号*/
@property(nonatomic,strong)NSNumber *topicId;
/**评论编号*/
@property(nonatomic,strong)NSNumber *replyId;
/**评论还是回复*/
@property(nonatomic,assign)BOOL commentOrNot;
@end
