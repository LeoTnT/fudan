//
//  SetWatchPermissionViewController.m
//  App3.0
//
//  Created by mac on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetWatchPermissionViewController.h"
#import "SomeCanWatchViewController.h"
#import "SomeNoCanWatchViewController.h"
#import "AddStatusViewController.h"
#import "AddVideoStatusViewController.h"
#import "FansListViewController.h"
#import "SetWatchPermissionCell.h"

@interface SetWatchPermissionViewController ()
/**确认按钮*/
@property(nonatomic,strong)UIButton *confirmBtn;
/**表格*/
@property(nonatomic,strong)UITableView *table;
@end

@implementation SetWatchPermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置子视图
    [self setSubviews];
    self.view.backgroundColor=BG_COLOR;
}
#pragma mark-设置子视图
-(void)setSubviews{
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //确认按钮
    self.confirmBtn= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [self.confirmBtn setTitle:Localized(@"material_dialog_default_title") forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.confirmBtn];
    self.navigationItem.rightBarButtonItem = item;
    self.table=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 5*60)];
    self.table.backgroundColor=[UIColor whiteColor];
    self.table.dataSource=self;
    self.table.delegate=self;
    self.table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.table];
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID=@"cellID";
    SetWatchPermissionCell *cell;
    cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[SetWatchPermissionCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        cell.textLabel.font=[UIFont systemFontOfSize:16];
        cell.detailTextLabel.font=[UIFont systemFontOfSize:14];
        cell.detailTextLabel.textColor=mainGrayColor;
    }
    //内容
    if (indexPath.row==0) {
        cell.textLabel.text=@"公开";
        cell.detailTextLabel.text=@"所有人可见";
    }else if(indexPath.row==1){
        cell.textLabel.text=@"私密";
        cell.detailTextLabel.text=@"仅自己可见";
    }else if(indexPath.row==2){
        cell.textLabel.text=@"部分可见";
        cell.detailTextLabel.text=@"选中的朋友可见";
    }else{
        cell.textLabel.text=@"不给谁看";
        cell.detailTextLabel.text=@"选中的朋友不可见";
    }
    if(indexPath.row==self.permissionType){
        cell.tipsImg.hidden=NO;
    }
    return cell;
}
#pragma mark-点击确认  返回，并传给前一个界面权限
-(void)confirm{
    AddStatusViewController *tempVC=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if(self.permissionType==permissionSomeCan||self.permissionType==permissionSomeNoCan){
        if (self.users.count==0) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"此选项需选定好友！" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }else{//传值
                tempVC.permissionType=self.permissionType;
                tempVC.permissionUsers=self.users;
        }
    }else{
        tempVC.permissionType=self.permissionType;
    }
    [self.navigationController popToViewController:tempVC animated:YES];
}
#pragma mark-设置表格的行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
#pragma mark-点击某行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //去除之前默认的图片
    for (int i=0; i<4; i++) {
        SetWatchPermissionCell *cell=[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        cell.tipsImg.hidden=YES;
    }
    SetWatchPermissionCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    cell.tipsImg.hidden=NO;
    if(indexPath.row==0){//公开
        self.permissionType=permissionPublic;
    }
    if(indexPath.row==1){//私密
        self.permissionType=permissionPrivacy;
    }
    if (indexPath.row==2) {
        self.permissionType=permissionSomeCan;
        FansListViewController *fansList=[[FansListViewController alloc] init];
        [self.navigationController pushViewController:fansList animated:YES];
        //        SomeCanWatchViewController *can=[[SomeCanWatchViewController alloc] init];
        //        [self.navigationController pushViewController:can animated:YES];
    }else if((indexPath.row==3)){
        self.permissionType=permissionSomeNoCan;
        //        SomeNoCanWatchViewController *noCan=[[SomeNoCanWatchViewController alloc] init];
        //        [self.navigationController pushViewController:noCan animated:YES];
        FansListViewController *fansList=[[FansListViewController alloc] init];
        [self.navigationController pushViewController:fansList animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
