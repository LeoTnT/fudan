//
//  FansCircleViewController.h
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface FansCircleViewController : XSBaseTableViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
/**进入用户主页更新的内容*/
@property(nonatomic,strong)NSDictionary *updateDic;
/**某条回复*/
@property(nonatomic,strong)Reply *reply;
@end
