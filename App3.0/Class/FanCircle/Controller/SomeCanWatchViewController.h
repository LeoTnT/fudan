//
//  SomeCanWatchViewController.h
//  App3.0
//
//  Created by mac on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface SomeCanWatchViewController : XSBaseViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray *users;
@end
