//
//  FansListViewController.m
//  App3.0
//
//  Created by mac  on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//


#import "FansListViewController.h"
#import "FansCircleModel.h"
#import "LoginModel.h"
#import "SomeCanWatchViewController.h"
#import "SomeNoCanWatchViewController.h"
#import "RemindViewController.h"
#import "FanListCell.h"
#import "SetWatchPermissionViewController.h"
#import "AddStatusViewController.h"
#import "AddVideoStatusViewController.h"

@interface FansListViewController ()
@property(nonatomic,strong)UITableView *table;
@end
@implementation FansListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSubViews];
}
#pragma mark-请求数据
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
#pragma mark-设置子视图
-(void)setSubViews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        @strongify(self);
        [self resign];
    }];
    self.navigationItem.title=Localized(@"me_contact_fans");
    UIButton *confirmBtn=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [confirmBtn setTitle:Localized(@"material_dialog_default_title") forState:UIControlStateNormal];
    [confirmBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(goFront) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:confirmBtn];
    self.navigationItem.rightBarButtonItem = item;
    self.view.backgroundColor=BG_COLOR;
    //表格
    self.table=[[UITableView alloc] initWithFrame:CGRectMake(0,STATUS_HEIGHT+navBarHeight, mainWidth, mainHeight-navBarHeight-STATUS_HEIGHT) style:UITableViewStylePlain];
    self.table.delegate=self;
    self.table.dataSource=self;
    self.table.hidden=YES;
    [self.view addSubview:self.table];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.chooseFans=[NSMutableArray array];
    [self getFansArray];
}
-(void)getFansArray{
    [XSTool showToastWithView:self.view Text:@"正在获取数据"];
    [HTTPManager getFansWithSuccess:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            self.fans=[Fans mj_objectWithKeyValues: dic].data;
            if(self.fans.count==0){
                [XSTool showToastWithView:self.view Text:@"您还没有粉丝哦"];
            }else{
                self.table.hidden=NO;
                [self.table reloadData];
            }
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError * error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.fans.count;
}
#pragma mark-放弃选择
-(void)resign{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"确定放弃选择联系人？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:confirm];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"normal";
     FanListCell *cell=[self.table dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[FanListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.fan=[self.fans objectAtIndex:indexPath.row];
//    //加手势
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addOrDeleteFan:)];
    [cell addGestureRecognizer:tap];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addOrDeleteFan:(UITapGestureRecognizer *)tap{
    FanListCell *cell=(FanListCell *)tap.view;
    cell.isSelected=!cell.isSelected;
    if (cell.isSelected==YES) {
        [cell.tipsButton setBackgroundImage:[UIImage imageNamed:@"reg_select"] forState:UIControlStateNormal];
        Fan *fan=[self.fans objectAtIndex:[self.table indexPathForCell:cell].row];
        [self.chooseFans addObject:fan];
    }else{
        [cell.tipsButton setBackgroundImage:[UIImage imageNamed:@"reg_unselect"] forState:UIControlStateNormal];
        Fan *fan=[self.fans objectAtIndex:[self.table indexPathForCell:cell].row];
        [self.chooseFans removeObject:fan];
    }
}

#pragma mark-返回
-(void)goFront{
    //    for (UIViewController *vc in self.navigationController.viewControllers) {
    ////        if ([vc isKindOfClass:[SomeCanWatchViewController class]]||[vc isKindOfClass:[SomeNoCanWatchViewController class]]||[vc isKindOfClass:[RemindViewController class]]) {
    ////            ((SomeCanWatchViewController *)vc).users=self.chooseFans;
    ////            [self.navigationController popToViewController:vc animated:YES];
    ////        }
    UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if ([vc isKindOfClass:[SetWatchPermissionViewController class]]) {
        ((SetWatchPermissionViewController *)vc).users=self.chooseFans;
        [self.navigationController popToViewController:vc animated:YES];
        return;
    }
    if ([vc isKindOfClass:[AddStatusViewController class]]||[vc isKindOfClass:[AddVideoStatusViewController class]]) {
//        ((AddVideoStatusViewController *)vc).remindUsers=self.chooseFans;
        [self.navigationController popToViewController:vc animated:YES];
        
    }
}
@end
