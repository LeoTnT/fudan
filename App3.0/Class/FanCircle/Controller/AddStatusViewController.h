//
//  AddStatusViewController.h
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYQAssetPickerController.h"
#import "FansCircleModel.h"
#import "SetWatchPermissionViewController.h"
@interface AddStatusViewController : XSBaseViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate>
/**公开类型*/
@property(nonatomic,assign)PermissionType permissionType;
/**屏蔽或者可见的粉丝*/
@property(nonatomic,strong)NSArray *permissionUsers;
///**提醒*/
//@property(nonatomic,strong)NSArray *remindUsers;
@property (nonatomic,assign) BOOL isShare;
@property(nonatomic,strong)FanCircleShare *share;


@end
