//
//  UserMainViewController.h
//  App3.0
//
//  Created by mac on 2017/4/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface UserMainViewController : XSBaseTableViewController
/**用户编号*/
@property(nonatomic,copy)NSString *uid;
/**用户编号*/
@property(nonatomic,copy)NSString *userName;
/**某条回复*/
@property(nonatomic,strong)Reply *reply;
@end
