//
//  AddStatusViewController.m
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddStatusViewController.h"
#import "SetWatchPermissionViewController.h"
#import "AddStatusTopCell.h"
#import "S_BaiduMap.h"
#import "FansListViewController.h"
#import "UIAlertView+XSAlertView.h"
#import "XSChoseImagePicker.h"
#import "GoodsDetailViewController.h"
#import "PersonViewController.h"
#import "PersonalViewController.h"
#import "S_StoreInformation.h"

@interface AddStatusViewController ()<MapLocationViewDelegate>
/**表格*/
@property(nonatomic,strong)UITableView *table;
/**存储图片的数组*/
@property(nonatomic,strong)NSMutableArray *photosArray;
/**发表按钮*/
@property(nonatomic,strong)UIButton *publishBtn;
/**顶部的cell*/
@property(nonatomic,strong)AddStatusTopCell *topCell;
/**顶部的cell的高度*/
@property(nonatomic,assign)CGFloat topCellHeight;
/**拍照控制器*/
@property(nonatomic,strong)UIImagePickerController *imagePickVC;
/**地址字符串*/
@property(nonatomic,copy)NSString *locationStr;
/**上传图片目录路径*/
@property(nonatomic,copy)NSMutableString *imagesUrlStr;
@property(nonatomic,assign) BOOL isAppered;
@end

@implementation AddStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=BG_COLOR;
    [self setSubViews];
}

-(NSMutableArray *)photosArray{
    if (!_photosArray) {
        _photosArray=[NSMutableArray arrayWithObject:[UIImage imageNamed:@"user_fans_addphoto_thin"]];
    }
    return _photosArray;
}

-(NSMutableString *)imagesUrlStr{
    if (!_imagesUrlStr) {
        _imagesUrlStr=[NSMutableString string];
    }
    return _imagesUrlStr;
}
#pragma mark-延迟返回
-(void)waitPop{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-添加子视图
-(void)setSubViews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"退出此次编辑？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:conAct];
        [alertVC addAction:cancelAct];
        [self presentViewController:alertVC animated:YES completion:nil];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //发表按钮
    self.publishBtn= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [self.publishBtn setTitle:@"发表" forState:UIControlStateNormal];
    [self.publishBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.publishBtn addTarget:self action:@selector(publishStatus) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.publishBtn];
    self.navigationItem.rightBarButtonItem = item;
    //添加谁可以看  位置  提醒谁看
    self.table=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) ];
    self.table.dataSource=self;
    self.table.delegate=self;
    self.table.backgroundColor=[UIColor whiteColor];
    self.table.tableFooterView=[[UIView alloc] init];
    [self.view addSubview:self.table];
    self.permissionType=permissionPublic;
}
#pragma mark-发表
-(void)publishStatus{
    [self.view endEditing:YES];
    //获取提醒用户列表
    NSString *remindUserIdStr=@"";
    //    NSMutableString *tempStr1=[NSMutableString string];
    //    for (int i=0; i<self.remindUsers.count; i++) {
    //        Fan *fan=[self.remindUsers objectAtIndex:i];
    //        [tempStr1 appendString:[NSString stringWithFormat:@"%@,",fan.uid]];
    //    }
    //    if (tempStr1.length) {
    //       remindUserIdStr=[tempStr1 substringToIndex:tempStr1.length-1];
    //    }
    //权限
    NSString *permitStr=[NSString stringWithFormat:@"%i",self.permissionType];
    NSString *permitUserIdStr=@"";
    switch (self.permissionType) {
        case 0:
            break;
        case 1:
            break;
        case 2:
        case 3:
        {
            NSMutableString *tempStr2=[NSMutableString string];
            for (int i=0; i<self.permissionUsers.count; i++) {
                Fan *fan=[self.permissionUsers objectAtIndex:i];
                [tempStr2 appendString:[NSString stringWithFormat:@"%@,",fan.uid]];
            }
            if (tempStr2.length) {
                permitUserIdStr=[tempStr2 substringToIndex:tempStr2.length-1];
            }
        }
            break;
        default:
            break;
    }
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *inputString = [[NSString alloc]initWithString:[self.topCell.inputView.text stringByTrimmingCharactersInSet:whiteSpace]];
    //去掉最后一张加号图片
    NSMutableArray *tempImageArray=[NSMutableArray arrayWithArray:self.photosArray];
    [tempImageArray removeLastObject];
    [XSTool showProgressHUDTOView:self.view withText:self.share?@"分享中":@"发表中"];
    if(self.share ||tempImageArray.count==0){
        NSDictionary *paraDic;
        if (self.share) {
            NSMutableString *shareStr=[NSMutableString stringWithString:@"{\"image_url\":\""];
            [shareStr appendString:self.share.image_url.length?self.share.image_url:@""];
            [shareStr appendString:@"\",\"head\":\""];
            [shareStr appendString:self.share.head.length?self.share.head:@""];
            [shareStr appendString:@"\",\"url\":\""];
            [shareStr appendString:self.share.url.length?self.share.url:@""];
            [shareStr appendString:@"\"}"];
            paraDic = @{@"content":inputString, @"position":self.locationStr.length?self.locationStr:@"", @"permit":permitStr, @"uid_str":permitUserIdStr,  @"uid_str_remind":@"", @"is_share":@1,@"share_info":shareStr};
        }else{
             paraDic=@{@"content":inputString,@"position":self.locationStr.length?self.locationStr:@"",@"permit":permitStr,@"uid_str":permitUserIdStr,@"uid_str_remind":@""};
        }
        [HTTPManager addStatusOnlyWordWithDic:paraDic WithSuccess:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if(state.status){
                if (self.share) {
                    [XSTool showToastWithView:self.view Text:@"分享成功"];
                }else{
                    [XSTool showToastWithView:self.view Text:@"发表成功"];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"fanCircleGetNewStatus"  object:nil userInfo:nil];
                [self performSelector:@selector(waitPop) withObject:nil afterDelay:0.25];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }else{
        NSDictionary *param=@{@"type":@"article",@"formname":@"file"};
        NSMutableString *tempStr=[NSMutableString string];
        [HTTPManager upLoadPhotosWithDic:param andDataArray:tempImageArray WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
            if(state){//发表
                NSArray *arr = dic[@"data"];
                for (int i = 0; i < arr.count; i++) {
                    NSString *str=dic[@"data"][i];
                    [tempStr appendString:[NSString stringWithFormat:@"%@,",str]];
                }
                NSDictionary *paraDic=[NSDictionary dictionaryWithObjectsAndKeys:inputString,@"content",[tempStr substringToIndex:tempStr.length-1],@"imgs",self.locationStr?self.locationStr:@"",@"position",permitStr,@"permit",permitUserIdStr,@"uid_str",remindUserIdStr,@"uid_str_remind",nil];
                [HTTPManager addStatusOnlyWordWithDic:paraDic WithSuccess:^(NSDictionary *dic, resultObject *state) {
                    [XSTool hideProgressHUDWithView:self.view];
                    if(state.status){
                        [XSTool showToastWithView:self.view Text:@"发表成功"];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"fanCircleGetNewStatus" object:nil userInfo:nil];
                         [self performSelector:@selector(waitPop) withObject:nil afterDelay:0.25];
                    }else{
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                } fail:^(NSError *error) {
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                }];
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }
}
#pragma mark-设置查看权限
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UITableViewCell *cell1=[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (self.permissionType==permissionPublic) {
        cell1.detailTextLabel.text=@"公开";
    }else if(self.permissionType==permissionPrivacy){
        cell1.detailTextLabel.text=@"仅自己可见";
    }else {
        NSMutableString *showStr=[NSMutableString stringWithString:self.permissionType==permissionSomeCan?@"可见：":@"不可见："];
        for (Fan *fan in self.permissionUsers) {
            [showStr appendString:[NSString stringWithFormat:@"%@，",fan.showname]];
        }
        cell1.detailTextLabel.text=showStr;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        static NSString *topCellID=@"top";
        AddStatusTopCell *topCell=[tableView dequeueReusableCellWithIdentifier:topCellID];
        if (!topCell) {
            topCell=[[AddStatusTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topCellID];
        }
        if (self.isShare) {
            topCell.share=self.share;
            [topCell.shareView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goUrl)]];
        }else{
            topCell.photosArray=self.photosArray;
            for (int i=0; i<topCell.deletBtnArray.count; i++) {
                [[topCell.deletBtnArray objectAtIndex:i] addTarget:self action:@selector(deletePicture:) forControlEvents:UIControlEventTouchUpInside];
            }
            [topCell.lastImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePhotos)]];
        }
        [topCell addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
            [self.view endEditing:YES];
        }]];
        self.topCellHeight=topCell.height;
        self.topCell=topCell;
        //给cell的删除按钮绑定方法
        return topCell;
    }else{
        static NSString *cellId=@"normalCell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        }
        if (indexPath.row==1) {
            cell.imageView.image=[UIImage imageNamed:@"user_fans_cansee"];
            cell.textLabel.text=@"谁可以看";
            cell.detailTextLabel.text=@"公开";
            if (self.permissionType==permissionPublic) {
                cell.detailTextLabel.text=@"公开";
            }else if(self.permissionType==permissionPrivacy){
                cell.detailTextLabel.text=@"仅自己可见";
            }else{
                NSMutableString *showStr=[NSMutableString stringWithString:self.permissionType==permissionSomeCan?@"可见":@"不可见"];
                for (Fan *fan in self.permissionUsers) {
                    [showStr appendString:[NSString stringWithFormat:@"%@，",fan.showname]];
                }
                cell.detailTextLabel.text=showStr;
            }
        }else if (indexPath.row==2){
            cell.textLabel.text=@"所在位置";
            cell.imageView.image=[UIImage imageNamed:@"user_fans_location"];
            if (self.locationStr) {
                cell.textLabel.text=self.locationStr;
                cell.imageView.image=[UIImage imageNamed:@"user_fans_location-1"];
            }
        }
        cell.detailTextLabel.font=[UIFont systemFontOfSize:15];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
#pragma mark-定位
-(void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address{
    self.locationStr=address;
    [self.table reloadData];
}
#pragma mark-删除图片
-(void)deletePicture:(UIButton *)button{
    NSInteger index=[self.topCell.deletBtnArray indexOfObject:button];
    [button removeFromSuperview];
    [self.photosArray removeObjectAtIndex:index];
    self.topCell.photosArray=self.photosArray;
    [self.table reloadData];
}
#pragma mark-设置行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return self.topCellHeight;
    }else{
        return 50;
    }
}
#pragma mark-选择照片
-(void)choosePhotos{
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cameraAct=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 9-self.photosArray.count+1;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *photoAct=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cameraAct];
    [alertVC addAction:photoAct];
    [alertVC addAction:cancelAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}
#pragma mark-点击空白，收起键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
#pragma mark-拍照
-(void)takeAPhoto{
    //拍照模式是否可用
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.imagePickVC=[[UIImagePickerController alloc] init];
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}
#pragma mark-拍照完毕
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    NSData *data = UIImageJPEGRepresentation(image, 0.0);
    //保存图片到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [self.photosArray insertObject:[UIImage imageWithData:data] atIndex:self.photosArray.count-1];
    [self dismissViewControllerAnimated:YES completion:nil];
    //刷新表格
    self.topCell.photosArray=self.photosArray;
    [self.table reloadData];
}

#pragma mark-取消
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark-点击某行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        [self.view endEditing:YES];
    }else if (indexPath.row==1) {
        SetWatchPermissionViewController *set=[[SetWatchPermissionViewController alloc] init];
        set.permissionType=self.permissionType;
        [self.navigationController pushViewController:set animated:YES];
    }else if (indexPath.row==2){//开始定位
        S_BaiduMap *locationController = [[S_BaiduMap alloc] init];
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        locationController.fatherVC=self;
        [self.navigationController pushViewController:locationController animated:YES];
    }
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        [asset setGetFullScreenImage:^(UIImage * result) {
            NSData *data = UIImageJPEGRepresentation(result, 0.0);
            [self.photosArray insertObject:[UIImage imageWithData:data] atIndex:self.photosArray.count-1];
            [self.table reloadData];
        }];
    }
}
-(void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker{
    [XSTool showToastWithView:picker.view Text:@"一次最多选择九张图片"];
}
-(void)goUrl{
    NSString *url=((AddStatusTopCell *)self.topCell).shareView.share.url;
    NSArray *paramArr = [url componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![url containsString:ImageBaseUrl] || arr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
