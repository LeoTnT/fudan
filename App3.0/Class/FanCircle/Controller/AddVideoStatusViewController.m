//
//  AddVideoStatusViewController.m
//  App3.0
//
//  Created by mac on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddVideoStatusViewController.h"
#import "AddVideoStatusTopCell.h"
#import "SetWatchPermissionViewController.h"
#import "RemindViewController.h"
#import "AddStatusTopCell.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import <AVFoundation/AVTime.h>
#import <AVKit/AVKit.h>
#import "S_BaiduMap.h"
#import "FansListViewController.h"
#import "RemindUserCell.h"

@interface AddVideoStatusViewController ()<CLLocationManagerDelegate>
@property(nonatomic,strong)UITableView *table;
@property(nonatomic,strong)UIButton *publishBtn;
/**全局的网络播放器*/
@property(nonatomic,strong)AVPlayer *avPlayer;
/**全局网络播放器用的播放条目*/
@property(nonatomic,strong)AVPlayerItem *playItem;
@property(nonatomic,strong)NSTimer *timer;
/**播放视频的控制器*/
@property(nonatomic,strong)AVPlayerViewController *playVC;
@property(nonatomic,strong)AddVideoStatusTopCell *topCell;
/**地址字符串*/
@property(nonatomic,copy)NSString *locationStr;
@end

@implementation AddVideoStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=BG_COLOR;
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setSubViews];
}

#pragma mark-添加子视图
-(void)setSubViews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:nil action:^{
        @strongify(self);
        [self.view endEditing:YES];
        UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:@"退出此次编辑？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *conAct=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:conAct];
        [alertVC addAction:cancelAct];
        [self presentViewController:alertVC animated:YES completion:nil];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //发表按钮
    self.publishBtn= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [self.publishBtn setTitle:@"发表" forState:UIControlStateNormal];
    [self.publishBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.publishBtn addTarget:self action:@selector(publishStatus) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.publishBtn];
    self.navigationItem.rightBarButtonItem = item;
    //添加谁可以看  位置  提醒谁看
    self.table=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight) ];
    self.table.dataSource=self;
    self.table.delegate=self;
    self.table.backgroundColor=[UIColor whiteColor];
    self.table.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.table];
    self.permissionType=permissionPublic;
}
#pragma mark-设置行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
#pragma mark-上传动态
-(void)publishStatus{
    //获取提醒用户列表
    NSString *remindUserIdStr=@"";
    //    NSMutableString *tempStr1=[NSMutableString string];
    //    for (int i=0; i<self.remindUsers.count; i++) {
    //        Fan *fan=[self.remindUsers objectAtIndex:i];
    //        [tempStr1 appendString:[NSString stringWithFormat:@"%@,",fan.uid]];
    //    }
    //    if (tempStr1.length) {
    //        remindUserIdStr=[tempStr1 substringToIndex:tempStr1.length-1];
    //    }
    //权限
    NSString *permitStr=[NSString stringWithFormat:@"%i",self.permissionType];
    NSString *userIdStr;
    switch (self.permissionType) {
        case 0:
        case 1:
            userIdStr=@"";
            break;
        case 2:
        case 3:
        {
            NSMutableString *tempStr=[NSMutableString string];
            for (int i=0; i<self.permissionUsers.count; i++) {
                Fan *fan=[self.permissionUsers objectAtIndex:i];
                [tempStr appendString:[NSString stringWithFormat:@"%@,",fan.uid]];
            }
            userIdStr=[tempStr substringToIndex:tempStr.length-1];
        }
            break;
        default:
            break;
    }
    NSDictionary *param=@{@"type":@"v_fans",@"formname":@"file"};
    [XSTool showProgressHUDTOView:self.view withText:@"发表中"];
    AddVideoStatusTopCell *cell=[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    @weakify(self);
    [HTTPManager upLoadDataIsVideo:YES WithDic:param andData:self.videoData WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
        @strongify(self);
        if(state){
            //发表动态
            //主题视频，缩略图目录，逗号分隔
            NSDictionary *paraDic=[NSDictionary dictionaryWithObjectsAndKeys:cell.inputView.text,@"content",dic[@"data"][@"video"],@"videos",self.locationStr?self.locationStr:@"",@"position",permitStr,@"permit",userIdStr,@"uid_str",remindUserIdStr,@"uid_str_remind",dic[@"data"][@"image"],@"videos_img",nil];
            [HTTPManager addStatusOnlyWordWithDic:paraDic WithSuccess:^(NSDictionary *dic, resultObject *state) {
                [XSTool hideProgressHUDWithView:self.view];
                if(state.status){
                    [XSTool showToastWithView:self.view Text:@"发表成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"fanCircleGetNewStatus" object:nil userInfo:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-设置cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        static NSString *topCellID=@"top";
        AddVideoStatusTopCell *topCell=[tableView dequeueReusableCellWithIdentifier:topCellID];
        if (!topCell) {
            topCell=[[AddVideoStatusTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topCellID];
        }
        topCell.image=self.image;
        [topCell.playBtn addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        self.topCell=topCell;
        return topCell;
    }else{
        static NSString *cellId=@"normalCell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        }
        if (indexPath.row==1) {
            cell.imageView.image=[UIImage imageNamed:@"user_fans_cansee"];
            cell.textLabel.text=@"谁可以看";
            cell.detailTextLabel.text=@"公开";
            if (self.permissionType==permissionPublic) {
                cell.detailTextLabel.text=@"公开";
            }else if(self.permissionType==permissionPrivacy){
                cell.detailTextLabel.text=@"仅自己可见";
            }else{
                NSMutableString *showStr=[NSMutableString stringWithString:self.permissionType==permissionSomeCan?@"可见：":@"不可见："];
                for (Fan *fan in self.permissionUsers) {
                    [showStr appendString:[NSString stringWithFormat:@"%@，",fan.showname]];
                }
                cell.detailTextLabel.text=showStr;
            }
        }else if (indexPath.row==2){
            cell.textLabel.text=@"所在位置";
            cell.imageView.image=[UIImage imageNamed:@"user_fans_location"];
            if (self.locationStr) {
                cell.textLabel.text=self.locationStr;
                cell.imageView.image=[UIImage imageNamed:@"user_fans_location-1"];
            }
        }
        cell.detailTextLabel.font=[UIFont systemFontOfSize:15];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
#pragma mark-定位
-(void)sendLocationLatitude:(CLLocationCoordinate2D)coordinate andAddress:(NSString *)address{
    self.locationStr=address;
    [self.table reloadData];
}
#pragma mark-设置查看权限
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UITableViewCell *cell=[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (self.permissionType==permissionPublic) {
        cell.detailTextLabel.text=@"公开";
    }else if(self.permissionType==permissionPrivacy){
        cell.detailTextLabel.text=@"仅自己可见";
    }else{
        NSMutableString *showStr=[NSMutableString stringWithString:self.permissionType==permissionSomeCan?@"可见":@"不可见"];
        for (Fan *fan in self.permissionUsers) {
            [showStr appendString:[NSString stringWithFormat:@"%@，",fan.showname]];
        }
        cell.detailTextLabel.text=showStr;
    }
}
#pragma mark-设置行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 270;
    }else{
        return 50;
    }
}
#pragma mark-点击空白，收起键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
#pragma mark-点击某行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        [self.view endEditing:YES];
    }else if (indexPath.row==1) {
        SetWatchPermissionViewController *set=[[SetWatchPermissionViewController alloc] init];
        set.permissionType=self.permissionType;
        [self.navigationController pushViewController:set animated:YES];
    }else if (indexPath.row==2){
        S_BaiduMap *locationController = [[S_BaiduMap alloc] init];
        locationController.fatherVC=self;
        locationController.showType = BMKShowSearchList;
        locationController.delegate = self;
        [self.navigationController pushViewController:locationController animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)playVideo{
    self.playVC=[[AVPlayerViewController alloc] init];
    AVPlayerItem *item=[[AVPlayerItem alloc] initWithURL:self.url];
    self.playVC.player=[[AVPlayer alloc] initWithPlayerItem:item];
    self.playVC.showsPlaybackControls=YES;
    [self presentViewController:self.playVC animated:YES completion:nil];
}
@end
