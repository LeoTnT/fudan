//
//  FansCircleViewController.m
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FansCircleViewController.h"
#import "FansCircleTopCell.h"
#import "FansCircleStatusCell.h"
#import "AddVideoStatusViewController.h"
#import "AddStatusViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "LoginModel.h"
#import "ReplyView.h"
#import "CLPlayerView.h"
#import "UserModel.h"
#import "IdentityCardModel.h"
#import "UserMainViewController.h"
#import "TOCropViewController.h"
#import "XSClipImage.h"
#import "ChatKeyBoard.h"
#import "FaceSourceManager.h"
#import "VideoHandleModel.h"
#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "PersonViewController.h"
#import "PersonalViewController.h"

@interface FansCircleViewController ()<TOCropViewControllerDelegate, ChatKeyBoardDelegate, ChatKeyBoardDataSource,VideoHandleModelDelegate>
/**记录被点击的评论点赞的cell*/
@property(nonatomic,strong)FansCircleStatusCell *tempCell;
/**记录点击过评论点赞的cell*/
@property(nonatomic,strong)NSMutableArray *praiseAndCommentBtnArray;
/**评论或者回复的某条动态*/
@property(nonatomic,strong)FansCircleStatusCell *replyOrCommentCell;
/**保存cell高度的字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
/**视频控制器*/
@property(nonatomic,strong)UIImagePickerController *videoPickerVC;
/**图片控制器*/
@property(nonatomic,strong)UIImagePickerController *imagePickerVC;
/**用户信息*/
@property(nonatomic,strong)LoginDataParser *loginDataParser;
/**记录全局的动态页数*/
@property(nonatomic,assign)int page;
/**动态数组*/
@property(nonatomic,strong)NSMutableArray  *statusArray;
@property (strong, nonatomic) ChatKeyBoard *keyBoard;
/**评论还是回复*/
@property(nonatomic,assign)BOOL commentOrNot;
/**动态编号*/
@property(nonatomic,strong)NSNumber *topicId;
/**评论编号*/
@property(nonatomic,strong)NSNumber *replyId;
/**粉丝圈背景图片*/
@property(nonatomic,strong)UIImage *backImage;
//图片播放器
@property(nonatomic,strong)CLPlayerView *playerView;
@end

@implementation FansCircleViewController
#pragma mark-Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.autoHideKeyboard = YES;
    //注册接受发送动态的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewStatusArray) name:@"fanCircleGetNewStatus" object:nil];
    //设置子视图
    [self setSubViews];
    //加载数据
    [self getUserInfo];
    [self.tableView.mj_header beginRefreshing];
    //    [self getNewStatusArray];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    if(self.updateDic){
        NSArray *deleteArray=[self.updateDic valueForKey:@"delete"];
        [deleteArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger topicID = [deleteArray[idx] integerValue];
            [self.statusArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                Status *sta = self.statusArray[idx];
                if ([sta.topic_id integerValue] == topicID) {
                    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:idx inSection:1];
                    if (indexPath.row == idx) {
                        NSLog(@"  - - -=- = %@ ",indexPath);
                        [self.statusArray removeObjectAtIndex:idx];
                        if (indexPath.section == 1) {
                            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                        }
                    }
                }
            }];
        }];
        NSArray *updateArray=[self.updateDic valueForKey:@"update"];
        [updateArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            Status *status=updateArray[idx];
            [self.statusArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                Status *s=self.statusArray[idx];
                if ([s.topic_id integerValue]==[status.topic_id integerValue]){
                    FansCircleStatusCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:1]];
                    cell.status=status;
                    self.statusArray[idx]=status;//!!!更新数据源
                }
            }];
        }];
        [self.tableView reloadData];
        self.updateDic=nil;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    if (self.playerView) {
        [self.playerView pausePlay];
        [self.playerView endPlay:^{
            [self.playerView destroyPlayer];
        }];
        self.playerView=nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark-Lazy Loading
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}
-(NSMutableArray *)statusArray{
    if (!_statusArray) {
        _statusArray=[NSMutableArray array];
    }
    return _statusArray;
}


#pragma mark-Private
-(void)lookPartOrNot:(UIButton *)btn{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview;
    if ([btn.titleLabel.text isEqualToString:@"全文"]) {
        //展开
        cell.lookAll=YES;
    }else{
        //收起
        cell.lookAll=NO;
    }
    [self.tableView reloadData];
}
-(void)getUserInfo{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [user objectForKey:USERINFO_LOGIN];
    LoginDataParser *loginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    self.loginDataParser=loginInfo;
    @weakify(self);
    [HTTPManager getOtherInfoWithUserName:loginInfo.username success:^(NSDictionary * _Nullable dic, resultObject *state) {
        @strongify(self);
        if ([dic[@"status"] isEqual:@1]) {
            UserDataParser *parser1=[UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            FansCircleTopCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            cell.nickAndAvatarAndBgImgDic=@{@"nickName":parser1.nickname?parser1.nickname:@"",@"avatar":parser1.logo?parser1.logo:@"",@"bgImg":parser1.logo_fandom_bg?parser1.logo_fandom_bg:@"",@"placeHoldImg":self.backImage?self.backImage:[UIImage imageNamed:@"user_fans_bg"]};
            //开启子线程  获取当前背景图
            [self saveBackImageWithUrlStr:parser1.logo_fandom_bg];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        @strongify(self);
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)setSubViews{
    self.navigationItem.title=@"粉丝圈";
    self.view.backgroundColor = BG_COLOR;
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    //相机按钮
    UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 25, 25)];
    [addBtn setBackgroundImage:[UIImage imageNamed:@"user_addStatus_gray"] forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(chooseAddStatus) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    self.navigationItem.rightBarButtonItem = item;
    //表格
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    
    [self setRefreshHeader:^{
        @strongify(self);
        [self getNewStatusArray];
    }];
    [self setRefreshFooter:^{
        @strongify(self);
        [self getOldStatusArray];
    }];
    
    self.praiseAndCommentBtnArray=[NSMutableArray array];
    self.keyBoard = [ChatKeyBoard keyBoard];
    self.keyBoard.keyBoardStyle = KeyBoardStyleComment;
    self.keyBoard.associateTableView = self.tableView;
    self.keyBoard.delegate = self;
    self.keyBoard.dataSource = self;
    self.keyBoard.allowVoice = NO;
    self.keyBoard.allowMore = NO;
    self.keyBoard.allowFace = NO;
    self.keyBoard.allowSend = YES;
    [self.view addSubview:self.keyBoard];
}

- (void)refreshAfterCommitOrReply {
    Status *status=self.replyOrCommentCell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:status.reply];
    [tempArray addObject:self.reply];
    status.reply=tempArray;
    self.replyOrCommentCell.status=status;
    [self.tableView reloadData];
    self.replyOrCommentCell=nil;
    self.reply=nil;
}

-(void)getOldStatusArray{
    self.page++;
    @weakify(self);
    [HTTPManager getOldStatusArrayWithPageNum:@(self.page) WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        if(state.status){
            FansCircleModel *model=[FansCircleModel mj_objectWithKeyValues:dic];
            if (model.data.count==0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多内容"];
            }else{
                [self.statusArray addObjectsFromArray:model.data];
                [self.tableView reloadData];
            }
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        self.page--;
    }];
}
-(void)getNewStatusArray{
    self.page=1;
    //     [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
    @weakify(self);
    [HTTPManager getNewStatusArrayWithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if(state.status){
            [self.statusArray removeAllObjects];
            FansCircleModel *model=[FansCircleModel mj_objectWithKeyValues:dic];
            if (model.data.count==0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多内容"];
            } else {
                [self.statusArray addObjectsFromArray:model.data];
            }
            [self.tableView reloadData];
            self.tableView.contentOffset=CGPointZero;
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
-(void)goToUserMainController:(UIButton *)btn{
    UserMainViewController *userVC=[[UserMainViewController alloc] init];
    userVC.hidesBottomBarWhenPushed=YES;
    if ([btn.superview.superview isKindOfClass:[FansCircleTopCell class]]) {
        [self.navigationController pushViewController:userVC animated:YES];
    }else{
        FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview;
        if ([cell.status.is_owner integerValue]==1) {
            [self.navigationController pushViewController:userVC animated:YES];
        }else{
            PersonalViewController *personVC=[[PersonalViewController alloc] initWithUid:cell.status.uid];
            personVC.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:personVC animated:YES];
        }
    }
}
-(void)replyOrDeleteComment:(UIButton *)btn{
    ReplyView *view=(ReplyView *)btn.superview;
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview.superview.superview;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.reply];
    if ([btn.titleLabel.text isEqualToString:@"回复"]) {
        //        CommentViewController *comment=[[CommentViewController alloc] init];
        //        comment.commentOrNot=NO;
        //        comment.replyId=view.reply.reply_id;
        //        replyOrCommentCell=cell;
        //        [self.navigationController pushViewController:comment animated:YES];
        self.replyOrCommentCell=cell;
        self.commentOrNot = NO;
        self.replyId=view.reply.reply_id;
        self.keyBoard.placeHolder = [NSString stringWithFormat:@"回复%@:",view.reply.from_nickname];
        [self.keyBoard keyboardUpforComment];
    }else{
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil  preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [XSTool showProgressHUDTOView:self.view withText:nil];
            @weakify(self);
            [HTTPManager deleteCommentWithId:view.reply.reply_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [tempArray removeObject:view.reply];
                    cell.status.reply=tempArray;
                    [self.tableView reloadData];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:confirm];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void)playVideo:(UIButton *)btn{
    //之前的停止播放
    if (self.playerView) {
        [self.playerView destroyPlayer];
    }
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview.superview;
    NSURL *videoUrl=[NSURL URLWithString:((Video *)cell.status.videos[0]).video];
    self.playerView = [[CLPlayerView alloc] initWithFrame:cell.videoImageView.frame];
    [cell.contentView addSubview:self.playerView];
    //视频地址
    self.playerView.url =videoUrl;
    self.playerView.fillMode=ResizeAspect;
    self.playerView.autoFullScreen=NO;
    //播放
    [self.playerView playVideo];
    //播放完成回调
    [self.playerView endPlay:^{
        //销毁播放器
        [self.playerView destroyPlayer];
    }];
}
-(void)changeBackImage{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"修改背景图片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    self.imagePickerVC=[[UIImagePickerController alloc] init];
    self.imagePickerVC.delegate=self;
    UIAlertAction *photoAct=[UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePickerVC animated:YES completion:nil];
    }];
    UIAlertAction *cameraAct=[UIAlertAction actionWithTitle:@"拍一张" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus ==ALAuthorizationStatusRestricted ||authStatus ==ALAuthorizationStatusDenied)  {
            // 无权限 引导去开启
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication]canOpenURL:url]) {
                [[UIApplication sharedApplication]openURL:url];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else{
            self.imagePickerVC.sourceType =UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePickerVC animated:YES completion:nil];
        }
    }];
    UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:photoAct];
    [alert addAction:cameraAct];
    [alert addAction:cancelAct];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)addComment:(UIButton *)button{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview.superview;
    cell.praiseAndConmmentViewHidden=YES;
    [self.praiseAndCommentBtnArray removeObject:cell];
    self.tempCell=nil;
    self.replyOrCommentCell=cell;
    self.topicId=cell.status.topic_id;
    self.commentOrNot = YES;
    self.keyBoard.placeHolder = @"";
    [self.keyBoard keyboardUpforComment];
    //    CommentViewController *comment=[[CommentViewController alloc] init];
    //    comment.commentOrNot=YES;
    //    comment.topicId=cell.status.topic_id;
    //    replyOrCommentCell=cell;
    //    [self.navigationController pushViewController:comment animated:YES];
}
-(void)addPraise:(UIButton *)button{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview.superview;
    Status *status=cell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.thumbs_up_users];
    cell.praiseBtn.imageView.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.75 delay:0 options:0 animations: ^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
        }];
        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    } completion:^(BOOL finished) {
        cell.praiseAndConmmentViewHidden=YES;
        [self.praiseAndCommentBtnArray removeObject:cell];
        self.tempCell=nil;
        if ([cell.status.is_thumbs_up isEqual:@1]) {//取消点赞
            @weakify(self);
            [HTTPManager cancelThumbUpWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state.status) {
                    for (int i=0; i<cell.status.thumbs_up_users.count; i++) {
                        Thumbs_up_user *user=[cell.status.thumbs_up_users objectAtIndex:i];
                        if ([user.username isEqualToString:self.loginDataParser.username]) {
                            [tempArray removeObject:user];
                        }
                    }
                    status.thumbs_up_users=tempArray;
                    status.is_thumbs_up=@0;
                    [self.tableView reloadData];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }else{//点赞
            @weakify(self);
            [HTTPManager thumbUpWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state.status) {
                    Thumbs_up_user *user=[Thumbs_up_user mj_objectWithKeyValues:@{@"username":self.loginDataParser.username,@"nikename":self.loginDataParser.nickname}];
                    [tempArray addObject:user];
                    status.thumbs_up_users=tempArray;
                    status.is_thumbs_up=@1;
                    [self.tableView reloadData];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }
    }];
}
-(void)chooseAddStatus{
    UIAlertController *alertVC=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cameraAct=[UIAlertAction actionWithTitle:@"拍摄视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self video];
    }];
    UIAlertAction *photoAct=[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self addStatus];
    }];
    UIAlertAction *cancelAct=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cameraAct];
    [alertVC addAction:photoAct];
    [alertVC addAction:cancelAct];
    [self presentViewController:alertVC animated:YES completion:nil];
}
-(void)deleteStatus:(UIButton *)deleteBtn{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        FansCircleStatusCell *cell=(FansCircleStatusCell *)deleteBtn.superview.superview;
        [XSTool showProgressHUDTOView:self.view withText:nil];
        @weakify(self);
        [HTTPManager deleteStatusWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if(state.status){
                [self.statusArray removeObject:cell.status];
                [self.tableView reloadData];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showPraiseOrCommentBtn:(UIButton *)button{
    button.selected=!button.selected;
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview;
    if (button.selected) {
        self.tempCell=cell;
        //其他的评论点赞按钮隐藏
        for (int i=0; i<self.praiseAndCommentBtnArray.count; i++) {
            FansCircleStatusCell *cell1=[self.praiseAndCommentBtnArray objectAtIndex:i];
            if (![cell1 isEqual:self.tempCell]) {
                cell1.praiseAndConmmentViewHidden=YES;
                [self.praiseAndCommentBtnArray removeObject:cell1];
            }
        }
        [self.praiseAndCommentBtnArray addObject:cell];
        cell.praiseAndConmmentViewHidden=NO;
    }else{
        cell.praiseAndConmmentViewHidden=YES;
        self.tempCell=nil;
        [self.praiseAndCommentBtnArray removeObject:self.tempCell];
    }
}
-(void)addStatus{
    AddStatusViewController *add=[[AddStatusViewController alloc] init];
    [self.navigationController pushViewController:add animated:YES];
}
-(void)sendVideoDataWithDic:(NSDictionary *)videoDic{
    AddVideoStatusViewController *add=[[AddVideoStatusViewController alloc] init];
    add.image=videoDic[@"image"];
    add.url=videoDic[@"url"];
    add.videoData=videoDic[@"videoData"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:add animated:YES];
        [self.videoPickerVC dismissViewControllerAnimated:NO completion:nil];
    });
}
-(void)goUrl:(UITapGestureRecognizer *)tap{
    UIView *tempView=tap.view;
    if ([tempView isKindOfClass:[UILabel class]]) {//内容链接跳转
        [self cellContentUrlDidClick:[((UILabel *)tempView).attributedText.string urlSubString]];
    }else{//分享跳转
        [self cellContentUrlDidClick:[((FanCircleShareView *)tempView).share.url urlSubString]];
    }
}
- (void)cellContentUrlDidClick:(NSString *)url {
    NSArray *paramArr = [url componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![url containsString:ImageBaseUrl] || arr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}
-(void)video{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.videoPickerVC=[[UIImagePickerController alloc] init];
    //数据源
    self.videoPickerVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    self.videoPickerVC.mediaTypes=@[(NSString *)kUTTypeMovie];
    //展示拍照控板
    self.videoPickerVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.videoPickerVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModeVideo;
    //后置摄像头
    self.videoPickerVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.videoPickerVC.delegate=self;
    //视频最大时间
    self.videoPickerVC.videoMaximumDuration=10;
    self.videoPickerVC.videoQuality=UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:self.videoPickerVC animated:YES completion:nil];
}

-(void)saveBackImageWithUrlStr:(NSString *)urlStr{
    if (urlStr.length==0) {
        self.backImage=[UIImage imageNamed:@"user_fans_bg"];
    }else{
        dispatch_queue_t queue = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue, ^{
            NSData *data = [NSData  dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
            self.backImage=[UIImage imageWithData:data];
        });
        
    }
}

#pragma mark -- ChatKeyBoardDataSource
- (NSArray<ChatToolBarItem *> *)chatKeyBoardToolbarItems
{
    ChatToolBarItem *item1 = [ChatToolBarItem barItemWithKind:kBarItemFace normal:@"face" high:@"face_HL" select:@"keyboard"];
    ChatToolBarItem *item4 = [ChatToolBarItem barItemWithKind:kBarItemSwitchBar normal:@"switchDown" high:nil select:nil];
    return @[item1, item4];
}

- (NSArray<FaceThemeModel *> *)chatKeyBoardFacePanelSubjectItems
{
    return [FaceSourceManager loadFaceSource];
}

- (void)chatKeyBoardTextViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)chatKeyBoardSendText:(NSString *)text{
    [self.view endEditing:YES];
    if (self.commentOrNot==YES) {//评论
        @weakify(self);
        [HTTPManager commentWithId:self.topicId andContent:text WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                self.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                [self refreshAfterCommitOrReply];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }else{//回复
        @weakify(self);
        [HTTPManager replyWithId:self.replyId andContent:text WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                self.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                [self refreshAfterCommitOrReply];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }
}

#pragma mark-TableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return self.statusArray.count;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (indexPath.section==0) {
        static NSString *cellId=@"topCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[FansCircleTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeBackImage)];
            [((FansCircleTopCell *)cell).backImage addGestureRecognizer:tap];
            [((FansCircleTopCell *)cell).headImg addTarget:self action:@selector(goToUserMainController:) forControlEvents:UIControlEventTouchUpInside];
        }
    }else{
        static NSString *cellId=@"statusCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[FansCircleStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        ((FansCircleStatusCell *)cell).currentVC=self;
        ((FansCircleStatusCell *)cell).status=[self.statusArray objectAtIndex:indexPath.row];
        [((FansCircleStatusCell *)cell).deleteBtn addTarget:self action:@selector(deleteStatus:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).praiseAndCommentClearBtn addTarget:self action:@selector(showPraiseOrCommentBtn:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).commentBtn addTarget:self action:@selector(addComment:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).praiseBtn addTarget:self action:@selector(addPraise:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).headImgBtn addTarget:self action:@selector(goToUserMainController:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).contentLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goUrl:)]];
        [((FansCircleStatusCell *)cell).lookPartOrAllBtn addTarget:self action:@selector(lookPartOrNot:) forControlEvents:UIControlEventTouchUpInside];
        for (UIButton *btn in ((FansCircleStatusCell *)cell).replyOrDeleteCommentBtnArray) {
            [btn addTarget:self action:@selector(replyOrDeleteComment:) forControlEvents:UIControlEventTouchUpInside];
        }
        if(((FansCircleStatusCell *)cell).playVideoBtn){
            [((FansCircleStatusCell *)cell).playVideoBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        }
        if(((FansCircleStatusCell *)cell).shareView){
            [((FansCircleStatusCell *)cell).shareView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goUrl:)]];
        }
        //记录高度
        self.heightDic[@(indexPath.row)]=@(((FansCircleStatusCell *)cell).height);
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 200;
    }else{
        if ([self.heightDic[@(indexPath.row)] floatValue]==0) {
            return 0.1;
        }
        return [self.heightDic[@(indexPath.row)] floatValue];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 10;
    }else{
        return 0.1;
    }
}
#pragma mark-ScrollView Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.playerView) {
        [self.playerView destroyPlayer];
    }
}
#pragma mark-ImagePickerController Delegate
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self dismissViewControllerAnimated:YES completion:^{
        [XSTool showProgressHUDTOView:self.view withText:nil];
        //上传图片  压缩图片和粉丝圈背景图一样高
        @weakify(self);
        NSDictionary *param=@{@"type":@"background",@"formname":@"file"};
        [HTTPManager upLoadDataIsVideo:NO WithDic:param andData:[XSClipImage scaleImage:image toScale:0.8]  WithSuccess:^(NSDictionary * _Nullable dic, BOOL state) {
            @strongify(self);
            if (state) {
                NSString *file=dic[@"data"];
                @weakify(self);
                [HTTPManager updateUserFancircleBgImgWithImgStr:file WithSuccess:^(NSDictionary *dic, resultObject *state) {
                    @strongify(self);
                    [XSTool hideProgressHUDWithView:self.view];
                    if (state.status) {
                        [self getUserInfo];
                    }else{
                        [XSTool showToastWithView:self.view Text:dic[@"info"]];
                    }
                } fail:^(NSError *error) {
                    @strongify(self);
                    [XSTool hideProgressHUDWithView:self.view];
                    [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
                }];
            }else{
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    if([picker isEqual:self.imagePickerVC]){
        UIImage *image=info[UIImagePickerControllerOriginalImage];
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
        cropViewController.delegate =self;
        CGFloat percent=mainWidth/200;
        cropViewController.customAspectRatio=CGSizeMake(percent, 1);
        [picker pushViewController:cropViewController animated:YES];
    }else{
        //视频路径
        NSURL *url=[info objectForKey:@"UIImagePickerControllerMediaURL"];
        VideoHandleModel *model=[[VideoHandleModel alloc]init];
        model.delegate=self;
        [model getVideoFirstImageAndDataAndUrlWithVideoUrl:url];
    }
}
@end
