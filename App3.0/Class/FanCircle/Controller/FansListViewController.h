//
//  FansListViewController.h
//  App3.0
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface FansListViewController : XSBaseViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray *fans;
@property(nonatomic,strong)NSMutableArray *chooseFans;
@end
