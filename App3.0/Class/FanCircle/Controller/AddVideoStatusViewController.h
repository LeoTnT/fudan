//
//  AddVideoStatusViewController.h
//  App3.0
//
//  Created by mac on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
#import "SetWatchPermissionViewController.h"
@interface AddVideoStatusViewController : XSBaseViewController<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
/**公开类型*/
@property(nonatomic,assign)PermissionType permissionType;
/**屏蔽或者可见的粉丝*/
@property(nonatomic,strong)NSArray *permissionUsers;
///**提醒*/
//@property(nonatomic,strong)NSArray *remindUsers;
/**视频缩略图*/
@property(nonatomic,strong)UIImage *image;
/**路径*/
@property(nonatomic,strong)NSURL *url;
/**视频数据*/
@property(nonatomic,strong)NSData *videoData;
@end
