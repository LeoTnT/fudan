//
//  CommentViewController.m
//  App3.0
//
//  Created by mac on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CommentViewController.h"
#import "FansCircleModel.h"
#import "FansCircleViewController.h"
#import "UserMainViewController.h"

@interface CommentViewController ()
{
    /**输入框*/
    UITextView *inputTextView;
    UIView *inputView;
    UIButton *publishBtn;
    /**提示文本*/
    UILabel *tipsLabel;
}
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置子视图
    [self setSubViews];
    self.view.backgroundColor=BG_COLOR;
}
#pragma mark-设置子视图
-(void)setSubViews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
//    self.navigationController.navigationBarHidden=NO;
    //发表按钮
    publishBtn= [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-45, 10, 40, 20)];
    [publishBtn setTitle:@"发表" forState:UIControlStateNormal];
    publishBtn.userInteractionEnabled=NO;//禁用
    [publishBtn setTitleColor:mainGrayColor forState:UIControlStateNormal];
    [publishBtn addTarget:self action:@selector(publish) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:publishBtn];
    self.navigationItem.rightBarButtonItem = item;
    inputView=[[UIView alloc] initWithFrame: CGRectMake(0, 0, mainWidth, 180)];
    inputView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:inputView];
    inputTextView=[[UITextView alloc] initWithFrame: CGRectMake(0, 0, mainWidth, 150)];
    inputTextView.backgroundColor=[UIColor whiteColor];
    [inputView addSubview:inputTextView];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)];
    [inputView addGestureRecognizer:tap];
    //设置提示文本
    tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(inputTextView.frame)/3.0, 20)];
    tipsLabel.font=[UIFont systemFontOfSize:12];
    tipsLabel.text=@"这一刻的想法...";
    tipsLabel.textAlignment=NSTextAlignmentLeft;
    tipsLabel.textColor=mainGrayColor;
    [inputTextView addSubview:tipsLabel];
    inputTextView.font=[UIFont systemFontOfSize:14];
    inputTextView.delegate=self;
}
#pragma mark-检测输入框
-(void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length>0) {
        tipsLabel.hidden=YES;
        [publishBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        publishBtn.userInteractionEnabled=YES;
    }else{
        tipsLabel.hidden=NO;
         [publishBtn setTitleColor:mainGrayColor forState:UIControlStateNormal];
        publishBtn.userInteractionEnabled=NO;
    }
}
#pragma mark-收起键盘
-(void)endEditing{
    [self.view endEditing:YES];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing];
}
#pragma mark-发表评论
-(void)publish{
    /**转为utf8方法 待测试*/
//     NSMutableString *sendString=[[NSMutableString alloc]init];
//     for (int i =0 ; i<inputTextView.text.length;) {
//     NSString *tmpStr=[inputTextView.text substringWithRange:NSMakeRange(i,1)];
//     if ([tmpStr UTF8String] == NULL) {
////     如果不对 就取2个 那么肯定就是emoji表情了
//     NSString *tmpStr=[inputTextView.text substringWithRange:NSMakeRange(i,2)];
////     转换成utf8
//      NSString *dataUTF8 = [tmpStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//         [sendString appendString:dataUTF8];
//     i+=2;
//     }else{
//     [sendString appendString:tmpStr];
//     i++;
//     }
//     }
//    NSLog(@" %@",sendString);
//    
//    
//    NSString *hexstr = @"";
//    if ([inputTextView.text length] >= 2) {
//        
//        for (int i = 0; i < [inputTextView.text length] / 2 && ([inputTextView.text length] % 2 == 0) ; i++)
//        {
//            // three bytes
//            if (([inputTextView.text characterAtIndex:i*2] & 0xFF00) == 0 ) {
////                hexstr = [hexstr stringByAppendingFormat:@"Ox%1X 0x%1X",[inputTextView.text characterAtIndex:i*2],[inputTextView.text characterAtIndex:i*2+1]];
//                hexstr=[NSString stringWithFormat:@"%c%c",[inputTextView.text characterAtIndex:i*2],[inputTextView.text characterAtIndex:i*2+1]];
//            }
//            else
//            {// four bytes
//                hexstr = [hexstr stringByAppendingFormat:@"U+%1X ",MULITTHREEBYTEUTF16TOUNICODE([inputTextView.text characterAtIndex:i*2],[inputTextView.text characterAtIndex:i*2+1])];
//            }
//            
//        }
//        NSLog(@"(unicode) [%@]",hexstr);
//    }
//    else
//    {
//        NSLog(@"(unicode) U+%1X",[inputTextView.text characterAtIndex:0]);
//    }
    [XSTool showProgressHUDTOView:self.view withText:nil];
        FansCircleViewController *fanVc;
        for (int i=0; i<self.navigationController.viewControllers.count; i++) {
            if ([[self.navigationController.viewControllers objectAtIndex:i] isKindOfClass:[FansCircleViewController class]]||[[self.navigationController.viewControllers objectAtIndex:i] isKindOfClass:[UserMainViewController class]] ) {
                fanVc=[self.navigationController.viewControllers objectAtIndex:i];
            }
        }
        if (self.commentOrNot==YES) {//评论
            NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *commentString = [[NSString alloc]initWithString:[inputTextView.text stringByTrimmingCharactersInSet:whiteSpace]];
            [HTTPManager commentWithId:self.topicId andContent:commentString WithSuccess:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    [XSTool hideProgressHUDWithView:self.view];
                    fanVc.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                    [XSTool showToastWithView:self.view Text:@"评论成功"];
                     [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * _Nonnull error) {
                 [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }else{//回复
            NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *commentString = [[NSString alloc]initWithString:[inputTextView.text stringByTrimmingCharactersInSet:whiteSpace]];
            [HTTPManager replyWithId:self.replyId andContent:commentString WithSuccess:^(NSDictionary *dic, resultObject *state) {
                if (state.status) {
                    fanVc.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                    [XSTool showToastWithView:self.view Text:@"回复成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * _Nonnull error) {
                 [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }
   }
- (NSString *)hexStringFromString:(NSString *)string
{
    //字符串每个字符转4位16进制Unicode编码
    NSString *newHexStr;
    NSMutableString *enUnicode;
    NSMutableString *tempHexStr;
    for (int i=0; i<string.length; i++) {
        int value=[string characterAtIndex:i];
        if(value>65536||value<0)
        {
            return nil;
        }
        newHexStr = [NSString stringWithFormat:@"%x",value];//16进制数
        
        if(newHexStr.length<4)
        {
            NSString *zero=@"0000";
            tempHexStr=[NSMutableString stringWithString:zero];
            
            [tempHexStr replaceCharactersInRange:NSMakeRange(4-newHexStr.length, newHexStr.length) withString:newHexStr];
        }
        else
            tempHexStr=[NSMutableString stringWithString:newHexStr];
        if(i==0)
            enUnicode=[NSMutableString stringWithString:tempHexStr];
        else
            [enUnicode appendString:tempHexStr];
    }
    //NSLog(@"%@",[enUnicode uppercaseString]);
    return [enUnicode uppercaseString];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**判断是否含有表情*/
/**
 __block BOOL returnValue = NO;
 
 [inputTextView.text enumerateSubstringsInRange:NSMakeRange(0, [inputTextView.text length])
 options:NSStringEnumerationByComposedCharacterSequences
 usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
 const unichar hs = [substring characterAtIndex:0];
 if (0xd800 <= hs && hs <= 0xdbff) {
 if (substring.length > 1) {
 const unichar ls = [substring characterAtIndex:1];
 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
 if (0x1d000 <= uc && uc <= 0x1f77f) {
 returnValue = YES;
 }
 }
 } else if (substring.length > 1) {
 const unichar ls = [substring characterAtIndex:1];
 if (ls == 0x20e3) {
 returnValue = YES;
 }
 } else {
 if (0x2100 <= hs && hs <= 0x27ff) {
 returnValue = YES;
 } else if (0x2B05 <= hs && hs <= 0x2b07) {
 returnValue = YES;
 } else if (0x2934 <= hs && hs <= 0x2935) {
 returnValue = YES;
 } else if (0x3297 <= hs && hs <= 0x3299) {
 returnValue = YES;
 } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
 returnValue = YES;
 }
 }
 }];
 
 **/

/**编码
 NSString *hexstr = @"";
 
 for (int i=0;i< [inputTextView.text length];i++)
 {
 hexstr = [hexstr stringByAppendingFormat:@"%@",[NSString stringWithFormat:@"0x%1X ",[inputTextView.text characterAtIndex:i]]];
 }
 NSLog(@"UTF16 [%@]",hexstr);
 
 hexstr = @"";
 
 unsigned long slen = strlen([inputTextView.text UTF8String]);
 
 for (int i = 0; i < slen; i++)
 {
 //fffffff0 去除前面六个F & 0xFF
 hexstr = [hexstr stringByAppendingFormat:@"%@",[NSString stringWithFormat:@"0x%X ",[inputTextView.text UTF8String][i] & 0xFF ]];
 }
 NSLog(@"UTF8 [%@]",hexstr);
 
 hexstr = @"";
 
 if ([inputTextView.text length] >= 2) {
 
 for (int i = 0; i < [inputTextView.text length] / 2 && ([inputTextView.text length] % 2 == 0) ; i++)
 {
 // three bytes
 if (([inputTextView.text characterAtIndex:i*2] & 0xFF00) == 0 ) {
 hexstr = [hexstr stringByAppendingFormat:@"Ox%1X 0x%1X",[inputTextView.text characterAtIndex:i*2],[inputTextView.text characterAtIndex:i*2+1]];
 }
 else
 {// four bytes
 hexstr = [hexstr stringByAppendingFormat:@"U+%1X ",MULITTHREEBYTEUTF16TOUNICODE([inputTextView.text characterAtIndex:i*2],[inputTextView.text characterAtIndex:i*2+1])];
 }
 
 }
 NSLog(@"(unicode) [%@]",hexstr);
 }
 else
 {
 NSLog(@"(unicode) U+%1X",[inputTextView.text characterAtIndex:0]);
 }
 **/

/**判断emoji表情，并将其转为编码
 NSMutableString *sendString=[[NSMutableString alloc]init];
 for (int i =0 ; i<inputTextView.text.length;) {
 //
 NSString *tmpStr=[inputTextView.text substringWithRange:NSMakeRange(i,1)];//每次取1个长度
 NSLog(@"%@ length is %lu",tmpStr,tmpStr.length);
 if ([tmpStr UTF8String] == NULL) {
 //如果不对 就取2个 那么肯定就是emoji表情了
 NSString *tmpStr=[inputTextView.text substringWithRange:NSMakeRange(i,2)];
 //调用函数转换查询字符串
 NSString *tmpSendStr=[self emojiSelectUnicodeWithUTF8:tmpStr];
 [sendString appendString:tmpSendStr];
 i+=2;
 }else{
 [sendString appendString:tmpStr];
 i++;
 }
 NSLog(@" %@ %u",sendString,i);
 }

 **/
/**十六进制与字符串互相转化
 + (NSString *)hexStringFromString:(NSString *)string
 {
 //字符串每个字符转4位16进制Unicode编码
 NSString *newHexStr;
 NSMutableString *enUnicode;
 NSMutableString *tempHexStr;
 for (int i=0; i<string.length; i++) {
 int value=[string characterAtIndex:i];
 if(value>65536||value<0)
 {
 return nil;
 }
 newHexStr = [NSString stringWithFormat:@"%x",value];//16进制数
 
 if(newHexStr.length<4)
 {
 NSString *zero=@"0000";
 tempHexStr=[NSMutableString stringWithString:zero];
 
 [tempHexStr replaceCharactersInRange:NSMakeRange(4-newHexStr.length, newHexStr.length) withString:newHexStr];
 }
 else
 tempHexStr=[NSMutableString stringWithString:newHexStr];
 if(i==0)
 enUnicode=[NSMutableString stringWithString:tempHexStr];
 else
 [enUnicode appendString:tempHexStr];
 }
 //NSLog(@"%@",[enUnicode uppercaseString]);
 return [enUnicode uppercaseString];
 }
 
 
 
 + (NSString *)stringFromHexString:(NSString *)hexString
 {
 //每4位16进制Unicode编码转为一个字符
 if(hexString.length%4!=0||hexString==nil)
 return nil;
 NSString *enUnicode;
 NSMutableString *deUnicode;
 for(NSInteger i=0;i<(hexString.length/4);i++)
 {   unsigned int anInt;
 enUnicode=[hexString substringWithRange:NSMakeRange(i*4,4)];
 NSScanner * scanner = [[NSScanner alloc] initWithString:enUnicode];
 [scanner scanHexInt:&anInt];
 enUnicode=[NSString stringWithFormat:@"%C",(unichar)anInt];
 if(i==0)
 deUnicode=[NSMutableString stringWithString:enUnicode];
 else
 [deUnicode appendString:enUnicode];
 }
 
 return deUnicode;
 }
 **/


@end
