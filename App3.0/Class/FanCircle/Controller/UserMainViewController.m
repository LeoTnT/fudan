//
//  UserMainViewController.m
//  App3.0
//
//  Created by mac on 2017/4/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UserMainViewController.h"
#import "FansCircleViewController.h"
#import "FansCircleTopCell.h"
#import "FansCircleStatusCell.h"
#import "CommentViewController.h"
#import "AddVideoStatusViewController.h"
#import "AddStatusViewController.h"
#import "LoginModel.h"
#import "ReplyView.h"
#import "CLPlayerView.h"
#import "UserModel.h"
#import "IdentityCardModel.h"
#import "ChatKeyBoard.h"
#import "FaceSourceManager.h"
#import "GoodsDetailViewController.h"
#import "S_StoreInformation.h"
#import "PersonViewController.h"
#import "PersonalViewController.h"

@interface UserMainViewController ()<ChatKeyBoardDelegate,ChatKeyBoardDataSource>
/**记录被点击的评论点赞的cell*/
@property(nonatomic,strong)FansCircleStatusCell *tempCell;
/**记录点击过评论点赞的cell*/
@property(nonatomic,strong)NSMutableArray *praiseAndCommentBtnArray;
/**评论或者回复的某条动态*/
@property(nonatomic,strong)  FansCircleStatusCell *replyOrCommentCell;
/**保存cell高度的字典*/
@property(nonatomic,strong)NSMutableDictionary *heightDic;
/**视频控制器*/
@property(nonatomic,strong)UIImagePickerController *videoPickerVC;
/**视频控制器*/
@property(nonatomic,strong)UIImagePickerController *imagePickerVC;
/**记录全局的动态页数*/
@property(nonatomic,assign)int page;
/**动态数组*/
@property(nonatomic,strong) NSMutableArray  *statusArray;
/**评论还是回复*/
@property(nonatomic,assign)BOOL commentOrNot;
/**动态编号*/
@property(nonatomic,strong)NSNumber *topicId;
/**评论编号*/
@property(nonatomic,strong)NSNumber *replyId;
@property (strong, nonatomic) ChatKeyBoard *keyBoard;
/**记录删除和修改的cell的字典*/
@property(nonatomic,strong)NSMutableDictionary *updateDic;
@property(nonatomic,strong)LoginDataParser *loginData;
/*视频播放器*/
@property(nonatomic,strong)CLPlayerView *playerView;
@end

@implementation UserMainViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.autoHideKeyboard=YES;
    //设置子视图
    [self setSubViews];
    //加载数据
    [XSTool showProgressHUDTOView:self.view withText:@"正在获取数据"];
    [self getUserDataParser];
    //获取数据
    //    [self getNewStatusArray];
    [self getUserInfo];
    [self.tableView.mj_header beginRefreshing];
}
-(NSMutableArray *)statusArray{
    if (!_statusArray) {
        _statusArray=[NSMutableArray array];
    }
    return _statusArray;
}
-(NSMutableDictionary *)updateDic{
    if (!_updateDic) {
        _updateDic=[NSMutableDictionary dictionary];
    }
    return _updateDic;
}

#pragma mark-设置子视图
-(void)getUserDataParser{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSData *userData = [user objectForKey:USERINFO_LOGIN];
    self.loginData= [NSKeyedUnarchiver unarchiveObjectWithData:userData];
}
-(void)setSubViews{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        if(self.navigationController.viewControllers.count>=2){
            if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[PersonalViewController class]] ||[[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[FansCircleViewController class]]) {
                PersonalViewController *personlVC =[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
                personlVC.updateDic=self.updateDic;
                self.updateDic=nil;
            }
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.title = @"个人动态";
    self.view.backgroundColor = BG_COLOR;
    //表格
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.showRefreshHeader = YES;
    self.showRefreshFooter = YES;
    
    [self setRefreshHeader:^{
        @strongify(self);
        [self getNewStatusArray];
    }];
    [self setRefreshFooter:^{
        @strongify(self);
        [self getOldStatusArray];
    }];
    
    self.praiseAndCommentBtnArray=[NSMutableArray array];
    self.keyBoard = [ChatKeyBoard keyBoard];
    self.keyBoard.keyBoardStyle = KeyBoardStyleComment;
    self.keyBoard.associateTableView = self.tableView;
    self.keyBoard.delegate = self;
    self.keyBoard.dataSource = self;
    self.keyBoard.allowVoice = NO;
    self.keyBoard.allowMore = NO;
    self.keyBoard.allowFace = NO;
    self.keyBoard.allowSend = YES;
    [self.view addSubview:self.keyBoard];
}
#pragma mark-每次进入界面  刷新
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    //    if (replyOrCommentCell&&self.reply) {//评论或者回复，刷新单条数据
    //        Status *status=replyOrCommentCell.status;
    //        NSMutableArray *tempArray=[NSMutableArray arrayWithArray:status.reply];
    //        [tempArray addObject:self.reply];
    //        status.reply=tempArray;
    //        replyOrCommentCell.status=status;
    //        [_tableView reloadData];
    //        replyOrCommentCell=nil;
    //        self.reply=nil;
    //    }else if(replyOrCommentCell){//点了评论回复，没有输入内容，直接返回
    //
    //    }else{
    //        [self getNewStatusArray];
    //    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    if (self.playerView) {
        [self.playerView pausePlay];
        [self.playerView endPlay:^{
            [self.playerView destroyPlayer];
        }];
        [self.playerView destroyPlayer];
        self.playerView=nil;
    }
}
-(NSMutableDictionary *)heightDic{
    if (!_heightDic) {
        _heightDic=[NSMutableDictionary dictionary];
    }
    return _heightDic;
}
#pragma mark-获取用户信息  刷新顶部
-(void)getUserInfo{
    [HTTPManager getOtherInfoWithUserName:self.userName?self.userName:self.loginData.username success:^(NSDictionary * _Nullable dic, resultObject *state) {
        if ([dic[@"status"] isEqual:@1]) {
            UserDataParser *parser1=[UserDataParser mj_objectWithKeyValues:dic[@"data"]];
            FansCircleTopCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            cell.nickAndAvatarAndBgImgDic=@{@"nickName":parser1.nickname?parser1.nickname:@"",@"avatar":parser1.logo?parser1.logo:@"",@"bgImg":parser1.logo_fandom_bg?parser1.logo_fandom_bg:@"",};
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-内容收起或全文
-(void)lookPartOrNot:(UIButton *)btn{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview;
    if ([btn.titleLabel.text isEqualToString:@"全文"]) {
        //展开
        cell.lookAll=YES;
    }else{
        //收起
        cell.lookAll=NO;
    }
    [self.tableView reloadData];
}
#pragma mark-设置刷新样式
-(void)setTableRefreshStyle{
    @weakify(self);
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self getNewStatusArray];
    }];
    //设置不同刷新状态的文字
    [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
    [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
    [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    header.stateLabel.font=[UIFont systemFontOfSize:14];
    header.stateLabel.textColor=[UIColor darkGrayColor];
    header.lastUpdatedTimeLabel.hidden=YES;
    self.tableView.mj_header=header;
    MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self getOldStatusArray];
    }];
    [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    //设置字体大小和文字颜色
    footer.stateLabel.font=[UIFont systemFontOfSize:14];
    footer.stateLabel.textColor=[UIColor darkGrayColor];
    self.tableView.mj_footer=footer;
}
#pragma mark-获取旧的数据
-(void)getOldStatusArray{
    self.page++;
    @weakify(self);
    [HTTPManager getOldStatusArrayWithUID:self.uid?self.uid:self.loginData.uid PageNum:@(self.page) WithSuccess:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        if(state.status){
            FansCircleModel *model=[FansCircleModel mj_objectWithKeyValues:dic];
            if (model.data.count==0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多内容"];
            }else{
                [self.statusArray addObjectsFromArray:model.data];
                [self.tableView reloadData];
            }
        }else{
            self.page--;
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [self.tableView.mj_footer endRefreshing];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        self.page--;
    }];
}
#pragma mark-获取最新数据
-(void)getNewStatusArray{
    self.page=1;
    @weakify(self);
    [HTTPManager getNewStatusArrayWithUID:self.uid?self.uid:self.loginData.uid Success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        if(state.status){
            [self.statusArray removeAllObjects];
            FansCircleModel *model=[FansCircleModel mj_objectWithKeyValues:dic];
            if (model.data.count==0) {
                [XSTool showToastWithView:self.view Text:@"暂无更多内容"];
            } else {
                [self.statusArray addObjectsFromArray: model.data];
            }
            [self.tableView reloadData];
        }else{
            [XSTool showToastWithView:self.view Text:dic[@"info"]];
        }
    } fail:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
    }];
}
#pragma mark-设置表格行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return self.statusArray.count;
    }
}
#pragma mark-设置分组数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
#pragma mark-设置表格的cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (indexPath.section==0) {
        static NSString *cellId=@"topCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[FansCircleTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        [((FansCircleTopCell *)cell).headImg addTarget:self action:@selector(contactFriend:) forControlEvents:UIControlEventTouchUpInside];
        //        ((FansCircleTopCell *)cell).tipsLabel.hidden=YES;
        //        ((FansCircleTopCell *)cell).picture.hidden=YES;
    }else{
        static NSString *cellId=@"statusCell";
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell=[[FansCircleStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        ((FansCircleStatusCell *)cell).currentVC=self;
        ((FansCircleStatusCell *)cell).status=[self.statusArray objectAtIndex:indexPath.row];
        [((FansCircleStatusCell *)cell).deleteBtn addTarget:self action:@selector(deleteStatus:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).praiseAndCommentClearBtn addTarget:self action:@selector(showPraiseOrCommentBtn:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).commentBtn addTarget:self action:@selector(addComment:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).praiseBtn addTarget:self action:@selector(addPraise:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).contentLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goUrl:)]];
        [((FansCircleStatusCell *)cell).lookPartOrAllBtn addTarget:self action:@selector(lookPartOrNot:) forControlEvents:UIControlEventTouchUpInside];
        [((FansCircleStatusCell *)cell).headImgBtn addTarget:self action:@selector(contactFriend:) forControlEvents:UIControlEventTouchUpInside];
        for (UIButton *btn in ((FansCircleStatusCell *)cell).replyOrDeleteCommentBtnArray) {
            [btn addTarget:self action:@selector(replyOrDeleteComment:) forControlEvents:UIControlEventTouchUpInside];
        }
        if(((FansCircleStatusCell *)cell).playVideoBtn){
            [((FansCircleStatusCell *)cell).playVideoBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        }
        if(((FansCircleStatusCell *)cell).shareView){
            [((FansCircleStatusCell *)cell).shareView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goUrl:)]];
        }
        //记录高度
        self.heightDic[@(indexPath.row)]=@(((FansCircleStatusCell *)cell).height);
    }
    return cell;
}
-(void)contactFriend:(UIButton *)btn{
    UITableViewCell *cell=(UITableViewCell *)btn.superview.superview;
    if ([cell isKindOfClass:[FansCircleTopCell class]]) {
        if(self.uid){
            if(self.navigationController.viewControllers.count>=2){
                UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
                if ([vc isKindOfClass:[PersonalViewController class]]) {
                    ((PersonalViewController *)vc).updateDic=self.updateDic;
                    self.updateDic=nil;
                    [self.navigationController popViewControllerAnimated:YES];
                    return;
                }
            }
            PersonalViewController *personVC=[[PersonalViewController alloc] initWithUid:self.uid];
            [self.navigationController pushViewController:personVC animated:YES];
        }
    }else{
        if([((FansCircleStatusCell *)cell).status.is_owner integerValue]!=1){
            if(self.navigationController.viewControllers.count>=2){
                UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
                if ([vc isKindOfClass:[PersonalViewController class]]) {
                    ((PersonalViewController *)vc).updateDic=self.updateDic;
                    self.updateDic=nil;
                    [self.navigationController popViewControllerAnimated:YES];
                    return;
                }
            }
            PersonalViewController *personVC=[[PersonalViewController alloc] initWithUid:((FansCircleStatusCell *)cell).status.uid];
            [self.navigationController pushViewController:personVC animated:YES];
        }
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.playerView) {
        [self.playerView destroyPlayer];
    }
}
#pragma mark-删除或者回复评论
-(void)replyOrDeleteComment:(UIButton *)btn{
    ReplyView *view=(ReplyView *)btn.superview;
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview.superview.superview;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.reply];
    if ([btn.titleLabel.text isEqualToString:@"回复"]) {
        //        CommentViewController *comment=[[CommentViewController alloc] init];
        //        comment.commentOrNot=NO;
        //        comment.replyId=view.reply.reply_id;
        //        replyOrCommentCell=cell;
        //        [self.navigationController pushViewController:comment animated:YES];
        self.replyOrCommentCell=cell;
        self.commentOrNot = NO;
        self.replyId=view.reply.reply_id;
        self.keyBoard.placeHolder = [NSString stringWithFormat:@"回复%@:",view.reply.from_nickname];
        [self.keyBoard keyboardUpforComment];
    }else{
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil  preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [XSTool showProgressHUDTOView:self.view withText:nil];
            @weakify(self);
            [HTTPManager deleteCommentWithId:view.reply.reply_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                if (state.status) {
                    [tempArray removeObject:view.reply];
                    cell.status.reply=tempArray;
                    [self.tableView reloadData];
                    NSMutableArray *array=[NSMutableArray arrayWithArray:[self.updateDic valueForKey:@"update"]];
                    //先查看是否存在  避免重复添加
                    for (int i=0; i<array.count;i++) {
                        Status *s=[array objectAtIndex:i];
                        if ([s.topic_id integerValue]==[cell.status.topic_id integerValue]) {
                            s=cell.status;
                            [self.updateDic setValue:array forKey:@"update"];
                            return;
                        }
                    }
                    [array  addObject:cell.status];
                    [self.updateDic setValue:array forKey:@"update"];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError *error) {
                @strongify(self);
                [XSTool hideProgressHUDWithView:self.view];
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:confirm];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
#pragma mark-播放视频
-(void)playVideo:(UIButton *)btn{
    //之前的停止播放
    if (self.playerView) {
        [self.playerView destroyPlayer];
    }
    FansCircleStatusCell *cell=(FansCircleStatusCell *)btn.superview.superview.superview;
    NSURL *videoUrl=[NSURL URLWithString:((Video *)cell.status.videos[0]).video];
    self.playerView = [[CLPlayerView alloc] initWithFrame:cell.videoImageView.frame];
    [cell.contentView addSubview:self.playerView];
    //视频地址
    self.playerView.url =videoUrl;
    self.playerView.fillMode=ResizeAspect;
    self.playerView.autoFullScreen=NO;
    //播放
    [self.playerView playVideo];
    //播放完成回调
    [self.playerView endPlay:^{
        //销毁播放器
        [self.playerView destroyPlayer];
    }];
}
#pragma mark-发表评论
-(void)addComment:(UIButton *)button{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview.superview;
    cell.praiseAndConmmentViewHidden=YES;
    [self.praiseAndCommentBtnArray removeObject:cell];
    self.tempCell=nil;
    self.replyOrCommentCell=cell;
    self.topicId=cell.status.topic_id;
    self.commentOrNot = YES;
    self.keyBoard.placeHolder = @"";
    [self.keyBoard keyboardUpforComment];
    //    CommentViewController *comment=[[CommentViewController alloc] init];
    //    comment.commentOrNot=YES;
    //    comment.topicId=cell.status.topic_id;
    //    replyOrCommentCell=cell;
    //    [self.navigationController pushViewController:comment animated:YES];
}
#pragma mark-点赞
-(void)addPraise:(UIButton *)button{
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview.superview;
    Status *status=cell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:cell.status.thumbs_up_users];
    //点赞动画
    cell.praiseBtn.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.75 delay:0 options:0 animations: ^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
            cell.praiseBtn.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
        }];
        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
            cell.praiseBtn.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    } completion:^(BOOL finished) {
        cell.praiseAndConmmentViewHidden=YES;
        [self.praiseAndCommentBtnArray removeObject:cell];
        self.tempCell=nil;
        @weakify(self);
        if ([cell.status.is_thumbs_up isEqual:@1]) {//取消点赞
            [HTTPManager cancelThumbUpWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state.status) {
                    for (int i=0; i<cell.status.thumbs_up_users.count; i++) {
                        Thumbs_up_user *user=[cell.status.thumbs_up_users objectAtIndex:i];
                        if ([user.username isEqualToString:self.loginData.username]) {
                            [tempArray removeObject:user];
                        }
                    }
                    status.thumbs_up_users=tempArray;
                    status.is_thumbs_up=@0;
                    [self.tableView reloadData];
                    NSMutableArray *array=[NSMutableArray arrayWithArray:[self.updateDic valueForKey:@"update"]];
                    //先查看是否存在  避免重复添加
                    for (int i=0; i<array.count;i++) {
                        Status *s=[array objectAtIndex:i];
                        if ([s.topic_id integerValue]==[cell.status.topic_id integerValue]) {
                            s=cell.status;
                            [self.updateDic setValue:array forKey:@"update"];
                            return;
                        }
                    }
                    [array  addObject:cell.status];
                    [self.updateDic setValue:array forKey:@"update"];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * error) {
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }else{//点赞
            [HTTPManager thumbUpWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
                @strongify(self);
                if (state.status) {
                    Thumbs_up_user *user=[ Thumbs_up_user mj_objectWithKeyValues:@{@"username":self.loginData.username,@"nikename":self.loginData.nickname}];
                    [tempArray addObject:user];
                    status.thumbs_up_users=tempArray;
                    status.is_thumbs_up=@1;
                    [self.tableView reloadData];
                    NSMutableArray *array=[NSMutableArray arrayWithArray:[self.updateDic valueForKey:@"update"]];
                    //先查看是否存在  避免重复添加
                    for (int i=0; i<array.count;i++) {
                        Status *s=[array objectAtIndex:i];
                        if ([s.topic_id integerValue]==[cell.status.topic_id integerValue]) {
                            s=cell.status;
                            [self.updateDic setValue:array forKey:@"update"];
                            return;
                        }
                    }
                    [array  addObject:cell.status];
                    [self.updateDic setValue:array forKey:@"update"];
                }else{
                    [XSTool showToastWithView:self.view Text:dic[@"info"]];
                }
            } fail:^(NSError * error) {
                [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
            }];
        }
    }];
}
#pragma mark-删除动态
-(void)deleteStatus:(UIButton *)deleteBtn{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"确定要删除吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        FansCircleStatusCell *cell=(FansCircleStatusCell *)deleteBtn.superview.superview;
        [XSTool showProgressHUDTOView:self.view withText:nil];
        @weakify(self);
        [HTTPManager deleteStatusWithStatusId:cell.status.topic_id WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            if(state.status){
                [self.statusArray removeObject:cell.status];
                [self.tableView reloadData];
                NSMutableArray *deleteArray=[NSMutableArray arrayWithArray:[self.updateDic valueForKey:@"delete"]];
                [deleteArray addObject:cell.status.topic_id];
                [self.updateDic setValue:deleteArray forKey:@"delete"];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            @strongify(self);
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-设置行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 200;
    }else{
        if ([self.heightDic[@(indexPath.row)] floatValue]==0) {
            return 0.1;
        }
        return [self.heightDic[@(indexPath.row)] floatValue];
    }
}
#pragma mark-展示选择点赞或者评论
-(void)showPraiseOrCommentBtn:(UIButton *)button{
    button.selected=!button.selected;
    FansCircleStatusCell *cell=(FansCircleStatusCell *)button.superview.superview;
    if (button.selected) {
        self.tempCell=cell;
        //其他的评论点赞按钮隐藏
        for (int i=0; i<self.praiseAndCommentBtnArray.count; i++) {
            FansCircleStatusCell *cell1=[self.praiseAndCommentBtnArray objectAtIndex:i];
            if (![cell1 isEqual:self.tempCell]) {
                cell1.praiseAndConmmentViewHidden=YES;
                [self.praiseAndCommentBtnArray removeObject:cell1];
            }
        }
        [self.praiseAndCommentBtnArray addObject:cell];
        cell.praiseAndConmmentViewHidden=NO;
    }else{
        cell.praiseAndConmmentViewHidden=YES;
        self.tempCell=nil;
        [self.praiseAndCommentBtnArray removeObject:self.tempCell];
    }
    
}
#pragma mark-点击发表状态
-(void)addStatus{
    AddStatusViewController *add=[[AddStatusViewController alloc] init];
    [self.navigationController pushViewController:add animated:YES];
}
#pragma mark-分组头标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
#pragma mark-设置脚标题的高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 10;
    }else{
        return 0.1;
    }
}
#pragma mark -- ChatKeyBoardDataSource
- (NSArray<ChatToolBarItem *> *)chatKeyBoardToolbarItems
{
    ChatToolBarItem *item1 = [ChatToolBarItem barItemWithKind:kBarItemFace normal:@"face" high:@"face_HL" select:@"keyboard"];
    ChatToolBarItem *item4 = [ChatToolBarItem barItemWithKind:kBarItemSwitchBar normal:@"switchDown" high:nil select:nil];
    return @[item1, item4];
}

- (NSArray<FaceThemeModel *> *)chatKeyBoardFacePanelSubjectItems
{
    return [FaceSourceManager loadFaceSource];
}

#pragma mark - 输入状态
- (void)chatKeyBoardTextViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)chatKeyBoardSendText:(NSString *)text{
    [self.view endEditing:YES];
    @weakify(self);
    if (self.commentOrNot==YES) {//评论
        [HTTPManager commentWithId:self.topicId andContent:text WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                self.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                [self refreshAfterCommitOrReply];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }else{//回复
        [HTTPManager replyWithId:self.replyId andContent:text WithSuccess:^(NSDictionary *dic, resultObject *state) {
            @strongify(self);
            if (state.status) {
                self.reply=[Reply mj_objectWithKeyValues:dic[@"data"]];
                [self refreshAfterCommitOrReply];
            }else{
                [XSTool showToastWithView:self.view Text:dic[@"info"]];
            }
        } fail:^(NSError *error) {
            [XSTool showToastWithView:self.view Text:@"网络异常，请稍后重试"];
        }];
    }
}

- (void)refreshAfterCommitOrReply {
    Status *status=self.replyOrCommentCell.status;
    NSMutableArray *tempArray=[NSMutableArray arrayWithArray:status.reply];
    [tempArray addObject:self.reply];
    status.reply=tempArray;
    self.replyOrCommentCell.status=status;
    [self.tableView reloadData];
    NSMutableArray *array=[NSMutableArray arrayWithArray:[self.updateDic valueForKey:@"update"]];
    //先查看是否存在  避免重复添加
    for (int i=0; i<array.count;i++) {
        Status *s=[array objectAtIndex:i];
        if ([s.topic_id integerValue]==[status.topic_id integerValue]) {
            s=status;
            [self.updateDic setValue:array forKey:@"update"];
            return;
        }
    }
    [array addObject:status];
    [self.updateDic setValue:array forKey:@"update"];
    self.replyOrCommentCell=nil;
    self.reply=nil;
}
-(void)goUrl:(UITapGestureRecognizer *)tap{
    UIView *tempView=tap.view;
    if ([tempView isKindOfClass:[UILabel class]]) {//内容链接跳转
        [self cellContentUrlDidClick:[((UILabel *)tempView).attributedText.string urlSubString]];
    }else{//分享跳转
        [self cellContentUrlDidClick:[((FanCircleShareView *)tempView).share.url urlSubString]];
    }
}
- (void)cellContentUrlDidClick:(NSString *)url {
    NSArray *paramArr = [url componentsSeparatedByString:@"?"];
    if (paramArr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *paramStr = paramArr[1];
    NSArray *arr = [paramStr componentsSeparatedByString:@"&"];
    if (![url containsString:ImageBaseUrl] || arr.count < 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return;
    }
    NSString *param1 = arr[0];
    NSString *param2 = arr[1];
    NSString *qrtype, *qrid;
    NSString *deleteStr1 = @"qrtype=";
    NSString *deleteStr2 = @"id=";
    if ([param1 containsString:deleteStr1]) {
        qrtype = [param1 substringFromIndex:deleteStr1.length];
    }
    if ([param2 containsString:deleteStr2]) {
        qrid = [param2 substringFromIndex:deleteStr2.length];
    }
    
    if ([qrtype isEqualToString:@"product"]) {
        GoodsDetailViewController *vc = [GoodsDetailViewController new];
        vc.goodsID = qrid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([qrtype isEqualToString:@"supply"]){
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = qrid;
        
        [XSTool hideProgressHUDWithView:self.view];
        [self.navigationController pushViewController:infor animated:YES];
        
    } else if ([qrtype isEqualToString:@"app"]){
        [XSTool showToastWithView:self.view Text:[NSString stringWithFormat: @"您已安装%@",APP_NAME]];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if ([qrtype isEqualToString:@"user"]){
        if ([qrid isEqualToString:[UserInstance ShardInstnce].uid]) {
            PersonViewController *personVC = [[PersonViewController alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        } else {
            PersonalViewController *perVC = [[PersonalViewController alloc] initWithUid:qrid];
            [self.navigationController pushViewController:perVC animated:YES];
        }
    } else if ([qrtype isEqualToString:@"mall"]) {
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
