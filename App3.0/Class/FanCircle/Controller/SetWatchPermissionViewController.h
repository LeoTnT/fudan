//
//  SetWatchPermissionViewController.h
//  App3.0
//
//  Created by mac on 2017/3/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
typedef enum
{
    permissionPublic = 0,
    permissionPrivacy,
    permissionSomeCan,
    permissionSomeNoCan,
}PermissionType;
@interface SetWatchPermissionViewController : XSBaseViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray *users;
/**公开类型*/
@property(nonatomic,assign)PermissionType permissionType;
@end
