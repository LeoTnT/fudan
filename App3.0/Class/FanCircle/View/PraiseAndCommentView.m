//
//  PraiseAndCommentView.m
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PraiseAndCommentView.h"
#import "PraiseView.h"
#import "ReplyView.h"

@implementation PraiseAndCommentView
#pragma mark-刷新界面
-(void)setPraiseArray:(NSArray *)praiseArray{
    _praiseArray=praiseArray;
     self.backgroundColor=[UIColor hexFloatColor:@"f3f3f5"];
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[PraiseView class]]||[view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
    if (praiseArray.count==0) {
        self.praiseHeight=0;
    }else{
        PraiseView *praise=[[PraiseView alloc] init];
        praise.praiseArray=praiseArray;
        self.praiseHeight=praise.praiseHeight;
        praise.frame=CGRectMake(0, 10, mainWidth-3*10-50, praise.praiseHeight);
        [self addSubview:praise];
    }
    self.height=self.praiseHeight+10;
}
#pragma mark-刷新界面
-(void)setCommentArray:(NSArray *)commentArray{
    _commentArray=commentArray;
    if (self.praiseArray.count&&commentArray.count) {
        //添加分割线
        UILabel *cutLine=[[UILabel alloc] initWithFrame:CGRectMake(0, self.praiseHeight+10+5, mainWidth-3*10-50, 0.5)];
        [cutLine setBackgroundColor:LINE_COLOR];
        [self addSubview:cutLine];
    }
     self.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
    //清空界面
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[ReplyView class]]) {
            [view removeFromSuperview];
        }
    }
    NSMutableArray *tempArray=[NSMutableArray array];
    CGFloat tempHeight = self.praiseArray.count? self.praiseHeight+20 : self.praiseHeight;
        for (int i=0; i<commentArray.count; i++) {
            @autoreleasepool {
                ReplyView *reply=[[ReplyView alloc] init];
                reply.reply=[commentArray objectAtIndex:i];
                reply.frame=CGRectMake(0, tempHeight, mainWidth-3*10-50, reply.replyViewHeight);
                [self addSubview:reply];
                [tempArray addObject:reply.replyOrDeleteBtn];
                tempHeight+=reply.replyViewHeight;
            }
    }
    // 评论框整体高度
    self.height=tempHeight;
    self.replyOrDeleteCommentBtnArray=tempArray;
}
@end
