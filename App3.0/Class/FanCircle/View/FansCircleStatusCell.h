//
//  FansCircleStatusCell.h
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PraiseAndCommentView.h"
#import "FansCircleModel.h"
#import "LoginModel.h"
#import "ReuseImageView.h"
#import "UIView+XLExtension.h"
#import "XLPhotoBrowser.h"
#import "FanCircleShareView.h"

@interface FansCircleStatusCell : UITableViewCell<XLPhotoBrowserDelegate, XLPhotoBrowserDatasource>
/**高度*/
@property(nonatomic,assign)CGFloat height;
/**用户头像*/
@property(nonatomic,strong)UIButton *headImgBtn;
/**昵称*/
@property(nonatomic,strong)UILabel *nickNameLabel;
/**发表内容*/
@property(nonatomic,strong)UILabel *contentLabel;
/**评论内容*/
@property(nonatomic,strong)PraiseAndCommentView *praiseAndCommentView;
/**时间*/
@property(nonatomic,strong)UILabel *timeLabel;
/**点赞评论按钮*/
@property(nonatomic,strong)UIButton *praiseAndCommentBtn;
/**点赞*/
@property(nonatomic,strong)UIButton *praiseBtn;
/**点赞评论按钮区域*/
@property(nonatomic,strong)UIView *praiseAndCommentBtnView;
/**评论*/
@property(nonatomic,strong)UIButton *commentBtn;
/**删除按钮*/
@property(nonatomic,strong)UIButton *deleteBtn;
/**播放按钮*/
@property(nonatomic,strong)UIButton *playVideoBtn;
/**动态*/
@property(nonatomic,strong)Status *status;
/**当前用户*/
@property(nonatomic,strong)LoginDataParser *parser;
/**点赞评论背景图*/
@property(nonatomic,strong)UIImageView *commentAndPraiseTopView;
/**地址*/
@property(nonatomic,strong)UILabel *positionLabel;
/**视频缩略图*/
@property(nonatomic,strong)ReuseImageView *videoImageView;
/**删除或者回复评论按钮数组*/
@property(nonatomic,strong)NSArray *replyOrDeleteCommentBtnArray;
/**图片网址数组*/
@property(nonatomic,strong)NSMutableArray *imageUrlArray;
/**评论点赞是否展示*/
@property(nonatomic,assign)BOOL praiseAndConmmentViewHidden;
/**评论点赞的覆盖的透明按钮*/
@property(nonatomic,strong)UIButton *praiseAndCommentClearBtn;
/**当前控制器*/
@property(nonatomic,strong) UIViewController *currentVC;
@property(nonatomic,strong)FanCircleShareView *shareView;
@property(nonatomic,strong)UIButton *lookPartOrAllBtn;//收起  全文按钮
@property(nonatomic,assign)BOOL lookAll;//全文或收起
@end

@interface UIImageViewAnimation : NSObject
+(void)enlargeAndSmallWithImageView:(UIImageView *)imageView completion:(void (^)(BOOL finished))completion;
@end
