//
//  SetWatchPermissionCell.m
//  App3.0
//
//  Created by syn on 2017/6/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "SetWatchPermissionCell.h"

@implementation SetWatchPermissionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.tipsImg=[[UIImageView alloc] initWithFrame:CGRectMake(mainWidth-10-10, (60-10)/2.0, 10, 10)];
        self.tipsImg.image=[UIImage imageNamed:@"user_fans_selected"];
        self.tipsImg.hidden=YES;
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.tipsImg];
    }
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
