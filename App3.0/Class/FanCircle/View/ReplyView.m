//
//  ReplyView.m
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ReplyView.h"
#import "FansCircleModel.h"
#import "XSFormatterDate.h"

@interface ReplyView()
@end
@implementation ReplyView
#pragma mark-重写构造方法
-(instancetype)init{
    if (self=[super init]) {
        //头像
        self.avatar = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 35, 35)];
        [self addSubview:self.avatar];
        //昵称
        self.nickName=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatar.frame)+5,5, mainWidth-3*10-45-35-3*5, 17.5)];
        self.nickName.textAlignment=NSTextAlignmentLeft;
        self.nickName.textColor=[UIColor hexFloatColor:@"5a6e97"];
        self.nickName.numberOfLines=1;
        self.nickName.font=[UIFont systemFontOfSize:13];
        [self addSubview:self.nickName];
        //评论内容
        self.nickNameAndContentLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatar.frame)+5,CGRectGetMaxY(self.nickName.frame), mainWidth-3*10-45-35-3*5, 17.5)];
        self.nickNameAndContentLabel.textAlignment=NSTextAlignmentLeft;
        self.nickNameAndContentLabel.textColor=[UIColor blackColor];
        self.nickNameAndContentLabel.numberOfLines=0;
        self.nickNameAndContentLabel.font=[UIFont systemFontOfSize:13];
        [self addSubview:self.nickNameAndContentLabel];
        //时间
        self.timeLabel=[[UILabel alloc] initWithFrame:CGRectMake(mainWidth-45-3*10-5-100,5, 100, 17.5)];
        self.timeLabel.textAlignment=NSTextAlignmentRight;
        self.timeLabel.textColor=[UIColor darkGrayColor];
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        [self addSubview:self.timeLabel];
        //回复按钮
        self.replyOrDeleteBtn=[[UIButton alloc] init];
        self.replyOrDeleteBtn.titleLabel.textAlignment=NSTextAlignmentRight;
        self.replyOrDeleteBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [self.replyOrDeleteBtn setTitleColor:[UIColor hexFloatColor:@"5a6e97"] forState:UIControlStateNormal];
        [self addSubview:self.replyOrDeleteBtn];
        // 分割线
        self.lineView = [[UIView alloc] init];
        self.lineView.backgroundColor = LINE_COLOR;
        self.lineView.alpha=0.5;
        [self addSubview:self.lineView];
    }

    return self;
}
#pragma mark-测试数组
-(void)setReply:(Reply *)reply{
    _reply=reply;
    if ([reply.is_owner isEqual:@1]) {
        [self.replyOrDeleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    }else{
        [self.replyOrDeleteBtn setTitle:@"回复" forState:UIControlStateNormal];
    }
    //判断是评论还是回复
    if (reply.to_username.length==0) {//评论
        //        NSString *str1=reply.from_nickname;
        //        long length1=str1.length;
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",reply.content]];
        //    [attri addAttribute:NSForegroundColorAttributeName value:TAB_SELECTED_COLOR range:NSMakeRange(0, length1)];
        self.nickNameAndContentLabel.attributedText=attri;
    }else{
        //        NSString *str1=[NSString stringWithFormat:@"%@  ",reply.from_nickname];
        //        long  length1=str1.length;
        NSString *str2=[NSString stringWithFormat:@"回复"];
        long length2=str2.length;
        NSString *str3=[str2 stringByAppendingString:reply.to_nickname];
        long length3=reply.to_nickname.length;
        NSString *str4=[str3 stringByAppendingString:@":"];
        NSString *str5=[str4 stringByAppendingString:reply.content];
        NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:str5];
        //        [attri addAttribute:NSForegroundColorAttributeName value:TAB_SELECTED_COLOR range:NSMakeRange(0, length1)];
        [attri addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"5a6e97"] range:NSMakeRange(length2, length3)];
        self.nickNameAndContentLabel.attributedText=attri;
    }
    [self.avatar getImageWithUrlStr:reply.logo andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.nickName.text = reply.from_nickname;
    self.timeLabel.text=[XSFormatterDate convertStringByData:[NSString stringWithFormat:@"%@",reply.w_time]];
    //根据昵称内容获取行高
    NSDictionary * dic1=@{NSFontAttributeName:[UIFont systemFontOfSize:13]};
    CGRect frame1= [self.nickNameAndContentLabel.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.nickNameAndContentLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic1 context:nil];
    self.nickNameAndContentLabel.frame=CGRectMake(CGRectGetMaxX(self.avatar.frame)+5,CGRectGetMaxY(self.nickName.frame), mainWidth-3*10-45-35-3*5, frame1.size.height);
    
    CGFloat maxY = CGRectGetMaxY(self.avatar.frame) > CGRectGetMaxY(self.nickNameAndContentLabel.frame) ? CGRectGetMaxY(self.avatar.frame) : CGRectGetMaxY(self.nickNameAndContentLabel.frame);
    //    self.timeLabel.frame=CGRectMake(10, maxY, mainWidth-4*10-50, 20);
    self.replyOrDeleteBtn.frame=CGRectMake(mainWidth-3*10-45-5-30, maxY, 30, 20);
    self.lineView.frame = CGRectMake(5, CGRectGetMaxY(self.replyOrDeleteBtn.frame)+2, CGRectGetMaxX(self.replyOrDeleteBtn.frame)-5, 0.5);
    self.replyViewHeight=CGRectGetMaxY(self.lineView.frame);
}
@end
