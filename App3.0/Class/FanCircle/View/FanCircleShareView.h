//
//  FanCircleShareView.h
//  App3.0
//
//  Created by syn on 2017/7/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"

@interface FanCircleShareView : UIView
@property(nonatomic,strong)FanCircleShare *share;
@end
