//
//  AddStatusTopCell.m
//  App3.0
//
//  Created by mac on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddStatusTopCell.h"
#import "UUImageAvatarBrowser.h"
@interface AddStatusTopCell()<UITextViewDelegate>
/**所有的除了加号的图片*/
@property(nonatomic,strong)NSMutableArray *imagesArray;
@end
@implementation AddStatusTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.inputView=[[UITextView alloc] initWithFrame: CGRectMake(0, 0, mainWidth, 100)];
        self.inputView.textColor=[UIColor blackColor];
        self.inputView.backgroundColor=[UIColor whiteColor];
        [self.contentView addSubview:self.inputView];
        //设置提示文本
        self.tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.inputView.frame)/3.0, 20)];;
        self.tipsLabel.font=[UIFont systemFontOfSize:12];
        self.tipsLabel.text=@"这一刻的想法...";
        self.tipsLabel.textAlignment=NSTextAlignmentLeft;
        self.tipsLabel.textColor=mainGrayColor;
        [self.inputView addSubview:self.tipsLabel];
        self.inputView.font=[UIFont systemFontOfSize:15];
        self.inputView.delegate=self;
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
#pragma mark-刷新界面
-(void)setPhotosArray:(NSArray *)photosArray{
    _photosArray=photosArray;
    self.deletBtnArray=[NSMutableArray array];
    self.imagesArray=[NSMutableArray array];
    //清空界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]||[view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    UIImageView *img;
    if (self.photosArray.count==0) {
        self.height=200;
        return;
    }
    for (int i=0; i<self.photosArray.count; i++) {
        if (i<9) {
            int columns=4;
            //列数
            int col=i%columns;
            //行数
            int row=i/columns;
            UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10+col*((mainWidth-5*10)/4.0+10),CGRectGetMaxY(self.inputView.frame)+10+row*((mainWidth-5*10)/4.0+10), (mainWidth-5*10)/4.0, (mainWidth-5*10)/4.0)];
            imgView.image=[self.photosArray objectAtIndex:i];
            img=imgView;
            imgView.userInteractionEnabled=YES;
            [self.contentView addSubview:imgView];
            if (i<self.photosArray.count-1) {
                [self.imagesArray addObject:imgView];
                imgView.tag=i+1;
                UIButton *deleteBtn=[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)-10, CGRectGetMinY(imgView.frame)-10, 20, 20)];
                deleteBtn.backgroundColor = [UIColor whiteColor];
                [deleteBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
                deleteBtn.layer.cornerRadius = CGRectGetHeight(deleteBtn.frame)/2;
                deleteBtn.layer.masksToBounds = YES;
                deleteBtn.tag=i+1;
                [self.contentView addSubview:deleteBtn];
                [self.deletBtnArray addObject:deleteBtn];
                [imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLargeImage:)]];
            }
            if (i==self.photosArray.count-1) {
                self.lastImage=imgView;
            }
        }
        self.height=CGRectGetMaxY(img.frame)+20;
    }
}
-(void)enLargeImage:(UITapGestureRecognizer *)tap{
    [self.inputView resignFirstResponder];
    [UUImageAvatarBrowser showImage:(UIImageView *)tap.view title:nil];
}
-(void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length>0) {
        self.tipsLabel.hidden=YES;
    }else{
        self.tipsLabel.hidden=NO;
    }
}
-(void)setShare:(FanCircleShare *)share{
    _share=share;
    self.shareView=[[FanCircleShareView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.inputView.frame), mainWidth-2*10, 50)];
    [self.contentView addSubview:self.shareView];
    self.shareView.share=share;
    self.height=CGRectGetMaxY(self.shareView.frame)+20;
}
@end
