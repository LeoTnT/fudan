//
//  FansCircleTopCell.m
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FansCircleTopCell.h"
#import "AddStatusViewController.h"
@interface FansCircleTopCell()
@property(nonatomic,strong)UIImageView *headImageView;
@end
@implementation FansCircleTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
#pragma mark-重写构造方法
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //背景图片
        self.backImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,mainWidth,200 )];
        self.backImage.userInteractionEnabled=YES;
        self.backImage.image=[UIImage imageNamed:@"user_fans_bg"];
        [self.contentView addSubview:self.backImage];
        //头像按钮
        self.headImageView=[[UIImageView alloc] initWithFrame:CGRectMake(20,200-15-60 , 60, 60)];
        self.headImageView.userInteractionEnabled=YES;
        self.headImageView.image=[UIImage imageNamed:@"user_fans_avatar"];
        [self.contentView addSubview:self.headImageView];
        self.headImg=[[UIButton alloc] initWithFrame:CGRectMake(20,200-15-60 , 60, 60)];
        [self.headImg setBackgroundColor:[UIColor clearColor]];
        self.headImg.layer.borderWidth=2;
        self.headImg.layer.borderColor=[UIColor whiteColor].CGColor;
        [self.contentView addSubview:self.headImg];
        //昵称
        self.nickName=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImg.frame)+10, CGRectGetMinY(self.headImg.frame)+(60-20)/2.0, mainWidth-20-60-10*2,20 )];
        self.nickName.textColor=[UIColor whiteColor];
        self.nickName.font=[UIFont systemFontOfSize:15];
        self.nickName.numberOfLines=0;
        [self.contentView addSubview:self.nickName];
        //提示语
//        self.tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(self.backImage.frame) , mainWidth-60-3*10,40)];
//        self.tipsLabel.backgroundColor=[UIColor whiteColor];
//        self.tipsLabel.text=@"这一刻的想法...";
//        self.tipsLabel.font=[UIFont systemFontOfSize:15];
//        self.tipsLabel.textColor=[UIColor lightGrayColor];
//        self.tipsLabel.userInteractionEnabled=YES;
//        [self.contentView addSubview:self.tipsLabel];
        //图片按钮
//        self.picture=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-10-32, CGRectGetMaxY(self.backImage.frame)+(40-30)/2.0, 32, 30)];
//        [self.contentView addSubview:self.picture];
//        [self.picture setBackgroundImage:[UIImage imageNamed:@"user_fans_photo"] forState:UIControlStateNormal];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
-(void)setNickAndAvatarAndBgImgDic:(NSDictionary *)nickAndAvatarAndBgImgDic{
    _nickAndAvatarAndBgImgDic=nickAndAvatarAndBgImgDic;
    self.nickName.text=nickAndAvatarAndBgImgDic[@"nickName"];
    //根据内容长度计算高度
    [self.headImageView getImageWithUrlStr:nickAndAvatarAndBgImgDic[@"avatar"] andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    //用户可能未设置背景图片
    if ([NSString stringWithFormat:@"%@",nickAndAvatarAndBgImgDic[@"bgImg"]].length) {//加默认图片防止换背景图的时候闪一下
        [self.backImage getImageWithUrlStr:nickAndAvatarAndBgImgDic[@"bgImg"]andDefaultImage:[UIImage imageNamed:@"user_fans_bg"]];
    }else{
        self.backImage.image=[UIImage imageNamed:@"user_fans_bg"];
    }
   
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
