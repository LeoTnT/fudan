//
//  AddStatusTopCell.h
//  App3.0
//
//  Created by mac on 2017/3/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
#import "FanCircleShareView.h"

@interface AddStatusTopCell : UITableViewCell<UITextViewDelegate>
/**输入框*/
@property(nonatomic,strong)UITextView *inputView;
/**输入框标签*/
@property(nonatomic,strong)UILabel *tipsLabel;
/**图片数组*/
@property(nonatomic,strong)NSArray *photosArray;
/**cell的高度*/
@property(nonatomic,assign)CGFloat height;
/**最后一个图片*/
@property(nonatomic,strong)UIImageView *lastImage;
/**所有的删除按钮*/
@property(nonatomic,strong)NSMutableArray *deletBtnArray;
/**是否是分享*/
@property(nonatomic,assign)BOOL isShare;
/**分享内容模型*/
@property(nonatomic,strong)FanCircleShare *share;
/**分享界面*/
@property(nonatomic,strong)FanCircleShareView *shareView;
@end
