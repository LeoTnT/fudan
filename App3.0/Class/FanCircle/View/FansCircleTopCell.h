//
//  FansCircleTopCell.h
//  App3.0
//
//  Created by mac on 2017/3/14.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FansCircleTopCell : UITableViewCell
/**背景图片**/
@property(nonatomic,strong)UIImageView *backImage;
/**头像按钮**/
@property(nonatomic,strong)UIButton *headImg;
/**昵称**/
@property(nonatomic,strong)UILabel *nickName;
/**图片按钮**/
//@property(nonatomic,strong)UIButton *picture;
/**昵称  头像字典*/
@property(nonatomic,strong)NSDictionary *nickAndAvatarAndBgImgDic;
/**提示语*/
//@property(nonatomic,strong)UILabel *tipsLabel;
@end
