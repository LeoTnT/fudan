//
//  PraiseView.m
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "PraiseView.h"
#import "FansCircleModel.h"

@implementation PraiseView
#pragma mark-重写构造方法
-(instancetype)init{
    if (self=[super init]) {
        self.praiseLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 0, mainWidth-4*10-50, 12)];
        self.praiseLabel.textAlignment=NSTextAlignmentLeft;
        self.praiseLabel.textColor=[UIColor hexFloatColor:@"5a6e97"];
        self.praiseLabel.backgroundColor=[UIColor clearColor];
        self.praiseLabel.numberOfLines=0;
        self.praiseLabel.font=[UIFont systemFontOfSize:12];
        [self addSubview:self.praiseLabel];
    }
    return self;
}
#pragma mark-测试
-(void)setPraiseArray:(NSArray *)praiseArray{
    _praiseArray=praiseArray;
    if (praiseArray.count==0) {
        self.praiseHeight=0;
        return;
    }
    NSMutableString *tempStr=[NSMutableString string];
    for (Thumbs_up_user *user in praiseArray) {
        [tempStr appendString:[NSString stringWithFormat:@"%@,",user.nikename]];
    }
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[tempStr substringToIndex:tempStr.length-1]];
    long length=tempStr.length-1;
   [attri addAttribute:NSForegroundColorAttributeName value:[UIColor hexFloatColor:@"5a6e97"] range:NSMakeRange(0, length)];
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    // 表情图片
    attch.image = [UIImage imageNamed:@"user_fans_favour_red"];
    // 设置图片大小
    attch.bounds = CGRectMake(0, -1, 10, 10);
    // 创建带有图片的富文本
    NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:string atIndex:0];// 插入某个位置
    // 用label的attributedText属性来使用富文本
    self.praiseLabel.attributedText = attri;
    self.praiseLabel.contentMode=UIViewContentModeTop;
    //计算高度
    NSDictionary * dic1=@{NSFontAttributeName:[UIFont systemFontOfSize:12]};
    CGRect frame1= [self.praiseLabel.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.praiseLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic1 context:nil];
    self.praiseLabel.frame=CGRectMake(10,0 , mainWidth-4*10-50,frame1.size.height);
    self.praiseHeight=CGRectGetHeight(self.praiseLabel.frame);
}
@end
