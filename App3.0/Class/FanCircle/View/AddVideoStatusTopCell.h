//
//  AddVideoStatusTopCell.h
//  App3.0
//
//  Created by mac on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddVideoStatusTopCell : UITableViewCell<UITextViewDelegate>
/**输入框*/
@property(nonatomic,strong)UITextView *inputView;
/**按钮*/
@property(nonatomic,strong)UIButton *playBtn;
/**视频缩略图*/
@property(nonatomic,strong)UIImage *image;
@end
