//
//  ReplyView.h
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface ReplyView : UIView
/**头像*/
@property(nonatomic,strong)UIImageView *avatar;
/**昵称*/
@property(nonatomic,strong)UILabel *nickName;
/**分割线*/
@property(nonatomic,strong)UIView *lineView;
/**昵称  评论内容*/
@property(nonatomic,strong)UILabel *nickNameAndContentLabel;
/**时间*/
@property(nonatomic,strong)UILabel *timeLabel;
/**回复或者删除按钮*/
@property(nonatomic,strong)UIButton *replyOrDeleteBtn;
/**一条回复的高度*/
@property(nonatomic,assign)CGFloat replyViewHeight;
/**测试数组*/
@property(nonatomic,strong) Reply*reply;
@end
