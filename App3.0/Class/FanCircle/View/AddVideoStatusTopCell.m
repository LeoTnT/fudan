//
//  AddVideoStatusTopCell.m
//  App3.0
//
//  Created by mac on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AddVideoStatusTopCell.h"
@interface AddVideoStatusTopCell()<UITextViewDelegate>
{
    UIImageView *imageView;
}
/**输入框标签*/
@property(nonatomic,strong)UILabel *tipsLabel;
@end
@implementation AddVideoStatusTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.inputView=[[UITextView alloc] initWithFrame: CGRectMake(0, 0, mainWidth, 100)];
        self.inputView.textColor=[UIColor blackColor];
        self.inputView.backgroundColor=[UIColor whiteColor];
        [self.contentView addSubview:self.inputView];
        //设置提示文本
        self.tipsLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.inputView.frame)/3.0, 20)];
        self.tipsLabel.font=[UIFont systemFontOfSize:12];
        self.tipsLabel.text=@"这一刻的想法...";
        self.tipsLabel.textAlignment=NSTextAlignmentLeft;
        self.tipsLabel.textColor=mainGrayColor;
        [self.inputView addSubview:self.tipsLabel];
        self.inputView.font=[UIFont systemFontOfSize:15];
        self.inputView.delegate=self;
        //图片
        imageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 100+10, mainWidth/3, 120)];
        imageView.userInteractionEnabled=YES;
        [self.contentView addSubview:imageView];
        //按钮
        CGFloat width=50;
        self.playBtn=[[UIButton alloc] initWithFrame:CGRectMake(( mainWidth/3-width)/2.0, (120-width)/2.0, width, width)];
        [self.playBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_play"] forState:UIControlStateNormal];
        [imageView addSubview:self.playBtn];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
-(void)setImage:(UIImage *)image{
    _image=image;
    imageView.image=image;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length>0) {
        self.tipsLabel.hidden=YES;
    }else{
        self.tipsLabel.hidden=NO;
    }
}
@end
