//
//  PraiseAndCommentView.h
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PraiseAndCommentView : UIView
/**点赞数组*/
@property(nonatomic,strong)NSArray *praiseArray;
/**评论回复数组*/
@property(nonatomic,strong)NSArray *commentArray;
/**点赞高度*/
@property(nonatomic,assign)CGFloat praiseHeight;
/**总高度*/
@property(nonatomic,assign)CGFloat height;
/**回复或者删除评论按钮数组*/
@property(nonatomic,strong)NSArray *replyOrDeleteCommentBtnArray;
@end
