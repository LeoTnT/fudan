//
//  FanListCell.h
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FansCircleModel.h"
@interface FanListCell : UITableViewCell
@property(nonatomic,strong)Fan *fan;
@property(nonatomic,strong)UIButton *tipsButton;
@property(nonatomic,assign)BOOL isSelected;
@end
