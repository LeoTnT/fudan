//
//  FanListCell.m
//  App3.0
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FanListCell.h"
@interface FanListCell()
@property(nonatomic,strong)UIImageView *headImg;
@property(nonatomic,strong)UILabel *nameLabel;
@end
@implementation FanListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        //图片
        self.headImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
        [self.contentView addSubview:self.headImg];
        //昵称
        self.nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImg.frame)+10, 10, mainWidth-30-4*10-20, 30)];
        [self.contentView addSubview:self.nameLabel];
        self.tipsButton=[[UIButton alloc] initWithFrame:CGRectMake(mainWidth-20-20, (50-20)/2.0, 20, 20)];
        [self.contentView addSubview:self.tipsButton];
        [self.tipsButton setBackgroundImage:[UIImage imageNamed:@"reg_unselect"] forState:UIControlStateNormal];
//        [self.tipsButton setBackgroundImage:[UIImage imageNamed:@"reg_select"] forState:UIControlStateSelected];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
#pragma mark-刷新界面
-(void)setFan:(Fan *)fan{
    _fan=fan;
    [self.headImg getImageWithUrlStr:fan.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.nameLabel.text=fan.showname;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
