//
//  FansCircleStatusCell.m
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FansCircleStatusCell.h"
#import "PraiseAndCommentView.h"
#import "UUImageAvatarBrowser.h"
#import "ReuseImageView.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import <AVFoundation/AVTime.h>
#import "XSFormatterDate.h"
#import "CLPlayerView.h"

#define LEFTSPACE  10

@interface FansCircleStatusCell ()
@property(nonatomic,strong)UIImageView *headImageView;
@end
@implementation FansCircleStatusCell
#pragma mark-重写构造方法
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        CGFloat space=10;
        self.headImageView=[[UIImageView alloc] initWithFrame:CGRectMake(space, space, 45, 45)];
        self.headImageView.userInteractionEnabled=YES;
        self.headImageView.image=[UIImage imageNamed:@"user_fans_avatar"];
        [self.contentView addSubview:self.headImageView];
        self.headImgBtn=[[UIButton alloc] initWithFrame:CGRectMake(space,space, 45, 45)];
        [self.headImgBtn setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.headImgBtn];
        self.nickNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImageView.frame)+space, CGRectGetMinY(self.headImageView.frame), mainWidth-3*space-45, 25)];
        self.nickNameLabel.font=[UIFont boldSystemFontOfSize:14];
        self.nickNameLabel.textColor=[UIColor colorWithRed:90/255.0 green:107/255.0 blue:143/255.0 alpha:1];
        [self.contentView addSubview:self.nickNameLabel];
        self.contentLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame),CGRectGetMaxY(self.nickNameLabel.frame) , CGRectGetWidth(self.nickNameLabel.frame), 20)];
        self.contentLabel.font=[UIFont systemFontOfSize:15];
        self.contentLabel.numberOfLines=0;
        self.contentLabel.userInteractionEnabled=YES;
        [self.contentView addSubview:self.contentLabel];
        self.lookPartOrAllBtn=[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)-10, 40, 40)];
        self.lookPartOrAllBtn.hidden=YES;
        [self.lookPartOrAllBtn setTitleColor:[UIColor colorWithRed:90/255.0 green:107/255.0 blue:143/255.0 alpha:1] forState:UIControlStateNormal];
        self.lookPartOrAllBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        [self.lookPartOrAllBtn setTitle:@"全文" forState:UIControlStateNormal];
        [self.contentView addSubview:self.lookPartOrAllBtn];
        self.positionLabel=[[UILabel alloc] init];
        self.positionLabel.font=[UIFont systemFontOfSize:13];
        self.positionLabel.textColor=[UIColor hexFloatColor:@"5a6e97"];
        [self.contentView addSubview:self.positionLabel];
        self.timeLabel=[[UILabel alloc] init];
        self.timeLabel.font=[UIFont systemFontOfSize:13];
        self.timeLabel.textColor=mainGrayColor;
        [self.contentView addSubview:self.timeLabel];
        self.deleteBtn=[[UIButton alloc] init];
        [self.deleteBtn setTitleColor:[UIColor colorWithRed:90/255.0 green:107/255.0 blue:143/255.0 alpha:1] forState:UIControlStateNormal];
        [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        self.deleteBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.deleteBtn];
        self.praiseAndCommentBtn=[[UIButton alloc] init];
        [self.praiseAndCommentBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_handle"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.praiseAndCommentBtn];
        self.praiseAndCommentClearBtn=[[UIButton alloc] init];
        [self.praiseAndCommentClearBtn setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.praiseAndCommentClearBtn];
        self.praiseAndCommentBtnView=[[UIView alloc] init];
        self.praiseAndCommentBtnView.backgroundColor=[UIColor hexFloatColor:@"4c5154"];
        self.praiseAndCommentBtnView.layer.cornerRadius=5;
        [self.contentView addSubview:self.praiseAndCommentBtnView];
        self.praiseBtn=[[UIButton alloc] init];
        [self.praiseBtn setImage:[UIImage imageNamed:@"user_fans_favour"] forState:UIControlStateNormal];
        [self.praiseAndCommentBtnView addSubview:self.praiseBtn];
        self.praiseBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.praiseBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        self.commentBtn=[[UIButton alloc] init];
        [self.commentBtn setImage:[UIImage imageNamed:@"user_fans_comment"] forState:UIControlStateNormal];
        [self.commentBtn setTitle:@"  评论" forState:UIControlStateNormal];
        [self.praiseAndCommentBtnView addSubview:self.commentBtn];
        self.commentBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.commentBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        self.praiseAndCommentView=[[PraiseAndCommentView alloc] init];
        [self.contentView addSubview:self.praiseAndCommentView];
        self.commentAndPraiseTopView=[[UIImageView alloc] init];
        self.commentAndPraiseTopView.image=[UIImage imageNamed:@"user_fans_triangle"];
        [self.contentView addSubview:self.commentAndPraiseTopView];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
#pragma mark-刷新界面
-(void)setStatus:(Status *)status{
    _status=status;
    //清界面
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[ReuseImageView class]]||[view isKindOfClass:[CLPlayerView class]]||[view isKindOfClass:[FanCircleShareView class]]) {
            [view removeFromSuperview];
        }
    }
    [self.headImageView getImageWithUrlStr:status.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
    self.nickNameLabel.text=status.nickname;
     CGFloat space=10,tempHeight = CGRectGetMaxY(self.nickNameLabel.frame);
    self.contentLabel.attributedText=[status.content changeToEmojiStringWithHaveReply:NO WithFontSize:15];
    if (status.content.length<=0) {
        self.contentLabel.frame=CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(self.nickNameLabel.frame) , CGRectGetWidth(self.nickNameLabel.frame), 0);
        self.lookPartOrAllBtn.hidden=YES;
    }else{
        CGRect frame= [self.contentLabel.attributedText boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.contentLabel.frame), 100000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if(frame.size.height>120){
            if (self.lookAll) {//判断收起状态  默认没有设置过是收起
                self.contentLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), CGRectGetMaxY(self.nickNameLabel.frame), CGRectGetWidth(self.nickNameLabel.frame),frame.size.height);
                [self.lookPartOrAllBtn setTitle:@"收起" forState:UIControlStateNormal];
            }else{
                self.contentLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), CGRectGetMaxY(self.nickNameLabel.frame), CGRectGetWidth(self.nickNameLabel.frame),120);
                [self.lookPartOrAllBtn setTitle:@"全文" forState:UIControlStateNormal];
            }
            self.lookPartOrAllBtn.hidden=NO;
            self.lookPartOrAllBtn.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), CGRectGetMaxY(self.contentLabel.frame)-10, 40, 40);
        }else{
            self.contentLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), CGRectGetMaxY(self.nickNameLabel.frame), CGRectGetWidth(self.nickNameLabel.frame),frame.size.height);
            self.lookPartOrAllBtn.hidden=YES;
        }
    }
    if (self.lookPartOrAllBtn.hidden) {
        tempHeight=CGRectGetMaxY(self.contentLabel.frame);
    }else{
        tempHeight=CGRectGetMaxY(self.lookPartOrAllBtn.frame);
    }
    if (status.is_share) {
        self.shareView=[[FanCircleShareView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImageView.frame)+space, tempHeight+space, mainWidth-CGRectGetWidth(self.headImageView.frame)-3*space, 50)];
        [self.contentView addSubview:self.shareView];
        self.shareView.share=status.share_info;
        tempHeight=CGRectGetMaxY(self.shareView.frame);
    }else if (status.imgs.count) {
        self.imageUrlArray=[NSMutableArray array];
        if (status.imgs.count==1) {
            CGFloat imgWidth = CGRectGetWidth(self.contentLabel.frame) - 20;
            CGFloat imgHeight = CGRectGetWidth(self.contentLabel.frame) - 30;
            if (status.width.floatValue >0 && status.height.floatValue >0) {
                CGFloat rate = status.width.floatValue/status.height.floatValue;
                if (status.width.floatValue <= status.height.floatValue) {//宽 < 高
                    if (status.height.floatValue < imgHeight) {
                        imgHeight =status.height.floatValue;
                    }
                    imgWidth = rate *imgHeight;
                }else{
                    if (status.width.floatValue < imgWidth) {
                        imgWidth =status.height.floatValue;
                    }
                    imgHeight = imgWidth/rate;
                }
            }
            ReuseImageView *image=[[ReuseImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame),tempHeight+space , imgWidth, imgHeight)];
            image.userInteractionEnabled=YES;
            [image getImageWithUrlStr:status.imgs.firstObject andDefaultImage:[UIImage imageNamed:@"no_pic"]];
            image.clipsToBounds=NO;//超出裁剪
            [self.contentView addSubview:image];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
            [image addGestureRecognizer:tap];
            tempHeight=CGRectGetMaxY(image.frame);
            image.tag=0;
            [self.imageUrlArray addObject:status.imgs.firstObject];
            tempHeight=CGRectGetMaxY(image.frame);
        }else if(status.imgs.count==2){
            CGFloat width=(CGRectGetWidth(self.contentLabel.frame)-5)/2.0;
            CGFloat height=width;
            for (int i=0; i<status.imgs.count; i++) {
                int column=i%2;
                ReuseImageView *image=[[ReuseImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame)+column*(width+5), tempHeight+space ,width,height)];
                image.userInteractionEnabled=YES;
                [image getImageWithUrlStr:status.imgs[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                image.contentMode=UIViewContentModeScaleAspectFill;
                image.clipsToBounds=YES;//超出裁剪
                [self.contentView addSubview:image];
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
                [image addGestureRecognizer:tap];
                image.tag=i;
                [self.imageUrlArray addObject:status.imgs[i]];
                if (i==1) {
                    tempHeight=CGRectGetMaxY(image.frame);
                }
            }
        }else{
            CGFloat width=(CGRectGetWidth(self.contentLabel.frame)-2*5)/3.0;
            CGFloat height=width;
            UIView *tempView;
            for (int i=0; i<status.imgs.count; i++) {
                int line=i/3;
                int column=i%3;
                ReuseImageView *image=[[ReuseImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame)+column*(5+width), tempHeight+space+line*width+(line+1)*5,width,height)];
                image.userInteractionEnabled=YES;
                [image getImageWithUrlStr:status.imgs[i] andDefaultImage:[UIImage imageNamed:@"no_pic"]];
                image.contentMode = UIViewContentModeScaleAspectFill;
                image.clipsToBounds=YES;//超出裁剪
                [self.contentView addSubview:image];
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enLarge:)];
                [image addGestureRecognizer:tap];
                image.tag=i;
                tempView=image;
                [self.imageUrlArray addObject:status.imgs[0]];
            }
            tempHeight=CGRectGetMaxY(tempView.frame);
        }
    }else if(status.videos.count){
        CGFloat imgWidth = CGRectGetWidth(self.contentLabel.frame) - 20;
        CGFloat imgHeight = CGRectGetWidth(self.contentLabel.frame) - 30;
        if (status.width.floatValue >0 && status.height.floatValue >0) {
            CGFloat rate = status.width.floatValue/status.height.floatValue;
            if (status.width.floatValue <= status.height.floatValue) {//宽 < 高
                if (status.height.floatValue < imgHeight) {
                    imgHeight =status.height.floatValue;
                }
                imgWidth = rate *imgHeight;
            }else{
                if (status.width.floatValue < imgWidth) {
                    imgWidth =status.height.floatValue;
                }
                imgHeight = imgWidth/rate;
            }
        }
        self.videoImageView=[[ReuseImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nickNameLabel.frame),tempHeight+space , imgWidth, imgHeight)];
        self.videoImageView.userInteractionEnabled=YES;
        [self.videoImageView getImageWithUrlStr:((Video *)status.videos[0]).img andDefaultImage:[UIImage imageNamed:@"no_pic"]];
        [self.contentView addSubview:self.videoImageView];
        CGFloat width=40;
        //播放按钮
        self.playVideoBtn=[[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.videoImageView.frame)-40)/2.0, (CGRectGetHeight(self.videoImageView.frame)-40)/2.0, width, width)];
        [self.playVideoBtn setBackgroundImage:[UIImage imageNamed:@"user_fans_play"] forState:UIControlStateNormal];
        [self.videoImageView addSubview:self.playVideoBtn];
        tempHeight=CGRectGetMaxY(self.videoImageView.frame);
    }
    self.timeLabel.text=[XSFormatterDate dateWithTimeIntervalString:[NSString stringWithFormat:@"%@",status.w_time]];
    if ([status.position isEqualToString:@"0"]|| status.position.length<=0) {
        self.positionLabel.frame=CGRectZero;
        self.timeLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), tempHeight+space,  CGRectGetWidth(self.nickNameLabel.frame), 20);
    }else{
        self.positionLabel.text=status.position;
        self.positionLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame), tempHeight+space, CGRectGetWidth(self.nickNameLabel.frame), 20);
        self.timeLabel.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame),CGRectGetMaxY(self.positionLabel.frame),  CGRectGetWidth(self.nickNameLabel.frame), 20);
    }
     self.deleteBtn.frame=CGRectMake(CGRectGetMinX(self.timeLabel.frame)+120, CGRectGetMinY(self.timeLabel.frame), 50, 20);
    if ([status.is_owner integerValue]==1) {
        self.deleteBtn.hidden=NO;
    }else{
        self.deleteBtn.hidden=YES;
    }
    if ([status.is_thumbs_up integerValue]==1) {
        [self.praiseBtn setTitle:@"  取消" forState:UIControlStateNormal];
    }else{
         [self.praiseBtn setTitle:@"  赞" forState:UIControlStateNormal];
    }
    self.praiseAndCommentBtn.frame=CGRectMake(mainWidth-space-25, CGRectGetMinY(self.timeLabel.frame)-2.5, 25, 25);
    self.praiseAndCommentClearBtn.frame=CGRectMake(mainWidth-50, CGRectGetMinY(self.timeLabel.frame)-15, 50, 50);
    self.praiseAndCommentBtnView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentBtn.frame)-10, CGRectGetMinY(self.praiseAndCommentBtn.frame)-5, 0, 35);
    self.praiseBtn.frame=CGRectMake(0, 0, 0, 35);
    self.commentBtn.frame=CGRectMake(75, 0, 0, 35);
    self.praiseAndCommentView.praiseArray=status.thumbs_up_users;
    self.praiseAndCommentView.commentArray=status.reply;
    self.praiseAndCommentView.frame=CGRectMake(CGRectGetMinX(self.nickNameLabel.frame),CGRectGetMaxY(self.timeLabel.frame)+10 ,CGRectGetWidth(self.nickNameLabel.frame), self.praiseAndCommentView.height);
    if (self.status.thumbs_up_users.count||self.status.reply.count) {
        self.commentAndPraiseTopView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentView.frame)+10, CGRectGetMinY(self.praiseAndCommentView.frame)-5, 10, 5);
        self.height=CGRectGetMaxY(self.praiseAndCommentView.frame)+space;
    }else{
        self.commentAndPraiseTopView.frame=CGRectZero;
        self.praiseAndCommentView.frame=CGRectZero;
        self.height=CGRectGetMaxY(self.praiseAndCommentBtn.frame)+space;
    }
    self.replyOrDeleteCommentBtnArray=self.praiseAndCommentView.replyOrDeleteCommentBtnArray;
  
   }
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark-放大图片
-(void)enLarge:(UITapGestureRecognizer *)tap{
    NSInteger imageTag = tap.view.tag;
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:self.status.imgs currentImageIndex:imageTag pageControlHidden:YES];
    [browser setActionSheetWithTitle:@"" delegate:self cancelButtonTitle:Localized(@"cancel_btn") deleteButtonTitle:@"" otherButtonTitles:Localized(@"person_qr_save"), nil];
    browser.isCusctomAdd = YES;
    browser.delegate = self;
    [self.currentVC.navigationController setNavigationBarHidden:YES animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
    [self.currentVC.tabBarController.view addSubview:browser];
}
-(void)photoBrowserDismiss{
    [self.currentVC.navigationController setNavigationBarHidden:NO animated:NO];
    [self.currentVC.tabBarController.tabBar setHidden:YES];
}

#pragma mark    -   XLPhotoBrowserDelegate

- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    // do something yourself
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"点击了actionSheet索引是:%zd , 当前展示的图片索引是:%zd",actionSheetindex,currentImageIndex);
            [browser saveCurrentShowImage];
        }
            break;
        default:
            break;
    }
}
-(void)setPraiseAndConmmentViewHidden:(BOOL)praiseAndConmmentViewHidden{
    _praiseAndConmmentViewHidden=praiseAndConmmentViewHidden;
    if (praiseAndConmmentViewHidden) {
        self.praiseAndCommentBtnView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentBtn.frame)-10, CGRectGetMinY(self.praiseAndCommentBtn.frame)-5, 0, 35);
        self.praiseBtn.frame=CGRectMake(0, 0, 0, 35);
        self.commentBtn.frame=CGRectMake(75, 0, 0, 35);
        self.praiseAndCommentBtn.selected=NO;
    }else{
        self.praiseAndCommentBtnView.frame=CGRectMake(CGRectGetMinX(self.praiseAndCommentBtn.frame)-10-150, CGRectGetMinY(self.praiseAndCommentBtn.frame)-5, 150, 35);
        self.praiseBtn.frame=CGRectMake(0, 0, 150/2.0, 35);
        self.commentBtn.frame=CGRectMake(75, 0, 150/2.0, 35);
        self.praiseAndCommentBtn.selected=YES;
    }
}
@end
