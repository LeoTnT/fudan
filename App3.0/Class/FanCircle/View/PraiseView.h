//
//  PraiseView.h
//  App3.0
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PraiseView : UIView
/**测试数组*/
@property(nonatomic,strong)NSArray *praiseArray;
/**展示标签*/
@property(nonatomic,strong)UILabel *praiseLabel;
/**点赞高度*/
@property(nonatomic,assign)CGFloat praiseHeight;
@end
