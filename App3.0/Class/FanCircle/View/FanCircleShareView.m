//
//  FanCircleShareView.m
//  App3.0
//
//  Created by syn on 2017/7/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "FanCircleShareView.h"
@interface FanCircleShareView()
@property(nonatomic,strong)UIImageView *shareImage;
@property(nonatomic,strong)UILabel *shareTitleLabel;
@end

@implementation FanCircleShareView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor hexFloatColor:@"ececec"];
        CGFloat smallSpace=5,space=10,height=50;
        self.shareImage=[[UIImageView alloc] initWithFrame:CGRectMake(smallSpace, smallSpace, height-2*smallSpace, 50-2*smallSpace)];
        self.shareImage.image=[UIImage imageNamed:@"AppIcon"];
        [self addSubview:self.shareImage];
        self.shareTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.shareImage.frame)+space, smallSpace, CGRectGetWidth(frame)-2*space-smallSpace-(height-2*smallSpace), 50-2*smallSpace)];
        self.shareTitleLabel.numberOfLines=0;
        self.shareTitleLabel.font=[UIFont systemFontOfSize:14];
        [self addSubview:self.shareTitleLabel];
    }
    return self;
}
-(void)setShare:(FanCircleShare *)share{
    _share=share;
    [self.shareImage getImageWithUrlStr:share.image_url andDefaultImage:[UIImage imageNamed:@"AppIcon"]];
    self.shareTitleLabel.text=share.head;
}
@end
