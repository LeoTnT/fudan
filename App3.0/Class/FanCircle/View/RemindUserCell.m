//
//  RemindUserCell.m
//  App3.0
//
//  Created by syn on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "RemindUserCell.h"
#import "FansCircleModel.h"

@implementation RemindUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setUserArray:(NSArray *)userArray{
    _userArray=userArray;
    CGFloat tempWidth=mainWidth/1.5;
    CGFloat imageWidth=30;
    for (UIView *view in self.contentView.subviews) {
        if(view .tag==100){
            [view removeFromSuperview];
        }
    }
    if (userArray.count) {
        for (int i=0; i<userArray.count; i++) {
            Fan *fan=[userArray objectAtIndex:i];
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(tempWidth,(50-imageWidth)/2.0 , imageWidth, imageWidth)];
            imageView.tag=100;
            [imageView getImageWithUrlStr:fan.avatar andDefaultImage:[UIImage imageNamed:@"user_fans_avatar"]];
            [self.contentView addSubview:imageView];
            tempWidth+=imageWidth+5;
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
