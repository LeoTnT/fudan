//
//  CartModel.h
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

//规格信息
@interface CartProductSpecValueParser : NSObject
@property(nonatomic,copy)NSString *spec_value_id;
@property(nonatomic,copy)NSString *spec_value_name;
@end

@interface CartProductSpecParser : NSObject
@property(nonatomic,copy)NSString *spec_id;
@property(nonatomic,copy)NSString *spec_name;
@property(nonatomic,strong)NSArray *spec_value;
@end
// 购物车每一个商品model
@interface CartProductListParser : NSObject
@property (nonatomic, copy)NSString *product_ext_id;
@property (nonatomic, copy)NSString *product_id;
@property (nonatomic, copy)NSString *product_no;
@property (nonatomic, copy)NSString *product_weight;
@property (nonatomic, copy)NSString *spec_name;
@property (nonatomic, copy)NSString *spec_value;
@property (nonatomic, copy)NSString *stock;
@property (nonatomic, copy)NSString *sell_price;
@property (nonatomic, copy)NSString *market_price;
@property (nonatomic, copy)NSString *supply_price;
@property (nonatomic, copy)NSString *pv;
@property (nonatomic, copy)NSString *score;
@property (nonatomic, strong)NSNumber *coupon;
@property (nonatomic, copy)NSString *user_id;//商家id
@property (nonatomic, copy)NSString *username;
@property (nonatomic, strong)NSNumber *sell_type;
@property (nonatomic, copy)NSString *sup_name;//商家名称
@property (nonatomic, copy)NSString *product_name;
@property (nonatomic, copy)NSString *image;
@property (nonatomic, strong)NSNumber *is_cod;
@property (nonatomic, strong)NSNumber *logistics_type;
@property (nonatomic, strong)NSNumber *total_num;
@property (nonatomic, copy)NSString *total_supply_price;
@property (nonatomic, copy)NSString *total_market_price;
@property (nonatomic, copy)NSString *total_sell_price;
@property (nonatomic, copy)NSString *total_pv;
@property (nonatomic, copy)NSString *total_score;
@property (nonatomic, copy)NSString *total_coupon;
@property (nonatomic, copy)NSString *total_product_weight;
@property (nonatomic, strong)NSArray *spec;//商品所有的规格
@property (nonatomic, assign)BOOL selected;
@end

@interface CartDeliveryListParser : NSObject
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSArray *product_list;
@end

// 购物车每一个商家的商品model，以商家分类
@interface CartProductParser : NSObject
@property (nonatomic, copy)NSString *supply_id;
@property (nonatomic, strong)NSNumber *total_num;
@property (nonatomic, copy)NSString *total_supply_price;
@property (nonatomic, copy)NSString *total_market_price;
@property (nonatomic, copy)NSString *total_sell_price;
@property (nonatomic, copy)NSString *total_pv;
@property (nonatomic, copy)NSString *total_score;
@property (nonatomic, copy)NSString *total_coupon;
@property (nonatomic, copy)NSString *total_product_weight;
@property (nonatomic, copy)NSString *sup_name;
@property (nonatomic, strong)NSNumber *yunfei_total;
@property (nonatomic, strong)NSArray *delivery_list;
@property (nonatomic, assign)BOOL selected;
@end

// 购物车data model
@interface CartDataParser : NSObject
@property (nonatomic, strong)NSNumber *num_total;
@property (nonatomic, strong)NSNumber *yunfei_total;
@property (nonatomic, strong)NSNumber *supply_price_total;
@property (nonatomic, strong)NSNumber *market_price_total;
@property (nonatomic, strong)NSNumber *sell_price_total;
@property (nonatomic, strong)NSNumber *pv_total;
@property (nonatomic, strong)NSNumber *score_total;
@property (nonatomic, strong)NSNumber *coupon_total;
@property (nonatomic, strong)NSNumber *product_weight_total;
@property (nonatomic, strong)NSArray *products; // 包含子字典，key是动态的商家id
@end

// 购物车整体model
@interface CartParser : NSObject
@property (nonatomic, strong)CartDataParser *data;
@end

// 选择要购买的商品存储信息
@interface ProductBuyParser : NSObject
@property (nonatomic, copy)NSString *product_ext_id;//商品id
@property (nonatomic, strong)NSNumber *buyNumber;//购买数量
@property (nonatomic, copy)NSString *sellPrice;//价格
@property (nonatomic, copy)NSString *product_id;


@end

@interface CartModel : NSObject

@end
