//
//  CartInstance.m
//  App3.0
//
//  Created by mac on 17/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartInstance.h"

static CartInstance* cart = nil;

@interface CartInstance()
{
    NSMutableArray *_buyArray; // 存储已选择的购物车商品model
    NSMutableArray *_nomalArray;//购物车全部商品信息model
}
@end

@implementation CartInstance
+(CartInstance*)ShardInstnce
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cart = [[CartInstance alloc] init];
    });
    return cart;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _buyArray = [NSMutableArray array];
        _nomalArray = [NSMutableArray array];
        self.cartStatus = CartStatusNormal;
    }
    return self;
}

- (NSArray *)getBuyProducts
{
    return _buyArray;
}
-(NSArray *)getNormalProducts{
    return _nomalArray;
}
- (void)addOrUpdateProductModel:(ProductBuyParser *)model
{
    for (ProductBuyParser *parser in _buyArray) {
        // 如果已经存在商品信息，更新
        if ([parser.product_ext_id isEqualToString:model.product_ext_id]) {
            parser.buyNumber = model.buyNumber;
            parser.sellPrice = model.sellPrice;
            parser.product_id = model.product_id;
            return;
        }
    }
    // 否则，添加一条商品信息
    [_buyArray addObject:model];
}

- (void)deleteProductModelWithProductExtId:(NSString *)peId
{
    for (ProductBuyParser *parser in _buyArray) {
        // 如果已经存在商品信息，删除
        if ([parser.product_ext_id isEqualToString:peId]) {
            [_buyArray removeObject:parser];
            break;
        }
    }
}

- (void)removeAllProducts
{
    [_buyArray removeAllObjects];
}


- (void)refreshCartTotalView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESHLIST object:nil];
}

#pragma mark - Setter
- (void)setCartStatus:(CartStatus)cartStatus
{
    _cartStatus = cartStatus;
}
@end
