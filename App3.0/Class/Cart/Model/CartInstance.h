//
//  CartInstance.h
//  App3.0
//
//  Created by mac on 17/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartModel.h"

typedef NS_ENUM(NSInteger, CartStatus) {
    CartStatusNormal = 1,
    CartStatusEdit
};

@interface CartInstance : NSObject
+(CartInstance*)ShardInstnce;

// 增加或者刷新已选择列表
- (void)addOrUpdateProductModel:(ProductBuyParser *)model;

// 删除全部
- (void)removeAllProducts;

// 删除一条已选择数据
- (void)deleteProductModelWithProductExtId:(NSString *)peId;

// 获取所有已选择商品
- (NSArray *)getBuyProducts;

// 刷新结算信息view
- (void)refreshCartTotalView;

/**获取所有的商品*/
- (NSArray *) getNormalProducts;
@property (nonatomic) CartStatus cartStatus;
@end
