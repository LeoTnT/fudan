//
//  CartModel.m
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartModel.h"
@implementation CartProductSpecValueParser

@end

@implementation CartProductSpecParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"spec_value" : @"CartProductSpecValueParser"
             };
}
@end

@implementation CartProductListParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"spec" : @"CartProductSpecParser"
             };
}
@end

@implementation CartDeliveryListParser

@synthesize uid,name,product_list;
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"product_list" : @"CartProductListParser"
             };
}
@end

@implementation CartProductParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"delivery_list" : @"CartDeliveryListParser"
             };
}
@end

@implementation CartDataParser
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"products" : @"CartProductParser"
             };
}
@end

@implementation CartParser

@end

@implementation ProductBuyParser

@end

@implementation CartModel

@end
