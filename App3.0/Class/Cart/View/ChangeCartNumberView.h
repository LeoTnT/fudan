//
//  ChangeCartNumberView.h
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeCartNumberViewDelegate <NSObject>

- (void)changeCartNumber:(int)numebr;

@end

@interface ChangeCartNumberView : UIView
@property (nonatomic, weak) id<ChangeCartNumberViewDelegate>delegate;
@property (nonatomic) int proNumber;
@end
