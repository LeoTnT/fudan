//
//  ChangeCartNumberView.m
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ChangeCartNumberView.h"

@interface ChangeCartNumberView()
@property (strong, nonatomic) UIButton *subBtn;
@property (strong, nonatomic) UIButton *addBtn;
@property (strong, nonatomic) UIButton *numberBtn;
@property (strong, nonatomic) UITextField *numberTextField;
@end

@implementation ChangeCartNumberView
@synthesize delegate;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [COLOR_666666 CGColor];
        self.layer.borderWidth = 0.5f;
        _subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_subBtn setBackgroundColor:[UIColor whiteColor]];
        [_subBtn setImage:[UIImage imageNamed:@"mall_cart_sub"] forState:UIControlStateNormal];
        [_subBtn setImage:[UIImage imageNamed:@"mall_cart_sub_h"] forState:UIControlStateDisabled];
        [_subBtn addTarget:self action:@selector(subAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_subBtn];
        
        _numberTextField = [UITextField new];
        _numberTextField.text = @"1";
        _numberTextField.textAlignment = NSTextAlignmentCenter;
        _numberTextField.font = [UIFont boldSystemFontOfSize:13];
        _numberTextField.keyboardType = UIKeyboardTypeNumberPad;
        [self addSubview:_numberTextField];
        _numberTextField.enabled = NO;
        
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setBackgroundColor:[UIColor whiteColor]];
        [_addBtn setImage:[UIImage imageNamed:@"mall_cart_add"] forState:UIControlStateNormal];
        [_addBtn setImage:[UIImage imageNamed:@"mall_cart_add_h"] forState:UIControlStateDisabled];
        [_addBtn addTarget:self action:@selector(addAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_addBtn];

        UIView *lineView1 = [UIView new];
        lineView1.backgroundColor = [UIColor hexFloatColor:@"9F9F9F"];
        [self addSubview:lineView1];
        
        UIView *lineView2 = [UIView new];
        lineView2.backgroundColor = [UIColor hexFloatColor:@"9F9F9F"];
        [self addSubview:lineView2];
        
        [_subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(22);
        }];
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(22);
        }];
        [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_subBtn.mas_right);
            make.top.mas_equalTo(self).offset(5);
            make.bottom.mas_equalTo(self).offset(-5);
            make.width.mas_equalTo(0.5f);
        }];
        [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_addBtn.mas_left);
            make.top.bottom.mas_equalTo(lineView1);
            make.width.mas_equalTo(0.5f);
        }];
        [_numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(lineView1.mas_right);
            make.right.mas_equalTo(lineView2.mas_left);
            make.top.bottom.mas_equalTo(self);
        }];
        
    }
    return self;
}

- (void)numberAction:(UIButton *)sender
{
    
}

- (void)subAction:(UIButton *)sender
{
//    self.proNumber = self.proNumber - 1;
    if (self.proNumber-1 <= 1) {
        [_subBtn setEnabled:NO];
    }
    if ([delegate respondsToSelector:@selector(changeCartNumber:)]) {
        [delegate changeCartNumber:self.proNumber-1];
    }
}

- (void)addAction:(UIButton *)sender
{
//    self.proNumber = self.proNumber + 1;
    if (!_subBtn.isEnabled && self.proNumber+1 > 1) {
        [_subBtn setEnabled:YES];
    }
    if ([delegate respondsToSelector:@selector(changeCartNumber:)]) {
        [delegate changeCartNumber:self.proNumber+1];
    }
}

#pragma mark - setter
- (void)setProNumber:(int)proNumber
{
    _proNumber = proNumber;
    if (self.proNumber <= 1) {
        [_subBtn setEnabled:NO];
    }
    _numberTextField.text = [NSString stringWithFormat:@"%d",proNumber];
}
@end
