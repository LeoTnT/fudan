//
//  CartTitleView.h
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartTitleViewDelegate <NSObject>

- (void)cartTitleClick:(NSString *)supNo;
- (void)cartCheckboxClick:(BOOL)select;

@end

@interface CartTitleView : UITableViewHeaderFooterView
@property (nonatomic, weak) id<CartTitleViewDelegate>delegate;
// 商家名称
@property (nonatomic, strong) NSString *supName;

// 商家编号
@property (nonatomic, strong) NSString *supNo;

// 是否选择
@property (nonatomic) BOOL cbSelected;

@property (nonatomic ,copy)void (^ TapAction)(NSString *supNo);
@property (nonatomic ,copy)void (^ checkBoxAction)(BOOL select);
@end
