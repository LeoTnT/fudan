//
//  CartTotalView.h
//  App3.0
//
//  Created by mac on 17/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartTotalViewDelegate <NSObject>

- (void)selectAllClick:(BOOL)select;
- (void)settlementClick;
- (void)deleteCartClick;

@end
@interface CartTotalView : UIView
@property (nonatomic, weak) id<CartTotalViewDelegate>delegate;
@property (nonatomic) int totalNumber; // 总数量
@property (nonatomic) CGFloat totalPrice; //总金额
@property (strong, nonatomic) UIButton *checkbox;
- (void)changeCartStatus:(BOOL)edit;
@end
