//
//  CartContentView.m
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartContentView.h"
#import "ChangeCartNumberView.h"
#import "CartInstance.h"
#import "UIImage+XSWebImage.h"

@interface CartContentView() <ChangeCartNumberViewDelegate>
{
    UIButton *_checkbox;
    UIImageView *_productPic;
    UILabel *_productTitle;
    UILabel *_productDesc;
    UILabel *_price;
    UIButton *_subBtn;
    UIButton *_addBtn;
    UIButton *_editSpecBtn;
    ChangeCartNumberView *_changeView;
    CartProductListParser *_proListModel;
}
@end
@implementation CartContentView
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        // 选中
        _checkbox = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [_checkbox addTarget:self action:@selector(checkboxAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_checkbox];
        
        // 商品图片
        _productPic = [[UIImageView alloc] init];
        [self addSubview:_productPic];
        
        // 商品名称
        _productTitle = [[UILabel alloc] init];
        _productTitle.font = [UIFont systemFontOfSize:13];
        _productTitle.numberOfLines = 2;
        [self addSubview:_productTitle];
        
        // 商品描述
        _productDesc = [[UILabel alloc] init];
        _productDesc.textColor = mainGrayColor;
        _productDesc.font = [UIFont systemFontOfSize:12];
        [self addSubview:_productDesc];
        
        // 编辑状态下商品规格
        _editSpecBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editSpecBtn setTitleColor:mainGrayColor forState:UIControlStateNormal];
        _editSpecBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _editSpecBtn.layer.masksToBounds = YES;
        _editSpecBtn.layer.borderColor = LINE_COLOR_NORMAL.CGColor;
        _editSpecBtn.layer.borderWidth = 0.5f;
        _editSpecBtn.layer.cornerRadius = 2;
        [_editSpecBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_editSpecBtn addTarget:self action:@selector(editContentAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_editSpecBtn];
        _editSpecBtn.hidden = YES;
        
        // 价格
        _price = [[UILabel alloc] init];
        _price.textColor = [UIColor redColor];
        _price.text = @"¥";
        _price.font = [UIFont systemFontOfSize:12];
        [self addSubview:_price];
        
        // 改变数量
        _changeView = [ChangeCartNumberView new];
        _changeView.delegate = self;
        [self addSubview:_changeView];
        
        [_checkbox mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.left.mas_equalTo(self).offset(12);
            make.height.mas_equalTo(80);
        }];
        [_productPic mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.left.mas_equalTo(_checkbox.mas_right).offset(9);
            make.size.mas_equalTo(CGSizeMake(85, 85));
        }];
        [_productTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_productPic);
            make.left.mas_equalTo(_productPic.mas_right).offset(12);
            make.right.mas_lessThanOrEqualTo(self).offset(-70);
        }];
        [_productDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_productTitle.mas_bottom).offset(4);
            make.left.mas_equalTo(_productTitle);
            make.right.mas_lessThanOrEqualTo(self).offset(-70);
        }];
        [_editSpecBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_productTitle.mas_bottom).offset(4);
            make.left.mas_equalTo(_productTitle);
            make.right.mas_equalTo(self).offset(-70);
        }];
        [_price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_productTitle);
            make.bottom.mas_equalTo(_productPic);
        }];
        [_changeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-12);
            make.bottom.mas_equalTo(_productPic);
            make.size.mas_equalTo(CGSizeMake(92, 22));
        }];
    }
    return self;
}

- (void)setCartContent:(CartProductListParser *)model
{
    _proListModel = model;
    [_productPic getImageWithUrlStr:model.image andDefaultImage:[UIImage imageNamed:@"no_pic"]];
    _productTitle.text = model.product_name;
    _productDesc.text = model.spec_name;
    [_editSpecBtn setTitle:model.spec_name forState:UIControlStateNormal];
    _price.text = [NSString stringWithFormat:@"¥%@",model.sell_price];
    _changeView.proNumber = [model.total_num intValue];
    self.cbSelected = model.selected;
}

- (void)setBuyNumber:(NSString *)buyNumber {
    _buyNumber = buyNumber;
    _changeView.proNumber = [buyNumber intValue];
}

- (void)checkboxAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    _proListModel.selected = sender.selected;
    if (sender.selected) {
        // 加入已选择列表
        ProductBuyParser *parser = [[ProductBuyParser alloc] init];
        parser.product_ext_id = _proListModel.product_ext_id;
        parser.sellPrice = _proListModel.sell_price;
        parser.buyNumber = _proListModel.total_num;
        parser.product_id = _proListModel.product_id;
        [[CartInstance ShardInstnce] addOrUpdateProductModel:parser];
        [[CartInstance ShardInstnce] refreshCartTotalView];
    } else {
        // 从已选择列表中删除
        [[CartInstance ShardInstnce] deleteProductModelWithProductExtId:_proListModel.product_ext_id];
        [[CartInstance ShardInstnce] refreshCartTotalView];
    }
    
    if (self.checkBoxAction) {
        self.checkBoxAction(sender.selected);
    }
}

// 编辑状态规格选择
- (void)editContentAction:(XSCustomButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EDIT_SPEC object:@[_proListModel, self]];
}

#pragma mark - ChangeCartNumberViewDelegate
- (void)changeCartNumber:(int)numebr
{
    if (!self.cbSelected) {
        self.cbSelected = YES;
        _proListModel.selected = YES;
    }
    
    [XSTool showProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
    [HTTPManager editCartWithPeid:_proListModel.product_ext_id productNumber:numebr newPeid:@"" success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
        if (state.status) {
            _changeView.proNumber = numebr;
            
            ProductBuyParser *parser = [[ProductBuyParser alloc] init];
            parser.product_ext_id = _proListModel.product_ext_id;
            parser.sellPrice = _proListModel.sell_price;
            parser.buyNumber = [NSNumber numberWithInt:numebr];
            parser.product_id = _proListModel.product_id;
            [[CartInstance ShardInstnce] addOrUpdateProductModel:parser];
            [[CartInstance ShardInstnce] refreshCartTotalView];
        } else {
            _changeView.proNumber = _changeView.proNumber;  // 复原数量
            [XSTool showToastWithView:[UIApplication sharedApplication].keyWindow Text:state.info];
        }
    } fail:^(NSError *error) {
        _changeView.proNumber = _changeView.proNumber;  // 复原数量
        [XSTool hideProgressHUDWithView:[UIApplication sharedApplication].keyWindow];
        [XSTool showToastWithView:[UIApplication sharedApplication].keyWindow Text:TOAST_REQUEST_FAIL];
    }];

}

#pragma mark - setter
- (void)setCbSelected:(BOOL)cbSelected
{
    _checkbox.selected = cbSelected;
    
    if (cbSelected) {
        // 加入已选择列表
        ProductBuyParser *parser = [[ProductBuyParser alloc] init];
        parser.product_ext_id = _proListModel.product_ext_id;
        parser.sellPrice = _proListModel.sell_price;
        parser.buyNumber = _proListModel.total_num;
        [[CartInstance ShardInstnce] addOrUpdateProductModel:parser];
        [[CartInstance ShardInstnce] refreshCartTotalView];
    } else {
        // 从已选择列表中删除
        [[CartInstance ShardInstnce] deleteProductModelWithProductExtId:_proListModel.product_ext_id];
        [[CartInstance ShardInstnce] refreshCartTotalView];
    }
}

- (BOOL)cbSelected
{
    return _checkbox.selected;
}

- (void)setEdit:(BOOL)edit
{
    _edit = edit;
    if (edit) {
        _productDesc.hidden = YES;
        if (![_proListModel.spec_name isEqualToString:@""]) {
            _editSpecBtn.hidden = NO;
        }
        
    } else {
        _productDesc.hidden = NO;
        _editSpecBtn.hidden = YES;
    }
}
@end
