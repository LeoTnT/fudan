//
//  CartTotalView.m
//  App3.0
//
//  Created by mac on 17/4/1.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartTotalView.h"
#import "CartInstance.h"

@interface CartTotalView()
{
    UIButton *_settlementBtn;
    UILabel *_totalPriceLabel;
    UILabel *_freightLabel;
    UIButton *_deleteBtn;
}
@end
@implementation CartTotalView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        // 全选
        _checkbox = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [_checkbox addTarget:self action:@selector(checkboxAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_checkbox];
        _checkbox.selected = YES;
        
        UILabel *checkText = [UILabel new];
        checkText.text = @"全选";
        checkText.font = [UIFont systemFontOfSize:14];
        [self addSubview:checkText];
        
        _settlementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settlementBtn setBackgroundColor:mainColor];
        [_settlementBtn setTitle:@"结算" forState:UIControlStateNormal];
        [_settlementBtn.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_settlementBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_settlementBtn addTarget:self action:@selector(settlementAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_settlementBtn];
        
        // 总金额
        _totalPriceLabel = [UILabel new];
        _totalPriceLabel.font = [UIFont systemFontOfSize:12];
        _totalPriceLabel.textAlignment = NSTextAlignmentRight;
        // 添加富文本
        NSString *priceStr = @"总计：¥0";
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attStr addAttribute:NSForegroundColorAttributeName
                        value:[UIColor redColor]
                        range:NSMakeRange(3, priceStr.length-3)];
        _totalPriceLabel.attributedText = attStr;
        [self addSubview:_totalPriceLabel];
        
        // 运费
        _freightLabel = [UILabel new];
        _freightLabel.text = @"不含运费";
        _freightLabel.font = [UIFont systemFontOfSize:12];
        _freightLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_freightLabel];
        
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:mainColor forState:UIControlStateNormal];
        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        _deleteBtn.layer.masksToBounds = YES;
        _deleteBtn.layer.borderWidth = 0.5f;
        _deleteBtn.layer.borderColor = mainColor.CGColor;
        _deleteBtn.layer.cornerRadius = 3;
        [_deleteBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteBtn];
        _deleteBtn.hidden = YES;
        
        [_checkbox mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(12);
            make.centerY.mas_equalTo(self);
            make.height.mas_equalTo(self);
        }];
        [checkText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_checkbox.mas_right).offset(12);
            make.centerY.mas_equalTo(self);
        }];
        [_settlementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(120);
        }];
        [_freightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_settlementBtn.mas_left).offset(-12);
            make.bottom.mas_equalTo(self).offset(-8);
        }];
        [_totalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_settlementBtn.mas_left).offset(-12);
            make.top.mas_equalTo(self).offset(8);
        }];
        [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-12);
            make.centerY.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(60, 40));
        }];
    }
    return self;
}

- (void)checkboxAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(selectAllClick:)]) {
        [self.delegate selectAllClick:sender.selected];
    }
}

// 结算
- (void)settlementAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(settlementClick)]) {
        [self.delegate settlementClick];
    }
}

// 删除
- (void)deleteAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(deleteCartClick)]) {
        [self.delegate deleteCartClick];
    }
}

- (void)changeCartStatus:(BOOL)edit
{
    if (!edit) {
        _settlementBtn.hidden = NO;
        _totalPriceLabel.hidden = NO;
        _freightLabel.hidden = NO;
        _deleteBtn.hidden = YES;
    } else {
        _settlementBtn.hidden = YES;
        _totalPriceLabel.hidden = YES;
        _freightLabel.hidden = YES;
        _deleteBtn.hidden = NO;
    }
}

#pragma mark - setter
- (void)setTotalPrice:(CGFloat)totalPrice
{
    _totalPrice = totalPrice;
    NSString *priceStr = [NSString stringWithFormat:@"总计：¥%.2f",totalPrice];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attStr addAttribute:NSForegroundColorAttributeName
                   value:[UIColor redColor]
                   range:NSMakeRange(3, priceStr.length-3)];
    _totalPriceLabel.attributedText = attStr;
}

- (void)setTotalNumber:(int)totalNumber
{
    _totalNumber = totalNumber;
    [_settlementBtn setTitle:[NSString stringWithFormat:@"结算(%d)",totalNumber] forState:UIControlStateNormal];
}
@end
