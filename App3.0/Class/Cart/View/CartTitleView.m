//
//  CartTitleView.m
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartTitleView.h"

@interface CartTitleView()
@property (strong, nonatomic) UIButton *checkbox;
@property (strong, nonatomic) UILabel *supNameLabel;
@property (strong, nonatomic) UIButton *contactSup;
@end

@implementation CartTitleView
@synthesize delegate;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIView *bgView = [UIView new];
        bgView.backgroundColor = [UIColor hexFloatColor:@"FAFAFA"];
        [self addSubview:bgView];
        
        // 选中
        _checkbox = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_no"] forState:UIControlStateNormal];
        [_checkbox setImage:[UIImage imageNamed:@"user_site_check_yes"] forState:UIControlStateSelected];
        [_checkbox addTarget:self action:@selector(checkboxAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:_checkbox];
        
        UIImageView *storeImg = [[UIImageView alloc] init];
        [storeImg setImage:[UIImage imageNamed:@"user_cart_store"]];
        [bgView addSubview:storeImg];
        
        UIImageView *arrow = [[UIImageView alloc] init];
        [arrow setImage:[UIImage imageNamed:@"cart_gostore_arrow"]];
        [bgView addSubview:arrow];
        
        // 商家名称
        _supNameLabel = [[UILabel alloc] init];
        _supNameLabel.font = [UIFont systemFontOfSize:14];
        [bgView addSubview:_supNameLabel];
        
        // 联系卖家
        _contactSup = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contactSup setTitle:@"联系卖家" forState:UIControlStateNormal];
        [_contactSup setTitleColor:mainColor forState:UIControlStateNormal];
        _contactSup.titleLabel.font = [UIFont systemFontOfSize:14];
        [_contactSup addTarget:self action:@selector(contactSupplyAction) forControlEvents:UIControlEventTouchUpInside];
//        [bgView addSubview:_contactSup];
        
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(self);
            make.top.mas_equalTo(self).offset(12);
        }];
        [_checkbox mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.left.mas_equalTo(bgView).offset(12);
            make.height.mas_equalTo(bgView);
        }];
        [storeImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.left.mas_equalTo(_checkbox.mas_right).offset(9);
        }];
        [_supNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.left.mas_equalTo(storeImg.mas_right).offset(8);
        }];
        [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.left.mas_equalTo(_supNameLabel.mas_right).offset(10);
        }];
//        [_contactSup mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.mas_equalTo(bgView);
//            make.right.mas_equalTo(bgView).offset(-12);
//        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)contactSupplyAction {
    
}

- (void)checkboxAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (self.checkbox) {
        self.checkBoxAction(sender.selected);
    }
}

- (void)tapAction:(UITapGestureRecognizer *)sender
{
//    if ([delegate respondsToSelector:@selector(cartTitleClick:)]) {
//        [delegate cartTitleClick:self.supNo];
//    }
    if (self.TapAction) {
        self.TapAction(self.supNo);
    }
}

- (void)setSupName:(NSString *)supName
{
    _supName = supName;
    _supNameLabel.text = supName;
}

#pragma mark - setter
- (void)setCbSelected:(BOOL)cbSelected
{
    _checkbox.selected = cbSelected;
}

- (BOOL)cbSelected
{
    return _checkbox.selected;
}
@end
