//
//  CartContentView.h
//  App3.0
//
//  Created by mac on 17/3/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartModel.h"

@interface CartContentView : UITableViewCell
- (void)setCartContent:(CartProductListParser *)model;

// 选中状态
@property (nonatomic) BOOL cbSelected;

// 编辑状态
@property (nonatomic) BOOL edit;

@property (copy, nonatomic) NSString *buyNumber;

@property (nonatomic ,copy)void (^ checkBoxAction)(BOOL select);
@end
