//
//  CartVC.m
//  App3.0
//
//  Created by mac on 17/3/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "CartVC.h"
#import "CartEmptyView.h"
#import "CartModel.h"
#import "CartTotalView.h"
#import "CartInstance.h"
#import "GoodsDetailSpecView.h"
#import "GoodsTypeViewController.h"
#import "TabMallVC.h"
#import "VerifyOrderViewController.h"
#import "GoodsDetailViewController.h"
#import "CollectionViewController.h"
#import "CartContentView.h"
#import "CartTitleView.h"
#import "S_StoreInformation.h"

@interface CartVC () <CartEmptyViewDelegate, CartTotalViewDelegate, GoodsDetailSpecViewDelegate>
{
    CartDataParser *_cartData;
    NSMutableArray *_supProArr;
    CartTotalView *_cartTotalView;
    CartEmptyView *_emptyView;
}
@property (nonatomic, strong) VerifyOrderViewController *vertifyFormController;
@property (nonatomic, strong) NSMutableArray *selectedProductsArray;
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, strong) GoodsDetailSpecView *specView;
@property (nonatomic, strong) UIView *grayView;
@property (strong, nonatomic) CartContentView *cartContentView;
@property (strong, nonatomic) CartProductListParser *curParser;
@end

@implementation CartVC
#pragma mark-Lazy loading
-(UIView *)grayView{
    if (!_grayView) {
        _grayView=[[UIView alloc] initWithFrame:self.view.bounds];
        UIColor *bColor = [UIColor blackColor];
        _grayView.backgroundColor = [bColor colorWithAlphaComponent:0.5];
        [self.view addSubview:_grayView];
        _grayView.hidden=YES;
    }
    return _grayView;
}

-(NSMutableArray *)selectedProductsArray{
    if (_selectedProductsArray==nil) {
        _selectedProductsArray = [NSMutableArray array];
    }
    return _selectedProductsArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[CartInstance ShardInstnce] removeAllProducts];
    // Do any additional setup after loading the view.

    self.navigationItem.title = @"购物车";
    self.view.backgroundColor = BG_COLOR;
    self.autoHideKeyboard = YES;
    // 显示nav bar
    
    _cartData = [[CartDataParser alloc] init];
    
    self.tableViewStyle = UITableViewStyleGrouped;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50+kTabbarSafeBottomMargin, 0));
    }];
    
    _emptyView = [[CartEmptyView alloc] initWithFrame:self.view.bounds];
    _emptyView.delegate = self;
    [self.view addSubview:_emptyView];
    _emptyView.hidden = YES;
    
    _cartTotalView = [[CartTotalView alloc] initWithFrame:CGRectZero];
    _cartTotalView.delegate = self;
    [self.view addSubview:_cartTotalView];
    [_cartTotalView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).offset(kTabbarSafeBottomMargin);
    }];
    _cartTotalView.hidden = YES;
    
    __weak typeof(self) wSelf = self;
    [self actionCustomRightBtnWithNrlImage:nil htlImage:nil title:Localized(@"edit") action:^{
        wSelf.navRightBtn.selected = !wSelf.navRightBtn.selected;
        if (wSelf.navRightBtn.selected) {
            [[CartInstance ShardInstnce] setCartStatus:CartStatusEdit];
            self.isEditing = YES;
        } else {
            self.isEditing = NO;
            [[CartInstance ShardInstnce] setCartStatus:CartStatusNormal];
        }
        [_cartTotalView changeCartStatus:wSelf.navRightBtn.selected];
        //        NSArray *productArray = _cartData.products;
        for (int i = 0; i < _cartData.products.count; i++) {
            CartProductParser *data = _cartData.products[i];
            CartDeliveryListParser *delivery = [data.delivery_list firstObject];
            for (int j = 0; j < delivery.product_list.count; j++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                CartContentView *cell = (CartContentView *)[self.tableView cellForRowAtIndexPath:indexPath];
                cell.edit = wSelf.navRightBtn.selected;
            }
        }
    }];
    [self.navRightBtn setTitle:Localized(@"完成") forState:UIControlStateSelected];
    [self.navRightBtn setTitleColor:mainColor forState:UIControlStateSelected];
    self.navRightBtn.hidden = YES;
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back_h" title:nil action:^{
        for (UIViewController *controller in wSelf.navigationController.viewControllers) {
            if ([controller isKindOfClass:[GoodsTypeViewController class]]) {
                [wSelf.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self getCartData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCartData) name:NOTIFICATION_REFRESH_CART object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCartTotal) name:NOTIFICATION_REFRESHLIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editProductSpec:) name:NOTIFICATION_EDIT_SPEC object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lookGoodsDetail:) name:NOTIFICATION_LOOK_DETAIL object:nil];
    
    self.vertifyFormController = [[VerifyOrderViewController alloc] init];
    self.isEditing = NO;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBarHidden = NO;
//}

- (void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 弹出商品规格
- (void)editProductSpec:(NSNotification *)noti
{
    self.cartContentView = noti.object[1];
    self.curParser = (CartProductListParser *)noti.object[0];
    self.grayView.hidden=NO;
    self.specView=[[GoodsDetailSpecView alloc] initWithProductParser:self.curParser];
    self.specView.frame=CGRectMake(0, mainHeight-self.specView.height, mainWidth, self.specView.height);
    self.specView.delegate=self;
    [self.grayView addSubview:self.specView];
}
#pragma mark-GoodsDetailSpecView Delegate
-(void)clickCloseBtnWithSpecDic:(NSMutableDictionary *)specDic goodsNum:(NSUInteger)goodsNum stockNum:(NSUInteger)stockNum{
    self.grayView.hidden=YES;
    [self.specView removeFromSuperview];
    self.specView=nil;
}

-(void)clickConfirmBtnWithNewPeid:(NSString *)peid goodsPrice:(CGFloat)goodsPrice stockNum:(NSUInteger)stockNum specStr:(NSString *)specStr specShowStr:(NSString *)showStr{
    self.grayView.hidden=YES;
    [self.specView removeFromSuperview];
    self.specView=nil;//勿删
    
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager editCartWithPeid:self.curParser.product_ext_id productNumber:[self.curParser.total_num integerValue] newPeid:peid?peid:@"" success:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            if (self.cartContentView) {
                [[CartInstance ShardInstnce] deleteProductModelWithProductExtId:self.curParser.product_ext_id];
                
                CartProductListParser *cpl = self.curParser;
                NSDictionary *dat = dic[@"data"][0];
                cpl.product_ext_id = dat[@"product_ext_id"];
                cpl.spec_name = dat[@"spec_name"];
                cpl.sell_price = dat[@"sell_price"];
                [self.cartContentView setCartContent:cpl];
                self.cartContentView.buyNumber = @"1";
                
                // 刷新本地数据
                ProductBuyParser *parser = [[ProductBuyParser alloc] init];
                parser.product_ext_id = cpl.product_ext_id;
                parser.sellPrice = cpl.sell_price;
                parser.buyNumber = @(1);
                parser.product_id = cpl.product_id;
                [[CartInstance ShardInstnce] addOrUpdateProductModel:parser];
                [[CartInstance ShardInstnce] refreshCartTotalView];
            }
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
    }];
}


-(void)lookGoodsDetail:(NSNotification *) notification{
    if (!self.isEditing) {
        GoodsDetailViewController *goodsDetailController = [[GoodsDetailViewController alloc] init];
        NSString *goodsId = notification.userInfo[@"productId"] ;
        goodsDetailController.goodsID = goodsId;
        [self.navigationController pushViewController:goodsDetailController animated:YES];
    }
    
}
// 刷新购物车total数据
- (void)refreshCartTotal
{
    NSArray *array = [[CartInstance ShardInstnce] getBuyProducts];
    CGFloat totalPrice = 0;
    for (ProductBuyParser *parser in array) {
        totalPrice += [parser.buyNumber integerValue] * [parser.sellPrice floatValue];
        
        for (int i = 0; i < _cartData.products.count; i++) {
            
            CartProductParser *data = _cartData.products[i];
            CartDeliveryListParser *delivery = [data.delivery_list firstObject];
            for (int j = 0; j < delivery.product_list.count; j++) {
                CartProductListParser *plParser = delivery.product_list[j];
                if ([parser.product_id isEqualToString:plParser.product_id]) {
                    plParser.product_ext_id = parser.product_ext_id;
                    plParser.total_num = parser.buyNumber;
                    break;
                }
            }
        }
    }
    _cartTotalView.totalPrice = totalPrice;
}

// 获取和刷新数据
- (void)getCartData
{
    __weak typeof(self) wSelf = self;
    [XSTool showProgressHUDWithView:self.view];
    [HTTPManager getCartListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        CartParser *parser = [CartParser mj_objectWithKeyValues:dic];
        if (state.status) {
            //拿到数据
            if (parser.data.products.count > 0) {
                _emptyView.hidden = YES;
                _cartTotalView.hidden = NO;
                wSelf.navRightBtn.hidden = NO;
                _cartData = parser.data;
                [wSelf.tableView reloadData];
                [wSelf selectAllClick:YES];
            } else {
                _emptyView.hidden = NO;
                _cartTotalView.hidden = YES;
                wSelf.navRightBtn.hidden = YES;
            }
            
            [XSTool hideProgressHUDWithView:self.view];
        }
    } fail:^(NSError * _Nonnull error) {
        [XSTool hideProgressHUDWithView:self.view];
    }];
}

#pragma mark - CartTotalViewDelegate
- (void)selectAllClick:(BOOL)select
{
    for (int i = 0; i < _cartData.products.count; i++) {
        CartTitleView *view = (CartTitleView *)[self.tableView headerViewForSection:i];
        view.cbSelected = select;
        //全选的商品
        [self.selectedProductsArray addObjectsFromArray:_cartData.products];
        
        CartProductParser *data = _cartData.products[i];
        data.selected = select;
        CartDeliveryListParser *delivery = [data.delivery_list firstObject];
        for (int j = 0; j < delivery.product_list.count; j++) {
            CartProductListParser *plParser = delivery.product_list[j];
            plParser.selected = select;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
            CartContentView *cell = (CartContentView *)[self.tableView cellForRowAtIndexPath:indexPath];
            cell.cbSelected = select;
        }
    }
    
    if (select) {
        for (int i = 0; i < _cartData.products.count; i++) {
            
            CartProductParser *data = _cartData.products[i];
            
            CartDeliveryListParser *delivery = [data.delivery_list firstObject];
            for (int j = 0; j < delivery.product_list.count; j++) {
                CartProductListParser *plParser = delivery.product_list[j];
                // 加入已选择列表
                ProductBuyParser *parser = [[ProductBuyParser alloc] init];
                parser.product_ext_id = plParser.product_ext_id;
                parser.sellPrice = plParser.sell_price;
                parser.buyNumber = plParser.total_num;
                [[CartInstance ShardInstnce] addOrUpdateProductModel:parser];
            }
        }
        [self refreshCartTotal];
    }
    
}

// 结算
- (void)settlementClick
{
    NSArray *buyerArray = [[CartInstance ShardInstnce] getBuyProducts];
    if (buyerArray.count>0) {
        VerifyOrderViewController *verifyOrderController = [[VerifyOrderViewController alloc] init];
        NSMutableString *tempGoodsNum = [NSMutableString string];
        NSMutableString *tempExtIds =  [NSMutableString string];
        for (ProductBuyParser *buyParser in buyerArray) {
            [tempGoodsNum appendString:[NSString stringWithFormat:@"%@,",buyParser.buyNumber]];
            [tempExtIds appendString:[NSString stringWithFormat:@"%@,",buyParser.product_ext_id]];
        }
        NSString *goodsNum = [tempGoodsNum substringToIndex:[tempGoodsNum length]-1];
        NSString *extIds = [tempExtIds substringToIndex:[tempExtIds length]-1];
        verifyOrderController.goodsNum = goodsNum;
        verifyOrderController.extendId = extIds;
        verifyOrderController.addressId = @"";
        [self.navigationController pushViewController:verifyOrderController animated:YES];
    } else {
        [XSTool showToastWithView:self.view Text:@"请选择商品！"];
    }
    
}
//删除
- (void)deleteCartClick
{
    NSArray *array = [[CartInstance ShardInstnce] getBuyProducts];
    if (array.count == 0) {
        [XSTool showToastWithView:self.view Text:@"还没有选中商品"];
        return;
    }
    [XSTool showProgressHUDWithView:self.view];
    __weak typeof(self) wSelf = self;
    for (ProductBuyParser *parser in array) {
        [HTTPManager deleteCartWithProductId:parser.product_ext_id success:^(NSDictionary *dic, resultObject *state) {
            [[CartInstance ShardInstnce] deleteProductModelWithProductExtId:parser.product_ext_id];
            if ([[CartInstance ShardInstnce] getBuyProducts].count == 0) {
                [self.selectedProductsArray removeAllObjects];
                [XSTool hideProgressHUDWithView:self.view];
                [wSelf getCartData];
                
            }
        } fail:^(NSError * _Nonnull error) {
            [XSTool hideProgressHUDWithView:self.view];
        }];
    }
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _cartData.products.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_cartData.products) {
        CartProductParser *parser = _cartData.products[section];
        CartDeliveryListParser *delivery = [parser.delivery_list firstObject];
        return delivery.product_list.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 56;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartProductParser *data = _cartData.products[indexPath.section];
    CartDeliveryListParser *delivery = [data.delivery_list firstObject];
    CartProductListParser *product = delivery.product_list[indexPath.row];
    
    GoodsDetailViewController *goodsDetailController = [[GoodsDetailViewController alloc] init];
    goodsDetailController.goodsID = product.product_id;
    [self.navigationController pushViewController:goodsDetailController animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *identifier = [NSString stringWithFormat:@"cartHeaderView_%ld",section];
    CartTitleView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (view == nil) {
        view = [[CartTitleView alloc] initWithReuseIdentifier:identifier];
        
    }
    CartProductParser *data = _cartData.products[section];
    view.supNo = data.supply_id;
    view.supName = data.sup_name;
    view.cbSelected = data.selected;
    
    [view setTapAction:^(NSString *supNo){
        // goto商家
        S_StoreInformation *infor = [S_StoreInformation new];
        infor.storeInfor = supNo;
        [self.navigationController pushViewController:infor animated:YES];
    }];
    
    [view setCheckBoxAction:^(BOOL select){
        CartProductParser *data = _cartData.products[section];
        data.selected = select;
        CartDeliveryListParser *delivery = [data.delivery_list firstObject];
        for (int i = 0; i < delivery.product_list.count; i++) {
            CartProductListParser *plParser = delivery.product_list[i];
            plParser.selected = select;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
            CartContentView *cell = (CartContentView *)[tableView cellForRowAtIndexPath:indexPath];
            cell.cbSelected = select;
        }
    }];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"cart_Cell_%ld",indexPath.section*1000+indexPath.row];
    CartContentView *cell = (CartContentView*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[CartContentView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (_cartData) {
        CartProductParser *data = _cartData.products[indexPath.section];
        CartDeliveryListParser *delivery = [data.delivery_list firstObject];
        [cell setCartContent:delivery.product_list[indexPath.row]];
    }
    
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    //添加一个删除按钮
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        NSLog(@"点击了删除");
        CartProductParser *data = _cartData.products[indexPath.section];
        CartDeliveryListParser *delivery = [data.delivery_list firstObject];
        CartProductListParser *product = delivery.product_list[indexPath.row];
        [XSTool showProgressHUDWithView:self.view];
        [HTTPManager deleteCartWithProductId:product.product_ext_id success:^(NSDictionary *dic, resultObject *state) {
            [XSTool hideProgressHUDWithView:self.view];
            if (state.status) {
                [self getCartData];
            } else {
                [XSTool showToastWithView:self.view Text:state.info];
            }
        } fail:^(NSError *error) {
            [XSTool hideProgressHUDWithView:self.view];
            [XSTool showToastWithView:self.view Text:TOAST_REQUEST_FAIL];
        }];
    }];
    //删除按钮颜色
    deleteAction.backgroundColor = [UIColor hexFloatColor:@"f43531"];
    
    //将设置好的按钮方到数组中返回
    return @[deleteAction];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - CartEmptyViewDelegate
- (void)goBuyClick
{
    
    BOOL returnExistPage = NO;
    UIViewController *tempController;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[TabMallVC class]]) {
            returnExistPage = YES;
            tempController = (TabMallVC *) controller;
        }
        
    }
    if (returnExistPage) {
        [self.navigationController popToViewController:tempController animated:YES];
    } else {
        [self.tabBarController setSelectedIndex:2];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
}

- (void)goCollectClick
{
    CollectionViewController *collectController = [[CollectionViewController alloc] init];
    [self.navigationController pushViewController:collectController animated:YES];
}

@end
