//
//  XSAlertView.m
//  App3.0
//
//  Created by mac on 17/5/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSAlertView.h"

@interface XSAlertView ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, strong) UIButton *cancelButton;
@end

@implementation XSAlertView
- (instancetype)initWithTitle:(NSString *)title detail:(NSString *)detail
{
    self = [super initWithFrame:CGRectMake(mainWidth*0.1, mainHeight/2-50, mainWidth*0.8, 100)];
    if (self) {
        CGFloat space = 10.0f;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = mainGrayColor;
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_titleLabel];
        
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.numberOfLines = 0;
        [self addSubview:_detailLabel];
        
        _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setTitle:Localized(@"material_dialog_positive_text") forState:UIControlStateNormal];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(space);
            make.top.mas_equalTo(self.mas_top).offset(space);
            make.right.mas_equalTo(self.mas_right).offset(-space);
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(space);
            make.top.mas_equalTo(self.mas_top).offset(space);
            make.right.mas_equalTo(self.mas_right).offset(-space);
        }];
        
    }
    return self;
}
@end
