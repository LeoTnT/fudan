//
//  YKMutableLevelTableView.m
//  MutableLevelTableView
//
//  Created by 杨卡 on 16/9/8.
//  Copyright © 2016年 杨卡. All rights reserved.
//

#import "YKMultiLevelTableView.h"
#import "YKNodeModel.h"


@interface YKNodeCell : UITableViewCell


@property (nonatomic, strong) YKNodeModel *model;


@property (nonatomic, strong) UILabel *nodeLabel;

@property (nonatomic, strong) UIView *line;

@property (nonatomic, assign) CGRect rect;

@end




static CGFloat const leftMargin = 15; //left indentation
@implementation YKNodeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withLeft:(CGFloat)leaf{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        
        _nodeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _nodeLabel.font  =[UIFont systemFontOfSize:15];
        _nodeLabel.textColor = [UIColor blackColor];
        [self addSubview:_nodeLabel];
        
        _line = [[UIView alloc] initWithFrame:CGRectZero];
        _line.backgroundColor = LINE_COLOR;
        [self addSubview:_line];
        [self setFrame];
    }
    return self;
}


- (void)setModel:(YKNodeModel *)model {
    _model = model;

    _nodeLabel.text = model.category_supply_name;
 
    [self setFrame];
}

- (void) setFrame {
    
    CGFloat cellHeight = self.mj_h;
    CGFloat cellWidth  = self.mj_w;
    CGRect frame = CGRectMake(leftMargin, 0, cellWidth - leftMargin, cellHeight);
    frame.origin.x = leftMargin * (2 * self.model.leaf ) +15 ;
    _nodeLabel.frame = frame;
    
    CGRect frame2 = CGRectMake(0, 0, mainWidth, .5);
    
    _line.frame = frame2;
}


@end


//_______________________________________________________________________________________________________________
#pragma mark 
#pragma mark YKMultiLevelTableView
@interface YKMultiLevelTableView ()<UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, strong) NSMutableArray *reloadArray;

@property (nonatomic, copy) YKSelectBlock block;

@end

static CGFloat const cellHeight = 45.0;
@implementation YKMultiLevelTableView

#pragma mark
#pragma mark life cycle
- (instancetype)initWithFrame:(CGRect)frame needPreservation:(BOOL)need selectBlock:(YKSelectBlock)block{
    self = [self initWithFrame:frame];
    if (self) {
        
        
        self.block = [block copy];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
    
        _reloadArray = [NSMutableArray array];
        self.delegate = self;
        self.dataSource = self;
        self.separatorStyle =UITableViewCellSeparatorStyleNone;
    }
    return self;
}

#pragma mark
#pragma mark set node's leaf and root propertys ,and level
- (void)setNodes:(NSMutableArray *)nodes{
    _nodes = nodes;

    [self reloadData];
}



#pragma mark
#pragma mark UITableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.nodes.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    YKNodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    YKNodeModel *model = [self.nodes objectAtIndex:indexPath.row];
    if (!cell) {
        cell = [[YKNodeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier withLeft:model.leaf];
    }
    cell.model = model;
    cell.accessoryType = (model._child || model.root)? UITableViewCellAccessoryDisclosureIndicator :UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YKNodeModel *currentNode = [self.nodes objectAtIndex:indexPath.row];
    if (!currentNode._child.count) {
        self.block(currentNode);
        return;
    }else{
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [_reloadArray removeAllObjects];
        if (currentNode.isOpen) {
            currentNode.isOpen = NO;
            [self.nodes replaceObjectAtIndex:indexPath.row withObject:currentNode];
            [self foldNodesForLevel:currentNode currentIndex:indexPath.row];
            [self deleteRowsAtIndexPaths:_reloadArray withRowAnimation:UITableViewRowAnimationNone];
        }else{
            [self expandNodesForParent:currentNode  currentIndex:indexPath.row];
            [tableView insertRowsAtIndexPaths:_reloadArray withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
}



/**
 关闭
 */
- (void)foldNodesForLevel:(YKNodeModel *)currentNode currentIndex:(NSUInteger)currentIndex{

        for (int x = 0 ; x < currentNode._child.count; x ++) {
            YKNodeModel * childModel = currentNode._child[x];
            if (childModel.leaf > currentNode.leaf) {
                [self.nodes removeObject:childModel];
                currentIndex ++ ;
                childModel.leaf = 0;
                [_reloadArray addObject:[NSIndexPath indexPathForRow:currentIndex inSection:0]];
                [self foldNodesForLevel:childModel currentIndex:currentIndex+1];
            }

        }
  
}


/**
 展开
 */
- (NSUInteger)expandNodesForParent:(YKNodeModel *)currentNode currentIndex:(NSUInteger)currentIndex{
    if (!currentNode.isOpen || currentNode.root) {
        currentNode.isOpen = YES;
        [self.nodes replaceObjectAtIndex:currentIndex withObject:currentNode];
        for (NSInteger x = 0 ; x < currentNode._child.count; x ++) {
            YKNodeModel * childModel = currentNode._child[x];
            childModel.leaf = currentNode.leaf + 1;
            currentIndex ++;
            childModel.isOpen = NO;
            [self.nodes insertObject:childModel atIndex:currentIndex];
            [_reloadArray addObject:[NSIndexPath indexPathForRow:currentIndex inSection:0]];//need reload nodes
        }

    }

    return currentIndex;

}



@end
