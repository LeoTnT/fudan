//
//  XSBaseWKWebViewController.h
//  App3.0
//
//  Created by nilin on 2017/12/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import <WebKit/WebKit.h>

typedef NS_ENUM(NSInteger,WKWebViewURLType) {
    WKWebViewURLNormal = 0,
    WKWebViewURLRefundDetail, // 退款退货
    WKWebViewURLRefundList, // 退款退货
    WKWebViewURLOffLine, // 线下商家
    WKWebViewURLPresell, // 预售
    WKWebViewURLShare, // 推广
    WKWebViewURLOfflineGoodsDetail, // 线下商品详情
    WKWebViewURLGroupBuy,//团购
    WKWebViewURLPayLink,//网页支付
};

@interface XSBaseWKWebViewController : XSBaseViewController
@property (nonatomic ,strong)UIProgressView *progressView;

@property (nonatomic ,strong) WKWebView *webView;

@property (nonatomic, copy) NSString *urlStr;

@property (nonatomic ,assign,)WKWebViewURLType urlType;

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message;

- (void) setSubViews;

/**返回的控制器可能需要刷新界面*/
- (void)backClick;

@end


@interface WeakScriptMessageDelegate : NSObject<WKScriptMessageHandler>

@property (nonatomic, assign) id<WKScriptMessageHandler> scriptDelegate;

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;

@end
