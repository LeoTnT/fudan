//
//  XSBaseViewController.m
//  App3.0
//
//  Created by apple on 2017/6/19.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseNaviController.h"

@interface XSBaseNaviController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, getter=isPushing) BOOL pushing;

@end

@implementation XSBaseNaviController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    @weakify(self);
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        @strongify(self);
        self.interactivePopGestureRecognizer.delegate = self;
    }
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    // 设置目标控制器隐藏选项卡
    
    
    
    if (self.childViewControllers.count > 0) {
        // 不是栈底控制器, 也就是子控制器
        viewController.hidesBottomBarWhenPushed = YES;
        
    }
//    if (self.pushing == YES) {
//        
//        return;
//    } else {
//        
//        self.pushing = YES;
//    }
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.interactivePopGestureRecognizer.enabled = YES;
        
    }
    
    [super pushViewController:viewController animated:YES];
}


#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        navigationController.interactivePopGestureRecognizer.enabled = YES;
        
    }
    self.pushing = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}



#pragma mark - 判断页面数，页面数>1时执行手势
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (self.viewControllers.count > 1) {
        return YES;
    }
    else{
        return NO;
    }
}


@end
