//
//  XSChoseImagePicker.h
//  App3.0
//
//  Created by xinshang on 2017/7/11.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSChoseImagePicker : NSObject

@property (nonatomic ,copy)void (^ChoseImageData)(UIImage *);

+ (instancetype)creatXSChoseImage:(UIViewController *)viewController;

@end
