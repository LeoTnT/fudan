//
//  XSBaseViewController.h
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultView.h"

@interface XSBaseViewController : UIViewController

/* 当前页面是否隐藏tabbar */
@property (nonatomic, assign) BOOL hidesBottomBarWhenPushed;

#pragma mark UI related properties & functions
@property (nonatomic, assign) BOOL showBackBtn;
@property (nonatomic, assign) BOOL shareFriend;
@property (nonatomic,   copy) NSString *itemTitle;
@property (nonatomic,   copy) NSString *requestURL;
@property (nonatomic, strong) UIButton *navLeftBtn;
@property (nonatomic, strong) UIButton *navRightBtn;
//@property (nonatomic, strong) DefaultView *baseDefaultView;//默认界面

@property (nonatomic,assign,readonly) NSInteger navi_Height;
@property (nonatomic,assign,readonly) NSInteger tab_barHeight;
@property (nonatomic,assign,readonly) CGFloat iphonx_bottom;

@property (nonatomic ,copy)NSString *navi_title;

// 点击空白处隐藏键盘
@property (nonatomic, assign) BOOL autoHideKeyboard;


- (void)actionCustomLeftBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                  title:(NSString *)title
                                 action:(void(^)())btnClickBlock;
- (void)actionCustomRightBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                   title:(NSString *)title
                                  action:(void(^)())btnClickBlock;

/*
 此方法根据用户是否登录跳转界面
 */
- (void)xs_pushViewController:(UIViewController *)vc;


- (void)setWhiteLeftBackBtn;
- (void)setBlackLeftBackBtn;
- (void)setNavBackGroundColor:(UIColor *)color;
- (void)setNavTitle:(NSString *)title color:(UIColor *)color;
- (void)setNavTitle:(NSString *)title isShowBack:(BOOL)isShowBack;

- (void)setWhiteBackGroundWithTitle:(NSString *)title;

- (void)setBlackColorBackGroundWithTitle:(NSString *)title;

// 选择图片
- (void)choosePhotoes;

// 获取图片
- (void) getImageWithController:(UIImage *)image;

- (void) shareActiveAction:(UIImage *)image;


@end
