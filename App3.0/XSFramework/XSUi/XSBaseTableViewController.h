//
//  XSBaseTableViewController.h
//  App3.0
//
//  Created by mac on 2017/9/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

/** @brief tabeleView的cell高度 */
#define KCELLDEFAULTHEIGHT 50

@interface XSBaseTableViewController : XSBaseViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;

@property (assign, nonatomic) UITableViewStyle tableViewStyle;

/** @brief 是否启用下拉加载更多，默认为NO */
@property (nonatomic) BOOL showRefreshHeader;
/** @brief 是否启用上拉加载更多，默认为NO */
@property (nonatomic) BOOL showRefreshFooter;

@property (nonatomic, copy) void (^refreshHeader)();
@property (nonatomic, copy) void (^refreshFooter)();

@end
