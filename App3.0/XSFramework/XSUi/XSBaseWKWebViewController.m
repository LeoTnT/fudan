//
//  XSBaseWKWebViewController.m
//  App3.0
//
//  Created by nilin on 2017/12/21.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseWKWebViewController.h"
#import "OrderDetailViewController.h"
#import "UserModel.h"
#import "ChatViewController.h"
#import "OrderPayViewController.h"
#import "XSShare.h"
#import "XSShareView.h"

#define progressBarColor XSYCOLOR(0x23a7f1)

@interface XSBaseWKWebViewController ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler,UIScrollViewDelegate>
@property(nonatomic,strong)UIButton *reloadBtn;
@property (nonatomic, strong) NSArray *weakScriptArray;
@property (nonatomic ,strong)WKWebViewConfiguration *configur;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, copy) NSString *currentRequestUrl;

@end

@implementation XSBaseWKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = BG_COLOR;
    [self setSubViews];
    [self.view addSubview:self.webView];
    
    //进度条初始化
    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 2)];
    self.progressView.tintColor = progressBarColor;
    self.progressView.trackTintColor = [UIColor whiteColor];
    //设置进度条的高度，下面这句代码表示进度条的宽度变为原来的1倍，高度变为原来的1.5倍.
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view addSubview:self.progressView];
}

#pragma mark-Lazy Loading
- (WKWebViewConfiguration *)configur {
    if (!_configur) {
        _configur = [[WKWebViewConfiguration alloc] init];
        WKPreferences *preferences = [[WKPreferences alloc] init];
        _configur.preferences = preferences;
        preferences.javaScriptEnabled = YES;
    }
    return _configur;
}

- (WKWebView *)webView {
    if (_webView == nil) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, mainHeight-kStatusBarAndNavigationBarHeight) configuration:self.configur];
        _webView.scrollView.bounces = NO;
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.allowsBackForwardNavigationGestures = YES;
      
         [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew| NSKeyValueObservingOptionOld context:nil];
    }
    return _webView;
}

#pragma mark-Private
- (void)setSubViews{

    self.backButton = [[UIButton alloc]  initWithFrame:CGRectMake(13, navBarHeight/2-20/2, 20, 20)];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left1 = [[UIBarButtonItem alloc] initWithCustomView:self.backButton];
    
    self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.backButton.frame)+10, navBarHeight/2-20/2, 20, 20)];
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"nav_close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left2 = [[UIBarButtonItem alloc] initWithCustomView:self.closeButton];
    
    UIBarButtonItem *fixBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixBar.width = 20;
    
    self.navigationItem.leftBarButtonItems = @[left1,fixBar,left2];
    
    self.reloadBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth-13-20, navBarHeight/2-20/2, 20, 20)];
    [self.reloadBtn  setBackgroundImage:[UIImage imageNamed:@"mall_exchange"] forState:UIControlStateNormal];
    [self.reloadBtn  addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.reloadBtn];
    self.navigationItem.rightBarButtonItem = item;
    
}

- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    
    // 添加device参数
    if (![_urlStr containsString:@"device="]) {
        NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
        if (isEmptyString(guid)) {
            guid = @"app";
        }
        if ([_urlStr containsString:@"?"]) {
            _urlStr = [NSString stringWithFormat:@"%@&device=%@",_urlStr,guid];
        } else {
            _urlStr = [NSString stringWithFormat:@"%@?device=%@",_urlStr,guid];
        }
    }
    
    // 添加多语言参数
    if (![_urlStr containsString:@"lang="]) {
        NSString *language = SelectLang;
        if (!language || [language isEqualToString:@""]) {
            language = @"zh-Hans";
            [[NSUserDefaults standardUserDefaults] setObject:language forKey:MyLanguage];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        //英文传en,中文传zh-cn;繁体传zh-tw
        if ([language isEqualToString:@"zh-Hans"]) {
            language = @"zh-cn";
        } else if ([language isEqualToString:@"en"]) {
            language = @"en";
        } else if ([language isEqualToString:@"zh-Hant"]) {
            language = @"zh-tw";
        }
        
        if ([_urlStr containsString:@"?"]) {
            _urlStr = [NSString stringWithFormat:@"%@&lang=%@",_urlStr,language];
        } else {
            _urlStr = [NSString stringWithFormat:@"%@?lang=%@",_urlStr,language];
        }
    }
}

- (void)setUrlType:(WKWebViewURLType)urlType {
    switch (urlType) {
        case WKWebViewURLNormal:
        {
            self.weakScriptArray = @[@"toChat",@"toCall",@"toShare"];
        }
            break;
        case WKWebViewURLRefundDetail:
        {
            self.weakScriptArray = @[@"toChat",@"toCall"];
        }
            break;
        case WKWebViewURLRefundList:
        {
            self.weakScriptArray = @[@"toChat",@"toCall"];
        }
            break;
        case WKWebViewURLOffLine:
        {
            self.weakScriptArray = @[@"toChat",@"toCall",@"toPay"];
        }
            break;
        case WKWebViewURLPresell:
        {
            self.weakScriptArray = @[@"toChat",@"toCall",@"toPay"];
        }
            break;
        case WKWebViewURLShare:
        {
            self.weakScriptArray = @[@"shareToWx",@"shareToWxCircle",@"shareToQQ",@"shareToQZONE"];
        }
            break;
        case WKWebViewURLOfflineGoodsDetail:
        {
            self.weakScriptArray = @[@"toChat",@"toCall",@"toPay"];
        }
            break;
        case WKWebViewURLGroupBuy:
        {
            self.weakScriptArray = @[@"toChat",@"toPay"];
        }
            break;
            case WKWebViewURLPayLink:
        {
            self.weakScriptArray = @[@"toApp"];
        }
            break;
    }
    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
    [self.weakScriptArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [userContentController addScriptMessageHandler:[[WeakScriptMessageDelegate alloc] initWithDelegate:self] name:self.weakScriptArray[idx]];
    }];
    self.configur.userContentController = userContentController;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10]];
}

- (void)backClick {
    //网页支付返回处理
    if ([self.currentRequestUrl containsString:@"wap/#/order/pay/success"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
         [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if ([self.currentRequestUrl containsString:@"wap/#/order/pay/fail"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PAY_FAIL_LIST object:nil];
         [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)closeClick {
    if ([self.currentRequestUrl containsString:@"wap/#/order/pay/success"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if ([self.currentRequestUrl containsString:@"wap/#/order/pay/fail"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PAY_FAIL_LIST object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshData{
    [self.webView reload];
}

//监听网页加载进度  加载完成，刷新按钮旋转
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (self.webView.estimatedProgress== 1) {
            [self.progressView setProgress:0 animated:NO];
            [UIView animateWithDuration:0.7                                                                                                                                                                                                                                  animations:^{
                self.reloadBtn.transform=CGAffineTransformRotate(self.reloadBtn.transform, M_PI);
            }];
            
            /*
             *添加一个简单的动画，将progressView的Height变为1.4倍，在开始加载网页的代理中会恢复为1.5倍
             *动画时长0.25s，延时0.3s后开始动画
             *动画结束后将progressView隐藏
             */
            __weak typeof (self)weakSelf = self;
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
            } completion:^(BOOL finished) {
                weakSelf.progressView.hidden = YES;
            }];
        } else {
            self.progressView.hidden = NO;
            [self.progressView setProgress:newprogress animated:YES];
        }
//        self.progressView.progress = self.webView.estimatedProgress;
//        if (self.progressView.progress == 1) {
//            /*
//             *添加一个简单的动画，将progressView的Height变为1.4倍，在开始加载网页的代理中会恢复为1.5倍
//             *动画时长0.25s，延时0.3s后开始动画
//             *动画结束后将progressView隐藏
//             */
//            __weak typeof (self)weakSelf = self;
//            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
//                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
//            } completion:^(BOOL finished) {
//                weakSelf.progressView.hidden = YES;
//
//            }];
//        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
        [XSTool showProgressHUDTOView:self.view withText:nil];
    //开始加载网页时展示出progressView
    self.progressView.hidden = NO;
    //开始加载网页的时候将progressView的Height恢复为1.5倍
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    //防止progressView被网页挡住
    [self.view bringSubviewToFront:self.progressView];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
        [XSTool hideProgressHUDWithView:self.view];
    self.progressView.hidden = YES;
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
        [XSTool hideProgressHUDWithView:self.view];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
        [XSTool hideProgressHUDWithView:self.view];
    self.progressView.hidden = YES;
}

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
}

// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *urlString = [NSString stringWithFormat:@"%@",navigationAction.request.URL];
    NSLog(@"urlString   =%@",urlString);
    self.currentRequestUrl = urlString;
    
    if ([urlString containsString:@"weixin://wap/pay"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{@"scene_info":@{@"h5_info":@{@"type":@"IOS",@"app_name":@"福旦",@"bundle_id":@"com.xsy.app"}}} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    if ([message.name isEqualToString:@"toShare"]) {
        NSArray *arr = message.body;
        [self shareInfoWithArticleID:arr[1]];
    }else if ([message.name isEqualToString:@"back"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([message.name isEqualToString:@"toCall"]){
        NSString *arr = message.body;
        [self clickToCallMan:arr];
    }else if ([message.name isEqualToString:@"toChat"]){
        NSArray *arr = message.body;
        [self clickToMassageMan:arr];
    }else if ([message.name isEqualToString:@"toPay"]){
        NSArray *arr = message.body;
        [self clickToPayMoney:arr];
    } else if ([message.name isEqualToString:@"shareToWx"]){
        [XSShare singleShareToWeChat:message.body parentVC:self];
    } else if ([message.name isEqualToString:@"shareToWxCircle"]){
        [XSShare singleShareToWeChatCircle:message.body parentVC:self];
    } else if ([message.name isEqualToString:@"shareToQQ"]){
        [XSShare singleShareToQQ:message.body parentVC:self];
    } else if ([message.name isEqualToString:@"shareToQZONE"]){
        [XSShare singleShareToQZONE:message.body parentVC:self];
    } else if ([message.name isEqualToString:@"toApp"]){

        //注： 暂时vue屏蔽了此按钮

         //网页支付->成功跳转
        // 参数对可能取值：
       /*
        window.webkit.messageHandlers.toApp.postMessage([routerName, query])
        1 OrderDetail, {id: ids}  // 订单详情页，ids订单id
        2 Index                   // 商城首页
        3 QuickpayList            // 快速买单记录
        4 CFOrderDetail, {id: ids} // 众筹订单详情页面
        5 CrowdFundList            // 众筹订单列表页面
        6 rechargelists            // 充值记录页面
        **/
    }
}

//原生分享
- (void)shareInfoWithArticleID:(NSString *)articleID {
    
    //分享的url
    NSString *guid = [XSTool getStrUseKey:APPAUTH_KEY];
    NSURL *urlToShare = [NSURL URLWithString:[NSString stringWithFormat:@"%@/wap/#/article2/detail/%@?device=%@", ImageBaseUrl, articleID, guid]];
    
    NSArray *activityItems = @[urlToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
    //不出现在活动项目
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
    // 分享之后的回调
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            NSLog(@"completed");
            //分享 成功
        } else  {
            NSLog(@"cancled");
            //分享 取消
        }
    };
}

- (void)clickToPayMoney:(NSArray *) array {
    OrderPayViewController *controller = [[OrderPayViewController alloc] init];
    controller.orderId = [NSString stringWithFormat:@"%@",[array firstObject]];
    controller.tableName = [NSString stringWithFormat:@"%@",[array lastObject]];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)clickToCallMan:(NSString *)phone {
    
    if ([phone isEqualToString:@""]||[phone isEqualToString:@" "]) {
        [XSTool showToastWithView:self.view Text:@"电话错误"];
    } else {
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel://",phone]];
        [[UIApplication sharedApplication] openURL:telURL];
    }
}

- (void)clickToMassageMan:(NSArray *) array {
    
    [XSTool showProgressHUDWithView:self.view];
    @weakify(self);
    [HTTPManager getUserInfoWithUid:[array firstObject] success:^(NSDictionary *dic, resultObject *state) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        if (state.status) {
            UserInfoDataParser *parser = [UserInfoParser mj_objectWithKeyValues:dic].data;
            [XSTool hideProgressHUDWithView:self.view];
#ifdef ALIYM_AVALABLE
            YWPerson *person = [[YWPerson alloc] initWithPersonId:parser.uid];
            YWP2PConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SPKitExample sharedInstance].ywIMKit.IMCore];
            YWChatViewController *chatVC = [[YWChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [parser getName];
            chatVC.avatarUrl = parser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#elif defined EMIM_AVALABLE
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:parser.uid type:EMConversationTypeChat createIfNotExist:YES];
            ChatViewController *chatVC = [[ChatViewController alloc] initWithConversation:conversation];
            chatVC.title = [parser getName];
            chatVC.avatarUrl = parser.logo;
            chatVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatVC animated:YES];
#else
            XMChatController *chatVC = [XMChatController new];
            SiganlChatModel *chatModel = [[SiganlChatModel alloc] initWithChatWithJID:parser.uid title:[parser getName] avatarURLPath:parser.logo];
            chatVC.conversationModel = [[ConversationModel alloc] initWithSiganlChatModel:chatModel];
            [self.navigationController pushViewController:chatVC animated:YES];
#endif
            
        } else {
            [XSTool showToastWithView:self.view Text:state.info];
        }
    } fail:^(NSError *error) {
        @strongify(self);
        [XSTool hideProgressHUDWithView:self.view];
        [XSTool showToastWithView:self.view Text:NetFailure];
    }];
}

- (void)dealloc {
    [self.weakScriptArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[self.webView configuration].userContentController removeScriptMessageHandlerForName:self.weakScriptArray[idx]];
    }];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

@end
@implementation WeakScriptMessageDelegate

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate {
    self = [super init];
    if (self) {
        _scriptDelegate = scriptDelegate;
    }
    return self;
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
}

@end
