//
//  XSPopView.h
//  Pop
//
//  Created by apple on 2017/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FP_POPOVER_ARROW_HEIGHT 20.0
#define FP_POPOVER_ARROW_BASE 20.0
#define FP_POPOVER_RADIUS 10.0


typedef NS_ENUM(NSInteger, XSPopoverArrowDirection) {
  
    XSPopoverArrowDirectionUp = 0,
    XSPopoverArrowDirectionDown,
    XSPopoverArrowDirectionLeft,
    XSPopoverArrowDirectionRight,
    
    XSPopoverArrowDirectionVertical = XSPopoverArrowDirectionUp | XSPopoverArrowDirectionDown,
    
    XSPopoverArrowDirectionHorizontal = XSPopoverArrowDirectionLeft | XSPopoverArrowDirectionRight,
    
    XSPopoverArrowDirectionAny = XSPopoverArrowDirectionUp | XSPopoverArrowDirectionDown |
    XSPopoverArrowDirectionLeft | XSPopoverArrowDirectionRight
};

#define XSPopoverArrowDirectionIsVertical(direction)    ((direction) == XSPopoverArrowDirectionVertical || (direction) == XSPopoverArrowDirectionUp || (direction) == XSPopoverArrowDirectionDown)

#define XSPopoverArrowDirectionIsHorizontal(direction)    ((direction) == XSPopoverArrowDirectionHorizontal || (direction) == XSPopoverArrowDirectionLeft || (direction) == XSPopoverArrowDirectionRight)

@interface XSPopView : UIView

@property(nonatomic,assign) CGPoint relativeOrigin;

@property (nonatomic ,strong)UIView *contentView;

- (void)setArrowDirection:(XSPopoverArrowDirection)arrowDirection;
- (XSPopoverArrowDirection)getDirection;

+ (instancetype)initWithFrame:(CGRect)frame creatContent:(UIView *)contentView setArrowDirection:(XSPopoverArrowDirection)arrowDirection;

@end






typedef void (^FPTouchedOutsideBlock)();
typedef void (^FPTouchedInsideBlock)();

@protocol XSPopTouchDelegate <NSObject>

- (void)touchOutSide;

@end



@interface FPTouchView : UIView

@property (nonatomic ,copy)FPTouchedOutsideBlock outsideBlock;
@property (nonatomic ,copy)FPTouchedInsideBlock insideBlock;
@property (nonatomic ,weak)id<XSPopTouchDelegate> toucheDelegate;

@end




@interface UIView (YYAdd)
@property (nonatomic) CGFloat left;        ///< Shortcut for frame.origin.x.
@property (nonatomic) CGFloat top;         ///< Shortcut for frame.origin.y
@property (nonatomic) CGFloat right;       ///< Shortcut for frame.origin.x + frame.size.width
@property (nonatomic) CGFloat bottom;      ///< Shortcut for frame.origin.y + frame.size.height
@property (nonatomic) CGFloat width;       ///< Shortcut for frame.size.width.
@property (nonatomic) CGFloat height;      ///< Shortcut for frame.size.height.
@property (nonatomic) CGFloat centerX;     ///< Shortcut for center.x
@property (nonatomic) CGFloat centerY;     ///< Shortcut for center.y
@property (nonatomic) CGPoint origin;      ///< Shortcut for frame.origin.
@property (nonatomic) CGSize  size;        ///< Shortcut for frame.size.

@end

