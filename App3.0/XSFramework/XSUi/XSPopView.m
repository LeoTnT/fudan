//
//  XSPopView.m
//  Pop
//
//  Created by apple on 2017/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "XSPopView.h"


@implementation XSPopView
{
    XSPopoverArrowDirection xsPopDirection;
}


- (instancetype)initWithFrame:(CGRect)frame creatContent:(UIView *)contentView setArrowDirection:(XSPopoverArrowDirection)arrowD
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        
        self.layer.shadowOpacity = 0.7;
        self.layer.shadowRadius = 5;
        self.layer.shadowOffset = CGSizeMake(-3, 3);
        self.contentMode = UIViewContentModeRedraw;
    
 
        [self setArrowDirection:arrowD];
        [self addContentView:contentView];
    }
    return self;
}

+ (instancetype)initWithFrame:(CGRect)frame creatContent:(UIView *)contentView setArrowDirection:(XSPopoverArrowDirection)arrowDirection{
    
    return [[self alloc]initWithFrame:frame creatContent:contentView setArrowDirection:arrowDirection];
}


-(void)addContentView:(UIView *)contentView
{
    if(_contentView != contentView)
    {
        [_contentView removeFromSuperview];
        
        _contentView = contentView;
        [self addSubview:_contentView];
    }
    [self setupViews];
}

- (void)setArrowDirection:(XSPopoverArrowDirection)arrowDirection {
    
    xsPopDirection = arrowDirection;
    [self setNeedsDisplay];
}


- (XSPopoverArrowDirection)getDirection {
    return xsPopDirection;
}


#pragma mark drawing

//the content with the arrow
-(CGPathRef)createContentPathWithBorderWidth:(CGFloat)borderWidth arrowDirection:(XSPopoverArrowDirection)direction
{
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    CGFloat ah = FP_POPOVER_ARROW_HEIGHT; //is the height of the triangle of the arrow
    CGFloat aw = FP_POPOVER_ARROW_BASE/2.0; //is the 1/2 of the base of the arrow
    CGFloat radius = FP_POPOVER_RADIUS;
    CGFloat b = borderWidth;
    
    CGRect rect;
    
    switch (direction) {
        case XSPopoverArrowDirectionUp:
        {
            rect.size.width = w - 2*b;
            rect.size.height = h - ah - 2*b;
            rect.origin.x = b;
            rect.origin.y = ah + b;
        }
            break;
        case XSPopoverArrowDirectionDown:
        {
            rect.size.width = w - 2*b;
            rect.size.height = h - ah - 2*b;
            rect.origin.x = b;
            rect.origin.y = b;
        }
            break;
        case XSPopoverArrowDirectionRight:
        {
            rect.size.width = w - ah - 2*b;
            rect.size.height = h - 2*b;
            rect.origin.x = b;
            rect.origin.y = b;
        }
            break;
        case XSPopoverArrowDirectionLeft:
        {
            rect.size.width = w - ah - 2*b;
            rect.size.height = h - 2*b;
            rect.origin.x = ah + b;
            rect.origin.y = b;
        }
            break;
    }

    
    //the arrow will be near the origin
    CGFloat ax = self.relativeOrigin.x - aw; //the start of the arrow when UP or DOWN
    if(ax < aw + b) ax = aw + b;
    else if (ax +2*aw + 2*b> self.bounds.size.width) ax = self.bounds.size.width - 2*aw - 2*b;
    
    CGFloat ay = self.relativeOrigin.y - aw; //the start of the arrow when RIGHT or LEFT
    if(ay < aw + b) ay = aw + b;
    else if (ay +2*aw + 2*b > self.bounds.size.height) ay = self.bounds.size.height - 2*aw - 2*b;
    
    
    //ROUNDED RECT
    // arrow UP
    CGRect innerRect = CGRectInset(rect, radius, radius);
    CGFloat inside_right = innerRect.origin.x + innerRect.size.width;
    CGFloat outside_right = rect.origin.x + rect.size.width;
    CGFloat inside_bottom = innerRect.origin.y + innerRect.size.height;
    CGFloat outside_bottom = rect.origin.y + rect.size.height;
    CGFloat inside_top = innerRect.origin.y;
    CGFloat outside_top = rect.origin.y;
    CGFloat outside_left = rect.origin.x;
    
    
    //drawing the border with arrow
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, innerRect.origin.x, outside_top);
    
    //top arrow
    if(direction == XSPopoverArrowDirectionUp)
    {
        CGPathAddLineToPoint(path, NULL, ax, ah+b);
        CGPathAddLineToPoint(path, NULL, ax+aw, b);
        CGPathAddLineToPoint(path, NULL, ax+2*aw, ah+b);
        
    }
    
    
    CGPathAddLineToPoint(path, NULL, inside_right, outside_top);
    CGPathAddArcToPoint(path, NULL, outside_right, outside_top, outside_right, inside_top, radius);
    
    //right arrow
    if(direction == XSPopoverArrowDirectionRight)
    {
        CGPathAddLineToPoint(path, NULL, outside_right, ay);
        CGPathAddLineToPoint(path, NULL, outside_right + ah+b, ay + aw);
        CGPathAddLineToPoint(path, NULL, outside_right, ay + 2*aw);
    }
    
    
    CGPathAddLineToPoint(path, NULL, outside_right, inside_bottom);
    CGPathAddArcToPoint(path, NULL,  outside_right, outside_bottom, inside_right, outside_bottom, radius);
    
    //down arrow
    if(direction == XSPopoverArrowDirectionDown)
    {
        CGPathAddLineToPoint(path, NULL, ax+2*aw, outside_bottom);
        CGPathAddLineToPoint(path, NULL, ax+aw, outside_bottom + ah);
        CGPathAddLineToPoint(path, NULL, ax, outside_bottom);
    }
    
    CGPathAddLineToPoint(path, NULL, innerRect.origin.x, outside_bottom);
    CGPathAddArcToPoint(path, NULL,  outside_left, outside_bottom, outside_left, inside_bottom, radius);
    
    //left arrow
    if(direction == XSPopoverArrowDirectionLeft)
    {
        CGPathAddLineToPoint(path, NULL, outside_left, ay + 2*aw);
        CGPathAddLineToPoint(path, NULL, outside_left - ah-b, ay + aw);
        CGPathAddLineToPoint(path, NULL, outside_left, ay);
    }
    
    
    CGPathAddLineToPoint(path, NULL, outside_left, inside_top);
    CGPathAddArcToPoint(path, NULL,  outside_left, outside_top, innerRect.origin.x, outside_top, radius);
    
    
    CGPathCloseSubpath(path);
    
    return path;
}





- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    //content fill
    CGPathRef contentPath = [self createContentPathWithBorderWidth:2.0 arrowDirection:xsPopDirection];
    
    CGContextAddPath(ctx, contentPath);
    CGContextClip(ctx);
    
    CGPoint start;
    CGPoint end;
    if(xsPopDirection == XSPopoverArrowDirectionUp)
    {
        start = CGPointMake(self.width/2.0, 0);
        end = CGPointMake(self.width/2.0,40);
    }
    else
    {
        start = CGPointMake(self.width/2.0, 0);
        end = CGPointMake(self.width/2.0,20);
    }
    
    
    
    CGContextSetRGBFillColor(ctx, 1, 1, 1, 0);
    
    
    CGContextFillRect(ctx, CGRectMake(0, end.y, self.width, self.height-end.y));
    //internal border
    CGContextBeginPath(ctx);
    CGContextAddPath(ctx, contentPath);
    CGContextSetRGBStrokeColor(ctx, 0.7, 0.7, 0.7, 1.0);
    CGContextSetLineWidth(ctx, 1);
    CGContextSetLineCap(ctx,kCGLineCapRound);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextStrokePath(ctx);
    CGPathRelease(contentPath);
    
    //external border
    CGPathRef externalBorderPath = [self createContentPathWithBorderWidth:1.0 arrowDirection:xsPopDirection];
    CGContextBeginPath(ctx);
    CGContextAddPath(ctx, externalBorderPath);
    CGContextSetRGBStrokeColor(ctx, 0.4, 0.4, 0.4, 1.0);
    CGContextSetLineWidth(ctx, 1);
    CGContextSetLineCap(ctx,kCGLineCapRound);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextStrokePath(ctx);
    CGPathRelease(externalBorderPath);
    
    //3D border of the content view
    CGRect cvRect = _contentView.frame;
    //firstLine
    CGContextSetRGBStrokeColor(ctx, 0.7, 0.7, 0.7, 1.0);
    CGContextStrokeRect(ctx, cvRect);
    //secondLine
    cvRect.origin.x -= 1; cvRect.origin.y -= 1; cvRect.size.height += 2; cvRect.size.width += 2;
    CGContextSetRGBStrokeColor(ctx, 1, 1, 1, 1.0);
    CGContextStrokeRect(ctx, cvRect);
    
    
    
    CGContextRestoreGState(ctx);
}

-(void)setupViews
{
    //content posizion and size
    CGRect contentRect = _contentView.frame;
    
    if(xsPopDirection == XSPopoverArrowDirectionUp)
    {
        contentRect.origin = CGPointMake(10, 30);
        contentRect.size = CGSizeMake(self.width-20, self.height-40);

    }
    else if(xsPopDirection == XSPopoverArrowDirectionDown)
    {
        contentRect.origin = CGPointMake(10, 40);
        contentRect.size = CGSizeMake(self.width-20, self.height-70);

    }
    
    
    else if(xsPopDirection == XSPopoverArrowDirectionRight)
    {
        contentRect.origin = CGPointMake(10, 40);
        contentRect.size = CGSizeMake(self.width-40, self.height-50);

    }
    
    else if(xsPopDirection == XSPopoverArrowDirectionLeft)
    {
        contentRect.origin = CGPointMake(10 + FP_POPOVER_ARROW_HEIGHT, 40);
        contentRect.size = CGSizeMake(self.width-40, self.height-50);

    }
    
    _contentView.frame = contentRect;
    
    
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    [self setupViews];
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self setupViews];
}

-(void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    [self setupViews];
}



@end



/*  FPTouchView  */

@implementation FPTouchView



/**
  获取到点击的View
 */
-(UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *subview = [super hitTest:point withEvent:event];
    
    if(UIEventTypeTouches == event.type)
    {
        BOOL touchedInside = subview != self;
        if(!touchedInside)
        {
            for(UIView *s in self.subviews)
            {
                if(s == subview)
                {
                    //touched inside
                    touchedInside = YES;
                    break;
                }
            }
        }


        
        if(touchedInside && _insideBlock)
        {
            _insideBlock();
            _insideBlock = nil;
        }
        else if(!touchedInside && _outsideBlock)
        {
            _outsideBlock();
        }

    }
    
    return subview;
}


@end



@implementation UIView (YYAdd)

- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

- (CGFloat)centerY {
    return self.center.y;
}

- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

@end
