//
//  XSChoseImage.h
//  App3.0
//
//  Created by apple on 2017/5/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSChoseImage : UIView

@property (nonatomic ,copy)void (^ChoseImageData)(UIImage *);
@property (nonatomic ,strong)UIImageView *imageView;
+ (instancetype)creatXSChoseImage:(UIViewController *)viewController;


@end

