//
//  XSBaseTableViewController.m
//  App3.0
//
//  Created by mac on 2017/9/22.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseTableViewController.h"

@interface XSBaseTableViewController ()

@end

@implementation XSBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setter

- (void)setTableViewStyle:(UITableViewStyle)tableViewStyle {
    _tableViewStyle = tableViewStyle;
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:tableViewStyle];
    _tableView.backgroundColor = BG_COLOR;
    _tableView.accessibilityIdentifier = @"table_view";
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    if (tableViewStyle == UITableViewStyleGrouped) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
#ifdef __IPHONE_11_0
    if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
#endif
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
//    [self.view layoutIfNeeded];
//    
//    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.00001)];
    
    _showRefreshHeader = NO;
    _showRefreshFooter = NO;
}

- (void)setShowRefreshHeader:(BOOL)showRefreshHeader
{
    if (_showRefreshHeader != showRefreshHeader) {
        _showRefreshHeader = showRefreshHeader;
        if (_showRefreshHeader) {
            __weak XSBaseTableViewController *weakSelf = self;
            MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [weakSelf tableViewDidTriggerHeaderRefresh];
            }];
            //设置不同刷新状态的文字
            [header setTitle:Localized(@"释放刷新") forState:MJRefreshStatePulling];
            [header setTitle:Localized(@"加载中，请稍候") forState:MJRefreshStateRefreshing];
            [header setTitle:Localized(@"下拉刷新") forState:MJRefreshStateIdle];
            //设置字体大小和文字颜色
            header.stateLabel.font=[UIFont systemFontOfSize:12];
            header.stateLabel.textColor=[UIColor darkGrayColor];
            header.lastUpdatedTimeLabel.hidden=YES;
            self.tableView.mj_header=header;
            self.tableView.mj_header.accessibilityIdentifier = @"refresh_header";
            //            header.updatedTimeHidden = YES;
        }
        else{
            [self.tableView setMj_header:nil];
        }
    }
}

- (void)setShowRefreshFooter:(BOOL)showRefreshFooter
{
    if (_showRefreshFooter != showRefreshFooter) {
        _showRefreshFooter = showRefreshFooter;
        if (_showRefreshFooter) {
            __weak XSBaseTableViewController *weakSelf = self;
            MJRefreshBackNormalFooter *footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [weakSelf tableViewDidTriggerFooterRefresh];
            }];
            [footer setTitle:@"释放加载" forState:MJRefreshStatePulling];
            [footer setTitle:@"正在加载，请稍候" forState:MJRefreshStateRefreshing];
            [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
            //设置字体大小和文字颜色
            footer.stateLabel.font=[UIFont systemFontOfSize:14];
            footer.stateLabel.textColor=[UIColor darkGrayColor];
            self.tableView.mj_footer=footer;
            self.tableView.mj_footer.accessibilityIdentifier = @"refresh_footer";
        }
        else{
            [self.tableView setMj_footer:nil];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return KCELLDEFAULTHEIGHT;
}

#pragma mark - public refresh

- (void)autoTriggerHeaderRefresh
{
    if (self.showRefreshHeader) {
        [self tableViewDidTriggerHeaderRefresh];
    }
}

- (void)tableViewDidTriggerHeaderRefresh
{
    if (self.refreshHeader) {
        self.refreshHeader();
    }
}

- (void)tableViewDidTriggerFooterRefresh
{
    if (self.refreshFooter) {
        self.refreshFooter();
    }
}

- (void)tableViewDidFinishTriggerHeader:(BOOL)isHeader reload:(BOOL)reload
{
    __weak XSBaseTableViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (reload) {
            [weakSelf.tableView reloadData];
        }
        
        if (isHeader) {
            [weakSelf.tableView.mj_header endRefreshing];
        }
        else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
    });
}
@end
