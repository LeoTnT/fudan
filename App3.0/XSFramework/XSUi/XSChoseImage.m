//
//  XSChoseImage.m
//  App3.0
//
//  Created by apple on 2017/5/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSChoseImage.h"
#import <MobileCoreServices/MobileCoreServices.h>
@interface XSChoseImage ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic ,strong) UIImagePickerController *pickerVC;
@end

@implementation XSChoseImage
{
    UIViewController *_viewController;
}
+ (instancetype)creatXSChoseImage:(UIViewController *)viewController {
    
    
    return [[self alloc]initWithXSChoseImage:viewController];
}


-(instancetype)initWithXSChoseImage:(UIViewController *)viewController {
    
    if (self = [super init]) {
        _viewController = viewController;
        [self creatContentView];
    }
    return self;
}


- (void)creatContentView{
    
    
    self.imageView = UIImageView.new;
    [self addSubview:self.imageView];
    self.imageView.userInteractionEnabled = YES;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    @weakify(self);
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        @strongify(self);
        [self showChouseAlertView];
    }]];
    
}

- (void)showChouseAlertView {

    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"选择照片" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self xiangpian];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [_viewController presentViewController:alert animated:YES completion:nil];
    
}


-(void)xiangpian
{

    [_viewController presentViewController:self.pickerVC animated:YES completion:nil];
    
    
}


-(void)takeAPhoto{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //数据源
        self.pickerVC.sourceType=UIImagePickerControllerSourceTypeCamera;
        //展示拍照控板
        self.pickerVC.showsCameraControls=YES;
        //摄像头捕获模式
        self.pickerVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
        //后置摄像头
        self.pickerVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
        self.pickerVC.allowsEditing = NO;
        [_viewController presentViewController:self.pickerVC animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"照相机不可用" message:@"" delegate:self cancelButtonTitle:Localized(@"material_dialog_positive_text") otherButtonTitles:nil, nil];
        alert.tag = 1102;
        [alert show];
    }
    
}


#pragma mark - ----- -
#pragma mark - ----- - - -照片查看器代理方法

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        [UIApplication sharedApplication].statusBarHidden = NO;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
    }];
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    //判断是静态图像还是视频
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        //获取用户编辑之后的图像
        UIImage* editedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        
//        NSData *data = UIImageJPEGRepresentation(editedImage, 0.2);

        if (_ChoseImageData) {
            _ChoseImageData(editedImage);
        }

    }
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self.pickerVC dismissViewControllerAnimated:YES completion:nil];
}


-(UIImagePickerController *)pickerVC
{
    if (_pickerVC) {
        _pickerVC = [[UIImagePickerController alloc] init];
        _pickerVC.delegate = self;
    }
    return _pickerVC;
}
@end
