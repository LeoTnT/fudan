//
//  PopViewC.m
//  Pop
//
//  Created by apple on 2017/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "PopViewC.h"
@interface PopViewC ()


-(CGPoint)originFromView:(UIView*)fromView;
-(CGFloat)parentWidth;
-(CGFloat)parentHeight;

-(CGRect)bestArrowDirectionAndFrameFromView:(UIView*)v;

@end

@implementation PopViewC

{
    CGSize popContentSize;
    FPTouchView *touchView;
    XSPopView *xsContentPopView;
    UIWindow *window;
    UIView *_parentView;
    UIView *showPopView;
    UIDeviceOrientation deviceOrientation;
    UIViewController *showViewController;
    XSPopoverArrowDirection arrowDirection;
}





+ (instancetype)creatViewController:(UIViewController *)viewController arrowDirection:(XSPopoverArrowDirection)arrowDirection showPopPoint:(UIView *)view contentSize:(NSInteger )dataCount{
    
    return [[self alloc] initWithViewController:viewController arrowDirection:arrowDirection showPopPoint:view contentSize:dataCount];
}

-(instancetype)initWithViewController:(UIViewController*)viewController
                       arrowDirection:(XSPopoverArrowDirection)arrowDic
                         showPopPoint:(UIView *)view
                          contentSize:(NSInteger )dataCount
{
    if(self = [super init])
    {
        
        if (dataCount == 0) {
            return nil;
        }
        
        NSAssert([view isKindOfClass:[UIView class]] != NO, @"showPopPoint 必须为UIView子类");
        showViewController = viewController;
        arrowDirection = XSPopoverArrowDirectionAny;
        showPopView = view;
        
        self.clipsToBounds = NO;
        self.userInteractionEnabled = YES;
        
        touchView = [[FPTouchView alloc] initWithFrame:self.bounds];
        touchView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        touchView.backgroundColor = [UIColor clearColor];
        touchView.clipsToBounds = NO;
        @weakify(self);
        [touchView setOutsideBlock:^{
            @strongify(self);
            [self dismissPopoverAnimated:YES];
        }];
        [self addSubview:touchView];
        CGFloat contentWidth;
        
        
        contentWidth = mainWidth/2;
        CGFloat height =  dataCount*40;
        popContentSize = CGSizeMake(contentWidth,height);
        xsContentPopView = [XSPopView initWithFrame:CGRectMake(0, 0,contentWidth, height) creatContent:viewController.view setArrowDirection:XSPopoverArrowDirectionUp];
        
        [touchView addSubview:xsContentPopView];
        
        
        xsContentPopView.clipsToBounds = NO;
        
        showViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self presentPopoverFromView:showPopView];
        
        
    }
    return self;
}

-(void)presentPopoverFromView:(UIView*)fromView
{
    [self presentPopoverFromPoint:[self originFromView:fromView]];
}




#pragma mark - View lifecycle

-(void)setupView
{
    self.frame = CGRectMake(0, 0, [self parentWidth], [self parentHeight]);
    
    [self bestArrowDirectionAndFrameFromView:showPopView];
    
}



#pragma mark presenting

-(CGFloat)parentWidth
{
    return _parentView.bounds.size.width;
    
}
-(CGFloat)parentHeight
{
    return _parentView.bounds.size.height;
    
}




/**
 从一点
 */
-(void)presentPopoverFromPoint:(CGPoint)fromPoint
{
    self.origin = fromPoint;
    xsContentPopView.relativeOrigin = [_parentView convertPoint:fromPoint toView:xsContentPopView];
    
    [self removeFromSuperview];
    _parentView=nil;
    window = [UIApplication sharedApplication].keyWindow;
    if(window.subviews.count > 0){
        _parentView = [window.subviews objectAtIndex:0] ;
        [_parentView addSubview:self];
    }
    
    
    [self setupView];
    self.alpha = 0.0;
    [UIView animateWithDuration:0.2 animations:^{
        
        self.alpha = 1.0;
    }];
    
    
}



/**
 获得点击视图并  返回视图中间点
 */
-(CGPoint)originFromView:(UIView*)fromView
{
    CGPoint p;
    //    if([xsContentPopView getDirection] == XSPopoverArrowDirectionUp)
    //    {
    //        p.x = fromView.left + fromView.width/2.0;
    //        p.y = fromView.top + fromView.height;
    //    }
    //    else if([xsContentPopView getDirection] == XSPopoverArrowDirectionDown)
    //    {
    //        p.x = fromView.left + fromView.width/2.0;
    //        p.y = fromView.top;
    //    }
    //    else if([xsContentPopView getDirection] == XSPopoverArrowDirectionLeft)
    //    {
    //        p.x = fromView.left + fromView.width;
    //        p.y = fromView.top + fromView.height/2.0;
    //    }
    //    else {
    //        p.x = fromView.left;
    //        p.y = fromView.top + fromView.height/2.0;
    //    }
    p.x = fromView.mj_w;
    p.y = fromView.mj_y;
    return p;
}


-(void)dismissPopover
{
    [self removeFromSuperview];
    window=nil;
    _parentView=nil;
}

-(void)dismissPopoverAnimated:(BOOL)animated
{
    if(animated)
    {
        [UIView animateWithDuration:.25 animations:^{
            self.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            [self dismissPopover];
        }];
    }
    else
    {
        [self dismissPopover];
    }
    
}

-(void)setOrigin:(CGPoint)origin
{
    _origin = origin;
}





-(CGRect)bestArrowDirectionAndFrameFromView:(UIView*)v
{
    
    
    
    CGPoint p = [v.superview convertPoint:v.frame.origin toView:self];
    
    CGFloat ht = p.y; //available vertical space on top of the view
    CGFloat hb = [self parentHeight] -  (p.y + v.frame.size.height); //on the bottom
    CGFloat wl = p.x; //on the left
    CGFloat wr = [self parentWidth] - (p.x + v.frame.size.width); //on the right
    
    CGFloat best_h = MAX(ht, hb); //much space down or up ?
    CGFloat best_w = MAX(wl, wr);
    
    CGRect r;
    r.size = popContentSize;
    
    XSPopoverArrowDirection bestDirection;
    
    if(XSPopoverArrowDirectionIsVertical(arrowDirection) ||
       (arrowDirection == XSPopoverArrowDirectionAny && best_h >= best_w))
    {
        
        //ok, will be vertical
        if(ht == best_h || arrowDirection == XSPopoverArrowDirectionDown)
        {
            //on the top and arrow down
            bestDirection = XSPopoverArrowDirectionDown;
            
            r.origin.x = p.x + v.frame.size.width/2.0 - r.size.width/2.0;
            r.origin.y = p.y - r.size.height;
        }
        else
        {
            //on the bottom and arrow up
            bestDirection = XSPopoverArrowDirectionUp;
            
            r.origin.x = p.x + v.frame.size.width/2.0 - r.size.width/2.0;
            r.origin.y = p.y + v.frame.size.height;
        }
        
        
    }
    
    
    else
    {
        
        if(wl == best_w || arrowDirection == XSPopoverArrowDirectionRight)
        {
            
            bestDirection = XSPopoverArrowDirectionRight;
            
            r.origin.x = p.x - r.size.width;
            r.origin.y = p.y + v.frame.size.height/2.0 - r.size.height/2.0;
            
        }
        else
        {
            
            bestDirection = XSPopoverArrowDirectionLeft;
            
            r.origin.x = p.x + v.frame.size.width;
            r.origin.y = p.y + v.frame.size.height/2.0 - r.size.height/2.0;
        }
        
        
    }
    
    
    
    //need to moved left ?
    if(r.origin.x + r.size.width > [self parentWidth])
    {
        r.origin.x = [self parentWidth] - r.size.width;
    }
    
    //need to moved right ?
    else if(r.origin.x < 0)
    {
        r.origin.x = 0;
    }
    
    
    //need to move up?
    if(r.origin.y < 0)
    {
        CGFloat old_y = r.origin.y;
        r.origin.y = 0;
        r.size.height += old_y;
    }
    
    //need to be resized horizontally ?
    if(r.origin.x + r.size.width > [self parentWidth])
    {
        r.size.width = [self parentWidth] - r.origin.x;
    }
    
    //need to be resized vertically ?
    if(r.origin.y + r.size.height > [self parentHeight])
    {
        r.size.height = [self parentHeight] - r.origin.y;
    }
    
    
    
    
    xsContentPopView.arrowDirection = bestDirection;
    xsContentPopView.frame = r;
    
    self.origin = v.center;
    xsContentPopView.relativeOrigin = [_parentView convertPoint:self.origin toView:xsContentPopView];
    
    
    return r;
}




@end

