//
//  ContentTable.m
//  Pop
//
//  Created by apple on 2017/4/18.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ContentTable.h"
#import "PopViewC.h"

@implementation ContentTable
{
    NSArray *listArr;
    
}



+ (instancetype)creatTableListWithData:(NSArray *)data {
    return [[self alloc]initWithContentData:data];
}

- (instancetype)initWithContentData:(NSArray *)data {
    if (self = [super init]) {
        if (data.count == 0) return nil;
        
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        listArr = data;
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    }
    return self;
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    
    id data = listArr[indexPath.row];
    if ([data isKindOfClass:[NSDictionary class]]) {
        cell.textLabel.text = listArr[indexPath.row][@"title"];
    }else if ([data isKindOfClass:[NSString class]]){
        cell.textLabel.text = data;
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (_selectedBlock) {
        _selectedBlock(indexPath.row);
        _selectedBlock = nil;
        
    }
}


@end


@implementation XSShowPopView


+ (void)pointView:(UIView *)view data:(NSArray *)dataSurce selectedRow:(void (^)(NSInteger))index{
    
    ContentTable *controller = [ContentTable creatTableListWithData:dataSurce];
    PopViewC *pop = [PopViewC creatViewController:controller arrowDirection:XSPopoverArrowDirectionAny showPopPoint:view contentSize:dataSurce.count];
    [controller setSelectedBlock:^(NSInteger indexRow) {
        if (index) {index(indexRow);}
        [pop dismissPopoverAnimated:YES];
    }];
    
}

@end

