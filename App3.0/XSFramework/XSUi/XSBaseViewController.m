//
//  XSBaseViewController.m
//  App3.0
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"
#import "FDLoginController.h"
#import "UserLoginViewController.h"
#import "TOCropViewController.h"
#import "XSClipImage.h"

@interface XSBaseViewController () <UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate>
@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, strong) UIImagePickerController *imagePickVC;
@property (nonatomic, assign) BOOL flag;

@end
static char *btnClickAction;
@implementation XSBaseViewController
@synthesize showBackBtn;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.hidesBottomBarWhenPushed) {
        self.tabBarController.tabBar.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.hidesBottomBarWhenPushed) {
        self.tabBarController.tabBar.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;

    _tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)];
    _tap.delegate = self;
    [self.view addGestureRecognizer:_tap];
    _tap.enabled = NO;
    self.view.backgroundColor = BG_COLOR;
    

    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:@"nav_back" title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (NSInteger)navi_Height {
    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
    CGRect rectOfNavigationbar = self.navigationController.navigationBar.frame;
    return rectOfStatusbar.size.height + rectOfNavigationbar.size.height;
}

- (NSInteger)tab_barHeight {
    return self.tabBarController.tabBar.frame.size.height;
}

- (CGFloat)iphonx_bottom {
    
    return self.navi_Height == 88 ? 34:0;
}

- (void)setNavi_title:(NSString *)navi_title {
    UIFont *font = [UIFont systemFontOfSize:18];
    UILabel *label = [BaseTool labelWithTitle:navi_title textAlignment:(NSTextAlignmentCenter) font:font titleColor:[UIColor blackColor]];
    label.frame = CGRectMake(0, 0, 100, 30);
    self.navigationItem.titleView = label;
}

//- (void) popHandleGesture:(UIGestureRecognizer *)gestureRecognizer {
//    
//    
//    
//}

- (void)xs_pushViewController:(UIViewController *)vc {
    if (isEmptyString([UserInstance ShardInstnce].uid)) {
        FDLoginController *login = [[FDLoginController alloc] init];
        XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
        [self presentViewController:navi animated:YES completion:nil];
    } else {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)endEditing {
    [self.view endEditing:YES];
}

//- (DefaultView *)baseDefaultView {
//    if (!_baseDefaultView) {
//        _baseDefaultView = [[DefaultView alloc] initWithFrame:self.view.bounds];
//        _baseDefaultView.button.hidden = YES;
//        _baseDefaultView.baseButton.hidden = YES;
//        [_baseDefaultView.button setImage:nil forState:UIControlStateNormal];
//        [_baseDefaultView.button setImage:nil forState:UIControlStateSelected];
//        [_baseDefaultView.button setImage:nil forState:UIControlStateHighlighted];
//    }
//    return _baseDefaultView;
//}

- (void)setAutoHideKeyboard:(BOOL)autoHideKeyboard
{
    _autoHideKeyboard = autoHideKeyboard;
    if (_autoHideKeyboard) {
        _tap.enabled = YES;
    } else {
        _tap.enabled = NO;
    }
    
}
- (void)setNavTitle:(NSString *)title isShowBack:(BOOL)isShowBack
{
    [self setNavTitle:title color:[UIColor blackColor]];
    if (isShowBack) {
        [self setBlackLeftBackBtn];
    }
//    [self setNavBackGroundColor:];
}

- (void)setWhiteBackGroundWithTitle:(NSString *)title
{
    [self setNavTitle:title color:[UIColor blackColor]];
    [self setBlackLeftBackBtn];
    [self setNavBackGroundColor:[UIColor whiteColor]];
}

- (void)setMainColorBackGroundWithTitle:(NSString *)title
{
    [self setNavTitle:title color:[UIColor whiteColor]];
    [self setWhiteLeftBackBtn];
    [self setNavBackGroundColor:mainColor];
}
- (void)setNavTitle:(NSString *)title color:(UIColor *)color
{
    self.title = title;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],NSForegroundColorAttributeName:color}];
    
}
- (void)setNavBackGroundColor:(UIColor *)color
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage xl_imageWithColor:color size:CGSizeMake(mainWidth, 64)] forBarMetrics:UIBarMetricsDefault];
    
}
#pragma mark -actionCustomLeftBtnWithNrlImage
- (void)setWhiteLeftBackBtn{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)setBlackLeftBackBtn{
    @weakify(self);
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark-手势代理，解决和tableview点击发生的冲突
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSLog(@"%@",NSStringFromClass([touch.view class]));
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if ([NSStringFromClass([touch.view class]) isEqualToString:@"RFRecordButton"]) {
            return NO;
        }
        return YES;
    }
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {//判断如果点击的是tableView的cell，就把手势给关闭了
        return NO;//关闭手势
    }//否则手势存在
    return YES;
}

#pragma mark -actionCustomLeftBtnWithNrlImage
- (void)actionCustomLeftBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                  title:(NSString *)title
                                 action:(void(^)())btnClickBlock {
    self.navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navLeftBtn setBackgroundColor:[UIColor clearColor]];
    objc_setAssociatedObject(self.navLeftBtn, &btnClickAction, btnClickBlock, OBJC_ASSOCIATION_COPY);
    [self actionCustomNavBtn:self.navLeftBtn nrlImage:nrlImage htlImage:hltImage title:isEmptyString(title)?@"":title];
    self.navLeftBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.navLeftBtn];
}

#pragma mark -actionCustomRightBtnWithNrlImage
- (void)actionCustomRightBtnWithNrlImage:(NSString *)nrlImage htlImage:(NSString *)hltImage
                                   title:(NSString *)title
                                  action:(void(^)())btnClickBlock {
    self.navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    objc_setAssociatedObject(self.navRightBtn, &btnClickAction, btnClickBlock, OBJC_ASSOCIATION_COPY);
    [self actionCustomNavBtn:self.navRightBtn nrlImage:nrlImage htlImage:hltImage title:title];
    self.navRightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.navRightBtn];
}

#pragma mark -actionCustomNavBtn
- (void)actionCustomNavBtn:(UIButton *)btn nrlImage:(NSString *)nrlImage
                  htlImage:(NSString *)hltImage
                     title:(NSString *)title {
    [btn setImage:[UIImage imageNamed:nrlImage] forState:UIControlStateNormal];
    if (hltImage) {
        [btn setImage:[UIImage imageNamed:hltImage] forState:UIControlStateHighlighted];
    } else {
        [btn setImage:[UIImage imageNamed:nrlImage] forState:UIControlStateNormal];
    }
    btn.frame = CGRectMake(0, 0, 44, 44);
    if (title) {
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:16.];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitle:title forState:UIControlStateHighlighted];
        [btn setTitleColor:mainColor forState:UIControlStateNormal];
        [btn setTitleColor:mainColor forState:UIControlStateHighlighted];
        
        if (title.length > 2) {
            CGFloat width = 20*title.length > 100 ? 100 : 20*title.length;
            btn.frame = CGRectMake(0, 0, width, 44);
        }
    }

    
    
    [btn addTarget:self action:@selector(actionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -actionBtnClick
- (void)actionBtnClick:(UIButton *)btn {
    void (^btnClickBlock) (void) = objc_getAssociatedObject(btn, &btnClickAction);
    btnClickBlock();
}

#pragma mark -getter or setter
- (void)setItemTitle:(NSString *)title {
    _itemTitle = title;
    [self.navigationItem setTitle:_itemTitle];
}

- (void)setShowBackBtn:(BOOL)showBack {
    @weakify(self);
    
    [self actionCustomLeftBtnWithNrlImage:@"btn_back" htlImage:nil title:nil action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event

{
    
    //不要调用super,发现调用super时又不能起作用了
    
}

- (void)choosePhotoes {
    [self.view endEditing:YES];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *photograph=[UIAlertAction actionWithTitle:Localized(@"拍照") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takeAPhoto];
    }];
    UIAlertAction *select=[UIAlertAction actionWithTitle:Localized(@"从相册中选择") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //打开相册
        self.imagePickVC = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.imagePickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        self.imagePickVC.delegate = self;
        self.imagePickVC.allowsEditing = NO;
        
        [self presentViewController:self.imagePickVC animated:YES completion:nil];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:Localized(@"cancel_btn") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:photograph];
    [alert addAction:select];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)takeAPhoto {
    self.imagePickVC=[[UIImagePickerController alloc] init];
    
    //拍照模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持拍照") message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    } else {
        
        //后置摄像头是否可用
        if(!([UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceFront)]||[UIImagePickerController isCameraDeviceAvailable:(UIImagePickerControllerCameraDeviceRear)])){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"当前设备不支持摄像头") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
        self.flag = YES;
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (self.flag) {
        
        //获取原始照片
        UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
        toVC.delegate=self;
        [picker presentViewController:toVC animated:NO completion:nil];
    } else {
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        
        //当选择的类型是图片
        if ([type isEqualToString:@"public.image"]) {
            UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            TOCropViewController *toVC=[[TOCropViewController alloc] initWithImage:image];
            toVC.delegate=self;
            [picker pushViewController:toVC animated:YES];
        }
    }
}
#pragma mark TOCropViewControllerDelegate
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if (self.flag) {
        //        [self.imagePickerVC dismissViewControllerAnimated:YES completion:nil];
        
        __weak typeof(self) weakSelf = self;
        [cropViewController dismissViewControllerAnimated:NO completion:^{
            //            XSLog(@"retain  count = %ld\n",CFGetRetainCount((__bridge  CFTypeRef)(weakSelf.imagePickerVC)));
            [weakSelf.imagePickVC dismissViewControllerAnimated:YES completion:nil];
        }];
        
    } else {
        [cropViewController.navigationController popViewControllerAnimated:YES];
    }
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [self dismissViewControllerAnimated:YES completion:^{
        
        UIImage *tempImage;
        NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
        if (imageData.length>=1024*1024*2) {//2M以及以上
            if (imageData.length<1024*1024*4) {
                imageData = UIImageJPEGRepresentation(image, 0.4);
            } else if (imageData.length<1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.2);
                
            } if (imageData.length>1024*1024*10) {
                imageData = UIImageJPEGRepresentation(image, 0.1);
                
            }
            tempImage = [UIImage imageWithData:imageData];
        } else {
            tempImage = [UIImage imageWithData:imageData];
            
        }
        
        [self getImageWithController:tempImage];
    }];
}
- (void) getImageWithController:(UIImage *)image{
    
}


- (void) shareActiveAction:(UIImage *)image {
    UIImage *imageToShare = image;
    if (isEmptyString(imageToShare)) {
        //        Alert(@"获取图片失败");
        return;
    }
    
    //分享的url
    //    NSURL *urlToShare = [NSURL URLWithString:@"http://www.baidu.com"];
    //在这里呢 如果想分享图片 就把图片添加进去  文字什么的通上
    //    NSArray *activityItems = @[textToShare,imageToShare, urlToShare];
    
    //需要被分享的内容
    //    UIImage *image = self.payQRView.QRImageV.image;
    //    NSString *add = self.payQRView.addLabel.text;
    //
    //    NSArray *activityItems = @[image, add];
    
    NSArray *activityItems = @[imageToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
    //不出现在活动项目
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
    
    // 分享之后的回调
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            NSLog(@"completed");
            //分享 成功
        } else  {
            NSLog(@"cancled");
            //分享 取消
        }
    };
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc {
    NSLog(@"控制器被dealloc: %@", [[self class]description]);
}
@end
