//
//  PopViewC.h
//  Pop
//
//  Created by apple on 2017/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XSPopView.h"


@interface PopViewC : UIView


@property(nonatomic,assign) CGPoint origin;

+ (instancetype)creatViewController:(UIViewController *)viewController arrowDirection:(XSPopoverArrowDirection)arrowDirection showPopPoint:(UIView *)view contentSize:(NSInteger )dataCount;


-(void)dismissPopoverAnimated:(BOOL)animated;


@end

