//
//  ContentTable.h
//  Pop
//
//  Created by apple on 2017/4/18.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentTable : UITableViewController

+ (instancetype)creatTableListWithData:(NSArray *)data;

@property (nonatomic ,copy)void(^selectedBlock)();


@end


@interface XSShowPopView : NSObject

+ (void)pointView:(UIView *)view data:(NSArray *)dataSurce selectedRow:(void (^)(NSInteger))index;


@end
