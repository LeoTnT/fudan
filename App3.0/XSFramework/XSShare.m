//
//  XSShare.m
//  App3.0
//
//  Created by mac on 2018/3/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XSShare.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>

@implementation XSSingleShareModel
@end

@interface XSShare () <WXApiDelegate, QQApiInterfaceDelegate>
@end

@implementation XSShare

+ (void)singleShareToWeChat:(NSDictionary *)shareData parentVC:(UIViewController *)vc {
    if (![WXApi isWXAppInstalled]) {
        [XSTool showToastWithView:vc.view Text:@"请移步App Store去下载微信客户端"];
        return;
    }
    
    XSSingleShareModel *model = [XSSingleShareModel mj_objectWithKeyValues:shareData];
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = model.title;
    message.description = model.content;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.logo]];
    UIImage *image = [UIImage imageWithData:imageData];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(64, 64)];
    [message setThumbImage:image];
    
    // 多媒体消息中包含的网页数据对象
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    // 网页的url地址
    webpageObject.webpageUrl = model.link;
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

+ (void)singleShareToWeChatCircle:(NSDictionary *)shareData parentVC:(UIViewController *)vc {
    if (![WXApi isWXAppInstalled]) {
        [XSTool showToastWithView:vc.view Text:@"请移步App Store去下载微信客户端"];
        return;
    }
    
    XSSingleShareModel *model = [XSSingleShareModel mj_objectWithKeyValues:shareData];
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = model.title;
    message.description = model.content;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.logo]];
    UIImage *image = [UIImage imageWithData:imageData];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(64, 64)];
    [message setThumbImage:image];
    
    // 多媒体消息中包含的网页数据对象
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    // 网页的url地址
    webpageObject.webpageUrl = model.link;
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    
    req.scene = WXSceneTimeline;
    [WXApi sendReq:req];
}

+ (void)singleShareToQQ:(NSDictionary *)shareData parentVC:(UIViewController *)vc {
    if (![TencentOAuth iphoneQQInstalled]) {
        [XSTool showToastWithView:vc.view Text:@"请移步App Store去下载腾讯QQ客户端"];
        return;
    }
    
    XSSingleShareModel *model = [XSSingleShareModel mj_objectWithKeyValues:shareData];
    
    // 防止qq自动拼接appinstall=0
    NSMutableString *mutStr = [[NSMutableString alloc] initWithString:model.link];
    if ([model.link containsString:@"#"]) {
        NSRange range = [mutStr rangeOfString:@"#"];
        [mutStr insertString:@"?" atIndex:range.location];
    }
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.logo]];
    UIImage *image = [UIImage imageWithData:imageData];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(64, 64)];
    imageData = UIImageJPEGRepresentation(image, 1);
    
    QQApiNewsObject *newsObj = [QQApiNewsObject
                                objectWithURL:[NSURL URLWithString:[mutStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                title:model.title
                                description:model.content
                                previewImageData:imageData];
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
    [QQApiInterface sendReq:req];

}

+ (void)singleShareToQZONE:(NSDictionary *)shareData parentVC:(UIViewController *)vc {
    if (![TencentOAuth iphoneQQInstalled]) {
        [XSTool showToastWithView:vc.view Text:@"请移步App Store去下载腾讯QQ客户端"];
        return;
    }
    
    XSSingleShareModel *model = [XSSingleShareModel mj_objectWithKeyValues:shareData];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.logo]];
    UIImage *image = [UIImage imageWithData:imageData];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(64, 64)];
    imageData = UIImageJPEGRepresentation(image, 1);
    
    QQApiNewsObject *newsObj = [QQApiNewsObject
                                objectWithURL:[NSURL URLWithString:[model.link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                title:model.title
                                description:model.content
                                previewImageData:imageData];
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
    [QQApiInterface SendReqToQZone:req];

}

#pragma mark 图片压缩

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

#pragma mark 

-(void)onReq:(id)req {
    
}

- (void)onResp:(id)resp {
    
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        //把返回的类型转换成与发送时相对于的返回类型,这里为SendMessageToWXResp
        SendMessageToWXResp *sendResp = (SendMessageToWXResp *)resp;
        
        //使用UIAlertView 显示回调信息
        NSString *str = [NSString stringWithFormat:@"%d",sendResp.errCode];
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"回调信息" message:str delegate:self cancelButtonTitle:Localized(@"material_dialog_default_title") otherButtonTitles:nil, nil];
        [alertview show];
        
        if (sendResp.errCode == WXSuccess) {
            
        }
    }
    if ([resp isKindOfClass:[SendMessageToQQResp class]]) {
        
        SendMessageToQQResp * tmpResp = (SendMessageToQQResp *)resp;
        
        if (tmpResp.type == ESENDMESSAGETOQQRESPTYPE && [tmpResp.result integerValue] == EQQAPISENDSUCESS) {
            
            [XSTool showToastWithView:[UIApplication sharedApplication].keyWindow Text:@"分享成功！"];
        }
    }
}


-(void)isOnlineResponse:(NSDictionary *)response {
    
}
@end
