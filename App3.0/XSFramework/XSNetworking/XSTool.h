//
//  XSTool.h
//  App3.0
//
//  Created by apple on 2017/5/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSTool : NSObject
/***创建控件***/
//创建label
+(UILabel *)createLabelWithText:(NSString *)text Font:(UIFont *)font textColor:(UIColor *)textColor lines:(int)numLines textAlignment:(NSTextAlignment)textAlignment;
//创建圆形imageview
+(UIImageView *)createCircleImageViewWithImage:(UIImage *)image radius:(CGFloat)radius;
//创建按钮
+(UIButton *)createBtnWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font bgColor:(UIColor *)bgColor image:(UIImage *)image bgImage:(UIImage *)bgImage;

/*MBHUBProgress*/
+(UIViewController *)getCurrentVC;
+ (void)showProgressHUDWithView:(UIView *)view;
+ (void)showProgressHUDTOView:(UIView *)view withText:(NSString *)text;
+ (void)hideProgressHUDWithView:(UIView *)view;
+ (void)showToastWithView:(UIView *)view Text:(NSString *)text;

/**
 登录成功调用
 */
+ (UIViewController *)enterMainViewController;

/**
 登录状态异常
 */
+ (void) otherLogIn;

+ (NSString *)convertToJsonData:(NSDictionary *)dict;

+ (NSDictionary *)convertToParamFromParameters:(NSDictionary *)dict;




+ (BOOL) creatPlistWithExceptionReport:(id )value key:(NSString *)key;

+ (BOOL) deleteExceptionReportForKey:(NSString *)key;

+ (NSMutableDictionary *) getContentsOfFile;

+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
/**
 根据路径获取字典
 
 */
+(NSDictionary *)returnDictionaryWithDataPath:(NSString *)path;


/**
 yyyy-MM-dd HH:mm:ss
 */
+ (NSDateFormatter *)shareYMDHMS;


/**
 yyyy-MM-dd
 */
+ (NSDateFormatter *) shareYMD;

//设置日期显示
+ (NSString*)dateStrStringFromDate:(NSDate *)inputDate;

//根据日期得到周几
+(NSString*)weekdayStringFromDate:(NSDate*)inputDate;

//日期 向前||向后 ?年 ?月  ?天
+ (NSDate*)dateStringFromDate:(NSDate *)inputDate
                         year:(NSInteger)year
                        month:(NSInteger)month
                          day:(NSInteger)day;

+ (NSString *)getStrUseKey:(NSString *)key ;

+ (void)saveStr:(NSString *)str forKey:(NSString *)key ;

+ (void)removeObjectForKey:(NSString *)str;



/**
 倒计时  要手动取消

 @param queue queue description
 @param sourceTime sourceTime description
 @param walltime walltime description
 @param stopAction stopAction description
 @param otherAction otherAction description
 */
+ (void)setTimeQueue:(dispatch_queue_t )queue dispatch_source:(dispatch_source_t )sourceTime
            walltime:(int) walltime stop:(void(^)())stopAction
         otherAction:(void(^)(int))otherAction;

+ (UIViewController *)createViewControllerWithClassName:(NSString *)className;

@end


@interface NSMutableArray (CCCC)

- (NSMutableArray *)sortedWithChineseKey ;

@end

@interface NSArray (decription)

@end

@interface NSString (Chinese)

/// 拼音字符串
@property (nonatomic, copy, readonly) NSString *pinyinString;

@end

@interface MBProgressHUD (MyHUD)


/**
 自动隐藏
 
 */
+ (void)showMessage:(NSString *)message view:(UIView *)view;


/**
 需要手动隐藏
 */
+ (void)showHUDAddToView:(UIView *)view withText:(NSString *)text;


/**
 设置延迟隐藏时间
 */
+ (void)showMessage:(NSString *)message view:(UIView *)view hideTime:(int) time doSomeThing:(void (^)())someThing ;

@end


@interface UIFont (PX_Fount)

+ (UIFont *)qsh_systemFontOfSize:(CGFloat)pxSize;


/**
 计算字符串高度
 @param text 字符串
 @param lableWidth lableWidth description
 @param fontSize fontSize description
 @return return value description
 */
+ (CGFloat)heightForLableWithText:(NSString *)text lableWidth:(CGFloat)lableWidth font:(CGFloat)fontSize;
@end


@interface UIView (XSView)


@property (nonatomic, assign) CGFloat xs_centerX;
@property (nonatomic, assign) CGFloat xs_centerY;


@end
