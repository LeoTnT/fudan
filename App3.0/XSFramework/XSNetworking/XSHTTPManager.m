//
//  XSHTTPManager.m
//  App3.0
//
//  Created by apple on 2017/5/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSHTTPManager.h"
#import "TimeConsuming.h"
#import <Reachability/Reachability.h>
#import "AppDelegate.h"

@implementation resultObject


@end

@implementation XSHTTPManager

+ (instancetype)sharedClient {
    
    static XSHTTPManager *appClient = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        config.timeoutIntervalForRequest = 15.0;
        appClient = [[XSHTTPManager alloc]initWithBaseURL:[NSURL URLWithString:ImageBaseUrl] sessionConfiguration:config];
        
        // 声明获取到的数据格式
        appClient.responseSerializer = [AFHTTPResponseSerializer serializer];
        appClient.requestSerializer = [AFHTTPRequestSerializer serializer];

        //        appClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        
        
    });
    
    return appClient;
    
}

+(void)checkNetStatus:(XSNetworkStatusBlock)block {
    // 1.获得网络监控的管理者
    AFNetworkReachabilityManager * reachManager = [AFNetworkReachabilityManager sharedManager];
    
    [reachManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        // 当网络状态改变了, 就会调用这个block
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown: // 未知网络
                [XSHTTPManager sharedClient].networkStats = XSNetworkStatusUnknown;
                block(XSNetworkStatusUnknown);
                break;
                
            case AFNetworkReachabilityStatusNotReachable: // 没有网络(断网)
                [XSHTTPManager sharedClient].networkStats = XSNetworkStatusNotReachable;
                block(XSNetworkStatusNotReachable);
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
                [XSHTTPManager sharedClient].networkStats = XSNetworkStatusReachableViaWWAN;
                block(XSNetworkStatusReachableViaWWAN);
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
                [XSHTTPManager sharedClient].networkStats = XSNetworkStatusReachableViaWiFi;
                block(XSNetworkStatusReachableViaWiFi);
                
                break;
        }
    }];
    
    [reachManager startMonitoring];
}

+(BOOL)isHaveNetwork {
    Reachability *conn = [Reachability reachabilityForInternetConnection];
    if ([conn currentReachabilityStatus] == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

static NSInteger onlyTime = 0;

+ (void)post:(NSString *)URLString parameters:(id)parameters
            success:(XSAPIClientSuccess)success
                failure:(XSAPIClientFailure)failure {
    AFHTTPSessionManager *manager = [XSHTTPManager sharedClient];
    NSDictionary *param =  [XSTool convertToParamFromParameters:parameters];
    onlyTime ++;
    NSString *timeIdentifier = [NSString stringWithFormat:@"URL_%ld_%@",(long)onlyTime,currentTime()];
    @autoreleasepool {
        [XSTool creatPlistWithExceptionReport:@"" key:timeIdentifier];
    }
    //打印请求
    NSString * tStr = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,URLString];
    [self printUrl:tStr parameters:(NSMutableDictionary *)param];
    
    [manager POST:URLString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //打印JSON数据
        [self printJson:responseObject path:URLString];
        
        @autoreleasepool {
            [TimeConsuming exceptionReport:task removeForKey:timeIdentifier];
        }
        
        NSHTTPURLResponse *responseTask = (NSHTTPURLResponse *)task.response;
        NSDictionary *headerFields = responseTask.allHeaderFields;
        
        NSString *web_status = headerFields[@"web_status"];
        if ([web_status integerValue] == 0) {
            // 系统维护
            [[NSNotificationCenter defaultCenter] postNotificationName:@"systemMaintenance" object:nil];
        }
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil] ;
        resultObject *res = [resultObject mj_objectWithKeyValues:dict];
        
        if (res && dict) {
            
            if ([res.info isEqualToString:@"无效用户"]) {
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:Localized(@"err_user_login_another_device") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
                }];
                [alert addAction:action1];
                AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
                return;
            }
            success(dict,res);
            
        } else {
            res = [resultObject new];
            res.status = NO;
            res.info = @"返回数据为空";
            success(nil,res);
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error.code != -1009 ||error.code != -1001) {
            [TimeConsuming upDataErrorReport:task description:error.description];
        }
        failure(error);
    }];
}
//打印网络请求
+(void)printUrl:(NSString *)Url parameters:(NSMutableDictionary *)parameters{
    
    NSArray * keysArray = [parameters allKeys];
    NSString * urlPerfix = [NSString stringWithFormat:@"%@?",Url];
    for (int i=0; i<keysArray.count; i++)
    {
        NSString * oneKey = [keysArray objectAtIndex:i];
        NSString * oneValue = [parameters objectForKey:oneKey];
        if (i == (keysArray.count - 1) ) {
            urlPerfix=[urlPerfix stringByAppendingFormat:@"%@=%@",oneKey,oneValue];
        }else{
            urlPerfix=[urlPerfix stringByAppendingFormat:@"%@=%@&",oneKey,oneValue];
        }
    }
    
    NSLog(@"\n -------↓↓↓↓-网络请求-↓↓↓↓---------%@----------URL---》 \n %@ \n 《----------↑↑↑↑-网络请求-↑↑↑↑---------------------------- \n",Url,urlPerfix);
}

//打印JSON数据
+(void)printJson:(id)responseData path:(NSString *)path

{
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //    NSLog(@"------%@-----\n%@\n------------",path,responseStr);
    NSLog(@"\n -------↓↓↓↓-Json-↓↓↓↓---------%@----------URL---》 \n %@ \n 《----------↑↑↑↑-Json-↑↑↑↑---------------------------- \n",path,responseStr);
    
}


+ (RACSignal *)rac_POSTURL:(NSString *)url params:(NSDictionary *)params {
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        NSString *uuu;
        if ([url hasPrefix:@"http"]) {
            uuu = url;
        }else if ([url hasPrefix:@"/api/v1"]) {
            uuu = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,url];
        } else {
            uuu = [NSString stringWithFormat:@"%@/api/v1/%@",JYSBaseURL,url];
        }
        [XSHTTPManager post:uuu parameters:params success:^(NSDictionary *dic, resultObject *state) {
            //            NSLog(@"url =%@ params = %@  state == %@",url,params.mj_keyValues,state.mj_keyValues);
            id value = dic[@"data"];
            resultObject *res;
            res = state;
            if ([value isKindOfClass:[NSDictionary class]]) {
               res = state;
            }else if ([value isKindOfClass:[NSArray class]]){
                res.data = @{@"data":value};
            }else{
                res.returnValue = [NSString stringWithFormat:@"%@",value];
            }
            
            
            [subscriber sendNext:res];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
            
        }];
        return  nil;
    }];
}

-(void)dealloc {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

@end
