//
//  XSApi.m
//  App3.0
//
//  Created by mac on 17/2/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSApi.h"
#import "NSString+MD5.h"

#import "TimeConsuming.h"
@implementation XSApi

static NSInteger oneIdentifier = 0;

+ (void)requestWithUrl:(NSString *)URLString
            parameters:(id)parameters
               success:(requestSuccessBlock)success
               failure:(requestFailureBlock)failure
{
    oneIdentifier ++;
    
    XSHTTPManager *manager = [XSHTTPManager sharedClient];
    NSString *timeIdentifier = [NSString stringWithFormat:@"URL_%ld_%@",(long)oneIdentifier,currentTime()];
    [XSTool creatPlistWithExceptionReport:@"" key:timeIdentifier];
    
    [manager POST:URLString parameters:[XSTool convertToParamFromParameters:parameters] progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [TimeConsuming exceptionReport:task removeForKey:timeIdentifier];
        });
        //隐藏进度条
        // 请求成功
        if(responseObject){
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            resultObject *res = [resultObject mj_objectWithKeyValues:dict];
            if (res && dict) {
                success(dict,YES);
            } else {
                success(dict,NO);
            }
            
            
            
        } else {
            success(@{@"msg":Localized(@"no_data")}, NO);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [TimeConsuming upDataErrorReport:task description:error.description];
        failure(error);
        
    }];
}
static NSInteger upData_Identifier = 0;
#pragma mark - 上传
+ (void)uploadWithUrlString:(nonnull NSString *)urlString andUpData:(nullable NSObject *) upData paramters:(nullable id)  parameters progress:(nullable  requestProgressBlock) progress success:(nullable requestSuccessBlock)success fail:(nullable requestFailureBlock)fail{
    upData_Identifier ++;
    
    NSString *PRIMARY_KEY = [NSString stringWithFormat:@"URL_%ld_%@",(long)upData_Identifier,currentTime()];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [XSTool creatPlistWithExceptionReport:@"" key:PRIMARY_KEY];
    });
    XSHTTPManager *manager = [XSHTTPManager sharedClient];
    [manager POST:urlString parameters:[XSTool convertToParamFromParameters:parameters] constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //判断文件类型
        if ([upData isKindOfClass:[UIImage class]]) {
            // 把图片转换成data
            NSData *data= UIImageJPEGRepresentation((UIImage *)upData, 1);
            [formData appendPartWithFileData:data name:@"file" fileName:@"123.jpg" mimeType:@"image/png"];
        }else{//视频
            [formData appendPartWithFileData:(NSData *)upData name:@"file" fileName:@"xs.mp4"mimeType:@"video/mp4"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        // 打印上传进度
        NSLog(@"%lf",1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [TimeConsuming exceptionReport:task removeForKey:PRIMARY_KEY];
        });
        //请求成功
        if(responseObject){
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            resultObject *res = [resultObject mj_objectWithKeyValues:dict];
            if (res && dict) {
                success(dict,YES);
            } else {
                success(dict,NO);
            }
        } else {
            success(@{@"msg":Localized(@"no_data")}, NO);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [TimeConsuming upDataErrorReport:task description:error.description];
        
        
        // 请求失败
        fail(error);
    }];
    
}

static NSInteger twoIdentifier = 0;

+ (void)uploadWithUrlString:(nonnull NSString *)urlString andUpImgs:(nullable NSArray *) upImgs paramters:(nullable id)  parameters progress:(nullable  requestProgressBlock) progress success:(nullable requestSuccessBlock)success fail:(nullable requestFailureBlock)fail{
    XSHTTPManager *manager = [XSHTTPManager sharedClient];
    NSString *PRIMARY_KEY = [NSString stringWithFormat:@"URL_%ld_%@",(long)twoIdentifier,currentTime()];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [XSTool creatPlistWithExceptionReport:@"" key:PRIMARY_KEY];
    });
    
    
    
    [manager POST:urlString parameters:[XSTool convertToParamFromParameters:parameters] constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (UIImage *image in upImgs) {
            // 把图片转换成data
            NSData *data = UIImageJPEGRepresentation(image, 1);
            // 拼接数据到请求题中
            [formData appendPartWithFileData:data name:@"file[]" fileName:@"123.jpg" mimeType:@"image/png"];
        }
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [TimeConsuming exceptionReport:task removeForKey:PRIMARY_KEY];
        });
        if(responseObject){
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            resultObject *res = [resultObject mj_objectWithKeyValues:dict];
            if (res && dict) {
                success(dict,YES);
            } else {
                success(dict,NO);
            }
            
        } else {
            success(@{@"msg":Localized(@"no_data")}, NO);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [TimeConsuming upDataErrorReport:task description:error.description];
        fail(error);
    }];
}









@end

#pragma mark - - - - - - - - - - -
#pragma mark - - - - - - - - - - -  HTTPManager

@implementation HTTPManager

//商城新增这接口 异步注册im 聊天
+ (void)CheckUserRegImSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Y_CheckUserRegImUrl parameters:nil success:success failure:fail];
}

//意见反馈
+ (void)feedbackAddWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:@"/api/v1/user/feedback/Add" parameters:param success:success failure:failure];
}


//18.获取我的团队信息
+(void)yun_getMyTreeWithPage:(NSString *)page
                     success:(XSAPIClientSuccess)success
                        fail:(XSAPIClientFailure)fail{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (!page) {
        page = @"0";
    }
    if (page.intValue != 0) {
        [param setObject:page forKey:@"page"];
    }
    [XSHTTPManager post:Yun_getMyTreeUrl parameters:param success:success failure:fail];
}


//19.获取奖金记录
+(void)yun_getAwardInfoWithType:(NSString *)type
                           page:(NSString *)page
                        account:(NSString *)account
                        success:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (type.length >=1) {
        [param setObject:type forKey:@"type"];
    }
    if (account) {
        [param setObject:account forKey:@"coin"];
    }
    if (!page) {
        page = @"1";
    }
    [param setObject:page forKey:@"page"];
    [XSHTTPManager post:Yun_getAwardInfoUrl parameters:param success:success failure:fail];
}

//47.谷歌验证页面
+(void)yun_googleVerifyIndexSuccess:(XSAPIClientSuccess)success
                               fail:(XSAPIClientFailure)fail{
    NSDictionary *param = nil;
    [XSHTTPManager post:Yun_googleVerifyIndexUrl parameters:param success:success failure:fail];
}
//48.谷歌验证
+(void)yun_googleVerifyWithSecret:(NSString *)secret
                       old_verify:(NSString *)old_verify
                           verify:(NSString *)verify
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail{
    
    NSMutableDictionary *param = [@{
                                    @"secret":secret,
                                    @"verify":verify
                                    } mutableCopy];
    if (old_verify) {
        [param setValue:old_verify forKey:@"old_verify"];
    }
    [XSHTTPManager post:Yun_googleVerifyUrl parameters:param success:success failure:fail];
}


+ (void)getShopConfigWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:@"/api/v1/basic/config/GetShopConfig" parameters:nil success:success failure:fail];
}

/* - - - - - - 线下商家- - - - - - -*/


//首页顶部轮播图广告(v1offline)
+ (void)OffHomeGetTopGgsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    
    [XSHTTPManager post:OffLine_HomeGetTopGgsUrl parameters:nil success:success failure:fail];
}
//首页广告位(v1offline)
+ (void)OffGetMiddleGgsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    
    [XSHTTPManager post:OffLine_GetMiddleGgsUrl parameters:nil success:success failure:fail];
}
//线下商品：为您推荐（猜你喜欢）的商品(v1offline)
/*
 city    必须    string    市名
 user_position    必须    array    当前地图定位信息
 |--longitude        string    经度
 |--latitude        string    纬度
 */
+ (void)OffGuessYouLikeListsWithPage:(NSString *)page
                                city:(NSString *)city
                       user_position:(NSDictionary *)user_position
                             success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:user_position
                                                       options:kNilOptions
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    NSDictionary *param = @{@"page":page,
                            @"city":city,
                            @"user_position":jsonString
                            };
    [XSHTTPManager post:OffLine_GuessYouLikeListsUrl parameters:param success:success failure:fail];
}
//线下商家:获取全部行业分类(v1offline) supply/industry/Lists
+ (void)OffSupplyIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    
    [XSHTTPManager post:OffLine_industryListsUrl parameters:nil success:success failure:fail];
}

//线下商家：获取某行业下所有子分类(v1offline)
+ (void)OffIndustrySubListsWithIndustry:(NSString *)industry
                                success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    if (!industry) {
        industry = @"";
    }
    NSDictionary *param = @{@"industry":industry};
    [XSHTTPManager post:OffLine_industrySubListsUrl parameters:param success:success failure:fail];
}

//二级界面====>

//行业首页顶部轮播图广告(v1offline)
+ (void)OffindustryGetTopGgsWithIndustry:(NSString *)industry
                             success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    if (!industry) {
        industry = @"";
    }
    NSDictionary *param = @{@"industry":industry};
    [XSHTTPManager post:OffLine_IndustryGetTopGgsUrl parameters:param success:success failure:fail];
}

//获取店铺列表筛选条件(v1offline)
+ (void)OffGetFileterConditionsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:OffLine_FileterConditionsUrl parameters:nil success:success failure:fail];
}

//店铺列表(v1offline)筛选 type
+ (void)OffSupplyListsWithPage:(NSString *)page
                          type:(int)isTj
                          city:(NSString *)city
                      industry:(NSString *)industry
                       keyword:(NSString *)keyword
                        filter:(NSDictionary *)filter
                 user_position:(NSDictionary *)user_position
                       success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSError *error = nil;
    NSData *positionData = [NSJSONSerialization dataWithJSONObject:user_position
                                                           options:kNilOptions
                                                             error:&error];
    NSString *positionStr = [[NSString alloc] initWithData:positionData
                                                  encoding:NSUTF8StringEncoding];
    
    
    NSData *filterData = [NSJSONSerialization dataWithJSONObject:filter
                                                         options:kNilOptions
                                                           error:&error];
    NSString *filterStr = [[NSString alloc] initWithData:filterData
                                                encoding:NSUTF8StringEncoding];
    if (!industry) {
        industry = @"";
    }
    NSMutableDictionary *param = [@{@"page":page,@"city":city,@"user_position":positionStr} mutableCopy];
    
    if (isTj == 1) {
        [param setValue:@(isTj) forKey:@"is_tj"];        
    }
    
    if (!isEmptyString(industry)) {
        [param setValue:industry forKey:@"industry"];
    }else{
        if (keyword.length>0) {
            [param setValue:keyword forKey:@"keyword"];
        }
    }
    if (filter) {
        [param setValue:filterStr forKey:@"filter"];
    }
    
    [XSHTTPManager post:OffLine_SupplyListsUrl parameters:param success:success failure:fail];
}

//店铺列表(v1offline)筛选
+ (void)OffSupplyListsWithPage:(NSString *)page
                          city:(NSString *)city
                      industry:(NSString *)industry
                       keyword:(NSString *)keyword
                        filter:(NSDictionary *)filter
                 user_position:(NSDictionary *)user_position
                       success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSError *error = nil;
    NSData *positionData = [NSJSONSerialization dataWithJSONObject:user_position
                                                           options:kNilOptions
                                                             error:&error];
    NSString *positionStr = [[NSString alloc] initWithData:positionData
                                                  encoding:NSUTF8StringEncoding];
    
    
    NSData *filterData = [NSJSONSerialization dataWithJSONObject:filter
                                                         options:kNilOptions
                                                           error:&error];
    NSString *filterStr = [[NSString alloc] initWithData:filterData
                                                encoding:NSUTF8StringEncoding];
    if (!industry) {
        industry = @"";
    }
    NSMutableDictionary *param = [@{@"page":page,@"city":city,@"user_position":positionStr} mutableCopy];
    
    if (!isEmptyString(industry)) {
        [param setValue:industry forKey:@"industry"];
    }else{
        if (keyword.length>0) {
            [param setValue:keyword forKey:@"keyword"];
        }
    }
    if (filter) {
        [param setValue:filterStr forKey:@"filter"];
    }
        
    [XSHTTPManager post:OffLine_SupplyListsUrl parameters:param success:success failure:fail];
}


//获取某城市下的区县
+ (void)OffGetCountyByCityWithcity_code:(NSString *)city_code
                                  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
   
    NSDictionary *param = @{@"city_code":city_code};
    [XSHTTPManager post:OffLine_GetCountyByCityUrl parameters:param success:success failure:fail];
}

//获取某区县下的街道
+ (void)OffGetTownByCountyWithCounty_code:(NSString *)county_code
                                success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"county_code":county_code};
    [XSHTTPManager post:OffLine_GetTownByCountyUrl parameters:param success:success failure:fail];
}


/* - - - - - - - - - - - - -*/
//获取验证设置
+(void)GetWithdrawVerifySuccess:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:GetWithdrawVerifyUrl parameters:nil success:success failure:fail];
}

/*
 设置验证
 type    'mobile','email','google'
 verify  验证码
 */
+ (void)setWithdrawVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:SetWithdrawVerifyUrl parameters:paraDic success:success failure:fail];
}



+ (void)getSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{

    NSDictionary *param = @{@"mobile":mobile};

    [XSHTTPManager post:GetSmsVerifyUrl parameters:param success:success failure:fail];
}
+ (void)doLoginin:(NSString *)name
         password:(NSString *)password
          success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"name":name,@"passwd":password};
    [XSHTTPManager post:LoginUrl parameters:param success:success failure:fail];
}


+ (void)doLoginin:(NSString *)name
         password:(NSString *)password
            email:(NSString *)email
           mobile:(NSString *)mobile
           google:(NSString *)google
          success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSMutableDictionary *param = [@{@"name":name,@"passwd":password} mutableCopy];
    
    if (!isEmptyString(email)) {
        [param setObject:email forKey:@"email"];
    }
    if (!isEmptyString(mobile)) {
        [param setObject:mobile forKey:@"mobile"];
    }
    if (!isEmptyString(google)) {
        [param setObject:google forKey:@"google"];
    }
    [XSHTTPManager post:LoginUrl parameters:param success:success failure:fail];
}



+ (void)getSmsVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:GetSmsVerifyUrl parameters:paraDic success:success failure:fail];
}

+ (void)getEmailVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:GetEmailVerifyUrl parameters:paraDic success:success failure:fail];
}

+ (void)checkSmsVerifyValidWithMobile:(NSString *)mobile smsVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"mobile":mobile,@"verify":verify};
    [XSHTTPManager post:CheckSmsVerifyValidUrl parameters:param success:success failure:fail];
}

+ (void)loginBySms:(NSString *)name verify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"name":name,@"verify":verify};
    [XSHTTPManager post:LoginBySmsUrl parameters:param success:success failure:fail];
}

+ (void)checkMobileExist:(NSString *)mobile Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"mobile":mobile};
    [XSHTTPManager post:Url_CheckMobileExist parameters:param success:success failure:fail];
}

+ (void)resetLoginPwdWithMobile:(NSString *)mobile pwdType:(NSString *)pwdType smsVerify:(NSString *)smsVerify andNewPwd:(NSString *)newPwd success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"mobile":mobile,@"mode":@(ResetPwdByMobile),@"type":pwdType,@"verify":smsVerify,@"password":newPwd};
    [XSHTTPManager post:ResetLoginPwdUrl parameters:param success:success failure:fail];
}

+(void)LoginOutWithName:(NSString *)name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    NSDictionary *param = @{@"name":name};
    [XSHTTPManager post:LoginOutUrl parameters:param success:success failure:fail];
}

+ (void)setupMobile:(NSString *)mobile verify:(NSString *)verify token:(NSString *)token success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"mobile":mobile,@"token":token,@"verify":verify};
    
    [XSHTTPManager post:ChangeMobile parameters:param success:success failure:failure];
    
}
+ (void)verifyPassWordWithPassword:(NSString *) password type:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"oldPwd":password,@"type":type};
    
    [XSHTTPManager post:VerifyPassword parameters:param success:success failure:failure];
}

+ (void)mobileVerifyWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_MobileVerify parameters:nil success:success failure:failure];
}

+ (void)mobileValidateWithVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_MobileValidate parameters:@{@"verify":verify} success:success failure:failure];
}

+ (void)autoLoginWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    
    [XSHTTPManager post:AutoLoginUrl parameters:nil success:success failure:fail];
}

+ (void)searchUserWithString:(NSString *)string success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"search_item":string};
    [XSHTTPManager post:Url_New_UserSearch parameters:param success:success failure:fail];
}

+ (void)registerWithIntro:(NSString *)intro nickName:(NSString *)nickname mobile:(NSString *)mobile verify:(NSString *)verify password:(NSString *)pwd success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"intro":intro,@"nickname":nickname,@"mobile":mobile,@"verify":verify,@"passwd_one":pwd};
    [XSHTTPManager post:RegisterAppUrl parameters:param success:success failure:fail];
}

+ (void)registerWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:RegisterUrl parameters:params success:success failure:fail];
}

+ (void)regGetSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"mobile":mobile, @"type":@"reg"};
    [XSHTTPManager post:GetSmsVerifyUrl parameters:param success:success failure:fail];
}

+ (void)getRegisterInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GetRegisterInfo parameters:nil success:success failure:fail];
}

+ (void)getCartListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:GetCartListUrl parameters:nil success:success failure:fail];
}

+ (void)editCartWithPeid:(NSString *)peid productNumber:(NSInteger)pNum newPeid:(NSString *)nPeid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":peid,@"num":@(pNum),@"new_peid":nPeid};
    [XSHTTPManager post:EditCartUrl parameters:param success:success failure:fail];
}

+ (void)deleteCartWithProductId:(NSString *)pId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":pId};
    [XSHTTPManager post:DeleteCartUrl parameters:param success:success failure:fail];
}

+(void)addGoodsToCartWithgoodsId:(NSString *)goodsId goodsNum:(NSString *)goodsNum extId:(NSString *)extId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"pid":goodsId,@"num":goodsNum,@"ext_id":extId};
    [XSHTTPManager post:Url_CartAdd parameters:param success:success failure:fail];
}

+ (void) getProductListWithOrder:(NSDictionary *)params Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetProductList parameters:params success:success failure:fail];
}

+ (void)getAgreementWithType:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure{
    [XSHTTPManager post:AgreementUrl parameters:@{@"type":type} success:success failure:failure];
    
}


// 行业列表
+(void)getIndustryListsParams:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_Industry_Lists parameters:params success:success failure:failure];
}


+ (void)getSupply_ListsParams:(SupplyModel  *)model Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_Supply_Lists parameters:model.mj_keyValues success:success failure:failure];
}


+ (void)getAreaWithLevelTWoSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_Area_leve parameters:nil success:success failure:failure];
    
}


+ (void)getAreaChildList:(NSString *)city_ID Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_area_Child parameters:@{@"code":city_ID,
                                                    @"level":@"3"} success:success failure:failure];
}


//http://shangcheng.xsy.dsceshi.cn/api/v1/supply/supply/GetSupplyIndex?uid=57494

+(void)getStoreInformation:(int)store_ID Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    
    //    http://shangcheng.xsy.dsceshi.cn
    //    http://shangcheng.xsy.dsceshi.cn/supply/supply/GetSupplyIndex
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_supply_GetSupplyIndex parameters:@{@"uid":[NSString stringWithFormat:@"%d",store_ID]} success:success failure:failure];
    
    
}

//http://shangcheng.xsy.dsceshi.cn/api/v1/basic/advert/Lists/cid/1PC幻灯片|2PC公告下|3PC楼层|4PC底部|5手机幻灯片|6手机左|7手机右上|8手机右下左|9手机右下右|10手机店铺列表页上/limit/2/category_id/0
+(void)basicadvertListsSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:@"/api/v1/basic/atvert/Lists" parameters:@{@"flag_str":@"mobile_supply_list_top"} success:success failure:failure];
}


+(void)setFavorite:(NSString *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_user_favorite parameters:@{@"favType":@"1",
                                                       @"favId":params} success:success failure:failure];
}
//http://shangcheng.xsy.dsceshi.cn/api/v1/supply/scategory/Edit/id/1

//http://shangcheng.xsy.dsceshi.cn/api/v1/supply/scategory/Lists/id/1
/**
 获取某个商家分类信息
 */
+ (void)scategoryEdit:(NSString *)storeId page:(NSNumber *)page Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_supplys_category parameters:@{@"id":storeId,@"page":page} success:success failure:failure];
}


//http://shangcheng.xsy.dsceshi.cn/api/v1/basic/area/GetHotArea
+ (void)getHotAreaSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_GetHotArea parameters:@{@"id":@"1"} success:success failure:failure];
}

//http://shangcheng.xsy.dsceshi.cn/api/v1/buyer/quickorder/CreateQuickOrder
+(void) quickorderCreateQuickOrder:(NSDictionary *)dic Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_CreateQuickOrder parameters:dic success:success failure:failure];
    
}

//share/GetShareInfo
+ (void)getShareInfo:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Url_GetShareInfo parameters:params success:success failure:failure];
}





/*  收藏 - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - -*/

+ (void)addCollectionInfoWithFavType:(NSString *)favType favId:(NSString *)favId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/favorite/SetFavorite/";
    NSDictionary *param = @{
                            @"favType":favType,
                            @"favId":favId
                            };
    
    [XSHTTPManager post:path parameters:param success:success failure:failure];
    
    
}

+ (void)getCollectionInfoWithFavType:(NSString *)favTypeId andPage:(NSString *)page andLimit:(NSString *) limit type:(nonnull NSString *)type success:(nonnull XSAPIClientSuccess)success failure:(nonnull XSAPIClientFailure)failure {
    NSLog(@" %s ",__FUNCTION__);
    NSDictionary *param = @{
                            @"favType":favTypeId,
                            @"page":page,
                            @"limit":limit
                            };
    NSString *path = @"/api/v1offline/user/favorite/GetFavoriteList";
    if ([type isEqualToString:@"online"]) {
        path = GetCollectionInfoUrl;
    }
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

+ (void)deleteCollectionInfoIsOffline:(BOOL)isOffline WithId:(NSString * )product_id success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSLog(@" %s ",__FUNCTION__);
    NSDictionary *param = @{
                            @"favId":product_id
                            };
    if (isOffline) {
        [XSHTTPManager post:Offline_deleteCollectGoods parameters:param success:success failure:failure];
    }else{
        [XSHTTPManager post:DeleteCollectionUrl parameters:param success:success failure:failure];
    }
    
    
}

/*  历史记录 - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - -*/
+ (void)getHistoryListsWithPage:(NSString *)page andLimit:(NSString *)limit success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *historyListUrl = @"/api/v1/user/history/GetHistoryList/";
    NSDictionary *param = @{
                            @"page" :page,
                            @"limit":limit
                            };
    [XSHTTPManager post:historyListUrl parameters:param success:success failure:failure];
}

+ (void)deleteHistoryInfoWithIdsString:(NSString *)ids success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *deleteHistories = @"/api/v1/user/history/DelHistory/";
    
    //id字符串-逗号分隔
    NSDictionary *param = @{
                            @"ids" :ids
                            
                            };
    [XSHTTPManager post:deleteHistories parameters:param success:success failure:failure];
}

/*  实名认证 - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - -*/
+ (void)upLoadUserInfoWithIconImage:(UIImage *)iconImage progress:(requestProgressBlock)progress success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"type":@"avatar",
                            @"formname":@"file"
                            };
    [XSHTTPManager post:UploadIconUrl parameters:param success:success failure:failure];
}

+ (void) submitIdentityCarInfoWithImageString:(NSString *)imageString trueName:(NSString *) trueName card:(NSString *)card Success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    //上传完整信息
    NSDictionary *param = @{
                            @"img_card": imageString,
                            @"truename": trueName,
                            @"card_no": card,
                            };
    [XSHTTPManager post:VertifyUrl parameters:param success:success failure:failure];
}

+ (void)submitIdentityCarInfoWithImageString:(NSString *)imageString trueName:(NSString *)trueName cardType:(NSString *)card_id card:(NSString *)card Success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    //上传完整信息
    NSDictionary *param = @{
                            @"img_card": imageString,
                            @"truename": trueName,
                            @"card_type": card_id,
                            @"card_no": card,
                            };
    [XSHTTPManager post:VertifyUrl parameters:param success:success failure:failure];
}

/**获取实名认证信息*/
+ (void)getIdentifyInfoWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/approve/GetApproveInfo/";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/*  订单 - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - - - - - - -  - - - - - - - - -  - - - - -*/
//获取订单各个状态统计数量
static NSString *OrderStatusCount = @"/api/v1/buyer/order/GetStatusCount/";

//获取买家订单列表
static NSString *OrderListUrl = @"/api/v1/buyer/order/GetOrderList/";

//取消订单
static NSString *OrderCloseUrl = @"/api/v1/buyer/order/CloseOrder/";

//退货
static NSString *OrderExitUrl = @"/api/v1/buyer/order/Reject/";

//收货
static NSString *OrderGetGoods = @"/api/v1/buyer/order/Receipt/";
/**获取某规格的商品信息--拓展信息*/
+ (void)getProductExtForPreviewOrderWithId:(NSString *)goodsId spec:(NSString *)spec success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/basic/product_ext/Info/";
    NSDictionary *param = @{
                            @"id":goodsId,
                            @"spec":spec
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**获取用户可用的抵扣券*/
+ (void)getValidDeductionCouponListWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/deductioncoupon/GetValidDeductionCouponList/";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**订单确认*/
+ (void)previewOrderWithParams:(NSDictionary *)param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/Preview/";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**创建订单*/
+ (void)createOrderWithOptionIds:(NSString *)opIds num:(NSString *)num addressId:(NSString *)addressId logtype:(NSDictionary *)logtype description:(NSDictionary *) description privateId:(NSString *)privateId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/Create";
    NSDictionary *param = @{
                            @"op_ids":opIds,
                            @"num":num,
                            @"address_id":addressId,
                            @"logtype":[XSTool convertToJsonData:logtype],
                            @"desc":[XSTool convertToJsonData:description],
                            @"is_private":privateId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

+ (void)createOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/Create";
    [XSHTTPManager post:path parameters:params success:success failure:failure];
}

/**可用的配送站*/
+ (void)orderGetValidDeliveryWithModel:(NSDictionary *)param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/GetValidDelivery/";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**获取订单统计数量*/
+ (void)getOrderStatusCountSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:OrderStatusCount parameters:nil success:success failure:failure];
}

/**再次购买*/
+ (void)orderBuyAgainWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/BuyAgain/";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}
/**获取全部订单列表--status , 分割*/
+ (void)getOrderInfoWithOrderStatus:(NSString *)orderStatus andPage:(NSString *)page andLimit:(NSString *)limit success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"page":page,
                            @"limit":limit,
                            @"orderStatus":orderStatus
                            };
    [XSHTTPManager post:OrderListUrl parameters:param success:success failure:failure];
}

/**关闭订单*/
+ (void)closeOrderWithOrderId:(NSString *)orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":orderId
                            };
    [XSHTTPManager post:OrderCloseUrl parameters:param success:success failure:failure];
    
}

/**获取订单详情*/
+ (void)getDetailOrderInfoWithId:(NSString *)orderId supply:(NSString *)supply success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *Url_GetOrderDetailInfo = @"/api/v1/buyer/order/View/";
    NSDictionary *param = @{
                            @"id":orderId,
                            @"supply":supply
                            };
    [XSHTTPManager post:Url_GetOrderDetailInfo parameters:param success:success failure:failure];
}

/**退货前*/
+ (void)getRejectOrderInfoWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/RejectAdd/";
    NSDictionary *param = @{
                            @"id":orderId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**退货*/
+ (void)cancelOrderGoodsWithId:(NSString *) orderId ReasonId:(NSString *) reasonId moneyType:(NSString *)moneyType money:(NSString *)money remark:(NSString *) remark success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":orderId,
                            @"reason": reasonId,
                            moneyType: money,
                            @"remark":remark
                            };
    [XSHTTPManager post:OrderExitUrl parameters:param success:success failure:failure];
}

/**收货*/
+ (void)receiveGoodsWithId:(NSString *) orderId andPayPwd:(NSString *) payPwd success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":orderId,
                            @"pay_pwd": payPwd
                            
                            };
    [XSHTTPManager post:OrderGetGoods parameters:param success:success failure:failure];
}

/**退款*/
+ (void)refoundMoneyWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/Refund/";
    NSDictionary *param = @{
                            @"id":orderId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**提醒发货*/
+ (void)orderRemindOrderSendWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/RemindOrderSend";
    NSDictionary *param = @{
                            @"id":orderId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
};

/**单个 追加评价*/
+ (void)evaluateZhuiEvaluationWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/evaluate/ZhuiEvaluation";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**多个 追加评价*/
+ (void)evaluateZhuiEvaluationsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/evaluate/ZhuiEvaluations";
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**获取支付信息*/
+ (void)getPayInfoWithIds:(NSString *)orderId tname:(NSString *)tname success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/cashier/GetPrePayConf/";
    NSDictionary *param = @{
                            @"ids":orderId,
                            @"tname":tname
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**支付*/
+(void)payMoneyWithPayname:(NSString *)payname payids:(NSString *)payids otherInfo:(NSDictionary *)otherInfo success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/cashier/Pay/";
    [otherInfo setValue:payname forKey:@"payname"];
    [otherInfo setValue:payids forKey:@"payids"];
    NSLog(@"======%@",otherInfo);
    [XSHTTPManager post:path parameters:otherInfo success:success failure:failure];
    
}

/**快速买单准备*/
+(void)getCashierInfoWithPayname:(NSString *)payname payids:(NSString *)payids success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/cashier/Prepare";
    NSDictionary *param = @{
                            @"payname":payname,
                            @"payids":payids
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
    
}

/**查看支付结果*/
+ (void)lookPayResultStatusWithTradeNumber:(NSString *)tradeNumber success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/buyer/order/GetPayStatus";
    NSDictionary *param = @{@"pay_no":tradeNumber};
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}



#pragma mark - AreaModel
/**获取省级地域*/
+ (void)getProvinceInfoWithId:(NSString *)idNum level:(NSString *) level success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"code":idNum,@"level":level};
    [XSHTTPManager post:GetAreaUrl parameters:param success:success failure:failure];
}

/**获取完整的所选的地域名称*/
+ (void)getFullAreaInfoWithId:(NSString * )idNum success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"id":idNum};
    [XSHTTPManager post:SelectedEndAreaUrl parameters:param success:success failure:failure];
}

#pragma mark - BankCardModel
static NSString *BankTypeUrl = @"/api/v1/user/bank/GetBankConf/";
static NSString *AddBankCardUrl = @"/api/v1/user/bank/Insert/";
static NSString *GetBankCardInfoUrl = @"/api/v1/user/bank/Lists";
static NSString *DefaultBankUrl = @"/api/v1/user/bank/SetDefault/";
static NSString *DeleteBankUrl = @"/api/v1/user/bank/Delete/";
/**获取银行卡类型*/
+ (void)getBankTypeSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:BankTypeUrl parameters:nil success:success failure:failure];
}

/**添加银行卡*/
+ (void)addBankCardWithBankName:(NSString *)bankName bankId:(NSString *)bankId bankCard:(NSString *)bankCard bankAddress:(NSString *)bankAddress success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"bankname":bankName,
                            @"bank_id":bankId,
                            @"bankcard":bankCard,
                            @"bankaddress":bankAddress
                            };
    [XSHTTPManager post:AddBankCardUrl parameters:param success:success failure:failure];
}

/**添加银行卡*/
+ (void)addBankCardWithBankName:(NSString *)bankName bankId:(NSString *)bankId bankCard:(NSString *)bankCard bankAddress:(NSString *)bankAddress passWord:(NSString *)password success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"bankname":bankName,
                            @"bank_id":bankId,
                            @"bankcard":bankCard,
                            @"bankaddress":bankAddress,
                            @"paypwd":password,
                            @"picture":@""
                            };
    [XSHTTPManager post:AddBankCardUrl parameters:param success:success failure:failure];
}

/**获取绑定的银行卡*/
+ (void)getBindBankCardInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:GetBankCardInfoUrl parameters:nil success:success failure:failure];
    
}

/**编辑银行卡*/
+ (void)updateBankCardWithBankName:(NSString *)bankName bankId: (NSString *) bankId bankCard: (NSString *) bankCard bankAddress: (NSString *) bankAddress userId: (NSString *) userId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"bankname":bankName,
                            @"bank_id":bankId,
                            @"bankcard":bankCard,
                            @"bankaddress":bankAddress,
                            @"id": userId
                            };
    [XSHTTPManager post:AddBankCardUrl parameters:param success:success failure:failure];
}

/**编辑银行卡*/
+ (void)updateBankCardWithBankName:(NSString *)bankName bankId: (NSString *) bankId bankCard: (NSString *) bankCard bankAddress: (NSString *) bankAddress userId: (NSString *) userId passWord:(NSString *)pwd success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"bankname":bankName,
                            @"bank_id":bankId,
                            @"bankcard":bankCard,
                            @"bankaddress":bankAddress,
                            @"id": userId,
                            @"paypwd":pwd,
                            @"picture":@""
                            };
    [XSHTTPManager post:AddBankCardUrl parameters:param success:success failure:failure];
}

/**设为默认*/
+ (void)defaultBankCardWithBankId:(NSString *)bankId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":bankId
                            };
    [XSHTTPManager post:DefaultBankUrl parameters:param success:success failure:failure];
}

/**删除*/
+ (void)deleteBankCardWithBankId:(NSString *)bankId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":bankId
                            };
    [XSHTTPManager post:DeleteBankUrl parameters:param success:success failure:failure];
}


/**添加评价*/
+(void)addEvaluteUrlWithOrderId:(NSString *) orderId eva:(NSArray *) eva success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *Url_AddEvalution = @"/api/v1/buyer/evaluate/AddEvaluation";
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:eva
                                                       options:kNilOptions
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    NSDictionary *param = @{
                            @"id":orderId,
                            @"eva":jsonString
                            };
    [XSHTTPManager post:Url_AddEvalution parameters:param success:success failure:failure];
}

/**获取物流信息*/
+(void)getLogisticsInfoWithId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *Url_Logistic = @"/api/v1/buyer/order/Express/";
    NSDictionary *param = @{
                            @"id": orderId
                            };
    [XSHTTPManager post:Url_Logistic parameters:param success:success failure:failure];
}


/**添加收货地址*/
+ (void)addAddressWithProvince:(NSString *)provinceId city:(NSString *)cityId county:(NSString *)countyId town:(NSString *)townId provinceName:(NSString *)provinceName cityName:(NSString *)cityName countyName:(NSString *)countyName townName:(NSString *)townName address:(NSString *)address contact:(NSString *)contact tel:(NSString *)tel isDefault:(NSString *)isDefault success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSDictionary *param = @{
                            @"is_default": isDefault,
                            @"province_code":provinceId,
                            @"city_code":cityId,
                            @"county_code":countyId,
                            @"town_code":townId,
                            @"province":provinceName,
                            @"city":cityName,
                            @"county":countyName,
                            @"town":townName,
                            @"address":address,
                            @"contact":contact,
                            @"tel":tel
                            };
    [XSHTTPManager post:AddAddressInfoUrl parameters:param success:success failure:failure];
    
}

/**更新收货地址*/
+ (void)updateAddressWithId:(NSString *)addressId province:(NSString *)provinceId city:(NSString *)cityId county:(NSString *)countyId town:(NSString *)townId provinceName:(NSString *)provinceName cityName:(NSString *)cityName countyName:(NSString *)countyName townName:(NSString *)townName address:(NSString *)address contact:(NSString *)contact tel:(NSString *)tel isDefault:(NSString *)isDefault success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSDictionary *param = @{
                            @"id":addressId,
                            @"province_code":provinceId,
                            @"city_code":cityId,
                            @"county_code":countyId,
                            @"town_code":townId,
                            @"province":provinceName,
                            @"city":cityName,
                            @"county":countyName,
                            @"town":townName,
                            @"address":address,
                            @"contact":contact,
                            @"tel":tel,
                            @"is_default":isDefault
                            };
    [XSHTTPManager post:UpdateAddressUrl parameters:param success:success failure:failure];
    
}

/**获取收货地址*/
+ (void)getAddressListsSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:GetAddressInfoUrl parameters:nil success:success failure:failure];
}

+ (void)getAppointAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/address/Info/";
    NSDictionary *param = @{
                            @"id":addressId
                            };
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**设置默认收货地址*/
+ (void)setDefaultAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":addressId
                            };
    [XSHTTPManager post:SetDefaultInfoUrl parameters:param success:success failure:failure];
}

/**删除收货地址*/
+ (void)deleteAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{
                            @"id":addressId
                            };
    [XSHTTPManager post:DeleteInfoUrl parameters:param success:success failure:failure];
}

/**获取店铺信息*/
+(void)getShopCarInfoWithFavType:(NSString *) favTypeId page:(NSString *) page limit:(NSString *) limit type:(NSString *) type success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    
    NSDictionary *param = @{
                            @"favType":favTypeId,
                            @"page":page,
                            @"limit":limit
                            };
    NSString *path = @"/api/v1offline/user/favorite/GetFavoriteList/";
    if ([type isEqualToString:@"online"]) {
        path = @"/api/v1/user/favorite/GetFavoriteList/";
    }
    [XSHTTPManager post:path parameters:param success:success failure:failure];
    
}
/**删除店铺收藏*/
+(void)deleteShopCarInfoWithFavId:(NSString *) favId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *DeleteShopCarUrl = @"/api/v1/user/favorite/Delete/favType/1";
    NSDictionary *param = @{
                            @"favId":favId
                            
                            };
    [XSHTTPManager post:DeleteShopCarUrl parameters:param success:success failure:failure];
}

#pragma mark - UserModel
/**获取用户信息*/
+(void)getUserInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:GetUserUrl parameters:nil success:success failure:failure];
}

/**设置,更新用户信息*/
+(void)setUserInfoWithNickName:(NSString *) nickName sex:(NSString *) sex success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    // [XSHTTPManager post:DeleteShopCarUrl parameters:param success:success failure:failure];
}

/**根据编号获取用户信息*/
+(void)getOtherInfoWithUserName:(NSString *)userName success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetOtherInfo parameters:@{@"username":userName} success:success failure:failure];
}

/**设置,更新用户信息*/
+(void)setUserInformationWithNickName:(NSString *)nickName success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"nickname":nickName};
    [XSHTTPManager post:SetPersonInfo parameters:param success:success failure:failure];
}

+(void)setUserInformationWithSex:(NSString *)sex success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"sex":sex};
    [XSHTTPManager post:SetPersonInfo parameters:param success:success failure:failure];
    
}

+(void)setUserInformationWithLogo:(NSString *)avatar success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"avatar":avatar};
    [XSHTTPManager post:SetPersonInfo parameters:param success:success failure:failure];
}

/**上传头像*/
+(void)upLoadUserInformationWithIconImage:(UIImage *)iconImage progress:(requestProgressBlock)progress success:(nonnull requestSuccessBlock)success failure:(nonnull requestFailureBlock)failure {
    //    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BaseUrl,UploadIconUrl];
    NSDictionary *param = @{
                            @"type":@"avatar",
                            @"formname":@"file"
                            };
    //    [XSHTTPManager post:UploadIconUrl parameters:param success:success failure:failure];
    [XSApi uploadWithUrlString:UploadIconUrl andUpData:iconImage paramters:param progress:progress success:success fail:failure];
}



/**获取分类名称*/
+(void)getCategoryNameSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *Url_GetCategoryNameUrl = @"/api/v1/basic/category/Lists/category_id/0";
    [XSHTTPManager post:Url_GetCategoryNameUrl parameters:nil success:success failure:failure];
}

/**获取分类子类目*/
+(void)getCategoryWith:(NSString *) category_id success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString * Url_GetCategoryUrl = @"/api/v1/basic/category/Lists/";
    NSDictionary *param = @{
                            @"category_id":category_id
                            };
    
    [XSHTTPManager post:Url_GetCategoryUrl parameters:param success:success failure:failure];
}

/**获取分类的基本商品信息*/
+(void)getCategoryGoodsWithPage:(NSString *) page limit:(NSString *) limit cid:(NSString *) cid stype:(NSString *) stype success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *Url_GetBaseProduct = @"/api/v1/basic/product/Lists/ext/supply/";
    NSDictionary *param = @{
                            @"page":page,
                            @"limit":limit,
                            @"cid":cid,
                            @"stype":stype
                            };
    
    [XSHTTPManager post:Url_GetBaseProduct parameters:param success:success failure:failure];
}


+(void)getSocialLoginListSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/login/GetSocialBindList";
    [XSHTTPManager post:path parameters:nil success:success failure:failure];
}

/**取消第三方绑定*/
+(void)cancelSocialBindWithType:(NSString *) type success:(nonnull XSAPIClientSuccess)success failure:(nonnull XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/login/UnbindUser/";
    NSDictionary *param = @{@"type":type};
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}

/**获取绑定信息*/
+ (void)getBindUserOrSocialInformationWithType:(NSString *) type code:(NSString *) code openid:(NSString *)openid success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure {
    NSString *path = @"/api/v1/user/login/GetBindOrSocialInfo/";
    NSLog(@"--%@--",path);
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:type forKey:@"type"];
    if (code) {
        [dictionary setObject:code forKey:@"code"];
    }
    if (openid) {
        [dictionary setObject:openid forKey:@"openid"];
    }
    [XSHTTPManager post:path parameters:dictionary success:success failure:failure];
}

/**绑定用户*/
+ (void)bindUserBySocialWithParam:(NSDictionary *)param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSString *path = @"/api/v1/user/login/BindUserBySocial/";
   
    [XSHTTPManager post:path parameters:param success:success failure:failure];
}



/*---------------------------------------------------联系人群组-begin-----------------------------------------------*/
#pragma mark -- 群组联系人
+ (void)getRelationListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"relation":@(0)};
    [XSHTTPManager post:Url_New_MyRelation parameters:param success:success failure:fail];
}

+ (void)getFriendsListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_MyFriend parameters:nil success:success failure:fail];
}

+ (void)getAttentionListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_MyFollow parameters:nil success:success failure:fail];
}

+ (void)getFansListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_MyFans parameters:nil success:success failure:fail];
}

+ (void)getMyGroupListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_MyGroup parameters:nil success:success failure:fail];
}

/*
 获取我的黑名单列表
 */
+ (void)getMyBlackListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_ShieldList parameters:nil success:success failure:fail];
}

/*
 我的聊天设置
 */
+ (void)getMyChatSetWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_ChatUser parameters:nil success:success failure:fail];
}

/*
 设置我的聊天
 */
+ (void)setMyChatSetWithParams:(NSDictionary *)dic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_ChatUser parameters:dic success:success failure:fail];
}

+ (void)getUserInfoWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"uid":uid?uid:@""};
    [XSHTTPManager post:Url_New_UserFetch parameters:param success:success failure:fail];
}

+ (void)setUserRemarkNameWithUid:(NSString *)uid remarkName:(NSString *)remark success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":uid,@"remark":remark};
    [XSHTTPManager post:Url_New_GroupMemberRemark parameters:param success:success failure:fail];
}

+ (void)setContact:(NSString *)uid isShield:(NSInteger)is_shield isNotDisturb:(NSInteger)is_not_disturb success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSMutableDictionary *param = [@{@"id":uid} mutableCopy];
    if (is_shield != -1) {
        [param setObject:@(is_shield) forKey:@"is_shield"];
    }
    if (is_not_disturb != -1) {
        [param setObject:@(is_not_disturb) forKey:@"is_not_disturb"];
    }
    [XSHTTPManager post:Url_New_Shield parameters:param success:success failure:fail];
}

+ (void)attentionFansWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":uid,@"type":@(1)};
    [XSHTTPManager post:Url_New_Follow parameters:param success:success failure:fail];
}

+ (void)attentionFansCancelWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":uid,@"type":@(0)};
    [XSHTTPManager post:Url_New_Follow parameters:param success:success failure:fail];
}

+ (void)addGroup:(NSDictionary *)groupData success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_GroupAdd parameters:groupData success:success failure:fail];
}

+ (void)generateAvatarForGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId};
    [XSHTTPManager post:Url_New_GroupAvatar parameters:param success:success failure:fail];
}

+ (void)getMemberListWithIds:(NSString *)ids ofGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId,@"id_str":ids};
    [XSHTTPManager post:Url_New_GroupMemberList parameters:param success:success failure:fail];
}

+ (void)deleteGroupWithGroupId:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId};
    [XSHTTPManager post:Url_New_GroupDelete parameters:param success:success failure:fail];
}

+ (void)addMembers:(NSString *)id_str forGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId,@"id_str":id_str};
    [XSHTTPManager post:Url_New_GroupMemberAdd parameters:param success:success failure:fail];
}

+ (void)deleteMembers:(NSString *)id_str forGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId,@"id_str":id_str};
    [XSHTTPManager post:Url_New_GroupMemberDelete parameters:param success:success failure:fail];
}

+ (void)fetchGroupInfoWithGroupId:(NSString *)gId getMembers:(int)is_get_member order:(NSString *)order success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId,@"is_get_member":@(is_get_member),@"order":order};
    [XSHTTPManager post:Url_New_GroupInfo parameters:param success:success failure:fail];
}

+ (void)CheckUserInGroupWithGroupId:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gId};
    [XSHTTPManager post:Url_New_CheckUserInGroup parameters:param success:success failure:fail];
}

+ (void)setShield:(NSInteger)is_shield notDisturb:(NSInteger)is_not_disturb forGroup:(NSString *)gid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSMutableDictionary *param = [@{@"id":gid} mutableCopy];
    if (is_shield != -1) {
        [param setObject:@(is_shield) forKey:@"is_shield"];
    }
    if (is_not_disturb != -1) {
        [param setObject:@(is_not_disturb) forKey:@"is_not_disturb"];
    }
    [XSHTTPManager post:Url_New_GroupSet parameters:param success:success failure:fail];
}

+ (void)getNickNameWithIds:(NSString *)ids success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"ids":ids};
    [XSHTTPManager post:GetNicknameUrl parameters:param success:success failure:fail];
}

+ (void)setMemberMuteWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    [XSHTTPManager post:Url_New_GroupMemberMute parameters:params success:success failure:fail];
}

+ (void)getMemberMuteListWithGroupId:(NSString *)gid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSDictionary *param = @{@"id":gid};
    [XSHTTPManager post:Url_New_GroupMemberMuteLists parameters:param success:success failure:fail];
}

+ (void)updateGroupInfoWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    //    NSDictionary *param = @{@"id":data.gid,@"name":data.name,@"avatar":data.avatar,@"desc":data.desc};
    [XSHTTPManager post:Url_New_GroupUpdate parameters:params success:success failure:fail];
}

+ (void)getDeviceFansWithContents:(NSArray *)list success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:list
                                                       options:kNilOptions
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    NSLog(@"--------------%@",jsonString);
    NSDictionary *param = @{@"list":jsonString};
    [XSHTTPManager post:Url_New_Find parameters:param success:success failure:fail];
}


/*---------------------------------------------------联系人群组-end------------------------------------------------*/

/*---------------------------------------------------粉丝圈-begin------------------------------------------------*/
#pragma mark-获取粉丝列表
+(void)getFansWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetFansUrl parameters:nil success:success failure:fail];
}
#pragma mark-获取新的动态列表
+(void)getNewStatusArrayWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:StatusListUrl parameters:nil success:success failure:fail];
}
#pragma mark- 获取之前的动态列表
+(void)getOldStatusArrayWithPageNum:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:StatusListUrl parameters:@{@"page":page} success:success failure:fail];
}
#pragma mark-获取新的动态列表
+(void)getNewStatusArrayWithUID:(NSString *)userName Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:StatusListUrl parameters:@{@"uid":@([userName integerValue])} success:success failure:fail];
}
#pragma mark- 获取之前的动态列表
+(void)getOldStatusArrayWithUID:(NSString *)userName PageNum:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:StatusListUrl parameters:@{@"page":page,@"uid":@([userName integerValue])} success:success failure:fail];
}
#pragma mark-点赞
+(void)thumbUpWithStatusId:(NSNumber *)idNum  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:ThumbUpUrl parameters: @{@"id":idNum} success:success failure:fail];
}
#pragma mark-取消点赞
+(void)cancelThumbUpWithStatusId:(NSNumber *)idNum WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CancelThumbUpUrl parameters: @{@"id":idNum} success:success failure:fail];
}
#pragma mark-删除动态
+(void)deleteStatusWithStatusId:(NSNumber *)idNum WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:DeleteStatusUrl parameters: @{@"id":idNum} success:success failure:fail];
}
#pragma mark-评论
+(void)commentWithId:(NSNumber *)idNum andContent:(NSString *)content  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CommentOrReplyUrl parameters: @{@"id":idNum,@"content":content} success:success failure:fail];
}
#pragma mark-回复
+(void)replyWithId:(NSNumber *)replyIdNum andContent:(NSString *)content  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CommentOrReplyUrl parameters: @{@"reply_id":replyIdNum,@"content":content} success:success failure:fail];
}
#pragma mark-删除评论
+(void)deleteCommentWithId:(NSNumber *)replyIdNum WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:DeleteCommentUrl parameters: @{@"id":replyIdNum} success:success failure:fail];
}
#pragma mark-修改用户背景图片
+(void)updateUserFancircleBgImgWithImgStr:(NSString *)imgStr WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:UpdateUserFancircleBgImg parameters:  @{@"logo_fandom_bg":imgStr} success:success failure:fail];
}
//粉丝圈只发表文字
+(void)addStatusOnlyWordWithDic:(NSDictionary *)paradic  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:ADDSTATUSURL parameters: paradic success:success failure:fail];
}
//粉丝圈多文件上传   多张图片
+(void)upLoadPhotosWithDic:(NSDictionary *)paraDic andDataArray:(NSArray *)dataArray   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@",UPLOADPHOTOS];
    [XSApi  uploadWithUrlString:urlStr andUpImgs:dataArray paramters:paraDic progress:nil success:success fail:fail];
}
//单文件上传 图片或者视频
+(void)upLoadDataIsVideo:(BOOL)isVideo WithDic:(NSDictionary *)paraDic andData:(NSObject *)data   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail{
    NSString *urlStr;
    if(isVideo){
        urlStr=VideoUpLoadUrl;
    }else{
        urlStr=SingleFileUpLoadUrl;
    }
    [XSApi uploadWithUrlString:urlStr andUpData:data paramters:paraDic progress:nil success:success fail:fail];
}
/*
 'avatar' => 5*1024*1024,
 //店铺
 'logo' => 1*1024*1024,
 'background' => 5*1024*1024, //背景图
 'banner_pc' => 5*1024*1024, //店铺首页幻灯片
 'banner_wap' => 5*1024*1024, //店铺首页幻灯片
 //认证
 'idcard' => 5*1024*1024, //身份证
 'usercard' => 5*1024*1024, //其他证件
 'license' => 5*1024*1024, //营业执照
 'orglicense' => 5*1024*1024, //组织机构代码证
 //银行卡
 'bankcard' => 5*1024*1024,
 //商品
 'product' => 5*1024*1024, //商品图
 //文章
 'article' => 5*1024*1024, //文章图
 //相册
 'album' => 5*1024*1024, //相册图片
 //广告
 'gug_supply' => 5*1024*1024, //店铺广告
 //众筹
 'collect' => 1*1024*1024, //标题
 'collect_detail' => 5*1024*1024, //详情
 'collect_level' => 1*1024*1024, //档位
 'collect_progress' => 5*1024*1024, //进度
 //秒杀
 'secondbuy' => 1*1024*1024, //标题
 //拼团
 'groupbuy' => 1*1024*1024, //标题
 //物流单
 'logistics_sheet' => 500*1024, //标题
 //退款
 'reject' => 5*1024*1024, //退款退货
 //评价
 'evaluation' => 5*1024*1024, //买家评价图片
 //意见反馈
 'feedback' => 5*1024*1024, //意见反馈
 // 商家入驻
 'approvesupply' => 5*1024*1024, //商家入驻
*/
+(void)JYSupLoadDataIsVideo:(BOOL)isVideo WithDic:(NSDictionary *)paraDic andData:(NSObject *)data   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail{
    NSString *urlStr;
    if(isVideo){
        urlStr=VideoUpLoadUrl;
    }else{
        urlStr=SingleFileUpLoadUrl;
    }
    [XSApi uploadWithUrlString:urlStr andUpData:data paramters:paraDic progress:nil success:success fail:fail];
}
/*---------------------------------------------------粉丝圈-end------------------------------------------------*/
/*---------------------------------------------------钱包begin------------------------------------------------*/
#pragma mark- 获取钱包首界面信息
+(void)getWalletInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWalletInfoUrl parameters:nil success:success failure:fail];
}
//获取钱包现金、购物券等详情记录
+(void)getAccountDetailWithParamDic:(NSDictionary *)paraDic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWalletAccountDetailUrl parameters:paraDic success:success failure:fail];
}
//获取转账规则
+(void)getTransferRulesWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetTransferRule parameters:nil success:success failure:fail];
}
//提交转账
+(void)commitTransAccountWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CommitTransAccount parameters:dic success:success failure:fail];
}
//获取转账记录
+(void)getNewTransferAccountRecordWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetTransferAccountRecord parameters:@{@"page":@1} success:success failure:fail];
}
//获取旧的转账记录
+(void)getOldTransferAccountRecordWithPage:(NSNumber *)page Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetTransferAccountRecord parameters:@{@"page":page} success:success failure:fail];
}
//获取提现钱包
+(void)getWithDrawWalletWithType:(NSString *)type Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWithDrawWallet parameters:@{@"type":type} success:success failure:fail];
}
//获取提现规则
+(void)getWithDrawRuleWithWalletType:(NSString *)walletType  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWithDrawRule parameters:@{@"wallet_type":walletType} success:success failure:fail];
}
//提交提现
+(void)commitWithDrawWithParamDic:(NSDictionary *)dic  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CommitWithDraw parameters:dic success:success failure:fail];
}
//撤回提现
+(void)cancelWithDrawWithId:(NSString *)recordId  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:CancelWithDraw parameters:@{@"id":recordId} success:success failure:fail];
}
//新的提现记录
+(void)getNewWithDrawRecordWithWalletType:(NSString *)walletType WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWithDrawRecord parameters:@{@"page":@1,@"wallet_type":walletType} success:success failure:fail];
}
//旧的提现记录
+(void)getOldWithDrawRecordWithWalletType:(NSString *)walletType WithPage:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWithDrawRecord parameters:@{@"page":page,@"wallet_type":walletType}  success:success failure:fail];
}
//充值记录
+(void)getRechargeRecordWithDic:(NSDictionary *)paraDic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
      [XSHTTPManager post:GetRechargeRecord parameters:paraDic success:success failure:fail];
}
//获取钱包记录交易类型
+(void)getWalletRecordTradeTypeWithWalletType:(NSString *)walletType Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:GetWalletRecordTradeType parameters:@{@"wallet_type":walletType}  success:success failure:fail];
}
//充值规则
+(void)getRechargeRule:(XSAPIClientSuccess)success failure:(requestFailureBlock)failure {
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Recharge parameters:@{@"type":@"app"} success:success failure:failure];
}
// 充值提交
+(void)rechargeSubmissionParams:(NSDictionary *)params succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:Recharge_Submission parameters:params success:success failure:failure];
}
// 充值记录
+ (void)rechargeListSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure{
    NSLog(@" %s ",__FUNCTION__);
    [XSHTTPManager post:recharge_Lists parameters:nil success:success failure:failure];
}
/*---------------------------------------------------钱包end------------------------------------------------*/
/*---------------------------------------------------商城begin------------------------------------------------*/
//获取商城首页滚动式图信息
+(void) getMallScrollViewADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"flag_str":@"mobile_index_slide"} success:success failure:failure];
}
//获取商城首页滚动式图下的菜单
+(void)getMallMenuInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallMenuList parameters:@{@"limit":@(16)} success:success failure:failure];
}
//获取公告列表信息
+(void)getMallNoticeInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallNoticeList parameters:nil success:success failure:failure];
}

/**
 获取公告列表信息
 */
+(void)getMallNoticeInfoWithPage:(NSInteger)page limit:(NSInteger)limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    NSDictionary *param = @{@"page":@(page),@"limit":@(limit)};
    [XSHTTPManager post:Url_GetMallNoticeList parameters:param success:success failure:failure];
}

//公告详情
+(void)getNoticeDetailInfoWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetNoticeDetail parameters:paraDic success:success failure:fail];
}

//商品图文详情
+(void)getGoodsDetailPictureInfoWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetGoodsDetailPictureInfo parameters:paraDic success:success failure:fail];
}

//获取左边广告
+(void)getMallLeftADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"cid":@6} success:success failure:failure];
}
// 获取右上方广告
+(void)getMallRightTopADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"cid":@7} success:success failure:failure];
}

+(void)getMobile_All_InfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"flag_str":@"mobile_index_left_up,mobile_index_right_up,mobile_index_left_down,mobile_index_right_down"} success:success failure:failure];
}
//获取右右下方广告
+(void) getMallRightBottomRightADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure {
    [XSHTTPManager post:Url_GetMallADInfo parameters:@{@"cid":@9} success:success failure:failure];
}
//获取商城首页分类信息（服装服饰、家居用品等）
+(void) getMallCategoryInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GetMallCategoryInfo parameters:@{@"num_category":@5,@"num_product":@5} success:success failure:fail];
}
//获取商城首页猜你喜欢商品列表信息
+(void)getMallGussYouLikeGoodsListInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetMallGoodsListInfo parameters:@{@"page":@1,@"recommend":@1,@"_order":@"rand"} success:success failure:fail];
}
//获取更多商城首页猜你喜欢商品列表信息
+(void)getMallMoreGussYouLikeGoodsListInfoWithPage:(NSNumber *)page Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetMallGoodsListInfo parameters:@{@"page":page,@"recommend":@1,@"_order":@"rand"} success:success failure:fail];
}
//获取商品详情
+(void)getGoodsDetailWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GetGoodsDetailInfo parameters:params success:success failure:fail];
}
//获取商品详情推荐商品
+(void)getGoodsDetailRecommendGoodsWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
     [XSHTTPManager post:Url_GetGoodsDetailRecommedGoods parameters:paraDic success:success failure:fail];
}
//根据参数字典获取商品列表
+(void)getADGoodsListWithParaDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetMallGoodsListInfo parameters:paraDic success:success failure:fail];
}
//获取品牌列表
+(void)getBrandListParaDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetBrandListInfo parameters:paraDic success:success failure:fail];
}
//根据规格获取商品信息
+(void)getGoodsInfoWithGoodsID:(NSString *)goodsID spec:(NSString *)specStr  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    NSDictionary *paraDic;
    if (specStr.length) {
        paraDic=@{@"id":goodsID,@"spec":specStr};
    }else{
        paraDic=@{@"id":goodsID};
    }
    [XSHTTPManager post:Url_GetGoodsInfoWithSpec parameters:paraDic success:success failure:fail];
}
+(void)getGoodsEvaluateWithParaDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetGoodsEvaluate parameters:paraDic success:success failure:fail];
}
//获取商品的商家统计信息
+(void)getGoodsStoreStatisticsInfoWithStoreId:(NSString *)storeId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetGoodsStoreStatisticsInfo parameters:@{@"supply_id":storeId} success:success failure:fail];
}
////获取批发商品不同规格的数据
//+(void)getWholeGoodsSpecInfoWithProduct_id:(NSString *)pId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
//    [XSHTTPManager post:Url_GetWholeGoodsSpecInfo parameters:@{@"product_id":pId} success:success failure:fail];
//}

+ (void)getMallFloorInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_GetMallFloorInfo parameters:nil success:success failure:fail];
}
/*---------------------------------------------------商城end------------------------------------------------*/




/**
 商品二维码  获取 商品 ID
 */
+ (void) getCommodityProduct:(NSString *)str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    
    [XSHTTPManager post:Url_commodityPoduct parameters:@{@"barcode":str} success:success failure:fail];
}

// 技能
/**技能列表*/
+ (void)getSkillListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillLists parameters:nil success:success failure:fail];
}
/**新增技能*/
+ (void)addSkills:(NSString *)skill_str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSDictionary *param = @{@"skill_name_str":skill_str};
    [XSHTTPManager post:Url_SkillAdd parameters:param success:success failure:fail];
}
/**我擅长的技能列表*/
+ (void)getMySkillListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillMy parameters:nil success:success failure:fail];
}
/**设置我擅长的技能*/
+ (void)setMySkills:(NSString *)skill_str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    NSDictionary *param = @{@"skill_str":skill_str};
    [XSHTTPManager post:Url_SkillSetMy parameters:param success:success failure:fail];
}
/**技能交换列表*/
+ (void)getSkillExchangeListWithParams:(NSDictionary *)params WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeLists parameters:params success:success failure:fail];
}

/**发布技能交换*/
+ (void)addSkillExchangeWithData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeAdd parameters:data success:success failure:fail];
}
/** 删除技能交换*/
+ (void)deleteSkillExchangeWithId:(NSString *)se_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeDelete parameters:@{@"skill_exchange_id":se_id} success:success failure:fail];
}

/**点赞*/
+ (void)thumbupForSkillExchange:(NSString *)skill_exchange_id flag:(int)flag success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeThumbup parameters:@{@"skill_exchange_id":skill_exchange_id,@"flag":@(flag)} success:success failure:fail];
}

/**评论*/
+ (void)commentForSkillExchangeWithCommentData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeComment parameters:data success:success failure:fail];
}
/**删除评论*/
+ (void)deleteCommentForSkillExchangeWithCommentId:(NSString *)comment_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeComment parameters:@{@"flag":@(0),@"comment_id":comment_id} success:success failure:fail];
}

/**回复*/
+ (void)replyForSkillExchangeWithReplyData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeReply parameters:data success:success failure:fail];
}
/**删除回复*/
+ (void)deleteReplyForSkillExchangeWithReplyId:(NSString *)reply_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_SkillExchangeReply parameters:@{@"flag":@(0),@"reply_id":reply_id} success:success failure:fail];
}

/***************************************************************************找回 重置密码begin************************************************************************************/
#pragma mark- 修改支付密码
+(void)resetPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:InstallResetPayPwdUrl parameters:dic success:success failure:fail];
}
#pragma mark- 修改登录密码
+(void)resetLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:InstallResetLoginPwdUrl parameters:dic success:success failure:fail];
}
//找回支付密码
+(void)findPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:InstallFindPayPwdUrl parameters:dic success:success failure:fail];
}
//找回登录密码
+(void)findLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:InstallFindLoginPwdUrl parameters:dic success:success failure:fail];
}
/***************************************************************************找回 重置密码end************************************************************************************/

/***************************************************************************版本更新begin************************************************************************************/
+(void)getVersionInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail{
    [XSHTTPManager post:Url_GetVersionInfo parameters:@{@"type":@"ios"} success:success failure:fail];
}
/***************************************************************************版本更新end************************************************************************************/

+ (void)getCustomerServicesWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail {
    [XSHTTPManager post:Url_MyService parameters:nil success:success failure:fail];
}
@end


