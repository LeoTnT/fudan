//
//  ErrorReportingController.m
//  oooo
//
//  Created by apple on 2017/8/3.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ErrorReportingController.h"
#import "ZYQAssetPickerController.h"
#import "TimeConsuming.h"
@interface ErrorReportingController ()<UITextViewDelegate,UINavigationControllerDelegate, ZYQAssetPickerControllerDelegate,UIImagePickerControllerDelegate>
@property(nonatomic,strong)UIImagePickerController *imagePickVC;

@property(nonatomic,strong)NSMutableArray *photosArray;

@property (nonatomic ,strong) UILabel *photoNumber1;

@property (nonatomic ,strong)TakePhotoView *takePhotoButton;

@property (nonatomic ,strong)UITextView *textView1;
@end

@implementation ErrorReportingController
{
    
    UILabel *textNumer;


}

-(NSMutableArray *)photosArray{
    if (!_photosArray) {
        _photosArray=[NSMutableArray array];
    }
    return _photosArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"bug_submit");
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UILabel *label1 = [UILabel new];
    label1.text = @"问题和意见";
    
    [self.view addSubview:label1];
    
    _textView1 = [UITextView new];
    _textView1.text = Localized(@"bug_submit_content_sugg");
    _textView1.textColor = [UIColor grayColor];
    _textView1.delegate = self;
    _textView1.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:_textView1];
    CGFloat space = 10;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(space/2);
        make.right.mas_equalTo(self.view).offset(-space/2);
        make.top.mas_equalTo(self.view).offset(80);
    }];
    
    [_textView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(label1);
        make.top.mas_equalTo(label1.mas_bottom).offset(space);
        make.height.mas_equalTo(90);
    }];
    
    
    UIView *back1 = [UIView new];
    back1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:back1];
    
    
    UILabel *textLength = [UILabel new];
    textLength.text = @"/240";
    textLength.textColor = [UIColor grayColor];
    [back1 addSubview:textLength];
    textNumer = [UILabel new];
    textNumer.text = @"0";
    [textLength addSubview:textNumer];

    [back1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(_textView1);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(_textView1.mas_bottom);
    }];
    
    textLength.font = textNumer.font = [UIFont systemFontOfSize:13];
    
    [textLength mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(back1);
        make.right.mas_equalTo(back1.mas_right).offset(-space);
    }];
    
    
    [textNumer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(textLength.mas_left);
        make.top.mas_equalTo(textLength);
    }];
    
    
    
    _takePhotoButton = [TakePhotoView new];
    [self.view addSubview:_takePhotoButton];
    [_takePhotoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(back1.mas_bottom).offset(3*space);
        make.left.right.mas_equalTo(_textView1);
        make.height.mas_equalTo(150);
    }];
    
    
    UILabel *label2 = [UILabel new];
    label2.text = Localized(@"bug_submit_img");
    [_takePhotoButton addSubview:label2];
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_takePhotoButton).offset(space);
        make.top.mas_equalTo(_takePhotoButton).offset(space);
    }];
    
    
    UILabel *photoNumber = [UILabel new];
    photoNumber.text = @"/4";
    textLength.textColor = [UIColor grayColor];
    [_takePhotoButton addSubview:photoNumber];
    [photoNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label2);
        make.right.mas_equalTo(_takePhotoButton).offset(-space);
    }];
    
    
    self.photoNumber1 = [UILabel new];
    self.photoNumber1.text = @"0";
    
    [self.takePhotoButton addSubview:self.photoNumber1];
    [self.photoNumber1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(photoNumber.mas_left);
        make.top.mas_equalTo(photoNumber);
    }];
    self.photoNumber1.font = photoNumber.font = [UIFont systemFontOfSize:13];
    
    
    @weakify(self);
    [self.takePhotoButton setIsTakePhoto:^{
        @strongify(self);
        [self.textView1 resignFirstResponder];
        [self choosePhotos];
        
    }];
    
    [self.takePhotoButton setGetImageNumber:^(NSInteger number){
        @strongify(self);
//        [self.photosArray removeObjectAtIndex:number];
        self.photoNumber1.text = [NSString stringWithFormat:@"%ld",(long)number];
    }];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btn setTitle:Localized(@"bug_submit_do") forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 60, 40);
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(tijiaoAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    
    
}



- (void) tijiaoAction:(UIButton *)sender {
    if (isEmptyString(_textView1.text) || _textView1.text.length <10 || [_textView1.text isEqualToString:Localized(@"bug_submit_content_sugg")]) {

        [MBProgressHUD showMessage:Localized(@"bug_submit_content_sugg") view:self.view];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableArray *arr = [NSMutableArray array];
    
    [self.photosArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSData *data = UIImageJPEGRepresentation(self.photosArray[idx], .2);
        [arr addObject:data];
    }];
    
    XSBenchmark(^{
    [TimeConsuming  feedBackWithName:@"测试" content:_textView1.text data:arr type:FeedBackServiceImage];
    }, ^(double ms) {
       
        [MBProgressHUD showMessage:Localized(@"user_approve_success") view:self.view hideTime:2 doSomeThing:^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
    
}


-(void)choosePhotos{
    [UIAlertController showAlertCntrollerWithViewController:self alertControllerStyle:UIAlertControllerStyleAlert title:@"选择照片" message:nil CallBackBlock:^(NSInteger btnIndex) {
        if (btnIndex == 1) {
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
            picker.maximumNumberOfSelection = 4;
            picker.assetsFilter = ZYQAssetsFilterAllAssets;
            picker.showEmptyGroups=NO;
            picker.delegate=self;
            [self presentViewController:picker animated:YES completion:nil];
        }else if(btnIndex == 2){
            [self takeAPhoto];
        }
    } cancelButtonTitle:Localized(@"cancel_btn") destructiveButtonTitle:nil otherButtonTitles:Localized(@"从相册中选择"), Localized(@"take_picture"), nil];

}


#pragma mark-拍照
-(void)takeAPhoto{
    //拍照模式是否可用
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==AVAuthorizationStatusRestricted ||authStatus ==AVAuthorizationStatusDenied) {
        // 无权限 引导去开启
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    self.imagePickVC=[[UIImagePickerController alloc] init];
    //数据源
    self.imagePickVC.sourceType=UIImagePickerControllerSourceTypeCamera;
    //展示拍照控板
    self.imagePickVC.showsCameraControls=YES;
    //摄像头捕获模式
    self.imagePickVC.cameraCaptureMode=UIImagePickerControllerCameraCaptureModePhoto;
    //后置摄像头
    self.imagePickVC.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    self.imagePickVC.delegate=self;
    [self presentViewController:self.imagePickVC animated:YES completion:^{
    }];
}

#pragma mark-拍照完毕
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //获取原始照片
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    //保存图片到相册
//    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [self.photosArray insertObject:image atIndex:self.photosArray.count];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    self.takePhotoButton.imageData = self.photosArray;
    self.photoNumber1.text = [NSString stringWithFormat:@"%ld",self.photosArray.count];
    
}


#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    for (int i=0; i<assets.count; i++) {
        ZYQAsset *asset=assets[i];
        @weakify(self);
        [asset setGetFullScreenImage:^(UIImage * result) {
            @strongify(self);
            [self.photosArray insertObject:result atIndex:self.photosArray.count];
            self.takePhotoButton.imageData = self.photosArray;
            self.photoNumber1.text = [NSString stringWithFormat:@"%ld",self.photosArray.count];
        }];
    }
}
-(void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker{
    [MBProgressHUD showMessage:@"一次最多选择四张图片" view:picker.view ];
}


#pragma mark-取消
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.markedTextRange == nil) {
        NSLog(@"text:%lu", (unsigned long)textView.text.length);
        textNumer.text = [NSString stringWithFormat:@"%ld",textView.text.length];
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    textView.text = nil;
    _textView1.textColor = [UIColor blackColor];
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        _textView1.text = Localized(@"bug_submit_content_sugg");
        _textView1.textColor = [UIColor grayColor];
        
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSInteger existedLength = textView.text.length;
    NSInteger selectedLength = range.length;
    NSInteger replaceLength = text.length;
    NSInteger pointLength = existedLength - selectedLength + replaceLength;
    
    
    NSLog(@"pointLength  = %ld",(long)pointLength);
    
    //超过16位 就不能在输入了
    if (pointLength > 239) {
        
        return NO;
    }else{
        return YES;
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [_textView1 resignFirstResponder];
    
}


@end


@interface TakePhotoView ()

@property (nonatomic ,strong)UIButton *btn;

@end

@implementation TakePhotoView

{

    UIButton *lastImage;
    
    NSMutableArray *imageDataSource;
    CGFloat space;
    CGSize imageViewSize,buttonSize;
}

- (instancetype)init {
    if (self  = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        [self creatView];
    }
    return self;
}


- (void) creatView {
    
    
    space = 10;
    _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    imageViewSize = CGSizeMake(90, 90);
    buttonSize = CGSizeMake(70, 70);
    [_btn setBackgroundImage:[UIImage imageNamed:@"user_fans_addphoto_thin"] forState:UIControlStateNormal];

    [_btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_btn];

}



- (void)updateConstraints {
    [_btn mas_remakeConstraints:^(MASConstraintMaker *make) {
        lastImage  ? make.left.mas_equalTo(lastImage.mas_right).offset(space):make.left.mas_equalTo(self.mas_left).offset(space);
        make.bottom.mas_equalTo(self).offset(-space);
        make.size.mas_equalTo(buttonSize);
    }];
    
    [super updateConstraints];
    
}



- (void) buttonAction:(UIButton *)sender {
 
    if (_isTakePhoto) {
        self.isTakePhoto();
    }

}

- (void)setImageData:(NSMutableArray *)imageData {
    _imageData = imageData;
 
    if (imageData.count >4) return;
    
    if (imageData.count == 4) _btn.hidden = YES;
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[TakePhotoCancel class]]) {
            [view removeFromSuperview];
            lastImage = nil;
            
        }
            

    }
    
    for (NSInteger x= 0; x < imageData.count; x ++) {
        
        TakePhotoCancel *imageView = [TakePhotoCancel buttonWithType:UIButtonTypeCustom];
        imageView.imageView1.image = imageData[x];
        
        [self addSubview:imageView];
        imageView.index = x;
        [imageView setButtonCancel:^(NSInteger sender){
            
            [self buttonRemove:sender];
            
        }];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            lastImage?  make.left.mas_equalTo(lastImage.mas_right):make.left.mas_equalTo(self);
            make.centerY.mas_equalTo(_btn);
            make.size.mas_equalTo(imageViewSize);
        }];
        lastImage = imageView ;
    }
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}



- (void) buttonRemove:(NSInteger )sender {
    _btn.hidden = NO;
    [_imageData removeObjectAtIndex:sender];
    
    if (self.getImageNumber) {
        self.getImageNumber(_imageData.count);
    }
    [self setImageData:_imageData];
    
    

}

@end



@implementation TakePhotoCancel

-(void)setIndex:(NSInteger)index
{
    _index = index;
    self.btn.tag = index;
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self  = [super initWithFrame:frame]) {
        
        [self creatView];
    }
    return self;
}

- (void) creatView {
    
    _imageView1 = [UIImageView new];
    _imageView1.backgroundColor = [UIColor whiteColor];
    [self addSubview:_imageView1];
    self.btn.hidden =NO;
}

-(UIButton *)btn
{
    UIButton * _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn setImage:[UIImage imageNamed:@"user_fans_delete-1"] forState:UIControlStateNormal];
    //    _btn.tag = self.tag;
    [_btn addTarget:self action:@selector(buttonRemove:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_btn];
    
    
    [_imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_imageView1.mas_right);
        make.centerY.mas_equalTo(_imageView1.mas_top);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    return _btn;
    
}


- (void) buttonRemove:(UIButton *)sender {

    if (self.ButtonCancel) {
        self.ButtonCancel(sender.tag);
    }
}

@end
