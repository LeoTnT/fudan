//
//  TimeConsuming.h
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface XSErrorReport : NSObject

@property (nonatomic ,copy)NSString *project_mark;


/**
 0-响应慢，1-出错，2-重定向
 */
@property (nonatomic ,copy)NSString *problem;


/**
 设备识别号
 */
@property (nonatomic ,copy)NSString *identify_num;


/**
 设备型号
 */
@property (nonatomic ,copy)NSString *machine_type;

@property (nonatomic ,copy)NSString *model;


/**
 方法
 */
@property (nonatomic ,copy)NSString *method;


/**
 时间戳  ，//异常发生时间
 */
@property (nonatomic ,copy)NSString *occurr_time;


/**
 耗时（毫秒）
 */
@property (nonatomic ,copy)NSString *use_time;


/**
 状态码
 */
@property (nonatomic ,copy)NSString *http_status;


/**
 接口地址
 */
@property (nonatomic ,copy)NSString *interface_url;


/**
 接口参数
 */
@property (nonatomic ,copy)NSString *interface_param;


+ (BOOL) saveRootExpetion:(XSErrorReport *)model;

@end

@interface TimeConsuming : NSObject
//NS_ASSUME_NONNULL_BEGIN

+ (void) xmlReport:(NSData *)data ;

/**
 接口异常收集
 */
+ (void)exceptionReport:(NSURLSessionDataTask * )task removeForKey:(NSString *)key;


/**
 错误 上报
*/
+ (void)upDataErrorReport:(NSURLSessionDataTask *  )task description:(NSString *)desc;

/**
 崩溃异常上报
 ·
 @param exception 崩溃问题
 */
+ (void)cashReport:(NSString *)exception;


/**
 问题反馈
 
 @param name 反馈姓名
 @param contentString 反馈内容
 @param data 图片 或者XML
 @param type 上传类型
 */
+(void)feedBackWithName:(NSString *)name content:(NSString *)contentString data:(NSArray *)data type:(FeedBackServiceType)type;

//NS_ASSUME_NONNULL_END

@end
