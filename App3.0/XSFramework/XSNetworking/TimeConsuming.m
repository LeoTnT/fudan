//
//  TimeConsuming.m
//  App3.0
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "TimeConsuming.h"
#import "GDataXMLNode.h"
@implementation TimeConsuming

/**
 上传耗时
 
 */
+ (void)exceptionReport:(NSURLSessionDataTask * _Nonnull )task removeForKey:(NSString *)key{
    
    // 耗时暂时不上传了
    return;
    
    double endTime =  [currentTime() doubleValue];
    NSMutableDictionary *mudic = [XSTool getContentsOfFile];
    
    NSString *responOBJ = [NSString stringWithFormat:@"%@",task.currentRequest.URL];
    NSString *onlyKey =  key;
    // 删除 错误 异常 列表
    if (mudic.allKeys.count > 30) {
        [mudic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [XSTool deleteExceptionReportForKey:mudic.allKeys[idx]];
        }];
    }
    NSDictionary *resDic = mudic[onlyKey];
    double startTime = [resDic[@"time"] doubleValue];
    if (startTime == 0) return;
    
    
    double delta_T = endTime - startTime;
    if (delta_T < 1000) {
        return;
    }
    NSString *time = [NSString stringWithFormat:@"%.0f",delta_T];

    
//    [TimeConsuming exceptionReport:responOBJ response:[NSString stringWithFormat:@"%@",task.response] occurr_time:time method:@"" problem:@"0" removeForKey:key];
    
    NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
//    NSLog(@"%ld",(long)responses.statusCode);
    XSErrorReport *model = [XSErrorReport new];
    model.problem = @"0";
    NSString *baseUrl = BaseUrl;
    model.model = responOBJ.length >baseUrl.length ? [responOBJ substringFromIndex:baseUrl.length]:@"user";
    model.use_time = time;
    model.http_status = [NSString stringWithFormat:@"%ld",(long)responses.statusCode];
    model.interface_url = responOBJ;
    [XSErrorReport saveRootExpetion:model];
    
    NSLog(@" * * * * * ** delta_T  =%ld sta =%ld en = %ld",(long)delta_T,(long)endTime,(long)startTime);
//1500016873433 .930908
}



/**
 error
 */
+ (void)upDataErrorReport:(NSURLSessionDataTask * _Nonnull )task description:(NSString *)desc {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSString *responOBJ = [NSString stringWithFormat:@"%@",task.currentRequest.URL];
        NSString *deltaString = [currentTime() substringToIndex:10];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        [TimeConsuming exceptionReport:responOBJ response:[NSString stringWithFormat:@"%@>>>>>>description:%@",task.response,desc] occurr_time:deltaString method:responOBJ problem:@"1" removeForKey:[NSString stringWithFormat:@"%ld",(long)responses.statusCode]];
    });
}



/**
 URL 异常
 
 @param problem 崩溃方法 type 0-响应慢，1-出错，2-重定向
 */
+ (void)exceptionReport:(NSString *)url  response:(NSString *)response occurr_time:(NSString *)occurr_time method:(NSString *)method problem:(NSString *)problem removeForKey:(NSString *)PRIMARY_KEY{
    if ([problem integerValue] == 0) {
        // 响应慢不上传
        return;
    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = [self getExceptionOneUrlS:url response:response occurr_time:occurr_time method:method problem:problem http_status:PRIMARY_KEY];
    
    NSString *postUrl = [NSString stringWithFormat:@"%@/public/index",Report_IP];
    [manager POST:postUrl parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSString *result = [[NSString alloc] initWithData:responseObject  encoding:NSUTF8StringEncoding];

                NSLog(@"上传  异常  =%@",result);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        NSLog(@"URL 异常  =%@",error.localizedDescription);
    }];
}

/**
 崩溃异常上报
 ·
 @param exception 崩溃问题
 */
+ (void)cashReport:(NSString *)exception {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSDictionary *params = [self getExceptionError:exception];
    //    NSLog(@"崩溃问题 params =%@",params);
    NSString *url = [NSString stringWithFormat:@"%@/public/crash",Report_IP];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"result  =%@ ",result);
        NSInteger status = [result[@"status"] integerValue];
        if (status == 0) {
            [XSTool removeObjectForKey:CashReport];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        NSLog(@"崩溃问题  error  =%@",error.localizedDescription);
        
        
    }];
    
}


/**
 问题反馈
 
 @param name 反馈姓名
 @param contentString 反馈内容
 @param data 图片 或者XML
 @param type 上传类型
 */
+(void)feedBackWithName:(NSString *)name content:(NSString *)contentString data:(NSArray *)data type:(FeedBackServiceType)type{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSDictionary *params = [self upSaveFeedAndLogWithName:name content:contentString ];
    
    NSString *url = [NSString stringWithFormat:@"%@/public/savefeed",Report_IP];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (data.count !=0){
            
            
            switch (type) {
                case 0:
                {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [formData appendPartWithFileData:data[idx] name:@"imageFile" fileName:@"imageFile.jpg" mimeType:@"image/*"];
                    }];
                }
                    ;break;
                case 1:[formData appendPartWithFileData:data[0] name:@"xmlFile" fileName:@"xmlFile.xml" mimeType:@"xml/*"]; break;
                case 2:{
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        idx == 0 ? [formData appendPartWithFileData:data[idx] name:@"imageFile" fileName:@"imageFile.jpg" mimeType:@"image/*"]:[formData appendPartWithFileData:data[idx] name:@"xmlFile" fileName:@"xmlFile.xml" mimeType:@"xml/*"];
                        
                    }];
                }
                    break;

            }
            
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSString *result = [[NSString alloc] initWithData:responseObject  encoding:NSUTF8StringEncoding];
        
        NSLog(@"问题反馈result =%@",result);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"问题反馈error  =%@",error.localizedDescription);
    }];
    
}



+ (void) xmlReport:(NSData *)data {
  
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10;
    NSDictionary *params = [self xmlReport];
    
    NSString *url = [NSString stringWithFormat:@"%@/public/saveexceptionxml",Report_IP];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        [formData appendPartWithFileData:data name:@"xmlFile" fileName:@"xmlFile.xml" mimeType:@"xml/*"];
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
        NSString *result = [[NSString alloc] initWithData:responseObject  encoding:NSUTF8StringEncoding];
        
        NSLog(@"xmlReport - - result =%@",result);
//        if ([result containsString:Localized(@"my_skill_sava_success")]) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil] ;
        NSInteger status = [dict[@"status"] integerValue];
        if (status == 0) {
            NSError *error;
            NSString *path1 = XSPath(@"XSErrorReport.xml");
            NSString *path2 = XSPath(@"exceptionReport.plist");
            if (path1) [[NSFileManager defaultManager] removeItemAtPath:path1 error:&error];
            if (path2) [[NSFileManager defaultManager] removeItemAtPath:path2 error:&error];
            NSLog(@" -  - - -删除成功  -%@",NSHomeDirectory());
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"xmlReport - - error  =%@",error.localizedDescription);
    }];
}

/**
 接口异常警告
 
 */
+(NSDictionary *)getExceptionOneUrlS:(NSString *)urlString response:(NSString *)response occurr_time:(NSString *)occurr_time method:(NSString *)method problem:(NSString *)problem http_status:(NSString *)http_status{
    
    NSInteger timeInt = [[currentTime() substringToIndex:10] integerValue];
    NSDictionary *dataDic = @{@"ip":ImageBaseUrl,
                              @"problem":problem,
                              @"identify_num":GET_UUID(),
                              @"machine_type":GET_SystemVersion(),
                              @"model":@"XSAPi",
                              @"method":@"InterfaceException",
                              @"occurr_time":@(timeInt),
                              @"http_status":http_status,
                              @"project_mark":Project_Mark,
                              @"interface_url":urlString,
                              @"response":response,
                              @"use_time":occurr_time,
                              @"create_time":@(timeInt)
                              };


    NSString *ssssss = [NSString stringWithFormat:@"efServicesaveException%@%@",[XSTool dictionaryToJson:dataDic],Project_Secret_key];
    return @{@"sign":[NSString md5:ssssss],
             @"mark":Project_Mark,
             @"model":@"efService",
             @"method":@"saveException",
             @"data":[XSTool dictionaryToJson:dataDic]};
}



/**
 异常抛出
 
 */
+ (NSDictionary *)getExceptionError:(NSString *)content {
    
    NSInteger timeInt = [[currentTime() substringToIndex:10] integerValue];
    NSDictionary *dataDic = @{@"create_time":@(timeInt),
                              @"ip":ImageBaseUrl,
                              @"identify_num":GET_UUID(),
                              @"machine_type":GET_SystemVersion(),
                              @"project_mark":Project_Mark,
                              @"content":content};
    NSString *dataJSON = [XSTool dictionaryToJson:dataDic];
    NSString *signM = [NSString stringWithFormat:@"crashLogServicesaveCrash%@%@",dataJSON,Project_Secret_key];
    return @{@"sign":[NSString md5:signM],
             @"mark":Project_Mark,
             @"model":@"crashLogService",
             @"method":@"saveCrash",
             @"data":dataJSON};
    
}



/**
 问题反馈
 
 */
+ (NSDictionary *)upSaveFeedAndLogWithName:(NSString *)name content:(NSString *)content {
    
    NSDictionary *dataDic = @{@"create_time":[currentTime() substringToIndex:10],
                              @"ip":ImageBaseUrl,
                              @"identify_num":GET_UUID(),
                              @"machine_type":GET_SystemVersion(),
                              @"project_mark":Project_Mark,
                              @"refer_name":name,
                              @"problem_desc":content};
    
    NSString *dataJSON = [XSTool dictionaryToJson:dataDic];
    NSString *signM = [NSString stringWithFormat:@"feedBackServicesaveFeedAndLog%@%@",dataJSON,Project_Secret_key];
    return @{@"sign":[NSString md5:signM],
             @"data":dataJSON,
             @"mark":Project_Mark,
             @"model":@"feedBackService",
             @"method":@"saveFeedAndLog"};
}



/**
 XML Report
 */
+ (NSDictionary *)xmlReport {
    
    NSDictionary *dic;
    
    
    NSString *sign = [NSString stringWithFormat:@"efServicesaveExceptionXml%@",Project_Secret_key];
    dic = @{@"sign":[NSString md5:sign],
            @"mark":Project_Mark,
            @"data":@"",
            @"model":@"efService",
            @"method":@"saveExceptionXml"};
    
    return dic;
}

@end


@implementation XSErrorReport

-(instancetype)init {
    if (self = [super init]) {
     
        self.machine_type = GET_SystemVersion();
        self.identify_num = GET_UUID();
        self.occurr_time = [currentTime() substringToIndex:10];
        self.project_mark = Project_Mark;
    }
    return self;
}



+ (BOOL) saveRootExpetion:(XSErrorReport *)model {


    NSData *xmlData = [[NSData alloc] initWithContentsOfFile:XSPath(@"XSErrorReport.xml")];
    
    if (xmlData) {
        NSError *error = nil;
        GDataXMLDocument *doc= [[GDataXMLDocument alloc]initWithData:xmlData encoding:NSUTF8StringEncoding error:&error];
        GDataXMLElement *rootElement = [doc rootElement];
        GDataXMLElement *element = [XSErrorReport creatElementWithModel:model];
        [rootElement addChild:element];
        
       return  [XSErrorReport saveXMLDocumemt:rootElement];
        
    }else{
        GDataXMLElement *element = [XSErrorReport creatElementWithModel:model];
        GDataXMLElement *rootElement = [GDataXMLNode elementWithName:@"exception"];
        [rootElement addChild:element];
       return  [XSErrorReport saveXMLDocumemt:rootElement];
    }

}


/**
 拼接 model 为 xml格式
 @param model model description
 @return return 标签属性
 */
+ (GDataXMLElement *) creatElementWithModel:(XSErrorReport *) model {
    
    GDataXMLElement *element = [GDataXMLNode elementWithName:@"api" stringValue:nil];
    NSMutableDictionary *dic;
    @try {
        dic = model.mj_keyValues;
    } @catch (NSException *exception) {
        dic = nil;
    } @finally {
        
    }
    
    if (dic == nil) return nil;
    
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        // 标签属性
        GDataXMLElement *attribute = [GDataXMLNode attributeWithName:key stringValue:dic[key]];
        
        [element addAttribute:attribute];
    }];
    return element;
}


/**
 不存在xml文件 创建xml 文件 并写入
 */
+ (BOOL ) saveXMLDocumemt:(GDataXMLElement *)rootElement {
    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithRootElement:rootElement];
    NSData *data = [xmlDoc XMLData];
    return  [data writeToFile:XSPath(@"XSErrorReport.xml") atomically:YES];
}


@end
