//
//  XSTool.m
//  App3.0
//
//  Created by apple on 2017/5/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSTool.h"
#import <objc/runtime.h>
#import <CoreText/CoreText.h>
#import "AppDelegate.h"
#import "CYTabBarController.h"
#import "FDFindViewController.h"
#import "FDHomeViewController.h"
#import "FDMeController.h"

@implementation XSTool
static NSString *key = @"asdf87290oiapso";

//创建label
+(UILabel *)createLabelWithText:(NSString *)text Font:(UIFont *)font textColor:(UIColor *)textColor lines:(int)numLines textAlignment:(NSTextAlignment)textAlignment{
    UILabel *label=[UILabel new];
    label.text=text;
    label.font=font;
    label.textColor=textColor;
    label.numberOfLines=numLines;
    label.textAlignment=textAlignment;
    label.userInteractionEnabled=YES;
    return label;
}
//创建圆形imageview
+(UIImageView *)createCircleImageViewWithImage:(UIImage *)image radius:(CGFloat)radius{
    UIImageView *imageView=[UIImageView new];
    imageView.image=image;
    imageView.layer.cornerRadius=radius;
    imageView.layer.masksToBounds=YES;
    imageView.userInteractionEnabled=YES;
    return imageView;
}
//创建按钮
+(UIButton *)createBtnWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font bgColor:(UIColor *)bgColor image:(UIImage *)image bgImage:(UIImage *)bgImage{
    UIButton *btn=[UIButton new];
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:textColor forState:UIControlStateNormal];
    btn.titleLabel.font=font;
    if (image) {
        [btn setImage:image forState:UIControlStateNormal];
    }
    if (bgImage) {
        [btn setBackgroundImage:bgImage forState:UIControlStateNormal];
    }else{
        [btn setBackgroundColor:bgColor];
    }
    return btn;
}

+(UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else if ([appRootVC isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)appRootVC;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        return  result=nav.childViewControllers.lastObject;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        result=nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    
    
    return result;
}



+ (void)showProgressHUDWithView:(UIView *)view
{
    [XSTool showProgressHUDTOView:view withText:@""];
}

+ (void)showProgressHUDTOView:(UIView *)view withText:(NSString *)text
{
    if (view == nil)
    {
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
//    hud.mode= MBProgressHUDModeText;
    hud.contentColor = [UIColor whiteColor];
    hud.label.textColor = [UIColor whiteColor];
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.bezelView.color = [UIColor colorWithWhite:0.f alpha:0.7];
    if (!text || text.length <= 0) {
        
    } else {
        hud.label.text= text;
    }
}

+ (void)hideProgressHUDWithView:(UIView *)view
{
    if (view == nil)
    {
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    [MBProgressHUD hideHUDForView:view animated:YES];
}

+ (void)showToastWithView:(UIView *)view Text:(NSString *)text 
{
    if (isEmptyString(text)) {
        return;
    }
    if (view == nil)
    {
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.mode= MBProgressHUDModeText;
    hud.label.textColor = [UIColor whiteColor];
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.bezelView.color = [UIColor colorWithWhite:0.f alpha:0.7];
    if (!text) {
        text = @"";
    }
    
    hud.animationType = MBProgressHUDAnimationZoomOut;
    hud.square = NO;
    
    if (text.length >15) {
        hud.detailsLabel.text = Localized(text);
    }else{
        hud.label.text = Localized(text);
    }
    [hud hideAnimated:YES afterDelay:1.5];
  
}

+ (UIViewController *)createViewControllerWithClassName:(NSString *)className {
    Class cla = NSClassFromString(className);
    
    return [cla new];
}


+ (UITabBarController *)setRootVC {

    XSBaseNaviController *firstVC = [XSTool naviNormalImage:@"Tab_dry_planet" selectedImage:@"Tab_dry_planet" title:Localized(@"去燥星球") controller:[[FDHomeViewController alloc] init] tag:0];
    
    XSBaseNaviController *secondVC = [XSTool naviNormalImage:@"Tab_planet_hole" selectedImage:@"Tab_planet_hole" title:Localized(@"星球燥洞") controller:[[FDHomeViewController alloc] init] tag:0];

    XSBaseNaviController *thirdVC = [XSTool naviNormalImage:@"Tab_planet_notice" selectedImage:@"Tab_planet_notice" title:Localized(@"星球公告") controller:[[FDHomeViewController alloc] init] tag:0];

    XSBaseNaviController *fourVC = [XSTool naviNormalImage:@"Tab_mine" selectedImage:@"Tab_mine" title:Localized(@"ME我") controller:[[FDHomeViewController alloc] init] tag:0];

    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = @[firstVC, secondVC, thirdVC, fourVC];
    tabBarController.delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    tabBarController.selectedIndex = 0;
    
    //设置背景图片，@"barBackgound"是一张全通道全透明的图片
    [tabBarController.tabBar setBackgroundImage:[UIImage imageNamed:@"fd_tabbar_clearBg"]];
    //边界线透明化
    tabBarController.tabBar.shadowImage = [[UIImage alloc]init];
    // 设置TabBar标题偏移
    [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    
    // 全局设置NavigationBar 颜色
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    return tabBarController;
}


+ (XSBaseNaviController *)naviNormalImage:(NSString *)normal selectedImage:(NSString *)selecteImage title:(NSString *)title controller:(UIViewController *)viewControler tag:(NSInteger )tag{
    
    XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:viewControler];
    UIImage *unselectedImage = [UIImage imageNamed:normal];
    UIImage *selectedImage = [UIImage imageNamed:selecteImage];
    
    viewControler.tabBarItem = [[UITabBarItem alloc] initWithTitle:title
                                                             image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                     selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    viewControler.tabBarItem.tag = tag;
    
    return navi;
    
}


+ (UIViewController *)enterMainViewController {
    [ChatHelper shareHelper];
    
    UIViewController *vc;
#ifdef APP_SHOW_JYS
    JYSTabBarViewController * tabbarVC = [[JYSTabBarViewController alloc] init];
    tabbarVC.fd_prefersNavigationBarHidden = YES;
    vc = tabbarVC;
#else
    vc = [XSTool setRootVC];
#endif

#ifdef ALIYM_AVALABLE
    [[ChatCallManager sharedManager] setMainController:vc];
    [[TILChatCallManager sharedManager] setMainController:vc];
    [[ChatHelper shareHelper].tabChatVC refreshConversationLists];
#elif defined EMIM_AVALABLE
    [[ChatCallManager sharedManager] setMainController:vc];
    [[ChatHelper shareHelper].replyVC refreshConversationLists];
#else
    
#endif
    
    return vc;
}

+ (void) otherLogIn {
    
    
    [[UserInstance ShardInstnce] logout];
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:Localized(@"prompt") message:@"登录已失效，请重新登录" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1=[UIAlertAction actionWithTitle:Localized(@"material_dialog_positive_text") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //        EnterViewController *enter = [[EnterViewController alloc] init];
        //        UINavigationController* enterNav = [[UINavigationController alloc] initWithRootViewController:enter];
        [UIApplication sharedApplication].keyWindow.rootViewController = [XSTool enterMainViewController];
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }];
    [alert addAction:action1];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window.rootViewController presentViewController:alert animated:YES completion:nil];
}


+ (NSString *)convertToJsonData:(NSDictionary *)dict
{
    // 字典key首字母排序
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    NSMutableArray *mArr = [NSMutableArray array];
    NSArray *keysArray = [dict allKeys];
    NSArray *sortedArray = [keysArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSDiacriticInsensitiveSearch];
    }];
    
    // 把排序后的key和对应的value放到新的字典中
    for (NSString *key in sortedArray) {
        [mArr addObject:[dict objectForKey:key]];
        [mDic setValue:[dict objectForKey:key] forKey:key];
    }
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i = 0; i < sortedArray.count; i++) {
        /* 适应冒号前加反斜杠
         NSString *keyValueStr;
         if ([mArr[i] isKindOfClass:[NSString class]] && ([mArr[i] hasPrefix:@"["] || [mArr[i] hasPrefix:@"{"])) {
         NSString *str = mArr[i];
         NSMutableString *mutStr = [NSMutableString stringWithString:str];
         NSRange range = {0,str.length};
         [mutStr replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSLiteralSearch range:range];
         keyValueStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",sortedArray[i],mutStr];
         } else {
         keyValueStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",sortedArray[i],mArr[i]];
         }
         */
        NSString *keyValueStr;
        if ([mArr[i] isKindOfClass:[NSString class]]) {
            NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *receiverString = [[NSString alloc]initWithString:[mArr[i] stringByTrimmingCharactersInSet:whiteSpace]];
            keyValueStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",sortedArray[i],receiverString];
        } else {
            keyValueStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",sortedArray[i],mArr[i]];
        }
        [signArray addObject:keyValueStr];
        
        
        //        if ([str isKindOfClass:[NSString class]] && [str hasPrefix:@"["]) {
        //            NSString *keyValueStr = [NSString stringWithFormat:@"\"%@\":%@",sortedArray[i],mArr[i]];
        //            [signArray addObject:keyValueStr];
        //        } else if ([str isKindOfClass:[NSString class]] && [str hasPrefix:@"{"]) {
        //            NSString *keyValueStr = [NSString stringWithFormat:@"\"%@\":%@",sortedArray[i],mArr[i]];
        //            [signArray addObject:keyValueStr];
        //        } else {
        //            NSString *keyValueStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",sortedArray[i],mArr[i]];
        //            [signArray addObject:keyValueStr];
        //        }
        
    }
    NSString *signString = [signArray componentsJoinedByString:@","];
    
    NSString *jsonString = [NSString stringWithFormat:@"{%@}",signString];
    
    //    // 字符串生成json字典
    //    NSData *resData = [[NSData alloc] initWithData:[jsonString1 dataUsingEncoding:NSUTF8StringEncoding]];
    //    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:resData options:NSJSONReadingMutableLeaves error:nil];
    /*
     NSError *error;
     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mDic options:NSJSONWritingPrettyPrinted error:&error];
     
     NSString *jsonString;
     if (!jsonData) {
     NSLog(@"%@",error);
     }else{
     jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
     }
     */
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    //    NSLog(@"--------------%@",mutStr);
    //    NSRange range = {0,jsonString.length};
    //
    //    //去掉字符串中的空格
    //    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
    //将字符串中的换行符替换成"\n"字符串
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSLiteralSearch range:range2];
    //
    //    // 去掉反斜杠
    //    NSRange range3 = {0,mutStr.length};
    //    //去掉字符串中的换行符
    //    [mutStr replaceOccurrencesOfString:@"\\" withString:@"" options:NSLiteralSearch range:range3];
    return mutStr;
}

+ (NSDictionary *)convertToParamFromParameters:(NSDictionary *)dict
{
    NSMutableDictionary *argDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    // 获取本地存储的guid
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *guid = [ud objectForKey:APPAUTH_KEY];
//    NSLog(@"guid = %@",guid);
    if (!guid) {
        guid = @"";
    }
    [argDic setValue:guid forKey:@"guid"];
    NSString *language = SelectLang;
    
    if (!language || [language isEqualToString:@""]) {
        language = @"zh-Hans";//简体中文
        [[NSUserDefaults standardUserDefaults] setObject:language forKey:MyLanguage];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    //英文传en,中文传zh-cn;繁体传zh-tw
    if ([language isEqualToString:@"zh-Hans"]) {
        language = @"zh-cn";
    }else if ([language isEqualToString:@"en"]) {
        language = @"en";
    }else if ([language isEqualToString:@"zh-Hant"]) {
        language = @"zh-tw";
    }
    [argDic setValue:language forKey:@"lang"];
    
    [argDic setValue:@"app" forKey:@"device"];
    //版本号
    [argDic setValue:[AppConfigManager ShardInstnce].app_Version forKey:@"app_version"];
    if ([NSJSONSerialization isValidJSONObject:argDic])
    {
//        NSLog(@"jsonobj is valid");
    }
    
    NSString *jsonStr = [self convertToJsonData:argDic];
    NSString *sign = [NSString md5:[NSString stringWithFormat:@"%@%@",jsonStr,key]];
//    NSDictionary *param = @{@"sign":sign,@"arg":jsonStr};
    
    // 更换参数新规则，去掉arg包裹
    NSMutableDictionary *param1 = [NSMutableDictionary dictionaryWithDictionary:argDic];
    [param1 setValue:sign forKey:@"sign"];
    
//    NSLog(@"\n***********************************\n jsonStr:%@\n key:%@\n sign:%@\n argDic%@\n***********************************\n",jsonStr,key,sign,argDic);
//    NSLog(@"上传数据:%@",param1);
    
    
    return param1;
}





+ (BOOL) creatPlistWithExceptionReport:(id )value key:(NSString *)key {
    
    NSError *error = nil;
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSMutableDictionary *temp;
    if (plistXML) {
        temp = (NSMutableDictionary *) [NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
        [temp setValue:@{@"time":currentTime()} forKey:key];
    }else{
        temp = [NSMutableDictionary dictionary];
        [temp setValue:@{@"time":currentTime()} forKey:key];
        
    }
    NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:temp format:NSPropertyListXMLFormat_v1_0 options:kCFPropertyListMutableContainersAndLeaves error:&error];
    if(plistData) {
        return  [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(@" * * * *   = %@", error);
        return NO;
        
    }
    
}


+ (NSMutableDictionary *) getContentsOfFile {
//    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
//    NSString *plistPath = [path stringByAppendingPathComponent:@"exceptionReport.plist"];
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    if (!plistPath) {
        return NULL;
    }
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    return userInfo;
}

+ (BOOL) deleteExceptionReportForKey:(NSString *)key {
//    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
//    NSString *plistPath = [path stringByAppendingPathComponent:@"exceptionReport.plist"];
    NSString *plistPath = XSPath(@"exceptionReport.plist");
    if (!plistPath) {
        return NO;
    }
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    [userInfo removeObjectForKey:key];
    return  [userInfo writeToFile:plistPath atomically:YES];
}

+ (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}


+(NSDictionary *)returnDictionaryWithDataPath:(NSString *)path
{
    
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *temp = (NSDictionary *) [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
    
    return temp;
}


+ (NSDateFormatter *)shareYMDHMS {
    
    static NSDateFormatter *ymdFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ymdFormatter = [[NSDateFormatter alloc] init];
        [ymdFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
        [ymdFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    });
    
    return ymdFormatter;
}

+ (NSDateFormatter *) shareYMD {
    
    static NSDateFormatter *ymdFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ymdFormatter = [[NSDateFormatter alloc] init];
        [ymdFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
        [ymdFormatter setDateFormat:@"yyyy-MM-dd"];
    });
    
    return ymdFormatter;
}

//设置日期显示
+ (NSString*)dateStrStringFromDate:(NSDate *)inputDate
{
    if (!inputDate) {
        return @"";
    }
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM月dd日";
    NSString *dateStr = [dateFormatter stringFromDate:inputDate];
    
    return dateStr;
}

//根据日期得到周几
+(NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六",nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [calendar setTimeZone: timeZone];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    NSString *weekdayStr = [weekdays objectAtIndex:theComponents.weekday];
    
    return weekdayStr;
}

//日期 向前||向后 ?年 ?月  ?天
+ (NSDate*)dateStringFromDate:(NSDate *)inputDate
                         year:(NSInteger)year
                        month:(NSInteger)month
                          day:(NSInteger)day
{
    if (!inputDate) {
        return [NSDate date];
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *adcomps = [[NSDateComponents alloc] init];
    
    [adcomps setYear:year];
    [adcomps setMonth:month];//向后?个月
    [adcomps setDay:day];
    NSDate *newDate = [calendar dateByAddingComponents:adcomps toDate:inputDate options:0];
    
    return newDate;
}

+ (void)saveStr:(NSString *)str forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:str forKey:key];
    [defaults synchronize];
}

+ (NSString *)getStrUseKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:key];
    return str;
}

+ (void)removeObjectForKey:(NSString *)str {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:str];
    [userDefaults synchronize];
}



+ (void)setTimeQueue:(dispatch_queue_t )queue dispatch_source:(dispatch_source_t )sourceTime
            walltime:(int) walltime stop:(void(^)())stopAction
         otherAction:(void(^)(int))otherAction{
    __block NSInteger time = walltime; //倒计时时间
    NSLock *lock = [NSLock new];
    dispatch_source_set_timer(sourceTime,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(sourceTime, ^{
        [lock lock];
        NSInteger tt = time;
        [lock unlock];
        if(tt <= 0){ //倒计时结束，关闭
            dispatch_async(dispatch_get_main_queue(), ^{
                stopAction();
            });
            
        }else{
            int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                otherAction(seconds);
            });
            [lock lock];
            time --;
            [lock unlock];
        }
        
    });
    
    dispatch_resume(sourceTime);
    
    
}



@end


@implementation NSMutableArray (CCCC)


- (NSMutableArray *)sortedWithChineseKey {
    
    NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:self.count];
    
    for (int i = 0; i < self.count; ++i) {
        NSString *chineseString = self[i] ;
        [tmpArray addObject:@{@"obj": self[i], @"pinyin": chineseString.pinyinString.lowercaseString}];
    }
    
    [tmpArray sortUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        return [obj1[@"pinyin"] compare:obj2[@"pinyin"]];
    }];
    
    return [tmpArray valueForKey:@"obj"];;
}





@end


@implementation NSString (Chinese)

- (NSString *)pinyinString {
    NSAssert([self isKindOfClass:[NSString class]], @"必须是字符串");
    
    if (self == nil) {
        return nil;
    }
    
    NSMutableString *pinyin = [self mutableCopy];
    
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripDiacritics, NO);
    
    return pinyin;
}

@end


@implementation NSDictionary (Log)

- (NSString *)descriptionWithLocale:(id)locale
{
    NSMutableString *string = [NSMutableString string];
    
    // 开头有个{
    [string appendString:@"{\n"];
    
    // 遍历所有的键值对
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [string appendFormat:@"\t%@", key];
        [string appendString:@" : "];
        [string appendFormat:@"%@,\n", obj];
    }];
    
    // 结尾有个}
    [string appendString:@"}"];
    
    // 查找最后一个逗号
    NSRange range = [string rangeOfString:@"," options:NSBackwardsSearch];
    if (range.location != NSNotFound)
        [string deleteCharactersInRange:range];
    
    return string;
}


@end

@implementation NSArray (decription)


- (NSString *)descriptionWithLocale:(id)locale
{
    NSMutableString *str = [NSMutableString stringWithFormat:@"%lu (\n", (unsigned long)self.count];
    
    for (id obj in self) {
        [str appendFormat:@"\t%@, \n", obj];
    }
    
    [str appendString:@")"];
    
    return str;
}
@end

@implementation MBProgressHUD (MyHUD)


+ (void)showMessage:(NSString *)message view:(UIView *)view {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    if (Localized(message).length >15) {
        hud.detailsLabel.text = Localized(message);
    }else{
        hud.label.text = Localized(message);
    }
//    hud.label.text = Localized(message);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        sleep(1);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [hud hideAnimated:YES];
        });
    });
}



+ (void)showHUDAddToView:(UIView *)view withText:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if (text == nil) {
        hud.label.text = @"";
    } else {
        hud.detailsLabel.text = Localized(text); 
    }
}

+ (void)showMessage:(NSString *)message view:(UIView *)view hideTime:(int) time doSomeThing:(void (^)())someThing {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if (Localized(message).length >15) {
        hud.detailsLabel.text = Localized(message);
    }else{
        hud.label.text = Localized(message);
    }
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        sleep(time);
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            if (someThing) {
                
                someThing();
            }
            
        });
    });
}


@end



@implementation UIFont (PX_Fount)



+ (UIFont *)qsh_systemFontOfSize:(CGFloat)pxSize{
    CGFloat pt = pxSize;
    if (IS_IPHONE_5) {
        pt =  pt -1;
    }else if(IS_IPHONE_6){
        pt = pt;
    }else{
        pt = pt +1;
    }
    UIFont *font = [UIFont systemFontOfSize:pt];
    return font;
}


+ (CGFloat)heightForLableWithText:(NSString *)text lableWidth:(CGFloat)lableWidth font:(CGFloat)fontSize{
    
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL);
    CGFloat fontHeight =  CTFontGetCapHeight(ctFont);
    CFRange textRange = CFRangeMake(0, text.length);
    CFMutableAttributedStringRef string = CFAttributedStringCreateMutable(kCFAllocatorDefault, text.length);
    CFAttributedStringReplaceString(string, CFRangeMake(0, 0), (CFStringRef) text);
    CFAttributedStringSetAttribute(string, textRange, kCTFontAttributeName, ctFont);
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(string);
    CGSize targetSize = CGSizeMake(lableWidth, CGFLOAT_MAX);
    CFRange fitRange;
    CGSize size = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, (CFIndex)[text length]), NULL, targetSize, &fitRange);
    CFRelease(framesetter);
    CFRelease(string);
    CFRelease(ctFont);
    return size.height + fontHeight;
}


@end


@implementation UIView(XSView)



-(void) setXs_centerX:(CGFloat)xs_centerX {
    CGPoint center = self.center;
    center.x = xs_centerX;
    self.center = center;
}

-(CGFloat)xs_centerX {
    return self.center.x;
}

- (void) setXs_centerY:(CGFloat)xs_centerY {
    CGPoint center = self.center;
    center.y = xs_centerY;
    self.center = center;
}

-(CGFloat)xs_centerY {
    return self.center.y;
}



@end


