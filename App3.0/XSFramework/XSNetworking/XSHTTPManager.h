//
//  XSHTTPManager.h
//  App3.0
//
//  Created by apple on 2017/5/12.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
//NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, XSNetworkStatu) {
    XSNetworkStatusUnknown           = 0, //未知网络
    XSNetworkStatusNotReachable      = 1,//没有网络
    XSNetworkStatusReachableViaWWAN  = 2,    //手机自带网络
    XSNetworkStatusReachableViaWiFi  = 3     //wifi
};
typedef void(^XSNetworkStatusBlock)(NSUInteger netStatus);


@interface resultObject : NSObject

@property (nonatomic,assign)BOOL status;

@property (nonatomic,copy)NSString *info;

@property (nonatomic ,strong) NSDictionary*data;


/**
 返回 data 数据 不是 字典或者数组 用value接收
 */
@property (nonatomic ,copy)NSString *returnValue;
@end

typedef void(^XSAPIClientSuccess)(NSDictionary * dic, resultObject *state);

typedef void(^XSAPIClientFailure)(NSError * error);

@interface XSHTTPManager : AFHTTPSessionManager

+ (instancetype)sharedClient;

/** 网络状态 */
@property (nonatomic, assign) XSNetworkStatu networkStats;

/**
 判断当前网络状态
 * 0 -> 未知网络
 * 1 -> 没有网络
 * 2 -> 手机自带网络
 * 3 -> wifi
 */
+ (void)checkNetStatus:(XSNetworkStatusBlock)block;

/**
 判断是否有网
 */
+ (BOOL)isHaveNetwork;

+ (void)post:(NSString *)URLString
  parameters:(id)parameters
     success:(XSAPIClientSuccess)success
     failure:(XSAPIClientFailure)failure;

+ (RACSignal *)rac_POSTURL:(NSString *)url params:(NSDictionary *)params;

@end
//NS_ASSUME_NONNULL_END
