//
//  XSApi.h
//  App3.0
//
//  Created by mac on 17/2/17.
//  Copyright © 2017年 mac. All rights reserved.
//





#import <Foundation/Foundation.h>
#import "S_StoreViewController.h"
#import "ContactModel.h"
#import "ProductModel.h"

#import <sys/time.h>
NS_ASSUME_NONNULL_BEGIN

static inline NSString *currentTime (){
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%f", [datenow timeIntervalSince1970]*1000];
    return timeSp;
}

static inline void XSBenchmark(void (^block)(void), void (^complete)(double ms)) {

    struct timeval t0, t1;
    gettimeofday(&t0, NULL);
    block();
    gettimeofday(&t1, NULL);
    double ms = (double)(t1.tv_sec - t0.tv_sec) * 1e3 + (double)(t1.tv_usec - t0.tv_usec) * 1e-3;
    complete(ms);
}



static inline NSString *GET_UUID() {
    
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}


static inline NSString *GET_SystemVersion() {
    
    return [[UIDevice currentDevice] systemVersion];
}






typedef enum : NSUInteger {
    FeedBackServiceImage = 0,
    FeedBackServiceXML,
    FeedBackServiceImageAndXML,
} FeedBackServiceType;


@interface XSApi : NSObject



@property (nonatomic ,assign)FeedBackServiceType FeedBackServicetype;




// 字典转字符串
//-(nullable NSString *)convertToJsonData:(nullable NSDictionary *)dict;

// 初始参数转换成接口可用参数
//- (nullable NSDictionary *)convertToParamFromParameters:(nullable NSDictionary *)dict;

+ (void)requestWithUrl:(nonnull NSString *)URLString
            parameters:(nullable id)parameters
               success:(nullable requestSuccessBlock)success
               failure:(nullable requestFailureBlock)failure;

+ (void)uploadWithUrlString:(nonnull NSString *)urlString andUpData:(nullable NSObject *) upData paramters:(nullable id)  parameters progress:(nullable  requestProgressBlock) progress success:(nullable requestSuccessBlock)success fail:(nullable requestFailureBlock)fail;

+ (void)uploadWithUrlString:(nonnull NSString *)urlString andUpImgs:(nullable NSArray *) upImgs paramters:(nullable id)  parameters progress:(nullable  requestProgressBlock) progress success:(nullable requestSuccessBlock)success fail:(nullable requestFailureBlock)fail;

@end

#pragma mark  - - - - - - - --  - - -
#pragma mark  - - - - - - - --  - - - HTTPManager
@interface HTTPManager : NSObject

//商城新增这接口 异步注册im 聊天
+ (void)CheckUserRegImSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//意见反馈
+ (void)feedbackAddWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

//18.获取我的团队信息
+(void)yun_getMyTreeWithPage:(NSString *)page
                     success:(XSAPIClientSuccess)success
                        fail:(XSAPIClientFailure)fail;


//19.获取奖金记录
+(void)yun_getAwardInfoWithType:(NSString *)type
                           page:(NSString *)page
                        account:(NSString *)account
                        success:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail;

//47.谷歌验证页面
+(void)yun_googleVerifyIndexSuccess:(XSAPIClientSuccess)success
                               fail:(XSAPIClientFailure)fail;
//48.谷歌验证
+(void)yun_googleVerifyWithSecret:(NSString *)secret
                       old_verify:(NSString *)old_verify
                           verify:(NSString *)verify
                          success:(XSAPIClientSuccess)success
                             fail:(XSAPIClientFailure)fail;


// app获取商城配置
+ (void)getShopConfigWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/* - - - - - - 线下商家- - - - - - -*/
//首页顶部轮播图广告(v1offline)
+ (void)OffHomeGetTopGgsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//首页广告位(v1offline)
+ (void)OffGetMiddleGgsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//线下商品：为您推荐（猜你喜欢）的商品(v1offline)
/*
 city    必须    string    市名
 user_position    必须    array    当前地图定位信息
 |--longitude        string    经度
 |--latitude        string    纬度
 */
+ (void)OffGuessYouLikeListsWithPage:(NSString *)page
                                city:(NSString *)city
                       user_position:(NSDictionary *)user_position
                             success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//线下商家:获取全部行业分类(v1offline) supply/industry/Lists
+ (void)OffSupplyIndustryListsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//线下商家：获取某行业下所有子分类(v1offline)
+ (void)OffIndustrySubListsWithIndustry:(NSString *)industry
                                success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//二级界面====>

//行业首页顶部轮播图广告(v1offline)
+ (void)OffindustryGetTopGgsWithIndustry:(NSString *)industry
                             success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//获取店铺列表筛选条件(v1offline)
+ (void)OffGetFileterConditionsSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//店铺列表(v1offline)筛选 type
+ (void)OffSupplyListsWithPage:(NSString *)page
                          type:(int)isTj
                          city:(NSString *)city
                      industry:(NSString *)industry
                       keyword:(NSString *)keyword
                        filter:(NSDictionary *)filter
                 user_position:(NSDictionary *)user_position
                       success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
    
//店铺列表(v1offline)筛选
+ (void)OffSupplyListsWithPage:(NSString *)page
                          city:(NSString *)city
                      industry:(NSString *)industry
                       keyword:(NSString *)keyword
                        filter:(NSDictionary *)filter
                 user_position:(NSDictionary *)user_position
                       success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;



//获取某城市下的区县
+ (void)OffGetCountyByCityWithcity_code:(NSString *)city_code
                                success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//获取某区县下的街道
+ (void)OffGetTownByCountyWithCounty_code:(NSString *)county_code
                                  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/* - - - - - - - - - - - - -*/
#pragma mark  - - - - - - - --  - - - 登录
//获取验证设置
+(void)GetWithdrawVerifySuccess:(XSAPIClientSuccess)success
                           fail:(XSAPIClientFailure)fail;

/*
 设置验证
 */
+ (void)setWithdrawVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 普通登录
 */
+ (void)doLoginin:(NSString *)name
         password:(NSString *)password
          success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/*
 普通登录+二次验证
 */
+ (void)doLoginin:(NSString *)name
         password:(NSString *)password
            email:(NSString *)email
           mobile:(NSString *)mobile
           google:(NSString *)google
          success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取短信验证码
 */
+ (void)getSmsVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/*
 获取邮箱验证码
 */
+ (void)getEmailVerifyWithDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/*
 获取短信验证码
 */
+ (void)getSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 验证短信验证码
 */
+ (void)checkSmsVerifyValidWithMobile:(NSString *)mobile smsVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 短信验证码登录
 */
+ (void)loginBySms:(NSString *)name verify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 检测手机号是否存在
 */
+ (void)checkMobileExist:(NSString *)mobile Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 重置登录密码
 */
+ (void)resetLoginPwdWithMobile:(NSString *)mobile pwdType:(NSString *)pwdType smsVerify:(NSString *)smsVerify andNewPwd:(NSString *)newPwd success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**退出*/
+(void)LoginOutWithName:(NSString *) name success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/**更换绑定手机号*/
+ (void)setupMobile:(NSString *) mobile verify:(NSString *) verify token:(NSString *) token success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;

/**验证密码*/
+ (void)verifyPassWordWithPassword:(NSString *) password type:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;

/**
 给原手机号发验证码
 */
+ (void)mobileVerifyWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**
 原手机号验证
 */
+ (void)mobileValidateWithVerify:(NSString *)verify success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**获取系统内的协议*/
+ (void)getAgreementWithType:(NSString *) type success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;


/**
 自动登录
 */
+ (void)autoLoginWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

#pragma mark  - - - - - - - --  - - - 注册
//注册
+ (void)registerWithIntro:(NSString *)intro nickName:(NSString *)nickname mobile:(NSString *)mobile verify:(NSString *)verify password:(NSString *)pwd success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
+ (void)registerWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 注册时获取短信验证码，需要额外参数type:“reg”
 */
+ (void)regGetSmsVerify:(NSString *)mobile success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**
 用户注册表单信息
 字段           类型及范围	说明
 show           array	需要显示的注册项
 |--intro       string	邀请码
 |--mobile      string	手机号
 |--nickname	string	昵称
 |--province	string	区域
 |--address     string	详细地址
 |--agreement	string	协议
 |--verify      string	验证码
 require        array	必须的注册项
 |--同上         string	同上
 introducer     array	系统推荐的邀请码
 intro          string	未知
 */
+ (void)getRegisterInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

#pragma mark  - - - - - - - --  - - - 搜索
+ (void)searchUserWithString:(NSString *)string success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

#pragma mark  - - - - - - - --  - - - 购物车
// 获取购物车列表
+ (void)getCartListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

// 更新购物车
+ (void)editCartWithPeid:(NSString *)peid productNumber:(NSInteger)pNum newPeid:(NSString *)nPeid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/* 删除购物车
 pid 商品拓展id或者all（删除全部）
 */
+ (void)deleteCartWithProductId:(NSString *)pId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**加入购物车*/
+ (void) addGoodsToCartWithgoodsId:(NSString *) goodsId goodsNum:(NSString *) goodsNum extId:(NSString *) extId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

#pragma mark  - - - - - - - --  - - - 商品列表
/**
 * 获取商品列表
 * @param order 排序方式:product_id(综合) look_num(人气) sell_num(销量) sell_price(价格)
 * @param sortType 1升序 0降序
 */
+ (void)getProductListWithOrder:(NSDictionary *)params Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;;


#pragma mark  - - - - - - - --  - - -
/**
 行业列表
 */
+(void)getIndustryListsParams:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
/**
 商家列表
 */
+ (void)getSupply_ListsParams:(SupplyModel *)model Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**
 2级城市
 */
+ (void)getAreaWithLevelTWoSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure ;



/**
 获取某一级城市
 */
+ (void)getAreaChildList:(NSString *)city_ID Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**
 获取商家首页信息
 
 @param store_ID 商家ID
 
 */
+(void)getStoreInformation:(int )store_ID Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**
 店铺广告
 */
+(void)basicadvertListsSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure ;


/**
 收藏
 */
+(void)setFavorite:(NSString *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**
 获取某个商家分类信息
 */
+ (void)scategoryEdit:(NSString *)storeId page:(NSNumber *)page Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**
 热门城市

 */
+ (void)getHotAreaSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**
 快速生成订单
 */
+(void) quickorderCreateQuickOrder:(NSDictionary *)dic Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**
 获取分享数据
*/

+ (void)getShareInfo:(NSDictionary *)params Succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**添加收藏*/
+ (void)addCollectionInfoWithFavType:(NSString * )favType favId:(NSString *)favId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取收藏信息*/
+ (void)getCollectionInfoWithFavType:(NSString *)favTypeId andPage:(NSString *)page andLimit:(NSString *) limit  type:(NSString *) type success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**移除收藏*/
+ (void)deleteCollectionInfoIsOffline:(BOOL)isOffline WithId:(NSString * )product_id success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;



/**获取浏览历史*/
+ (void)getHistoryListsWithPage:(NSString *)page andLimit:(NSString *)limit success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**删除浏览记录*/
+ (void)deleteHistoryInfoWithIdsString:(NSString *)ids success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**上传图片*/
+ (void)upLoadUserInfoWithIconImage:(UIImage *)iconImage progress:(requestProgressBlock)progress success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**提交验证信息*/
+ (void) submitIdentityCarInfoWithImageString:(NSString *)imageString trueName:(NSString *) trueName card:(NSString *)card Success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**提交验证信息_new*/
+ (void) submitIdentityCarInfoWithImageString:(NSString *)imageString trueName:(NSString *) trueName cardType:(NSString *)card_id card:(NSString *)card Success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取实名认证信息*/
+ (void)getIdentifyInfoWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取某规格的商品信息--拓展信息*/
+ (void)getProductExtForPreviewOrderWithId:(NSString *)goodsId spec:(NSString *)spec success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取用户可用的抵扣券*/
+ (void)getValidDeductionCouponListWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**订单确认*/
+ (void)previewOrderWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**创建订单*/
+ (void)createOrderWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**可用的配送站*/
+ (void)orderGetValidDeliveryWithModel:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**添加配送站地址*/

/**再次购买*/
+ (void)orderBuyAgainWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取订单统计数量*/
+ (void)getOrderStatusCountSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取全部订单列表--status , 分割*/
+ (void)getOrderInfoWithOrderStatus:(NSString *)orderStatus andPage:(NSString *)page andLimit:(NSString *)limit success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**关闭订单*/
+ (void)closeOrderWithOrderId:(NSString *)orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取订单详情*/
+ (void)getDetailOrderInfoWithId:(NSString *)orderId supply:(NSString *)supply success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**退货前*/
+ (void)getRejectOrderInfoWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**退货*/
+ (void)cancelOrderGoodsWithId:(NSString *) orderId ReasonId:(NSString *) reasonId moneyType:(NSString *)moneyType money:(NSString *)money remark:(NSString *) remark success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**收货*/
+ (void)receiveGoodsWithId:(NSString *) orderId andPayPwd:(NSString *) payPwd success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**退款*/
+ (void)refoundMoneyWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**提醒发货*/
+ (void)orderRemindOrderSendWithOrderId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**单个 追加评价*/
+ (void)evaluateZhuiEvaluationWithParams:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**多个 追加评价*/
+ (void)evaluateZhuiEvaluationsWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取支付信息*/
+ (void)getPayInfoWithIds:(NSString *)orderId tname:(NSString *)tname success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**支付*/
+(void)payMoneyWithPayname:(NSString *)payname payids:(NSString *)payids otherInfo:(NSDictionary *)otherInfo success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**快速买单准备*/
+(void)getCashierInfoWithPayname:(NSString *)payname payids:(NSString *)payids success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**查看支付结果*/
+ (void)lookPayResultStatusWithTradeNumber:(NSString *)tradeNumber success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;



/**获取省级地域*/
+ (void)getProvinceInfoWithId:(NSString *)idNum level:(NSString *) level success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取完整的所选的地域名称*/
+ (void)getFullAreaInfoWithId:(NSString *)idNum  success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取银行卡类型*/
+ (void)getBankTypeSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**添加银行卡*/
+ (void)addBankCardWithBankName:(NSString *)bankName bankId:(NSString *)bankId bankCard:(NSString *)bankCard bankAddress:(NSString *)bankAddress success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**添加银行卡*/
+ (void)addBankCardWithBankName:(NSString *)bankName bankId:(NSString *)bankId bankCard:(NSString *)bankCard bankAddress:(NSString *)bankAddress passWord:(NSString *)password success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取绑定的银行卡*/
+ (void)getBindBankCardInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**编辑银行卡*/
+ (void)updateBankCardWithBankName:(NSString *)bankName bankId: (NSString *) bankId bankCard: (NSString *) bankCard bankAddress: (NSString *) bankAddress userId: (NSString *) userId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**编辑银行卡*/
+ (void)updateBankCardWithBankName:(NSString *)bankName bankId: (NSString *) bankId bankCard: (NSString *) bankCard bankAddress: (NSString *) bankAddress userId: (NSString *) userId passWord:(NSString *)pwd success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**设为默认*/
+ (void)defaultBankCardWithBankId:(NSString *)bankId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**删除*/
+ (void)deleteBankCardWithBankId:(NSString *)bankId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**添加评价*/
+(void)addEvaluteUrlWithOrderId:(NSString *) orderId eva:(NSArray *) eva success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取物流信息*/
+(void)getLogisticsInfoWithId:(NSString *) orderId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**添加收货地址*/
+ (void)addAddressWithProvince:(NSString *)provinceId city:(NSString * )cityId county:(NSString * )countyId town:(NSString * )townId provinceName:(NSString *)provinceName cityName:(NSString * )cityName countyName:(NSString * )countyName townName:(NSString * )townName address:(NSString *)address contact:(NSString *)contact tel:(NSString *)tel isDefault:(NSString *) isDefault success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**更新收货地址*/
+ (void)updateAddressWithId:(NSString *)addressId province:(NSString *)provinceId city:(NSString *)cityId county:(NSString *)countyId town:(NSString *)townId provinceName:(NSString *)provinceName cityName:(NSString * )cityName countyName:(NSString * )countyName townName:(NSString * )townName address:(NSString *)address contact:(NSString *)contact tel:(NSString *)tel isDefault:(NSString *)isDefault success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取收货地址*/
+ (void)getAddressListsSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

+ (void)getAppointAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**设置默认收货地址*/
+ (void)setDefaultAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**删除收货地址*/
+ (void)deleteAddressWithId:(NSString *)addressId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取店铺信息*/
+(void) getShopCarInfoWithFavType:(NSString *) favTypeId page:(NSString *) page limit:(NSString *) limit type:(NSString *) type success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
/**删除店铺收藏*/
+(void) deleteShopCarInfoWithFavId:(NSString *) favId success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取用户信息*/
+(void)getUserInfoSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
/**设置,更新用户信息*/
+(void)setUserInfoWithNickName:(NSString *) nickName sex:(NSString *) sex success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
/**根据编号获取用户信息*/
+(void)getOtherInfoWithUserName:(NSString *)userName success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**设置,更新用户信息*/
+(void)setUserInformationWithNickName:(NSString *)nickName success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

+(void)setUserInformationWithSex:(NSString *)sex success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

+(void)setUserInformationWithLogo:(NSString *)avatar success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**上传头像*/
+(void)upLoadUserInformationWithIconImage:(UIImage *)iconImage progress:(requestProgressBlock)progress success:(requestSuccessBlock)success failure:(requestFailureBlock)failure;

/**获取分类名称*/
+(void)getCategoryNameSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取分类子类目*/
+(void)getCategoryWith:(NSString *) category_id success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取分类的基本商品信息*/
+(void)getCategoryGoodsWithPage:(NSString *) page limit:(NSString *) limit cid:(NSString *) cid stype:(NSString *) stype success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


/**获取第三方登录方式*/
+(void)getSocialLoginListSuccess:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**取消第三方绑定*/
+(void)cancelSocialBindWithType:(NSString *) type success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**获取绑定信息*/
+ (void)getBindUserOrSocialInformationWithType:(NSString *) type code:(NSString *) code openid:(NSString *)openid success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;

/**绑定用户*/
+ (void)bindUserBySocialWithParam:(NSDictionary *) param success:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;


#pragma mark  - - - - - - - --  - - - 联系人群组 ---
/*---------------------------------------------------联系人群组-begin-----------------------------------------------*/

/*
 获取获取与我有联系的所有用户列表   关系(所有0|好友1|关注的2|粉丝3)
 */
+ (void)getRelationListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

 /*
 获取好友列表
 */
+ (void)getFriendsListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取我的关注列表
 */
+ (void)getAttentionListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取我的粉丝列表
 */
+ (void)getFansListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取我的群组列表
 */
+ (void)getMyGroupListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取我的黑名单列表
 */
+ (void)getMyBlackListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 我的聊天设置
 */
+ (void)getMyChatSetWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 设置我的聊天
 */
+ (void)setMyChatSetWithParams:(NSDictionary *)dic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取用户信息
 */
+ (void)getUserInfoWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 设置备注信息
 */
+ (void)setUserRemarkNameWithUid:(NSString *)uid remarkName:(NSString *)remark success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 设置免打扰和屏蔽
 id             必须	int	目标用户id
 is_shield      2选1	int	是否屏蔽(0否|1是)
 is_not_disturb	2选1	int	是否免打扰(0否|1是)
 */
+ (void)setContact:(NSString *)uid isShield:(NSInteger)is_shield isNotDisturb:(NSInteger)is_not_disturb success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 关注
 */
+ (void)attentionFansWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 取消关注
 */
+ (void)attentionFansCancelWithUid:(NSString *)uid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 新建群聊
 */
/*
 name                   string	群名称
 desc                   string	群描述
 type                   int     群类型(0普通)
 public                 string	是否公开群(1是|0否)
 maxusers               string	群成员上限(200-2000)
 public                 string	加公开群是否需要批准(0否|1是)
 allowinvites           string	私有群时：是否允许成员邀请(0否|1是)
 invite_need_confirm	string	公开群时：入群是否需要审核(0否|1是)
 id_str                 string	邀请的用户拼接字符串(123,123456)
 */
+ (void)addGroup:(NSDictionary *)groupData success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 生成群头像
 */
+ (void)generateAvatarForGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 删除群聊
 */
+ (void)deleteGroupWithGroupId:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取群成员
 id         必须	string	群id
 id_str     可选	string	用户id(无则返回所有成员)
 */
+ (void)getMemberListWithIds:(NSString *)ids ofGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 添加群成员
 */
+ (void)addMembers:(NSString *)id_str forGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 移除群成员
 */
+ (void)deleteMembers:(NSString *)id_str forGroup:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取群信息
 id             必须	int	群id
 is_get_member	可选	int	是否获取群成员
 order          可选	int	群成员排序(time按时间|spell按拼音)
 */
+ (void)fetchGroupInfoWithGroupId:(NSString *)gId getMembers:(int)is_get_member order:(NSString *)order success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 用户是否在群中
 id             必须    int    群id
 */
+ (void)CheckUserInGroupWithGroupId:(NSString *)gId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 设置群是否屏蔽
 id             必须	int	群id
 is_shield      必须	int	屏蔽群(0否|1是)
 is_not_disturb	必须	int	免打扰(0否|1是)
 */
+ (void)setShield:(NSInteger)is_shield notDisturb:(NSInteger)is_not_disturb forGroup:(NSString *)gid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 更新群信息
 id         必须	int     群id
 name       可选	string	群名称
 avatar     可选	string	群头像
 desc       可选	string	群描述
 public     可选	string	是否公开群(1是|0否)
 approval	可选	string	加群是否需审核(0否|1是)
 */
+ (void)updateGroupInfoWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 获取群用户昵称和头像
 @param ids 群用户id拼接字符串
 */
+ (void)getNickNameWithIds:(NSString *)ids success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 禁言|解禁群成员
 id         必须	string	群id
 type       必须	string	操作(1禁言|0解禁)
 id_str     必须	string	用户id
 duration	可选	string	秒数(默认24*3600)
 */
+ (void)setMemberMuteWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 禁言成员列表
 id	必须	string	群id
 */
+ (void)getMemberMuteListWithGroupId:(NSString *)gid success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/*
 根据手机通讯录筛选已注册用户和好友
 list 通讯录信息格式:[{"name":"one","phone":["13566666666","13577788888"]},{"name":"two","phone":["15111111111"]}]
 */
+ (void)getDeviceFansWithContents:(NSArray *)list success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/*---------------------------------------------------联系人群组-end------------------------------------------------*/


#pragma mark  - - - - - - - --  - - -


/***************************************************粉丝圈begin********************************************************/
//获取粉丝列表
+ (void)getFansWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取新的动态列表
+(void)getNewStatusArrayWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取之前的动态列表
+(void)getOldStatusArrayWithPageNum:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//根据用户名获取新的动态列表
+(void)getNewStatusArrayWithUID:(NSString *)userName Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//根据用户名获取之前的动态列表
+(void)getOldStatusArrayWithUID:(NSString *)userName PageNum:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//点赞
+(void)thumbUpWithStatusId:(NSNumber *)idNum  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//取消点赞
+(void)cancelThumbUpWithStatusId:(NSNumber *)idNum  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//删除动态
+(void)deleteStatusWithStatusId:(NSNumber *)idNum  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//评论
+(void)commentWithId:(NSNumber *)idNum andContent:(NSString *)content WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//回复
+(void)replyWithId:(NSNumber *)replyIdNum andContent:(NSString *)content  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//删除评论  回复
+(void)deleteCommentWithId:(NSNumber *)replyIdNum WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//修改用户粉丝圈背景图
+(void)updateUserFancircleBgImgWithImgStr:(NSString *)imgStr WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//粉丝圈只发表文字,直接上传
+(void)addStatusOnlyWordWithDic:(NSDictionary *)paraDic  WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//粉丝圈多文件上传   多张图片
+(void)upLoadPhotosWithDic:(NSDictionary *)paraDic andDataArray:(NSArray *)dataArray   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail;
//单文件上传 图片或者视频
+(void)upLoadDataIsVideo:(BOOL)isVideo WithDic:(NSDictionary *)paraDic andData:(NSObject *)data   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail;
//交易所单文件上传 图片或者视频
+(void)JYSupLoadDataIsVideo:(BOOL)isVideo WithDic:(NSDictionary *)paraDic andData:(NSObject *)data   WithSuccess:(requestSuccessBlock)success fail:(requestFailureBlock)fail;
/***************************************************粉丝圈end********************************************************/
/***************************************************钱包begin********************************************************/
//获取钱包首界面信息
+(void)getWalletInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取钱包现金、购物券等详情记录
+(void)getAccountDetailWithParamDic:(NSDictionary *)paraDic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取转账规则
+(void)getTransferRulesWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//提交转账
+(void)commitTransAccountWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取新的转账记录
+(void)getNewTransferAccountRecordWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取旧的转账记录
+(void)getOldTransferAccountRecordWithPage:(NSNumber *)page Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取提现钱包
+(void)getWithDrawWalletWithType:(NSString *)type Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取提现规则
+(void)getWithDrawRuleWithWalletType:(NSString *)walletType  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//提交提现
+(void)commitWithDrawWithParamDic:(NSDictionary *)dic  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//撤回提现
+(void)cancelWithDrawWithId:(NSString *)recordId  Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//新的提现记录
+(void)getNewWithDrawRecordWithWalletType:(NSString *)walletType WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//旧的提现记录
+(void)getOldWithDrawRecordWithWalletType:(NSString *)walletType WithPage:(NSNumber *)page WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//充值记录
+(void)getRechargeRecordWithDic:(NSDictionary *)paraDic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//充值规则@param success http://shangcheng.xsy.dsceshi.cn/api/v1/user/recharge/Add/type/pc@param failure http://shangcheng.xsy.dsceshi.cn/api/v1/user/recharge/Add/type/pc
+(void)getRechargeRule:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure ;
 //充值提交
+(void)rechargeSubmissionParams:(NSDictionary *)params succrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
+ (void)rechargeListSuccrss:(XSAPIClientSuccess)success failure:(XSAPIClientFailure)failure;
//获取钱包记录交易类型
+(void)getWalletRecordTradeTypeWithWalletType:(NSString *)walletType Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************钱包end********************************************************/


/***************************************************商城begin********************************************************/
//获取商城首页猜你喜欢商品列表信息
+(void)getMallGussYouLikeGoodsListInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取更多商城首页猜你喜欢商品列表信息
+(void)getMallMoreGussYouLikeGoodsListInfoWithPage:(NSNumber *)page Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取商品详情
+(void)getGoodsDetailWithParams:(NSDictionary *)params success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取商品详情推荐商品
+(void)getGoodsDetailRecommendGoodsWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//根据参数字典获取商品列表
+(void)getADGoodsListWithParaDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取品牌列表
+(void)getBrandListParaDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**获取商城首页滚动式图信息 */
+(void) getMallScrollViewADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
/**获取商城首页滚动式图下的菜单*/
+(void)getMallMenuInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
///** 获取公告列表信息 */
+(void)getMallNoticeInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
/**
 获取公告列表信息
 */
+(void)getMallNoticeInfoWithPage:(NSInteger)page limit:(NSInteger)limit success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;

//公告详情
+(void)getNoticeDetailInfoWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

//商品图文详情
+(void)getGoodsDetailPictureInfoWithDic:(NSDictionary *)paraDic Success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

///**获取左边广告 */
//+(void)getMallLeftADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
///**获取右上方广告 */
//+(void)getMallRightTopADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;


+(void)getMobile_All_InfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure ;


/**获取右右下方广告*/
//+(void)getMallRightBottomRightADInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)failure;
/**获取商城首页分类信息（服装服饰、家居用品等）*/
+(void) getMallCategoryInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//根据规格获取商品详情
+(void)getGoodsInfoWithGoodsID:(NSString *)goodsID spec:(NSString *)specStr  success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取商品评价 好评、中评、差评
+(void)getGoodsEvaluateWithParaDic:(NSDictionary *)paraDic success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//获取商品的商家统计信息
+(void)getGoodsStoreStatisticsInfoWithStoreId:(NSString *)storeId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
////获取批发商品不同规格的数据
//+(void)getWholeGoodsSpecInfoWithProduct_id:(NSString *)pId success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

+ (void)getMallFloorInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************商城end********************************************************/




+ (void) getCommodityProduct:(NSString *)str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


// 技能
/**预置技能列表*/
+ (void)getSkillListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**新增技能*/
+ (void)addSkills:(NSString *)skill_str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**我擅长的技能列表*/
+ (void)getMySkillListWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**设置我擅长的技能*/
+ (void)setMySkills:(NSString *)skill_str success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**获取技能交换列表*/
+(void)getSkillExchangeListWithParams:(NSDictionary *)params WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**发布技能交换
 skill_id_str_gt	必须	string	擅长技能id
 skill_id_str_wt	必须	string	想学技能id
 img_str	可选	string	图片(最多3张)
 remark	可选	string	备注
 */
+ (void)addSkillExchangeWithData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/** 删除技能交换*/
+ (void)deleteSkillExchangeWithId:(NSString *)se_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**点赞
 flag		int	操作(1点赞|0取消)
 skill_exchange_id	必须	string	技能交换id
 */
+ (void)thumbupForSkillExchange:(NSString *)skill_exchange_id flag:(int)flag success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**评论*/
+ (void)commentForSkillExchangeWithCommentData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**删除评论*/
+ (void)deleteCommentForSkillExchangeWithCommentId:(NSString *)comment_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

/**回复*/
+ (void)replyForSkillExchangeWithReplyData:(NSDictionary *)data success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/**删除回复*/
+ (void)deleteReplyForSkillExchangeWithReplyId:(NSString *)reply_id success:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;


/***************************************************************************找回 重置密码begin************************************************************************************/
//修改支付密码
+(void)resetPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//修改登录密码
+(void)resetLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//找回支付密码
+(void)findPayPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
//找回登录密码
+(void)findLoginPwdWithParamDic:(NSDictionary *)dic WithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************************************找回 重置密码end************************************************************************************/

/***************************************************************************版本更新begin************************************************************************************/
+(void)getVersionInfoWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;
/***************************************************************************版本更新end************************************************************************************/

/* 我的客服
 */
+ (void)getCustomerServicesWithSuccess:(XSAPIClientSuccess)success fail:(XSAPIClientFailure)fail;

@end


NS_ASSUME_NONNULL_END
