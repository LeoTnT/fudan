//
//  ErrorReportingController.h
//  oooo
//
//  Created by apple on 2017/8/3.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UpDataAction)();

@interface ErrorReportingController : UIViewController

@property (nonatomic ,copy)UpDataAction upAction;


@end


@interface TakePhotoView : UIView

@property (nonatomic ,strong)NSMutableArray *imageData;
@property (nonatomic ,copy)void (^isTakePhoto)();
@property (nonatomic ,copy)void (^getImageNumber)(NSInteger number);
@end

@interface TakePhotoCancel : UIButton
@property (nonatomic ,strong)UIImageView *imageView1;
@property (nonatomic ,copy)void (^ButtonCancel)(NSInteger);
@property (assign,nonatomic) NSInteger index;
@property (strong,nonatomic) UIButton *btn;
@end
