//
//  XSCacheManager.m
//  App3.0
//
//  Created by mac on 17/5/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSCacheManager.h"
#import "XSClearCache.h"

static XSCacheManager *cacheManager = nil;
@implementation XSCacheManager

#pragma mark -sharedInstance
+ (XSCacheManager *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cacheManager = [[XSCacheManager alloc] init];
    });
    return cacheManager;
}

- (NSString *)getAllCacheSize
{
    // docment文件夹 用来保存应由程序运行时生成的需要持久化的数据， iTunes会自动备份该目录
//    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    // cache size
//    NSLog(@"doc cache size: %@",[XSClearCache getCacheSizeWithFilePath:docPath]);
    
    // library 用来存储程序的默认设置和其他状态信息，iTunes也会自动备份该目录。
//    NSString *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
//    NSLog(@"library cache size: %@",[XSClearCache getCacheSizeWithFilePath:libraryPath]);
    
    // Library/Caches 用来存放缓存文件，iTunes不会备份此目录，此目录下的文件不会在程序退出后删除，一般存放体积比较大但又不太重要的文件。
    NSString *cachesPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSLog(@"caches cache size: %@",[XSClearCache getCacheSizeWithFilePath:cachesPath]);
    
    // tmp 保存应用程序的临时文件夹
    NSString *tmpPath = NSTemporaryDirectory();
    NSLog(@"tmp cache size: %@",[XSClearCache getCacheSizeWithFilePath:tmpPath]);
    
    return [XSClearCache getCacheSizeWithFilePath:cachesPath];
}

- (void)clearLocalCache
{
//    // docment文件夹 用来保存应由程序运行时生成的需要持久化的数据， iTunes会自动备份该目录
//    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    [XSClearCache clearCacheWithFilePath:docPath];
//    
//    // library 用来存储程序的默认设置和其他状态信息，iTunes也会自动备份该目录。
//    NSString *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
//    [XSClearCache clearCacheWithFilePath:libraryPath];
    
    // Library/Caches 用来存放缓存文件，iTunes不会备份此目录，此目录下的文件不会在程序退出后删除，一般存放体积比较大但又不太重要的文件。
    NSString *cachesPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    [XSClearCache clearCacheWithFilePath:cachesPath];
    
    // tmp 保存应用程序的临时文件夹
    NSString *tmpPath = NSTemporaryDirectory();
    [XSClearCache clearCacheWithFilePath:tmpPath];
}

- (void)clearConversationCache
{
#ifdef ALIYM_AVALABLE
    [[[SPKitExample sharedInstance].ywIMKit.IMCore getConversationService] asyncRemoveAllConversationAndMessage:^(NSError *error) {
        
    }];
#elif defined EMIM_AVALABLE
    NSArray *conArray = [[EMClient sharedClient].chatManager getAllConversations];
    [[EMClient sharedClient].chatManager deleteConversations:conArray isDeleteMessages:YES completion:^(EMError *aError) {
        [[ChatHelper shareHelper].replyVC refreshConversationLists];
        
    }];
#else
    
#endif
    
}
@end
