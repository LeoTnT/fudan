//
//  XSCacheManager.h
//  App3.0
//
//  Created by mac on 17/5/2.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSCacheManager : NSObject
+ (XSCacheManager *)sharedInstance;

/*
 * 缓存大小
 */
- (NSString *)getAllCacheSize;

/*
 * 清除所有缓存
 */
- (void)clearLocalCache;

/*
 * 清除会话缓存
 */
- (void)clearConversationCache;
@end
