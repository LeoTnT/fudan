//
//  YKNodeModel.h
//  MutableLevelTableView
//
//  Created by 杨卡 on 16/9/8.
//  Copyright © 2016年 杨卡. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKNodeModel : NSObject

@property (nonatomic, copy) NSString *category_supply_id;

@property (nonatomic, copy) NSString *category_supply_name;

@property (nonatomic, copy) NSString *parent_id;

@property (nonatomic ,strong)NSArray *_child;





@property (nonatomic, assign) NSUInteger level;// depth in the tree sturct

@property (nonatomic, assign) NSInteger leaf;

@property (nonatomic, assign) BOOL root;

@property (nonatomic, assign) BOOL isOpen;
@end
