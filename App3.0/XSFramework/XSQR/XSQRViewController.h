//
//  XSQRViewController.h
//  App3.0
//
//  Created by mac on 17/5/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSBaseViewController.h"

@interface XSQRViewController : XSBaseViewController
@property (copy, nonatomic) NSString *qrTitle;
@property (copy, nonatomic) NSString *qrDesc;
@property (copy, nonatomic) NSString *qrContent;
@end
