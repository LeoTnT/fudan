//
//  XSQR.m
//  App3.0
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSQR.h"

@implementation XSQRModel
@end

@implementation XSQR
/**
 *  mobile/load/index?          通用路径
 *  mobile/product/details?     商品二维码
 *  mobile/supply/index?        商家二维码
 *  mobile/supply/index?        商家快捷支付
 **/
+ (UIImage *)createQrImageWithContentString:(NSString *)content type:(XSQRType)type
{
    // 1. 实例化二维码滤镜
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // 2. 恢复滤镜的默认属性
    [filter setDefaults];
    
    // 3. 将字符串转换成NSData
    //    NSLog(@"%@",content);
    //    NSDictionary *dic = @{@"content":content,@"type":@(type)};
    //    NSError *error;
    //    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *urlString;
    NSString *typeString;
    NSString *codeString;//最终的二维码url
    switch (type) {
        case XSQRTypeUser:
            urlString = [NSString stringWithFormat:@"%@/wap/#/download?",ImageBaseUrl];
            typeString = @"user";
            codeString = [NSString stringWithFormat:@"%@qrtype=%@&id=%@",urlString,typeString,[UserInstance ShardInstnce].uid];
            break;
        case XSQRTypePAY:
            codeString = content;
            break;
        case XSQRTypePRODUCT:
            codeString = content;
            break;
        case XSQRTypeSELLER:
            
            break;
        case XSQRTypeArtGroup:
            urlString = [NSString stringWithFormat:@"%@/wap/#/download?",ImageBaseUrl];
            typeString = @"TYPE_ART_GROUP";
            codeString = [NSString stringWithFormat:@"%@qrtype=%@&id=%@",urlString,typeString,content];
            break;
        case XSQRTypeGroup:
        {
            urlString = [NSString stringWithFormat:@"%@/wap/#/download?",ImageBaseUrl];
            typeString = @"TYPE_NORMAL_GROUP";
            NSDate *datenow = [NSDate date];
            NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
            codeString = [NSString stringWithFormat:@"%@qrtype=%@&id=%@&TIME=%@",urlString,typeString,content,timeSp];
        }
            break;
        case XSQRTypeSellerPay:
            urlString = [NSString stringWithFormat:@"%@/wap/#/supply/index?",ImageBaseUrl];
            typeString = @"supply_pay";
            codeString = [NSString stringWithFormat:@"%@qrtype=%@&id=%@",urlString,typeString,[UserInstance ShardInstnce].uid];
            break;
            
        case XSQRTypeArticleShare:
            codeString = content;
            break;
        case XSQRTypeMenuList:
            codeString = content;
            break;
        case XSQRTypeRegisterEnter:
            codeString = content;
            break;
        case XSQRTypeShareInfo:
            codeString = content;
            break;
        case XSQRTypeUserInfo:
            codeString = content;
            break;
        case XSQRTypeCreatePoster:
            codeString = content;
            break;
        case XSQRTypeSupply:
            codeString = content;
            break;
            
    }
    
    //    NSString *str = [NSString stringWithFormat:@"%@qrtype=%@&id=%@",urlString,typeString,[UserInstance ShardInstnce].uid];
    
    NSData *data =[codeString dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4. 通过KVO设置滤镜inputMessage数据
    [filter setValue:data forKey:@"inputMessage"];
    
    // 5. 获得滤镜输出的图像
    CIImage *outputImage = [filter outputImage];
    
    // 根据CIImage生成指定大小的UIImage
    CGRect extent = CGRectIntegral(outputImage.extent);
    CGFloat scale = MIN(200/CGRectGetWidth(extent), 200/CGRectGetHeight(extent));
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:outputImage fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGColorSpaceRelease(cs);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    
    return [UIImage imageWithCGImage:scaledImage];
}
@end
