//
//  XSQRViewController.m
//  App3.0
//
//  Created by mac on 17/5/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSQRViewController.h"
#import "UserInstance.h"

@interface XSQRViewController ()

@end

@implementation XSQRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self actionCustomLeftBtnWithNrlImage:@"nav_back" htlImage:nil title:nil action:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.view.backgroundColor = [UIColor whiteColor];
    [self addSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSubviews {
    CGFloat space = 10.0f;
    CGFloat width = mainWidth/2;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(space, 100, width-2*space, 30)];
    title.text = self.qrTitle;
    title.textColor = mainGrayColor;
    title.font = [UIFont systemFontOfSize:18];
    title.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:title];
    
    UIImageView *qrImgView = [[UIImageView alloc] initWithFrame:CGRectMake((mainWidth-width)/2, CGRectGetMaxY(title.frame)+20, width, width)];
    UIImage *qrImage = [XSQR createQrImageWithContentString:self.qrContent type:XSQRTypeUser];
    [qrImgView setImage:qrImage];
    [self.view addSubview:qrImgView];
    
    
    UILabel *desLabel = [[UILabel alloc] initWithFrame:CGRectMake(space, CGRectGetMaxY(qrImgView.frame)+20, mainWidth-2*space, 30)];
    desLabel.text = self.qrDesc;
    desLabel.textColor = mainGrayColor;
    desLabel.font = [UIFont systemFontOfSize:14];
    desLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:desLabel];
}

@end
