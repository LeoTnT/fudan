//
//  XSQR.h
//  App3.0
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, XSQRType) {
    XSQRTypeUser = 1,   // 个人
    XSQRTypeSupply,
    XSQRTypePAY,
    XSQRTypePRODUCT,
    XSQRTypeSELLER,
    XSQRTypeGroup,      // 群组
    XSQRTypeArtGroup,
    XSQRTypeSellerPay,
    XSQRTypeArticleShare,
    XSQRTypeMenuList,
    XSQRTypeRegisterEnter,
    XSQRTypeShareInfo,
    XSQRTypeUserInfo,
    XSQRTypeCreatePoster,
};

@interface XSQRModel : NSObject
@property (nonatomic, copy)NSString *content;
@property (nonatomic, strong)NSNumber *type;
@end

@interface XSQR : NSObject
+ (UIImage *)createQrImageWithContentString:(NSString *)content type:(XSQRType)type;
@end
