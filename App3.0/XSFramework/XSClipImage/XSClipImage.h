//
//  XSClipImage.h
//  App3.0
//
//  Created by mac on 2017/4/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSClipImage : NSObject
//按比率压缩图片
+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;
//压缩图片到固定尺寸
+ (UIImage*)imageByScalingAndCroppingWithImage:(UIImage *)image ForSize:(CGSize)targetSize;
@end
