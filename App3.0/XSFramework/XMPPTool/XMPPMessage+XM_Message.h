//
//  XMPPMessage+XM_Message.h
//  BIT
//
//  Created by Sunny on 2018/5/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>

@interface XMPPMessage (XM_Message)


- (NSString *)messageExtension;
- (NSDictionary *)messageEXTDic;
@end
