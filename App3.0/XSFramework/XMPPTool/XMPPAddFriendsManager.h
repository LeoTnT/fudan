//
//  AddFriendsManager.h
//  tigaseXMPP
//
//  Created by apple on 2018/1/3.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPAddFriendsManager : NSObject


/**
 好友列表 更改
 */
@property (nonatomic ,strong,readonly)RACSubject *addFriedSignal;

+(instancetype) shareManager;


/**
 加好友 传 UID  请求好友数据
 
 @param jidString jidString description
 */
+ (void)addBuddyWithJid:(NSString *)jidString ;

/**
 删除好友
 
 @param jidString jidString description
 */
+ (void)removeBuddyWithJid:(NSString *)jidString;
@end
