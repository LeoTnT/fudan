//
//  XMPPManager.h
//  XMPPDemo
//
//  Created by apple on 2018/1/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMChatMessage.h"
// 用于提供电子名片增删改查操作
#import <XMPPFramework/XMPPvCardTempModule.h>
// 代表电子名片Modal
#import <XMPPFramework/XMPPvCardTemp.h>
// 头像模块:可以根据代理方法获取头像变化的时机
#import <XMPPFramework/XMPPvCardAvatarModule.h>
// 自动连接
#import <XMPPFramework/XMPPConstants.h>

// 花名册模块(好友列表)
#import <XMPPFramework/XMPPRoster.h>
#import <XMPPFramework/XMPPRosterMemoryStorage.h>




// 登录状态
typedef NS_ENUM(NSUInteger, XIM_STREAMSTATE) {
    XIM_XMPP_NOTCONNECT_NEVER = 0,
    
    XIM_XMPP_CONNECTING = 100,//正在连接
    XIM_XMPP_CONNECT,  //  已经连接  连接成功
    
    XIM_XMPP_NOTCONNECT =200, // 没有连接
    
    XIM_XMPP_REGISTERFAILED  = 300,// 注册失败
    XIM_XMPP_AUTHFAILD, // 身份验证失败
    
    XIM_XMPP_TIMEOUT, // 连接超时
    XIM_XMPP_DISCONNECTED ,// 断开连接
    XIM_XMPP_CONNECTDEFILED, // 连接失败
    XIM_XMPP_COUNT_ERROR // 单点登录
};



typedef void(^XIM_StreamState)(XIM_STREAMSTATE type);


@interface XMPPManager : NSObject
@property (nonatomic ,strong ,readonly) XMPPStream *stream;

@property (nonatomic ,strong ,readonly)XMPPMUC *xmppMUC;

/**
 连接状态
 */
@property (nonatomic,assign) XIM_STREAMSTATE state;


/**
 *  自动连接
 */
@property (nonatomic, strong) XMPPReconnect *reconnect;

/**
 心跳包
 */
@property (nonatomic ,strong)XMPPAutoPing *xmppAutoPing;

@property (nonatomic ,strong)XMPPPubSub *pubSub;

/**
 *  花名册 -- 好友列表
 // */
@property (nonatomic, strong) XMPPRoster *roster;

@property (nonatomic, strong) XMPPRosterMemoryStorage *xmppRosterMemoryStorage;

/* - - -  - - - - - -*/
@property (nonatomic ,strong )RACSubject *deletedMessageSubject;

@property (nonatomic ,strong )RACSubject *messageSubject;

@property (nonatomic ,strong )RACSubject *roomListSubject;


@property (nonatomic ,strong)NSMutableArray *roomList;


/* - - -  - - - - - -*/

@property (nonatomic,copy) NSString *callTime;


@property (nonatomic ,assign)XMPPMessageSendState sendState;

@property (nonatomic ,copy) NSString *fileName;
@property (nonatomic ,assign)BOOL isLog;

+(instancetype)sharedManager;

- (void) configServer;
- (void) connectionServer;

- (void)sendOnlineMessage;
- (void)sendOfflineMessage;
- (void)xmppUserLogout;
- (void) disconnectdXmppStream;


/**
 显示提示语

 @param type type description
 @return return value description
 */
+ (NSString *) getStreamConnet:(XIM_STREAMSTATE)type;



+ (void) xmppUserLogIn:(NSString *)userName complment:(void(^)(XIM_STREAMSTATE connectedType))complent;

+ (void)queryRoomOfJoined;

+ (XMPPJID *) jidWithString:(NSString *)jid;


// */
//+ (void) getGroupWithService;

+ (NSString *) getJidStringWithNnumber:(NSString *)number;



+(XMPPJID *) getJIDWithRoomName:(NSString *)name;
//+ (void) senderMessage:(NSString *)message to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

@end
