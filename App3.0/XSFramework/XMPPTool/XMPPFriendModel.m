//
//  XMPPFriendModel.m
//  App3.0
//
//  Created by Sunny on 2018/6/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPFriendModel.h"
#import "XSApi.h"
@implementation XMPPFriendModel


+ (void) updataUserInfor:(NSString *)uid relation:(NSNumber *)number{
     [XMPPFriendModel signalWithContactID:uid relation:number];
 }

+ (void) signalWithContactID:(NSString *)UID relation:(NSNumber *)number {
    
    NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:UID];
    if (contacts != nil && contacts.count > 0) {
        // 本地数据库存在
        ContactDataParser *contact = contacts[0];
        
        
        [[DBHandler sharedInstance] updateContactWithKey:@"relation" value:[number stringValue] byUid:UID];
        
        
        
    } else {
        // 本地数据库不存在
        
        [HTTPManager getUserInfoWithUid:UID success:^(NSDictionary * dic, resultObject *state) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            
            if (state.status) {
                
                    ContactDataParser *cdParser = [ContactDataParser mj_objectWithKeyValues:parser.data];
 
                    NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
                     [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
 
            }
        } fail:^(NSError * _Nonnull error) {
            
        }];
    }
}



+ (void) getUserInfor:(NSString *)uid {
    [HTTPManager getUserInfoWithUid:uid success:^(NSDictionary * dic, resultObject *state) {
        
        if (state.status) {
            UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
            UserInfoDataParser *_userInfo = parser.data;
            ContactDataParser *cdParser = [[ContactDataParser alloc] init];
            cdParser.uid = _userInfo.uid;
            cdParser.username = _userInfo.username;
            cdParser.nickname = _userInfo.nickname;
            cdParser.avatar = _userInfo.logo;
            cdParser.relation = _userInfo.relation;
            cdParser.mobile = _userInfo.mobile;
            cdParser.remark = _userInfo.remark;
            [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
            
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}

@end
