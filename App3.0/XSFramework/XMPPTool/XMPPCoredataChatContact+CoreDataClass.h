//
//  XMPPCoredataChatContact+CoreDataClass.h
//  
//
//  Created by Sunny on 2018/6/4.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface XMPPCoredataChatContact : NSManagedObject


@end

NS_ASSUME_NONNULL_END

#import "XMPPCoredataChatContact+CoreDataProperties.h"
