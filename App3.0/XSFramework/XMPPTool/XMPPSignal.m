//
//  XMPPSignal.m
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPSignal.h"
#import "XMPPCoredataChatContact+CoreDataProperties.h"

@implementation XMPPSignal

-(RACSubject *)messageSubject {return [XMPPManager sharedManager].messageSubject;}



+ (void) configServer {

    [[XMPPManager sharedManager] configServer];
}


/*
 <message type="chat" to="7@localhost.localdomain/TigaseMessenger"><received xmlns="urn:xmpp:receipts" id="3D692240-A798-4D01-8916-94F1F0A4E267"/>
 </message>
 */

+ (void) sendReceiveMessageID:(NSString *)jid {
    if (isEmptyString(jid)) return;
    
    XMPPMessage *message = [XMPPSignal messageBody:nil to:jid];
    NSXMLElement *received = [NSXMLElement elementWithName:@"received" xmlns:@"urn:xmpp:receipts"];
    [message addChild:received];
    [[XMPPManager sharedManager].stream sendElement:message];
    
 
    
}

 
+ (void) sendMessage:(NSString *)text to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state{
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:text  to:jid];
    [message addReceiptRequest];
    [[XMPPManager sharedManager].stream sendElement:message];
   if (state)  state([XMPPManager sharedManager].sendState);
}

+ (void) sendMessage:(NSString *)text to:(NSString *)jid ext:(NSDictionary *)ext complete:(void(^)(XMPPMessageSendState))state{
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:text  to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"ext" stringValue:ext.mj_JSONString]];
    [message addChild:receipt];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state)  state([XMPPManager sharedManager].sendState);
}

+ (void) sendImage:(NSString *)text to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state{
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:text  to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"image"]];
    [message addChild:receipt];
     [[XMPPManager sharedManager].stream sendElement:message];
    if (state)  state([XMPPManager sharedManager].sendState);
}

+ (void) sendVoice:(NSString *)text time:(NSString *)time to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state {
    
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageWithVoice:text time:time to:jid];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state) state([XMPPManager sharedManager].sendState);
    
}


+ (void) sendCMD:(NSString *)body to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state {
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:body to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"CMD"]];
    [message addChild:receipt];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state)  state([XMPPManager sharedManager].sendState);
}


/*
 <message id="xqlGDA" type="chat" to="17@localhost.localdomain"><thread>AfUj5qPmdGoPvmbCGm</thread><request xmlns="urn:xmpp:receipts"/><x xmlns="jabber:x:oob">
 <type>video</type>
 <displayName>xxx.mp4</displayName>
 <fileSize>2.0M</fileSize>
 <time>10</time>
 <thumbPath>http://122.6.144.114:58088/assist/uploadFolder/20180316/20180606183504528.jpg</thumbPath>
 </x>
 <body>http://122.6.144.114:58088/assist/uploadFolder/20180316/20180606183504528.mp4</body>
 </message>
 */

+ (void) sendWithVideo:(NSString *)text time:(NSString *)time to:(NSString *)jid
                 image:(NSString *)image  size:(NSString *)size
                  name:(NSString *)displayName
              complete:(void(^)(XMPPMessageSendState))state  {
    
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageWithVideo:text time:time to:jid image:image size:size name:displayName];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state) state([XMPPManager sharedManager].sendState);
}




+ (void) sendLocation:(NSString *)address latitude:(NSString *)lat
            longitude:(NSString *)lon to:(NSString *)jid
             complete:(void(^)(XMPPMessageSendState))state{
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:address   to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"location"]];
    [receipt addChild:[NSXMLElement elementWithName:@"latitude" stringValue:lat]];
    [receipt addChild:[NSXMLElement elementWithName:@"longitude" stringValue:lon]];
    [message addChild:receipt];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state) state([XMPPManager sharedManager].sendState);
}





+ (void) sendFileUrl:(NSString *)url displayName:(NSString *)displayName
            fileSize:(NSString *)size to:(NSString *)jid
            complete:(void (^)(XMPPMessageSendState))state{
    if (isEmptyString(jid)) return;
    XMPPMessage *message = [XMPPSignal messageBody:url  to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"file"]];
    [receipt addChild:[NSXMLElement elementWithName:@"displayName" stringValue:displayName]];
    [receipt addChild:[NSXMLElement elementWithName:@"fileSize" stringValue:size]];
    [message addChild:receipt];
    [[XMPPManager sharedManager].stream sendElement:message];
    if (state) state([XMPPManager sharedManager].sendState);
}

+ (XMPPMessage *) messageWithVoice:(NSString *)text time:(NSString *)time to:(NSString *)jid  {
    XMPPMessage *message = [XMPPSignal messageBody:text   to:jid];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"voice"]];
    [receipt addChild:[NSXMLElement elementWithName:@"time" stringValue:time]];
    [message addChild:receipt];
    return message;
}

+ (XMPPMessage *) messageWithVideo:(NSString *)text time:(NSString *)time
                                to:(NSString *)jid image:(NSString *)image
                              size:(NSString *)size name:(NSString *)displayName{
    
    XMPPMessage *message = [XMPPSignal messageBody:text  to:jid];
    NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [x addChild:[NSXMLElement elementWithName:@"type" stringValue:@"video"]];
    [x addChild:[NSXMLElement elementWithName:@"time" stringValue:time]];
    [x addChild:[NSXMLElement elementWithName:@"thumbPath" stringValue:image]];
    [x addChild:[NSXMLElement elementWithName:@"fileSize" stringValue:size]];
    [x addChild:[NSXMLElement elementWithName:@"displayName" stringValue:displayName]];
    [message addChild:x];
    return message;
}

+ (XMPPMessage *) messageBody:(NSString *)body to:(NSString *)jid {
    XMPPMessage *message ;
    
    if (!isEmptyString(jid)) {
        
        
        XMPPJID *xmJID;
        NSString *messageType;
        messageType =[jid containsString:XMPPR_ROOM_Regulation] ? @"groupchat": @"chat";
        xmJID = [XMPPManager jidWithString:jid];
        NSString *ID = [[XMPPManager sharedManager].stream generateUUID];
        message =[XMPPMessage messageWithType:messageType to:xmJID elementID:ID];
    }else{
        message =[XMPPMessage messageWithType:@"chat" to:nil];
    }
    
    [message addBody:body];
    return message;
}


/* _ _ _  _ __ _ _  _ __ _ _ _ _ _ _ _ _ _ __ _ _ _ _ _ _ _ _ _ _ */


+ (XMPPMessage *) roomSendRedPacket:(NSString *)text  ext:(NSDictionary *)ext {
    
    XMPPMessage *message = [XMPPMessage message];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:text];
    [message addChild:body];
    NSXMLElement *receipt = [NSXMLElement elementWithName:@"x" xmlns:XMPP_MESSAGE_VOICT_IMAGE];
    [receipt addChild:[NSXMLElement elementWithName:@"type" stringValue:@"redpacket"]];
    [receipt addChild:[NSXMLElement elementWithName:@"ext" stringValue:ext.mj_JSONString]];
    [message addChild:receipt];
    return message;
}

/**
 群聊转发图片，分享商品
 
 @param model model description
 @param contact contact description
 @param finash finash description
 */
+ (void) shareXM_GroupModel:(XM_TransModel *)model contact:(ConversationModel *)contact finash:(void(^)(NSString *title))finash {
    
    if (model.type == TranMessageShareCustom) {
        [contact.xmppRoomManager.xmppRoom sendMessageWithBody:model.messageText];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (finash) finash(@"分享成功");
        });
        
    } else if (model.type == TranMessageShareImage) {
        [XMPPSignal groupShareImage:model.imageData room:contact.xmppRoomManager.xmppRoom success:finash];
        
    } else {
        
    }
}


/**
 单聊转发图片，分享商品
 
 @param model model description
 @param contact contact description
 @param finash finash description
 */
+ (void) shareXM_SignalModel:(XM_TransModel *)model contact:(ConversationModel *)contact finash:(void(^)(NSString *title))finash {
    
    if (model.type == TranMessageShareCustom) {
        [XMPPSignal sendMessage:model.messageText to:contact.conversation ext:model.extDic complete:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (finash) finash(@"分享成功");
        });
    } else if (model.type == TranMessageShareImage) {
        [XMPPSignal signalShareImage:model.imageData to:contact.conversation success:finash];
        
    } else {
        [XMPPSignal sendMessage:model.messageText to:contact.conversation complete:^(XMPPMessageSendState state) {
            dispatch_async(dispatch_get_main_queue(), ^{
 
                if (finash) finash(@"分享成功");
            });
        }];
    }
    
}


+ (void) groupShareImage:(NSData *)data room:(XMPPRoom *)room success:(void(^)(NSString *message))finash {
    
    if (!room) {
        finash(@"分享失败");
        return;
    }
    [XMPPBaseMessage xmppUpimage:data progress:nil completed:^(NSError *error,NSString *url) {
        if (error) {
            finash(@"分享失败");
        }else{
            XMPP_RoomMessage *message = [XMPP_RoomMessage new];
            message.type = @"image";
            message.body = url;
            NSString *messageString = message.mj_JSONString;
            [room sendMessageWithBody:messageString];
            finash(@"分享成功");
        }
    }];
}

+ (void) signalShareImage:(NSData *)data to:(NSString *)jid success:(void(^)(NSString *message))finash {
    
    [XMPPBaseMessage xmppUpimage:data progress:nil completed:^(NSError *error,NSString *url) {
        if (error) {
            finash(@"分享失败");
        }else{
            if (!isEmptyString(url)) {
                [XMPPSignal sendImage:url to:jid complete:nil];
                finash(@"分享成功");
            }else{
                finash(@"分享失败");
            }
        }
    }];
    
    
}




+ (XMPPRoomManager *) roomConfigWithRoomName:(NSString *)roomName  {
    
    if (isEmptyString(roomName)) return nil;
    NSArray *arrr = [XMPPManager sharedManager].roomList;
    XMPPRoomManager *roomManager;
    BOOL isContain = false;
    for (NSInteger idx = 0; idx <arrr.count; idx ++) {
        XMPPRoomManager *manager = arrr[idx];
        XMPP_LOG(@" - - - - -  manager.roomJID.user =%@   roomName =%@",manager.xmppRoom.roomJID.user,roomName)
        if ([manager.xmppRoom.roomJID.user isEqualToString:roomName]) {
            roomManager = manager;
            isContain = YES;
        }
    }
    
    if (!isContain) {
        XMPPJID *jid = roomJID(roomName);
        roomManager = [[XMPPRoomManager alloc] initRoomWithJID:jid];
        XMPP_LOG(@"  - - - - -    没加入model.bareJID =%@ 群组  重新生成 =%@",roomName,jid)
        [[XMPPManager sharedManager].roomList addObject:roomManager];
    }
    
    return roomManager;
}

+ (ConversationModel *) roomConfigWithConversation:(ConversationModel *)model {
    
    if (model.chatType != UUChatTypeChatRoom) return nil;
    
    
    /* 从服务器上获取 加入的群列表*/
    NSArray *arrr = [XMPPManager sharedManager].roomList;
    
    BOOL isContain = false;
    
    for (NSInteger idx = 0; idx <arrr.count; idx ++) {
        XMPPRoomManager *manager = arrr[idx];
        if ([manager.xmpp_roomName isEqualToString:model.bareJID.full]) {
            XMPP_LOG(@"manager.roomJID =%@   model.bareJID =%@",manager.xmpp_roomName,model.bareJID.full)
            model.xmppRoomManager = manager;
            isContain = YES;
        }
    }
    
    if (!isContain) {
        
//        XMPP_LOG(@"   没加入model.bareJID =%@ 群组",model.bareJID)
//        [XMPPSignal MR_deletedContactID:model.conversation];
//        [XMPPSignal deleteGroupHistoryWithRoomID:model.conversation];
//        [XMPPSignal deletedContactListWith:model.conversation];
        
        return nil;
    }
    
    return model;
    
}



 


/*
 <iq from='fluellen@shakespeare.lit/pda' id='kick1' to='harfleur@henryv.shakespeare.lit' type='set'>
     <query xmlns='http://jabber.org/protocol/muc#admin'>
         <item nick='pistol' role='none'>
             <reason>Avaunt, you cullion!</reason>
         </item>
     </query>
 </iq>
 
<iq type="set" to="20180306173200714299临沂交警@muc.tigase.dsceshi.cn">
     <query xmlns="http://jabber.org/protocol/muc#admin">
         <item nick="68@localhost.localdomain" role="none">
             <reason>comment</reason>
         </item>
     </query>
 </iq>
 
 <iq to="20180312090354681494@muc.localhost.localdomain" id="f4Z7k-221" type="set">
     <query xmlns="http://jabber.org/protocol/muc#admin">
         <item nick="50" role="none">
             <reason>你被踢出此聊天室</reason>
         </item>
     </query>
 </iq>
 
 <iq type="set" to="20180312090354681494@muc.localhost.localdomain">
 <query xmlns="http://jabber.org/protocol/muc#admin">
 <item nick="50" role="none"/>
 
 </query>
 
 </iq>
 */
+ (void) removeUsersWithUID:(NSString *)user withJID:(XMPPJID *)roomJID {
 
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:roomJID];
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/muc#admin"];
    [iq addChild:query];
    NSXMLElement *item = [NSXMLElement elementWithName:@"item"];
    [item addAttributeWithName:@"nick" stringValue:user];
    [item addAttributeWithName:@"role" stringValue:@"none"];
    [query addChild:item];
     [item addChild:[NSXMLElement elementWithName:@"reason" stringValue:@"comment"]];
     [[XMPPManager sharedManager].stream sendElement:iq];
    
    
    
}


+ (void) generateReceiptResponse:(XMPPMessage *)message {
    XMPPMessage *msg = [XMPPMessage messageWithType:@"messageStatus" to:message.from];
    NSXMLElement *recieved = [NSXMLElement elementWithName:@"received" xmlns:@"urn:xmpp:receipts"];
    [msg addChild:recieved];
    [[XMPPManager sharedManager].stream sendElement:msg];
}

+ (BOOL)isValidWithString:(NSString *)string
{
    NSString *regex =@"[a-zA-z]+://[^\\s]*";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [urlTest evaluateWithObject:string];
}

+ (BOOL) isFileExist:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filePath = [path stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [fileManager fileExistsAtPath:filePath];
    NSLog(@"这个文件已经存在：%@",result?@"是的":@"不存在");
    return result;
}

+ (BOOL) isCachContain:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filePath = [path stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [fileManager fileExistsAtPath:filePath];
    NSLog(@"这个文件已经存在：%@",result?@"是的":@"不存在");
    return result;
}

 

 

+ (BOOL )deletedFileWith:(NSString *)fileName {
    
    if (![XMPPSignal isFileExist:fileName]) {
        return YES;
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filePath = [path stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [fileManager removeItemAtPath:filePath error:nil];
    return result;
//    BOOL result = [fileManager fileExistsAtPath:filePath];
}





/**
 设置已读
 */
+ (void) updateXMPPMessageArchiving:(NSString *)contactID messageID:(NSString *)ID {
    dispatch_async(dispatch_get_main_queue(), ^{
         XMPPMessageArchivingCoreDataStorage * archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = archivingStorage.mainThreadManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:archivingStorage.messageEntityName];
        NSPredicate *predicate;
        predicate = isEmptyString(ID)? [NSPredicate predicateWithFormat:@"bareJidStr = %@  && streamBareJidStr = %@",getJID(contactID),[XMPPManager sharedManager].stream.myJID.bare]:[NSPredicate predicateWithFormat:@"bareJidStr=%@ && messageID = %@ &&streamBareJidStr = %@",getJID(contactID),ID,[XMPPManager sharedManager].stream.myJID.bare];
        request.predicate = predicate;
        NSArray *deleArray = [context executeFetchRequest:request error:nil];
        for (XMPPMessageArchiving_Message_CoreDataObject *object in deleArray) {
            if (object.messageStatus.integerValue == 3) {
                object.messageStatus = @"4";
            }
            
        }
        NSError *error = nil;
        //保存--记住保存
        if ([context save:&error] ) {
            
            XMPP_LOG(@"  * * * *XMPPMessageArchiving_Contact_CoreDataObject 更新数据  success");
        }else{
            XMPP_LOG(@"更新数据失败 %@", error);
        }
    });
    
    
}



+ (void) xmpp_deletedMessageWith:(NSString *)messageID contactID:(NSString *)contactID {
 
    dispatch_async(dispatch_get_main_queue(), ^{
        
        XMPPMessageArchivingCoreDataStorage * archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = archivingStorage.mainThreadManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:archivingStorage.messageEntityName];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@ && messageID = %@ &&streamBareJidStr = %@",getJID(contactID),messageID,[XMPPManager sharedManager].stream.myJID.bare];
        request.predicate = predicate;
        request.fetchLimit = 1;
        NSArray *deleArray = [context executeFetchRequest:request error:nil];
        for (XMPPMessageArchiving_Message_CoreDataObject *object in deleArray) {
             [context deleteObject:object];
        }
        NSError *error = nil;
        //保存--记住保存
        if ([context save:&error] ) {
            
            XMPP_LOG(@"  * * * *XMPPMessageArchiving_Contact_CoreDataObject 更新数据  success");
        }else{
            XMPP_LOG(@"更新数据失败 %@", error);
        }
    });
}


+ (void) im_ChangeMessage:(NSString *)messageID contact:(NSString *)contactID saveBlock:(void(^)(XMPPMessageArchiving_Message_CoreDataObject *messageObject))save  finash:(void(^)())finash{
    dispatch_async(dispatch_get_main_queue(), ^{
        XMPPMessageArchivingCoreDataStorage * archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = archivingStorage.mainThreadManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:archivingStorage.messageEntityName];
        NSPredicate *  predicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@ && messageID = %@ &&streamBareJidStr = %@",getJID(contactID),messageID,[XMPPManager sharedManager].stream.myJID.bare];
        request.predicate = predicate;
        request.fetchLimit = 1;
        NSArray *deleArray = [context executeFetchRequest:request error:nil];
        for (XMPPMessageArchiving_Message_CoreDataObject *object in deleArray) {
 
            if (save) {
                save(object);
            }
            
        }
        NSError *error = nil;
        //保存--记住保存
        if ([context save:&error] ) {
            
            XMPP_LOG(@"  * * * *XMPPMessageArchiving_Contact_CoreDataObject 更新数据  success");
        }else{
            XMPP_LOG(@"更新数据失败 %@", error);
        }
        
        
        if (finash) {
            NSLog(@"wancheng ");
            finash();
        }
        
        
    });
    
}


+ (void) MR_changeIMMessageContact:(NSString *)contactID  saveBlock:(void(^)(XMPPCoredataChatContact *contactModel))save {
    if (isEmptyString(contactID) || isEmptyString([XMPPManager sharedManager].stream.myJID.bare)) return;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==  %@ AND bareJidStr ==  %@", [XMPPManager sharedManager].stream.myJID.bare, contactID];
    XMPPCoredataChatContact *xm_contact = [XMPPCoredataChatContact MR_findFirstWithPredicate:predicate];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        if (xm_contact) {
            save(xm_contact);
        }
    }];
}

+ (void) MR_updateXMPPMessageArchiving_Contact:(NSString *)contactID  {
    if (isEmptyString(contactID) || isEmptyString([XMPPManager sharedManager].stream.myJID.bare)) return;
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==  %@ AND bareJidStr ==  %@", [XMPPManager sharedManager].stream.myJID.bare, contactID];
    XMPPCoredataChatContact *xm_contact  = [XMPPCoredataChatContact MR_findFirstWithPredicate:predicate inContext:localContext];
    if (xm_contact) {
        xm_contact.messageCount = @"0";
    }
    [localContext MR_saveToPersistentStoreAndWait];

}

+ (void) MR_updateXMPPMessageArchiving_ContactIsBurn:(NSString *)contactID {
    if (isEmptyString(contactID) || isEmptyString([XMPPManager sharedManager].stream.myJID.bare)) return;
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==  %@ AND bareJidStr ==  %@", [XMPPManager sharedManager].stream.myJID.bare, contactID];
    XMPPCoredataChatContact *xm_contact  = [XMPPCoredataChatContact MR_findFirstWithPredicate:predicate inContext:localContext];
    if (xm_contact) {
        xm_contact.isBurnMessage = @"0";
    }
    [localContext MR_saveToPersistentStoreAndWait];

}


+ (void) updateMessageWithChat:(NSString *)messageId messageBody:(NSString *)messageBody{
    dispatch_async(dispatch_get_main_queue(), ^{
         XMPPMessageArchivingCoreDataStorage * archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = archivingStorage.mainThreadManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:archivingStorage.messageEntityName];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageID = %@ ",messageId];
        request.predicate = predicate;
        request.fetchLimit = 1;
        NSArray *deleArray = [context executeFetchRequest:request error:nil];
        for (XMPPMessageArchiving_Message_CoreDataObject *object in deleArray) {
            UUMessage *message = [UUMessage mj_objectWithKeyValues:object.uumessageString];
            message.strContent = messageBody;
            object.body = messageBody;
            object.uumessageString = message.mj_JSONString;
            
         }
        NSError *error = nil;
        //保存--记住保存
        if ([context save:&error] ) {
            
            XMPP_LOG(@"  * * * *XMPPMessageArchiving_Contact_CoreDataObject 更新数据  success");
        }else{
            XMPP_LOG(@"更新数据失败 %@", error);
        }
    });
    
}


+ (void) MR_deletedContactID:(NSString *)contactID {
    if (isEmptyString(contactID) || isEmptyString([XMPPManager sharedManager].stream.myJID.bare)) return;
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==  %@ AND bareJidStr ==  %@", [XMPPManager sharedManager].stream.myJID.bare, contactID];
  BOOL isSuccess = [XMPPCoredataChatContact MR_deleteAllMatchingPredicate:predicate inContext:localContext];
    
    [localContext MR_saveToPersistentStoreAndWait];
    
    if ([contactID containsString:XMMPP_BASESTR]) {
        [XMPPSignal deletedChatHistoryWithUserID:contactID];
    }else{
    
        [XMPPSignal deletedChatHistoryWithUserID:contactID];
    }
    
    
}





+ (void) deletedChatHistoryWithUserID:(NSString *)userID {
    if (isEmptyString(userID)) return;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *uid  = [userID containsString:XMMPP_BASESTR] ? userID :getJID(userID);
        XMPPMessageArchivingCoreDataStorage *messageCDS = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *messageContext = messageCDS.mainThreadManagedObjectContext;
        NSFetchRequest *messageRequest = [NSFetchRequest fetchRequestWithEntityName:messageCDS.messageEntityName];
        NSPredicate *messagePredicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@ AND streamBareJidStr =  %@" ,uid,[XMPPManager sharedManager].stream.myJID.bare];
        messageRequest.predicate = messagePredicate;
        NSArray *deleMessageArray = [messageContext executeFetchRequest:messageRequest error:nil];
        for (XMPPMessageArchiving_Message_CoreDataObject *object in deleMessageArray) {
            [messageContext deleteObject:object];
        }
        [messageContext save:nil];
        
        [XMPPSignal MR_deletedContactID:uid];
    });

    
    
    
}


+ (void) deleteGroupHistoryWithRoomID:(NSString *)roomID {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *room ;
        if ( [roomID containsString:XMPPR_ROOM_Regulation]) {
            room = [roomID componentsSeparatedByString:XMPPR_ROOM_Regulation].firstObject;
        }else{
            room = roomID;
        }
    XMPPRoomCoreDataStorage *groupManager = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *groupContext = groupManager.mainThreadManagedObjectContext;
    NSFetchRequest *groupRequest = [NSFetchRequest fetchRequestWithEntityName:groupManager.messageEntityName];
    NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"roomJIDStr=%@  AND streamBareJidStr =  %@",roomID,[XMPPManager sharedManager].stream.myJID.bare];
    [groupRequest setPredicate:groupPredicate];
    NSArray *deleArray = [groupContext executeFetchRequest:groupRequest error:nil];
    for (XMPPRoomOccupantCoreDataStorageObject *object in deleArray) {
        [groupContext deleteObject:object];
    }
    NSError *error = nil;
    //保存--记住保存
    if ([groupContext save:&error]) {
        
        XMPP_LOG(@"  * * * * 删除数据  success");
    }else{
        XMPP_LOG(@"删除数据失败, %@", error);
    }
    
        [XMPPSignal MR_deletedContactID:roomID];
    });
}



+ (void) deletedContactListWith:(NSString *)ID {
dispatch_async(dispatch_get_main_queue(), ^{
    XMPPMessageArchivingCoreDataStorage * archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *context = archivingStorage.mainThreadManagedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:archivingStorage.contactEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@ AND streamBareJidStr =  %@", ID,[XMPPManager sharedManager].stream.myJID.bare];
    request.predicate = predicate;
    NSArray *deleArray = [context executeFetchRequest:request error:nil];
     for (XMPPMessageArchiving_Contact_CoreDataObject *object in deleArray) {
        [context deleteObject:object];
    }
    
     NSError *error = nil;
    //保存--记住保存
    if ([context save:&error]) {
        
        XMPP_LOG(@"  * * * *XMPPMessageArchiving_Contact_CoreDataObject 会话 删除  success");
    }else{
        XMPP_LOG(@"删除数据失败, %@", error);
    }
    });
}

+ (void) groupUpMessageWith:(NSString *)roomName messageID:(NSString *)messageID finash:(void(^)(NSString *str))finash {
    XMPPRoomCoreDataStorage *groupManager = [XMPPRoomCoreDataStorage sharedInstance];
    NSManagedObjectContext *groupContext = groupManager.mainThreadManagedObjectContext;
    NSFetchRequest *groupRequest = [NSFetchRequest fetchRequestWithEntityName:groupManager.messageEntityName];
    
    NSString *predicateFormat = @" streamBareJidStr == %@ AND roomJIDStr == %@ AND messageID == %@";
    
    NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:predicateFormat,[XMPPManager sharedManager].stream.myJID.bare,roomName, messageID];
    [groupRequest setPredicate:groupPredicate];
    [groupRequest setFetchLimit:1];
    NSArray *deleArray = [groupContext executeFetchRequest:groupRequest error:nil];
    for (XMPPRoomMessageCoreDataStorageObject *object in deleArray) {
        NSString*messString = object.message.body;
        XMPP_RoomMessage *message = [XMPP_RoomMessage mj_objectWithKeyValues:[messString mj_JSONObject]];
        NSDictionary *dic = message.ext.mj_JSONObject;
        NSMutableDictionary *dicc = [[NSMutableDictionary alloc] initWithDictionary:dic];
        if (dicc) {
            if ([dicc[MSG_TYPE] isEqualToString:GROUP_INVITE_NEED_CONFIRM]) {
                [dicc setObject:GROUP_INVITE_CONFIRM forKey:MSG_TYPE];
                message.ext = dicc.mj_JSONString;
                
                UUMessage *mmmm = [object updateRoomMessageWithBody:dicc];
                object.uumessageString = mmmm.mj_JSONString;
                
            }
        }
    }
    NSError *error = nil;
    //保存--记住保存
    if ([groupContext save:&error]) {
        
        XMPP_LOG(@"  * * * * 更新  success");
    }else{
        XMPP_LOG(@"更新数据失败, %@", error);
    }
}

+ (void) applicationDidEnterBackground {
    [[XMPPManager sharedManager].roomList.rac_sequence.signal subscribeNext:^(XMPPRoomManager *manager) {
        [manager.xmppRoom removeDelegate:manager];
    }completed:^{
        [[XMPPManager sharedManager].roomList removeAllObjects];
    }];
    
}



// 统计未读消息
+ (void)setupUnreadMessageCount:(void(^)(NSInteger count))finash {
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr =%@", [XMPPManager sharedManager].stream.myJID.bare];
    NSFetchRequest *peopleRequest = [XMPPCoredataChatContact MR_requestAllWithPredicate:predicate];
    NSArray *arr = [XMPPCoredataChatContact MR_executeFetchRequest:peopleRequest];
    __block NSInteger messageCount = 0;
    [arr.rac_sequence.signal subscribeNext:^(XMPPCoredataChatContact *obj) {
        NSInteger count = obj.messageCount.integerValue;
        messageCount += count;
    }completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (finash) {
                finash(messageCount);
            }

            [[ChatHelper shareHelper] setBadgeNumber:messageCount];
            // 设置应用角标
            UIApplication *application = [UIApplication sharedApplication];
            [application setApplicationIconBadgeNumber:messageCount];
        });
    }];
}










@end


@implementation XMPP_RoomMessage

- (BOOL) isRoomOwn
{
    if (!isEmptyString(self.ext))
    {
        NSDictionary *extdic = self.ext.mj_JSONObject;
        NSString *group_INVITE_OWNER = extdic[GROUP_INVITE_OWNER];
        if ([[UserInstance ShardInstnce].uid isEqualToString:group_INVITE_OWNER]) {
            return YES;
        }else{
            return NO;
        }
    }
    return NO;
}

@end

@implementation XMPP_RoomVoiceMessage
@end

@implementation XMPP_RoomLocationMessage
@end

@implementation XMPP_RoomFileMessage
@end

@implementation XMPP_BaseMessage

@end
@implementation XMPP_SiganlMessage

@end

@implementation XM_TransModel

- (instancetype)initDefaultShareText:(NSString *)messageText ext:(NSDictionary *)extDic  {
    
    if (self = [super init]) {
        self.messageText = messageText;
        self.extDic = extDic;
        if (extDic) {
            self.type = TranMessageShareCustom;
        } else {
            self.type = TranMessageShareDefault;
        }
        
        
    }
    return self;
}


- (instancetype)initShareImage:(NSData *)imageData  {
    
    if (self = [super init]) {
        self.imageData = imageData;
        self.type = TranMessageShareImage;
        
    }
    return self;
}

@end
