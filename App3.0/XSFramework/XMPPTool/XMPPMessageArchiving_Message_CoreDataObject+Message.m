//
//  XMPPMessageArchiving_Message_CoreDataObject+Message.m
//  App3.0
//
//  Created by Sunny on 2018/4/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPMessageArchiving_Message_CoreDataObject+Message.h"
 

@implementation XMPPMessageArchiving_Message_CoreDataObject (Message)
@dynamic uumessage;
@dynamic messageID;
@dynamic uumessageString;
@dynamic messageStatus;
@dynamic uuFrame;

+(void)load {
    
    Method  one = class_getInstanceMethod(self, @selector(willInsertObject));
    Method  two= class_getInstanceMethod(self, @selector(xm_willInsertObject));
    method_exchangeImplementations(one, two);
    
    
    Method m1 = class_getInstanceMethod(self, @selector(didUpdateObject));
    Method m2 = class_getInstanceMethod(self, @selector(xm_didUpdateObject));
    method_exchangeImplementations(m1, m2);
}

- (void)xm_willInsertObject {
    
    
    
    [self willChangeValueForKey:@"uumessageString"];
     [self willChangeValueForKey:@"messageID"];
    [self willChangeValueForKey:@"messageStatus"];
 
    if (isEmptyString(self.message.fromStr) || [self.message.from.user isEqualToString:[UserInstance ShardInstnce].uid]) {
        self.isOutgoing = YES;
    }else{
        self.isOutgoing = NO;
    }
 
    
    UUMessage *uumessagge = [self getChatTypeWithMessage];
    self.messageStatus = @"3";
    uumessagge.status = [self.messageStatus integerValue];
    self.uumessageString = uumessagge.mj_JSONString;
    self.messageID = self.message.elementID;
    
    
    [self didChangeValueForKey:@"uumessageString"];
    [self didChangeValueForKey:@"messageID"];
    [self didChangeValueForKey:@"messageStatus"];
 
    
 
}




- (void)xm_didUpdateObject {
    
    
    XMPP_LOG(@"xm_didUpdateObject");
    
}


-(UUMessage *)uumessage {
    [self willAccessValueForKey:@"uumessage"];
    UUMessage *uumessage = [UUMessage mj_objectWithKeyValues:self.uumessageString context:self.managedObjectContext];
    uumessage.timestamp = self.timestamp;
    uumessage.status = [self.messageStatus integerValue];
    [self didAccessValueForKey:@"uumessage"];
    return uumessage ? uumessage:nil;
}


-(UUMessageFrame *)uuFrame {
    [self willAccessValueForKey:@"uumessage"];
    ChatModel *model = [ChatModel new];
    UUMessageFrame *uumessage = [model messageWithSignaleMessage:self refreshTime:YES];
 
    [self didAccessValueForKey:@"uumessage"];
    return uumessage ? uumessage:nil;
    
}

/*
#define MESSAGE_ATTR_IS_VOICE_CALL @"MESSAGE_ATTR_IS_VOICE_CALL"
#define MESSAGE_ATTR_IS_VIDEO_CALL @"MESSAGE_ATTR_IS_VIDEO_CALL"
*/
- (NSArray *) noticeForMessage {
    NSArray *voice_chat = @[VOICE_CHAT_REQUEST,VOICE_CHAT_AGREE,VOICE_CHAT_DISAGREE,VOICE_CHAT_LINE_BUSY,VOICE_CHAT_PAY,VOICE_CHAT_TIME,CHAT_DESTROY,CHAT_DESTROY_AGREE,CHAT_DESTROY_DISAGREE,MESSAGE_ATTR_IS_VOICE_CALL,MESSAGE_ATTR_IS_VIDEO_CALL];
    return voice_chat;
}

/**
 判断 聊天类型
 
 <message type="chat" to="68@localhost.localdomain/TigaseMessenger">
 
 <body>http://122.6.144.114:58088/assist/uploadFolder/20180302/20180305170259120.jpg</body>
 <message>message</message>
 <request xmlns="jabber:x:oob"></request>
 
 </message>
 
 @param message message description
 */
- (UUMessage *) getChatTypeWithMessage{
    UUMessage *uuMessage = [UUMessage new];
    uuMessage.messageId = self.message.elementID;
    NSString *displayName = [[self.message from] user];
    uuMessage.strName = displayName;
    NSDateFormatter* formatter = [XSTool shareYMDHMS];
    uuMessage.strTime = [formatter stringFromDate:self.timestamp];
    uuMessage.from = !self.isOutgoing;
    uuMessage.chatType = UUChatTypeChat;
    uuMessage.relation = [ChatHelper isfriendWithUid:self.message.xm_messageUser];
 
    uuMessage.status = [self.messageStatus integerValue];
    NSXMLElement *request = [self.message elementForName:@"x"];
     if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ){
         NSString *type = [request elementForName:@"type"].stringValue;
        NSString *time = [request elementForName:@"time"].stringValue;
        if ([type isEqualToString:@"voice"]) {
            uuMessage.type = XMPPMessageTypeVoice;
            if (!isEmptyString(self.message.body)) uuMessage.downVoicePath = [NSURL URLWithString:self.message.body];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *path = [paths objectAtIndex:0];
            NSString *filName = [self.message.body componentsSeparatedByString:@"/"].lastObject;
            NSString *filePath = [NSString stringWithFormat:@"%@/%@",path,filName];
            uuMessage.voiceUrl = [NSURL URLWithString:filePath];
            uuMessage.strVoiceTime = time;
            BOOL isExist = [XMPPSignal isFileExist:filName];
            if (!isExist)  [uuMessage downloadVoiceWith:uuMessage.downVoicePath finash:nil];
 
        }else if ([type isEqualToString:@"image"]){
            uuMessage.type = XMPPMessageTypePicture;
            uuMessage.pictureURL =  self.message.body;
        }else if ([type isEqualToString:@"location"]){
            uuMessage.type = UUMessageTypeLocation;
            uuMessage.strLocation =self.message.body;
            uuMessage.strLatitude = [request elementForName:@"latitude"].stringValue;
            uuMessage.strLongitude = [request elementForName:@"longitude"].stringValue;
        }else if ([type isEqualToString:@"file"]){
            uuMessage.type = UUMessageTypeFile;
            uuMessage.fileUrl = self.message.body;
            uuMessage.fileTitle = [request elementForName:@"displayName"].stringValue;
            uuMessage.fileDesc = [request elementForName:@"fileSize"].stringValue;
        }else if ([type isEqualToString:@"video"]){
            uuMessage.type = UUMessageTypeVideo;
            uuMessage.fileUrl = self.message.body;
            uuMessage.fileTitle = [request elementForName:@"displayName"].stringValue;
            uuMessage.fileDesc = [request elementForName:@"fileSize"].stringValue;
            uuMessage.pictureURL = [request elementForName:@"thumbPath"].stringValue;
            uuMessage.videoTime = [request elementForName:@"time"].stringValue;
            uuMessage.strContent = self.message.body;
        }  else {
            NSString *jsonStr = [request elementForName:@"ext"].stringValue;
            NSDictionary *dic = jsonStr.mj_JSONObject;
            uuMessage.ext = dic;
            NSString *type = dic[MSG_TYPE];
            if ([type isEqualToString:READ_PACKET]) {
                // 红包
                uuMessage.type = XMPPMessageTypeRedPacket;
                uuMessage.rpId = dic[CUSTOM_MSG_ID];
                uuMessage.strRedMessage = self.message.body;
                uuMessage.moneyStr = dic[TRANSFER_BANLANCE_TAG];
            } else if ([type isEqualToString:TRANSFER_BANLANCE] || [type isEqualToString:TRANSFER_MONEY_COLLECTION] || [type isEqualToString:TRANSFER_MONEY_COLLECTION_REJECT]) {
                // 转账
                uuMessage.type = XMPPMessageTypeTransfer;
                uuMessage.rpId = dic[CUSTOM_MSG_ID];
                uuMessage.strRedMessage = self.message.body;
                uuMessage.moneyStr = dic[TRANSFER_BANLANCE_TAG];
            } else if ([type isEqualToString:MESSAGE_SHARE_TYPE]) {
                // 分享
                uuMessage.type = UUMessageTypeShare;
                uuMessage.shareJosnString = dic[MESSAGE_SHARE_DATA];
            } else if ([type isEqualToString:SHOW_PRODUCT_CONTENT]) {
                // 商品消息
                uuMessage.type = UUMessageTypeProduct;
                uuMessage.shareJosnString = self.message.body;
            } else if ([type isEqualToString:MESSAGE_PRODUCT_TYPE]) {
                // 商品链接消息
                uuMessage.type = UUMessageTypeProductShare;
                uuMessage.shareJosnString = dic[@"MESSAGE_SHARE_DATA"];
            } else if ([type isEqualToString:MESSAGE_SYSTEM_PUSH]) {
                // 系统推送
                uuMessage.type = UUMessageTypeSystemPush;
                uuMessage.shareJosnString = self.message.body;
                
                /*
                 static NSString  * const VOICE_CHAT_REQUEST = @"VOICE_CHAT_REQUEST";
                 static NSString  * const VOICE_CHAT_AGREE = @"VOICE_CHAT_AGREE";
                 static NSString  * const VOICE_CHAT_DISAGREE = @"VOICE_CHAT_DISAGREE";
                 static NSString  * const VOICE_CHAT_LINE_BUSY = @"VOICE_CHAT_LINE_BUSY";
                 

                static NSString  * const VOICE_CHAT_PAY = @"VOICE_CHAT_PAY";
                static NSString  * const VOICE_CHAT_TIME = @"VOICE_CHAT_TIME";
                 static NSString  * const  CHAT_DESTROY = @"CHAT_DESTROY";//请求焚毁
                 static NSString  * const  CHAT_DESTROY_AGREE = @"CHAT_DESTROY_AGREE"; //同意请求焚毁
                 static NSString  * const  CHAT_DESTROY_DISAGREE = @"CHAT_DESTROY_DISAGREE";//拒绝请求焚毁：
                 */

            }else if ([self.noticeForMessage containsObject:type]) {
                // 系统推送
                uuMessage.type = XMPPMessageTypeText;
                uuMessage.strContent = self.message.body;
            }else {
                uuMessage.strNotice = [uuMessage noticeStringConvertByMessage:self.message ext:dic];
            }
        }
        
        //        }
    }else{
        uuMessage.type = XMPPMessageTypeText;
        uuMessage.strContent =  self.message.body;
        
    }
    
    
    
    return uuMessage;
}

//- (void) signalWithContactID:(NSString *)UID  finash:(void(^)(ConversationModel *model))finash{
//    ConversationModel *model = [ConversationModel new];
//    if (!self.isOutgoing) {
//        NSString *avatar = [UserInstance ShardInstnce].avatarImgUrl;
//        model.avatarURLPath = avatar;
//        if (finash) finash(model);
//    }else{
//
//        NSArray *contacts = [[DBHandler sharedInstance] getContactByUid:UID];
//        if (contacts != nil && contacts.count > 0) {
//            // 本地数据库存在
//            ContactDataParser *contact = contacts[0];
//            model.title = [contact getName];
//            model.avatarURLPath = contact.avatar;
//            model.userName = contact.username;
//            model.relation = contact.relation;
//
//            if (finash) finash(model);
//
//        } else {
//            // 本地数据库不存在
//            [HTTPManager getUserInfoWithUid:UID success:^(NSDictionary * dic, resultObject *state) {
//                UserInfoParser *parser = [UserInfoParser mj_objectWithKeyValues:dic];
//
//                if (state.status) {
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//                        ContactDataParser *cdParser = [[ContactDataParser alloc] init];
//
//                        // 会话对象是机器人特殊处理
//                        if ([UID isEqualToString:@"robot"]) {
//                            cdParser.username = Localized(@"系统消息");
//                            cdParser.remark = Localized(@"系统消息");
//                            cdParser.nickname = Localized(@"系统消息");
//                            model.title = Localized(@"系统消息");
//
//                        } else {
//                            cdParser.username = parser.data.username;
//                            cdParser.remark = parser.data.remark;
//                            cdParser.nickname = parser.data.nickname;
//                            model.title = [parser.data getName];
//                        }
//                        cdParser.uid = UID;
//                        cdParser.avatar = parser.data.logo;
//                        cdParser.relation = parser.data.relation;
//                        cdParser.mobile = parser.data.mobile;
//
//                        model.avatarURLPath = parser.data.logo;
//                        model.userName = parser.data.username;
//                        model.relation = parser.data.relation;
//                        // 防止同一时间刷新两次列表造成数据重复添加
//                        NSArray *contactT = [[DBHandler sharedInstance] getContactByUid:cdParser.uid];
//                        if (contactT.count == 0) {
//                            if (finash) finash(model);
//                        }
//                        // 添加数据库操作放在最后
//                        [[DBHandler sharedInstance] addOrUpdateContact:cdParser];
//                    });
//                }
//            } fail:^(NSError * _Nonnull error) {
//                if (finash) finash(nil);
//            }];
//        }
//
//    }
//}

 

@end
