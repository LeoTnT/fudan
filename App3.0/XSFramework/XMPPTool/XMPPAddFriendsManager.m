//
//  AddFriendsManager.m
//  tigaseXMPP
//
//  Created by apple on 2018/1/3.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMPPAddFriendsManager.h"


@interface XMPPAddFriendsManager ()<XMPPRosterDelegate,XMPPRosterMemoryStorageDelegate>


@end

@implementation XMPPAddFriendsManager
static dispatch_once_t onceToken;
static XMPPAddFriendsManager *manager;
static RACSubject *_addFriedSignal;
+(instancetype) shareManager {
    
    dispatch_once(&onceToken, ^{
        manager = [[XMPPAddFriendsManager alloc] init];
        _addFriedSignal = [RACSubject subject];
        
    });
    return manager;
}


- (RACSubject *)addFriedSignal {return _addFriedSignal;};


+ (void)addBuddyWithJid:(NSString *)jidString {
    
    NSString *jid = getJID(jidString);
    
    if ([jid isEqualToString:[XMPPManager sharedManager].stream.myJID.user]) {
        
        return;
    }
    
    XMPPJID *xmppjid = [XMPPJID jidWithString:jid];
    
    
    [[XMPPManager sharedManager].roster subscribePresenceToUser:xmppjid];
    
    // 发送添加好友请求
    /*
     presence.type有以下几种状态：
     available: 表示处于在线状态(通知好友在线)
     unavailable: 表示处于离线状态（通知好友下线）
     subscribe: 表示发出添加好友的申请（添加好友请求）
     unsubscribe: 表示发出删除好友的申请（删除好友请求）
     unsubscribed: 表示拒绝添加对方为好友（拒绝添加对方为好友）
     error: 表示presence信息报中包含了一个错误消息。（出错）
     */
    
}

+ (void)removeBuddyWithJid:(NSString *)jidString {
    NSString *jid = getJID(jidString);
    if ([jid isEqualToString:[XMPPManager sharedManager].stream.myJID.user]) {
        return;
    }
    XMPPJID *xmppjid = [XMPPJID jidWithString:jid];
    [[XMPPManager sharedManager].roster removeUser:xmppjid];
    
}


#pragma mark -- XMPPRosterDelegate监听好友请求,收到好友请求时调用该代理方法
- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence {
    
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"%@请求添加你为好友", nil),presence.from.user];
    
    XMPP_LOG(@"message  =%@",message)
    //    [UIAlertView showWithTitle:nil message:message cancelButtonTitle:NSLocalizedString(@"却笑", nil) otherButtonTitles:@[NSLocalizedString(@"确定", nil)] tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
    XMPPRoster *roster = [XMPPManager sharedManager].roster;
    //        NSLog(@"buttonIndex   =%ld",(long)buttonIndex);
    //        if (buttonIndex == 0) {
    //            // 拒绝
    //            [roster rejectPresenceSubscriptionRequestFrom:presence.from];
    //        } else {
    //            // 同意加好友
    [roster acceptPresenceSubscriptionRequestFrom:presence.from andAddToRoster:YES];
    //        }
    //    }];
}




/**
 添加好友同意 后
 */
- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterPush:(XMPPIQ *)iq {
    
    //    @"添加成功!!!didReceiveRosterPush -> :%@", nil),iq.description);
    
    DDXMLElement *query = [iq elementsForName:@"query"][0];
    DDXMLElement *item = [query elementsForName:@"item"][0];
    //    <iq xmlns="jabber:client" type="set" to="123@81kfc0vjdnilocj/845469324-tigase-161" id="rsttig3"><query xmlns="jabber:iq:roster" ver="c4284ca8706f3160193f7030ee7ab381">
    //    <item subscription="both" jid="syw@81kfc0vjdnilocj" name="syw"><group>Friends</group></item>
    //    </query>
    //    </iq>
    
    NSString *subscription = [[item attributeForName:@"subscription"] stringValue];
    XMPP_LOG(@"subscription  =%@",subscription)
    
    
    NSString *sss = [[item attributeForName:@"jid"] stringValue];
    if ([sss containsString:@"@"]) {
        NSString *uid = [sss componentsSeparatedByString:@"@"].firstObject;
        if (!isEmptyString(uid)) {
            if ([subscription isEqualToString:@"both"]) {
                [XMPPFriendModel getUserInfor:uid];
                [XMPPFriendModel updataUserInfor:uid relation:@1];
            }else{
                [XMPPFriendModel updataUserInfor:uid relation:@0];
            }
        }
        
    }
    
 
}



/**
 已经互为好友以后，会回调此
 
 *  好友信息状态有5种
 both - 互为好友
 none - 互不为好友
 to - 请求添加对方为好友，对方还没有同意
 from - 对方添加我为好友，自己还没有同意
 remove - 曾经删除的好友
 
 */
- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(NSXMLElement *)item {
    
    
    
}

/**
 * Catch-all change notification.
 *
 * When the roster changes, for any of the reasons listed below, this delegate method fires.
 * This method always fires after the more precise delegate methods listed below.
 **/
- (void)xmppRosterDidChange:(XMPPRosterMemoryStorage *)sender {
    
    
    
}

/**
 * Notification that the roster has received the roster from the server.
 *
 * If parent.autoFetchRoster is YES, the roster will automatically be fetched once the user authenticates.
 **/
- (void)xmppRosterDidPopulate:(XMPPRosterMemoryStorage *)sender {
    
}

/**
 * Notifications that the roster has changed.  好友列表操作 添加好友、更新好友信息、移除好友
 *
 * This includes times when users are added or removed from our roster, or when a nickname is changed,
 * including when other resources logged in under the same user account as us make changes to our roster.
 *
 * This does not include when resources simply go online / offline.
 **/
- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddUser:(XMPPUserMemoryStorageObject *)user {
    
    [XMPPFriendModel updataUserInfor:user.jid.user relation:@1];
    
}
- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didUpdateUser   =%@",sender);
    [_addFriedSignal sendNext:@1];
}
- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveUser:(XMPPUserMemoryStorageObject *)user {
    
    [XMPPFriendModel updataUserInfor:user.jid.user relation:@0];
    
}





@end
