//
//  XMPPIQ+Help.m
//  XMPPDemo
//
//  Created by apple on 2018/1/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMPPIQ+Help.h"

@implementation XMPPIQ (Help)

-(BOOL)isRosterQuery{
    if (self.childCount>0){
        for (NSXMLElement* element in self.children) {
            if ([element.name isEqualToString:@"query"] && [element.xmlns isEqualToString:@"jabber:iq:roster"]){
                return YES;
            }
        }
    }
    return NO;
}
-(BOOL)isChatRoomItems{
    if (self.childCount>0){
        for (NSXMLElement* element in self.children) {
            if ([element.name isEqualToString:@"query"] &&
                [element.xmlns isEqualToString:@"http://jabber.org/protocol/disco#items"]){
                return YES;
            }
        }
    }
    return NO;
}
-(BOOL)isChatRoomInfo{
    if (self.childCount>0){
        for (NSXMLElement* element in self.children) {
            if ([element.name isEqualToString:@"query"] &&
                [element.xmlns isEqualToString:@"http://jabber.org/protocol/disco#info"]){
                BOOL has_identity=NO;
                BOOL has_feature=NO;
                for (NSXMLElement* element_item in element.children) {
                    if ([element_item.name isEqualToString:@"identity"]){
                        has_identity=YES;
                    }
                    if ([element_item.name isEqualToString:@"feature"]){
                        has_feature=YES;
                    }
                }
                return has_identity && has_feature;
            }
        }
    }
    return NO;
}


+ (XMPPIQ *)queryRoomOfService {
    XMPPJID *serverJID = [XMPPJID jidWithString:XMPPR_ROOM_Regulation];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" to:serverJID];
    [iq addAttributeWithName:@"from" stringValue:[XMPPManager sharedManager].stream.myJID.full];
    NSXMLElement *query = [NSXMLElement elementWithName:@"query"];
    
    [query addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/disco#items"];
    [iq addChild:query];
    return iq;
}

+ (XMPPIQ *)queryRoomOfJoined {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get"];
    [iq addChild:[NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#rooms"]];
    [iq addAttributeWithName:@"to" stringValue:XMPPR_ROOM_Regulation];
    
    return iq;
}



+ (XMPPIQ *) getMembersOfRoom:(NSString *)roomJID {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get"];
    [iq addAttributeWithName:@"to" stringValue:roomJID];
    [iq addChild:[NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#users"]];
    return iq;
}


@end

@implementation XMPPPresence (Help)
-(BOOL)isChatRoomPresence{
    if (self.childCount>0){
        for (NSXMLElement* element in self.children) {
            if ([element.name isEqualToString:@"x"] &&
                [element.xmlns isEqualToString:@"http://jabber.org/protocol/muc#user"])
                return YES;
        }
    }
    return NO;
}
@end

@implementation XMPPMessage (Help)


- (NSString *)xm_messageUser {
    
    if ([self.fromStr containsString:XMPPR_ROOM_Regulation] && [self.fromStr containsString:@"/"]) {
        return [self.fromStr componentsSeparatedByString:@"/"].lastObject;
    }else if ([self.fromStr containsString:XMMPP_BASESTR]){
        return [self.fromStr componentsSeparatedByString:@"@"].firstObject;
    }else if (isEmptyString(self.fromStr)) {
        return [UserInstance ShardInstnce].uid;
    }
    return nil;
}

-(BOOL)isChatRoomInvite{
    if (self.childCount>0){
        for (NSXMLElement* element in self.children) {
            if ([element.name isEqualToString:@"x"] && [element.xmlns isEqualToString:@"http://jabber.org/protocol/muc#user"]){
                for (NSXMLElement* element_a in element.children) {
                    if ([element_a.name isEqualToString:@"invite"]){
                        return YES;
                    }
                }
            }
        }
    }
    return NO;
}

@end

@implementation XMPPJID (Help)

- (BOOL)xm_isChatSigal {
     if ([self.full containsString:XMMPP_BASESTR] && self.full.length > XMMPP_BASESTR.length ) {
         return YES;
    }
    return NO;
}


- (NSInteger ) xm_roomLength {
    
    return 18 + [UserInstance ShardInstnce].uid.length;
}


- (NSString *)xm_roomName {
    
    if (!self.xm_isChatSigal && self.full.length > (self.xm_roomLength + XMPPR_ROOM_Regulation.length)) {
        if (![self.full containsString:@"@"]) return nil;
        NSString *jidString = [self.full componentsSeparatedByString:@"@"].firstObject;
        if (jidString.length < self.xm_roomIdentifier.length)  return nil;
         NSString *name = [jidString substringFromIndex:self.xm_roomIdentifier.length];
         return name;
     }
 
    return nil;
}

- (NSString *)xm_roomIdentifier {
 
    NSString *roomID =self.full.length > self.xm_roomLength ? [self.full substringToIndex:self.xm_roomLength] :nil;
     return roomID;
}


- (NSString *)xm_jidNumber {
    return [self.full componentsSeparatedByString:@"@"].firstObject;
}

//1520230374549926121@muc.tigase.dsceshi.cn/68
- (NSString *)xm_roomUserID {
    if (![self.full containsString:@"/"]) return nil;
    return [self.full componentsSeparatedByString:@"/"].lastObject;
}


+ (XMPPJID *) getUserJIDWithString:(NSString *)jidString {
    
    return [XMPPJID jidWithString:[NSString stringWithFormat:@"%@%@",jidString,XMMPP_BASESTR]];
}


- (NSString *)xmpp_ID {
    
    if (self.xm_isChatSigal) {
        return [self.full componentsSeparatedByString:@"@"].firstObject;
    }else{
        return self.xm_roomIdentifier;
    }
}

- (NSString *)im_UserID {
    NSString *ss = [NSString stringWithFormat:@"%@/",XMPPR_ROOM_Regulation];
    if ([self.full containsString:ss]) {
        return [self.full componentsSeparatedByString:ss].lastObject;
    }else if([self.full containsString:XMMPP_BASESTR]){
         return [self.full componentsSeparatedByString:XMMPP_BASESTR].firstObject;
    }
    return nil;
}

- (NSString *)xm_isFromID {

    if (self.xm_isChatSigal) {
        return self.xm_jidNumber;
    }else{
        return self.xm_roomUserID;
    }

}

@end
