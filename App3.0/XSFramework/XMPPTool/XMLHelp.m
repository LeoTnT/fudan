//
//  XMLHelp.m
//  XMPPDemo
//
//  Created by apple on 2018/1/30.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMLHelp.h"

@implementation XMLHelp



+ (void) exchanegMethodClas:(Class)clas oldMetgod:(SEL)sel newMethod:(SEL)other {
    Method m1 = class_getInstanceMethod(clas, sel);
    Method m2 = class_getInstanceMethod(clas, other);
    method_exchangeImplementations(m1, m2);
}

/*
 
 实体请求所有当前的订阅
 
 
 <iq xmlns="jabber:client" type="result" to="syw@pc-201606131421/53057991-tigase-3" from="pubsub.pc-201606131421">
 <pubsub xmlns="http://jabber.org/protocol/pubsub">
 <subscriptions>
 <subscription jid="syw@pc-201606131421" subscription="subscribed" node="boyNode">
 </subscription>
 </subscriptions>
 </pubsub>
 </iq>
 
 */
+ (XMPPIQ *) queryPubsubInfor {
    NSXMLElement *element = [NSXMLElement elementWithName:@"pubsub" xmlns:@"http://jabber.org/protocol/pubsub"];
    [element addChild:[DDXMLNode elementWithName:@"subscriptions"]];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" child:element];
    [iq addAttributeWithName:@"form" stringValue:[XMPPManager sharedManager].stream.myJID.user];
    [iq addAttributeWithName:@"to" stringValue:[XMPPMUCManager sharedManager].pubSubID];
    return iq;
}



/*
  <iq xmlns="jabber:client" type="result" from="pubsub.pc-201606131421" to="syw@pc-201606131421/53057991-tigase-48"><pubsub xmlns="http://jabber.org/protocol/pubsub"><items node="boyNode"><item id="1001"><message xmlns="pubsub:cvtalk"><body>1001:消息发布:Mon Jan 29 15:07:05 CST 2018</body></message></item><item id="voice-guide1517213069134"><picture xmlns="pubsub:test:picture"><title>标题</title><content>内容</content></picture></item><item id="voice-guide1517214690298"><picture xmlns="pubsub:test:picture"><title>标题</title><content>内容</content></picture></item><item id="voice-guide1517277145023"><message xmlns="pubsub:cvtalk"><body>:消息发布:Tue Jan 30 09:52:25 CST 2018</body></message></item><item id="voice1517291645095"><message xmlns="pubsub:cvtalk"><body>:消息发布:Tue Jan 30 13:54:05 CST 2018</body></message></item><item id="voice1517291871911"><message xmlns="pubsub:cvtalk"><body>:测试消息发布:Tue Jan 30 13:57:51 CST 2018</body></message></item><item id="voice1517292504332"><message xmlns="pubsub:cvtalk"><body>:测试消息发布:Tue Jan 30 14:08:24 CST 2018</body></message></item></items></pubsub></iq>
 */

/**
<iq type="get" form="syw@pc-201606131421/53057991-tigase-48" to="pubsub.pc-201606131421">
 
     <pubsub xmlns="http://jabber.org/protocol/pubsub">
 
           <items node="boyNode"> </items>
 
      </pubsub>

 </iq>

 @return return 返回所有条目
 
 */
+ (XMPPIQ *)getServiceMessage {
    NSXMLElement *element = [NSXMLElement elementWithName:@"pubsub" xmlns:@"http://jabber.org/protocol/pubsub"];
    NSXMLElement *ee = [NSXMLElement elementWithName:@"items"];
    [ee addAttributeWithName:@"node" stringValue:@"boyNode"];
//    [ee addAttributeWithName:@"max_items" stringValue:@"2"]; // 返回最近2条
    [element addChild:ee];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" child:element];
    [iq addAttributeWithName:@"form" stringValue:[XMPPManager sharedManager].stream.myJID.user];
    [iq addAttributeWithName:@"to" stringValue:[XMPPMUCManager sharedManager].pubSubID];
    return iq;
}


+ (XMPPIQ *) getPubSubFormServier {
    NSXMLElement *element = [NSXMLElement elementWithName:@"pubsub" xmlns:@"http://jabber.org/protocol/pubsub"];
    NSXMLElement *ee = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#info"];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" child:element];
    [element addChild:ee];
    [iq addAttributeWithName:@"form" stringValue:[XMPPManager sharedManager].stream.myJID.user];
    return iq;
}


+ (void) getMemberOfRoom:(NSString *)roomJID {
    
    [[XMPPManager sharedManager].stream sendElement:[XMPPIQ getMembersOfRoom:roomJID]];
    
}

@end
