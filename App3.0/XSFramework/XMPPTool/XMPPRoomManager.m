//
//  XMPPRoomManager.m
//  App3.0
//
//  Created by apple on 2018/2/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPRoomManager.h"

@implementation XMPPRoomManager
{
    BOOL isCreate;
}
@synthesize xmppRoom = _xmppRoom;
@synthesize xmpp_roomName = _xmpp_roomName;
@synthesize creatTime = _creatTime;
@synthesize node = _node;

- (instancetype)initRoomWithJID:(XMPPJID *)roomJid {
    
    if (self = [super init]) {
        isCreate = NO;
        
        XMPPRoomCoreDataStorage *dataStorage = [XMPPRoomCoreDataStorage sharedInstance];
        _xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:dataStorage jid:roomJid dispatchQueue: dispatch_get_main_queue() ];
        [_xmppRoom activate:[XMPPManager sharedManager].stream];
        NSXMLElement *p=[NSXMLElement elementWithName:@"history"];
        [p addAttributeWithName:@"maxstanzas" stringValue:@"0"];
        [_xmppRoom joinRoomUsingNickname:[UserInstance ShardInstnce].uid history:p];
        [_xmppRoom addDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
        _xmpp_roomName = roomJid.full;
        
        
    }
    return self;
}




- (instancetype) initCreatRoomWith:(XMPPJID *)roomJid {
    
    if (self = [super init]) {
        
        isCreate = YES;
        XMPPRoomCoreDataStorage *dataStorage = [XMPPRoomCoreDataStorage sharedInstance];
        
        _xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:dataStorage jid:roomJid dispatchQueue:dispatch_get_main_queue()];
        [_xmppRoom activate:[XMPPManager sharedManager].stream];
        NSXMLElement *p=[NSXMLElement elementWithName:@"history"];
        [p addAttributeWithName:@"maxstanzas" stringValue:@"0"];
        [_xmppRoom joinRoomUsingNickname:[UserInstance ShardInstnce].uid history:p password:nil];
        [self configNewRoom:_xmppRoom];
        [_xmppRoom addDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
        //         _roomMessage = [RACSubject subject];
        _xmpp_roomName = roomJid.full;
        
    }
    return self;
}

/**
 * Invoked when a message is received.
 * The occupant parameter may be nil if the message came directly from the room, or from a non-occupant.
 **/
- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID {
    
    
    
}



- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
    
    XMPP_LOG(@"*******create group success ******* %@",sender.roomJID);
    
}

- (void)configNewRoom:(XMPPRoom *)xmppRoom
{
    NSXMLElement *x = [NSXMLElement elementWithName:@"x"xmlns:@"jabber:x:data"];
    NSXMLElement *p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var"stringValue:@"muc#roomconfig_persistentroom"];//永久房间
    [p addChild:[NSXMLElement elementWithName:@"value"stringValue:@"1"]];
    [x addChild:p];
    
    p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var"stringValue:@"muc#roomconfig_maxusers"];//最大用户
    [p addChild:[NSXMLElement elementWithName:@"value"stringValue:@"2000"]];
    [x addChild:p];
    
    p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var"stringValue:@"muc#roomconfig_changesubject"];//允许改变主题
    [p addChild:[NSXMLElement elementWithName:@"value"stringValue:@"1"]];
    [x addChild:p];
    
    p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var"stringValue:@"muc#roomconfig_memberroom"];//公共房间
    [p addChild:[NSXMLElement elementWithName:@"value"stringValue:@"0"]];
    [x addChild:p];
    
    p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var"stringValue:@"muc#roomconfig_allowinvites"];//允许邀请
    [p addChild:[NSXMLElement elementWithName:@"value"stringValue:@"1"]];
    [x addChild:p];
    
    [xmppRoom configureRoomUsingOptions:x];
}


- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
    
    
    if ( isCreate &&_creatRommSuccess) {
        XMPP_LOG(@"******* 创建群  并  加入群成功*******");
        [_xmppRoom fetchModeratorsList];
           _creatRommSuccess();
        NSString *memberList = self.menberString;
        if (isEmptyString(memberList)) {
            return;
        }
        NSArray *member = [memberList componentsSeparatedByString:@","];
        [member enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *jidstring = member[idx];
            if (![jidstring isEqualToString:[UserInstance ShardInstnce].uid]) {
                XMPPJID *jid = [XMPPJID getUserJIDWithString:jidstring];
                XMPP_LOG(@"jid   == %@",jid);
                [self.xmppRoom inviteUser:jid withMessage:NSLocalizedString(@"加入我们", nil)];
            }
        }];
     
    }
    
}


- (void)xmppRoomDidLeave:(XMPPRoom *)sender {
    
    
    [XMPPRoomModel roomDestroy:sender.roomJID.user];
    
    
    
}


//TODO:群成员状态改变
- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
    XMPP_LOG(@"%@",presence)
    if (![presence.type isEqualToString:@"unavailable"]) {
        if ([occupantJID.resource isEqualToString:[UserInstance ShardInstnce].uid]) {
            [XMPPRoomModel roomDestroy:sender.roomJID.user];
        }else{
            
            [XMPPRoomModel getRoomInfor:occupantJID.user];
            
        }
        
    }
}


- (NSXMLElement *) elementWithVar:(NSString *)var childValue:(NSString *)value {
    NSXMLElement *p = [NSXMLElement elementWithName:@"field" ];
    [p addAttributeWithName:@"var" stringValue:var];
    [p addChild:[NSXMLElement elementWithName:@"value" stringValue:value]];
    return p;
}
- (void)xmppRoomDidDestroy:(XMPPRoom *)sender {
    
    XMPP_LOG(@"群解散了 %@",sender.roomJID)
    [XMPPRoomModel roomDestroy:sender.roomJID.user];
    
    
}


- (void)dealloc {
    
    XMPP_LOG(@"消失 XMPPRoomManager  ")
}

@end

