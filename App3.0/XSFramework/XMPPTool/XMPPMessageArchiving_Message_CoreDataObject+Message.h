//
//  XMPPMessageArchiving_Message_CoreDataObject+Message.h
//  App3.0
//
//  Created by Sunny on 2018/4/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>
#import "UUMessageFrame.h"

/**
 单聊 数据模型
 */
@interface XMPPMessageArchiving_Message_CoreDataObject (Message)


//@property (nonatomic,copy) NSString *messageModel;
@property (nonatomic,strong) NSString *messageID;
@property (nonatomic,strong) NSString *uumessageString;
@property (nonatomic,strong) UUMessage *uumessage;


/**
 
UUMessageStatusDelivering = 0,        \~chinese 正在发送 \~english Delivering
UUMessageStatusSuccessed  1,         \~chinese 发送成功 \~english Successed
UUMessageStatusFailed  2,            \~chinese 发送失败 \~english Failed
UUMessageStatusUnRead  3,          / ~chinese 未读 \~english Failed
UUMessageStatusRead 4,
 **/
@property (nonatomic,strong) NSString *messageStatus;

@property (nonatomic,strong) UUMessageFrame *uuFrame;
@end
