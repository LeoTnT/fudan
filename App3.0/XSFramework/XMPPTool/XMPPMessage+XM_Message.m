//
//  XMPPMessage+XM_Message.m
//  BIT
//
//  Created by Sunny on 2018/5/9.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMPPMessage+XM_Message.h"

@implementation XMPPMessage (XM_Message)

+(void)load {
    
    Method  one = class_getInstanceMethod(self, @selector(isGroupChatMessageWithBody));
    Method  two= class_getInstanceMethod(self, @selector(xm_isGroupChatMessageWithBody));
    method_exchangeImplementations(one, two);
    
    
    Method m1 = class_getInstanceMethod(self, @selector(isMessageWithBody));
    Method m2 = class_getInstanceMethod(self, @selector(isMessageWithBody));
        method_exchangeImplementations(m1, m2);
}

- (BOOL)xm_isMessageWithBody {
   
    if (([self elementForName:@"body"] != nil)) {
        NSString *body = [[self elementForName:@"body"] stringValue];
        if (([body length] > 0)) {
            XMPP_RoomMessage *rooMessage = [XMPP_RoomMessage mj_objectWithKeyValues:self.body.mj_keyValues];
            NSString *ext = rooMessage.ext;
            if (ext.length >0) {
                NSDictionary *extdic =  ext.mj_JSONObject;
                NSString *type = [extdic objectForKey:MSG_TYPE];
                if (type.length >0) {
                    NSString *group_INVITE_OWNER = extdic[GROUP_INVITE_OWNER];
                    if ([type isEqualToString:GROUP_INVITE_NEED_CONFIRM] && ![self.to.user isEqualToString:group_INVITE_OWNER]) {
                        return NO;
                    }else{
                        return YES;
                    }
                }else{
                    return YES;
                }
                
            }else{
                return YES;
            }
            
            return NO;
        }
        return NO;
    }else{
        return NO;
    }
 }

- (BOOL)xm_isGroupChatMessageWithBody
{
    if ([self isGroupChatMessage])
    {
        NSString *body = [[self elementForName:@"body"] stringValue];
        if (([body length] > 0)) {
            XMPP_RoomMessage *rooMessage = [XMPP_RoomMessage mj_objectWithKeyValues:self.body.mj_keyValues];
            NSString *ext = rooMessage.ext;
            if (ext.length >0) {
                NSMutableDictionary *extdic = ext.mj_JSONObject;
                NSString *type = [extdic objectForKey:MSG_TYPE];
                if (type.length >0) {
                    NSString *group_INVITE_OWNER = extdic[GROUP_INVITE_OWNER];
                    if ([type isEqualToString:GROUP_INVITE_NEED_CONFIRM] && ![self.to.user isEqualToString:group_INVITE_OWNER]) {
                        return NO;
                    }else{
                        return YES;
                    }
                }else{
                    return YES;
                }
                
            }else{
                return YES;
            }
            
            return NO;
        }
        return NO;
    }
    
    return NO;
}


- (NSString *)messageExtension {
    
    
    NSXMLElement *request = [self elementForName:@"x"];
    NSString *type;
    if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ){
        NSString *jsonStr = [request elementForName:@"ext"].stringValue;
        NSDictionary *dic = jsonStr.mj_JSONObject;
        type = dic[MSG_TYPE];
        
    }
    return type;
}

- (NSDictionary *) messageEXTDic {
    NSXMLElement *request = [self elementForName:@"x"];
    NSDictionary *dic;
    if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ){
        NSString *jsonStr = [request elementForName:@"ext"].stringValue;
        dic = jsonStr.mj_JSONObject;
    }
    return dic;
}

@end
