//
//  XMPPMUCManager.m
//  XMPPDemo
//
//  Created by apple on 2018/1/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMPPMUCManager.h"

@implementation XMPPMUCManager


@synthesize pubSubID = _pubSubID;
static RACSubject *_roomInvacation;
+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static XMPPMUCManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[XMPPMUCManager alloc] init];
        _roomInvacation = [RACSubject subject];
    });
    return manager;
}

-(RACSubject *)roomInvacation{return _roomInvacation;}

- (void)xmppMUC:(XMPPMUC *)sender didDiscoverServices:(NSArray *)services {
    
}

- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *)roomJID didReceiveInvitation:(XMPPMessage *)message {
    
    if ([message isChatRoomInvite]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __block BOOL isCotain = NO;
            
            [[XMPPManager sharedManager].roomList.rac_sequence.signal subscribeNext:^(XMPPRoomManager *manager) {
                XMPP_LOG(@"manager   =%@   2 =%@",manager.xmppRoom.roomJID.user,roomJID.user)

                if ([manager.xmppRoom.roomJID.user isEqualToString:roomJID.user])  {
                    isCotain = YES;
                    NSLog(@"  - - -- - - 包含");
                }
            }completed:^{
                if (!isCotain) {
                    XMPP_LOG(@"  * * * * *上线重新加入群 * * * * *")
                    XMPPRoomManager *rooManager = [[XMPPRoomManager alloc] initRoomWithJID:roomJID ];
                    [[XMPPManager sharedManager].roomList addObject:rooManager];
                }else{
                    NSLog(@" - - - --  -- - - -- -wewe");
                }
            }];
        });
        [_roomInvacation sendNext:roomJID];
    }
    
    NSLog(@"%s",__FUNCTION__);
    
}
- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *)roomJID didReceiveInvitationDecline:(XMPPMessage *)message {
    
    NSLog(@" roomJID =%@ %@",roomJID,message);
}





@end

