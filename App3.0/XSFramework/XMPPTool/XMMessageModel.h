//
//  XMMessageModel.h
//  App3.0
//
//  Created by Sunny on 2018/6/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMMessageModel : NSObject

@property (nonatomic ,strong ,readonly)NSFetchedResultsController *resultController;



@property (nonatomic ,strong) RACSignal *searchTextSignal;

- (instancetype)initSearchText:(NSString *)string bareJidStr:(NSString *)bareJidStr;
@end
