//
//  XMPPFriendModel.h
//  App3.0
//
//  Created by Sunny on 2018/6/20.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPFriendModel : NSObject


/**
 好友关系更新

 @param uid uid description
 */
+ (void) updataUserInfor:(NSString *)uid relation:(NSNumber *)number;



/**
 获取用户信息

 */
+ (void) getUserInfor:(NSString *)uid;
@end
