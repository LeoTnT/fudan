//
//  XMLHelp.h
//  XMPPDemo
//
//  Created by apple on 2018/1/30.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLHelp : NSObject


+ (void) exchanegMethodClas:(Class)clas oldMetgod:(SEL)sel newMethod:(SEL)other;
/**
 
 
 实体请求所有当前的订阅
 
<iq type="get" form="syw@pc-201606131421/53057991-tigase-45" to="pubsub.pc-201606131421">
    <pubsub xmlns="http://jabber.org/protocol/pubsub">
     <subscriptions>
     </subscriptions>
    </pubsub>
 </iq>

 @return return
 
 <iq xmlns="jabber:client" type="result" from="pubsub.pc-201606131421" to="syw@pc-201606131421/53057991-tigase-45">
    <pubsub xmlns="http://jabber.org/protocol/pubsub">
       <subscriptions>
          <subscription jid="syw@pc-201606131421" node="boyNode" subscription="subscribed"/>
       </subscriptions>
    </pubsub>
 </iq>
 
 */
+ (XMPPIQ *) queryPubsubInfor;


/**
 返回所有条目

 @return return value description
 */
+ (XMPPIQ *)getServiceMessage;



/**
 获取服务

 @return return value description
 */
+ (XMPPIQ *) getPubSubFormServier;

/**
 获取群成员列表
 node 0 未在线  1 在线
 返回格式
 @code
 <iq xmlns="jabber:client" to="syw@wang/3641859-tigase-19" type="result" from="room8@muc.wang">
 <query xmlns="http://jabber.org/protocol/disco#items">
 <item name="wang" node="0" jid="wang@wang/Spark"></item>
 <item name="test" node="1" jid="test@wang/Spark"></item>
 <item name="成员-syw@wang" node="1" jid="syw@wang/3641859-tigase-1"></item>
 </query>
 </iq>
 @endcode
 */
+ (void) getMemberOfRoom:(NSString *)roomJID;


@end
