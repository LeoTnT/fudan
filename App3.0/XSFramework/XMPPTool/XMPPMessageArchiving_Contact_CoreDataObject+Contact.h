//
//  XMPPMessageArchiving_Contact_CoreDataObject+Contact.h
//  App3.0
//
//  Created by Sunny on 2018/4/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>

@interface XMPPMessageArchiving_Contact_CoreDataObject (Contact)

@property (nonatomic,strong) NSString *contactString;

@property (nonatomic,strong) ConversationModel *contactModel;
@property (nonatomic,strong) NSString *messageCount;

@end
