//
//  XMPPCoredataChatContact+CoreDataProperties.m
//  
//
//  Created by Sunny on 2018/6/4.
//
//

#import "XMPPCoredataChatContact+CoreDataProperties.h"

@implementation XMPPCoredataChatContact (CoreDataProperties)

+ (NSFetchRequest<XMPPCoredataChatContact *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"XMPPCoredataChatContact"];
}

@dynamic messageCount;
@dynamic contactModel;
@dynamic contactString;
@dynamic streamBareJidStr;
@dynamic bareJidStr;
@dynamic bareJid;
@dynamic mostRecentMessageTimestamp;
@dynamic isBurnMessage;
@dynamic mostRecentMessageBody;
@dynamic contactType;
@end
