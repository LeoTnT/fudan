//
//  XMPPMUCManager.h
//  XMPPDemo
//
//  Created by apple on 2018/1/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPMUCManager : NSObject

+(instancetype)sharedManager;


/**
 
 RACSubject send XMPPRoomManager Object
 
 */
@property (nonatomic ,strong ,readonly)RACSubject *roomInvacation;

@property (nonatomic ,copy ,readonly)NSString *pubSubID;


@end
