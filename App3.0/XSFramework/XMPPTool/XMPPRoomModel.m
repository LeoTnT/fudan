//
//  XMPPRoomModel.m
//  App3.0
//
//  Created by Sunny on 2018/6/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPRoomModel.h"

@implementation XMPPRoomModel


/**
 roomjidstring 

 @param roomID **********、**********@muc.localhost.localdomain
 @return **********@muc.localhost.localdomain
 */
+ (NSString *) roomIDWithString:(NSString *)roomID {
    NSString *roomJIDString;
    roomJIDString = roomID;
    if (![roomJIDString containsString:XMPPR_ROOM_Regulation]) roomJIDString = [NSString stringWithFormat:@"%@%@",roomID,XMPPR_ROOM_Regulation];
    return roomJIDString;
}

+ (NSString *) getRoomID:(NSString *)roomInfor {
    if (isEmptyString(roomInfor)) return nil;
    
    if ([roomInfor containsString:XMPPR_ROOM_Regulation]) {
       CGFloat lenth = roomInfor.length - XMPPR_ROOM_Regulation.length;
       roomInfor = [roomInfor substringFromIndex:lenth];
    }
    
    return roomInfor;
}


+ (void)queryRoomOfJoined {
    XMPPIQ *iq = [XMPPIQ queryRoomOfJoined];
    if (!iq) return;
    [[XMPPManager sharedManager].stream sendElement:iq];
}


+ (void)queryRoomForService:(void (^)(NSArray *))roomArr {
    
    [[XSHTTPManager rac_POSTURL:REQUEST_URL(@"user/groupchat/GroupListsMy") params:nil] subscribeNext:^(resultObject *object) {
         if (object.status) {
            if (roomArr) {
               NSArray *arr = object.data[@"data"];
                roomArr(arr);
            }
        }
    }];
 
}

+ (void) joinRoom:(NSArray *)roomArr {
    NSMutableArray *roomIDArr = [NSMutableArray array];
    [roomArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        GroupMemberAddResultModel *dataModel = [GroupMemberAddResultModel mj_objectWithKeyValues:obj];
        if (!isEmptyString(dataModel.gid)) {
            [roomIDArr addObject:dataModel.gid];
            NSString *roomJID = [NSString stringWithFormat:@"%@%@",dataModel.gid,XMPPR_ROOM_Regulation];
            XMPPJID *jid = [XMPPJID jidWithString:roomJID];
            XMPPRoomManager *roomManager = [[XMPPRoomManager alloc] initRoomWithJID:jid];
            [[XMPPManager sharedManager].roomList addObject:roomManager];
        }
    }];
    
    
}

+ (void) joinRoomFormService:(NSArray *)roomArr {
    if (roomArr) {
        [XMPPRoomModel joinRoom:roomArr];
    }else{
        [XMPPRoomModel queryRoomOfJoined];
    }
 }

//TODO: 查询加入群，没加入 删除会话及历史消息。
+ (void) isJoinRoomWithID:(NSString *)roomID isFinash:(void(^)(BOOL))finash {
    
    [[XSHTTPManager rac_POSTURL:REQUEST_URL(@"user/groupchat/GroupListsMy") params:nil] subscribeNext:^(resultObject *object) {
        if (object.status) {
             NSArray *arr = object.data[@"data"];
            __block BOOL isContain;
            [arr.rac_sequence.signal subscribeNext:^(NSDictionary *dic) {
                 if ([dic[@"gid"] isEqualToString:roomID]) {
                    isContain = YES;
                }
            } completed:^{
                if (finash) {
                    finash(isContain);
                }
            }];
 
        }
    }];
}

// TODO:xmpp 删除房间   不应该直接删除使用，先确定不在房间在执行删除。
+ (void) xmpp_roomDeletedWithName:(NSString *)roomName {
    if (isEmptyString(roomName)) return;
    NSArray *arrr = [XMPPManager sharedManager].roomList;
    
    [arrr.rac_sequence.signal subscribeNext:^(XMPPRoomManager *manager) {
        
        NSLog(@"manager   =%@   roomName =%@",manager.xmppRoom.roomJID.user,roomName);
        if ([manager.xmppRoom.roomJID.user isEqualToString:roomName]) {
            [manager.xmppRoom leaveRoom];
            [manager.xmppRoom deactivate];
             [manager.xmppRoom removeDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
            [[XMPPManager sharedManager].roomList removeObject:manager];
            manager = nil;
            NSLog(@"删除 加入群数据");
        }
    }];
    
}



// TODO:删除群回话，群聊天历史记录  是否包含群，包含不删除数据，不包含删除数据
+ (void) roomDestroy:(NSString *)roomName {
    
    [XMPPRoomModel checkIsJoinGroup:roomName finash:^(BOOL isJoin, GroupDataModel *groupModel) {
        if (!isJoin) {
            [[XMPPManager sharedManager].deletedMessageSubject sendNext:RACTuplePack(@"deleteRoom",nil)];
            [XMPPRoomModel xmpp_roomDeletedWithName:roomName];
            [XMPPRoomModel deletedRoomHistory:roomName];
            
        }
    }];
 
    
}

//TODO:删除群历史记录  *******@muc.localhost.localdomain
+ (void) deletedRoomHistory:(NSString *)roomID {
    roomID = [XMPPRoomModel roomIDWithString:roomID];
    [XMPPSignal MR_deletedContactID:roomID];
    [XMPPSignal deletedContactListWith:roomID];
    [XMPPSignal deleteGroupHistoryWithRoomID:roomID];
}


+ (void) getRoomInfor:(NSString *)roomID {
    if (isEmptyString(roomID))  return;
    
    [HTTPManager fetchGroupInfoWithGroupId:roomID getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
        if (state.status) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
                [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
            });
        }else{
            NSString *roomJIDString;
            roomJIDString =  [XMPPRoomModel roomIDWithString:roomID];
            [XMPPRoomModel deletedRoomHistory:roomJIDString];
        }
        [[XMPPManager sharedManager].deletedMessageSubject sendNext:RACTuplePack(@"reloadRoomMember",nil)];
    } fail:^(NSError * _Nonnull error) {
        
    }];
}


//TODO: roomID
+ (void) getRoomInfor:(NSString *)roomID getSuccess:(void(^)(NSString *))title failure:(void(^)())failure {
    if (isEmptyString(roomID)) return;
    
    
    [HTTPManager fetchGroupInfoWithGroupId:roomID getMembers:0 order:@"time" success:^(NSDictionary * dic, resultObject *state) {
        if (state.status) {
             GroupFetchParser *parser = [GroupFetchParser mj_objectWithKeyValues:dic];
             [[DBHandler sharedInstance] addOrUpdateGroup:parser.data];
            if (title) title(parser.data.name);
        }else{
            NSString *roomJIDString = [XMPPRoomModel roomIDWithString:roomID];
            [XMPPRoomModel deletedRoomHistory:roomJIDString];
            if (failure) failure();
        }
    } fail:^(NSError * _Nonnull error) {
        
    }];
}


+ (void) checkIsJoinGroup:(NSString *)roomID finash:(void(^)(BOOL isJoin ,GroupDataModel *groupModel))finash {
    [HTTPManager CheckUserInGroupWithGroupId:roomID success:^(NSDictionary * dic, resultObject *state){
        if (state.status) {
            GroupDataModel *groupModel = [GroupDataModel mj_objectWithKeyValues:dic[@"data"]];
            [[DBHandler sharedInstance] addOrUpdateGroup:groupModel];
            if (groupModel.in_group) {
                if (finash) finash(YES,groupModel);
            } else {
                if (finash)  finash(NO,groupModel);
            }
            
        } else {
            if (finash)  finash(NO,nil);
        }
    } fail:nil];
    
}

@end


@implementation RoomInfor

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"publicID":@"public"};
}

@end
