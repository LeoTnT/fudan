//
//  XMPPRoomManager.h
//  App3.0
//
//  Created by apple on 2018/2/24.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 群 * * * * *
 */
@interface XMPPRoomManager : NSObject

@property (nonatomic ,strong ,readonly)XMPPRoom *xmppRoom;


@property (nonatomic ,strong ,readonly)NSString *xmpp_roomName;


@property (nonatomic ,strong ,readonly)NSString *creatTime;

@property (nonatomic ,copy)void (^creatRommSuccess)();


@property (nonatomic ,strong)NSString *menberString;
/**
 是否重新加入
 */
@property (nonatomic ,assign) BOOL isJoined;

/**
 1是创建者，0非创建者
 */
@property (nonatomic ,assign)BOOL node;



/**
 加入群
 
 @param roomJid roomJid description
 @return return value description
 */
- (instancetype)initRoomWithJID:(XMPPJID *)roomJid;

- (instancetype) initCreatRoomWith:(XMPPJID *)roomJid ;



@end

