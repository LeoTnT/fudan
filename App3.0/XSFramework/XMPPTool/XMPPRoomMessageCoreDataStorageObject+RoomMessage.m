//
//  XMPPRoomMessageCoreDataStorageObject+RoomMessage.m
//  App3.0
//
//  Created by Sunny on 2018/5/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPRoomMessageCoreDataStorageObject+RoomMessage.h"

@implementation XMPPRoomMessageCoreDataStorageObject (RoomMessage)
@dynamic uumessage;
@dynamic uumessageString;
 

-(UUMessage *)uumessage {
    [self willAccessValueForKey:@"uumessage"];
    
    UUMessage *uumessage = [UUMessage mj_objectWithKeyValues:self.uumessageString.mj_keyValues];
    
    [self didAccessValueForKey:@"uumessage"];
    
    return uumessage ? uumessage:nil;
}

/**
群成员 确认 操作
*/
- (UUMessage *) updateRoomMessageWithBody:(NSDictionary *)dic {
    XMPPMessage *xmppMessage = self.message;
    NSString*messString = xmppMessage.body;
    UUMessage *uuMessage = [UUMessage new];
    uuMessage.messageId = self.message.elementID;
    uuMessage.strName = self.nickname;
    NSDateFormatter* formatter = [XSTool shareYMDHMS];
    uuMessage.strTime = [formatter stringFromDate:self.localTimestamp];
    uuMessage.from = !self.isFromMe;
    uuMessage.chatType = UUChatTypeGroupChat;
    uuMessage.strId = self.jid.xm_isFromID;
    uuMessage.messageId = self.message.elementID;
    uuMessage.body = self.body;
    
    if (dic)   uuMessage.strNotice = [self changeNoticeTitle:xmppMessage ext:dic];
    
    return uuMessage;
}

- (NSString *) changeNoticeTitle:(XMPPMessage *)message ext:(NSDictionary *)ext  {
    NSString *str = nil;
    if ([ext[GROUP_INVITE_OWNER] isEqualToString:[UserInstance ShardInstnce].uid]) {
        // 是群主
        NSDictionary *dic = [message.body mj_JSONObject];
        XMPP_RoomMessage *roomMessage = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
        
        str = [NSString stringWithFormat:@"%@ %@",roomMessage.body,NSLocalizedString(@"去确认" ,nil)];
        
        if ([ext[MSG_TYPE] isEqualToString:GROUP_INVITE_CONFIRM]) {
            str = [NSString stringWithFormat:@"%@ %@",roomMessage.body,NSLocalizedString(@"已确认",nil)];
        }
    } else {
        str = nil;
    }
    return str;
}

- (UUMessage *) roomMessageWithBody {
    XMPPMessage *xmppMessage = self.message;
    
    
    
    NSString*messString = xmppMessage.body;
    UUMessage *uuMessage = [UUMessage new];
    uuMessage.messageId = self.message.elementID;
    uuMessage.strName = self.nickname;
    NSDateFormatter* formatter = [XSTool shareYMDHMS];
    uuMessage.strTime = [formatter stringFromDate:self.localTimestamp];
    uuMessage.from = !self.isFromMe;
    uuMessage.chatType = UUChatTypeGroupChat;
    uuMessage.strId = self.jid.xm_isFromID;
    uuMessage.messageId = self.message.elementID;
    uuMessage.body = self.body;
    NSDictionary *dic = [messString mj_JSONObject];
    NSString *type = dic[@"type"];
    
    
    if ([type isEqualToString:@"txt"]) {
        XMPP_RoomMessage *message = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
        NSDictionary *dic = message.ext.mj_JSONObject;
        if (!dic) {
            uuMessage.type = XMPPMessageTypeText;
            uuMessage.strContent =  message.body;
        } else {
            uuMessage.ext = dic;
            NSString *type = dic[MSG_TYPE];
            if ([type isEqualToString:READ_PACKET]) {
                // 红包
                uuMessage.type = XMPPMessageTypeRedPacket;
                uuMessage.rpId = dic[CUSTOM_MSG_ID];
                uuMessage.strRedMessage = message.body;
                uuMessage.moneyStr = dic[TRANSFER_BANLANCE_TAG];
            } else if ([type isEqualToString:TRANSFER_BANLANCE] || [type isEqualToString:TRANSFER_MONEY_COLLECTION] || [type isEqualToString:TRANSFER_MONEY_COLLECTION_REJECT]) {
                // 转账
                uuMessage.type = XMPPMessageTypeTransfer;
                uuMessage.rpId = dic[CUSTOM_MSG_ID];
                uuMessage.strRedMessage = message.body;
                uuMessage.moneyStr = dic[TRANSFER_BANLANCE_TAG];
            } else if ([type isEqualToString:MESSAGE_SHARE_TYPE]) {
                // 分享
                uuMessage.type = UUMessageTypeShare;
                uuMessage.shareJosnString = dic[MESSAGE_SHARE_DATA];
            } else if ([type isEqualToString:MESSAGE_SYSTEM_PUSH]) {
                // 系统推送
                uuMessage.type = UUMessageTypeSystemPush;
                uuMessage.shareJosnString = message.body;
            } else {
                uuMessage.strNotice = [uuMessage noticeStringConvertByMessage:xmppMessage ext:dic];
            }
        }
        
    }else if ([type isEqualToString:@"voice"]){
        XMPP_RoomVoiceMessage *message = [XMPP_RoomVoiceMessage mj_objectWithKeyValues:dic];
        uuMessage.type = XMPPMessageTypeVoice;
        if (!isEmptyString(message.body)) {
            uuMessage.downVoicePath = [NSURL URLWithString:message.body];
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        NSString *filName = [ message.body componentsSeparatedByString:@"/"].lastObject;
        NSString *filePath = [NSString stringWithFormat:@"%@/%@",path,filName];
        uuMessage.voiceUrl = [NSURL URLWithString:filePath];
        uuMessage.strVoiceTime = message.strVoiceTime;
        BOOL isExist = [XMPPSignal isFileExist:filName];
        if (!isExist) {
            [uuMessage downloadVoiceWith:uuMessage.downVoicePath finash:nil];
        }
    }else if ([type isEqualToString:@"image"]){
        XMPP_RoomMessage *message = [XMPP_RoomMessage mj_objectWithKeyValues:dic];
        uuMessage.type = XMPPMessageTypePicture;
        uuMessage.pictureURL =  message.body;
        
    }else if ([type isEqualToString:@"location"]){
        XMPP_RoomLocationMessage *message = [XMPP_RoomLocationMessage mj_objectWithKeyValues:dic];
        uuMessage.type = UUMessageTypeLocation;
        uuMessage.strLocation =  message.body;
        uuMessage.strLatitude = [NSString stringWithFormat:@"%.12f",message.longitude];
        uuMessage.strLongitude = [NSString stringWithFormat:@"%.12f",message.latitude];
    }else if ([type isEqualToString:@"file"]){
        XMPP_RoomFileMessage *message = [XMPP_RoomFileMessage mj_objectWithKeyValues:dic];
        uuMessage.type = UUMessageTypeFile;
        uuMessage.fileUrl = message.body;
        uuMessage.fileTitle = message.displayName;
        uuMessage.fileDesc = message.fileSize;
        
    }
    
    return uuMessage;
}

@end
