
//
//  XMPPManager.m
//  XMPPDemo
//
//  Created by apple on 2018/1/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "XMPPManager.h"
#import "XMPPMessage+XM_Message.h"

const NSTimeInterval XMPPStreamTimeoutNone = 10;

@interface XMPPManager ()<XMPPReconnectDelegate,XMPPAutoPingDelegate>

@property (nonatomic ,strong)XMPPRoom *xmppRoom;
/**
 消息回执
 */
@property (nonatomic ,strong)XMPPMessageDeliveryReceipts *deliveryReceipts;

@property (nonatomic, strong) XMPPMessageArchivingCoreDataStorage *archivingStorage;



/**
 *  聊天模块
 */
@property (nonatomic, strong) XMPPMessageArchiving *messageArchiving;
 
@property (nonatomic ,strong)XMPPStream *stream;

@property (nonatomic ,strong)XMPPMUC *xmppMuc;
@end

@implementation XMPPManager


XIM_StreamState _streamState;
static NSArray *roomDataSource;


static XMPPManager *manager = nil;
//static RACSubject *_deletedMessageSubject;
- (XMPPStream *)stream {return _stream;}


- (RACSubject *)messageSubject {
    
    if (!_messageSubject) {
        _messageSubject = [RACSubject subject];
    }
    
    return _messageSubject;
    
};

+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[XMPPManager alloc] init];
        manager.roomList = [NSMutableArray array];
        manager.deletedMessageSubject = [RACSubject subject];
     });
    return manager;
}


- (NSString *)pwd {
    
    NSString *staticStr = @"msg_easemob_zsefvgyjmkol";
    NSString *uid = [XMPPManager sharedManager].stream.myJID.user;
    return [NSString md5:[NSString stringWithFormat:@"%@%@",staticStr,uid]];
}

- (void) configServer {
    
    if (!self.stream) {
        _stream = [[XMPPStream alloc] init];
 
        _stream.hostName = kHostName;
        _stream.hostPort = kHostPort;
         [_stream setEnableBackgroundingOnSocket:YES];
        [_stream addDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        self.xmppRosterMemoryStorage = [[XMPPRosterMemoryStorage alloc] init];
        self.roster = [[XMPPRoster alloc] initWithRosterStorage:self.xmppRosterMemoryStorage dispatchQueue:dispatch_get_main_queue()];
        [self.roster addDelegate:[XMPPAddFriendsManager shareManager] delegateQueue:dispatch_get_global_queue(0, 0)];
        [self.roster activate:self.stream];
        
        /*  消息回执 */
        self.deliveryReceipts = [[XMPPMessageDeliveryReceipts alloc] init];
        [self.deliveryReceipts activate:_stream];
        
        
        _xmppAutoPing = [XMPPAutoPing new];
        _xmppAutoPing.pingTimeout = 10;
        [_xmppAutoPing activate:_stream];
         _xmppAutoPing.pingInterval=20;
        [_xmppAutoPing addDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
        /**
         *  XMPPReconnect模块激活
         */
        self.reconnect = [[XMPPReconnect alloc] init];
        self.reconnect.reconnectTimerInterval = 20;
        [self.reconnect addDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        [self.reconnect activate:_stream];
        
        /**
         *  XMPPMessageArchiving聊天模块激活
         */
        self.archivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        
        self.messageArchiving = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:self.archivingStorage dispatchQueue:dispatch_get_main_queue()];
        [self.messageArchiving activate:self.stream];
        
        
        
        _xmppMuc = [[XMPPMUC alloc] initWithDispatchQueue:dispatch_get_main_queue()];
        [_xmppMuc activate:self.stream];
        [_xmppMuc addDelegate:[XMPPMUCManager sharedManager] delegateQueue:dispatch_get_main_queue()];
        
        
    }
    
    
}


- (void) getRelationsData {
    [HTTPManager getRelationListWithSuccess:^(NSDictionary *dic, resultObject *state) {
        if (state.status) {
            ContactParser *dataParser = [ContactParser mj_objectWithKeyValues:dic];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSMutableArray *tempArr = [NSMutableArray array];
                NSMutableArray *allContacts = [[DBHandler sharedInstance] getAllContact];
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:allContacts];
                for (ContactDataParser *parser in dataParser.data) {
                    [tempArr addObject:parser];
                    
                    // 加入数据库
                    [[DBHandler sharedInstance] addOrUpdateContact:parser];
                    
                    NSLog(@"%@",parser.nickname);
                    // 更新群组成员数据库
                    NickNameDataParser *nndP = [NickNameDataParser new];
                    nndP.logo = parser.avatar;
                    nndP.nickname = parser.nickname;
                    nndP.uid = parser.uid;
                    [[DBHandler sharedInstance] addOrUpdateGroupMember:nndP];
                    
                    // 数据库排除现有联系人，剩下的就是将要删除的
                    for (ContactDataParser *oldParser in allContacts) {
                        if ([oldParser.uid isEqualToString:parser.uid]) {
                            [deleteArr removeObject:oldParser];
                        }
                    }
                }
                
                // 数据库删除不存在的联系人
                for (ContactDataParser *delParser in deleteArr) {
                    if ([delParser.relation integerValue] > 0) {
                        [[DBHandler sharedInstance] deleteContactByUid:delParser.uid];
                    }
                    
                }
 
            });
        } else {
            
        }
    } fail:^(NSError *error) {
        
    }];
}


+ (NSString *) getStreamConnet:(XIM_STREAMSTATE)type {
    NSString * str;
    if (type == XIM_XMPP_CONNECTDEFILED) {
        str = @"登录失败";
    }else if (type == XIM_XMPP_TIMEOUT){
        str = @"登录超时";
    }else if (type == XIM_XMPP_AUTHFAILD){
        str = @"登录失败";
    }
    
    return str;
}


+ (void) xmppUserLogIn:(NSString *)userName complment:(void(^)(XIM_STREAMSTATE connectedType))complent{
    if (userName.length ==0) return;
    
    @weakify(self);
    [XMPPRoomModel queryRoomForService:^(NSArray *arr) {
        @strongify(self);
        roomDataSource = arr;
    }];
    
    [manager.stream disconnect];
    _streamState = complent;
    XMPPJID *jid = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@%@",userName,XMMPP_BASESTR] resource:XMMPP_BASESTR_CONFIG];
    manager.stream.myJID = jid;
    [manager connectionServer];
    
}


- (void) connectionServer {
    manager.state = XIM_XMPP_CONNECTING;
    if (!_stream.isConnected) {
        NSError *error;
        [_stream connectWithTimeout:10 error:&error];
        if (error) {
            NSLog(@"  - - - --  -- - - errror =%@",error);
            if (_streamState) {
                manager.state = XIM_XMPP_CONNECTDEFILED;
                _streamState(XIM_XMPP_CONNECTDEFILED);
            }
        }
    }
    
}



/**
 重连
 */
- (void) disconnectdXmppStream {
    
    if (manager.state == XIM_XMPP_CONNECTING ||manager.state == XIM_XMPP_CONNECT) {
        return;
    }
    if (manager.state == XIM_XMPP_DISCONNECTED) {
        // 断开连接
        [self xm_reconnect];
    }else if (manager.state == XIM_XMPP_CONNECTDEFILED){
        // 连接失败
        [XMPPManager xmppUserLogIn:[UserInstance ShardInstnce].uid complment:nil];
    }else if (manager.state == XIM_XMPP_TIMEOUT || manager.state == XIM_XMPP_NOTCONNECT_NEVER){
        // 超时
        [[XMPPManager sharedManager]xmppUserLogout];
        [XMPPManager xmppUserLogIn:[UserInstance ShardInstnce].uid complment:nil];
        [self connectionServer];
    }else if (manager.state == XIM_XMPP_AUTHFAILD){
        if (connectCount <=5) {
            [self xm_reconnect];
        }
    }
    
}
// TODO : 重连
- (void) xm_reconnect {
    // 断开连接
    if (_stream.isConnected) {
        [_stream authenticateWithPassword:self.pwd error:nil];
    }else{
        [[XMPPManager sharedManager]xmppUserLogout];
        [XMPPManager xmppUserLogIn:[UserInstance ShardInstnce].uid complment:nil];
    }
}

#pragma mark - 上线
- (void)sendOnlineMessage {
    XMPPPresence *pre = [XMPPPresence presenceWithType:@"available"];
    [self.stream sendElement:pre];
}

#pragma mark - 离线时,接受离线消息
- (void)sendOfflineMessage {
    [self getOfflineMsg];
    XMPPPresence *pre = [XMPPPresence presenceWithType:@"unavailable"];
    [self.stream sendElement:pre];
}

/**
 接收离线消息  设置
 */

- (void) getOfflineMsg {
    NSString *jid = [XMPPManager sharedManager].stream.myJID.user;
    XMPPIQ *iq = [[XMPPIQ alloc] initWithXMLString:[NSString stringWithFormat:@"<presence from='%@'><priority>1</priority></presence>",jid]error:nil];
    [self.stream sendElement:iq];
}

#pragma mark -- 用户注销
- (void)xmppUserLogout {
    [self sendOfflineMessage];
    [self.stream removeDelegate:self delegateQueue:dispatch_get_main_queue()];
    [self.xmppAutoPing removeDelegate:self delegateQueue:dispatch_get_global_queue(0, 0)];
    [self.reconnect removeDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    
    [self.stream disconnect];
    self.stream = nil;
    [self.reconnect stop];
    
    manager.state = XIM_XMPP_NOTCONNECT;
    [[XMPPManager sharedManager].roomList removeAllObjects];
    [[XMPPManager sharedManager] configServer];
}






+ (void)queryRoomOfJoined {
    XMPPIQ *iq = [XMPPIQ queryRoomOfJoined];
    if (!iq) return;
    [[XMPPManager sharedManager].stream sendElement:iq];
}


- (void)xmppStreamWillConnect:(XMPPStream *)sender {
    
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called after the tcp socket has connected to the remote host.
 * It may be used as a hook for various things, such as updating the UI or extracting the server's IP address.
 *
 * If developing an iOS app that runs in the background,
 * please use XMPPStream's enableBackgroundingOnSocket property as opposed to doing it directly on the socket here.
 **/
- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    
    
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called after a TCP connection has been established with the server,
 * and the opening XML stream negotiation has started.
 **/
- (void)xmppStreamDidStartNegotiation:(XMPPStream *)sender {
    XMPP_LOG(@"%@",sender)
}


- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings {
    XMPP_LOG(@"%@",sender)
    [settings setObject:[NSNumber numberWithBool:YES]
                 forKey:(NSString *)GCDAsyncSocketManuallyEvaluateTrust];
    
}



/**
 * This method is called after the stream has been secured via SSL/TLS.
 * This method may be called if the server required a secure connection during the opening process,
 * or if the secureConnection: method was manually invoked.
 **/
- (void)xmppStreamDidSecure:(XMPPStream *)sender {
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called after the XML stream has been fully opened.
 * More precisely, this method is called after an opening <xml/> and <stream:stream/> tag have been sent and received,
 * and after the stream features have been received, and any required features have been fullfilled.
 * At this point it's safe to begin communication with the server.
 **/
- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    
    NSError *error;
    
    [_stream authenticateWithPassword:self.pwd error:&error];
    if (error) {
        NSLog(@"authenticateWithPassword  =%@",error.localizedDescription);
    }else{
        
    }
    
    
    
}


- (void) isJoinRoom {
    
    roomDataSource ? [XMPPRoomModel joinRoomFormService:roomDataSource]:
    [XMPPRoomModel queryRoomOfJoined];
}

/**
 没注册 用不到
 **/
- (void)xmppStream:(XMPPStream *)sender didNotRegister:(NSXMLElement *)error {
    
    manager.state = XIM_XMPP_REGISTERFAILED;
    if (_streamState) {
        _streamState(XIM_XMPP_REGISTERFAILED);
    }
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called after authentication has successfully finished.
 * If authentication fails for some reason, the xmppStream:didNotAuthenticate: method will be called instead.
 **/
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    connectCount = 0;
    [[XMPPManager sharedManager].roomList removeAllObjects];
    [self isJoinRoom];
    [[XMPPManager sharedManager].stream sendElement:[XMPPPresence presence]];
    [[XMPPManager sharedManager] sendOnlineMessage];
    manager.state = XIM_XMPP_CONNECT;
    if (_streamState) {
        _streamState(XIM_XMPP_CONNECT);
    }
    [manager getRelationsData];
    @weakify(self);
    [XMPPRoomModel queryRoomForService:^(NSArray *arr) {
        @strongify(self);
        roomDataSource = arr;
    }];
    
    [XMPPSignal setupUnreadMessageCount:nil];
    
}

/**
 密码验证 登录
 **/
- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
    
    manager.state = XIM_XMPP_AUTHFAILD;
    if (_streamState)  _streamState(XIM_XMPP_AUTHFAILD);
    
    NSLog(@"xmppStreamDidAuthenticate   身份认证 失败 didNotAuthenticate");
    if (connectCount >= 5) {
        return;
    }
    connectCount++;
    // 身份认证失败 即是 IM没有注册成功， 需要调用下面接口注册IM
    [HTTPManager CheckUserRegImSuccess:^(NSDictionary *dic, resultObject *state) {
        [_stream authenticateWithPassword:self.pwd error:nil];
    } fail:^(NSError *error) {
//        [_stream authenticateWithPassword:self.pwd error:nil];
    }];
}


- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    
    //    XMPP_LOG(@"didReceive action   * * *  =%@",iq);
    if(iq.childElement.children.count !=0){
        for (DDXMLElement *element in iq.children)
        {
            DDXMLElement *firstAtt = (DDXMLElement *)element.children.firstObject;
            NSString *action = [[firstAtt attributeForName:@"action"] stringValue];
            if ([iq isChatRoomItems]) {
                if ([action isEqualToString:@"rooms"]) {
                    
                    NSMutableArray *array = [NSMutableArray array];
                    [element.children enumerateObjectsUsingBlock:^(DDXMLNode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        DDXMLElement *item =(DDXMLElement *) element.children[idx];
                        NSString *roomJID = [[item attributeForName:@"jid"] stringValue];
                        if (!isEmptyString(roomJID)) {
                            XMPPJID *jid = [XMPPJID jidWithString:roomJID];
                            XMPPRoomManager *roomManager = [[XMPPRoomManager alloc] initRoomWithJID:jid ];
                            [array addObject:roomManager];
                        }
                    }];
                    [[XMPPManager sharedManager].roomList addObjectsFromArray:array];
                    
                    
                }
            }
        }
        
    }
    
    return YES;
}


- (void) showNotice:(XMPPMessage *)message {
    
    NSLog(@"message.from.im_UserID %@",message.from.im_UserID);
    if (isEmptyString(message.fromStr) || [message.from.im_UserID isEqualToString:[UserInstance ShardInstnce].uid]) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (isEmptyString(message.body)) return ;
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
        {
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            
            localNotification.alertBody = @"您有一条新消息";
            localNotification.soundName = @"crunch.wav";//通知声音
            localNotification.applicationIconBadgeNumber +=  1;//标记数
              [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];//发送通知
        }else{
            NSDictionary *ddd  =[[NSUserDefaults standardUserDefaults] objectForKey:USER_CHATSET];
            BOOL is_receiver_voice = [ddd[@"is_receiver_voice"] boolValue];
            if (!isEmptyString(message.body)) {
                if (is_receiver_voice ||!ddd) {
                    [[ChatHelper shareHelper] createSystemSoundWithName:@"message" soundType:@"wav" vibrate:NO];
                }
            }
            
        }
        
    });
}



- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    
 
    [self showNotice:message];
 
    XMPP_LOG(@"message =%@",message);
    if ([message hasReceiptResponse]) {
        [XMPPSignal updateXMPPMessageArchiving:message.from.user messageID:nil];
    }
    
    NSXMLElement *request = [message elementForName:@"x"];
    if ([request.xmlns isEqualToString:XMPP_MESSAGE_VOICT_IMAGE] ) {
        NSURL *url = nil;
        for (NSXMLElement *element in message.children) {
            NSString *type = [element.children.firstObject stringValue];
            if ([type isEqualToString:@"voice"]) {
                NSString *filName = [ message.body componentsSeparatedByString:@"/"].lastObject;
                BOOL isExist = [XMPPSignal isFileExist:filName];
                NSLog(@"isExist  =%@",isExist?@"YES":@"NO");
                if (isExist) return;
                url = [NSURL URLWithString:message.body];
                NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
                AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                    NSString *cachePath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
                    
                    NSString *fileName=[cachePath stringByAppendingPathComponent:response.suggestedFilename];
                    
                    return [NSURL fileURLWithPath:fileName];
                    
                } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                    
                }];
                [downloadTask resume];
            } else if ([type isEqualToString:@"file"]) {
                
            }
            
        }
    }
    
    
    if ([message isChatMessage]) {
        [self.messageSubject sendNext:message];
    }
    
    
    /*
     <message xmlns="jabber:client" from="pubsub.1531879659052" id="1143" to="44@1531879659052">
     <amp xmlns="http://jabber.org/protocol/amp">
     <rule value="2018-08-02T00:00:00Z" condition="expire-at" action="drop">
     </rule>
     </amp>
     <event xmlns="http://jabber.org/protocol/pubsub#event">
     <items node="1531879659052_node">
     <item id="item-2" expire-at="2018-08-02T00:00:00Z">
     <item-entry>
     <title>你好</title>
     <author>你好</author>
     <content>你好</content>
     </item-entry>
     </item>
     </items>
     </event>
     
     
     
     </message>
     */
    if ([XMPPPubSub isPubSubMessage:message]) {
        
        XMPP_LOG(@" 订阅 =%@",message);
    }
    
}




#pragma mark - - - - - - 单点登录判断
- (void)xmppStream:(XMPPStream *)sender didReceiveError:(NSXMLElement *)error {
    
    DDXMLNode *errorNode = (DDXMLNode *)error;
 
        //遍历错误节点
        for(DDXMLNode *node in [errorNode children])
        {
            //若错误节点有【冲突】
            if([[node name] isEqualToString:@"conflict"])
            {
                
                
            XMPP_LOG(@" - - - - - - 单点登录判断")

                [XSTool otherLogIn];
            }
        }
  
    
}





/**
 * These methods are called after their respective XML elements are sent over the stream.
 * These methods may be used to listen for certain events (such as an unavailable presence having been sent),
 * or for general logging purposes. (E.g. a central history logging mechanism).
 **/
- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq {

}
- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    
    
    XMPP_LOG(@" * * * 发送 message =%@",message)
    
    [XMPPManager sharedManager].sendState = XMPPMessageStatusSuccessed;
}
- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence {
    
    
}

/**
 * These methods are called after failing to send the respective XML elements over the stream.
 * This occurs when the stream gets disconnected before the element can get sent out.
 **/
- (void)xmppStream:(XMPPStream *)sender didFailToSendIQ:(XMPPIQ *)iq error:(NSError *)error {
    
    manager.state = XIM_XMPP_DISCONNECTED;
    
   
    
}
- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error {
    
    XMPP_LOG(@"消息发送失败 x%@",message)
    manager.state = XIM_XMPP_DISCONNECTED;
    [XMPPManager sharedManager].sendState = XMPPMessageStatusFailed;
    [self showConnectedType];
    
}
- (void)xmppStream:(XMPPStream *)sender didFailToSendPresence:(XMPPPresence *)presence error:(NSError *)error {
 
    manager.state = XIM_XMPP_DISCONNECTED;
}



/**
 * This method is called if the disconnect method is called.
 * It may be used to determine if a disconnection was purposeful, or due to an error.
 *
 * Note: A disconnect may be either "clean" or "dirty".
 * A "clean" disconnect is when the stream sends the closing </stream:stream> stanza before disconnecting.
 * A "dirty" disconnect is when the stream simply closes its TCP socket.
 * In most cases it makes no difference how the disconnect occurs,
 * but there are a few contexts in which the difference has various protocol implications.
 *
 * @see xmppStreamDidSendClosingStreamStanza
 **/
- (void)xmppStreamWasToldToDisconnect:(XMPPStream *)sender {
    manager.state = XIM_XMPP_DISCONNECTED;
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called after the stream has sent the closing </stream:stream> stanza.
 * This signifies a "clean" disconnect.
 *
 * Note: A disconnect may be either "clean" or "dirty".
 * A "clean" disconnect is when the stream sends the closing </stream:stream> stanza before disconnecting.
 * A "dirty" disconnect is when the stream simply closes its TCP socket.
 * In most cases it makes no difference how the disconnect occurs,
 * but there are a few contexts in which the difference has various protocol implications.
 **/
- (void)xmppStreamDidSendClosingStreamStanza:(XMPPStream *)sender {
    XMPP_LOG(@"%@",sender)
}

/**
 * This method is called if the XMPP stream's connect times out.
 **/
- (void)xmppStreamConnectDidTimeout:(XMPPStream *)sender {
    
    manager.state = XIM_XMPP_TIMEOUT;
    if (_streamState) {
        _streamState(XIM_XMPP_TIMEOUT);
    }
    XMPP_LOG(@"%@",sender)
}
- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust
 completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler {
    
    if (completionHandler)completionHandler(YES);
    
}
/**
 * This method is called after the stream is closed.
 *
 * The given error parameter will be non-nil if the error was due to something outside the general xmpp realm.
 * Some examples:
 * - The TCP socket was unexpectedly disconnected.
 * - The SRV resolution of the domain failed.
 * - Error parsing xml sent from server.
 *
 * @see xmppStreamConnectDidTimeout:
 **/
- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    
    XMPP_LOG(@"服务器断开连接 %@",error)
    connectCount ++;
    manager.state = XIM_XMPP_DISCONNECTED;
    if (_streamState) {
        _streamState(XIM_XMPP_DISCONNECTED);
    }
    [self showConnectedType];
    
}


- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkReachabilityFlags)reachabilityFlags {
    
    NSLog(@"%s",__FUNCTION__);
    return connectCount < 5 ?YES:NO;
    
}


- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkConnectionFlags)connectionFlags {
    
    NSLog(@"didDetectAccidentalDisconnect   =%u",connectionFlags);
}

static BOOL isConnecting;
static NSInteger connectCount = 0;
- (void) showConnectedType {
    
    if (connectCount == 5) {
        [_stream disconnect];
        [manager configServer];
        return;
    }
    
    if (!isConnecting && _stream.isConnected) {
        isConnecting = YES;
        connectCount ++;
        [_stream authenticateWithPassword:self.pwd error:nil];
    }
    
}


- (void)xmppAutoPingDidSendPing:(XMPPAutoPing *)sender {
        XMPP_LOG(@" xmppAutoPingDidSendPing  =%@",sender)
}
- (void)xmppAutoPingDidReceivePong:(XMPPAutoPing *)sender {
        XMPP_LOG(@" xmppAutoPingDidReceivePong  =%@",sender)
 
}

- (void)xmppAutoPingDidTimeout:(XMPPAutoPing *)sender {
    
    XMPP_LOG(@" 超时  =%@",sender.targetJID)
    [self showConnectedType];
}


+ (XMPPJID *) jidWithString:(NSString *)jid {
    
    NSString *jidString;
    if ([jid containsString:XMPPR_ROOM_Regulation]) {
        jidString = jid;
    }else{
        if ([jid containsString:XMMPP_BASESTR]) {
            return [XMPPJID jidWithString:[NSString stringWithFormat:@"%@",jid]];
        }
        jidString = [NSString stringWithFormat:@"%@%@",jid,XMMPP_BASESTR];
        
    }
    return [XMPPJID jidWithString: jidString];
    
}



+ (NSString *) getJidStringWithNnumber:(NSString *)number {
    
    if ([number containsString:XMMPP_BASESTR]) {
        return number;
    }
    return [NSString stringWithFormat:@"%@%@",number,XMMPP_BASESTR];
}


+(XMPPJID *) getJIDWithRoomName:(NSString *)name {
    NSString *roomString = [NSString stringWithFormat:@"%@%@",name,XMPPR_ROOM_Regulation];
    return [XMPPJID jidWithString:roomString];
}




@end

