//
//  XMPPMessageArchivingCoreDataStorage+XM_MessageArchiving.m
//  App3.0
//
//  Created by Sunny on 2018/6/1.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPMessageArchivingCoreDataStorage+XM_MessageArchiving.h"
#import "XMPPCoredataChatContact+CoreDataProperties.h"
#import "XMPPCoredataChatContact+CoreDataClass.h"

@interface XMPPMessageArchivingCoreDataStorage ()

@property (nonatomic,assign) NSInteger count;
@property (nonatomic,assign) NSInteger isBurn;
@property (nonatomic,copy) NSString *contactType;
@end

@implementation XMPPMessageArchivingCoreDataStorage (XM_MessageArchiving)

static const int message_xmpp;
static const int message_xmMessage;
static const NSString *message_contact;
#pragma clang diagnostic push

#pragma clang diagnostic ignored"-Wundeclared-selector"

+(void)load {
 
    
    [XMLHelp exchanegMethodClas:[self class] oldMetgod:@selector(willInsertMessage:) newMethod:@selector(xm_willInsertMessage:)];
    [XMLHelp exchanegMethodClas:[self class] oldMetgod:@selector(didUpdateMessage:) newMethod:@selector(xm_didUpdateMessage:)];
//    [XMLHelp exchanegMethodClas:[self class] oldMetgod:@selector(willDeleteMessage:) newMethod:@selector(xm_willDeleteMessage:)];
    
    [XMLHelp exchanegMethodClas:[self class] oldMetgod:@selector(willInsertContact:) newMethod:@selector(xm_willInsertContact:)];
    [XMLHelp exchanegMethodClas:[self class] oldMetgod:@selector(didUpdateContact:) newMethod:@selector(xm_didUpdateContact:)];
    
    
//    mainThreadManagedObjectContextDidMergeChanges
    
}

#pragma clang diagnostic pop

- (void)setContactType:(NSString *)contactType {
    objc_setAssociatedObject(self, &message_contact, contactType, OBJC_ASSOCIATION_COPY);

}

- (NSString *)contactType {
    return objc_getAssociatedObject(self, &message_contact);
}

- (void)setCount:(NSInteger)count{

    objc_setAssociatedObject(self, &message_xmpp, @(count), OBJC_ASSOCIATION_ASSIGN);
}

- (NSInteger)count {
    return [objc_getAssociatedObject(self, &message_xmpp) integerValue];
}


- (void)setIsBurn:(NSInteger)isBurn {
    objc_setAssociatedObject(self, &message_xmMessage, @(isBurn), OBJC_ASSOCIATION_ASSIGN);
 }

- (NSInteger)isBurn {
 
    return [objc_getAssociatedObject(self, &message_xmMessage) integerValue];
}

- (void)xm_willInsertMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message {
 
    [self imWillReceiveMessage:message];
 
}


- (void)xm_didUpdateMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message {
    
    [self imWillReceiveMessage:message];
    
}



- (void) imWillReceiveMessage:(XMPPMessageArchiving_Message_CoreDataObject *)message {
 
    self.count = 0;
    self.contactType = message.message.type;

    // 自己发的
    if (isEmptyString(message.message.fromStr) || [message.message.from.im_UserID isEqualToString:[UserInstance ShardInstnce].uid]) {
        self.count =0;
    }else{
        if (message.messageStatus.integerValue == 3) {
            self.count += 1;
        }
    }
}



- (void) xm_willInsertContact:(XMPPMessageArchiving_Contact_CoreDataObject *)contact {
    self.count = 0;
    [self saveXMChatContactWith:contact];
    
}




- (void)xm_didUpdateContact:(XMPPMessageArchiving_Contact_CoreDataObject *)contact {
    XMPP_LOG(@"@@@   %@",contact.messageCount);
    
     [self saveXMChatContactWith:contact];


}


- (void) saveXMChatContactWith:(XMPPMessageArchiving_Contact_CoreDataObject *)contact {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr ==  %@ AND bareJidStr == %@", contact.streamBareJidStr, contact.bareJidStr];
    
    
    XMPPCoredataChatContact *xm_contact  = [XMPPCoredataChatContact MR_findFirstWithPredicate:predicate inContext:localContext];
    
    if (xm_contact) {
        NSInteger newCount = xm_contact.messageCount.integerValue + self.count;
        NSInteger messageCount = self.count >= 0 ? newCount:0;
        xm_contact.messageCount = [NSString stringWithFormat:@"%ld",(long)messageCount];
        xm_contact.bareJid = contact.bareJid;
        xm_contact.bareJidStr = contact.bareJidStr;
        xm_contact.streamBareJidStr = contact.streamBareJidStr;
        xm_contact.mostRecentMessageTimestamp = contact.mostRecentMessageTimestamp;
        xm_contact.mostRecentMessageBody = contact.mostRecentMessageBody;
        ConversationModel *conversionModel = [[ConversationModel alloc] initWithContact:contact];
        conversionModel.lastestMessageStr = xm_contact.mostRecentMessageBody;
        NSString *string = conversionModel.mj_JSONString;
        xm_contact.contactString = string;
        xm_contact.contactType = self.contactType;
        [localContext MR_saveToPersistentStoreAndWait];
        
    }else{
        
        
        [self setXMCoreData:contact];
        
    }
    
}



- (void) setXMCoreData:(XMPPMessageArchiving_Contact_CoreDataObject *)contact {
 
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_context];
    XMPPCoredataChatContact *xm_contact = [XMPPCoredataChatContact MR_createEntityInContext:localContext];

    // 插入固定为1
    NSInteger messageCount = 1;
    
    xm_contact.messageCount = [NSString stringWithFormat:@"%ld",(long)messageCount];
    xm_contact.bareJid = contact.bareJid;
    xm_contact.bareJidStr = contact.bareJidStr;
    xm_contact.streamBareJidStr = contact.streamBareJidStr;
    xm_contact.mostRecentMessageBody = contact.mostRecentMessageBody;
    xm_contact.mostRecentMessageTimestamp = contact.mostRecentMessageTimestamp;
    ConversationModel *conversionModel = [[ConversationModel alloc] initWithContact:contact];
    conversionModel.lastestMessageStr = xm_contact.mostRecentMessageBody;
    NSString *string = conversionModel.mj_JSONString;
    xm_contact.contactString = string;
    xm_contact.contactType = self.contactType;
    [localContext MR_saveToPersistentStoreAndWait];
}

@end
