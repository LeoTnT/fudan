//
//  XMPPRoomMessageCoreDataStorageObject+RoomMessage.h
//  App3.0
//
//  Created by Sunny on 2018/5/25.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>


/**
 群聊  数据模型
 */
@interface XMPPRoomMessageCoreDataStorageObject (RoomMessage)

@property (nonatomic,strong) NSString *uumessageString;
@property (nonatomic,strong) UUMessage *uumessage;
- (UUMessage *) roomMessageWithBody;

/**
 群成员 确认 操作
 */
- (UUMessage *) updateRoomMessageWithBody:(NSDictionary *)dic ;
@end
