//
//  XMPPMessageArchiving_Contact_CoreDataObject+Contact.m
//  App3.0
//
//  Created by Sunny on 2018/4/18.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPMessageArchiving_Contact_CoreDataObject+Contact.h"

@implementation XMPPMessageArchiving_Contact_CoreDataObject (Contact)

@dynamic contactModel;
@dynamic contactString;
@dynamic messageCount;

+(void)load {
    
    Method  one = class_getInstanceMethod(self, @selector(willInsertObject));
    Method  two= class_getInstanceMethod(self, @selector(xm_willInsertObject));
    method_exchangeImplementations(one, two);
 
    
    Method m1 = class_getInstanceMethod(self, @selector(didUpdateObject));
    Method m2 = class_getInstanceMethod(self, @selector(xm_didUpdateObject));
    method_exchangeImplementations(m1, m2);
 
}



- (void)xm_didUpdateObject {
    
    [self willChangeValueForKey:@"contactString"];
    [self willChangeValueForKey:@"contactModel"];
    
    ConversationModel *contact = [[ConversationModel alloc] initWithContact:self];
    NSString *string = contact.mj_JSONString;
    self.contactString = string;
    
    
 
    [self didChangeValueForKey:@"contactString"];
    [self didChangeValueForKey:@"contactModel"];
    
}

- (void)xm_willInsertObject {
 
    [self willChangeValueForKey:@"contactString"];
    [self willChangeValueForKey:@"contactModel"];
    
    
    ConversationModel *contact = [[ConversationModel alloc] initWithContact:self];
    NSString *string = contact.mj_JSONString;
    self.contactString = string;

    
    [self didChangeValueForKey:@"contactString"];
    [self didChangeValueForKey:@"contactModel"];
    
}


 

- (ConversationModel *)contactModel {
    [self willAccessValueForKey:@"contactModel"];
    ConversationModel *contact = [ConversationModel mj_objectWithKeyValues:self.contactString];
    if (!contact) contact = [[ConversationModel alloc] initWithContact:self];
    contact.lastestDate = self.mostRecentMessageTimestamp;
    contact.chatType = [self.bareJidStr containsString:XMMPP_BASESTR] ? UUChatTypeChat:UUChatTypeChatRoom;
    contact.bareJID = self.bareJid;
    contact.messageCount = [self.messageCount integerValue];
    [self didAccessValueForKey:@"contactModel"];
    return contact ? contact:nil;
}
 
 
@end
