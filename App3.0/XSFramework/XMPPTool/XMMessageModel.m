//
//  XMMessageModel.m
//  App3.0
//
//  Created by Sunny on 2018/6/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMMessageModel.h"

@interface XMMessageModel()<NSFetchedResultsControllerDelegate>

@property (nonatomic ,strong ,readwrite)NSFetchedResultsController *resultController;
@property (nonatomic ,copy) NSString *streamID;
@property (nonatomic ,copy) NSString *conversationID;
@end

@implementation XMMessageModel

- (instancetype)initSearchText:(NSString *)string bareJidStr:(NSString *)bareJidStr {
    
    if (self == [super init]) {
        self.conversationID = getJID(bareJidStr);
        [self searchMessage:string];
     }
    return self;
}

-(NSString *)streamID {
    return [XMPPManager sharedManager].stream.myJID.bare;
}

- (void)searchMessage:(NSString *)string {
 
    
    
    self.searchTextSignal = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
         XMPPMessageArchivingCoreDataStorage *messageCDS = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *context = messageCDS.mainThreadManagedObjectContext;
 
        NSString *predicateFrmt = @"bareJidStr == %@ AND streamBareJidStr == %@ AND body CONTAINS %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,
                                  self.conversationID,self.streamID,
                                  string];
        
//        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = [messageCDS messageEntity:context];
        fetchRequest.predicate = predicate;
//        fetchRequest.sortDescriptors = @[sortDescriptor];
        NSError *error = nil;
        NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
        if (error) {
            [subscriber sendNext:error];
        }else{
            [subscriber sendNext:results];
        }
        [subscriber sendCompleted];
        return nil;
        
    }];
 
}

@end
