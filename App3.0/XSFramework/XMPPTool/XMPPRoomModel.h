//
//  XMPPRoomModel.h
//  App3.0
//
//  Created by Sunny on 2018/6/26.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 name    可选    string    群名称
 desc    可选    string    群描述
 type    可选    int    群类型    0-普通
 maxusers        string    群成员上限(200-2000)
 public        string    是否公开群
 allowinvites        string    是否允许成员邀请
 invite_need_confirm        string    入群是否需要审核
 id_str
 */
@interface RoomInfor :NSObject


@property (nonatomic ,copy)NSString *auth;
@property (nonatomic ,copy)NSString *avatar;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *desc;
@property (nonatomic ,copy)NSString *type;
@property (nonatomic ,copy)NSString *maxusers;
@property (nonatomic ,copy)NSString *publicID;
@property (nonatomic ,copy)NSString *allowinvites;
@property (nonatomic ,copy)NSString *invite_need_confirm;
@property (nonatomic ,copy)NSString *id_str;
@property (nonatomic ,copy)NSString *roomID;

@end


@interface XMPPRoomModel : NSObject



/**
 java服务器查询加入的群
 */
+ (void)queryRoomOfJoined;


/**
 获取群信息 请求完数据 发送刷新信号
 
 @param roomName roomID
 */
+ (void) getRoomInfor:(NSString *)roomID;


/**
 删除群配置
 
 @param roomName roomID
 */
+ (void) roomDestroy:(NSString *)roomName;


/**
 删除群历史记录
 */
+ (void) deletedRoomHistory:(NSString *)roomID;

/**
 xmpp 删除房间   不应该直接删除使用，先确定不在房间在执行删除。
 [ROOM leaveRoom];
 [ROOM deactivate];
 */
+ (void) xmpp_roomDeletedWithName:(NSString *)roomName;


/**
 查询加入的群
 */
+ (void)queryRoomForService:(void (^)(NSArray *))roomArr;

/**
 加入群 
 */
+ (void) joinRoomFormService:(NSArray *)roomArr;


/**
 查询是否加入群 查询加入群，没加入 删除会话及历史消息。
 请求
 */
+ (void) isJoinRoomWithID:(NSString *)roomID isFinash:(void(^)(BOOL))finash;



/**
 查询是否加入该群
 */
+ (void) checkIsJoinGroup:(NSString *)roomID finash:(void(^)(BOOL isJoin ,GroupDataModel *groupModel))finash ;

/**
 获取群信息

 @param roomID roomID
 @param title title description
 @param failure failure descriptio
 */
+ (void) getRoomInfor:(NSString *)roomID getSuccess:(void(^)(NSString *))title failure:(void(^)())failure;

@end
