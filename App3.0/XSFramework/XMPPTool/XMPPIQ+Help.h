//
//  XMPPIQ+Help.h
//  XMPPDemo
//
//  Created by apple on 2018/1/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>

@interface XMPPIQ (Help)

///是否是获取联系人的请求
-(BOOL)isRosterQuery;
///是否是房间列表请求
-(BOOL)isChatRoomItems;
///是否是房间信息查询
-(BOOL)isChatRoomInfo;

/**
 查询服务器所有群列表
 
 @return return room list
 */
+ (XMPPIQ *)queryRoomOfService;


/**
 查询所加入的群
 
 @return return room list
 */
+ (XMPPIQ *)queryRoomOfJoined;


/**
 获取群成员列表
 
 @param roomJID roomJID description
 @return return value description
 */
+ (XMPPIQ *) getMembersOfRoom:(NSString *)roomJID;

@end

@interface XMPPPresence (Help)
///是否来自聊天室的状态
-(BOOL)isChatRoomPresence;

@end


@interface XMPPMessage(Help)

@property (nonatomic ,copy ,readonly)NSString *xm_messageUser;
///是否是来自房间邀请
-(BOOL)isChatRoomInvite;

@end



@interface XMPPJID (Help)



/**
 xmpp 用户

 @param jidString jidString description
 @return return value description
 */
+ (XMPPJID *) getUserJIDWithString:(NSString *)jidString;



/**
 是不是自己发的消息
 */
@property (nonatomic ,copy ,readonly)NSString *xm_isFromID;

 /**
 是不是单聊
 */
@property (nonatomic ,assign ,readonly)BOOL xm_isChatSigal;


@property (nonatomic ,copy ,readonly)NSString *xm_roomUserID;

@property (nonatomic ,copy ,readonly) NSString *im_UserID;

/**
 房间 名
 */
//@property (nonatomic ,copy ,readonly)NSString *xm_roomName;


/**
 房间 ID
 */
//@property (nonatomic ,copy ,readonly)NSString *xm_roomIdentifier;


/**
  用户 ID
 */
@property (nonatomic ,copy ,readonly)NSString *xm_jidNumber;



/**
 群  单聊 唯一 ID
 */
//@property (nonatomic ,copy ,readonly)NSString *xmpp_ID;
@end
