//
//  XMPPCoredataChatContact+CoreDataProperties.h
//  
//
//  Created by Sunny on 2018/6/4.
//
//

#import "XMPPCoredataChatContact+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface XMPPCoredataChatContact (CoreDataProperties)

+ (NSFetchRequest<XMPPCoredataChatContact *> *)fetchRequest;

@property (nonatomic,strong) NSString *messageCount;
@property (nullable, nonatomic, strong) ConversationModel *contactModel;
@property (nullable, nonatomic, copy) NSString *contactString;
@property (nullable, nonatomic, copy) NSString *streamBareJidStr;
@property (nullable, nonatomic, copy) NSString *bareJidStr;
@property (nullable, nonatomic, strong) XMPPJID *bareJid;
@property (nullable, nonatomic, strong) NSDate *mostRecentMessageTimestamp;
@property (nonatomic,copy) NSString *isBurnMessage;
@property (nonatomic,copy) NSString *mostRecentMessageBody;
@property (nonatomic,copy) NSString *contactType;
 @end

NS_ASSUME_NONNULL_END
