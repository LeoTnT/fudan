//
//  XMPPSignal.h
//  App3.0
//
//  Created by apple on 2018/2/2.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConversationModel.h"
#import <Photos/PHAsset.h>
#import <Photos/Photos.h>
#import "XMPPCoredataChatContact+CoreDataClass.h"
typedef enum : NSUInteger {
    TranMessageShareDefault, // 分享文字
    TranMessageShareCustom, // 自定义分享
    TranMessageShareImage// 分享图片、二维码
} TranMessageType;

@interface XM_TransModel :NSObject


@property (nonatomic,assign) TranMessageType type;
@property (nonatomic,strong) NSData *imageData;

/* 单聊 分享扩展*/
@property (nonatomic,strong) NSDictionary *extDic;

/* 群分享商品XMPP_RoomMessage json字符串  单聊  messageText*/
@property (nonatomic,copy) NSString *messageText;
/* - - - */

/* 分享。XMPP_RoomMessage  json字符串 -> messageText 商品*/
- (instancetype)initDefaultShareText:(NSString *)messageText ext:(NSDictionary *)extDic;

/**
 分享图片
 
 @param imageData imageData description
 @return return value descriptio
 */
- (instancetype)initShareImage:(NSData *)imageData;



@end




@interface XMPP_BaseMessage :NSObject

@property (nonatomic ,copy) NSString *body;

@end

@interface XMPP_SiganlMessage :NSObject

@property (nonatomic ,strong) NSData *imageData;

@end
 
@interface XMPP_RoomMessage : NSObject

@property (nonatomic ,copy) NSString *body;
@property (nonatomic ,copy) NSString *type;

/*  - - 扩展 json - */
@property (nonatomic ,copy)NSString *ext;
- (BOOL) isRoomOwn;
@end

/* 声音时间 */
@interface XMPP_RoomVoiceMessage :XMPP_RoomMessage

@property (nonatomic ,copy) NSString *strVoiceTime;

@end

/*  - - 位置 - */
@interface XMPP_RoomLocationMessage :XMPP_RoomMessage

@property (nonatomic ,assign) CGFloat latitude;
@property (nonatomic ,assign) CGFloat longitude;

@end

/*  - - 文件 - */
@interface XMPP_RoomFileMessage :XMPP_RoomMessage

@property (nonatomic ,copy) NSString *displayName;
@property (nonatomic ,copy) NSString *fileSize;

@end

@interface XMPPSignal : NSObject


/**
 单聊 接收消息
 */
@property (nonatomic ,strong ,readonly)RACSubject *messageSubject;


+ (void) configServer;

+ (void) sendReceiveMessageID:(NSString *)jid;


+ (XMPPMessage *) messageBody:(NSString *)body to:(NSString *)jid;
/**
 发送消息
 
 @param text 文本
 @param jid jid
 */
+ (void) sendMessage:(NSString *)text to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

+ (void) sendMessage:(NSString *)text to:(NSString *)jid ext:(NSDictionary *)ext complete:(void(^)(XMPPMessageSendState))state;

+ (void) sendImage:(NSString *)text to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

 
+ (void) sendVoice:(NSString *)text time:(NSString *)time to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state ;

+ (void) sendWithVideo:(NSString *)text time:(NSString *)time to:(NSString *)jid
                 image:(NSString *)image  size:(NSString *)size
                  name:(NSString *)displayName
              complete:(void(^)(XMPPMessageSendState))state;



/**
 所谓的透传按消息
*/
+ (void) sendCMD:(NSString *)body to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

+ (void) sendLocation:(NSString *)address latitude:(NSString *)lat longitude:(NSString *)lon to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

+ (void) sendFileUrl:(NSString *)url displayName:(NSString *)displayName fileSize:(NSString *)size to:(NSString *)jid complete:(void(^)(XMPPMessageSendState))state;

+ (XMPPMessage *) messageWithVoice:(NSString *)text time:(NSString *)time to:(NSString *)jid ;

/**
 已读回执
 */
+ (void) generateReceiptResponse:(XMPPMessage *)message;



+ (XMPPMessage *) roomSendRedPacket:(NSString *)text  ext:(NSDictionary *)ext;


/**
 分享
 */
+ (void) shareXM_GroupModel:(XM_TransModel *)model contact:(ConversationModel *)contact finash:(void(^)(NSString *title))finash;
+ (void) shareXM_SignalModel:(XM_TransModel *)model contact:(ConversationModel *)contact finash:(void(^)(NSString *title))finash;

/**
 分享图片二维码
 
 @param data data description
 @param jid jid description
 @param finash finash description
 */
+ (void) signalShareImage:(NSData *)data to:(NSString *)jid success:(void(^)(NSString *message))finash;

 

/**
 群分享图片二维码
 
 @param data data description
 @param room room description
 @param finash finash description
 */
+ (void) groupShareImage:(NSData *)data room:(XMPPRoom *)room success:(void(^)(NSString *message))finash;

/**
 踢人

 @param user yonhu
 @param roomJID 房间ID
 */
+ (void) removeUsersWithUID:(NSString *)user withJID:(XMPPJID *)roomJID;
/**
 匹配返回 群会话列表

 @param model model description
 @return return value description
 */
+ (ConversationModel *) roomConfigWithConversation:(ConversationModel *)model;

/**
 根据房间名字返回

 @param roomName roomName description
 @return return value descriptio
 */
+ (XMPPRoomManager *) roomConfigWithRoomName:(NSString *)roomName;



/**
 更新聊天
 */
+ (void) updateMessageWithChat:(NSString *)messageId messageBody:(NSString *)messageBody;
/**
 删除单聊聊天记录

 @param userID userID description
 */
+ (void) deletedChatHistoryWithUserID:(NSString *)userID;

/**
 删除历史记录
 */
+ (void) deleteGroupHistoryWithRoomID:(NSString *)roomID;

+ (void) deletedContactListWith:(NSString *)ID;



/**
 更新消息 列表
 */
+ (void) im_ChangeMessage:(NSString *)messageID contact:(NSString *)contactID saveBlock:(void(^)(XMPPMessageArchiving_Message_CoreDataObject *messageObject))save  finash:(void(^)())finash;
/**
 更新会话对象
 */
+ (void) MR_changeIMMessageContact:(NSString *)contactID  saveBlock:(void(^)(XMPPCoredataChatContact *contactModel))save;
/**
 删除消息
 */
+ (void) xmpp_deletedMessageWith:(NSString *)messageID contactID:(NSString *)contactID ;

/**
 更新单聊消息列表

 @param contactID 好友ID 7@localhost.localdomain
 @param ID 消息ID。为空 消息设置为已读
 */
+ (void) updateXMPPMessageArchiving:(NSString *)contactID messageID:(NSString *)ID;


/**
 更新会话列表下表

 @param contactID 好友ID 7@localhost.localdomain
 */
+ (void) MR_updateXMPPMessageArchiving_Contact:(NSString *)contactID;


/**
 焚毁聊天 状态

 @param contactID contactID description
 */
+ (void) MR_updateXMPPMessageArchiving_ContactIsBurn:(NSString *)contactID ;

/**
 删除会话列表

 @param contactID contactID description
 */
+ (void) MR_deletedContactID:(NSString *)contactID ;


/**
 未读消息
 */
+ (void)setupUnreadMessageCount:(void(^)(NSInteger count))finash ;

/**
 更新群邀请信息
 
 @param roomName roomName description
 @param date date description
 @param cell cell description
 */
+ (void) groupUpMessageWith:(NSString *)roomName messageID:(NSString *)messageID finash:(void(^)(NSString *str))finash ;

+ (void) applicationDidEnterBackground;

+ (BOOL) isFileExist:(NSString *)fileName;

+ (BOOL) isCachContain:(NSString *)fileName;

+ (BOOL )deletedFileWith:(NSString *)fileName;

+ (BOOL)isValidWithString:(NSString *)string;
@end

