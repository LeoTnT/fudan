//
//  XMPPHeader.h
//  XMPPDemo
//
//  Created by apple on 2018/1/31.
//  Copyright © 2018年 apple. All rights reserved.
//

#ifndef XMPPHeader_h
#define XMPPHeader_h


#ifdef __OBJC__

#import <XMPPFramework/XMPPFramework.h>
#import "XMPPMessage+XM_Message.h"
#import "XMPPManager.h"


#import "XMPPMUCManager.h"

#import "XMPPIQ+Help.h"
#import "XMLHelp.h"

#import "XMPPFriendModel.h"
#import "XMPPRoomManager.h"
#import "XMPPAddFriendsManager.h"
#import "XMPPSignal.h"
#import "XMPPRoomModel.h"
#import "XMPPMessageArchiving_Message_CoreDataObject+Message.h"
#import "XMPPRoomMessageCoreDataStorageObject+RoomMessage.h"
#import "XMPPMessageArchiving_Contact_CoreDataObject+Contact.h"



#endif


//#ifdef DEBUG
//#define kHostName @"192.168.0.184"

//#ifdef DEBUG

/*
 IP
 内网 192.168.0.184
 外网 122.6.144.114
 
 图片上传
 内网  192.168.0.184:8088/assist/up/fileUpload
 外网  122.6.144.114:58088/assist/up/fileUpload
 
 */

//static NSString  * const kHostName = @"122.6.144.114";
//static NSString  * const XMUP_PATH = @"http://122.6.144.114:58088/assist/up/fileUpload";
//static NSString  * const XMUP_PATH_MULTFILEUPLOAD = @"http://122.6.144.114:58088/assist/up/multfileUpload";
 
//#else

//
//#endif

#define XMPP_HOST_KEY @"XMPP_HOST_KEY"

#define XMUP_PATH_KEY @"XMUP_PATH_KEY"

//#define kHostName [XSTool getStrUseKey:XMPP_HOST_KEY]
//#define XMUP_PATH [XSTool getStrUseKey:XMUP_PATH_KEY]


//#define XMMPP_BASESTR @"@localhost.localdomain"


#define XMMPP_BASESTR_CONFIG @"/TigaseMessenger"

// 单聊
#define XMPP_SIGNAL_Regulation [NSString stringWithFormat:@"%@%@",XMMPP_BASESTR,XMMPP_BASESTR_CONFIG]
// 群组
//#define XMPPR_ROOM_Regulation  @"@muc.localhost.localdomain"

//#define XMPPR_ROOM_Regulation  @"@muc.wang"


// 获取XMPPJID 对象
#define xmppJID(jid) [XMPPManager jidWithString:jid]

#define roomJID(jid) [XMPPManager getJIDWithRoomName:jid]

// 返回 用户JID  不包含/TigaseMessenger
#define getJID(jid) [XMPPManager getJidStringWithNnumber:jid]

#define getRoomString(roomName) [NSString stringWithFormat:@"%@%@",roomName,XMPPR_ROOM_Regulation]

#define XMPP_LOG(format,...)  NSLog((@" * * * * * * * * * * %s" format),__FUNCTION__, ## __VA_ARGS__);




// 聊天
/*
 消息的回撤消息两个字段：与安卓统一
 1.消息的动作，是透传消息必须有的字段
 public static final String REVOKE_ACTION = "REVOKE_ACTION";
 2.需要撤回消息的id
 public static final String CUSTOM_MSG_ID = "CUSTOM_MSG_ID";
 */
// 自定义聊天消息
//1.
#define MSG_TYPE @"MSG_TYPE"
//2.
#define CUSTOM_MSG_ID @"CUSTOM_MSG_ID"
//3.1 撤回消息
#define REVOKE_ACTION @"REVOKE_ACTION"
//3.2.1 创建群组
#define CREATE_GROUP @"CREATE_GROUP"
//3.2.2 邀请某一个人加入群组
#define GROUP_INVITE_ACTION @"GROUP_INVITE_ACTION"
//3.2.3 删除群组里面的某一个人
#define DELE_GROUP_MEMBER @"DELE_GROUP_MEMBER_ACTION"
//3.2.4 离开了某一个群组，在微信和QQ上似乎没有这个功能
#define LEVAVE_GROUP @"LEVAVE_GROUP_MEMBER_ACTION"
//3.2.5 群组名称发生改变 xx 把群组名称修改为
#define GROUP_NAME_CHANGE @"GROUP_NAME_CHANGE"
//3.2.6 群组邀请需要群主审核
#define GROUP_INVITE_NEED_CONFIRM @"GROUP_INVITE_NEED_CONFIRM"
//3.2.7 群组邀请需要群主审核 记录ID
#define GROUP_INVITE_ID @"GROUP_INVITE_ID"
//3.2.8 群组邀请需要群主审核 审核通过
#define GROUP_INVITE_CONFIRM @"GROUP_INVITE_CONFIRM"
//3.2.9 群组邀请需要群主审核 记录群主
#define GROUP_INVITE_OWNER @"GROUP_INVITE_OWNER"
//3.3.1 红包选项  发送红包
#define READ_PACKET @"READ_PACKET_ACTION"
//3.3.2 接收某一条红包 带CUSTOM_MSG_ID作为回执    XX接收了XX的红包   这个表示红包已经被领取了
//带CUSTOM_MSG_ID作为回执(todo: 这个地方似乎并没有带回去回执消息)
#define READ_PACKET_BACK @"READ_PACKET_BACK"
//3.3.3 红包已经被退还
#define READ_PACKET_BACK_REJCET @"READ_PACKET_BACK_REJCET"
//3.3.4 红包已经被服务器处理
#define READ_PACKET_HANDLE @"READ_PACKET_HANDLE"
//3.3.5 红包是谁发的
#define MESSAGE_PACK_FROM @"MESSAGE_PACK_FROM"

//3.4.1 转账
#define TRANSFER_BANLANCE @"TRANSFER_BANLANCE"
//3.4.2 转账金额标记
#define TRANSFER_BANLANCE_TAG @"TRANSFER_BANLANCE_TAG"
//3.4.3 接收某一条转账  带CUSTOM_MSG_ID作为回执 这个是转账接收了
#define TRANSFER_MONEY_COLLECTION @"TRANSFER_MONEY_COLLECTION"
//3.4.4 接拒绝接收这一条转账
#define TRANSFER_MONEY_COLLECTION_REJECT @"TRANSFER_MONEY_COLLECTION_REJECT"
//3.5.1 本地消息通知
#define MESSAGE_SAFE_NOTCIE @"MESSAGE_SAFE_NOTCIE"
//3.6.1 好友关系发生改变
#define MESSAGE_RELATION_CHANGE @"MESSAGE_RELATION_CHANGE"
//3.7.1 消息分享
#define MESSAGE_SHARE_TYPE @"MESSAGE_SHARE_TYPE"
#define MESSAGE_SHARE_DATA @"MESSAGE_SHARE_DATA"
//3.8.1 关注消息
#define MESSAGE_FOCUS_TYPE @"MESSAGE_FOCUS_TYPE"
//3.9.1 系统活动消息推送
#define MESSAGE_SYSTEM_PUSH @"MESSAGE_SYSTEM_PUSH"
//3.10.1 语音视频
#define MESSAGE_ATTR_IS_VOICE_CALL @"MESSAGE_ATTR_IS_VOICE_CALL"
#define MESSAGE_ATTR_IS_VIDEO_CALL @"MESSAGE_ATTR_IS_VIDEO_CALL"
//3.11.1 用户名片分享
#define MESSAGE_SHARE_USER      @"MESSAGE_SHARE_USER"
#define MESSAGE_SHARE_USER_ICO  @"MESSAGE_SHARE_USER_ICO"
#define MESSAGE_SHARE_USER_ID   @"MESSAGE_SHARE_USER_ID"
//3.12.1 商品信息展示
#define SHOW_PRODUCT_CONTENT @"SHOW_PRODUCT_CONTENT"    // 仅自己可见的商品消息类型
#define MESSAGE_PRODUCT_TYPE @"MESSAGE_PRODUCT_TYPE"




static NSString  * const  CHAT_DESTROY = @"CHAT_DESTROY";//请求焚毁
static NSString  * const  CHAT_DESTROY_AGREE = @"CHAT_DESTROY_AGREE"; //同意请求焚毁
static NSString  * const  CHAT_DESTROY_DISAGREE = @"CHAT_DESTROY_DISAGREE";//拒绝请求焚毁：


// 语音
static NSString  * const kRecordAudioFile = @"myRecord.m4a";
// 视频
//myMovie.mov
static NSString  * const XMPP_VIDEO_FILE = @"myMovie.mov";
// 语音 图片 文件标识符
static NSString  * const XMPP_MESSAGE_VOICT_IMAGE = @"jabber:x:oob";

/* - -- - - 发起语音 收费状态 -BEGAIN - - -  -*/

static NSString  * const VOICE_CHAT_REQUEST = @"VOICE_CHAT_REQUEST";
static NSString  * const VOICE_CHAT_AGREE = @"VOICE_CHAT_AGREE";
static NSString  * const VOICE_CHAT_DISAGREE = @"VOICE_CHAT_DISAGREE";
static NSString  * const VOICE_CHAT_LINE_BUSY = @"VOICE_CHAT_LINE_BUSY";

/* - - - - - - - */
static NSString  * const VOICE_CHAT_PAY = @"VOICE_CHAT_PAY";
static NSString  * const VOICE_CHAT_TIME = @"VOICE_CHAT_TIME";


/* —— —— —— —— —— —— —— 通知 link_app —— —— —— —— —— ———— —— —— —— —— ——*/
static NSString  * const IMMessageSystemLinkTypeAddGrMember = @"groupadd";  // 群审核申请
static NSString  * const IMMessageSystemLinkTypeAddGrMemberFinash = @"groupaddFinash";  // 群审核申请
static NSString  * const IMMessageSystemLinkTypeAddGrMemberFail = @"groupaudit";  // 群审核申请  状态
static NSString  * const IMMessageSystemLinkTypeOther = @"21";  // 群审核申请


/* —— —— —— —— —— —— —— —— —— —— —— —— ———— —— —— —— —— ——*/
//    VOICE_CHAT_PAY ： “2RMB”
//    VOICE_CHAT_TIME :     "2分钟"

//请求： VOICE_CHAT_REQUEST, CUSTOM_MSG_ID : orderid
//同意： VOICE_CHAT_AGREE, CUSTOM_MSG_ID : orderid
//不同意：VOICE_CHAT_DISAGREE, CUSTOM_MSG_ID : orderid
//不同意： 占线的情况吧 VOICE_CHAT_LINE_BUSY, CUSTOM_MSG_ID : orderid

//VOICE_CHAT_LINE_BUSY
/* - -- - - 发起语音 收费状态 END - - - -  -*/



//同意请求焚毁： "CHAT_DESTROY_AGREE";
//拒绝请求焚毁： "CHAT_DESTROY_DISAGREE";

// 录音
#define RECORD_SLIDUP_CANCEL    @"手指上滑 取消发送"
#define RECORD_TOUCHOUT_CANCEL    @"松开手指 取消发送"
#define RECORD_TIME_SHORT    @"时间过短"



#endif /* XMPPHeader_h */

