//
//  XMPPRoomCoreDataStorage+RoomMessage.m
//  App3.0
//
//  Created by Sunny on 2018/5/28.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "XMPPRoomCoreDataStorage+RoomMessage.h"

@implementation XMPPRoomCoreDataStorage (RoomMessage)

+(void)load {
    
    Method  one = class_getInstanceMethod(self, @selector(didInsertMessage:));
    Method  two= class_getInstanceMethod(self, @selector(xm_didInsertMessage:));
    method_exchangeImplementations(one, two);
 
}


- (void)xm_didInsertMessage:(XMPPRoomMessageCoreDataStorageObject *)message{
    if (isEmptyString(message.body)) return;
    [self willChangeValueForKey:@"uumessageString"];
 
    UUMessage * uumessagge = [message roomMessageWithBody];
    message.uumessageString = uumessagge.mj_JSONString;
    
    [self didChangeValueForKey:@"uumessageString"];
 
}

@end
