//
//  XSFormatterDate.h
//  App3.0
//
//  Created by nilin on 2017/3/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, XSFormatterDateStyle) {
    XSFormatterDateStyleDefault,//年月日时分秒
    XSFormatterDateStyleYMD,//年月日
    XSFormatterDateStyleHMS,//时分秒
    XSFormatterDateStyleDHMS,//日时分秒
};

@interface XSFormatterDate : NSObject


+ (NSString *)timeWithStyle:(XSFormatterDateStyle)timeStyle timeFormat:(NSString *)timeFormat intervalString:(NSString *)timeString;

/* 福旦时间格式 */
+ (NSString *)fdDateWithTimeIntervalString:(NSString *)timeString;

/**时间戳转为时间  精确到分*/
+(NSString *) dateWithTimeIntervalString:(NSString *)timeString;

/**时间戳转为时间  精确到日*/
+(NSString *) dateAccurateToDayWithTimeIntervalString:(NSString *)timeString;

// 转换为秒
+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;

// 当前时间
+ (NSString *)currentTime;

// 朋友圈评论时间
+ (NSString *)convertStringByData:(NSString *)timeString;

// 日期字符串转时间戳（2017-01-01）
+ (NSString *)timeSpWithDateString:(NSString *)dateString;
/**年月日*/
+(NSString *)yearDateWithTimeIntervalString:(NSString *)timeString;

// 秒数 转换 小时：分钟：秒
+ (NSString*)getOvertime:(int)sconed;

// 时间戳转换成 x天x时x分x秒
+ (NSString*)timeRemainingWithSeconds:(long)seconds;
@end
