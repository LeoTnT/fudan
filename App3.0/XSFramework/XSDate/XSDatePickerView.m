//
//  XSDatePickerView.m
//  App3.0
//
//  Created by xinshang on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSDatePickerView.h"

#define kScreen_Width [UIScreen mainScreen].bounds.size.width
#define kScreen_Height [UIScreen mainScreen].bounds.size.height

@interface XSDatePickerView () <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) NSArray *dateList;
@end

@implementation XSDatePickerView
- (void)showInView:(UIView *)view withFrame:(CGRect)frame {
    
//    NSArray *data = @[@"2017"];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];
    NSInteger currentYear=[[formatter stringFromDate:currentDate] integerValue];
    
    NSMutableArray *dateArr = [NSMutableArray array];
    for (int i = 0; i<10; i++) {
        if (2017+i > currentYear) {
            break;
        }
        NSString *dateStr = [NSString stringWithFormat:@"%d",2017+i];
        [dateArr addObject:dateStr];
    }
    [self showData:dateArr InView:view withFrame:frame withTitle:nil];
}

- (void)showData:(NSArray *)data InView:(UIView *)view withFrame:(CGRect)frame withTitle:(NSString *)title {
    self.frame = frame;
    self.backgroundColor = [UIColor whiteColor];

    self.dateList = [NSArray arrayWithArray:data];
    
    if(!self.pickerView){
        self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 38, frame.size.width, frame.size.height - 38)];
    }
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    [self addSubview:self.pickerView];
    
    
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
    [topView setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]initWithTitle:Localized(@"cancel_btn") style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:Localized(Localized(@"完成")) style:UIBarButtonItemStyleDone target:self action:@selector(pickDoneYear)];
    
    
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelButton,btnSpace,doneButton,nil];
    
    [topView setItems:buttonsArray];
    [self addSubview:topView];
    [view addSubview:self];
    
    UILabel *topViewTitle = [UILabel new];
    topViewTitle.text = title;
    topViewTitle.textAlignment = NSTextAlignmentCenter;
    topViewTitle.font =[UIFont systemFontOfSize:18];
    [topView addSubview:topViewTitle];
    [topViewTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(topView);
        make.centerY.mas_equalTo(topView).offset(2);
    }];
    
    
    self.frame = CGRectMake(0, kScreen_Height, kScreen_Width, 0);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = frame;
    }];
}

#pragma Mark -- UIPickerViewDataSource
// pickerView 列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// pickerView 每列个数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.dateList count];
}

#pragma Mark -- UIPickerViewDelegate
// 每列宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return mainWidth-40;
}
// 返回选中的行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

//返回当前行的内容,此处是将数组中数值添加到滚动的那个显示栏上
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.dateList objectAtIndex:row];
}

- (void)showInView:(UIView *)view withFrame:(CGRect)frame andDatePickerMode:(UIDatePickerMode)mode{
    self.frame = frame;
    self.backgroundColor = [UIColor whiteColor];
    
    if(!self.picker){
        self.picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 38, frame.size.width, frame.size.height - 38)];
    }
    
    self.picker.datePickerMode = mode;
    [self.picker addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.picker];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:10];//设置最大时间为：当前时间推后十年
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-10];//设置最小时间为：当前时间前推十年
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];

    [self.picker setMaximumDate:maxDate];
    [self.picker setMinimumDate:minDate];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy"];
//    NSDate *maxDate = [dateFormatter dateFromString:@"2017"];
//    NSDate *minDate = [dateFormatter dateFromString:@"2016"];
//    [self.picker setDate:[NSDate date] animated:YES];//设置默认日期
//    [self.picker setMaximumDate:maxDate];
//    [self.picker setMinimumDate:minDate];
    
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, mainWidth, 40)];
    [topView setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]initWithTitle:Localized(@"cancel_btn") style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:Localized(@"完成") style:UIBarButtonItemStyleDone target:self action:@selector(pickDone)];
    
    
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelButton,btnSpace,doneButton,nil];
    
    [topView setItems:buttonsArray];
    [self addSubview:topView];
    [view addSubview:self];

    self.frame = CGRectMake(0, kScreen_Height, kScreen_Width, 0);
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = frame;
    }];
}

- (void)pickDoneYear {
    if (![self.pickerView respondsToSelector:@selector(valueChanged:)]) {
        NSInteger index = [self.pickerView selectedRowInComponent:0];
        [self.delegate pickerView:self ValueChanged:self.dateList[index]];
    }
    [self dismiss];
}

- (void)pickDone{
    if (![self.picker respondsToSelector:@selector(valueChanged:)]) {
        [self.delegate picker:self ValueChanged:self.picker.date];
    }
    [self dismiss];
}

- (void)dismiss{
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, kScreen_Height, kScreen_Width, 0);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
- (void)valueChanged:(UIDatePicker *)picker{
    if([self.delegate respondsToSelector:@selector(picker:ValueChanged:)]){
        [self.delegate picker:self ValueChanged:picker.date];
    }
}

@end
