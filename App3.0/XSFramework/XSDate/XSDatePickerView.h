//
//  XSDatePickerView.h
//  App3.0
//
//  Created by xinshang on 2017/7/29.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@class XSDatePickerView;

@protocol XSDatePickerDelegate <NSObject>

@optional
//当UIDatePicker值变化时所用到的代理
- (void)picker:(XSDatePickerView *)picker ValueChanged:(NSDate *)date;

// 当value值变化时用到
- (void)pickerView:(XSDatePickerView *)picker ValueChanged:(NSString *)value;
@end

@interface XSDatePickerView : UIView

@property  (weak, nonatomic) id<XSDatePickerDelegate> delegate;

@property  (strong, nonatomic) UIDatePicker *picker;

@property  (strong, nonatomic) UIPickerView *pickerView;

// 兼容红包选择年份初始化方法（其他地方不要使用）
- (void)showInView:(UIView *)view withFrame:(CGRect)frame;

// 日期时间类型的初始化方法
- (void)showInView:(UIView *)view withFrame:(CGRect)frame andDatePickerMode:(UIDatePickerMode)mode;

// 一般的选择器使用下面初始化方法
- (void)showData:(NSArray *)data InView:(UIView *)view withFrame:(CGRect)frame withTitle:(NSString *)title;

- (void)dismiss;

- (void)valueChanged:(UIDatePicker *)picker;

@end


