//
//  XSFormatterDate.m
//  App3.0
//
//  Created by nilin on 2017/3/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSFormatterDate.h"

@implementation XSFormatterDate

#pragma mark - 时间戳转为时间  2016-03-20 08:12
+ (NSString *)timeWithStyle:(XSFormatterDateStyle)timeStyle timeFormat:(NSString *)timeFormat intervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    //    formatter.timeZone = [NSTimeZone timeZoneWithName:@"linyi"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSString * dateFormatString;
    if (timeFormat.length) {
        dateFormatString = timeFormat;
    } else {
        if (timeStyle == XSFormatterDateStyleDefault) {
            dateFormatString = @"yyyy-MM-dd HH:mm:ss";
        } else if (timeStyle == XSFormatterDateStyleYMD) {
            dateFormatString = @"yyyy-MM-dd";
        } else if (timeStyle == XSFormatterDateStyleHMS) {
            dateFormatString = @"HH:mm:ss";
        } else if (timeStyle == XSFormatterDateStyleDHMS) {
            dateFormatString = @"dd日HH:mm:ss";
        }
    }
    
    [formatter setDateFormat:dateFormatString];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString *)fdDateWithTimeIntervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    //    formatter.timeZone = [NSTimeZone timeZoneWithName:@"linyi"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    
    return dateString;
}

#pragma mark - 时间戳转为时间  2016-03-20 08:12
+ (NSString *) dateWithTimeIntervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    formatter.timeZone = [NSTimeZone timeZoneWithName:@"linyi"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    
    return dateString;
}

/**时间戳转为时间  精确到日*/
+(NSString *) dateAccurateToDayWithTimeIntervalString:(NSString *)timeString{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    //    formatter.timeZone = [NSTimeZone timeZoneWithName:@"linyi"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *timeStr = timeString;
    if (timeStr.length > 10) {
        timeStr = [timeStr substringToIndex:10];
    }
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeStr doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

+ (NSString *)currentTime
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate date];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

+ (NSString *)convertStringByData:(NSString *)timeString {
    NSString *covertString;
    NSString *timeStr = [NSString stringWithFormat:@"%f",[timeString doubleValue]*1000];
    NSString *subStart = [self timeWithTimeIntervalString:timeStr];
    NSDate *startDate = [NSDate dateFromString:subStart withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *subEnd = [self currentTime];
    NSDate *endDate = [NSDate dateFromString:subEnd withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    // NSCalendarUnit枚举代表想获得哪些差值
    NSCalendarUnit unit =NSCalendarUnitYear |NSCalendarUnitMonth |NSCalendarUnitDay |NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond;
    // 计算两个日期之间的差值
    NSDateComponents *cmps = [calendar components:unit fromDate:startDate toDate:endDate options:0];
    NSLog(@"%@---%@---%@----%@----%@",timeString,subStart,startDate,endDate,cmps);
    if (cmps.year < 1) {
        if (cmps.month < 1) {
            if (cmps.day < 1) {
                if (cmps.hour < 1) {
                    if (cmps.minute < 1) {
                        covertString = @"刚刚";
                    } else {
                        covertString = [NSString stringWithFormat:@"%ld分钟前",(long)cmps.minute];
                    }
                } else {
                    covertString = [NSString stringWithFormat:@"%ld小时前",(long)cmps.hour];
                }
            } else {
                covertString = [NSString stringWithFormat:@"%ld天前",(long)cmps.day];
            }
        } else {
            covertString = [NSString stringWithFormat:@"%ld个月前",(long)cmps.month];
        }
    } else {
        covertString = subStart;
    }
    
    return covertString;
}

+ (NSString *)timeSpWithDateString:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSDate *date = [formatter dateFromString:dateString];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    return timeSp;
}

+(NSString *)yearDateWithTimeIntervalString:(NSString *)timeString {
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    //    formatter.timeZone = [NSTimeZone timeZoneWithName:@"linyi"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy年MM月dd"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString*)getOvertime:(int)sconed
{
    if (sconed <= 0)
    {
        return @"00:00";
    }
    
    NSInteger h = sconed/60/60%24;
    NSInteger  m = sconed/60%60;
    NSInteger  s = sconed%60;
    NSString *timeStr = @"";
    
    if (h > 0)
    {
        timeStr = [NSString stringWithFormat:@"%lu:%lu:%lu",h,m,s];
    } else if (m == 0) {
        timeStr = [NSString stringWithFormat:@"00:%lu",s];
    } else {
        timeStr = [NSString stringWithFormat:@"%lu:%lu",m,s];
    }
    
    return timeStr;
}

+ (NSString *)timeRemainingWithSeconds:(long)seconds {
    long countDown = seconds;
    NSString *day = [NSString stringWithFormat:@"%02ld",countDown/(3600 *24)];
    NSInteger hours = ((countDown-[day integerValue]*24*3600)/3600);
    NSInteger minute = (int)(countDown-[day integerValue]*24*3600-hours*3600)/60;
    NSInteger second = countDown-[day integerValue]*24*3600-hours*3600-minute*60;
    NSString *str_hour = [NSString stringWithFormat:@"%02ld",hours];
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",minute];
    NSString *str_second = [NSString stringWithFormat:@"%02ld",second];
    NSString *format_time = [NSString stringWithFormat:@"%@天 %@时 %@分 %@秒",day,str_hour,str_minute,str_second];
    return format_time;
}
@end
