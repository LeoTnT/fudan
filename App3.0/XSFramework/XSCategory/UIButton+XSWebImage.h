//
//  UIButton+XSWebImage.h
//  App3.0
//
//  Created by mac on 2017/8/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (XSWebImage)

/**获取图片*/
-(void)getBackImageWithUrlStr:(NSString *)urlStr andDefaultImage:(UIImage *)defaultImg forState:(UIControlState)state;

@end
