//
//  XSCustomButton.m
//  App3.0
//
//  Created by mac on 17/3/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSCustomButton.h"

@interface XSCustomButton()
{
    
}
@end

@implementation XSCustomButton
- (XSCustomButton *)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor fontSize:(CGFloat)fontSize backgroundColor:(UIColor *)bgColor higTitleColor:(UIColor *)htColor highBackgroundColor:(UIColor *)hbColor
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitle:title forState:UIControlStateNormal];
        [self setBackgroundImage:[self imageWithColor:bgColor] forState:UIControlStateNormal];
        [self setBackgroundImage:[self imageWithColor:hbColor] forState:UIControlStateHighlighted];
        [self setTitleColor:titleColor forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        [self setTitleColor:htColor forState:UIControlStateHighlighted];
    }
    return self;
}
-(XSCustomButton *)initWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont *)font cornerRadius:(CGFloat)radius backGroundColor:(UIColor *)backColor hBackGroundColor:(UIColor *)hBackColor{
    if (self=[super init]) {
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:color forState:UIControlStateNormal];
        self.titleLabel.font=font;
        self.layer.cornerRadius=radius;
        self.layer.masksToBounds = YES;
        [self setBackgroundImage:[self imageWithColor:backColor] forState:UIControlStateNormal];
        [self setBackgroundImage:[self imageWithColor:hBackColor] forState:UIControlStateHighlighted];
    }
    return self;
}
- (void)setBorderWith:(CGFloat)bWidth borderColor:(CGColorRef)bColor cornerRadius:(CGFloat)cRadius
{
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = bWidth;
    self.layer.cornerRadius = cRadius;
    self.layer.borderColor = bColor;
}

- (void)setDisabledBackgroundColor:(UIColor *)bgColor titleColor:(UIColor *)tColor
{
    [self setBackgroundImage:[self imageWithColor:bgColor] forState:UIControlStateDisabled];
    [self setTitleColor:tColor forState:UIControlStateDisabled];
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
