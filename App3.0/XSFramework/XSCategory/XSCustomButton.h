//
//  XSCustomButton.h
//  App3.0
//
//  Created by mac on 17/3/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSCustomButton : UIButton
- (XSCustomButton *)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor fontSize:(CGFloat)fontSize backgroundColor:(UIColor *)bgColor higTitleColor:(UIColor *)htColor highBackgroundColor:(UIColor *)hbColor;

- (void)setBorderWith:(CGFloat)bWidth borderColor:(CGColorRef)bColor cornerRadius:(CGFloat)cRadius;
- (void)setDisabledBackgroundColor:(UIColor *)bgColor titleColor:(UIColor *)tColor;
-(XSCustomButton *)initWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont *)font cornerRadius:(CGFloat)radius backGroundColor:(UIColor *)backColor hBackGroundColor:(UIColor *)hBackColor;
@end
