//
//  UIImage+XSWebImage.m
//  App3.0
//
//  Created by nilin on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UIImage+XSWebImage.h"
#import "UIImageView+WebCache.h"
@implementation UIImageView (XSWebImage)

#pragma mark - 获取网络图片
-(void)getImageWithUrlStr:(NSString *) urlStr andDefaultImage:(UIImage *) defaultImg{
    NSString *trueString;
    if ([urlStr hasPrefix:@"http"]) {
        trueString = urlStr;
    } else {
        trueString = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,urlStr];
    }
    NSURL *url = [NSURL URLWithString:trueString];

    [self sd_setImageWithURL:url placeholderImage:defaultImg];
//    [self yy_setImageWithURL:url placeholder:defaultImg options:YYWebImageOptionProgressiveBlur|YYWebImageOptionSetImageWithFadeAnimation completion:nil];
 

}
@end
