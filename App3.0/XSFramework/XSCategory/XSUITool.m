//
//  XSUITool.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSUITool.h"
//#import <UITextView+Placeholder.h>

@implementation XSUITool

NSString * const buyOrSoldDataRefreshNotificationString = @"buyOrSoldDataRefreshNotificationString";
NSString * const cancellationsNotificationString = @"cancellationsNotificationString";

+ (UIView *)creatViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor {
    UIView * view = [[UIView alloc] initWithFrame:frame];
    if (backgroundColor) {
        view.backgroundColor = backgroundColor;
    }
    return view;
}

+ (void)setView:(UIView *)view cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
    if (view) {
        if (cornerRadius) {
            view.layer.cornerRadius = cornerRadius;
            view.layer.masksToBounds = YES;
        }
        if (borderWidth) {
            view.layer.borderWidth = borderWidth;
        }
        if (borderColor) {
            view.layer.borderColor = borderColor.CGColor;
        }
    }
}

+(UIButton *)creatButtonWithFrame:(CGRect)frame buttonType:(UIButtonType)buttonType Target:(id)target action:(SEL)action{
    UIButton * button;
    if (buttonType) {
        button = [UIButton buttonWithType:buttonType];
    } else {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    button.frame = frame;
    
    if (target && action) {
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    
    return button;
}

+(void)setButton:(UIButton *)button titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image backgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state title:(NSString *)title {
    if (button) {
        if (state) {
            if (title) {
                [button setTitle:title forState:state];
            }
            if (titleColor) {
                [button setTitleColor:titleColor forState:state];
            }
            if (image) {
                [button setImage:image forState:state];
            }
            if (backgroundImage) {
                [button setBackgroundImage:backgroundImage forState:state];
            }
            if (backgroundColor) {
                [button setBackgroundImage:[UIImage yy_imageWithColor:backgroundColor] forState:state];
            }
        } else {
            if (title) {
                [button setTitle:title forState:UIControlStateNormal];
            }
            if (titleColor) {
                [button setTitleColor:titleColor forState:UIControlStateNormal];
            }
            if (image) {
                [button setImage:image forState:UIControlStateNormal];
            }
            if (backgroundImage) {
                [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
            }
            if (backgroundColor) {
                [button setBackgroundImage:[UIImage yy_imageWithColor:backgroundColor] forState:UIControlStateNormal];
            }
        }
        
        if (titleFont) {
            button.titleLabel.font = [UIFont systemFontOfSize:titleFont];
        } else {
            button.titleLabel.font = [UIFont systemFontOfSize:14];
        }
//        if (backgroundColor) {
//            [button setBackgroundColor:backgroundColor];
//
//        }
    }
}

+(void)setButton:(UIButton *)button cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
    if (button) {
        if (cornerRadius) {
            button.layer.cornerRadius = cornerRadius;
            button.layer.masksToBounds = YES;
        }
        if (borderWidth) {
            button.layer.borderWidth = borderWidth;
        }
        if (borderColor) {
            button.layer.borderColor = borderColor.CGColor;
        }
    }
}

+(void)setButton:(UIButton *)button contentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment contentVerticalAlignment:(UIControlContentVerticalAlignment)contentVerticalAlignment imageEdgeInsets:(UIEdgeInsets)imageEdgeInsets titleEdgeInsets:(UIEdgeInsets)titleEdgeInsets {
    if (button) {
        if (contentHorizontalAlignment) {
            button.contentHorizontalAlignment = contentHorizontalAlignment;
        }
        if (contentVerticalAlignment) {
            button.contentVerticalAlignment = contentVerticalAlignment;
        }
        button.imageEdgeInsets = imageEdgeInsets;
        button.titleEdgeInsets = titleEdgeInsets;
    }
}

+(UIButton *)buttonWithFrame:(CGRect)frame buttonType:(UIButtonType)buttonType Target:(id)target action:(SEL)action titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image backgroundImage:(UIImage *)backgroundImage title:(NSString *)title {
    UIButton * button;
    if (buttonType) {
        button = [UIButton buttonWithType:buttonType];
    } else {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    button.frame = frame;
    
    if (title) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    if (titleColor) {
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    }
    if (titleFont) {
        button.titleLabel.font = [UIFont systemFontOfSize:titleFont];
    } else {
        button.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    if (backgroundColor) {
        [button setBackgroundColor:backgroundColor];
    }
    if (image) {
        [button setImage:image forState:UIControlStateNormal];
    }
    if (backgroundImage) {
        [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    }
    if (target && action) {
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }

    return button;
}


+ (UILabel *)creatLabelWithFrame:(CGRect)frame titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor textAlignment:(NSTextAlignment)textAlignment title:(NSString *)title {
    UILabel * label = [[UILabel alloc] init];
    
    label.frame = frame;
    
    if (titleColor) {
        label.textColor = titleColor;
    }
    if (titleFont) {
        label.font = [UIFont systemFontOfSize:titleFont];
    }
    if (backgroundColor) {
        label.backgroundColor = backgroundColor;
    }
    if (title) {
        label.text = title;
    }
    if (textAlignment) {
        label.textAlignment = textAlignment;
    } else {
        label.textAlignment = NSTextAlignmentLeft;
    }
    
    return label;
}

+ (UIImageView *)creatImageViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image {
    UIImageView * imageView = [[UIImageView alloc] init];
   
    imageView.frame = frame;
    if (backgroundColor) {
        imageView.backgroundColor = backgroundColor;
    }
    if (image) {
        imageView.image = image;
    }
    
    return imageView;
}

+ (void)setImageView:(UIImageView *)imageView cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
    if (imageView) {
        if (cornerRadius) {
            imageView.layer.cornerRadius = cornerRadius;
            imageView.layer.masksToBounds = YES;
        }
        if (borderWidth) {
            imageView.layer.borderWidth = borderWidth;
        }
        if (borderColor) {
            imageView.layer.borderColor = borderColor.CGColor;
        }
    }
}

+ (UITextField *)creatTextFieldWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor font:(CGFloat)textFont textAlignment:(NSTextAlignment)textAlignment delegate:(id<UITextFieldDelegate>)delegate placeholderColor:(UIColor *)placeholderColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder text:(NSString *)text {
    UITextField * textField = [[UITextField alloc] init];
    textField.frame = frame;
    
    if (placeholder) {
        textField.placeholder = placeholder;
    }
    if (backgroundColor) {
        textField.backgroundColor = backgroundColor;
    }
    if (placeholder && placeholderColor) {
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
        attrs[NSForegroundColorAttributeName] = placeholderColor;
        NSAttributedString *placeholder1 = [[NSAttributedString alloc] initWithString:placeholder attributes:attrs];
        textField.attributedPlaceholder = placeholder1;
    }
    
    if (textFont) {
        textField.font = [UIFont systemFontOfSize:textFont];
    }
    
    if (textColor) {
        textField.textColor = textColor;
    }
    
    if (textAlignment) {
        textField.textAlignment = textAlignment;
    }
    
    textField.delegate = delegate;
    
    if (text) {
        textField.text = text;
    }
    
    return textField;
}

+ (void)setTextField:(UITextField *)textField cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
    if (textField) {
        if (cornerRadius) {
            textField.layer.cornerRadius = cornerRadius;
            textField.layer.masksToBounds = YES;
        }
        if (borderWidth) {
            textField.layer.borderWidth = borderWidth;
        }
        if (borderColor) {
            textField.layer.borderColor = borderColor.CGColor;
        }
    }
}

+ (void)setTextField:(UITextField *)textField returnKeyType:(UIReturnKeyType)returnKeyType keyboardType:(UIKeyboardType)keyboardType secureTextEntry:(BOOL)secureTextEntry clearButtonMode:(UITextFieldViewMode)clearButtonMode {
    if (textField) {
        if (returnKeyType) {
            textField.returnKeyType = returnKeyType;
        }
        if (keyboardType) {
            textField.keyboardType = keyboardType;
        }
        if (secureTextEntry) {
            textField.secureTextEntry = secureTextEntry;
        }
        if (clearButtonMode) {
            textField.clearButtonMode = clearButtonMode;
        }
    }
}

//+ (UITextView *)creatTextViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor font:(CGFloat)textFont textAlignment:(NSTextAlignment)textAlignment delegate:(id<UITextViewDelegate>)delegate placeholderColor:(UIColor *)placeholderColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder text:(NSString *)text
//{
//    UITextView * textView = [[UITextView alloc] init];
//    textView.frame = frame;
//    
//    if (placeholder) {
//        textView.placeholder = placeholder;
//    }
//    if (backgroundColor) {
//        textView.backgroundColor = backgroundColor;
//    }
//    if (placeholderColor) {
//        textView.placeholderColor = placeholderColor;
//    }
//    
//    if (textFont) {
//        textView.font = [UIFont systemFontOfSize:textFont];
//    }
//    if (textColor) {
//        textView.textColor = textColor;
//    }
//    if (textAlignment) {
//        textView.textAlignment = textAlignment;
//    }
//    textView.delegate = delegate;
//    if (text) {
//        textView.text = text;
//    }
//    return textView;
//}


@end
