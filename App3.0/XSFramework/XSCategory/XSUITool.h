//
//  XSUITool.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSUITool : NSObject

//extern NSString * const buyOrSoldDataRefreshNotificationString;
//extern NSString * const cancellationsNotificationString;

/**
 简单创建UIView
 @param frame                frame
 @param backgroundColor      背景色
 */
+ (UIView *)creatViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor;

/**
 设置View圆角边界
 @param view               要设置的View
 @param cornerRadius         圆角半径
 @param borderWidth          边线宽度
 @param borderColor          边线颜色
 */
+ (void)setView:(UIView *)view cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

/**
 简单创建Button
 @param frame                frame
 @param buttonType           buttonType
 @param target               目标
 @param action               点击事件
 */
+ (UIButton *)creatButtonWithFrame:(CGRect)frame buttonType:(UIButtonType)buttonType Target:(id)target action:(SEL)action;

/**
 设置按钮文字、背景、图片
 @param button               要设置的按钮
 @param titleColor           按钮文字颜色
 @param backgroundColor      按钮背景色
 @param image                按钮图片
 @param backgroundImage      按钮背景图
 @param state                按钮状态
 @param title                按钮文字
 */
+ (void)setButton:(UIButton *)button titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image backgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state title:(NSString *)title;

/**
 设置按钮圆角边界
 @param button               要设置的按钮
 @param cornerRadius         圆角半径
 @param borderWidth          边线宽度
 @param borderColor          边线颜色
 */
+ (void)setButton:(UIButton *)button cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

/**
 设置按钮内容显示位置
 @param button                          要设置的按钮
 @param contentHorizontalAlignment      水平居左||中||右
 @param contentVerticalAlignment        垂直居上||下||填充
 @param imageEdgeInsets                 图片内边距
 @param titleEdgeInsets                 title内边距
 */
+ (void)setButton:(UIButton *)button contentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment contentVerticalAlignment:(UIControlContentVerticalAlignment)contentVerticalAlignment imageEdgeInsets:(UIEdgeInsets)imageEdgeInsets titleEdgeInsets:(UIEdgeInsets)titleEdgeInsets;

/**
 创建按钮
 @param frame                frame
 @param buttonType           buttonType
 @param target               target
 @param action               点击事件
 @param titleColor           按钮文字颜色
 @param backgroundColor      按钮背景色
 @param image                按钮图片
 @param backgroundImage      按钮背景图
 @param title                按钮文字
 */
+ (UIButton *)buttonWithFrame:(CGRect)frame buttonType:(UIButtonType)buttonType Target:(id)target action:(SEL)action titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image backgroundImage:(UIImage *)backgroundImage title:(NSString *)title;

/**
 创建UILabel
 @param frame                frame
 @param titleColor           label文字颜色
 @param titleFont            label文字字号
 @param textAlignment        label文字显示居中|居左|居右
 @param backgroundColor      label背景色
 @param title                label文字
 */
+ (UILabel *)creatLabelWithFrame:(CGRect)frame titleColor:(UIColor *)titleColor titleFont:(CGFloat)titleFont backgroundColor:(UIColor *)backgroundColor textAlignment:(NSTextAlignment)textAlignment title:(NSString *)title;

/**
 创建UILabel
 @param frame                frame
 @param backgroundColor      label背景色
 @param image                图片
 */
+ (UIImageView *)creatImageViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor image:(UIImage *)image;

/**
 设置imageView圆角边界
 @param imageView            要设置的图像
 @param cornerRadius         圆角半径
 @param borderWidth          边线宽度
 @param borderColor          边线颜色
 */
+ (void)setImageView:(UIImageView *)imageView cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;


/**
 创建textField
 @param frame                frame
 @param placeholder          默认文字
 @param backgroundColor      背景色
 @param placeholderColor     默认文字颜色
 @param textFont             输入文字颜色
 @param delegate             代理
 @param text                 输入文字
 */
+ (UITextField *)creatTextFieldWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor font:(CGFloat)textFont textAlignment:(NSTextAlignment)textAlignment delegate:(id<UITextFieldDelegate>)delegate placeholderColor:(UIColor *)placeholderColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder text:(NSString *)text;

/**
 设置textField样式
 @param textField            要设置的输入框
 @param returnKeyType        done按钮样式
 @param keyboardType         键盘样式
 @param secureTextEntry      是否是密码
 @param clearButtonMode      清除按钮模式
 */
+ (void)setTextField:(UITextField *)textField returnKeyType:(UIReturnKeyType)returnKeyType keyboardType:(UIKeyboardType)keyboardType secureTextEntry:(BOOL)secureTextEntry clearButtonMode:(UITextFieldViewMode)clearButtonMode;

/**
 设置textField圆角边界
 @param textField            要设置的输入框
 @param returnKeyType        done按钮样式
 @param keyboardType         键盘样式
 @param secureTextEntry      是否是密码
 @param clearButtonMode      清除按钮模式
 */
+ (void)setTextField:(UITextField *)textField cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

/**
 创建textView
 @param frame                frame
 @param placeholder          默认文字
 @param backgroundColor      背景色
 @param placeholderColor     默认文字颜色
 @param textFont             输入文字颜色
 @param delegate             代理
 @param text                 输入文字
 */
//+ (UITextView *)creatTextViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor font:(CGFloat)textFont textAlignment:(NSTextAlignment)textAlignment delegate:(id<UITextViewDelegate>)delegate placeholderColor:(UIColor *)placeholderColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder text:(NSString *)text;

@end
