//
//  UIBarButtonItem+XSYExtension.h
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, XSYBarButtonItemType) {
    XSYBarButtonItemTypeDefult,//文字在右，图标在左
    XSYBarButtonItemTypeImageInRight,//文字在左，图标在右
    XSYBarButtonItemTypeImageAbove//文字在下，图标在上
};


@interface UIBarButtonItem (XSYExtension)

/**
 * 仅有图标的按钮
 */
+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action;

/**
 * 仅有文字
 */
+(instancetype)itemWithTitleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont target:(id)target action:(SEL)action titleString:(NSString *)title;

+ (instancetype)itemWithType:(XSYBarButtonItemType)itemType imageName:(NSString *)image highImageName:(NSString *)highImage titleColor:(UIColor *)titleColor target:(id)target action:(SEL)action titleString:(NSString *)title;


@end

@interface XSYBarButton : UIButton

@end
