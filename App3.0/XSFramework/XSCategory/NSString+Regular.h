//
//  NSString+Regular.h
//  App3.0
//
//  Created by mac on 2017/7/4.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Regular)
- (NSString *)urlSubString;
-(BOOL)stringContainsEmoji:(NSString *)string;
@end
