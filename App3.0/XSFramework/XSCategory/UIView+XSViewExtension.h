//
//  UIView+XSViewExtension.h
//  App3.0
//
//  Created by sunzhenkun on 2017/12/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (XSViewExtension)

/** size */
@property (nonatomic, assign) CGSize xs_size;
/** x */
@property (nonatomic, assign) CGFloat xs_x;
/** y */
@property (nonatomic, assign) CGFloat xs_y;
/** width */
@property (nonatomic, assign) CGFloat xs_width;
/** height */
@property (nonatomic, assign) CGFloat xs_height;
/** centerX */
@property (nonatomic, assign) CGFloat xs_centerX;
/** centerY */
@property (nonatomic, assign) CGFloat xs_centerY;
/** right */
@property (nonatomic, assign) CGFloat xs_right;
/** bottom */
@property (nonatomic, assign) CGFloat xs_bottom;
/** point */
@property (nonatomic, assign) CGPoint xs_origin;
/** 圆角 */
@property (nonatomic, assign) CGFloat xs_cornerRadius;

- (BOOL)intersectWithView:(UIView *)view;

@end
