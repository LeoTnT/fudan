//
//  UIView+XSViewExtension.m
//  App3.0
//
//  Created by sunzhenkun on 2017/12/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UIView+XSViewExtension.h"

@implementation UIView (XSViewExtension)

-(void)setXs_size:(CGSize)xs_size {
    CGRect frame = self.frame;
    frame.size = xs_size;
    self.frame = frame;
}
-(CGSize)xs_size {
    return self.frame.size;
}

-(void)setXs_x:(CGFloat)xs_x {
    CGRect frame = self.frame;
    frame.origin.x = xs_x;
    self.frame = frame;
}
-(CGFloat)xs_x {
    return self.frame.origin.x;
}

-(void)setXs_y:(CGFloat)xs_y {
    CGRect frame = self.frame;
    frame.origin.y = xs_y;
    self.frame = frame;
}
-(CGFloat)xs_y {
    return self.frame.origin.y;
}

-(void)setXs_width:(CGFloat)xs_width {
    CGRect frame = self.frame;
    frame.size.width = xs_width;
    self.frame = frame;
}
-(CGFloat)xs_width {
    return self.frame.size.width;
}

-(void)setXs_height:(CGFloat)xs_height {
    CGRect frame = self.frame;
    frame.size.height = xs_height;
    self.frame = frame;
}
-(CGFloat)xs_height {
    return self.frame.size.height;
}

-(void)setXs_centerX:(CGFloat)xs_centerX {
    CGPoint center = self.center;
    center.x = xs_centerX;
    self.center = center;
}
-(CGFloat)xs_centerX {
    return self.center.x;
}

-(void)setXs_centerY:(CGFloat)xs_centerY {
    CGPoint center = self.center;
    center.y = xs_centerY;
    self.center = center;
}
-(CGFloat)xs_centerY {
    return self.center.y;
}

-(void)setXs_right:(CGFloat)xs_right {
    self.xs_x = xs_right - self.xs_width;
}
-(CGFloat)xs_right {
    return CGRectGetMaxX(self.frame);
}

-(void)setXs_bottom:(CGFloat)xs_bottom {
    self.xs_y = xs_bottom - self.xs_height;
}
-(CGFloat)xs_bottom {
    return CGRectGetMaxY(self.frame);
}

-(void)setXs_origin:(CGPoint)xs_origin {
    CGRect frame = self.frame;
    frame.origin = xs_origin;
    self.frame = frame;
}
-(CGPoint)xs_origin {
    return self.frame.origin;
}

-(void)setXs_cornerRadius:(CGFloat)xs_cornerRadius {
    self.layer.cornerRadius = xs_cornerRadius;
    self.layer.masksToBounds = YES;
}
-(CGFloat)xs_cornerRadius {
    return self.layer.cornerRadius;
}


//判断View是否显示在当前页面
- (BOOL)intersectWithView:(UIView *)view
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    CGRect selfRect = [self convertRect:self.bounds toView:window];
    CGRect viewRect = [view convertRect:view.bounds toView:window];
    return CGRectIntersectsRect(selfRect, viewRect);
}

@end
