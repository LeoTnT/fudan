//
//  UIImage+XSWebImage.h
//  App3.0
//
//  Created by nilin on 2017/3/20.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (XSWebImage)

/**获取图片*/
-(void)getImageWithUrlStr:(NSString *)urlStr andDefaultImage:(UIImage *) defaultImg;
@end
