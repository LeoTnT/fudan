//
//  PhotoBrowserImage.h
//  App3.0
//
//  Created by mac on 2017/7/27.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoBrowserImage : UIImage
@property (nonatomic, strong) NSString *imageId;
@property (nonatomic) long long imageTime;
@end
