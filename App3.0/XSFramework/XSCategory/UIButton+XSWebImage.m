//
//  UIButton+XSWebImage.m
//  App3.0
//
//  Created by mac on 2017/8/25.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UIButton+XSWebImage.h"
@implementation UIButton (XSWebImage)

- (void)getBackImageWithUrlStr:(NSString *)urlStr andDefaultImage:(UIImage *)defaultImg forState:(UIControlState)state {
    NSString *trueString;
    if ([urlStr hasPrefix:@"http"]) {
        trueString = urlStr;
    } else {
        trueString = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,urlStr];
    }
    NSURL *url = [NSURL URLWithString:trueString];
    
    [self sd_setBackgroundImageWithURL:url forState:state placeholderImage:defaultImg];
}
@end
