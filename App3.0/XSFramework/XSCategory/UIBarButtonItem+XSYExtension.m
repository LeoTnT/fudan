//
//  UIBarButtonItem+XSYExtension.m
//  App3.0
//
//  Created by sunzhenkun on 2018/5/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "UIBarButtonItem+XSYExtension.h"

@implementation UIBarButtonItem (XSYExtension)

+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, 25*SCREEN_SCALE, 25*SCREEN_SCALE);
    [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [button setImage:[[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    [button setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[self alloc] initWithCustomView:button];
}

+(instancetype)itemWithTitleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont target:(id)target action:(SEL)action titleString:(NSString *)title {
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    NSUInteger buttonWidth;
    if (title.length < 2) {
        buttonWidth = 45;
    }else if (title.length >= 2) {
        buttonWidth = 60;
    }else if (title.length >= 3) {
        buttonWidth = 70;
    } else {
        buttonWidth = 75;
    }
    
    button.frame = CGRectMake(0, 0, FontNum(buttonWidth), FontNum(25));
    if (titleColor) {
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    } else {
        [button setTitleColor:XSYCOLOR(0x333333) forState:UIControlStateNormal];
    }
    if (titleFont) {
        button.titleLabel.font = titleFont;
    } else {
        button.titleLabel.font = [UIFont systemFontOfSize:FontNum(14)];
    }
    if (title) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.8;
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[self alloc] initWithCustomView:button];
}


+(instancetype)itemWithType:(XSYBarButtonItemType)itemType imageName:(NSString *)image highImageName:(NSString *)highImage titleColor:(UIColor *)titleColor target:(id)target action:(SEL)action titleString:(NSString *)title {
    
    UIButton * barBtn;
    if (itemType == XSYBarButtonItemTypeImageAbove) {
        barBtn = [XSYBarButton buttonWithType:UIButtonTypeCustom];
        barBtn.frame = CGRectMake(0, 0, FontNum(32), FontNum(32));
        barBtn.titleLabel.font = [UIFont systemFontOfSize:FontNum(9)];
        barBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, -15);
    } else {
        barBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        barBtn.frame = CGRectMake(0, 0, FontNum(50), FontNum(30));
        if (itemType == XSYBarButtonItemTypeDefult) {
            barBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            // 让按钮的内容往左边偏移0
            barBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        } else {
            barBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            // 让按钮的内容往右边偏移0
            barBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        barBtn.titleLabel.font = [UIFont systemFontOfSize:FontNum(14)];
    }
    
    if (image) {
        [barBtn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
        //        [barBtn setImage:[[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    }
    if (highImage) {
        [barBtn setImage:[UIImage imageNamed:image] forState:UIControlStateHighlighted];
        //        [barBtn setImage:[[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateHighlighted];
    }
    if (title) {
        [barBtn setTitle:title forState:UIControlStateNormal];
    }
    if (titleColor) {
        [barBtn setTitleColor:titleColor forState:UIControlStateNormal];
    } else {
        [barBtn setTitleColor:COLOR_666666 forState:UIControlStateNormal];
    }
    
    if (target && action) {
        [barBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    
    return [[self alloc] initWithCustomView:barBtn];
}

@end

@implementation XSYBarButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.xs_centerY = self.xs_height/3;
    self.imageView.xs_centerX = self.xs_width / 2;
    self.titleLabel.xs_x = 0;
    self.titleLabel.xs_y = self.imageView.xs_bottom;
    self.titleLabel.xs_height = self.xs_height-self.titleLabel.xs_y;
    self.titleLabel.xs_width = self.xs_width;
    XSLog(@"%f",self.frame.size.width);
}

@end

