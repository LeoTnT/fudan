//
//  UIAlertView+XSAlertView.h
//  App3.0
//
//  Created by apple on 2017/6/26.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (XSAlertView)


/**
 alertView

 @param title title description
 @param message message description
 @param cancelButtonTitle cancelButtonTitle description
 @param titles titles description
 @param completionBlock completionBlock description
 @param cancelBlock cancelBlock description
 @return return value description
 */
+ (id)alertViewWithTitle:(NSString *)title
                 message:(NSString *)message
       cancelButtonTitle:(NSString *)cancelButtonTitle
       otherButtonTitles:(NSArray*) titles
         completionBlock:(void (^)(UIAlertView* alertView, NSInteger selectedButtonIndex)) completionBlock
             cancelBlock:(void (^)()) cancelBlock;

@end
