//
//  XSAlertView.h
//  App3.0
//
//  Created by mac on 17/5/3.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSAlertView : UIView
- (instancetype)initWithTitle:(NSString *)title detail:(NSString *)detail;
@end
