//
//  XSToastManager.m
//  App3.0
//
//  Created by mac on 17/2/23.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSToastManager.h"
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>

#define minshowtime   0.5
@interface XSToastManager () {
    MBProgressHUD *toastHud;
    MBProgressHUD *progressHud;
}
@end

static XSToastManager *toastManager;
@implementation XSToastManager

#pragma mark -单例manager
+ (XSToastManager *)ShardInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        toastManager = [[XSToastManager alloc] init];
    });
    return toastManager;
}

#pragma mark -keyWindow
+ (UIWindow *)keyWindow {
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow *keywindow = delegate.window;
    return keywindow;
}

- (id)init {
    self = [super init];
    if (self) {
        UIWindow *keywindow = [XSToastManager keyWindow];
        toastHud = [[MBProgressHUD alloc] initWithView:keywindow];
        toastHud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        toastHud.minSize = CGSizeMake(220, 60);
        toastHud.userInteractionEnabled = NO;
        toastHud.mode = MBProgressHUDModeText;
        toastHud.minShowTime = minshowtime*2;
        toastHud.label.textColor = [UIColor whiteColor];
        toastHud.detailsLabel.textColor = [UIColor whiteColor];
        toastHud.bezelView.color = [UIColor colorWithWhite:0.f alpha:0.7];
//        toastHud.alpha = 0.3;
        [keywindow addSubview:toastHud];
        progressHud = [[MBProgressHUD alloc] initWithView:keywindow];
        progressHud.animationType = MBProgressHUDAnimationFade;
        //progressHud.mode = MBProgressHUDModeCustomView;
        progressHud.userInteractionEnabled = YES;
        progressHud.minShowTime = minshowtime;
        progressHud.label.text = @"加载中...";
        progressHud.square = YES;
        [keywindow addSubview:progressHud];
    }
    return self;
}

#pragma mark -toast
- (void)showtoast:(NSString *)toastStr {
    if (toastStr.length > 0) {
        if (toastStr.length > 15) {
            toastHud.label.text = @"";
            toastHud.detailsLabel.text = toastStr;
        } else {
            toastHud.label.text = toastStr;
            toastHud.detailsLabel.text = @"";
        }
        [[XSToastManager keyWindow] bringSubviewToFront:toastHud];
        [toastHud showAnimated:YES];
        [toastHud hideAnimated:YES];
    }
}

- (void)showtoast:(NSString *)toastStr wait:(double)wait
{
    toastHud.minShowTime = wait;
    [self showtoast:toastStr];
    toastHud.minShowTime = minshowtime * 2;
}

#pragma mark -progress
- (void)hideprogress {
    [progressHud hideAnimated:YES];
    [toastHud hideAnimated:YES];
}

- (void)showprogress {
    [toastHud hideAnimated:YES];
    [[XSToastManager keyWindow] bringSubviewToFront:progressHud];
    [progressHud setMinShowTime:0];
    progressHud.label.text  = @"";
    [progressHud showAnimated:NO];
}

- (void)showprogressWithTitle:(NSString *) title{
    [toastHud hideAnimated:YES];
    [[XSToastManager keyWindow] bringSubviewToFront:progressHud];
    [progressHud setMinShowTime:0];
    progressHud.label.text  = title;
    [progressHud showAnimated:NO];

}
#pragma mark - 显示提示框
- (void)showHUDAddToView:(UIView *)view withText:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.mode = MBProgressHUDModeAnnularDeterminate;
    if (text == nil) {
        hud.label.text = @"";
    } else {
        hud.detailsLabel.text = text; 
    }
}

#pragma mark -  隐藏提示框
- (void)hideHUDForView:(UIView *)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
    
}

@end
