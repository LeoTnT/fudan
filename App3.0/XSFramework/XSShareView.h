//
//  XSShareView.h
//  App3.0
//
//  Created by mac on 17/5/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ShareType) {
    ShareTypeDefault = 0, //默认店铺
    ShareTypeProduct,// 默认商品
    ShareTypeVideo,//视频分享
    ShareTypeAD,//广告分享
    ShareTypeUser,//用户分享
    ShareTypeApp,//客户端分享
};

@interface XSShareModel : NSObject
@property (nonatomic,copy)NSString* link;
@property (nonatomic,copy)NSString* qrcode;
@property (nonatomic,copy)NSString* logo;
@property (nonatomic,copy)NSString* title;
@property (nonatomic,copy)NSString* content;
@property (nonatomic ,strong)NSData *imageData;
@end

@interface XSShareView : UIView
+(instancetype)creatShareView:(NSString *)ID viewController:(UIViewController *)vc type:(ShareType )type;
@end

@interface XSShareBaseView : UIView
@property (nonatomic ,copy)void (^touchOutSide)();
@end

