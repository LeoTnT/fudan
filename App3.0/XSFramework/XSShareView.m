//
//  XSShareView.m
//  App3.0
//
//  Created by mac on 17/5/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSShareView.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "XSShowQRCodeController.h"
#import "TranspondListViewController.h"
#import "AddStatusViewController.h"
#import "UUMessage.h"
#import "FansCircleModel.h"

@implementation XSShareModel

@end

@interface XSShareView () <WXApiDelegate, QQApiInterfaceDelegate>
{
    ShareType _shareType;
    NSString *_shareId;
}
@property (nonatomic, copy) NSString *shareTitle;
@property (nonatomic, copy) NSString *shareContent;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) XSShareBaseView *parentView;
@property (nonatomic, strong) XSShareModel *shareModel;
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic ,assign)CGFloat shareHight;
@property(nonatomic,strong) AddStatusViewController *addVC;
@property(nonatomic,strong)FanCircleShare *share;
@property (nonatomic ,assign) BOOL isShareCircle;
@end

@implementation XSShareView



/**
 高度

 @return 分享界面高度  320
 */
-(CGFloat)shareHight{
    return 260;
}

+(instancetype)creatShareView:(NSString *)ID viewController:(UIViewController *)vc type:(ShareType )type{
    return [[self alloc]initWithID:ID viewController:vc type:type];
}

-(instancetype)initWithID:(NSString *)ID viewController:(UIViewController *)vc type:(ShareType )type{
    if (self = [super init]) {
        _viewController = vc;
        if ([ID isKindOfClass:[NSNumber class]]) {
            ID = [NSString stringWithFormat:@"%@",ID];
        }
        _shareId = ID;
        _shareType = type;
        if (ID.length >0 && ID) {
            [self creatContentView];
            [self createBgView];
            [self shareInformation:ID type:type];
        }
        
    }
    return self;
}

//  头像 内容 链接 二维码
- (void)shareInformation:(NSString *)ID type:(ShareType )type{
    
    
    
    NSString *QRtype;
    NSDictionary *params;
    if (type == ShareTypeApp) {
        params = @{@"type":@"app",
                   @"target":@"ios"};
    }else{
        
        switch (type) {
            case ShareTypeDefault: QRtype = @"supply";break;
            case ShareTypeProduct:QRtype = @"product";break;
            case ShareTypeVideo:QRtype = @"video";break;
            case ShareTypeAD:QRtype = @"ad";break;
            case ShareTypeUser:QRtype = @"user";break;
            case ShareTypeApp:break;
        }
        
        params = @{
                   @"type":QRtype,
                   @"target":ID
                   };
    }
    

  
    
    
    [XSTool showProgressHUDWithView:self.parentView];
    [HTTPManager getShareInfo:params Succrss:^(NSDictionary *dic, resultObject *state) {
        [XSTool hideProgressHUDWithView:self.parentView];
        self.shareModel = [XSShareModel mj_objectWithKeyValues:dic[@"data"]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *logoUrl = self.shareModel.logo;
            if (![logoUrl hasPrefix:@"http"]) {
                logoUrl = [NSString stringWithFormat:@"%@%@",ImageBaseUrl,self.shareModel.logo];
            }
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:logoUrl]];
            if (!data) {
                data = UIImagePNGRepresentation([UIImage imageNamed:@"fuDan_logo"]);
            }
            self.shareModel.imageData = data;
        });
        self.share=[FanCircleShare mj_objectWithKeyValues:@{@"image_url":self.shareModel.logo?self.shareModel.logo:@"",@"head":self.shareModel.title?self.shareModel.title:@"",@"url":self.shareModel.link?self.shareModel.link:@""}];
        if (self.isShareCircle == YES) {
            [self pushAddStatusVC];
        }
    } failure:^(NSError * _Nonnull error) {
    }];
}

- (void)createBgView {
    self.window = [UIApplication sharedApplication].keyWindow;
    if (_parentView) {
        [_parentView removeFromSuperview];
        _parentView = nil;
    }
    self.parentView = [XSShareBaseView new];
    self.parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3];
    self.parentView.frame = self.window.bounds;
    [self.window addSubview:self.parentView];
    [self.parentView addSubview:self];
    
    self.frame = CGRectMake(0, mainHeight, mainWidth, 0);
    [UIView animateWithDuration:.25 animations:^{
        self.frame = CGRectMake(0, mainHeight-self.shareHight, mainWidth, self.shareHight);
    }];
    
    @weakify(self);
    [self.parentView setTouchOutSide:^{
        @strongify(self);
        [self dismiss];
    }];
}

- (void)creatContentView {
    CGFloat topSpace = 15;
    CGFloat btnHeight = 62;
    self.backgroundColor = [UIColor whiteColor];
    
    
    
    UIButton *qq = [self creatButtonWithImage:@"share_qq" sel:@selector(qqAction)];
    UILabel *qqLabel = [self creatLabelWithTitle:Localized(@"QQ好友")];
    
    
    UIButton *myFriends = [self creatButtonWithImage:@"share_myFriends" sel:@selector(myFriendsAction)];
    UILabel *myFriendsLb = [self creatLabelWithTitle:@"聊天好友"];
    
    UIButton *qqQzone = [self creatButtonWithImage:@"share_qzone" sel:@selector(qqQzoneAction)];
    UILabel *qzoneLabel = [self creatLabelWithTitle:@"QQ空间"];
    
    UIButton *weixin = [self creatButtonWithImage:@"share_weixin" sel:@selector(weixinAction)];
    UILabel *weixinLabel = [self creatLabelWithTitle:Localized(@"微信好友")];
    
    UIButton *myFriendcicles = [self creatButtonWithImage:@"share_myFriendcircles" sel:@selector(myFriendCiclesAction)];
    UILabel *myFriendciclesLb = [self creatLabelWithTitle:@"粉丝圈"];
    
    UIButton *friend = [self creatButtonWithImage:@"share_weixin_friends" sel:@selector(friendAction)];
    UILabel *friendLabel = [self creatLabelWithTitle:@"微信朋友圈"];
    
    CGSize buttonSize = CGSizeMake(btnHeight, btnHeight);
    CGSize labelSize = CGSizeMake(btnHeight+13, 20);
    
    
    CGFloat tSpace = (mainWidth/3-btnHeight)/2;
    
    
    
    
    
    [myFriends mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(tSpace);
        make.top.mas_equalTo(self).offset(topSpace);
        make.size.mas_equalTo(buttonSize);
    }];
    [myFriendsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(tSpace-6);
        make.top.mas_equalTo(myFriends.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    
    [qq mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(mainWidth/3+tSpace);
        make.top.mas_equalTo(self).offset(topSpace);
        make.size.mas_equalTo(buttonSize);
    }];
    [qqLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(mainWidth/3+tSpace-6);
        make.top.mas_equalTo(qq.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    
    
    
    [qqQzone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(qqLabel.mas_bottom).offset(topSpace);
        make.left.mas_equalTo(qq.mas_left);
        make.size.mas_equalTo(buttonSize);
    }];
    [qzoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(qqLabel);
        make.top.mas_equalTo(qqQzone.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    
    [weixin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(mainWidth/3*2+tSpace);
        make.top.mas_equalTo(self).offset(topSpace);
        make.size.mas_equalTo(buttonSize);
    }];
    [weixinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(mainWidth/3*2+tSpace-6);
        make.top.mas_equalTo(weixin.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    
    
    
    [myFriendcicles mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(myFriendsLb.mas_bottom).offset(topSpace);
        make.left.mas_equalTo(myFriends.mas_left);
        make.size.mas_equalTo(buttonSize);
        
    }];
    
    [myFriendciclesLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(myFriendsLb);
        make.top.mas_equalTo(myFriendcicles.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    [friend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weixinLabel.mas_bottom).offset(topSpace);
        make.left.mas_equalTo(weixin.mas_left);
        make.size.mas_equalTo(buttonSize);
        
    }];
    
    [friendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weixinLabel);
        make.top.mas_equalTo(friend.mas_bottom).offset(topSpace/3);
        make.size.mas_equalTo(labelSize);
    }];
    
    
    
    UIButton *bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomBtn setTitle:@"取 消" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bottomBtn addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    bottomBtn.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:bottomBtn];
    
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(friendLabel.mas_bottom).offset(topSpace/2);
        make.height.mas_equalTo(40);
    }];
}
- (void)dismissAction {
    
    [self dismiss];
}

/*
 link = "http://txzy.xsy.dsceshi.cn/mobile/product/details?qrtype=product&id=2&userid=212";
 logo = "/upload/product/888888/thumb_150_150_201704141530585103.jpg";
 qrcode = "/upload/qrcode/product/212/94ccb9d1fc0a5469d202c471acd0328e.png";
 */

- (void)linkButtonAction {

    [UIPasteboard generalPasteboard].string = self.shareModel.link;
    [XSTool showToastWithView:self Text:Localized(@"copy_success")];
}



- (void)qrCodeButton {
  
    [self dismiss];
    XSShowQRCodeController *vc= [XSShowQRCodeController new];
    
    vc.qrCodelImage = self.shareModel.qrcode;
    
    [_viewController.navigationController pushViewController:vc animated:YES];
    
}




- (void)qqAction {
    [self dismiss];
    
    if (![TencentOAuth iphoneQQInstalled]) {
        
        [XSTool showToastWithView:self.viewController.view Text:@"请移步App Store去下载腾讯QQ客户端"];
    }else {
        NSString *title = self.shareModel.title;
        NSString *content = self.shareModel.content;
        
        // 防止qq自动拼接appinstall=0
        NSMutableString *mutStr = [[NSMutableString alloc] initWithString:self.shareModel.link];
        if ([self.shareModel.link containsString:@"#"]) {
            NSRange range = [mutStr rangeOfString:@"#"];
            [mutStr insertString:@"?" atIndex:range.location];
        }
        QQApiNewsObject *newsObj = [QQApiNewsObject
                                    objectWithURL:[NSURL URLWithString:mutStr]
                                    title:![title isEqualToString:@""] ? title:Localized(@"标题缺失")
                                    description:![content isEqualToString:@""]?content:Localized(@"内容缺失")
                                    previewImageData:self.shareModel.imageData];
        SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
        [QQApiInterface sendReq:req];
        
        
        
    }
}

- (void)qqQzoneAction
{
    [self dismiss];
    
    if (![TencentOAuth iphoneQQInstalled]) {
        
        [XSTool showToastWithView:self.viewController.view Text:@"请移步App Store去下载腾讯QQ客户端"];
    }else {
        NSString *title = self.shareModel.title;
        NSString *content = self.shareModel.content;
        
        
        QQApiNewsObject *newsObj = [QQApiNewsObject
                                    objectWithURL:[NSURL URLWithString:self.shareModel.link]
                                    title:![title isEqualToString:@""] ? title:Localized(@"标题缺失")
                                    description:![content isEqualToString:@""]?content:Localized(@"内容缺失")
                                    previewImageData:self.shareModel.imageData];
        SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
        [QQApiInterface SendReqToQZone:req];
        
    }
    
}


- (void)myFriendsAction{
    [self dismiss];

    UUMessageShareModel *model = [UUMessageShareModel new];
    model.mTitle = self.shareModel.title;
    model.mDigst = self.shareModel.content;
    model.mIocLink = self.shareModel.logo;
    model.mLink = self.shareModel.link;
    NSString *jsonString = model.mj_JSONString;
    NSDictionary *extDic = @{MSG_TYPE:MESSAGE_SHARE_TYPE,MESSAGE_SHARE_DATA:jsonString};
    // 生成message
#ifdef ALIYM_AVALABLE
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:extDic.mj_JSONString summary:model.mDigst];
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:body];
    trVC.isShare = YES;
    trVC.messageText = model.mDigst;
    [_viewController.navigationController pushViewController:trVC animated:YES];
#elif defined EMIM_AVALABLE
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:model.mDigst];
    EMMessage *message = [[EMMessage alloc] initWithConversationID:nil from:[UserInstance ShardInstnce].uid to:nil body:body ext:extDic];
    
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:message];
    trVC.isShare = YES;
    trVC.messageText = model.mDigst;
    [_viewController.navigationController pushViewController:trVC animated:YES];
#else
    XM_TransModel *transModel = [[XM_TransModel alloc] initDefaultShareText:model.mDigst ext:extDic];
    TranspondListViewController *trVC = [[TranspondListViewController alloc] initWithMessage:transModel];
    trVC.isShare = YES;
    [_viewController.navigationController pushViewController:trVC animated:YES];
#endif
    
    
}

-(void)myFriendCiclesAction
{
    [self dismiss];
//    NSString *messageStr = [self getCurrentText];
//    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,self.shareModel.logo]]];
//    
//    UIImage *messageImage = [UIImage imageWithData:data];
//    AddStatusViewController *add=[[AddStatusViewController alloc] init];
//    add.hidesBottomBarWhenPushed = YES;
//    add.isShare = YES;
//    add.messageText = messageStr;
//    add.image = messageImage;
//    [_viewController.navigationController pushViewController:add animated:YES];
    self.isShareCircle = YES;
    if (self.share.image_url) {
        [self pushAddStatusVC];
    }else{
        
        [self shareInformation:_shareId type:_shareType];
    }
}

- (void) pushAddStatusVC{
    if (!self.addVC) {
        self.addVC =[[AddStatusViewController alloc] init];
        self.addVC .hidesBottomBarWhenPushed = YES;
        self.addVC .isShare = YES;
        self.addVC .share=self.share;
        [_viewController.navigationController pushViewController:self.addVC  animated:YES];
    }
    
}

- (void) friendAction {
    [self dismiss];
    if (![WXApi isWXAppInstalled]) {
        [XSTool showToastWithView:self.viewController.view Text:@"请移步App Store去下载微信客户端"];
        return;
    }
    NSString *title = self.shareModel.title;
    NSString *content = self.shareModel.content;
    UIImage *image = [UIImage imageWithData:self.shareModel.imageData];
    //    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseUrl,self.shareModel.logo]]];
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = content;
    [message setThumbImage:image];
    
    // 多媒体消息中包含的网页数据对象
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    // 网页的url地址
    webpageObject.webpageUrl = self.shareModel.link;
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    
    req.scene = WXSceneTimeline;
    [WXApi sendReq:req];
}

- (void)weixinAction {
    
    [self dismiss];
    if (![WXApi isWXAppInstalled]) {
        [XSTool showToastWithView:self.viewController.view Text:@"请移步App Store去下载微信客户端"];
        return;
    }
    NSString *title = self.shareModel.title;
    NSString *content = self.shareModel.content;
    UIImage *image = [UIImage imageWithData:self.shareModel.imageData];
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = content;
    [message setThumbImage:image];
    
    // 多媒体消息中包含的网页数据对象
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    // 网页的url地址
    webpageObject.webpageUrl = self.shareModel.link;
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    
    req.scene = WXSceneSession;
    [WXApi sendReq:req];


}

- (void)dismiss {
    
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(0, mainHeight, mainWidth, 0);
        self.parentView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.parentView removeFromSuperview];
        [self removeFromSuperview];
        self.window = nil;
    }];
    
}

-(void)onReq:(id)req {
    
}

- (void)onResp:(id)resp {
    
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        //把返回的类型转换成与发送时相对于的返回类型,这里为SendMessageToWXResp
        SendMessageToWXResp *sendResp = (SendMessageToWXResp *)resp;
        
        //使用UIAlertView 显示回调信息
        NSString *str = [NSString stringWithFormat:@"%d",sendResp.errCode];
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"回调信息" message:str delegate:self cancelButtonTitle:Localized(@"material_dialog_default_title") otherButtonTitles:nil, nil];
        [alertview show];
        
        if (sendResp.errCode == WXSuccess) {
            
        }
    }
    if ([resp isKindOfClass:[SendMessageToQQResp class]]) {
        
        SendMessageToQQResp * tmpResp = (SendMessageToQQResp *)resp;
        
        if (tmpResp.type == ESENDMESSAGETOQQRESPTYPE && [tmpResp.result integerValue] == EQQAPISENDSUCESS) {
            
            [XSTool showToastWithView:[UIApplication sharedApplication].keyWindow Text:@"分享成功！"];
        }
    }
}


-(void)isOnlineResponse:(NSDictionary *)response {
    
}

- (UIButton *)creatButtonWithImage:(NSString *)name sel:(SEL)selected{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
    [btn addTarget:self action:selected forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    return btn;
}


- (UILabel *)creatLabelWithTitle:(NSString *)title{
    
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont qsh_systemFontOfSize:13];
    label.text = title;
    [self addSubview:label];
    return label;
}
-(NSString *)getCurrentText
{
    NSString *title = self.shareModel.title;
    NSString *content = self.shareModel.content;
    
    if (title.length <= 0) {
        title = @"";
    }
    if (content.length <= 0) {
        content = @"";
    }
    NSString *messageStr = @"";
    if (title.length > 0 && content.length > 0) {
        messageStr = [NSString stringWithFormat:@"%@\n%@\n%@",title,content,self.shareModel.link];
    }else if(title.length > 0 && content.length <= 0){
        messageStr = [NSString stringWithFormat:@"%@\n%@",title,self.shareModel.link];
    }else if(title.length <= 0 && content.length > 0){
        messageStr = [NSString stringWithFormat:@"%@\n%@",title,self.shareModel.link];
    }else{
        messageStr = [NSString stringWithFormat:@"%@",self.shareModel.link];
    }
    
    return messageStr;
}



@end

@implementation XSShareBaseView
{
    BOOL isFirst;
}
/**
 获取到点击的View
 */
-(UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *subview = [super hitTest:point withEvent:event];
    
    if(UIEventTypeTouches == event.type)
    {
        BOOL touchedInside = subview != self;
        if(!touchedInside)
        {
            for(UIView *s in self.subviews)
            {
                if(s == subview)
                {
                    //touched inside
                    touchedInside = YES;
                    break;
                }
            }
        }
        
        
        if (!touchedInside &&  _touchOutSide && !isFirst) {
            isFirst = YES;
            _touchOutSide();
        }
        
    }
    
    
    return subview;
}


@end
