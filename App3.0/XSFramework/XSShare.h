//
//  XSShare.h
//  App3.0
//
//  Created by mac on 2018/3/23.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface XSSingleShareModel : NSObject
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSString *link;
@property (copy, nonatomic) NSString *logo;
@end

@interface XSShare : NSObject
+ (void)singleShareToWeChat:(NSString *)shareData parentVC:(UIViewController *)vc;
+ (void)singleShareToWeChatCircle:(NSString *)shareData parentVC:(UIViewController *)vc;
+ (void)singleShareToQQ:(NSString *)shareData parentVC:(UIViewController *)vc;
+ (void)singleShareToQZONE:(NSString *)shareData parentVC:(UIViewController *)vc;
@end
