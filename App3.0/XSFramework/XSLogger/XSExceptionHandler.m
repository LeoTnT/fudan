//
//  XSExceptionHandler.m
//  App3.0
//
//  Created by mac on 17/2/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "XSExceptionHandler.h"



@implementation XSExceptionHandler

void UncaughtExceptionHandler(NSException * exception)
{
    NSArray * arr = [exception callStackSymbols];
    NSString * reason = [exception reason];
    NSString * name = [exception name];
    UIViewController *currentViewController;
    @try{
        currentViewController = [XSTool getCurrentVC];
    }@catch (NSException *excepption){
        
    }
    
    
    NSString * url = [NSString stringWithFormat:@"========异常错误报告=====\n 崩溃界面 %@ \n ==\nname:%@\nreason:\n%@\ncallStackSymbols:\n%@ ",NSStringFromClass([currentViewController class]),name,reason,[arr componentsJoinedByString:@"\n"]];

    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:url forKey:CashReport];
    [user synchronize];
    
    DDLogError(@"%@\n\n",url);
}


void upException(NSString *content){
    
    
}

+ (void)setDefaultHandler
{
    NSSetUncaughtExceptionHandler(&UncaughtExceptionHandler);
}

+ (NSUncaughtExceptionHandler *)getHandler
{
    return NSGetUncaughtExceptionHandler();
}

+ (void)TakeException:(NSException *)exception
{
    NSArray * arr = [exception callStackSymbols];
    NSString * reason = [exception reason];
    NSString * name = [exception name];
    NSString * url = [NSString stringWithFormat:@"========异常错误报告========\nname:%@\nreason:\n%@\ncallStackSymbols:\n%@",name,reason,[arr componentsJoinedByString:@"\n"]];
    DDLogError(@"%@",url);
}
@end
