//
//  XSFileLogger.h
//  App3.0
//
//  Created by mac on 17/2/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSFileLogger : NSObject
@property (nonatomic, strong, readwrite) DDFileLogger *fileLogger;

+(XSFileLogger *)sharedManager;

@end
