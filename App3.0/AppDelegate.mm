//
//  AppDelegate.m
//  App3.0
//
//  Created by mac on 17/2/16.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "JASidePanelController.h"
#import "RightPanelVC.h"
#import "EnterViewController.h"
#import "LoginModel.h"
#import "UserInstance.h"
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <AlipaySDK/AlipaySDK.h>
#import "ChatHelper.h"
#import "LaunchIntroductionView.h" // 引导页
#import "PreLoadViewController.h"
#import "UPPaymentControl.h"

#import "TimeConsuming.h"
#import <TencentOpenAPI/QQApiInterface.h>

// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#import "FDFriendController.h"
#import "FDForumController.h"
#import "FDTabSecondController.h"

static NSString * const kRecipesStoreName = @"XMPPCoredataChatContact.sqlite";

@interface AppDelegate () <WXApiDelegate, TencentSessionDelegate,QQApiInterfaceDelegate,BMKGeneralDelegate,JPUSHRegisterDelegate> {
    TencentOAuth *_tencentOAuth;
    BMKMapManager *_mapManager;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [XSExceptionHandler setDefaultHandler];
    self.logger=[[XSFileLogger alloc] init];
    NSString *cashRe = [XSTool getStrUseKey:CashReport];
    if (cashRe) [TimeConsuming cashReport:cashRe];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *xmlData = [[NSData alloc] initWithContentsOfFile:XSPath(@"XSErrorReport.xml")];
        if (xmlData) [TimeConsuming xmlReport:xmlData];
    });
    // 预处理
    [BaseUITool configShopInfor:nil error:nil];


    //    // 向微信注册
//    [WXApi registerApp:WXAPPID];
//    _tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAPPID andDelegate:self];

//    // 百度地图
//    _mapManager = [[BMKMapManager alloc]init];
//    BOOL ret = [_mapManager start:BMKMapKey generalDelegate:self];
//    if (!ret) NSLog(@"manager start failed!");
    

    //注册登录状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateChange:)
                                                 name:KNOTIFICATION_LOGINCHANGE
                                               object:nil];

    // 环信初始化
    EMOptions *options = [EMOptions optionsWithAppkey:EMAPPKEY];
    NSString *apnsCertName = nil;
#if DEBUG
    apnsCertName = @"apns_dev";
#else
    apnsCertName = @"apns";
#endif
    options.apnsCertName = apnsCertName;
    options.enableDeliveryAck = YES;
    [[EMClient sharedClient] initializeSDKWithOptions:options];

    [ChatHelper shareHelper];
    [self copyDefaultStoreIfNecessary];
    [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelWarn];
    [MagicalRecord setupCoreDataStackWithStoreNamed:kRecipesStoreName];

    //状态栏文字颜色默认为黑色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    [self changeRootController];
    
    return YES;
}

//切换根控制器
- (void)changeRootController {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    /** 根控制器 */
    EnterViewController *enter = [[EnterViewController alloc] init];
    UINavigationController* enterNav = [[UINavigationController alloc] initWithRootViewController:enter];
    self.window.rootViewController = enterNav;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
}

- (void) copyDefaultStoreIfNecessary {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:kRecipesStoreName];
    
    // If the expected store doesn't exist, copy the default store.
    if (![fileManager fileExistsAtPath:[storeURL path]]) {
        NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:[kRecipesStoreName stringByDeletingPathExtension] ofType:[kRecipesStoreName pathExtension]];
        
        if (defaultStorePath) {
            NSError *error;
            BOOL success = [fileManager copyItemAtPath:defaultStorePath toPath:[storeURL path] error:&error];
            if (!success) {
                NSLog(@"Failed to install default recipe store");
            }
        }
    }
}

// 极光自定义消息接收
- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary * userInfo = [notification userInfo];
//    NSString *content = [userInfo valueForKey:@"content"];
    NSDictionary *extras = [userInfo valueForKey:@"extras"];
//    NSString *customizeField1 = [extras valueForKey:@"customizeField1"]; //服务端传递的Extras附加字段，key是自己定义的
    if (extras[@"type"]) {
        NSString *type = [NSString stringWithFormat:@"%@", extras[@"type"]];
        if ([type isEqualToString:@"100"]) {
            [[ChatHelper shareHelper] sendSystemPushMessage:userInfo];
        }
    }
}

- (void)onGetNetworkState:(int)iError {
    if (0 == iError) {
        NSLog(@"联网成功");
    }else {
        NSLog(@"onGetNetworkState %d",iError);
    }
}

- (void)onGetPermissionState:(int)iError {
    if (0 == iError) {
        NSLog(@"授权成功");
    }else {
        NSLog(@"onGetPermissionState %d",iError);
    }
}


#pragma mark - login changed
- (void)loginStateChange:(NSNotification *)notification {
    BOOL loginSuccess = [notification.object boolValue];
    if (loginSuccess) {//登陆成功加载主窗口控制器
        //加载申请通知的数据
        self.window.rootViewController = [XSTool enterMainViewController];
    }
    else{//登陆失败加载登陆页面控制器
        [[UserInstance ShardInstnce] logout];
        [ChatHelper shareHelper].replyVC = nil;
        
        EnterViewController *enter = [[EnterViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:enter];
        self.window.rootViewController = navigationController;
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
//    if(viewController == [tabBarController.viewControllers objectAtIndex:0] || viewController == [tabBarController.viewControllers objectAtIndex:1] || viewController == [tabBarController.viewControllers objectAtIndex:3]) {
//        if (isEmptyString([UserInstance ShardInstnce].uid)) {
//            LoginViewController *login = [[LoginViewController alloc] init];
//            XSBaseNaviController *navi = [[XSBaseNaviController alloc] initWithRootViewController:login];
//            [self.window.rootViewController presentViewController:navi animated:YES completion:nil];
//            return NO;
//        }
//    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [application.keyWindow endEditing:YES];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        [[EMClient sharedClient] applicationDidEnterBackground:application];
    UIApplication *app = [UIApplication sharedApplication];
    
    __block UIBackgroundTaskIdentifier bgTask;
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(bgTask != UIBackgroundTaskInvalid) {
                
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(bgTask != UIBackgroundTaskInvalid) {
                
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    });
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    [[EMClient sharedClient] applicationWillEnterForeground:application];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_TIMECHANGE object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"000000==%@",url.scheme);
    // 因为在我们的应用中可能不止实现一种分享途径，可能还有微信啊 微博啊这些，所以在这里最好判断一下。
    // QQAPPID:是你在QQ开放平台注册应用时候的AppID
    if ([url.scheme isEqualToString:WXAPPID]) {
        return [WXApi handleOpenURL:url delegate:self];
    }else if ([url.scheme isEqualToString:[NSString stringWithFormat:@"tencent%@",QQAPPID]]) {
        return [TencentOAuth HandleOpenURL:url];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation {
    NSLog(@"111111==%@",url.scheme);
    if ([url.scheme isEqualToString:WXAPPID]) {
        return [WXApi handleOpenURL:url delegate:self];
    } else if ([url.scheme isEqualToString:[NSString stringWithFormat:@"tencent%@",QQAPPID]]) {
        return [TencentOAuth HandleOpenURL:url];
    } else if ([url.scheme isEqualToString:@"xsyUPPay"]) {
        [[UPPaymentControl defaultControl] handlePaymentResult:url completeBlock:^(NSString *code, NSDictionary *data) {
            
            if([code isEqualToString:@"success"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
            }
            else if([code isEqualToString:@"fail"]) {
                //交易失败
                [XSTool showToastWithView:self.window Text:@"交易失败"];
            }
            else if([code isEqualToString:@"cancel"]) {
                //交易取消
                [XSTool showToastWithView:self.window Text:@"交易已取消"];
            }
        }];
        
        return YES;
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [self alipayBackType:[NSString stringWithFormat:@"%@",resultDic[@"resultStatus"]]];
            
        }];
        
      }
    return YES;
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    if ([url.host isEqualToString:[NSString stringWithFormat:@"pagename=%@", @"friend"]]) {
    //寻找燥友
        NSLog(@"==========寻找燥友");
        FDFriendController *findVC = [[FDFriendController alloc] init];
        findVC.isPresent = YES;
        [self.window.rootViewController presentViewController:findVC animated:YES completion:nil];
    }else if ([url.host isEqualToString:[NSString stringWithFormat:@"pagename=%@", @"dryness"]]) {
        //燥事区
        NSLog(@"==========燥事区");
        FDForumController *zsVC = [[FDForumController alloc] init];
        zsVC.isPresent = YES;
        [self.window.rootViewController presentViewController:zsVC animated:YES completion:nil];
    }else if ([url.host isEqualToString:[NSString stringWithFormat:@"pagename=%@", @"star"]]) {
        //星球燥洞
        NSLog(@"==========星球燥洞");
        FDTabSecondController *wzVC = [[FDTabSecondController alloc] init];
        wzVC.isPresent = YES;
        [self.window.rootViewController presentViewController:wzVC animated:YES completion:nil];
    }else if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic){
            [self alipayBackType:[NSString stringWithFormat:@"%@",resultDic[@"resultStatus"]]];}];
        
    } else if ([url.scheme isEqualToString:[NSString stringWithFormat:@"tencent%@",QQAPPID]]) {
        return [TencentOAuth HandleOpenURL:url];
    } else if ([url.scheme isEqualToString:WXAPPID]){
        
       return  [WXApi handleOpenURL:url delegate:self];
        
    } else if ([url.scheme isEqualToString:[NSString stringWithFormat:@"QQ%@",QQAPPID_16]]) {
        return [QQApiInterface handleOpenURL:url delegate:self];
    } else if ([url.scheme isEqualToString:@"xsyUPPay"]) {
        [[UPPaymentControl defaultControl] handlePaymentResult:url completeBlock:^(NSString *code, NSDictionary *data) {
            
            if([code isEqualToString:@"success"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
            }
            else if([code isEqualToString:@"fail"]) {
                //交易失败
                [XSTool showToastWithView:self.window Text:@"交易失败"];
            }
            else if([code isEqualToString:@"cancel"]) {
                //交易取消
                [XSTool showToastWithView:self.window Text:@"交易已取消"];
            }
        }];
        
        return YES;
    }
    return YES;
}

- (BOOL)verify:(NSString *)resultStr {
    
    //此处的verify，商户需送去商户后台做验签
    return NO;
}

- (void)alipayBackType:(NSString *)sender {
    NSString *title;
    
    if ([sender isEqualToString:@"9000"]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:PAY_SUCCESS_LIST object:nil];
        return;
    }else if ([sender isEqualToString:@"6001"]){
        title = @"您中途取消了支付";
    }else if ([sender isEqualToString:@"4000"]){
        title = @"您的订单支付失败";
    }else if ([sender isEqualToString:@"6002"]){
        title = @"网络连接出错";
    }else{
        title = @"未知错误";
    }
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:Localized(@"prompt") message:title delegate:nil cancelButtonTitle:Localized(@"material_dialog_positive_text") otherButtonTitles:nil];
    
    
    [alertView show];
}

- (void)onResp:(id)resp {
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        //把返回的类型转换成与发送时相对于的返回类型,这里为SendMessageToWXResp
        
        SendAuthResp *aresp = (SendAuthResp *)resp;
       
        if (aresp.errCode == WXSuccess) {
             NSLog(@" - -  - - - - -2resp  =%@----%@",aresp.code,resp);
            [[NSNotificationCenter defaultCenter]postNotificationName:@"WeixinCodeNotification" object:self userInfo:@{@"weiXinReturnCode":aresp.code}];
            
            
        }
    } else if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        NSString *strMsg;
        
        switch (((BaseResp *)resp).errCode) {
            case WXSuccess:
                strMsg = @"支付成功！";
                NSLog(@"支付成功－PaySuccess，retcode = %d", ((BaseResp *)resp).errCode);
                [[NSNotificationCenter defaultCenter]postNotificationName:PAY_SUCCESS_LIST object:nil];
                break;
            case WXErrCodeUserCancel:
                strMsg = [NSString stringWithFormat:@"支付失败！"];
                NSLog(@"错误，retcode = %d, retstr = %@", ((BaseResp *)resp).errCode,((BaseResp *)resp).errStr);
                break;
            case WXErrCodeCommon:
                strMsg = [NSString stringWithFormat:@"支付失败！"];
                NSLog(@"错误，retcode = %d, retstr = %@", ((BaseResp *)resp).errCode,((BaseResp *)resp).errStr);
                break;
            case WXErrCodeSentFail:
                strMsg = [NSString stringWithFormat:@"发送失败！"];
                NSLog(@"错误，retcode = %d, retstr = %@", ((BaseResp *)resp).errCode,((BaseResp *)resp).errStr);
                break;
            case WXErrCodeAuthDeny:
                 strMsg = [NSString stringWithFormat:@"授权失败！"];
                break;
            case WXErrCodeUnsupport:
                 strMsg = [NSString stringWithFormat:@"微信不支持！"];
                break;

        }
        [XSTool showToastWithView:self.window Text:strMsg];
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    if (application.applicationState == UIApplicationStateActive) {
        //  在前台 收到通知
        return;
    }
    if (application.applicationState == UIApplicationStateInactive) {
        // 当应用在后台收到通知
        NSLog(@"local  =%@",notification.userInfo);
        
        //通知之后可以取消对应的通知
    }
    
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

// 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    EMError *error = [[EMClient sharedClient] bindDeviceToken:deviceToken];
    if (error) {
        NSLog(@"%@",error.errorDescription);
    }
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}

// 注册deviceToken失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

#pragma mark- JPUSHRegisterDelegate
// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if (userInfo[@"type"] && [userInfo[@"type"] isEqualToString:@"100"]) {
        [[ChatHelper shareHelper] sendSystemPushMessage:userInfo];
    }
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Require d
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}

#pragma mark 是否横屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if(self.isEable) {
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
