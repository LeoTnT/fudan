# 对应后端版本：release-3.4.2.0

## 目录结构
~~~
│─Prodfile             	Cocoapods配置文件    <br> 
│─Prodfile.lock         Cocoapods配置文件   <br> 
│─README.md             README文件<br> 
│─Pods          		Cocoapods添加的第三方库<br> 
│─App3.0          		代码目录<br> 
│  │─Assets.xcassets    资源文件<br> 
│  │─Class            	主要代码<br> 
│  │  │─MainTab      	主要逻辑代码<br> 
│  │  │  │─Controller   <br> 
│  │  │  │─Model   		<br> 
│  │  │  │─View   		<br> 
│  │  │  └─ ...  	<br> 
│  │  │─Login      		登录模块<br> 
│  │  │─Register      	注册模块<br> 
│  │  │─XMChat          自有聊天模块<br> 
│  │  │─QRCode          二维码快速买单模块<br> 
│  │  │─TILCall         腾讯云音视频模块<br> 
│  │  │─Business        商家版模块<br> 
│  │  │─Call            环信音视频<br> 
│  │  │─Cart            购物车模块<br> 
│  │  │─Chat            聊天主模块<br> 
│  │  │─Contact         联系人模块<br> 
│  │  │─FanCircle       朋友圈模块<br>
│  │  │─Form            订单模块<br>
│  │  │─Mine            个人中心模块<br>
│  │  │─Product         商品模块<br>
│  │  │─SkillsExchange  技能交换模块<br>
│  │  │─Store           店铺模块<br>
│  │  │─Search          搜索模块<br>
│  │  │─Wallet          钱包模块<br>
│  │  └─ ...            更多类库目录<br> 
│  │─Utils        		项目用到的一些工具类<br> 
│  │  └─ ...            更多类库目录<br> 
│  │─XSFramework        自己封装的类<br> 
│  │  └─ ...            更多类库目录<br> 
│  │─Config        		获取后台配置<br> 
│  │─BaseTool        	项目用到的一些工具<br> 
│  │─AppDelegate.h      主入口.h文件<br> 
│  │─AppDelegate.m      主入口.m文件<br> 
│  │─info.plist         项目配置文件<br> 
│  │─Constants.h      	通用常亮放在这里面<br> 
│  │─app3.0-prefix.pch  预编译头文件<br> 
│  │


